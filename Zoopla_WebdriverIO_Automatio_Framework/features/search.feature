Feature: Property search feature with different criterias

    Scenario: Search for a particular property in the house prices search and confirm that it appears as the first result
        Given I am on the homePage page
        And I have accepted the cookies
        And I select the UK house prices & values from House prices section
        When I type particular property address 116 Green Lane, HA6 1AW in the search box
        Then I should see the particular property address 116 Green Lane, HA6 1AW as the first result

    Scenario: Search houses for sale including the key word “garage” and check that results have garages
        Given I am on the homePage page
        And I have accepted the cookies
        And I enter the details in For Sale section and click on Advanced Search
        And I enter in garage in keyboard textbox of Advance Search section
        When I click on Search button
        Then I can see results with keyword garage in it

    Scenario: Save a search for property within 15 minutes drive of SE1 2LH
        Given I am on travel time search page
        And I have accepted the cookies
        When I enter the postcode SE1 2LH in the search box
        And I click on travel time search button
        Then I can see the search results within 15 min drive

