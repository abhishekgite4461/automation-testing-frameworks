Feature: Add email alert feature

  Background:
    Given I am on the signin page
    And I have accepted the cookies
    And I login with janny776@litermssb.com and zoopla123
    And I should see the My Zoopla button on homepage

  Scenario: Register for daily email updates on rental property in London for 1 bed properties between £800 and £1000 per month
    Given I am on homepage and I select To rent section
    When I enter location , min price, max price and bedrooms
    And I click on search button
    Then I can see create email alert link on lisitng page
    And I select the daily email alert option from pop-up
    And Search saved message is displayed