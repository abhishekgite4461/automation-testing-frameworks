Feature: Change the email alert frequency

    Background:
        Given I am on the signin page
        And I have accepted the cookies
        And I login with janny776@litermssb.com and zoopla123
        And I should see the My Zoopla button on homepage

    Scenario: Change the frequency of an existing email update
        Given I am on Alerts & searches section of My Account page
        When there is a saved alert visible
        Then I can change the frequency of the email alert