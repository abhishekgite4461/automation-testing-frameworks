import { Given, When, Then } from 'cucumber';

import SigninPage from '../pageobjects/signin.page';
import HomePage from '../pageobjects/home.page';
import ListingPage from '../pageobjects/listing.page';
import MyAccountPage from '../pageobjects/myaccount.page';
import HousePricesPage from '../pageobjects/houseprices.page';
import TravelTimePage from '../pageobjects/traveltime.page';


const pages = {
    signin: SigninPage,
    myAccount: MyAccountPage,
    homePage: HomePage
}

Given(/^I am on the (\w+) page$/, (page) => {
    pages[page].open()
});

When(/^I login with (.*) and (.+)$/, (username, password) => {
    SigninPage.login(username, password)
});

Then(/^I should see the My Zoopla button on homepage$/, () => {
    (HomePage.myAccount).waitForExist();
});

Then(/^I am on homepage and I select To rent section$/, () => {
    HomePage.clickToRentTab();
});

Then(/^I enter location , min price, max price and bedrooms$/, () => {
    HomePage.insertSearchDetails();
});

Then(/^I click on search button$/, () => {
    HomePage.clickSearchButton();
});

Then(/^I can see create email alert link on lisitng page$/, () => {
    ListingPage.clickCreateEmailAlertLink();
});

Then(/^I select the daily email alert option from pop-up$/, () => {
    ListingPage.selectDailySummaryEmailsOption();
    ListingPage.clickSubmitButton();
});

Given(/^I am on Alerts & searches section of My Account page$/, () => {
    pages['myAccount'].openAlertSection();
});

When(/^there is a saved alert visible$/, () => {
    MyAccountPage.savedAlertIsDisplayed();
});

Then(/^I can change the frequency of the email alert$/, () => {
    MyAccountPage.selectEmailFrequency();
});

Given(/^I select the UK house prices & values from House prices section$/, () => {
    HomePage.selectUKHousePricesAndValues();
});

When(/^I type particular property address (.*) in the search box$/, (address) => {
    HousePricesPage.typeAddressInSearchBox();
    HousePricesPage.clickSearchButton();
});

When(/^I should see the particular property address (.*) as the first result$/, (address) => {
    HousePricesPage.verifyAddress(address);
});

Given(/^I enter the details in For Sale section and click on Advanced Search$/, () => {
    HomePage.insertSearchDetailsForSale();
});

Given(/^I enter in garage in keyboard textbox of Advance Search section$/, () => {
    HomePage.insertAdvancedSearchDetails();
});

When(/^I click on Search button$/, () => {
    HomePage.clickSearchButton();
});

When(/^I can see results with keyword garage in it$/, () => {
   console.log(HomePage.firstResultForHouseSaleText());
   console.log('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
   assert.include(HomePage.firstResultForHouseSaleText(), 'garage', 'The first search result incldues the keyword garage');
});

Given(/^I am on travel time search page$/, () => {
    TravelTimePage.open();
});

When(/^I enter the postcode (.*) in the search box$/, (postcode) => {
    TravelTimePage.typeAddressInSearchBox(postcode);
});

When(/^I click on travel time search button$/, () => {
    TravelTimePage.clickTravelTimeSearchButton();
});

Then(/^I can see the search results within 15 min drive$/, () => {
    assert.equal(TravelTimePage.firstResultsTravelTimeText(), '15 minutes by driving from SE1 2LH', 'The first search result does not include the expected time travel text');
});

When(/^I have accepted the cookies$/, () => {
    if (SigninPage.acceptCookieButton.isDisplayed()) {
        SigninPage.acceptAllCookies();
    }
});

Then(/^(.*) message is displayed$/, (message) => {
    assert.equal(ListingPage.alertSavedMessageText(), message, 'The saved alert message is not displayed');
});

Given(/^I can see the saved searches$/, () => {
    MyAccountPage.savedSearchesIsDisplayed();
});

When(/^I click on the View button of the first saved search$/, () => {
    MyAccountPage.clickViewButtonOnFirstSavedSearch();
});

Then(/^saved search is retireved and displayed on listing page$/, () => {
    ListingPage.listingPageIsDisplayed();
});