Feature: Retrieve the saved searches

 Background:
    Given I am on the signin page
    And I have accepted the cookies
    And I login with janny776@litermssb.com and zoopla123
    And I should see the My Zoopla button on homepage

Scenario: As a user I can retrieve saved searches from My Account
    Given I am on Alerts & searches section of My Account page
    And I can see the saved searches
    When I click on the View button of the first saved search
    Then saved search is retireved and displayed on listing page