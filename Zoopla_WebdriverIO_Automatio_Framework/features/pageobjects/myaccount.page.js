import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class MyAccountPage extends Page {
    /**
     * define selectors using getter methods
     */
    get firstSavedAlert() { return $('.clearfix.top.myaccount-alert-item.type-alert') }
    get emailFrequencyDropDown() { return $('.js-myaccount-alert-frequency.js-check.js-touched') }
    get savedSearches() {return $('.clearfix.top.myaccount-alert-item')}
    get viewButtonFirstSavedSearch() {return $('.icon.icon-search')}

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    savedAlertIsDisplayed() {
        browser.waitUntil(() => {
            return this.firstSavedAlert.isDisplayed();
        }, 10000, 'Email Alert not present');
    }

    selectEmailFrequency() {
        this.emailFrequencyDropDown.selectByVisibleText("Summaries every 3 days");
    }

    savedSearchesIsDisplayed() {
        browser.waitUntil(() => {
            return this.savedSearches.isDisplayed();
        }, 10000, 'Saved searches are not displayed');
    }

    clickViewButtonOnFirstSavedSearch() {
        this.viewButtonFirstSavedSearch.click();
    }

    /**
     * overwrite specifc options to adapt it to page object
     */
    open() {
        return super.open('myaccount');
    }

    openAlertSection() {
        return super.open('myaccount/alerts-searches/');
    }
}

export default new MyAccountPage();
