import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class HousePricesPage extends Page {
    /**
     * define selectors using getter methods
     */
    get searchBox() { return $('#search-input-location') }
    get searchButton() { return $('#search-submit') }
    get firstResultAddressTitle() { return $('/html[1]/body[1]/div[3]/div[2]/div[1]/div[4]/section[1]/div[1]/h3[1]/a[1]') }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */

    typeAddressInSearchBox() {
        this.searchBox.waitForDisplayed({ timeout: 3000, timeoutMsg: "UKHousePricesAndValues search box is not displayed" })
        this.searchBox.doubleClick();
        this.searchBox.setValue('116 Green Lane, HA6 1AW');
    }

    clickSearchButton() {
        this.searchButton.click();
    }

    firstTitleAddressText() {
        browser.waitUntil(() => {
            return this.firstResultAddressTitle.isDisplayed();
        }, 10000, 'Email Alert not present');
        return this.firstResultAddressTitle.getText();
    }

    verifyAddress(address) {
        expect(this.firstTitleAddressText()).to.be.equal(address);
    }


}

export default new HousePricesPage();
