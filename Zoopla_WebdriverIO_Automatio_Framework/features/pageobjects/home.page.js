import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class HomePage extends Page {
    /**
     * define selectors using getter methods
     */
    get myAccount() { return $('//body[@id="bhome"]/div/div/div/div/header/div/div/div[1]/div[1]/ul[1]/li[5]/a[1]') }
    get toRentTab() { return $('#search-tabs-to-rent') }
    get locationInputBox() { return $('#search-input-location') }
    get minPriceDropdown() { return $('#rent_price_min_per_month') }
    get maxPriceDropdown() { return $('#rent_price_max_per_month') }
    get bedsDropdown() { return $('#beds_min') }
    get searchButton() { return $('.search-bottom-right.right') }
    get housePricesNavMenu() { return $('//body[@id="bhome"]/div/div/div/div/header/div/div/div/nav/ul/li[3]') }
    get ukHousePricesAndValues() { return $('//a[contains(text(),"UK house prices & values")]') }
    get advancedSearchOptions() { return $('.search-advanced-toggle.link') }
    get keywordsTextBox() { return $('#keywords') }
    get firstSaleResults() { return $('.listing-results-right.clearfix') }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    myAccountIsVisible() {
        expect(this.myAccount).to.exist;
    }

    clickToRentTab() {
        expect(this.toRentTab).to.exist;
        this.toRentTab.click();
    }

    insertSearchDetails() {
        this.locationInputBox.doubleClick();
        this.locationInputBox.addValue('London, United Kingdom');
        this.bedsDropdown.doubleClick();
        this.minPriceDropdown.selectByVisibleText("£800 pcm");
        this.maxPriceDropdown.selectByVisibleText("£1,000 pcm");
        this.bedsDropdown.selectByVisibleText("1+");
    }

    clickSearchButton() {
        this.searchButton.click();
    }

    selectUKHousePricesAndValues() {
        this.housePricesNavMenu.waitForDisplayed({ timeout: 5000, timeoutMsg: "UKHousePricesAndValues is not displayed" })
        this.housePricesNavMenu.moveTo();
        this.ukHousePricesAndValues.waitForDisplayed({ timeout: 3000, timeoutMsg: "UKHousePricesAndValues is not displayed" })
        this.ukHousePricesAndValues.click();
    }

    insertSearchDetailsForSale() {
        this.locationInputBox.doubleClick();
        this.locationInputBox.addValue('Northwood, United Kingdom');
        this.advancedSearchOptions.click();
    }

    insertAdvancedSearchDetails() {
        this.keywordsTextBox.waitForDisplayed({ timeout: 3000, timeoutMsg: "Keywords text box is not displayed" })
        this.keywordsTextBox.doubleClick();
        this.keywordsTextBox.addValue('garage');
    }

    firstResultForHouseSaleText() {
        return this.firstSaleResults.getText();
    }

    /**
     * overwrite specifc options to adapt it to page object
     */
    open() {
        return super.open('');
    }

}

export default new HomePage();
