import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class ListingPage extends Page {
    /**
     * define selectors using getter methods
     */
    get createEmailAlert() {return $('.zoopla-promo.alert-search-button.maps-buttons-popup ')}
    get dailySummaryEmailsOption() {return $('//label[contains(text(),"Daily summary emails")]')};
    get submitButton() {return $('//button[@name="action:save"]')}
    get alertSavedMessage() {return $('#alerts-saved')}
    get filterResultsButton() {return $('//button[contains(text(),"Filter results")]')}

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    
     createEmailAlertIsDisplayed() {
        browser.waitUntil(() => {
            return this.createEmailAlert.isDisplayed();
        }, 10000, 'Email Alert Link not displayed');
     }

     clickCreateEmailAlertLink() {
        this.createEmailAlertIsDisplayed();
        this.createEmailAlert.click();
     }

     selectDailySummaryEmailsOption() {
        browser.waitUntil(() => {
            return this.dailySummaryEmailsOption.isDisplayed();;
        }, 10000, 'Email Alert popup window not visible');
        this.dailySummaryEmailsOption.click();      
     }

     clickSubmitButton () {
         this.submitButton.click();
     }

     alertSavedMessageText() {
        browser.waitUntil(() => {
            return this.alertSavedMessage.isDisplayed();;
        }, 10000, 'Alert saved message is not displayed');
        return this.alertSavedMessage.getText();
     }

     listingPageIsDisplayed() {
        browser.waitUntil(() => {
            return this.filterResultsButton.isDisplayed();;
        }, 10000, 'Listing Page is not displayed');
     }

}

export default new ListingPage();
