import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class TravelTimePage extends Page {
    /**
     * define selectors using getter methods
     */
    get searchBox() { return $('#search-input-location') }
    get searchButton() { return $('#search-submit') }
    get firstResultAddressTitle() { return $('/html[1]/body[1]/div[3]/div[2]/div[1]/div[4]/section[1]/div[1]/h3[1]/a[1]') }
    get resultsTravelTime() {return $('.travel-time')}

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */

    typeAddressInSearchBox(postcode) {
        this.searchBox.waitForDisplayed({ timeout: 3000, timeoutMsg: "TravelTime search box is not displayed" })
        this.searchBox.doubleClick();
        this.searchBox.setValue(postcode);
    }

    firstResultsTravelTimeText() {
        this.resultsTravelTime.waitForDisplayed({ timeout: 3000, timeoutMsg: "Results section is not displayed" })
        return this.resultsTravelTime.getText();
    }

    clickTravelTimeSearchButton() {
        this.searchButton.click();
    }
    /**
    * overwrite specifc options to adapt it to page object
    */
    open() {
        return super.open('travel-time/?search-section=to-rent');
    }


}

export default new TravelTimePage();
