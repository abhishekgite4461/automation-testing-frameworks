require_relative '../../../lib/core'
require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'site_prism'
# require 'rails_helper'

class Basket < SitePrism::Page
  element :basket_total,"span[id='sc-subtotal-amount-activecart']"

  def get_basket_total
    $basket_total = basket_total.text
  end
end