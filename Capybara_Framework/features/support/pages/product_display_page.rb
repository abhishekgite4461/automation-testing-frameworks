require_relative '../../../lib/core'
require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'site_prism'
# require 'rails_helper'

class Product_Display_Page < SitePrism::Page

  element :add_to_cart,"input[id='add-to-cart-button']"
  element :product_price,"span[id='priceblock_ourprice']"

  def add_product_to_cart
    add_to_cart.click
  end

  def get_product_price
    $price = product_price.text
  end

end