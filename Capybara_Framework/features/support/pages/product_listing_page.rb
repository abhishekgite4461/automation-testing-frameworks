require_relative '../../../lib/core'
require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'site_prism'
# require 'rails_helper'

class Product_Listing_Page < SitePrism::Page

  element :first_product,:xpath, "//*[@id='result_0']/div/div/div/div[2]/div[1]/div[1]/a/h2"

  def verify_if_text_exists(product)
    raise "Error: The first result does not have the word #{product}" unless first_product.text.include? product
  end


end