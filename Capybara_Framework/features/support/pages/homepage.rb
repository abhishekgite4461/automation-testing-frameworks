require_relative '../../../lib/core'
require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'site_prism'
# require 'rails_helper'

class Homepage < SitePrism::Page
  set_url EnvConfig['url']
  element :sign_in_button, "a[id='nav-link-yourAccount']"
  element :username, "input[id='ap_email']"
  element :password, "input[id='ap_password']"
  element :sign_in, "input[id='signInSubmit']"
  element :search_field,  "input[id='twotabsearchtextbox']"
  element :search_icon, "input[value='Go']"


  def signin_customer
    username.set "abhishek.gite446@gmail.com"
    password.set "nexus4461"
  end

  def search_product(product)
    search_field.set product
    search_icon.click
  end
  # set_url_matcher /google.com\/?/
  #
  # element :search_field, "input[name='q']"
  # element :search_button, "button[name='btnK']"
  # elements :footer_links, "#footer a"
  # section :menu, MenuSection, "#gbx3"
end