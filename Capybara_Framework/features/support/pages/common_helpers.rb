require_relative '../../../lib/core'
require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'site_prism'
# require 'rails_helper'

class Common_Helpers < SitePrism::Page
  element :basket,"a[id='nav-cart']"

  def click_basket_button
    basket.click
  end
end