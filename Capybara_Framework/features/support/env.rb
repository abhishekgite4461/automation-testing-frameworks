require_relative '../../lib/core/config'
require_relative '../../lib/core'
# require 'selenium-webdriver'
# require 'capybara'
# require 'cucumber'
# require 'capybara/cucumber'

ProjectConfig = Config.new
RunConfig = ProjectConfig.run_config
EnvConfig = ProjectConfig.environment

# initialize test data
Before do |_scenario|
  create_browser
  # Capybara.register_driver :selenium_chrome do |app|
  #   Capybara::Selenium::Driver.new(app, :browser => :chrome)
  # end
  # Capybara.current_driver = :selenium_chrome
end

After do |scenario|
  # take_screenshot if scenario.failed?
  # close_browser unless RunConfig['browser_leave_open']
  close_browser
end