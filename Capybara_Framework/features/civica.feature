Feature: Shop online at Amazon.co.uk
  As a Amazon user
  I want to Sign in
  So that I can continue my shopping journey

 Scenario: Sign into Amazon.co.uk
    Given Amazon.co.uk is open
    When I click Sign-in
    And enter valid user name password
#    Then I am logged in

  Scenario: Search for product and verify the first result is related to your product
    Given Amazon.co.uk is open and I am logged in
    When I search for "Tombow AirPress Ballpoint Pen Black"
    Then the first result has the word  "Tombow AirPress Ballpoint Pen Black" in it


  Scenario: verify that the original price of the product matches the price seen in the basket
    Given Amazon.co.uk is open and I am logged in
    And I add "Tombow AirPress Ballpoint Pen Black" to my basket
    When I check my basket total
    Then it should match the price of "Tombow AirPress Ballpoint Pen Black"
