
Given(/^Amazon\.co\.uk is open$/) do
  # visit EnvConfig['url']
  @home_page = Homepage.new
  @home_page.load
end

When(/^I click Sign\-in$/) do
  # click_link 'Sign in'
  @home_page.sign_in_button.click
end

And(/^enter valid user name password$/) do
  # fill_in 'ap_email', :with => "abhishek.gite446@gmail.com "
  # fill_in 'ap_password', :with => "Nexus4461"
  # click_button 'signInSubmit'
  @home_page.signin_customer
  @home_page.sign_in.click
end

Then(/^I am logged in$/) do
  # page.should have_content("Hello,")
  expect(page).to have_content ("Hello,")
end

Given(/^Amazon\.co\.uk is open and I am logged in$/) do
  @home_page = Homepage.new
  @home_page.load
  # click_link 'Sign in'
  # fill_in 'ap_email', :with => "velifafaku@minex-coin.com"
  # fill_in 'ap_password', :with => "mysterio"
  # click_button 'signInSubmit'
end


When(/^I search for "([^"]*)"$/) do |product|
  # fill_in 'twotabsearchtextbox', :with => product
  # page.find(:xpath,'//*[@id="nav-search"]/form/div[2]/div').click
  @home_page.search_product(product)
end

Then(/^the first result has the word  "([^"]*)" in it$/) do |product|
  # a = page.first(:xpath, '//*[@id="result_0"]/div/div[4]/div[1]/a/h2').text
  # raise "Error: The first result does not have the word #{product}" unless a.include? "Abhishek"
  @product_listing_page = Product_Listing_Page.new
  @product_listing_page.verify_if_text_exists(product)
end


And(/^I add "([^"]*)" to my basket$/) do |product|
  # fill_in 'twotabsearchtextbox', :with => product
  # page.find(:xpath,'//*[@id="nav-search"]/form/div[2]/div').click
  # page.first(:xpath, '//*[@id="result_0"]/div/div/div/div[1]/div/div/a/img').click
  # $price = page.find_by_id('priceblock_ourprice').text
  # puts $price
  # click_button 'add-to-cart-button'

  @home_page.search_product(product)

  @product_listing_page = Product_Listing_Page.new
  @product_listing_page.first_product.click

  @product_display_page = Product_Display_Page.new
  @product_display_page.get_product_price
  @product_display_page.add_product_to_cart


end

When(/^I check my basket total$/) do
  # click_link 'nav-cart'
  # $basket_total = page.find_by_id('sc-subtotal-amount-activecart').text
  # puts $basket_total

  @common_helpers = Common_Helpers.new
  @common_helpers.click_basket_button

  @basket = Basket.new
  @basket.get_basket_total

end

Then(/^it should match the price of "([^"]*)"$/) do |product|
  raise "The price for the product '#{product}' is '#{$price}' which does not match with the basket total '#{$basket_total}'" unless $price == $basket_total
end