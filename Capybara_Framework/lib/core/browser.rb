require 'selenium-webdriver'
require 'capybara'
require 'cucumber'
require 'capybara/cucumber'

def create_browser
  Capybara.register_driver :selenium_chrome do |app|
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
  end
  Capybara.current_driver = :selenium_chrome
end


def close_browser
  Capybara.current_session.driver.quit
end