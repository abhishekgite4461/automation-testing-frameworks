Given("I am on the home page of the app") do
  homepage.search_field_exist #It checks if the search field is displayed
end

When(/^I search for the text '(.*)'$/) do |tag|
  homepage.search_field.click() 
  homepage.search_field.send_keys(tag+"\n") #This step calls the method homepage.search_field and enters the tag
  sleep(2)
  common_helpers.swipe_action(185,372,184,-95) #This step calls the swipe_action method 
end

Then(/^all the App list item titles should match to API endpoint titles '(.*)'$/) do |tag|
    if api_helper.title_extractor(tag) == image_results_page.all_images_titles #This step compares the arrays returned my api_helper.title_extractor(tag) and image_results_page.all_images_titles method, then it assigns a bollean value to titles_matched
       titles_matched =  true
    else
       titles_matched = false
    end
    
    if titles_matched == false #If the titles are not matched then Titles extracted from the App and Titles extracted from API endpoint are printed to the console.
      puts "APP Titles : #{image_results_page.all_images_titles}"
      puts "API Titles : #{api_helper.title_extractor(tag)}"
    end

    raise 'Titles do not match' unless titles_matched == true #If the titles are not matched then it fails the step and raises an text message.
end