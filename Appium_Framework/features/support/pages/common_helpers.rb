def common_helpers
    @common_helpers ||= Common_Helpers.new
end

class Common_Helpers

	def swipe_action(x1,y1,x2,y2) #This method takes Start and Ending coordinates and performs a swipe action
		swipe = Appium::TouchAction.new.swipe(start_x: x1, start_y: y1, offset_x: x2, offset_y: y2)
  		swipe.perform
	end
end