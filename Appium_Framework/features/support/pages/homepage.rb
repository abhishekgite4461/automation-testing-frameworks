def homepage
    homepage ||= Homepage.new
end

class Homepage
  
  def search_field #This method is for the search field element
  	$driver.find_element(:xpath, '//XCUIElementTypeApplication[@name="FlickrBrowser-cal"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther')
  end

  def search_field_exist #If the search_field is not present then this method fails and raises an message
  	raise "Not on homepage" unless search_field.displayed?
  end
end