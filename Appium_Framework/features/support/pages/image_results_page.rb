def image_results_page
   @image_results_page ||= Image_Results_Page.new
end

class Image_Results_Page
  
  def all_images #This method represents path to all the images in the app
  	@driver.find_elements(:xpath, '//XCUIElementTypeApplication[@name="FlickrBrowser-cal"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell')
  end

  def all_images_titles #This method extract all the titles of the images displayed on the image results page
  	app_titles = []
	  i = 1

	  while i <= all_images.count.to_i do #This While loop executes till the number of images displayed in the app
		    h1 = @driver.find_element(:xpath, '//XCUIElementTypeApplication[@name="FlickrBrowser-cal"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell['+i.to_s+']/XCUIElementTypeOther/following-sibling::*').value #This stores the value (Title) of the given xpath
	      	if h1 == nil #If there is no image title then it is displayed as nil, but in the API endpoint it is displayed as blank. So this If statement check if the image title is nil then push the emty quotes in the resulting array
	           app_titles.push("")
	        else
	           app_titles.push(h1)
	        end
	        i=i+1
	  end
	  return app_titles
  end

end