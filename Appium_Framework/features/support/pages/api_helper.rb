def api_helper
    @api_helper ||= Api_Helper.new
end

class Api_Helper
	def title_extractor(tag) #This method takes tag as an input and returns the titles extarcted from the API endpoint
	    api_titles = []
		resp = Net::HTTP.get_response(URI.parse(EnvConfig['exampleapi']+tag))
		data = resp.body
		data_hash = JSON.parse(data) #All the raw data from the API endpoint is stored in data_hash
		puts "££££££££££££££££££££££££££££££££££££££££££££££"
		puts data_hash
		puts "££££££££££££££££££££££££££££££££££££££££££££££"
		h = 0
		while h < data_hash["items"].count #The while loop executes till the number of items returned by the endpoint
			api_titles.push(data_hash["items"][h]["title"]) #This step extarct the title from each item and then push it into api_titles
			h = h + 1
		end
    	return api_titles
	end
end