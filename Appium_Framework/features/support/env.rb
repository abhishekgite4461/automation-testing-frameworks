require 'appium_lib'
require_relative '../../lib/core/config'

ProjectConfig = Config.new
RunConfig = ProjectConfig.run_config 
EnvConfig = ProjectConfig.environment

#Before code executed before start of every scenario
Before do | scenario |
  caps = {
     caps: {
            "platformName": EnvConfig['platformName'],
            "app": EnvConfig['app_path'],
            "deviceName": EnvConfig['deviceName'],
            "automationName": EnvConfig['automationName'],
            "platformVersion": EnvConfig['platformVersion']
            }
         }

  @driver = Appium::Driver.new(caps)
  @driver.start_driver
end

#After code executed after a scenario is finished executing
After do | scenario |
  @driver.driver_quit if RunConfig['app_leave_open'] == false
end
