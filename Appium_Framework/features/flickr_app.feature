Feature: Flickr App Images
  As a Flickr App user
  I want to search images
  So that I can compare it with titles returned by API endpoint

  Scenario Outline: Match Titles
    Given I am on the home page of the app
    When I search for the text '<Tag>'
    Then all the App list item titles should match to API endpoint titles '<Tag>'

  Examples:
  	| Tag 	 |
  	| London |
    # | NoTagNoTagNoTagNoTagNoTag | 	