require 'yaml'

# Class to load Project and Environment configuration files
class Config
  attr_reader :config, :environment, :run_config

  def initialize
    @config = YAML.safe_load(ERB.new(File.read(File.join(Dir.pwd, 'config/config.yml'))).result)
    @run_config = @config['global']
    environment = ENV['ENV'] || @config['global']['environment']
    @environment = YAML.safe_load(ERB.new(File.read(File.join(Dir.pwd, 'environments/environments.yml'))).result)[environment]
   end
end