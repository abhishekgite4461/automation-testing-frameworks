Feature: Checkout with selected payment type
  As a basic user
  I should be able to select the payment type Bank-Transfer/Cheque
  So that I can place an order successfully


  Scenario: Place an Order with Payment type 'Bank Transfer'
    Given I have a product in the basket
    When  I login and proceed to the payment page
    And   I select the payment method Bank-Transfer
    Then  I can see the order confirmation page and order is confirmed


  Scenario: Place an Order with Payment type 'Pay by Cheque'
    Given I have a product in the basket
    When  I login and proceed to the payment page
    And   I select the payment method Pay by Cheque
    Then  I can see the order confirmation page and order is confirmed