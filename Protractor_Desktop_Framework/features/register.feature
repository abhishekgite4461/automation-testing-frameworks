Feature: Register an account
  As a basic user
  I should be able to register a new account
  So that I can place an order


Scenario Outline: Register a User
Given I go to "http://automationpractice.com/index.php"
And   I click on Sign-In button
When  I enter email-address "<email>" and click on Create-an-Account button
And   I enter following details: 'First Name':"<firstname>", 'Last name':"<lastname>", 'Password':"<password>", 'Address':"<address>", 'City':"<city>", 'Zip Code':"<zip>", 'Mobile phone':"<mobile>", 'State':"<state>"
Then  I should see the welcome message

Examples:
|email         |firstname|lastname|password|address           |city     |zip   |mobile      |state    |
|test@gmail.com|FirstName|LastName|12345678|21 Hillary Avenue |New York |10001 |07708923125 |New York |