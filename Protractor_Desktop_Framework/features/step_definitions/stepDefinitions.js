var {defineSupportCode} = require('cucumber');
var homepage = require('../pages/homePage.js');
var proddesc = require('../pages/productDescPage.js');
var signinpage = require('../pages/signinPage.js');
var myaccount = require('../pages/myAccount.js');
var registeraccount = require('../pages/registerAccount.js');
var cartpage = require('../pages/cartPage.js');
var addresspage = require('../pages/addressPage.js');
var shippingpage = require('../pages/shippingPage.js');
var paymentpage = require('../pages/paymentPage.js');
var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);
var expect = chai.expect;

defineSupportCode(function({Given, When, Then}) {
  Given('I go to {stringInDoubleQuotes}', {timeout: 20 * 1000}, function (site, next) {
    browser.waitForAngularEnabled(false);
    homepage.go(site); //This access the 'go' method from homePage.rb and visits the URL specified
    expect(browser.getTitle()).to.eventually.equal('My Store').and.notify(next); //This step compares the title of the current page to verify we are on the correct page
  });

  When('I search for {stringInDoubleQuotes}', {timeout: 20 * 1000}, function (text, next) {
        homepage.homePage.searchBar.sendKeys(text); // This steps accesses the locator for search bar from homePage.rb and types the given text
        homepage.homePage.searchButton.click(); // This steps accesses the locator for search button from homePage.rb and clicks on it
        next();
  });

  Then('I should see the correct product listing page', {timeout: 20 * 1000}, function (next) {
        expect(proddesc.prodDesc.searchedTitle.getText()).to.eventually.equal('"DRESS"').and.notify(next); //This step compares the searched text title to verify we are on the correct results page
  });


  Given('I click on Sign-In button',{timeout: 20 * 1000}, function (next) {
       //Following if statement checks if a user ia already logged in, if yes then it logsout the user
      homepage.homePage.signOutButton.isPresent().then(function(result) {
          if (result) {
              homepage.homePage.signOutButton.click();
          }
      });

      browser.wait(protractor.ExpectedConditions.presenceOf(homepage.homePage.signInButton), 5000, 'SignIn Button taking too long to appear in the DOM');
      homepage.homePage.signInButton.click(); // This steps accesses the locator for signin button from homePage.rb and clicks on it
      next();
  });

  When('I enter email-address {stringInDoubleQuotes} and click on Create-an-Account button',{timeout: 20 * 1000}, function (email, next) {
        signinpage.signinPage.emailCreate.sendKeys(Number(new Date())+email); //This will insert with number of milliseconds since January 1,1970 and append it with the email id specified. So that everytime a new account will be created
        signinpage.signinPage.submitCreate.click(); // This steps accesses the locator for submit button from signIn.rb and clicks on it
        expect(browser.getTitle()).to.eventually.equal('Login - My Store').and.notify(next);
  });

  When('I enter following details: \'First Name\':{stringInDoubleQuotes}, \'Last name\':{stringInDoubleQuotes}, \'Password\':{stringInDoubleQuotes}, \'Address\':{stringInDoubleQuotes}, \'City\':{stringInDoubleQuotes}, \'Zip Code\':{stringInDoubleQuotes}, \'Mobile phone\':{stringInDoubleQuotes}, \'State\':{stringInDoubleQuotes}',{timeout: 20 * 1000}, function (first_name, last_name, password, address, city, zip_code, mobile, state, next) {
      browser.wait(protractor.ExpectedConditions.presenceOf( registeraccount.registerAccount.firstName), 5000); //This step waits for the locator firstName to appear
      //Following steps access the locators from registerAccount and fill in the details provided
      registeraccount.registerAccount.firstName.sendKeys(first_name);
      registeraccount.registerAccount.lastName.sendKeys(last_name);
      registeraccount.registerAccount.password.sendKeys(password);
      registeraccount.registerAccount.address1.sendKeys(address);
      registeraccount.registerAccount.city.sendKeys(city);

      browser.wait(protractor.ExpectedConditions.visibilityOf(registeraccount.registerAccount.postCode), 5000);
      browser.executeScript("arguments[0].scrollIntoView();", registeraccount.registerAccount.postCode.getWebElement()); // This step scrolls to the locator postCode
      registeraccount.registerAccount.postCode.sendKeys(zip_code);

      registeraccount.stateOption(state); //This step access the method stateOption from registerAccount and select the state from the dropdown based on the specified state
      registeraccount.registerAccount.mobile.sendKeys(mobile);
      registeraccount.registerAccount.submitButton.click();
      next();

  });

  Then('I should see the welcome message', {timeout: 20 * 1000}, function (next) {
      browser.wait(protractor.ExpectedConditions.presenceOf(myaccount.myAccount.infoAccountMsg), 5000); //This step waits for the locator infoAccountMsg from myAccount.rb to appear
      expect(myaccount.myAccount.infoAccountMsg.getText()).to.eventually.equal('Welcome to your account. Here you can manage all of your personal information and orders.').and.notify(next);
  });


  When('I enter email-address {stringInDoubleQuotes} and password {stringInDoubleQuotes}', {timeout: 20 * 1000}, function (email, password, next) {
        signinpage.signinPage.emailInput.sendKeys(email);
        signinpage.signinPage.passwordInput.sendKeys(password);
        signinpage.signinPage.submitLoginButton.click();
        next();
  });


  Given('I have a product in the basket',{timeout: 20 * 1000}, function (next) {
      browser.waitForAngularEnabled(false);
      homepage.homePage.signOutButton.isPresent().then(function(result) {
          if (result) { homepage.homePage.signOutButton.click(); }
      });
      homepage.go('http://automationpractice.com/index.php?id_product=1&controller=product');
      proddesc.prodDesc.addToCart.click(); // This step access the locator addToCart from prodDesc and clicks on it
      browser.wait(protractor.ExpectedConditions.visibilityOf(proddesc.prodDesc.popUp_ProceedToCheckout), 5000); //This step and subsequent steps wait for the locator popUp_ProceedToCheckout, scroll to it and click on it
      browser.executeScript("arguments[0].scrollIntoView();", proddesc.prodDesc.popUp_ProceedToCheckout.getWebElement());
      proddesc.prodDesc.popUp_ProceedToCheckout.click();
      expect(browser.getCurrentUrl()).to.eventually.equal('http://automationpractice.com/index.php?controller=order').and.notify(next); // This step compares the current URL is requal to the given URL or not
  });


  When('I login and proceed to the payment page',{timeout: 20 * 1000}, function (next) {
      cartpage.cartPage.proceedToCheckout.click(); //This step access the locator proceedToCheckout from cartPage and clicks on it
      signinpage.signinPage.emailInput.sendKeys('testyoti@gmail.com');
      signinpage.signinPage.passwordInput.sendKeys('123456');
      signinpage.signinPage.submitLoginButton.click();
      addresspage.addressPage.proceedToCheckoutAddressPage.click(); //This step access the locator proceedToCheckoutAddressPage from addressPage and clicks on it
      shippingpage.shippingPage.checboxTerms.click(); //This step access the locator checkboxTerms from shippingPage and clicks on it
      browser.wait(protractor.ExpectedConditions.visibilityOf(shippingpage.shippingPage.proceedToCheckoutShippingPage), 5000);
      shippingpage.shippingPage.proceedToCheckoutShippingPage.click(); //This step access the locator proceedToCheckoutShippingPage from shippingPage and clicks on it
      expect(paymentpage.paymentPage.pageHeading.getText()).to.eventually.equal('PLEASE CHOOSE YOUR PAYMENT METHOD').and.notify(next); //This step compares the payment page heading with the heading specified
  });


  When('I select the payment method Bank-Transfer',{timeout: 20 * 1000}, function (next) {
        paymentpage.paymentPage.bankwirePaymentMethod.click(); //This steps access the locator bankwirePaymentMethod from paymentPage and clicks on it
        expect(browser.getCurrentUrl()).to.eventually.equal('http://automationpractice.com/index.php?fc=module&module=bankwire&controller=payment'); // This step compares the URL with specified URL
        paymentpage.paymentPage.confirmMyOrder.click(); //This step access the locator confirmMyOrder from paymentPage and clicks on it
        next();
  });


  Then('I can see the order confirmation page and order is confirmed',{timeout: 20 * 1000}, function (next) {
        expect(browser.getTitle()).to.eventually.equal('Order confirmation - My Store').and.notify(next); //This steps confirm the title of a successfull order with the given title
  });


  When('I select the payment method Pay by Cheque',{timeout: 20 * 1000}, function (next) {
       paymentpage.paymentPage.chequePaymentMethod.click(); //This steps access the locator chequePaymentMethod from paymentPage and clicks on it
       expect(browser.getCurrentUrl()).to.eventually.equal('http://automationpractice.com/index.php?fc=module&module=cheque&controller=payment'); // This step compares the URL with specified URL
       paymentpage.paymentPage.confirmMyOrder.click(); //This step access the locator confirmMyOrder from paymentPage and clicks on it
       next();
  });

  Then('I should see the alert error message {stringInDoubleQuotes}',{timeout: 20 * 1000}, function (alert, next) {
      // Following code first checks if the alertSection is present, if YES then it compares the alert text with the alert text specified
      signinpage.signinPage.alertSection.isPresent().then(function(result) {
            if (result) {
                expect(signinpage.signinPage.alertText.getText()).to.eventually.equal(alert).and.notify(next);
            }
      });
  });

});