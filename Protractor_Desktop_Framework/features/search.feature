Feature: Searching catalog on website
  As a basic user
  I should be able to search the catalog on the website
  So that I can browse the catalog

  Scenario: Visit the site and search for a product
    Given I go to "http://automationpractice.com/index.php"
    When  I search for "Dress"
    Then  I should see the correct product listing page