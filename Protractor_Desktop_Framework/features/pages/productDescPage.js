'use strict';
module.exports = {

    prodDesc: {
        searchedTitle: element(by.css('[class="lighter"]')),
        addToCart: element(by.id('add_to_cart')),
        popUp_ProceedToCheckout: element(by.css('[title="Proceed to checkout"]'))
    }
};
