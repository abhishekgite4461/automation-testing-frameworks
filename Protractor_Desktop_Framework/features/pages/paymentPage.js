'use strict';
module.exports = {

    paymentPage: {
        pageHeading: element(by.css('[class="page-heading"]')),
        bankwirePaymentMethod: element(by.css('[class="bankwire"]')),
        chequePaymentMethod: element(by.css('[class="cheque"]')),
        confirmMyOrder: element(by.css('[class="button btn btn-default button-medium"]'))
    }
};