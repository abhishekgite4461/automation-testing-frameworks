'use strict';
module.exports = {

    signinPage: {
        emailInput: element(by.id('email')),
        passwordInput: element(by.id('passwd')),
        submitLoginButton: element(by.id('SubmitLogin')),
        alertSection: element(by.css('[class="alert alert-danger"]')),
        alertText: element(by.xpath('//*[@id="center_column"]/div[1]/ol/li')),
        emailCreate: element(by.id('email_create')),
        submitCreate: element(by.id('SubmitCreate'))
    }
};
