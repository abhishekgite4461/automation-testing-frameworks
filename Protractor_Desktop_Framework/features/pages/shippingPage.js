'use strict';
module.exports = {

    shippingPage: {
       checboxTerms: element(by.css('[class="checker"]')),
       proceedToCheckoutShippingPage: element(by.css('[class="button btn btn-default standard-checkout button-medium"]'))
    }
};