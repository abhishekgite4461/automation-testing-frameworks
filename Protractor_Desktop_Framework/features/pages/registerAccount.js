'use strict';
module.exports = {

    registerAccount: {
        firstName: element(by.id('customer_firstname')),
        lastName: element(by.id('customer_lastname')),
        password: element(by.id('passwd')),
        address1: element(by.id('address1')),
        city: element(by.id('city')),
        postCode: element(by.css('[id="postcode"]')),
        mobile: element(by.id('phone_mobile')),
        submitButton: element(by.id('submitAccount'))
    },

    stateOption: function(state) {
      var angular = this.registerAccount;
        element(by.cssContainingText('option',state)).click();
    }
};