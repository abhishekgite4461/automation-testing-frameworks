'use strict';
module.exports = {

    cartPage: {
        proceedToCheckout: element(by.css('[class="button btn btn-default standard-checkout button-medium"]'))
    }
};