'use strict';
module.exports = {

  homePage: {
    searchBar:  element(by.id('search_query_top')),
    searchButton: element(by.css('[class="btn btn-default button-search"]')),
    signInButton: element(by.css('[class="login"]')),
    signOutButton: element(by.css('[class="logout"]'))
  },

  go: function(site) {
    browser.get(site);
  }
};
