Feature: Sign-In with valid and Invalid credentials
  As a basic user
  I should be able to Sign-In in the given account with valid credentials
  So that I can place an order and access my account


  Scenario Outline: Sign-In with valid credentials
    Given I go to "http://automationpractice.com/index.php"
    And   I click on Sign-In button
    When  I enter email-address "<email>" and password "<passwd>"
    Then  I should see the welcome message

    Examples:
      |email             |passwd|
      |testyoti@gmail.com|123456|

 #Negative-Scenario
  Scenario: Sign-In with invalid user login-id and invalid password
    Given I go to "http://automationpractice.com/index.php"
    And   I click on Sign-In button
    When  I enter email-address "test213testtest@gmail.com" and password "123456testtest"
    Then  I should see the alert error message "Authentication failed."

 #Negative-Scenario
  Scenario: Sign-In with valid user login-id and invalid password
    Given I go to "http://automationpractice.com/index.php"
    And   I click on Sign-In button
    When  I enter email-address "testyoti@gmail.com" and password "123"
    Then  I should see the alert error message "Invalid password."