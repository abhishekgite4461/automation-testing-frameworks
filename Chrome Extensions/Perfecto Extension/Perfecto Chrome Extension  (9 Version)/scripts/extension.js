    //Function to Request from Perfecto API
    async function perfectoRequest(url) {
        let response = await fetch(url, {
            method: 'get',
            headers: {
            "PERFECTO_AUTHORIZATION": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI1ZjNmZjU5MC1mZjE0LTRmNjYtYTZkYy0xNGE0ODVjOWJmZDQifQ.eyJqdGkiOiI0ZTg2NDliZC05YTk2LTQwNjAtODFhMi1lYTcyY2E4OGQ1ODIiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTg2NzEwNzcxLCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL2xsb3lkcy1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL2xsb3lkcy1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI5MmQ5YjU5Zi1iZmZlLTQ1ODktODJkYy0yYTFkYWE2ZDJlNGQiLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiIxZWQ3YjAzZC1mZjYyLTQ0NzYtOTE5MS1kZDU4ZGFiNGUxMzEiLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoib2ZmbGluZV9hY2Nlc3MifQ.-eB5oaMIWkPwIhS-016XTUiaaR99gPlJCXbjLTvL_b4",
            "cache-control": "no-cache",
            }
        });
        return await response.json();
    }

    //Function to get daily fail/pass in percentage
    function getPercentage(partialValue, totalValue) {
        if((partialValue < 1)|| (totalValue < 1)){
            return " ";
           }else{
            return ((100 * partialValue) / totalValue).toFixed(0);
           }
    }

    function AddZero(num) {
        return (num >= 0 && num < 10) ? "0" + num : num + "";
    }

    function currentEpochTime() {
        let now = new Date();
        return now;
    }

    //Function to get Timestamp
    function getTimeStamp(epochTime) {
        return [[AddZero(epochTime.getDate()), 
                AddZero(epochTime.getMonth() + 1), 
                epochTime.getFullYear()].join("/"), 
                [AddZero(epochTime.getHours()), 
                AddZero(epochTime.getMinutes())].join(":"), 
                epochTime.getHours() >= 12 ? "PM" : "AM"].join(" ");
    }

    //Function to get daily change in percentage as compared to yesterday
    function getPercentageChange(yesterdaysNumber, TodaysNumber){
        var res=(TodaysNumber-yesterdaysNumber)/yesterdaysNumber*100.0;
        return Math.floor(res);
    }

    //Function to get current workday in a specific format
    function getCurrentWorkday(){
        return moment().format('YYYY-MM-DDT03:00:00+00:00')
    }

    //Function to get previous workday
    function getPreviousWorkday(){
        let workday = moment();
        let day = workday.day();
        let diff = 1;  // This will return Yesterday
        if (day == 0 || day == 1){  // It will check if its Sunday or Monday
          diff = day + 2;  // It will return Friday
        }
        return workday.subtract(diff, 'days').format('YYYY-MM-DDT03:00:00+00:00');
    }

    //Function to check increase/decrease in fail/pass as compared to yesterday
    function checkIncreaseOrDecrease(value) {
        if (value > 0 || value == 0) {
            return `${value}% &#9650`;
        } else {
            return `${value}% &#9660`
        }
    }

    //Function to get dynamic url based on status, tag, platform, startTime, endTime
    function getUrlEndpoint(status, tag, platform, type, startTime, endTime) {
        if (platform == 'Android') {
            if (status == "BOTH") {
                if (tag == "ALL") {
                    return 'https://lloyds.reporting.perfectomobile.com/export/api/v1/test-executions?jobName[0]=nga-3-aft-weeknightMaster'+platform+''+type+'&startExecutionTime[0]='+startTime+'&endExecutionTime[0]='+endTime+'';
                } else {
                    return 'https://lloyds.reporting.perfectomobile.com/export/api/v1/test-executions?jobName[0]=nga-3-aft-weeknightMaster'+platform+''+type+'&tags[0]='+tag+'&startExecutionTime[0]='+startTime+'&endExecutionTime[0]='+endTime+'';
                }
            } else {
                return 'https://lloyds.reporting.perfectomobile.com/export/api/v1/test-executions?jobName[0]=nga-3-aft-weeknightMaster'+platform+''+type+'&tags[0]='+tag+'&status='+status+'&startExecutionTime[0]='+startTime+'&endExecutionTime[0]='+endTime+'';
            }
        } else {
            if (status == "BOTH") {
                if (tag == "ALL") {
                    return 'https://lloyds.reporting.perfectomobile.com/export/api/v1/test-executions?jobName[0]=nga-3-aft-weeknightMaster'+platform+''+type+'&startExecutionTime[0]='+startTime+'&endExecutionTime[0]='+endTime+'';
                } else {
                    return 'https://lloyds.reporting.perfectomobile.com/export/api/v1/test-executions?jobName[0]=nga-3-aft-weeknightMaster'+platform+''+type+'&tags[0]='+tag+'&startExecutionTime[0]='+startTime+'&endExecutionTime[0]='+endTime+'';
                }
            } else {
                return 'https://lloyds.reporting.perfectomobile.com/export/api/v1/test-executions?jobName[0]=nga-3-aft-weeknightMaster'+platform+''+type+'&tags[0]='+tag+'&status='+status+'&startExecutionTime[0]='+startTime+'&endExecutionTime[0]='+endTime+'';
            }
        }  
    }

    //Function to get background color for change in percentage
    function getBackgroundColor(result, value) {
        if ((result == "failed" && value > 0) || (result == "passed" && value < 0)) {
            return "red"
        } else {
            return "green";
        };
    }

    function getJobName(name, platform) {
        if (platform == 'Android') {
            return name.split('nga-3-aft-weeknightMaster')[1];
        } else {
            return name.split('nga-3-aft-weeknightMaster')[1];
        };
    }

    //Function to fetch all details sent by the API based on the request
    function getStatsForTheTeam(team, platform, type) {

        const todayStartTime = moment(getCurrentWorkday()).valueOf();
        const todayEndTime = moment(getCurrentWorkday()).add(10, 'hours').valueOf();

        const yesterdaysStartTime = moment(getPreviousWorkday()).valueOf();
        const yesterdaysEndTime = moment(getPreviousWorkday()).add(10, 'hours').valueOf();

        // const todayStartTime = '1591934400000';
        // const todayEndTime = '1591956000000';

        // const yesterdaysStartTime = '1591848000000';
        // const yesterdaysEndTime = '1591869600000';

        console.log("yesterdaysStartTime ABHI : ", + yesterdaysStartTime);
        console.log("yesterdaysEndTime ABHI : ", + yesterdaysEndTime);
        
        console.log("TODAY START TIME " + todayStartTime);
        console.log("TODAY END TIME " + todayEndTime);

        var error404Count = 0;
        var errorElementNotFoundCount = 0;
        var error500Count = 0;
        var error504Count = 0;
        var errorPerfectoCount = 0;
        var errorScriptErrorCount = 0;
        var errorTestDataErrorCount = 0;
        var errorSwitchErrorCount = 0;

        document.getElementById(`${platform.toLowerCase()}Team${type}`).textContent = team.toUpperCase();

        let urlFail = getUrlEndpoint("FAILED",`team-${team}`,platform,type,todayStartTime,todayEndTime);
        let urlPass = getUrlEndpoint("PASSED",`team-${team}`,platform,type,todayStartTime,todayEndTime);
        let urlTotal = getUrlEndpoint("BOTH",`team-${team}`,platform,type,todayStartTime,todayEndTime);
        let url = getUrlEndpoint("BOTH","ALL",platform,type,todayStartTime,todayEndTime);

        let urlYesterdayFail = getUrlEndpoint("FAILED",`team-${team}`,platform,type,yesterdaysStartTime,yesterdaysEndTime);
        let urlYesterdayPass = getUrlEndpoint("PASSED",`team-${team}`,platform,type,yesterdaysStartTime,yesterdaysEndTime);
        let urlYesterdayTotal = getUrlEndpoint("BOTH",`team-${team}`,platform,type,yesterdaysStartTime,yesterdaysEndTime);
        

        Promise.all([perfectoRequest(urlFail),perfectoRequest(urlPass),perfectoRequest(urlTotal),perfectoRequest(url),perfectoRequest(urlYesterdayFail),perfectoRequest(urlYesterdayPass),perfectoRequest(urlYesterdayTotal)]).then(function(values){
            
            if (values[3]["metadata"]["processingStatus"] == "PROCESSING_COMPLETE") {
                FailTotal = values[0]["resources"].length;

                for (let i=0; i<FailTotal; i++) {
                    if (values[0]["resources"][i]["message"].includes("status: 404")) {
                        error404Count += 1;
                    } else if ((values[0]["resources"][i]["message"].includes("was expected to be visible")) || (values[0]["resources"][i]["message"].includes("not found whilst scrolling!")) || (values[0]["resources"][i]["message"].includes("expected false to equal true")) || (values[0]["resources"][i]["message"].includes("No Elements founds for selector")) || (values[0]["resources"][i]["message"].includes("Elements not visible after")) || (values[0]["resources"][i]["message"].includes("An element could not be located on the page")) || (values[0]["resources"][i]["message"].includes("wasn't found")) || (values[0]["resources"][i]["message"].includes("Error: function timed out"))) {
                        errorElementNotFoundCount += 1;
                    } else if (values[0]["resources"][i]["message"].includes("Request failed with 500")) {
                        error500Count += 1;
                    } else if ((values[0]["resources"][i]["message"].includes("Reason: handset server: cannot launch application")) || (values[0]["resources"][i]["message"].includes("Failed to execute command"))) {
                        errorPerfectoCount += 1;
                    } else if ((values[0]["resources"][i]["message"].includes("SCRIPT ERROR")) || (values[0]["resources"][i]["message"].includes("Execute script is not supported")) || (values[0]["resources"][i]["message"].includes("is not a function")) || (values[0]["resources"][i]["message"].includes("AssertionError:")) || (values[0]["resources"][i]["message"].includes("Error: Enum")) || (values[0]["resources"][i]["message"].includes("Error: Screen")) || (values[0]["resources"][i]["message"].includes("Cannot read property"))) {
                        errorScriptErrorCount += 1;
                    } else if (values[0]["resources"][i]["message"].includes("TEST_DATA_ERROR")) {
                        errorTestDataErrorCount += 1;
                    } else if (values[0]["resources"][i]["message"].includes("Request failed with 504:")) {
                        error504Count += 1;
                    } else if (values[0]["resources"][i]["message"].includes("Could not find switch")) {
                        errorSwitchErrorCount += 1;
                    }

                }

                console.log(`${platform} ${type} 404 error count is : ` + error404Count);
                console.log(`${platform} ${type} Element Not Found error count is : ` + errorElementNotFoundCount);
                console.log(`${platform} ${type} 500 error count is : ` + error500Count);
                console.log(`${platform} ${type} PERFECTO error count is : ` + errorPerfectoCount);
                console.log(`${platform} ${type} SCIRPT ERROR  count is : ` + errorScriptErrorCount);
                console.log(`${platform} ${type} TEST_DATA_ERROR count is : ` + errorTestDataErrorCount);
                console.log(`${platform} ${type} 504 count is : ` + error504Count);
                console.log(`${platform} ${type} Switch Error count is : ` + errorSwitchErrorCount);

                if (error404Count == 0) {
                    document.getElementById(`${platform.toLowerCase()}404Count${type}`).style.display = 'none';
                } else {
                    document.getElementById(`${platform.toLowerCase()}404Count${type}`).style.display = 'flex';
                    document.getElementById(`${platform.toLowerCase()}404Count${type}`).textContent = "404 Error: " + error404Count;
                }
                
                if (error500Count == 0) {
                    document.getElementById(`${platform.toLowerCase()}500Count${type}`).style.display = 'none';
                } else {
                    document.getElementById(`${platform.toLowerCase()}500Count${type}`).style.display = 'flex';
                    document.getElementById(`${platform.toLowerCase()}500Count${type}`).textContent = "500 Error: " + error500Count;
                }
    
                if (error504Count == 0) {
                    document.getElementById(`${platform.toLowerCase()}504Count${type}`).style.display = 'none';
                } else {
                    document.getElementById(`${platform.toLowerCase()}504Count${type}`).style.display = 'flex';
                    document.getElementById(`${platform.toLowerCase()}504Count${type}`).textContent = "504 Error: " + error504Count;
                }
                
                if (errorElementNotFoundCount == 0) {
                    document.getElementById(`${platform.toLowerCase()}ElementNotFoundCount${type}`).style.display = 'none';
                } else {
                    document.getElementById(`${platform.toLowerCase()}ElementNotFoundCount${type}`).style.display = 'flex';
                    document.getElementById(`${platform.toLowerCase()}ElementNotFoundCount${type}`).textContent = "Element_Not_Found : " + errorElementNotFoundCount;
                }
                
                if (errorPerfectoCount == 0) {
                    document.getElementById(`${platform.toLowerCase()}PerfectoCount${type}`).style.display = 'none';
                } else {
                    document.getElementById(`${platform.toLowerCase()}PerfectoCount${type}`).style.display = 'flex';
                    document.getElementById(`${platform.toLowerCase()}PerfectoCount${type}`).textContent = "Perfecto Error : " + errorPerfectoCount;
                }
                
                if (errorScriptErrorCount == 0) {
                    document.getElementById(`${platform.toLowerCase()}ScriptErrorCount${type}`).style.display = 'none';
                } else {
                    document.getElementById(`${platform.toLowerCase()}ScriptErrorCount${type}`).style.display = 'flex';
                    document.getElementById(`${platform.toLowerCase()}ScriptErrorCount${type}`).textContent = "Script Error : " + errorScriptErrorCount;
                }
    
                if (errorTestDataErrorCount == 0) {
                    document.getElementById(`${platform.toLowerCase()}TestDataErrorCount${type}`).style.display = 'none';
                } else {
                    document.getElementById(`${platform.toLowerCase()}TestDataErrorCount${type}`).style.display = 'flex';
                    document.getElementById(`${platform.toLowerCase()}TestDataErrorCount${type}`).textContent = "TestData Error : " + errorTestDataErrorCount;
                }
                
                if (errorSwitchErrorCount == 0) {
                    document.getElementById(`${platform.toLowerCase()}SwitchErrorCount${type}`).style.display = 'none';
                } else {
                    document.getElementById(`${platform.toLowerCase()}SwitchErrorCount${type}`).style.display = 'flex';
                    document.getElementById(`${platform.toLowerCase()}SwitchErrorCount${type}`).textContent = "Could Not find switch Error: " + errorSwitchErrorCount;
                }


                PassTotal = values[1]["resources"].length;
                Total = values[2]["resources"].length;
                FailPercentage = " (" + getPercentage(FailTotal,Total) + "%)";
                PassPercentage = " (" + getPercentage(PassTotal,Total) + "%)";
                JobCompleteTime = getTimeStamp(new Date(values[3]["resources"][0]["endTime"]));
                JobName = values[3]["resources"][0]["job"]["name"];

                yesterdayFailTotal = values[4]["resources"].length;
                yesterdayPassTotal = values[5]["resources"].length;
                yesterdayTotal = values[6]["resources"].length;
                FailChangePercentage = checkIncreaseOrDecrease(getPercentageChange(yesterdayFailTotal, FailTotal));
                PassChangePercentage = checkIncreaseOrDecrease(getPercentageChange(yesterdayPassTotal, PassTotal));

                console.log(`yesterdayFailTotal Team-${team} and Platform-${platform} : `, + yesterdayFailTotal);
                console.log(`FailTotal Team-${team} and Platform-${platform} : `, + FailTotal);
                console.log(`Change in FAIL Percentage Team-${team} and Platform-${platform}  : ` +  getPercentageChange(yesterdayFailTotal, FailTotal));
                console.log(`yesterdayPassTotal Team-${team} and Platform-${platform}  : `, + yesterdayPassTotal);
                console.log(`PassTotal Team-${team} and Platform-${platform}  : `, + PassTotal);
                console.log(`Change in PASS Percentage Team-${team} and Platform-${platform}  : ` +  getPercentageChange(yesterdayPassTotal, PassTotal));
                console.log(`yesterdayTotal Team-${team} and Platform-${platform}  : `, + yesterdayTotal);

                document.getElementById(`${platform.toLowerCase()}FailedTotal${type}`).textContent = FailTotal;
                document.getElementById(`${platform.toLowerCase()}FailedPerc${type}`).textContent = FailPercentage;
                document.getElementById(`${platform.toLowerCase()}FailedChangePerc${type}`).innerHTML = FailChangePercentage;
                document.getElementById(`${platform.toLowerCase()}FailedChangePerc${type}`).style.backgroundColor = getBackgroundColor("failed",getPercentageChange(yesterdayFailTotal, FailTotal));
                document.getElementById(`${platform.toLowerCase()}PassedTotal${type}`).textContent = PassTotal;
                document.getElementById(`${platform.toLowerCase()}PassedPerc${type}`).textContent = PassPercentage;
                document.getElementById(`${platform.toLowerCase()}PassedChangePerc${type}`).innerHTML = PassChangePercentage;
                document.getElementById(`${platform.toLowerCase()}PassedChangePerc${type}`).style.backgroundColor = getBackgroundColor("passed",getPercentageChange(yesterdayPassTotal, PassTotal));
                document.getElementById(`${platform.toLowerCase()}Total${type}`).textContent = Total;
                document.getElementById(`${platform.toLowerCase()}JobCompleteTime${type}`).textContent = JobCompleteTime;
                document.getElementById(`${platform.toLowerCase()}Status${type}`).textContent = "Job finished" ;
                document.getElementById(`${platform.toLowerCase()}Status${type}`).style.color = "green";
                document.getElementById(`${platform.toLowerCase()}${type}`).textContent = getJobName(JobName, platform); 

            } else {
                document.getElementById(`${platform.toLowerCase()}Status${type}`).textContent = "Job Still running for today" ;
                document.getElementById(`${platform.toLowerCase()}Status${type}`).style.color = "red";
            };

            $('#timeStamp').html("Last Refreshed : " + getTimeStamp(currentEpochTime()));
            chrome.storage.local.set({'lastRefreshedTime': getTimeStamp(currentEpochTime())});

        });
    }

    //Following will trigger when any teams button is clicked
    document.getElementById('buttonWrapper').addEventListener('click', function (event) {
        console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        console.log(event.target.outerText);
        console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

        if (moment().day() === 1 || moment().day() === 2 || moment().day() === 3 || moment().day() === 4 || moment().day() === 5) {      //This line makes sure the code inside is not executed on a Weekend
            getStatsForTheTeam((event.target.outerText).toLowerCase(), 'Android', 'Generic');
            getStatsForTheTeam((event.target.outerText).toLowerCase(), 'Ios', 'Generic');
            getStatsForTheTeam((event.target.outerText).toLowerCase(), 'Ios', 'NonGeneric');
            getStatsForTheTeam((event.target.outerText).toLowerCase(), 'Android', 'NonGeneric');
            console.log('success');
        } else {
            $('#status').html("Master Job does not run over the weekend");
            $('#status').css({ 'color': 'red', 'font-size': '150%' });
        };
    });
