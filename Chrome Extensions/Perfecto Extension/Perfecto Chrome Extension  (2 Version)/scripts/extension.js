
    
    var button = document.getElementById('changelinks');
    var jobOne = document.getElementById('job1');
    var teamOne = document.getElementById('team1');
    var passedOne = document.getElementById('passed1');
    var failedOne = document.getElementById('failed1');
    var totalOne = document.getElementById('total1');
    var statusOne = document.getElementById('status1');
    var jobCompleteTimeOne = document.getElementById('jobCompleteTime1');
    
    var jobTwo = document.getElementById('job2');
    var teamTwo = document.getElementById('team2');
    var passedTwo = document.getElementById('passed2');
    var failedTwo = document.getElementById('failed2');
    var totalTwo = document.getElementById('total2');
    var statusTwo = document.getElementById('status2');
    var jobCompleteTimeTwo = document.getElementById('jobCompleteTime2');
    
    var jobThree = document.getElementById('job3');
    var teamThree = document.getElementById('team3');
    var passedThree = document.getElementById('passed3');
    var failedThree = document.getElementById('failed3');
    var totalThree = document.getElementById('total3');
    var statusThree = document.getElementById('status3');
    var jobCompleteTimeThree = document.getElementById('jobCompleteTime3');

    chrome.storage.local.get(['aovIosFail', 'aovIosPass', 'aovIosTotal', 'aovIosFailPerc', 'aovIosPassPerc',
    'aovAndroidFail', 'aovAndroidPass', 'aovAndroidTotal', 'aovAndroidFailPerc', 'aovAndroidPassPerc',
    'timeStamp', 'timeStampAndroid','timeStampIos', 'lastRefreshedTime'], function(data) {
        if (data.aovIosFail != undefined && data.aovIosFailPerc != undefined) {
            failedOne.textContent = data.aovIosFail + data.aovIosFailPerc;
        }

        if (data.aovIosPass != undefined && data.aovIosPassPerc != undefined) {
            passedOne.textContent = data.aovIosPass + data.aovIosPassPerc;
        }
        
        totalOne.textContent = data.aovIosTotal;
        jobCompleteTimeOne.textContent = data.timeStampIos;

        if (data.aovAndroidFail != undefined && data.aovAndroidFailPerc != undefined) {
            failedTwo.textContent = data.aovAndroidFail + data.aovAndroidFailPerc;
        }

        if (data.aovAndroidPass != undefined && data.aovAndroidPassPerc != undefined) {
            passedTwo.textContent = data.aovAndroidPass + data.aovAndroidPassPerc;
        }
        totalTwo.textContent = data.aovAndroidTotal;
        jobCompleteTimeTwo.textContent = data.timeStampAndroid;
        
        if (data.lastRefreshedTime != undefined) {
            $('#timeStamp').html("Last Refreshed : " + data.lastRefreshedTime);
        }
      });


    async function perfectoRequest(url) {
        let response = await fetch(url, {
            method: 'get',
            headers: {
            "PERFECTO_AUTHORIZATION": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI1ZjNmZjU5MC1mZjE0LTRmNjYtYTZkYy0xNGE0ODVjOWJmZDQifQ.eyJqdGkiOiI0ZTg2NDliZC05YTk2LTQwNjAtODFhMi1lYTcyY2E4OGQ1ODIiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTg2NzEwNzcxLCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL2xsb3lkcy1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL2xsb3lkcy1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI5MmQ5YjU5Zi1iZmZlLTQ1ODktODJkYy0yYTFkYWE2ZDJlNGQiLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiIxZWQ3YjAzZC1mZjYyLTQ0NzYtOTE5MS1kZDU4ZGFiNGUxMzEiLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoib2ZmbGluZV9hY2Nlc3MifQ.-eB5oaMIWkPwIhS-016XTUiaaR99gPlJCXbjLTvL_b4",
            "cache-control": "no-cache",
            }
        });
        return await response.json();
    }

    function getPercentage(partialValue, totalValue) {
        return ((100 * partialValue) / totalValue).toFixed(0);
    }

    function AddZero(num) {
        return (num >= 0 && num < 10) ? "0" + num : num + "";
    }

    function currentEpochTime() {
        let now = new Date();
        return now;
    }

    function getTimeStamp(epochTime) {
        return [[AddZero(epochTime.getDate()), 
                AddZero(epochTime.getMonth() + 1), 
                epochTime.getFullYear()].join("/"), 
                [AddZero(epochTime.getHours()), 
                AddZero(epochTime.getMinutes())].join(":"), 
                epochTime.getHours() >= 12 ? "PM" : "AM"].join(" ");
    }

    button.addEventListener('click', function () {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        
            const dateToStore = moment().format('YYYY-MM-DDT03:00:00+00:00')
            const todayStartTime = moment(dateToStore).valueOf();
            const todayEndTime = moment(dateToStore).add(9, 'hours').valueOf();
            
            console.log("TODAY START TIME " + todayStartTime);
            console.log("TODAY END TIME " + todayEndTime);

            function getUrlEndpoint(status, tag, platform) {
                if (status == "BOTH") {
                    return 'https://lloyds.reporting.perfectomobile.com/export/api/v1/test-executions?jobName[0]=nga-3-aft-weeknightFunctionalEnvironmentMaster'+platform+'&tags[0]='+tag+'&startExecutionTime[0]='+todayStartTime+'&endExecutionTime[0]='+todayEndTime+'';
                } else {
                    return 'https://lloyds.reporting.perfectomobile.com/export/api/v1/test-executions?jobName[0]=nga-3-aft-weeknightFunctionalEnvironmentMaster'+platform+'&tags[0]='+tag+'&status='+status+'&startExecutionTime[0]='+todayStartTime+'&endExecutionTime[0]='+todayEndTime+'';
                }
            }

            let urlAndroidJob = 'https://lloyds.reporting.perfectomobile.com/export/api/v1/test-executions?jobName[0]=nga-3-aft-weeknightFunctionalEnvironmentMasterAndroid&startExecutionTime[0]='+todayStartTime+'&endExecutionTime[0]='+todayEndTime+'';
            let urlIosJob = 'https://lloyds.reporting.perfectomobile.com/export/api/v1/test-executions?jobName[0]=nga-3-aft-weeknightFunctionalEnvironmentMasterIos&startExecutionTime[0]='+todayStartTime+'&endExecutionTime[0]='+todayEndTime+'';


            let urlFailAOVIos = getUrlEndpoint("FAILED","team-aov","Ios");
            let urlPassAOVIos = getUrlEndpoint("PASSED","team-aov","Ios");
            let urlTotalAOVIos = getUrlEndpoint("BOTH","team-aov","Ios");
            let urlFailAOVAndroid = getUrlEndpoint("FAILED","team-aov","Android");
            let urlPassAOVAndroid = getUrlEndpoint("PASSED","team-aov","Android");
            let urlTotalAOVAndroid = getUrlEndpoint("BOTH","team-aov","Android");

            Promise.all([perfectoRequest(urlFailAOVIos),perfectoRequest(urlPassAOVIos),perfectoRequest(urlTotalAOVIos),perfectoRequest(urlFailAOVAndroid),perfectoRequest(urlPassAOVAndroid),perfectoRequest(urlTotalAOVAndroid),perfectoRequest(urlAndroidJob),perfectoRequest(urlIosJob)]).then(function(values){
                if (values[6]["metadata"]["processingStatus"] == "PROCESSING_COMPLETE") {
                    aovAndroidFailTotal = values[3]["resources"].length;
                    aovAndroidPassTotal = values[4]["resources"].length;
                    aovAndroidTotal = values[5]["resources"].length;
                    aovAndroidFailPercentage = " ( " + getPercentage(aovAndroidFailTotal,aovAndroidTotal) + "% )";
                    aovAndroidPassPercentage = " ( " + getPercentage(aovAndroidPassTotal,aovAndroidTotal) + "% )";
                    androidJobCompleteTime = getTimeStamp(new Date(values[6]["resources"][0]["endTime"]));

                    failedTwo.textContent = aovAndroidFailTotal + aovAndroidFailPercentage;
                    passedTwo.textContent = aovAndroidPassTotal + aovAndroidPassPercentage;
                    totalTwo.textContent = aovAndroidTotal;
                    jobCompleteTimeTwo.textContent = androidJobCompleteTime;

                    statusTwo.textContent = "Job finished" ;
                    statusTwo.style.color = "green"; 
                    chrome.storage.local.set({'aovAndroidFail': aovAndroidFailTotal, 'aovAndroidPass': aovAndroidPassTotal, 'aovAndroidTotal': aovAndroidTotal, 'aovAndroidFailPerc': aovAndroidFailPercentage, 'aovAndroidPassPerc': aovAndroidPassPercentage,
                                            'timeStampAndroid': androidJobCompleteTime});
                } else {
                    statusTwo.textContent = "Job Still running for today" ;
                    statusTwo.style.color = "red";
                };

                if (values[7]["metadata"]["processingStatus"] == "PROCESSING_COMPLETE") {
                    aovIosFailTotal = values[0]["resources"].length;
                    aovIosPassTotal = values[1]["resources"].length;
                    aovIosTotal = values[2]["resources"].length;
                    aovIosFailPercentage = " ( " + getPercentage(aovIosFailTotal,aovIosTotal) + "% )";
                    aovIosPassPercentage = " ( " + getPercentage(aovIosPassTotal,aovIosTotal) + "% )";
                    iosJobCompleteTime = getTimeStamp(new Date(values[7]["resources"][0]["endTime"]));

                    failedOne.textContent = aovIosFailTotal + aovIosFailPercentage;
                    passedOne.textContent = aovIosPassTotal + aovIosPassPercentage;
                    totalOne.textContent = aovIosTotal;
                    jobCompleteTimeOne.textContent = iosJobCompleteTime;
                    statusOne.textContent = "Job finished" ;
                    statusOne.style.color = "green"; 

                    chrome.storage.local.set({'aovIosFail': aovIosFailTotal, 'aovIosPass': aovIosFailTotal, 'aovIosTotal': aovIosTotal, 'aovIosFailPerc': aovIosFailPercentage, 'aovIosPassPerc': aovIosPassPercentage,
                                            'timeStampIos': iosJobCompleteTime});
                } else {
                    statusOne.textContent = "Job Still running for today" ;
                    statusOne.style.color = "red";
                };

                $('#timeStamp').html("Last Refreshed : " + getTimeStamp(currentEpochTime()));
                chrome.storage.local.set({'lastRefreshedTime': getTimeStamp(currentEpochTime())});

            });
            console.log('success');
        });
    });
