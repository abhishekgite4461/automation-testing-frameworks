
    
    var button = document.getElementById('changelinks');
    var jobOne = document.getElementById('job1');
    var teamOne = document.getElementById('team1');
    var passedOne = document.getElementById('passed1');
    var failedOne = document.getElementById('failed1');
    var totalOne = document.getElementById('total1');
    var statusOne = document.getElementById('status1');
    var jobCompleteTimeOne = document.getElementById('jobCompleteTime1');
    
    var jobTwo = document.getElementById('job2');
    var teamTwo = document.getElementById('team2');
    var passedTwo = document.getElementById('passed2');
    var failedTwo = document.getElementById('failed2');
    var totalTwo = document.getElementById('total2');
    var statusTwo = document.getElementById('status2');
    var jobCompleteTimeTwo = document.getElementById('jobCompleteTime2');
    
    var jobThree = document.getElementById('job3');
    var teamThree = document.getElementById('team3');
    var passedThree = document.getElementById('passed3');
    var failedThree = document.getElementById('failed3');
    var totalThree = document.getElementById('total3');
    var statusThree = document.getElementById('status3');
    var jobCompleteTimeThree = document.getElementById('jobCompleteTime3');

    chrome.storage.local.get(['aovIosFail', 'aovIosPass', 'aovIosTotal', 'aovIosFailPerc', 'aovIosFailChangePerc', 'aovIosPassPerc', 'aovIosPassChangePerc',
    'aovAndroidFail', 'aovAndroidPass', 'aovAndroidTotal', 'aovAndroidFailPerc', 'aovAndroidFailChangePerc', 'aovAndroidPassPerc', 'aovAndroidPassChangePerc',
    'timeStamp', 'timeStampAndroid','timeStampIos', 'lastRefreshedTime'], function(data) {
        if (data.aovIosFail != undefined && data.aovIosFailPerc != undefined) {
            document.getElementById('failedTotal1').textContent = data.aovIosFail;
            document.getElementById('failedPerc1').textContent = data.aovIosFailPerc;
        }

        if (data.aovIosPass != undefined && data.aovIosPassPerc != undefined) {
            document.getElementById('passedTotal1').textContent =  data.aovIosPass;
            document.getElementById('passedPerc1').textContent = data.aovIosPassPerc;

        }
        
        totalOne.textContent = data.aovIosTotal;
        jobCompleteTimeOne.textContent = data.timeStampIos;

        if (data.aovAndroidFail != undefined && data.aovAndroidFailPerc != undefined) {
            document.getElementById('failedTotal2').textContent = data.aovAndroidFail;
            document.getElementById('failedPerc2').textContent = data.aovAndroidFailPerc;
        }

        if (data.aovAndroidPass != undefined && data.aovAndroidPassPerc != undefined) {
            document.getElementById('passedTotal2').textContent = data.aovAndroidPass;
            document.getElementById('passedPerc2').textContent = data.aovAndroidPassPerc;
        }
        
        totalTwo.textContent = data.aovAndroidTotal;
        jobCompleteTimeTwo.textContent = data.timeStampAndroid;
        
        if (data.lastRefreshedTime != undefined) {
            $('#timeStamp').html("Last Refreshed : " + data.lastRefreshedTime);
        }
      });


    async function perfectoRequest(url) {
        let response = await fetch(url, {
            method: 'get',
            headers: {
            "PERFECTO_AUTHORIZATION": "eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI1ZjNmZjU5MC1mZjE0LTRmNjYtYTZkYy0xNGE0ODVjOWJmZDQifQ.eyJqdGkiOiI0ZTg2NDliZC05YTk2LTQwNjAtODFhMi1lYTcyY2E4OGQ1ODIiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTg2NzEwNzcxLCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL2xsb3lkcy1wZXJmZWN0b21vYmlsZS1jb20iLCJhdWQiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL2xsb3lkcy1wZXJmZWN0b21vYmlsZS1jb20iLCJzdWIiOiI5MmQ5YjU5Zi1iZmZlLTQ1ODktODJkYy0yYTFkYWE2ZDJlNGQiLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiIxZWQ3YjAzZC1mZjYyLTQ0NzYtOTE5MS1kZDU4ZGFiNGUxMzEiLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoib2ZmbGluZV9hY2Nlc3MifQ.-eB5oaMIWkPwIhS-016XTUiaaR99gPlJCXbjLTvL_b4",
            "cache-control": "no-cache",
            }
        });
        return await response.json();
    }

    function getPercentage(partialValue, totalValue) {
        return ((100 * partialValue) / totalValue).toFixed(0);
    }

    function AddZero(num) {
        return (num >= 0 && num < 10) ? "0" + num : num + "";
    }

    function currentEpochTime() {
        let now = new Date();
        return now;
    }

    function getTimeStamp(epochTime) {
        return [[AddZero(epochTime.getDate()), 
                AddZero(epochTime.getMonth() + 1), 
                epochTime.getFullYear()].join("/"), 
                [AddZero(epochTime.getHours()), 
                AddZero(epochTime.getMinutes())].join(":"), 
                epochTime.getHours() >= 12 ? "PM" : "AM"].join(" ");
    }

    function getPercentageChange(yesterdaysNumber, TodaysNumber){
        var res=(TodaysNumber-yesterdaysNumber)/yesterdaysNumber*100.0;
        return Math.floor(res);
    }

    function getCurrentWorkday(){
        return moment().format('YYYY-MM-DDT03:00:00+00:00')
    }

    function getPreviousWorkday(){
        let workday = moment();
        let day = workday.day();
        let diff = 1;  // This will return Yesterday
        if (day == 0 || day == 1){  // It will check if its Sunday or Monday
          diff = day + 2;  // It will return Friday
        }
        return workday.subtract(diff, 'days').format('YYYY-MM-DDT03:00:00+00:00');
    }

    function checkIncreaseOrDecrease(value) {
        if (value > 0 || value == 0) {
            return `${value}% &#9650`;
        } else {
            return `${value}% &#9660`
        }
    }

    function getUrlEndpoint(status, tag, platform, startTime, endTime) {
        if (status == "BOTH") {
            if (tag == "ALL") {
                return 'https://lloyds.reporting.perfectomobile.com/export/api/v1/test-executions?jobName[0]=nga-3-aft-weeknightFunctionalEnvironmentMaster'+platform+'&startExecutionTime[0]='+startTime+'&endExecutionTime[0]='+endTime+'';
            } else {
                return 'https://lloyds.reporting.perfectomobile.com/export/api/v1/test-executions?jobName[0]=nga-3-aft-weeknightFunctionalEnvironmentMaster'+platform+'&tags[0]='+tag+'&startExecutionTime[0]='+startTime+'&endExecutionTime[0]='+endTime+'';
            }
        } else {
            return 'https://lloyds.reporting.perfectomobile.com/export/api/v1/test-executions?jobName[0]=nga-3-aft-weeknightFunctionalEnvironmentMaster'+platform+'&tags[0]='+tag+'&status='+status+'&startExecutionTime[0]='+startTime+'&endExecutionTime[0]='+endTime+'';
        }
    }

    function getBackgroundColor(result, value) {
        if ((result == "failed" && value > 0) || (result == "passed" && value < 0)) {
            return "red"
        } else {
            return "green";
        };
    }

    button.addEventListener('click', function () {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {

            // if (moment().day() != 6 || 0) {      //This line makes sure the code inside is not executed on a Weekend
                // const todayStartTime = moment(getCurrentWorkday()).valueOf();
                // const todayEndTime = moment(getCurrentWorkday()).add(10, 'hours').valueOf();

                // const yesterdaysStartTime = moment(getPreviousWorkday()).valueOf();
                // const yesterdaysEndTime = moment(getPreviousWorkday()).add(10, 'hours').valueOf();


            // Uncommetn above lines and Delete it afterwards following 4 lines are just to test over the weekend

                todayStartTime = "1586919600000";
                todayEndTime = "1586955600000";
                yesterdaysStartTime = "1586833200000";
                yesterdaysEndTime = "1586869200000";

                console.log("yesterdaysStartTime ABHI : ", + yesterdaysStartTime);
                console.log("yesterdaysEndTime ABHI : ", + yesterdaysEndTime);
                
                console.log("TODAY START TIME " + todayStartTime);
                console.log("TODAY END TIME " + todayEndTime);


                let urlFailAOVIos = getUrlEndpoint("FAILED","team-aov","Ios",todayStartTime,todayEndTime);
                let urlPassAOVIos = getUrlEndpoint("PASSED","team-aov","Ios",todayStartTime,todayEndTime);
                let urlTotalAOVIos = getUrlEndpoint("BOTH","team-aov","Ios",todayStartTime,todayEndTime);
                let urlFailAOVAndroid = getUrlEndpoint("FAILED","team-aov","Android",todayStartTime,todayEndTime);
                let urlPassAOVAndroid = getUrlEndpoint("PASSED","team-aov","Android",todayStartTime,todayEndTime);
                let urlTotalAOVAndroid = getUrlEndpoint("BOTH","team-aov","Android",todayStartTime,todayEndTime);
                let urlAndroidJob = getUrlEndpoint("BOTH","ALL","Android",todayStartTime,todayEndTime);
                let urlIosJob = getUrlEndpoint("BOTH","ALL","Android",todayStartTime,todayEndTime);

                let urlYesterdayFailAOVIos = getUrlEndpoint("FAILED","team-aov","Ios",yesterdaysStartTime,yesterdaysEndTime);
                let urlYesterdayPassAOVIos = getUrlEndpoint("PASSED","team-aov","Ios",yesterdaysStartTime,yesterdaysEndTime);
                let urlYesterdayTotalAOVIos = getUrlEndpoint("BOTH","team-aov","Ios",yesterdaysStartTime,yesterdaysEndTime);
                let urlYesterdayFailAOVAndroid = getUrlEndpoint("FAILED","team-aov","Android",yesterdaysStartTime,yesterdaysEndTime);
                let urlYesterdayPassAOVAndroid = getUrlEndpoint("PASSED","team-aov","Android",yesterdaysStartTime,yesterdaysEndTime);
                let urlYesterdayTotalAOVAndroid = getUrlEndpoint("BOTH","team-aov","Android",yesterdaysStartTime,yesterdaysEndTime);
                let urlYesterdayAndroidJob = getUrlEndpoint("BOTH","ALL","Android",yesterdaysStartTime,yesterdaysEndTime);
                let urlYesterdayIosJob = getUrlEndpoint("BOTH","ALL","Ios",yesterdaysStartTime,yesterdaysEndTime);

                Promise.all([perfectoRequest(urlFailAOVIos),perfectoRequest(urlPassAOVIos),perfectoRequest(urlTotalAOVIos),perfectoRequest(urlFailAOVAndroid),perfectoRequest(urlPassAOVAndroid),perfectoRequest(urlTotalAOVAndroid),perfectoRequest(urlAndroidJob),perfectoRequest(urlIosJob),perfectoRequest(urlYesterdayFailAOVIos),perfectoRequest(urlYesterdayPassAOVIos),perfectoRequest(urlYesterdayTotalAOVIos),perfectoRequest(urlYesterdayFailAOVAndroid),perfectoRequest(urlYesterdayPassAOVAndroid),perfectoRequest(urlYesterdayTotalAOVAndroid),perfectoRequest(urlYesterdayAndroidJob),perfectoRequest(urlYesterdayIosJob)]).then(function(values){
                    if (values[6]["metadata"]["processingStatus"] == "PROCESSING_COMPLETE") {
                        aovAndroidFailTotal = values[3]["resources"].length;
                        aovAndroidPassTotal = values[4]["resources"].length;
                        aovAndroidTotal = values[5]["resources"].length;
                        aovAndroidFailPercentage = " (" + getPercentage(aovAndroidFailTotal,aovAndroidTotal) + "%)";
                        aovAndroidPassPercentage = " (" + getPercentage(aovAndroidPassTotal,aovAndroidTotal) + "%)";
                        androidJobCompleteTime = getTimeStamp(new Date(values[6]["resources"][0]["endTime"]));

                        yesterdayAovAndroidFailTotal = values[11]["resources"].length;
                        yesterdayAovAndroidPassTotal = values[12]["resources"].length;
                        yesterdayAovAndroidTotal = values[13]["resources"].length;
                        aovAndroidFailChangePercentage = checkIncreaseOrDecrease(getPercentageChange(yesterdayAovAndroidFailTotal, aovAndroidFailTotal));
                        aovAndroidPassChangePercentage = checkIncreaseOrDecrease(getPercentageChange(yesterdayAovAndroidPassTotal, aovAndroidPassTotal));

                        document.getElementById('failedTotal2').textContent = aovAndroidFailTotal;
                        document.getElementById('failedPerc2').textContent = aovAndroidFailPercentage;
                        document.getElementById('failedChangePerc2').innerHTML = aovAndroidFailChangePercentage;
                        document.getElementById('failedChangePerc2').style.backgroundColor = getBackgroundColor("failed",getPercentageChange(yesterdayAovAndroidFailTotal, aovAndroidFailTotal));
                        document.getElementById('passedTotal2').textContent = aovAndroidPassTotal;
                        document.getElementById('passedPerc2').textContent = aovAndroidPassPercentage;
                        document.getElementById('passedChangePerc2').innerHTML = aovAndroidPassChangePercentage;
                        document.getElementById('passedChangePerc2').style.backgroundColor = getBackgroundColor("passed",getPercentageChange(yesterdayAovAndroidPassTotal, aovAndroidPassTotal));
                        totalTwo.textContent = aovAndroidTotal;
                        jobCompleteTimeTwo.textContent = androidJobCompleteTime;

                        console.log("yesterdayAovAndroidFailTotal : ", + yesterdayAovAndroidFailTotal);
                        console.log("aovAndroidFailTotal : ", + aovAndroidFailTotal);
                        console.log("Change in FAIL Percentage : " +  getPercentageChange(yesterdayAovAndroidFailTotal, aovAndroidFailTotal));
                        console.log("yesterdayAovAndroidPassTotal : ", + yesterdayAovAndroidPassTotal);
                        console.log("aovAndroidPassTotal : ", + aovAndroidPassTotal);
                        console.log("Change in PASS Percentage : " +  getPercentageChange(yesterdayAovAndroidPassTotal, aovAndroidPassTotal));
                        console.log("yesterdayAovAndroidTotal : ", + yesterdayAovAndroidTotal);


                        statusTwo.textContent = "Job finished" ;
                        statusTwo.style.color = "green"; 
                        chrome.storage.local.set({'aovAndroidFail': aovAndroidFailTotal, 'aovAndroidPass': aovAndroidPassTotal, 'aovAndroidTotal': aovAndroidTotal, 'aovAndroidFailPerc': aovAndroidFailPercentage,
                                                'aovAndroidFailChangePerc': aovAndroidFailChangePercentage, 'aovAndroidPassPerc': aovAndroidPassPercentage, 'aovAndroidPassChangePerc': aovAndroidPassChangePercentage,
                                                'timeStampAndroid': androidJobCompleteTime});
                    } else {
                        statusTwo.textContent = "Job Still running for today" ;
                        statusTwo.style.color = "red";
                    };

                    if (values[7]["metadata"]["processingStatus"] == "PROCESSING_COMPLETE") {
                        aovIosFailTotal = values[0]["resources"].length;
                        aovIosPassTotal = values[1]["resources"].length;
                        aovIosTotal = values[2]["resources"].length;
                        aovIosFailPercentage = " (" + getPercentage(aovIosFailTotal,aovIosTotal) + "%)";
                        aovIosPassPercentage = " (" + getPercentage(aovIosPassTotal,aovIosTotal) + "%)";
                        iosJobCompleteTime = getTimeStamp(new Date(values[7]["resources"][0]["endTime"]));

                        yesterdayAovIosFailTotal = values[8]["resources"].length;
                        yesterdayAovIosPassTotal = values[9]["resources"].length;
                        yesterdayAovIosTotal = values[10]["resources"].length;
                        aovIosFailChangePercentage = checkIncreaseOrDecrease(getPercentageChange(yesterdayAovIosFailTotal, aovIosFailTotal));
                        aovIosPassChangePercentage = checkIncreaseOrDecrease(getPercentageChange(yesterdayAovIosPassTotal, aovIosPassTotal));

                        console.log("yesterdayAovIosFailTotal : ", + yesterdayAovIosFailTotal);
                        console.log("aovIosFailTotal : ", + aovIosFailTotal);
                        console.log("Change in FAIL Percentage : " +  getPercentageChange(yesterdayAovIosFailTotal, aovIosFailTotal));
                        console.log("yesterdayAovIosPassTotal : ", + yesterdayAovIosPassTotal);
                        console.log("aovIosPassTotal : ", + aovIosPassTotal);
                        console.log("Change in PASS Percentage : " +  getPercentageChange(yesterdayAovIosPassTotal, aovIosPassTotal));
                        console.log("yesterdayAovIosTotal : ", + yesterdayAovIosTotal);

                        document.getElementById('failedTotal1').textContent = aovIosFailTotal;
                        document.getElementById('failedPerc1').textContent = aovIosFailPercentage;
                        document.getElementById('failedChangePerc1').innerHTML = aovIosFailChangePercentage;
                        document.getElementById('failedChangePerc1').style.backgroundColor = getBackgroundColor("failed",getPercentageChange(yesterdayAovIosFailTotal, aovIosFailTotal));
                        document.getElementById('passedTotal1').textContent = aovIosPassTotal;
                        document.getElementById('passedPerc1').textContent = aovIosPassPercentage;
                        document.getElementById('passedChangePerc1').innerHTML = aovIosPassChangePercentage;
                        document.getElementById('passedChangePerc1').style.backgroundColor = getBackgroundColor("passed",getPercentageChange(yesterdayAovIosPassTotal, aovIosPassTotal));
                        totalOne.textContent = aovIosTotal;
                        jobCompleteTimeOne.textContent = iosJobCompleteTime;
                        statusOne.textContent = "Job finished" ;
                        statusOne.style.color = "green"; 

                        chrome.storage.local.set({'aovIosFail': aovIosFailTotal, 'aovIosPass': aovIosPassTotal, 'aovIosTotal': aovIosTotal, 'aovIosFailPerc': aovIosFailPercentage,
                                                'aovIosFailChangePerc': aovIosFailChangePercentage, 'aovIosPassPerc': aovIosPassPercentage, 'aovIosPassChangePerc': aovIosPassChangePercentage,
                                                'timeStampIos': iosJobCompleteTime});
                    } else {
                        statusOne.textContent = "Job Still running for today" ;
                        statusOne.style.color = "red";
                    };

                    $('#timeStamp').html("Last Refreshed : " + getTimeStamp(currentEpochTime()));
                    chrome.storage.local.set({'lastRefreshedTime': getTimeStamp(currentEpochTime())});

                });
                console.log('success');
            // } else {
            //     $('#status').html("Master Job does not run over the weekend");
            //     $('#status').css({ 'color': 'red', 'font-size': '150%' });
            // };
        });
    });
