import "../css/popup.css";
// import hello from "./popup/example";

// hello();

var EventEmitter = require('events').EventEmitter;
var Util = require('util');
// var request = require('request');
var parseString = require('xml2js').parseString;
const fetch = require('node-fetch');
const $ = require("jquery");
var moment = require('moment');

function fetchDataFromRSS(appId) {
    var self = this;
    var url = 'https://itunes.apple.com/rss/customerreviews/page=1/id='+appId+'/sortby=mostrecent/xml?cc=gb';
    console.log('Outside Fetch ........................');
    (async () => {
        const response = await fetch(url);
        if (response.status == 200) {
            const body = await response.text();
            var data;
            parseString(body, function (err, result) {
                data = result;
                console.log({ data });
                if (err) {
                    console.log(err)
                }
            });
            var entry = data['feed']['entry'];
            var links = data['feed']['link'];

            console.log({ entry });
            console.log({ links });

            if (entry && links) {
                var totalComment = [];
                for (var i = 0; i < entry.length; i++) {
                    var rawReview = entry[i];
                    // console.log({rawReview});
                    if ('author' in rawReview) {
                        try {
                            var comment = [];
                            comment['id'] = rawReview['id'][0];
                            // comment['app'] = app;
                            comment['author'] = rawReview['author'][0]['name'][0];
                            comment['version'] = rawReview['im:version'][0];
                            comment['rate'] = rawReview['im:rating'][0];
                            comment['title'] = rawReview['title'][0];
                            comment['comment'] = rawReview['content'][0]['_'];
                            comment['vote'] = rawReview['im:voteCount'][0];
                            comment['updated'] = rawReview['updated'][0];
                            // comment['country'] = country;
                            totalComment.push(comment);
                            console.log({ comment });

                            // self.emit('review', comment);
                        } catch (err) {
                            console.log(err);
                        }
                    }
                }

                // document.getElementById('ember475').innerText = totalComment[0]['author'];
                // document.getElementById('we-customer-review-3').innerText = totalComment[0]['title'];
                // document.getElementById('ember478').innerText = totalComment[0]['comment'];
                // multiplyNode(document.querySelector('.review-container'), totalComment.length, true);
                console.log('********************************');
                console.log({ totalComment });
                $('.main-container').html('');    //This line clears the previous output
                $.each(totalComment, function (i, x) {
                    // console.log({ x });
                    $('.main-container').append(
                        '<div id="ember471" class="review-container"><div role="article" aria-labelledby="we-customer-review-3" aria-posinset="1" aria-setsize="-1" id="ember473" class="we-customer-review lockup ember-view" tabindex="0"><figure aria-label="5 out of 5" id="ember474" class="we-star-rating ember-view we-customer-review__rating we-star-rating--large"><span class="we-star-rating-stars-outlines"><span class="we-star-rating-stars we-star-rating-stars-5" style="width: '+ x.rate*24 +'px"></span></span></figure><div class="we-customer-review__header we-customer-review__header--user"><span dir="ltr" id="ember475" class="we-truncate we-truncate--single-line ember-view we-customer-review__user">' + x.author + '</span>' +
                        '<time data-test-customer-review-date="" datetime="2019-03-19T12:32:44.000Z" aria-label="19 March 2019" class="we-customer-review__date">' + moment(x.updated).startOf('hour').fromNow() + '</time></div>' +
                        // '<div class="we-customer-review__header we-customer-review__header--user">' +
                        // '<time data-test-customer-review-date="" datetime="2019-03-19T12:32:44.000Z" aria-label="19 March 2019" class="we-customer-review__date"> Ver: ' + x.version + '</time></div>' +
                        '<h3 dir="ltr" id="we-customer-review-3" class="we-truncate we-truncate--single-line ember-view we-customer-review__title">' + x.title + '</h3>' +
                        '<blockquote id="ember476" class="we-truncate we-truncate--multi-line we-truncate--interactive we-truncate--truncated ember-view we-customer-review__body">' +
                        '<div data-clamp="" id="ember478" class="we-clamp ember-view" ismorertl="false" style=" -webkit-mask: linear-gradient(0deg, rgba(0, 0, 0, 0) 0px, rgba(0, 0, 0, 0) 18.0001px, rgb(0, 0, 0) 18.0001px), linear-gradient(270deg, rgba(0, 0, 0, 0) 0px, rgba(0, 0, 0, 0) 32.8px, rgb(0, 0, 0) 68.8002px);">' +
                        '<p dir="ltr" data-test-bidi="">' + x.comment + '</p><br></div></blockquote></div></div>')});
            } else {
                                self.emit('empty', data['feed']['id'][0]);
                            }
                        }
                    })();
                };
                
function multiplyNode(node, count, deep) {
    for (var i = 0, copy; i < count - 1; i++) {
                                copy = node.cloneNode(deep);
                            node.parentNode.insertBefore(copy, node);
                        }
                    }
                    
// fetchDataFromRSS();

$('#lds').click(function() {
    fetchDataFromRSS(469964520);
});

$('#bos').click(function() {
    fetchDataFromRSS(485728109);
});

$('#hfx').click(function() {
    fetchDataFromRSS(486355738);
});

$('#mbna').click(function() {
    fetchDataFromRSS(687485560);
});