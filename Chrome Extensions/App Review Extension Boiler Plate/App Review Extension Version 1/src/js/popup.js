import "../css/popup.css";
// import hello from "./popup/example";

// hello();

var EventEmitter = require('events').EventEmitter;
var Util = require('util');
// var request = require('request');
var parseString = require('xml2js').parseString;
const fetch = require('node-fetch');

console.log('This is from Popup.js');

function fetchDataFromRSS() {
    var self = this;
    var url = 'https://itunes.apple.com/rss/customerreviews/page=1/id=469964520/sortby=mostrecent/xml?cc=gb';
    console.log('Outside Fetch ........................');
    (async () => {
        const response = await fetch(url);
        if (response.status == 200) { 
            const body = await response.text();
            var data;
			parseString(body, function (err, result) {
                data = result;
                console.log({data});
				if (err) {
					console.log(err)
				}
			});
            var entry = data['feed']['entry'];
            var links = data['feed']['link'];

            console.log({entry});
            console.log({links});
            
            if (entry && links) {
                var totalComment = [];
				for (var i = 0; i < entry.length ;i++) {
                    var rawReview = entry[i];
                    // console.log({rawReview});
					if ('author' in rawReview) {
						try {		
							var comment = [];
							comment['id'] = rawReview['id'][0];
							// comment['app'] = app;
							comment['author'] = rawReview['author'][0]['name'][0];
							comment['version'] = rawReview['im:version'][0];
							comment['rate'] = rawReview['im:rating'][0];
							comment['title'] = rawReview['title'][0];
							comment['comment'] = rawReview['content'][0]['_'];
							comment['vote'] = rawReview['im:voteCount'][0];
							comment['updated'] = rawReview['updated'][0];
                            // comment['country'] = country;
                            totalComment.push(comment);
                            console.log({comment});
							
							// self.emit('review', comment);
						} catch (err) {
							console.log(err);
						}
                    }
                }
                
                document.getElementById('ember475').innerText = totalComment[0]['author'];
                document.getElementById('we-customer-review-3').innerText = totalComment[0]['title'];
                document.getElementById('ember478').innerText = totalComment[0]['comment'];
                multiplyNode(document.querySelector('.review-container'), totalComment.length, true);
                console.log('********************************');
                console.log({totalComment});
				
			} else {
				self.emit('empty', data['feed']['id'][0]);
			}
        }
    })();
};

function multiplyNode(node, count, deep) {
    for (var i = 0, copy; i < count - 1; i++) {
        copy = node.cloneNode(deep);
        node.parentNode.insertBefore(copy, node);
    }
}

fetchDataFromRSS();