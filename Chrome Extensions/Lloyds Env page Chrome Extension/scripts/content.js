chrome.runtime.onMessage.addListener( function(request, sender, sendResponse) {

    if (document.location == "https://confluence.devops.lloydsbanking.com/display/DI2ENV/DI2+Environments+Availability+Status") {
        console.log("something happening from the extension");

        var envFirst = document.getElementsByClassName('confluenceTd')[1].innerText;
        var verFirst = document.getElementsByClassName('confluenceTd')[2].innerText;
        var statusFirst = document.getElementsByClassName('confluenceTd')[3].innerText;
        var dbFirst = document.getElementsByClassName('confluenceTd')[7].innerText;
    
        var envSecond = document.getElementsByClassName('confluenceTd')[11].innerText;
        var verSecond = document.getElementsByClassName('confluenceTd')[12].innerText;
        var statusSecond = document.getElementsByClassName('confluenceTd')[13].innerText;
        var dbSecond = document.getElementsByClassName('confluenceTd')[17].innerText;
    
        var envThird = document.getElementsByClassName('confluenceTd')[21].innerText;
        var verThird = document.getElementsByClassName('confluenceTd')[22].innerText;
        var statusThird = document.getElementsByClassName('confluenceTd')[23].innerText;
        var dbThird = document.getElementsByClassName('confluenceTd')[27].innerText;
    
        function AddZero(num) {
            return (num >= 0 && num < 10) ? "0" + num : num + "";
        }

        var now = new Date();
        var strDateTime = [[AddZero(now.getDate()), 
                            AddZero(now.getMonth() + 1), 
                            now.getFullYear()].join("/"), 
                            [AddZero(now.getHours()), 
                            AddZero(now.getMinutes())].join(":"), 
                            now.getHours() >= 12 ? "PM" : "AM"].join(" ");

        chrome.storage.local.set({'envFirst': envFirst, 'verFirst': verFirst, 'statusFirst': statusFirst, 'dbFirst': dbFirst, 'envSecond': envSecond, 'verSecond': verSecond, 'statusSecond': statusSecond, 'dbSecond': dbSecond, 'envThird': envThird, 'verThird': verThird, 'statusThird': statusThird, 'dbThird': dbThird, 'timeStamp': strDateTime});
        sendResponse({data: "Correct response", success: true});
    } else {
        sendResponse({data: "Incorrect response", success: false});
    }
});

