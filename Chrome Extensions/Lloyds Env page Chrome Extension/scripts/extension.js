
    document.getElementById('status').textContent = "Extension loaded";
    
    var button = document.getElementById('changelinks');
    
    var envOne = document.getElementById('env1');
    var verOne = document.getElementById('ver1');
    var statusOne = document.getElementById('status1');
    var dbOne = document.getElementById('db1');
    
    var envTwo = document.getElementById('env2');
    var verTwo = document.getElementById('ver2');
    var statusTwo = document.getElementById('status2');
    var dbTwo = document.getElementById('db2');
    
    var envThree = document.getElementById('env3');
    var verThree = document.getElementById('ver3');
    var statusThree = document.getElementById('status3');
    var dbThree = document.getElementById('db3');

    chrome.storage.local.get(['envFirst', 'verFirst', 'statusFirst', 'dbFirst', 'envSecond', 'verSecond', 'statusSecond', 'dbSecond', 'envThird', 'verThird', 'statusThird', 'dbThird', 'timeStamp'], function(data) {
        envOne.textContent = data.envFirst;
        verOne.textContent = data.verFirst;
        statusOne.textContent = data.statusFirst;
        dbOne.textContent = data.dbFirst;

        envTwo.textContent = data.envSecond;
        verTwo.textContent = data.verSecond;
        statusTwo.textContent = data.statusSecond;
        dbTwo.textContent = data.dbSecond;

        envThree.textContent = data.envThird;
        verThree.textContent = data.verThird;
        statusThree.textContent = data.statusThird;
        dbThree.textContent = data.dbThird;
        
        $('#timeStamp').html("Last Updated : " + data.timeStamp);
      });

    button.addEventListener('click', function () {
            chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                chrome.tabs.sendMessage(tabs[0].id, {data: "Request recieved"}, function(response) {    
                    if (response.data == "Correct response") {

                        console.log("Received response");
                        
                        $('#status').html('Successfully Updated');
                        document.getElementById('status').style.color = "green";

                        chrome.storage.local.get(['envFirst', 'verFirst', 'statusFirst', 'dbFirst', 'envSecond', 'verSecond', 'statusSecond', 'dbSecond', 'envThird', 'verThird', 'statusThird', 'dbThird', 'timeStamp'], function(data) {
                        envOne.textContent = data.envFirst;
                        verOne.textContent = data.verFirst;
                        statusOne.textContent = data.statusFirst;
                        dbOne.textContent = data.dbFirst;
             
                        envTwo.textContent = data.envSecond;
                        verTwo.textContent = data.verSecond;
                        statusTwo.textContent = data.statusSecond;
                        dbTwo.textContent = data.dbSecond;

                        envThree.textContent = data.envThird;
                        verThree.textContent = data.verThird;
                        statusThree.textContent = data.statusThird;
                        dbThree.textContent = data.dbThird;
                        document.getElementById("timeStamp").style.visibility="visible"; 
                        $('#timeStamp').html("Last Updated : " + data.timeStamp);
                      });

                        console.log('success');
                    } else { 
                        document.getElementById('status').textContent = "Please log in sandbox and go to confluence page" ;
                        document.getElementById('status').style.color = "red";
                    };

                });
            });
    });
    
