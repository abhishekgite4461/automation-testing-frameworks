var keys = [ 
	'ERPIITKGP_ERPLoginID', 
	'ERPIITKGP_ERPPassword', 
];

var inputTags = {
	'ERPIITKGP_ERPLoginID' : document.getElementById('user_id'),
	'ERPIITKGP_ERPPassword' : document.getElementById('password')
};

chrome.storage.sync.get(keys, function(authData) {
    if (document.URL == "https://oabrmls.paragonrels.com/ParagonLS/Default.mvc/Login") {
        if (authData['ERPIITKGP_ERPLoginID'] || authData['ERPIITKGP_ERPPassword']) {
            if (document.getElementsByClassName('ui-corner-all ui-state-error')[0]) {
                if (document.getElementsByClassName('ui-corner-all ui-state-error')[0].innerText == "We do not recognize your username and/or password. Please try again.") {
                    chrome.runtime.sendMessage({greeting: "Invalid username or password."}, async function(response) {
                        await console.log('Sent message from content.js to background.js');
                         if (response) {
                             console.log('The response is : ', response.response);
                         }
                    });
                } else {
                        document.getElementById('LoginName').value = authData['ERPIITKGP_ERPLoginID'];
                        document.getElementById('Password').value = authData['ERPIITKGP_ERPPassword'];
                        document.querySelectorAll('input[type="submit"]')[0].click();
                }
            } else {
                    document.getElementById('LoginName').value = authData['ERPIITKGP_ERPLoginID'];
                    document.getElementById('Password').value = authData['ERPIITKGP_ERPPassword'];
                    document.querySelectorAll('input[type="submit"]')[0].click();
            }
        } else {
            console.log('Chrome Storage is EMPTY');
            chrome.runtime.sendMessage({greeting: "Username and Password missing in Chrome-Extension"}, async function(response) {
                await console.log('Sent message from content.js to background.js');
                 if (response) {
                     console.log('The response is : ', response.response);
                 }
            });
        }
    } 
});



// setTimeout(function() {
//     if (document.URL == "https://oabrmls.paragonrels.com/ParagonLS/Default.mvc") {
//         chrome.runtime.sendMessage({greeting: "Logged In Successfully"}, async function(response) {
//             await console.log('Sent message from content.js to background.js');
//              if (response) {
//                  console.log('The response is : ', response.response);
//              }
//         });
//     }
// }, 3000);