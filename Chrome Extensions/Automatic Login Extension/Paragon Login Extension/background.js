chrome.runtime.onMessage.addListener(
    async function(request, sender, sendResponse) {
        if (await request.greeting == "Logged In Successfully") {
            await createNotification("SUCCESS", request.greeting, "Passed.png");
            sendResponse({
                response: "Message received"
            });
            return true;
        }

        if (await request.greeting == "Invalid username or password.") {
            await createNotification("LOGIN FAILED", request.greeting, "Failed.png");
            sendResponse({
                response: "Message received"
            });
            return true;
        }

        if (await request.greeting == "Username and Password missing in Chrome-Extension") {
            await createNotification("LOGIN FAILED", request.greeting, "Alert.png");
            sendResponse({
                response: "Message received"
            });
            return true;
        }
        
    });
function createNotification(titlemsg, messagemsg, icon){
     var opt = {type: "basic",title: titlemsg,message: messagemsg,iconUrl: icon}
     chrome.notifications.create("notificationName",opt,function(){});

//include this line if you want to clear the notification after 5 seconds
    setTimeout(function(){chrome.notifications.clear("notificationName",function(){});},10000);
}