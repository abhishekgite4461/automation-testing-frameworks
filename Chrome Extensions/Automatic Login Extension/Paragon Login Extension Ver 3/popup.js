$("#ERPLoginID").blur(function() {
    var username = $("#ERPLoginID").val();
  
    if (username == "" || username == null) {
      alert("Insert the Username!");
      return;
    }
  });
  
  // Saves options to chrome.storage
  function save_options() {
    chrome.storage.sync.set(
      {
        ERPIITKGP_ERPLoginID: document.getElementById("ERPLoginID").value,
        ERPIITKGP_ERPPassword: document.getElementById("ERPPassword").value,
      },
      function() {
        document.getElementById("status").innerHTML =
          '<div class="alert alert-success" role="alert">Your credentials have been saved. Open <a href="https://oabrmls.paragonrels.com/ParagonLS/Default.mvc/Login" target="_blank">Paragon Login Page</a>.</div>';
      }
    );
  }
  
  function recovery_previous_memory() {
    chrome.storage.sync.get(function(results) {
      Object.keys(results).forEach(function(key) {
        console.log(results);
        if (
          results[key] &&
          document.getElementById(key.substring(10, key.length))
        ) {
          document.getElementById(key.substring(10, key.length)).value =
            results[key];
        }
      });
    });
  }
  
  function show_open_if_already_saved() {
    chrome.storage.sync.get(results => {
      // (chrome.extension.getBackgroundPage()).console.log("results are  ", results);
      let values = Object.values(results);
      if (!values.includes(""))
        document.getElementById("status").innerHTML =
        '<div class="alert alert-success" role="alert">Your credentials have been saved. Open <a href="https://oabrmls.paragonrels.com/ParagonLS/Default.mvc/Login" target="_blank">Paragon Login Page</a>.</div>';
    });
  }
  
  function reset_options() {
    document.getElementById("ERPLoginID").value = "";
    document.getElementById("ERPPassword").value = "";
    chrome.storage.sync.set(
      {
        ERPIITKGP_ERPLoginID: document.getElementById("ERPLoginID").value,
        ERPIITKGP_ERPPassword: document.getElementById("ERPPassword").value,
      },
      function() {
        document.getElementById("status").innerHTML =
          '<div class="alert alert-success" role="alert">Your credentials have been reset. Open <a href="https://oabrmls.paragonrels.com/ParagonLS/Default.mvc/Login" target="_blank">Paragon Login Page</a>.</div>';
      }
    );
  }
  
  $(document).ready(function() {
    [
      "ERPLoginID",
      "ERPPassword"
    ].forEach(element => {
      document
        .getElementById(element)
        .addEventListener("input", () => save_options());
    });
    show_open_if_already_saved();
    document.getElementById("reset").addEventListener("click", reset_options);
    recovery_previous_memory();
  });
  