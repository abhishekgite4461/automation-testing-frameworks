Paragon Auto-login chrome extension. 

The Chrome Extension logs the user in (provided the username and password are already saved) autoamtically. This Chrome extension will retry to login if an error message is displayed stating 'Maximum number connections used'.

In order to install please follow the steps.

1) Download the Zip Folder and unzip it in the desire location
2) In Chrome, navigate to chrome://extensions/
3) Check the box for Developer mode in the top right
4) Click on the “Load unpacked” button, and go to the location where you saved the unziped folder
5) And click on the OK button to install that Chrome extension.
6) Once installed you can see the Chrome extension icon beside the URL bar.
7) Click on the chrome extension.
8) Enter the username and password (It Automatically SAVES it as you type)
9) Click on the chrome extension icon again to make the pop-up window disabled.
10) Navigate to your website https://oabrmls.paragonrels.com/ParagonLS/Default.mvc/Login
11) Once you are navigated to the above link you will be logged in automatically.




If the Password is changed/expired:

- If the password is changed the chrome extension will try to login with the credentials you entered when you installed the extension.
- It will give you a notification that the Username/Password is incorrect.
- Just follow the installation steps mentioned above and update the new password in the extension.
- The revisit the website https://oabrmls.paragonrels.com/ParagonLS/Default.mvc/Login

