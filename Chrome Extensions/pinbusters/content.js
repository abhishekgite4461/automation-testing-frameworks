function removePins(node) {

    // Get the images which are promoted-ads
    // Delay used to make sure all images are loaded
    setTimeout(function() {
        if(!node.querySelector('.tBJ.dyH.iFc.MF7.pBj.DrD.IZT.mWe.z-6')) {
            if (node.querySelector('.XiG.sLG.zI7.iyn.Hsu')) {
                node.querySelector('.XiG.sLG.zI7.iyn.Hsu').setAttribute('style', 'display:none');
            }
        }
    }, 500);
}

// Observe DOM for changes
// Define the mutation observer
var observer = new MutationObserver(function (mutations) {
    // console.log('Mutation Length : ',mutations.length);
    for (var i = 0; i < mutations.length; i++) {
        var mutation = mutations[i];
        // console.log({mutation});
        // console.log('Mutation Added Nodes',mutation.addedNodes);

        if (mutation.addedNodes && mutation.addedNodes.length > 0) {
            for (var j = 0; j < mutation.addedNodes.length; j++) {
                var newNode = mutation.addedNodes[j];
                // console.log({newNode});
                // console.log('NewNocde Class list ', newNode.classList);
                if (newNode.nodeType === Node.ELEMENT_NODE) {
                        removePins(newNode);
                }
            }
        }
    }
});


 // Following will run the removePins function and hide the non-promo tiles
    var objects = document.querySelectorAll('.Yl-.MIw.Hb7');
    for (var i = 0; i < objects.length; i++) {
        removePins(objects[i]);
    }

// Following will constantly check when we scroll down and new tiles are added and then will check if ther are non-promo tiles
    observer.observe(document.body, {
        childList: true,
        subtree: true
    });
