module.exports = {
  rootDir: '../../',
  verbose: true,
  testResultsProcessor: './node_modules/jest-junit-reporter',
  testRegex: './(src|test|.githooks)/.+\\.spec\\.jsx?$',
  modulePaths: ['<rootDir>'],
  testPathIgnorePatterns: [
    'node_modules',
    './test/apiResponses/coreapi/wishlist/*',
  ],
}
