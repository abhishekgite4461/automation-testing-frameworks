const webpack = require('webpack')
const path = require('path')
const webpackConfig = require('./webpack/dev.config.js')
const config = require('./config.js')
const { Server } = require('hapi')
const WebpackPlugin = require('hapi-webpack-plugin')
const h2o2 = require('h2o2')

const assets = {
  contentBase: path.join(__dirname, '../src'),
  quiet: false,
  noInfo: false,
  lazy: false,
  stats: {
    chunks: false,
    colors: true,
    children: false
  },
  watchOptions: {
    poll: true
  },
  historyApiFallback: false,
  publicPath: '/assets/',
  proxy: {
    '*': 'http://localhost:' + (process.env.PORT || 3000)
  },
  host: 'localhost'
}

const server = new Server()
server.connection({ port: process.env.DEV_SERVER_PORT || 8080, state: { strictHeader: false } })
const compiler = new webpack(webpackConfig)

server.register(h2o2, (error) => {
  if (error) {
    return console.error(error)
  }
  server.route({
    method: '*',
    path: '/{param*}',
    config: {
      handler: {
        proxy: {
          mapUri: function (request, callback) {
            callback(null, `http://${request.info.hostname}:3000${request.url.path}`)
          },
          passThrough: true,
          xforward: true
        }
      }
    }
  })

  server.register({
    register: WebpackPlugin,
    options: {compiler, assets}
  }, (error) => {
    if (error) {
      return console.error(error);
    }
  })
})

server.start(() => console.log('Hapi webpack dev server running on port', server.info.port));
