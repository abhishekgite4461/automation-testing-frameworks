const NO_WHERE = (source) => [source]
const PREFIXED = (source, affix) => [affix, source]
const SUFFIXED = (source, affix) => [source, affix]

function InjectorPlugin(config) {
  this.config = Object.assign({
    test: /^$/,
    affix: () => '',
    where: NO_WHERE,
  }, config, { seperator: '\n'})
}

InjectorPlugin.prototype.apply = function (compiler) {
  compiler.plugin('emit', (compilation, next) => {
    Promise.resolve().then(() => {
      const { test, affix: getAffix, where: how, seperator } = this.config

      const affix = getAffix(compilation.assets)

      Object.keys(compilation.assets)
        .filter(name => test.test(name))
        .forEach(name => {
          const asset = compilation.assets[name]
          const content = how(asset.source(), affix).join(seperator)

          compilation.assets[name] = {
            source: () => content,
            size: () => Buffer.byteLength(content, 'utf8')
          }
        })


      next()
    })
      .catch(err => next(err))
  })
}

InjectorPlugin.POSITION = {
  NO_WHERE,
  PREFIXED,
  SUFFIXED
}

module.exports = InjectorPlugin
