const postcss = require('postcss')
const postcssUrl = require('postcss-url')
const path = require('path')


function BrandCssPlugin(config, cb) {
  this.config = config
  this.cb = cb
}

BrandCssPlugin.prototype.apply = function (compiler) {
  compiler.plugin('emit', (compilation, next) => {
    const bundleName = Object.keys(compilation.assets).find((asset) => {
      return asset.startsWith('bundle') && asset.endsWith('.css')
    })
    if (!compilation.assets[bundleName]) {
      return next()
    }
    const bundleCss = compilation.assets[bundleName].source()

    const promises = this.config.brands.map((brand) => {
      const brandCssName = Object.keys(compilation.assets).find((asset) => {
        return asset === `${brand}.css`
      })

      const extraCss = compilation.assets[brandCssName] && compilation.assets[brandCssName].source() || ''
      const css = (bundleCss + extraCss).replace(/\\\-\-/g, '--')

      return postcss(this.cb(brand, this.config.dev))
        .use(postcssUrl({
          url: this.config.convertUrlImports
            ? 'copy'
            : (url) => {
              return url
                .replace(`public/${brand}/`, '')
                .replace(`public/common/`, '../common/')
            },
          useHash: this.config.convertUrlImports
        }))
        .process(css, {
          to: path.join(__dirname, `../../../public/${brand}/styles.css`)
        })
        .then((result) => {
          result.warnings().forEach(console.log)

          compilation.assets[brand + '/styles.css'] = {
            source: () => result.css,
            size: () => Buffer.byteLength(result.css, 'utf8')
          }
        })
        .catch(({ message, name, reason }) => {
          // If the reporter throws an error, the message should simply be logged to the console.
          // Otherwise the server will crash.
          if (/postcss-reporter/.test(message)) {
            return console.log(message)
          }
          throw new Error(`\x1b[31m Error in postcss compilation ${name} ${reason} \x1b[0m`)
        })
    })

    Promise.all(promises).then(() => next())
      .catch(err => {
        return next(err)
      })
  })
}

module.exports = BrandCssPlugin
