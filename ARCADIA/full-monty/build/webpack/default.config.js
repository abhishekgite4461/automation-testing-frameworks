const webpack = require('webpack')
const config = require('../config.js')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HappyPack = require('happypack')
const plugins = require('./plugins')
const ChunkManifestPlugin = require('chunk-manifest-webpack-plugin')
const WriteFilePlugin = require('write-file-webpack-plugin')
const buildUtilities = require('./lib/build-utilities.js')
const NewRelicSourceMapPlugin = require('new-relic-source-map-webpack-plugin')
const path = require('path')

const shouldOverride = (shouldOverride) => (overrideValue, defaultValue) => {
  return shouldOverride ? overrideValue : defaultValue
}

module.exports = (isProduction = false) => {
  const extractCSS = new ExtractTextPlugin({
    filename: '[name].css',
    allChunks: true,
  })
  const extractSCSS = new ExtractTextPlugin({
    filename: 'bootstrap-grid.css',
    allChunks: true,
  })
  const override = shouldOverride(isProduction)
  const cleanup = (list) => list.filter((p) => p)

  const entry = {
    'polyfill-ie11': './public/common/vendors/polyfill-ie11.js',
    bundle: cleanup([
      override(undefined, 'webpack-hot-middleware/client'),
      'babel-polyfill',
      process.env.FUNCTIONAL_TESTS === 'true'
        ? './public/common/vendors/polyfill-fetch-override.js'
        : undefined,
      './src/client/index.jsx',
      './build/webpack/bootstrap.scss',
    ]),
    vendor: [
      'lodash.throttle',
      'ramda',
      'react-hammerjs',
      'react-helmet',
      'react-router',
      'react',
      'redux-thunk',
      'redux',
      'superagent',
    ].concat(
      override(
        ['react-dom/server'],
        [
          'amator',
          'countdown',
          'he',
          'md5',
          'react-dom',
          'react-redux',
          'redux-async-queue',
        ]
      )
    ),
    'service-desk': ['./src/client/lib/service-desk.js'],
  }

  const chunks = Object.keys(entry).filter((name) => name !== 'polyfill-ie11')

  return buildUtilities[override('addBrandsProd', 'addBrandsDev')]({
    name: 'client',
    target: 'web',
    context: config.get('root_dir'),
    cache: override(false, true),
    devtool: override(
      'source-map',
      process.env.WEBPACK_DEVTOOL || 'eval-source-maps'
    ),
    stats: override({ chunks: true }),

    entry,
    output: {
      filename: config.get('dir_common') + '/[name].js',
      path: config.get('dir_assets'),
      publicPath: '/assets/',
      sourceMapFilename: override('[file].map'),
    },
    resolve: {
      modules: ['node_modules', 'src', 'vendor', 'src/shared/components'],
      extensions: ['.js', '.jsx', '.json'],
    },
    plugins: cleanup([
      new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        filename: config.get('dir_common') + '/[name].js',
        chunks,
      }),
      new webpack.IgnorePlugin(
        /ignored-server-require|cache-service-redis|moment-timezone|ua-parser-js|newrelic/
      ),
      extractCSS,
      extractSCSS,
      new webpack.optimize.OccurrenceOrderPlugin(),
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        'process.env.ENABLE_CLIENT_ERROR_LOGGING':
          process.env.ENABLE_CLIENT_ERROR_LOGGING === 'true',
      }),
      override(
        undefined,
        new HappyPack({
          id: 'js',
          threads: 8,
          loaders: ['babel-loader'],
        })
      ),
      override(
        new webpack.optimize.UglifyJsPlugin({
          sourceMap: true,
          compress: {
            unused: true,
            dead_code: true,
            warnings: false,
          },
        })
      ),

      override(new webpack.NoEmitOnErrorsPlugin()),

      new plugins.BrandCssPlugin(
        {
          dirCommon: config.get('dir_common'),
          brands: config.get('brands'),
          convertUrlImports: override(true, false),
        },
        buildUtilities.customPostcss
      ),

      new plugins.InjectorPlugin({
        test: /.css$/,
        affix: (assets) => {
          const asset =
            assets[
              Object.keys(assets).find((name) =>
                /^bootstrap-grid.css$/.test(name)
              )
            ]
          return asset ? asset.source() : ''
        },
        where: plugins.InjectorPlugin.POSITION.PREFIXED,
      }),
      new plugins.RemovalPlugin([
        {
          test: new RegExp(
            `^(?!((${config
              .get('brands')
              .concat(['default'])
              .join('|')})\/.+)).*\\.css$`
          ),
        },
        {
          test: new RegExp(
            `(${config
              .get('brands')
              .concat(['default'])
              .join('|')}).*\\.(js)$`
          ),
        },
        {
          test: new RegExp(
            `(${config
              .get('brands')
              .concat(['bootstrap-grid', 'bundle'])
              .join('|')}).*\\.(map)$`
          ),
        },
      ]),
      new plugins.ResponsiveCssPlugin(),

      override(
        new plugins.CssNanoPlugin({ autoprefixer: false, zindex: false })
      ),
      override(new plugins.AddHashesPlugin()),

      new ChunkManifestPlugin({
        filename: 'webpack-manifest.json',
        manifestVariable: 'webpackManifest',
      }),
      new plugins.GenerateAssets(),
      override(
        new NewRelicSourceMapPlugin({
          applicationId: process.env.NEW_RELIC_APPLICATION_ID,
          nrAdminKey: process.env.NEWRELIC_KEY,
          staticAssetUrl: process.env.WEB_URL,
          noop: typeof process.env.NEWRELIC_KEY === 'undefined',
        })
      ),
      override(
        undefined,
        new WriteFilePlugin({
          log: false,
          test: /generated-assets.json$/,
          useHashIndex: true,
        })
      ),
      override(undefined, new webpack.HotModuleReplacementPlugin()),
      new webpack.NormalModuleReplacementPlugin(
        /server\/lib\/logger\.js/,
        path.resolve(config.get('dir_src'), 'client/lib/logger.js')
      ),
    ]),
    module: {
      loaders: [
        {
          test: /\.css$/,
          exclude: [/src\/custom/],
          use: extractCSS.extract({
            use: [
              'css-loader?sourceMap&-url',
              {
                loader: 'postcss-loader',
                options: {
                  plugins: (loader) => [require('lost')],
                },
              },
            ],
          }),
        },
        {
          test: /\.css$/,
          include: [/src\/custom/],
          loader: 'postcss-loader',
          options: {
            plugins: (loader) => [
              require('postcss-import')(),
              require('postcss-simple-vars')(),
              require('postcss-conditionals')(),
              require('postcss-cssnext')({
                browsers: ['iOS >= 7', 'Android >= 4', 'Chrome >= 43'],
              }),
              require('postcss-reporter')(),
            ],
          },
        },
        {
          test: /\.scss$/,
          loader: extractSCSS.extract({
            use: [
              'css-loader',
              'sass-loader',
              {
                loader: 'postcss-loader',
                options: {
                  ident: 'postcss',
                  plugins: () => [
                    require('postcss-cssnext')({
                      features: {
                        customProperties: {
                          variables: Object.assign(
                            {},
                            require('../../src/shared/constants/responsive.js')
                              .grid
                          ),
                        },
                      },
                    }),
                  ],
                },
              },
            ],
          }),
        },
      ].concat(
        override(
          [
            {
              test: /\.(js|jsx)$/,
              exclude: /node_modules/,
              loader: 'babel-loader',
            },
          ],
          [
            {
              test: /\.(js|jsx)$/,
              exclude: [/node_modules/, /variables/],
              loader: ['happypack/loader?id=js'],
            },
            {
              include: /\.json$/,
              loader: ['json-loader'],
            },
          ]
        )
      ),
    },
  })
}
