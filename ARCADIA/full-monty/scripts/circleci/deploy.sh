#!/bin/bash
set -x

VERSION=$1
EB_ENVIRONMENT_NAME=$2
EB_BUCKET="${3:-elasticbeanstalk-eu-west-1-623971903498}"
MONTY_MEMORY_SIZE="${MONTY_MEMORY_SIZE:-512}"

source "$(dirname $0)/../lib/config.sh"
source "$(dirname $0)/../lib/shell_utils.sh"
source "$(dirname $0)/../lib/eb.sh"
source "$(dirname $0)/../lib/docker.sh"

scripts/docker_tag_exists_on_ecr.sh full-monty $VERSION
echo "result: $?"
if [ "$?" = 0 ]; then
  echo "The image $DOCKER_IMAGE_NAME in ECR already contains the tag $VERSION, continuing..."
else
  echo "The image $DOCKER_IMAGE_NAME doesn't contain the tag $VERSION, building and pushing it to ECR..."
  build_and_push ${DOCKER_ECR_IMAGE_PREFIX}/full-monty ${DOCKER_ECR_IMAGE_PREFIX}/monty-lb ${DOCKER_ECR_IMAGE_PREFIX}/metricbeat $VERSION
fi

if [ "$?" != 0 ]; then
  echo "something when wrong, exiting..."
  exit 1
fi

echo "Creating application version $VERSION..."
create_application_version $APPLICATION_NAME $VERSION $EB_BUCKET $MONTY_MEMORY_SIZE $EB_ENVIRONMENT_NAME true

environment_exists $EB_ENVIRONMENT_NAME
if [ "$?" != 0 ]; then
  echo "elasticbeanstalk environment with name $EB_ENVIRONMENT_NAME doesn't exist, creating it..."
  environment_id=$(create_environment $EB_ENVIRONMENT_NAME $APPLICATION_NAME $VERSION | get_value_of_json_key "EnvironmentId")
  wait_for_environment_state $environment_id "Ready"
else
  echo "elasticbeanstalk environment with name $EB_ENVIRONMENT_NAME already exists, waiting for it to be ready and then deploying $VERSION to it..."
  environment_id=$(get_environment_info $EB_ENVIRONMENT_NAME | get_value_of_json_key "EnvironmentId")
  wait_for_environment_state $environment_id "Ready"
  environment_id=$(update_environment $EB_ENVIRONMENT_NAME ECR-$VERSION | get_value_of_json_key "EnvironmentId")
  wait_for_environment_state $environment_id "Ready"
  deployed_version=$(get_deployed_version_by_environment_id $environment_id)
  if [ ${deployed_version} != ECR-${VERSION} ]; then
    echo "Deployment failed! Previous version (${deployed_version}) is still in the ${EB_ENVIRONMENT_NAME} environment"
    exit 1
  fi
fi
