const basket = require('./basket.json')
const creditCard = require('./creditCard')
const deliveryLocations = require('./deliveryLocations.json')
const discounts = require('./discounts.json')
const giftCards = require('./giftCards.json')
const savedAddresses = require('./savedAddresses.json')
const plpProducts = require('./plpProducts.json')
const plpRefinements = require('./plpRefinements.json')
const productAssets = require('./productAssets.json')
const inventoryPositions = require('./inventoryPositions')
const orderAddress = require('./orderAddress.json')
const userDetails = require('./userDetails.json')
const pdpProduct = require('./pdpProduct.json')
const savedBasketProduct = require('./savedBasketProduct.json')
const account = require('./account.json')
const orderLines = require('./orderLines.json')
const paymentDetails = require('./paymentDetails.json')
const orderSummary = require('./orderSummary')

module.exports = {
  basket,
  creditCard,
  deliveryLocations,
  discounts,
  giftCards,
  savedAddresses,
  plpProducts,
  plpRefinements,
  productAssets,
  inventoryPositions,
  orderAddress,
  userDetails,
  pdpProduct,
  savedBasketProduct,
  account,
  orderLines,
  paymentDetails,
  orderSummary
}
