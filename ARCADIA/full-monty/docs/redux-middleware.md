# New Pipeline

The new pipeline works as follows:

## Actions

To create an API request you now write an action that returns a [MONTY_API] json object. (See below for the format and options).

## Preprocess API Middleware

Converts `[MONTY_API]` into `[CALL_API]` object, with the following options set.

* Adds brandcode header
* Adds content-type: application/json header
* Enables same origin cookies
* Converts typesPrefix in to the three required types with the `_API_[REQUESTS|SUCCESS|FAILURE]` suffixes
* Converts the `meta` sub-object in to the correct format and merges the `meta.all` options in to the request/success/failure objects
* Adds `requestURL` to the meta objects
* Moves the `formName` property to the meta objects
* Converts the `body` property to JSON
* On Node prefixes `http://localhost:3000` to the request URL

## Redux_API_Middleware

Takes the `[CALL_API]` object and uses Isomorphic-fetch to make the API call. On receipt of the `[CALL_API]` it dispatches an `typesPrefix_API_REQUEST` Redux action. Once the fetch returns it dispatches a second action, which can be either `typesPrefix_API_SUCCESS` or `typesPrefix_API_FAILURE`.

# UPDATE: Do not use redux-sagas, use redux-thunk instead. redux-saga is deprecated and will be removed.

## Redux-Sagas

The next stage of the pipeline uses [ Redux-Saga](https://redux-saga.github.io/redux-saga/) to process Action Side Effects. The following Sagas may be activated as the dispatch action pass through this stage of the middleware pipeline.

### Ajax Counter Saga

For any action ending in `_API_REQUEST`, fires the `ajaxCounter('increment')`.
For any action ending in `_API_SUCCESS` or `_API_FAILURE` fire `ajaxCounter('decrement')`.

### Analytics Saga

Looks for any action that contains `meta.analytics` object and process the contained analytics.

### API Errors Saga

Process error messages attached to actions by Redux_API_Middleware

### Form Submit Saga

Looks for action types that contain `_FORM_API_` and can then fire the `setFormMessage`, setFormSuccess`and`setFormMeta` actions depending on the options in the meta part of the action.

### Next API Saga

If an action contains `meta.nextAPI` that is a valid `[MONTY_API]` or `[CALL_API]` action then dispatch it. This allows for the chaining of API calls. The response from the first API call is passed to the next API call.

### Forget Password Request Saga

This is a custom saga for the `FORGOT_PASSWORD_FORM_API_SUCCESS` action.

## Reducers

Once the Sagas have completed, the action is then passed to the reducers to add the data to the store. In the past Redux-Thunk was used to fire lots of actions. In this model, the reducers listen for every action that could effect the data they look after.

# Making an API request

API request actions can now be written in the following format.

```js
export function myApiRequest(data) {
  return {
    [MONTY_API]: {
      endpoint: <String: URL>,
      method: <String: HTTP Method>,
      body: data,
      overlay: <BOOL>,
      typesPrefix: <String: Redux type>,
      formName: <String>,
      meta: {
        all: {
          message: <String>,
          dispatch: [pureActionCreater(payload)],
          formMeta: <Array: setFormMeta options (Execlude formName)>,
          errorAlertBox: <BOOL>,
          analytics: {
            type: <String: Analytics type>,
            data,
            events: <Array: String>
          }
        }
        request: { ... }
        success: { ... }
        failure: { ... }
      }
    }
  }
}
```

At it simplest that can be reduced down to:

```js
export function orderHistoryRequest() {
  return {
    [MONTY_API]: {
      endpoint: '/account/order-history',
      method: 'GET',
      overlay: true,
      typesPrefix: 'SET_ORDER_HISTORY',
    },
  }
}
```

The above example causes a 'SET_ORDER_HISTORY_API_REQUEST' action to be sent, followed by either a 'SET_ORDER_HISTORY_API_SUCCESS' or 'SET_ORDER_HISTORY_API_FAILURE' action. These actions inherit the meta object of the MONTY_API action.

# Localising API response messages

Additional middleware has been added to localise API Response messages. To use it, list the properties that need translating in the `meta.localise` property. It will except either a string, or an array of strings.

```js
{
  type: 'FOO',
  message: 'Text to translate',
  meta: {
    localise: 'message'
  }
}
```

It also supports FSAs and actions with nested properties.

```js
{
  type: 'FOO',
  payload: {
    data: {
      message1: 'foo',
      message1: 'bar',
    }
  },
  meta: {
    localise: [
      'data.message1'
      'data.message2'
    ]
  }
}
```

# Custom side effects

In situations where an action requires a side effect that can not be handled directly by a reducer function. A custom Saga should be written to process this.

Here is an example of a simple Saga that observes for password resets and the updates the store. Once that it is done it then observes the password reset accordion being closed and updates the page. Ther `forgetPwdRequest()` function sets up the main observer for the `'FORGOT_PASSWORD_FORM_API_SUCCESS'` action and then calls `forgetPwdRequestSaga()`.

This `forgetPwdRequestSaga()` function gets the current pathname from the store using `yield select()`, then fires the `setPostResetUrl` action using `yield put()`. It then waits for the `TOGGLE_ACCORDION` action and then dispatches `setFromMessage` to clear the now colapsed password reset form.

```js
import { takeEvery } from 'redux-saga'
import { select, put, take } from 'redux-saga/effects'
import { setPostResetUrl } from '../../actions/components/ChangedPasswordActions'
import { setFormMessage } from '../../actions/common/formActions'

const getPathname = (state) => state.routing.location.pathname

export function* forgetPwdRequestSaga() {
  const pathname = yield select(getPathname)
  yield put(
    setPostResetUrl(
      pathname.startsWith('/checkout') ? '/checkout' : '/my-account'
    )
  )

  yield take(
    (action) =>
      action.type === 'TOGGLE_ACCORDION' && action.key === 'forgetPassword'
  )
  yield put(setFormMessage('forgetPassword', {}))
}

export default [
  function* forgetPwdRequest() {
    yield* takeEvery('FORGOT_PASSWORD_FORM_API_SUCCESS', forgetPwdRequestSaga)
  },
]
```
