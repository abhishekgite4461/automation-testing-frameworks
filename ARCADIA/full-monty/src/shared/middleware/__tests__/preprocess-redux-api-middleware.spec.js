const testModule = '../preprocess-redux-api-middleware'
const auth = { token: 'token' }
const config = { storeCode: 'storeCode' }
const viewport = { media: 'mobile' }
const hostname = { isMobile: true }
const getState = () => ({ auth, config, viewport, hostname })
const snapshot = (action) => expect(action).toMatchSnapshot()

function testMiddleware(isBrowser) {
  let dispatch
  let _MONTY_API_

  beforeEach(() => {
    global.process.browser = !!isBrowser
    jest.resetModules()

    // eslint-disable-next-line import/no-dynamic-require
    const { MONTY_API, montyApiMiddleware } = require(testModule)
    _MONTY_API_ = MONTY_API
    dispatch = montyApiMiddleware({ getState })(snapshot)
  })

  afterEach(() => {
    delete global.process.browser
  })

  it('Ignore non Monty_API request', () => {
    dispatch({ type: 'FOO' })
  })

  it('Processes simple request', () => {
    dispatch({
      [_MONTY_API_]: {
        endpoint: '/foo',
        method: 'GET',
        overlay: false,
        typesPrefix: 'FOO',
      },
    })
  })

  it('Processes complex request', () => {
    dispatch({
      [_MONTY_API_]: {
        endpoint: '/foo',
        method: 'FORM',
        body: { foo: 'bar' },
        overlay: true,
        typesPrefix: 'FOO_FORM',
        formName: 'formName',
        meta: {
          all: {
            foo: 'all',
          },
          request: {
            bar: 'request',
          },
          success: {
            bar: 'success',
          },
          failure: {
            bar: 'failire',
          },
        },
      },
    })
  })

  it('Throws invalid endpoint', (done) => {
    try {
      dispatch({
        [_MONTY_API_]: {
          endpoint: {},
          method: 'GET',
          overlay: false,
          typesPrefix: 'FOO',
        },
      })
    } catch (e) {
      expect(e.message).toBe('Specify a string endpoint URL')
      expect(e).toMatchSnapshot()
      done()
    }
  })
}

describe('Monty API Middleware', () => {
  describe('Server', () => {
    testMiddleware()
  })

  describe('Browser', () => {
    testMiddleware(true)
  })
})
