jest.mock('../../lib/localisation', () => ({
  localise: jest.fn(() => 'translated'),
}))
import localiseMiddleware from '../localise-middleware'

const snapshot = (action) => expect(action).toMatchSnapshot()
const getState = () => ({
  config: {
    language: 'XX',
    brandName: 'YY',
  },
})

describe('Localise Middleware', () => {
  const dispatchSnapshot = localiseMiddleware({ getState })(snapshot)

  it('Ingores actions without meta.localise', () => {
    dispatchSnapshot({
      type: 'FOO',
      message: 'bar',
    })
  })

  it('Processes actions with meta.localise', () => {
    dispatchSnapshot({
      type: 'FOO',
      message: 'bar',
      meta: {
        localise: 'message',
      },
    })
  })

  it('Processes complex actions with meta.localise', () => {
    dispatchSnapshot({
      type: 'FOO',
      data: {
        message: 'bar',
      },
      meta: {
        localise: 'data.message',
      },
    })
  })

  it('Processes FSAs with meta.localise', () => {
    dispatchSnapshot({
      type: 'FOO',
      payload: {
        message: 'bar',
      },
      meta: {
        localise: 'message',
      },
    })
  })

  it('Processes complex FSAs with meta.localise', () => {
    dispatchSnapshot({
      type: 'FOO',
      payload: {
        data: {
          message: 'bar',
        },
      },
      meta: {
        localise: 'data.message',
      },
    })
  })
})
