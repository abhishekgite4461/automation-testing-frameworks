import { CALL_API } from 'redux-api-middleware'
import { isMobileHostname } from '../lib/hostname'
import { path } from 'ramda'

const MONTY_API = Symbol('Call Arcdia API')
const apiTypes = ['REQUEST', 'SUCCESS', 'FAILURE']
const prefix = process.browser
  ? ''
  : `${
      process.env.CORE_API_HOST
        ? `http://${process.env.CORE_API_HOST}`
        : 'http://localhost'
    }:${process.env.CORE_API_PORT || 3000}`

function setEndpoint(path) {
  if (typeof path !== 'string') {
    throw new TypeError('Specify a string endpoint URL')
  }

  return path.includes('https') ? path : `${prefix}/api${path}`
}

function ensureString(body) {
  return typeof body === 'object' ? JSON.stringify(body) : body
}

function getCodeFromState(state) {
  return state.config.storeCode
}

function setBrandHeader(state) {
  return { 'BRAND-CODE': getCodeFromState(state) }
}

function setIsMobileHostnameHeader(state) {
  let hostnameIsMobile

  if (!process.browser) {
    hostnameIsMobile = path(['hostname', 'isMobile'], state)
  } else {
    hostnameIsMobile = isMobileHostname(window.location.host)
  }

  return {
    // Converting to string otherwise the header won't be passed on server side rendering.
    'monty-mobile-hostname': String(hostnameIsMobile),
  }
}

function setCookieHeader({
  auth: { token },
  jsessionid: { value: jsessionid } = {},
  viewport: { media: deviceType },
}) {
  if (!process.browser) {
    const cookies = []
    if (token) {
      cookies.push(`token=${token}`)
    }
    // This mimics the functionality in shared/lib/api-service.js, only it is applied to redux-saga account actions.
    // In the event the server-side renderer needs to produce an account page, the component's needs will trigger a call to get account details.
    // In order for WCS to return account details, a jsessionid needs to be provided as a cookie.
    // The jsessionid is obtained from the store, as it should have been saved from the request header before this stage.
    if (jsessionid && process.env.USE_NEW_HANDLER === 'true') {
      cookies.push(`jsessionid=${jsessionid}`)
    }

    // Retrieving from the state the device type set by server-side-renderer and setting a cookie that
    // will be used in coreApi to send to WCS data about the device making the request.
    cookies.push(`deviceType=${deviceType}`)

    return { cookie: `${cookies.join('; ')}` }
  }
}

function attachHeaders(headers = {}, state) {
  return Object.assign(
    headers,
    { 'Content-Type': 'application/json' },
    setBrandHeader(state),
    setCookieHeader(state),
    setIsMobileHostnameHeader(state)
  )
}

function ensureObjects(data = {}) {
  return {
    all: data.all || {},
    each: [data.request || {}, data.success || {}, data.failure || {}],
  }
}

function createTypes(action) {
  const meta = ensureObjects(action.meta)

  meta.all.overlay = !!action.overlay
  meta.all.requestUrl = setEndpoint(action.endpoint)
  if (action.formName) meta.all.formName = action.formName

  return apiTypes.map((apiType, i) => ({
    type: `${action.typesPrefix}_API_${apiType}`,
    meta: Object.assign({}, meta.all, meta.each[i]),
  }))
}

function createNewAction(action, state) {
  return {
    body: ensureString(action.body),
    credentials: 'same-origin',
    endpoint: setEndpoint(action.endpoint),
    headers: attachHeaders(action.headers, state),
    types: createTypes(action),
  }
}

function processApiRequest(action, state) {
  const newAction = createNewAction(action, state)

  delete action.formName
  delete action.meta
  delete action.overlay
  delete action.typesPrefix

  return Object.assign(action, newAction)
}

const montyApiMiddleware = ({ getState }) => (next) => (action) => {
  return next(
    action[MONTY_API]
      ? { [CALL_API]: processApiRequest(action[MONTY_API], getState()) }
      : action
  )
}

export { MONTY_API, montyApiMiddleware }
