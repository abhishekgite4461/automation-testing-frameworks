module.exports = {
  'wishlist-icon-width': '22px',
  'wishlist-icon-height': '22px',
  'wishlist-header-icon-height': '18px',
  'wishlist-header-icon-width': '18px',
  'wishlist-transition-property': 'transform',
  'wishlist-transition-duration': '200ms',
  'wishlist-transition-timing': 'ease-in',
  'wishlist-icon-animation-speed': 'fast',
  'wishlist-icon-transform': 'scale(1.1)',
  'wishlist-icon-animation-duration': '1000ms',

  'wishlist-plp-icon-height-mega': '31px',
}
