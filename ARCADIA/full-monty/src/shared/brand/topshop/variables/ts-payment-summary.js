module.exports = {
  'payment-summary-header-line-height': '1.09',
  'payment-summary-table-head-font-weight': '300',
  'payment-summary-table-row-font-family': 'Platform',
  'payment-summary-total-col-padding': '20px 0',
}
