module.exports = {
  'font-family-primary': 'Platform, sans-serif',
  'font-family-secondary': 'Interstate, sans-serif',
  'line-height-base': '18px',
  'overlay-opacity': '0.6',
  'font-weight-base': 'lighter',
  'font-size-base': '14px',
}
