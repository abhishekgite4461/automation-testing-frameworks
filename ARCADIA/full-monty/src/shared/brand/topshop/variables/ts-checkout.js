const colors = require('./ts-colors')

const general = {
  'checkout-secure-payment-margin-top': '0',
  'checkout-secure-payment-padding': '10px',
}

const checkoutBagSide = {
  'checkout-bag-side-simple-total-section-padding-vertical': '20px',

  'checkout-bag-side-border-color': colors['md-gray'],
  'checkout-bag-side-title-font-size': '21px',
}

module.exports = Object.assign(general, checkoutBagSide)
