const colors = require('./ts-colors')

module.exports = {
  'refinements-summary-header-margin-top': '15px',
  'refinements-summary-header-font-weight': '600',
  'refinements-summary-item-title-color': colors.black,
  'refinements-summary-item-title-font-weight': '500',
  'refinements-summary-item-background-color': 'transparent',
  'refinements-summary-item-padding': '0',
  'refinements-summary-item-color': colors['dk-gray'],

  // desktop styles
  'refinements-summary-item-top-desktop': '0',
}
