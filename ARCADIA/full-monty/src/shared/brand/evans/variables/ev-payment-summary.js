module.exports = {
  'payment-summary-header-font-size': '22px',
  'payment-summary-header-font-weight': 'bold',
  'payment-summary-header-line-height': '1.09',

  'payment-summary-table-head-font-weight': 'bold',

  'payment-summary-table-row-line-height': '1.25',
  'payment-summary-table-row-letter-spacing': 'normal',
  'payment-summary-table-row-font-weight': 'bold',
  'payment-summary-total-col-padding': '0 0 20px',

  'payment-summary-table-col-right-font-size': '16px',
  'payment-summary-table-col-right-font-weight': 'bold',
  'payment-summary-table-col-right-line-height': '1.25',
}
