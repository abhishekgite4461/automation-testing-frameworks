const colors = require('./ev-colors')

module.exports = {
  'refinements-header-border-bottom': `1px solid ${colors['md-gray']}`,

  'refinements-close-icon-width': '12px',

  'refinements-clear-button-disabled-color': colors.black,

  'refinements-title-font-size': '2.3em',
  'refinements-title-font-weight': '300',
  'refinements-title-margin-top': '0',

  'refinements-items-header-font-weight': 'bold',
}
