const colors = require('./ev-colors')

module.exports = {
  'myaccount-border-color': colors['lt-gray'],

  'myaccount-item-title-font-weight': '500',
}
