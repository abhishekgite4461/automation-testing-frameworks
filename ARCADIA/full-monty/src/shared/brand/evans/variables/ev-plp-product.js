const base = require('./ev-base')
const colors = require('./ev-colors')

const plpProduct = {
  'plp-product-borders': 'none',
  'plp-product-alignment': 'left',
  'plp-product-info-padding-left-desktop': '0',
  'plp-product-info-padding-bottom-desktop': '0',
  'plp-product-info-margin-bottom': '30px',
  'plp-product-line-height': '13px',
  'plp-product-name-margin-bottom-desktop': '5px',
  'plp-product-image-aspect-ratio': '135.9433962264%',
}

const plpProductTitle = {
  'plp-product-title-font': base['font-family-primary'],
  'plp-product-title-font-size': '1em',
  'plp-product-title-font-size-desktop': '12px',
  'plp-product-title-font-color': colors['dk-gray'],
  'plp-product-title-font-color-desktop': colors['desk-black'],
  'plp-product-title-font-weight': 'normal',
  'plp-product-title-font-weight-desktop': 'normal',
}

const plpProductPrice = {
  'plp-product-price-font': base['font-family-primary'],
  'plp-product-price-spacing': '0',

  'plp-product-unit-price-font-size': '1em',
  'plp-product-unit-price-font-color': colors['md-dk-gray'],
  'plp-product-unit-price-font-color-desktop': colors['desk-black'],
  'plp-product-unit-price-font-weight-desktop': 'bold',

  'plp-product-old-price-font-size': '1em',
  'plp-product-old-price-font-color': colors['md-dk-gray'],

  'plp-product-now-price-font-size': '1em',
  'plp-product-now-price-font-size-desktop': '13px',
  'plp-product-now-price-font-color': colors['now-price-red'],
}

const plpProductSwatches = {
  'plp-product-swatch-selected-border-color': colors['lt-gray'],
  'plp-product-swatch-button-font-color': colors.black,
  'plp-product-swatch-button-font-size': 0,
  'plp-product-swatch-button-image':
    'url("public/evans/images/nav-arrow.svg") ',
  'plp-product-swatch-button-prev-transform': 'rotate(180deg)',
}

const plpProductAttributeBanner = {
  'plp-product-attribute-banner-left-property': '0%', // IE compatible left align, must be modified if width changes
}

const plpProductPromoBanner = {
  'plp-product-promo-banner-align': 'left',
}

module.exports = Object.assign(
  plpProduct,
  plpProductTitle,
  plpProductPrice,
  plpProductSwatches,
  plpProductAttributeBanner,
  plpProductPromoBanner
)
