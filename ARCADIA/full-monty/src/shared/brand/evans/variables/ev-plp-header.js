const base = require('./ev-base')

module.exports = {
  'plp-header-total-font-family': base['font-family-primary'],
  'plp-header-total-font-weight': 'normal',
  'plp-header-total-font-size': '1.2em',
  'plp-header-total-vertical-align': 'middle',
  'plp-header-padding': '15px 10px 10px',
  'plp-header-title-font-weight': '400',
  'plp-header-title-margin-bottom': null,
  'plp-cat-header-margin-bottom': '10px',
}
