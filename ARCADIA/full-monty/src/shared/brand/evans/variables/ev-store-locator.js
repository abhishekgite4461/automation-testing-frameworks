const colors = require('./ev-colors')

module.exports = {
  'store-locator-store-name-color': colors['dk-gray'],
  'store-locator-store-name-font-size': '0.9em',
  'store-locator-store-name-text-transform': 'none',
  'store-locator-opening-hours-title-font-weight': '600',
  'store-locator-store-address-font-weight': '300',
  'store-locator-details-border-top': 'none',
  'store-locator-accordion-wrapper-border-top': 'none',
  'store-locator-view-in-maps-button-background-color': colors.white,
  'store-locator-view-in-maps-text-color': colors['md-gray'],
  'store-locator-opening-hours-details-font-weight': '300',
}
