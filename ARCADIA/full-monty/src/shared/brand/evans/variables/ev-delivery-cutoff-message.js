module.exports = {
  'delivery-cutoff-message-colour': 'inherit',
  'delivery-cutoff-message-font-weight': '300',
  'delivery-cutoff-message-font-size': '12px',
  'delivery-cutoff-message-line-height': '19px',
  'delivery-cutoff-message-countdown-colour': 'inherit',
}
