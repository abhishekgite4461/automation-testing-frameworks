module.exports = {
  'shopping-cart-display': 'block',
  'shopping-cart-margin': '0 0 0 10px',
  'shopping-cart-margin-tablet': '0 0 0 10px',

  'shopping-cart-label-font-size': '14px',
  'shopping-cart-label-font-size-tablet': '12px',

  'shopping-cart-icon': 'url(/assets/evans/images/shopping-cart-icon.svg)',
  'shopping-cart-icon-active':
    'url(/assets/evans/images/shopping-cart-icon-active.svg)',
  'shopping-cart-icon-height': '27px',
  'shopping-cart-icon-width': '32px',
  'shopping-cart-icon-height-tablet': '22px',
  'shopping-cart-icon-width-tablet': '26px',

  'shopping-cart-count-bg-color': null,
  'shopping-cart-count-color': '#ffffff',
  'shopping-cart-count-display': 'inline-block',
  'shopping-cart-count-font-size': '14px',
  'shopping-cart-count-font-size-tablet': '11px',
  'shopping-cart-count-left': null,
  'shopping-cart-count-line-height': '2em',
  'shopping-cart-count-min-width': null,
  'shopping-cart-count-position': 'relative',
  'shopping-cart-count-top': '2px',
  'shopping-cart-count-vertical-align': 'middle',
}
