module.exports = {
  'wishlist-icon': 'url(/assets/evans/images/wishlist-button.svg)',
  'wishlist-icon-selected':
    'url(/assets/evans/images/wishlist-button-filled.svg)',

  'wishlist-header-icon': 'url(/assets/evans/images/wishlist-button.svg)',
  'wishlist-header-icon-selected':
    'url(/assets/evans/images/wishlist-button-filled.svg)',
  'wishlist-header-icon-background-size': 'contain',
  'wishlist-header-icon-height': '27px',
  'wishlist-header-icon-width': '32px',
  'wishlist-header-icon-height-tablet': '22px',
  'wishlist-header-icon-width-tablet': '26px',
  'wishlist-header-icon-label-font-size': '14px',
  'wishlist-header-icon-label-font-size-tablet': '12px',
  'wishlist-header-icon-margin': '0 0 0 10px',
  'wishlist-header-icon-margin-tablet': '0 0 0 10px',
  'wishlist-transition-property': 'transform',
  'wishlist-transition-duration': '200ms',
  'wishlist-transition-timing': 'ease-in',
  'wishlist-icon-hover': 'url(/assets/evans/images/wishlist-button.svg)',
  'wishlist-icon-animation-speed': 'fast',
  'wishlist-icon-transform': 'scale(1.1)',
  'wishlist-icon-animation-duration': '1000ms',

  'wishlist-plp-icon': 'url(/assets/evans/images/wishlist-button.svg)',
  'wishlist-plp-icon-hover': 'url(/assets/evans/images/wishlist-button.svg)',
  'wishlist-plp-icon-selected':
    'url(/assets/evans/images/wishlist-button-filled.svg)',
}
