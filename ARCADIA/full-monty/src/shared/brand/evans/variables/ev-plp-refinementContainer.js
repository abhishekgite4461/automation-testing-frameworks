const plpRefinementListContainer = {
  'plp-refinementListContainer-padding-right': '15px',
}

const plpRefinementListContainerFixed = {
  'plp-refinementListContainerFixed-padding-right': '12px',
}

module.exports = Object.assign(
  plpRefinementListContainer,
  plpRefinementListContainerFixed
)
