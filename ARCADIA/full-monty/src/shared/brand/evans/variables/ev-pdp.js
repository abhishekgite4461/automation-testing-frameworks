const colors = require('./ev-colors')

const pdpGeneral = {
  'pdp-vertical-padding': '15px',
}

const pdpTitle = {
  'pdp-product-title-font-size-tablet': '20px',
  'pdp-product-title-line-height-tablet': '24px',
  'pdp-product-title-font-size-laptop': '22px',
  'pdp-product-title-line-height-laptop': '26px',
}

const pdpPrices = {
  'pdp-product-old-price-font-size': '1em',
}

const pdpProductSizes = {
  'pdp-product-size-title-align': 'left',
  'pdp-product-size-title-color': colors.black,
  'pdp-product-size-title-text-transform': 'capitalize',
  'pdp-product-size-title-font-weight': 'normal',
  'pdp-product-size-title-content-after': '":"',
  'pdp-product-size-title-margin-bottom': '10px',

  'pdp-product-size-gutter': '6%',
  'pdp-product-size-height': '50px',
  'pdp-product-size-vertical-margin': '28px',
  'pdp-product-size-oos-opacity': '0.5',
}

const pdpSwatches = {
  'pdp-product-swatch-size': '40px',
  'pdp-product-swatch-border-width': '2px',
  'pdp-product-swatch-selected-border-color': colors['lt-gray'],
  'pdp-product-swatch-border-radius': '50%',
  'pdp-product-swatch-link-border-color': colors.white,
  'pdp-product-swatch-link-border-width': '2px',
}

const pdpRecommendations = {
  'pdp-recommendations-title-border': null,
  'pdp-recommendations-title-text-align': 'left',
  'pdp-recommendations-title-font-size': '1.2em',
  'pdp-recommendations-padding': '0 0 10px',
}

const pdpSizeGuide = {
  'pdp-size-guide-position': null,
  'pdp-size-guide-block-bottom': '15px',
  'pdp-size-guide-offset-bottom': '20px',
  'pdp-size-guide-offset-top': null,
  'pdp-size-guide-offset-left': null,
  'pdp-size-guide-offset-right': null,
  'pdp-size-guide-color': colors['dk-gray'],
  'pdp-size-guide-icon-display': 'none',
  'pdp-size-guide-text-transform': 'capitalize',
  'pdp-size-guide-font-weight': 'bold',
  'pdp-size-guide-border-color': colors['dk-gray'],
  'pdp-size-guide-border-width': '1px',
}

const pdpQuantity = {
  'pdp-product-quantity-label-after-content': ':',
}

const pdpPriceWrapper = {
  'pdp-price-font-size': '1.3em',
}

module.exports = Object.assign(
  pdpGeneral,
  pdpTitle,
  pdpPrices,
  pdpProductSizes,
  pdpSwatches,
  pdpSizeGuide,
  pdpRecommendations,
  pdpQuantity,
  pdpPriceWrapper
)
