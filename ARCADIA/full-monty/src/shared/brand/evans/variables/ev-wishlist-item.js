module.exports = {
  'wishlist-item-title-color': '#666666',
  'wishlist-item-title-font-size': '12px',
  'wishlist-item-title-font-weight': '400',
  'wishlist-item-title-letter-spacing': '0',
  'wishlist-item-title-line-height': '13px',
  'wishlist-item-title-margin': '14px 14px 0 0',
  'wishlist-item-title-text-align': 'left',
  'wishlist-item-title-text-margin': '0 0 5px',
  'wishlist-item-button-remove-mask': 'none',
  'wishlist-item-button-remove-mask-size': '0',

  'wishlist-item-price-font-size': '13px',
  'wishlist-item-price-font-weight': '700',
  'wishlist-item-price-line-height': '13px',
  'wishlist-item-price-margin': '0',
  'wishlist-item-price-text-align': 'left',

  'wishlist-item-select-margin': '15px 0',
}
