const colors = require('./ev-colors')

const general = {
  'checkout-secure-payment-padding': '12px',
  'checkout-secure-payment-padding-min-tablet': '17px',
  'checkout-secure-payment-padding-min-laptop': '16px',

  'checkout-continue-shopping-margin-right': '20px',
  'checkout-continue-shopping-margin-top-min-tablet': '21px',
  'checkout-continue-shopping-margin-top-min-laptop': '26px',
}

const checkoutBagSide = {
  'checkout-bag-side-border-color': colors['lt-gray'],

  'checkout-bag-side-title-font-size': '22px',
  'checkout-bag-side-title-font-weight': 'bold',

  'checkout-bag-side-total-font-size': '16px',
  'checkout-bag-side-total-font-weight': 'bold',

  'checkout-bag-side-simple-total-section-font-size': '13px',

  'checkout-bag-side-simple-total-border-top': `1px solid ${colors['lt-gray']}`,
  'checkout-bag-side-simple-total-border-bottom': `1px solid ${
    colors['lt-gray']
  }`,
  'checkout-bag-side-simple-total-background-color': '#f8f8f8',
}

const delivery = {
  'checkout-delivery-title-font-weight': 700,
}

const DeliveryType = {
  'checkout-delivery-type-description-font-size': null,
  'checkout-delivery-type-description-font-weight': 'normal',
  'checkout-delivery-type-title-font-size': '0.95em',
  'checkout-delivery-type-title-font-weight': 700,
  'checkout-delivery-type-title-line-height': '20px',
}

module.exports = Object.assign(general, checkoutBagSide, delivery, DeliveryType)
