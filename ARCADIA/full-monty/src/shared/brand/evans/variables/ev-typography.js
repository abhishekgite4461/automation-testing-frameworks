const base = require('./ev-base')
const colors = require('./ev-colors')

module.exports = {
  'font-family-body': base['font-family-primary'],
  'font-family-h1': base['font-family-primary'],
  'font-family-h2': base['font-family-primary'],
  'font-family-h3': base['font-family-primary'],
  'font-family-h4': base['font-family-primary'],
  'font-family-p': base['font-family-primary'],
  'font-family-label': base['font-family-primary'],
  'font-family-input': base['font-family-primary'],
  'font-family-select': base['font-family-primary'],
  'font-family-button': base['font-family-secondary'],

  'font-size-h1': '22px',
  'font-size-h2': '20px',
  'font-size-h3': '20px',
  'font-size-input': '16px',
  'font-size-label': '14px',
  // 'font-size-h2': '1em',
  // 'font-size-h3': '0.85em',
  // 'font-size-h4': '1em',
  'font-size-navigation': '1em',

  'line-height-h1': '26px',
  'line-height-h2': '24px',
  'line-height-h3': '20px',
  'line-height-input': '18px',
  'line-height-label': '18px',

  'letter-spacing-h1': '0',

  'font-color-base': colors.black,
  'font-color-h1': colors.black,
  'font-color-h2': colors.black,
  'font-color-h3': colors.black,
  'font-color-h4': colors.black,
  'font-color-p': colors.black,
  'font-color-label': colors.black,

  'font-weight-h1': 700,
  'font-weight-h2': 700,
  'font-weight-h3': 700,
  'font-weight-h4': base['font-weight-base'],
  'font-weight-p': base['font-weight-base'],
  'font-weight-label': base['font-weight-base'],
  'font-weight-input': 'normal',

  'text-transform-h1': 'capitalize',
  'text-transform-h2': 'capitalize',
  'text-transform-h3': 'none',
  'text-transform-h4': 'none',
}
