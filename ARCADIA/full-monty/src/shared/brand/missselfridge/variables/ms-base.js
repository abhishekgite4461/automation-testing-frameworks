module.exports = {
  'font-family-primary': 'AvenirNext, sans-serif',
  'font-family-secondary': 'AvenirNext, sans-serif',
  'line-height-base': '1.2',
  'font-size-base': '14px',
  'font-weight-base': 'lighter',
}
