module.exports = {
  'wishlist-icon': 'url(/assets/missselfridge/images/wishlist-button.svg)',
  'wishlist-icon-selected':
    'url(/assets/missselfridge/images/wishlist-button-filled.svg)',

  'wishlist-header-icon':
    'url(/assets/missselfridge/images/wishlist-header.svg)',
  'wishlist-header-icon-selected':
    'url(/assets/missselfridge/images/wishlist-header-filled.svg)',
  'wishlist-header-icon-height': '22px',
  'wishlist-header-icon-width': '24px',
  'wishlist-header-icon-height-tablet': '22px',
  'wishlist-header-icon-width-tablet': '24px',
  'wishlist-header-icon-margin': '0 0 0 20px',
  'wishlist-header-icon-margin-tablet': '0 0 0 15px',
  'wishlist-transition-property': 'transform',
  'wishlist-transition-duration': '200ms',
  'wishlist-transition-timing': 'ease-in',
  'wishlist-icon-animation-speed': 'slow',
  'wishlist-icon-hover':
    'url(/assets/missselfridge/images/wishlist-button.svg)',
  'wishlist-icon-transform': 'scale(1.1)',
  'wishlist-icon-animation-duration': '1100ms',

  'wishlist-plp-icon': 'url(/assets/missselfridge/images/wishlist-button.svg)',
  'wishlist-plp-icon-hover':
    'url(/assets/missselfridge/images/wishlist-button.svg)',
  'wishlist-plp-icon-selected':
    'url(/assets/missselfridge/images/wishlist-button-filled.svg)',
}
