const colors = require('./ms-colors')

module.exports = {
  'refinements-header-border-bottom': `1px solid ${colors['dk-gray']}`,
  'refinements-header-padding': '10px 0',

  'refinements-close-icon-top': '20px',
  'refinements-close-icon-right': '15px',
  'refinements-close-icon-width': '40px',

  'refinements-clear-button-disabled-color': colors.black,

  'refinements-title-margin-top': '0.5em',
  'refinements-title-margin-bottom': '1.2em',
}
