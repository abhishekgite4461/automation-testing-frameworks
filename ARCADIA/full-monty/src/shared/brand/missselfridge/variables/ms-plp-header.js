const base = require('./ms-base')
const colors = require('./ms-colors')

module.exports = {
  'plp-header-total-font-family': base['font-family-primary'],
  'plp-header-total-color': colors['md-gray'],
  'plp-header-total-font-size': '0.76em',
}
