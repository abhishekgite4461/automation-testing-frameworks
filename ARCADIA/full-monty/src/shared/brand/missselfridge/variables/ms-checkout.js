const colors = require('./ms-colors')

const checkoutBagSide = {
  'checkout-bag-side-border-color': colors['ex-lt-gray'],

  'checkout-bag-side-title-margin-horizontal': '0',
  'checkout-bag-side-title-margin-vertical': '0',
  'checkout-bag-side-title-padding-horizontal': '25px',
  'checkout-bag-side-title-padding-vertical': '15px',

  'checkout-bag-side-background-color': '#FCE9ED',

  'checkout-bag-side-title-font-size': '18px',
  'checkout-bag-side-title-font-weight': '600',
  'checkout-bag-side-simple-total-section-font-size': '12px',

  'checkout-bag-side-total-font-size': '14px',
  'checkout-bag-side-total-font-weight': '600',

  'checkout-secure-payment-padding': '5px',
}

module.exports = Object.assign(checkoutBagSide)
