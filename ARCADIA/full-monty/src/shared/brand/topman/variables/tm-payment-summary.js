module.exports = {
  'payment-summary-header-font-size': '18px',
  'payment-summary-header-font-weight': '500',

  'payment-summary-table-row-line-height': '1.25',
  'payment-summary-table-row-letter-spacing': 'normal',
  'payment-summary-table-row-font-weight': 'normal',
  'payment-summary-table-row-font-size': '14px',

  'payment-summary-table-col-right-font-size': '14px',
  'payment-summary-table-col-right-line-height': '1.25',

  'payment-summary-total-table-col-padding': '0 0 20px',
}
