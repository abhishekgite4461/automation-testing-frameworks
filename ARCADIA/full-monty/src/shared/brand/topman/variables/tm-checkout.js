const colors = require('./tm-colors')

const general = {
  'checkout-secure-payment-padding': '5px',
}

const checkoutBagSide = {
  'checkout-bag-side-border-color': colors['lt-md-gray'],

  'checkout-bag-side-title-font-size': '18px',
  'checkout-bag-side-title-font-weight': '500',

  'checkout-bag-side-simple-total-section-font-size': '14px',
  'checkout-bag-side-simple-total-background-color': colors.white,
  'checkout-bag-side-simple-total-section-padding-vertical': '12px',
  'checkout-bag-side-simple-total-border-bottom': `0 solid ${
    colors['md-gray']
  }`,
}

module.exports = Object.assign(general, checkoutBagSide)
