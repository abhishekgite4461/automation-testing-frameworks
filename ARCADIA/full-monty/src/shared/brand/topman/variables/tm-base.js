module.exports = {
  'font-family-primary': 'Interstate, sans-serif',
  'font-family-secondary': 'sans-serif',
  'line-height-base': 1.2,
  'font-weight-base': '300',
  'page-padding': '15px',
  'espot-padding': '0',

  'font-size-base': '14px',
}
