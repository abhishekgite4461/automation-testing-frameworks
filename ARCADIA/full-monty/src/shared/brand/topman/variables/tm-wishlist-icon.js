module.exports = {
  'wishlist-icon': 'url(/assets/common/images/wishlist-header.svg)',
  'wishlist-icon-selected':
    'url(/assets/common/images/wishlist-header-filled.svg)',
  'wishlist-icon-width': '22px',
  'wishlist-icon-height': '22px',
  'wishlist-header-icon-height': '18px',
  'wishlist-header-icon-width': '18px',
  'wishlist-transition-property': 'transform',
  'wishlist-transition-duration': '200ms',
  'wishlist-transition-timing': 'ease-in',
  'wishlist-icon-animation-speed': 'fast',
  'wishlist-icon-transform': 'scale(1.1)',
  'wishlist-icon-animation-duration': '1000ms',

  'wishlist-plp-icon': 'url(/assets/common/images/wishlist-header.svg)',
  'wishlist-plp-icon-selected':
    'url(/assets/common/images/wishlist-header-filled.svg)',

  'wishlist-plp-icon-width-small': '15px',
  'wishlist-plp-icon-height-small': '12px',
  'wishlist-plp-icon-width-medium': '22px',
  'wishlist-plp-icon-height-medium': '18px',
  'wishlist-plp-icon-width-large': '28px',
  'wishlist-plp-icon-height-large': '25px',
  'wishlist-plp-icon-width-mega': '35px',
  'wishlist-plp-icon-height-mega': '31px',
}
