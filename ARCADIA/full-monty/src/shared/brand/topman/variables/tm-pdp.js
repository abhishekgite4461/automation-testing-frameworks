const colors = require('./tm-colors')

const pdpGeneral = {
  'pdp-vertical-padding': '20px',
}

const pdpTitle = {
  'pdp-product-title-font-size-tablet': '18px',
  'pdp-product-title-line-height-tablet': '22px',
  'pdp-product-title-font-size-laptop': '20px',
  'pdp-product-title-line-height-laptop': '24px',
}

const pdpPrices = {
  'pdp-product-unit-price-font-size': '1em',
  'pdp-product-old-price-font-size': '1.15em',
  'pdp-product-now-price-font-size': '1.15em',
}

const pdpProductSizes = {
  'pdp-product-size-gutter': '4%',
  'pdp-product-size-height': '30px',

  'pdp-product-size-item-font-size': '1em',

  'pdp-product-size-active-border-width': '1px',
  'pdp-product-size-border-color': colors['lt-md-gray'],
  'pdp-product-size-active-border-color': colors['md-gray'],
  'pdp-product-size-active-background-color': colors['lt-gray'],
  'pdp-product-size-color': colors.black,

  'pdp-product-size-sizes-margin-bottom': '25px',

  'pdp-product-size-oos-margin-offset': '50%',
  'pdp-product-size-oos-rotation': '-45deg',
  'pdp-product-size-oos-slash': 'block',
  'pdp-product-size-oos-opacity': '1',

  'pdp-product-size-title-margin-top': '30px',
  'pdp-product-size-title-text-transform': 'initial',
  'pdp-product-size-title-font-size': '0.85em',
}
const pdpSwatches = {
  'pdp-product-swatch-size': '38px',
  'pdp-product-swatch-margin': '4px',
  'pdp-product-swatch-border-width': '1px',
  'pdp-product-swatch-selected-border-color': colors.black,
  'pdp-product-swatch-border-radius': '50%',
  'pdp-product-swatch-link-border-width': '1px',
}

const pdpSizeGuide = {
  'pdp-size-guide-block-bottom': '20px',
  'pdp-size-guide-offset-bottom': '10px',
  'pdp-size-guide-offset-top': null,
  'pdp-size-guide-offset-left': null,
  'pdp-size-guide-offset-right': null,
  'pdp-size-guide-color': colors['dk-gray'],
  'pdp-size-guide-icon-display': 'none',
  'pdp-size-guide-text-transform': 'initial',
  'pdp-size-guide-border-color': colors.black,
  'pdp-size-guide-border-width': '1px',
}

const pdpRecommendations = {
  'pdp-recommendations-title-text-transform': 'none',
}

const pdpDescription = {}

const pdpQuantity = {
  'pdp-product-quantity-margin-bottom': '25px',
}

module.exports = Object.assign(
  pdpGeneral,
  pdpTitle,
  pdpPrices,
  pdpProductSizes,
  pdpSizeGuide,
  pdpSwatches,
  pdpRecommendations,
  pdpDescription,
  pdpQuantity
)
