const base = require('./dp-base')
const colors = require('./dp-colors')

const plpProduct = {
  'plp-product-borders': 'none',
  'plp-product-border-top': null,
  'plp-product-alignment': 'center',
  'plp-product-info-padding-horizontal': '.6em',
  'plp-product-info-padding-top': '.7em',
  'plp-product-info-padding-bottom': '.7em',
  'plp-product-info-margin-bottom': '30px',
  'plp-product-name-margin-bottom': '12px',
  'plp-product-name-margin-bottom-desktop': '5px',
  'plp-product-link-padding-top': '1.2em',
  'plp-product-bottom-padding': '0.5em',
  'plp-product-rating-image-margin-top': '0.6em',
  'plp-product-name-font-weight': '300',
  'plp-product-image-aspect-ratio': '135.9509202454%',
}

const plpProductTitle = {
  'plp-product-title-font': base['font-family-primary'],
  'plp-product-title-font-size': '13px',
  'plp-product-title-font-size-desktop': '13px',
  'plp-product-title-font-color': colors['dk-gray'],
  'plp-product-title-font-weight': 'normal',
  'plp-product-title-font-weight-desktop': '300',
  'plp-product-title-text-transform': 'capitalize',
}

const plpProductPrice = {
  'plp-product-price-font': base['font-family-primary'],
  'plp-product-price-spacing': '0 0 2px',

  'plp-product-unit-price-font-size': '13px',
  'plp-product-unit-price-font-color': colors['dk-gray'],
  'plp-product-unit-price-font-weight': '300',

  'plp-product-old-price-font-size': '13px',
  'plp-product-old-price-font-color': colors['md-gray'],
  'plp-product-old-price-font-style': 'none',
  'plp-product-old-price-font-weight': 'normal',

  'plp-product-now-price-font-size': '13px',
  'plp-product-now-price-font-color': colors['now-price-red'],
  'plp-product-now-price-font-weight': '300',
}

const plpProductSwatches = {
  'plp-product-swatch-list-justify': 'center',
  'plp-product-swatch-list-padding': '6px 0 12px',

  'plp-product-swatch-radius': '50%',
  'plp-product-swatch-selected-border-color': colors['md-gray'],

  'plp-product-swatch-button-image':
    'url("public/dorothyperkins/images/carousel-arrow-right.svg") ',
  'plp-product-swatch-button-font-color': 'transparent',
  'plp-product-swatch-button-font-size': '0.8125em',
  'plp-product-swatch-button-prev-transform': 'rotate(180deg)',
}

const plpQuickview = {
  'quickview-description-display': 'block',
}

module.exports = Object.assign(
  plpProduct,
  plpProductTitle,
  plpProductPrice,
  plpProductSwatches,
  plpQuickview
)
