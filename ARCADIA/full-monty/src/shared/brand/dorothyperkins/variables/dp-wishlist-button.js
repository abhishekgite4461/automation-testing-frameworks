const colors = require('./dp-colors')

module.exports = {
  'wishlist-button-padding': '11px',

  'wishlist-pdp-button-background-color': colors['ex-lt-gray'],

  'wishlist-plp-button-padding-small': '0',
  'wishlist-plp-button-padding-medium': '0',
  'wishlist-plp-button-padding-large': '0',
  'wishlist-plp-button-padding-mega': '0',
  'wishlist-plp-button-top-small': '21px',
  'wishlist-plp-button-top-medium': '26px',
  'wishlist-plp-button-top-large': '28px',
  'wishlist-plp-button-top-mega': '33px',
}
