module.exports = {
  'modal-close-icon-size': '22px',
  'modal-close-icon-font-weight': 300,
  'modal-image-carousel-close-icon-top': '3px',
  'modal-image-carousel-close-icon-right': '7px',
}
