const plpRefinementListContainer = {
  'plp-refinementListContainer-padding-right': '10px',
}

const plpRefinementListContainerFixed = {
  'plp-refinementListContainerFixed-padding-right': '5px',
}

module.exports = Object.assign(
  plpRefinementListContainer,
  plpRefinementListContainerFixed
)
