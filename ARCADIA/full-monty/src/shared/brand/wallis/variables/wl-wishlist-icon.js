module.exports = {
  'wishlist-icon': 'url(/assets/wallis/images/wishlist-icon.svg)',
  'wishlist-icon-selected':
    'url(/assets/wallis/images/wishlist-button-filled.svg)',
  'wishlist-icon-width': '42px',
  'wishlist-icon-height': '42px',

  'wishlist-header-icon': 'url(/assets/wallis/images/wishlist-icon.svg)',
  'wishlist-header-icon-selected':
    'url(/assets/wallis/images/wishlist-icon-active.svg)',
  'wishlist-header-icon-height': '27px',
  'wishlist-header-icon-width': '30px',
  'wishlist-header-icon-height-tablet': '27px',
  'wishlist-header-icon-width-tablet': '30px',
  'wishlist-header-icon-label-font-size': null,
  'wishlist-header-icon-label-font-size-tablet': null,
  'wishlist-header-icon-margin': '0 0 0 20px',
  'wishlist-header-icon-margin-tablet': '0 0 0 10px',
  'wishlist-icon-animation-speed': 'fast',
  'wishlist-icon-hover':
    'url(/assets/wallis/images/wishlist-button-filled.svg)',
  'wishlist-icon-animation-duration': '1000ms',

  'wishlist-plp-icon': 'url(/assets/wallis/images/wishlist-icon.svg)',
  'wishlist-plp-icon-selected':
    'url(/assets/wallis/images/wishlist-icon-active.svg)',

  'wishlist-plp-icon-hover':
    'url(/assets/wallis/images/wishlist-icon-active.svg)',
}
