const colors = require('./wl-colors')

const checkoutBagSide = {
  'checkout-bag-side-border-color': colors['md-gray'],
}

const general = {
  'checkout-delivery-title-font-weight': 300,
}

module.exports = Object.assign(general, checkoutBagSide)
