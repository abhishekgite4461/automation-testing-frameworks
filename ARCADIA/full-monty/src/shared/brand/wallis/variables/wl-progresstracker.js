const colors = require('./wl-colors')

const general = {
  'progresstracker-margin-desktop': '0 0 30px',
  'progresstracker-line-color': colors['lt-md-gray'],

  'progresstracker-item-background-color': colors['lt-md-gray'],
  'progresstracker-item-active-background-color': colors['dk-gray'],
  'progresstracker-item-flex': null,

  'progresstracker-label-color': colors['lt-md-gray'],
  'progresstracker-label-active-color': colors['dk-gray'],
}

module.exports = Object.assign(general)
