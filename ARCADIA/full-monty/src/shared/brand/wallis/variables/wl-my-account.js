const colors = require('./wl-colors')

module.exports = {
  'myaccount-border-color': colors['lt-md-gray'],

  'myaccount-arrow-width': '14px',
  'myaccount-arrow-height': '22px',

  'myaccount-item-title-font-weight': null,
}
