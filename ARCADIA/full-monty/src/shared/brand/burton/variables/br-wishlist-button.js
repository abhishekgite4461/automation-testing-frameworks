const colors = require('./br-colors.js')

module.exports = {
  'wishlist-button-border-radius': '50%',
  'wishlist-button-background-color': colors['lt-md-gray'],
  'wishlist-button-padding': '12px',

  'wishlist-pdp-button-width': '50px',
  'wishlist-pdp-button-height': '50px',

  'wishlist-plp-button-padding-small': '4px 4px',
  'wishlist-plp-button-padding-medium': '5px 5px',
  'wishlist-plp-button-padding-large': '6px 6px',
  'wishlist-plp-button-padding-mega': '8px 8px',
  'wishlist-plp-button-right-small': '9px',
  'wishlist-plp-button-top-small': '14px',
  'wishlist-plp-button-right-medium': '12px',
  'wishlist-plp-button-top-medium': '17px',
  'wishlist-plp-button-right-large': '12px',
  'wishlist-plp-button-top-large': '17px',
  'wishlist-plp-button-right-mega': '14px',
  'wishlist-plp-button-top-mega': '19px',
}
