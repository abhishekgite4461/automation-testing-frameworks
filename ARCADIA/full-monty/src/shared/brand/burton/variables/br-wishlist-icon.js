module.exports = {
  'wishlist-icon': 'url(/assets/burton/images/wishlist-icon-inactive.svg)',
  'wishlist-icon-selected':
    'url(/assets/burton/images/wishlist-icon-active.svg)',
  'wishlist-icon-width': '26px',
  'wishlist-icon-height': '26px',
  'wishlist-icon-minibag-height': '16px',
  'wishlist-icon-minibag-width': '17px',
  'wishlist-minibag-button-icon-padding': '5px',

  'wishlist-header-icon': 'url(/assets/burton/images/icon-wishlist.svg)',
  'wishlist-header-icon-selected':
    'url(/assets/burton/images/icon-wishlist-active.svg)',
  'wishlist-header-icon-height': '22px',
  'wishlist-header-icon-width': '22px',
  'wishlist-header-icon-height-tablet': '22px',
  'wishlist-header-icon-width-tablet': '22px',
  'wishlist-header-icon-margin': '0 0 0 20px',
  'wishlist-header-icon-margin-tablet': '0 0 0 15px',
  'wishlist-icon-animation-speed': 'fast',
  'wishlist-icon-hover': 'url(/assets/burton/images/icon-wishlist-active.svg)',
  'wishlist-icon-animation-duration': '1000ms',

  'wishlist-plp-icon': 'url(/assets/burton/images/wishlist-icon-inactive.svg)',
  'wishlist-plp-icon-selected':
    'url(/assets/burton/images/wishlist-icon-active.svg)',

  'wishlist-plp-icon-hover':
    'url(/assets/burton/images/wishlist-icon-active.svg)',
  'wishlist-plp-icon-width-small': '10px',
  'wishlist-plp-icon-height-small': '10px',
  'wishlist-plp-icon-width-medium': '16px',
  'wishlist-plp-icon-height-medium': '16px',
  'wishlist-plp-icon-width-large': '18px',
  'wishlist-plp-icon-height-large': '18px',
  'wishlist-plp-icon-width-mega': '29px',
  'wishlist-plp-icon-height-mega': '29px',
}
