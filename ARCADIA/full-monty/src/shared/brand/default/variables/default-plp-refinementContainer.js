const plpRefinementContainer = {
  'plp-refinementContainer-width': '200px',
  'plp-refinementContainer-margin-right': '15px',
  'plp-refinementContainer-margin-left': '10px',
  'plp-refinementContainer-title-margin': '15px',
}

const plpRefinementListContainer = {
  'plp-refinementListContainer-padding-right': '20px',
  'plp-refinementListContainer-width': '200px',
}

const plpRefinementListContainerFixed = {
  'plp-refinementListContainerFixed-width': '200px',
  'plp-refinementListContainerFixed-padding-right': '15px',
}

module.exports = Object.assign(
  plpRefinementContainer,
  plpRefinementListContainer,
  plpRefinementListContainerFixed
)
