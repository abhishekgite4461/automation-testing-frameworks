const colors = require('./default-colors')

const general = {
  'checkout-elements-margin-top': '10px',
  'checkout-divider-width': '1px',
  'checkout-divider-color': colors['lt-gray'],
  'checkout-separator-height': '15px',
  'checkout-content-padding': '10px',

  'checkout-secure-payment-margin-top': '2px',
  'checkout-secure-payment-padding': '10px',
  'checkout-secure-payment-padding-min-tablet': null,
  'checkout-secure-payment-padding-min-laptop': null,

  'checkout-continue-shopping-margin-top': '8px',
  'checkout-continue-shopping-margin-right': null,
  'checkout-continue-shopping-margin-top-min-tablet': null,
  'checkout-continue-shopping-margin-top-min-laptop': null,

  'checkout-container-margin-vertical': '10px',
  'checkout-container-margin-horizontal': '20px',
  'checkout-delivery-option-icon-width': '40px',
  'checkout-delivery-title-font-weight': 'normal',
  'checkout-delivery-option-font-weight': null,
}

const delivery = {
  'checkout-delivery-padding-vertical-min-tablet': '10px',
  'checkout-delivery-margin-top': '10px',
  'checkout-delivery-icon-display': null,
  'checkout-delivery-change-font-weight': 'bold',
}

const deliveryInstructions = {
  'checkout-delivery-instructions-divider-color': colors['md-gray'],
  'checkout-delivery-instructions-margin-vertical': '10px',
  'checkout-delivery-instructions-chars-margin-top': '-15px',
  'checkout-delivery-instructions-chars-text-align': 'right',
  'checkout-delivery-instructions-chars-font-size': '0.8em',
  'checkout-delivery-instructions-chars-font-weight': '300',
}

const DeliveryType = {
  'checkout-delivery-type-description-font-size': '0.8em',
  'checkout-delivery-type-description-font-weight': null,
  'checkout-delivery-type-title-font-size': null,
  'checkout-delivery-type-title-font-weight': null,
  'checkout-delivery-type-title-line-height': null,
}

const deliveryMethod = {
  'checkout-delivery-method-description-font-size': '0.8em',
}

const cardDetails = {
  'checkout-card-margin-top': '10px',
  'checkout-card-title-font-size': '15px',
  'checkout-card-description-font-size': '13px',
}

const payments = {
  'checkout-payments-padding-vertical': '10px',
  'checkout-payments-icon-height': '20px',
  'checkout-payments-change-link-font-size': '0.8em',
  'checkout-payments-change-link-font-weight': null,

  'checkout-payments-title-margin': '10px 0 0',

  'checkout-description-padding': '10px',
}

const summary = {
  'checkout-summary-account-title-margin': '10px 0 -20px',
}

const totals = {
  'checkout-totals-padding-vertical': '10px',
  'checkout-totals-subtotal-display': 'none',
  'checkout-totals-delivery-font-weight': null,
  'checkout-totals-parenthesis-font-weight': null,
  'checkout-totals-total-font-weight': 'bold',
  'checkout-totals-total-font-size': null,
  'checkout-totals-background-color': colors['ex-lt-gray'],
}

const error = {
  'checkout-error-session-image-size': '40px',
}

const checkoutBagSide = {
  'checkout-bag-side-title-margin-horizontal': '30px',
  'checkout-bag-side-title-margin-vertical': '0',
  'checkout-bag-side-title-padding-horizontal': '0',
  'checkout-bag-side-title-padding-vertical': '15px',

  'checkout-bag-side-margin-horizontal': null,
  'checkout-bag-side-margin-vertical': null,
  'checkout-bag-side-padding-horizontal': '30px',
  'checkout-bag-side-padding-vertical': '15px',

  'checkout-bag-side-background-color': null,

  'checkout-bag-side-title-font-size': null,
  'checkout-bag-side-title-font-weight': '500',

  'checkout-bag-side-delivery-icon-width': '35px',

  'checkout-bag-side-total-font-size': null,
  'checkout-bag-side-total-font-weight': null,

  'checkout-bag-side-border-color': colors['md-gray'],

  'checkout-bag-side-simple-total-section-padding-vertical': '20px',

  'checkout-bag-side-simple-total-background-color': colors['ex-lt-gray'],
  'checkout-bag-side-simple-total-border-top': `1px solid ${colors['md-gray']}`,
  'checkout-bag-side-simple-total-border-bottom': `1px solid ${
    colors['md-gray']
  }`,

  'checkout-bag-side-simple-total-section-font-size': null,
}

module.exports = Object.assign(
  general,
  delivery,
  deliveryInstructions,
  cardDetails,
  payments,
  totals,
  summary,
  error,
  checkoutBagSide,
  DeliveryType,
  deliveryMethod
)
