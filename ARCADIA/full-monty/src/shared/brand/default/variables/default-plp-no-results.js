module.exports = {
  'plp-no-results-padding': '30px 10px 10px',
  'plp-no-results-border': null,

  'plp-no-results-clear-filters-border': null,
  'plp-no-results-clear-filters-padding': null,
  'plp-no-results-clear-filters-decoration': 'underline',
  'plp-no-results-clear-filters-text-transform': null,
  'plp-no-results-clear-filters-font-weight': null,
  'plp-no-results-clear-filters-before-content': null,
  'plp-no-results-clear-filters-before-margin-right': null,
  'plp-no-results-clear-filters-before-margin-left': null,
  'plp-no-results-clear-filters-text-align': null,
  'plp-no-results-clear-filters-font-size': null,
  'plp-no-results-clear-filters-width': '100%',

  'plp-no-results-message-text-align': 'center',
  'plp-no-results-message-font-weight': null,
  'plp-no-results-error-image-display': 'block',
  'plp-no-results-error-image-size': '40px',
  'plp-no-results-filter-type-padding-top': '10px',
  'plp-no-results-filter-type-text-align': null,
  'plp-no-results-clear-filters-letter-spacing': null,
  'plp-no-results-message-margin-bottom': null,
  'plp-no-results-message-padding': null,

  'plp-no-results-filter-type-error-image-display': null,
  'plp-no-results-filter-type-message-padding': null,
}
