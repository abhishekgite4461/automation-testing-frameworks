const colors = require('./default-colors')

const myAccountMain = {
  'myaccount-border-color': null,
  'myaccount-divider-color': colors['lt-md-gray'],
  'myaccount-arrow-width': '12px',
  'myaccount-arrow-height': '16px',

  'myaccount-item-title-font-weight': null,
  'myaccount-item-title-text-transform': 'capitalize',
  'myaccount-item-title-spacing': '0.25em',
}

const myOrderDetails = {
  'myorder-details-products-list-margin-horizontal': '10px',
  'myorder-details-products-cell-padding-desktop': '10px',
  'myorder-details-address-icon-width': '40px',
}

module.exports = Object.assign(myAccountMain, myOrderDetails)
