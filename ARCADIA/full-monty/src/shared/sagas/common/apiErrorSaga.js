import { put, call, takeEvery } from 'redux-saga/effects'
import {
  setGenericError,
  setApiError,
  setCmsError,
} from '../../actions/common/errorMessageActions'
import { sessionExpired } from '../../actions/common/sessionActions'

export function logError(payload) {
  const response = payload.response || payload
  const msg = `API ${payload.name}: ${payload.message}${
    response && response.message ? `: ${response.message}` : ''
  }`
  console.error(msg) // eslint-disable-line no-console
}

export function* showError({ payload: { statusCode, response }, meta }) {
  if (meta && meta.requestUrl && meta.requestUrl.includes('/api/cms/')) {
    yield put(setCmsError(response))
  } else if (statusCode === 500 || statusCode === 404) {
    yield put(setApiError(response))
  } else if (meta && meta.errorAlertBox === true) {
    yield put(setGenericError(response))
  }
}

export function* apiErrorSaga(action) {
  const { payload } = action
  const response = payload && payload.response ? payload.response : {}

  logError(payload)

  if (
    (response && response.headers && response.headers['session-expired']) ||
    response.message === 'Must be logged in to perform this action'
  ) {
    yield put(sessionExpired())
  } else {
    yield call(showError, action)
  }

  // TODO: add validation errors to formErrorSaga
}

export const apiErrorMatcher = ({ error, payload }) => !!(error && payload)

export default function start() {
  return takeEvery(apiErrorMatcher, apiErrorSaga)
}
