import { put, takeEvery } from 'redux-saga/effects'
import { authEventAnalytics } from '../../lib/analytics/auth'

export function* analyticSaga(action) {
  const { analytics } = action.meta

  switch (analytics.type) {
    case 'AUTH':
      yield put(authEventAnalytics(analytics.data, analytics.events))
      break
    default:
      throw new TypeError(`Unknown analytic type: ${analytics.type}`)
  }
}

export const analyticsMatcher = (action) =>
  !!(action.meta && action.meta.analytics)

export default function start() {
  return takeEvery(analyticsMatcher, analyticSaga)
}
