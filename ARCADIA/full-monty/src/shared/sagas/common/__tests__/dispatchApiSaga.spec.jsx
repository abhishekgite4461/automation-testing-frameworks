import { testSaga } from 'redux-saga-test-plan'
import { CALL_API } from 'redux-api-middleware'
import { MONTY_API } from '../../../middleware/preprocess-redux-api-middleware'
import * as sagas from '../dispatchApiSaga'
import toJson from 'enzyme-to-json'

const montyApi = {
  [MONTY_API]: {
    foo: 'bar',
  },
}

const callApi = {
  [CALL_API]: {
    foo: 'bar',
  },
}

function testRSAA(RSAA) {
  testSaga(sagas.dispatchApiSaga, {
    meta: {
      dispatchApi: RSAA,
    },
  })
    .next()
    .put(RSAA)
    .next()
    .isDone()
}

describe('Dispatch Api Saga', () => {
  it('starts', () => expect(toJson(sagas.default())).toMatchSnapshot())

  it('takes actions with meta.dispatchApi property', () => {
    const testMeta = sagas.dispatchApiMatcher
    expect(testMeta({ type: 'foo' })).toBe(false)
    expect(testMeta({ type: 'foo', meta: { dispatchApi: 'bar' } })).toBe(true)
  })

  it('Called with valid [MONTY_API] RSAA', () => {
    testRSAA(montyApi)
  })

  it('Called with valid [CALL_API] RSAA', () => {
    testRSAA(callApi)
  })

  it('Throws error when called with invalid RSAA', (done) => {
    try {
      testRSAA({ type: 'FOO' })
    } catch (e) {
      expect(e).toMatchSnapshot()
      done()
    }
  })

  it('throws error when called with non-object', () => {
    expect(() => testRSAA()).toThrow()
    expect(() => testRSAA(new Map())).toThrow()
    expect(() => testRSAA(new class Foo {}())).toThrow()
    expect(() => testRSAA(() => {})).toThrow()
  })
})
