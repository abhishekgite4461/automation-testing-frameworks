import { testSaga } from 'redux-saga-test-plan'
import { ajaxCounter } from '../../../actions/components/LoaderOverlayActions'
import * as sagas from '../ajaxCounterSaga'

const type = 'FOO'

function testCounter(overlay) {
  return testSaga(sagas.ajaxCounterSaga, type, { meta: { overlay } })
}

describe('Ajax Counter Saga', () => {
  it('starts', () => expect(sagas.default()).toMatchSnapshot())

  describe('Take actions', () => {
    it('Ending _API_REQUEST', () => {
      const takeEvery = sagas.incrementMatcher
      expect(takeEvery({ type: 'FOO' })).toBe(false)
      expect(takeEvery({ type: 'FOO_API_REQUEST' })).toBe(true)
    })

    it('Ending _API_SUCCESS and _API_FAILURE', () => {
      const takeEvery = sagas.decrementMatcher
      expect(takeEvery({ type: 'FOO' })).toBe(false)
      expect(takeEvery({ type: 'FOO_API_SUCCESS' })).toBe(true)
      expect(takeEvery({ type: 'FOO_API_FAILURE' })).toBe(true)
    })
  })

  describe('Overlay', () => {
    it('True', () => {
      testCounter(true)
        .next()
        .put(ajaxCounter(type))
        .next()
        .isDone()
    })

    it('False', () => {
      testCounter(false)
        .next()
        .isDone()
    })
  })
})
