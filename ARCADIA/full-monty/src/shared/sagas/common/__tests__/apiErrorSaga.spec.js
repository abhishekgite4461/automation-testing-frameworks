import { testSaga } from 'redux-saga-test-plan'
import * as sagas from '../apiErrorSaga'

jest.mock('../../../actions/common/sessionActions', () => ({
  sessionExpired: jest.fn(() => ({ type: 'SESSION_EXPIRED_MOCK' })),
}))
import { sessionExpired } from '../../../actions/common/sessionActions'

jest.mock('../../../actions/common/errorMessageActions', () => ({
  setGenericError: jest.fn((payload) => payload),
  setApiError: jest.fn((payload) => payload),
  setCmsError: jest.fn((payload) => payload),
}))
import {
  setGenericError,
  setApiError,
  setCmsError,
} from '../../../actions/common/errorMessageActions'

global.console = { error: jest.fn() }

function showError(action, effect) {
  testSaga(sagas.showError, action)
    .next()
    .put(effect)
    .next()
    .isDone()
}

describe('API Error Saga', () => {
  it('starts', () => expect(sagas.default()).toMatchSnapshot())

  it('takes actions ending _API_REQUEST', () => {
    const takeEvery = sagas.apiErrorMatcher
    expect(takeEvery({ type: 'FOO' })).toBe(false)
    expect(takeEvery({ payload: 'foo' })).toBe(false)
    expect(takeEvery({ error: 'bar' })).toBe(false)
    expect(takeEvery({ payload: 'foo', error: 'bar' })).toBe(true)
  })

  it('Processes standard error', () => {
    const response = 'foo'
    const action = { payload: { response }, error: 'bar' }

    testSaga(sagas.apiErrorSaga, action)
      .next()
      .call(sagas.showError, action)
      .next()
      .isDone()
  })

  it('Processes session timeout error', () => {
    const response = { headers: { 'session-expired': true } }
    const action = { payload: { response }, error: 'bar' }

    testSaga(sagas.apiErrorSaga, action)
      .next()
      .put(sessionExpired())
      .next()
      .isDone()
  })

  it('Logs error to the console', () => {
    sagas.logError({ name: 'foo', message: 'bar' })
    expect(console.error).toBeCalled() // eslint-disable-line no-console
  })

  describe('Displays Error', () => {
    it('Ignore', () => {
      testSaga(sagas.showError, { payload: 'foo' })
        .next()
        .isDone()
    })
    it('CMS', () => {
      const payload = { response: 'cms error' }
      const action = { payload, meta: { requestUrl: '/api/cms/foo' } }
      const effect = setCmsError(payload.response)
      showError(action, effect)
    })

    it('500', () => {
      const payload = { response: 'foo', statusCode: 500 }
      const action = { payload }
      const effect = setApiError(payload.response)
      showError(action, effect)
    })

    it('404', () => {
      const payload = { response: 'foo', statusCode: 404 }
      const action = { payload }
      const effect = setApiError(payload.response)
      showError(action, effect)
    })

    it('meta.errorAlertBox', () => {
      const payload = { response: 'alert box' }
      const action = { payload, meta: { errorAlertBox: true } }
      const effect = setGenericError(payload.response)
      showError(action, effect)
    })
  })
})
