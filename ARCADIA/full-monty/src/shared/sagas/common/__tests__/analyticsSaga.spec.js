import { testSaga } from 'redux-saga-test-plan'

jest.mock('../../../lib/analytics/auth', () => ({
  authEventAnalytics: jest.fn(() => ({ type: 'AUTH' })),
}))
import { authEventAnalytics } from '../../../lib/analytics/auth'
import * as sagas from '../analyticSaga'

const testActionBad = { type: 'FOO', meta: { analytics: { type: 'BAR' } } }
const testActionGood = {
  type: 'FOO',
  meta: {
    analytics: {
      type: 'AUTH',
      data: {
        username: 'username',
        email: 'email',
      },
      events: ['event109'],
    },
  },
}

describe('Analytics Saga', () => {
  it('starts', () => expect(sagas.default()).toMatchSnapshot())

  it('Takes actions with meta.analytics property', () => {
    const takeEvery = sagas.analyticsMatcher
    expect(takeEvery({ type: 'foo' })).toBe(false)
    expect(takeEvery(testActionGood)).toBe(true)
  })

  it('Called with valid Analytics Type', () => {
    testSaga(sagas.analyticSaga, testActionGood)
      .next()
      .put(authEventAnalytics({}))
      .next()
      .isDone()
  })

  it('Throws error when called with invalid Analytics Type', (done) => {
    try {
      testSaga(sagas.analyticSaga, testActionBad).next()
    } catch (e) {
      expect(e.message).toBe('Unknown analytic type: BAR')
      expect(e).toMatchSnapshot()
      done()
    }
  })
})
