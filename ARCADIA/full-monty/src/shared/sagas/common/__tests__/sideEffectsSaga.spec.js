import { testSaga } from 'redux-saga-test-plan'
import { put } from 'redux-saga/effects'
import * as sagas from '../sideEffectsSaga'

function testActionCreator() {
  return {
    type: 'FOO',
  }
}

function testDispatch(actions, response) {
  testSaga(sagas.sideEffectsSaga, {
    meta: {
      dispatch: actions,
    },
  })
    .next()
    .parallel(response)
    .next()
    .isDone()
}

describe('Side Effects Saga', () => {
  it('starts', () => expect(sagas.default()).toMatchSnapshot())

  it('Takes actions with meta.dispatch property', () => {
    const takeEvery = sagas.sideEffectMatcher
    expect(takeEvery({ type: 'foo' })).toBe(false)
    expect(takeEvery({ type: 'foo', meta: { dispatch: 'bar' } })).toBe(true)
  })

  describe('Called with valid Actions', () => {
    it('Single Action', () => {
      testDispatch(testActionCreator, [put(testActionCreator())])
    })

    it('Array of actions', () => {
      testDispatch(
        [testActionCreator, testActionCreator, testActionCreator],
        [
          put(testActionCreator()),
          put(testActionCreator()),
          put(testActionCreator()),
        ]
      )
    })

    it('Undefined Actions', () => {
      testDispatch([undefined, undefined], [])
    })
  })

  it('Throws error when pased a Thunk', (done) => {
    try {
      testDispatch(() => testActionCreator)
    } catch (e) {
      expect(e.message).toBe('Action does not return an object')
      expect(e).toMatchSnapshot()
      done()
    }
  })
})
