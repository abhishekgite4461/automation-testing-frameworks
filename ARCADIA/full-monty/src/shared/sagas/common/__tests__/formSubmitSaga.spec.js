import { testSaga } from 'redux-saga-test-plan'
import { put, call } from 'redux-saga/effects'
import {
  setFormMessage,
  setFormSuccess,
  setFormMeta,
} from '../../../actions/common/formActions'
import * as sagas from '../formSubmitSaga'

describe('Form Submit Saga', () => {
  it('starts', () => expect(sagas.default()).toMatchSnapshot())

  describe('Take actions', () => {
    const tests = {
      REQUEST: sagas.requestMatcher,
      SUCCESS: sagas.successMatcher,
      FAILURE: sagas.failureMatcher,
    }

    Object.keys(tests).forEach((type) => {
      it(`Ending _FORM_API_${type}`, () => {
        const takeEvery = tests[type]
        expect(takeEvery({ type: 'FOO' })).toBe(false)
        expect(
          takeEvery({ type: `FOO_API_${type}`, meta: { formName: 'fooForm' } })
        ).toBe(true)
      })
    })
  })

  describe('Set Message', () => {
    const formName = 'Form Name'
    const action = {
      type: 'FOO',
      meta: { formName },
    }

    it('Error', () => {
      testSaga(sagas.setMessageSaga, 'Error msg', 500, action)
        .next()
        .put(
          setFormMessage(formName, {
            type: 'error',
            message: 'Error msg',
          })
        )
        .next()
        .isDone()
    })

    it('Confirm', () => {
      testSaga(sagas.setMessageSaga, 'Confirm msg', sagas.HTTP_OK, action)
        .next()
        .put(
          setFormMessage(formName, {
            type: 'confirm',
            message: 'Confirm msg',
          })
        )
        .next()
        .isDone()
    })
  })

  describe('Form Response Message', () => {
    let testAction

    beforeEach(() => {
      testAction = {
        payload: { statusCode: 418 }, // I'm a teapot (RFC 2324)
        meta: {},
      }
    })

    function test(msgIn, msgOut, action) {
      testSaga(sagas.formResponseMessageSaga, msgIn, action)
        .next()
        .call(sagas.setMessageSaga, msgOut, action.payload.statusCode, action)
        .next()
        .isDone()
    }

    it('Response', () => {
      testAction.payload.message = 'Response'
      test('Default', 'Response', testAction)
    })

    it('MessageKey', () => {
      testAction.meta.messageKey = 'MessageKey'
      test('Default', 'MessageKey', testAction)
    })

    it('Response & MessageKey', () => {
      testAction.payload.message = 'Response'
      testAction.meta.messageKey = 'MessageKey'
      test('Default', 'Response', testAction)
    })

    it('Default', () => {
      test('Default', 'Default', testAction)
    })
  })

  it('Sets formMeta', () => {
    const formName = 'formName'
    const formMeta = ['foo', 'bar']
    const effect = put(setFormMeta(formName, ...formMeta))
    expect(sagas.formMeta({ formName, formMeta })).toEqual(effect)
  })

  describe('Form Success', () => {
    it('With formMeta', () => {
      const testAction = {
        meta: {
          formName: 'Form Name',
          formMeta: 'Form Meta',
        },
      }

      testSaga(sagas.formSuccessSaga, testAction)
        .next()
        .parallel([
          call(sagas.formResponseMessageSaga, '', testAction),
          put(setFormSuccess(testAction.meta.formName, true)),
          put(
            setFormMeta(testAction.meta.formName, ...testAction.meta.formMeta)
          ),
        ])
        .next()
        .isDone()
    })

    it('Without formMeta', () => {
      const testAction = {
        meta: {
          formName: 'Form Name',
        },
      }

      testSaga(sagas.formSuccessSaga, testAction)
        .next()
        .parallel([
          call(sagas.formResponseMessageSaga, '', testAction),
          put(setFormSuccess(testAction.meta.formName, true)),
        ])
        .next()
        .isDone()
    })
  })
})
