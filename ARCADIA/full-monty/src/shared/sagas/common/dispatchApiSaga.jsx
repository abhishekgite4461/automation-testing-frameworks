import { takeEvery } from 'redux-saga'
import { put } from 'redux-saga/effects'
import { CALL_API } from 'redux-api-middleware'
import { MONTY_API } from '../../middleware/preprocess-redux-api-middleware'

export function isRSAA(action) {
  return (
    Object.prototype.toString.call(action) === '[object Object]' &&
    (Object.prototype.hasOwnProperty.call(action, MONTY_API) ||
      Object.prototype.hasOwnProperty.call(action, CALL_API))
  )
}

export function* dispatchApiSaga({ meta: { dispatchApi } }) {
  if (isRSAA(dispatchApi)) {
    yield put(dispatchApi)
  } else {
    throw new ReferenceError('Expected RSAA action ([MONTY_API] or [CALL_API])')
  }
}

export const dispatchApiMatcher = (action) =>
  !!(action.meta && action.meta.dispatchApi)

export default function start() {
  return takeEvery(dispatchApiMatcher, dispatchApiSaga)
}
