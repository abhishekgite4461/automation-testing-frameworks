import { put, takeEvery } from 'redux-saga/effects'

export function* sideEffectsSaga(action) {
  let { dispatch } = action.meta

  if (!Array.isArray(dispatch)) dispatch = [dispatch]

  yield dispatch.filter((a) => !!a).map((sideAction) => {
    const effect = sideAction(action.payload || action)
    if (typeof effect === 'function')
      throw new TypeError('Action does not return an object')
    return put(effect)
  })
}

export const sideEffectMatcher = (action) =>
  !!(action.meta && action.meta.dispatch)

export default function start() {
  return takeEvery(sideEffectMatcher, sideEffectsSaga)
}
