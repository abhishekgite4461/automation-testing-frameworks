import { put, takeEvery } from 'redux-saga/effects'
import { ajaxCounter } from '../../actions/components/LoaderOverlayActions'

export function* ajaxCounterSaga(type, action) {
  if (action.meta && action.meta.overlay) yield put(ajaxCounter(type))
}

export const incrementMatcher = (action) => /\w+_API_REQUEST/.test(action.type)
export const decrementMatcher = (action) =>
  /\w+_API_(SUCCESS|FAILURE)/.test(action.type)

export default function start() {
  return [
    takeEvery(incrementMatcher, ajaxCounterSaga, 'increment'),
    takeEvery(decrementMatcher, ajaxCounterSaga, 'decrement'),
  ]
}
