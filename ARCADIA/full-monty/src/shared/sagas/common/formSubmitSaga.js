import { call, put, takeEvery } from 'redux-saga/effects'
import {
  setFormMessage,
  setFormSuccess,
  setFormMeta,
} from '../../actions/common/formActions'

export const getConfig = (state) => state.config
export const HTTP_OK = 200

export function* setMessageSaga(message, statusCode, { meta: { formName } }) {
  yield put(
    setFormMessage(formName, {
      type: statusCode && statusCode !== HTTP_OK ? 'error' : 'confirm',
      message,
    })
  )
}

export function* formResponseMessageSaga(defaultMessage, action) {
  const response = action.payload.response || action.payload
  const { message, statusCode } = response
  const messageKey = action.meta && action.meta.messageKey
  let msg

  if (response.originalMessage === 'Validation error') {
    msg = response.validationErrors[0].message
  } else {
    msg = message || messageKey || defaultMessage
  }

  yield call(setMessageSaga, msg, statusCode, action)
}

export function formMeta(meta) {
  const { formName, formMeta } = meta
  return put(setFormMeta(formName, ...formMeta))
}

export function* formSuccessSaga(action) {
  const { meta } = action
  const nextActions = [
    call(formResponseMessageSaga, '', action),
    put(setFormSuccess(meta.formName, true)),
  ]

  if (Object.prototype.hasOwnProperty.call(meta, 'formMeta'))
    nextActions.push(formMeta(meta))

  yield nextActions
}

function matcher(patten, { type, meta }) {
  return patten.test(type) && meta && !!meta.formName
}

export const requestMatcher = matcher.bind(null, /\w+_API_REQUEST/)
export const successMatcher = matcher.bind(null, /\w+_API_SUCCESS/)
export const failureMatcher = matcher.bind(null, /\w+_API_FAILURE/)

export default function start() {
  return [
    takeEvery(requestMatcher, setMessageSaga, '', HTTP_OK),
    takeEvery(successMatcher, formSuccessSaga),
    takeEvery(failureMatcher, formResponseMessageSaga, 'Unknown server error'),
  ]
}
