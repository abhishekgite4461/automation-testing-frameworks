import { testSaga } from 'redux-saga-test-plan'
import * as sagas from '../forgetPwdRequestSaga'
import { setPostResetUrl } from '../../../actions/components/ChangePasswordActions'
import { setFormMessage } from '../../../actions/common/formActions'

function testForgetPwdRequestSaga(pathname) {
  testSaga(sagas.forgetPwdRequestSaga, {})
    .next()
    .select(sagas.getPathname)
    .next(pathname)
    .put(setPostResetUrl(pathname))
    .next()
    .take(sagas.chkAccordionAction)
    .next()
    .put(setFormMessage('forgetPassword', {}))
    .next()
    .isDone()
}

describe('Forget Password Request Saga', () => {
  it('starts', () => expect(sagas.default()).toMatchSnapshot())

  describe('Updates page', () => {
    it('Checkout', () => {
      testForgetPwdRequestSaga('/checkout')
    })

    it('My Account', () => {
      testForgetPwdRequestSaga('/my-account')
    })
  })

  it('Take TOGGLE_ACCORDION', () => {
    expect(
      sagas.chkAccordionAction({
        type: 'TOGGLE_ACCORDION',
        key: 'forgetPassword',
      })
    ).toBe(true)
  })
})
