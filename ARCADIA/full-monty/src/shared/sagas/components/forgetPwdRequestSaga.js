import { select, put, take, takeEvery } from 'redux-saga/effects'
import { setPostResetUrl } from '../../actions/components/ChangePasswordActions'
import { setFormMessage } from '../../actions/common/formActions'

export const getPathname = (state) => state.routing.location.pathname
export const chkAccordionAction = (action) =>
  action.type === 'TOGGLE_ACCORDION' && action.key === 'forgetPassword'

export function* forgetPwdRequestSaga() {
  const pathname = yield select(getPathname)
  yield put(
    setPostResetUrl(
      pathname.startsWith('/checkout') ? '/checkout' : '/my-account'
    )
  )

  yield take(chkAccordionAction)
  yield put(setFormMessage('forgetPassword', {}))
}

export default function start() {
  return takeEvery('FORGOT_PASSWORD_FORM_API_SUCCESS', forgetPwdRequestSaga)
}
