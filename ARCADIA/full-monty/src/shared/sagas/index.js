import { flatten } from 'ramda'

const commonSagas = [
  require('./common/ajaxCounterSaga'),
  require('./common/analyticSaga'),
  require('./common/formSubmitSaga'),
  require('./common/dispatchApiSaga'),
  require('./common/apiErrorSaga'),
]

const componentSagas = [require('./components/forgetPwdRequestSaga')]

function start(sagas) {
  return flatten(sagas.map((saga) => saga.default()))
}

export default function* rootSaga() {
  yield [...start(commonSagas), ...start(componentSagas)]
}
