import rootSaga from '../index'
import toJson from 'enzyme-to-json'

describe('RootSaga', () => {
  it('Starts Sagas', () => {
    expect(toJson(rootSaga().next())).toMatchSnapshot()
  })
})
