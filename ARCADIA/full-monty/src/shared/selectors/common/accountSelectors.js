// TODO: Move this file to ../ (main selectors folder) https://arcadiagroup.atlassian.net/browse/PTM-575

import {
  all,
  defaultTo,
  either,
  find,
  isEmpty,
  isNil,
  map,
  merge,
  mergeAll,
  not,
  omit,
  path,
  pathOr,
  pick,
  pipe,
  pluck,
  propEq,
  values,
} from 'ramda'

// constants
import { formNames } from '../../constants/forms/myCheckoutDetailsConstants'
// selectors
import { getCountryCodeFromQAS } from './configSelectors'
import { createSelector } from 'reselect'

export const getUser = (state) => {
  return pathOr({}, ['account', 'user'], state)
}

export const getUserDetails = (state, type) => {
  return pathOr({}, ['account', 'user', type, 'nameAndPhone'], state)
}

export const getUserAddress = (state, type) => {
  return pathOr({}, ['account', 'user', type, 'address'], state)
}

export const getUserCountry = (state, type) => {
  return pathOr(
    'United Kingdom',
    ['account', 'user', type, 'address', 'country'],
    state
  )
}

export const selectStoredPaymentDetails = (state) =>
  pathOr({}, ['account', 'user', 'creditCard'], state)

export const checkPaymentMethodExistsOrDefault = (state, type) => {
  const defaultPaymentMethod = 'CARD'
  if (!('paymentMethods' in state)) return defaultPaymentMethod
  if (Array.isArray(state.paymentMethods)) {
    const contains = state.paymentMethods.some(
      (payment) => payment.value === type
    )
    return contains ? type : defaultPaymentMethod
  }
  return defaultPaymentMethod
}

export const getMyCheckoutDetailsEditingEnabled = (state) => {
  return pathOr(
    false,
    ['account', 'myCheckoutDetails', 'editingEnabled'],
    state
  )
}

export const getMyCheckoutDetailForm = (state) => {
  return pathOr(
    null,
    ['forms', 'account', 'myCheckoutDetails', 'myCheckoutDetailsForm'],
    state
  )
}

export const getFormNames = (addressType) => formNames[addressType]

export const getMCDAddressForm = (addressType, formName, state) => {
  return pathOr({}, ['forms', 'account', 'myCheckoutDetails', formName], state)
}

export const getMCDPaymentMethodForm = (state) => {
  /* @todo get the right form names when v2 checkout component more generic
    return pathOr({}, ['forms', 'account', 'myCheckoutDetails', formNames.payment.paymentCardDetails], state) */
  const formName = getFormNames('payment').paymentCardDetails
  return pathOr({}, ['forms', 'checkout', formName], state)
}

export const getSelectedPaymentMethodValue = (state) => {
  const cardForm = getMCDPaymentMethodForm(state)
  return pathOr(undefined, ['fields', 'paymentType', 'value'], cardForm)
}

export const getUserSelectedPaymentOptionType = (state) => {
  const value =
    getSelectedPaymentMethodValue(state) ||
    selectStoredPaymentDetails(state).type
  return value === 'VISA'
    ? 'CARD'
    : checkPaymentMethodExistsOrDefault(state, value)
}

export const getCountryFor = (addressType, formName, state) => {
  const addressForm = getMCDAddressForm(addressType, formName, state)
  const formFieldContry = path(['fields', 'country', 'value'], addressForm)
  const accountCountry =
    addressType === 'delivery'
      ? path(
          ['account', 'user', 'deliveryDetails', 'address', 'country'],
          state
        )
      : addressType === 'billing'
        ? path(
            ['account', 'user', 'billingDetails', 'address', 'country'],
            state
          )
        : 'United Kingdom'
  return formFieldContry || accountCountry
}

export const getFindAddressIsVisible = (
  addressType,
  formName,
  country,
  state
) => {
  const addressForm = getMCDAddressForm(addressType, formName, state)
  const isManualAddress = !!(
    path(['fields', 'isManual', 'value'], addressForm) ||
    path(['fields', 'address1', 'value'], addressForm)
  )
  const QASCountry = getCountryCodeFromQAS(state, country)
  return !isManualAddress && QASCountry !== ''
}

export const isSaveMyCheckoutDetailsDisabled = (state) => {
  const forms = {
    billingAddressForm: getMCDAddressForm(
      'billing',
      getFormNames('billing').address,
      state
    ),
    deliveryAddressForm: getMCDAddressForm(
      'delivery',
      getFormNames('delivery').address,
      state
    ),
    billingDetailsForm: getMCDAddressForm(
      'billing',
      getFormNames('billing').details,
      state
    ),
    deliveryDetailsForm: getMCDAddressForm(
      'delivery',
      getFormNames('delivery').details,
      state
    ),
    billingFindAddressForm: getMCDAddressForm(
      'billing',
      getFormNames('billing').findAddress,
      state
    ),
    deliveryFindAddressForm: getMCDAddressForm(
      'delivery',
      getFormNames('delivery').findAddress,
      state
    ),
    paymentMethodForm: getMCDPaymentMethodForm(state),
  }
  const errors = mergeAll(values(pluck('errors', forms)))
  const errorMessages = values(omit(['cvv'], errors))
  return not(all(either(isNil, isEmpty), errorMessages))
}

const orderDetailsSelector = (state) =>
  pathOr({}, ['orderHistory', 'orderDetails'], state)
const returnDetailsSelector = (state) =>
  pathOr({}, ['returnHistory', 'returnDetails'], state)
const orderPaymentDetailsSelector = (state) =>
  pathOr({}, ['orderHistory', 'orderDetails', 'paymentDetails'], state)
const returnPaymentDetailsSelector = (state) =>
  pathOr({}, ['returnHistory', 'returnDetails', 'paymentDetails'], state)
const paymentMethodsSelector = (state) => state.paymentMethods

const decoratePaymentDetailsWithPaymentMethodData = (
  paymentDetails,
  paymentMethods
) => {
  const decoratePaymentDetail = (paymentDetail) =>
    pipe(
      find(propEq('label', path(['paymentMethod'], paymentDetail))),
      defaultTo({}),
      pick(['icon', 'type', 'value']),
      merge(paymentDetail)
    )(paymentMethods)

  return map(decoratePaymentDetail, paymentDetails)
}

const decorateOrderPaymentDetails = createSelector(
  orderPaymentDetailsSelector,
  paymentMethodsSelector,
  decoratePaymentDetailsWithPaymentMethodData
)

const decorateReturnPaymentDetails = createSelector(
  returnPaymentDetailsSelector,
  paymentMethodsSelector,
  decoratePaymentDetailsWithPaymentMethodData
)

export const getDecoratedOrderDetails = createSelector(
  orderDetailsSelector,
  decorateOrderPaymentDetails,
  (orderDetails, paymentDetails) => ({
    ...orderDetails,
    paymentDetails,
  })
)

export const getDecoratedReturnDetails = createSelector(
  returnDetailsSelector,
  decorateReturnPaymentDetails,
  (orderDetails, paymentDetails) => ({
    ...orderDetails,
    paymentDetails,
  })
)

export const getPaymentCardDetailsMCD = path([
  'forms',
  'account',
  'myCheckoutDetails',
  'paymentCardDetailsMCD',
])

export const selectDeliveryCountry = (state) => {
  return (
    path(
      ['forms', 'checkout', 'yourAddress', 'fields', 'country', 'value'],
      state
    ) ||
    path(['account', 'user', 'deliveryDetails', 'address', 'country'], state) ||
    path(['config', 'country'], state)
  )
}

export const selectBillingCountry = (state) => {
  return (
    path(
      ['forms', 'checkout', 'billingAddress', 'fields', 'country', 'value'],
      state
    ) ||
    path(['account', 'user', 'billingDetails', 'address', 'country'], state) ||
    path(['config', 'country'], state)
  )
}
