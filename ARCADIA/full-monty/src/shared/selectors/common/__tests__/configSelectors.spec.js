import {
  getBrandName,
  getPostCodeRules,
  getCountryCodeFromQAS,
  getCountriesByAddressType,
  getRegion,
} from '../configSelectors'

import configMock from '../../../../../test/mocks/config'
import siteOptionsMock from '../../../../../test/mocks/siteOptions'

const snapshot = (action) => expect(action).toMatchSnapshot()

describe('@Config Selectors', () => {
  const state = {
    config: configMock,
    siteOptions: siteOptionsMock,
  }

  describe('[getBrandName selector]', () => {
    it('extract brandName from config', () => {
      snapshot(getBrandName(state))
    })
  })

  describe('[getPostCodeRules selector]', () => {
    it('getRegion should return region', () => {
      expect(getRegion(state)).toBe(state.config.region)
    })
  })

  describe('[getPostCodeRules selector]', () => {
    it('extract post code rules from config', () => {
      snapshot(getPostCodeRules(state, 'United Kingdom'))
    })

    it('return empty when state is empty', () => {
      expect(getPostCodeRules({})).toEqual({})
      expect(getPostCodeRules(null)).toEqual({})
      expect(getPostCodeRules(undefined)).toEqual({})
    })
  })

  describe('[getCountryCodeFromQAS selector]', () => {
    it('extract post country code from config', () => {
      expect(getCountryCodeFromQAS(state, 'United Kingdom')).toBe('GBR')
    })

    it('return empty when state is empty', () => {
      expect(getCountryCodeFromQAS({}, null)).toEqual('')
      expect(getCountryCodeFromQAS(null)).toEqual('')
      expect(getCountryCodeFromQAS(undefined)).toEqual('')
    })
  })

  describe('[getCountriesByAddressType selector]', () => {
    it('extract delivery countries from siteOptions', () => {
      snapshot(getCountriesByAddressType(state, 'delivery'))
    })

    it('extract billing countries from siteOptions', () => {
      snapshot(getCountriesByAddressType(state, 'billing'))
    })

    it('return empty when state is empty', () => {
      expect(getCountriesByAddressType({}, null)).toEqual([])
      expect(getCountriesByAddressType(null)).toEqual([])
      expect(getCountriesByAddressType(undefined)).toEqual([])
    })
  })
})
