import {
  getUser,
  getUserDetails,
  getUserAddress,
  getUserCountry,
  selectStoredPaymentDetails,
  getMyCheckoutDetailsEditingEnabled,
  getMyCheckoutDetailForm,
  getFormNames,
  getMCDAddressForm,
  getMCDPaymentMethodForm,
  getCountryFor,
  getFindAddressIsVisible,
  isSaveMyCheckoutDetailsDisabled,
  selectDeliveryCountry,
  selectBillingCountry,
  getPaymentCardDetailsMCD,
} from '../accountSelectors'
import myAccountMock from '../../../../../test/mocks/myAccount-response.json'
import { paymentMethodsList } from '../../../../../test/mocks/paymentMethodsMocks'
import myCheckoutDetailsMocks, {
  myCheckoutDetailsErrors,
  myCheckoutDetailsNoErrors,
  myCheckoutDetailsCVVError,
} from '../../../../../test/mocks/forms/myCheckoutDetailsFormsMocks'
import configMock from '../../../../../test/mocks/config'

const snapshot = (action) => expect(action).toMatchSnapshot()

describe('@Account Selectors', () => {
  const state = {
    config: configMock,
    account: {
      user: myAccountMock,
      myCheckoutDetails: {
        editingEnabled: false,
      },
    },
    paymentMethods: paymentMethodsList,
    forms: {
      account: {
        myCheckoutDetails: myCheckoutDetailsMocks,
      },
      // @todo: move that behind forms.acount.myCheckoutDetails
      checkout: {
        billingCardDetails: {
          fields: {
            paymentType: {
              value: 'VISA',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
          },
        },
      },
    },
    findAddress: {
      isManual: false,
    },
  }

  describe('[getUser selector', () => {
    it('return empty when state is empty', () => {
      expect(getUser({})).toEqual({})
    })
    it('return user from account', () => {
      expect(getUser(state)).toEqual(myAccountMock)
    })
  })

  describe('[getUserDetails selector]', () => {
    it('return empty when state is empty', () => {
      expect(getUserDetails({}, 'deliveryDetails')).toEqual({})
    })

    it('return empty when type is empty', () => {
      expect(getUserDetails({}, '')).toEqual({})
      expect(getUserDetails({}, undefined)).toEqual({})
      expect(getUserDetails({}, null)).toEqual({})
      expect(getUserDetails(state, '')).toEqual({})
      expect(getUserDetails(state, undefined)).toEqual({})
      expect(getUserDetails(state, null)).toEqual({})
    })

    it('extract userDetails from user deliveryDetails', () => {
      snapshot(getUserDetails(state, 'deliveryDetails'))
    })

    it('extract userDetails from user billingDetails', () => {
      snapshot(getUserDetails(state, 'billingDetails'))
    })
  })

  it('[getUserAddress selector] return empty when state is empty', () => {
    expect(getUserAddress({}, 'deliveryDetails')).toEqual({})
  })

  it('[getUserAddress selector] return empty when type is empty', () => {
    expect(getUserAddress({}, '')).toEqual({})
    expect(getUserAddress({}, undefined)).toEqual({})
    expect(getUserAddress({}, null)).toEqual({})
    expect(getUserAddress(state, '')).toEqual({})
    expect(getUserAddress(state, undefined)).toEqual({})
    expect(getUserAddress(state, null)).toEqual({})
  })

  it('[getUserAddress selector] extract user Address from deliveryDetails', () => {
    snapshot(getUserAddress(state, 'deliveryDetails'))
  })

  it('[getUserAddress selector] extract user Address from billingDetails', () => {
    snapshot(getUserAddress(state, 'billingDetails'))
  })

  it('[get User Countries], extract delivery country', () => {
    snapshot(getUserCountry(state, 'deliveryDetails'))
  })

  it('[get User Countries], extract billing country', () => {
    snapshot(getUserCountry(state, 'billingDetails'))
  })

  it('[get User Countries selector] return empty when state is empty', () => {
    expect(getUserCountry({}, 'deliveryDetails')).toEqual('United Kingdom')
  })

  it('[get User Countries], return empty when type is empty', () => {
    expect(getUserCountry({}, '')).toEqual('United Kingdom')
    expect(getUserCountry({}, undefined)).toEqual('United Kingdom')
    expect(getUserCountry({}, null)).toEqual('United Kingdom')
    expect(getUserCountry(state, '')).toEqual('United Kingdom')
    expect(getUserCountry(state, undefined)).toEqual('United Kingdom')
    expect(getUserCountry(state, null)).toEqual('United Kingdom')
  })

  describe('[selectStoredPaymentDetails selector]', () => {
    it('should extract creditCard from user', () => {
      snapshot(selectStoredPaymentDetails(state))
    })

    it('should return empty when state is empty', () => {
      expect(selectStoredPaymentDetails({})).toEqual({})
      expect(selectStoredPaymentDetails(null)).toEqual({})
      expect(selectStoredPaymentDetails(undefined)).toEqual({})
    })
  })

  describe('[getMyCheckoutDetailsEditingEnabled selector]', () => {
    it('false by default', () => {
      expect(getMyCheckoutDetailsEditingEnabled(state)).toBe(false)
    })
    it('true when changed', () => {
      const state = {
        account: {
          myCheckoutDetails: {
            editingEnabled: true,
          },
        },
      }
      expect(getMyCheckoutDetailsEditingEnabled(state)).toBe(true)
    })
    it('return false when state is empty', () => {
      expect(getMyCheckoutDetailsEditingEnabled({})).toEqual(false)
      expect(getMyCheckoutDetailsEditingEnabled(null)).toEqual(false)
      expect(getMyCheckoutDetailsEditingEnabled(undefined)).toEqual(false)
    })
  })

  describe('[getMyCheckoutDetailForm selector]', () => {
    const form = getMyCheckoutDetailForm(state)
    it('emtpy by default', () => {
      expect(form.fields.isDeliveryAndBillingAddressEqual).toEqual({
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      })
    })
    it('return null when state is empty', () => {
      expect(getMyCheckoutDetailForm({})).toEqual(null)
      expect(getMyCheckoutDetailForm(null)).toEqual(null)
      expect(getMyCheckoutDetailForm(undefined)).toEqual(null)
    })
  })

  describe('[getFormNames selector]', () => {
    it('get MyCheckoutDetails delivery form names', () => {
      expect(getFormNames('delivery')).toEqual({
        address: 'deliveryAddressMCD',
        details: 'deliveryDetailsAddressMCD',
        findAddress: 'deliveryFindAddressMCD',
      })
    })
    it('get MyCheckoutDetails billing form names', () => {
      expect(getFormNames('billing')).toEqual({
        address: 'billingAddressMCD',
        details: 'billingDetailsAddressMCD',
        findAddress: 'billingFindAddressMCD',
      })
    })
    it('get MyCheckoutDetails payment form names', () => {
      expect(getFormNames('payment')).toEqual({
        paymentCardDetails: 'billingCardDetails',
        paymentCardDetailsMCD: 'paymentCardDetailsMCD',
      })
    })
  })

  describe('[getMCDAddressForm selector]', () => {
    it('return null when state is empty', () => {
      expect(getMCDAddressForm(null, null, null)).toEqual({})
    })

    describe('get Delivery Forms', () => {
      const addressType = 'delivery'
      const formNames = getFormNames(addressType)
      it('get Address form', () => {
        const form = getMCDAddressForm(addressType, formNames.address, state)
        expect(form.fields).toBeDefined()
        expect(form.fields).toEqual(
          expect.objectContaining({
            address1: {
              value: '2 Britten Close',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
          })
        )
      })
      it('get Details form', () => {
        const form = getMCDAddressForm(addressType, formNames.details, state)
        expect(form.fields).toBeDefined()
        expect(form.fields).toEqual(
          expect.objectContaining({
            title: {
              value: 'Mr',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
          })
        )
      })
      it('get FindAddress form', () => {
        const form = getMCDAddressForm(
          addressType,
          formNames.findAddress,
          state
        )
        expect(form.fields).toBeDefined()
        expect(form.fields).toEqual(
          expect.objectContaining({
            findAddress: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
          })
        )
      })
    })

    describe('get Billing Forms', () => {
      const addressType = 'billing'
      const formNames = getFormNames(addressType)
      it('get Address form', () => {
        const form = getMCDAddressForm(addressType, formNames.address, state)
        expect(form.fields).toBeDefined()
        expect(form.fields).toEqual(
          expect.objectContaining({
            address1: {
              value: '35 Britten Close',
              isDirty: true,
              isTouched: false,
              isFocused: false,
            },
          })
        )
      })
      it('get Details form', () => {
        const form = getMCDAddressForm(addressType, formNames.details, state)
        expect(form.fields).toBeDefined()
        expect(form.fields).toEqual(
          expect.objectContaining({
            firstName: {
              value: 'Jose Billing',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
          })
        )
      })
      it('get FindAddress form', () => {
        const form = getMCDAddressForm(
          addressType,
          formNames.findAddress,
          state
        )
        expect(form.fields).toBeDefined()
        expect(form.fields).toEqual(
          expect.objectContaining({
            findAddress: {
              value: '',
              isDirty: false,
              isTouched: true,
              isFocused: false,
            },
          })
        )
      })
    })
  })

  describe('[getMCDPaymentMethodForm selector]', () => {
    it('get Card Details form', () => {
      const form = getMCDPaymentMethodForm(state)
      expect(form.fields).toBeDefined()
      expect(form.fields).toEqual(
        expect.objectContaining({
          paymentType: {
            value: 'VISA',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
        })
      )
    })
  })

  describe('[getCountryFor selector]', () => {
    it('return United Kingdom when state is empty', () => {
      expect(getCountryFor()).toEqual('United Kingdom')
      expect(getCountryFor({})).toEqual('United Kingdom')
      expect(getCountryFor(null)).toEqual('United Kingdom')
      expect(getCountryFor(undefined)).toEqual('United Kingdom')
    })

    describe('Delivery', () => {
      const addressType = 'delivery'
      const formNames = getFormNames(addressType)

      it('get delivery Country from address form', () => {
        const country = getCountryFor(addressType, formNames.address, state)
        expect(country).toEqual('United States')
      })
      it('should not get delivery Country from details form (return default)', () => {
        const country = getCountryFor(addressType, formNames.details, state)
        expect(country).toEqual('United Kingdom')
      })
      it('should not get delivery Country from findAddress form (return default)', () => {
        const country = getCountryFor(addressType, formNames.findAddress, state)
        expect(country).toEqual('United Kingdom')
      })
    })

    describe('Billing', () => {
      const addressType = 'billing'
      const formNames = getFormNames(addressType)

      it('get delivery Country from address form', () => {
        const country = getCountryFor(addressType, formNames.address, state)
        expect(country).toEqual('Samoa')
      })
      it('should not get delivery Country from details form (return default)', () => {
        const country = getCountryFor(addressType, formNames.details, state)
        expect(country).toEqual('United Kingdom')
      })
      it('should not get delivery Country from findAddress form (return default)', () => {
        const country = getCountryFor(addressType, formNames.findAddress, state)
        expect(country).toEqual('United Kingdom')
      })
    })
  })

  describe('[getFindAddressIsVisible selector]', () => {
    it('return false when state is empty', () => {
      expect(getFindAddressIsVisible()).toEqual(false)
      expect(getFindAddressIsVisible({})).toEqual(false)
      expect(getFindAddressIsVisible(null)).toEqual(false)
      expect(getFindAddressIsVisible(undefined)).toEqual(false)
    })

    describe('Delivery', () => {
      const addressType = 'delivery'
      const formNames = getFormNames(addressType)
      const country = 'United Kingdom'
      it('get findAddressIsVisible === false when there is address configured', () => {
        const findAddressIsVisible = getFindAddressIsVisible(
          addressType,
          formNames.address,
          country,
          state
        )
        expect(findAddressIsVisible).toEqual(false)
      })
      it('get findAddressIsVisible === true when there is no address configured and we have a valid country code', () => {
        const state = {
          config: configMock,
          forms: {
            account: {
              myCheckoutDetails: {
                deliveryAddressMCD: {
                  fields: {
                    address1: {
                      value: null,
                    },
                  },
                },
              },
            },
          },
          findAddress: {
            isManual: false,
          },
        }
        const findAddressIsVisible = getFindAddressIsVisible(
          addressType,
          formNames.address,
          country,
          state
        )
        expect(findAddressIsVisible).toEqual(true)
      })
      it('get findAddressIsVisible === false when even if we have no address configure we still do not have valid country code', () => {
        const country = 'Samoa'
        const state = {
          config: configMock,
          forms: {
            account: {
              myCheckoutDetails: {
                deliveryAddressMCD: {
                  fields: {
                    address1: {
                      value: null,
                    },
                  },
                },
              },
            },
          },
          findAddress: {
            isManual: false,
          },
        }
        const findAddressIsVisible = getFindAddressIsVisible(
          addressType,
          formNames.address,
          country,
          state
        )
        expect(findAddressIsVisible).toEqual(false)
      })
      it('get findAddressIsVisible === false when findAddress:isManual state is true', () => {
        const country = 'Samoa'
        const state = {
          config: configMock,
          forms: {
            account: {
              myCheckoutDetails: {
                deliveryAddressMCD: {
                  fields: {
                    address1: {
                      value: null,
                    },
                  },
                },
              },
            },
          },
          findAddress: {
            isManual: true,
          },
        }
        const findAddressIsVisible = getFindAddressIsVisible(
          addressType,
          formNames.address,
          country,
          state
        )
        expect(findAddressIsVisible).toEqual(false)
      })
    })
  })
  describe(isSaveMyCheckoutDetailsDisabled.name, () => {
    it('return true when there are validation errors', () => {
      expect(isSaveMyCheckoutDetailsDisabled(myCheckoutDetailsErrors)).toEqual(
        true
      )
    })
    it('return false when there are no validation errors', () => {
      expect(
        isSaveMyCheckoutDetailsDisabled(myCheckoutDetailsNoErrors)
      ).toEqual(false)
    })
    it('return false when the only validation error is the CVV field', () => {
      expect(
        isSaveMyCheckoutDetailsDisabled(myCheckoutDetailsCVVError)
      ).toEqual(false)
    })
  })
  describe('selectDeliveryCountry selector', () => {
    it('should select the delivery country from the checkout form if present', () => {
      const state = {
        forms: {
          checkout: {
            yourAddress: {
              fields: {
                country: {
                  value: 'United Kingdom',
                },
              },
            },
          },
        },
      }

      expect(selectDeliveryCountry(state)).toEqual('United Kingdom')
    })

    it('should select the delivery country from the user account if present and a checkout form is missing', () => {
      const state = {
        account: {
          user: {
            deliveryDetails: {
              address: {
                country: 'United Kingdom',
              },
            },
          },
        },
      }

      expect(selectDeliveryCountry(state)).toEqual('United Kingdom')
    })

    it('should select the delivery country from the config if all else fails', () => {
      const state = {
        config: {
          country: 'United Kingdom',
        },
      }

      expect(selectDeliveryCountry(state)).toEqual('United Kingdom')
    })
  })

  describe('selectBillingCountry selector', () => {
    it('should select the billing country from the checkout form if present', () => {
      const state = {
        forms: {
          checkout: {
            billingAddress: {
              fields: {
                country: {
                  value: 'United Kingdom',
                },
              },
            },
          },
        },
      }

      expect(selectBillingCountry(state)).toEqual('United Kingdom')
    })

    it('should select the billing country from the user account if present and a checkout form is missing', () => {
      const state = {
        account: {
          user: {
            billingDetails: {
              address: {
                country: 'United Kingdom',
              },
            },
          },
        },
      }

      expect(selectBillingCountry(state)).toEqual('United Kingdom')
    })

    it('should select the billing country from the config if all else fails', () => {
      const state = {
        config: {
          country: 'United Kingdom',
        },
      }

      expect(selectBillingCountry(state)).toEqual('United Kingdom')
    })
  })

  describe('getPaymentCardDetailsMCD', () => {
    it('should return payment card details for my checkout detail', () => {
      const paymentCardDetailsMCD = 'paymentCardDetailsMCD'
      const state = {
        forms: {
          account: {
            myCheckoutDetails: {
              paymentCardDetailsMCD,
            },
          },
        },
      }
      expect(getPaymentCardDetailsMCD(state)).toBe(paymentCardDetailsMCD)
    })

    it('should safely return undefined if state does not have forms', () => {
      expect(getPaymentCardDetailsMCD({})).toBeUndefined()
    })
  })
})
