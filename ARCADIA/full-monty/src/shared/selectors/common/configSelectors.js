// TODO: Move this file to ../ (main selectors folder) https://arcadiagroup.atlassian.net/browse/PTM-575

import { pathOr, path } from 'ramda'

export const getBrandName = (state) => {
  return path(['config', 'brandName'], state)
}

export const getRegion = (state) => {
  return pathOr(false, ['config', 'region'], state)
}

export const getPostCodeRules = (state, country) => {
  return pathOr({}, ['config', 'checkoutAddressFormRules', country], state)
}

export const getCountryCodeFromQAS = (state, country) => {
  return pathOr('', ['config', 'qasCountries', country], state)
}

export const getCountriesByAddressType = (state, addressType) => {
  const defaultValue = []
  switch (addressType) {
    case 'delivery':
      return pathOr(defaultValue, ['siteOptions', 'deliveryCountries'], state)
    case 'billing':
      return pathOr(defaultValue, ['siteOptions', 'billingCountries'], state)
    default:
      return defaultValue
  }
}
