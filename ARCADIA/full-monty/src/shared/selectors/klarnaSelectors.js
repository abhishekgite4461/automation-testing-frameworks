import { path } from 'ramda'
import {
  assembleAddressPayload,
  hashOrderSummary,
} from './../lib/checkout-utilities/klarna-utils'

function isCheckEligibilityButtonVisible(state) {
  const authorizationToken = path(['klarna', 'authorizationToken'], state)
  const shown = path(['klarna', 'shown'], state)
  return !!(!authorizationToken && shown)
}

function getAddressPayload(state) {
  const checkoutForms = state.forms.checkout
  const klarnaForm = state.forms.klarna
  const user = state.account.user
  const config = state.config
  const orderSummary = state.checkout.orderSummary
  return assembleAddressPayload({
    ...checkoutForms,
    klarnaForm,
    user,
    config,
    orderSummary,
  })
}

function isKlarnaSessionOutdated(state) {
  const orderSummaryHash = state.klarna.orderSummaryHash
  const checkoutForms = state.forms.checkout
  const orderSummary = state.checkout.orderSummary
  const shoppingBag = state.shoppingBag
  const currentHash =
    orderSummaryHash ||
    (process.browser && window.localStorage.getItem('orderHash'))
  const newHash = hashOrderSummary({
    ...checkoutForms,
    shoppingBag,
    orderSummary,
  })
  return !!(currentHash && currentHash !== newHash)
}

function calculateOrderSummaryHash(state) {
  const orderSummary = state.checkout.orderSummary
  const shoppingBag = state.shoppingBag
  const checkoutForms = state.forms.checkout
  return hashOrderSummary({ ...checkoutForms, shoppingBag, orderSummary })
}

export {
  isCheckEligibilityButtonVisible,
  getAddressPayload,
  isKlarnaSessionOutdated,
  calculateOrderSummaryHash,
}
