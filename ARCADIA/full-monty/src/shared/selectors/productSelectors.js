import { createSelector } from 'reselect'
import { path, pathOr, filter, find, map, propEq } from 'ramda'
import { isInCheckout } from './routingSelectors'

export const rootProductsSelector = (state) => state.products || {}

export const getProductsSearchResultsTotal = createSelector(
  rootProductsSelector,
  (products) => Number(products.totalProducts) || 0
)

export const getSelectedSKUs = (state) => {
  return filter(
    (value) => value,
    map(path(['fields', 'size', 'value']), state.forms.bundlesAddToBag)
  )
}

export const getSelectedSKU = (productId, state) => {
  return getSelectedSKUs(state)[productId]
}

export const getCurrentProduct = (state) => path(['currentProduct'], state)

export const getCurrentProductId = (state) =>
  pathOr('', ['productId'], getCurrentProduct(state)).toString()

export const getPdpBreadcrumbs = (state) =>
  pathOr([], ['breadcrumbs'], getCurrentProduct(state))

export const getPlpBreadcrumbs = pathOr([], ['products', 'breadcrumbs'])

export const getProductDetail = (state) => path(['productDetail'], state)

export const getSizeGuideType = (state) =>
  path(['productDetail', 'sizeGuideType'], state)

export const getProducts = (state) =>
  pathOr([], ['products', 'products'], state)

export const getProductById = (id, state) =>
  find(propEq('productId', id), getProducts(state))

export const getShouldIndex = (state) => Boolean(state.products.shouldIndex)

export const getSelectedProductSwatches = createSelector(
  rootProductsSelector,
  ({ selectedProductSwatches }) => selectedProductSwatches
)

export const getOrderProductsBasket = (state) => {
  if (
    !!isInCheckout(state) &&
    path(['checkout', 'orderSummary', 'basket'], state)
  ) {
    return path(['checkout', 'orderSummary', 'basket'], state)
  } else if (path(['shoppingBag', 'bag'], state)) {
    return state.shoppingBag.bag
  }

  return {}
}
