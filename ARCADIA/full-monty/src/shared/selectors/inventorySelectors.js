import { omit, path } from 'ramda'
import { createSelector } from 'reselect'
import { getCurrentProduct, getProductDetail } from './productSelectors'
import {
  getCatEntryId,
  getInventoryPositions,
  getBasketProductsWithInventory,
} from '../lib/ffs/product-inventory-utilities'

export const checkIfOSS = (productItems) =>
  !(
    productItems &&
    productItems.length &&
    productItems.some((item) => item.quantity > 0)
  )

/**
 * Selector function that returns the active product and inventory details
 * @NOTE please refer to productWithInventory.json schema
 */
export const getActiveProductWithInventory = createSelector(
  getCurrentProduct,
  getProductDetail,
  (currentProduct, productDetail) => {
    const productDataQuantity = path(['productDataQuantity'], currentProduct)
    const activeItemSKU = path(['activeItem', 'sku'], productDetail)
    const catEntryId = getCatEntryId(activeItemSKU, productDataQuantity)

    return activeItemSKU && catEntryId
      ? {
          catEntryId,
          inventoryPositions: omit(
            ['catentryId'],
            getInventoryPositions(catEntryId, productDataQuantity)
          ),
          name: path(['name'], currentProduct),
          productId: path(['productId'], currentProduct),
          quantity: path(['selectedQuantity'], productDetail),
          size: path(['activeItem', 'size'], productDetail),
          sku: activeItemSKU,
        }
      : null
  }
)

export const getShoppingBagProductsWithInventory = (state) => {
  const bag = path(['shoppingBag', 'bag'], state)
  return getBasketProductsWithInventory(bag)
}
