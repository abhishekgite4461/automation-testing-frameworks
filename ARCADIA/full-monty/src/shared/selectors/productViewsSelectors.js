import { PRODUCT, OUTFIT } from '../constants/productImageTypes'
import { isFeatureResponsiveProductViewEnabled } from './featureSelectors'
import { isMaxViewPort } from './viewportSelectors'
import { path } from 'ramda'

const getOverriddenDefaultView = (state) => {
  const defaultViewType = path(['productViews', 'defaultViewType'], state)
  return isFeatureResponsiveProductViewEnabled(state)
    ? isMaxViewPort(state)('mobile')
      ? PRODUCT
      : OUTFIT
    : defaultViewType || PRODUCT
}

const isProductViewSelected = (state) => {
  const selectedViewType = path(['productViews', 'selectedViewType'], state)
  const viewTypeSelected =
    selectedViewType === PRODUCT || selectedViewType === OUTFIT
  const overriddenDefaultView = getOverriddenDefaultView(state)
  return (
    (viewTypeSelected ? selectedViewType : overriddenDefaultView) === PRODUCT
  )
}

const isOutfitViewSelected = (state) => !isProductViewSelected(state)

export { getOverriddenDefaultView, isProductViewSelected, isOutfitViewSelected }
