import { pathOr } from 'ramda'

function getUserDefaultPaymentType(state) {
  return pathOr(null, ['account', 'user', 'creditCard', 'type'], state)
}

function getUserAddressCountry(accountState, type) {
  return pathOr('United Kingdom', [type, 'address', 'country'], accountState)
}

function isKlarnaDefaultPaymentType(state) {
  return getUserDefaultPaymentType(state) === 'KLRNA'
}

function isLoggedIn(state) {
  return pathOr(false, ['account', 'user', 'exists'], state)
}

function isUserAuthenticated(state) {
  return pathOr(false, ['auth', 'authentication'], state)
}

export {
  getUserDefaultPaymentType,
  getUserAddressCountry,
  isKlarnaDefaultPaymentType,
  isLoggedIn,
  isUserAuthenticated,
}
