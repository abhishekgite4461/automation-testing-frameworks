import { pathOr } from 'ramda'

const isModalOpen = (state) => pathOr(false, ['modal', 'open'], state)

const getScrollPositionOnOpenModal = (state) =>
  pathOr(null, ['modal', 'scrollPositionOnOpenModal'], state)

export { getScrollPositionOnOpenModal, isModalOpen }
