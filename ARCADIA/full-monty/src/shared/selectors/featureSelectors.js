import { createSelector } from 'reselect'

const rootSelector = (state) => state.features

export const getFeaturesStatus = createSelector(
  rootSelector,
  (features = {}) => features.status || {}
)

export const isFeatureStickyHeaderEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_STICKY_HEADER
)

export const isFeatureCFSIEnabled = createSelector(
  getFeaturesStatus,
  (status) => Boolean(status.FEATURE_CFSI)
)

export const isFeatureShowCFSIEspotEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_SHOW_CFSI_PDP_ESPOT
)

export const isFeaturePUDOEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_PUDO
)

export const isFeatureWishlistEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_WISHLIST
)

export const isFeatureMyCheckoutDetailsEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_MY_CHECKOUT_DETAILS
)

export const isFeatureSavePaymentDetailsEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_SAVE_PAYMENT_DETAILS
)

export const isFeatureTransferBasketEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_TRANSFER_BASKET
)

export const isFeatureAddressBookEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_ADDRESS_BOOK
)

export const isFeaturePLPRedirectIfOneProductEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_PLP_REDIRECT_IF_ONE_PRODUCT
)

export const isFeatureQubitHiddenEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_QUBIT_HIDDEN
)

export const isFeatureResponsiveProductViewEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_RESPONSIVE_PRODUCT_VIEW
)

export const isFeatureCarouselThumbnailEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_PRODUCT_CAROUSEL_THUMBNAIL
)

export const isFeatureDesktopResetPasswordEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_DESKTOP_RESET_PASSWORD
)

export const isAmplienceFeatureEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_USE_AMPLIENCE
)

export const isFeatureDDPEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_DDP
)

export const isFeatureEnhancedMessagingEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_ENHANCED_MESSAGING
)

export const isFeatureDDPPromoEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_DISPLAY_DDP_PROMOTION
)

export const getFeaturesKeepAlive = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_KEEP_ALIVE
)

export const isFeatureUnifiedLoginRegisterEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_UNIFIED_LOGIN_REGISTER
)

export const isFeatureHomePageSegmentationEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_HOME_PAGE_SEGMENTATION
)

export const isFeatureLogBadAttributeBannersEnabled = createSelector(
  getFeaturesStatus,
  (status) => status.FEATURE_LOG_BAD_ATTRIBUTE_BANNERS
)
