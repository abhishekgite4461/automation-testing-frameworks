import { path, pathOr } from 'ramda'
import { createSelector } from 'reselect'

// @NOTE to refactor using reselect
const getDeliveryCountries = (state) => state.siteOptions.deliveryCountries

const getBillingCountries = (state) => state.siteOptions.billingCountries

const selectSiteOptions = (state) => path(['siteOptions'], state)

const getDDPProduct = createSelector(selectSiteOptions, (siteOptions) =>
  path(['ddp', 'ddpProduct'], siteOptions)
)

const getDDPSkus = createSelector(getDDPProduct, (ddpProduct) =>
  pathOr([], ['ddpSkus'], ddpProduct)
)

const getDDPSkuItem = (state, sku) => {
  const ddpSkus = getDDPSkus(state)
  return ddpSkus.find((elem) => elem.sku === sku)
}

const getDDPDefaultSku = (state) => {
  const ddpSkus = getDDPSkus(state)
  return ddpSkus.find((elem) => elem.default === true) || {}
}

export {
  getDeliveryCountries,
  getBillingCountries,
  selectSiteOptions,
  getDDPSkus,
  getDDPSkuItem,
  getDDPDefaultSku,
  getDDPProduct,
}
