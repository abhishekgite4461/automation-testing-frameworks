import { clone, path, uniq, isEmpty, pathOr } from 'ramda'
import { createSelector } from 'reselect'
import { rootProductsSelector } from './productSelectors'

export const getActiveRefinements = (state) => {
  const refinements = path(['products', 'activeRefinements'], state)
  if (refinements) {
    // Currently only supported in CoreAPI
    const activeRefinements = {}
    refinements.forEach((refinement) => {
      const propertyName = refinement.propertyName
      const dimensionName = refinement.dimensionName
      const lowerBound = refinement.lowerBound
      const upperBound = refinement.upperBound
      if (propertyName && propertyName.toUpperCase() === 'NOWPRICE') {
        activeRefinements[propertyName] = {}
        activeRefinements[propertyName].key = propertyName.toUpperCase()
        activeRefinements[propertyName].title = 'Price'
        activeRefinements[propertyName].values = [
          {
            key: `${propertyName}${lowerBound}${upperBound}`.toUpperCase(),
            label: '',
            lowerBound,
            upperBound,
          },
        ]
      } else {
        if (!activeRefinements[dimensionName]) {
          activeRefinements[dimensionName] = {}
          activeRefinements[dimensionName].key = dimensionName.toUpperCase()
          activeRefinements[dimensionName].title =
            refinement.properties.refinement_name
          activeRefinements[dimensionName].values = []
        }
        activeRefinements[dimensionName].values.push({
          key: refinement.properties.SourceId.toUpperCase(),
          label: refinement.label,
          seoUrl: path(['removeAction', 'navigationState'], refinement),
        })
      }
    })
    return Object.keys(activeRefinements).map((key) => {
      return activeRefinements[key]
    })
  }
  return []
}

// state = {key: string, value: string}
// query = [key1:value]
// return [{key: string, value: array}, {key1: string, value: array2}]
export const mergeStateAndQueryRefinements = (
  selectedOptions = {},
  appliedOptions = {},
  queryrefinements = []
) => {
  const selected = clone(selectedOptions)
  const applied = clone(appliedOptions)

  // convert state to Map
  queryrefinements.forEach((query) => {
    const token = query.split(':')
    if (token.length === 2) {
      const key = decodeURIComponent(token[0])
      if (!selected[key]) {
        selected[key] = []
      }
    }
  })
  Object.keys(selected).forEach((key) => {
    if (typeof selected[key] === 'string') {
      selected[key] = selected[key].split(',')
    }
    queryrefinements.forEach((query) => {
      const token = query.split(':')
      if (token.length === 2) {
        const value = decodeURIComponent(token[1])
        if (key === decodeURIComponent(token[0]) && value) {
          selected[key].push(value)
        }
      }
    })
  })
  Object.keys(applied).forEach((key) => {
    if (typeof applied[key] === 'string') {
      applied[key] = applied[key].split(',')
    }
    if (!selected[key]) {
      selected[key] = applied[key]
    } else {
      selected[key] = selected[key].concat(applied[key])
    }
  })
  const result = []
  Object.keys(selected).forEach((key) => {
    result.push({
      key,
      value: uniq(selected[key]).join(','),
    })
  })
  return result
}

export const getSelectedRefinements = ({ refinements: { selectedOptions } }) =>
  Object.entries(selectedOptions).map(([key, options]) => ({
    key,
    value: options.join(','),
  }))

export const getRefinements = createSelector(
  rootProductsSelector,
  ({ refinements = [] }) =>
    refinements.filter((refinement) => refinement && !isEmpty(refinement))
)

export const getAppliedOptions = (state) =>
  pathOr({}, ['refinements', 'appliedOptions'], state)

export const getLowerCaseAppliedOptionsKeys = createSelector(
  getAppliedOptions,
  (appliedOptions) =>
    Object.keys(appliedOptions).map(
      (x) => (typeof x === 'string' ? x.toLowerCase() : x)
    )
)

export const getNumberRefinements = createSelector(
  getRefinements,
  getLowerCaseAppliedOptionsKeys,
  (refinements, filteredAppliedOptions) => {
    const selectedRefinements = refinements
      .filter((x) => {
        return (
          x.refinementOptions &&
          x.refinementOptions.filter((y) => y.selectedFlag).length > 0
        )
      })
      .map((x) => (x.label ? x.label.toLowerCase() : undefined))
    return uniq(
      selectedRefinements.concat(filteredAppliedOptions).filter((x) => x)
    ).length
  }
)

export const getRefinementOptions = ({
  refinements: { selectedOptions, appliedOptions },
}) => ({
  selectedOptions,
  appliedOptions,
})
