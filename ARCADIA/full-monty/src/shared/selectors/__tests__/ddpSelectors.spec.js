import {
  isDDPUser,
  isDDPRenewable,
  isDDPOrder,
  isPartialOrderDDP,
  isDDPPromotionEnabled,
  isDDPUserInPreExpiryWindow,
  isDDPRenewablePostWindow,
  isDDPActiveUserPreRenewWindow,
  isCurrentOrRecentDDPSubscriber,
} from '../ddpSelectors'

describe('DDP selectors', () => {
  describe('isDDPUserInPreExpiryWindow', () => {
    it('should return true if isDDPUser status is true and the user is in Pre Expiry Window', () => {
      expect(
        isDDPUserInPreExpiryWindow({
          account: {
            user: {
              isDDPUser: true,
              isDDPRenewable: true,
            },
          },
        })
      ).toEqual(true)
    })

    it('should return false if isDDPUser status is false and the user is in Pre Expiry Window', () => {
      expect(
        isDDPUserInPreExpiryWindow({
          account: {
            user: {
              isDDPUser: false,
              isDDPRenewable: true,
            },
          },
        })
      ).toEqual(false)
    })

    it('should return false if isDDPUser status is true and the user is not in Pre Expiry Window', () => {
      expect(
        isDDPUserInPreExpiryWindow({
          account: {
            user: {
              isDDPUser: true,
              isDDPRenewable: false,
            },
          },
        })
      ).toEqual(false)
    })
  })

  describe('isDDPRenewablePostWindow', () => {
    it('should return true if isDDPUser status is false and the user is in the Post Expiry Window', () => {
      expect(
        isDDPRenewablePostWindow({
          account: {
            user: {
              isDDPUser: false,
              isDDPRenewable: true,
            },
          },
        })
      ).toEqual(true)
    })

    it('should return false if isDDPUser status is true and the user is in the Post Expiry Window', () => {
      expect(
        isDDPRenewablePostWindow({
          account: {
            user: {
              isDDPUser: true,
              isDDPRenewable: true,
            },
          },
        })
      ).toEqual(false)
    })

    it('should return false if isDDPUser status is false and the user is not in the Post Expiry Window', () => {
      expect(
        isDDPRenewablePostWindow({
          account: {
            user: {
              isDDPUser: false,
              isDDPRenewable: false,
            },
          },
        })
      ).toEqual(false)
    })
  })

  describe('isDDPActiveUserPreRenewWindow', () => {
    it('should return true if isDDPUser status is true and the user is not in the Renewal Window', () => {
      expect(
        isDDPActiveUserPreRenewWindow({
          account: {
            user: {
              isDDPUser: true,
              isDDPRenewable: false,
            },
          },
        })
      ).toEqual(true)
    })

    it('should return false if isDDPUser status is false and the user is not in the Renewal Window', () => {
      expect(
        isDDPActiveUserPreRenewWindow({
          account: {
            user: {
              isDDPUser: false,
              isDDPRenewable: false,
            },
          },
        })
      ).toEqual(false)
    })

    it('should return false if isDDPUser status is true and the user is in the Renewal Window', () => {
      expect(
        isDDPActiveUserPreRenewWindow({
          account: {
            user: {
              isDDPUser: true,
              isDDPRenewable: true,
            },
          },
        })
      ).toEqual(false)
    })
  })

  describe('isCurrentOrRecentDDPSubscriber', () => {
    it('should return true if isDDPUser status is true', () => {
      expect(
        isCurrentOrRecentDDPSubscriber({
          account: {
            user: {
              isDDPUser: true,
              isDDPRenewable: false,
            },
          },
        })
      ).toEqual(true)
    })
    it('should return true if isDDPRenewable status is true', () => {
      expect(
        isCurrentOrRecentDDPSubscriber({
          account: {
            user: {
              isDDPUser: false,
              isDDPRenewable: true,
            },
          },
        })
      ).toEqual(true)
    })
    it('should return false if isDDPUser status is false and isDDPRenewable status is false', () => {
      expect(
        isCurrentOrRecentDDPSubscriber({
          account: {
            user: {
              isDDPUser: false,
              isDDPRenewable: false,
            },
          },
        })
      ).toEqual(false)
    })
  })

  describe('isDDPUser', () => {
    it('should return isDDPUser when value exists in the state', () => {
      expect(
        isDDPUser({
          account: {
            user: {
              isDDPUser: true,
            },
          },
        })
      ).toEqual(true)
    })

    it(`should return falsy when value doesn't exist in the state`, () => {
      expect(isDDPUser({})).toBeFalsy()
    })
  })

  describe('isDDPRenewable', () => {
    it('should return isDDPRenewable when value exists in the state', () => {
      expect(
        isDDPRenewable({
          account: {
            user: {
              isDDPRenewable: true,
            },
          },
        })
      ).toEqual(true)
    })

    it(`should return falsy when value doesn't exist in the state`, () => {
      expect(isDDPRenewable({})).toBeFalsy()
    })
  })

  describe('isDDPOrder', () => {
    it('should return isDDPOrder when value exists in the state', () => {
      expect(
        isDDPOrder({
          checkout: {
            orderSummary: {
              basket: {
                isDDPOrder: true,
              },
            },
          },
        })
      ).toEqual(true)
    })

    it(`should return false when value doesn't exist in the state`, () => {
      expect(isDDPOrder({})).toEqual(false)
    })
  })

  describe('isPartialOrderDDP', () => {
    it('should return isPartialOrderDDP when value exists in the state', () => {
      expect(
        isPartialOrderDDP({
          checkout: {
            partialOrderSummary: {
              basket: {
                isDDPOrder: true,
              },
            },
          },
        })
      ).toEqual(true)
    })

    it(`should return false when value doesn't exist in the state`, () => {
      expect(isDDPOrder({})).toEqual(false)
    })
  })

  describe('isDDPPromotionEnabled', () => {
    describe('FEATURE_DISPLAY_DDP_PROMOTION feature flag is enabled', () => {
      const state = {
        features: {
          status: {
            FEATURE_DISPLAY_DDP_PROMOTION: true,
          },
        },
      }

      it('should return true if user is not DDP subscriber and isDDPOrder status is false', () => {
        expect(
          isDDPPromotionEnabled({
            ...state,
            account: {
              user: {
                isDDPUser: false,
              },
            },
            checkout: {
              orderSummary: {
                basket: {
                  isDDPOrder: false,
                },
              },
            },
          })
        ).toEqual(true)
      })

      it('should return false if user is DDP subscriber and not in Renewal Window', () => {
        expect(
          isDDPPromotionEnabled({
            ...state,
            account: {
              user: {
                isDDPUser: true,
                isDDPRenewable: false,
              },
            },
          })
        ).toEqual(false)
      })

      it('should return true if user is DDP subscriber in Renewal Window and isDDPOrder status is false', () => {
        expect(
          isDDPPromotionEnabled({
            ...state,
            account: {
              user: {
                isDDPUser: true,
                isDDPRenewable: true,
              },
            },
            checkout: {
              orderSummary: {
                basket: {
                  isDDPOrder: false,
                },
              },
            },
          })
        ).toEqual(true)
      })

      it('should return false if isDDPOrder status is true', () => {
        expect(
          isDDPPromotionEnabled({
            ...state,
            checkout: {
              orderSummary: {
                basket: {
                  isDDPOrder: true,
                },
              },
            },
          })
        ).toEqual(false)
      })

      it(`should return true when values do not exist in the state`, () => {
        expect(isDDPPromotionEnabled(state)).toEqual(true)
      })
    })

    describe('FEATURE_DISPLAY_DDP_PROMOTION feature flag is disabled', () => {
      it('should always return false', () => {
        expect(
          isDDPPromotionEnabled({
            features: {
              status: {
                FEATURE_DISPLAY_DDP_PROMOTION: false,
              },
            },
            account: {
              user: {
                isDDPUser: false,
                isDDPRenewable: false,
              },
            },
            checkout: {
              orderSummary: {
                basket: {
                  isDDPOrder: false,
                },
              },
            },
          })
        ).toEqual(false)
      })
    })
  })
})
