import deepFreeze from 'deep-freeze'
import * as storeLocatorSelectors from '../storeLocatorSelectors'

describe('Store locator selectors', () => {
  describe('getStoreLocatorStores()', () => {
    it('should return the list of stores', () => {
      const stores = deepFreeze([
        { foo: 'bar' },
        { bar: 'baz' },
        { baz: 'qux' },
      ])
      const state = { storeLocator: { stores } }
      expect(storeLocatorSelectors.getStoreLocatorStores(state)).toBe(stores)
    })
  })

  describe('getStoreByIndex()', () => {
    it('should return the requested store', () => {
      const state = {
        storeLocator: {
          stores: [{ foo: 'bar' }, { bar: 'baz' }, { baz: 'qux' }],
        },
      }
      expect(storeLocatorSelectors.getStoreByIndex(state, 1)).toEqual({
        bar: 'baz',
      })
    })
  })

  describe('getStoreLocatorFilters()', () => {
    it('should return the store locator filters', () => {
      const state = {
        storeLocator: {
          filters: 'some filters',
        },
      }
      expect(storeLocatorSelectors.getStoreLocatorFilters(state)).toBe(
        'some filters'
      )
    })
  })

  describe('isMapExpanded()', () => {
    it('should return the `mapExpanded` flag', () => {
      const state = {
        storeLocator: {
          mapExpanded: 'some boolean',
        },
      }
      expect(storeLocatorSelectors.isMapExpanded(state)).toBe('some boolean')
    })
  })

  describe('getActiveItem()', () => {
    it('should return the currently active item', () => {
      const state = {
        findInStore: {
          activeItem: 'some active item',
        },
      }
      expect(storeLocatorSelectors.getActiveItem(state)).toBe(
        'some active item'
      )
    })
  })
})
