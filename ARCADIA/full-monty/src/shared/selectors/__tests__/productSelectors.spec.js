import {
  getSelectedSKU,
  getSelectedSKUs,
  getCurrentProduct,
  getCurrentProductId,
  getProductDetail,
  getSizeGuideType,
  getProducts,
  getProductById,
  getPdpBreadcrumbs,
  getPlpBreadcrumbs,
  getShouldIndex,
  getProductsSearchResultsTotal,
  getSelectedProductSwatches,
  getOrderProductsBasket,
} from '../productSelectors'

describe('Product Selectors', () => {
  const mockState = {
    forms: {
      bundlesAddToBag: {
        29750936: {
          fields: {
            size: {
              value: '602017001154355',
            },
          },
        },
        29752415: {
          fields: {
            size: {
              value: '602017001154368',
            },
          },
        },
      },
    },
    products: {
      products: [{ productId: 123 }, { productId: 321 }],
    },
  }

  describe('#getProductsSearchResultsTotal', () => {
    it('should return total products', () => {
      const result = getProductsSearchResultsTotal({
        products: { totalProducts: 10 },
      })
      expect(result).toBe(10)
    })
    it('should return default total products when not available', () => {
      expect(getProductsSearchResultsTotal({ products: {} })).toBe(0)
    })
  })

  describe('#getShouldIndex', () => {
    it('should return true', () => {
      expect(getShouldIndex({ products: { shouldIndex: true } })).toBe(true)
    })
    it('should return false', () => {
      expect(getShouldIndex({ products: { shouldIndex: false } })).toBe(false)
    })
  })

  describe('#getSelectedSKU', () => {
    it('should get SKU from `bundlesAddToBag` form state', () => {
      expect(getSelectedSKU(29750936, mockState)).toBe('602017001154355')
    })

    it('should return undefined if can‘t find product', () => {
      expect(getSelectedSKU(29750122, mockState)).toBeUndefined()
    })
  })

  describe('#getSelectedSKUs', () => {
    it('should get SKUs from `bundlesAddToBag` form state', () => {
      expect(getSelectedSKUs(mockState)).toEqual({
        29750936: '602017001154355',
        29752415: '602017001154368',
      })
    })

    it('should filter out empty values', () => {
      const mockState = {
        forms: {
          bundlesAddToBag: {
            29750936: {
              fields: {
                size: {
                  value: '602017001154355',
                },
              },
            },
            29752415: {
              fields: {
                size: {
                  value: '',
                },
              },
            },
          },
        },
      }
      expect(getSelectedSKUs(mockState)).toEqual({
        29750936: '602017001154355',
      })
    })

    it('should return an empty object if nothing in state', () => {
      const mockState = {
        forms: {
          bundlesAddToBag: {},
        },
      }
      expect(getSelectedSKUs(mockState)).toEqual({})
    })
  })

  describe('#getCurrentProduct', () => {
    it('should return undefined if currentProduct is not found', () => {
      expect(getCurrentProduct()).toBeUndefined()
      expect(getCurrentProduct([])).toBeUndefined()
      expect(getCurrentProduct({})).toBeUndefined()
    })
    it('should return currentProduct when found', () => {
      const mockedCurrentProduct = {
        name: 'Red Flower Ruffle Mini Skirt',
      }
      expect(
        getCurrentProduct({
          ...mockState,
          currentProduct: mockedCurrentProduct,
        })
      ).toBe(mockedCurrentProduct)
    })
  })

  describe('#getPdpBreadcrumbs', () => {
    it('should return empty array when currentProduct is not found', () => {
      expect(getPdpBreadcrumbs()).toEqual([])
    })

    it('should return empty array currentProduct is found but breadcrumb is not found', () => {
      expect(getPdpBreadcrumbs({ currentProduct: {} })).toEqual([])
    })

    it('should return breadcrumb array when currentProduct is found but breadcrumb is found', () => {
      const label = 'Home'
      const breadcrumbs = [{ label }]
      const result = getPdpBreadcrumbs({
        currentProduct: {
          breadcrumbs,
        },
      })
      expect(result).toEqual(breadcrumbs)
      expect(result[0].label).toEqual(label)
    })
  })

  describe('#getPlpBreadcrumbs', () => {
    it('should return PLP breadcrumbs when they exist', () => {
      const mockBreadcrumbs = 'fake breadcrumbs'
      expect(
        getPlpBreadcrumbs({ products: { breadcrumbs: mockBreadcrumbs } })
      ).toBe(mockBreadcrumbs)
    })

    it('should return empty array when there are no PLP products', () => {
      expect(getPlpBreadcrumbs({ foo: 'bar' })).toEqual([])
    })

    it('should return empty array when there are no PLP product breadcrumbs', () => {
      expect(getPlpBreadcrumbs({ products: { foo: 'bar' } })).toEqual([])
    })
  })

  describe('#getCurrentProductId', () => {
    it('should return empty string if there is no productId', () => {
      expect(getCurrentProductId()).toBe('')
      expect(getCurrentProductId([])).toBe('')
      expect(getCurrentProductId({})).toBe('')
    })
    it('should return currentProduct when found', () => {
      expect(
        getCurrentProductId({
          currentProduct: {
            name: 'Red Flower Ruffle Mini Skirt',
            productId: 1007,
          },
        })
      ).toBe('1007')
    })
  })

  describe('#getProductDetail', () => {
    it('should return undefined if productDetail is not found', () => {
      expect(getProductDetail()).toBeUndefined()
      expect(getProductDetail([])).toBeUndefined()
      expect(getProductDetail({})).toBeUndefined()
    })
    it('should return productDetail when found', () => {
      const mockedProductDetail = {
        activeItem: { sku: '007' },
      }
      expect(
        getProductDetail({
          ...mockState,
          productDetail: mockedProductDetail,
        })
      ).toBe(mockedProductDetail)
    })
  })

  describe('#getSizeGuideType', () => {
    it('should return undefined if sizeGuideType is not found', () => {
      expect(getSizeGuideType()).toBeUndefined()
    })
    it('should return sizeGuideType when found', () => {
      const mockedSizeGuideType = 'Dresses'
      expect(
        getSizeGuideType({
          ...mockState,
          productDetail: {
            sizeGuideType: mockedSizeGuideType,
          },
        })
      ).toBe(mockedSizeGuideType)
    })
  })

  describe('#getProducts', () => {
    it('should return the list of all products', () => {
      expect(getProducts(mockState)).toEqual(mockState.products.products)
    })

    it('should return an empty array when no products can be found', () => {
      expect(getProducts({})).toEqual([])
    })
  })

  describe('#getProductById', () => {
    it('should return a product with matching productId', () => {
      const product = mockState.products.products[0]

      expect(getProductById(product.productId, mockState)).toEqual(product)
    })

    it('should return undefined where no product was found with the given productId', () => {
      expect(getProductById('non-existing-id', mockState)).toEqual(undefined)
    })
  })

  describe('getSelectedProductSwatches', () => {
    it('should return selected product swatches', () => {
      const selectedProductSwatches = { 111: 222, 333: 444 }
      expect(
        getSelectedProductSwatches({
          products: { selectedProductSwatches },
        })
      ).toEqual(selectedProductSwatches)
    })
  })

  describe('getOrderProducts', () => {
    let fakeState = {}
    beforeEach(() => {
      fakeState = {
        routing: {
          location: {
            pathname: '/checkout/delivery',
          },
        },
        checkout: {
          orderSummary: {
            basket: {
              products: [
                {
                  pippo: 'pluto',
                },
              ],
            },
          },
        },
        shoppingBag: {
          bag: {
            products: [
              {
                pippo: 'minnie',
              },
            ],
          },
        },
      }
    })
    it('should return order products from shoppingbag when outside checkout (NO order summary)', () => {
      fakeState.routing.location.pathname = '/plp'
      fakeState.checkout.orderSummary = {}
      expect(getOrderProductsBasket(fakeState)).toEqual(
        fakeState.shoppingBag.bag
      )
    })
    it('should return order product (basket) from orderSummary->basket if in checkout', () => {
      expect(getOrderProductsBasket(fakeState)).toEqual(
        fakeState.checkout.orderSummary.basket
      )
    })
    it('should return order product (basket) from orderSummary->basket if in order complete', () => {
      fakeState.routing.location.pathname = '/order-complete'
      fakeState.shoppingBag = {}
      expect(getOrderProductsBasket(fakeState)).toEqual({})
    })
    it('should return empty object as default (defensive js function)', () => {
      fakeState.checkout.orderSummary = {}
      fakeState.shoppingBag = {}
      expect(getOrderProductsBasket(fakeState)).toEqual({})
    })
    it('should return always an object also if no routing', () => {
      fakeState.routing = {}
      fakeState.shoppingBag = {}
      expect(getOrderProductsBasket(fakeState)).toEqual({})
    })
  })
})
