import {
  getActiveRefinements,
  mergeStateAndQueryRefinements,
  getNumberRefinements,
  getSelectedRefinements,
  getRefinementOptions,
  getRefinements,
  getAppliedOptions,
  getLowerCaseAppliedOptionsKeys,
} from '../refinementsSelectors'

describe('Refinements Selectors', () => {
  describe('getRefinements', () => {
    it('should return empty array if refinements is not in state', () => {
      expect(getRefinements({})).toEqual([])
    })
    it('should return non falsy refinements from the state', () => {
      expect(
        getRefinements({
          products: {
            refinements: [null, 1, false, '2', undefined, [], {}],
          },
        })
      ).toEqual([1, '2'])
    })
  })
  describe('#getAppliedOptions', () => {
    it('should return empty object when no applied option in state', () => {
      const state = {}
      expect(getAppliedOptions(state)).toEqual({})
    })
    it('should return applied options when in state', () => {
      const appliedOptions = 'appliedOptions'
      const state = {
        refinements: {
          appliedOptions,
        },
      }
      expect(getAppliedOptions(state)).toBe(appliedOptions)
    })
  })
  describe('#getFilteredAppliedOptions', () => {
    it('should return empty array if no applied options in state', () => {
      const state = {}
      expect(getLowerCaseAppliedOptionsKeys(state)).toEqual([])
    })
    it('should return lower case applied options keys', () => {
      const state = {
        refinements: {
          appliedOptions: {
            A: '1',
            b: '2',
            C: '3',
          },
        },
      }
      expect(getLowerCaseAppliedOptionsKeys(state)).toEqual(['a', 'b', 'c'])
    })
  })
  describe('#getActiveRefinements', () => {
    it('should return empty array when no active refinements in state', () => {
      const state = {
        products: {
          activeRefinements: [],
        },
      }
      expect(getActiveRefinements(state)).toEqual([])
    })
    it('should return array of active refinements from state', () => {
      const state = {
        products: {
          activeRefinements: [
            {
              dimensionName: 'TopShop_uk_ce3_product_type',
              properties: {
                SourceId: 'ECMC_PROD_CE3_PRODUCT_TYPE_1_dresses',
                refinement_name: 'Product Type',
              },
              label: 'dresses',
              removeAction: {
                navigationState: 'XXXXX',
              },
            },
            {
              propertyName: 'nowPrice',
              upperBound: '32.0',
              lowerBound: '5.0',
            },
          ],
        },
      }
      expect(getActiveRefinements(state)).toEqual([
        {
          key: 'TOPSHOP_UK_CE3_PRODUCT_TYPE',
          title: 'Product Type',
          values: [
            {
              key: 'ECMC_PROD_CE3_PRODUCT_TYPE_1_DRESSES',
              label: 'dresses',
              seoUrl: 'XXXXX',
            },
          ],
        },
        {
          key: 'NOWPRICE',
          title: 'Price',
          values: [
            {
              key: 'NOWPRICE5.032.0',
              label: '',
              upperBound: '32.0',
              lowerBound: '5.0',
              seoUrl: undefined,
            },
          ],
        },
      ])
    })
  })

  describe('#mergeStateAndQueryRefinements', () => {
    it('should handle no state', () => {
      const stateRefinements = {}
      const appliedOptions = {}
      const queryRefinements = ['color:red', 'color:blue']
      const expectedRefinement = [
        {
          key: 'color',
          value: 'red,blue',
        },
      ]
      expect(
        mergeStateAndQueryRefinements(
          stateRefinements,
          appliedOptions,
          queryRefinements
        )
      ).toEqual(expectedRefinement)
    })

    it('should handle no query', () => {
      const stateRefinements = {
        color: ['red', 'blue'],
      }
      const queryRefinements = []
      const appliedOptions = {}
      const expectedRefinement = [
        {
          key: 'color',
          value: 'red,blue',
        },
      ]
      expect(
        mergeStateAndQueryRefinements(
          stateRefinements,
          appliedOptions,
          queryRefinements
        )
      ).toEqual(expectedRefinement)
    })

    it('should merged state and query', () => {
      const stateRefinements = {
        color: ['red', 'blue'],
      }
      const queryRefinements = ['color:green', 'color:navy']
      const appliedOptions = {}
      const expectedRefinement = [
        {
          key: 'color',
          value: 'red,blue,green,navy',
        },
      ]
      expect(
        mergeStateAndQueryRefinements(
          stateRefinements,
          appliedOptions,
          queryRefinements
        )
      ).toEqual(expectedRefinement)
    })
    it('should handle duplicates', () => {
      const stateRefinements = {
        color: ['black'],
      }
      const queryRefinements = ['color:black', 'color:black', 'color:black']
      const appliedOptions = {}
      const expectedRefinement = [
        {
          key: 'color',
          value: 'black',
        },
      ]
      expect(
        mergeStateAndQueryRefinements(
          stateRefinements,
          appliedOptions,
          queryRefinements
        )
      ).toEqual(expectedRefinement)
    })
    it('should handle space', () => {
      const stateRefinements = {
        'waist size': ['20'],
      }
      const queryRefinements = ['waist%20size:20']
      const appliedOptions = {}
      const expectedRefinement = [
        {
          key: 'waist size',
          value: '20',
        },
      ]
      expect(
        mergeStateAndQueryRefinements(
          stateRefinements,
          appliedOptions,
          queryRefinements
        )
      ).toEqual(expectedRefinement)
    })
    it('should handle space', () => {
      const stateRefinements = {
        'waist size': ['20'],
      }
      const queryRefinements = ['']
      const appliedOptions = {}
      const expectedRefinement = [
        {
          key: 'waist size',
          value: '20',
        },
      ]
      expect(
        mergeStateAndQueryRefinements(
          stateRefinements,
          appliedOptions,
          queryRefinements
        )
      ).toEqual(expectedRefinement)
    })
    it('should handle strings', () => {
      const stateRefinements = {
        'waist size': '20',
      }
      const queryRefinements = ['']
      const appliedOptions = {}
      const expectedRefinement = [
        {
          key: 'waist size',
          value: '20',
        },
      ]
      expect(
        mergeStateAndQueryRefinements(
          stateRefinements,
          appliedOptions,
          queryRefinements
        )
      ).toEqual(expectedRefinement)
    })
    it('should handle applied options', () => {
      const stateRefinements = {
        color: 'red',
      }
      const queryRefinements = ['']
      const appliedOptions = {
        color: 'blue',
      }
      const expectedRefinement = [
        {
          key: 'color',
          value: 'red,blue',
        },
      ]
      expect(
        mergeStateAndQueryRefinements(
          stateRefinements,
          appliedOptions,
          queryRefinements
        )
      ).toEqual(expectedRefinement)
    })
  })

  describe('getSelectedRefinements()', () => {
    it('should return ordered refinements', () => {
      const state = {
        refinements: {
          selectedOptions: {
            colour: ['black'],
            size: [4, 6, 8],
          },
        },
      }
      expect(getSelectedRefinements(state)).toEqual([
        {
          key: 'colour',
          value: 'black',
        },
        {
          key: 'size',
          value: '4,6,8',
        },
      ])
    })
  })

  describe('#getNumberRefinements', () => {
    it('should use product refinements selected Flag', () => {
      const state = {
        products: {
          refinements: [
            {
              label: 'Filter',
              refinementOptions: [],
            },
            {
              label: 'Colour',
              refinementOptions: [
                {
                  type: 'VALUE',
                  label: 'black',
                  value: 'black',
                },
                {
                  type: 'VALUE',
                  label: 'blue',
                  value: 'blue',
                  selectedFlag: true,
                },
              ],
            },
            {
              label: 'Price',
              refinementOptions: [
                {
                  type: 'RANGE',
                  label: 0,
                  value: 0,
                },
              ],
            },
          ],
        },
      }
      expect(getNumberRefinements(state)).toEqual(1)
    })

    it('should handle duplicates in appliedOptions', () => {
      const state = {
        refinements: {
          appliedOptions: {
            price: [],
            colour: [],
            3: [],
          },
        },
        products: {
          refinements: [
            {
              refinementOptions: [
                {
                  type: 'VALUE',
                  label: 'black',
                  value: 'black',
                },
                {
                  type: 'VALUE',
                  label: 'blue',
                  value: 'blue',
                  selectedFlag: true,
                },
              ],
            },
            {
              label: 'Filter',
              refinementOptions: [],
            },
            {
              label: 'Colour',
              refinementOptions: [
                {
                  type: 'VALUE',
                  label: 'black',
                  value: 'black',
                },
                {
                  type: 'VALUE',
                  label: 'blue',
                  value: 'blue',
                  selectedFlag: true,
                },
              ],
            },
            {
              label: 'Price',
              refinementOptions: [
                {
                  type: 'RANGE',
                  label: 0,
                  value: 0,
                },
              ],
            },
          ],
        },
      }
      expect(getNumberRefinements(state)).toEqual(3)
    })
  })

  describe('#getRefinementOptions', () => {
    it('should return selected and applied refinement options', () => {
      const selectedOptions = 'fake selected options'
      const appliedOptions = 'fake applied options'

      const state = {
        refinements: {
          selectedOptions,
          appliedOptions,
        },
      }
      expect(getRefinementOptions(state)).toEqual({
        selectedOptions,
        appliedOptions,
      })
    })
  })
})
