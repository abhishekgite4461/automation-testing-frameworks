import * as userLocatorSelectors from '../userLocatorSelectors'

describe('User locator selectors', () => {
  describe('getSelectedPlaceId()', () => {
    it('should return the selected place ID', () => {
      const state = {
        userLocator: {
          selectedPlaceDetails: {
            place_id: 'abc123',
          },
        },
      }
      expect(userLocatorSelectors.getSelectedPlaceId(state)).toBe('abc123')
    })
  })
})
