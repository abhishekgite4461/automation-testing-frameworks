import { getCFSDay } from '../fulfilmentSelectors'
import { getFulfilmentDetails } from '../../lib/get-delivery-days'
import { deliveryDays } from '../../lib/get-basket-availability'

jest.mock('../../lib/get-delivery-days', () => ({
  getFulfilmentDetails: jest.fn(),
}))

jest.mock('../../lib/get-basket-availability', () => ({
  deliveryDays: jest.fn(),
}))

const mockedState = {
  checkout: {
    orderSummary: {
      basket: {},
    },
  },
}

const mockedStore = {
  storeId: 'TS0873',
}

describe('@getCFSDay(state, storeDetails, storeLocatorType)', () => {
  beforeEach(jest.clearAllMocks)

  it('should get basket collection day if storeLocator === "collectFromStore"', () => {
    deliveryDays.mockReturnValueOnce({ CFSiDay: 'today' })

    expect(getCFSDay(mockedState, mockedStore, 'collectFromStore')).toEqual(
      'today'
    )
    expect(deliveryDays).toHaveBeenCalledTimes(1)
    expect(getFulfilmentDetails).toHaveBeenCalledTimes(0)
  })
  it('should get currentProduct (PDP) collection day if storeLocator !== "collectFromStore"', () => {
    getFulfilmentDetails.mockReturnValueOnce({ CFSiDay: 'Saturday' })

    expect(getCFSDay(mockedState, mockedStore, 'findInStore')).toEqual(
      'Saturday'
    )
    expect(getFulfilmentDetails).toHaveBeenCalledTimes(1)
    expect(deliveryDays).toHaveBeenCalledTimes(0)
  })
})
