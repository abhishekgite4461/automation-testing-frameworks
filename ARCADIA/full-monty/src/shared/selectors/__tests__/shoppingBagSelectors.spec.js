import {
  getShoppingBag,
  getShoppingBagOrderId,
  getShoppingBagTotalItems,
  isShoppingBagEmpty,
  isShoppingBagLoading,
  getShoppingBagProducts,
  bagContainsDDPProduct,
  bagContainsOnlyDDPProduct,
  getShoppingBagTotal,
  isZeroValueBag,
} from '../shoppingBagSelectors'

describe('Shopping Bag Selectors', () => {
  describe('getShoppingBagOrderId', () => {
    it('should return the orderId', () => {
      expect(
        getShoppingBagOrderId({
          shoppingBag: {
            bag: {
              orderId: 1234,
            },
          },
        })
      ).toBe(1234)
    })
    it('should return null if the orderId is not defined', () => {
      expect(getShoppingBagOrderId({})).toBeUndefined()
    })
  })

  describe('getShoppingBag', () => {
    it('should return the bag', () => {
      expect(
        getShoppingBag({
          shoppingBag: {
            bag: {
              orderId: 1234,
            },
          },
        })
      ).toEqual({ orderId: 1234 })
    })
    it('should return {} if the bag is not defined', () => {
      expect(getShoppingBag({})).toEqual({})
    })
  })
  describe('getShoppingBagTotal', () => {
    it('should return the total of the bag', () => {
      expect(
        getShoppingBagTotal({
          shoppingBag: {
            bag: {
              total: 1234,
            },
          },
        })
      ).toEqual(1234)
    })
    it('should return the numerical total of the bag', () => {
      expect(
        getShoppingBagTotal({
          shoppingBag: {
            bag: {
              total: '1234',
            },
          },
        })
      ).toEqual(1234)
    })
    it('should return 0 if the total is not defined', () => {
      expect(getShoppingBagTotal({})).toEqual(0)
    })
  })
  describe('isZeroValueBag', () => {
    it('should return true if bag is 0', () => {
      expect(
        isZeroValueBag({
          shoppingBag: {
            bag: {
              total: 0,
            },
          },
        })
      ).toEqual(true)
    })
    it('should return false if bag is > 0', () => {
      expect(
        isZeroValueBag({
          shoppingBag: {
            bag: {
              total: 0.01,
            },
          },
        })
      ).toEqual(false)
    })
  })
  describe('getShoppingBagProducts', () => {
    it('should return the products in bag', () => {
      const products = ['product 1', 'product 2']
      expect(
        getShoppingBagProducts({
          shoppingBag: {
            bag: {
              products,
            },
          },
        })
      ).toEqual(products)
    })
    it('should return [] if the bag has no products', () => {
      expect(getShoppingBagProducts({})).toEqual([])
    })
  })
  describe('isShoppingBagEmpty', () => {
    it('should return true if no products in bag', () => {
      expect(
        isShoppingBagEmpty({
          shoppingBag: {
            totalItems: 0,
          },
        })
      ).toBe(true)
    })
    it('should return false if some products in bag', () => {
      expect(
        isShoppingBagEmpty({
          shoppingBag: {
            totalItems: 2,
          },
        })
      ).toBe(false)
    })
  })
  describe('getShoppingBagTotalItems', () => {
    it('should return 5 if there are products in bag', () => {
      expect(
        getShoppingBagTotalItems({
          shoppingBag: {
            totalItems: 5,
          },
        })
      ).toBe(5)
    })
    it(`should return undefined if totalItems doesn't exists in bag`, () => {
      expect(
        getShoppingBagTotalItems({
          shoppingBag: {},
        })
      ).toBeUndefined()
    })
  })
  describe('isShoppingBagLoading', () => {
    it('should return true if loading', () => {
      expect(
        isShoppingBagLoading({
          shoppingBag: {
            loadingShoppingBag: true,
          },
        })
      ).toBe(true)
    })
    it('should return false if not loading', () => {
      expect(
        isShoppingBagLoading({
          shoppingBag: {
            loadingShoppingBag: false,
          },
        })
      ).toBe(false)
    })
  })
  describe('bagContainsDDPProduct', () => {
    it('should return true if bag contains at least one DDP product', () => {
      const products = [{ isDDPProduct: true }, { isDDPProduct: false }]
      expect(
        bagContainsDDPProduct({
          shoppingBag: {
            bag: {
              products,
            },
          },
        })
      ).toBe(true)
    })

    it('should return false if bag does not contain any DDP product', () => {
      const products = [{ isDDPProduct: false }]
      expect(
        bagContainsDDPProduct({
          shoppingBag: {
            bag: products,
          },
        })
      ).toBe(false)
    })
  })

  describe('bagContainsOnlyDDPProduct', () => {
    it('should return true if bag contains one and only DDP product', () => {
      expect(
        bagContainsOnlyDDPProduct({
          shoppingBag: {
            totalItems: 1,
            bag: {
              products: [{ isDDPProduct: true }],
            },
          },
        })
      ).toBe(true)
    })

    it('should return false if bag contains a DDP product and there are other items in my bag', () => {
      expect(
        bagContainsOnlyDDPProduct({
          shoppingBag: {
            totalItems: 2,
            bag: {
              products: [{ isDDPProduct: true }, { isDDPProduct: false }],
            },
          },
        })
      ).toBe(false)
    })

    it('should return false if bag does not contain a DDP product and there is an item in my bag', () => {
      expect(
        bagContainsOnlyDDPProduct({
          shoppingBag: {
            totalItems: 1,
            bag: {
              products: [{ isDDPProduct: false }],
            },
          },
        })
      ).toBe(false)
    })

    it('should return false if bag does not contain any item and there is a DDP product in my bag', () => {
      expect(
        bagContainsOnlyDDPProduct({
          shoppingBag: {
            totalItems: 0,
            bag: {
              products: [],
            },
          },
        })
      ).toBe(false)
    })
  })
})
