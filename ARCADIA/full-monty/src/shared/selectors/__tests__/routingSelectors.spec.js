import {
  getLocation,
  getLocationQuery,
  getRoutePath,
  getRouteSearch,
  getRoutePathWithParams,
  getVisitedPaths,
  getPrevPath,
  isInCheckout,
  isHomePage,
  selectInDeliveryAndPayment,
  getRedirect,
  selectHostname,
} from '../routingSelectors'

describe('Routing Selectors', () => {
  describe(getLocation.name, () => {
    it('should return the location', () => {
      expect(
        getLocation({
          routing: {
            location: {
              propA: 'prop1',
            },
          },
        })
      ).toEqual({ propA: 'prop1' })
    })
    it('should return empty object if location is not defined', () => {
      expect(getLocation({})).toEqual({})
    })
  })
  describe(getLocationQuery.name, () => {
    it('should return query if exists', () => {
      expect(
        getLocationQuery({
          routing: {
            location: {
              query: {
                paramA: 'param1',
              },
            },
          },
        })
      ).toEqual({ paramA: 'param1' })
    })
    it('should return empty object there is no query', () => {
      expect(getLocationQuery({})).toEqual({})
    })
  })
  describe(getRoutePath.name, () => {
    it('should return pathname if any', () => {
      expect(
        getRoutePath({
          routing: {
            location: {
              pathname: '/random',
            },
          },
        })
      ).toEqual('/random')
    })
    it('should return empty string if the orderId is not defined', () => {
      expect(getRoutePath({})).toEqual('')
    })
  })
  describe(getRouteSearch.name, () => {
    const getStateWithSearch = (search) => ({
      routing: { location: { search } },
    })
    it('should return empty string if search is empty or invalid', () => {
      expect(getRouteSearch(getStateWithSearch())).toBe('')
      expect(getRouteSearch(getStateWithSearch(null))).toBe('')
      expect(getRouteSearch(getStateWithSearch(''))).toBe('')
    })
    it('should return routing search prop', () => {
      expect(getRouteSearch(getStateWithSearch('?q=red'))).toBe('?q=red')
    })
  })
  describe(getRoutePathWithParams.name, () => {
    const getStateWithPathAndParams = (pathname, search) => ({
      routing: { location: { pathname, search } },
    })
    it('should return empty string if both arguments are empty or invalid', () => {
      expect(getRoutePathWithParams(getStateWithPathAndParams())).toBe('')
      expect(
        getRoutePathWithParams(getStateWithPathAndParams(null, null))
      ).toBe('')
      expect(getRoutePathWithParams(getStateWithPathAndParams('', ''))).toBe('')
    })
    it('should return routing search prop', () => {
      expect(
        getRoutePathWithParams(
          getStateWithPathAndParams('/en/tsuk/product/red-bulls', '')
        )
      ).toBe('/en/tsuk/product/red-bulls')
      expect(
        getRoutePathWithParams(getStateWithPathAndParams('/search/', '?q=red'))
      ).toBe('/search/?q=red')
    })
  })

  describe(getVisitedPaths.name, () => {
    it('should return a list of visited paths', () => {
      expect(
        getVisitedPaths({
          routing: {
            visited: ['/some-path', '/another-path'],
          },
        })
      ).toEqual(['/some-path', '/another-path'])
    })
    it('should return empty array if no routes are defined', () => {
      expect(getVisitedPaths({})).toEqual([])
    })
  })

  describe(getPrevPath.name, () => {
    it('should return the pathname from where the current page was accessed', () => {
      const mockState = {
        routing: {
          location: { pathname: '/some-path' },
          visited: ['/', '/some-path'],
        },
      }
      expect(getPrevPath(mockState)).toBe('/')
    })
    it('should return the pathname from where the current page was most recently accessed', () => {
      const mockState = {
        routing: {
          location: { pathname: '/some-path' },
          visited: ['/', '/some-path', '/another-path', '/some-path'],
        },
      }
      expect(getPrevPath(mockState)).toBe('/another-path')
    })
    it('should return `direct link` if the current page was accessed from an external link or email', () => {
      const mockState = {
        routing: {
          location: { pathname: '/some-path' },
          visited: ['/some-path'],
        },
      }
      expect(getPrevPath(mockState)).toBe('direct link')
    })
  })

  describe(isInCheckout.name, () => {
    const getStateWithRoute = (route) => ({
      routing: { location: { pathname: route } },
    })
    it('should return false if is not a checkout route', () => {
      expect(isInCheckout(getStateWithRoute())).toBe(false)
      expect(isInCheckout(getStateWithRoute(null))).toBe(false)
      expect(isInCheckout(getStateWithRoute({}))).toBe(false)
      expect(isInCheckout(getStateWithRoute('/login'))).toBe(false)
    })
    it('should return true if is a checkout route', () => {
      expect(isInCheckout(getStateWithRoute('/checkout/delivery'))).toBe(true)
    })
  })
  describe(isHomePage.name, () => {
    it('should return true', () => {
      expect(
        isHomePage({
          routing: {
            location: {
              pathname: '/',
            },
          },
        })
      ).toEqual(true)
    })
    it('should return false', () => {
      expect(
        isHomePage({
          routing: {
            location: {
              pathname: '/not-home-page',
            },
          },
        })
      ).toEqual(false)
    })
  })

  describe('selectInDeliveryAndPayment', () => {
    it('should return false if the path does not end with "/delivery-payment"', () => {
      const mockState = {
        routing: {
          location: {
            pathname: '/',
          },
        },
      }

      expect(selectInDeliveryAndPayment(mockState)).toBe(false)
    })

    it('should return false if the path is empty', () => {
      const mockState = {
        routing: {
          location: {
            pathname: '',
          },
        },
      }
      expect(selectInDeliveryAndPayment(mockState)).toBe(false)
    })

    it('should return false if the path state does not exist', () => {
      const mockState = {
        routing: {},
      }
      expect(selectInDeliveryAndPayment(mockState)).toBe(false)
    })

    it('should return true if the path ends with "delivery-payment"', () => {
      const mockState = {
        routing: {
          location: {
            pathname: '/delivery-payment',
          },
        },
      }
      expect(selectInDeliveryAndPayment(mockState)).toBe(true)
    })
  })

  describe('getRedirect', () => {
    it('selects redirect', () => {
      const redirect = 'some redirect'
      const state = {
        routing: {
          redirect,
        },
      }
      expect(getRedirect(state)).toBe(redirect)
    })
  })

  describe('selectHostname', () => {
    it('selects the hostname', () => {
      const hostname = 'm.mans.nothot.com'
      const state = {
        routing: {
          location: {
            hostname,
          },
        },
      }

      expect(selectHostname(state)).toBe(hostname)
    })
  })
})
