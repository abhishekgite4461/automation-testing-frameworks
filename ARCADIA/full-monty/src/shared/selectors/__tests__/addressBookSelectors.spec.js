import { orderSummaryUkStandard as orderSummaryWithNoSavedAddressesMock } from '../../../../test/mocks/orderSummary/uk-standard'
import orderSummaryWithSavedAddressesMock from '../../../../test/mocks/orderSummary/savedAddresses'
import { addressBookFormMock } from '../../../../test/mocks/forms/addressBookFormMock'
import {
  getSavedAddresses,
  getIsNewAddressFormVisible,
  getFormNames,
  getAddressBookForm,
  getDefaultCountry,
  getCountryFor,
  getIsFindAddressVisible,
  getHasFoundAddress,
  getHasSelectedAddress,
} from '../addressBookSelectors'

describe('addressBookSelectors', () => {
  describe('getSavedAddresses', () => {
    describe('when there is no account delivery address and no saved addresses', () => {
      const state = {
        checkout: {
          orderSummary: orderSummaryWithNoSavedAddressesMock,
        },
        account: {
          user: {
            deliveryDetails: {
              addressDetailsId: -1,
              nameAndPhone: {
                title: '',
                firstName: '',
                lastName: '',
                telephone: '',
              },
              address: {
                address1: '',
                address2: '',
                city: '',
                state: '',
                country: '',
                postcode: '',
              },
            },
          },
        },
      }
      it('should return an empty array', () => {
        const actual = getSavedAddresses(state)
        const expected = []
        expect(actual).toEqual(expected)
      })
    })
    describe('when there is account delivery address and no saved addresses', () => {
      const state = {
        checkout: {
          orderSummary: orderSummaryWithNoSavedAddressesMock,
        },
        account: {
          user: {
            deliveryDetails: {
              addressDetailsId: 2245363,
              nameAndPhone: {
                title: 'Mrs',
                firstName: 'new first name',
                lastName: 'Williams',
                telephone: '07971134030',
              },
              address: {
                address1: '7 Hannah Close',
                address2: 'Llanishen',
                city: 'CARDIFF',
                state: '',
                country: 'United Kingdom',
                postcode: 'se5 9hr',
              },
            },
          },
        },
      }
      it('should return the account delivery address', () => {
        const actual = getSavedAddresses(state)
        const expected = [
          {
            id: 2245363,
            addressName: 'CARDIFF, se5 9hr, United Kingdom',
            selected: true,
            address1: '7 Hannah Close',
            address2: 'Llanishen',
            city: 'CARDIFF',
            state: '',
            postcode: 'se5 9hr',
            country: 'United Kingdom',
            title: 'Mrs',
            firstName: 'new first name',
            lastName: 'Williams',
            telephone: '07971134030',
          },
        ]
        expect(actual).toEqual(expected)
      })
    })
    describe('when there are saved addresses', () => {
      const state = {
        checkout: {
          orderSummary: orderSummaryWithSavedAddressesMock,
        },
        account: {
          user: {
            deliveryDetails: {
              addressDetailsId: 2245363,
              nameAndPhone: {
                title: 'Mrs',
                firstName: 'new first name',
                lastName: 'Williams',
                telephone: '07971134030',
              },
              address: {
                address1: '7 Hannah Close',
                address2: 'Llanishen',
                city: 'CARDIFF',
                state: '',
                country: 'United Kingdom',
                postcode: 'se5 9hr',
              },
            },
          },
        },
      }
      it('should return the saved addresses', () => {
        const actual = getSavedAddresses(state)
        const expected = [
          {
            id: 843489,
            addressName: 'London, E3 2DS, United Kingdom',
            selected: true,
            address1: '2 Foo Road',
            address2: '',
            city: 'London',
            state: '',
            postcode: 'E3 2DS',
            country: 'United Kingdom',
            title: 'Ms',
            firstName: 'Jane',
            lastName: 'Doe',
            telephone: '01234 567890',
          },
        ]
        expect(actual).toEqual(expected)
      })
    })
  })
  describe('getIsNewAddressFormVisible', () => {
    const state = {
      addressBook: {
        isNewAddressFormVisible: true,
      },
    }
    it('should isNewAddressFormVisible', () => {
      const actual = getIsNewAddressFormVisible(state)
      const expected = true
      expect(actual).toEqual(expected)
    })
  })
  describe('getFormNames', () => {
    it('should return the newAddress form names', () => {
      const actual = getFormNames('newAddress')
      const expected = {
        address: 'newAddress',
        details: 'newDetails',
        findAddress: 'newFindAddress',
      }
      expect(actual).toEqual(expected)
    })
  })
  describe('getAddressBookForm', () => {
    const state = {
      forms: {
        addressBook: addressBookFormMock,
      },
    }
    it('should return the newAddress form', () => {
      const actual = getAddressBookForm('delivery', 'newAddress', state)
      const expected = addressBookFormMock.newAddress
      expect(actual).toEqual(expected)
    })
    it('should return the newDetails form', () => {
      const actual = getAddressBookForm('delivery', 'newDetails', state)
      const expected = addressBookFormMock.newDetails
      expect(actual).toEqual(expected)
    })
    it('should return the newFindaddress form', () => {
      const actual = getAddressBookForm('delivery', 'newFindAddress', state)
      const expected = addressBookFormMock.newFindAddress
      expect(actual).toEqual(expected)
    })
  })
  describe('getDefaultCountry', () => {
    it('should return the country from user deliveryDetails when is set', () => {
      const state = {
        account: {
          user: {
            deliveryDetails: {
              address: {
                country: 'United Kingdom',
              },
            },
          },
        },
        config: {
          country: 'Albania',
        },
      }
      const actual = getDefaultCountry(state)
      const expected = 'United Kingdom'
      expect(actual).toBe(expected)
    })
    it('should return the country from the brand config when the user deliveryDetails is not set', () => {
      const state = {
        config: {
          country: 'Albania',
        },
      }
      const actual = getDefaultCountry(state)
      const expected = 'Albania'
      expect(actual).toBe(expected)
    })
  })
  describe('getCountryFor', () => {
    it('should return the country from forms addressBook when set', () => {
      const state = {
        account: {
          user: {
            deliveryDetails: {
              address: {
                country: 'United Kingdom',
              },
            },
          },
        },
        forms: {
          addressBook: {
            newAddress: {
              fields: {
                country: {
                  value: 'United States',
                },
              },
            },
          },
        },
        config: {
          country: 'Albania',
        },
      }
      const actual = getCountryFor('delivery', 'newAddress', state)
      const expected = 'United States'
      expect(actual).toBe(expected)
    })
    it('should return the country from the account user deliveryDetails when forms addressBook is not set', () => {
      const state = {
        account: {
          user: {
            deliveryDetails: {
              address: {
                country: 'United Kingdom',
              },
            },
          },
        },
      }
      const actual = getCountryFor('delivery', 'newAddress', state)
      const expected = 'United Kingdom'
      expect(actual).toBe(expected)
    })
    it('should return the country from the brand config when the user deliveryDetails and the the addressBook is not set', () => {
      const state = {
        config: {
          country: 'Albania',
        },
      }
      const actual = getCountryFor('delivery', 'newAddress', state)
      const expected = 'Albania'
      expect(actual).toBe(expected)
    })
  })
  describe('getIsFindAddressVisible', () => {
    it('should return true when addressBookForm field isManual is false, the address1 field is falsey and the QASCountry code is populated', () => {
      const state = {
        forms: {
          addressBook: {
            newAddress: {
              fields: {
                isManual: {
                  value: false,
                },
                country: {
                  value: 'United Kingdom',
                },
                address1: {
                  value: '',
                },
              },
            },
          },
        },
        config: {
          qasCountries: {
            'United Kingdom': 'UK',
          },
        },
      }

      const actual = getIsFindAddressVisible(
        'delivery',
        'newAddress',
        'United Kingdom',
        state
      )
      const expected = true
      expect(actual).toBe(expected)
    })

    it('should return false when addressBookFrom field isManual is false, address1 field is truthy and QASCountry code is populated', () => {
      const state = {
        forms: {
          addressBook: {
            newAddress: {
              fields: {
                isManual: {
                  value: false,
                },
                country: {
                  value: 'United Kingdom',
                },
                address1: {
                  value: 'a truthy string',
                },
              },
            },
          },
        },
        config: {
          qasCountries: {
            'United Kingdom': 'UK',
          },
        },
      }

      const actual = getIsFindAddressVisible(
        'delivery',
        'newAddress',
        'United Kingdom',
        state
      )
      const expected = false
      expect(actual).toBe(expected)
    })

    it('should return false when addressBookForm field isManual is true, address1 field is falsey and QASCountry code is populated', () => {
      const state = {
        forms: {
          addressBook: {
            newAddress: {
              fields: {
                isManual: {
                  value: true,
                },
                country: {
                  value: 'United Kingdom',
                },
                address1: {
                  value: '',
                },
              },
            },
          },
        },
        config: {
          qasCountries: {
            'United Kingdom': 'UK',
          },
        },
      }

      const actual = getIsFindAddressVisible(
        'delivery',
        'newAddress',
        'United Kingdom',
        state
      )
      const expected = false
      expect(actual).toBe(expected)
    })

    it('should return false when isManual is true, address1 field is truthy and QASCountry code returns an empty string', () => {
      const state = {
        forms: {
          addressBook: {
            newAddress: {
              fields: {
                isManual: {
                  value: true,
                },
                country: {
                  value: 'United Kingdom',
                },
                address1: {
                  value: 'a truthy string',
                },
              },
            },
          },
        },
        config: {
          qasCountries: {},
        },
      }

      const actual = getIsFindAddressVisible(
        'delivery',
        'newAddress',
        'United Kingdom',
        state
      )
      const expected = false
      expect(actual).toBe(expected)
    })
  })

  describe('getHasFoundAddress', () => {
    it('should return true when the value of findAddress is truthy', () => {
      const findAddressForm = {
        fields: {
          findAddress: {
            value: 'something true',
          },
        },
      }

      const actual = getHasFoundAddress(findAddressForm)
      const expected = true
      expect(actual).toBe(expected)
    })

    it('should return false when the value of findAddress is falsey', () => {
      const findAddressForm = {
        fields: {
          findAddress: {
            value: '',
          },
        },
      }

      const actual = getHasFoundAddress(findAddressForm)
      const expected = false
      expect(actual).toBe(expected)
    })
  })

  describe('getHasSelectedAddress', () => {
    it('should return true when the value of address1 is truthy', () => {
      const addressForm = {
        fields: {
          address1: {
            value: 'something true',
          },
        },
      }

      const actual = getHasSelectedAddress(addressForm)
      const expected = true
      expect(actual).toBe(expected)
    })

    it('should return false when the value of address1 is falsey', () => {
      const addressForm = {
        fields: {
          address1: {
            value: '',
          },
        },
      }

      const actual = getHasSelectedAddress(addressForm)
      const expected = false
      expect(actual).toBe(expected)
    })
  })
})
