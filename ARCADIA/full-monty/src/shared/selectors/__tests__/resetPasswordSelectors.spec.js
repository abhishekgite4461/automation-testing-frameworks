import { getSuccess } from '../resetPasswordSelectors'

describe('form selectors', () => {
  const populatedState = {
    resetPassword: {
      success: true,
    },
  }

  describe(getSuccess.name, () => {
    it('returns the state', () => {
      expect(getSuccess(populatedState)).toBe(true)
    })
  })
})
