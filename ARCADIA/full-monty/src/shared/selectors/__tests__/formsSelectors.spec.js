import {
  getForgetPasswordForm,
  getSelectedPaymentType,
  getOrderFormErrorMessage,
  getResetPasswordForm,
  selectCheckoutForms,
  selectBillingCardPaymentTypeFromCheckoutForm,
} from '../formsSelectors'

describe('form selectors', () => {
  const populatedState = {
    forms: {
      checkout: {
        billingCardDetails: {
          fields: {
            paymentType: {
              value: 'VISA',
            },
            cardNumber: {
              value: '4444333322221111',
            },
            expiryMonth: {
              value: '06',
            },
            expiryYear: {
              value: '2017',
            },
            cvv: {
              value: '123',
            },
          },
        },
        order: {
          message: {
            message: 'An error occurred...',
          },
        },
      },
      forgetPassword: {},
      resetPassword: {},
    },
  }

  describe(getForgetPasswordForm.name, () => {
    it('returns the forgetPassword form state', () => {
      expect(getForgetPasswordForm(populatedState)).toBe(
        populatedState.forms.forgetPassword
      )
    })
  })

  describe(getSelectedPaymentType.name, () => {
    it('returns the current payment type if selected', () => {
      expect(getSelectedPaymentType(populatedState)).toBe('VISA')
    })
    it('returns null if there is no payment type selected', () => {
      expect(getSelectedPaymentType()).toBe(null)
    })
  })

  describe(getOrderFormErrorMessage.name, () => {
    it('returns the order form error message if it is present', () => {
      expect(getOrderFormErrorMessage(populatedState)).toBe(
        populatedState.forms.checkout.order.message.message
      )
    })

    it('returns undefined if it cant be reached', () => {
      expect(getOrderFormErrorMessage()).toBe(undefined)
    })
  })

  describe(getResetPasswordForm.name, () => {
    it('returns the resetPassword form state', () => {
      expect(getResetPasswordForm(populatedState)).toBe(
        populatedState.forms.resetPassword
      )
    })
  })

  describe(selectCheckoutForms.name, () => {
    it('returns the checkout forms state', () => {
      expect(selectCheckoutForms(populatedState)).toBe(
        populatedState.forms.checkout
      )
    })
  })

  describe(selectBillingCardPaymentTypeFromCheckoutForm.name, () => {
    expect(selectBillingCardPaymentTypeFromCheckoutForm(populatedState)).toBe(
      populatedState.forms.checkout.billingCardDetails.fields.paymentType.value
    )
  })
})
