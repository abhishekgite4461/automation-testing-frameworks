import {
  getCardIcons,
  getCombinedPaymentMethodByPaymentMethodValue,
  getPaymentMethodByOptionType,
  getPaymentMethodByValue,
  getPaymentMethodTypeByValue,
  getPaymentOptionByType,
  getPaymentOptionTypes,
  getSelectedPaymentOptionType,
  isPaymentMethodsLoaded,
  selectCombinedPaymentMethods,
  selectDecoratedCombinedPaymentMethods,
  selectPaymentMethodForStoredPaymentDetails,
  selectPaymentMethodsValidForDeliveryAndBillingCountry,
} from '../paymentMethodSelectors'

// mocks
import { paymentMethodsList } from '../../../../test/mocks/paymentMethodsMocks'

describe('payment method selectors', () => {
  describe(getPaymentOptionTypes.name, () => {
    it('returns an empty array when there are no payment methods', () => {
      const state = { paymentMethods: [] }
      const types = getPaymentOptionTypes(state)
      expect(types).toEqual([])
    })

    describe('when the payment method type is card', () => {
      it('returns one `CARD` entry to represent all the card types', () => {
        const state = {
          paymentMethods: [
            { type: 'CARD', value: 'VISA' },
            { type: 'CARD', value: 'AMEX' },
          ],
        }
        const types = getPaymentOptionTypes(state)
        expect(types).toEqual(['CARD'])
      })
    })

    describe('when the payment method type is not CARD', () => {
      it('plucks the payment method `value` prop instead of the `type` prop and returns it as part of the result', () => {
        const state = {
          paymentMethods: [
            { type: 'OTHER', value: 'ACCNT' },
            { type: 'OTHER', value: 'PYPAL' },
          ],
        }
        const types = getPaymentOptionTypes(state)
        expect(types).toEqual(['ACCNT', 'PYPAL'])
      })
    })
  })

  describe(getPaymentMethodByOptionType.name, () => {
    describe('when the type is `CARD`', () => {
      it('returns a standard card entry that contains all the payment methods', () => {
        const type = 'CARD'
        const state = {
          paymentMethods: [
            { type: 'CARD', label: 'Visa' },
            { type: 'CARD', label: 'MasterCard' },
          ],
        }
        const result = getPaymentMethodByOptionType(state, type)
        const expectedResult = {
          value: 'CARD',
          type: 'CARD',
          label: 'Debit and Credit Card',
          description: 'Visa, MasterCard',
          paymentTypes: [
            { type: 'CARD', label: 'Visa' },
            { type: 'CARD', label: 'MasterCard' },
          ],
        }
        expect(result).toEqual(expectedResult)
      })
    })

    describe('when the type is not `CARD`', () => {
      it('returns the payment method whose `value` prop is the same as the `type` argument', () => {
        const type = 'ACCNT'
        const state = {
          paymentMethods: [{ value: 'ACCNT' }, { type: 'CARD' }],
        }
        const result = getPaymentMethodByOptionType(state, type)
        expect(result).toEqual(state.paymentMethods[0])
      })
    })
  })

  describe(getCardIcons.name, () => {
    it('returns each card payment method icon', () => {
      const state = {
        paymentMethods: [
          { type: 'CARD', icon: 'icon1' },
          { type: 'CARD', icon: 'icon2' },
        ],
      }
      const result = getCardIcons(state)
      expect(result).toEqual(['icon1', 'icon2'])
    })
  })

  describe(getPaymentMethodByValue.name, () => {
    it('returns the payment method whose value is the same as the value supplied', () => {
      const value = 'PYPAL'
      const state = {
        paymentMethods: [{ value: 'ACCNT' }, { value: 'PYPAL' }],
      }
      const result = getPaymentMethodByValue(state, value)
      expect(result).toEqual(state.paymentMethods[1])
    })
  })

  describe(isPaymentMethodsLoaded.name, () => {
    it('returns true when there are payment methods', () => {
      const state = { paymentMethods: [1] }
      const result = isPaymentMethodsLoaded(state)
      expect(result).toBe(true)
    })

    it('returns false when there are no payment methods', () => {
      const state = { paymentMethods: [] }
      const result = isPaymentMethodsLoaded(state)
      expect(result).toBe(false)
    })
  })

  describe(getSelectedPaymentOptionType.name, () => {
    describe('when the selected payment option is of a card type', () => {
      it('it returns `CARD`', () => {
        const state = {
          paymentMethods: [
            { value: 'VISA', type: 'CARD' },
            { value: 'PYPAL', type: 'OTHER' },
          ],
          forms: {
            checkout: {
              billingCardDetails: {
                fields: {
                  paymentType: {
                    value: 'VISA',
                  },
                },
              },
            },
          },
        }
        const result = getSelectedPaymentOptionType(state)
        expect(result).toBe('CARD')
      })
    })

    describe('when the selected payment option is not a card type', () => {
      it('returns the value of the selected payment option', () => {
        const state = {
          paymentMethods: [
            { value: 'VISA', type: 'CARD' },
            { value: 'PYPAL', type: 'OTHER' },
          ],
          forms: {
            checkout: {
              billingCardDetails: {
                fields: {
                  paymentType: {
                    value: 'PYPAL',
                  },
                },
              },
            },
          },
        }
        const result = getSelectedPaymentOptionType(state)
        expect(result).toBe('PYPAL')
      })
    })
  })

  describe(getPaymentOptionByType.name, () => {
    describe('when the type is `CARD`', () => {
      it('returns a standard card entry', () => {
        const type = 'CARD'
        const state = {
          paymentMethods: [
            { type: 'CARD', label: 'Visa', icon: 'visa-icon' },
            { type: 'CARD', label: 'MasterCard', icon: 'mc-icon' },
            { type: 'OTHER' },
          ],
        }
        const result = getPaymentOptionByType(state, type)
        const expectedResult = {
          value: 'CARD',
          type: 'CARD',
          icons: ['visa-icon', 'mc-icon'],
          label: 'Debit and Credit Card',
          description: 'Visa, MasterCard',
          paymentTypes: [
            { type: 'CARD', label: 'Visa', icon: 'visa-icon' },
            { type: 'CARD', label: 'MasterCard', icon: 'mc-icon' },
          ],
        }
        expect(result).toEqual(expectedResult)
      })
    })

    describe('when the type is not `CARD`', () => {
      it('returns a standard non card entry', () => {
        const type = 'PYPAL'
        const state = {
          paymentMethods: [{ value: 'PYPAL', icon: 'paypal-icon' }],
        }
        const result = getPaymentOptionByType(state, type)
        const expectedResult = {
          value: 'PYPAL',
          icons: ['paypal-icon'],
        }
        expect(result).toEqual(expectedResult)
      })
    })
  })

  describe(getPaymentMethodTypeByValue.name, () => {
    describe('if value exists in paymentMethods', () => {
      it('return type CARD if value is VISA', () => {
        const value = 'VISA'
        const state = {
          paymentMethods: paymentMethodsList,
        }
        const result = getPaymentMethodTypeByValue(state, value)
        expect(result).toEqual('CARD')
      })
      it('return type CARD if value is MCARD', () => {
        const value = 'MCARD'
        const state = {
          paymentMethods: paymentMethodsList,
        }
        const result = getPaymentMethodTypeByValue(state, value)
        expect(result).toEqual('CARD')
      })
      it('return type CARD if value is AMEX', () => {
        const value = 'AMEX'
        const state = {
          paymentMethods: paymentMethodsList,
        }
        const result = getPaymentMethodTypeByValue(state, value)
        expect(result).toEqual('CARD')
      })
      it('return type CARD if value is SWTCH', () => {
        const value = 'SWTCH'
        const state = {
          paymentMethods: paymentMethodsList,
        }
        const result = getPaymentMethodTypeByValue(state, value)
        expect(result).toEqual('CARD')
      })
      it('return type CARD if value is SWTCH', () => {
        const value = 'SWTCH'
        const state = {
          paymentMethods: paymentMethodsList,
        }
        const result = getPaymentMethodTypeByValue(state, value)
        expect(result).toEqual('CARD')
      })
      it('return type OTHER_CARD if value is ACCNT', () => {
        const value = 'ACCNT'
        const state = {
          paymentMethods: paymentMethodsList,
        }
        const result = getPaymentMethodTypeByValue(state, value)
        expect(result).toEqual('OTHER_CARD')
      })
      it('return type OTHER if value is PYPAL', () => {
        const value = 'PYPAL'
        const state = {
          paymentMethods: paymentMethodsList,
        }
        const result = getPaymentMethodTypeByValue(state, value)
        expect(result).toEqual('OTHER')
      })
      it('return type OTHER if value is MPASS', () => {
        const value = 'MPASS'
        const state = {
          paymentMethods: paymentMethodsList,
        }
        const result = getPaymentMethodTypeByValue(state, value)
        expect(result).toEqual('OTHER')
      })
      it('return type OTHER if value is KLRNA', () => {
        const value = 'KLRNA'
        const state = {
          paymentMethods: paymentMethodsList,
        }
        const result = getPaymentMethodTypeByValue(state, value)
        expect(result).toEqual('OTHER')
      })
    })

    describe('returns by default', () => {
      it('should return CARD if state is {}', () => {
        const result = getPaymentMethodTypeByValue({}, null)
        expect(result).toEqual('CARD')
      })
      it('should return CARD if state is type is null', () => {
        const state = {
          paymentMethods: paymentMethodsList,
        }
        const result = getPaymentMethodTypeByValue(state, null)
        expect(result).toEqual('CARD')
      })
      it('should return CARD if value does not exists in paymentMethods', () => {
        const value = 'NOVALUE'
        const state = {
          paymentMethods: paymentMethodsList,
        }
        const result = getPaymentMethodTypeByValue(state, value)
        expect(result).toEqual('CARD')
      })
      it('should return OTHER as default if specified', () => {
        const value = 'NOVALUE'
        const state = {
          paymentMethods: paymentMethodsList,
        }
        const result = getPaymentMethodTypeByValue(state, value, 'OTHER')
        expect(result).toEqual('OTHER')
      })
    })
  })

  describe('selectPaymentMethodForStoredPaymentDetails', () => {
    const state = {
      account: {
        user: {
          creditCard: {
            type: 'VISA',
          },
        },
      },
      paymentMethods: [
        {
          label: 'Visa',
          value: 'VISA',
          icon: 'visa.svg',
        },
      ],
    }

    it('should extract creditCard from user', () => {
      expect(
        selectPaymentMethodForStoredPaymentDetails(state)
      ).toMatchSnapshot()
    })

    it('should return credit/debit visa card when not payment methods list', () => {
      expect(
        selectPaymentMethodForStoredPaymentDetails({
          ...state,
          paymentMethods: [],
        })
      ).toEqual({})
    })

    it('should return empty when account/user/creditcard state is empty', () => {
      expect(
        selectPaymentMethodForStoredPaymentDetails({
          account: { user: { creditcard: {} } },
        })
      ).toEqual({})
    })
  })

  describe('selectCombinedPaymentMethods', () => {
    it('should return the combined payment methods (cards combined as one payment method)', () => {
      const mockState = {
        paymentMethods: [
          paymentMethodsList[0],
          paymentMethodsList[2],
          paymentMethodsList[5],
        ],
      }

      expect(selectCombinedPaymentMethods(mockState)).toEqual([
        {
          description: 'Visa, American Express',
          label: 'Debit and Credit Card',
          paymentTypes: [paymentMethodsList[0], paymentMethodsList[2]],
          type: 'CARD',
          value: 'CARD',
        },
        {
          description: 'Check out with your PayPal account',
          label: 'Paypal',
          type: 'OTHER',
          value: 'PYPAL',
          icon: 'icon_paypal.gif',
        },
      ])
    })
  })

  describe('selectDecoratedCombinedPaymentMethods', () => {
    let mockState

    beforeEach(() => {
      mockState = {
        paymentMethods: [
          paymentMethodsList[0],
          paymentMethodsList[2],
          paymentMethodsList[5],
        ],
      }
    })

    it('should return combinedPaymentMethods with the icons mapped to an array for each payment type', () => {
      expect(selectDecoratedCombinedPaymentMethods(mockState)).toEqual([
        {
          description: 'Visa, American Express',
          label: 'Debit and Credit Card',
          paymentTypes: [paymentMethodsList[0], paymentMethodsList[2]],
          type: 'CARD',
          value: 'CARD',
          icons: ['icon_visa.gif', 'icon_amex.gif'],
        },
        {
          description: 'Check out with your PayPal account',
          label: 'Paypal',
          paymentTypes: [],
          type: 'OTHER',
          value: 'PYPAL',
          icons: ['icon_paypal.gif'],
        },
      ])
    })
  })

  describe('getCombinedPaymentMethodByPaymentMethodValue', () => {
    let combinedPaymentMethods
    beforeEach(() => {
      combinedPaymentMethods = [
        {
          description: 'Visa, American Express',
          label: 'Debit and Credit Card',
          paymentTypes: [paymentMethodsList[0], paymentMethodsList[2]],
          type: 'CARD',
          value: 'CARD',
        },
        {
          description: 'Check out with your PayPal account',
          label: 'Paypal',
          type: 'OTHER',
          value: 'PYPAL',
          icon: 'icon_paypal.gif',
        },
      ]
    })
    it('should return a combined payment method if a non card payment method value property is provided', () => {
      expect(
        getCombinedPaymentMethodByPaymentMethodValue(
          combinedPaymentMethods,
          'VISA',
          undefined
        )
      ).toBe(combinedPaymentMethods[0])
    })
    it('should return the card combined payment method if a card payment method value property is provided', () => {
      expect(
        getCombinedPaymentMethodByPaymentMethodValue(
          combinedPaymentMethods,
          'PYPAL',
          undefined
        )
      ).toBe(combinedPaymentMethods[1])
    })
    it('should default to the supplied default value if no combined payment method can be found', () => {
      expect(
        getCombinedPaymentMethodByPaymentMethodValue(
          combinedPaymentMethods,
          'NON_EXISTENT',
          []
        )
      ).toEqual([])
    })
  })

  describe('selectPaymentMethodsValidForDeliveryAndBillingCountry', () => {
    it('should not filter out payment methods that do not define a billing country or delivery country', () => {
      const state = {
        config: {
          country: 'United Kingdom',
        },
        paymentMethods: [paymentMethodsList[0]],
      }

      expect(
        selectPaymentMethodsValidForDeliveryAndBillingCountry(state)
      ).toEqual([paymentMethodsList[0]])
    })

    it('should not filter out payment methods that match both billing country and delivery country', () => {
      const state = {
        config: {
          country: 'United Kingdom',
        },
        paymentMethods: [paymentMethodsList[7]],
      }

      expect(
        selectPaymentMethodsValidForDeliveryAndBillingCountry(state)
      ).toEqual([paymentMethodsList[7]])
    })

    it('should not filter out payment methods that match one or more billing countries', () => {
      const state = {
        config: {
          country: 'Germany',
        },
        paymentMethods: [paymentMethodsList[10]],
      }

      expect(
        selectPaymentMethodsValidForDeliveryAndBillingCountry(state)
      ).toEqual([paymentMethodsList[10]])
    })

    it('should filter out payment methods that are for the incorrect billing country', () => {
      const state = {
        config: {
          country: 'United Kingdom',
        },
        paymentMethods: [paymentMethodsList[0], paymentMethodsList[8]],
      }
      expect(
        selectPaymentMethodsValidForDeliveryAndBillingCountry(state)
      ).toEqual([paymentMethodsList[0]])
    })

    it('should filter out payment methods that are for the incorrect delivery country', () => {
      const hypotheticalPaymentMethod = {
        ...paymentMethodsList[1],
        deliveryCountry: ['Egypt'],
      }
      const state = {
        config: {
          country: 'United Kingdom',
        },
        paymentMethods: [paymentMethodsList[0], hypotheticalPaymentMethod],
      }
      expect(
        selectPaymentMethodsValidForDeliveryAndBillingCountry(state)
      ).toEqual([paymentMethodsList[0]])
    })
  })
})
