import deepFreeze from 'deep-freeze'

import * as ConfigSelectors from '../configSelectors'

describe('config selectors', () => {
  const state = deepFreeze({
    config: {
      brandCode: 'TS',
      currencySymbol: '£',
      currencyCode: 'GBP',
      brandName: 'brand name',
      brandDisplayName: 'Brand Name',
      langHostnames: {
        default: {
          defaultLanguage: 'English',
        },
      },
      region: 'uk',
      siteId: 'VALID_SITE_ID',
      storeCode: 'tsuk',
      lang: 'en',
      sizeTileMaximumDesktop: 7,
    },
  })

  describe('selectConfig', () => {
    it('returns config state', () => {
      expect(ConfigSelectors.selectConfig(state)).toBe(state.config)
    })
  })

  describe('getCurrencyCode', () => {
    it('returns currency code', () => {
      expect(ConfigSelectors.getCurrencyCode(state)).toBe(
        state.config.currencyCode
      )
    })
  })

  describe('getBrandDisplayName', () => {
    it('returns brand display name', () => {
      expect(ConfigSelectors.getBrandDisplayName(state)).toBe(
        state.config.brandDisplayName
      )
    })
  })

  describe('getCurrencySymbol', () => {
    it('returns currency symbol', () => {
      expect(ConfigSelectors.getCurrencySymbol(state)).toBe(
        state.config.currencySymbol
      )
    })
  })

  describe('getBrandCode', () => {
    it('returns brandCode', () => {
      expect(ConfigSelectors.getBrandCode(state)).toBe(state.config.brandCode)
    })
  })

  describe('getLangHostnames', () => {
    it('returns langHostnames', () => {
      expect(ConfigSelectors.getLangHostnames(state)).toEqual(
        state.config.langHostnames
      )
    })
  })

  describe('getLanguage', () => {
    it('returns lang', () => {
      expect(ConfigSelectors.getLanguage(state)).toEqual(state.config.lang)
    })
  })

  describe('getDefaultLanguage', () => {
    it('returns default language', () => {
      expect(ConfigSelectors.getDefaultLanguage(state)).toBe(
        state.config.langHostnames.default.defaultLanguage
      )
    })
  })

  describe('getStoreId', () => {
    it('returns storeId', () => {
      expect(ConfigSelectors.getStoreId(state)).toBe(state.config.siteId)
    })
  })

  describe('getBaseUrlPath', () => {
    it('returns baseUrlPath', () => {
      expect(ConfigSelectors.getBaseUrlPath(state)).toBe(
        `/${state.config.storeCode}/${state.config.lang}`
      )
    })
  })

  describe('getMaximumNumberOfSizeTiles', () => {
    describe('with mobile viewport', () => {
      it('returns MOBILE_MAXIMUM_NUMBER_SIZE_TILE in mobile viewport', () => {
        expect(
          ConfigSelectors.getMaximumNumberOfSizeTiles({
            ...state,
            viewport: {
              media: 'mobile',
            },
          })
        ).toBe(8)
      })
    })
    describe('with viewport different than mobile', () => {
      it('returns config.sizeTileMaximumDesktop value', () => {
        expect(ConfigSelectors.getMaximumNumberOfSizeTiles(state)).toBe(7)
      })
      it('returns DEFAULT_DESKTOP_MAXIMUM_NUMBER_SIZE_TILE', () => {
        expect(
          ConfigSelectors.getMaximumNumberOfSizeTiles({ config: {} })
        ).toBe(8)
      })
    })
  })
})
