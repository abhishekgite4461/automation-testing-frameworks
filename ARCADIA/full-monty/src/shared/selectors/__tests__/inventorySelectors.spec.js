import { getActiveProductWithInventory } from '../inventorySelectors'

describe('ProductInventory Selectors', () => {
  describe('#getActiveProductWithInventory(state)', () => {
    it('should return null if invalid state', () => {
      expect(getActiveProductWithInventory()).toEqual(null)
      expect(getActiveProductWithInventory({})).toEqual(null)
      expect(getActiveProductWithInventory(null)).toEqual(null)
      expect(getActiveProductWithInventory(undefined)).toEqual(null)
    })
    it('should return null if productDetails or currentProduct are not populated', () => {
      expect(
        getActiveProductWithInventory({
          currentProduct: {},
          productDetail: {},
        })
      ).toEqual(null)
      expect(
        getActiveProductWithInventory({
          currentProduct: null,
          productDetail: {},
        })
      ).toEqual(null)
      expect(
        getActiveProductWithInventory({
          currentProduct: undefined,
          productDetail: {},
        })
      ).toEqual(null)
    })
    it('should return null if there is no active item populated', () => {
      expect(
        getActiveProductWithInventory({
          currentProduct: {},
          productDetail: { activeItem: {} },
        })
      ).toEqual(null)
    })
    it('should return null if catEntryId can not be found', () => {
      expect(
        getActiveProductWithInventory({
          currentProduct: {
            productDataQuantity: {
              SKUs: [{ skuid: '123', partnumber: '00006' }],
            },
          },
          productDetail: {
            activeItem: { sku: '00007' },
          },
        })
      ).toEqual(null)
    })
    it('should map product with its inventory positions', () => {
      const currentProduct = {
        productId: 29931446,
        name: 'Wide Fit Taupe Mallory Boots',
        productDataQuantity: {
          inventoryPositions: [
            { catentryId: '29931450', inventorys: [{}] },
            { catentryId: '29931452', inventorys: [{}], invavls: [{}] },
          ],
          SKUs: [
            { skuid: '29931450', value: '3', partnumber: '262017000922318' },
            { skuid: '29931452', value: '4', partnumber: '262017000922319' },
          ],
        },
      }
      const productDetail = {
        activeItem: { sku: '262017000922319', size: '4' },
        selectedQuantity: 1,
      }

      expect(
        getActiveProductWithInventory({ currentProduct, productDetail })
      ).toMatchSnapshot()
    })
    describe('selector recomputations', () => {
      beforeEach(() => {
        getActiveProductWithInventory()
        getActiveProductWithInventory.resetRecomputations()
      })
      const state = {
        currentProduct: {
          productDataQuantity: {
            SKUs: [{ skuid: '123', partnumber: '00006' }],
          },
        },
        productDetail: {
          activeItem: { sku: '00007' },
        },
      }
      it('should recompute on state change', () => {
        getActiveProductWithInventory(state)
        getActiveProductWithInventory({})
        expect(getActiveProductWithInventory.recomputations()).toBe(2)
      })
      it('should not recompute if currentProduct and productDetail did not change', () => {
        getActiveProductWithInventory(state)
        getActiveProductWithInventory(state)
        getActiveProductWithInventory({ ...state })
        expect(getActiveProductWithInventory.recomputations()).toBe(1)
      })
    })
  })

  describe('#getShoppingBagProductsWithInventory(state)', () => {
    beforeEach(jest.resetModules)

    it('calls getBasketProductsWithInventory', () => {
      jest.doMock('../../lib/ffs/product-inventory-utilities', () => ({
        getBasketProductsWithInventory: jest.fn(),
      }))
      const utils = require('../../lib/ffs/product-inventory-utilities')
      const selectors = require('../inventorySelectors')

      const state = {
        shoppingBag: { bag: ['mocked bag'] },
      }
      selectors.getShoppingBagProductsWithInventory(state)
      expect(utils.getBasketProductsWithInventory).toHaveBeenCalledTimes(1)
      expect(utils.getBasketProductsWithInventory).toHaveBeenCalledWith([
        'mocked bag',
      ])
    })
  })
})
