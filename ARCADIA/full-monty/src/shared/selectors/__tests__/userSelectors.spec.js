import * as userSelectors from '../userSelectors'

describe(userSelectors.getUserDefaultPaymentType.name, () => {
  it('returns the default payment type if available', () => {
    const state = {
      account: {
        user: {
          creditCard: {
            type: 'RANDOM',
          },
        },
      },
    }
    expect(userSelectors.getUserDefaultPaymentType(state)).toEqual('RANDOM')
  })
  it('returns null if there is no default payment type', () => {
    const state = {}
    expect(userSelectors.getUserDefaultPaymentType(state)).toEqual(null)
  })
})

describe(userSelectors.isKlarnaDefaultPaymentType.name, () => {
  it('returns true if the default payment type is Klarna', () => {
    const state = {
      account: {
        user: {
          creditCard: {
            type: 'KLRNA',
          },
        },
      },
    }
    expect(userSelectors.isKlarnaDefaultPaymentType(state)).toEqual(true)
  })
  it('returns true if the default payment type is Klarna', () => {
    const state = {}
    expect(userSelectors.isKlarnaDefaultPaymentType(state)).toEqual(false)
  })
})

describe(userSelectors.isLoggedIn.name, () => {
  const stateNotLogged = {
    account: {
      user: {},
    },
  }
  const stateLogged = {
    account: {
      user: {
        exists: true,
      },
    },
  }
  it('return false if user not logged in', () => {
    expect(userSelectors.isLoggedIn(stateNotLogged)).toEqual(false)
  })
  it('return true if user logged in (exist: true)', () => {
    expect(userSelectors.isLoggedIn(stateLogged)).toEqual(true)
  })
})

describe(userSelectors.getUserAddress, () => {
  const state = {
    deliveryDetails: {
      address: {
        country: 'United Kingdom',
      },
    },
    billingDetails: {
      address: {
        country: 'United Kingdom',
      },
    },
  }

  it('return billing and delivery details from user ', () => {
    expect(userSelectors.getUserAddressCountry(state, 'deliveryDetails')).toBe(
      'United Kingdom'
    )
  })
})

describe(userSelectors.isUserAuthenticated.name, () => {
  it('returns false if user is not authenticated', () => {
    expect(
      userSelectors.isUserAuthenticated({
        auth: {
          authentication: false,
        },
      })
    ).toBe(false)
  })
})

describe(userSelectors.isUserAuthenticated.name, () => {
  it('returns true if user is not authenticated', () => {
    expect(
      userSelectors.isUserAuthenticated({
        auth: {
          authentication: true,
        },
      })
    ).toBe(true)
  })
})
