import {
  getFeaturesStatus,
  isFeatureCFSIEnabled,
  isFeatureShowCFSIEspotEnabled,
  isFeaturePUDOEnabled,
  isFeatureSavePaymentDetailsEnabled,
  isFeatureTransferBasketEnabled,
  isFeatureWishlistEnabled,
  isFeatureMyCheckoutDetailsEnabled,
  isFeatureAddressBookEnabled,
  isFeaturePLPRedirectIfOneProductEnabled,
  isFeatureQubitHiddenEnabled,
  isFeatureCarouselThumbnailEnabled,
  isFeatureDesktopResetPasswordEnabled,
  isFeatureResponsiveProductViewEnabled,
  isFeatureStickyHeaderEnabled,
  isAmplienceFeatureEnabled,
  isFeatureDDPEnabled,
  isFeatureDDPPromoEnabled,
  isFeatureHomePageSegmentationEnabled,
  isFeatureEnhancedMessagingEnabled,
} from '../featureSelectors'

describe('Feature selectors', () => {
  const selectors = {
    FEATURE_CFSI: isFeatureCFSIEnabled,
    FEATURE_SHOW_CFSI_PDP_ESPOT: isFeatureShowCFSIEspotEnabled,
    FEATURE_PUDO: isFeaturePUDOEnabled,
    FEATURE_SAVE_PAYMENT_DETAILS: isFeatureSavePaymentDetailsEnabled,
    FEATURE_TRANSFER_BASKET: isFeatureTransferBasketEnabled,
    FEATURE_WISHLIST: isFeatureWishlistEnabled,
    FEATURE_MY_CHECKOUT_DETAILS: isFeatureMyCheckoutDetailsEnabled,
    FEATURE_ADDRESS_BOOK: isFeatureAddressBookEnabled,
    FEATURE_PLP_REDIRECT_IF_ONE_PRODUCT: isFeaturePLPRedirectIfOneProductEnabled,
    FEATURE_QUBIT_HIDDEN: isFeatureQubitHiddenEnabled,
    FEATURE_PRODUCT_CAROUSEL_THUMBNAIL: isFeatureCarouselThumbnailEnabled,
    FEATURE_DESKTOP_RESET_PASSWORD: isFeatureDesktopResetPasswordEnabled,
    FEATURE_RESPONSIVE_PRODUCT_VIEW: isFeatureResponsiveProductViewEnabled,
    FEATURE_STICKY_HEADER: isFeatureStickyHeaderEnabled,
    FEATURE_USE_AMPLIENCE: isAmplienceFeatureEnabled,
    FEATURE_DDP: isFeatureDDPEnabled,
    FEATURE_DISPLAY_DDP_PROMOTION: isFeatureDDPPromoEnabled,
    FEATURE_HOME_PAGE_SEGMENTATION: isFeatureHomePageSegmentationEnabled,
    FEATURE_ENHANCED_MESSAGING: isFeatureEnhancedMessagingEnabled,
  }

  const getState = (feature, value) => ({
    features: {
      status: {
        [feature]: value,
      },
    },
  })

  describe('getFeaturesStatus()', () => {
    it('should return the full features status object', () => {
      const mockStatus = Object.freeze({ foo: 'bar' })
      expect(getFeaturesStatus({ features: { status: mockStatus } })).toBe(
        mockStatus
      )
    })
  })

  Object.entries(selectors).forEach(([feature, selector]) => {
    describe(feature, () => {
      it('should return true', () => {
        expect(selector(getState(feature, true))).toBe(true)
      })
      it('should return false', () => {
        expect(selector(getState(feature, false))).toBe(false)
      })
    })
  })
})
