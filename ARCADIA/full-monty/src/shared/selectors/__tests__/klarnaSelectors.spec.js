/* eslint-disable object-curly-spacing,quote-props  */
import {
  getAddressPayload,
  calculateOrderSummaryHash,
  isCheckEligibilityButtonVisible,
  isKlarnaSessionOutdated,
} from '../klarnaSelectors'

beforeEach(() => {
  jest.resetModules()
})

describe(isCheckEligibilityButtonVisible.name, () => {
  it('returns true if shown flag is true and the Klarna auth token is not valid', () => {
    const state = {
      klarna: {
        authorizationToken: undefined,
        shown: true,
      },
    }
    expect(isCheckEligibilityButtonVisible(state)).toEqual(true)
  })
  it('returns false if the Klarna auth token is valid', () => {
    const state = {
      klarna: {
        authorizationToken: 'ValidToken',
        shown: true,
      },
    }
    expect(isCheckEligibilityButtonVisible(state)).toEqual(false)
  })
  it('returns false if the shown flag is false', () => {
    const state = {
      klarna: {
        authorizationToken: 'ValidToken',
        shown: false,
      },
    }
    expect(isCheckEligibilityButtonVisible(state)).toEqual(false)
  })
})

describe(getAddressPayload.name, () => {
  it('calls assembleAddressPayload with the correct props', () => {
    jest.doMock('./../../lib/checkout-utilities/klarna-utils', () => ({
      assembleAddressPayload: jest.fn(() => 'MOCKED'),
    }))
    const klarnaUtils = require('./../../lib/checkout-utilities/klarna-utils')
    const klarnaSelectors = require('../klarnaSelectors')

    const state = {
      checkout: {
        orderSummary: { PROP_A: 'VALUE_A' },
      },
      forms: {
        checkout: { PROP_B: 'VALUE_B' },
        klarna: { PROP_C: 'VALUE_C' },
      },
      account: {
        user: { PROP_D: 'VALUE_D' },
      },
      config: { PROP_E: 'VALUE_E' },
    }
    klarnaSelectors.getAddressPayload(state)
    expect(klarnaUtils.assembleAddressPayload).toHaveBeenCalledWith({
      PROP_B: 'VALUE_B',
      config: { PROP_E: 'VALUE_E' },
      klarnaForm: { PROP_C: 'VALUE_C' },
      orderSummary: { PROP_A: 'VALUE_A' },
      user: { PROP_D: 'VALUE_D' },
    })
  })
})

describe(isKlarnaSessionOutdated.name, () => {
  const state = {
    forms: {
      checkout: { PROP_B: 'VALUE_B' },
    },
    checkout: {
      orderSummary: { PROP_C: 'VALUE_C' },
    },
    shoppingBag: {
      PROP_D: 'VALUE_D',
    },
  }
  const processBrowser = global.process.browser
  beforeEach(() => {
    jest.doMock('./../../lib/checkout-utilities/klarna-utils', () => ({
      hashOrderSummary: jest.fn(() => 'HASH_1'),
    }))
    global.process.browser = true
  })
  afterEach(() => {
    global.process.browser = processBrowser
  })
  it('returns true when the saved hash and the calculated orderSummary hash are different', () => {
    const klarnaUtils = require('./../../lib/checkout-utilities/klarna-utils')
    const klarnaSelectors = require('../klarnaSelectors')

    expect(
      klarnaSelectors.isKlarnaSessionOutdated({
        ...state,
        klarna: {
          orderSummaryHash: 'HASH_OUTDATED',
        },
      })
    ).toEqual(true)
    expect(klarnaUtils.hashOrderSummary).toHaveBeenCalledWith({
      PROP_B: 'VALUE_B',
      orderSummary: { PROP_C: 'VALUE_C' },
      shoppingBag: { PROP_D: 'VALUE_D' },
    })
  })
  it('returns false when the saved hash and the calculated orderSummary hash are equal', () => {
    const klarnaUtils = require('./../../lib/checkout-utilities/klarna-utils')
    const klarnaSelectors = require('../klarnaSelectors')
    expect(
      klarnaSelectors.isKlarnaSessionOutdated({
        ...state,
        klarna: {
          orderSummaryHash: 'HASH_1',
        },
      })
    ).toEqual(false)
    expect(klarnaUtils.hashOrderSummary).toHaveBeenCalledWith({
      PROP_B: 'VALUE_B',
      orderSummary: { PROP_C: 'VALUE_C' },
      shoppingBag: { PROP_D: 'VALUE_D' },
    })
  })
  it('get the order summary hash from the local storage if missing in the state', () => {
    const klarnaUtils = require('./../../lib/checkout-utilities/klarna-utils')
    const klarnaSelectors = require('../klarnaSelectors')
    global.window.localStorage = {
      getItem: () => null,
    }

    expect(
      klarnaSelectors.isKlarnaSessionOutdated({
        ...state,
        klarna: {
          orderSummaryHash: undefined,
        },
      })
    ).toEqual(false)
    expect(klarnaUtils.hashOrderSummary).toHaveBeenCalledWith({
      PROP_B: 'VALUE_B',
      orderSummary: { PROP_C: 'VALUE_C' },
      shoppingBag: { PROP_D: 'VALUE_D' },
    })
  })
})

describe(calculateOrderSummaryHash.name, () => {
  it('calls assembleAddressPayload with the correct props', () => {
    jest.doMock('./../../lib/checkout-utilities/klarna-utils', () => ({
      hashOrderSummary: jest.fn(() => 'HASH_1'),
    }))
    const klarnaUtils = require('./../../lib/checkout-utilities/klarna-utils')
    const klarnaSelectors = require('../klarnaSelectors')

    const state = {
      forms: {
        checkout: { PROP_B: 'VALUE_B' },
      },
      checkout: {
        orderSummary: { PROP_C: 'VALUE_C' },
      },
      shoppingBag: {
        PROP_D: 'VALUE_D',
      },
    }
    expect(klarnaSelectors.calculateOrderSummaryHash(state)).toEqual('HASH_1')
    expect(klarnaUtils.hashOrderSummary).toHaveBeenCalledWith({
      PROP_B: 'VALUE_B',
      orderSummary: { PROP_C: 'VALUE_C' },
      shoppingBag: { PROP_D: 'VALUE_D' },
    })
  })
})
