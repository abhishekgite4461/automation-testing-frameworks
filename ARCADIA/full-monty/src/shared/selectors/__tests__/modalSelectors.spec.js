import { getScrollPositionOnOpenModal, isModalOpen } from '../modalSelectors'

describe(isModalOpen.name, () => {
  it('returns true when the value is true', () => {
    const state = { modal: { open: true } }
    expect(isModalOpen(state)).toEqual(true)
  })
  it('returns false when the value is false', () => {
    const state = { modal: { open: false } }
    expect(isModalOpen(state)).toEqual(false)
  })
})

describe(getScrollPositionOnOpenModal, () => {
  it('returns the latest scroll position when valid', () => {
    const state = { modal: { scrollPositionOnOpenModal: 111 } }
    expect(getScrollPositionOnOpenModal(state)).toEqual(111)
  })
  it('returns null when latest scroll position is not defined', () => {
    const state = { modal: { scrollPositionOnOpenModal: null } }
    expect(getScrollPositionOnOpenModal(state)).toEqual(null)
  })
})
