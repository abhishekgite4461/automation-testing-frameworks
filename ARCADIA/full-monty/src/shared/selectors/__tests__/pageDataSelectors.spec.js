import deepFreeze from 'deep-freeze'
import { getMetaDescription, getPageTitle } from '../pageDataSelectors'

describe('Page data selectors', () => {
  const state = deepFreeze({
    pageData: {
      description: 'meta description',
      title: 'page title',
    },
  })

  describe('getMetaDescription', () => {
    it('should return meta description', () => {
      expect(getMetaDescription(state)).toBe(state.pageData.description)
    })
  })

  describe('getPageTitle', () => {
    it('should return page title', () => {
      expect(getPageTitle(state)).toBe(state.pageData.title)
    })
  })
})
