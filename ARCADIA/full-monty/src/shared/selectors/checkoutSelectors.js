import { createSelector } from 'reselect'
import {
  compose,
  defaultTo,
  equals,
  find,
  filter,
  identity,
  isNil,
  isEmpty,
  map,
  path,
  pathOr,
  pick,
  pluck,
  propOr,
  reject,
  without,
} from 'ramda'

import {
  findSelected,
  isStoreDeliveryLocation,
  isStoreOrParcelDeliveryLocation,
  getDeliveryTypeFromBasket,
  getDefaultDeliveryType,
  matchesParcelShopLabel,
} from '../lib/checkout-utilities/delivery-options-utils'
import {
  deliveryDays,
  hasParcelShopDeliveryDay,
} from '../lib/get-basket-availability'
import { normaliseEstimatedDeliveryDate } from '../lib/checkout-utilities/order-summary'
import { isDeliveryOptionDisabled } from '../../shared/lib/is-delivery-disabled'
import { isFeatureCFSIEnabled, isFeaturePUDOEnabled } from './featureSelectors'
import { getFormNames } from '../lib/checkout-utilities/utils'
import { selectedDeliveryLocation } from '../lib/checkout-utilities/reshaper'
import { fixTotal } from '../lib/checkout'
import { normalizePrice } from '../lib/price'
import { getSelectedBrandFulfilmentStore } from '../reducers/common/selectedBrandFulfilmentStore'
import { isInCheckout } from './routingSelectors'

const rootSelector = (state) => state.checkout || {}

export const getCheckoutOrderSummary = createSelector(
  rootSelector,
  (checkout) => checkout.orderSummary || {}
)

export const getCheckoutOrderSummaryBasket = createSelector(
  getCheckoutOrderSummary,
  (orderSummary) => orderSummary.basket || {}
)

export const getCheckoutAmount = createSelector(
  getCheckoutOrderSummaryBasket,
  (basket) => basket.total
)

export const getCheckoutOrderSummaryProducts = createSelector(
  getCheckoutOrderSummaryBasket,
  (basket) => basket.products || []
)

export const getCheckoutOrderSummaryPromotions = createSelector(
  getCheckoutOrderSummaryBasket,
  (basket) => basket.promotions || []
)

export const getCheckoutOrderSummaryShippingCountry = createSelector(
  getCheckoutOrderSummary,
  (orderSummary) => orderSummary.shippingCountry
)

export const getCheckoutOrderSummaryDeliveryDetails = createSelector(
  getCheckoutOrderSummary,
  (orderSummary) => orderSummary.deliveryDetails || {}
)

export const getCheckoutPartialOrderSummary = createSelector(
  rootSelector,
  (checkout) => checkout.partialOrderSummary || {}
)

export const getCheckoutPartialOrderSummaryBasket = createSelector(
  getCheckoutPartialOrderSummary,
  (partialOrderSummary) => partialOrderSummary.basket || {}
)

export const getCheckoutPartialOrderSummaryShippingCountry = createSelector(
  getCheckoutPartialOrderSummary,
  (partialOrderSummary) => partialOrderSummary.shippingCountry
)

export const getCheckoutPartialOrderSummaryProducts = createSelector(
  getCheckoutPartialOrderSummaryBasket,
  (basket) => basket.products || []
)

export const getCheckoutOrderCompleted = createSelector(
  rootSelector,
  (checkout) => checkout.orderCompleted || {}
)

export const getCheckoutOrderLines = createSelector(
  getCheckoutOrderCompleted,
  (orderCompleted) => orderCompleted.orderLines
)

export const getCheckoutOrderId = createSelector(
  getCheckoutOrderCompleted,
  (orderCompleted) => orderCompleted.orderId
)

export const getCheckoutOrderError = createSelector(
  rootSelector,
  (checkout) => checkout.orderError
)

export const getCheckoutDeliveryStore = createSelector(
  rootSelector,
  (checkout) => checkout.deliveryStore
)

export const getCheckoutOrderCompletedDeliveryStoreCode = createSelector(
  getCheckoutOrderCompleted,
  (orderCompleted) => orderCompleted.deliveryStoreCode
)

export const getCheckoutOrderCountry = createSelector(
  getCheckoutOrderSummaryShippingCountry,
  getCheckoutOrderSummaryDeliveryDetails,
  getCheckoutPartialOrderSummaryShippingCountry,
  (
    orderSummaryShippingCountry,
    checkoutOrderSummaryDeliveryDetails,
    checkoutPartialOrderSummaryShippingCountry
  ) =>
    path(['address', 'country'], checkoutOrderSummaryDeliveryDetails) ||
    checkoutOrderSummaryDeliveryDetails.country ||
    checkoutPartialOrderSummaryShippingCountry ||
    orderSummaryShippingCountry
)

// TODO: convert following selector using reselect (live above selectors) when touched

function getDeliveryLocations(state) {
  const isPUDOEnabled = isFeaturePUDOEnabled(state)
  const deliveryLocations = pathOr(
    [],
    ['checkout', 'orderSummary', 'deliveryLocations'],
    state
  )
  return deliveryLocations.filter((location) => {
    return (isPUDOEnabled && location.deliveryLocationType === 'PARCELSHOP') ||
      location.deliveryLocationType !== 'PARCELSHOP'
      ? location
      : undefined
  })
}

const getSelectedDeliveryLocation = compose(
  defaultTo(null),
  findSelected,
  getDeliveryLocations
)

function getSelectedDeliveryLocationType(state) {
  const selectedDeliveryLocation = getSelectedDeliveryLocation(state)
  return propOr(null, 'deliveryLocationType', selectedDeliveryLocation)
}

function selectedDeliveryLocationTypeEquals(state, deliveryLocationType) {
  return equals(getSelectedDeliveryLocationType(state), deliveryLocationType)
}

const getSelectedDeliveryMethod = compose(
  defaultTo(null),
  findSelected,
  pathOr([], ['deliveryMethods']),
  getSelectedDeliveryLocation
)

const getSelectedDeliveryOptionFromBasket = compose(
  defaultTo(null),
  findSelected,
  pathOr([], ['checkout', 'orderSummary', 'basket', 'deliveryOptions'])
)

function hasCheckedOut(state) {
  return Object.keys(state.checkout.orderSummary).length > 0
}

const getAddressForm = (addressType, state) => {
  const addressFormKey = (getFormNames(addressType) || {}).address
  return pathOr({}, ['forms', 'checkout', addressFormKey], state)
}

const getDetailsForm = (detailsType, state) => {
  const detailsFormKey = (getFormNames(detailsType) || {}).details
  return pathOr({}, ['forms', 'checkout', detailsFormKey], state)
}

const getFindAddressForm = (findAddressType, state) => {
  const detailsFormKey = (getFormNames(findAddressType) || {}).findAddress
  return pathOr({}, ['forms', 'checkout', detailsFormKey], state)
}

const isManualAddress = (addressType, state) => {
  const addressForm = getAddressForm(addressType, state)
  return !!(
    path(['findAddress', 'isManual'], state) ||
    path(['fields', 'address1', 'value'], addressForm)
  )
}

const isQASCountry = (addressType, state) => {
  const addressForm = getAddressForm(addressType, state)
  const country =
    path(['fields', 'country', 'value'], addressForm) ||
    path(['checkout', 'orderSummary', 'shippingCountry'], state)
  return !!path(['config', 'qasCountries', country], state)
}

const findAddressIsVisible = (addressType, state) =>
  !isManualAddress(addressType, state) && isQASCountry(addressType, state)

function isDeliveryEditingEnabled(state) {
  return pathOr(
    false,
    ['checkout', 'deliveryAndPayment', 'deliveryEditingEnabled'],
    state
  )
}

function isDeliveryStoreChosen(state) {
  const deliveryStore = pathOr('', ['checkout', 'deliveryStore'], state)
  return !isEmpty(deliveryStore)
}

function isStoreDelivery(state) {
  return isStoreDeliveryLocation(getSelectedDeliveryLocation(state))
}

function getUseDeliveryAsBilling(store) {
  return pathOr(false, ['checkout', 'useDeliveryAsBilling'], store)
}

function getDeliveryOptions(state) {
  const featureCFSIEnabled = isFeatureCFSIEnabled(state)
  const brandName = path(['config', 'brandName'], state)
  const shippingCountry = path(
    ['checkout', 'orderSummary', 'shippingCountry'],
    state
  )
  const basket = path(['checkout', 'orderSummary', 'basket'], state)
  const basketDeliveryOptions = pathOr([], ['deliveryOptions'], basket)
  const homeStandard = basketDeliveryOptions.find(
    (opt) => opt.deliveryOptionExternalId === 's'
  )
  const cfsStandard = basketDeliveryOptions.find(
    (opt) => opt.deliveryOptionExternalId === 'retail_store_standard'
  )
  const parcelCollectDay = hasParcelShopDeliveryDay(basket)
  const isParcelDisabled = featureCFSIEnabled && parcelCollectDay === false

  const collectionDay = ''
  const collectionImg = 'cfs.svg'
  const ukCountrySelected = shippingCountry === 'United Kingdom'

  const deliveryOptions = {
    HOME: {
      title: 'Home Delivery',
      description: homeStandard ? homeStandard.plainLabel : '',
      additionalDescription: 'Next or Named Day Delivery',
      collectionDay: null,
      disabled: false,
      iconUrl: `/assets/${brandName}/images/lorry-icon.svg`,
    },
    STORE: {
      title: 'Collect from Store',
      description: cfsStandard ? cfsStandard.plainLabel : '',
      additionalDescription: 'Express Delivery (next day)',
      collectionDay: `${collectionDay}`,
      disabled: !ukCountrySelected,
      iconUrl: `/assets/${brandName}/images/${collectionImg}`,
    },
    PARCELSHOP: {
      title: 'Collect from ParcelShop',
      description: 'Thousands of local shops open early and late',
      additionalDescription: '',
      collectionDay: null,
      disabled: !ukCountrySelected || isParcelDisabled,
      iconUrl: `/assets/${brandName}/images/hermes-icon.svg`,
    },
  }

  return deliveryOptions
}

function getEnrichedDeliveryLocations(state) {
  const deliveryLocations = getDeliveryLocations(state)
  const deliveryOptions = getDeliveryOptions(state)

  return deliveryLocations.map((location) => {
    const option = deliveryOptions[location.deliveryLocationType]
    const enrichedDeliveryLocation = { ...location, ...option }
    return enrichedDeliveryLocation
  })
}

function getEnrichedDeliveryMethods(state) {
  const featureCFSIEnabled = isFeatureCFSIEnabled(state)
  const deliveryMethods = pathOr(
    [],
    ['deliveryMethods'],
    getSelectedDeliveryLocation(state)
  )
  const basket = path(['checkout', 'orderSummary', 'basket'], state)
  const deliveryStoreDetails = getSelectedBrandFulfilmentStore(state)
  const deliveryDayAvailability = deliveryDays(basket, deliveryStoreDetails)

  return deliveryMethods.map((deliveryMethod) => {
    const disabled =
      featureCFSIEnabled && deliveryMethod.enabled
        ? isDeliveryOptionDisabled(
            deliveryMethod.label,
            deliveryDayAvailability
          )
        : !deliveryMethod.enabled

    return {
      ...deliveryMethod,
      disabled,
    }
  })
}

function isStoreOrParcelDelivery(state) {
  return isStoreOrParcelDeliveryLocation(getSelectedDeliveryLocation(state))
}

function getSelectedDeliveryStoreType(state) {
  const { deliveryStoreCode } = state.checkout.orderSummary
  return deliveryStoreCode && deliveryStoreCode.startsWith('S')
    ? 'shop'
    : 'store'
}

function getSelectedStoreDetails(state) {
  return path(['checkout', 'orderSummary', 'storeDetails'], state)
}

function isDeliveryStoreChoiceAccepted(state) {
  return !isNil(state.checkout.orderSummary.storeDetails)
}

function getPaymentType(state) {
  return path(
    [
      'forms',
      'checkout',
      'billingCardDetails',
      'fields',
      'paymentType',
      'value',
    ],
    state
  )
}

function getKlarnaAuthToken(state) {
  return path(['klarna', 'authorizationToken'], state)
}

const getErrors = (formNames, state) => {
  const forms = {
    ...pathOr({}, ['forms', 'checkout'], state),
    giftCard: pathOr({}, ['forms', 'giftCard'], state),
  }

  return compose(
    reject(isEmpty),
    map(filter(identity)),
    reject(isNil),
    pluck('errors'),
    pick(formNames)
  )(forms)
}

const getFormErrors = (formName, state) => {
  return getErrors([formName], state)[formName] || {}
}

const isReturningCustomer = (state) => {
  const id = path(
    ['account', 'user', 'billingDetails', 'addressDetailsId'],
    state
  )
  return id !== -1 && Number.isInteger(id)
}

const hasSelectedStore = (state) => {
  return (
    !pathOr(false, ['checkout', 'storeUpdating'], state) &&
    !!pathOr(false, ['checkout', 'orderSummary', 'storeDetails'], state)
  )
}

const getDeliveryCountry = (state) => {
  return (
    path(['fields', 'country', 'value'], getAddressForm('delivery', state)) ||
    path(
      ['checkout', 'orderSummary', 'deliveryDetails', 'address', 'country'],
      state
    ) ||
    path(['checkout', 'orderSummary', 'deliveryDetails', 'country'], state) ||
    path(['checkout', 'partialOrderSummary', 'shippingCountry'], state) ||
    path(['checkout', 'orderSummary', 'shippingCountry'], state) ||
    path(['shippingDestination', 'destination'], state)
  )
}

const getBillingCountry = (state) => {
  return (
    path(['fields', 'country', 'value'], getAddressForm('billing', state)) ||
    path(
      ['checkout', 'orderSummary', 'billingDetails', 'address', 'country'],
      state
    ) ||
    path(['shippingDestination', 'destination'], state)
  )
}

const getCountryFor = (type, state) => {
  switch (type) {
    case 'delivery':
      return getDeliveryCountry(state)
    case 'billing':
      return getBillingCountry(state)
    default:
      return ''
  }
}

const getOrderCost = (state) => {
  const { deliveryLocations = [], basket } = pathOr(
    {},
    ['checkout', 'orderSummary'],
    state
  )
  if (deliveryLocations.length && basket) {
    const subTotal = basket.subTotal
    const shippingCost = selectedDeliveryLocation(deliveryLocations).cost
    const discounts = basket.discounts || []
    return fixTotal(subTotal, shippingCost, pluck('value', discounts))
  }
}

const shouldDisplayDeliveryInstructions = (state) => {
  const deliveryCountry = getDeliveryCountry(state)
  const deliveryLocation = getSelectedDeliveryLocationType(state)
  return deliveryCountry === 'United Kingdom' && deliveryLocation === 'HOME'
}

const getDeliveryPageFormNames = (state) => {
  const formNames = ['yourDetails', 'yourAddress', 'findAddress']
  if (shouldDisplayDeliveryInstructions(state)) {
    formNames.push('deliveryInstructions')
  }

  return formNames
}

const getDeliveryPaymentPageFormNames = (state) => {
  const formNames = [
    'yourDetails',
    'yourAddress',
    'findAddress',
    'deliveryInstructions',
    'giftCard',
    'billingDetails',
    'billingAddress',
    'billingFindAddress',
    'billingCardDetails',
    'order',
  ]
  if (shouldDisplayDeliveryInstructions(state)) {
    return formNames
  }

  return without(['deliveryInstructions'], formNames)
}

const getDeliveryPaymentPageFormErrors = (state) => {
  const formNames = getDeliveryPaymentPageFormNames(state)

  return getErrors(formNames, state)
}

const isNotEmpty = (obj) => {
  return obj && Object.keys(obj).length > 0
}

const extractDiscountInfo = (state) => {
  // Fallback Order Summary Way
  const orderSummary = path(['checkout', 'orderSummary'], state)
  if (!isEmpty(orderSummary))
    return pathOr([], ['basket', 'discounts'], orderSummary)

  // Fallback Order Completed
  const orderCompleted = path(['checkout', 'orderCompleted'], state)
  if (isNotEmpty(orderCompleted)) {
    const { totalOrdersDiscount, totalOrdersDiscountLabel } = pick(
      ['totalOrdersDiscount', 'totalOrdersDiscountLabel'],
      orderCompleted
    )
    const discountsArray = !totalOrdersDiscount
      ? []
      : totalOrdersDiscount
          .toString()
          .split('-')
          .slice(1)
    return discountsArray.length === 1
      ? [
          {
            label: totalOrdersDiscountLabel,
            value: normalizePrice(discountsArray[0]),
          },
        ]
      : discountsArray.map((discount) => {
          return {
            label: 'Discount',
            value: normalizePrice(discount),
          }
        })
  }
}

const getSubTotal = (state) => {
  // order Summary Way
  const orderSummarySubTotal = path(
    ['checkout', 'orderSummary', 'basket', 'subTotal'],
    state
  )
  if (orderSummarySubTotal) return orderSummarySubTotal

  // Order Completed
  const orderCompleted = path(['checkout', 'orderCompleted'], state)
  if (isNotEmpty(orderCompleted)) {
    const { deliveryCost, deliveryPrice, subTotal, totalOrderPrice } = pathOr(
      {},
      ['checkout', 'orderCompleted'],
      state
    )
    return (
      subTotal || (totalOrderPrice - (deliveryCost || deliveryPrice)).toFixed(2)
    )
  }
}

const getTotal = (state) =>
  // via Order Summary
  path(['checkout', 'orderSummary', 'basket', 'total'], state) ||
  // or via Order Completed
  path(['checkout', 'orderCompleted', 'totalOrderPrice'], state)

const isCollectFromOrder = (state) => {
  // order Summary Check
  const deliveryStoreCode = path(
    ['checkout', 'orderSummary', 'deliveryStoreCode'],
    state
  )
  if (deliveryStoreCode !== undefined) return true

  // order Completed Check
  const orderCompleted = path(['checkout', 'orderCompleted'], state)
  if (isNotEmpty(orderCompleted)) {
    return pathOr('', ['deliveryCarrier'], orderCompleted).includes(
      'Retail Store'
    )
  }
}

const getDeliveryDate = (state) => {
  // order Summary Check
  const estimatedDelivery = path(
    ['checkout', 'orderSummary', 'estimatedDelivery'],
    state
  )

  // order Completed Check
  const deliveryDate = path(
    ['checkout', 'orderCompleted', 'deliveryDate'],
    state
  )
  return deliveryDate || normaliseEstimatedDeliveryDate(estimatedDelivery)
}

function getShoppingBagOrderId(state) {
  return pathOr(null, ['shoppingBag', 'bag', 'orderId'], state)
}

const isSavePaymentDetailsEnabled = function isSavePaymentDetailsEnabled(
  state
) {
  return pathOr(false, ['checkout', 'savePaymentDetails'], state)
}

function shouldUpdateOrderSummaryStore(state) {
  const orderSummaryStore = path(
    ['checkout', 'orderSummary', 'deliveryStoreCode'],
    state
  )

  const selectedStoreForCheckout = path(
    ['checkout', 'deliveryStore', 'deliveryStoreCode'],
    state
  )
  // selectedFulfilmentStore gets updated with selectedStoreForCheckout when a product is added to bag
  const selectedFulfilmentStore = path(
    ['storeId'],
    getSelectedBrandFulfilmentStore(state)
  )

  return (
    !!orderSummaryStore &&
    !!selectedStoreForCheckout &&
    selectedStoreForCheckout !== orderSummaryStore &&
    selectedStoreForCheckout === selectedFulfilmentStore
  )
}

const getSelectedDeliveryType = (state) => {
  const selectedDeliveryType = pathOr(
    null,
    ['deliveryType'],
    getSelectedDeliveryMethod(state)
  )

  // When DeliveryMethods are populated with a selected option
  if (selectedDeliveryType) {
    return selectedDeliveryType
  }
  // When in checkout and deliveryMethods are empty
  if (isInCheckout(state)) {
    return getDefaultDeliveryType(getSelectedDeliveryLocationType(state))
  }
  // When deliveryMethods are empty and an option has been selected in MiniBag
  return getDeliveryTypeFromBasket(getSelectedDeliveryOptionFromBasket(state))
}

const getSelectedDeliveryMethodLabel = (state) => {
  const deliveryMethod = getSelectedDeliveryMethod(state)
  return pathOr(null, ['label'], deliveryMethod)
}

// Fix to get PARCELSHOP shipModeId
// The orderSummary PUT request to change delivery method requires shipModeId for ParcelShop
// When deliveryMethods are not populated for ParcelShop in ordeSummary we need
// to fetch the shipModeId from the basket deliveryOptions
// @NOTE To find a better solution once JSON endpoints are ready
const getShipModeIdFromBasket = (state) => {
  const deliveryOptions = pathOr(
    [],
    ['checkout', 'orderSummary', 'basket', 'deliveryOptions'],
    state
  )

  // When not in checkout returns selected deliveryOption shipModeId from MiniBag
  if (!isInCheckout(state)) {
    return compose(
      path(['deliveryOptionId']),
      findSelected
    )(deliveryOptions)
  }
  // If in checkout, parcelshop selected but deliveryMethods are not populated
  if (getSelectedDeliveryLocationType(state) === 'PARCELSHOP') {
    return compose(
      path(['deliveryOptionId']),
      find(matchesParcelShopLabel)
    )(deliveryOptions)
  }

  return undefined
}

const getShipModeId = (state) => {
  const selectedDeliveryMethod = getSelectedDeliveryMethod(state)

  if (selectedDeliveryMethod) {
    const selectedDeliveryOption = compose(
      findSelected,
      pathOr([], ['deliveryOptions'])
    )(selectedDeliveryMethod)

    return selectedDeliveryOption
      ? path(['shipModeId'], selectedDeliveryOption)
      : path(['shipModeId'], selectedDeliveryMethod)
  }
  // If deliveryMethods are not populated we need to get shipModeId from basket's deliveyOptions
  return getShipModeIdFromBasket(state)
}

export {
  extractDiscountInfo,
  findAddressIsVisible,
  getAddressForm,
  getBillingCountry,
  getCountryFor,
  getDeliveryCountry,
  getDeliveryDate,
  getDeliveryLocations,
  getDeliveryOptions,
  getDeliveryPageFormNames,
  getDeliveryPaymentPageFormNames,
  getDeliveryPaymentPageFormErrors,
  getDetailsForm,
  getEnrichedDeliveryLocations,
  getEnrichedDeliveryMethods,
  getErrors,
  getFindAddressForm,
  getFormErrors,
  getKlarnaAuthToken,
  getOrderCost,
  getPaymentType,
  getSelectedDeliveryLocation,
  getSelectedDeliveryLocationType,
  getSelectedDeliveryMethod,
  getSelectedDeliveryMethodLabel,
  getSelectedDeliveryOptionFromBasket,
  getSelectedDeliveryStoreType,
  getSelectedDeliveryType,
  getSelectedStoreDetails,
  getShipModeId,
  getShoppingBagOrderId,
  getSubTotal,
  getTotal,
  getUseDeliveryAsBilling,
  hasCheckedOut,
  hasSelectedStore,
  isCollectFromOrder,
  isDeliveryEditingEnabled,
  isDeliveryStoreChoiceAccepted,
  isDeliveryStoreChosen,
  isManualAddress,
  isQASCountry,
  isReturningCustomer,
  isSavePaymentDetailsEnabled,
  isStoreDelivery,
  isStoreOrParcelDelivery,
  selectedDeliveryLocationTypeEquals,
  shouldDisplayDeliveryInstructions,
  shouldUpdateOrderSummaryStore,
}
