import { path, pathOr } from 'ramda'

const isDebugAllowed = path(['debug', 'isAllowed'])

const isDebugShown = path(['debug', 'isShown'])

const getBuildVersion = pathOr('FIXME', ['debug', 'buildInfo', 'tag'])

export { isDebugAllowed, isDebugShown, getBuildVersion }
