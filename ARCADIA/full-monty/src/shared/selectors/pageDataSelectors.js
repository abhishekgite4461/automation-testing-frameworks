import { createSelector } from 'reselect'

export const rootSelector = (state) => state.pageData || {}

export const getMetaDescription = createSelector(
  rootSelector,
  (pageData) => pageData.description
)

export const getPageTitle = createSelector(
  rootSelector,
  (pageData) => pageData.title
)
