import { path } from 'ramda'

export const getSuccess = (state) => path(['resetPassword', 'success'], state)
