import { getFulfilmentDetails } from '../lib/get-delivery-days'
import { deliveryDays } from '../lib/get-basket-availability'
import { getActiveProductWithInventory } from './inventorySelectors'
import { path, pathOr } from 'ramda'

const getActiveProductCollectionDay = (state, storeDetails) => {
  const activeProduct = getActiveProductWithInventory(state)
  const collectionDays = getFulfilmentDetails(activeProduct, storeDetails)
  return pathOr('', ['CFSiDay'], collectionDays)
}

const getBasketCollectionDay = (state, storeDetails) => {
  const basket = path(['checkout', 'orderSummary', 'basket'], state)
  const collectionDays = deliveryDays(basket, storeDetails)
  return pathOr('', ['CFSiDay'], collectionDays)
}

export const getCFSDay = (state, storeDetails, storeLocatorType) => {
  return storeLocatorType === 'collectFromStore'
    ? getBasketCollectionDay(state, storeDetails)
    : getActiveProductCollectionDay(state, storeDetails)
}
