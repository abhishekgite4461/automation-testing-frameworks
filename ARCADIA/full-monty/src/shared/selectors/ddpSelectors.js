import { createSelector } from 'reselect'
import { pathOr } from 'ramda'
import { isFeatureDDPPromoEnabled } from './featureSelectors'
import { getUser } from './common/accountSelectors'

export const isDDPUser = createSelector(getUser, (user) => user.isDDPUser)

export const isDDPRenewable = createSelector(
  getUser,
  (user) => user.isDDPRenewable
)

export const isDDPUserInPreExpiryWindow = createSelector(
  isDDPUser,
  isDDPRenewable,
  (isDDPUser, isDDPRenewable) => isDDPUser && isDDPRenewable
)

export const isDDPRenewablePostWindow = createSelector(
  isDDPUser,
  isDDPRenewable,
  (isDDPUser, isDDPRenewable) => !isDDPUser && isDDPRenewable
)

export const isDDPActiveUserPreRenewWindow = createSelector(
  isDDPUser,
  isDDPRenewable,
  (isDDPUser, isDDPRenewable) => isDDPUser && !isDDPRenewable
)

export const isCurrentOrRecentDDPSubscriber = createSelector(
  isDDPUser,
  isDDPRenewable,
  (isDDPUser, isDDPRenewable) => isDDPUser || isDDPRenewable
)

export const isDDPOrder = (state) =>
  pathOr(false, ['checkout', 'orderSummary', 'basket', 'isDDPOrder'], state)

export const isPartialOrderDDP = (state) =>
  pathOr(
    false,
    ['checkout', 'partialOrderSummary', 'basket', 'isDDPOrder'],
    state
  )

export const isDDPPromotionEnabled = createSelector(
  isFeatureDDPPromoEnabled,
  isDDPOrder,
  isDDPActiveUserPreRenewWindow,
  (isDDPPromoEnabled, isDDPOrder, isDDPUserPreRenewWindow) =>
    isDDPPromoEnabled && !(isDDPOrder || isDDPUserPreRenewWindow)
)
