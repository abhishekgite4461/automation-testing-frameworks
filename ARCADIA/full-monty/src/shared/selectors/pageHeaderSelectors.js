import { createSelector } from 'reselect'

import { isTablet, isPortrait } from './viewportSelectors'
import { getBrandCode, getLanguage } from './configSelectors'
import isForceMobileHeader from './isForceMobileHeader'

export const shouldDisplayMobileHeaderIfSticky = createSelector(
  isTablet,
  isPortrait,
  getLanguage,
  getBrandCode,
  (tablet, portrait, language, brandCode) =>
    isForceMobileHeader({
      isTablet: tablet,
      isPortrait: portrait,
      language,
      brandCode,
    })
)

const rootSelector = (state) => state.pageHeader || {}

export const isHeaderSticky = createSelector(rootSelector, (pageHeader) =>
  Boolean(pageHeader.sticky)
)
