import { path } from 'ramda'

export const getSelectedPlaceId = path([
  'userLocator',
  'selectedPlaceDetails',
  'place_id',
])
