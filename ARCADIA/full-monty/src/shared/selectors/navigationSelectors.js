import { isEmpty } from 'ramda'
import { createSelector } from 'reselect'

export const rootSelector = (state) => state.navigation || {}

export const getMegaNav = createSelector(
  rootSelector,
  (navigation) => navigation.megaNav || {}
)

export const getMegaNavSelectedCategory = createSelector(
  rootSelector,
  (navigation) => navigation.megaNavSelectedCategory || ''
)

export const getMegaNavCategories = createSelector(
  getMegaNav,
  (megaNav) => megaNav.categories || []
)

export const getMegaNavCategoriesVisible = createSelector(
  getMegaNav,
  (megaNav) => {
    const { categories = [] } = megaNav
    return categories.filter((c) => !c.isHidden)
  }
)

export const getMegaNavCategoriesFilteredColumns = createSelector(
  getMegaNavCategoriesVisible,
  (categories) => {
    return categories.map((category = {}) => {
      const { columns = [] } = category
      return {
        ...category,
        columns: columns.filter(
          (column) =>
            Boolean(column.subcategories) && !isEmpty(column.subcategories)
        ),
      }
    })
  }
)
