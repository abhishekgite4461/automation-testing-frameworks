import { path, contains } from 'ramda'
import { createSelector } from 'reselect'
import breakpoints from '../constants/responsive'

const viewPortsOrderedArray = ['mobile', 'tablet', 'laptop', 'desktop']

const getViewport = (state) => (state && state.viewport ? state.viewport : {})

const isBreakpoint = (breakpoint) =>
  createSelector(getViewport, (viewport) => viewport.media === breakpoint)

export const isMobile = isBreakpoint('mobile')

export const isDesktop = isBreakpoint('desktop')

export const isTablet = isBreakpoint('tablet')

function isViewPort(isMax, state) {
  return (viewPortName) => {
    const currentViewPort = path(['viewport', 'media'], state)
    if (!currentViewPort) return false
    const currentViewPortIndex = viewPortsOrderedArray.findIndex(
      (i) => i === viewPortName
    )
    const viewPortNamesArray = isMax
      ? viewPortsOrderedArray.slice(0, currentViewPortIndex + 1)
      : viewPortsOrderedArray.slice(currentViewPortIndex)
    return contains(currentViewPort, viewPortNamesArray)
  }
}

export const getWindowWidth = createSelector(
  getViewport,
  (viewport) => viewport.width || 0
)
export const getWindowHeight = createSelector(
  getViewport,
  (viewport) => viewport.height || 0
)

export function isMinViewPort(state) {
  return isViewPort(false, state)
}

export function isMaxViewPort(state) {
  return isViewPort(true, state)
}

export function isMobileBreakpoint(state) {
  return state.viewport.width <= breakpoints.mobile.max
}

export const getHeight = createSelector(
  getViewport,
  (viewport) => viewport.height || 0
)

export const isPortrait = createSelector(
  getWindowWidth,
  getWindowHeight,
  (width, height) => height >= width
)
export const isLandscape = createSelector(
  getWindowWidth,
  getWindowHeight,
  (width, height) => width > height
)

export const getViewportMedia = path(['viewport', 'media'])
