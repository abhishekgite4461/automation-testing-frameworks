import { createSelector } from 'reselect'
import { path, pathOr } from 'ramda'

const rootSelector = (state) => state.storeLocator || {}

export const getStoreLocatorStores = createSelector(
  rootSelector,
  pathOr([], ['stores'])
)

export const getStoreByIndex = createSelector(
  getStoreLocatorStores,
  (_, storeIndex) => storeIndex,
  (stores, storeIndex) => stores[storeIndex]
)

export const getStoreLocatorFilters = createSelector(
  rootSelector,
  path(['filters'])
)

export const isMapExpanded = createSelector(rootSelector, path(['mapExpanded']))

// TODO: Move to another file? E.g. find-in-store-selectors.js
export const getActiveItem = path(['findInStore', 'activeItem'])
