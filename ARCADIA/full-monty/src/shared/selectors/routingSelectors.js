import { pathOr } from 'ramda'
import { createSelector } from 'reselect'

const getLocation = function getLocation(state) {
  return pathOr({}, ['routing', 'location'], state)
}

const getLocationQuery = function getLocationQuery(state) {
  return pathOr({}, ['routing', 'location', 'query'], state)
}

const getRoutePath = function getRoutePath(state) {
  return pathOr('', ['routing', 'location', 'pathname'], state)
}

const getRouteSearch = function getRouteSearch(state) {
  return pathOr('', ['routing', 'location', 'search'], state)
}

const getRoutePathWithParams = function getRoutePathWithParams(state) {
  return `${getRoutePath(state)}${getRouteSearch(state)}`
}

const getVisitedPaths = (state) => {
  return pathOr([], ['routing', 'visited'], state)
}

const getPrevPath = (state) => {
  const visitedPaths = getVisitedPaths(state)
  const currentPath = getRoutePath(state)
  const currentPathIndex = visitedPaths.lastIndexOf(currentPath)
  return currentPathIndex ? visitedPaths[currentPathIndex - 1] : 'direct link'
}

const isInCheckout = (state) => {
  return /\/checkout(\/.*)/.test(getRoutePath(state))
}

const isHomePage = function isHomePage(state) {
  return getRoutePath(state) === '/'
}

const selectInDeliveryAndPayment = createSelector(
  getRoutePath,
  (pathName) => pathName !== '' && pathName.endsWith('/delivery-payment')
)

const getRedirect = function getRouting(state) {
  return state.routing.redirect
}

export const selectHostname = (state) =>
  pathOr('', ['routing', 'location', 'hostname'], state)

export {
  getLocation,
  getLocationQuery,
  getRoutePath,
  getRouteSearch,
  getRoutePathWithParams,
  getVisitedPaths,
  getPrevPath,
  isInCheckout,
  isHomePage,
  selectInDeliveryAndPayment,
  getRedirect,
}
