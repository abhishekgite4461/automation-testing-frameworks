import {
  __,
  contains,
  defaultTo,
  either,
  equals,
  filter,
  find,
  path,
  pathOr,
  pipe,
  pluck,
  propEq,
  where,
} from 'ramda'
import {
  selectBillingCountry,
  selectDeliveryCountry,
  selectStoredPaymentDetails,
} from './common/accountSelectors'
import { createSelector } from 'reselect'
import { combineCardOptions } from '../lib/checkout-utilities/utils'

export function getPaymentOptionTypes(state) {
  const types = []
  let hasCards = false

  state.paymentMethods.forEach((method) => {
    if (method.type === 'CARD') {
      hasCards = true
    } else {
      types.push(method.value)
    }
  })

  if (hasCards) {
    types.unshift('CARD')
  }

  return types
}

export function getPaymentMethodByOptionType(state, type) {
  let result
  const paymentMethods = state.paymentMethods

  if (type === 'CARD') {
    const options = paymentMethods.filter(({ type }) => type === 'CARD')
    result = {
      value: 'CARD',
      type: 'CARD',
      label: 'Debit and Credit Card',
      description: options.map((option) => option.label).join(', '),
      paymentTypes: options,
    }
  } else {
    result = paymentMethods.filter((option) => type === option.value)[0]
  }

  return result
}

function useSvg(filename) {
  return filename.replace(/.gif/, '.svg')
}

export function getCardIcons(state) {
  return state.paymentMethods
    .filter((method) => method.type === 'CARD')
    .map((method) => useSvg(method.icon))
    .filter((icon) => !!icon)
}

export function getPaymentOptionByType(state, type) {
  const paymentMethod = getPaymentMethodByOptionType(state, type)
  if (paymentMethod) {
    const icons =
      'icon' in paymentMethod
        ? [useSvg(paymentMethod.icon)]
        : getCardIcons(state)
    const result = {
      ...paymentMethod,
      icons,
    }
    delete result.icon
    return result
  }
  return {}
}

export function getPaymentMethodByValue(state, value) {
  return state.paymentMethods.filter((m) => {
    return m.value === value
  })[0]
}

export function isPaymentMethodsLoaded(state) {
  return state.paymentMethods.length > 0
}

export function getSelectedPaymentOptionType(state) {
  if (!state.paymentMethods.length) return

  const value = path(
    [
      'forms',
      'checkout',
      'billingCardDetails',
      'fields',
      'paymentType',
      'value',
    ],
    state
  )
  const method = getPaymentMethodByValue(state, value)

  return method && method.type === 'CARD' ? 'CARD' : value
}

/**
 * Function that returns PaymentMethod type (CARD, OTHER_CARD, OTHER, ...) by providing the value (VISA, PYPAL, ...)
 * Note: If current value doesn't exists, then returns CARD as a default type
 * @param {object} state
 * @param {string} value
 */
export function getPaymentMethodTypeByValue(
  state,
  value,
  defaultVale = 'CARD'
) {
  const paymentMethods = pathOr([], ['paymentMethods'], state)
  const paymentMethodFound = paymentMethods.find(
    (payment) => payment.value === value
  )
  return paymentMethodFound ? paymentMethodFound.type : defaultVale
}

export const selectPaymentMethods = (state) =>
  pathOr([], ['paymentMethods'], state)
const defaultToEmptyObject = defaultTo({})

const filterPaymentMethodsByDeliveryAndBillingCountry = (
  deliveryCountry,
  billingCountry,
  paymentMethods
) => {
  const paymentMethodValidForDeliveryAndBillingCountry = where({
    deliveryCountry: either(equals(undefined), contains(deliveryCountry)),
    billingCountry: either(equals(undefined), contains(billingCountry)),
  })

  return filter(paymentMethodValidForDeliveryAndBillingCountry, paymentMethods)
}

export const selectPaymentMethodsValidForDeliveryAndBillingCountry = createSelector(
  selectDeliveryCountry,
  selectBillingCountry,
  selectPaymentMethods,
  filterPaymentMethodsByDeliveryAndBillingCountry
)

export const selectPaymentMethodForStoredPaymentDetails = createSelector(
  selectStoredPaymentDetails,
  selectPaymentMethods,
  ({ type }, paymentMethods) =>
    defaultToEmptyObject(find(propEq('value', type))(paymentMethods))
)

export const selectCombinedPaymentMethods = createSelector(
  selectPaymentMethodsValidForDeliveryAndBillingCountry,
  (paymentMethods) => combineCardOptions(paymentMethods)
)

export const selectDecoratedCombinedPaymentMethods = createSelector(
  selectCombinedPaymentMethods,
  (combinedPaymentMethods) =>
    combinedPaymentMethods.map(
      ({ label, description, value, type, icon, paymentTypes = [] }) => {
        const icons = icon ? [icon] : paymentTypes.map(({ icon }) => icon)
        return {
          label,
          description,
          value,
          type,
          icons,
          paymentTypes,
        }
      }
    )
)

export const getCombinedPaymentMethodByPaymentMethodValue = (
  combinedPaymentMethods,
  value,
  defaultResult
) => {
  const filter = (combinedPaymentMethod) => {
    const checkIfBankCard = pipe(
      pathOr([], ['paymentTypes'], __),
      pluck('value'),
      contains(value)
    )
    const checkIfOther = propEq('value', value)

    return either(checkIfBankCard, checkIfOther)(combinedPaymentMethod)
  }

  return defaultTo(defaultResult)(find(filter)(combinedPaymentMethods))
}
