import { equals, pathOr } from 'ramda'

const matchProductId = (x) => ({ productId }) => productId === x

export function getDefaultWishlistId(state) {
  return pathOr(null, ['wishlist', 'giftListId'], state)
}

export function getWishlistItemIds(state) {
  return pathOr(null, ['wishlist', 'productList'], state)
}

export function getWishlistItemDetails(state) {
  return pathOr(null, ['wishlist', 'itemDetails'], state)
}

export function getWishlistItemCount(state) {
  const wishlistedItems = getWishlistItemIds(state) || []
  return wishlistedItems.length
}

export function isProductAddedToWishlist(state, productId) {
  const wishlistedItems = getWishlistItemIds(state) || []
  return wishlistedItems.some(matchProductId(productId))
}

export function isMovingProductToWishlist(state, productId) {
  return state.wishlist.movingProductToWishlist === productId
}
export function isMovingAnyProductToWishlist(state) {
  return !!state.wishlist.movingProductToWishlist
}

export function getWishlistedItem(state, productId) {
  const wishlistedItems = getWishlistItemIds(state) || []
  return wishlistedItems.find(matchProductId(productId)) || {}
}

export function isWishlistSync(state) {
  const wishlistItemIds = getWishlistItemIds(state)
  const wishlistItemDetails = getWishlistItemDetails(state)

  return wishlistItemIds && wishlistItemDetails
    ? equals(
        wishlistItemIds.map(({ productId }) => productId),
        wishlistItemDetails.map(({ parentProductId }) =>
          parseInt(parentProductId, 10)
        )
      )
    : false
}
