import { path, pathOr } from 'ramda'
import { shouldInvertHeaderEspotPositions } from './brandConfigSelectors'
import espotsDesktopConstants from '../constants/espotsDesktop'
import {
  isFeatureCFSIEnabled,
  isFeatureShowCFSIEspotEnabled,
} from './featureSelectors'

export const getResponsiveCMSUrl = (state, identifier) =>
  path(['espot', 'cmsData', identifier, 'responsiveCMSUrl'], state)

export const getBrandHeaderEspotName = (state) =>
  shouldInvertHeaderEspotPositions(state)
    ? espotsDesktopConstants.navigation.global
    : espotsDesktopConstants.navigation.brandHeader

export const getGlobalEspotName = (state) =>
  shouldInvertHeaderEspotPositions(state)
    ? espotsDesktopConstants.navigation.brandHeader
    : espotsDesktopConstants.navigation.global

/*
* TODO consider replacing this with a more robust solution
* this is currently needed for PLP espots
*/
export const getProductListEspots = (state) => {
  const data = pathOr({}, ['espot', 'cmsData'], state)
  return Object.entries(data)
    .filter(
      ([key, value]) =>
        key.startsWith('productList') && value.responsiveCMSUrl !== ''
    )
    .map(([key, value]) => ({
      ...value,
      identifier: key,
    }))
}

// CFS = collect from store
export const isCFSIEspotEnabled = (state) =>
  isFeatureCFSIEnabled(state) && isFeatureShowCFSIEspotEnabled(state)
