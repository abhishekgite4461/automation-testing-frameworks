import { createSelector } from 'reselect'

const rootSelector = (state) => state.shoppingBag || {}

export const getShoppingBag = createSelector(
  rootSelector,
  (shoppingBag) => shoppingBag.bag || {}
)

export const getShoppingBagTotal = createSelector(
  getShoppingBag,
  (bag) => (bag.total ? Number(bag.total) : 0)
)

export const isZeroValueBag = createSelector(
  getShoppingBagTotal,
  (total) => total === 0
)

export const getShoppingBagProducts = createSelector(
  getShoppingBag,
  (bag) => bag.products || []
)

export const getShoppingBagOrderId = createSelector(
  getShoppingBag,
  (bag) => bag.orderId
)

export const getShoppingBagTotalItems = createSelector(
  rootSelector,
  (shoppingBag) => shoppingBag.totalItems
)

export const isShoppingBagEmpty = createSelector(
  getShoppingBagTotalItems,
  (shoppingBagTotalItems) => shoppingBagTotalItems <= 0
)

export const isShoppingBagLoading = createSelector(
  rootSelector,
  (shoppingBag) => shoppingBag.loadingShoppingBag
)

export const bagContainsDDPProduct = createSelector(
  getShoppingBagProducts,
  (products) =>
    products.reduce((rv, product) => rv || product.isDDPProduct, false)
)

export const bagContainsOnlyDDPProduct = createSelector(
  getShoppingBagTotalItems,
  bagContainsDDPProduct,
  (getShoppingBagTotalItems, bagContainsDDPProduct) =>
    getShoppingBagTotalItems === 1 && bagContainsDDPProduct
)
