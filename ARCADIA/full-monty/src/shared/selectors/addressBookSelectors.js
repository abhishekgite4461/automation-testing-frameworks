import { addressBookConstants } from '../constants/addressBookConstants'
import { path, pathOr } from 'ramda'
import { getCountryCodeFromQAS } from './common/configSelectors'

const getSavedAddresses = (state) => {
  const addressId = state.account.user.deliveryDetails.addressDetailsId
  const address = state.account.user.deliveryDetails.address
  const nameAndPhone = state.account.user.deliveryDetails.nameAndPhone
  if (state.checkout.orderSummary.savedAddresses.length > 0) {
    return state.checkout.orderSummary.savedAddresses
  } else if (addressId !== -1) {
    return [
      {
        id: state.account.user.deliveryDetails.addressDetailsId,
        addressName: `${address.city}, ${address.postcode}, ${address.country}`,
        selected: true,
        address1: address.address1,
        address2: address.address2,
        city: address.city,
        state: address.state,
        postcode: address.postcode,
        country: address.country,
        title: nameAndPhone.title,
        firstName: nameAndPhone.firstName,
        lastName: nameAndPhone.lastName,
        telephone: nameAndPhone.telephone,
      },
    ]
  }
  return []
}

const getIsNewAddressFormVisible = (state) => {
  return pathOr(false, ['addressBook', 'isNewAddressFormVisible'], state)
}

const getFormNames = (formType) => {
  return addressBookConstants.formNames[formType]
}

const getAddressBookForm = (addressType, formName, state) => {
  return pathOr({}, ['forms', 'addressBook', formName], state)
}

const getDefaultCountry = (state) => {
  const accountCountry = path(
    ['account', 'user', 'deliveryDetails', 'address', 'country'],
    state
  )
  const brandCountry = path(['config', 'country'], state)
  return accountCountry || brandCountry
}

const getCountryFor = (addressType, formName, state) => {
  const addressBookForm = getAddressBookForm(addressType, formName, state)
  const formFieldCountry = path(['fields', 'country', 'value'], addressBookForm)
  const defaultCountry = getDefaultCountry(state)
  return formFieldCountry || defaultCountry
}

const getIsFindAddressVisible = (addressType, formName, country, state) => {
  const addressBookForm = getAddressBookForm(addressType, formName, state)
  const isManual = !!(
    path(['fields', 'isManual', 'value'], addressBookForm) ||
    path(['fields', 'address1', 'value'], addressBookForm)
  )
  const QASCountry = getCountryCodeFromQAS(state, country)
  return !isManual && QASCountry !== ''
}

const getHasFoundAddress = (findAddressForm) => {
  return !!path(['fields', 'findAddress', 'value'], findAddressForm)
}

const getHasSelectedAddress = (addressForm) => {
  return !!path(['fields', 'address1', 'value'], addressForm)
}

export {
  getSavedAddresses,
  getIsNewAddressFormVisible,
  getFormNames,
  getAddressBookForm,
  getDefaultCountry,
  getCountryFor,
  getIsFindAddressVisible,
  getHasFoundAddress,
  getHasSelectedAddress,
}
