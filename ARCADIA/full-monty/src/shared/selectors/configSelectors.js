import { isMobile as isMobileSelector } from './viewportSelectors'

import { createSelector } from 'reselect'
import { pathOr } from 'ramda'

const MOBILE_MAXIMUM_NUMBER_SIZE_TILE = 8
const DEFAULT_DESKTOP_MAXIMUM_NUMBER_SIZE_TILE = 8

export const selectConfig = (state) => state.config || {}

export const getCurrencySymbol = createSelector(
  selectConfig,
  (config) => config.currencySymbol
)

export const getBrandCode = createSelector(
  selectConfig,
  (config) => config.brandCode
)

export const getLangHostnames = createSelector(
  selectConfig,
  (config) => config.langHostnames
)

export const getDefaultLanguage = createSelector(
  getLangHostnames,
  (langHostnames) => langHostnames.default.defaultLanguage
)

export const getBaseUrlPath = createSelector(
  selectConfig,
  ({ storeCode, lang }) => `/${storeCode}/${lang}`
)

export const getStoreId = createSelector(
  selectConfig,
  (config) => config.siteId
)

export const getCurrencyCode = createSelector(
  selectConfig,
  (config) => config.currencyCode
)

export const getRegion = createSelector(selectConfig, (config) =>
  pathOr(false, ['region'], config)
)

export const getLanguage = createSelector(selectConfig, (config) =>
  pathOr('en', ['lang'], config)
)

export const getBrandDisplayName = createSelector(
  selectConfig,
  (config) => config.brandDisplayName
)

export const getMaximumNumberOfSizeTiles = createSelector(
  selectConfig,
  isMobileSelector,
  (config, isMobile) =>
    isMobile
      ? MOBILE_MAXIMUM_NUMBER_SIZE_TILE
      : config.sizeTileMaximumDesktop ||
        DEFAULT_DESKTOP_MAXIMUM_NUMBER_SIZE_TILE
)
