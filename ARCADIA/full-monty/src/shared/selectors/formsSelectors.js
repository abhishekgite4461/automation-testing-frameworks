import { path, pathOr } from 'ramda'

const getForgetPasswordForm = (state) =>
  path(['forms', 'forgetPassword'], state)

const getOrderFormErrorMessage = (state) =>
  path(['forms', 'checkout', 'order', 'message', 'message'], state)

const getResetPasswordForm = (state) => path(['forms', 'resetPassword'], state)

const getSelectedPaymentType = (state) => {
  const fields = pathOr(
    {},
    ['forms', 'checkout', 'billingCardDetails', 'fields'],
    state
  )
  return pathOr(null, ['paymentType', 'value'], fields)
}

const selectCheckoutForms = (state) => path(['forms', 'checkout'], state)

const selectBillingCardPaymentTypeFromCheckoutForm = (state) =>
  path(
    [
      'forms',
      'checkout',
      'billingCardDetails',
      'fields',
      'paymentType',
      'value',
    ],
    state
  )

export {
  getForgetPasswordForm,
  getOrderFormErrorMessage,
  getResetPasswordForm,
  getSelectedPaymentType,
  selectCheckoutForms,
  selectBillingCardPaymentTypeFromCheckoutForm,
}
