import { MONTY_API } from '../../middleware/preprocess-redux-api-middleware'
import {
  getFormNames,
  getDefaultCountry,
} from '../../selectors/addressBookSelectors'
import { resetForm } from './formActions'
import { getOrderSummary } from './checkoutActions'

const hideNewAddressForm = () => ({
  type: 'ADDRESS_BOOK_HIDE_NEW_ADDRESS_FORM',
})

const showNewAddressForm = () => {
  return (dispatch, getState) => {
    const state = getState()
    const formNames = getFormNames('newAddress')
    const country = getDefaultCountry(state)
    dispatch(
      resetForm(formNames.address, {
        address1: '',
        address2: '',
        postcode: '',
        city: '',
        country,
        state: '',
        county: null,
        isManual: false,
      })
    )
    dispatch(
      resetForm(formNames.details, {
        title: '',
        firstName: '',
        lastName: '',
        telephone: '',
      })
    )
    dispatch(
      resetForm(formNames.findAddress, {
        houseNumber: '',
        message: '',
        findAddress: '',
        selectAddress: '',
        postcode: '',
      })
    )
    dispatch({
      type: 'ADDRESS_BOOK_SHOW_NEW_ADDRESS_FORM',
    })
  }
}

const createAddress = (address) => {
  return (dispatch) => {
    dispatch({
      [MONTY_API]: {
        endpoint: '/checkout/order_summary/delivery_address',
        method: 'POST',
        body: address,
        overlay: true,
        typesPrefix: 'ADDRESS_BOOK_CREATE_ADDRESS',
      },
    }).then((response) => {
      if (response.type === 'ADDRESS_BOOK_CREATE_ADDRESS_API_SUCCESS') {
        dispatch(hideNewAddressForm())
        dispatch(
          getOrderSummary({
            shouldUpdateBag: false,
            shouldUpdateForms: false,
          })
        )
      }
    })
  }
}

const deleteAddress = (address) => ({
  [MONTY_API]: {
    endpoint: '/checkout/order_summary/delivery_address',
    method: 'DELETE',
    body: {
      addressId: address.id ? address.id : address.addressId,
    },
    overlay: true,
    typesPrefix: 'ADDRESS_BOOK_DELETE_ADDRESS',
  },
})

const selectAddress = (address) => {
  return (dispatch) => {
    dispatch({
      [MONTY_API]: {
        endpoint: '/checkout/order_summary/delivery_address',
        method: 'PUT',
        body: { addressId: address.id ? address.id : address.addressId },
        overlay: true,
        typesPrefix: 'ADDRESS_BOOK_SELECT_ADDRESS',
      },
    }).then((response) => {
      if (response.type === 'ADDRESS_BOOK_SELECT_ADDRESS_API_SUCCESS') {
        dispatch(
          getOrderSummary({
            shouldUpdateBag: false,
            shouldUpdateForms: false,
          })
        )
      }
    })
  }
}

export {
  hideNewAddressForm,
  showNewAddressForm,
  createAddress,
  deleteAddress,
  selectAddress,
}
