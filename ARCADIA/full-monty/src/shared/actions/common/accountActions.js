// libs
import { MONTY_API } from '../../middleware/preprocess-redux-api-middleware'
import { get, put } from '../../lib/api-service'
import {
  getDefaultAddress,
  getDefaultPaymentOptions,
  isUserCreditCard,
} from '../../lib/checkout-utilities/actions-helpers'
import { path, pathOr, pluck } from 'ramda'
import { browserHistory } from 'react-router'

// selectors
import { getMCDAddressForm } from '../../selectors/common/accountSelectors'
import { getPaymentMethodTypeByValue } from '../../selectors/paymentMethodSelectors'

// actions
import { setFormMeta, setFormMessage, resetForm } from './formActions'
import { ajaxCounter } from '../components/LoaderOverlayActions'
import { setFindAddressMode } from './checkoutActions'

// constants
import { formNames } from '../../constants/forms/myCheckoutDetailsConstants'
import * as logger from '../../../server/lib/logger'

// Short user profile
export function userAccount(user) {
  return {
    type: 'USER_ACCOUNT',
    user,
  }
}

export function closeCustomerDetailsModal() {
  return setFormMeta('customerDetails', 'modalOpen', false)
}

export function getAccount() {
  return {
    [MONTY_API]: {
      endpoint: '/account',
      method: 'get',
      overlay: true,
      typesPrefix: 'GET_USER_ACCOUNT',
      meta: {
        failure: {
          errorAlertBox: true,
        },
      },
    },
  }
}

export function updateAccount(data) {
  return {
    [MONTY_API]: {
      endpoint: '/account/customerdetails',
      method: 'PUT',
      body: data,
      overlay: true,
      typesPrefix: 'UPDATE_USER_ACCOUNT_FORM',
      formName: 'customerDetails',
      meta: {
        success: {
          formMeta: ['modalOpen', true],
        },
      },
    },
  }
}

export function changeShortProfileRequest(data) {
  return {
    [MONTY_API]: {
      endpoint: '/account/shortdetails',
      method: 'PUT',
      body: data,
      overlay: true,
      typesPrefix: 'CHANGE_SHORT_PROFILE_FORM',
      formName: 'customerShortProfile',
      meta: {
        success: {
          messageKey: 'Your profile details have been successfully updated.',
        },
      },
    },
  }
}

export function changePwdRequest(data, resetPassword) {
  return {
    [MONTY_API]: {
      endpoint: '/account/changepassword',
      method: 'PUT',
      body: data,
      overlay: true,
      typesPrefix: 'CHANGE_PASSWORD_FORM',
      formName: 'changePassword',
      meta: {
        success: {
          analytics: {
            type: 'AUTH',
            data,
            events: ['event109'],
          },
          dispatchApi: resetPassword && getAccount,
          messageKey: 'Your password has been successfully changed.',
        },
      },
    },
  }
}

export function setForgetPassword(value) {
  return {
    type: 'TOGGLE_FORGET_PASSWORD',
    value,
  }
}

export function toggleForgetPassword() {
  return {
    type: 'TOGGLE_FORGET_PASSWORD',
  }
}

export function forgetPwdRequest(data) {
  return {
    [MONTY_API]: {
      endpoint: '/account/forgetpassword',
      method: 'POST',
      body: data,
      overlay: true,
      typesPrefix: 'FORGOT_PASSWORD_FORM',
      formName: 'forgetPassword',
      meta: {
        success: {
          analytics: {
            type: 'AUTH',
            data,
            events: ['event109'],
          },
        },
      },
    },
  }
}

export function resetPasswordLinkRequest(data) {
  return {
    [MONTY_API]: {
      endpoint: '/account/reset_password_link',
      method: 'POST',
      body: data,
      overlay: true,
      typesPrefix: 'FORGOT_PASSWORD_FORM',
      formName: 'forgetPassword',
      meta: {
        success: {
          analytics: {
            type: 'AUTH',
            data,
            events: ['event109'],
          },
        },
      },
    },
  }
}

export function resetPasswordRequest(data) {
  return {
    [MONTY_API]: {
      endpoint: '/account/reset_password',
      method: 'PUT',
      body: data,
      overlay: true,
      typesPrefix: 'RESET_PASSWORD_FORM',
      formName: 'resetPassword',
      meta: {
        success: {
          analytics: {
            type: 'AUTH',
            data,
            events: ['event109'],
          },
          messageKey: 'Your password has been successfully changed.',
        },
      },
    },
  }
}

const ORDER_HISTORY = '/account/order-history'

export function setOrderHistoryDetails(orderDetails) {
  return {
    type: 'SET_ORDER_HISTORY_DETAILS',
    orderDetails,
  }
}

export function orderHistoryRequest() {
  return {
    [MONTY_API]: {
      endpoint: ORDER_HISTORY,
      method: 'GET',
      overlay: true,
      typesPrefix: 'SET_ORDER_HISTORY',
    },
  }
}

export function orderHistoryDetailsRequest(orderId) {
  return {
    [MONTY_API]: {
      endpoint: `${ORDER_HISTORY}/${orderId}`,
      method: 'GET',
      overlay: true,
      typesPrefix: 'SET_ORDER_HISTORY_DETAILS',
    },
  }
}

const RETURN_HISTORY = '/account/return-history'

export function returnHistoryRequest() {
  return {
    [MONTY_API]: {
      endpoint: RETURN_HISTORY,
      method: 'GET',
      overlay: true,
      typesPrefix: 'SET_RETURN_HISTORY',
    },
  }
}

export function setReturnHistoryDetails(returnDetails) {
  return {
    type: 'SET_RETURN_HISTORY_DETAILS',
    returnDetails,
  }
}

export const returnHistoryDetailsRequest = (orderId, rmaId) => async (
  dispatch
) => {
  try {
    dispatch(ajaxCounter('increment'))
    const { body: returnHistoryDetails } = await dispatch(
      get(`${RETURN_HISTORY}/${orderId}/${rmaId}`)
    )
    dispatch(setReturnHistoryDetails(returnHistoryDetails))
    dispatch(ajaxCounter('decrement'))
  } catch (e) {
    logger.info('server-side-render', 'returnHistoryDetailsRequest', e)
    dispatch(setReturnHistoryDetails({}))
    dispatch(ajaxCounter('decrement'))
  }
}

/**
 * My Checkout Details actions
 */
export function setMyCheckoutDetailsInitialFocus(initialFocus) {
  return {
    type: 'SET_MCD_INITIAL_FOCUS',
    initialFocus,
  }
}

// fill the forms with account.user (Redux) data
export function resetMyCheckoutDetailsForms(user) {
  return function resetMyCheckoutDetailsFormsThunk(dispatch, getState) {
    const state = getState()
    const { siteOptions } = state
    const deliveryCountry = pathOr(
      null,
      ['deliveryDetails', 'address', 'country'],
      user
    )
    const defaultState =
      deliveryCountry === 'United States' ? siteOptions.USStates[0] : null
    const defaultAddress = {
      ...getDefaultAddress(),
      country: deliveryCountry,
      state: defaultState,
    }

    const deliveryAddress = pathOr(
      defaultAddress,
      ['deliveryDetails', 'address'],
      user
    )
    dispatch(
      resetForm(formNames.delivery.address, {
        ...deliveryAddress,
        county: null,
      })
    )

    const deliveryTelephone = pathOr(
      null,
      ['deliveryDetails', 'nameAndPhone', 'telephone'],
      user
    )
    const deliveryNameAndPhone = path(['deliveryDetails', 'nameAndPhone'], user)
    dispatch(
      resetForm(formNames.delivery.details, {
        ...deliveryNameAndPhone,
        telephone: deliveryTelephone,
      })
    )

    const billingAddress = pathOr(
      defaultAddress,
      ['billingDetails', 'address'],
      user
    )
    dispatch(
      resetForm(formNames.billing.address, {
        ...billingAddress,
        county: null,
      })
    )

    const billingTelephone = pathOr(
      null,
      ['billingDetails', 'nameAndPhone', 'telephone'],
      user
    )
    const billingNameAndPhone = path(['billingDetails', 'nameAndPhone'], user)
    dispatch(
      resetForm(formNames.billing.details, {
        ...billingNameAndPhone,
        telephone: billingTelephone,
      })
    )

    dispatch(setFindAddressMode())

    // Payment details
    const { paymentMethods } = state
    const defaultPaymentOptions = getDefaultPaymentOptions(
      paymentMethods,
      siteOptions.expiryMonths,
      siteOptions.expiryYears
    )

    const { type: value, expiryMonth, expiryYear } = user.creditCard
    const paymentMethodTypeForUser = getPaymentMethodTypeByValue(state, value)
    const userCardDetails = isUserCreditCard(user)
      ? {
          ...defaultPaymentOptions,
          paymentType: value,
          expiryMonth:
            paymentMethodTypeForUser === 'OTHER'
              ? defaultPaymentOptions.expiryMonth
              : expiryMonth,
          expiryYear:
            paymentMethodTypeForUser === 'OTHER'
              ? defaultPaymentOptions.expiryYear
              : expiryYear,
        }
      : null
    dispatch(
      resetForm(
        formNames.payment.paymentCardDetails,
        userCardDetails || defaultPaymentOptions
      )
    )
  }
}

/**
 * Action used on MyChekoutDetails to grab user data and fill MyCheckoutDetails forms
 */
export function getMyCheckoutDetailsData() {
  return (dispatch) => {
    dispatch(ajaxCounter('increment'))
    return dispatch(getAccount())
      .then((res) => {
        dispatch(ajaxCounter('decrement'))
        dispatch(resetMyCheckoutDetailsForms(res.payload))
      })
      .catch((err) => {
        dispatch(ajaxCounter('decrement'))
        const message =
          err.response && err.response.body
            ? err.response.body.message
            : 'An error has occured. Please try again.'
        dispatch(
          setFormMessage('myCheckoutDetailsForm', {
            type: 'error',
            message,
          })
        )
      })
  }
}

/**
 * Action that copies form data from source form names to destination form names
 * @param {object} sourceFormNames object with one key per each form name
 * @param {object} destinationFormNames object with one key per each form name
 */
export function setBillingFormsDataFromDeliveryForms(
  sourceFormNames,
  destinationFormNames
) {
  return (dispatch, getState) => {
    const state = getState()
    // sourceFormNames and destinationFormNames objects should have the same keys
    const sourceKeys = (sourceFormNames && Object.keys(sourceFormNames)) || []
    sourceKeys.forEach((formKey) => {
      const currentSourceFormName = sourceFormNames[formKey]
      const currentDestFormName = destinationFormNames[formKey]
      if (currentDestFormName) {
        const form = getMCDAddressForm('delivery', currentSourceFormName, state)
        if ('fields' in form) {
          // we need to resetForm with fields: { fieldName: <value> }
          const newFields = pluck('value', form.fields)
          dispatch(resetForm(currentDestFormName, newFields))
        }
      }
    })
  }
}

/**
 * Action that update user information using the My Checkout Details form
 * @param {*} data object
 * @param {*} formName form name where to write messages (confirm or error)
 */
export function updateMyCheckoutDetails(data, formName) {
  return (dispatch) => {
    dispatch(ajaxCounter('increment'))
    return dispatch(put('/account/customerdetails', data))
      .then(() => {
        return dispatch(getMyCheckoutDetailsData())
      })
      .then(() => {
        dispatch(ajaxCounter('decrement'))
        dispatch(
          setFormMessage(formName, {
            type: 'confirm',
            message: 'Your changes have been saved',
          })
        )
        browserHistory.push('/my-account/details')
      })
      .catch((err) => {
        dispatch(ajaxCounter('decrement'))
        const message =
          err.response && err.response.body
            ? err.response.body.message
            : `An error has occured. Please try again.`
        dispatch(
          setFormMessage(formName, {
            type: 'error',
            message,
          })
        )
      })
  }
}

export function checkAccountExists({
  email = '',
  successCallback,
  failureCallback,
}) {
  return async (dispatch) => {
    try {
      const res = await dispatch(get(`/account?email=${email}`))
      if (successCallback) successCallback(res)
    } catch (e) {
      logger.error(e)
      if (failureCallback) failureCallback(e)
    }
  }
}
