import {
  isStorageSupported,
  getCacheData,
  clearCacheData,
} from '../../../client/lib/storage'
import { getItem } from '../../../client/lib/cookie'
import { getBag, updateShoppingBagBadgeCount } from './shoppingBagActions'
import { setAuthentication } from './authActions'
import { updateMenuForAuthenticatedUser } from './navigationActions'

export function preCacheReset() {
  return {
    type: 'PRE_CACHE_RESET',
  }
}

export function retrieveCachedData() {
  return (dispatch) => {
    const bagCountCookie = getItem('bagCount')
    const authenticated = getItem('authenticated')
    const storageSupport = isStorageSupported()

    // Update Bag Count from cookie
    if (bagCountCookie) {
      const bagTotal = parseInt(bagCountCookie, 10)
      dispatch(updateShoppingBagBadgeCount(bagTotal))
      if (bagTotal > 0) {
        dispatch(getBag())
      } else {
        dispatch({ type: 'NO_BAG' })
      }
    } else {
      dispatch({ type: 'NO_BAG' })
    }

    // Update User Info from local storage or fetch Data
    if (storageSupport && authenticated === 'yes') {
      const data = getCacheData()
      if (data.auth) dispatch({ type: 'RETRIEVE_CACHED_DATA', ...data })
      else dispatch(setAuthentication(true))

      dispatch(updateMenuForAuthenticatedUser())
    } else if (storageSupport) {
      clearCacheData()
    }
  }
}
