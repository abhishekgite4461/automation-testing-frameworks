export function setJsessionid(jsessionid) {
  return {
    type: 'SET_JSESSION_ID',
    jsessionid,
  }
}
