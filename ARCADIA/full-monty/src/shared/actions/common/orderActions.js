import { map, path } from 'ramda'
import { browserHistory } from 'react-router'

import { localise } from '../../lib/localisation'
import { post, put } from '../../lib/api-service'
import { fixEuropeanOrderCompleted } from '../../lib/checkout-utilities/order-summary'
import { KLARNA } from '../../../shared/constants/paymentTypes'
import { createOrder as generateOrder } from '../../lib/checkout'
import {
  assembleFullPayload,
  hashOrderSummary,
} from '../../lib/checkout-utilities/klarna-utils'
import { isFeatureSavePaymentDetailsEnabled } from '../../selectors/featureSelectors'

// actions
import { ajaxCounter } from '../components/LoaderOverlayActions'
import { setFormMessage, resetForm } from '../../actions/common/formActions'
import { updateMenuForAuthenticatedUser } from '../../actions/common/navigationActions'
import { updateShoppingBagBadgeCount } from '../../actions/common/shoppingBagActions'
import { getAccount } from './accountActions'
import { syncClientForEmailExists } from './checkoutActions'
import { reAuthorize, resetKlarna, setOrderSummaryHash } from './klarnaActions'
import { createVisaForm } from '../../lib/checkout-utilities/create-visa-form'
import { sendAnalyticsErrorMessage, ANALYTICS_ERROR } from '../../analytics'
import { setThankyouPageEspots } from '../../actions/common/espot-actions'

export const setOrderConfirmation = (data) => {
  return {
    type: 'SET_ORDER_COMPLETED',
    data,
  }
}

export const setOrderPendingConfirm = (data) => {
  return {
    type: 'SET_ORDER_PENDING',
    data,
  }
}

export const setOrderError = (error) => {
  return {
    type: 'SET_ORDER_ERROR',
    error,
  }
}

const clearCheckoutForms = () => {
  return (dispatch, getState) => {
    dispatch(
      resetForm(
        'billingCardDetails',
        map(() => '', getState().forms.checkout.billingCardDetails.fields)
      )
    )
    dispatch(
      resetForm('giftCard', map(() => '', getState().forms.giftCard.fields))
    )
  }
}

const extractEmail = (orderPayload) => {
  return (
    (orderPayload.accountCreate && orderPayload.accountCreate.email) ||
    (orderPayload.accountLogin && orderPayload.accountLogin.email)
  )
}

const getCreateOrderErrorMessage = (err, getState) => {
  const { language, brandName } = getState().config
  const l = localise.bind(null, language, brandName)
  const body = path(['response', 'body'], err)
  if (body) {
    if (body.statusCode === 504)
      return l`There's been a temporary issue. Please confirm your order again.`
    if (body.message) return body.message
    if (body.validationErrors && body.validationErrors.length)
      return body.validationErrors[0].message
    if (body.originalMessage) return body.originalMessage
  }
  return path(['response', 'text'], err)
}

export const createOrder = (payload) => {
  return (dispatch, getState) => {
    dispatch(ajaxCounter('increment'))
    dispatch(setFormMessage('order', {}))
    dispatch(setOrderError(false)) // reset Order Errors
    dispatch(setOrderPendingConfirm(false)) // reset Order Visa or Paypal

    return dispatch(post(`/order`, payload))
      .then((res) => {
        dispatch({ type: 'EMPTY_SHOPPING_BAG' })
        if (res.body.paypalUrl || res.body.paymentUrl) {
          // supported : Paypal - ALIPAY - CHINAUNION
          return dispatch(getAccount()).then(() => {
            window.location.href = res.body.paypalUrl || res.body.paymentUrl
          })
        } else if (res.body.vbvForm) {
          // Verified By VISA
          return dispatch(getAccount()).then(() => {
            dispatch(setOrderPendingConfirm(createVisaForm(res.body.vbvForm)))
          })
        }

        const paymentType = path(['creditCard', 'type'], payload)
        const paidWithKlarna = paymentType === KLARNA
        if (paidWithKlarna) {
          dispatch(resetKlarna())
          dispatch(setOrderSummaryHash())
        }
        dispatch(ajaxCounter('decrement'))
        return dispatch(getAccount())
          .then(() => {
            // Fix Order-Completed for France billing/delivery SCRAPI BUG
            const { account, siteOptions } = getState()
            const { returnUrl } = payload
            const regOrderCompletePath = /https?:\/\/[\w-.:]+(\/[\w-]+)(\?.+)?/i
            dispatch(
              setOrderConfirmation(
                fixEuropeanOrderCompleted(
                  res.body.completedOrder,
                  account.user,
                  siteOptions.billingCountries
                )
              )
            )
            dispatch(clearCheckoutForms())
            browserHistory.push(regOrderCompletePath.exec(returnUrl)[1])
            dispatch(updateMenuForAuthenticatedUser())
            dispatch(updateShoppingBagBadgeCount(0))
            return dispatch(setThankyouPageEspots(res.body.completedOrder))
          })
          .catch((err) => {
            dispatch(clearCheckoutForms())
            return Promise.reject(err)
          })
      })
      .catch((err) => {
        // note: PAYMENT_DECLINED still creates Account & authenticates Session
        // via the syncClientForEmailExists action below
        const email = extractEmail(payload)
        if (email) dispatch(syncClientForEmailExists(email))
        dispatch(ajaxCounter('decrement'))
        const errorMessage = getCreateOrderErrorMessage(err, getState)
        dispatch(
          setFormMessage('order', {
            type: 'error',
            message: errorMessage,
          })
        )
        dispatch(sendAnalyticsErrorMessage(ANALYTICS_ERROR.CONFIRM_AND_PAY))
        return new Error(errorMessage)
      })
  }
}

export const submitOrder = (dependecies = {}) => (dispatch, getState) => {
  const deps = {
    assembleFullPayload,
    createOrder,
    generateOrder,
    hashOrderSummary,
    reAuthorize,
    ...dependecies,
  }
  const state = getState()
  const {
    account: { user },
    auth,
    checkout: { orderSummary, savePaymentDetails },
    forms: {
      checkout: {
        billingAddress,
        billingCardDetails,
        billingDetails,
        deliveryInstructions,
        yourAddress,
        yourDetails,
      },
    },
    paymentMethods,
  } = state
  const featureSavePaymentDetailsEnabled = isFeatureSavePaymentDetailsEnabled(
    state
  )
  const orderCompletePath = 'order-complete'
  const order = deps.generateOrder({
    auth,
    billingAddress,
    billingCardDetails,
    billingDetails,
    deliveryInstructions,
    orderSummary,
    paymentMethods,
    user,
    yourAddress,
    yourDetails,
    featureSavePaymentDetailsEnabled,
    savePaymentDetails,
    orderCompletePath,
  })
  if (billingCardDetails.fields.paymentType.value === KLARNA) {
    const {
      config,
      klarna: { orderSummaryHash, authorizationToken },
      shoppingBag,
    } = state
    const klarnaOrder = { ...order, authToken: authorizationToken }
    // Compare order summaries, if changed, we need to reauthorize Klarna
    const hash = orderSummaryHash || window.localStorage.getItem('orderHash')
    // Compare hash needs to be built of shoppingBag and not ordersummary bag
    // If user updates bag while on checkout, orderSummary is not updated, only bag
    const newOrderSummaryHash = deps.hashOrderSummary({
      billingAddress,
      billingCardDetails,
      billingDetails,
      orderSummary,
      shoppingBag,
      yourAddress,
      yourDetails,
    })
    if (hash && hash !== newOrderSummaryHash) {
      const fullPayload = deps.assembleFullPayload(false, true, {
        billingDetails,
        billingAddress,
        config,
        orderSummary,
        shoppingBag,
        user,
        yourAddress,
        yourDetails,
        orderCompletePath,
      })
      return deps.reAuthorize(fullPayload, order, true)
    }

    return dispatch(deps.createOrder(klarnaOrder))
  }

  return dispatch(deps.createOrder(order))
}

// called when returning to the site after a completed payment through a 3rd party provider
export const updateOrder = () => {
  return (dispatch, getState) => {
    const { language, brandName } = getState().config
    const l = localise.bind(null, language, brandName)
    const globalError = l`Unable to find order, please check your account`
    const payload = getState().checkout.verifyPayment
    dispatch(ajaxCounter('increment'))
    if (!payload) {
      dispatch(ajaxCounter('decrement'))
      dispatch(setOrderError(globalError))
    } else {
      return dispatch(put(`/order`, payload, false))
        .then(({ body }) => {
          dispatch(setOrderPendingConfirm(false)) // reset Order Visa or Paypal
          dispatch(updateMenuForAuthenticatedUser())
          dispatch(updateShoppingBagBadgeCount(0))
          dispatch(ajaxCounter('decrement'))
          // Fix Order-Completed for France billing/delivery SCRAPI BUG
          const { account, siteOptions } = getState()
          dispatch(
            setOrderConfirmation(
              fixEuropeanOrderCompleted(
                body,
                account.user,
                siteOptions.billingCountries
              )
            )
          )
          dispatch(getAccount())
          return dispatch(setThankyouPageEspots(body))
        })
        .catch((err) => {
          const resp = err.response
          const errMessage = (resp.body && resp.body.message) || resp.text
          dispatch(ajaxCounter('decrement'))
          dispatch(setOrderError(errMessage))
          dispatch(setOrderPendingConfirm(false)) // reset Order Visa or Paypal
        })
    }
  }
}
