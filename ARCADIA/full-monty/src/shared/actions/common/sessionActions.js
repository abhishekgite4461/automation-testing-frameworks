import { logoutRequest } from './authActions'
import { ajaxCounter } from '../components/LoaderOverlayActions'
import { closeMiniBag } from './shoppingBagActions'
import { closeModal } from './modalActions'

export function sessionReset() {
  return {
    type: 'RESET_SESSION_EXPIRED',
  }
}

// The side-effects on session timeour error are centralised on `sessionExpired` now
// All the references to <ErrorSession /> component have removed since it is not required for dual-run
// I would recommend to display a Session Timeout Error modal which on close will perform the redirect
// as opposed as just redirecting without any feedback
// This can be achieved by overriding the loginRequest redirect and dispatching a showModal action below
export function sessionExpired() {
  return (dispatch) => {
    dispatch(ajaxCounter('increment'))
    dispatch({ type: 'SESSION_EXPIRED' })
    dispatch(closeMiniBag())
    dispatch(closeModal())
    return dispatch(logoutRequest('/login')).then(() => {
      dispatch(ajaxCounter('decrement'))
      dispatch(sessionReset())
    })
  }
}
