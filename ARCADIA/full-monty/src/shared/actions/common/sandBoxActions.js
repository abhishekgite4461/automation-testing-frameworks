import { isEmpty, path } from 'ramda'

import { getCacheExpiration } from '../../lib/cacheable-urls'
import superagent from '../../lib/superagent'
import { get } from '../../lib/api-service'
import cmsConsts from '../../constants/cmsConsts'
import { isFeatureHomePageSegmentationEnabled } from '../../selectors/featureSelectors'
import { getViewportMedia } from '../../selectors/viewportSelectors'
import espotsDesktopConstants from '../../constants/espotsDesktop'

const isBrowser = !!process.browser
const prefix = isBrowser
  ? `${window.location.protocol}//${window.location.host}`
  : ':3000'

const isAnEspotError = (contentType, pageNameFromCMS) =>
  contentType === cmsConsts.ESPOT_CONTENT_TYPE &&
  pageNameFromCMS &&
  (pageNameFromCMS.toLowerCase().includes('error') ||
    pageNameFromCMS.includes('404'))

// e.g.:
// getContent({ pathname: '/en/tsuk/.../home' })
// getContent(null, 'home')
// getContent(null, espots.home[0], cmsConsts.ESPOT_CONTENT_TYPE)
//
// In case of error from mrCMS we try to call the getContent a second time to retrieve at least the error404 page to be able
// to show something to the User and not just an error message.
// In the just mentioned scenario the call to getContent is like:
//    getContent(null, 'error404', null, true, 'home')
// We pass as fourth argument the original page identifier so that we able to save it in the store in a way that is retrievable
// from the component. Hence in the store there will be sandbox>pages[home] containing the data of an error404 page.
export const getContent = (
  location,
  cmsPageName,
  contentType,
  secondCallToCMS,
  originalPage,
  { host = prefix } = {},
  forceMobile
) => {
  if (process.browser && location && location.pathname) {
    // In case of server side execution of the action the pathname is already encoded.
    location.pathname = encodeURIComponent(
      decodeURIComponent(location.pathname)
    )
  }

  const { query: { mobileCMSUrl, responsiveCMSUrl, formEmail } = {} } =
    location || {}

  const pageKeyInStore = originalPage || cmsPageName || location.pathname

  return (dispatch, getState) => {
    const {
      config: { storeCode, brandName, siteId },
      viewport: { media: viewportMedia },
    } = getState()

    const queryParams = {
      location,
      storeCode,
      brandName,
      cmsPageName,
      mobileCMSUrl,
      responsiveCMSUrl,
      formEmail,
      viewportMedia,
      siteId,
      forceMobile,
    }

    return superagent
      .get(`${host}/cmscontent`)
      .query(queryParams)
      .cache(
        process.env.NODE_ENV !== 'production'
          ? false
          : getCacheExpiration('hapi', '/cmscontent')
      )
      .then(({ body }) => {
        if (!body || isEmpty(body)) {
          throw new Error('Empty response body from mrCMS')
        }
        if (body.statusCode && !String(body.statusCode).startsWith('2')) {
          throw new Error('Unsuccessful response status code')
        }

        const pageNameFromCMS =
          body.props && body.props.data && body.props.data.pageName
        if (isAnEspotError(contentType, pageNameFromCMS)) {
          throw new Error('Error retrieving CMS content')
        }

        dispatch({
          type: 'SET_SANDBOX_CONTENT',
          key: pageKeyInStore,
          content: body,
        })
      })
      .catch((err) => {
        if (!secondCallToCMS && contentType !== cmsConsts.ESPOT_CONTENT_TYPE) {
          // Trying to display at least a 404 page to the User.
          return dispatch(
            getContent(
              null,
              'error404',
              null,
              true,
              cmsPageName || location.pathname
            )
          )
        }

        dispatch({
          type: 'SET_SANDBOX_CONTENT',
          key: pageKeyInStore,
          content: err.message || err,
        })
      })
  }
}

export const getSegmentedContent = (wcsEndpoint, identifier, cmsPageName) => {
  const getResponsiveCMSUrl = path([
    identifier,
    'EspotContents',
    'cmsMobileContent',
    'responsiveCMSUrl',
  ])
  return (dispatch, getState) => {
    return dispatch(get(wcsEndpoint)).then(({ body }) => {
      return exports.getContent(
        { pathname: encodeURIComponent(`/${getResponsiveCMSUrl(body)}`) },
        null,
        null,
        null,
        cmsPageName
      )(dispatch, getState)
    })
  }
}

// TODO: remove this action and instead call getSegmentedContent directly
//  from Home component when FEATURE_HOME_PAGE_SEGMENTATION is no longer needed
//  - we need this action for now so that we can check the feature flag when
//  the Home component's `needs` are executed on SSR
export const getHomePageContent = () => {
  return (dispatch, getState) => {
    if (
      isFeatureHomePageSegmentationEnabled(getState()) &&
      getViewportMedia(getState()) !== 'mobile'
    ) {
      return exports.getSegmentedContent(
        '/home',
        espotsDesktopConstants.home.mainBody,
        'home'
      )(dispatch, getState)
    }
    return exports.getContent(null, 'home')(dispatch, getState)
  }
}

export const removeContent = (key) => {
  return {
    type: 'REMOVE_SANDBOX_CONTENT',
    key,
  }
}

export const resetContent = () => {
  return {
    type: 'RESET_SANDBOX_CONTENT',
  }
}

export function showTacticalMessage() {
  return {
    type: 'SHOW_TACTICAL_MESSAGE',
  }
}

export function hideTacticalMessage() {
  return {
    type: 'HIDE_TACTICAL_MESSAGE',
  }
}
