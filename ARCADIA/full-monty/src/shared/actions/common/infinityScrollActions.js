import { addToProducts } from './productsActions'
import { browserHistory } from 'react-router'
import { omit } from 'ramda'

export function setInfinityActive() {
  return {
    type: 'SET_INFINITY_ACTIVE',
  }
}

export function setInfinityInactive() {
  return {
    type: 'SET_INFINITY_INACTIVE',
  }
}

export function nextPageInfinity() {
  return {
    type: 'NEXT_PAGE_INFINITY',
  }
}

export function setInfinityPage(page) {
  return {
    type: 'SET_INFINITY_PAGE',
    page,
  }
}

export function clearInfinityPage() {
  return (dispatch, getState) => {
    dispatch(setInfinityPage(1))
    const { pathname, query } = getState().routing.location
    if (query && query.currentPage) {
      browserHistory.replace({
        pathname,
        query: omit(['currentPage'], query),
      })
    }
  }
}

export function hitWaypoint() {
  return (dispatch, getState) => {
    const { isActive } = getState().infinityScroll
    dispatch(setInfinityInactive())
    if (isActive) dispatch(addToProducts())
  }
}

export function preserveScroll(preservedScroll) {
  return {
    type: 'PRESERVE_SCROLL',
    preservedScroll,
  }
}
