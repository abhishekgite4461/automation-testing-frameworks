/* eslint-disable camelcase */
import React from 'react' // TODO get rid of JSX from actions
import { post, put } from '../../lib/api-service'
import { createOrder } from '../../actions/common/orderActions'
import { browserHistory } from 'react-router'
import { ajaxCounter } from '../components/LoaderOverlayActions'
import { omit } from 'ramda'
import { setFormField } from '../common/formActions'
import { showModal, closeModal } from '../common/modalActions'
import Button from '../../components/common/Button/Button'
import { localise } from '../../lib/localisation'
import { scrollElementIntoView } from '../../lib/scroll-helper'
import {
  assembleSessionPayload,
  authorizeRequest,
  reAuthorizeRequest,
  loadFormRequest,
} from '../../lib/checkout-utilities/klarna-utils'
import {
  isKlarnaSessionOutdated,
  calculateOrderSummaryHash,
  getAddressPayload,
} from '../../selectors/klarnaSelectors'
import {
  getShoppingBagOrderId,
  isStoreDelivery,
} from '../../selectors/checkoutSelectors'
import { isKlarnaDefaultPaymentType } from '../../selectors/userSelectors'

// Localization helper
const getLocalise = ({ config: { language, brandName } }) => {
  return localise.bind(null, language, brandName)
}

// Klarna Global Error Content
const closeModalOnClick = (dispatch) => {
  dispatch(closeModal())
}

export const errorContent = (errorMsg, l, dispatch) => (
  <div className="CheckoutContainer-errorMessage">
    <p>{errorMsg}</p>
    <Button clickHandler={() => closeModalOnClick(dispatch)}>{l`Ok`}</Button>
  </div>
)

export function setKlarnaSessionId(sessionId) {
  return {
    type: 'SET_KLARNA_SESSION_ID',
    sessionId,
  }
}

export function setKlarnaClientToken(clientToken) {
  return {
    type: 'SET_KLARNA_CLIENT_TOKEN',
    clientToken,
  }
}

export function setKlarnaAuthorizationToken(authorizationToken) {
  return {
    type: 'SET_KLARNA_AUTHORIZATION_TOKEN',
    authorizationToken,
  }
}

export function setKlarnaFormVisibility(shown) {
  return {
    type: 'SET_KLARNA_FORM_VISIBILITY',
    shown,
  }
}

export function setLoadFormPending(loadFormPending) {
  return {
    type: 'SET_KLARNA_LOADFORM_PENDING',
    loadFormPending,
  }
}

export function setAuthorizePending(authorizePending) {
  return {
    type: 'SET_KLARNA_AUTHORIZE_PENDING',
    authorizePending,
  }
}

export function setReAuthorizePending(reAuthorizePending) {
  return {
    type: 'SET_KLARNA_REAUTHORIZE_PENDING',
    reAuthorizePending,
  }
}

export function setOrderSummaryHash(orderSummaryHash) {
  return {
    type: 'SET_ORDER_SUMMARY_HASH',
    orderSummaryHash,
  }
}

export function setIsUpdatingKlarnaEmail(isUpdatingEmail) {
  return {
    type: 'SET_KLARNA_UPDATING_EMAIL',
    isUpdatingEmail,
  }
}

export function checkAndAuthoriseKlarna() {
  return function checkAndAuthoriseKlarnaThunk(dispatch, getState) {
    const state = getState()
    const storeDeliverySelected = isStoreDelivery(state)
    const klarnaSessionOutdated = isKlarnaSessionOutdated(state)
    const addressPayload = getAddressPayload(state)

    if (klarnaSessionOutdated) {
      const orderId = getShoppingBagOrderId(state)
      const newHash = calculateOrderSummaryHash(state)
      return dispatch(
        // eslint-disable-next-line no-use-before-define
        updateAndAuthorizeV2(
          {
            orderId,
            deliveryType: storeDeliverySelected ? 'store' : 'home',
          },
          addressPayload,
          newHash
        )
      )
    }
    // eslint-disable-next-line no-use-before-define
    return dispatch(authorizeV2(addressPayload))
  }
}

export function loadForm(clientToken, container, updateDetails) {
  return (dispatch, getState) => {
    const state = getState()
    if (state.klarna.loadFormPending) return
    const l = getLocalise(state)
    dispatch(ajaxCounter('increment'))
    dispatch(setLoadFormPending(true))
    return loadFormRequest(clientToken, container, updateDetails)
      .then(() => {
        dispatch(ajaxCounter('decrement'))
        dispatch(setLoadFormPending(false))
        dispatch(setKlarnaFormVisibility(true))
        if (isKlarnaDefaultPaymentType(state)) {
          dispatch(checkAndAuthoriseKlarna())
        }
      })
      .catch(() => {
        dispatch(ajaxCounter('decrement'))
        dispatch(setLoadFormPending(false))
        const errorMsg = l`An error has occured. Please try again.`
        dispatch(
          showModal(errorContent(errorMsg, l, dispatch), {
            mode: 'warning',
            type: 'alertdialog',
          })
        )
      })
  }
}

export function handleDisapproval() {
  return function handleDisapprovalThunk(dispatch, getState) {
    const l = getLocalise(getState())
    const {
      location: { pathname, search },
    } = getState().routing
    const close = () => {
      dispatch(setFormField('billingCardDetails', 'paymentType', 'VISA'))
      dispatch(closeModal())
      const el = document.getElementById('CardDetails')
      if (el) scrollElementIntoView(el, 0)
    }
    const modalContent = (
      <div>
        <p
        >{l`The buy now, pay later payment option is not available for this order. Please select an alternative payment option.`}</p>
        <Button clickHandler={() => close()}>{l`Ok`}</Button>
      </div>
    )
    if (
      !pathname.includes('checkout/payment') &&
      !pathname.includes('checkout/delivery-payment')
    ) {
      browserHistory.push(`/checkout/payment${search}#CardDetails`)
    }
    dispatch(showModal(modalContent), { type: 'alertdialog' })
  }
}

export function createSession(data, update = false) {
  return function createSessionThunk(dispatch, getState) {
    const l = getLocalise(getState())
    dispatch(ajaxCounter('increment'))
    return dispatch(post('/klarna-session', data))
      .then((res) => {
        dispatch(ajaxCounter('decrement'))

        if (res.body && res.body.sessionId) {
          // Create session response
          dispatch(setKlarnaSessionId(res.body.sessionId))
          dispatch(setKlarnaClientToken(res.body.clientToken))
          const clientToken = getState().klarna.clientToken
          if (!update)
            dispatch(
              loadForm(
                clientToken,
                document.querySelector('.KlarnaForm-container')
              )
            )
        }
        return res
      })
      .catch(() => {
        dispatch(ajaxCounter('decrement'))
        const errorMsg = l`An error has occured. Please try again.`
        dispatch(
          showModal(errorContent(errorMsg, l, dispatch), {
            mode: 'warning',
            type: 'alertdialog',
          })
        )
      })
  }
}

export function authorize(updateDetails = {}, redirect = true) {
  return function authorizeThunk(dispatch, getState) {
    if (getState().klarna.authorizePending) return
    const l = getLocalise(getState())
    dispatch(setAuthorizePending(true))
    return authorizeRequest(updateDetails)
      .then((res) => {
        dispatch(setAuthorizePending(false))
        if (res.authorization_token) {
          dispatch(setKlarnaAuthorizationToken(res.authorization_token))
          if (redirect) {
            browserHistory.push('/checkout/summary'.concat(location.search))
          }
        } else {
          throw new Error()
        }
      })
      .catch((err) => {
        if (err && (err.approved === false || err.show_form === false)) {
          dispatch(handleDisapproval())
        } else {
          const errorMsg = l`An error has occured. Please try again.`
          dispatch(
            showModal(errorContent(errorMsg, l, dispatch), {
              mode: 'warning',
              type: 'alertdialog',
            })
          )
        }
        dispatch(setAuthorizePending(false))
      })
  }
}

export function authorizeV2(updateDetails = {}) {
  return authorize(updateDetails, false)
}

export function directKlarnaUpdateSession(data) {
  return function directKlarnaUpdateSessionThunk(dispatch) {
    dispatch(ajaxCounter('increment'))
    return dispatch(put('/klarna-session', data))
      .then(() => {
        dispatch(ajaxCounter('decrement'))
      })
      .catch(() => {
        dispatch(ajaxCounter('decrement'))
        dispatch(handleDisapproval())
      })
  }
}

export function updateAndAuthorize(updateData, authData, newHash) {
  return (dispatch, getState) => {
    // When needed, createSession updates and doesn't create a new session
    // (server-side switch needed because of cookies)
    dispatch(createSession(updateData, true))
      .then(({ body }) => {
        const { config, shoppingBag } = getState()
        // create session update payload for Klarna based on orderlines from WCS
        const sessionPayload = {
          ...assembleSessionPayload({ config, shoppingBag }),
          order_lines: body.orderLines,
        }
        dispatch(directKlarnaUpdateSession(sessionPayload)).then(() => {
          dispatch(authorize(authData))
          dispatch(setOrderSummaryHash(newHash))
        })
      })
      .catch(() => {
        dispatch(handleDisapproval())
      })
  }
}

export function updateAndAuthorizeV2(updateData, authData, newHash) {
  return function updateAndAuthorizeV2Thunk(dispatch, getState) {
    // When needed, createSession updates and doesn't create a new session
    // (server-side switch needed because of cookies)
    return dispatch(createSession(updateData, true))
      .then(({ body }) => {
        const { config, shoppingBag } = getState()
        // create session update payload for Klarna based on orderlines from WCS
        const sessionPayload = {
          ...assembleSessionPayload({ config, shoppingBag }),
          order_lines: body.orderLines,
        }
        return dispatch(directKlarnaUpdateSession(sessionPayload))
      })
      .then(() => {
        return Promise.all([
          dispatch(authorizeV2(authData)),
          dispatch(setOrderSummaryHash(newHash)),
        ])
      })
      .catch(() => {
        dispatch(handleDisapproval())
      })
  }
}

// Reauthorized is only called when customer has changed something
// and has agreed to submit the order --> it submits the order on success
export function reAuthorize(
  updateDetails = {},
  orderPayload,
  shouldCreateOrder = true,
  updateEmailAddress = false
) {
  const detailsNoCallbackLink = omit(['merchant_urls'], updateDetails) // submitOrder needs it, reauthorize doesn't
  return (dispatch, getState) => {
    if (getState().klarna.reAuthorizePending) return
    const {
      location: { search },
    } = getState().routing
    const l = getLocalise(getState())
    dispatch(ajaxCounter('increment'))
    dispatch(setReAuthorizePending(true))
    reAuthorizeRequest(detailsNoCallbackLink)
      .then((res) => {
        dispatch(ajaxCounter('decrement'))
        dispatch(closeModal())
        dispatch(setReAuthorizePending(false))
        dispatch(setIsUpdatingKlarnaEmail(false))
        if (res.authorization_token) {
          // overwrite old auth token in the order payload
          if (updateEmailAddress) {
            dispatch(
              setFormField(
                'account',
                'email',
                updateDetails.billing_address.email
              )
            )
            browserHistory.push('/checkout/summary'.concat(search))
          }
          dispatch(setKlarnaAuthorizationToken(res.authorization_token))
          if (shouldCreateOrder)
            dispatch(
              createOrder({
                ...orderPayload,
                authToken: res.authorization_token,
              })
            )
        } else if (!res.approved || !res.show_form) {
          dispatch(handleDisapproval())
        } else {
          // Update the error when UX/Brands wake up !
          const errorMsg = l`An error has occured. Please try again.`
          dispatch(
            showModal(errorContent(errorMsg, l, dispatch), {
              mode: 'warning',
              type: 'alertdialog',
            })
          )
        }
      })
      .catch((err) => {
        dispatch(ajaxCounter('decrement'))
        dispatch(closeModal())
        dispatch(setReAuthorizePending(false))
        dispatch(setIsUpdatingKlarnaEmail(false))
        if (err && (err.approved === false || err.show_form === false))
          dispatch(handleDisapproval())

        // Update the error when UX/Brands wake up !
        const errorMsg = l`An error has occured. Please try again.`
        dispatch(
          showModal(errorContent(errorMsg, l, dispatch), {
            mode: 'warning',
            type: 'alertdialog',
          })
        )
      })
  }
}

export function setKlarnaEmail(email) {
  return {
    type: 'SET_KLARNA_EMAIL',
    email,
  }
}

export function resetKlarna() {
  return {
    type: 'RESET_KLARNA',
  }
}

export function setKlarnaFormSubmitted(formSubmitted) {
  return {
    type: 'SET_KLARNA_FORM_SUBMITTED',
    formSubmitted,
  }
}
