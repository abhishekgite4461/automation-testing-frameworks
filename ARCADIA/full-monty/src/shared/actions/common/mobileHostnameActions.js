export function setIsMobileHostname(isMobile) {
  return {
    type: 'SET_IS_MOBILE_HOSTNAME',
    isMobile,
  }
}
