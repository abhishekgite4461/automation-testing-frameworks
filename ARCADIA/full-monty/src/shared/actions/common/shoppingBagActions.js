import React from 'react'
import Button from '../../components/common/Button/Button'
import { get, post, put, del } from '../../lib/api-service'
import * as analytics from '../../lib/analytics'
import { updateUniversalVariable } from '../../lib/analytics/qubit-analytics'
import {
  resetForm,
  setFormMeta,
  setFormMessage,
  setFormLoading,
} from '../../actions/common/formActions'
import { setGenericError, setApiError } from './errorMessageActions'
import { showModal, closeModal } from './modalActions'
import { ajaxCounter } from '../components/LoaderOverlayActions'
import { joinQuery } from '../../lib/query-helper'
import { localise } from '../../lib/localisation'
import { getOrderSummary, updateOrderSummaryProduct } from './checkoutActions'
import { setMiniBagEspots } from './espot-actions'
import { getDeliveryStoreDetails } from '../components/StoreLocatorActions'
import {
  sendAnalyticsClickEvent,
  sendAnalyticsErrorMessage,
  ANALYTICS_ERROR,
  GTM_ACTION,
} from '../../analytics'
import { scrollElementIntoView } from '../../lib/scroll-helper'
import { browserHistory } from 'react-router'
import { path, difference } from 'ramda'
import {
  getLocationQuery,
  isInCheckout,
} from '../../selectors/routingSelectors'

import {
  shouldTransferShoppingBag,
  removeTransferShoppingBagParams,
} from '../../lib/transfer-shopping-bag'
import { isFeatureTransferBasketEnabled } from '../../selectors/featureSelectors'
import { isMobile } from '../../selectors/viewportSelectors'
import {
  bagContainsDDPProduct,
  getShoppingBagTotalItems,
  isZeroValueBag,
  isShoppingBagEmpty,
} from '../../selectors/shoppingBagSelectors'
import { isDDPActiveUserPreRenewWindow } from '../../selectors/ddpSelectors'

export function showMiniBagConfirm(isShown = true) {
  return {
    type: 'SHOW_MINIBAG_CONFIRM',
    payload: isShown,
  }
}

export function productsAdded(products, quantity) {
  return {
    type: 'PRODUCTS_ADDED_TO_BAG',
    payload: {
      products,
      quantity,
    },
  }
}

export function setLoadingShoppingBag(isLoading) {
  return {
    type: 'SET_LOADING_SHOPPING_BAG',
    isLoading,
  }
}

export function updateShoppingBagBadgeCount(count) {
  return {
    type: 'UPDATE_SHOPPING_BAG_BADGE_COUNT',
    count,
  }
}

const checkForZeroValueOrder = () => (dispatch, getState) => {
  const { language, brandName } = getState().config
  const l = localise.bind(null, language, brandName)
  if (isZeroValueBag(getState()) && !isShoppingBagEmpty(getState())) {
    const modalHtml = (
      <div className="OrderProducts-modal">
        <p>{l`We are unable to accept a zero value order`}</p>
        <Button clickHandler={() => dispatch(closeModal())}>{l`Ok`}</Button>
      </div>
    )
    dispatch(showModal(modalHtml))
  }
}

export function updateBag(
  bag,
  updateOrderSummary = true,
  shouldUpdateForms = false,
  buyNow = false
) {
  return (dispatch, getState) => {
    if (
      getState().routing.location.pathname.startsWith('/checkout') &&
      updateOrderSummary
    ) {
      dispatch(getOrderSummary({ shouldUpdateBag: false, shouldUpdateForms }))
    }
    const ungroupedProductsCount = bag.products.reduce(
      (prevItemCount, nextItem) => prevItemCount + nextItem.quantity,
      0
    )
    dispatch(updateShoppingBagBadgeCount(ungroupedProductsCount))

    if (bag.total !== '' && bag.subTotal !== '') {
      dispatch({
        type: 'UPDATE_BAG',
        bag,
      }).then(() => {
        dispatch(checkForZeroValueOrder())
      })
      dispatch(getDeliveryStoreDetails())
    }
    if (buyNow) browserHistory.push('/checkout')
    dispatch(updateUniversalVariable())
  }
}

export function setPromotionCode(promotionCode) {
  return {
    type: 'SET_PROMOTION_CODE',
    promotionCode,
  }
}

export function setPromotionCodeConfirmation(promotionCodeConfirmation) {
  return {
    type: 'SET_PROMOTION_CODE_CONFIRMATION',
    promotionCodeConfirmation,
  }
}

export function addPromotionCode({ gtmCategory, errorCallback, ...data }) {
  return (dispatch, getState) => {
    const {
      routing: { location },
    } = getState()
    dispatch(setFormLoading('promotionCode', true))
    dispatch(setFormMessage('promotionCode', {}))
    return dispatch(post('/shopping_bag/addPromotionCode', data))
      .then((res) => {
        dispatch(setFormLoading('promotionCode', false))
        dispatch(updateBag(res.body, false))
        dispatch(resetForm('promotionCode', { promotionCode: '' }))
        dispatch(setFormMeta('promotionCode', 'isVisible', false))
        dispatch(setPromotionCodeConfirmation(true))
        if (location.pathname.match(/\/checkout/)) {
          dispatch(
            getOrderSummary({ shouldUpdateBag: true, shouldUpdateForms: false })
          )
        }
        dispatch(setPromotionCode(null))
        if (gtmCategory) {
          dispatch(
            sendAnalyticsClickEvent({
              category: gtmCategory,
              action: GTM_ACTION.PROMO_CODE_APPLIED,
              label: data.promotionId,
            })
          )
        }
      })
      .catch((err) => {
        dispatch(setFormLoading('promotionCode', false))
        dispatch(
          setFormMessage('promotionCode', {
            type: 'error',
            message: err.response.body.message,
          })
        )

        if (errorCallback) errorCallback(err)
        scrollElementIntoView(document.querySelector('.PromotionCode'))
        dispatch(
          sendAnalyticsErrorMessage(ANALYTICS_ERROR.SHOPPING_BAG_PROMO_CODE)
        )
      })
  }
}

const isPromoCodeAdded = (code, bag) =>
  bag.promotions.some(({ promotionCode }) => promotionCode === code)

// The site supports setting a promotion code by adding ARCPROMO_CODE=<code> as a url parameter.
// Sadly the ScrAPI does not support this.
// when a promocode is sent with the url, it will be store in a cookie (by the hapi middleware) until we can apply it.
// We can only add a promoCode if we have items in the bag. (because of scrAPI)
export const addStoredPromoCode = () => (dispatch, getState) => {
  if (typeof document === 'undefined') return // don't run serverside
  const bag = getState().shoppingBag.bag
  const promoCode = getState().shoppingBag.promotionCode
  if (promoCode && bag.products.length && !isPromoCodeAdded(promoCode, bag)) {
    dispatch(addPromotionCode({ promotionId: promoCode }))
  }
}

export function getBagRequest(getOrderSummary = true) {
  return (dispatch) => {
    dispatch(setLoadingShoppingBag(true))
    return dispatch(get('/shopping_bag/get_items'))
      .then(({ body }) => {
        dispatch(setLoadingShoppingBag(false))
        dispatch(updateBag(body, getOrderSummary))
        dispatch(addStoredPromoCode())
        return dispatch(setMiniBagEspots(body))
      })
      .catch((err) => {
        if (err.response && err.response.body) {
          err.message = err.response.body.message
          dispatch(setGenericError(err))
        } else {
          dispatch(setApiError(err))
        }
        dispatch(setLoadingShoppingBag(false))
      })
  }
}

export function getBag(getOrderSummary = true, isMergeRequest = false) {
  return {
    queue: 'BAG_ORDER_SUMMARY',
    callback: (next, dispatch, getState) => {
      const {
        shoppingBag: { promotionCode, miniBagOpen, bag },
      } = getState()
      if (isMergeRequest)
        dispatch({
          type: 'BAG_MERGE_STARTED',
        })
      return dispatch(getBagRequest(getOrderSummary)).then(() => {
        if (isMergeRequest)
          dispatch({
            type: 'BAG_MERGE_FINISHED',
          })
        if (miniBagOpen) analytics.viewBag(bag)
        if (promotionCode)
          dispatch(addPromotionCode({ promotionId: promotionCode }))
        next()
      })
    },
  }
}

export function openMiniBag(autoClose = false) {
  return (dispatch, getState) => {
    dispatch(closeModal())
    dispatch({
      type: 'OPEN_MINI_BAG',
      autoClose,
    })
    analytics.viewBag(getState().shoppingBag.bag)
    document.dispatchEvent(new Event('openMiniBag'))
  }
}

export function closeMiniBag() {
  return {
    type: 'CLOSE_MINI_BAG',
  }
}

export function toggleMiniBag() {
  return (dispatch, getState) => {
    if (getState().shoppingBag.miniBagOpen) {
      dispatch(closeMiniBag())
    } else {
      dispatch(openMiniBag())
    }
  }
}

export function synchroniseBag(preLoginState) {
  return (dispatch, getState) => {
    const postLoginBagCount = getShoppingBagTotalItems(getState())
    if (
      postLoginBagCount !== getShoppingBagTotalItems(preLoginState) &&
      isInCheckout(getState())
    ) {
      dispatch(exports.getBag(true, true))
    }
  }
}

export function checkForMergedItemsInBag(preLoginState) {
  return (dispatch, getState) => {
    const postLoginBagCount = getShoppingBagTotalItems(getState())
    const preLoginProductCount = getShoppingBagTotalItems(preLoginState)
    const postLoginActiveDDPUser = isDDPActiveUserPreRenewWindow(getState())
    const preLoginDdpInBag = bagContainsDDPProduct(preLoginState)
    const removingDdpFromBag = postLoginActiveDDPUser && preLoginDdpInBag
    const { language, brandName } = getState().config
    const l = localise.bind(null, language, brandName)
    const onCheckout = isInCheckout(getState())

    if (postLoginBagCount === preLoginProductCount && !removingDdpFromBag)
      return
    let previousItemsMessage
    if (postLoginBagCount !== preLoginProductCount) {
      if (postLoginBagCount > 0) {
        previousItemsMessage = onCheckout
          ? l`You have items in your shopping bag from a previous visit. Please review the items in your bag before continuing to payment.`
          : l`You have items in your shopping bag from a previous visit.`
      }
    }

    const ddpMessage =
      removingDdpFromBag &&
      l`Great news! You already have a delivery subscription.`

    if (onCheckout && process.browser) window.scrollTo(0, 0)

    const content = (
      <div className="CheckoutContainer-errorMessage">
        {previousItemsMessage && <p>{previousItemsMessage}</p>}
        {removingDdpFromBag && <p>{ddpMessage}</p>}
        <div className="CheckoutContainer-secondaryButtonGroup">
          {onCheckout ? (
            <Button
              className="CheckoutContainer-close"
              clickHandler={() => dispatch(closeModal())}
            >{l`Ok`}</Button>
          ) : (
            <Button
              className="CheckoutContainer-viewBag"
              clickHandler={() => dispatch(openMiniBag())}
            >{l`View bag`}</Button>
          )}
        </div>
      </div>
    )
    dispatch(showModal(content))
  }
}

function addToBagSuccess(res, successHTML) {
  return (dispatch, getState) => {
    const {
      shoppingBag: { promotionCode },
    } = getState()
    const isMobile = getState().viewport.media === 'mobile'
    const responseText = JSON.parse(res.text)
    dispatch(ajaxCounter('decrement'))
    if (responseText.success === false) {
      throw new Error(responseText.message)
    }
    if (promotionCode) {
      dispatch(addPromotionCode({ promotionId: promotionCode }))
    }
    if (isMobile) dispatch(showModal(successHTML))
    dispatch(updateBag(res.body))
    dispatch(updateUniversalVariable())
  }
}

function buyNowSuccess(res) {
  return (dispatch) => {
    const responseText = JSON.parse(res.text)
    dispatch(ajaxCounter('decrement'))
    if (responseText.success === false) {
      throw new Error(responseText.message)
    }
    dispatch(updateBag(res.body, true, false, true))
  }
}

export function addToBag(
  productId,
  sku,
  quantity = 1,
  successHTML,
  bundleItems = undefined
) {
  return (dispatch, getState) => {
    dispatch(ajaxCounter('increment'))
    const item = {
      productId,
      sku,
      quantity: bundleItems ? undefined : quantity,
      bundleItems,
    }

    const previousBagProducts = getState().shoppingBag.bag.products

    return dispatch(post('/shopping_bag/add_item', item))
      .then((res) => {
        const addedProducts = difference(res.body.products, previousBagProducts)

        dispatch(productsAdded(addedProducts, quantity))

        if (successHTML) {
          dispatch(addToBagSuccess(res, successHTML))
        } else {
          dispatch(buyNowSuccess(res))
        }
        return dispatch(setMiniBagEspots(res.body))
      })
      .catch((err) => {
        if (err.response && err.response.body) {
          err.message = err.response.body.message
          dispatch(setGenericError(err))
        } else {
          dispatch(setApiError(err))
        }
        dispatch(ajaxCounter('decrement'))
        return err
      })
  }
}

export function addToBagWithCatEntryId(catEntryId, successHTML, quantity = 1) {
  return (dispatch, getState) => {
    dispatch(ajaxCounter('increment'))
    const previousBagProducts = getState().shoppingBag.bag.products
    const payload = { catEntryId, quantity }
    const isSmallViewport = isMobile(getState())

    return dispatch(post('/wishlist/add_to_bag', payload))
      .then((res) => {
        const addedProducts = difference(res.body.products, previousBagProducts)

        dispatch(productsAdded(addedProducts, quantity))

        if (successHTML) {
          dispatch(addToBagSuccess(res, successHTML))
        }
        if (!successHTML) {
          dispatch(buyNowSuccess(res))
        }
        if (!isSmallViewport) {
          dispatch(toggleMiniBag())
        }
        return dispatch(setMiniBagEspots(res.body))
      })
      .catch((err) => {
        if (err.response && err.response.body) {
          err.message = err.response.body.message
          dispatch(setGenericError(err))
        } else {
          dispatch(setApiError(err))
        }
        dispatch(ajaxCounter('decrement'))
        return err
      })
  }
}

export function deleteFromBag(orderId, product, successModalText = null) {
  return (dispatch, getState) => {
    const { orderItemId } = product
    const shouldUpdateOrderSummaryForms = !product.inStock
    dispatch(ajaxCounter('increment'))
    return dispatch(
      del(`/shopping_bag/delete_item${joinQuery({ orderId, orderItemId })}`)
    )
      .then((res) => {
        dispatch(ajaxCounter('decrement'))
        const responseText = JSON.parse(res.text)
        if (responseText.success === false) {
          throw new Error(responseText.message)
        }
        dispatch(updateBag(res.body, true, shouldUpdateOrderSummaryForms))
        analytics.bagChange(product, 'scRemove')
        if (successModalText) {
          const { language, brandName } = getState().config
          const l = localise.bind(null, language, brandName)
          const successModalHTML = (
            <div className="OrderProducts-modal">
              <p>{successModalText}</p>
              <Button
                clickHandler={() => dispatch(closeModal())}
              >{l`Ok`}</Button>
            </div>
          )
          dispatch(showModal(successModalHTML))
        }
      })
      .catch(() => {
        dispatch(ajaxCounter('decrement'))
        dispatch(closeModal())
      })
  }
}

export function emptyShoppingBag() {
  return (dispatch, getState) => {
    const products = getState().shoppingBag.bag.products
    return dispatch(del('/shopping_bag/empty_bag'))
      .then(() => {
        dispatch({ type: 'EMPTY_SHOPPING_BAG' })
        products.forEach((product) => analytics.bagChange(product, 'scRemove'))
      })
      .catch((err) => {
        if (err.response && err.response.body) {
          err.message = err.response.body.message
          dispatch(setGenericError(err))
        } else {
          dispatch(setApiError(err))
        }
      })
  }
}

export function updateShoppingBagProduct(index, update) {
  return {
    type: 'UPDATE_SHOPPING_BAG_PRODUCT',
    index,
    update,
  }
}

export function fetchProductItemSizesAndQuantities(index) {
  return (dispatch, getState) => {
    dispatch(ajaxCounter('increment'))
    const catEntryId = path(
      ['shoppingBag', 'bag', 'products', index, 'catEntryId'],
      getState()
    )
    return dispatch(
      get(
        `/shopping_bag/fetch_item_sizes_and_quantities?catEntryId=${catEntryId}`,
        false
      )
    )
      .then(({ body: { items } }) => {
        dispatch(updateShoppingBagProduct(index, { items }))
        dispatch(updateOrderSummaryProduct(index, { items }))
      })
      .catch((err) => {
        if (err.response && err.response.body) {
          err.message = err.response.body.message
          dispatch(setGenericError(err))
        } else {
          dispatch(setApiError(err))
        }
      })
      .then(() => {
        dispatch(ajaxCounter('decrement'))
      })
  }
}

export function persistShoppingBagProduct(index) {
  return (dispatch, getState) => {
    dispatch(ajaxCounter('increment'))
    const product = getState().shoppingBag.bag.products[index]
    const quantity = product.quantitySelected || product.quantity
    const catEntryIdToAdd =
      product.catEntryIdToAdd === undefined
        ? product.catEntryId
        : product.items[product.catEntryIdToAdd].catEntryId

    return dispatch(
      put(
        '/shopping_bag/update_item',
        {
          quantity,
          catEntryIdToDelete: product.catEntryId,
          catEntryIdToAdd,
        },
        false
      )
    )
      .then((res) => {
        dispatch(ajaxCounter('decrement'))
        dispatch(updateBag(res.body))
        const updatedProduct = getState().shoppingBag.bag.products.find(
          (p) => p.catEntryId === catEntryIdToAdd
        )

        if (updatedProduct.quantity > product.quantity) {
          analytics.bagChange(updatedProduct, 'scAdd')
        } else if (updatedProduct.quantity < product.quantity) {
          analytics.bagChange(updatedProduct, 'scRemove')
        }

        if (product.catEntryId !== updatedProduct.catEntryId) {
          analytics.bagChange(updatedProduct, 'event30')
        }

        dispatch(updateShoppingBagProduct(index, { editing: false }))
      })
      .catch((err) => {
        if (err.response && err.response.body) {
          err.message = err.response.body.message
          dispatch(setGenericError(err))
        } else {
          dispatch(setApiError(err))
        }
        dispatch(ajaxCounter('decrement'))
      })
  }
}

export function delPromotionCode(data) {
  return (dispatch) => {
    dispatch(ajaxCounter('increment'))
    return dispatch(del('/shopping_bag/delPromotionCode', data))
      .then((res) => {
        dispatch(ajaxCounter('decrement'))
        dispatch(updateBag(res.body))
      })
      .catch((err) => {
        dispatch(ajaxCounter('decrement'))
        if (err.response && err.response.body) {
          err.message = err.response.body.message
          dispatch(setGenericError(err))
        } else {
          dispatch(setApiError(err))
        }
      })
  }
}

export function changeDeliveryType(payload) {
  return (dispatch) => {
    dispatch(ajaxCounter('increment'))
    return dispatch(put('/shopping_bag/delivery', payload, false))
      .then((res) => {
        dispatch(ajaxCounter('decrement'))
        dispatch(updateBag(res.body))
      })
      .catch((err) => {
        dispatch(ajaxCounter('decrement'))
        if (err.response && err.response.body) {
          err.message = err.response.body.message
          dispatch(setGenericError(err))
        } else {
          dispatch(setApiError(err))
        }
      })
  }
}

const getBagAndRemoveTranferShoppingBagParams = (dispatch, getState) => {
  return dispatch(getBag(false)).finally(() => {
    removeTransferShoppingBagParams(getState())
  })
}

export const initShoppingBag = function initShoppingBagThunk() {
  return function transferShoppingBagThunk(dispatch, getState) {
    const state = getState()
    const featureTransferBasketEnabled = isFeatureTransferBasketEnabled(state)
    const locationQuery = getLocationQuery(state)
    const transferStoreID = window.parseInt(locationQuery.transferStoreID)
    const transferOrderID = window.parseInt(locationQuery.transferOrderID)
    if (
      featureTransferBasketEnabled &&
      shouldTransferShoppingBag(transferStoreID, transferOrderID)
    ) {
      dispatch(
        post('/shopping_bag/transfer', { transferStoreID, transferOrderID })
      ).finally(() => {
        getBagAndRemoveTranferShoppingBagParams(dispatch, getState)
      })
    }
  }
}
