import React from 'react'
import { renderToString } from 'react-dom/server'
import InfoModal from '../../components/common/Modal/InfoModal'

function openModal() {
  // window.scrollY - Modern Way (Chrome, Firefox)
  // window.pageYOffset - Modern IE, including IE11
  return {
    type: 'OPEN_MODAL',
    entryPoint:
      process.env.NODE_ENV === 'production' &&
      process.browser &&
      document.activeElement,
    scrollPositionOnOpenModal: process.browser
      ? window.scrollY || window.pageYOffset
      : null,
  }
}

export function closeModal(shouldScrollToPreviousPosition = true) {
  return {
    type: 'CLOSE_MODAL',
    shouldScrollToPreviousPosition,
  }
}

export function resetScrollPositionOnOpenModal() {
  return {
    type: 'RESET_SCROLL_POSITION_ON_OPEN_MODAL',
  }
}

export function toggleModal() {
  // window.scrollY - Modern Way (Chrome, Firefox)
  // window.pageYOffset - Modern IE, including IE11
  return {
    type: 'TOGGLE_MODAL',
    entryPoint: process.browser && document.activeElement,
    scrollPositionOnOpenModal: process.browser
      ? window.scrollY || window.pageYOffset
      : null,
  }
}

export function setModalMode(mode) {
  return {
    type: 'SET_MODAL_MODE',
    mode,
  }
}

export function setModalType(type) {
  return {
    type: 'SET_MODAL_TYPE',
    modalType: type,
  }
}

export function setModalCancelled(cancelled) {
  return {
    type: 'SET_MODAL_CANCELLED',
    cancelled,
  }
}

export function clearModalChildren() {
  return {
    type: 'CLEAR_MODAL_CHILDREN',
  }
}

export function setModalChildren(children) {
  return {
    type: 'SET_MODAL_CHILDREN',
    children: process.browser ? children : renderToString(children),
  }
}

export function showModal(children, options = {}) {
  const { mode, type } = options
  return (dispatch) => {
    dispatch(setModalCancelled(false))
    dispatch(setModalMode(mode || 'normal'))
    dispatch(setModalChildren(children))
    dispatch(setModalType(type || 'dialog'))
    dispatch(openModal())
  }
}

export function showInfoModal(props, options) {
  return showModal(<InfoModal {...props} />, options)
}
