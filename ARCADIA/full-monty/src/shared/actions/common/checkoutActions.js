import {
  compose,
  head,
  isEmpty,
  path,
  pathOr,
  pickAll,
  pluck,
  isNil,
} from 'ramda'
import { browserHistory } from 'react-router'

// actions
import { getAccount } from './accountActions'
import {
  setFormMessage,
  setFormMeta,
  resetForm,
  resetFormDirty,
  touchedMultipleFormFields,
} from './formActions'
import { updateMenuForAuthenticatedUser } from './navigationActions'
import { updateBag, addStoredPromoCode } from './shoppingBagActions'
import { ajaxCounter } from '../components/LoaderOverlayActions'
import {
  applyFilters,
  getFulfilmentStore,
} from '../components/StoreLocatorActions'
import { setSelectedBrandFulfilmentStore } from '../common/selectedBrandFulfilmentStoreActions'
import {
  sendAnalyticsErrorMessage,
  sendAnalyticsDeliveryOptionChangeEvent,
  sendAnalyticsDeliveryMethodChangeEvent,
  ANALYTICS_ERROR,
} from '../../analytics'

// libs
import { get, put, post, del } from '../../lib/api-service'
import {
  fixOrderSummary,
  removeCFSIFromOrderSummary,
} from '../../lib/checkout-utilities/order-summary'
import { isSelected } from '../../lib/checkout-utilities/delivery-options-utils'
import { localise } from '../../lib/localisation'
import { joinQuery } from '../../lib/query-helper'
import { getItem, setStoreCookie } from '../../../client/lib/cookie'
import {
  getDefaultAddress,
  getDefaultNameAndPhone,
  getDefaultPaymentOptions,
  isUserCreditCard,
  outOfStockMultiLang,
} from '../../lib/checkout-utilities/actions-helpers'
import {
  getErrors,
  getSelectedDeliveryMethod,
  getSelectedDeliveryMethodLabel,
  selectedDeliveryLocationTypeEquals,
  isReturningCustomer,
  shouldUpdateOrderSummaryStore,
  getSelectedDeliveryType,
  getShipModeId,
} from '../../selectors/checkoutSelectors'
import { isUserAuthenticated } from '../../selectors/userSelectors'
import { isFeatureCFSIEnabled } from '../../selectors/featureSelectors'

import { scrollToFormField } from '../../lib/scroll-helper'

import { eventBasedAnalytics } from '../../lib/analytics/analytics'
import { getSelectedBrandFulfilmentStore } from '../../reducers/common/selectedBrandFulfilmentStore'
import { setOrderSummaryEspots } from './espot-actions'

export function updateOrderSummaryProduct(index, update) {
  return (dispatch, getState) => {
    if (
      getState().checkout.orderSummary.basket &&
      getState().checkout.orderSummary.basket.products
    ) {
      dispatch({
        type: 'UPDATE_ORDER_SUMMARY_PRODUCT',
        index,
        update,
      })
    }
  }
}

export function clearCheckoutForms() {
  return {
    type: 'CLEAR_CHECKOUT_FORMS',
  }
}

export function toggleExpressDeliveryOptions(bool) {
  return {
    type: 'TOGGLE_EXPRESS_DELIVERY_OPTIONS_SUMMARY_PAGE',
    bool,
  }
}

export function setPartialOrderSummary(data) {
  return {
    type: 'SET_PARTIAL_ORDER_SUMMARY',
    data,
  }
}

export function setStoreUpdating(updating) {
  return {
    type: 'SET_STORE_UPDATING',
    updating,
  }
}

export function setDeliveryStore(store) {
  return {
    type: 'SET_DELIVERY_STORE',
    store: {
      deliveryStoreCode: path(['storeId'], store),
      storeAddress1: path(['address', 'line1'], store),
      storeAddress2: path(['address', 'line2'], store),
      storeCity: path(['address', 'city'], store),
      storePostcode: path(['address', 'postcode'], store),
    },
  }
}

export function resetStoreDetails() {
  return {
    type: 'RESET_STORE_DETAILS',
  }
}

function setFilters() {
  return function setFiltersThunk(dispatch, getState) {
    const { deliveryLocations } = getState().checkout.orderSummary
    if (deliveryLocations) {
      const selectedDeliveryLocation = deliveryLocations.find(isSelected)
      const filters =
        selectedDeliveryLocation.deliveryLocationType === 'STORE'
          ? ['brand', 'other']
          : ['parcel']
      dispatch(applyFilters(filters))
    }
  }
}

export function setOrderSummary(orderSummary) {
  return function setOrderSummaryThunk(dispatch, getState) {
    const { FEATURE_CFSI } = getState().features.status

    // @TODO to be refactored with a getOrderSummary selector that filters out
    // deliveryMethods if FEATURE_CFSI is disabled
    let hackedOrderSummary = orderSummary
    if (!FEATURE_CFSI)
      hackedOrderSummary = removeCFSIFromOrderSummary(hackedOrderSummary)

    const partialData = isEmpty(hackedOrderSummary)
      ? {}
      : pickAll(['basket', 'deliveryStoreCode'])(hackedOrderSummary)

    dispatch(setPartialOrderSummary(partialData))
    dispatch({
      type: 'FETCH_ORDER_SUMMARY_SUCCESS',
      data: hackedOrderSummary,
    })
    dispatch(setFilters())
    return dispatch(setOrderSummaryEspots(orderSummary))
  }
}

export function clearOrderSummaryBasket() {
  return {
    type: 'FETCH_ORDER_SUMMARY_SUCCESS',
    data: {},
  }
}

export function setOrderSummaryField(field, value) {
  return {
    type: 'SET_ORDER_SUMMARY_FIELD',
    field,
    value,
  }
}

export function setOrderSummaryError(data) {
  return {
    type: 'SET_ORDER_SUMMARY_ERROR',
    data,
  }
}

// @NOTE setSameDeliveryAsBillingFlag is meant to replace setDeliveryAddressToBillingBool
export function setDeliveryAsBillingFlag(val) {
  return {
    type: 'SET_DELIVERY_AS_BILLING_FLAG',
    val,
  }
}

export function resetAddress() {
  return {
    type: 'RESET_SEARCH',
  }
}

export function setManualAddressMode() {
  return {
    type: 'SET_ADDRESS_MODE_TO_MANUAL',
  }
}

export function emptyOrderSummary() {
  return {
    type: 'EMPTY_ORDER_SUMMARY',
  }
}

export function setFindAddressMode() {
  return {
    type: 'SET_ADDRESS_MODE_TO_FIND',
  }
}

export function setMonikerAddress(data) {
  return {
    type: 'UPDATE_MONIKER',
    data,
  }
}

/**
 * resets the Billing form either to the delivery details or the default values
 * @deprecated Use setDeliveryAsBilling
 */
export function setDeliveryAddressToBilling(useDeliveryFormData) {
  return (dispatch, getState) => {
    const {
      checkout: { orderSummary },
      forms: {
        checkout: { yourDetails, yourAddress },
      },
    } = getState()
    const defaultAddress = {
      ...getDefaultAddress(),
      country: orderSummary.shippingCountry,
    }
    const formDeliveryDetails = yourDetails.fields
    const formDeliveryAddress = yourAddress.fields
    dispatch(
      resetFormDirty(
        'billingDetails',
        useDeliveryFormData
          ? pluck('value', formDeliveryDetails)
          : getDefaultNameAndPhone()
      )
    )
    if (!orderSummary.storeDetails) {
      dispatch(
        resetFormDirty(
          'billingAddress',
          useDeliveryFormData
            ? pluck('value', formDeliveryAddress)
            : defaultAddress
        )
      )
    }
  }
}

export const copyDeliveryValuesToBillingForms = () => {
  return (dispatch, getState) => {
    const state = getState()
    const homeDeliverySelected = selectedDeliveryLocationTypeEquals(
      state,
      'HOME'
    )

    // billingDetails
    const {
      forms: {
        checkout: { yourDetails, yourAddress },
      },
    } = state
    const details = pluck('value', yourDetails.fields)
    dispatch(resetFormDirty('billingDetails', details))
    if (homeDeliverySelected) {
      // billingAddress
      const address = pluck('value', yourAddress.fields)
      dispatch(resetFormDirty('billingAddress', address))
      // address mode
      dispatch(setManualAddressMode())
    }
  }
}

export const resetBillingForms = () => {
  return (dispatch, getState) => {
    const state = getState()

    // billingDetails
    const defaultDetails = getDefaultNameAndPhone()
    dispatch(resetForm('billingDetails', defaultDetails))
    // billingAddress
    const {
      checkout: { orderSummary },
    } = state
    const defaultAddress = {
      ...getDefaultAddress(),
      country: orderSummary.shippingCountry,
    }
    dispatch(resetForm('billingAddress', defaultAddress))
  }
}

// @NOTE setDeliveryAsBilling is meant to replace setDeliveryAddressToBilling
export function setDeliveryAsBilling(useDeliveryAsBilling = true) {
  return (dispatch, getState) => {
    const state = getState()
    const {
      checkout: { orderSummary },
      forms: {
        checkout: { yourDetails, yourAddress },
      },
    } = state
    const homeDeliverySelected = selectedDeliveryLocationTypeEquals(
      state,
      'HOME'
    )

    if (useDeliveryAsBilling) {
      // billingDetails
      const formDeliveryDetails = pluck('value', yourDetails.fields)
      dispatch(resetFormDirty('billingDetails', formDeliveryDetails))
      if (homeDeliverySelected) {
        // billingAddress
        const formDeliveryAddress = pluck('value', yourAddress.fields)
        dispatch(resetFormDirty('billingAddress', formDeliveryAddress))
        // address mode
        dispatch(setManualAddressMode())
      }
    } else {
      // billingDetails
      const defaultDetails = getDefaultNameAndPhone()
      dispatch(resetForm('billingDetails', defaultDetails))
      // billingAddress
      const defaultAddress = {
        ...getDefaultAddress(),
        country: orderSummary.shippingCountry,
      }
      dispatch(resetForm('billingAddress', defaultAddress))
    }
  }
}

export function resetCheckoutForms(orderSummary) {
  return (dispatch, getState) => {
    const deliveryTelephone =
      path(
        ['account', 'user', 'deliveryDetails', 'nameAndPhone', 'telephone'],
        getState()
      ) || null
    const billingTelephone =
      path(
        ['account', 'user', 'billingDetails', 'nameAndPhone', 'telephone'],
        getState()
      ) || null
    const { siteOptions, paymentMethods } = getState()
    const { shippingCountry } = orderSummary
    const defaultState =
      shippingCountry === 'United States' ? siteOptions.USStates[0] : null
    const defaultAddress = {
      ...getDefaultAddress(),
      country: orderSummary.shippingCountry,
      state: defaultState,
    }
    const deliveryAddress =
      path(['deliveryDetails', 'address'], orderSummary) || defaultAddress
    dispatch(resetForm('yourAddress', { ...deliveryAddress, county: null }))

    const deliveryNameAndPhone =
      path(['deliveryDetails', 'nameAndPhone'], orderSummary) ||
      getDefaultNameAndPhone()
    dispatch(
      resetForm('yourDetails', {
        ...deliveryNameAndPhone,
        telephone: deliveryTelephone,
      })
    )

    const billingAddress =
      path(['billingDetails', 'address'], orderSummary) || defaultAddress
    dispatch(resetForm('billingAddress', { ...billingAddress, county: null }))
    dispatch(setFindAddressMode())

    const billingNameAndPhone =
      path(['billingDetails', 'nameAndPhone'], orderSummary) ||
      getDefaultNameAndPhone()
    dispatch(
      resetForm('billingDetails', {
        ...billingNameAndPhone,
        telephone: billingTelephone,
      })
    )
    // Payment details
    const {
      account: { user },
    } = getState()
    let userCardDetails = null

    const defaultPaymentOptions = getDefaultPaymentOptions(
      paymentMethods,
      siteOptions.expiryMonths,
      siteOptions.expiryYears
    )

    // scrAPI Paypal users come back with broken card details so we set them to be valid.
    if (isUserCreditCard(user)) {
      const { type, expiryMonth, expiryYear } = user.creditCard
      userCardDetails = {
        ...defaultPaymentOptions,
        paymentType: type,
        expiryMonth:
          type === 'PYPAL' || 'ALIPAY' || 'CUPAY'
            ? defaultPaymentOptions.expiryMonth
            : expiryMonth,
        expiryYear:
          type === 'PYPAL' || 'ALIPAY' || 'CUPAY'
            ? defaultPaymentOptions.expiryYear
            : expiryYear,
      }
    }
    dispatch(
      resetForm('billingCardDetails', userCardDetails || defaultPaymentOptions)
    )
  }
}

export function putOrderSummary(payload) {
  return (dispatch, getState) => {
    const { language, brandName } = getState().config
    const l = localise.bind(null, language, brandName)
    dispatch(ajaxCounter('increment'))
    dispatch(setOrderSummaryError({}))
    return dispatch(put('/checkout/order_summary', payload))
      .then((res) => {
        dispatch(ajaxCounter('decrement'))
        if (res.body.message) {
          dispatch(setOrderSummaryError(res.body))
        } else {
          const { account, siteOptions, config } = getState()
          // WARNING: scrAPI FIX HERE.... fixOrderSummary will 'fix' the malformed address data from scrAPI
          const fixedOrderSummary = fixOrderSummary(
            res.body,
            account.user,
            siteOptions.billingCountries,
            config
          )
          dispatch(updateBag(res.body.basket, false))
          dispatch(setOrderSummary(fixedOrderSummary))
        }
      })
      .catch((err) => {
        const message =
          err.response && err.response.body
            ? err.response.body.message
            : l`An error has occured. Please try again.`
        const isOutOfStock =
          err.response && err.response.body
            ? outOfStockMultiLang(err.response.body.message)
            : false
        dispatch(ajaxCounter('decrement'))
        dispatch(setOrderSummaryError({ message, isOutOfStock }))
      })
  }
}

export function updateDeliveryOptions() {
  return function updateDeliveryOptionsThunk(dispatch, getState) {
    const state = getState()
    const { checkout } = state
    const selectedDeliveryLocation = checkout.orderSummary.deliveryLocations.find(
      isSelected
    )
    let deliveryStore = {}
    if (selectedDeliveryLocation.deliveryLocationType !== 'HOME') {
      if (checkout.deliveryStore) {
        deliveryStore = checkout.deliveryStore
      } else {
        // user has to select a store first
        return Promise.resolve()
      }
    }

    return dispatch(
      putOrderSummary({
        orderId: checkout.orderSummary.basket.orderId,
        deliveryType: getSelectedDeliveryType(state),
        shippingCountry: checkout.orderSummary.shippingCountry,
        shipModeId: getShipModeId(state),
        ...deliveryStore,
      })
    )
  }
}

export function selectDeliveryStore(deliveryStore) {
  return function selectDeliveryStoreThunk(dispatch, getState) {
    const state = getState()
    const isAnonymous = path(
      ['routing', 'location', 'query', 'isAnonymous'],
      state
    )
    const isBrandStore =
      path(['storeId'], deliveryStore) && !deliveryStore.storeId.startsWith('S')

    dispatch(setDeliveryStore(deliveryStore))
    dispatch(setStoreUpdating(false))
    // @NOTE only if it is Collect From Store
    if (isBrandStore) {
      setStoreCookie(deliveryStore)
      dispatch(setSelectedBrandFulfilmentStore(deliveryStore))
    }
    dispatch(updateDeliveryOptions()).then(() => {
      const pathname = isReturningCustomer(state)
        ? '/checkout/delivery-payment'
        : '/checkout/delivery'

      browserHistory.push({
        pathname,
        query: {
          isAnonymous,
        },
      })
    })
  }
}

export function selectDeliveryLocation(index) {
  return (dispatch, getState) => {
    const state = getState()
    const orderSummary = pathOr({}, ['checkout', 'orderSummary'], state)
    const selectedBrandFulfilmentStore = getSelectedBrandFulfilmentStore(state)
    const isCollectFromStore = index === 1

    dispatch(setStoreUpdating(true))
    dispatch(
      setOrderSummary({
        ...orderSummary,
        deliveryLocations: orderSummary.deliveryLocations.map(
          (location, currentIndex) => ({
            ...location,
            selected: index === currentIndex,
          })
        ),
      })
    )

    // @NOTE We need to wipe out the store details so that the user will be forced to reselect a store
    // in the event that the api call to actually select the store failed for some reason
    dispatch(resetStoreDetails())

    if (
      isCollectFromStore &&
      isFeatureCFSIEnabled(state) &&
      !isEmpty(selectedBrandFulfilmentStore)
    ) {
      dispatch(selectDeliveryStore(selectedBrandFulfilmentStore))
    } else {
      dispatch(updateDeliveryOptions())
    }

    dispatch(
      sendAnalyticsDeliveryOptionChangeEvent(
        orderSummary.deliveryLocations[index].deliveryLocationType
      )
    )
  }
}

export function selectDeliveryType(index) {
  return (dispatch, getState) => {
    const { orderSummary } = getState().checkout
    dispatch(
      setOrderSummary({
        ...orderSummary,
        deliveryLocations: orderSummary.deliveryLocations.map((location) => {
          return location.selected
            ? {
                ...location,
                deliveryMethods: location.deliveryMethods.map(
                  (method, currentIndex) => ({
                    ...method,
                    selected: index === currentIndex,
                  })
                ),
              }
            : location
        }),
      })
    )
    dispatch(updateDeliveryOptions())

    dispatch(
      sendAnalyticsDeliveryMethodChangeEvent(
        getSelectedDeliveryMethodLabel(getState())
      )
    )
  }
}

export function forceDeliveryMethodSelection() {
  return (dispatch, getState) => {
    const state = getState()
    const selectedDeliveryMethod = getSelectedDeliveryMethod(state)
    if (!selectedDeliveryMethod || shouldUpdateOrderSummaryStore(state)) {
      return dispatch(updateDeliveryOptions())
    }
  }
}

// Due to a bug in the scrAPI occationally the basket total will wrongly be 0.0
// when this happens we want to fetch the shoppingBag, and then refetch the ordersummary.
// This works for some reason.
export function getOrderSummaryRequest({
  shouldUpdateBag = true,
  shouldUpdateForms = true,
} = {}) {
  return (dispatch, getState) => {
    const { language, brandName } = getState().config
    const l = localise.bind(null, language, brandName)
    dispatch(ajaxCounter('increment'))
    dispatch(setOrderSummaryError({}))

    return dispatch(get('/checkout/order_summary'))
      .then((res) => {
        dispatch(ajaxCounter('decrement'))
        if (res.body.message) {
          dispatch(setOrderSummaryError(res.body))
        } else {
          const { account, siteOptions, config } = getState()
          // WARNING: scrAPI FIX HERE.... fixOrderSummary will 'fix' the malformed address data from scrAPI
          const fixedOrderSummary = fixOrderSummary(
            res.body,
            account.user,
            siteOptions.billingCountries,
            config
          )
          dispatch(addStoredPromoCode()) // It is still possible to have a stored promo code when on checkout.
          dispatch(setOrderSummary(fixedOrderSummary))
          if (shouldUpdateForms) {
            dispatch(resetCheckoutForms(fixedOrderSummary))
          }
          if (shouldUpdateBag) {
            dispatch(updateBag(fixedOrderSummary.basket, false))
          }
          // scrAPI eu returns no selected delivery method
          dispatch(forceDeliveryMethodSelection())
        }
      })
      .catch((err) => {
        dispatch(ajaxCounter('decrement'))
        const message =
          err.response && err.response.body
            ? err.response.body.message
            : l`An error has occured. Please try again.`
        const isOutOfStock =
          err.response && err.response.body
            ? outOfStockMultiLang(err.response.body.message)
            : false
        dispatch(setOrderSummary({}))
        dispatch(setPartialOrderSummary({}))
        dispatch(setOrderSummaryError({ message, isOutOfStock }))

        if (err.response && err.response.body) {
          const { data } = err.response.body
          const { basket } = data || {}

          if (basket) {
            dispatch(updateBag(basket, false))
          }
        }
      })
  }
}

export function getAccountAndOrderSummary() {
  return (dispatch, getState) => {
    return isUserAuthenticated(getState())
      ? dispatch(getAccount()).then(() => dispatch(getOrderSummaryRequest()))
      : dispatch(getOrderSummaryRequest())
  }
}

export function getOrderSummary(args) {
  return (dispatch) =>
    dispatch({
      queue: 'BAG_ORDER_SUMMARY',
      callback: (next, dispatch) => {
        dispatch(getOrderSummaryRequest(args)).then(next)
      },
    })
}

export function selectDeliveryCountry(country) {
  return (dispatch, getState) => {
    dispatch(
      setOrderSummary({
        ...getState().checkout.orderSummary,
        shippingCountry: country,
      })
    )
    dispatch(updateDeliveryOptions())
  }
}

export function getAddressByMoniker({ moniker, country }, formName) {
  const name = formName
  return function getAddressByMonikerThunk(dispatch, getState) {
    dispatch(ajaxCounter('increment'))
    return dispatch(get(`/address/${moniker}?country=${country}`)).then(
      ({ body }) => {
        const isCheckout = /\/checkout(\/.*)/.test(
          getState().routing.location.pathname
        )
        dispatch(ajaxCounter('decrement'))
        dispatch(resetForm(name, body))
        dispatch(setManualAddressMode())
        if (isCheckout) {
          const {
            checkout: { orderSummary },
          } = getState()
          dispatch(setOrderSummary({ ...orderSummary, deliveryDetails: body }))
        }
      },
      (err) => {
        dispatch(ajaxCounter('decrement'))
        dispatch(setFormMessage(name, err.response.body.message))
      }
    )
  }
}

export function findAddress(data, yourAddressFormName, findAddressFormName) {
  return (dispatch, getState) => {
    const { language, brandName } = getState().config
    const l = localise.bind(null, language, brandName)
    const error = l`We are unable to find your address at the moment. Please enter your address manually.`
    dispatch(ajaxCounter('increment'))
    dispatch(setFormMessage(findAddressFormName, ''))
    return dispatch(get(`/address${joinQuery(data)}`))
      .then((res) => {
        dispatch(ajaxCounter('decrement'))
        dispatch(setMonikerAddress(res.body))
        if (res.body.length === 1) {
          const moniker = res.body[0].moniker
          dispatch(
            getAddressByMoniker(
              {
                country: data.country,
                moniker,
              },
              yourAddressFormName
            )
          )
        } else if (!res.body.length) {
          dispatch(setFormMessage(findAddressFormName, error))
        }
      })
      .catch((err) => {
        dispatch(ajaxCounter('decrement'))
        if (err.response && err.response.statusCode === 503) {
          // service unavailable (timeout)
          dispatch(setFormMessage(findAddressFormName, error))
        } else {
          dispatch(
            setFormMessage(
              findAddressFormName,
              (err.response.body && err.response.body.message) || error
            )
          )
        }
      })
  }
}

export function showGiftCardBanner(message) {
  return (dispatch) => {
    return dispatch(setFormMeta('giftCard', 'banner', message))
  }
}

export function hideGiftCardBanner() {
  return (dispatch) => {
    return dispatch(setFormMeta('giftCard', 'banner', false))
  }
}

export function addGiftCard(data) {
  return (dispatch, getState) => {
    dispatch(ajaxCounter('increment'))
    const { language, brandName } = getState().config
    const l = localise.bind(null, language, brandName)
    return dispatch(post('/checkout/gift-card', data, true))
      .then((res) => {
        dispatch(ajaxCounter('decrement'))
        dispatch(
          showGiftCardBanner(l`Thank you, your gift card has been added.`)
        )
        dispatch(setFormMessage('giftCard', { message: null, type: 'error' }))
        dispatch(setOrderSummary(res.body))
        dispatch(resetForm('giftCard', { giftCardNumber: '', pin: '' }))
        eventBasedAnalytics({
          events: 'event127',
        })
      })
      .catch((err) => {
        const message =
          (err.response && err.response.body && l(err.response.body.message)) ||
          l`An error has occured. Please try again.`
        dispatch(ajaxCounter('decrement'))
        dispatch(setFormMessage('giftCard', { message, type: 'error' }))
        eventBasedAnalytics({
          events: 'event128',
        })
        dispatch(sendAnalyticsErrorMessage(ANALYTICS_ERROR.GIFT_CARD_ERROR))
      })
  }
}

export function removeGiftCard(giftCardId) {
  return (dispatch, getState) => {
    dispatch(ajaxCounter('increment'))
    const { language, brandName } = getState().config
    const l = localise.bind(null, language, brandName)
    return dispatch(del(`/checkout/gift-card?giftCardId=${giftCardId}`))
      .then((res) => {
        dispatch(ajaxCounter('decrement'))
        dispatch(hideGiftCardBanner())
        dispatch(setOrderSummary(res.body))
      })
      .catch((err) => {
        const message =
          (err.response && err.response.body && l(err.response.body.message)) ||
          l`An error has occured. Please try again.`
        dispatch(ajaxCounter('decrement'))
        dispatch(setFormMessage('giftCard', { message, type: 'error' }))
      })
  }
}

export const syncClientForEmailExists = (email) => (dispatch) => {
  dispatch(ajaxCounter('increment'))
  return dispatch(get(`/account?email=${email}`))
    .then((resp) => {
      dispatch(ajaxCounter('decrement'))
      if (resp.body.exists) {
        dispatch(updateMenuForAuthenticatedUser())
        return dispatch(getAccount())
      }
    })
    .catch(() => {
      dispatch(ajaxCounter('decrement'))
    })
}

export function setDeliveryEditingEnabled(enabled) {
  return {
    type: 'DELIVERY_AND_PAYMENT_SET_DELIVERY_EDITING_ENABLED',
    enabled,
  }
}

export const validateForms = (
  formNames,
  { onValid = () => {}, onInvalid = () => {} } = {}
) => {
  return (dispatch, getState) => {
    const errors = getErrors(formNames, getState())
    if (isEmpty(errors)) {
      onValid()
    } else {
      Object.keys(errors).forEach((formName) => {
        dispatch(
          touchedMultipleFormFields(formName, Object.keys(errors[formName]))
        )
      })
      // get the first error (using the formNames ordering)
      const firstErroringForm = formNames.find((formName) => formName in errors)
      const erroringField = compose(
        head,
        Object.keys
      )(errors[firstErroringForm])
      scrollToFormField(erroringField)
      onInvalid(errors)
    }
  }
}

export const setSavePaymentDetailsEnabled = function setSavePaymentDetailsEnabled(
  enabled
) {
  return {
    type: 'SET_SAVE_PAYMENT_DETAILS_ENABLED',
    enabled,
  }
}

export function loadFulfilmentStoreFromCookie() {
  return (dispatch, getState) => {
    const state = getState()
    const selectedBrandFulfilmentStore = getSelectedBrandFulfilmentStore(state)
    const pickUpStoreId = getItem('WC_pickUpStore')
    if (isEmpty(selectedBrandFulfilmentStore) && !isNil(pickUpStoreId)) {
      const siteId = pathOr(null, ['config', 'siteId'], state)
      dispatch(
        getFulfilmentStore({
          search: `?store_id=${pickUpStoreId}&brand=${siteId}`,
        })
      )
    }
  }
}
