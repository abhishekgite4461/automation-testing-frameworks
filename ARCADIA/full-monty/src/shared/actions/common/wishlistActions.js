import React from 'react'

import { get, post, del } from '../../lib/api-service'

import { showModal, showInfoModal, toggleModal } from './modalActions'
import { ajaxCounter } from '../components/LoaderOverlayActions'

import { isUserAuthenticated } from '../../selectors/userSelectors'
import {
  getDefaultWishlistId,
  getWishlistItemCount,
  isProductAddedToWishlist,
  getWishlistedItem,
} from '../../selectors/wishlistSelectors'
import { isFeatureWishlistEnabled } from '../../selectors/featureSelectors'
import {
  MAX_WISHLIST_ITEMS,
  MAX_WISHLIST_INFO_MODAL,
} from '../../constants/wishlistConstants'
import { isMobile, getWindowWidth } from '../../selectors/viewportSelectors'

/**
 * Add to Wishlist
 */
const addToWishlistSuccess = (wishlist) => ({
  type: 'ADD_TO_WISHLIST_SUCCESS',
  wishlist,
})

const addToWishlistFailure = () => ({
  type: 'ADD_TO_WISHLIST_FAILURE',
})

const captureWishlistEvent = (type, options = {}) => (dispatch) =>
  dispatch({ type, ...options })

// Temporarily save the catEntry in the store so that it can be added after the user aunthenticates
const storeProductId = (productId) => ({
  type: 'WISHLIST_STORE_PRODUCT_ID',
  productId,
})

const deleteStoredProductId = () => ({
  type: 'WISHLIST_DELETE_STORED_PRODUCT_ID',
})

const setMovingProductToWishlist = (productId) => ({
  type: 'SET_MOVING_PRODUCT_TO_WISHLIST',
  productId,
})

const clearMovingProductToWishlist = () => ({
  type: 'CLEAR_MOVING_PRODUCT_TO_WISHLIST',
})

const addToWishlist = (productId, modifier) => (dispatch, getState) => {
  const wishlistCount = getWishlistItemCount(getState())
  const infoModalProps = {
    infoText: MAX_WISHLIST_INFO_MODAL,
    cancelClick: () => dispatch(toggleModal()),
    cancelText: 'cancel',
    actionLink: '/wishlist',
    actionClick: () => dispatch(toggleModal()),
    actionText: 'go to wishlist',
  }

  if (wishlistCount >= MAX_WISHLIST_ITEMS) {
    return dispatch(showInfoModal(infoModalProps, {})).then(() => {
      dispatch(addToWishlistFailure())
      return Promise.reject('Promise rejected')
    })
  }

  return dispatch(post('/wishlist/add_item', { productId, modifier }))
    .then((res) => {
      return Promise.all([
        dispatch(
          captureWishlistEvent('GA_ADD_TO_WISHLIST', { productId, modifier })
        ),
        dispatch(addToWishlistSuccess(res.body)),
      ])
    })
    .catch(() => {
      // @TODO error handling strategy to be defined
      dispatch(addToWishlistFailure())
      return Promise.reject('Promise rejected')
    })
}

const triggerWishlistLoginModal = (
  productId,
  ModalComponent,
  modifier,
  afterAddToWishlist,
  onCancelLogin
) => (dispatch, getState) => {
  const state = getState()
  const isSmallViewport = isMobile(state)
  const viewportWidth = getWindowWidth(state)
  const mode = isSmallViewport && viewportWidth ? 'rollFull' : 'wishlistLogin'
  if (productId) dispatch(storeProductId(productId))
  return dispatch(
    showModal(
      <ModalComponent
        afterAddToWishlist={afterAddToWishlist}
        modifier={modifier}
        onCancelLogin={onCancelLogin}
      />,
      { mode }
    )
  )
}

const addToWishlistAfterLogin = (pendingProductId, modifier) => (
  dispatch,
  getState
) => {
  const state = getState()

  if (isProductAddedToWishlist(state, pendingProductId)) {
    return Promise.all([
      dispatch(
        captureWishlistEvent('GA_ADD_TO_WISHLIST', {
          productId: pendingProductId,
          modifier,
        })
      ),
      dispatch(deleteStoredProductId()),
    ])
  }
  return dispatch(addToWishlist(pendingProductId, modifier)).then(() =>
    dispatch(deleteStoredProductId())
  )
}

/**
 * Create Wishlist
 */
const createWishlistSuccess = (wishlist) => ({
  type: 'CREATE_WISHLIST_SUCCESS',
  wishlist,
})

const createWishlistFailure = () => ({
  type: 'CREATE_WISHLIST_FAILURE',
})

const createDefaultWishlist = () => (dispatch) => {
  return dispatch(post('/wishlist/create', { wishlistName: 'default' }))
    .then((res) => {
      dispatch(createWishlistSuccess(res.body))
    })
    .catch(() => {
      // @TODO error handling strategy to be defined
      dispatch(createWishlistFailure())
    })
}

/**
 * Get All Wishlists
 */
const getAllWishlistsSuccess = (payload) => ({
  type: 'GET_ALL_WISHLISTS_SUCCESS',
  payload,
})

const getAllWishlistsFailure = () => ({
  type: 'GET_ALL_WISHLISTS_FAILURE',
})

const getAllWishlists = () => (dispatch) => {
  return dispatch(get('/wishlists'))
    .then((res) => {
      dispatch(getAllWishlistsSuccess(res.body))
      return Promise.resolve(res.body)
    })
    .catch(() => {
      // @TODO error handling strategy to be defined
      dispatch(getAllWishlistsFailure())
      return Promise.reject('Promise rejected')
    })
}

/**
 * Get Wishlist
 */
const getWishlistSuccess = (wishlist) => ({
  type: 'GET_WISHLIST_SUCCESS',
  wishlist,
})

const getWishlistFailure = () => ({
  type: 'GET_WISHLIST_FAILURE',
})

const getWishlist = (wishlistId) => (dispatch) => {
  return dispatch(get(`/wishlist/item_ids?wishlistId=${wishlistId}`))
    .then((res) => {
      dispatch(getWishlistSuccess(res.body))
    })
    .catch(() => {
      // @TODO error handling strategy to be defined
      dispatch(getWishlistFailure())
    })
}

const getPaginatedWishlist = (pageNo = 1) => (dispatch, getState) => {
  const state = getState()
  const wishlistId = getDefaultWishlistId(state)

  if (!wishlistId) return dispatch(getWishlistFailure())

  dispatch(ajaxCounter('increment'))
  return dispatch(
    // @NOTE replace MAX_WISHLIST_ITEMS with ITEMS_TO_SHOW constant when working on DES-3599
    get(
      `/wishlist?wishlistId=${wishlistId}&pageNo=${pageNo}&maxItemsPerPage=${MAX_WISHLIST_ITEMS}`
    )
  )
    .then((res) => {
      dispatch(ajaxCounter('decrement'))
      dispatch(getWishlistSuccess(res.body))
    })
    .catch(() => {
      dispatch(ajaxCounter('decrement'))
      // @TODO error handling strategy to be defined
      dispatch(getWishlistFailure())
    })
}

const getDefaultWishlist = () => (dispatch, getState) => {
  const state = getState()

  // @NOTE checking wishlist is enabled because action is used in Main.jsx needs
  return isFeatureWishlistEnabled(state) && isUserAuthenticated(state)
    ? dispatch(getAllWishlists())
        .then((wishlists) => {
          const defaultWishlist = wishlists.find(
            ({ default: isDefault }) => isDefault === 'Yes'
          )
          if (defaultWishlist) {
            return dispatch(getWishlist(defaultWishlist.giftListId))
          }
          return dispatch(createDefaultWishlist())
        })
        .catch(() => {
          // @TODO error handling strategy to be defined
          dispatch(getWishlistFailure())
        })
    : null
}

/**
 * Remove from Wishlist
 */
const removeWishlistProduct = (productId) => ({
  type: 'REMOVE_ITEM_FROM_WISHLIST',
  productId,
})

const removeWishListFailure = () => ({
  type: 'REMOVE_WISHLIST_FAILURE',
})

const removeProductFromWishlist = (productId, modifier) => (
  dispatch,
  getState
) => {
  const state = getState()
  const wishlistId = getDefaultWishlistId(state)
  const { giftListItemId: wishlistItemId } = getWishlistedItem(state, productId)
  if (!wishlistItemId) return dispatch(removeWishListFailure())

  return dispatch(del('/wishlist/remove_item', { wishlistId, wishlistItemId }))
    .then((res) => {
      if (res.statusCode === 200) {
        return Promise.all([
          dispatch(removeWishlistProduct(productId)),
          dispatch(
            captureWishlistEvent('GA_REMOVE_FROM_WISHLIST', {
              productId,
              modifier,
            })
          ),
        ])
      }
      return dispatch(removeWishListFailure())
    })
    .catch(() => {
      // @TODO error handling strategy to be defined
      dispatch(removeWishListFailure())
      return Promise.reject('Promise rejected')
    })
}

const clearWishlist = () => ({
  type: 'CLEAR_WISHLIST',
})

export {
  addToWishlistSuccess,
  addToWishlistFailure,
  addToWishlist,
  addToWishlistAfterLogin,
  storeProductId,
  deleteStoredProductId,
  triggerWishlistLoginModal,
  createWishlistSuccess,
  createWishlistFailure,
  createDefaultWishlist,
  getAllWishlistsSuccess,
  getAllWishlistsFailure,
  getAllWishlists,
  getWishlistSuccess,
  getWishlistFailure,
  getWishlist,
  getPaginatedWishlist,
  getDefaultWishlist,
  removeProductFromWishlist,
  clearWishlist,
  captureWishlistEvent,
  setMovingProductToWishlist,
  clearMovingProductToWishlist,
  // minibagAddToWishlist,
}
