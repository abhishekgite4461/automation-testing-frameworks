import { pathOr } from 'ramda'
import { defaultLanguages } from '../../constants/languages'
import { getLangHostnames } from '../../selectors/configSelectors'
import {
  getTransferBasketParameters,
  shippingDestinationRedirect,
} from '../../lib/change-shipping-destination'

export function setLanguage(language) {
  return {
    type: 'SET_LANGUAGE',
    language,
  }
}

export function setShippingDestination(destination) {
  return (dispatch) => {
    const defaultLanguage = defaultLanguages[destination]
    dispatch({
      type: 'SET_SHIPPING_DESTINATION',
      destination,
    })
    dispatch(setLanguage(defaultLanguage))
  }
}

/* eslint-disable  no-else-return */
export const redirect = (countryName, languageName) => (dispatch, getState) => {
  const state = getState()
  const hostname = pathOr('', ['routing', 'location', 'hostname'], state)
  const optionalParameters = {
    ...getTransferBasketParameters(state),
  }
  shippingDestinationRedirect(
    countryName,
    getLangHostnames(state),
    languageName,
    optionalParameters,
    hostname
  )
}
