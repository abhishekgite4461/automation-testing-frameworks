import createLocation from 'history/lib/createLocation'
import { splitQuery } from '../../lib/query-helper'

function unescaped(search) {
  try {
    return decodeURIComponent(search)
  } catch (ex) {
    return unescape(search)
  }
}

export function updateLocationServer(url, hostname) {
  const baseLocation = createLocation(url)
  const search = unescaped(baseLocation.search)

  return {
    type: 'UPDATE_LOCATION_SERVER',
    location: {
      ...baseLocation,
      hostname,
      protocol: 'https:',
      search,
      query: splitQuery(search),
    },
  }
}

export function urlRedirectServer({ url, permanent = false }) {
  return {
    type: 'URL_REDIRECT_SERVER',
    redirect: {
      url,
      permanent,
    },
  }
}

export function setPageSatusCode(statusCode) {
  return {
    type: 'SET_PAGE_STATUS_CODE',
    statusCode,
  }
}

export function removeFromVisited(index) {
  return {
    type: 'REMOVE_FROM_VISITED',
    index,
  }
}
