export function setPeeriusCallback(callback) {
  return {
    type: 'SET_PEERIUS_CALLBACK',
    callback,
  }
}

export function trackPeerius(track) {
  return {
    type: 'TRACK_PEERIUS',
    track,
  }
}

export function setPeeriusDomain() {
  return (dispatch) => {
    if (process.env.PEERIUS_ENV === 'uat') {
      dispatch({
        type: 'SET_PEERIUS_DOMAIN',
        domain: 'uat',
      })
    }
  }
}
