import React from 'react'
import { range } from 'ramda'

import ProductQuickview from '../../../components/containers/ProductQuickview/ProductQuickview'
import {
  getMockStoreWithInitialReduxState,
  mockStoreCreator,
} from 'test/unit/helpers/get-redux-mock-store'
import { analyticsPdpClickEvent } from '../../../analytics/tracking/site-interactions'

import * as actions from '../productsActions'

jest.mock('../../../analytics/tracking/site-interactions', () => ({
  analyticsPdpClickEvent: jest.fn(),
}))

jest.mock('../../../lib/api-service', () => ({
  get: jest.fn(),
  put: jest.fn(),
  post: jest.fn(),
  del: jest.fn(),
}))
import { get } from '../../../lib/api-service'

jest.mock('../../../lib/products-utils', () => ({
  processRedirectUrl: jest.fn(),
  parseCurrentPage: jest.fn(),
}))
import { processRedirectUrl } from '../../../lib/products-utils'

jest.mock('../../../lib/localisation', () => ({
  localise: jest.fn(() => {
    return 'error'
  }),
}))

jest.mock('../../../lib/get-product-route', () => ({
  getRouteFromUrl: jest.fn(),
}))
import { getRouteFromUrl } from '../../../lib/get-product-route'

const snapshot = (action) => expect(action).toMatchSnapshot()

jest.mock('react-router', () => ({
  browserHistory: {
    replace: jest.fn(),
  },
}))
import { browserHistory } from 'react-router'

import { showModal } from '../modalActions'

jest.mock('../modalActions', () => ({
  showModal: jest.fn(),
}))

describe('productsActions', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  describe('getProducts', () => {
    const initialState = {}
    const store = getMockStoreWithInitialReduxState(initialState)

    beforeEach(() => {
      store.clearActions()
      get.mockImplementation((url) => () => {
        switch (url) {
          case '/products?q=redirect':
            return Promise.resolve({
              // that's been called with dispatch as well
              type: 'MOCK_products',
              body: {
                products: [],
                invetoryPositions: {},
                redirectUrl: 'https://www.tsn.ca',
                total: '5',
                subTotal: '5',
              },
            })
          case '/products?q=ivyPark':
            return Promise.resolve({
              type: 'Mock Products',
              body: {
                product: [],
                invetoryPositions: {},
                permanentRedirectUrl: 'https://www.topshop.com/ivyPark',
                total: '5',
                subTotal: '5',
              },
            })
          default:
            return Promise.resolve({})
        }
      })
    })

    it('makes call to actions', (done) => {
      const location = {
        search: '?q=redirect',
        pathname: '/plp',
        query: {
          q: 'redirect',
        },
      }
      const secondDispatch = store
        .dispatch(actions.getProducts(location))
        .then(() => {
          secondDispatch.then(() => {
            expect(processRedirectUrl).toHaveBeenCalledWith(
              'https://www.tsn.ca'
            )
            done()
          })
          const expectedAction = [
            { page: 1, type: 'SET_INFINITY_PAGE' },
            {
              location: {
                pathname: '/plp',
                query: { q: 'redirect' },
                search: '?q=redirect',
              },
              type: 'SET_PRODUCTS_LOCATION',
            },
            { type: 'REMOVE_PRODUCTS' },
            { loading: 'redirect', type: 'LOADING_PRODUCTS' },
          ]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
          done()
        })
    })
    describe('when permanentRedirectUrl is on body response (WCS redirect update)', () => {
      it('should clear the cached products location and call redirect', (done) => {
        const location = {
          search: '?q=ivyPark',
          pathname: '/plp',
          query: {
            q: 'ivyPark',
          },
        }
        const secondDispatch = store
          .dispatch(actions.getProducts(location))
          .then(() => {
            secondDispatch.then(() => {
              expect(store.getActions()).toEqual(
                expect.arrayContaining([actions.removeProductsLocation()])
              )
              expect(processRedirectUrl).toHaveBeenCalledWith(
                'https://www.topshop.com/ivyPark'
              )
              done()
            })
          })
      })
    })
  })

  it('setProductDetail', () => {
    snapshot(actions.setProductDetail({ productId: '123' }))
  })
  it('setProductQuickview', () => {
    snapshot(actions.setProductQuickview({ productId: '123' }))
  })
  it('setProductIdQuickview', () => {
    snapshot(actions.setProductIdQuickview('123'))
  })
  it('updateActiveItem', () => {
    snapshot(actions.updateActiveItem({ sku: '123' }))
  })
  it('updateActiveItemQuantity', () => {
    const selectedQuantity = 3
    snapshot(actions.updateActiveItemQuantity(selectedQuantity))
    expect(analyticsPdpClickEvent).toHaveBeenCalledWith(
      `productquantity-${selectedQuantity}`
    )
  })
  it('updateSelectedOosItem', () => {
    snapshot(actions.updateSelectedOosItem({ productId: '123' }))
  })
  it('updateShowItemsError', () => {
    snapshot(actions.updateShowItemsError({ error: '123' }))
  })
  it('setSizeGuide(sizeGuideType)', () => {
    const sizeGuideType = 'tops'
    expect(actions.setSizeGuide(sizeGuideType)).toEqual({
      type: 'SET_SIZE_GUIDE',
      sizeGuideType,
    })
  })
  it('showSizeGuide', () => {
    snapshot(actions.showSizeGuide())
  })
  it('hideSizeGuide', () => {
    snapshot(actions.hideSizeGuide())
  })
  it('clearProduct', () => {
    snapshot(actions.clearProduct())
  })
  it('loadingProducts', () => {
    snapshot(actions.loadingProducts('url'))
  })
  it('removeProductsLocation', () => {
    snapshot(actions.removeProductsLocation())
  })
  it('loadingMoreProducts', () => {
    snapshot(actions.loadingMoreProducts(false))
  })
  it('setProductsLocation', () => {
    snapshot(actions.setProductsLocation('location name'))
  })
  it('removeProducts', () => {
    snapshot(actions.removeProducts())
  })
  it('removeProductsLocation', () => {
    snapshot(actions.removeProductsLocation())
  })

  describe('getProduct', () => {
    const responses = {
      '/products/23423': {
        message: 'this is a body',
      },
      '/products/23424': {
        message: 'this is a body',
        sourceUrl: 'url-hash',
      },
      '/products/23425': {
        items: [{ quantity: 3 }, { quantity: 4 }],
      },
      '/products/23426': {
        items: [{ quantity: 0 }, { quantity: 0 }],
      },
      '/products/23427': {
        items: [{ quantity: 4 }],
      },
      '/products/23428': {
        items: [{ quantity: 0 }],
      },
      '/products/30263823': {
        productId: '30263823',
        seeMoreValue: [
          {
            seeMoreLabel: 'Jeans',
            seeMoreLink: '/_/N-2bswZ1xjf',
          },
          {
            seeMoreLabel: 'Spray On Jeans',
            seeMoreLink: '/_/N-2bu3Z1xjf',
          },
        ],
      },
      '/products/TS02G02PNAV': {
        productId: '12113313',
        seeMoreValue: [
          {
            seeMoreLabel: 'Jeans',
            seeMoreLink: '/_/N-2bswZ1xjf',
          },
          {
            seeMoreLabel: 'Spray On Jeans',
            seeMoreLink: '/_/N-2bu3Z1xjf',
          },
        ],
      },
      '/products/BUNDLE_17X01NMUS36X02NMUS': {
        productId: '2432434234',
        seeMoreValue: [
          {
            seeMoreLabel: 'Jeans',
            seeMoreLink: '/_/N-2bswZ1xjf',
          },
          {
            seeMoreLabel: 'Spray On Jeans',
            seeMoreLink: '/_/N-2bu3Z1xjf',
          },
        ],
      },
      '/products?endecaSeoValue=N-2bswZ1xjf': {
        canonicalUrl: 'www.topman.com.arcadiagroup.co.uk/Jeans/_/N-2bswZ1xjf',
      },
      '/products?endecaSeoValue=N-2bu3Z1xjf': {
        canonicalUrl:
          'www.topman.com.arcadiagroup.co.uk/Spray-On-Jeans/_/N-2bu3Z1xjf',
      },
    }
    const initialState = {
      routing: {
        location: {
          pathname: '/path/to/product',
          query: '?this=is&a=query',
        },
      },
    }
    const store = getMockStoreWithInitialReduxState(initialState)

    beforeEach(() => {
      store.clearActions()
      get.mockImplementation((url) => () => {
        if (responses[url]) {
          return Promise.resolve({
            type: 'MOCK_GET',
            body: responses[url],
          })
        }
        return Promise.reject({})
      })
    })

    beforeEach(() => {
      jest.clearAllMocks()
    })

    it('if quickview then it should call setProductQuickview ', (done) => {
      store
        .dispatch(actions.getProduct({ identifier: 23423 }, true))
        .then(() => {
          const expectedAction = [
            {
              product: { message: 'this is a body' },
              type: 'SET_PRODUCT_QUICKVIEW',
            },
          ]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
          done()
        })
    })
    it('if not quickview then it should call setProductDetail, getRouteFromUrl, browserHistory.replace', (done) => {
      getRouteFromUrl.mockImplementation(() => 'aaaa')
      store
        .dispatch(actions.getProduct({ identifier: 23424 }, false))
        .then(() => {
          const expectedAction = [
            {
              product: { message: 'this is a body', sourceUrl: 'url-hash' },
              type: 'SET_PRODUCT',
            },
          ]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
          expect(getRouteFromUrl).toHaveBeenCalledTimes(1)
          expect(getRouteFromUrl).toHaveBeenLastCalledWith('url-hash')
          expect(browserHistory.replace).toHaveBeenCalledTimes(1)
          expect(browserHistory.replace).toHaveBeenLastCalledWith({
            pathname: 'aaaa',
            query: initialState.routing.location.query,
          })
          done()
        })
    })
    it('if get throws an error', (done) => {
      store
        .dispatch(actions.getProduct({ identifier: 999 }, false))
        .then(() => {
          const expectedAction = [
            { product: { success: false }, type: 'SET_PRODUCT' },
          ]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
          done()
        })
    })
    it('if get returns a multi size item with quantity > 0 then it should call updateActiveItem with {}', (done) => {
      store
        .dispatch(actions.getProduct({ identifier: 23425 }, false))
        .then(() => {
          const expectedAction = [
            { activeItem: {}, type: 'UPDATE_ACTIVE_ITEM' },
          ]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
          done()
        })
    })
    it('if get returns a multi size item with quantity = 0 then it should call updateActiveItem with {}', (done) => {
      store
        .dispatch(actions.getProduct({ identifier: 23426 }, false))
        .then(() => {
          const expectedAction = [
            { activeItem: {}, type: 'UPDATE_ACTIVE_ITEM' },
          ]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
          done()
        })
    })
    it('if get returns a single size item with quantity > 0 then it should call updateActiveItem with first item', (done) => {
      store
        .dispatch(actions.getProduct({ identifier: 23427 }, false))
        .then(() => {
          const expectedAction = [
            { activeItem: { quantity: 4 }, type: 'UPDATE_ACTIVE_ITEM' },
          ]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
          done()
        })
    })
    it('if get returns a single size item with quantity = 0 then it should call updateActiveItem with {}', (done) => {
      store
        .dispatch(actions.getProduct({ identifier: 23428 }, false))
        .then(() => {
          const expectedAction = [
            { activeItem: {}, type: 'UPDATE_ACTIVE_ITEM' },
          ]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
          done()
        })
    })

    it('should dispatch `SET_PRODUCT` action with retrieved product', () => {
      const store = mockStoreCreator({
        routing: {
          location: {
            pathname:
              '/en/tmuk/product/black-acid-wash-biker-spray-on-ripped-jeans-7170990',
          },
        },
      })

      return store
        .dispatch(actions.getProduct({ identifier: 30263823 }))
        .then(() => {
          expect(store.getActions()).toEqual(
            expect.arrayContaining([
              {
                type: 'SET_PRODUCT',
                product: {
                  productId: '30263823',
                  seeMoreValue: [
                    {
                      seeMoreLabel: 'Jeans',
                      seeMoreLink: '/_/N-2bswZ1xjf',
                    },
                    {
                      seeMoreLabel: 'Spray On Jeans',
                      seeMoreLink: '/_/N-2bu3Z1xjf',
                    },
                  ],
                },
              },
            ])
          )
        })
    })

    it('handles the scenario where "query.productId" is received as argument instead of "identifier"', () => {
      const store = mockStoreCreator({
        routing: {
          location: {
            pathname: 'whatever',
            query: { productId: 30263823 },
          },
        },
      })

      return store.dispatch(actions.getProduct({})).then(() => {
        expect(store.getActions()).toEqual(
          expect.arrayContaining([
            {
              type: 'SET_PRODUCT',
              product: {
                productId: '30263823',
                seeMoreValue: [
                  {
                    seeMoreLabel: 'Jeans',
                    seeMoreLink: '/_/N-2bswZ1xjf',
                  },
                  {
                    seeMoreLabel: 'Spray On Jeans',
                    seeMoreLink: '/_/N-2bu3Z1xjf',
                  },
                ],
              },
            },
          ])
        )
      })
    })

    it('handles the scenario where "query.partNumber" (Product) is received as argument instead of "identifier"', () => {
      const store = mockStoreCreator({
        routing: {
          location: {
            pathname: 'whatever',
            query: { partNumber: 'TS02G02PNAV' },
          },
        },
      })

      return store.dispatch(actions.getProduct({})).then(() => {
        expect(store.getActions()).toEqual(
          expect.arrayContaining([
            {
              type: 'SET_PRODUCT',
              product: {
                productId: '12113313',
                seeMoreValue: [
                  {
                    seeMoreLabel: 'Jeans',
                    seeMoreLink: '/_/N-2bswZ1xjf',
                  },
                  {
                    seeMoreLabel: 'Spray On Jeans',
                    seeMoreLink: '/_/N-2bu3Z1xjf',
                  },
                ],
              },
            },
          ])
        )
      })
    })

    it('handles the scenario where "query.partNumber" (Bundle) is received as argument instead of "identifier"', () => {
      const store = mockStoreCreator({
        routing: {
          location: {
            pathname: 'whatever',
            query: { partNumber: 'BUNDLE_17X01NMUS36X02NMUS' },
          },
        },
      })

      return store.dispatch(actions.getProduct({})).then(() => {
        expect(store.getActions()).toEqual(
          expect.arrayContaining([
            {
              type: 'SET_PRODUCT',
              product: {
                productId: '2432434234',
                seeMoreValue: [
                  {
                    seeMoreLabel: 'Jeans',
                    seeMoreLink: '/_/N-2bswZ1xjf',
                  },
                  {
                    seeMoreLabel: 'Spray On Jeans',
                    seeMoreLink: '/_/N-2bu3Z1xjf',
                  },
                ],
              },
            },
          ])
        )
      })
    })

    it('should dispatch `UPDATE_SEE_MORE_URL` action with retrieved see more URL', () => {
      const store = mockStoreCreator({
        routing: {
          location: {
            pathname:
              '/en/tmuk/product/black-acid-wash-biker-spray-on-ripped-jeans-7170990',
          },
        },
      })

      return store
        .dispatch(actions.getProduct({ identifier: 30263823 }))
        .then(() => {
          expect(store.getActions()).toEqual(
            expect.arrayContaining([
              {
                type: 'UPDATE_SEE_MORE_URL',
                seeMoreLink: '/_/N-2bswZ1xjf',
                seeMoreUrl:
                  'www.topman.com.arcadiagroup.co.uk/Jeans/_/N-2bswZ1xjf',
              },
              {
                type: 'UPDATE_SEE_MORE_URL',
                seeMoreLink: '/_/N-2bu3Z1xjf',
                seeMoreUrl:
                  'www.topman.com.arcadiagroup.co.uk/Spray-On-Jeans/_/N-2bu3Z1xjf',
              },
            ])
          )
        })
    })

    describe('On failure', () => {
      it('should not trigger api error on products?endecaSeoValue failure', () => {
        get
          .mockImplementationOnce((url) => () =>
            Promise.resolve({
              type: 'MOCK_GET',
              body: responses[url],
            })
          )
          .mockImplementation(() => () => {
            return Promise.reject({
              error: 'Bad Gateway',
              message: 'Error parsing upstream data',
              statusCode: 502,
            })
          })

        const store = mockStoreCreator({
          routing: {
            location: {
              pathname:
                '/en/tmuk/product/black-acid-wash-biker-spray-on-ripped-jeans-7170990',
            },
          },
        })
        return store
          .dispatch(actions.getProduct({ identifier: 30263823 }))
          .then(() => {
            expect(store.getActions()).toEqual([
              {
                type: 'SET_PRODUCT',
                product: {
                  productId: '30263823',
                  seeMoreValue: [
                    {
                      seeMoreLabel: 'Jeans',
                      seeMoreLink: '/_/N-2bswZ1xjf',
                    },
                    {
                      seeMoreLabel: 'Spray On Jeans',
                      seeMoreLink: '/_/N-2bu3Z1xjf',
                    },
                  ],
                },
              },
            ])
          })
      })
    })
  })

  describe('addToProducts', () => {
    const breadcrumbs = { label: 'Shop All Shoes', category: '208492,331499' }
    const location = {
      pathname:
        '/en/tsuk/category/shoes-430/shop-all-shoes-6909322/black/blue/N-8gyZdeoZdepZdgl',
      query: {},
    }
    const products = [
      { productId: 100001 },
      { productId: 100002 },
      { productId: 100003 },
      { productId: 100004 },
    ]
    const refinements = [
      {
        label: 'Colour',
        refinementOptions: [{ value: 'black' }, { value: 'red' }],
      },
    ]

    it('should dispatch action to set seo refinements before fetching products', async () => {
      const store = mockStoreCreator({
        products: {
          location,
          products,
          refinements,
          breadcrumbs,
        },
      })
      const expectedActions = [{ type: 'SET_SEO_REFINEMENTS', refinements }]
      store.clearActions()
      await store.dispatch(actions.addToProducts())
      expect(store.getActions()).toEqual(
        expect.arrayContaining(expectedActions)
      )
    })
  })

  describe('getSeeMoreUrls()', () => {
    const getMockResponses = {
      '/products?endecaSeoValue=N-2bswZ1xjf': {
        canonicalUrl: 'www.topman.com.arcadiagroup.co.uk/Jeans/_/N-2bswZ1xjf',
      },
      '/products?endecaSeoValue=N-2bu3Z1xjf': {
        canonicalUrl:
          'www.topman.com.arcadiagroup.co.uk/Spray-On-Jeans/_/N-2bu3Z1xjf',
      },
    }
    const store = mockStoreCreator()
    beforeEach(() => {
      get.mockImplementation((url) => () =>
        Promise.resolve({
          type: 'MOCK_GET',
          body: getMockResponses[url],
        })
      )
      store.clearActions()
    })

    describe('On incorrect input format', () => {
      it('should return `[]` when `seeMoreValue` has no elements', () => {
        return store.dispatch(actions.getSeeMoreUrls()).then(() => {
          expect(store.getActions()).toEqual([])
        })
      })
      it('should return `[]` and should not break the app when `seeMoreValue` items undefined, null, empty ot with incorrect format', () => {
        return store
          .dispatch(
            actions.getSeeMoreUrls([null, {}, undefined, { prop1: 'prop1' }])
          )
          .then(() => {
            expect(store.getActions()).toEqual([])
          })
      })
      it('should return `[]` and should not break the app when `seeMoreLink` accidentally empty', () => {
        const seeMoreValue = [
          {
            seeMoreLabel: '',
            seeMoreLink: '',
          },
        ]
        return store.dispatch(actions.getSeeMoreUrls(seeMoreValue)).then(() => {
          expect(store.getActions()).toEqual([])
        })
      })
    })

    it('should dispatch `UPDATE_SEE_MORE_URL` action creator with the returned see more url', () => {
      const seeMoreValue = [
        {
          seeMoreLabel: 'Jeans',
          seeMoreLink: '/_/N-2bswZ1xjf',
        },
      ]
      return store.dispatch(actions.getSeeMoreUrls(seeMoreValue)).then(() => {
        expect(store.getActions()).toEqual(
          expect.arrayContaining([
            {
              type: 'UPDATE_SEE_MORE_URL',
              seeMoreLink: '/_/N-2bswZ1xjf',
              seeMoreUrl:
                'www.topman.com.arcadiagroup.co.uk/Jeans/_/N-2bswZ1xjf',
            },
          ])
        )
      })
    })
    it('should dispatch more than one `UPDATE_SEE_MORE_URL` action creator with the returned see more url', () => {
      const seeMoreValue = [
        {
          seeMoreLabel: 'Jeans',
          seeMoreLink: '/_/N-2bswZ1xjf',
        },
        {
          seeMoreLabel: 'Spray On Jeans',
          seeMoreLink: '/_/N-2bu3Z1xjf',
        },
      ]
      return store.dispatch(actions.getSeeMoreUrls(seeMoreValue)).then(() => {
        expect(store.getActions()).toEqual([
          {
            type: 'UPDATE_SEE_MORE_URL',
            seeMoreLink: '/_/N-2bswZ1xjf',
            seeMoreUrl: 'www.topman.com.arcadiagroup.co.uk/Jeans/_/N-2bswZ1xjf',
          },
          {
            type: 'UPDATE_SEE_MORE_URL',
            seeMoreLink: '/_/N-2bu3Z1xjf',
            seeMoreUrl:
              'www.topman.com.arcadiagroup.co.uk/Spray-On-Jeans/_/N-2bu3Z1xjf',
          },
        ])
      })
    })
  })

  it('updateCurrentProducts')

  describe('fetchCombinedProducts() is called on server side when current page is greater than 1', () => {
    const store = getMockStoreWithInitialReduxState({})

    const getMockResponses = {
      '/products?currentPage=1&q=red&pageSize=24': {
        products: range(1, 25),
      },
      '/products?currentPage=2&q=red&pageSize=24': {
        products: range(25, 49),
      },
      '/products?currentPage=1&q=blue&pageSize=24': {
        products: range(1, 25),
      },
      '/products?currentPage=2&q=blue&pageSize=24': {
        products: range(25, 49),
      },
      '/products?currentPage=3&q=blue&pageSize=24': {
        products: range(49, 73),
      },
      '/products?currentPage=1&pageSize=24&category=203984,208523': {
        products: range(1, 25),
      },
      '/products?currentPage=2&pageSize=24&category=203984,208523': {
        products: range(25, 49),
      },
    }

    beforeEach(() => {
      get.mockImplementation((url) => () =>
        Promise.resolve({
          type: 'MOCK_GET_PRODUCTS',
          body: getMockResponses[url],
        })
      )
      store.clearActions()
    })

    it('should return 48 products with a search query and currentPage is 2', (done) => {
      const pathname = '/search/'
      const query = { q: 'red', currentPage: 2, pageSize: 24 }
      const secondDispatch = store
        .dispatch(actions.fetchCombinedProducts(pathname, query))
        .then(({ products }) => {
          secondDispatch.then(() => {
            expect(get).toHaveBeenCalledTimes(2)
            expect(get).toHaveBeenCalledWith(
              '/products?currentPage=1&q=red&pageSize=24'
            )
            expect(get).toHaveBeenCalledWith(
              '/products?currentPage=2&q=red&pageSize=24'
            )
            expect(products).toHaveLength(48)
            done()
          })
        })
    })

    it('should return 48 products with a category selected and currentPage is 2', (done) => {
      const pathname = '/en/tsuk/category/clothing-427/dresses-442'
      const query = {
        currentPage: '2',
        q: undefined,
        category: '203984,208523',
      }
      const secondDispatch = store
        .dispatch(actions.fetchCombinedProducts(pathname, query))
        .then(({ products }) => {
          secondDispatch.then(() => {
            expect(get).toHaveBeenCalledTimes(2)
            expect(get).toHaveBeenCalledWith(
              '/products?currentPage=1&pageSize=24&category=203984,208523'
            )
            expect(get).toHaveBeenCalledWith(
              '/products?currentPage=2&pageSize=24&category=203984,208523'
            )
            expect(products).toHaveLength(48)
            done()
          })
        })
    })

    it('should return max limit of 72 products (3 pages). If currentPage is greater than 3', (done) => {
      const pathname = '/search/'
      const query = { q: 'blue', currentPage: 10, pageSize: 24 }
      const secondDispatch = store
        .dispatch(actions.fetchCombinedProducts(pathname, query))
        .then(({ products }) => {
          expect(products.length).toEqual(72)
          secondDispatch.then(() => {
            expect(get).toHaveBeenCalledTimes(3)
            expect(get).toHaveBeenCalledWith(
              '/products?currentPage=1&q=blue&pageSize=24'
            )
            expect(get).toHaveBeenCalledWith(
              '/products?currentPage=2&q=blue&pageSize=24'
            )
            expect(get).toHaveBeenCalledWith(
              '/products?currentPage=3&q=blue&pageSize=24'
            )
            done()
          })
        })
    })
  })

  describe('getServerProducts', () => {
    const findAction = (store, type) => {
      return store.getActions().find((action) => action.type === type)
    }

    const mockGetProductsResponse = (body) =>
      get.mockImplementation(() => () =>
        Promise.resolve({
          type: 'MOCK_GET',
          body,
        })
      )

    it('does not dispatch redirect when permanent redirect URL is not present', () => {
      mockGetProductsResponse({})
      const store = getMockStoreWithInitialReduxState({})
      return store
        .dispatch(actions.getServerProducts({ search: '', pathname: '/path' }))
        .then(() => {
          expect(findAction(store, 'URL_REDIRECT_SERVER')).toBeUndefined()
          expect(findAction(store, 'SET_ERROR')).toBeUndefined()
        })
    })

    it('does dispatch redirect when permanent redirect URL is present', () => {
      const store = getMockStoreWithInitialReduxState({})
      const pathname = '/canonical-path-name'
      const permanentRedirectUrl = `http://some-host.com${pathname}`
      mockGetProductsResponse({ permanentRedirectUrl })

      return store
        .dispatch(
          actions.getServerProducts({ search: '', pathname: '/something-else' })
        )
        .then(() => {
          const redirectToUrlAction = findAction(store, 'URL_REDIRECT_SERVER')
          expect(redirectToUrlAction.redirect).toEqual({
            url: pathname,
            permanent: true,
          })
          expect(findAction(store, 'SET_ERROR')).toBeUndefined()
        })
    })
  })

  it('emailMeStock')

  it('getCategoryFromBreadcrumbs')

  it('getCategoryFromState')

  describe('fetchProducts', () => {
    const getMockResponses = {
      '/products': {
        canonicalUrl: 'www.topman.com.arcadiagroup.co.uk/Jeans/_/N-2bswZ1xjf',
        refinements: 'AAAAA',
      },
      '/products/seo?seoUrl=/products&pageSize=24': {
        canonicalUrl: 'www.topman.com.arcadiagroup.co.uk/Jeans/_/N-2bswZ1xjf',
        refinements: 'AAAAA',
        breadcrumbs: [
          {
            category: 'AAAAA',
          },
        ],
      },
    }
    const initialState = {
      refinements: {
        selectedOptions: {
          price: [10, 40],
          size: [4, 6, 8],
        },
      },
      sorting: {
        currentSortOption: 'BBBB',
      },
    }
    const store = getMockStoreWithInitialReduxState(initialState)

    beforeEach(() => {
      get.mockImplementation((url) => () =>
        Promise.resolve({
          type: 'MOCK_GET',
          body: getMockResponses[url],
        })
      )
      store.clearActions()
    })
    it('should handle request with category', (done) => {
      const pathname = '/products'
      const query = { category: '1234', currentPage: 3, pageSize: 24 }
      const secondDispatch = store
        .dispatch(actions.fetchProducts(pathname, query))
        .then(() => {
          secondDispatch.then(() => {
            expect(get).toHaveBeenCalledTimes(1)
            expect(get).toHaveBeenCalledWith(
              '/products?currentPage=3&pageSize=24&refinements=%5B%7B%22key%22%3A%22price%22%2C%22value%22%3A%2210%2C40%22%7D%2C%7B%22key%22%3A%22size%22%2C%22value%22%3A%224%2C6%2C8%22%7D%5D&sort=BBBB&category=1234'
            )
            done()
          })
        })
    })
    it('should handle request with q', (done) => {
      const pathname = '/products'
      const query = { q: 'shoes', currentPage: 3, pageSize: 24 }
      const secondDispatch = store
        .dispatch(actions.fetchProducts(pathname, query))
        .then(() => {
          secondDispatch.then(() => {
            expect(get).toHaveBeenCalledTimes(1)
            expect(get).toHaveBeenCalledWith(
              '/products?currentPage=3&q=shoes&pageSize=24&refinements=%5B%7B%22key%22%3A%22price%22%2C%22value%22%3A%2210%2C40%22%7D%2C%7B%22key%22%3A%22size%22%2C%22value%22%3A%224%2C6%2C8%22%7D%5D&sort=BBBB'
            )
            done()
          })
        })
    })

    describe('should handle legacy requests', () => {
      const pathname =
        '/webapp/wcs/stores/servlet/CatalogNavigationSearchResultCmd'

      it('should set the correct fields', (done) => {
        const initialState = {
          sorting: {
            currentSortOption: '',
          },
        }
        const testStore = getMockStoreWithInitialReduxState(initialState)
        const query = {
          currentPage: 3,
          pageSize: 20,
          someProperty: true,
          categoryId: '3213',
          searchTerm: 'Red Jeans',
          sort_field: 'Price%20Ascending',
        }
        const secondDispatch = testStore
          .dispatch(actions.fetchProducts(pathname, query))
          .then(() => {
            secondDispatch.then(() => {
              expect(get).toHaveBeenCalledTimes(1)
              expect(get).toHaveBeenCalledWith(
                '/products?currentPage=3&pageSize=20&someProperty=true&searchTerm=Red%20Jeans&sort=Price%20Ascending&category=3213'
              )
              done()
            })
          })
      })
      it('should override sort_field, with sort', (done) => {
        const query = {
          currentPage: 3,
          pageSize: 20,
          someProperty: true,
          categoryId: '3213',
          searchTerm: 'Red Jeans',
          sort_field: 'Price%20Ascending',
        }
        const secondDispatch = store
          .dispatch(actions.fetchProducts(pathname, query))
          .then(() => {
            secondDispatch.then(() => {
              expect(get).toHaveBeenCalledTimes(1)
              expect(get).toHaveBeenCalledWith(
                '/products?currentPage=3&pageSize=20&someProperty=true&searchTerm=Red%20Jeans&sort=BBBB&refinements=%5B%7B%22key%22%3A%22price%22%2C%22value%22%3A%2210%2C40%22%7D%2C%7B%22key%22%3A%22size%22%2C%22value%22%3A%224%2C6%2C8%22%7D%5D&category=3213'
              )
              done()
            })
          })
      })
    })
    describe('fetchProductsBySeo', () => {
      const expectedUrl = '/products/seo?seoUrl=/products&pageSize=24'
      it('should set seo refinements from body', (done) => {
        const pathname = '/products'
        const query = {}
        const secondDispatch = store
          .dispatch(actions.fetchProducts(pathname, query))
          .then(() => {
            secondDispatch.then(() => {
              expect(get).toHaveBeenCalledTimes(1)
              expect(get).toHaveBeenCalledWith(expectedUrl)
              expect(store.getActions()).toEqual([
                {
                  type: 'SET_SEO_REFINEMENTS',
                  refinements: 'AAAAA',
                },
              ])
              done()
            })
          })
      })

      it('should call fetchProducts if with have refinementsQuery', (done) => {
        const pathname = '/products'
        const query = { refinements: 'AAAA' }
        const secondDispatch = store
          .dispatch(actions.fetchProducts(pathname, query))
          .then(() => {
            secondDispatch.then(() => {
              expect(get).toHaveBeenCalledTimes(2)
              expect(get).toHaveBeenCalledWith(expectedUrl)
              expect(store.getActions()).toEqual([
                {
                  type: 'SET_SEO_REFINEMENTS',
                  refinements: 'AAAAA',
                },
              ])
              done()
            })
          })
      })

      it('should call fetchProducts if currentPage > 1', (done) => {
        const pathname = '/products'
        const query = { currentPage: 3 }
        const secondDispatch = store
          .dispatch(actions.fetchProducts(pathname, query))
          .then(() => {
            secondDispatch.then(() => {
              expect(get).toHaveBeenCalledTimes(2)
              expect(get).toHaveBeenCalledWith(expectedUrl)
              expect(store.getActions()).toEqual([
                {
                  type: 'SET_SEO_REFINEMENTS',
                  refinements: 'AAAAA',
                },
              ])
              done()
            })
          })
      })

      it('should call fetchProducts if sorted', (done) => {
        const pathname = '/products'
        const query = { sort: 'true' }
        const secondDispatch = store
          .dispatch(actions.fetchProducts(pathname, query))
          .then(() => {
            secondDispatch.then(() => {
              expect(get).toHaveBeenCalledTimes(2)
              expect(get).toHaveBeenCalledWith(expectedUrl)
              expect(store.getActions()).toEqual([
                {
                  type: 'SET_SEO_REFINEMENTS',
                  refinements: 'AAAAA',
                },
              ])
              done()
            })
          })
      })
    })
  })
  it('showQuickviewModal', () => {
    expect(showModal).toHaveBeenCalledTimes(0)
    actions.showQuickviewModal()
    expect(showModal).toHaveBeenCalledTimes(1)
    expect(showModal).toHaveBeenCalledWith(<ProductQuickview />, {
      mode: 'plpQuickview',
    })
  })

  it('isSameList')
  it('fixRefinements')
})
