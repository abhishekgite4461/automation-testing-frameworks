import configureMockStore from '../../../../../test/unit/lib/configure-mock-store'
import * as analytics from '../../../lib/analytics/analytics'
import * as qubitEvent from '../../../lib/analytics/qubit-analytics'
import * as acc from '../analyticsActions'
import * as pageData from '../../../lib/analytics/page-data'
import * as checkoutSelectors from '../../../selectors/checkoutSelectors'

const snapshot = (action) => expect(action).toMatchSnapshot()

describe('Analytics Actions', () => {
  beforeAll(() => {
    global.s = {
      Util: {
        cookieWrite: jest.fn(),
      },
    }
  })

  it('setAdobeAnalyticsEnv(environment)', () => {
    snapshot(acc.setAdobeAnalyticsEnv('foo'))
  })
  it('setAdobeAnalyticsEnv(environment)', () => {
    snapshot(acc.percentagePageViewed(87))
  })
  it('setInternalNavigation(intNav)', () => {
    snapshot(
      acc.setInternalNavigation({
        type: 'Footer',
        details: 'Footer:Gift card',
      })
    )
  })
  it('clearInternalNavigation()', () => {
    snapshot(acc.clearInternalNavigation())
  })
})
describe('Analytics Actions and Reducers', () => {
  const isReturningCustomerSpy = jest.spyOn(
    checkoutSelectors,
    'isReturningCustomer'
  )

  beforeAll(() => {
    global.s = {
      Util: {
        cookieWrite: jest.fn(),
      },
    }
  })

  afterEach(() => {
    isReturningCustomerSpy.mockReset()
  })

  afterAll(() => {
    isReturningCustomerSpy.mockRestore()
  })

  describe('setAdobeAnalyticsEnv', () => {
    it('should update environment in state', () => {
      const store = configureMockStore({
        adobe: {
          environment: 'stage',
        },
      })
      expect(store.getState().adobe.environment).toBe('stage')
      store.dispatch(acc.setAdobeAnalyticsEnv('production'))
      expect(store.getState().adobe.environment).toBe('production')
    })
    it('should set environment in state', () => {
      const store = configureMockStore({
        adobe: {},
      })
      expect(store.getState().adobe.environment).toBeUndefined()
      store.dispatch(acc.setAdobeAnalyticsEnv('production'))
      expect(store.getState().adobe.environment).toBe('production')
    })
  })
  describe('percentagePageViewed', () => {
    it('should update percentagePageViewed in state', () => {
      const store = configureMockStore({
        adobe: {
          percentagePageViewed: 28,
        },
      })
      expect(store.getState().adobe.percentagePageViewed).toBe(28)
      store.dispatch(acc.percentagePageViewed(76))
      expect(store.getState().adobe.percentagePageViewed).toBe(76)
    })
    it('should set environment in state', () => {
      const store = configureMockStore({
        adobe: {
          percentagePageViewed: {},
        },
      })
      expect(store.getState().adobe.percentagePageViewed).toEqual({})
      store.dispatch(acc.percentagePageViewed(76))
      expect(store.getState().adobe.percentagePageViewed).toBe(76)
    })
  })

  const newIntNav = {
    type: 'Global Nav',
    details: 'Global Nav:Dresses',
  }
  it('setInternalNavigation should update intNav in state', () => {
    const store = configureMockStore({
      adobe: {
        intNav: {
          type: 'Search',
        },
      },
    })
    expect(store.getState().adobe.intNav).toEqual({
      type: 'Search',
    })
    store.dispatch(acc.setInternalNavigation(newIntNav))
    expect(store.getState().adobe.intNav).toEqual(newIntNav)
  })
  it('setInternalNavigation should set intNav in state', () => {
    const store = configureMockStore({
      adobe: {
        intNav: {},
      },
    })
    expect(store.getState().adobe.intNav).toEqual({})
    store.dispatch(acc.setInternalNavigation(newIntNav))
    expect(store.getState().adobe.intNav).toBe(newIntNav)
  })

  describe('pageLoaded', () => {
    analytics.pageBasedAnalytics = jest.fn()
    qubitEvent.dispatchQubitEventHook = jest.fn()

    const actualPageData = pageData.default
    pageData.default = {
      home: {
        getData: () => ({
          pageName: 'Home Page',
          pageType: 'Home Page',
          granularPageName: 'Home Page',
          firstCategory: 'Home Page',
          eVar3: 'foo',
          prop2: 'bar',
        }),
      },
      'checkout-login': {
        getData: () => ({
          pageName: 'Checkout/Logon',
          granularPageName: 'Checkout/Logon',
          firstCategory: 'Checkout',
          isCheckout: true,
        }),
      },
    }
    const dispatchMock = jest.fn()
    const getStateMock = jest.fn(() => ({
      adobe: {
        intNav: newIntNav,
        percentagePageViewed: 86,
      },
      debug: {
        buildInfo: 'HELLO',
      },
      routing: {
        visited: ['/', '/another/page'],
      },
    }))

    afterEach(() => {
      analytics.pageBasedAnalytics.mockClear()
      qubitEvent.dispatchQubitEventHook.mockClear()
      dispatchMock.mockClear()
    })

    afterAll(() => {
      analytics.pageBasedAnalytics.mockRestore()
      qubitEvent.dispatchQubitEventHook.mockRestore()
      pageData.default = actualPageData
    })

    it('should call `pageBasedAnalytics`, with `eVar87=Returning`if returning customer', () => {
      isReturningCustomerSpy.mockReturnValue(true)
      const getState = () => ({
        adobe: {
          percentagePageViewed: 86,
        },
        routing: {
          visited: ['/'],
        },
        debug: {
          buildInfo: 'AAAAA',
        },
        features: {
          status: {
            FEATURE_NEW_CHECKOUT: true,
          },
        },
        checkout: {
          checkoutVersion: 'V2',
        },
      })
      const pageLoadedAction = acc.pageLoaded('checkout-login', 2431, true)
      pageLoadedAction(dispatchMock, getState)
      const data = analytics.pageBasedAnalytics.mock.calls[0][0]
      expect(data.eVar87).toBe('Returning')
      expect(qubitEvent.dispatchQubitEventHook).not.toBeCalled()
    })

    it('should not call `pageBasedAnalytics`, with added adobe data if sendAdobe is false', () => {
      const pageLoadedAction = acc.pageLoaded('home', 2431, false)
      pageLoadedAction(dispatchMock, getStateMock)
      expect(analytics.pageBasedAnalytics).not.toHaveBeenCalled()
    })

    it('should dispatch `PAGE_LOADED` action, with `page` and `loadTime`', () => {
      const pageLoadedAction = acc.pageLoaded('home', 2431, true)
      pageLoadedAction(dispatchMock, getStateMock)
      const { type, payload } = dispatchMock.mock.calls[1][0]
      expect(type).toBe('PAGE_LOADED')
      expect(payload).toEqual(
        expect.objectContaining({
          pageName: 'home',
          loadTime: 2431,
        })
      )
    })

    it('should dispatch `clearInternalNavigation`', () => {
      const pageLoadedAction = acc.pageLoaded('home', 2431, true)
      pageLoadedAction(dispatchMock, getStateMock)
      const { type } = dispatchMock.mock.calls[0][0]
      expect(type).toBe('CLEAR_INT_NAV')
    })
  })
})
