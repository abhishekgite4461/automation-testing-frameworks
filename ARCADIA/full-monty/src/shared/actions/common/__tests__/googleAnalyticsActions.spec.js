import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { sendOrderCompleteEvent } from '../googleAnalyticsActions'

jest.mock('../../../../client/lib/cookie/utils', () => ({
  getItem: jest.fn((x) => {
    const cookies = {
      source: 'CoreAPI',
      traceId2: 'traceIdMock',
      ['dual-run']: 'monty', // eslint-disable-line
    }
    return cookies[x]
  }),
}))

const baseState = {
  config: {
    storeCode: 'storeCodeMock',
  },
  debug: {
    buildInfo: {
      tag: 'tagMock',
    },
  },
}

const initialProcessBrowser = process.browser
const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

beforeEach(() => {
  global.process.browser = true
  jest.clearAllMocks()
})

afterEach(() => {
  global.process.browser = initialProcessBrowser
})

describe(sendOrderCompleteEvent.name, () => {
  beforeEach(() => {
    jest.mock()
  })

  it('send a "Order Complete: Success" event if the order is successful', () => {
    const store = mockStore(baseState)
    const orderError = false
    const orderCompleted = {
      paymentDetails: [{ paymentMethod: 'paymentMethodMock' }],
      totalOrderPrice: '100.00',
      deliveryPrice: '10.00',
    }
    store.dispatch(sendOrderCompleteEvent(orderCompleted, orderError))
    expect(store.getActions()).toEqual([
      {
        event: {
          custom: {
            brandCode: 'storeCodeMock',
            buildVersion: 'tagMock',
            source: 'CoreAPI',
            traceId: 'traceIdMock',
          },
          eventAction: 'Order Complete: Success',
          eventCategory: 'Checkout',
          eventLabel: 'paymentMethodMock',
          eventValue: '90.00',
        },
        type: 'GOOGLE_ANALYTICS_SEND_EVENT',
      },
    ])
  })
  it('send a "Order Complete: Success" event if the order is successful, 0.00 price', () => {
    const store = mockStore(baseState)
    const orderError = false
    const orderCompleted = {
      paymentDetails: [{ paymentMethod: 'paymentMethodMock' }],
      totalOrderPrice: '0.00',
      deliveryPrice: '0.00',
    }
    store.dispatch(sendOrderCompleteEvent(orderCompleted, orderError))
    expect(store.getActions()).toEqual([
      {
        event: {
          custom: {
            brandCode: 'storeCodeMock',
            buildVersion: 'tagMock',
            source: 'CoreAPI',
            traceId: 'traceIdMock',
          },
          eventAction: 'Order Complete: Success',
          eventCategory: 'Checkout',
          eventLabel: 'paymentMethodMock',
          eventValue: '0.00',
        },
        type: 'GOOGLE_ANALYTICS_SEND_EVENT',
      },
    ])
  })
  it('send a "Order Complete: Failure" event if the order fails', () => {
    const store = mockStore(baseState)
    const orderError = 'Order error mock'
    const orderCompleted = {}
    store.dispatch(sendOrderCompleteEvent(orderCompleted, orderError))
    expect(store.getActions()).toEqual([
      {
        event: {
          custom: {
            brandCode: 'storeCodeMock',
            buildVersion: 'tagMock',
            source: 'CoreAPI',
            traceId: 'traceIdMock',
          },
          eventAction: 'Order Complete: Failure',
          eventCategory: 'Checkout',
          eventLabel: undefined,
          eventValue: 'Order error mock',
        },
        type: 'GOOGLE_ANALYTICS_SEND_EVENT',
      },
    ])
  })
})
