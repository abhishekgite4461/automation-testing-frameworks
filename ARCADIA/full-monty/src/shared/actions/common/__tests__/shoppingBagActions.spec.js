import React from 'react'
import { assocPath, identity } from 'ramda'
import * as actions from '../shoppingBagActions'
import {
  mockStoreCreator,
  getMockStoreWithInitialReduxState,
} from 'test/unit/helpers/get-redux-mock-store'

// mocks
import { setGenericError, setApiError } from '../errorMessageActions'
import { post, get, put, del } from '../../../lib/api-service'
import { browserHistory } from 'react-router'
import { updateUniversalVariable } from '../../../lib/analytics/qubit-analytics'
import { getDeliveryStoreDetails } from '../../components/StoreLocatorActions'
import { getOrderSummary } from '../checkoutActions'
import * as modalActions from '../modalActions'
import { bagChange, viewBag } from '../../../lib/analytics/shoppingBag'
import {
  shouldTransferShoppingBag,
  removeTransferShoppingBagParams,
} from '../../../lib/transfer-shopping-bag'
import { isDDPActiveUserPreRenewWindow } from '../../../selectors/ddpSelectors'
import {
  bagContainsDDPProduct,
  getShoppingBagTotalItems,
  isZeroValueBag,
  isShoppingBagEmpty,
} from '../../../selectors/shoppingBagSelectors'
import * as espotActions from '../../common/espot-actions/espot-actions-by-page'
import { isMobile } from '../../../selectors/viewportSelectors'
import {
  isInCheckout,
  getLocationQuery,
} from '../../../selectors/routingSelectors'

jest.mock('../../../selectors/routingSelectors', () => ({
  isInCheckout: jest.fn().mockReturnValue(false),
  getLocationQuery: jest.fn().mockReturnValue({}),
}))
jest.mock('../../../selectors/viewportSelectors', () => ({
  isMobile: jest.fn(),
}))
jest.mock('../../../selectors/ddpSelectors', () => ({
  isDDPActiveUserPreRenewWindow: jest.fn().mockReturnValue(false),
}))
jest.mock('../../../selectors/shoppingBagSelectors', () => ({
  bagContainsDDPProduct: jest.fn().mockReturnValue(false),
  getShoppingBagTotalItems: jest.fn().mockReturnValue(1),
  isShoppingBagEmpty: jest.fn().mockReturnValue(false),
  isZeroValueBag: jest.fn().mockReturnValue(false),
}))

jest.mock('../errorMessageActions', () => ({
  setGenericError: jest.fn(),
  setApiError: jest.fn(),
}))

jest.spyOn(espotActions, 'setMiniBagEspots').mockImplementation(() => ({
  type: 'SET_ESPOT_CONTENT',
}))

jest.mock('../../../lib/api-service', () => ({
  post: jest.fn(),
  get: jest.fn(),
  put: jest.fn(),
  del: jest.fn(),
}))
jest.mock('react-router', () => ({
  browserHistory: {
    push: jest.fn(),
  },
}))
jest.mock('../../../lib/analytics/qubit-analytics', () => ({
  updateUniversalVariable: jest.fn(),
}))
jest.mock('../../components/StoreLocatorActions', () => ({
  getDeliveryStoreDetails: jest.fn(),
}))
jest.mock('../checkoutActions', () => ({
  getOrderSummary: jest.fn(),
}))
jest.mock('../../../lib/analytics/shoppingBag', () => ({
  bagChange: jest.fn(),
  submitPromotionCode: jest.fn(),
  viewBag: jest.fn(),
}))
jest.mock('../../../lib/transfer-shopping-bag', () => ({
  shouldTransferShoppingBag: jest.fn(),
  removeTransferShoppingBagParams: jest.fn(),
}))
jest.mock('../../../lib/localisation', () => ({
  localise: jest.fn().mockImplementation((l, b, m) => m),
}))

const getState = jest.fn()
const dispatch = jest.fn()

const getModalContent = (
  store // TODO merge with DES-4061
) => store.getActions().find((a) => a.type === 'SET_MODAL_CHILDREN').children

describe('Shopping Bag Actions', () => {
  const initialState = {
    routing: { location: { pathname: '/login' } },
  }
  const store = getMockStoreWithInitialReduxState(initialState)
  beforeEach(() => {
    // action creators mocks
    // for those external functions which don't return a plain action creator
    // best idea is to mock them all returning a valid action creator { type: 'xxx' }
    getDeliveryStoreDetails.mockImplementation(() => ({
      type: 'MOCK_getDeliveryStoreDetails',
    }))
    updateUniversalVariable.mockImplementation(() => ({
      type: 'MOCK_updateUniversalVariable',
    }))
    getOrderSummary.mockImplementation(() => ({ type: 'MOCK_getOrderSummary' }))
    setGenericError.mockImplementation(() => ({ type: 'MOCK_setGenericError' }))
    setApiError.mockImplementation(() => ({ type: 'MOCK_setApiError' }))
    // that's default post implementation,
    // if we need to reject then implement then use jest.mockImplementationOnce in that specific test
    post.mockImplementation((url) => () => {
      switch (url) {
        case '/shopping_bag/addPromotionCode':
          return Promise.resolve({
            body: {
              products: [],
              invetoryPositions: {},
              total: '',
              subTotal: '',
            },
          })
        case '/shopping_bag/add_item':
          return Promise.resolve({
            text: '{"success" : true}',
            body: {
              products: [
                {
                  lineNumber: 'KSJ78213',
                  name: 'cool product',
                  quantity: 1,
                },
                {
                  lineNumber: 'KSFS0213',
                  name: 'cool second product',
                  quantity: 1,
                },
              ],
            },
          })
        default:
          return Promise.resolve({})
      }
    })
    get.mockImplementation((url) => () => {
      switch (url) {
        case '/shopping_bag/get_items':
          return Promise.resolve({
            // that's been called with dispatch as well
            type: 'MOCK_get_items',
            body: {
              products: [],
              invetoryPositions: {},
              total: '5',
              subTotal: '5',
            },
          })
        default:
          return Promise.resolve({})
      }
    })
    del.mockImplementation((url) => () => {
      switch (true) {
        case url.indexOf('/shopping_bag/delete_item') >= 0:
          return Promise.resolve({
            text: '{"success" : true}',
            body: {
              products: [{}],
            },
          })
        case url === '/shopping_bag/empty_bag':
          return Promise.resolve({
            body: 'hello',
          })
        case url === '/shopping_bag/delPromotionCode':
          return Promise.resolve({
            type: 'MOCK_delPromotionCode',
            body: {
              products: [],
              invetoryPositions: {},
              total: '',
              subTotal: '',
            },
          })
        default:
          return Promise.resolve({})
      }
    })
    jest.clearAllMocks()
    // important to clearActions to avoid concatenation
    store.clearActions()
  })

  describe('Action Creators', () => {
    describe('showMiniBagConfirm', () => {
      it('should create an action to show mini bag confirm (default true)', () => {
        expect(actions.showMiniBagConfirm(undefined)).toEqual({
          type: 'SHOW_MINIBAG_CONFIRM',
          payload: true,
        })
      })
      it('should create an action to show mini bag confirm (true)', () => {
        expect(actions.showMiniBagConfirm(true)).toEqual({
          type: 'SHOW_MINIBAG_CONFIRM',
          payload: true,
        })
      })
      it('should create an action to show mini bag confirm (false)', () => {
        expect(actions.showMiniBagConfirm(false)).toEqual({
          type: 'SHOW_MINIBAG_CONFIRM',
          payload: false,
        })
      })
    })
    describe('productsAdded', () => {
      it('should create an action to notify products have been added to bag', () => {
        const products = [{ productId: 'super modern jeans', size: 6 }]
        const quantity = 2
        expect(actions.productsAdded(products, quantity)).toEqual({
          type: 'PRODUCTS_ADDED_TO_BAG',
          payload: {
            products,
            quantity,
          },
        })
      })
    })
    describe('setLoadingShoppingBag', () => {
      it('should create an action to set loading shopping bad (true)', () => {
        expect(actions.setLoadingShoppingBag(true)).toEqual({
          type: 'SET_LOADING_SHOPPING_BAG',
          isLoading: true,
        })
      })
      it('should create an action to set loading shopping bad (false)', () => {
        expect(actions.setLoadingShoppingBag(false)).toEqual({
          type: 'SET_LOADING_SHOPPING_BAG',
          isLoading: false,
        })
      })
    })
    describe('updateShoppingBagBadgeCount', () => {
      it('should create an action to update shopping bag badge counter', () => {
        const count = 250
        expect(actions.updateShoppingBagBadgeCount(count)).toEqual({
          type: 'UPDATE_SHOPPING_BAG_BADGE_COUNT',
          count,
        })
      })
    })
    describe('setPromotionCode', () => {
      it('should create an action to set promotion code', () => {
        const promotionCode = '50OFF'
        expect(actions.setPromotionCode(promotionCode)).toEqual({
          type: 'SET_PROMOTION_CODE',
          promotionCode,
        })
      })
    })
    describe('setPromotionCodeConfirmation', () => {
      it('should create an action to set promotion code confirmation', () => {
        const promotionCodeConfirmation = 'confirmed.'
        expect(
          actions.setPromotionCodeConfirmation(promotionCodeConfirmation)
        ).toEqual({
          type: 'SET_PROMOTION_CODE_CONFIRMATION',
          promotionCodeConfirmation,
        })
      })
    })
  })

  describe('updateBag', () => {
    it('should call getDeliveryStoreDetails when bag.total and bag.subTotal is not empty', () => {
      const bag = {
        products: [],
        invetoryPositions: {},
        total: '5',
        subTotal: '5',
      }
      store.dispatch(actions.updateBag(bag))
      const expectedAction = [{ type: 'MOCK_getDeliveryStoreDetails' }]
      expect(store.getActions()).toEqual(expect.arrayContaining(expectedAction))
    })

    it('should push /chekout into browserHistory when buyNow is true', () => {
      const mockPush = browserHistory.push
      const bag = {
        products: [],
        invetoryPositions: {},
        total: '',
        subTotal: '',
      }
      const buyNow = true
      store.dispatch(actions.updateBag(bag, false, false, buyNow))
      expect(mockPush).toHaveBeenCalledWith('/checkout')
    })

    it('should update shopping bag badge count with number of products', () => {
      const bag = {
        products: [
          { product: 'p1', quantity: 5 },
          { product: 'p2', quantity: 2 },
        ],
        invetoryPositions: {},
        total: '',
        subTotal: '',
      }
      store.dispatch(actions.updateBag(bag))
      const expectedAction = [
        { type: 'UPDATE_SHOPPING_BAG_BADGE_COUNT', count: 7 },
      ]
      expect(store.getActions()).toEqual(expect.arrayContaining(expectedAction))
    })

    it('should call getOrderSumary when routing is /checkout and updateOrderSummary is true', () => {
      const initialState = {
        routing: { location: { pathname: '/checkout' } },
      }
      // avoid create store for each test, unless we need portion to be different
      const store = getMockStoreWithInitialReduxState(initialState)
      const bag = {
        products: [],
        invetoryPositions: {},
        total: '5',
        subTotal: '5',
      }
      const updateOrderSummary = true
      store.dispatch(actions.updateBag(bag, updateOrderSummary))
      const expectedAction = [{ type: 'MOCK_getOrderSummary' }]
      expect(store.getActions()).toEqual(expect.arrayContaining(expectedAction))
    })

    it('should display message when bag is zero and bag is not empty', () => {
      isZeroValueBag.mockReturnValueOnce(true)
      isShoppingBagEmpty.mockReturnValueOnce(false)
      const bag = {
        products: [],
        invetoryPositions: {},
        total: '1',
        subTotal: '0',
      }
      const updateOrderSummary = true
      return store
        .dispatch(actions.updateBag(bag, updateOrderSummary))
        .then(() => {
          const expectedAction = [
            {
              type: 'SET_MODAL_CHILDREN',
              children: expect.any(String),
            },
          ]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
          expect(isZeroValueBag).toHaveBeenCalledTimes(1)
          expect(isShoppingBagEmpty).toHaveBeenCalledTimes(1)
          expect(getModalContent(store)).toMatch(
            /We are unable to accept a zero value order/
          )
        })
    })
    it('should not display message when bag is zero and bag is empty', () => {
      isZeroValueBag.mockReturnValueOnce(true)
      isShoppingBagEmpty.mockReturnValueOnce(true)
      const bag = {
        products: [],
        invetoryPositions: {},
        total: '0',
        subTotal: '0',
      }
      const updateOrderSummary = true
      return store
        .dispatch(actions.updateBag(bag, updateOrderSummary))
        .then(() => {
          const expectedAction = [
            {
              type: 'SET_MODAL_CHILDREN',
              children: expect.any(String),
            },
          ]
          expect(store.getActions()).not.toEqual(
            expect.arrayContaining(expectedAction)
          )
          expect(isZeroValueBag).toHaveBeenCalledTimes(1)
          expect(isShoppingBagEmpty).toHaveBeenCalledTimes(1)
        })
    })
    it('should not display message when bag is non-zero', () => {
      isZeroValueBag.mockReturnValueOnce(false)
      const bag = {
        products: [],
        invetoryPositions: {},
        total: '1',
        subTotal: '0',
      }
      const updateOrderSummary = true
      return store
        .dispatch(actions.updateBag(bag, updateOrderSummary))
        .then(() => {
          const expectedAction = [
            {
              type: 'SET_MODAL_CHILDREN',
              children: expect.any(String),
            },
          ]
          expect(store.getActions()).not.toEqual(
            expect.arrayContaining(expectedAction)
          )
          expect(isZeroValueBag).toHaveBeenCalledTimes(1)
        })
    })
  })

  describe('addPromotionCode', () => {
    const promotionId = 'PROMO'
    const gtmCategory = 'shoppingBag'
    const errorCallback = jest.fn()

    it('addPromotionCode action creators snapshot [post SUCCESS]', () => {
      return store
        .dispatch(actions.addPromotionCode({ promotionId, gtmCategory }))
        .then(() => {
          expect(store.getActions()).toMatchSnapshot()
        })
    })

    it('should call getOrderSummary when in checkout', () => {
      const initialState = {
        routing: { location: { pathname: '/checkout' } },
      }
      const store = getMockStoreWithInitialReduxState(initialState)
      return store
        .dispatch(actions.addPromotionCode({ promotionId }))
        .then(() => {
          const expectedAction = [{ type: 'MOCK_getOrderSummary' }]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
        })
    })

    describe('On Failure', () => {
      beforeEach(() => {
        post.mockImplementationOnce(() => () =>
          Promise.reject({
            response: {
              body: {
                message: 'error',
              },
            },
          })
        )
      })
      it('addPromotionCode action creators snapshot [post FAILURE] and scroll to container', () => {
        return store
          .dispatch(actions.addPromotionCode({ promotionId, errorCallback }))
          .then(() => {
            expect(errorCallback).toHaveBeenCalled()
            expect(store.getActions()).toMatchSnapshot()
          })
      })

      it('addPromotionCode action creators and do not scroll to container', () => {
        return store
          .dispatch(actions.addPromotionCode({ promotionId }))
          .then(() => {
            expect(errorCallback).not.toHaveBeenCalled()
          })
      })
    })
  })

  describe('addStoredPromoCode', () => {
    const initialState = {
      shoppingBag: {
        bag: {
          products: [
            {
              product: 'p1',
            },
          ],
          promotions: ['PROMO'],
        },
        promotionCode: 'PROMO',
      },
    }
    const store = getMockStoreWithInitialReduxState(initialState)
    it('should call addPromotionCode when promotionCode and products exists in the state', () => {
      store.dispatch(actions.addStoredPromoCode())
      const expectedActions = [
        {
          type: 'SET_FORM_LOADING',
          formName: 'promotionCode',
          isLoading: true,
        },
      ]
      expect(store.getActions()).toEqual(
        expect.arrayContaining(expectedActions)
      )
    })
  })

  describe('getBagRequest', () => {
    const initialState = {
      shoppingBag: {
        bag: {
          products: [
            {
              product: 'p1',
            },
          ],
          promotions: ['PROMO'],
        },
        promotionCode: 'PROMO',
      },
    }
    const store = getMockStoreWithInitialReduxState(initialState)

    it('should call setEspotGroup when get shopping bag items successfuly', () => {
      return store.dispatch(actions.getBagRequest()).then(() => {
        expect(espotActions.setMiniBagEspots).toHaveBeenCalledWith({
          products: [],
          invetoryPositions: {},
          total: '5',
          subTotal: '5',
        })
      })
    })

    it('should call updateBag when get shopping bag items successfuly', () => {
      return store.dispatch(actions.getBagRequest()).then(() => {
        const expectedAction = [
          {
            type: 'UPDATE_BAG',
            bag: {
              products: [],
              invetoryPositions: {},
              total: '5',
              subTotal: '5',
            },
          },
        ]
        expect(store.getActions()).toEqual(
          expect.arrayContaining(expectedAction)
        )
      })
    })

    it('should call addStoredPromoCode if there is some promotionCode', () => {
      return store.dispatch(actions.getBagRequest()).then(() => {
        const expectedAction = [
          {
            type: 'SET_PROMOTION_CODE_CONFIRMATION',
            promotionCodeConfirmation: true,
          },
        ]
        expect(store.getActions()).toEqual(
          expect.arrayContaining(expectedAction)
        )
      })
    })

    it('should snapshot all the actions when sucess', () => {
      return store.dispatch(actions.getBagRequest()).then(() => {
        expect(store.getActions()).toMatchSnapshot()
      })
    })

    describe('On Failure', () => {
      it('should set Error Form when error message', () => {
        get.mockImplementationOnce(() => () =>
          Promise.reject({
            response: {
              body: {
                message: 'error',
              },
            },
          })
        )
        return store.dispatch(actions.getBagRequest()).then(() => {
          const expectedAction = [{ type: 'MOCK_setGenericError' }]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
        })
      })
      it('should set API Error if failure when not error message', () => {
        get.mockImplementationOnce(() => () =>
          Promise.reject({
            response: {},
          })
        )
        return store.dispatch(actions.getBagRequest()).then(() => {
          const expectedAction = [{ type: 'MOCK_setApiError' }]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
        })
      })
    })
  })

  describe('getBag', () => {
    const initialState = {
      shoppingBag: {
        bag: {
          products: [
            {
              product: 'p1',
            },
          ],
          promotions: ['PROMO'],
        },
        promotionCode: 'PROMO',
        miniBagOpen: true,
      },
    }
    const store = getMockStoreWithInitialReduxState(initialState)

    it('should call `BAR_ORDER_SUMMARY` queue using redux-async-queue middleware', () => {
      const asyncQueueObject = actions.getBag()
      expect(asyncQueueObject).toEqual({
        queue: 'BAG_ORDER_SUMMARY',
        callback: expect.any(Function),
      })
    })

    it('should call getBagRequest as callback', () => {
      // special scenario when testing redux-async-queue middleware
      const action = actions.getBag()
      return action
        .callback(() => {}, store.dispatch, store.getState)
        .then(() => {
          expect(store.getActions()).toMatchSnapshot()
        })
    })

    it('should call next function from redux-async-queue in the callback', () => {
      const nextMock = jest.fn()
      return actions
        .getBag()
        .callback(nextMock, store.dispatch, store.getState)
        .then(() => {
          expect(nextMock).toHaveBeenCalledTimes(1)
        })
    })

    describe('merge request', () => {
      let store
      beforeEach(() => {
        store = getMockStoreWithInitialReduxState(initialState)
      })

      it('should fire BAG_MERGE_STARTED and BAG_MERGE_FINISHED actions if isMergeRequest = true', () => {
        return actions
          .getBag(true, true)
          .callback(() => {}, store.dispatch, store.getState)
          .then(() => {
            expect(store.getActions()).toEqual(
              expect.arrayContaining([
                { type: 'BAG_MERGE_STARTED' },
                { type: 'BAG_MERGE_FINISHED' },
              ])
            )
          })
      })

      it('should not fire the bag merge actions if isMergeRequest = false', () => {
        const actionHasBeenFired = (type) =>
          store.getActions().some((action) => action.type === type)
        return actions
          .getBag(true, false)
          .callback(() => {}, store.dispatch, store.getState)
          .then(() => {
            expect(actionHasBeenFired('BAG_MERGE_STARTED')).toBe(false)
            expect(actionHasBeenFired('BAG_MERGE_FINISHED')).toBe(false)
          })
      })

      it('should not fire the bag merge actions if isMergeRequest is not provided', () => {
        const actionHasBeenFired = (type) =>
          store.getActions().some((action) => action.type === type)
        return actions
          .getBag()
          .callback(() => {}, store.dispatch, store.getState)
          .then(() => {
            expect(actionHasBeenFired('BAG_MERGE_STARTED')).toBe(false)
            expect(actionHasBeenFired('BAG_MERGE_FINISHED')).toBe(false)
          })
      })
    })
  })

  describe('openMiniBag', () => {
    it('should call close modal and open mini bag', () => {
      const mockedCloseModalAction = {
        type: 'MOCKED_CLOSE_MODAL',
      }
      jest
        .spyOn(modalActions, 'closeModal')
        .mockReturnValue(mockedCloseModalAction)
      store.dispatch(actions.openMiniBag())
      expect(modalActions.closeModal).toHaveBeenCalled()
      const expectedAction = [mockedCloseModalAction, { type: 'OPEN_MINI_BAG', autoClose: false }]
      expect(store.getActions()).toEqual(expectedAction)
      modalActions.closeModal.mockRestore()
    })

    it('should call viewBag analytics when mini bag open', () => {
      expect(viewBag).not.toHaveBeenCalled()
      store.dispatch(actions.openMiniBag())
      expect(viewBag).toHaveBeenCalledTimes(1)
      expect(viewBag).toHaveBeenCalledWith({ orderId: 0, products: [] })
    })

    it('should call open mini bag with autoClose set to true', () => {
      store.dispatch(actions.openMiniBag(true))
      const expectedAction = [{ type: 'OPEN_MINI_BAG', autoClose: true }]

      expect(store.getActions()).toEqual(expect.arrayContaining(expectedAction))
    })
  })

  describe('closeMiniBag', () => {
    it('should call CLOSE_MINI_BAG action creator', () => {
      store.dispatch(actions.closeMiniBag())
      const expectedAction = [{ type: 'CLOSE_MINI_BAG' }]
      expect(store.getActions()).toEqual(expectedAction)
    })
  })

  describe('toggleMiniBag', () => {
    const initialState = {
      shoppingBag: {
        miniBagOpen: true,
      },
    }
    const store = getMockStoreWithInitialReduxState(initialState)

    it('should call CLOSE_MINI_BAG when mini bag is open', () => {
      store.dispatch(actions.toggleMiniBag())
      const expectedAction = [{ type: 'CLOSE_MINI_BAG' }]
      expect(store.getActions()).toEqual(expectedAction)
    })
    it('should call CLOSE_MINI_BAG when mini bag is open', () => {
      const initialState = {
        shoppingBag: {
          miniBagOpen: false,
        },
      }
      const mockedCloseModalAction = {
        type: 'MOCKED_CLOSE_MODAL',
      }
      jest
        .spyOn(modalActions, 'closeModal')
        .mockReturnValue(mockedCloseModalAction)
      const store = getMockStoreWithInitialReduxState(initialState)
      store.dispatch(actions.toggleMiniBag())
      const expectedAction = [mockedCloseModalAction, { type: 'OPEN_MINI_BAG', autoClose: false }]
      expect(store.getActions()).toEqual(expectedAction)
    })
  })

  describe('checkForMergedItemsInBag', () => {
    const preLoginState = {
      preLogin: true,
    }
    const initialState = {
      routing: { location: { pathname: '/checkout/login' } },
    }
    const getModalContent = (store) =>
      store.getActions().find((a) => a.type === 'SET_MODAL_CHILDREN').children

    // we have to add clearActions here because we are using scoped store variable
    // instead the global one for the whole test
    beforeEach(() => {
      store.clearActions()
      jest.clearAllMocks()
    })

    it('should call showModal if the BagCount has Changed (checkout)', () => {
      const store = getMockStoreWithInitialReduxState(initialState)
      getShoppingBagTotalItems.mockReturnValueOnce(1)
      getShoppingBagTotalItems.mockReturnValueOnce(2)
      isInCheckout.mockReturnValueOnce(true)
      store.dispatch(actions.checkForMergedItemsInBag(preLoginState, 2))
      const expectedActions = [
        {
          type: 'SET_MODAL_CHILDREN',
          children: expect.any(String),
        },
      ]
      expect(store.getActions()).toEqual(
        expect.arrayContaining(expectedActions)
      )

      expect(getShoppingBagTotalItems).toHaveBeenCalledTimes(2)
      expect(getShoppingBagTotalItems.mock.calls[0][0]).toBe(store.getState())
      expect(getShoppingBagTotalItems.mock.calls[1][0]).toBe(preLoginState)
      expect(isDDPActiveUserPreRenewWindow).toHaveBeenCalledWith(
        store.getState()
      )
      expect(bagContainsDDPProduct).toHaveBeenCalledWith(preLoginState)
      expect(isInCheckout).toHaveBeenCalledWith(store.getState())

      const modalContent = getModalContent(store)
      expect(modalContent).toMatch(
        /You have items in your shopping bag from a previous visit. Please review the items in your bag before continuing to payment./
      )
    })

    it('should call showModal if the BagCount has Changed (non-checkout)', () => {
      getShoppingBagTotalItems.mockReturnValueOnce(1)
      getShoppingBagTotalItems.mockReturnValueOnce(2)
      const store = getMockStoreWithInitialReduxState({
        ...initialState,
        routing: { location: { pathname: '/login' } },
      })
      store.dispatch(actions.checkForMergedItemsInBag(preLoginState))
      const expectedActions = [
        {
          type: 'SET_MODAL_CHILDREN',
          children: expect.any(String),
        },
      ]
      expect(store.getActions()).toEqual(
        expect.arrayContaining(expectedActions)
      )
      const modalContent = getModalContent(store)
      expect(modalContent).toMatch(
        /You have items in your shopping bag from a previous visit./
      )
    })

    it('should not show modal if bagCount has not changed', () => {
      const store = getMockStoreWithInitialReduxState(initialState)
      store.dispatch(actions.checkForMergedItemsInBag(preLoginState))
      expect(store.getActions()).toEqual([])
    })

    it('should not show modal if bagCount has not changed, has DDP product in bag but is not active DDP user', () => {
      bagContainsDDPProduct.mockReturnValueOnce(true)
      isDDPActiveUserPreRenewWindow.mockReturnValueOnce(false)
      const store = getMockStoreWithInitialReduxState(initialState)
      store.dispatch(actions.checkForMergedItemsInBag(preLoginState))
      expect(store.getActions()).toEqual([])
    })

    it('should not show modal if bagCount has not changed, is active DDP user but has does not have DDP product in bag', () => {
      bagContainsDDPProduct.mockReturnValueOnce(false)
      isDDPActiveUserPreRenewWindow.mockReturnValueOnce(true)
      const store = getMockStoreWithInitialReduxState(initialState)
      store.dispatch(actions.checkForMergedItemsInBag(preLoginState))
      expect(store.getActions()).toEqual([])
    })

    it('should show modal if bagCount has not changed, has DDP product in bag and is active DDP user', () => {
      bagContainsDDPProduct.mockReturnValueOnce(true)
      isDDPActiveUserPreRenewWindow.mockReturnValueOnce(true)
      const store = getMockStoreWithInitialReduxState({
        ...initialState,
      })
      store.dispatch(actions.checkForMergedItemsInBag(preLoginState))
      const expectedActions = [
        {
          type: 'SET_MODAL_CHILDREN',
          children: expect.any(String),
        },
      ]
      expect(store.getActions()).toEqual(
        expect.arrayContaining(expectedActions)
      )
      const modalContent = getModalContent(store)
      expect(modalContent).not.toMatch(
        /You have items in your shopping bag from a previous visit. Please review the items in your bag before continuing to payment./
      )
      expect(modalContent).toMatch(
        /Great news! You already have a delivery subscription./
      )
    })

    it('should show modal if bagCount has changed, has DDP product in bag and is active DDP user', () => {
      getShoppingBagTotalItems.mockReturnValueOnce(1)
      getShoppingBagTotalItems.mockReturnValueOnce(2)
      bagContainsDDPProduct.mockReturnValueOnce(true)
      isDDPActiveUserPreRenewWindow.mockReturnValueOnce(true)
      isInCheckout.mockReturnValueOnce(true)
      const store = getMockStoreWithInitialReduxState(initialState)
      store.dispatch(actions.checkForMergedItemsInBag(preLoginState))
      const expectedActions = [
        {
          type: 'SET_MODAL_CHILDREN',
          children: expect.any(String),
        },
      ]
      expect(store.getActions()).toEqual(
        expect.arrayContaining(expectedActions)
      )
      const modalContent = getModalContent(store)
      expect(modalContent).toMatch(
        /You have items in your shopping bag from a previous visit. Please review the items in your bag before continuing to payment./
      )
      expect(modalContent).toMatch(
        /Great news! You already have a delivery subscription./
      )
    })

    it('should show modal with DDP message only if bagCount has changed and is now zero, has had DDP product removed from bag and is active DDP user', () => {
      bagContainsDDPProduct.mockReturnValueOnce(true)
      isDDPActiveUserPreRenewWindow.mockReturnValueOnce(true)
      const store = getMockStoreWithInitialReduxState({
        ...initialState,
      })
      store.dispatch(actions.checkForMergedItemsInBag(0))
      const expectedActions = [
        {
          type: 'SET_MODAL_CHILDREN',
          children: expect.any(String),
        },
      ]
      expect(store.getActions()).toEqual(
        expect.arrayContaining(expectedActions)
      )
      const modalContent = getModalContent(store)
      expect(modalContent).not.toMatch(
        /You have items in your shopping bag from a previous visit. Please review the items in your bag before continuing to payment./
      )
      expect(modalContent).toMatch(
        /Great news! You already have a delivery subscription./
      )
    })
  })

  describe('synchroniseBag', () => {
    const preLoginState = {
      preLogin: true,
    }
    const postLoginState = {
      preLogin: false,
    }
    const fakeGetBagAction = { type: 'got_bag' }

    let dispatchSpy
    let getStateSpy
    beforeEach(() => {
      dispatchSpy = jest.fn(identity)
      getStateSpy = jest.fn(() => postLoginState)
      jest.spyOn(actions, 'getBag').mockReturnValue(fakeGetBagAction)
    })
    afterEach(() => {
      jest.clearAllMocks()
    })
    afterAll(() => {
      actions.getBag.mockRestore()
    })

    it('should call getBag if bag has changed and is in checkout', () => {
      getShoppingBagTotalItems.mockReturnValueOnce(1)
      getShoppingBagTotalItems.mockReturnValueOnce(0)
      isInCheckout.mockReturnValueOnce(true)

      actions.synchroniseBag(preLoginState)(dispatchSpy, getStateSpy)

      expect(actions.getBag).toHaveBeenCalled()
      expect(actions.getBag).toHaveBeenCalledWith(true, true)
      expect(dispatchSpy).toHaveBeenCalledWith(fakeGetBagAction)
    })

    it('should not call getBag if bag has changed but not in checkout', () => {
      getShoppingBagTotalItems.mockReturnValueOnce(1)
      getShoppingBagTotalItems.mockReturnValueOnce(0)
      isInCheckout.mockReturnValueOnce(false)

      actions.synchroniseBag(preLoginState)(dispatchSpy, getStateSpy)

      expect(dispatchSpy).not.toHaveBeenCalled()
      expect(actions.getBag).not.toHaveBeenCalled()
    })

    it('should not call getBag if bag has not changed', () => {
      getShoppingBagTotalItems.mockReturnValueOnce(0)
      getShoppingBagTotalItems.mockReturnValueOnce(0)
      isInCheckout.mockReturnValueOnce(true)

      actions.synchroniseBag(preLoginState)(dispatchSpy, getStateSpy)

      expect(dispatchSpy).not.toHaveBeenCalled()
      expect(actions.getBag).not.toHaveBeenCalled()
    })
  })

  describe('addToBag', () => {
    const data = {
      productId: 123,
      sku: 123456,
    }
    const product = {
      lineNumber: 'KSJ78213',
      name: 'cool product',
      quantity: 1,
    }
    const initialState = {
      config: { language: '', brandName: '' },
      routing: { location: { pathname: '/login' } },
      viewport: { media: 'mobile' },
      shoppingBag: {
        bag: {
          products: [product],
          invetoryPositions: {},
          total: '5',
          subTotal: '5',
        },
        promotionCode: 'PROMO',
      },
    }
    const store = mockStoreCreator(initialState)

    beforeEach(() => {
      store.clearActions()
    })

    it('should call buyNowSuccess then Update shopping bag badge count', () => {
      return store
        .dispatch(actions.addToBag(data.productId, data.sku))
        .then(() => {
          const expectedAction = [
            { type: 'UPDATE_SHOPPING_BAG_BADGE_COUNT', count: 2 },
          ]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
        })
    })

    it('should call addToBagSuccess then show modal if mobile', () => {
      const successHTML = <div>success</div>
      return store
        .dispatch(actions.addToBag(data.productId, data.sku, 1, successHTML))
        .then(() => {
          const expectedAction = [
            {
              type: 'SET_MODAL_CHILDREN',
              children:
                '<div data-reactroot="" data-reactid="1" data-react-checksum="-220065197">success</div>',
            },
          ]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
        })
    })

    it('should dispatch `PRODUCTS_ADDED_TO_BAG` action with added products', () => {
      post.mockImplementation(() => () =>
        Promise.resolve({
          text: '{"success" : true}',
          body: {
            products: [
              product,
              {
                lineNumber: 'KSFS0213',
                name: 'cool second product',
                quantity: 2,
              },
            ],
          },
        })
      )
      const store = mockStoreCreator(initialState)

      return store.dispatch(actions.addToBag(data)).then(() => {
        expect(store.getActions()).toContainEqual({
          type: 'PRODUCTS_ADDED_TO_BAG',
          payload: {
            products: [
              {
                lineNumber: 'KSFS0213',
                name: 'cool second product',
                quantity: 2,
              },
            ],
            quantity: 1,
          },
        })
      })
    })

    it('add to bag from wishlist using catEntryId', async () => {
      const successHTML = <div>success</div>
      const body = {
        products: [
          {
            lineNumber: 'KSJ78213',
            name: 'cool product',
          },
        ],
      }
      post.mockImplementation(() => () =>
        Promise.resolve({
          text: '{"success" : true}',
          body,
        })
      )
      post.type = 'MOCK_addToBag'

      const expectedActions = [
        {
          type: 'PRODUCTS_ADDED_TO_BAG',
          payload: {
            products: [{ lineNumber: 'KSJ78213', name: 'cool product' }],
            quantity: 1,
          },
        },
        {
          type: 'SET_FORM_LOADING',
          formName: 'promotionCode',
          isLoading: true,
        },
        {
          type: 'SET_FORM_MESSAGE',
          formName: 'promotionCode',
          message: {},
          key: null,
          meta: { localise: 'message.message' },
        },
        { type: 'SET_MODAL_MODE', mode: 'normal' },
        {
          type: 'SET_MODAL_CHILDREN',
          children:
            '<div data-reactroot="" data-reactid="1" data-react-checksum="-220065197">success</div>',
        },
        { type: 'SET_MODAL_TYPE', modalType: 'dialog' },
        {
          type: 'OPEN_MODAL',
          entryPoint: false,
          scrollPositionOnOpenModal: null,
        },
        { type: 'UPDATE_SHOPPING_BAG_BADGE_COUNT', count: NaN },
        {
          type: 'UPDATE_BAG',
          bag: { products: [{ lineNumber: 'KSJ78213', name: 'cool product' }] },
        },
        {
          type: 'SET_FORM_LOADING',
          formName: 'promotionCode',
          isLoading: false,
        },
        { type: 'UPDATE_SHOPPING_BAG_BADGE_COUNT', count: NaN },
        {
          type: 'UPDATE_BAG',
          bag: { products: [{ lineNumber: 'KSJ78213', name: 'cool product' }] },
        },
      ]

      await store.dispatch(actions.addToBagWithCatEntryId(1234, successHTML))
      expect(store.getActions()).toEqual(
        expect.arrayContaining(expectedActions)
      )
    })

    it('toggles the mini bag if we are not viewing on a mobile', async () => {
      isMobile.mockReturnValueOnce(false)
      const successHTML = <div>success</div>
      const body = {
        products: [
          {
            lineNumber: 'KSJ78213',
            name: 'cool product',
          },
        ],
      }
      post.mockImplementation(() => () =>
        Promise.resolve({
          text: '{"success" : true}',
          body,
        })
      )
      post.type = 'MOCK_addToBag'

      await store.dispatch(actions.addToBagWithCatEntryId(1234, successHTML))
      expect(store.getActions()).toEqual(
        expect.arrayContaining([
          {
            type: 'OPEN_MINI_BAG',
            autoClose: false,
          },
        ])
      )
    })

    it('does not open the mini bag when we are viewing the application on a small viewport', async () => {
      isMobile.mockReturnValueOnce(true)
      const successHTML = <div>success</div>
      const body = {
        products: [
          {
            lineNumber: 'KSJ78213',
            name: 'cool product',
          },
        ],
      }
      post.mockImplementation(() => () =>
        Promise.resolve({
          text: '{"success" : true}',
          body,
        })
      )
      post.type = 'MOCK_addToBag'

      await store.dispatch(actions.addToBagWithCatEntryId(1234, successHTML))
      expect(store.getActions()).not.toEqual(
        expect.arrayContaining([
          {
            type: 'OPEN_MINI_BAG',
          },
        ])
      )
    })

    describe('On Failure', () => {
      it('should call decrement counter if [post FAILURE]', () => {
        post.mockImplementationOnce(() => () =>
          Promise.reject({
            response: {
              body: {
                message: 'error',
              },
            },
          })
        )
        return store.dispatch(actions.addToBag(data)).then(() => {
          const expectedAction = [
            { type: 'AJAXCOUNTER_INCREMENT' },
            { type: 'MOCK_setGenericError' },
            { type: 'AJAXCOUNTER_DECREMENT' },
          ]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
        })
      })
      it('should set Error Form when error message', () => {
        post.mockImplementationOnce(() => () =>
          Promise.reject({
            response: {
              body: {
                message: 'error',
              },
            },
          })
        )
        return store.dispatch(actions.addToBag(data)).then(() => {
          const expectedAction = [{ type: 'MOCK_setGenericError' }]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
        })
      })
      it('should set API Error if failure when not error message', () => {
        post.mockImplementationOnce(() => () =>
          Promise.reject({
            response: {},
          })
        )
        return store.dispatch(actions.addToBag(data)).then(() => {
          const expectedAction = [{ type: 'MOCK_setApiError' }]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
        })
      })
    })
  })

  describe('persistShoppingBagProduct', () => {
    it('should call bagChange analytics when products size changes', () => {
      const product2 = {
        lineNumber: 'LSJ78213',
        name: 'cool product 2',
        quantity: 1,
        catEntryId: 321,
      }
      const product1 = {
        lineNumber: 'KSJ78213',
        name: 'cool product',
        quantity: 1,
        catEntryId: 123,
        catEntryIdToAdd: 1,
        items: [{}, product2],
      }
      const initialState = {
        shoppingBag: {
          bag: {
            products: [product1, product2],
          },
        },
      }
      const store = getMockStoreWithInitialReduxState(initialState)
      put.mockImplementation(() => () =>
        Promise.resolve({ body: { products: [product1, product2] } })
      )
      expect(bagChange).not.toHaveBeenCalled()
      return store.dispatch(actions.persistShoppingBagProduct(0)).then(() => {
        expect(bagChange).toHaveBeenCalledTimes(1)
        expect(bagChange).toHaveBeenLastCalledWith(product2, 'event30')
      })
    })

    it('should call bagChange analytics when products quantity increases', () => {
      const product = {
        lineNumber: 'KSJ78213',
        name: 'cool product',
        quantity: 1,
        quantitySelected: '2',
        catEntryId: 123,
        items: [],
      }
      const updatedProduct = {
        lineNumber: 'KSJ78213',
        name: 'cool product',
        quantity: 2,
        catEntryId: 123,
        items: [],
      }
      const initialState = {
        shoppingBag: {
          bag: {
            products: [updatedProduct, product],
          },
        },
      }
      const store = getMockStoreWithInitialReduxState(initialState)
      put.mockImplementation(() => () =>
        Promise.resolve({ body: { products: [product, updatedProduct] } })
      )
      expect(bagChange).not.toHaveBeenCalled()
      return store.dispatch(actions.persistShoppingBagProduct(1)).then(() => {
        expect(bagChange).toHaveBeenCalledTimes(1)
        expect(bagChange).toHaveBeenLastCalledWith(updatedProduct, 'scAdd')
      })
    })

    it('should call bagChange analytics when products quantity decreases', () => {
      const product = {
        lineNumber: 'KSJ78213',
        name: 'cool product',
        quantity: 2,
        quantitySelected: '2',
        catEntryId: 123,
        items: [],
      }
      const updatedProduct = {
        lineNumber: 'KSJ78213',
        name: 'cool product',
        quantity: 1,
        catEntryId: 123,
        items: [],
      }
      const initialState = {
        shoppingBag: {
          bag: {
            products: [updatedProduct, product],
          },
        },
      }
      const store = getMockStoreWithInitialReduxState(initialState)
      put.mockImplementation(() => () =>
        Promise.resolve({ body: { products: [product, updatedProduct] } })
      )
      expect(bagChange).not.toHaveBeenCalled()
      return store.dispatch(actions.persistShoppingBagProduct(1)).then(() => {
        expect(bagChange).toHaveBeenCalledTimes(1)
        expect(bagChange).toHaveBeenLastCalledWith(updatedProduct, 'scRemove')
      })
    })

    it('should call decrement counter if failure', () => {
      const product2 = {
        lineNumber: 'LSJ78213',
        name: 'cool product 2',
        quantity: 1,
        catEntryId: 321,
      }
      const product1 = {
        lineNumber: 'KSJ78213',
        name: 'cool product',
        quantity: 1,
        catEntryId: 123,
        catEntryIdToAdd: 1,
        items: [{}, product2],
      }
      const initialState = {
        shoppingBag: {
          bag: {
            products: [product1, product2],
          },
        },
      }
      const store = getMockStoreWithInitialReduxState(initialState)
      put.mockImplementationOnce(() => () =>
        Promise.reject({
          response: {
            body: {
              message: 'error',
            },
          },
        })
      )
      return store.dispatch(actions.persistShoppingBagProduct(0)).then(() => {
        const expectedAction = [
          { type: 'AJAXCOUNTER_INCREMENT' },
          { type: 'MOCK_setGenericError' },
          { type: 'AJAXCOUNTER_DECREMENT' },
        ]
        expect(store.getActions()).toEqual(
          expect.arrayContaining(expectedAction)
        )
      })
    })
  })

  describe('deleteFromBag', () => {
    const orderId = 12345
    const product = {
      name: 'cool product',
    }
    it('should call bagChange analytics when remove product', () => {
      expect(bagChange).not.toHaveBeenCalled()
      return store
        .dispatch(actions.deleteFromBag(orderId, product))
        .then(() => {
          expect(bagChange).toHaveBeenCalledTimes(1)
          expect(bagChange).toHaveBeenLastCalledWith(product, 'scRemove')
        })
    })
    it('should not show modal if there is no modal text', () => {
      return store
        .dispatch(actions.deleteFromBag(orderId, product))
        .then(() => {
          const modalChildrenAction = store
            .getActions()
            .find((e) => e.type === 'SET_MODAL_CHILDREN')
          expect(modalChildrenAction).toBeUndefined()
        })
    })
    it('should show modal with provided text', () => {
      const successText = 'Success'
      return store
        .dispatch(actions.deleteFromBag(orderId, product, successText))
        .then(() => {
          const modalChildrenAction = store
            .getActions()
            .find((e) => e.type === 'SET_MODAL_CHILDREN')
          expect(modalChildrenAction).not.toBeNull()
          expect(modalChildrenAction.children).toMatch(/Success/)
        })
    })
  })

  describe('emptyShoppingBag', () => {
    const product = {
      lineNumber: 'KSJ78213',
      name: 'cool product',
      quantity: 1,
      catEntryId: 123,
    }
    const initialState = {
      shoppingBag: {
        bag: {
          products: [product],
        },
      },
    }
    const store = getMockStoreWithInitialReduxState(initialState)

    it('should call bagChange analytics when remove product', () => {
      expect(bagChange).not.toHaveBeenCalled()
      return store.dispatch(actions.emptyShoppingBag()).then(() => {
        expect(bagChange).toHaveBeenCalledTimes(1)
        expect(bagChange).toHaveBeenLastCalledWith(product, 'scRemove')
      })
    })
  })

  describe('delPromotionCode', () => {
    it('should call increment and decrement', () => {
      return store.dispatch(actions.delPromotionCode({})).then(() => {
        expect(store.getActions()).toEqual([
          { type: 'AJAXCOUNTER_INCREMENT' },
          { type: 'AJAXCOUNTER_DECREMENT' },
          { type: 'UPDATE_SHOPPING_BAG_BADGE_COUNT', count: 0 },
          { type: 'MOCK_updateUniversalVariable' },
        ])
      })
    })

    describe('On Failure', () => {
      it('should set Error Form when error message', () => {
        del.mockImplementationOnce(() => () =>
          Promise.reject({
            response: {
              body: {
                message: 'error',
              },
            },
          })
        )
        return store.dispatch(actions.delPromotionCode({})).then(() => {
          const expectedAction = [{ type: 'MOCK_setGenericError' }]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
        })
      })
      it('should set API Error if failure when not error message', () => {
        del.mockImplementationOnce(() => () =>
          Promise.reject({
            response: {},
          })
        )
        return store.dispatch(actions.delPromotionCode({})).then(() => {
          const expectedAction = [{ type: 'MOCK_setApiError' }]
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedAction)
          )
        })
      })
    })

    describe('initShoppingBag', () => {
      const query = {
        transferStoreID: '1234',
        transferOrderID: '5678',
      }
      const initialState = {
        features: {
          status: {
            FEATURE_NEW_CHECKOUT: true,
            FEATURE_TRANSFER_BASKET: true,
          },
        },
      }
      beforeEach(() => {
        dispatch.mockImplementation(() => ({
          finally: (f) => f(),
        }))
        getState.mockImplementation(() => initialState)
        shouldTransferShoppingBag.mockImplementation(() => true)
      })

      it(
        'transfers the shopping bag when the "FEATURE_TRANSFER_BASKET" feature flag is enabled' +
          'and the url contains the "transferStoreID" and "transferOrderID" valid params',
        () => {
          getState.mockImplementationOnce(() => initialState)
          getLocationQuery.mockReturnValueOnce(query)
          actions.initShoppingBag()(dispatch, getState)
          expect(post).toHaveBeenCalledWith('/shopping_bag/transfer', {
            transferStoreID: 1234,
            transferOrderID: 5678,
          })
          expect(dispatch.mock.calls[1][0]).toMatchObject({
            queue: 'BAG_ORDER_SUMMARY',
          })
          expect(removeTransferShoppingBagParams).toHaveBeenCalled()
        }
      )

      it('doesn\'t transfer the bag when the "FEATURE_TRANSFER_BASKET" feature flag is disabled', () => {
        const state = assocPath(
          ['features', 'status', 'FEATURE_TRANSFER_BASKET'],
          false,
          initialState
        )
        getLocationQuery.mockReturnValueOnce(query)
        getState.mockImplementation(() => state)
        actions.initShoppingBag()(dispatch, getState)
        expect(post).not.toHaveBeenCalled()
      })
    })
  })

  describe('changeDeliveryType', () => {
    it('updates the bag delivery type', async () => {
      put.mockReturnValue(Promise.resolve({ body: {} }))
      const payload = {}
      const dispatch = (x) => x

      await actions.changeDeliveryType(payload)(dispatch)

      expect(put).toHaveBeenCalledWith('/shopping_bag/delivery', payload, false)
    })

    it('on error from backend raise a generic error', async () => {
      const err = { response: { body: { message: 'ERROR' } } }
      put.mockReturnValue(Promise.reject(err))
      const payload = {}
      const dispatch = (x) => x

      await actions.changeDeliveryType(payload)(dispatch)

      expect(setGenericError).toHaveBeenCalledWith({ ...err, message: 'ERROR' })
    })

    it('on error without body raises API error', async () => {
      const err = {}
      put.mockReturnValue(Promise.reject(err))
      const payload = {}
      const dispatch = (x) => x

      await actions.changeDeliveryType(payload)(dispatch)

      expect(setApiError).toHaveBeenCalledWith(err)
    })
  })
})
