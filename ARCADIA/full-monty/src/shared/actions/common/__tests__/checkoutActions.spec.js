import deepFreeze from 'deep-freeze'
import { pickAll } from 'ramda'
import { mockStoreCreator } from 'test/unit/helpers/get-redux-mock-store'
import * as checkoutSelectors from '../../../selectors/checkoutSelectors'
import * as checkoutActions from '../checkoutActions'
import * as formActions from '../formActions'
import * as scrollHelper from '../../../lib/scroll-helper'
import { getItem } from '../../../../client/lib/cookie'
import * as storeLocatorActions from '../../components/StoreLocatorActions'
import * as espotActions from '../espot-actions/espot-actions-by-page'
import * as actionsHelpers from '../../../lib/checkout-utilities/actions-helpers'
import * as analyticsActions from '../../../analytics/analytics-actions'
import { getAccount } from '../accountActions'

import { get } from '../../../lib/api-service'
import * as shoppingBagActions from '../shoppingBagActions'
import * as orderSummaryUtils from '../../../lib/checkout-utilities/order-summary'

jest.mock('../../../../shared/lib/localisation', () => ({
  localise: jest.fn(),
}))
jest.mock('../../../lib/analytics/analytics', () => ({
  eventBasedAnalytics: jest.fn(),
}))
jest.mock('../../../../client/lib/cookie', () => ({
  setItem: jest.fn(() => 'TS0001'),
  getItem: jest.fn(() => 'TS0001'),
  setStoreCookie: jest.fn(),
}))

jest.mock('../../components/StoreLocatorActions')
jest.mock('../../../lib/checkout-utilities/order-summary')
jest.mock('../accountActions', () => ({
  getAccount: jest.fn(),
}))
jest.mock('../shoppingBagActions')
jest.mock('../../../selectors/checkoutSelectors')
jest.mock('../../../lib/api-service', () => ({
  get: jest.fn(),
  post: jest.fn(),
}))
jest.mock('../../../lib/localisation', () => ({
  localise: jest.fn().mockImplementation((l, b, m) => m[0]),
}))

import { eventBasedAnalytics } from '../../../lib/analytics/analytics'

import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

const snapshot = (action) => expect(action).toMatchSnapshot()

const mockActionHelper = (name) => (...args) => ({ name, args })
const mockGetDefaultAddress = mockActionHelper('getDefaultAddress')
const mockGetDefaultNameAndPhone = mockActionHelper('getDefaultNameAndPhone')

describe('Checkout Actions', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  describe('request order summary', () => {
    const middlewares = [thunk]
    const mockStore = configureMockStore(middlewares)
    const initialState = {
      account: { user: {} },
      checkout: {
        orderSummary: {},
      },
      config: {
        language: 'en',
        brandName: 'ts',
      },
      features: {
        status: {
          FEATURE_CFSI: true,
        },
      },
      siteOptions: { billingCountries: {} },
    }
    const store = mockStore(initialState)
    const params = {
      shouldUpdateForms: false,
      shouldUpdateBag: false,
    }
    const mockOrderSummary = {
      basket: {},
      deliveryStoreCode: 12345,
    }

    shoppingBagActions.addStoredPromoCode.mockImplementation(() => ({
      type: 'MOCK_ADD_STORED_PROMO_CODE',
    }))
    storeLocatorActions.applyFilters.mockReturnValue({})
    orderSummaryUtils.fixOrderSummary.mockReturnValue(mockOrderSummary)
    orderSummaryUtils.removeCFSIFromOrderSummary.mockReturnValue({})
    checkoutSelectors.getSelectedDeliveryMethod.mockReturnValue({})

    beforeEach(() => {
      store.clearActions()
    })

    describe('getAccountAndOrderSummary', () => {
      it('dispatches `getAccount` action if authenticated', async () => {
        const store = mockStore({
          ...initialState,
          auth: { authentication: true },
        })
        const getAccountMock = () => {
          const p = new Promise((resolve) => resolve())
          p.type = 'GET_USER_ACCOUNT'
          return p
        }
        getAccount.mockImplementationOnce(getAccountMock)
        const orderSummaryRequestMock = () => {
          const p = new Promise((resolve) => resolve())
          p.type = 'MOCK_ORDER_SUMMARY_REQUEST'
          return p
        }
        get.mockImplementationOnce(orderSummaryRequestMock)
        const expectedActions = [getAccountMock(), orderSummaryRequestMock()]

        await store.dispatch(checkoutActions.getAccountAndOrderSummary())
        expect(getAccount).toHaveBeenCalledTimes(1)
        expect(store.getActions()).toEqual(
          expect.arrayContaining(expectedActions)
        )
      })

      it('does not dispatch `getAccount` action if not authenticated', async () => {
        const store = mockStore({
          ...initialState,
          auth: { authentication: false },
        })
        const orderSummaryRequestMock = () => {
          const p = new Promise((resolve) => resolve())
          p.type = 'MOCK_ORDER_SUMMARY_REQUEST'
          return p
        }
        get.mockImplementationOnce(orderSummaryRequestMock)
        const expectedActions = [orderSummaryRequestMock()]

        await store.dispatch(checkoutActions.getAccountAndOrderSummary())
        expect(getAccount).not.toHaveBeenCalled()
        expect(store.getActions()).toEqual(
          expect.arrayContaining(expectedActions)
        )
      })
    })

    describe('getOrderSummaryRequest', () => {
      describe('success', () => {
        it('dispatches the expected actions', async () => {
          const getOrderSummaryRequestMock = () => {
            const p = new Promise((resolve) =>
              resolve({ body: { mockOrderSummary } })
            )
            p.type = 'MOCK_REQUEST_SUCCESS'
            return p
          }
          get.mockImplementationOnce(getOrderSummaryRequestMock)
          const expectedActions = [
            { type: 'AJAXCOUNTER_INCREMENT' },
            { type: 'SET_ORDER_SUMMARY_ERROR', data: {} },
            getOrderSummaryRequestMock(),
            { type: 'AJAXCOUNTER_DECREMENT' },
            { type: 'MOCK_ADD_STORED_PROMO_CODE' },
            { type: 'SET_PARTIAL_ORDER_SUMMARY', data: mockOrderSummary },
            { type: 'FETCH_ORDER_SUMMARY_SUCCESS', data: mockOrderSummary },
          ]

          await store.dispatch(checkoutActions.getOrderSummaryRequest(params))
          expect(store.getActions()).toEqual(expectedActions)
        })

        it('sets message if exists in response', async () => {
          const message = 'An error has occured'
          const getOrderSummaryRequestMock = () => {
            const p = new Promise((resolve) => resolve({ body: { message } }))
            p.type = 'MOCK_REQUEST_SUCCESS'
            return p
          }
          get.mockImplementationOnce(getOrderSummaryRequestMock)
          const expectedActions = [
            getOrderSummaryRequestMock(),
            { type: 'SET_ORDER_SUMMARY_ERROR', data: { message } },
          ]

          await store.dispatch(checkoutActions.getOrderSummaryRequest())
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedActions)
          )
        })
      })

      describe('error', () => {
        it('clears order summary and partial order summary', async () => {
          const getOrderSummaryRequestMock = () => {
            const p = new Promise((resolve, reject) => reject({}))
            p.type = 'MOCK_REQUEST_ERROR'
            return p
          }
          get.mockImplementationOnce(getOrderSummaryRequestMock)
          const expectedActions = [
            getOrderSummaryRequestMock(),
            { type: 'SET_PARTIAL_ORDER_SUMMARY', data: {} },
            { type: 'FETCH_ORDER_SUMMARY_SUCCESS', data: {} },
          ]

          await store.dispatch(checkoutActions.getOrderSummaryRequest())
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedActions)
          )
        })

        it('sets expected error message if exists in response', async () => {
          const message = 'An error has occured'
          const getOrderSummaryRequestMock = () => {
            const p = new Promise((resolve, reject) =>
              reject({
                response: { body: { message } },
              })
            )
            p.type = 'MOCK_REQUEST_ERROR'
            return p
          }
          get.mockImplementationOnce(getOrderSummaryRequestMock)
          const expectedActions = [
            getOrderSummaryRequestMock(),
            {
              type: 'SET_ORDER_SUMMARY_ERROR',
              data: { message, isOutOfStock: false },
            },
          ]

          await store.dispatch(checkoutActions.getOrderSummaryRequest())
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedActions)
          )
        })

        it('sets expected error message if out of stock', async () => {
          const message = 'This product is out of stock'
          const getOrderSummaryRequestMock = () => {
            const p = new Promise((resolve, reject) =>
              reject({
                response: { body: { message } },
              })
            )
            p.type = 'MOCK_REQUEST_ERROR'
            return p
          }
          get.mockImplementationOnce(getOrderSummaryRequestMock)
          const expectedActions = [
            getOrderSummaryRequestMock(),
            {
              type: 'SET_ORDER_SUMMARY_ERROR',
              data: { message, isOutOfStock: true },
            },
          ]

          await store.dispatch(checkoutActions.getOrderSummaryRequest())
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedActions)
          )
        })

        it('sets default error message if none in response', async () => {
          const message = 'An error has occured. Please try again.'
          const getOrderSummaryRequestMock = () => {
            const p = new Promise((resolve, reject) => reject({ response: {} }))
            p.type = 'MOCK_REQUEST_ERROR'
            return p
          }
          get.mockImplementationOnce(getOrderSummaryRequestMock)
          const expectedActions = [
            getOrderSummaryRequestMock(),
            {
              type: 'SET_ORDER_SUMMARY_ERROR',
              data: { message, isOutOfStock: false },
            },
          ]

          await store.dispatch(checkoutActions.getOrderSummaryRequest())
          expect(store.getActions()).toEqual(
            expect.arrayContaining(expectedActions)
          )
        })

        it('should set the shopping bag state from error response if data is available', async () => {
          const basketData = {
            orderId: 1234,
          }

          get.mockImplementation(() => () =>
            Promise.reject({
              response: {
                body: {
                  message: 'Test message',
                  data: {
                    basket: basketData,
                  },
                },
              },
            })
          )

          shoppingBagActions.updateBag.mockImplementation(() => ({
            type: 'IGNORE',
          }))

          await store.dispatch(checkoutActions.getOrderSummaryRequest())

          expect(shoppingBagActions.updateBag).toHaveBeenCalledTimes(1)
          expect(shoppingBagActions.updateBag).toBeCalledWith(basketData, false)
        })
      })
    })
  })

  describe('updateOrderSummaryProduct', () => {
    const index = 123
    const update = 456

    it('should dispatch an action when there is a basket with products', () => {
      const store = mockStoreCreator({
        checkout: {
          orderSummary: {
            basket: { products: {} },
          },
        },
      })

      store.dispatch(checkoutActions.updateOrderSummaryProduct(index, update))
      expect(store.getActions()).toEqual([
        { type: 'UPDATE_ORDER_SUMMARY_PRODUCT', index, update },
      ])
    })

    it('should not dispatch an action when there is a basket with no products', () => {
      const store = mockStoreCreator({
        checkout: {
          orderSummary: {
            basket: {},
          },
        },
      })

      store.dispatch(checkoutActions.updateOrderSummaryProduct(index, update))
      expect(store.getActions()).toEqual([])
    })

    it('should not dispatch an action when there is no basket', () => {
      const store = mockStoreCreator({
        checkout: {
          orderSummary: {},
        },
      })

      store.dispatch(checkoutActions.updateOrderSummaryProduct(index, update))
      expect(store.getActions()).toEqual([])
    })
  })

  describe('clearCheckoutForms', () => {
    it('returns the correct action', () => {
      expect(checkoutActions.clearCheckoutForms()).toEqual({
        type: 'CLEAR_CHECKOUT_FORMS',
      })
    })
  })

  describe('toggleExpressDeliveryOptions', () => {
    it('returns the correct action', () => {
      const bool = 'fake bool'

      expect(checkoutActions.toggleExpressDeliveryOptions(bool)).toEqual({
        type: 'TOGGLE_EXPRESS_DELIVERY_OPTIONS_SUMMARY_PAGE',
        bool,
      })
    })
  })

  describe('setPartialOrderSummary', () => {
    it('returns the correct action', () => {
      const data = 'fake data'

      expect(checkoutActions.setPartialOrderSummary(data)).toEqual({
        type: 'SET_PARTIAL_ORDER_SUMMARY',
        data,
      })
    })
  })

  describe('setStoreUpdating', () => {
    it('returns the correct action', () => {
      const updating = 'fake updating'

      expect(checkoutActions.setStoreUpdating(updating)).toEqual({
        type: 'SET_STORE_UPDATING',
        updating,
      })
    })
  })

  describe('setDeliveryStore', () => {
    it('returns the correct action', () => {
      const store = {
        storeId: 123,
        address: {
          line1: 'foo line1',
          line2: 'foo line2',
          city: 'foo city',
          postcode: 'foo postcode',
        },
      }

      const expectedStore = {
        deliveryStoreCode: 123,
        storeAddress1: 'foo line1',
        storeAddress2: 'foo line2',
        storeCity: 'foo city',
        storePostcode: 'foo postcode',
      }

      expect(checkoutActions.setDeliveryStore(store)).toEqual({
        type: 'SET_DELIVERY_STORE',
        store: expectedStore,
      })
    })
  })

  describe('resetStoreDetails', () => {
    it('returns the correct action', () => {
      expect(checkoutActions.resetStoreDetails()).toEqual({
        type: 'RESET_STORE_DETAILS',
      })
    })
  })

  describe('setOrderSummary', () => {
    const pickPartialData = pickAll(['basket', 'deliveryStoreCode'])

    const describeTestsWithOrderSummary = (orderSummary) => {
      deepFreeze(orderSummary)
      const partialOrderSummary = deepFreeze(pickPartialData(orderSummary))

      const state = {
        // TODO: Test the FEATURE_CFSI === false execution path when `setOrderSummary` has been refactored.
        features: { status: { FEATURE_CFSI: true } },
      }

      describe('when there are delivery locations', () => {
        const mockActionCreator = (type) => (...args) => ({ type, args })

        beforeEach(() => {
          storeLocatorActions.applyFilters.mockImplementation(
            mockActionCreator('applyFilters')
          )
          jest
            .spyOn(espotActions, 'setOrderSummaryEspots')
            .mockImplementation(mockActionCreator('setOrderSummaryEspots'))
        })

        it('should dispatch the correct actions when selected delivery location is a store', () => {
          const store = mockStoreCreator({
            ...state,
            checkout: {
              orderSummary: {
                deliveryLocations: [
                  { foo: 123 },
                  { selected: true, deliveryLocationType: 'STORE' },
                ],
              },
            },
          })

          store.dispatch(checkoutActions.setOrderSummary(orderSummary))
          expect(store.getActions()).toEqual([
            checkoutActions.setPartialOrderSummary(partialOrderSummary),
            { type: 'FETCH_ORDER_SUMMARY_SUCCESS', data: orderSummary },
            storeLocatorActions.applyFilters(['brand', 'other']),
            espotActions.setOrderSummaryEspots(orderSummary),
          ])
        })

        it('should dispatch the correct actions when selected delivery location is not a store', () => {
          const store = mockStoreCreator({
            ...state,
            checkout: {
              orderSummary: {
                deliveryLocations: [
                  { foo: 123 },
                  { selected: true, deliveryLocationType: 'FOO' },
                ],
              },
            },
          })

          store.dispatch(checkoutActions.setOrderSummary(orderSummary))
          expect(store.getActions()).toEqual([
            checkoutActions.setPartialOrderSummary(partialOrderSummary),
            { type: 'FETCH_ORDER_SUMMARY_SUCCESS', data: orderSummary },
            storeLocatorActions.applyFilters(['parcel']),
            espotActions.setOrderSummaryEspots(orderSummary),
          ])
        })
      })

      describe('when there are no delivery locations', () => {
        it('should dispatch the correct actions', () => {
          const store = mockStoreCreator({
            ...state,
            checkout: {
              orderSummary: {},
            },
          })

          store.dispatch(checkoutActions.setOrderSummary(orderSummary))
          expect(store.getActions()).toEqual([
            checkoutActions.setPartialOrderSummary(partialOrderSummary),
            { type: 'FETCH_ORDER_SUMMARY_SUCCESS', data: orderSummary },
            espotActions.setOrderSummaryEspots(orderSummary),
          ])
        })
      })
    }

    describe('when there is a non-empty order', () => {
      describeTestsWithOrderSummary({
        basket: { foo: 123 },
        deliveryStoreCode: { bar: 456 },
        fish: 789,
        bread: 'rolls',
      })
    })

    describe('when there is an empty order', () => {
      describeTestsWithOrderSummary({
        fish: 789,
        bread: 'rolls',
      })
    })
  })

  describe('clearOrderSummaryBasket', () => {
    it('returns the correct action', () => {
      expect(checkoutActions.clearOrderSummaryBasket()).toEqual({
        type: 'FETCH_ORDER_SUMMARY_SUCCESS',
        data: {},
      })
    })
  })

  describe('setOrderSummaryField', () => {
    it('returns the correct action', () => {
      const field = 123
      const value = 456

      expect(checkoutActions.setOrderSummaryField(field, value)).toEqual({
        type: 'SET_ORDER_SUMMARY_FIELD',
        field,
        value,
      })
    })
  })

  describe('setOrderSummaryError', () => {
    it('returns the correct action', () => {
      const data = 123

      expect(checkoutActions.setOrderSummaryError(data)).toEqual({
        type: 'SET_ORDER_SUMMARY_ERROR',
        data,
      })
    })
  })

  describe('emptyOrderSummary', () => {
    it('returns the correct action', () => {
      expect(checkoutActions.emptyOrderSummary()).toEqual({
        type: 'EMPTY_ORDER_SUMMARY',
      })
    })
  })

  describe('setDeliveryAsBillingFlag', () => {
    it('returns the correct action', () => {
      const val = 123

      expect(checkoutActions.setDeliveryAsBillingFlag(val)).toEqual({
        type: 'SET_DELIVERY_AS_BILLING_FLAG',
        val,
      })
    })
  })

  describe('resetAddress', () => {
    it('returns the correct action', () => {
      expect(checkoutActions.resetAddress()).toEqual({ type: 'RESET_SEARCH' })
    })
  })

  describe('setManualAddressMode', () => {
    it('returns the correct action', () => {
      expect(checkoutActions.setManualAddressMode()).toEqual({
        type: 'SET_ADDRESS_MODE_TO_MANUAL',
      })
    })
  })

  describe('setFindAddressMode', () => {
    it('returns the correct action', () => {
      expect(checkoutActions.setFindAddressMode()).toEqual({
        type: 'SET_ADDRESS_MODE_TO_FIND',
      })
    })
  })

  describe('setMonikerAddress', () => {
    it('returns the correct action', () => {
      const data = 123

      expect(checkoutActions.setMonikerAddress(data)).toEqual({
        type: 'UPDATE_MONIKER',
        data,
      })
    })
  })

  describe('setDeliveryAddressToBilling', () => {
    const describeSetDeliveryAddressToBilling = ({
      useDeliveryFormData,
      expectedBillingDetails,
      expectedBillingAddress,
    }) => {
      it('should dispatch the correct actions when store details are present', () => {
        const store = mockStoreCreator({
          checkout: {
            orderSummary: {
              shippingCountry: 'fish',
              storeDetails: {},
            },
          },
          forms: {
            checkout: {
              yourDetails: {
                fields: [
                  {
                    value: 'foo details',
                  },
                  {
                    value: 'bar details',
                  },
                ],
              },
              yourAddress: {
                fields: [
                  {
                    value: 'foo address',
                  },
                  {
                    value: 'bar address',
                  },
                ],
              },
            },
          },
        })

        store.dispatch(
          checkoutActions.setDeliveryAddressToBilling(useDeliveryFormData)
        )
        expect(store.getActions()).toEqual([
          formActions.resetFormDirty('billingDetails', expectedBillingDetails),
        ])
      })

      it('should dispatch the correct actions when store details are not present', () => {
        const store = mockStoreCreator({
          checkout: {
            orderSummary: {
              shippingCountry: 'fish',
            },
          },
          forms: {
            checkout: {
              yourDetails: {
                fields: [
                  {
                    value: 'foo details',
                  },
                  {
                    value: 'bar details',
                  },
                ],
              },
              yourAddress: {
                fields: [
                  {
                    value: 'foo address',
                  },
                  {
                    value: 'bar address',
                  },
                ],
              },
            },
          },
        })

        store.dispatch(
          checkoutActions.setDeliveryAddressToBilling(useDeliveryFormData)
        )
        expect(store.getActions()).toEqual([
          formActions.resetFormDirty('billingDetails', expectedBillingDetails),
          formActions.resetFormDirty('billingAddress', expectedBillingAddress),
        ])
      })
    }

    describe('with delivery form data', () => {
      describeSetDeliveryAddressToBilling({
        useDeliveryFormData: true,
        expectedBillingDetails: ['foo details', 'bar details'],
        expectedBillingAddress: ['foo address', 'bar address'],
      })
    })

    describe('without delivery form data', () => {
      beforeEach(() => {
        jest
          .spyOn(actionsHelpers, 'getDefaultAddress')
          .mockImplementation(mockGetDefaultAddress)
        jest
          .spyOn(actionsHelpers, 'getDefaultNameAndPhone')
          .mockImplementation(mockGetDefaultNameAndPhone)
      })

      describeSetDeliveryAddressToBilling({
        useDeliveryFormData: false,
        expectedBillingDetails: mockGetDefaultNameAndPhone(),
        expectedBillingAddress: { ...mockGetDefaultAddress(), country: 'fish' },
      })
    })
  })

  describe('copyDeliveryValuesToBillingForms', () => {
    it('should dispatch the correct actions when home delivery is selected', () => {
      const state = deepFreeze({
        checkout: {
          orderSummary: {
            shippingCountry: 'fish',
          },
        },
        forms: {
          checkout: {
            yourDetails: {
              fields: [
                {
                  value: 'foo details',
                },
                {
                  value: 'bar details',
                },
              ],
            },
            yourAddress: {
              fields: [
                {
                  value: 'foo address',
                },
                {
                  value: 'bar address',
                },
              ],
            },
          },
        },
      })
      const store = mockStoreCreator(state)
      const selectedDeliveryLocationTypeEqualsSpy = jest
        .spyOn(checkoutSelectors, 'selectedDeliveryLocationTypeEquals')
        .mockReturnValue(true)

      store.dispatch(checkoutActions.copyDeliveryValuesToBillingForms())
      expect(store.getActions()).toEqual([
        formActions.resetFormDirty('billingDetails', [
          'foo details',
          'bar details',
        ]),
        formActions.resetFormDirty('billingAddress', [
          'foo address',
          'bar address',
        ]),
        checkoutActions.setManualAddressMode(),
      ])
      expect(selectedDeliveryLocationTypeEqualsSpy).toHaveBeenCalledTimes(1)
      expect(selectedDeliveryLocationTypeEqualsSpy).toHaveBeenCalledWith(
        state,
        'HOME'
      )
    })

    it('should dispatch the correct actions when home delivery is not selected', () => {
      const state = deepFreeze({
        checkout: {
          orderSummary: {
            shippingCountry: 'fish',
          },
        },
        forms: {
          checkout: {
            yourDetails: {
              fields: [
                {
                  value: 'foo details',
                },
                {
                  value: 'bar details',
                },
              ],
            },
          },
        },
      })
      const store = mockStoreCreator(state)
      const selectedDeliveryLocationTypeEqualsSpy = jest
        .spyOn(checkoutSelectors, 'selectedDeliveryLocationTypeEquals')
        .mockReturnValue(true)

      store.dispatch(checkoutActions.copyDeliveryValuesToBillingForms())
      expect(store.getActions()).toEqual([
        formActions.resetFormDirty('billingDetails', [
          'foo details',
          'bar details',
        ]),
      ])
      expect(selectedDeliveryLocationTypeEqualsSpy).toHaveBeenCalledTimes(1)
      expect(selectedDeliveryLocationTypeEqualsSpy).toHaveBeenCalledWith(
        state,
        'HOME'
      )
    })
  })

  describe('resetCheckoutForms', () => {
    beforeEach(() => {
      jest
        .spyOn(actionsHelpers, 'getDefaultAddress')
        .mockImplementation(mockGetDefaultAddress)
      jest
        .spyOn(actionsHelpers, 'getDefaultNameAndPhone')
        .mockImplementation(mockGetDefaultNameAndPhone)
    })

    const months = [
      '01',
      '02',
      '03',
      '04',
      '05',
      '06',
      '07',
      '08',
      '09',
      '10',
      '11',
      '12',
    ]
    const month = new Date().getMonth() + 1

    const state = deepFreeze({
      siteOptions: {
        expiryYears: ['2017'],
        expiryMonths: months,
        titles: ['Mr'],
      },
      paymentMethods: [{ value: 'VISA', type: 'CARD', label: 'visa' }],
      routing: { location: { pathname: '/checkout ' } },
      account: {
        user: {
          creditCard: {
            type: 'VISA',
            expiryMonth: month < 10 ? `0${month}` : `${month}`,
            expiryYear: '2016',
          },
        },
      },
      config: {
        country: 'United Kingdom',
      },
    })

    const {
      siteOptions: { expiryMonths, expiryYears },
      paymentMethods,
    } = state

    const defaultAddress = deepFreeze({
      ...mockGetDefaultAddress(),
      country: 'United Kingdom',
      county: null,
      state: null,
    })

    const defaultNameAndPhone = deepFreeze({
      ...mockGetDefaultNameAndPhone(),
      telephone: null,
    })

    const expectedPaymentOptions = deepFreeze(
      actionsHelpers.getDefaultPaymentOptions(
        paymentMethods,
        expiryMonths,
        expiryYears
      )
    )

    it('should dispatch the correct actions when order summary is empty', () => {
      const orderSummary = {
        shippingCountry: 'United Kingdom',
      }

      const store = mockStoreCreator(state)
      store.dispatch(checkoutActions.resetCheckoutForms(orderSummary))
      expect(store.getActions()).toEqual([
        formActions.resetForm('yourAddress', defaultAddress),
        formActions.resetForm('yourDetails', defaultNameAndPhone),
        formActions.resetForm('billingAddress', defaultAddress),
        checkoutActions.setFindAddressMode(),
        formActions.resetForm('billingDetails', defaultNameAndPhone),
        formActions.resetForm('billingCardDetails', expectedPaymentOptions),
      ])
    })

    it('should dispatch the correct actions when order summary has delivery and billing details', () => {
      const orderSummary = {
        billingDetails: {
          address: {
            field: 'address',
          },
          nameAndPhone: {
            field: 'nameAndPhone',
          },
        },
        deliveryDetails: {
          address: {
            field: 'address',
          },
          nameAndPhone: {
            field: 'nameAndPhone',
          },
        },
        shippingCountry: 'United Kingdom',
      }

      const expectedAddress = {
        field: 'address',
        county: null,
      }

      const expectedNameAndPhone = {
        field: 'nameAndPhone',
        telephone: null,
      }

      const store = mockStoreCreator(state)
      store.dispatch(checkoutActions.resetCheckoutForms(orderSummary))
      expect(store.getActions()).toEqual([
        formActions.resetForm('yourAddress', expectedAddress),
        formActions.resetForm('yourDetails', expectedNameAndPhone),
        formActions.resetForm('billingAddress', expectedAddress),
        checkoutActions.setFindAddressMode(),
        formActions.resetForm('billingDetails', expectedNameAndPhone),
        formActions.resetForm('billingCardDetails', expectedPaymentOptions),
      ])
    })

    it('should dispatch the correct actions when user has no credit card', () => {
      // TODO: DRY, this Date snippet should go in a test helper somewhere...
      const RealDate = Date

      const mockDate = (...isoDate) => {
        global.Date = class {
          constructor(...dateArgs) {
            if (Array.isArray(dateArgs) && dateArgs.length) {
              return new RealDate(...dateArgs)
            }
            return new RealDate(...isoDate)
          }
        }
      }

      afterEach(() => {
        global.Date = RealDate
      })

      mockDate('2000-01-01T00:00:00.000Z')

      const stateWithNoCreditCard = {
        ...state,
        account: { user: { creditCard: { type: '' } } },
      }

      const orderSummary = {
        shippingCountry: 'United Kingdom',
      }

      const defaultPaymentOptions = {
        ...expectedPaymentOptions,
        expiryMonth: '01',
      }

      const store = mockStoreCreator(stateWithNoCreditCard)
      store.dispatch(checkoutActions.resetCheckoutForms(orderSummary))
      expect(store.getActions()).toEqual([
        formActions.resetForm('yourAddress', defaultAddress),
        formActions.resetForm('yourDetails', defaultNameAndPhone),
        formActions.resetForm('billingAddress', defaultAddress),
        checkoutActions.setFindAddressMode(),
        formActions.resetForm('billingDetails', defaultNameAndPhone),
        formActions.resetForm('billingCardDetails', defaultPaymentOptions),
      ])
    })
  })

  describe('validateForms', () => {
    const getErrorsSpy = jest.spyOn(checkoutSelectors, 'getErrors')
    const getErrortouchedMultipleFormFieldsSpy = jest.spyOn(
      formActions,
      'touchedMultipleFormFields'
    )
    const scrollToFormFieldSpy = jest.spyOn(scrollHelper, 'scrollToFormField')

    const formNames = ['billingCardDetails', 'billingAddress']

    const errors = {
      billingAddress: {
        address1: 'This field is required',
      },
      billingCardDetails: {
        cardNumber: 'A 16 digit card number is required',
      },
    }

    it('should call `onValid` argument if no errors', () => {
      const getState = () => {}
      getErrorsSpy.mockReturnValue({})
      const onValidMock = jest.fn()
      checkoutActions.validateForms(formNames, { onValid: onValidMock })(
        null,
        getState
      )
      expect(onValidMock).toHaveBeenCalled()
    })

    it('should touch the fields on error', () => {
      const dispatchMock = jest.fn()
      const getState = () => {}
      getErrorsSpy.mockReturnValue(errors)
      checkoutActions.validateForms(formNames)(dispatchMock, getState)
      expect(dispatchMock).toHaveBeenCalledTimes(2)
      expect(getErrortouchedMultipleFormFieldsSpy).toHaveBeenCalledWith(
        'billingAddress',
        ['address1']
      )
      expect(getErrortouchedMultipleFormFieldsSpy).toHaveBeenCalledWith(
        'billingCardDetails',
        ['cardNumber']
      )
    })

    it('should scroll to first form in the `formNames` array', () => {
      const dispatchMock = jest.fn()
      const getState = () => {}
      getErrorsSpy.mockReturnValue(errors)
      checkoutActions.validateForms(formNames)(dispatchMock, getState)
      expect(scrollToFormFieldSpy).toHaveBeenCalledWith('cardNumber')
    })

    it('should call `onInvlid` arguments if errors', () => {
      const dispatchMock = jest.fn()
      const getState = () => {}
      getErrorsSpy.mockReturnValue(errors)
      const onInvalidMock = jest.fn()
      checkoutActions.validateForms(formNames, { onInvalid: onInvalidMock })(
        dispatchMock,
        getState
      )
      expect(onInvalidMock).toHaveBeenCalled()
    })
  })

  const state = {
    config: {
      language: 'en-gb',
      brandName: 'ts',
    },
  }

  it('addGiftCard(data)', () => {
    snapshot(checkoutActions.addGiftCard({}))
  })

  it('should call eventBasedAnalytics with proper data when gitcard correct', () => {
    const action = checkoutActions.addGiftCard({ body: 'hello' })
    expect(eventBasedAnalytics).not.toHaveBeenCalled()
    const dispatch = jest.fn(() => {
      return Promise.resolve({ body: 'hello' })
    })
    const getState = jest.fn(() => state)
    return action(dispatch, getState).then(() => {
      expect(eventBasedAnalytics).toHaveBeenCalledTimes(1)
      expect(eventBasedAnalytics).toHaveBeenCalledWith({
        events: 'event127',
      })
    })
  })

  it('should call eventBasedAnalytics with proper data when gitcard incorrect', () => {
    const action = checkoutActions.addGiftCard({})
    expect(eventBasedAnalytics).not.toHaveBeenCalled()

    const dispatch = jest.fn(() => {
      return Promise.reject({ response: { body: { message: 'hello' } } })
    })
    const getState = jest.fn(() => state)
    return action(dispatch, getState).then(() => {
      expect(eventBasedAnalytics).toHaveBeenCalledTimes(1)
      expect(eventBasedAnalytics).toHaveBeenCalledWith({
        events: 'event128',
      })
    })
  })

  describe('loadFulfilmentStoreFromCookie', () => {
    const dispatch = jest.fn()

    it('does nothing if delivery store details is not empty', () => {
      getItem.mockImplementationOnce(() => 'STORE_ID')
      const state = {
        selectedBrandFulfilmentStore: {
          key1: 'value1',
        },
        config: {
          siteId: 'br',
        },
      }
      checkoutActions.loadFulfilmentStoreFromCookie()(dispatch, () => state)
      expect(dispatch).not.toHaveBeenCalled()
    })
    it('does nothing if there is no pickup store ID saved in a cookie', () => {
      getItem.mockImplementationOnce(() => undefined)
      const state = {
        selectedBrandFulfilmentStore: {},
        config: {
          siteId: 'br',
        },
      }
      checkoutActions.loadFulfilmentStoreFromCookie()(dispatch, () => state)
      expect(dispatch).not.toHaveBeenCalled()
    })
    it('dispatches the getFulfilmentStore information if missing', () => {
      const mockStoreId = 'TSCOOL1'
      getItem.mockImplementationOnce(() => mockStoreId)
      const state = {
        selectedBrandFulfilmentStore: {},
        config: {
          siteId: 'br',
        },
      }
      checkoutActions.loadFulfilmentStoreFromCookie()(dispatch, () => state)
      expect(dispatch).toHaveBeenCalled()
      expect(storeLocatorActions.getFulfilmentStore).toHaveBeenCalledWith({
        search: `?store_id=${mockStoreId}&brand=${state.config.siteId}`,
      })
    })
  })

  describe('selectDeliveryLocation', () => {
    const initialState = {
      features: {
        status: {
          FEATURE_CFSI: true,
        },
      },
      storeLocator: {
        selectedStore: {},
      },
      checkout: {
        orderSummary: {
          deliveryLocations: [
            { deliveryLocationType: 'HOME' },
            { deliveryLocationType: 'STORE' },
          ],
        },
      },
    }

    const dispatch = jest.fn()
    const getState = jest.fn(() => initialState)

    const executeThunk = (reduxAction) => {
      const thunk = reduxAction
      thunk(dispatch, getState)
    }

    describe('default execution with Home Delivery selected', () => {
      it('updates UI', () => {
        executeThunk(checkoutActions.selectDeliveryLocation(0))
        expect(dispatch.mock.calls[0][0]).toEqual({
          type: 'SET_STORE_UPDATING',
          updating: true,
        })
      })
      it('sets orderSummary with selected option', () => {
        executeThunk(checkoutActions.selectDeliveryLocation(0))
        expect(dispatch.mock.calls[1][0].name).toEqual('setOrderSummaryThunk')
      })
      it('resets delivery store details if any', () => {
        executeThunk(checkoutActions.selectDeliveryLocation(0))
        expect(dispatch.mock.calls[2][0]).toEqual({
          type: 'RESET_STORE_DETAILS',
        })
      })
      it('updates the orderSummary with selected delivery option', () => {
        executeThunk(checkoutActions.selectDeliveryLocation(0))
        expect(dispatch.mock.calls[3][0].name).toEqual(
          'updateDeliveryOptionsThunk'
        )
      })
      it('dispatches an analytics event with the selected delivery option ', () => {
        executeThunk(checkoutActions.selectDeliveryLocation(0))
        expect(dispatch.mock.calls[4][0]).toEqual(
          analyticsActions.sendAnalyticsDeliveryOptionChangeEvent('HOME')
        )
      })
    })
    describe('with Collect From Store selected', () => {
      it('sets a delivery store if CFSI is ON and a brand store was previously defined ', () => {
        getState.mockReturnValue({
          ...initialState,
          selectedBrandFulfilmentStore: {
            name: 'Oxford Street',
          },
        })
        executeThunk(checkoutActions.selectDeliveryLocation(1))
        expect(dispatch.mock.calls[3][0].name).toEqual(
          'selectDeliveryStoreThunk'
        )
      })
    })
  })
})
