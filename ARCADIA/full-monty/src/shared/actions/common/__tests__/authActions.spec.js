import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { browserHistory } from 'react-router'

import * as authActions from '../authActions'
import { post, del } from '../../../lib/api-service'

jest.mock('../../../lib/api-service', () => ({
  get: jest.fn(),
  post: jest.fn(),
  del: jest.fn(),
}))

jest.mock('../wishlistActions', () => ({
  getDefaultWishlist: jest.fn(),
  clearWishlist: jest.fn(),
}))
import { getDefaultWishlist, clearWishlist } from '../wishlistActions'

jest.mock('../shoppingBagActions', () => ({
  getBag: jest.fn(),
  checkForMergedItemsInBag: jest.fn(),
  synchroniseBag: jest.fn(),
  updateShoppingBagBadgeCount: jest.fn(),
}))
import {
  getBag,
  synchroniseBag,
  checkForMergedItemsInBag,
  updateShoppingBagBadgeCount,
} from '../shoppingBagActions'

jest.mock('../navigationActions', () => ({
  updateMenuForUnauthenticatedUser: jest.fn(),
  updateMenuForAuthenticatedUser: jest.fn(),
}))
import {
  updateMenuForUnauthenticatedUser,
  updateMenuForAuthenticatedUser,
} from '../navigationActions'

jest.mock('../paymentMethodsActions', () => ({
  getPaymentMethods: jest.fn(),
}))
import { getPaymentMethods } from '../paymentMethodsActions'

jest.mock('../../../lib/analytics/auth', () => ({
  authEventAnalytics: jest.fn(),
  logoutEventAnalytics: jest.fn(),
}))
import {
  authEventAnalytics,
  logoutEventAnalytics,
} from '../../../lib/analytics/auth'

import { ANALYTICS_ERROR } from '../../../analytics'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
const dispatch = jest.fn()
const getState = jest.fn()
const snapshot = (action) => expect(action).toMatchSnapshot()

// Mocks for the actions called by logging in / out
const logonResponse = {
  body: {},
  headers: {
    bvToken: '1234',
  },
}
const logonMock = Promise.resolve(logonResponse)
logonMock.type = 'LOGON_MOCK'

describe('Authentication actions', () => {
  browserHistory.replace = jest.fn()
  browserHistory.push = jest.fn()
  const defaultState = {
    routing: {
      location: 'test',
    },
    config: {
      language: 'english',
      brandName: 'Topshop',
    },
  }
  const store = mockStore(defaultState)
  beforeEach(() => {
    jest.resetAllMocks()
    store.clearActions()

    post.mockReturnValueOnce(logonMock)

    const logoutMock = Promise.resolve({
      body: {},
    })
    logoutMock.type = 'LOGOUT_MOCK'

    del.mockReturnValueOnce(logoutMock)

    getBag.mockReturnValue({
      type: 'GET_BAG_MOCK',
    })

    getDefaultWishlist.mockReturnValue({
      type: 'GET_WISHLIST_MOCK',
    })

    updateShoppingBagBadgeCount.mockImplementation((count) => ({
      type: 'UPDATE_BADGE_COUNT_MOCK',
      count,
    }))

    updateMenuForAuthenticatedUser.mockReturnValue({
      type: 'UPDATE_MENU_MOCK',
    })

    getPaymentMethods.mockReturnValue({
      type: 'PAYMENT_METHODS_MOCK',
    })

    authEventAnalytics.mockReturnValue({
      type: 'ANALYTICS_MOCK',
    })

    checkForMergedItemsInBag.mockReturnValue({
      type: 'BAG_MERGE_MOCK',
    })

    synchroniseBag.mockReturnValue({
      type: 'BAG_SYNCHRONISE_MOCK',
    })

    clearWishlist.mockReturnValue({
      type: 'CLEAR_WISHLIST_MOCK',
    })

    updateMenuForUnauthenticatedUser.mockReturnValue({
      type: 'UPDATE_MENU_MOCK',
    })

    logoutEventAnalytics.mockReturnValue({
      type: 'LOGOUT_ANALYTICS_MOCK',
    })
  })

  describe(authActions.loginRequest.name, () => {
    describe('login', () => {
      it('login request - success, password NOT reset', async () => {
        const nextRoute = '/next'
        const loginArgs = {
          credentials: { username: 'lol', password: '' },
          getNextRoute: jest.fn().mockReturnValue(nextRoute),
          formName: 'login',
          successCallback: jest.fn(),
        }
        const expectedActions = [
          { type: 'AUTH_PENDING', loading: true },
          { type: 'AJAXCOUNTER_INCREMENT' },
          {
            type: 'SET_FORM_MESSAGE',
            formName: 'login',
            message: {},
            key: null,
            meta: { localise: 'message.message' },
          },
          { type: 'LOGIN', bvToken: undefined, loginLocation: 'test' },
          { type: 'UPDATE_BADGE_COUNT_MOCK' },
          { type: 'AUTH_PENDING', loading: false },
          { type: 'AJAXCOUNTER_DECREMENT' },
          { type: 'USER_ACCOUNT', user: {} },
          { type: 'SET_AUTHENTICATION', authentication: true },
          { type: 'UPDATE_MENU_MOCK' },
          { type: 'PAYMENT_METHODS_MOCK' },
          { type: 'ANALYTICS_MOCK' },
          { type: 'BAG_MERGE_MOCK' },
          { type: 'BAG_SYNCHRONISE_MOCK' },
        ]

        await store.dispatch(authActions.loginRequest(loginArgs))
        expect(store.getActions()).toEqual(
          expect.arrayContaining(expectedActions)
        )
        expect(browserHistory.push).toHaveBeenCalledWith(nextRoute)
        expect(loginArgs.successCallback).toHaveBeenCalled()
      })

      it('login request - success, password reset', async () => {
        post.mockReset()
        browserHistory.push = jest.fn()
        const mockResponse = {
          body: {
            isPwdReset: true,
          },
          headers: {
            bvtoken: 'mocktoken',
          },
        }
        const loginSuccess = Promise.resolve(mockResponse)
        loginSuccess.type = 'POST_MOCK'
        post.mockReturnValueOnce(loginSuccess)
        const loginArgs = {
          credentials: { username: 'lol', password: 'hello' },
          getNextRoute: () => '/next',
          formName: 'login',
          successCallback: jest.fn(),
        }
        const expectedActions = [
          { type: 'AUTH_PENDING', loading: true },
          { type: 'AJAXCOUNTER_INCREMENT' },
          {
            type: 'SET_FORM_MESSAGE',
            formName: 'login',
            message: {},
            key: null,
            meta: { localise: 'message.message' },
          },
          { type: 'AUTH_PENDING', loading: false },
          { type: 'AJAXCOUNTER_DECREMENT' },
          { type: 'USER_ACCOUNT', user: { isPwdReset: true } },
          { type: 'SET_AUTHENTICATION', authentication: true },
          { type: 'UPDATE_MENU_MOCK' },
          { type: 'BAG_SYNCHRONISE_MOCK' },
        ]

        await store.dispatch(authActions.loginRequest(loginArgs))
        expect(store.getActions()).toEqual(
          expect.arrayContaining(expectedActions)
        )
        expect(browserHistory.push).toHaveBeenCalledWith('/reset-password')
        expect(loginArgs.successCallback).toHaveBeenCalled()
      })

      it('login request - success, defaults to /my-account', async () => {
        post.mockReset()
        browserHistory.push = jest.fn()
        const mockResponse = {
          body: {
            isPwdReset: false,
          },
          headers: {
            bvtoken: 'mocktoken',
          },
        }
        const loginSuccess = Promise.resolve(mockResponse)
        loginSuccess.type = 'POST_MOCK'
        post.mockReturnValueOnce(loginSuccess)
        const loginArgs = {
          credentials: { username: 'lol', password: 'hello' },
          getNextRoute: () => null,
          formName: 'login',
          successCallback: jest.fn(),
        }

        await store.dispatch(authActions.loginRequest(loginArgs))
        expect(browserHistory.push).toHaveBeenCalledWith('/my-account')
        expect(loginArgs.successCallback).toHaveBeenCalled()
      })

      it('login request - fail', async () => {
        post.mockReset()
        const loginRejected = Promise.reject({
          response: { body: { message: 'error' } },
        })
        loginRejected.type = 'POST_MOCK'
        post.mockReturnValueOnce(loginRejected)
        const loginArgs = {
          credentials: { username: 'lol', password: 'hello' },
          getNextRoute: () => '/next',
          formName: 'login',
          successCallback: jest.fn(),
          errorCallback: jest.fn(),
        }
        const expectedActions = [
          { type: 'AUTH_PENDING', loading: true },
          { type: 'AJAXCOUNTER_INCREMENT' },
          {
            type: 'SET_FORM_MESSAGE',
            formName: 'login',
            message: {},
            key: null,
            meta: { localise: 'message.message' },
          },
          {
            type: 'SET_FORM_MESSAGE',
            formName: 'login',
            message: { type: 'error', message: 'error' },
            key: null,
            meta: { localise: 'message.message' },
          },
          { type: 'AUTH_PENDING', loading: false },
          { type: 'ANALYTICS_MOCK' },
          { type: 'AJAXCOUNTER_DECREMENT' },
        ]

        await store.dispatch(authActions.loginRequest(loginArgs))
        expect(store.getActions()).toEqual(
          expect.arrayContaining(expectedActions)
        )
        expect(loginArgs.errorCallback).toHaveBeenCalled()
        expect(loginArgs.successCallback).not.toHaveBeenCalled()
      })

      it('should call getBag with isMergeRequest = true if not on checkout login', async () => {
        const loginArgs = {
          credentials: { username: 'lol', password: 'hello' },
          getNextRoute: () => '/next',
          formName: 'login',
          successCallback: undefined,
        }
        await store.dispatch(authActions.loginRequest(loginArgs))
        expect(getBag).toHaveBeenCalled()
        expect(getBag).toHaveBeenCalledWith(true, true)
      })
    })

    describe('Wishlist enabled', () => {
      it('should dispatch the action to get the wishlist after login', async () => {
        const store = mockStore({
          ...defaultState,
          features: {
            status: {
              FEATURE_WISHLIST: true,
            },
          },
        })

        const expectedActions = [
          {
            type: 'GET_WISHLIST_MOCK',
          },
        ]

        await store.dispatch(
          authActions.loginRequest({
            successCallback: false,
            getNextRoute: false,
          })
        )
        expect(store.getActions()).toEqual(
          expect.arrayContaining(expectedActions)
        )
      })
    })

    describe('Wishlist disabled', () => {
      it('should not dispatch the action to get the wishlist after login', async () => {
        const store = mockStore(defaultState)
        await store.dispatch(
          authActions.loginRequest({
            getNextRoute: false,
          })
        )
        const expectedActions = [
          {
            type: 'GET_WISHLIST_MOCK',
          },
        ]
        expect(store.getActions()).not.toEqual(
          expect.arrayContaining(expectedActions)
        )
      })
    })

    describe('login via WishlistLoginModal', () => {
      it('should dispatch getBag on login', async () => {
        const expectedAction = [{ type: 'GET_BAG_MOCK' }]
        const authArgs = {
          formData: {},
          formName: 'wishlistLoginModal',
        }
        await store.dispatch(authActions.loginRequest(authArgs))
        expect(store.getActions()).toEqual(
          expect.arrayContaining(expectedAction)
        )
      })
    })

    describe('login error', () => {
      it('sets error message on login form', async () => {
        post.mockReset()
        const loginRejected = Promise.reject({
          response: {
            body: {
              message: 'error message',
            },
          },
        })
        loginRejected.type = 'LOGIN_MOCK'
        post.mockReturnValueOnce(loginRejected)

        const store = mockStore(defaultState)
        await store.dispatch(
          authActions.loginRequest({
            getNextRoute: false,
          })
        )

        const expectedAction = [
          {
            type: 'SET_FORM_MESSAGE',
            formName: 'login',
            message: {
              type: 'error',
              message: 'error message',
            },
            key: null,
            meta: {
              localise: 'message.message',
            },
          },
          {
            type: `MONTY/ANALYTICS.SEND_ERROR_MESSAGE`,
            errorMessage: ANALYTICS_ERROR.LOGIN_FAILED,
          },
        ]

        expect(store.getActions()).toEqual(
          expect.arrayContaining(expectedAction)
        )
      })
    })
  })

  describe(authActions.logout.name, () => {
    describe('logout', () => {
      it('dispatches a chain of actions', () => {
        const expectedActions = [
          { type: 'LOGOUT' },
          { type: 'UPDATE_MENU_MOCK' },
        ]
        store.dispatch(authActions.logout())
        expect(store.getActions()).toEqual(
          expect.arrayContaining(expectedActions)
        )
      })
    })
  })

  // @TODO to complete test coverage
  describe(authActions.logoutRequest.name, () => {
    it('calls browserHistory with default `home` path', async () => {
      await store.dispatch(authActions.logoutRequest())
      expect(browserHistory.replace).toHaveBeenCalledTimes(1)
      expect(browserHistory.replace).toHaveBeenCalledWith({ pathname: '/' })
    })

    it('calls browserHistory with specified route', async () => {
      const routeLocation = '/test'

      await store.dispatch(authActions.logoutRequest(routeLocation))
      expect(browserHistory.replace).toHaveBeenCalledTimes(1)
      expect(browserHistory.replace).toHaveBeenCalledWith({
        pathname: routeLocation,
      })
    })

    it('sets shopping Bag Badge Count to 0', async () => {
      const expectedAction = {
        type: 'UPDATE_BADGE_COUNT_MOCK',
        count: 0,
      }

      await store.dispatch(authActions.logoutRequest())
      expect(store.getActions()).toEqual(
        expect.arrayContaining([expectedAction])
      )
    })

    describe('Wishlist enabled', () => {
      it('should dispatch the action to clear the wishlist after logout', async () => {
        const store = mockStore({
          ...defaultState,
          features: {
            status: {
              FEATURE_WISHLIST: true,
            },
          },
        })

        const expectedActions = [
          {
            type: 'CLEAR_WISHLIST_MOCK',
          },
        ]

        await store.dispatch(authActions.logoutRequest())
        expect(store.getActions()).toEqual(
          expect.arrayContaining(expectedActions)
        )
      })
    })

    describe('Wishlist not enabled', () => {
      it('should not call the action to clear the wishlist', async () => {
        const store = mockStore(defaultState)

        const expectedActions = [
          {
            type: 'CLEAR_WISHLIST_MOCK',
          },
        ]

        await store.dispatch(authActions.logoutRequest())
        expect(store.getActions()).not.toEqual(
          expect.arrayContaining(expectedActions)
        )
      })
    })
  })

  describe('authentication', () => {
    it('authPending - true', () => {
      snapshot(authActions.authPending(true))
    })

    it('authPending - false', () => {
      snapshot(authActions.authPending(false))
    })

    it('authLogin', () => {
      const mockBvToken = 'abcd45'
      getState.mockReturnValueOnce({ routing: { location: '/somewhere' } })

      authActions.authLogin(mockBvToken)(dispatch, getState)
      expect(dispatch).toHaveBeenCalledWith({
        type: 'LOGIN',
        bvToken: mockBvToken,
        loginLocation: '/somewhere',
      })
    })

    it('setAuthentication - true', () => {
      snapshot(authActions.setAuthentication(true))
    })

    it('setAuthentication - false', () => {
      snapshot(authActions.setAuthentication(false))
    })
  })

  describe('registerRequest', () => {
    it('register success action', () => {
      snapshot(authActions.registerSuccess({ user: 'user thing' }))
    })

    it('register error action', () => {
      snapshot(authActions.registerError({ error: 'i am an error' }))
    })

    it('register success, wishlist DISABLED', async () => {
      const nextRoute = '/nextRoute'
      const expectedActions = [
        { type: 'AUTH_PENDING', loading: true },
        { type: 'AJAXCOUNTER_INCREMENT' },
        {
          type: 'SET_FORM_MESSAGE',
          formName: 'register',
          message: {},
          key: null,
          meta: { localise: 'message.message' },
        },
        { type: 'LOGIN', bvToken: undefined, loginLocation: 'test' },
        { type: 'USER_ACCOUNT', user: {} },
        { type: 'SET_AUTHENTICATION', authentication: true },
        { type: 'AUTH_PENDING', loading: false },
        { type: 'AJAXCOUNTER_DECREMENT' },
        { type: 'UPDATE_MENU_MOCK' },
        { type: 'ANALYTICS_MOCK' },
      ]

      await store.dispatch(
        authActions.registerRequest({
          formData: {},
          getNextRoute: () => nextRoute,
        })
      )
      expect(store.getActions()).toEqual(
        expect.arrayContaining(expectedActions)
      )
      expect(browserHistory.push).toHaveBeenCalledWith(nextRoute)
    })
    it('register success, no next route, defaults to /my-account', async () => {
      await store.dispatch(
        authActions.registerRequest({
          formData: {},
          getNextRoute: () => null,
        })
      )
      expect(browserHistory.push).toHaveBeenCalledWith('/my-account')
    })

    it('register success, wishlist ENABLED', async () => {
      const nextRoute = '/nextRoute'
      const store = mockStore({
        ...defaultState,
        features: {
          status: {
            FEATURE_WISHLIST: true,
          },
        },
      })

      const expectedActions = [
        { type: 'AUTH_PENDING', loading: true },
        { type: 'AJAXCOUNTER_INCREMENT' },
        {
          type: 'SET_FORM_MESSAGE',
          formName: 'register',
          message: {},
          key: null,
          meta: { localise: 'message.message' },
        },
        { type: 'LOGIN', bvToken: undefined, loginLocation: 'test' },
        { type: 'USER_ACCOUNT', user: {} },
        { type: 'SET_AUTHENTICATION', authentication: true },
        { type: 'AUTH_PENDING', loading: false },
        { type: 'AJAXCOUNTER_DECREMENT' },
        { type: 'UPDATE_MENU_MOCK' },
        { type: 'ANALYTICS_MOCK' },
        { type: 'GET_WISHLIST_MOCK' },
      ]

      await store.dispatch(
        authActions.registerRequest({
          formData: {},
          getNextRoute: () => nextRoute,
        })
      )
      expect(store.getActions()).toEqual(
        expect.arrayContaining(expectedActions)
      )
      expect(browserHistory.push).toHaveBeenCalledWith(nextRoute)
    })

    it('register error', async () => {
      post.mockReset()
      const mockErrorResponse = { response: { body: { message: 'error' } } }
      const postMock = Promise.reject(mockErrorResponse)
      postMock.type = 'POST_MOCK'
      post.mockReturnValueOnce(postMock)
      const expectedActions = [
        {
          type: 'SET_FORM_MESSAGE',
          formName: 'register',
          message: {},
          key: null,
          meta: { localise: 'message.message' },
        },
        {
          type: 'SET_FORM_MESSAGE',
          formName: 'register',
          message: { type: 'error', message: 'error' },
          key: null,
          meta: { localise: 'message.message' },
        },
        {
          type: `MONTY/ANALYTICS.SEND_ERROR_MESSAGE`,
          errorMessage: ANALYTICS_ERROR.REGISTRATION_FAILED,
        },
      ]

      await store.dispatch(authActions.registerRequest({}, '/anotherRoute'))
      expect(store.getActions()).toEqual(
        expect.arrayContaining(expectedActions)
      )
      expect(browserHistory.push).not.toHaveBeenCalled()
    })

    it('register error, user logged in when registering', async () => {
      post.mockReset()
      const mockErrorResponse = {
        response: {
          body: {
            originalMessage:
              'Unable to create account while user logged in, please logout',
          },
        },
      }
      const delMock = Promise.resolve({})
      delMock.type = 'DELETE_MOCK'
      const postMock = Promise.reject(mockErrorResponse)
      postMock.type = 'POST_MOCK'

      del.mockReturnValueOnce(delMock)
      post.mockReturnValue(postMock)
      getState.mockReturnValueOnce({ routing: { location: '/somewhere' } })
      const expectedActions = [
        {
          type: 'SET_FORM_MESSAGE',
          formName: 'register',
          message: {},
          key: null,
          meta: {
            localise: 'message.message',
          },
        },
      ]

      await store.dispatch(authActions.registerRequest({}, '/anotherRoute'))
      expect(store.getActions()).toEqual(
        expect.arrayContaining(expectedActions)
      )
      expect(del).toHaveBeenCalledWith('/account/logout')
      expect(
        store
          .getActions()
          .filter((action) => action.type === 'SET_FORM_MESSAGE').length
      ).toEqual(1)
    })

    describe('register via WishlistLoginModal', () => {
      it('should dispatch getBag on register', async () => {
        const expectedAction = [{ type: 'GET_BAG_MOCK' }]
        const authArgs = {
          formData: {},
          formName: 'wishlistLoginModal',
        }
        await store.dispatch(authActions.registerRequest(authArgs))
        expect(store.getActions()).toEqual(
          expect.arrayContaining(expectedAction)
        )
      })
    })
  })

  describe(authActions.setInitialWishlist.name, () => {
    const getDefaultWishlistMock = () => {
      const p = Promise.resolve({})
      p.type = 'GET_DEFAULT_WISHLIST_MOCK'
      return p
    }
    const expectedActions = [
      { type: 'AJAXCOUNTER_INCREMENT' },
      getDefaultWishlistMock(),
      { type: 'AJAXCOUNTER_DECREMENT' },
    ]

    it('should get the default wishlist', async () => {
      getDefaultWishlist.mockReturnValue(getDefaultWishlistMock())
      await store.dispatch(authActions.setInitialWishlist())
      expect(store.getActions()).toEqual(expectedActions)
    })

    it('should resolve the success callback if passed', async () => {
      const successCallbackMock = jest.fn(() => Promise.resolve())
      getDefaultWishlist.mockReturnValue(getDefaultWishlistMock())
      await store.dispatch(authActions.setInitialWishlist(successCallbackMock))
      expect(store.getActions()).toEqual(expectedActions)
      expect(successCallbackMock).toHaveBeenCalled()
    })
  })
})
