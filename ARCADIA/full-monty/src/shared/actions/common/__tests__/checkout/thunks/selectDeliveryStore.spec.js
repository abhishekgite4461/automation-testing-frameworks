import { selectDeliveryStore } from '../../../checkoutActions'
import { setStoreCookie } from '../../../../../../client/lib/cookie'
import { setSelectedBrandFulfilmentStore } from '../../../../common/selectedBrandFulfilmentStoreActions'
import { browserHistory } from 'react-router'
import { isReturningCustomer } from '../../../../../selectors/checkoutSelectors'
import { isFeatureCheckoutV2Enabled } from '../../../../../selectors/featureSelectors'

jest.mock('../../../../../../client/lib/cookie', () => ({
  setStoreCookie: jest.fn(),
}))
jest.mock('../../../../common/selectedBrandFulfilmentStoreActions')
jest.mock('react-router', () => ({
  browserHistory: {
    push: jest.fn(),
  },
}))
jest.mock('../../../../../selectors/checkoutSelectors', () => ({
  isReturningCustomer: jest.fn(() => false),
}))
jest.mock('../../../../../selectors/featureSelectors', () => ({
  isFeatureCheckoutV2Enabled: jest.fn(() => false),
}))
jest.mock('../../../../../selectors/ddpSelectors', () => ({}))

describe('selectDeliveryStore()', () => {
  const dispatch = jest.fn((fn) => fn)
  const getState = jest.fn()
  const deliveryStore = {
    storeId: 'TS001',
    brandName: 'Topshop',
    address: {
      line1: 'line1',
      line2: 'line2',
      city: 'city',
      postcode: 'postcode',
    },
  }
  const initialState = {
    routing: {
      location: {
        query: {
          isAnonymous: true,
        },
      },
    },
    config: {
      brandName: 'Topshop',
    },
    account: {
      user: {},
    },
  }

  beforeEach(() => {
    jest.clearAllMocks()
    getState.mockReturnValue(initialState)
  })

  function executeThunk(action) {
    const promise = Promise.resolve()
    dispatch.mockImplementation(() => promise)

    const thunk = action
    thunk(dispatch, getState)

    return promise
  }

  describe('default execution', () => {
    it('sets DeliveryStore', () => {
      executeThunk(selectDeliveryStore(deliveryStore))
      expect(dispatch.mock.calls[0][0]).toEqual({
        type: 'SET_DELIVERY_STORE',
        store: {
          deliveryStoreCode: 'TS001',
          storeAddress1: 'line1',
          storeAddress2: 'line2',
          storeCity: 'city',
          storePostcode: 'postcode',
        },
      })
    })

    it('updates UI', () => {
      executeThunk(selectDeliveryStore(deliveryStore))
      expect(dispatch.mock.calls[1][0]).toEqual({
        type: 'SET_STORE_UPDATING',
        updating: false,
      })
    })

    it('updates Delivery options with selected store', () => {
      executeThunk(selectDeliveryStore(deliveryStore))
      expect(dispatch.mock.calls[3][0].name).toBe('updateDeliveryOptionsThunk')
    })

    describe('with brand store selected (aka arcadia shop)', () => {
      it('sets cookie and delivery store details', () => {
        executeThunk(selectDeliveryStore(deliveryStore))
        expect(setStoreCookie).toHaveBeenCalledTimes(1)
        expect(setStoreCookie).toHaveBeenCalledWith(deliveryStore)
        expect(setSelectedBrandFulfilmentStore).toHaveBeenCalledTimes(1)
        expect(setSelectedBrandFulfilmentStore).toHaveBeenLastCalledWith(
          deliveryStore
        )
      })
    })

    describe('with parcel shop selected', () => {
      it('should not set cookie or delivery store details', () => {
        executeThunk(
          selectDeliveryStore({
            ...deliveryStore,
            storeId: 'S0007',
          })
        )
        expect(setStoreCookie).not.toHaveBeenCalled()
        expect(setSelectedBrandFulfilmentStore).not.toHaveBeenCalled()
      })
    })
  })

  it('redirects to delivery', () => {
    const promise = executeThunk(selectDeliveryStore(deliveryStore))
    isReturningCustomer.mockImplementation(() => false)

    return promise.then(() => {
      expect(browserHistory.push).toHaveBeenCalledTimes(1)
      expect(browserHistory.push).toHaveBeenCalledWith({
        pathname: '/checkout/delivery',
        query: {
          isAnonymous: true,
        },
      })
    })
  })

  describe('in checkout V2 workflow', () => {
    it('redirects to delivery and payment when the delivery details and credit card details are available', () => {
      isReturningCustomer.mockImplementation(() => true)
      isFeatureCheckoutV2Enabled.mockImplementation(() => true)
      getState.mockReturnValue({
        ...initialState,
        features: {
          status: {
            FEATURE_NEW_CHECKOUT: true,
          },
        },
      })
      const promise = executeThunk(selectDeliveryStore(deliveryStore))

      return promise.then(() => {
        expect(browserHistory.push).toHaveBeenCalledTimes(1)
        expect(browserHistory.push).toHaveBeenCalledWith({
          pathname: '/checkout/delivery-payment',
          query: {
            isAnonymous: true,
          },
        })
      })
    })
  })
})
