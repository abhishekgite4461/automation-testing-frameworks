import { findAddress, getAddressByMoniker } from '../../../checkoutActions'

// Helper
jest.mock('../../../../../lib/localisation')
import { localise } from '../../../../../lib/localisation'

describe('findAddress()', () => {
  const dispatch = jest.fn()
  const getState = jest.fn()

  beforeEach(() => {
    jest.clearAllMocks()
    getState.mockReturnValue({
      config: {
        brandName: '',
        language: '',
      },
    })
  })
  const action = findAddress({}, 'yourAddressFormName', 'findAddressFormName')

  it('should increment and then decrement ajax counter on success', () => {
    dispatch.mockImplementation(() => {
      return Promise.resolve({ body: {} })
    })
    return action(dispatch, getState).then(() => {
      expect(dispatch.mock.calls[0][0]).toEqual({
        type: 'AJAXCOUNTER_INCREMENT',
      })
      expect(dispatch.mock.calls[3][0]).toEqual({
        type: 'AJAXCOUNTER_DECREMENT',
      })
    })
  })

  it('should increment and then decrement ajax counter on failure', () => {
    dispatch.mockImplementation(() => {
      return Promise.reject({ response: { body: { message: '' } } })
    })
    return action(dispatch, getState).then(() => {
      expect(dispatch.mock.calls[0][0]).toEqual({
        type: 'AJAXCOUNTER_INCREMENT',
      })
      expect(dispatch.mock.calls[3][0]).toEqual({
        type: 'AJAXCOUNTER_DECREMENT',
      })
    })
  })

  it('should clear form message', () => {
    dispatch.mockImplementation(() => {
      return Promise.resolve({ body: {} })
    })
    return action(dispatch, getState).then(() => {
      expect(dispatch.mock.calls[1][0]).toEqual({
        type: 'SET_FORM_MESSAGE',
        formName: 'findAddressFormName',
        key: null,
        message: {
          message: '',
          type: 'error',
        },
        meta: {
          localise: 'message.message',
        },
      })
    })
  })

  it('should call setMonikerAddress on success', () => {
    dispatch.mockImplementation(() => {
      return Promise.resolve({ body: {} })
    })
    return action(dispatch, getState).then(() => {
      expect(dispatch.mock.calls[4][0]).toEqual({
        type: 'UPDATE_MONIKER',
        data: {},
      })
    })
  })

  it('should call setMonikerAddress on success', () => {
    dispatch.mockImplementation(() => {
      return Promise.resolve({ body: {} })
    })
    return action(dispatch, getState).then(() => {
      expect(dispatch.mock.calls[4][0]).toEqual({
        type: 'UPDATE_MONIKER',
        data: {},
      })
    })
  })

  it('should call getAddressByMoniker on success if res.body has length', () => {
    dispatch.mockImplementation(() => {
      return Promise.resolve({ body: [{}] })
    })
    return action(dispatch, getState).then(() => {
      expect(dispatch.mock.calls[5][0].name).toBe('getAddressByMonikerThunk')
    })
  })

  it('should set form message to error on success if res.body has no length', () => {
    dispatch.mockImplementation(() => {
      return Promise.resolve({ body: [] })
    })
    localise.mockImplementation(() => {
      return 'We are unable to find your address at the moment. Please enter your address manually.'
    })
    return action(dispatch, getState).then(() => {
      expect(dispatch.mock.calls[5][0]).toEqual({
        type: 'SET_FORM_MESSAGE',
        formName: 'findAddressFormName',
        key: null,
        message: {
          message:
            'We are unable to find your address at the moment. Please enter your address manually.',
          type: 'error',
        },
        meta: {
          localise: 'message.message',
        },
      })
    })
  })

  it('should set form message to error on failure and statusCode is 503', () => {
    dispatch.mockImplementation(() => {
      return Promise.reject({ response: { statusCode: 503 } })
    })
    return action(dispatch, getState).then(() => {
      expect(dispatch.mock.calls[4][0]).toEqual({
        type: 'SET_FORM_MESSAGE',
        formName: 'findAddressFormName',
        key: null,
        message: {
          message:
            'We are unable to find your address at the moment. Please enter your address manually.',
          type: 'error',
        },
        meta: {
          localise: 'message.message',
        },
      })
    })
  })

  it('should set form message to error on failure and statusCode is not 503 and theres no message', () => {
    dispatch.mockImplementation(() => {
      return Promise.reject({ response: { body: { message: '' } } })
    })
    return action(dispatch, getState).then(() => {
      expect(dispatch.mock.calls[4][0]).toEqual({
        type: 'SET_FORM_MESSAGE',
        formName: 'findAddressFormName',
        key: null,
        message: {
          message:
            'We are unable to find your address at the moment. Please enter your address manually.',
          type: 'error',
        },
        meta: {
          localise: 'message.message',
        },
      })
    })
  })

  it('should set form message to message on failure and statusCode is not 503 and theres is a message', () => {
    dispatch.mockImplementation(() => {
      return Promise.reject({ response: { body: { message: 'testMessage' } } })
    })
    return action(dispatch, getState).then(() => {
      expect(dispatch.mock.calls[4][0]).toEqual({
        type: 'SET_FORM_MESSAGE',
        formName: 'findAddressFormName',
        key: null,
        message: {
          message: 'testMessage',
          type: 'error',
        },
        meta: {
          localise: 'message.message',
        },
      })
    })
  })
})

describe('getAddressByMoniker', () => {
  const getAddressByMonikerAction = getAddressByMoniker(
    { moniker: 'VVVFGGGHHHTTFGGDDF4455566', country: 'UK' },
    'formName'
  )

  const dispatch = jest.fn()
  const getState = jest.fn()

  const fakeAddressBody = {
    adress: 'Street',
    postcode: '234556',
    city: 'London',
  }

  beforeEach(() => {
    jest.clearAllMocks()
    getState.mockReturnValue({
      checkout: {
        orderSummary: {},
      },
      routing: {
        location: {
          pathname: '/my-account/accountDetails',
        },
      },
    })
  })

  describe('Invoke function from Checkout', () => {
    it('Call GetAddressByMoniker should call setOrderSummary (mean call from Checkout)', () => {
      dispatch.mockImplementation(() => {
        return Promise.resolve({ body: fakeAddressBody })
      })
      getState.mockReturnValue({
        checkout: {
          orderSummary: {},
        },
        routing: {
          location: {
            pathname: '/checkout/accountDetails',
          },
        },
      })
      return getAddressByMonikerAction(dispatch, getState).then(() => {
        expect(dispatch.mock.calls[5][0].name).toBe('setOrderSummaryThunk')
      })
    })
    it('Call GetAddressByMoniker whit no response should call a error', () => {
      dispatch.mockImplementation(() => {
        return Promise.reject({ response: { body: 'no address' } })
      })
      return getAddressByMonikerAction(dispatch, getState).then(() => {
        expect(dispatch.mock.calls[3][0]).toEqual({
          formName: 'formName',
          key: null,
          message: undefined,
          meta: {
            localise: 'message.message',
          },
          type: 'SET_FORM_MESSAGE',
        })
      })
    })
  })

  describe('Invoke function from MyAccount --> MyCheckoutDetails', () => {
    it('Call GetAddressByMoniker should NOT call setOrderSummary', () => {
      dispatch.mockImplementation(() => {
        return Promise.resolve({ body: fakeAddressBody })
      })
      return getAddressByMonikerAction(dispatch, getState).then(() => {
        expect(dispatch.mock.calls[5]).toBe(undefined)
      })
    })
    it('Call GetAddressByMoniker should call resetForm', () => {
      dispatch.mockImplementation(() => {
        return Promise.resolve({ body: fakeAddressBody })
      })
      return getAddressByMonikerAction(dispatch, getState).then(() => {
        expect(dispatch.mock.calls[3][0]).toEqual({
          type: 'RESET_FORM',
          formName: 'formName',
          initialValues: {
            adress: 'Street',
            postcode: '234556',
            city: 'London',
          },
          key: null,
        })
      })
    })
  })
})
