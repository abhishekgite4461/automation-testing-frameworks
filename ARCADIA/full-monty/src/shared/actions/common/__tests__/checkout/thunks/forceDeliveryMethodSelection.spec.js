import { forceDeliveryMethodSelection } from '../../../checkoutActions'
import {
  getSelectedDeliveryMethod,
  shouldUpdateOrderSummaryStore,
} from '../../../../../selectors/checkoutSelectors'

jest.mock('../../../../../selectors/checkoutSelectors', () => ({
  getSelectedDeliveryMethod: jest.fn(),
  shouldUpdateOrderSummaryStore: jest.fn(),
}))

describe(forceDeliveryMethodSelection.name, () => {
  const dispatch = jest.fn()
  const getState = jest.fn()

  beforeEach(() => {
    jest.clearAllMocks()
  })

  const executeThunk = (actionThunk) => actionThunk(dispatch, getState)

  it('should not update orderSummary delivery options if there is a selectedDeliveryMethod or orderSummary store does not need to be updated', () => {
    getSelectedDeliveryMethod.mockImplementation(() => ({
      deliveryType: 'STORE_EXPRESS',
      selected: true,
    }))
    shouldUpdateOrderSummaryStore.mockImplementation(() => false)

    executeThunk(forceDeliveryMethodSelection())
    expect(dispatch).toHaveBeenCalledTimes(0)
  })
  it('should update OrderSummary delivery options if there is not selectedDeliveryMethod', () => {
    getSelectedDeliveryMethod.mockImplementation(() => null)
    shouldUpdateOrderSummaryStore.mockImplementation(() => false)

    executeThunk(forceDeliveryMethodSelection())
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0].name).toBe('updateDeliveryOptionsThunk')
  })
  it('should update OrderSummary delivery options if orderSummary store needs to be updated', () => {
    getSelectedDeliveryMethod.mockImplementation(() => ({
      deliveryType: 'STORE_EXPRESS',
      selected: true,
    }))
    shouldUpdateOrderSummaryStore.mockImplementation(() => true)

    executeThunk(forceDeliveryMethodSelection())
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0].name).toBe('updateDeliveryOptionsThunk')
  })
})
