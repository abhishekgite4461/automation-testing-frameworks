import nock from 'nock'
import { identity } from 'ramda'

import * as apiService from '../../../lib/api-service'
import * as sandboxActions from '../sandBoxActions'
import * as featureSelectors from '../../../selectors/featureSelectors'
import espotsDesktopConstants from '../../../constants/espotsDesktop'

jest.mock('../../../lib/api-service')
jest.mock('../../../selectors/featureSelectors')

// The following is necessary in order to avoid using the manual mock of 'superagent' created for
// other unit tests. These tests have been written before the implementation of the manual mocking
// and there is no need here of the usage of it.
jest.unmock('superagent')

const getState = () => ({
  config: 'config',
  viewport: {},
})

describe('sandboxActions', () => {
  beforeEach(() => {
    nock.cleanAll()
  })

  describe('#getContent', () => {
    it('dispatches SET_SANDBOX_CONTENT with body and pageName', () => {
      const nockScope = nock('https://cms.digital.arcadiagroup.co.uk')
        .get('/cmscontent')
        .query({
          location: { pathname: '' },
          cmsPageName: 'pageName',
        })
        .reply(200, {
          foo: 'bar',
        })
      const actionCreator = sandboxActions.getContent(
        { pathname: '' },
        'pageName',
        undefined,
        undefined,
        undefined,
        { host: 'https://cms.digital.arcadiagroup.co.uk' }
      )
      const dispatch = jest.fn()

      expect(dispatch).toHaveBeenCalledTimes(0)

      return actionCreator(dispatch, getState).then(() => {
        expect(dispatch).toHaveBeenCalledTimes(1)
        expect(dispatch).toHaveBeenCalledWith({
          type: 'SET_SANDBOX_CONTENT',
          key: 'pageName',
          content: {
            foo: 'bar',
          },
        })
        expect(nockScope.isDone()).toBe(true)
      })
    })

    it('dispatches SET_SANDBOX_CONTENT with body and location.pathname', () => {
      const nockScope = nock('https://cms.digital.arcadiagroup.co.uk')
        .get('/cmscontent')
        .query({
          location: { pathname: 'locationpathname' },
        })
        .reply(200, {
          foo: 'bar',
        })
      const actionCreator = sandboxActions.getContent(
        { pathname: 'locationpathname' },
        undefined,
        undefined,
        undefined,
        undefined,
        { host: 'https://cms.digital.arcadiagroup.co.uk' }
      )
      const dispatch = jest.fn()

      expect(dispatch).toHaveBeenCalledTimes(0)

      return actionCreator(dispatch, getState).then(() => {
        expect(dispatch).toHaveBeenCalledTimes(1)
        expect(dispatch).toHaveBeenCalledWith({
          type: 'SET_SANDBOX_CONTENT',
          key: 'locationpathname',
          content: { foo: 'bar' },
        })
        expect(nockScope.isDone()).toBe(true)
      })
    })

    it('dispatches SET_SANDBOX_CONTENT with error content in case of missing body in response', () => {
      const nockScope = nock('https://cms.digital.arcadiagroup.co.uk')
        .get('/cmscontent')
        .query({
          location: { pathname: 'locationpathname' },
        })
        .reply(200)
      const actionCreator = sandboxActions.getContent(
        { pathname: 'locationpathname' },
        undefined,
        undefined,
        true,
        undefined,
        { host: 'https://cms.digital.arcadiagroup.co.uk' }
      )
      const dispatch = jest.fn()

      expect(dispatch).toHaveBeenCalledTimes(0)

      return actionCreator(dispatch, getState).then(() => {
        expect(dispatch).toHaveBeenCalledTimes(1)
        expect(dispatch).toHaveBeenCalledWith({
          type: 'SET_SANDBOX_CONTENT',
          key: 'locationpathname',
          content: 'Empty response body from mrCMS',
        })
        expect(nockScope.isDone()).toBe(true)
      })
    })

    it('dispatches SET_SANDBOX_CONTENT with error message in case e-spot error', () => {
      const nockScope = nock('https://cms.digital.arcadiagroup.co.uk')
        .get('/cmscontent')
        .query({
          cmsPageName: 'espot1',
        })
        .reply(200, {
          props: {
            data: {
              pageName: 'error - 404',
            },
          },
        })
      const actionCreator = sandboxActions.getContent(
        undefined,
        'espot1',
        'espot',
        true,
        undefined,
        { host: 'https://cms.digital.arcadiagroup.co.uk' }
      )
      const dispatch = jest.fn()

      expect(dispatch).toHaveBeenCalledTimes(0)

      return actionCreator(dispatch, getState).then(() => {
        expect(dispatch).toHaveBeenCalledTimes(1)
        expect(dispatch).toHaveBeenCalledWith({
          type: 'SET_SANDBOX_CONTENT',
          key: 'espot1',
          content: 'Error retrieving CMS content',
        })
        expect(nockScope.isDone()).toBe(true)
      })
    })

    it('should pass the `mobileCMSUrl` from the query string to the `cmscontent` endpoint', () => {
      const nockScope = nock('https://cms.digital.arcadiagroup.co.uk')
        .get('/cmscontent')
        .query({
          location: {
            query: {
              mobileCMSUrl:
                'https://www.topshop.com/cms/pages/json/json-0000116968/json-0000116968.json',
            },
          },
          mobileCMSUrl:
            'https://www.topshop.com/cms/pages/json/json-0000116968/json-0000116968.json',
        })
        .reply(200)
      const actionCreator = sandboxActions.getContent(
        {
          query: {
            mobileCMSUrl:
              'https://www.topshop.com/cms/pages/json/json-0000116968/json-0000116968.json',
          },
        },
        undefined,
        undefined,
        undefined,
        undefined,
        { host: 'https://cms.digital.arcadiagroup.co.uk' }
      )

      return actionCreator(() => {}, getState).then(() => {
        expect(nockScope.isDone()).toBe(true)
      })
    })

    it('should pass the `responsiveCMSUrl` from the query string to the `cmscontent` endpoint', () => {
      const nockScope = nock('https://cms.digital.arcadiagroup.co.uk')
        .get('/cmscontent')
        .query({
          location: {
            query: {
              responsiveCMSUrl:
                'https://www.topshop.com/cms/pages/json/json-0000116968/json-0000116968.json',
            },
          },
          responsiveCMSUrl:
            'https://www.topshop.com/cms/pages/json/json-0000116968/json-0000116968.json',
        })
        .reply(200)
      const actionCreator = sandboxActions.getContent(
        {
          query: {
            responsiveCMSUrl:
              'https://www.topshop.com/cms/pages/json/json-0000116968/json-0000116968.json',
          },
        },
        undefined,
        undefined,
        undefined,
        undefined,
        { host: 'https://cms.digital.arcadiagroup.co.uk' }
      )

      return actionCreator(() => {}, getState).then(() => {
        expect(nockScope.isDone()).toBe(true)
      })
    })

    it('sends viewportMedia and siteId query parameter', () => {
      const tsukSiteId = 12556
      const nockScope = nock('https://whatever')
        .get('/cmscontent')
        .query({
          viewportMedia: 'desktop',
          siteId: tsukSiteId,
        })
        .reply(200, { body: 'body' })
      const actionCreator = sandboxActions.getContent(
        {},
        undefined,
        undefined,
        undefined,
        undefined,
        { host: 'https://whatever' }
      )

      return actionCreator(
        () => {},
        () => ({
          config: { siteId: tsukSiteId },
          viewport: { media: 'desktop' },
        })
      ).then(() => {
        expect(nockScope.isDone()).toBe(true)
      })
    })

    it('sends forceMobile=true as query parameter when it receives forceMobile argument as "true"', () => {
      const tsukSiteId = 12556
      const nockScope = nock('https://whatever')
        .get('/cmscontent')
        .query({
          viewportMedia: 'desktop',
          siteId: tsukSiteId,
          forceMobile: true,
        })
        .reply(200, { body: 'body' })
      const actionCreator = sandboxActions.getContent(
        {},
        undefined,
        undefined,
        undefined,
        undefined,
        { host: 'https://whatever' },
        true
      )

      return actionCreator(
        () => {},
        () => ({
          config: { siteId: tsukSiteId },
          viewport: { media: 'desktop' },
        })
      ).then(() => {
        expect(nockScope.isDone()).toBe(true)
      })
    })

    it('does not send forceMobile as query parameter when it does not receive forceMobile argument', () => {
      const tsukSiteId = 12556
      const nockScope = nock('https://whatever')
        .get('/cmscontent')
        .query({
          viewportMedia: 'desktop',
          siteId: tsukSiteId,
        })
        .reply(200, { body: 'body' })
      const actionCreator = sandboxActions.getContent(
        {},
        undefined,
        undefined,
        undefined,
        undefined,
        { host: 'https://whatever' }
      )

      return actionCreator(
        () => {},
        () => ({
          config: { siteId: tsukSiteId },
          viewport: { media: 'desktop' },
        })
      ).then(() => {
        expect(nockScope.isDone()).toBe(true)
      })
    })

    it('avoids sending double encoded location.pathname query parameter', () => {
      global.process.browser = true
      const tsukSiteId = 12556
      const nockScope = nock('https://whatever')
        .get('/cmscontent')
        .query({
          location: { pathname: 'abc%2F%26' },
          viewportMedia: 'desktop',
          siteId: 12556,
        })
        .reply(200, { body: 'body' })

      const actionCreator = sandboxActions.getContent(
        {
          pathname: 'abc%2F%26',
        },
        undefined,
        undefined,
        undefined,
        undefined,
        { host: 'https://whatever' }
      )

      return actionCreator(
        () => {},
        () => ({
          config: { siteId: tsukSiteId },
          viewport: { media: 'desktop' },
        })
      ).then(() => {
        expect(nockScope.isDone()).toBe(true)
      })
    })

    it('should use the originalPage as the state key if provided', () => {
      const nockScope = nock('https://whatever')
        .get('/cmscontent')
        .query({
          location: { pathname: 'jason.json' },
          cmsPageName: 'nameWeDontWant',
        })
        .reply(200, { body: 'body' })

      const actionCreator = sandboxActions.getContent(
        { pathname: 'jason.json' },
        'nameWeDontWant',
        null,
        null,
        'whatWeWant',
        { host: 'https://whatever' }
      )

      const dispatch = jest.fn()

      return actionCreator(dispatch, getState).then(() => {
        expect(dispatch).toHaveBeenCalledTimes(1)
        expect(dispatch).toHaveBeenCalledWith({
          type: 'SET_SANDBOX_CONTENT',
          key: 'whatWeWant',
          content: { body: 'body' },
        })
        expect(nockScope.isDone()).toBe(true)
      })
    })
  })

  describe('getSegmentedContent()', () => {
    const getState = () => ({
      config: 'config',
      viewport: {},
    })
    const identifier = 'id1'

    let actionCreator
    beforeAll(() => {
      apiService.get.mockResolvedValue({
        body: {
          [identifier]: {
            EspotContents: {
              cmsMobileContent: {
                responsiveCMSUrl: 'jason.json',
              },
            },
          },
        },
      })
      actionCreator = sandboxActions.getSegmentedContent(
        '/endpoint',
        identifier,
        'page-name'
      )
    })

    afterAll(() => {
      apiService.get.mockClear()
    })

    it('should send a GET request to the given wcsEndpoint', () => {
      const dispatch = jest.fn().mockImplementation(identity)
      expect(apiService.get).not.toHaveBeenCalled()
      return actionCreator(dispatch, getState).then(() => {
        expect(apiService.get).toHaveBeenCalled()
        expect(apiService.get).toHaveBeenCalledWith('/endpoint')
      })
    })

    it('should call getContent with the responsiveCmsUrl from wcs and the given page name', () => {
      const dispatch = jest.fn().mockImplementation(identity)
      const thunkSpy = jest.fn()
      jest.spyOn(sandboxActions, 'getContent').mockReturnValue(thunkSpy)
      return actionCreator(dispatch, getState).then(() => {
        expect(sandboxActions.getContent).toHaveBeenCalled()
        expect(sandboxActions.getContent).toHaveBeenCalledWith(
          { pathname: encodeURIComponent(`/jason.json`) },
          null,
          null,
          null,
          'page-name'
        )
        expect(thunkSpy).toHaveBeenCalledWith(dispatch, getState)
        sandboxActions.getContent.mockClear()
      })
    })
  })

  describe('getHomePageContent()', () => {
    beforeEach(() => {
      const thunkSpy = jest.fn()
      jest
        .spyOn(sandboxActions, 'getSegmentedContent')
        .mockReturnValue(thunkSpy)
      jest.spyOn(sandboxActions, 'getContent').mockReturnValue(thunkSpy)
    })

    afterEach(() => {
      jest.clearAllMocks()
    })

    const dispatch = () => {}
    const getDesktopState = () => ({
      viewport: { media: 'desktop' },
      config: {},
    })
    const getMobileState = () => ({ viewport: { media: 'mobile' }, config: {} })

    describe('if FEATURE_HOME_PAGE_SEGMENTATION is enabled and is NOT mobile device', () => {
      it('should call getSegmentedContent', () => {
        featureSelectors.isFeatureHomePageSegmentationEnabled.mockReturnValue(
          true
        )
        sandboxActions.getHomePageContent()(dispatch, getDesktopState)
        expect(sandboxActions.getSegmentedContent).toHaveBeenCalled()
        expect(sandboxActions.getSegmentedContent).toHaveBeenCalledWith(
          '/home',
          espotsDesktopConstants.home.mainBody,
          'home'
        )
        expect(sandboxActions.getContent).not.toHaveBeenCalled()
      })
    })
    describe('if FEATURE_HOME_PAGE_SEGMENTATION is enabled and is mobile device', () => {
      it('should call getContent', () => {
        featureSelectors.isFeatureHomePageSegmentationEnabled.mockReturnValue(
          true
        )
        sandboxActions.getHomePageContent()(dispatch, getMobileState)
        expect(sandboxActions.getContent).toHaveBeenCalled()
        expect(sandboxActions.getContent).toHaveBeenCalledWith(null, 'home')
        expect(sandboxActions.getSegmentedContent).not.toHaveBeenCalled()
      })
    })
    describe('if FEATURE_HOME_PAGE_SEGMENTATION is disabled and is NOT a mobile device', () => {
      it('should call getContent', () => {
        featureSelectors.isFeatureHomePageSegmentationEnabled.mockReturnValue(
          false
        )
        sandboxActions.getHomePageContent()(dispatch, getDesktopState)
        expect(sandboxActions.getContent).toHaveBeenCalled()
        expect(sandboxActions.getContent).toHaveBeenCalledWith(null, 'home')
        expect(sandboxActions.getSegmentedContent).not.toHaveBeenCalled()
      })
    })
    describe('if FEATURE_HOME_PAGE_SEGMENTATION is disabled and is mobile device', () => {
      it('should call getSegmentedContent', () => {
        featureSelectors.isFeatureHomePageSegmentationEnabled.mockReturnValue(
          false
        )
        sandboxActions.getHomePageContent()(dispatch, getMobileState)
        expect(sandboxActions.getContent).toHaveBeenCalled()
        expect(sandboxActions.getContent).toHaveBeenCalledWith(null, 'home')
        expect(sandboxActions.getSegmentedContent).not.toHaveBeenCalled()
      })
    })
  })

  describe('#resetContent', () => {
    it('returns action RESET_SANDBOX_CONTENT', () => {
      expect(sandboxActions.resetContent()).toEqual({
        type: 'RESET_SANDBOX_CONTENT',
      })
    })
  })
})
