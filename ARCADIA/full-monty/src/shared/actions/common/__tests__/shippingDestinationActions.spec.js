import * as ShippingDestinationActions from '../shippingDestinationActions'
import { getLangHostnames } from '../../../selectors/configSelectors'
import {
  shippingDestinationRedirect,
  getTransferBasketParameters,
} from '../../../lib/change-shipping-destination'

jest.mock('../../../selectors/configSelectors', () => ({
  getLangHostnames: jest.fn(),
}))
jest.mock('../../../lib/change-shipping-destination', () => ({
  shippingDestinationRedirect: jest.fn(),
  getTransferBasketParameters: jest.fn(),
}))

const state = {
  routing: {
    location: {
      hostname: '',
    },
  },
}

describe('Shipping Destination Actions', () => {
  const dispatch = jest.fn()
  const getState = jest.fn().mockReturnValue(state)

  beforeEach(() => {
    jest.clearAllMocks()
    getTransferBasketParameters.mockReturnValueOnce({})
  })

  describe('redirect', () => {
    it('calls `shippingDestinationRedirect`', () => {
      getLangHostnames.mockReturnValue('getLangHostNamesMockValue')
      ShippingDestinationActions.redirect('United Kingdom', 'English')(
        dispatch,
        getState
      )
      expect(shippingDestinationRedirect).toHaveBeenCalledTimes(1)
      expect(shippingDestinationRedirect).toHaveBeenCalledWith(
        'United Kingdom',
        'getLangHostNamesMockValue',
        'English',
        {},
        ''
      )
    })
  })
})
