import * as acc from '../accountActions'
import { MONTY_API } from '../../../middleware/preprocess-redux-api-middleware'

// mocks
import { myCheckoutDetails } from '../../../../../test/mocks/forms/myCheckoutDetailsFormsMocks'
import userMock from '../../../../../test/mocks/myAccount-response.json'
import siteOptionsMock from '../../../../../test/mocks/siteOptions'
import { paymentMethodsList } from '../../../../../test/mocks/paymentMethodsMocks'
import { returnHistoryDetailsMock } from '../../../../../test/mocks/returnHistoryMocks'
import { browserHistory } from 'react-router'
import { put, get } from '../../../lib/api-service'

// constants
import { formNames } from '../../../constants/forms/myCheckoutDetailsConstants'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as logger from '../../../../server/lib/logger'

// mocking helpers
jest.mock('../../../lib/api-service', () => ({
  put: jest.fn(),
  get: jest.fn(),
}))
jest.mock('react-router', () => ({
  browserHistory: {
    push: jest.fn(),
  },
}))

jest.mock('../../../../server/lib/logger', () => ({
  error: jest.fn(),
  info: jest.fn(),
}))

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

const snapshot = (action) => expect(action).toMatchSnapshot()

describe('Account Actions', () => {
  it('userAccount(user)', () => {
    snapshot(acc.userAccount('foo'))
  })

  it('closeCustomerDetailsModal()', () => {
    snapshot(acc.closeCustomerDetailsModal())
  })

  it('getAccount()', () => {
    snapshot(acc.getAccount())
  })

  it('updateAccount(data)', () => {
    snapshot(acc.updateAccount({}))
  })

  it('changeShortProfileRequest(data)', () => {
    snapshot(acc.changeShortProfileRequest({}))
  })

  it('changePwdRequest(data, resetPassword)', () => {
    snapshot(acc.changePwdRequest({}, true))
    snapshot(acc.changePwdRequest({}, false))
    snapshot(acc.changePwdRequest({}))
  })

  it('setForgetPassword(value)', () => {
    snapshot(acc.setForgetPassword('foo'))
  })

  it('toggleForgetPassword()', () => {
    snapshot(acc.toggleForgetPassword())
  })

  it('forgetPwdRequest(data)', () => {
    snapshot(acc.forgetPwdRequest({}))
  })

  it('resetPasswordRequest(data)', () => {
    snapshot(acc.resetPasswordRequest({ foo: 'bar' }))
  })

  it('orderHistoryRequest()', () => {
    snapshot(acc.orderHistoryRequest())
  })

  it('orderHistoryDetailsRequest(orderId)', () => {
    snapshot(acc.orderHistoryDetailsRequest('foo'))
  })

  it('setOrderHistoryDetails(orderDetails)', () => {
    snapshot(acc.orderHistoryDetailsRequest({ orderId: 'foo' }))
  })

  it('returnHistoryRequest()', () => {
    snapshot(acc.returnHistoryRequest())
  })

  it('setReturnHistory returns a setReturnHistory action', () => {
    snapshot(acc.setReturnHistoryDetails(returnHistoryDetailsMock))
  })

  describe('checkAccountExists', () => {
    let successCallback
    let failureCallback
    let store
    let mockData

    beforeEach(async () => {
      successCallback = jest.fn()
      failureCallback = jest.fn()
      store = mockStore()
    })

    describe('onSuccess', () => {
      beforeEach(async () => {
        mockData = {
          body: {
            exists: true,
            email: 'mansnothot@smail.com',
            version: '1.6',
          },
        }

        const getMock = () => {
          const p = Promise.resolve(mockData)
          p.type = 'checkAccountExistsResolvedPromise'
          return p
        }

        get.mockImplementationOnce(getMock)
      })

      it('should do a get request on /account with an email query param', async () => {
        await store.dispatch(
          acc.checkAccountExists({
            email: 'mansnothot@gmail.com',
            successCallback,
            failureCallback,
          })
        )
        expect(get).toHaveBeenCalledWith('/account?email=mansnothot@gmail.com')
      })
      it('on success it should call the successCallback (if it exists) with the returned json object', async () => {
        await store.dispatch(
          acc.checkAccountExists({
            email: 'mansnothot@gmail.com',
            successCallback,
            failureCallback,
          })
        )
        expect(successCallback).toHaveBeenCalledWith(mockData)
      })
      it('should not fail if an email address is not passed to the action', async () => {
        store.dispatch(
          acc.checkAccountExists({ successCallback, failureCallback })
        )
        expect(get).toHaveBeenCalledWith('/account?email=')
      })

      it('should not call the success callback if it is not passed to the action', async () => {
        store.dispatch(
          acc.checkAccountExists({
            email: 'mansnothot@gmail.com',
            failureCallback,
          })
        )
        expect(successCallback).not.toHaveBeenCalled()
      })
    })

    describe('onFailure', () => {
      beforeEach(async () => {
        mockData = {
          // Given if the email param is missing entirely
          errorStatusCode: 422,
          error: 'Unprocessable Entity',
          message: 'Must be logged in to perform this action',
          originalMessage: 'Must be logged in to perform this action',
        }

        const getMock = () => {
          const p = Promise.reject(mockData)
          p.type = 'checkAccountExistsRejectedPromise'
          return p
        }

        get.mockImplementationOnce(getMock)
      })

      it('on failure it should call failureCallback (if it exists) with the returned error object', async () => {
        await store.dispatch(
          acc.checkAccountExists({ successCallback, failureCallback })
        )

        expect(failureCallback).toHaveBeenCalledWith(mockData)
      })

      it('should not fail if a failure callback is not passed to the action', async () => {
        await store.dispatch(acc.checkAccountExists({ successCallback }))

        expect(failureCallback).not.toHaveBeenCalled()
      })

      it('should log an error message on failure', async () => {
        await store.dispatch(
          acc.checkAccountExists({ successCallback, failureCallback })
        )

        expect(logger.error).toHaveBeenCalledWith(mockData)
      })
    })
  })

  describe('returnHistoryDetailsRequest action', () => {
    let store
    let getMock

    beforeEach(() => {
      store = mockStore()
    })

    it('on success', async () => {
      getMock = () => {
        const p = Promise.resolve({
          body: returnHistoryDetailsMock,
        })
        p.type = 'returnHistoryDetailsResolvedPromise'
        return p
      }

      get.mockImplementationOnce(getMock)

      await store.dispatch(acc.returnHistoryDetailsRequest(1, 2))

      expect(get).toHaveBeenCalledWith('/account/return-history/1/2')

      expect(store.getActions()[0]).toEqual({
        type: 'AJAXCOUNTER_INCREMENT',
      })

      expect(store.getActions()[1]).toEqual(getMock())

      expect(store.getActions()[2]).toEqual({
        type: 'SET_RETURN_HISTORY_DETAILS',
        returnDetails: returnHistoryDetailsMock,
      })

      expect(store.getActions()[3]).toEqual({
        type: 'AJAXCOUNTER_DECREMENT',
      })
    })

    it('on failure', async () => {
      getMock = () => {
        const p = Promise.reject(new Error('Request Error')).catch(() => {})
        p.type = 'returnHistoryDetailsRejectedPromise'
        return p
      }

      get.mockImplementation(getMock)

      await store.dispatch(acc.returnHistoryDetailsRequest(1, 2))

      expect(get).toHaveBeenCalledWith('/account/return-history/1/2')

      expect(store.getActions()[0]).toEqual({
        type: 'AJAXCOUNTER_INCREMENT',
      })

      expect(store.getActions()[1]).toEqual(getMock())

      expect(store.getActions()[2]).toEqual({
        type: 'SET_RETURN_HISTORY_DETAILS',
        returnDetails: {},
      })

      expect(store.getActions()[3]).toEqual({
        type: 'AJAXCOUNTER_DECREMENT',
      })
    })
  })

  describe('My Checkout Details Actions', () => {
    beforeEach(() => {
      jest.clearAllMocks()
    })

    describe('resetMyCheckoutDetailsForms action', () => {
      const dispatch = jest.fn()
      const getState = () => {
        return {
          forms: {
            account: {
              myCheckoutDetails,
            },
          },
          siteOptions: siteOptionsMock,
          paymentMethods: paymentMethodsList,
        }
      }

      it('should dispatch RESET_FORM for delivery address', () => {
        const user = userMock
        acc.resetMyCheckoutDetailsForms(user)(dispatch, getState)
        expect(dispatch).toHaveBeenCalledWith({
          type: 'RESET_FORM',
          initialValues: {
            address1: '7 Hannah Close',
            address2: 'Llanishen',
            city: 'CARDIFF',
            state: '',
            country: 'United Kingdom',
            postcode: 'se5 9hr',
            county: null,
          },
          formName: formNames.delivery.address,
          key: null,
        })
      })
      it('should dispatch RESET_FORM for delivery details', () => {
        const user = userMock
        acc.resetMyCheckoutDetailsForms(user)(dispatch, getState)
        expect(dispatch).toHaveBeenCalledWith({
          type: 'RESET_FORM',
          initialValues: {
            title: 'Mrs',
            firstName: 'new first name',
            lastName: 'Williams',
            telephone: '07971134030',
          },
          formName: formNames.delivery.details,
          key: null,
        })
      })
      it('should dispatch RESET_FORM for billing address', () => {
        const user = userMock
        acc.resetMyCheckoutDetailsForms(user)(dispatch, getState)
        expect(dispatch).toHaveBeenCalledWith({
          type: 'RESET_FORM',
          initialValues: {
            address1: '7 Hannah Close',
            address2: 'Llanishen',
            city: 'CARDIFF',
            state: '',
            country: 'United Kingdom',
            postcode: 'hp3 9fs',
            county: null,
          },
          formName: formNames.billing.address,
          key: null,
        })
      })
      it('should dispatch RESET_FORM for billing details', () => {
        const user = userMock
        acc.resetMyCheckoutDetailsForms(user)(dispatch, getState)
        expect(dispatch).toHaveBeenCalledWith({
          type: 'RESET_FORM',
          initialValues: {
            title: 'Mrs',
            firstName: 'new first name',
            lastName: 'Williams',
            telephone: '07971134030',
          },
          formName: formNames.billing.details,
          key: null,
        })
      })
      it('should dispatch setFindAddressMode', () => {
        const user = userMock
        acc.resetMyCheckoutDetailsForms(user)(dispatch, getState)
        expect(dispatch).toHaveBeenCalledWith({
          type: 'SET_ADDRESS_MODE_TO_FIND',
        })
      })
      it('should dispatch RESET_FORM', () => {
        const user = userMock
        acc.resetMyCheckoutDetailsForms(user)(dispatch, getState)
        expect(dispatch).toHaveBeenCalledWith({
          type: 'RESET_FORM',
          initialValues: {
            paymentType: 'PYPAL',
            cardNumber: '',
            expiryMonth: expect.any(String),
            expiryYear: '2017',
            startMonth: '',
            startYear: '',
            cvv: '',
          },
          formName: formNames.payment.paymentCardDetails,
          key: null,
        })
      })
    })

    describe('setMyCheckoutDetailsInitialFocus action', () => {
      it('setMyCheckoutDetailsInitialFocus() to true with initialFocus', () => {
        expect(
          acc.setMyCheckoutDetailsInitialFocus('#initial-focus-selector')
        ).toEqual({
          type: 'SET_MCD_INITIAL_FOCUS',
          initialFocus: '#initial-focus-selector',
        })
      })

      it('setMyCheckoutDetailsInitialFocus() to true without initialFocus', () => {
        expect(acc.setMyCheckoutDetailsInitialFocus(undefined)).toEqual({
          type: 'SET_MCD_INITIAL_FOCUS',
        })
      })
    })

    describe('getMyCheckoutDetailsData action', () => {
      describe('No Account state previously created', () => {
        const dispatch = jest.fn()
        const getState = () => {
          return {
            account: {
              user: {},
            },
          }
        }

        it('call getAccount and resetMyCheckoutDetailsForms when account.user.deliveryAddress does not exists', () => {
          dispatch.mockImplementation(() => {
            return Promise.resolve({ payload: {} })
          })
          return acc
            .getMyCheckoutDetailsData()(dispatch, getState)
            .then(() => {
              expect(dispatch).toHaveBeenCalledTimes(4)
              expect(dispatch.mock.calls[0][0]).toEqual({
                type: 'AJAXCOUNTER_INCREMENT',
              })
              expect(
                Object.getOwnPropertySymbols(dispatch.mock.calls[1][0])[0]
              ).toBe(MONTY_API)
              expect(dispatch.mock.calls[1][0][MONTY_API]).toEqual({
                endpoint: '/account',
                method: 'get',
                overlay: true,
                typesPrefix: 'GET_USER_ACCOUNT',
                meta: {
                  failure: {
                    errorAlertBox: true,
                  },
                },
              })
              expect(dispatch.mock.calls[2][0]).toEqual({
                type: 'AJAXCOUNTER_DECREMENT',
              })
              expect(dispatch.mock.calls[3][0].name).toEqual(
                'resetMyCheckoutDetailsFormsThunk'
              )
            })
        })

        describe('On failure', () => {
          const dispatch = jest.fn((param) => {
            // we are implementing a dispatch mock which sometimes act as Promise, sometimes not.
            // so in order to avoid (node:87324) UnhandledPromiseRejectionWarning: Unhandled promise rejection
            // we are implementing the mock like that
            return Object.getOwnPropertySymbols(param)[0] === MONTY_API
              ? Promise.reject({
                  response: {
                    body: { message: 'We can not connect to getAccount' },
                  },
                })
              : () => {}
          })

          it('should increment and then decrement ajax counter on failure', () => {
            return acc
              .getMyCheckoutDetailsData()(dispatch, getState)
              .then(() => {})
              .catch(() => {
                expect(dispatch).toHaveBeenCalledTimes(4)
                expect(dispatch.mock.calls[0][0]).toEqual({
                  type: 'AJAXCOUNTER_INCREMENT',
                })
                expect(dispatch.mock.calls[2][0]).toEqual({
                  type: 'AJAXCOUNTER_DECREMENT',
                })
              })
          })
          it('should set form message to error on failure', () => {
            return acc
              .getMyCheckoutDetailsData()(dispatch, getState)
              .catch(() => {
                expect(dispatch.mock.calls[3][0]).toEqual({
                  type: 'SET_FORM_MESSAGE',
                  formName: 'yourAddress',
                  key: null,
                  message: {
                    message: 'We can not connect to getAccount',
                    type: 'error',
                  },
                  meta: {
                    localise: 'message.message',
                  },
                })
              })
          })
        })
      })
    })

    describe('setBillingFormsDataFromDeliveryForms action', () => {
      const dispatch = jest.fn()
      const getState = () => {
        return {
          forms: {
            account: {
              myCheckoutDetails,
            },
          },
        }
      }
      it('should do nothing when sourceFormNames and destinationFormNames are empty', () => {
        acc.setBillingFormsDataFromDeliveryForms({}, {})(dispatch, getState)
        expect(dispatch).toHaveBeenCalledTimes(0)
      })
      it('should do nothing when sourceFormNames and destinationFormNames have different keys', () => {
        acc.setBillingFormsDataFromDeliveryForms({ formA: {} }, { formB: {} })(
          dispatch,
          getState
        )
        expect(dispatch).toHaveBeenCalledTimes(0)
      })
      it('should call only common keys which have fields when sourceFormNames and destinationFormNames have different number of keys', () => {
        const source = {
          address: 'deliveryAddressMCD',
        }
        const dest = {
          address: 'billingAddressMCD',
          other: 'otherName',
        }
        acc.setBillingFormsDataFromDeliveryForms(source, dest)(
          dispatch,
          getState
        )
        expect(dispatch).toHaveBeenCalledTimes(1)
      })
      it('should dispatch RESET_FORM with plain key: values', () => {
        const source = {
          address: 'deliveryAddressMCD',
        }
        const dest = {
          address: 'billingAddressMCD',
        }
        acc.setBillingFormsDataFromDeliveryForms(source, dest)(
          dispatch,
          getState
        )
        expect(dispatch).toHaveBeenCalledWith({
          type: 'RESET_FORM',
          formName: 'billingAddressMCD',
          initialValues: {
            address1: '2 Britten Close',
            address2: null,
            city: 'LONDON',
            country: 'United States',
            postcode: 'NW11 7HQ',
            state: 'CO',
          },
          key: null,
        })
        expect(dispatch).toHaveBeenCalledTimes(1)
      })
      it('should dispatch RESET_FORM 3 times when 2 forms provided', () => {
        const source = {
          address: 'deliveryAddressMCD',
          details: 'deliveryDetailsAddressMCD',
          findAddress: 'deliveryFindAddressMCD',
        }
        const dest = {
          address: 'billingAddressMCD',
          details: 'billingDetailsAddressMCD',
          findAddress: 'billingFindAddressMCD',
        }
        acc.setBillingFormsDataFromDeliveryForms(source, dest)(
          dispatch,
          getState
        )
        expect(dispatch).toHaveBeenCalledTimes(3)
      })
    })

    describe('updateMyCheckoutDetails action', () => {
      const dispatch = jest.fn()
      const getState = () => {
        return {
          config: {
            language: '',
            brandName: '',
          },
        }
      }
      const data = {
        billingDetails: {
          nameAndPhone: {
            title: 'Dr',
            firstName: 'Jose',
            lastName: 'Quinto',
            telephone: '01111111222',
          },
          address: {
            address1: '11 Britten Close',
            address2: '11111',
            city: 'LONDON',
            state: '',
            country: 'United Kingdom',
            postcode: 'NW11 7HQ',
          },
        },
        deliveryDetails: {
          nameAndPhone: {
            title: 'Dr',
            firstName: 'Jose Delivery',
            lastName: 'Quinto Delivery',
            telephone: '0980090911',
          },
          address: {
            address1: '6 Britten Close',
            address2: 'Nooo',
            city: 'LONDON',
            state: '',
            country: 'United Kingdom',
            postcode: 'NW11 7HQ',
          },
        },
        creditCard: {
          expiryYear: '2017',
          expiryMonth: '11',
          type: 'ACCNT',
          cardNumber: '1111222233334444',
        },
      }
      const formName = 'myCheckoutDetailsForm'

      describe('On success', () => {
        beforeEach(() => {
          jest.clearAllMocks()
        })

        dispatch.mockImplementation(() => {
          return Promise.resolve({ payload: {} })
        })

        it('should call put with /account/customerdetails', () => {
          acc
            .updateMyCheckoutDetails(data, formName)(dispatch, getState)
            .then(() => {
              expect(put).toHaveBeenCalledWith('/account/customerdetails', data)
            })
        })

        it('should set form message confirming the success', () => {
          acc
            .updateMyCheckoutDetails(data, formName)(dispatch, getState)
            .then(() => {
              expect(dispatch).toHaveBeenCalledWith({
                type: 'SET_FORM_MESSAGE',
                formName,
                key: null,
                message: {
                  message: 'Your changes have been saved',
                  type: 'confirm',
                },
                meta: {
                  localise: 'message.message',
                },
              })
            })
        })

        it('should increment and decrement ajax counter', () => {
          acc
            .updateMyCheckoutDetails(data, formName)(dispatch, getState)
            .then(() => {
              expect(dispatch).toHaveBeenCalledWith({
                type: 'AJAXCOUNTER_INCREMENT',
              })
              expect(dispatch).toHaveBeenCalledWith({
                type: 'AJAXCOUNTER_DECREMENT',
              })
            })
        })

        it('should call browserHistory.push', () => {
          expect(browserHistory.push).toHaveBeenCalledTimes(0)
          acc
            .updateMyCheckoutDetails(data, formName)(dispatch, getState)
            .then(() => {
              expect(browserHistory.push).toHaveBeenCalledTimes(1)
              expect(browserHistory.push).toHaveBeenCalledWith(
                '/my-account/details'
              )
            })
        })
      })

      describe('On failure', () => {
        // we are implementing a dispatch mock which sometimes act as Promise, sometimes not.
        // so in order to avoid (node:87324) UnhandledPromiseRejectionWarning: Unhandled promise rejection
        // we are implementing the mock like that
        const dispatch = jest.fn((param) => {
          return param === undefined
            ? Promise.reject({
                response: { body: { message: 'We can not save' } },
              })
            : () => {}
        })

        it('should increment and then decrement ajax counter on failure', () => {
          acc
            .updateMyCheckoutDetails(data, formName)(dispatch, getState)
            .then(() => {
              expect(dispatch).toHaveBeenCalledWith({
                type: 'AJAXCOUNTER_INCREMENT',
              })
              expect(dispatch).toHaveBeenCalledWith({
                type: 'AJAXCOUNTER_DECREMENT',
              })
            })
        })
        it('should set form message to error on failure', () => {
          acc
            .updateMyCheckoutDetails(data, formName)(dispatch, getState)
            .then(() => {
              expect(dispatch).toHaveBeenCalledWith({
                type: 'SET_FORM_MESSAGE',
                formName,
                key: null,
                message: {
                  message: 'We can not save',
                  type: 'error',
                },
                meta: {
                  localise: 'message.message',
                },
              })
            })
        })
      })
    })
  })
})
