import {
  getPaymentMethods,
  getAllPaymentMethods,
  removePaymentMethod,
} from '../paymentMethodsActions'
import { mockedReq } from '../../../../../test/mocks/paymentMethodsMocks'
import { MONTY_API } from '../../../middleware/preprocess-redux-api-middleware'

const snapshot = (action) => expect(action).toMatchSnapshot()

describe('Payment Method Actions', () => {
  describe('getPaymentMethods creates api GET request action', () => {
    it('creates with delivery and billing params', () => {
      snapshot(getPaymentMethods(mockedReq))
    })
    it('creates with delivery, billing and amount params', () => {
      snapshot(getPaymentMethods({ ...mockedReq, amount: 100 }))
    })
    it('defaults to getting all payment methods if the query string is empty due to undefined params', () => {
      expect(getPaymentMethods({})).toEqual({
        [MONTY_API]: {
          endpoint: `/payments?value=all`,
          method: 'GET',
          overlay: true,
          typesPrefix: 'GET_PAYMENT_METHODS',
        },
      })
    })
    it('defaults to getting all payment methods if the query string is empty due to empty string params', () => {
      expect(
        getPaymentMethods({ delivery: '', billing: '', amount: '', value: '' })
      ).toEqual({
        [MONTY_API]: {
          endpoint: `/payments?value=all`,
          method: 'GET',
          overlay: true,
          typesPrefix: 'GET_PAYMENT_METHODS',
        },
      })
    })
  })

  describe('removePaymentMethod creates api DELETE request action', () => {
    it('creates with request body', () => {
      snapshot(removePaymentMethod('FOO'))
    })
  })

  describe('getAllPaymentMethods', () => {
    it('returns a method that calls dispatch with getPaymentMethods', () => {
      const dispatch = jest.fn()
      const thunk = getAllPaymentMethods()

      thunk(dispatch)

      expect(dispatch).toHaveBeenCalledWith({
        [MONTY_API]: {
          endpoint: `/payments?value=all`,
          method: 'GET',
          overlay: true,
          typesPrefix: 'GET_PAYMENT_METHODS',
        },
      })
    })
  })
})
