import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { MONTY_API } from '../../../middleware/preprocess-redux-api-middleware'

import {
  createAddress,
  deleteAddress,
  selectAddress,
  showNewAddressForm,
  hideNewAddressForm,
} from '../addressBookActions'
import { resetForm } from '../formActions'
import { getOrderSummary } from '../checkoutActions'

jest.mock('../formActions', () => ({
  resetForm: jest.fn(() => ({
    type: 'RESET_FORM',
  })),
}))
jest.mock('../checkoutActions', () => ({
  getOrderSummary: jest.fn(),
}))

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

beforeEach(() => {
  jest.clearAllMocks()
})

describe('addressBookActions', () => {
  describe('hideNewAddressForm', () => {
    it('should return action to hide address book form', () => {
      const action = {
        type: 'ADDRESS_BOOK_HIDE_NEW_ADDRESS_FORM',
      }
      expect(hideNewAddressForm()).toEqual(action)
    })
  })

  describe('showNewAddressForm', () => {
    const state = {
      account: {
        user: {
          deliveryDetails: {
            address: {
              country: 'United Kingdom',
            },
          },
        },
      },
    }
    const store = mockStore(state)

    it('should dispatch action to reset relevant address forms', () => {
      store.dispatch(showNewAddressForm())
      expect(resetForm).toHaveBeenCalledTimes(3)
      expect(resetForm).toHaveBeenCalledWith('newAddress', {
        address1: '',
        address2: '',
        postcode: '',
        city: '',
        country: 'United Kingdom',
        state: '',
        county: null,
        isManual: false,
      })
      expect(resetForm).toHaveBeenCalledWith('newDetails', {
        title: '',
        firstName: '',
        lastName: '',
        telephone: '',
      })
      expect(resetForm).toHaveBeenCalledWith('newFindAddress', {
        houseNumber: '',
        message: '',
        findAddress: '',
        selectAddress: '',
        postcode: '',
      })
    })

    it('should dispatch action to show new address form', () => {
      const expectedAction = [
        {
          type: 'ADDRESS_BOOK_SHOW_NEW_ADDRESS_FORM',
        },
      ]
      store.dispatch(showNewAddressForm())
      expect(store.getActions()).toEqual(expect.arrayContaining(expectedAction))
    })
  })

  describe('createAddress', () => {
    const address = {
      address1: '101 Acme Road',
      address2: '',
      city: 'London',
    }
    const dispatchMock = jest.fn(() => Promise.resolve({}))

    it('should dispatch action to create a delivery address', async () => {
      const action = {
        [MONTY_API]: {
          endpoint: '/checkout/order_summary/delivery_address',
          method: 'POST',
          body: address,
          overlay: true,
          typesPrefix: 'ADDRESS_BOOK_CREATE_ADDRESS',
        },
      }
      await createAddress(address)(dispatchMock)
      expect(dispatchMock).toHaveBeenCalledWith(action)
    })

    it('should dispatch actions to hide address form and get order summary on request success', async () => {
      dispatchMock.mockReturnValueOnce(
        Promise.resolve({
          type: 'ADDRESS_BOOK_CREATE_ADDRESS_API_SUCCESS',
        })
      )
      await createAddress(address)(dispatchMock)
      expect(dispatchMock).toHaveBeenCalledWith({
        type: 'ADDRESS_BOOK_HIDE_NEW_ADDRESS_FORM',
      })
      expect(getOrderSummary).toHaveBeenCalledTimes(1)
    })

    it('should not dispatch actions to hide address form or get order summary on request failure', async () => {
      dispatchMock.mockReturnValueOnce(
        Promise.resolve({
          type: 'ADDRESS_BOOK_CREATE_ADDRESS_API_FAILURE',
        })
      )

      await createAddress(address)(dispatchMock)
      expect(dispatchMock).not.toHaveBeenCalledWith({
        type: 'ADDRESS_BOOK_HIDE_NEW_ADDRESS_FORM',
      })
      expect(getOrderSummary).not.toHaveBeenCalled()
    })
  })

  describe('deleteAddress', () => {
    it('should return action to delete delivery address with address.id', () => {
      const id = 123456
      const action = {
        [MONTY_API]: {
          endpoint: '/checkout/order_summary/delivery_address',
          method: 'DELETE',
          body: { addressId: id },
          overlay: true,
          typesPrefix: 'ADDRESS_BOOK_DELETE_ADDRESS',
        },
      }
      expect(deleteAddress({ id })).toEqual(action)
    })

    it('should return action to delete delivery address with address.addressId', () => {
      const addressId = 123456
      const action = {
        [MONTY_API]: {
          endpoint: '/checkout/order_summary/delivery_address',
          method: 'DELETE',
          body: { addressId },
          overlay: true,
          typesPrefix: 'ADDRESS_BOOK_DELETE_ADDRESS',
        },
      }
      expect(deleteAddress({ addressId })).toEqual(action)
    })
  })

  describe('selectAddress', () => {
    const addressId = 123456
    const dispatchMock = jest.fn(() => Promise.resolve({}))

    it('should dispatch action to select a delivery address', async () => {
      const action = {
        [MONTY_API]: {
          endpoint: '/checkout/order_summary/delivery_address',
          method: 'PUT',
          body: { addressId },
          overlay: true,
          typesPrefix: 'ADDRESS_BOOK_SELECT_ADDRESS',
        },
      }
      await selectAddress({ addressId })(dispatchMock)
      expect(dispatchMock).toHaveBeenCalledWith(action)
    })

    it('should dispatch action to get order summary on request success', async () => {
      dispatchMock.mockReturnValueOnce(
        Promise.resolve({
          type: 'ADDRESS_BOOK_SELECT_ADDRESS_API_SUCCESS',
        })
      )
      await selectAddress({ addressId })(dispatchMock)
      expect(getOrderSummary).toHaveBeenCalledTimes(1)
    })

    it('should not dispatch action to get order summary on request failure', async () => {
      dispatchMock.mockReturnValueOnce(
        Promise.resolve({
          type: 'ADDRESS_BOOK_SELECT_ADDRESS_API_FAILURE',
        })
      )
      await selectAddress({ addressId })(dispatchMock)
      expect(getOrderSummary).not.toHaveBeenCalled()
    })
  })
})
