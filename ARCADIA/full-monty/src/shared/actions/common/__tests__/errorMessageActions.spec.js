import * as actions from '../errorMessageActions'

const dispatch = jest.fn()

const getState = (state) => {
  return () => state
}

describe('Error Message Actions', () => {
  describe('removeError', () => {
    it('returns appropriate object', () => {
      expect(actions.removeError()).toEqual({
        type: 'SET_ERROR',
        error: null,
      })
    })
  })

  describe('setCmsError', () => {
    it('default error', () => {
      expect(actions.setCmsError()).toEqual({
        error: {
          statusCode: 404,
        },
        type: 'SET_ERROR',
      })
    })
  })

  describe('setGenericError', () => {
    const inititalState = {
      config: {
        language: 'en-gb',
        brandName: 'topshop',
      },
    }

    beforeEach(() => {
      jest.clearAllMocks()
    })

    afterEach(() => {
      dispatch.mockClear()
    })

    it('ignores session expiry error header', () => {
      const error = {
        response: {
          headers: {
            'session-expired': true,
          },
        },
      }
      const action = actions.setGenericError(error)
      action(dispatch, getState(inititalState))
      expect(dispatch.mock.calls.length).toBe(0)
    })

    it('ignores session expiry error message', () => {
      const error = {
        message: 'Must be logged in to perform this action',
      }
      const action = actions.setGenericError(error)
      action(dispatch, getState(inititalState))
      expect(dispatch.mock.calls.length).toBe(0)
    })

    it('handles default error', () => {
      const error1 = {
        message: 'AAAAAAHHHH',
      }
      const action = actions.setGenericError(error1)
      action(dispatch, getState(inititalState))
      expect(dispatch.mock.calls.length).toBe(1)
      const { type, error } = dispatch.mock.calls[0][0]
      expect(type).toEqual('SET_ERROR')
      expect(error).toEqual({
        isOverlay: true,
        message: 'AAAAAAHHHH',
        nativeError: {
          message: 'AAAAAAHHHH',
        },
        noReload: true,
      })
    })
  })

  describe('setApiError', () => {
    const inititalState = {
      config: {
        language: 'en-gb',
        brandName: 'topshop',
      },
    }

    beforeEach(() => {
      jest.clearAllMocks()
    })

    afterEach(() => {
      dispatch.mockClear()
    })

    it('ignores session expiry error header', () => {
      const error = {
        response: {
          headers: {
            'session-expired': true,
          },
        },
      }
      const action = actions.setApiError(error)
      action(dispatch, getState(inititalState))
      expect(dispatch.mock.calls.length).toBe(0)
    })

    it('ignores session expiry error message', () => {
      const error = {
        message: 'Must be logged in to perform this action',
      }
      const action = actions.setApiError(error)
      action(dispatch, getState(inititalState))
      expect(dispatch.mock.calls.length).toBe(0)
    })

    it('handles response.body', () => {
      const error1 = {
        response: {
          body: {
            message: {
              field: 'AAAAAAHHHHAAAAA',
            },
          },
        },
      }
      const action = actions.setApiError(error1)
      action(dispatch, getState(inititalState))
      expect(dispatch.mock.calls.length).toBe(1)
      const { type, error } = dispatch.mock.calls[0][0]
      expect(type).toEqual('SET_ERROR')
      expect(error).toEqual({
        isOverlay: true,
        message: {
          field: 'AAAAAAHHHHAAAAA',
        },
      })
    })

    it('handles default error', () => {
      const error1 = {
        message: 'AAAAAAHHHH',
      }
      const action = actions.setApiError(error1)
      action(dispatch, getState(inititalState))
      expect(dispatch.mock.calls.length).toBe(1)
      const { type, error } = dispatch.mock.calls[0][0]
      expect(type).toEqual('SET_ERROR')
      expect(error).toEqual({
        isOverlay: true,
        message: 'There was a problem, please try again later.',
        nativeError: {
          message: 'AAAAAAHHHH',
        },
      })
    })
  })
})
