import { setJsessionid } from '../jsessionidActions'

describe('#setJsessionid', () => {
  it('returns the expected object', () => {
    expect(setJsessionid('jsessionid')).toEqual({
      type: 'SET_JSESSION_ID',
      jsessionid: 'jsessionid',
    })
  })
})
