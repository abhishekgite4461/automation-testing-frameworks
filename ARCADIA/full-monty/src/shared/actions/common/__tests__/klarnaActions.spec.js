import {
  authorizeV2,
  checkAndAuthoriseKlarna,
  updateAndAuthorizeV2,
  createSession,
  setKlarnaSessionId,
  setKlarnaClientToken,
  errorContent,
  setLoadFormPending,
  loadForm,
  setKlarnaFormVisibility,
  setKlarnaAuthorizationToken,
  setAuthorizePending,
  setReAuthorizePending,
  setOrderSummaryHash,
  setIsUpdatingKlarnaEmail,
  setKlarnaEmail,
  resetKlarna,
  setKlarnaFormSubmitted,
  authorize,
  directKlarnaUpdateSession,
  handleDisapproval,
} from '../klarnaActions'
import { renderToStaticMarkup } from 'react-dom/server'

jest.mock('../../common/modalActions', () => ({
  showModal: jest.fn(),
}))

jest.mock('../../../lib/checkout-utilities/klarna-utils', () => ({
  authorizeRequest: jest.fn(),
  assembleSessionPayload: jest.fn(),
  reAuthorizeRequest: jest.fn(),
  loadFormRequest: jest.fn(),
}))

jest.mock('../../../lib/localisation', () => ({
  localise: jest.fn(),
}))

jest.mock('../../../lib/api-service', () => ({
  post: jest.fn(),
  put: jest.fn(),
}))

jest.mock('../../../selectors/userSelectors', () => ({
  isKlarnaDefaultPaymentType: jest.fn(),
}))

jest.mock('react-router', () => ({
  browserHistory: {
    push: jest.fn(),
  },
}))

import { showModal } from '../../common/modalActions'
import {
  authorizeRequest,
  assembleSessionPayload,
} from '../../../lib/checkout-utilities/klarna-utils'
import { ajaxCounter } from '../../components/LoaderOverlayActions'

describe('Klarna Actions', () => {
  const dispatch = jest.fn()
  const getState = jest.fn()
  const { post } = require('../../../lib/api-service')
  const { localise } = require('../../../lib/localisation')

  beforeEach(() => {
    jest.resetModules()
    jest.resetAllMocks()
    getState.mockReturnValue({
      klarna: {},
      config: {},
    })
  })

  describe('actionCreators', () => {
    const param = 'mock-param'
    const actionCreators = [
      [
        setKlarnaSessionId,
        {
          type: 'SET_KLARNA_SESSION_ID',
          sessionId: param,
        },
      ],
      [
        setKlarnaClientToken,
        {
          type: 'SET_KLARNA_CLIENT_TOKEN',
          clientToken: param,
        },
      ],
      [
        setKlarnaAuthorizationToken,
        {
          type: 'SET_KLARNA_AUTHORIZATION_TOKEN',
          authorizationToken: param,
        },
      ],
      [
        setKlarnaFormVisibility,
        {
          type: 'SET_KLARNA_FORM_VISIBILITY',
          shown: param,
        },
      ],
      [
        setLoadFormPending,
        {
          type: 'SET_KLARNA_LOADFORM_PENDING',
          loadFormPending: param,
        },
      ],
      [
        setAuthorizePending,
        {
          type: 'SET_KLARNA_AUTHORIZE_PENDING',
          authorizePending: param,
        },
      ],
      [
        setReAuthorizePending,
        {
          type: 'SET_KLARNA_REAUTHORIZE_PENDING',
          reAuthorizePending: param,
        },
      ],
      [
        setOrderSummaryHash,
        {
          type: 'SET_ORDER_SUMMARY_HASH',
          orderSummaryHash: param,
        },
      ],
      [
        setIsUpdatingKlarnaEmail,
        {
          type: 'SET_KLARNA_UPDATING_EMAIL',
          isUpdatingEmail: param,
        },
      ],
      [
        setKlarnaEmail,
        {
          type: 'SET_KLARNA_EMAIL',
          email: param,
        },
      ],
      [
        resetKlarna,
        {
          type: 'RESET_KLARNA',
        },
      ],
      [
        setKlarnaFormSubmitted,
        {
          type: 'SET_KLARNA_FORM_SUBMITTED',
          formSubmitted: param,
        },
      ],
    ]

    actionCreators.forEach(([actionCreator, action]) => {
      it(`should create ${action.type}`, () => {
        expect(actionCreator(param)).toEqual(action)
      })
    })
  })

  describe('createSession', () => {
    const clientToken = 'mock clientToken'
    const data = 'mock data'
    const sessionId = 'mock sessionId'
    const translation = 'mock translation'
    const state = {
      klarna: {},
      config: { clientToken },
    }
    const promiseReturnedByDispatchOnPost = Promise.resolve({
      body: {
        sessionId,
        clientToken,
      },
    })

    it('update=false', () => {
      const update = false
      const createSessionThunk = createSession(data, update)

      getState.mockReturnValue(state)
      dispatch.mockReturnValueOnce()
      dispatch.mockReturnValueOnce(promiseReturnedByDispatchOnPost)
      localise.mockReturnValue(translation)
      global.document.querySelector = jest.fn()

      createSessionThunk(dispatch, getState)

      expect(dispatch).toHaveBeenCalledTimes(2)
      expect(dispatch.mock.calls[0][0]).toEqual(ajaxCounter('increment'))

      return promiseReturnedByDispatchOnPost.then(() => {
        expect(dispatch).toHaveBeenCalledTimes(6)
        expect(post.mock.calls[0][0]).toEqual('/klarna-session')
        expect(post.mock.calls[0][1]).toEqual(data)
        expect(dispatch.mock.calls[2][0]).toEqual(ajaxCounter('decrement'))
        expect(dispatch.mock.calls[3][0]).toEqual(setKlarnaSessionId(sessionId))
        expect(dispatch.mock.calls[4][0]).toEqual(
          setKlarnaClientToken(clientToken)
        )
        expect(typeof dispatch.mock.calls[5][0]).toBe('function')
        expect(document.querySelector.mock.calls[0][0]).toEqual(
          '.KlarnaForm-container'
        )
      })
    })

    it('update=true', () => {
      const update = true
      const createSessionThunk = createSession(data, update)

      getState.mockReturnValue(state)
      dispatch.mockReturnValueOnce()
      dispatch.mockReturnValueOnce(promiseReturnedByDispatchOnPost)
      localise.mockReturnValue(translation)

      createSessionThunk(dispatch, getState)

      expect(dispatch).toHaveBeenCalledTimes(2)
      expect(dispatch.mock.calls[0][0]).toEqual(ajaxCounter('increment'))

      return promiseReturnedByDispatchOnPost.then(() => {
        expect(dispatch).toHaveBeenCalledTimes(5)
        expect(post.mock.calls[0][0]).toEqual('/klarna-session')
        expect(post.mock.calls[0][1]).toEqual(data)
        expect(dispatch.mock.calls[2][0]).toEqual(ajaxCounter('decrement'))
        expect(dispatch.mock.calls[3][0]).toEqual(setKlarnaSessionId(sessionId))
        expect(dispatch.mock.calls[4][0]).toEqual(
          setKlarnaClientToken(clientToken)
        )
      })
    })

    it('error occured', () => {
      const update = false
      const createSessionThunk = createSession(data, update)

      getState.mockReturnValue(state)
      dispatch.mockReturnValueOnce()
      dispatch.mockReturnValueOnce(Promise.reject())
      localise.mockReturnValue(translation)

      const promiseReturnedByDispatchOnPost = createSessionThunk(
        dispatch,
        getState
      )

      expect(dispatch).toHaveBeenCalledTimes(2)
      expect(dispatch.mock.calls[0][0]).toEqual(ajaxCounter('increment'))

      return promiseReturnedByDispatchOnPost.then(() => {
        expect(dispatch).toHaveBeenCalledTimes(4)
        expect(post.mock.calls[0][0]).toEqual('/klarna-session')
        expect(post.mock.calls[0][1]).toEqual(data)
        expect(dispatch.mock.calls[2][0]).toEqual(ajaxCounter('decrement'))
        expect(showModal).toHaveBeenCalledTimes(1)
        expect(renderToStaticMarkup(showModal.mock.calls[0][0])).toBe(
          renderToStaticMarkup(
            errorContent(translation, () => translation, () => {})
          )
        )
        expect(showModal.mock.calls[0][1]).toEqual({
          mode: 'warning',
          type: 'alertdialog',
        })
      })
    })
  })

  describe('loadForm', () => {
    const {
      loadFormRequest,
    } = require('../../../lib/checkout-utilities/klarna-utils')
    const {
      isKlarnaDefaultPaymentType,
    } = require('../../../selectors/userSelectors')
    const clientToken = 'mock clientToken'
    const container = 'mock container'
    const updateDetails = 'mock updateDetails'
    const translation = 'mock translation'

    const testLoadForm = ({
      loadFormPending = false,
      loadFormRequestReturnValue = Promise.resolve(),
      isKlarnaDefaultPaymentTypeReturnValue = true,
    }) => {
      localise.mockReturnValue(translation)
      loadFormRequest.mockReturnValue(loadFormRequestReturnValue)
      isKlarnaDefaultPaymentType.mockReturnValue(
        isKlarnaDefaultPaymentTypeReturnValue
      )

      const loadFormThunk = loadForm(clientToken, container, updateDetails)
      const promiseReturnedByLoadFormThunk = loadFormThunk(dispatch, getState)

      if (loadFormPending) {
        expect(promiseReturnedByLoadFormThunk).toEqual(undefined)
        expect(dispatch).toHaveBeenCalledTimes(0)
        return
      }

      expect(dispatch).toHaveBeenCalledTimes(2)
      expect(dispatch.mock.calls[0][0]).toEqual(ajaxCounter('increment'))
      expect(dispatch.mock.calls[1][0]).toEqual(setLoadFormPending(true))
      expect(loadFormRequest.mock.calls[0][0]).toEqual(clientToken)
      expect(loadFormRequest.mock.calls[0][1]).toEqual(container)
      expect(loadFormRequest.mock.calls[0][2]).toEqual(updateDetails)

      let isRejected = false

      return loadFormRequestReturnValue
        .catch(() => {
          isRejected = true

          return promiseReturnedByLoadFormThunk
        })
        .then(() => {
          const calledTimes =
            !isRejected && isKlarnaDefaultPaymentTypeReturnValue ? 6 : 5

          expect(dispatch).toHaveBeenCalledTimes(calledTimes)
          expect(dispatch.mock.calls[2][0]).toEqual(ajaxCounter('decrement'))
          expect(dispatch.mock.calls[3][0]).toEqual(setLoadFormPending(false))

          if (isRejected) {
            expect(renderToStaticMarkup(showModal.mock.calls[0][0])).toEqual(
              renderToStaticMarkup(
                errorContent(translation, () => translation, () => {})
              )
            )
            expect(showModal.mock.calls[0][1]).toEqual({
              mode: 'warning',
              type: 'alertdialog',
            })
          } else {
            expect(dispatch.mock.calls[4][0]).toEqual(
              setKlarnaFormVisibility(true)
            )
            if (isKlarnaDefaultPaymentTypeReturnValue) {
              expect(dispatch.mock.calls[5][0].name).toEqual(
                'checkAndAuthoriseKlarnaThunk'
              )
            }
          }
        })
    }

    it('success - isKlarnaDefaultPaymentType returns true', () => {
      return testLoadForm({})
    })

    it('success - isKlarnaDefaultPaymentType returns false', () => {
      return testLoadForm({
        isKlarnaDefaultPaymentTypeReturnValue: false,
      })
    })

    it('exits when state.klarna.loadFormPending is true', () => {
      const loadFormPending = true

      getState.mockReturnValue({
        klarna: {
          loadFormPending,
        },
      })

      return testLoadForm({
        loadFormPending,
      })
    })

    it('failure - loadFormRequest rejected', () => {
      return testLoadForm({
        loadFormRequestReturnValue: Promise.reject(),
      })
    })
  })

  describe('handleDisapproval', () => {
    const {
      browserHistory: { push },
    } = require('react-router')
    const { localise } = require('../../../lib/localisation')
    const translation = 'mock translation'
    const handleDisapprovalThunk = handleDisapproval()

    localise.mockReturnValue(translation)

    it('should dispatch showModal', () => {
      const state = {
        config: {},
        routing: {
          location: {
            pathname: 'checkout/payment',
            search: 'mock search',
          },
        },
      }

      getState.mockReturnValue(state)
      showModal.mockReturnValue('mock modal')

      handleDisapprovalThunk(dispatch, () => state)

      expect(dispatch).toHaveBeenCalledTimes(1)
      expect(showModal).toHaveBeenCalledTimes(1)
      expect(renderToStaticMarkup(showModal.mock.calls[0][0])).toBe(
        '<div><p></p><button class="Button" type="button" role="button"></button></div>'
      )
      expect(dispatch.mock.calls[0][0]).toBe('mock modal')
      expect(dispatch.mock.calls[0][1]).toEqual({ type: 'alertdialog' })
    })

    it('should call browserHistory.push', () => {
      const search = 'mock search'
      const state = {
        config: {},
        routing: {
          location: {
            pathname: 'mock pathname',
            search,
          },
        },
      }

      getState.mockReturnValue(state)
      showModal.mockReturnValue('mock modal')

      handleDisapprovalThunk(dispatch, () => state)

      expect(push).toHaveBeenCalledTimes(1)
      expect(push.mock.calls[0][0]).toEqual(
        `/checkout/payment${search}#CardDetails`
      )
    })
  })

  describe('directKlarnaUpdateSession', () => {
    const { put } = require('../../../lib/api-service')
    const data = 'mock data'
    const directKlarnaUpdateSessionThunk = directKlarnaUpdateSession(data)

    it('should dispatch a chain of actions when succeed', () => {
      put.mockReturnValue('mock put')
      dispatch.mockReturnValue(Promise.resolve())

      return directKlarnaUpdateSessionThunk(dispatch, getState).then(() => {
        expect(put).toHaveBeenCalledTimes(1)
        expect(put.mock.calls[0][0]).toEqual('/klarna-session')
        expect(put.mock.calls[0][1]).toEqual(data)
        expect(dispatch).toHaveBeenCalledTimes(3)
        expect(dispatch.mock.calls[0][0]).toEqual(ajaxCounter('increment'))
        expect(dispatch.mock.calls[1][0]).toEqual('mock put')
        expect(dispatch.mock.calls[2][0]).toEqual(ajaxCounter('decrement'))
      })
    })

    it('should dispatch handleDisapproval when failed', () => {
      dispatch.mockReturnValue(Promise.reject())

      return directKlarnaUpdateSessionThunk(dispatch, getState).then(() => {
        expect(dispatch).toHaveBeenCalledTimes(4)
        expect(dispatch.mock.calls[0][0]).toEqual(ajaxCounter('increment'))
        expect(dispatch.mock.calls[2][0]).toEqual(ajaxCounter('decrement'))
        expect(dispatch.mock.calls[3][0].name).toBe('handleDisapprovalThunk')
      })
    })
  })

  describe('authorize', () => {
    ;[authorize, authorizeV2].forEach((actionCreator) => {
      describe(actionCreator.name, () => {
        const {
          browserHistory: { push },
        } = require('react-router')
        const updateDetails = {}
        const action = actionCreator(updateDetails)
        const authorizationToken = '036dddd8-a6c4-55cb-960d-27628da53401'

        it('should return if authorizePending', () => {
          getState.mockReturnValue({
            klarna: {
              authorizePending: true,
            },
          })
          action(dispatch, getState)
          expect(dispatch).not.toHaveBeenCalled()
        })

        it('should call dispath with setAuthorizePending true', () => {
          authorizeRequest.mockImplementation(() => {
            return Promise.resolve({
              authorization_token: authorizationToken,
            })
          })
          return action(dispatch, getState).then(() => {
            expect(dispatch.mock.calls[0][0]).toEqual({
              type: 'SET_KLARNA_AUTHORIZE_PENDING',
              authorizePending: true,
            })
          })
        })

        it('should call dispath with setAuthorizePending false on successful api call', () => {
          authorizeRequest.mockImplementation(() => {
            return Promise.resolve({
              authorization_token: authorizationToken,
            })
          })
          return action(dispatch, getState).then(() => {
            expect(dispatch.mock.calls[1][0]).toEqual({
              type: 'SET_KLARNA_AUTHORIZE_PENDING',
              authorizePending: false,
            })
          })
        })

        it('should call dispath with setKlarnaAuthorizationToken when we get auth token', () => {
          authorizeRequest.mockImplementation(() => {
            return Promise.resolve({
              authorization_token: authorizationToken,
            })
          })
          return action(dispatch, getState).then(() => {
            expect(dispatch.mock.calls[2][0]).toEqual({
              authorizationToken,
              type: 'SET_KLARNA_AUTHORIZATION_TOKEN',
            })
          })
        })

        it('should call throw if no auth token', (done) => {
          authorizeRequest.mockImplementation(() => {
            return Promise.resolve({ no_authorization_token: true })
          })
          dispatch.mockImplementationOnce(() => ({}))
          dispatch.mockImplementationOnce(() => ({}))
          dispatch.mockImplementationOnce(() => ({}))
          dispatch.mockImplementationOnce(() => {
            expect(dispatch.mock.calls[0][0]).toEqual({
              authorizePending: true,
              type: 'SET_KLARNA_AUTHORIZE_PENDING',
            })
            expect(dispatch.mock.calls[1][0]).toEqual({
              type: 'SET_KLARNA_AUTHORIZE_PENDING',
              authorizePending: false,
            })
            expect(dispatch.mock.calls[2][0]).toEqual(undefined)
            expect(dispatch.mock.calls[3][0]).toEqual({
              authorizePending: false,
              type: 'SET_KLARNA_AUTHORIZE_PENDING',
            })
            expect(dispatch).toHaveBeenCalledTimes(4)
            done()
          })
          action(dispatch, getState)
        })

        it('should call dispath with setAuthorizePending false on unsuccessful api call', () => {
          authorizeRequest.mockImplementation(() => {
            return Promise.reject()
          })
          return action(dispatch, getState).then(() => {
            expect(dispatch.mock.calls[2][0]).toEqual({
              type: 'SET_KLARNA_AUTHORIZE_PENDING',
              authorizePending: false,
            })
          })
        })

        it('should call handleDisapproval if error and approved is false', () => {
          authorizeRequest.mockImplementation(() => {
            return Promise.reject({
              approved: false,
            })
          })
          return action(dispatch, getState).then(() => {
            expect(dispatch.mock.calls[1][0].name).toBe(
              'handleDisapprovalThunk'
            )
          })
        })

        it('should call dispatch with handleDisapproval if error and approved is true and show_form is false', () => {
          authorizeRequest.mockImplementation(() => {
            return Promise.reject({
              approved: true,
              show_form: false,
            })
          })
          return action(dispatch, getState).then(() => {
            expect(dispatch.mock.calls[1][0].name).toBe(
              'handleDisapprovalThunk'
            )
          })
        })

        it('should call dispatch with', () => {
          authorizeRequest.mockImplementation(() => {
            return Promise.reject()
          })
          return action(dispatch, getState).then(() => {
            expect(showModal).toHaveBeenCalledTimes(1)
          })
        })

        if (actionCreator.name === 'authorize') {
          it('should call browserHistory.push when we get auth token', () => {
            authorizeRequest.mockImplementation(() => {
              return Promise.resolve({
                authorization_token: authorizationToken,
              })
            })
            return action(dispatch, getState).then(() => {
              expect(push).toHaveBeenCalledTimes(1)
              expect(push.mock.calls[0][0]).toEqual('/checkout/summary') // TODO concat location.search
            })
          })
        }
      })
    })
  })

  describe('updateAndAuthorizeV2', () => {
    const action = updateAndAuthorizeV2()

    it('should call dispatch with createSession', () => {
      dispatch.mockImplementation(() => {
        return Promise.resolve({ body: {} })
      })
      assembleSessionPayload.mockImplementation(() => {
        return {}
      })
      return action(dispatch, getState).then(() => {
        expect(dispatch.mock.calls[0][0].name).toBe('createSessionThunk')
      })
    })

    it('should call dispatch with directKlarnaUpdateSession after createSession success', () => {
      dispatch.mockImplementation(() => {
        return Promise.resolve({ body: {} })
      })
      assembleSessionPayload.mockImplementation(() => {
        return {}
      })
      return action(dispatch, getState).then(() => {
        expect(dispatch.mock.calls[1][0].name).toBe(
          'directKlarnaUpdateSessionThunk'
        )
      })
    })

    it('should call dispatch with authorizeV2 after createSession success', () => {
      dispatch.mockImplementation(() => {
        return Promise.resolve({ body: {} })
      })
      assembleSessionPayload.mockImplementation(() => {
        return {}
      })
      return action(dispatch, getState).then(() => {
        expect(dispatch.mock.calls[2][0].name).toBe('authorizeThunk')
      })
    })

    it('should call dispatch with setOrderSummaryHash after createSession success', () => {
      dispatch.mockImplementation(() => {
        return Promise.resolve({ body: {} })
      })
      assembleSessionPayload.mockImplementation(() => {
        return {}
      })
      return action(dispatch, getState).then(() => {
        expect(dispatch.mock.calls[3][0]).toEqual({
          orderSummaryHash: undefined,
          type: 'SET_ORDER_SUMMARY_HASH',
        })
      })
    })

    it('should call dispatch with handleDisapproval after createSession failure', () => {
      dispatch.mockImplementation(() => {
        return Promise.reject()
      })
      return action(dispatch, getState).then(() => {
        expect(dispatch.mock.calls[1][0].name).toBe('handleDisapprovalThunk')
      })
    })
  })

  describe(checkAndAuthoriseKlarna.name, () => {
    const checkoutState = {
      orderSummary: 'MOCKED',
    }

    describe('dispatch updateAndAuthorizeV2 when the klarna session is outdated', () => {
      beforeEach(() => {
        jest.doMock('../../../selectors/klarnaSelectors', () => ({
          isKlarnaSessionOutdated: () => true,
          getAddressPayload: () => {},
          calculateOrderSummaryHash: () => 'HASH',
        }))
      })

      it('when store delivery is selected', () => {
        jest.doMock('../../../selectors/checkoutSelectors', () => ({
          getShoppingBagOrderId: () => 'id',
          isStoreDelivery: () => true,
        }))
        const klarnaActions = require('../klarnaActions')
        const getState = () => ({ checkout: checkoutState })
        klarnaActions.checkAndAuthoriseKlarna()(dispatch, getState)
        expect(dispatch.mock.calls[0][0].name).toBe('updateAndAuthorizeV2Thunk')
      })
      it('when store delivery is not selected', () => {
        jest.doMock('../../../selectors/checkoutSelectors', () => ({
          getShoppingBagOrderId: () => 'id',
          isStoreDelivery: () => false,
        }))
        const klarnaActions = require('../klarnaActions')
        const getState = () => ({ checkout: checkoutState })
        klarnaActions.checkAndAuthoriseKlarna()(dispatch, getState)
        expect(dispatch.mock.calls[0][0].name).toBe('updateAndAuthorizeV2Thunk')
      })
    })

    it('dispatch authorizeV2 when the klarna session is not outdated', () => {
      jest.doMock('../../../selectors/klarnaSelectors', () => ({
        isKlarnaSessionOutdated: () => false,
        getAddressPayload: () => {},
        calculateOrderSummaryHash: () => 'HASH',
      }))
      jest.doMock('../../../selectors/checkoutSelectors', () => ({
        getShoppingBagOrderId: () => 'id',
        isStoreDelivery: () => true,
      }))

      const klarnaActions = require('../klarnaActions')
      const getState = () => ({ checkout: checkoutState })
      klarnaActions.checkAndAuthoriseKlarna()(dispatch, getState)
      expect(dispatch.mock.calls[0][0].name).toBe('authorizeThunk')
    })
  })
})
