import { setIsMobileHostname } from '../mobileHostnameActions'

describe('#setIsMobileHostname', () => {
  it('returns the expected object', () => {
    expect(setIsMobileHostname(true)).toEqual({
      type: 'SET_IS_MOBILE_HOSTNAME',
      isMobile: true,
    })
    expect(setIsMobileHostname(false)).toEqual({
      type: 'SET_IS_MOBILE_HOSTNAME',
      isMobile: false,
    })
  })
})
