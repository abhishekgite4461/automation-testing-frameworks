import * as actions from '../sessionActions'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { logoutRequest } from '../authActions'
import * as modalActions from '../modalActions'

jest.mock('../authActions', () => ({
  logoutRequest: jest.fn(() => ({ type: 'LOGOUT_REQUEST_MOCK' })),
}))

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

beforeEach(() => {
  jest.clearAllMocks()
})

describe('sessionActions', () => {
  it('dispatches a logoutRequest with redirect to `/login` page', async () => {
    const logoutRequestMock = () => {
      const p = new Promise((res) => res())
      p.type = 'LOGOUT_REQUEST_MOCK'
      return p
    }
    logoutRequest.mockImplementationOnce(logoutRequestMock)
    const mockedCloseModalAction = { type: 'MOCKED_CLOSE_MODAL' }
    jest
      .spyOn(modalActions, 'closeModal')
      .mockReturnValue(mockedCloseModalAction)

    const store = mockStore()

    const expectedActions = [
      { type: 'AJAXCOUNTER_INCREMENT' },
      { type: 'SESSION_EXPIRED' },
      { type: 'CLOSE_MINI_BAG' },
      mockedCloseModalAction,
      logoutRequestMock(),
      { type: 'AJAXCOUNTER_DECREMENT' },
      { type: 'RESET_SESSION_EXPIRED' },
    ]

    await store.dispatch(actions.sessionExpired())
    expect(store.getActions()).toEqual(expectedActions)
    expect(logoutRequest).toHaveBeenCalledWith('/login')
  })

  it('should create RESET_SESSION_EXPIRED action', () => {
    const expectedAction = {
      type: 'RESET_SESSION_EXPIRED',
    }

    expect(actions.sessionReset()).toEqual(expectedAction)
  })
})
