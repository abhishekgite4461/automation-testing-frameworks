import { browserHistory } from 'react-router'
import thunk from 'redux-thunk'

import * as api from '../../../lib/api-service'
import configureMockStore from 'redux-mock-store'
import { orderMock } from '../../../../../test/mocks/orderCreate'

// actions
import { getAccount } from '../accountActions'
import { createVisaForm } from '../../../lib/checkout-utilities/create-visa-form'
import { setFormMessage } from '../../../actions/common/formActions'
import { updateShoppingBagBadgeCount } from '../../../actions/common/shoppingBagActions'
import { updateMenuForAuthenticatedUser } from '../../../actions/common/navigationActions'
import * as orderActions from '../orderActions'
import { sendAnalyticsErrorMessage, ANALYTICS_ERROR } from '../../../analytics'
import * as espotActions from '../espot-actions/espot-actions-by-page'

jest.mock('../espot-actions/espot-actions-by-page', () => ({
  setThankyouPageEspots: jest.fn(() => ({
    type: 'SET_ESPOT_CONTENT',
  })),
}))

jest.mock('../../../actions/common/formActions', () => ({
  setFormMessage: jest.fn(),
  resetForm: jest.fn((formName) => ({
    type: 'RESET_FORM_MOCK',
    formName,
  })),
}))

jest.mock('../accountActions', () => ({
  getAccount: jest.fn(() => Promise.resolve({})),
}))

jest.mock('../../../actions/common/shoppingBagActions', () => ({
  updateShoppingBagBadgeCount: jest.fn(),
}))

jest.mock('../../../lib/checkout-utilities/create-visa-form', () => ({
  createVisaForm: jest.fn(),
}))

jest.mock('../../../lib/checkout-utilities/order-summary', () => ({
  fixEuropeanOrderCompleted: jest.fn(() => ({})),
}))

jest.mock('../../../lib/localisation', () => ({
  localise: jest.fn(() => {
    return "There's been a temporary issue. Please confirm your order again."
  }),
}))

jest.mock('../../../actions/common/navigationActions', () => ({
  updateMenuForAuthenticatedUser: jest.fn(),
}))

jest.spyOn(browserHistory, 'push').mockImplementation(() => null)

jest.spyOn(espotActions, 'setThankyouPageEspots').mockImplementation(() => ({
  type: 'SET_ESPOT_CONTENT',
}))

updateMenuForAuthenticatedUser.mockReturnValue({
  type: 'UPDATE_MENU_MOCK',
})

updateShoppingBagBadgeCount.mockReturnValue({
  type: 'UPDATE_BAG_MOCK',
})

setFormMessage.mockReturnValue({
  type: 'SET_FORM_MESSAGE_MOCK',
  formName: 'order',
})

createVisaForm.mockReturnValue({
  type: 'CREATE_VISA_FORM_MOCK',
  date: "<form method='post' action='https://secure-test.worldpay.com/",
})

const initialState = {
  config: {
    brandCode: 'ts',
  },
  account: {
    user: {},
  },
  siteOptions: {
    billCountries: {},
  },
  checkout: {
    verifyPayment: {},
    savePaymentDetails: false,
  },
  forms: {
    checkout: {
      billingCardDetails: {
        fields: {},
      },
    },
    giftCard: {
      fields: {},
    },
  },
  klarna: {
    orderSummaryHash: '123',
    authorizationToken: 'valid_token',
  },
}

describe('Order Actions', () => {
  const middlewares = [thunk]
  const mockStore = configureMockStore(middlewares)
  const postActions = {
    bad: {},
    good: {},
  }
  const createDispatchMock = (body) => {
    return jest.fn((event) => {
      const response = { body }
      if (event === postActions.good) {
        return Promise.resolve(response)
      } else if (event === postActions.bad) {
        return Promise.reject({ response })
      }
      return Promise.resolve()
    })
  }

  const createGetStateMock = () => {
    return jest.fn(() => ({
      config: {},
      account: {
        user: {},
      },
      siteOptions: {},
      checkout: {
        verifyPayment: {},
      },
    }))
  }

  const postSpy = jest.spyOn(api, 'post')
  const putSpy = jest.spyOn(api, 'put')

  beforeEach(() => {
    jest.clearAllMocks()
  })

  describe('createOrder', () => {
    afterEach(() => {
      postSpy.mockClear()
    })

    afterAll(() => {
      postSpy.mockRestore()
    })

    const postOrderMockSuccess = () => {
      const p = new Promise((resolve) =>
        resolve({
          body: {
            completedOrder: {
              deliveryAddress: '',
            },
          },
        })
      )
      p.type = 'POST_ORDER_MOCK_SUCCESS'
      return p
    }

    const postOrderMockSuccessVbv = () => {
      const p = new Promise((resolve) =>
        resolve({
          body: {
            vbvForm: {},
          },
        })
      )
      p.type = 'POST_ORDER_MOCK_SUCCESS'
      return p
    }

    const getAccountMock = () => {
      const p = new Promise((resolve) => resolve())
      p.type = 'GET_ACCOUNT_MOCK'
      return p
    }

    const postOrderMockFailed = () => {
      const p = new Promise((reject) => reject({}))
      p.type = 'POST_ORDER_MOCK_FAILED'
      return p
    }

    it('Checks the correct actions have been dispatched on a successful order', async () => {
      getAccount.mockImplementation(getAccountMock)
      postSpy.mockImplementation(postOrderMockSuccess)

      const store = mockStore(initialState)
      const expectedActions = [
        {
          type: 'AJAXCOUNTER_INCREMENT',
        },
        {
          type: 'SET_FORM_MESSAGE_MOCK',
          formName: 'order',
        },
        {
          error: false,
          type: 'SET_ORDER_ERROR',
        },
        {
          data: false,
          type: 'SET_ORDER_PENDING',
        },
        postOrderMockSuccess(),
        {
          type: 'EMPTY_SHOPPING_BAG',
        },
        {
          type: 'AJAXCOUNTER_DECREMENT',
        },
        getAccountMock(),
        {
          data: {},
          type: 'SET_ORDER_COMPLETED',
        },
        {
          formName: 'billingCardDetails',
          type: 'RESET_FORM_MOCK',
        },
        {
          formName: 'giftCard',
          type: 'RESET_FORM_MOCK',
        },
        {
          type: 'UPDATE_MENU_MOCK',
        },
        {
          type: 'UPDATE_BAG_MOCK',
        },
        {
          type: 'SET_ESPOT_CONTENT',
        },
      ]

      await store.dispatch(orderActions.createOrder(orderMock))
      expect(store.getActions()).toEqual(expectedActions)
    })
    it('Checks the correct actions have been dispatched on a failed order', async () => {
      postSpy.mockImplementation(postOrderMockFailed)

      const store = mockStore(initialState)
      const expectedActions = [
        {
          type: 'AJAXCOUNTER_INCREMENT',
        },
        {
          type: 'SET_FORM_MESSAGE_MOCK',
          formName: 'order',
        },
        {
          error: false,
          type: 'SET_ORDER_ERROR',
        },
        {
          data: false,
          type: 'SET_ORDER_PENDING',
        },
        postOrderMockFailed(),
        {
          type: 'EMPTY_SHOPPING_BAG',
        },
        {
          type: 'AJAXCOUNTER_INCREMENT',
        },
        {
          type: 'AJAXCOUNTER_DECREMENT',
        },
        {
          type: 'SET_FORM_MESSAGE_MOCK',
          formName: 'order',
        },
        {
          errorMessage: 'Error paying order',
          type: 'MONTY/ANALYTICS.SEND_ERROR_MESSAGE',
        },
      ]
      await store.dispatch(orderActions.createOrder(orderMock))
      expect(store.getActions()).toEqual(expectedActions)
    })
    it('should call setThankyouPageEspots action', async () => {
      postSpy.mockImplementation(postOrderMockSuccess)
      getAccount.mockImplementation(getAccountMock)

      const store = mockStore(initialState)

      expect(espotActions.setThankyouPageEspots).not.toHaveBeenCalled()
      await store.dispatch(orderActions.createOrder(orderMock))
      expect(espotActions.setThankyouPageEspots).toHaveBeenCalledTimes(1)
      expect(espotActions.setThankyouPageEspots).toHaveBeenCalledWith({
        deliveryAddress: '',
      })
    })
    it('should set correct form message if timeout posting order', async () => {
      postSpy.mockImplementation(() => postActions.bad)
      const dispatchMock = createDispatchMock({
        statusCode: 504,
      })
      const getStateMock = createGetStateMock()
      setFormMessage.mockReturnValue({
        type: 'SET_FORM_MESSAGE_MOCK',
        formName: 'order',
        message: {
          type: 'error',
          message:
            "There's been a temporary issue. Please confirm your order again.",
        },
      })

      await orderActions.createOrder({})(dispatchMock, getStateMock)
      expect(dispatchMock).toHaveBeenCalledWith({
        type: 'SET_FORM_MESSAGE_MOCK',
        formName: 'order',
        message: {
          type: 'error',
          message:
            "There's been a temporary issue. Please confirm your order again.",
        },
      })
    })
    it('should dispatch getAccount and dispatch setOrderPending if vbvForm is returned', async () => {
      postSpy.mockImplementation(postOrderMockSuccessVbv)
      getAccount.mockImplementation(getAccountMock)

      const store = mockStore(initialState)
      const expectedAction = [
        getAccountMock(),
        {
          type: 'SET_ORDER_PENDING',
          data: {
            date:
              "<form method='post' action='https://secure-test.worldpay.com/",
            type: 'CREATE_VISA_FORM_MOCK',
          },
        },
      ]
      await store.dispatch(orderActions.createOrder(orderMock))
      expect(store.getActions()).toEqual(expect.arrayContaining(expectedAction))
    })
    it('should return error object if posting order fails', () => {
      postSpy.mockImplementation(() => postActions.bad)
      const dispatchMock = createDispatchMock({
        validationErrors: [
          {
            message: 'Please insert a valid card number',
          },
        ],
      })

      const getStateMock = createGetStateMock()

      return orderActions
        .createOrder({})(dispatchMock, getStateMock)
        .then((error) => {
          expect(error.message).toBe('Please insert a valid card number')
        })
    })
    it('should push an error to the GTM DataLayer if posting order fails', async () => {
      postSpy.mockImplementation(() => postActions.bad)
      const store = mockStore({})
      const expectedActions = [
        {
          type: 'MONTY/ANALYTICS.SEND_ERROR_MESSAGE',
          errorMessage: 'Error paying order',
        },
      ]
      await store.dispatch(
        sendAnalyticsErrorMessage(ANALYTICS_ERROR.CONFIRM_AND_PAY)
      )
      expect(store.getActions()).toEqual(
        expect.arrayContaining(expectedActions)
      )
    })
  })

  describe('updateOrder', () => {
    it('should call setThankyouPageEspots action', () => {
      putSpy.mockImplementation(() => postActions.good)
      const resBody = { completedOrder: { deliveryAddress: '' } }
      const dispatchMock = createDispatchMock(resBody)
      const getStateMock = createGetStateMock()

      expect(espotActions.setThankyouPageEspots).toHaveBeenCalledTimes(0)
      return orderActions
        .updateOrder({})(dispatchMock, getStateMock)
        .then(() => {
          expect(espotActions.setThankyouPageEspots).toHaveBeenCalledWith(
            resBody
          )
        })
    })
  })
  describe('submitOrder', () => {
    const createGetStateMock = (paymentType = 'CARD') => {
      return jest.fn(() => ({
        account: {},
        checkout: { savePaymentDetails: false },
        forms: {
          checkout: {
            billingCardDetails: {
              fields: {
                paymentType: {
                  value: paymentType,
                },
              },
            },
          },
        },
        klarna: {
          orderSummaryHash: '123',
          authorizationToken: 'valid_token',
        },
      }))
    }

    it('should dispatch  correct `createOrder` action if not `KLARNA` payment type', () => {
      const dispatchMock = jest.fn()
      const getStateMock = createGetStateMock()
      const createOrderMock = jest.fn()
      const deps = {
        generateOrder: () => ({
          order: '123',
        }),
        createOrder: createOrderMock,
      }
      orderActions.submitOrder(deps)(dispatchMock, getStateMock)

      expect(createOrderMock).toHaveBeenCalledWith({ order: '123' })
    })

    it('should dispatch correct `createOrder` action if `KLARNA` payment type', () => {
      const dispatchMock = jest.fn()
      const getStateMock = createGetStateMock('KLRNA')
      const createOrderMock = jest.fn()
      const deps = {
        generateOrder: () => ({
          order: '123',
        }),
        hashOrderSummary: () => '123',
        createOrder: createOrderMock,
      }
      orderActions.submitOrder(deps)(dispatchMock, getStateMock)
      expect(createOrderMock).toHaveBeenCalledWith({
        order: '123',
        authToken: 'valid_token',
      })
    })

    it('should dispatch correct `createOrder` action if `KLARNA` payment type and `orderHash` is in local storage', () => {
      const dispatchMock = jest.fn()
      const getStateMock = createGetStateMock('KLRNA')
      const savedWindow = global.window
      global.window = {
        localStorage: {
          getItem: jest.fn(() => '123'),
        },
      }
      const createOrderMock = jest.fn()
      const deps = {
        generateOrder: () => ({
          order: '123',
        }),
        hashOrderSummary: () => '123',
        createOrder: createOrderMock,
      }
      orderActions.submitOrder(deps)(dispatchMock, getStateMock)
      expect(createOrderMock).toHaveBeenCalledWith({
        order: '123',
        authToken: 'valid_token',
      })
      global.window = savedWindow
    })

    it('should re-authorise Klarna if hash has changed', () => {
      const dispatchMock = jest.fn()
      const getStateMock = createGetStateMock('KLRNA')
      const fullPayload = {}
      const assembleFullPayloadMock = jest.fn(() => fullPayload)
      const reAuthorizeMock = jest.fn()
      const order = {
        order: '123',
      }
      const createOrderMock = jest.fn()
      const deps = {
        generateOrder: () => order,
        hashOrderSummary: () => '1234',
        assembleFullPayload: assembleFullPayloadMock,
        reAuthorize: reAuthorizeMock,
        createOrder: createOrderMock,
      }
      orderActions.submitOrder(deps)(dispatchMock, getStateMock)
      expect(reAuthorizeMock).toHaveBeenCalledWith(fullPayload, order, true)
    })
  })
})
