import * as actions from '../infinityScrollActions'
import { mockStoreCreator } from '../../../../../test/unit/helpers/get-redux-mock-store'

jest.mock('react-router', () => ({
  browserHistory: {
    replace: jest.fn(),
  },
}))
import { browserHistory } from 'react-router'

jest.mock('../productsActions.js', () => ({
  addToProducts: jest.fn(),
}))
import { addToProducts } from '../productsActions'

describe('Infinity Scroll Actions', () => {
  beforeEach(() => {
    jest.resetAllMocks()
  })
  it('setInfinityActive()', () => {
    expect(actions.setInfinityActive()).toEqual({
      type: 'SET_INFINITY_ACTIVE',
    })
  })
  it('setInfinityInactive()', () => {
    expect(actions.setInfinityInactive()).toEqual({
      type: 'SET_INFINITY_INACTIVE',
    })
  })
  it('nextPageInfinity()', () => {
    expect(actions.nextPageInfinity()).toEqual({
      type: 'NEXT_PAGE_INFINITY',
    })
  })
  it('setInfinityPage(page)', () => {
    expect(actions.setInfinityPage(13)).toEqual({
      type: 'SET_INFINITY_PAGE',
      page: 13,
    })
  })
  it('preserveScroll(preservedScroll)', () => {
    expect(actions.preserveScroll(1300)).toEqual({
      type: 'PRESERVE_SCROLL',
      preservedScroll: 1300,
    })
  })
  describe('clearInfinityPage', () => {
    const initialState = {
      infinityScroll: {
        currentPage: 3,
        isActive: true,
      },
      routing: {
        location: {
          pathname: 'some/pathname',
          query: {
            currentPage: 2,
            q: 'search',
          },
        },
      },
    }
    const onPage1Props = {
      infinityScroll: {
        isActive: true,
      },
      routing: {
        location: {
          pathname: 'some/pathname',
          query: {
            q: 'search',
          },
        },
      },
    }
    it('should call `setInfinityPage` and `browserHistory.replace`', () => {
      const store = mockStoreCreator(initialState)
      store.dispatch(actions.clearInfinityPage())
      const actionsCalled = store.getActions()
      expect(actionsCalled[0]).toEqual({ page: 1, type: 'SET_INFINITY_PAGE' })
      expect(browserHistory.replace).toHaveBeenCalledTimes(1)
      expect(browserHistory.replace).toHaveBeenCalledWith({
        pathname: 'some/pathname',
        query: {
          q: 'search',
        },
      })
    })
    it('should not call if currentPage doesnt exist', () => {
      const store = mockStoreCreator(onPage1Props)
      store.dispatch(actions.clearInfinityPage())
      const actionsCalled = store.getActions()
      expect(actionsCalled[0]).toEqual({ page: 1, type: 'SET_INFINITY_PAGE' })
      expect(browserHistory.replace).toHaveBeenCalledTimes(0)
    })
  })
  describe('hitWaypoint', () => {
    it('should call `addToProducts` if `isActive`', () => {
      const initialState = {
        infinityScroll: {
          isActive: true,
        },
      }
      const store = mockStoreCreator(initialState)
      store.dispatch(actions.hitWaypoint())
      const actionsCalled = store.getActions()
      expect(actionsCalled[0]).toEqual({ type: 'SET_INFINITY_INACTIVE' })
      expect(addToProducts).toHaveBeenCalledTimes(1)
    })
    it('should not call `addToProducts` if not `isActive`', () => {
      const initialState = {
        infinityScroll: {
          isActive: false,
        },
      }
      const store = mockStoreCreator(initialState)
      store.dispatch(actions.hitWaypoint())
      const actionsCalled = store.getActions()
      expect(actionsCalled[0]).toEqual({ type: 'SET_INFINITY_INACTIVE' })
      expect(addToProducts).not.toHaveBeenCalled()
    })
  })
})
