// TODO : bring back toast error message for logout
import { path } from 'ramda'
import { browserHistory } from 'react-router'
import { post, del } from '../../lib/api-service'
import { setFormMessage } from '../../actions/common/formActions'
import { setApiError } from '../common/errorMessageActions'
import { errorReport } from '../../../client/lib/reporter'
import { clearCacheData, isStorageSupported } from '../../../client/lib/storage'
import { userAccount } from './accountActions'
import { ajaxCounter } from '../components/LoaderOverlayActions'
import { sendAnalyticsErrorMessage, ANALYTICS_ERROR } from '../../analytics'
import {
  updateShoppingBagBadgeCount,
  checkForMergedItemsInBag,
  getBag,
  synchroniseBag,
} from './shoppingBagActions'
import {
  updateMenuForAuthenticatedUser,
  updateMenuForUnauthenticatedUser,
} from './navigationActions'
import { authEventAnalytics } from '../../lib/analytics/auth'
import { localise } from '../../lib/localisation'
import {
  resetKlarna,
  setOrderSummaryHash,
} from '../../actions/common/klarnaActions'
import { getPaymentMethods } from './paymentMethodsActions'
import { getDefaultWishlist, clearWishlist } from './wishlistActions'

// Selectors
import { getUserAddressCountry } from '../../selectors/userSelectors'
import { isFeatureWishlistEnabled } from '../../selectors/featureSelectors'

/* Global Auth pending State */
export function authPending(loading) {
  return {
    type: 'AUTH_PENDING',
    loading,
  }
}

export function authLogin(bvToken) {
  return (dispatch, getState) => {
    dispatch({
      type: 'LOGIN',
      bvToken,
      loginLocation: getState().routing.location,
    })
  }
}

export function setAuthentication(authentication) {
  return {
    type: 'SET_AUTHENTICATION',
    authentication,
  }
}

export function setInitialWishlist(successCallback) {
  return (dispatch) => {
    dispatch(ajaxCounter('increment'))
    return dispatch(getDefaultWishlist()).then(() => {
      if (successCallback)
        return successCallback().then(dispatch(ajaxCounter('decrement')))
      dispatch(ajaxCounter('decrement'))
    })
  }
}

export function loginRequest({
  credentials,
  getNextRoute,
  formName = 'login',
  successCallback,
  errorCallback,
}) {
  return (dispatch, getState) => {
    dispatch(authPending(true))
    dispatch(ajaxCounter('increment'))
    dispatch(setFormMessage(formName, {}))
    const preLoginState = getState()
    dispatch(post('/account/login', credentials)).then(
      (res) => {
        // get bag cool should only be performed on initial login and not on a checkout login
        // the successCallback is only passed down on the checkout login or from the wishlist login modal
        if (
          (formName === 'login' && !successCallback) ||
          formName === 'wishlistLoginModal'
        )
          dispatch(getBag(true, true))

        dispatch(updateShoppingBagBadgeCount(res.body.basketItemCount))
        if (res.body.isPwdReset) {
          dispatch(authPending(false))
          dispatch(ajaxCounter('decrement'))
          dispatch(userAccount(res.body))
          dispatch(setAuthentication(true))
          dispatch(updateMenuForAuthenticatedUser())
          dispatch(synchroniseBag(preLoginState))

          browserHistory.push('/reset-password')

          // successCallback is used on checkout to fetch the ordersummary after a successful login
          if (successCallback) successCallback()
          return
        }
        dispatch(authLogin(res.headers.bvtoken))

        dispatch(authPending(false))
        dispatch(ajaxCounter('decrement'))
        dispatch(userAccount(res.body))
        dispatch(setAuthentication(true))
        dispatch(updateMenuForAuthenticatedUser())

        dispatch(
          getPaymentMethods({
            delivery: getUserAddressCountry(res.body, 'deliveryDetails'),
            billing: getUserAddressCountry(res.body, 'billingDetails'),
            amount: 0,
          })
        )

        dispatch(authEventAnalytics(credentials, ['event11']))
        dispatch(checkForMergedItemsInBag(preLoginState))
        dispatch(synchroniseBag(preLoginState))

        if (isFeatureWishlistEnabled(getState()))
          return dispatch(setInitialWishlist(successCallback))

        if (successCallback) successCallback()

        const nextRoute = getNextRoute && getNextRoute(res.body)
        browserHistory.push(nextRoute || '/my-account')
      },
      (err) => {
        if (err.response && err.response.body && err.response.body.message) {
          dispatch(
            setFormMessage(formName, {
              type: 'error',
              message: err.response.body.message,
            })
          )
        }
        dispatch(authPending(false))
        dispatch(authEventAnalytics(credentials, ['event57']))
        dispatch(ajaxCounter('decrement'))
        dispatch(sendAnalyticsErrorMessage(ANALYTICS_ERROR.LOGIN_FAILED))

        if (errorCallback) errorCallback(err)
      }
    )
  }
}

export function logout() {
  return (dispatch) => {
    dispatch({
      type: 'LOGOUT',
    })
    dispatch(updateMenuForUnauthenticatedUser())
  }
}

export function logoutRequest(redirectLocation = '/') {
  return (dispatch, getState) => {
    dispatch(authPending(true))
    return dispatch(del('/account/logout'))
      .then(() => {
        if (isStorageSupported()) clearCacheData()
        dispatch(logout())
        dispatch(authPending(false))
        if (isFeatureWishlistEnabled(getState())) dispatch(clearWishlist())
        dispatch(resetKlarna())
        dispatch(setOrderSummaryHash())
        dispatch(updateShoppingBagBadgeCount(0))

        browserHistory.replace({ pathname: redirectLocation })
      })
      .catch((err) => {
        if (err.statusCode && err.statusCode >= 500) {
          dispatch(setApiError(err))
        } else {
          dispatch(logout())
          browserHistory.replace({ pathname: redirectLocation })
          const { language, brandName } = getState().config
          const l = localise.bind(null, language, brandName)
          const resp = err.response
          const error = {
            message:
              (resp && resp.body && l(resp.body.message)) ||
              l`logout worked but the api sent back errors`,
          }
          errorReport('authActions', error)
        }
      })
  }
}

/* User Registration Actions */
export function registerSuccess(user) {
  return {
    type: 'REGISTER_SUCCESS',
    user,
  }
}

export function registerError(error) {
  return {
    type: 'REGISTER_ERROR',
    error,
  }
}

export function registerRequest({
  formData,
  getNextRoute,
  formName = 'register',
  successCallback,
  errorCallback,
}) {
  return (dispatch, getState) => {
    dispatch(authPending(true))
    dispatch(ajaxCounter('increment'))
    dispatch(setFormMessage('register', {}))
    dispatch(post('/account/register', formData)).then(
      (resp) => {
        // Hook for the Qubit tag "iProspect Registration Confirmation"
        document.dispatchEvent(new Event('registrationSuccessful'))
        const events = ['event9']
        if (formData.subscribe) events.push('event4')

        if (formName === 'wishlistLoginModal') dispatch(getBag())

        dispatch(authLogin())
        dispatch(userAccount(resp.body))
        dispatch(setAuthentication(true))
        dispatch(authPending(false))
        dispatch(ajaxCounter('decrement'))
        dispatch(updateMenuForAuthenticatedUser())
        dispatch(authEventAnalytics({ username: formData.email }, events))
        const nextRoute = getNextRoute && getNextRoute()
        browserHistory.push(nextRoute || '/my-account')

        if (isFeatureWishlistEnabled(getState()))
          return dispatch(setInitialWishlist(successCallback))

        if (successCallback) successCallback()
      },
      (err) => {
        const handleFail = () => {
          dispatch(
            setFormMessage(formName, {
              type: 'error',
              message: path(['response', 'body', 'message'], err),
            })
          )
          dispatch(authPending(false))
          dispatch(authEventAnalytics(formData, ['event8']))
          dispatch(ajaxCounter('decrement'))
          if (errorCallback) errorCallback(err)
        }
        dispatch(sendAnalyticsErrorMessage(ANALYTICS_ERROR.REGISTRATION_FAILED))
        if (
          path(['response', 'body', 'originalMessage'], err) ===
          'Unable to create account while user logged in, please logout'
        ) {
          return dispatch(del('/account/logout'))
            .catch(() => handleFail())
            .then(() =>
              dispatch(registerRequest({ formData, getNextRoute, formName }))
            )
        }
        handleFail()
      }
    )
  }
}
