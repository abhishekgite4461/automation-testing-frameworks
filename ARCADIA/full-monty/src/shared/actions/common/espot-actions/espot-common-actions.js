// TODO: move this and espot-actions-by-page to Espot to module

import { path, pathOr } from 'ramda'
import { fixCmsUrl } from '../../../lib/cms-utilities'
import { setEspotContent } from './espot-content'

export const SET_ESPOT_DATA = 'SET_ESPOT_DATA'
export const REMOVE_PLP_ESPOTS = 'REMOVE_PLP_ESPOTS'

export function setEspotData(responsiveEspotData) {
  return (dispatch) => {
    responsiveEspotData.forEach(
      ({ identifier, responsiveCMSUrl, position, isPlpEspot = false }) => {
        dispatch({
          type: SET_ESPOT_DATA,
          payload: {
            identifier,
            responsiveCMSUrl,
            position,
            isPlpEspot,
          },
        })
      }
    )
  }
}

export function setEspot(
  apiResponse,
  espotGroupId,
  apiResponsePath = ['EspotContents', 'cmsMobileContent', 'responsiveCMSUrl']
) {
  return (dispatch, getState) => {
    const identifiers = pathOr(
      [],
      ['espot', 'identifiers', espotGroupId],
      getState()
    )

    const espotData = identifiers
      .map((identifier) => {
        const responsiveCMSUrl = path(
          [identifier, ...apiResponsePath],
          apiResponse
        )
        return (
          responsiveCMSUrl && {
            identifier,
            responsiveCMSUrl: fixCmsUrl(responsiveCMSUrl),
          }
        )
      })
      .filter(Boolean)

    setEspotData(espotData)(dispatch)
    return setEspotContent(espotData)(dispatch, getState)
  }
}

export function removePlpEspots() {
  return {
    type: REMOVE_PLP_ESPOTS,
  }
}
