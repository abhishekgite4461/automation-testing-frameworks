import cmsConsts from '../../../constants/cmsConsts'
import { getContent } from '../sandBoxActions'

const { ESPOT_CONTENT_TYPE } = cmsConsts

export function setEspotContent(espotData) {
  return (dispatch, getState) =>
    Promise.all(
      espotData.map(({ responsiveCMSUrl }) => {
        if (!responsiveCMSUrl) {
          return null
        }
        // pathname and query are both required to support client side and server sider rendering (See sandbox)
        const location = {
          pathname: responsiveCMSUrl,
          query: { responsiveCMSUrl },
        }
        return getContent(location, null, ESPOT_CONTENT_TYPE)(
          dispatch,
          getState
        )
      })
    )
}
