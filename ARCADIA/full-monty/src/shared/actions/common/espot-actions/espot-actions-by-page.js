import { path, pathOr } from 'ramda'
import { espotGroupId } from '../../../constants/espotsDesktop'
import { fixCmsUrl } from '../../../lib/cms-utilities'

import { setEspot, setEspotData, removePlpEspots } from './espot-common-actions'

import { setEspotContent } from './espot-content'

export function setThankyouPageEspots(apiResponse) {
  return (dispatch, getState) => {
    return setEspot(apiResponse.espots, espotGroupId.THANKYOU)(
      dispatch,
      getState
    )
  }
}

export function setNavigationEspots(apiResponse) {
  return (dispatch, getState) => {
    return setEspot(apiResponse, espotGroupId.NAVIGATION)(dispatch, getState)
  }
}

export function setMiniBagEspots(apiResponse) {
  return (dispatch, getState) => {
    const setEspotPromises = []
    setEspotPromises.push(
      setEspot(apiResponse.espots, espotGroupId.MINI_BAG)(dispatch, getState)
    )

    const { products = [] } = apiResponse
    const firstProduct = products[0]
    if (firstProduct) {
      setEspotPromises.push(
        setEspot(firstProduct, espotGroupId.MINI_BAG_MIDDLE)(dispatch, getState)
      )
    }
    return Promise.all(setEspotPromises)
  }
}

export function setProductEspots(apiResponse) {
  return (dispatch, getState) => {
    return setEspot(apiResponse.espots, espotGroupId.PRODUCT)(
      dispatch,
      getState
    )
  }
}

export function setProductListEspots(apiResponse) {
  return (dispatch, getState) => {
    const records = pathOr([], ['records'], apiResponse)
    const promises = []

    /*
     * TODO clean this up, do we need to handle this differently compared to other
     * espot groups? Can we consolidate these two approaches?
     */
    dispatch(removePlpEspots())
    records.forEach((record) => {
      const position = record.Position
      const identifier = `productList${position}`

      const responsiveCMSUrl = path(
        ['contentForMonty', 'members', 'cmsMobileContent', 'responsiveCMSUrl'],
        record
      )

      const responsiveEspotData = [
        {
          identifier,
          responsiveCMSUrl: fixCmsUrl(responsiveCMSUrl),
          position,
          isPlpEspot: true,
        },
      ]
      setEspotData(responsiveEspotData)(dispatch)
      promises.push(setEspotContent(responsiveEspotData)(dispatch, getState))
    })

    return Promise.all(promises)
  }
}

export function setOrderSummaryEspots(apiResponse) {
  return (dispatch, getState) => {
    return setEspot(apiResponse, espotGroupId.ORDER_SUMMARY)(dispatch, getState)
  }
}
