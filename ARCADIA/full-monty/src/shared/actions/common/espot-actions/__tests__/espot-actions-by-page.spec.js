import deepFreeze from 'deep-freeze'
import * as espotActionsByPage from '../espot-actions-by-page'
import * as espotCommonActions from '../espot-common-actions'
import * as espotContent from '../espot-content'

import { espotGroupId } from '../../../../constants/espotsDesktop'

describe('Espot Actions', () => {
  const dispatch = jest.fn()
  const getState = jest.fn()

  beforeEach(() => {
    jest.resetAllMocks()
    jest.spyOn(espotCommonActions, 'setEspotData').mockReturnValue(jest.fn())
    jest.spyOn(espotCommonActions, 'setEspot').mockReturnValue(jest.fn())
    jest.spyOn(espotCommonActions, 'removePlpEspots').mockReturnValue(jest.fn())
    jest.spyOn(espotContent, 'setEspotContent').mockReturnValue(jest.fn())
  })

  describe('setNavigationEspots', () => {
    it('should set responsive espots for navigation', () => {
      const apiResponse = Object.freeze({})

      espotActionsByPage.setNavigationEspots(apiResponse)(dispatch, getState)

      expect(espotCommonActions.setEspot).toHaveBeenCalledTimes(1)
      expect(espotCommonActions.setEspot).toHaveBeenCalledWith(
        apiResponse,
        'navigation'
      )
    })
  })

  describe('setMiniBagEspot', () => {
    it('should not dispatch for middle responsive espot when no product', () => {
      const apiResponse = Object.freeze({})
      espotActionsByPage.setMiniBagEspots(apiResponse)(dispatch)
      expect(espotCommonActions.setEspot).toHaveBeenCalledTimes(1)
      expect(espotCommonActions.setEspot.mock.calls[0][1]).not.toBe(
        espotGroupId.MINI_BAG_MIDDLE
      )
    })

    it('should set responsive espots for top / bottom and middle when there are products', () => {
      const apiResponse = deepFreeze({
        products: [{ firstProduct: 'first' }, { secondProduct: 'second' }],
        espots: { fakeEspot1: {} },
      })

      espotActionsByPage.setMiniBagEspots(apiResponse)(dispatch)

      expect(espotCommonActions.setEspot).toHaveBeenCalledTimes(2)
      expect(espotCommonActions.setEspot).toHaveBeenCalledWith(
        apiResponse.espots,
        'miniBag'
      )
      expect(espotCommonActions.setEspot).toHaveBeenCalledWith(
        apiResponse.products[0],
        'miniBagMiddle'
      )
    })
  })

  describe('setThankyouPageEspots', () => {
    it('should set responsive espots for thank you page', () => {
      const apiResponse = deepFreeze({
        sample: 'data',
        espots: { fakeEspot1: {} },
      })
      espotActionsByPage.setThankyouPageEspots(apiResponse)(dispatch)
      expect(espotCommonActions.setEspot).toHaveBeenCalledWith(
        apiResponse.espots,
        'thankyou'
      )
    })
  })

  describe('setProductListEspots', () => {
    const data = {
      records: [
        {
          Position: 1,
          contentForMonty: {
            members: {
              cmsMobileContent: {
                responsiveCMSUrl: 'url 1',
              },
              encodedcmsMobileContent: 'encoded content 1',
            },
          },
        },
        {
          Position: 2,
          contentForMonty: {
            members: {
              encodedcmsMobileContent: 'encoded content 2',
            },
          },
        },
      ],
    }

    it('should remove previously set PLP espots', () => {
      espotActionsByPage
        .setProductListEspots(data)(dispatch, getState)
        .then(() => {
          expect(espotCommonActions.removePlpEspots).toHaveBeenCalledTimes(1)
        })
    })

    it('should set product list espot data and content in the store', () =>
      espotActionsByPage
        .setProductListEspots(data)(dispatch, getState)
        .then(() => {
          const responsiveEspotData = [
            {
              identifier: 'productList1',
              responsiveCMSUrl: '/url 1',
              position: 1,
              isPlpEspot: true,
            },
          ]

          expect(espotCommonActions.setEspotData).toHaveBeenCalledTimes(2)
          expect(espotCommonActions.setEspotData).toHaveBeenCalledWith(
            responsiveEspotData
          )
          expect(espotContent.setEspotContent).toHaveBeenCalledTimes(2)
          expect(espotContent.setEspotContent).toHaveBeenCalledWith(
            responsiveEspotData
          )
        }))
  })

  describe('setProductEspots', () => {
    it('should set espot content and espot data for product detail in the store', () => {
      const apiResponse = deepFreeze({
        sample: 'data',
        espots: { fakeEspot1: {} },
      })

      espotActionsByPage.setProductEspots(apiResponse)(dispatch, getState)

      expect(espotCommonActions.setEspot).toHaveBeenCalledTimes(1)
      expect(espotCommonActions.setEspot).toHaveBeenCalledWith(
        apiResponse.espots,
        'product'
      )
    })
  })

  describe('setOrderSummaryEspots', () => {
    const identifier = 'fakeIdentifier'
    const orderSummary = {
      [identifier]: {
        EspotContents: {
          encodedcmsMobileContent: `%3C%21--+asdfCMS+Page+Version%+--%3E+`,
        },
      },
    }
    getState.mockImplementation(() => ({
      espot: {
        identifiers: {
          orderSummary: [identifier],
        },
      },
      config: {
        storeCode: 'tsuk',
      },
      debug: {
        environment: 'prod',
      },
    }))

    it('should set responsive espots with correct content', () => {
      espotActionsByPage.setOrderSummaryEspots(orderSummary)(dispatch, getState)

      expect(espotCommonActions.setEspot).toHaveBeenCalledTimes(1)
      expect(espotCommonActions.setEspot).toHaveBeenCalledWith(
        orderSummary,
        'orderSummary'
      )
    })
  })
})
