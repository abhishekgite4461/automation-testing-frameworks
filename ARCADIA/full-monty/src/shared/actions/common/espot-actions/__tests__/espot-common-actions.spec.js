import deepFreeze from 'deep-freeze'
import * as espotActions from '../espot-common-actions'
import * as espotContent from '../espot-content'

describe('Espot Actions', () => {
  const dispatch = jest.fn()
  const getState = jest.fn(() => ({
    config: {},
    viewport: {},
    espot: {
      identifiers: {
        navigation: [],
      },
    },
  }))

  const responsiveEspotData = deepFreeze([
    {
      responsiveCMSUrl: 'url1',
      identifier: 'id1',
      position: 1,
      isPlpEspot: true,
    },
  ])

  beforeEach(() => {
    jest.resetAllMocks()
  })

  describe('setEspotData', () => {
    it('should call dispatch with correct data', () => {
      espotActions.setEspotData(responsiveEspotData)(dispatch, getState)
      const {
        identifier,
        responsiveCMSUrl,
        position,
        isPlpEspot,
      } = responsiveEspotData[0]
      expect(dispatch).toHaveBeenCalledTimes(responsiveEspotData.length)
      expect(dispatch).toHaveBeenCalledWith({
        payload: {
          identifier,
          responsiveCMSUrl,
          position,
          isPlpEspot,
        },
        type: espotActions.SET_ESPOT_DATA,
      })
    })

    it('should provide a default of false for `isPlpEspot` if not provided', () => {
      const dataWithoutIsPlpEspot = [
        { ...responsiveEspotData[0], isPlpEspot: undefined },
      ]
      espotActions.setEspotData(dataWithoutIsPlpEspot)(dispatch, getState)
      const { identifier, responsiveCMSUrl, position } = responsiveEspotData[0]
      expect(dispatch).toHaveBeenCalledWith({
        payload: {
          identifier,
          responsiveCMSUrl,
          position,
          isPlpEspot: false,
        },
        type: espotActions.SET_ESPOT_DATA,
      })
    })
  })

  describe('setEspot', () => {
    it('should call setEspotContent with correct data', () => {
      jest.spyOn(espotContent, 'setEspotContent')
      const apiResponse = {}
      return espotActions
        .setEspot(apiResponse)(dispatch, getState)
        .then(() => {
          expect(espotContent.setEspotContent).toHaveBeenCalled()
        })
    })
  })

  describe('removePlpEspots', () => {
    it('should return an action with the `REMOVE_PLP_ESPOTS` type', () => {
      expect(espotActions.removePlpEspots()).toEqual({
        type: espotActions.REMOVE_PLP_ESPOTS,
      })
    })
  })
})
