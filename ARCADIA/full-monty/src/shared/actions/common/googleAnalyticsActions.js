import { pathOr } from 'ramda'
import { getItem } from '../../../client/lib/cookie/utils'

let dataLayer

if (process.browser) {
  dataLayer = require('../../analytics/dataLayer').default
} else {
  dataLayer = {
    push: (event) => event,
  }
}

const sendEvent = (eventCategory, eventAction, eventLabel, eventValue) => {
  return (dispatch, getState) => {
    if (process.browser) {
      const state = getState()
      const buildVersion = pathOr('', ['debug', 'buildInfo', 'tag'], state)
      const storeCode = pathOr('', ['config', 'storeCode'], state)
      const source = getItem('source') || ''
      const traceId = getItem('traceId2') || ''

      const event = {
        eventCategory,
        eventAction: `${eventAction}`,
        eventLabel,
        eventValue,
        custom: {
          buildVersion,
          brandCode: storeCode,
          source,
          traceId,
        },
      }

      dataLayer.push(event, null, 'GAEvent')
      dispatch({
        type: 'GOOGLE_ANALYTICS_SEND_EVENT',
        event,
      })
    }
  }
}

const sendOrderCompleteEvent = (orderCompleted, orderError) => (dispatch) => {
  if (!orderError) {
    const paymentMethod = pathOr(
      '',
      ['paymentDetails', 0, 'paymentMethod'],
      orderCompleted
    )
    const value = (
      parseFloat(orderCompleted.totalOrderPrice) -
      parseFloat(orderCompleted.deliveryPrice)
    ).toFixed(2)
    dispatch(
      sendEvent('Checkout', 'Order Complete: Success', paymentMethod, value)
    )
  } else {
    dispatch(
      sendEvent(
        'Checkout',
        'Order Complete: Failure',
        undefined,
        `${orderError}`
      )
    )
  }
}

const sendGeoIPEvent = (eventAction, eventLabel, eventValue) => {
  const event = {
    eventCategory: 'GeoIP',
    eventAction,
    eventLabel,
    eventValue,
  }

  dataLayer.push(event, null, 'GAEvent')
  return {
    type: 'GOOGLE_ANALYTICS_SEND_EVENT',
    event,
  }
}

export { sendEvent, sendOrderCompleteEvent, sendGeoIPEvent }
