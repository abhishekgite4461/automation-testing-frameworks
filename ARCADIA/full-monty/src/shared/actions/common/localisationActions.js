export function setLocaleDictionary(dictionary) {
  return {
    type: 'SET_LOCALE_DICTIONARY',
    dictionary,
  }
}
