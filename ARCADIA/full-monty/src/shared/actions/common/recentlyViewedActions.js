import { saveRecentlyViewedState } from '../../../client/lib/storage'

export function deleteRecentlyViewedProduct(productId) {
  return (dispatch, getState) => {
    dispatch({
      type: 'DELETE_RECENTLY_VIEWED_PRODUCT',
      productId,
    })
    saveRecentlyViewedState(getState().recentlyViewed)
  }
}

export function extractRecentlyDataFromProduct(product) {
  const {
    productId,
    lineNumber,
    name,
    amplienceAssets: { images: amplienceImages = [] } = {},
    assets,
    unitPrice,
    iscmCategory,
  } = product
  return (dispatch, getState) => {
    dispatch({
      type: 'ADD_RECENTLY_VIEWED_PRODUCT',
      product: {
        productId,
        lineNumber,
        name,
        imageUrl: assets.find((asset) => asset.assetType === 'IMAGE_THUMB').url,
        amplienceUrl: amplienceImages[0],
        unitPrice,
        iscmCategory,
      },
    })
    saveRecentlyViewedState(getState().recentlyViewed)
  }
}
