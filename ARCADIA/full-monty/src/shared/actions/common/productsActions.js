import React from 'react'
import { get } from '../../lib/api-service'
import { splitQuery, joinQuery } from '../../lib/query-helper'
import url from 'url'
import {
  setInfinityPage,
  setInfinityActive,
  clearInfinityPage,
} from './infinityScrollActions'
import { setGenericError, setApiError } from '../common/errorMessageActions'
import { setPageSatusCode, urlRedirectServer } from '../common/routingActions'
import { showModal } from './modalActions'
import ProductQuickview from '../../components/containers/ProductQuickview/ProductQuickview'
import { browserHistory } from 'react-router'
import { getRouteFromUrl } from '../../lib/get-product-route'
import { setSeoRefinements } from '../components/refinementsActions'
import { getContent } from './cmsActions'
import { setProductEspots, setProductListEspots } from './espot-actions'
import { setDefaultView } from './productViewsActions'
import { range, isEmpty, omit, path, pathOr } from 'ramda'
import { heDecode } from '../../lib/html-entities'
import { updateUniversalVariable } from '../../lib/analytics/qubit-analytics'
import * as analytics from '../../lib/analytics'
import { processRedirectUrl, parseCurrentPage } from '../../lib/products-utils'
import {
  getSelectedRefinements,
  mergeStateAndQueryRefinements,
} from '../../selectors/refinementsSelectors'
import { analyticsPdpClickEvent } from '../../analytics/tracking/site-interactions'
import { getItem } from '../../../client/lib/cookie/utils'
import { productListPageSize } from '../../../server/api/mapping/constants/plp'

const ALL_NUMBERS_REGEX = /^\d*$/
const PRODUCT_REGEX = /^[A-Z]{2}\w*/
const BUNDLE_REGEX = /^BUNDLE_\w*/

export function setProductDetail(product) {
  return {
    type: 'SET_PRODUCT',
    product,
  }
}

export function setProductQuickview(product) {
  return {
    type: 'SET_PRODUCT_QUICKVIEW',
    product,
  }
}

export function setProductIdQuickview(productId) {
  return {
    type: 'SET_PRODUCT_ID_QUICKVIEW',
    productId,
  }
}

export function updateActiveItem(activeItem) {
  return {
    type: 'UPDATE_ACTIVE_ITEM',
    activeItem,
  }
}

export function updateActiveItemQuantity(selectedQuantity) {
  analyticsPdpClickEvent(`productquantity-${selectedQuantity}`)
  return {
    type: 'UPDATE_ACTIVE_ITEM_QUANTITY',
    selectedQuantity,
  }
}

export function updateSelectedOosItem(selectedOosItem) {
  return {
    type: 'UPDATE_SELECTED_OOS_ITEM',
    selectedOosItem,
  }
}

export function updateShowItemsError(showError) {
  return {
    type: 'UPDATE_SHOW_ITEMS_ERROR',
    showError,
  }
}

export function setSizeGuide(sizeGuideType) {
  return {
    type: 'SET_SIZE_GUIDE',
    sizeGuideType,
  }
}

export function showSizeGuide() {
  return {
    type: 'SHOW_SIZE_GUIDE',
  }
}

export function hideSizeGuide() {
  return {
    type: 'HIDE_SIZE_GUIDE',
  }
}

export function updateSeeMoreUrl(seeMoreLink, seeMoreUrl) {
  return {
    type: 'UPDATE_SEE_MORE_URL',
    seeMoreLink,
    seeMoreUrl,
  }
}

export const getSeeMoreUrls = (seeMoreValue = []) => (dispatch) => {
  const extractEndecaId = (seeMoreLink) => {
    return seeMoreLink.split('/').pop()
  }
  const getSeeMoreUrl = (seeMoreValueItem) => {
    const seeMoreLink = seeMoreValueItem && seeMoreValueItem.seeMoreLink
    if (seeMoreLink) {
      return dispatch(
        get(`/products?endecaSeoValue=${extractEndecaId(seeMoreLink)}`)
      )
        .then(({ body }) => {
          return dispatch(updateSeeMoreUrl(seeMoreLink, body.canonicalUrl))
        })
        .catch(() => {})
    }
  }
  const promises = seeMoreValue
    .filter(Boolean)
    .filter(
      (i) =>
        !isEmpty(i) &&
        Object.prototype.hasOwnProperty.call(i, 'seeMoreLink') &&
        !isEmpty(i.seeMoreLink)
    )
    .map(getSeeMoreUrl)
  return Promise.all(promises)
}

export function getProduct(args, isQuickview) {
  return (dispatch, getState) => {
    // [MJI-1077] We need to be able to handle both:
    // /en/tsuk/product/{identifier}
    // /wcs/stores/servlet/ProductDisplay?langId=-1&catalogId=33057&storeId=12556&productId=32409407
    const query = getState().routing.location.query
    const identifier =
      args.identifier ||
      path(['productId'], query) ||
      path(['partNumber'], query)

    // If its not a productId send full pathname
    const param =
      typeof identifier === 'number' ||
      ALL_NUMBERS_REGEX.test(identifier) ||
      PRODUCT_REGEX.test(identifier) ||
      BUNDLE_REGEX.test(identifier)
        ? identifier
        : getState().routing.location.pathname

    return dispatch(get(`/products/${encodeURIComponent(param)}`))
      .then(({ body }) => {
        if (isQuickview) {
          dispatch(setProductQuickview(body))
        } else {
          dispatch(setProductDetail(body))
        }
        // if One size product then make that size active
        if (body.items) {
          const activeItem = body.items.find((item) => item.quantity > 0)
          dispatch(
            updateActiveItem(
              activeItem && body.items.length === 1 ? activeItem : {}
            )
          )
        }

        // if URL is based on a product code, update to use the full product name
        if (!isQuickview && browserHistory && param === identifier) {
          const pathname = getRouteFromUrl(body.sourceUrl)
          browserHistory.replace({ pathname, query })
        }

        const { seeMoreValue = [] } = body
        dispatch(getSeeMoreUrls(seeMoreValue))
        // TODO: remove espot info from product in state. Same goes for all set espots dispatch
        return dispatch(setProductEspots(body))
      })
      .catch((err) => {
        dispatch(setApiError(err))
        dispatch(setProductDetail({ success: false }))
      })
  }
}

export function emailMeStock(formData) {
  const query = joinQuery(formData)
  return (dispatch) => {
    return dispatch(get(`/email-me-in-stock${query}`))
      .then(({ body }) => {
        dispatch({
          type: 'EMAIL_ME_STOCK_SUCCESS',
          body,
        })
      })
      .catch((err) => {
        if (err.response && err.response.body) {
          err.message = err.response.body.message
          dispatch(setGenericError(err))
        } else {
          dispatch(setApiError(err))
        }
      })
  }
}

export function clearProduct() {
  return {
    type: 'CLEAR_PRODUCT',
  }
}

function getCategoryFromBreadcrumbs(breadcrumbs) {
  const breadcrumb =
    breadcrumbs && breadcrumbs.length && breadcrumbs[breadcrumbs.length - 1]
  return breadcrumb && breadcrumb.category
}

function getCategoryFromState({ products: { breadcrumbs } }) {
  return getCategoryFromBreadcrumbs(breadcrumbs)
}

function fetchProductsBySeo(pathname) {
  return get(`/products/seo?seoUrl=${pathname}&pageSize=${productListPageSize}`)
}

const ENDECA_DELIMETER = '|||'
const QUBIT_ENDECA_AB_COOKIE_KEY = 'qubitSearchABTestKey'

const endecaPOC = (query) => {
  if (!process.browser || !('q' in query)) return query

  const searchKey = getItem(QUBIT_ENDECA_AB_COOKIE_KEY)
  if (typeof searchKey !== 'string') return query

  return {
    ...query,
    q: `${query.q}${ENDECA_DELIMETER}${searchKey}`,
  }
}

function fetchProductsByQuery(query) {
  query = endecaPOC(query)

  // CANT ENCODE CATEGORY BECAUSE SERVER
  let search = joinQuery(omit(['category'], query))
  search = query.category ? `${search}&category=${query.category}` : search
  return get(`/products${search}`)
}

export function fetchProducts(pathname, query) {
  const {
    category,
    categoryId,
    q,
    refinements: nonSeoRefinements = '',
    currentPage,
    pageSize,
    sort: nonDefaultSort,
  } = query
  const isLegacyPath =
    pathname === '/webapp/wcs/stores/servlet/CatalogNavigationSearchResultCmd'
  return (dispatch, getState) => {
    if (category || q || isLegacyPath) {
      // We already have the category or the query - so we can just use the non seo endpoint
      const refinements = getSelectedRefinements(getState())
      const selectedOptions = pathOr(
        {},
        ['refinements', 'selectedOptions'],
        getState()
      )
      const appliedOptions = pathOr(
        {},
        ['refinements', 'appliedOptions'],
        getState()
      )
      const sort = getState().sorting.currentSortOption
      let searchQuery = {}
      if (isLegacyPath) {
        searchQuery = {
          ...omit(['sort_field', 'categoryId'], query),
          sort: decodeURIComponent(query.sort_field),
        }
      }
      if (currentPage) searchQuery.currentPage = currentPage
      if (categoryId) searchQuery.category = categoryId
      if (category) searchQuery.category = category
      if (q) searchQuery.q = q
      if (pageSize) searchQuery.pageSize = pageSize
      if (refinements.length)
        searchQuery.refinements = JSON.stringify(
          mergeStateAndQueryRefinements(
            selectedOptions,
            appliedOptions,
            nonSeoRefinements.split(',')
          )
        )
      if (sort) searchQuery.sort = decodeURIComponent(sort)
      return dispatch(fetchProductsByQuery(searchQuery))
    }

    // We don't have a query or category so we have to use the seo endpoint
    // This cannot refine or paginate.
    return dispatch(fetchProductsBySeo(pathname)).then((res) => {
      const { refinements, breadcrumbs } = res.body
      // Update the seo refinements with what we got from the response
      dispatch(setSeoRefinements(refinements))

      // If we haven't got all the refines we need, or we wanted a page greater than 1
      // And we've managed to get a catgeory - do it again.
      const seoResultCategory = getCategoryFromBreadcrumbs(breadcrumbs)
      if (
        seoResultCategory &&
        (nonSeoRefinements || currentPage > 1 || nonDefaultSort)
      ) {
        return dispatch(
          fetchProducts(pathname, {
            category: seoResultCategory,
            currentPage,
            pageSize,
            refinements: nonSeoRefinements,
          })
        )
      }

      // Or just return the products we've got, they're the right ones
      return res
    })
  }
}

export function loadingProducts(loading) {
  return {
    type: 'LOADING_PRODUCTS',
    loading: heDecode(loading),
  }
}

export function loadingMoreProducts(isLoadingMore = true) {
  return {
    type: 'LOADING_MORE_PRODUCTS',
    isLoadingMore,
  }
}

export function addToProducts() {
  return (dispatch, getState) => {
    const category = getCategoryFromState(getState())
    const {
      products: {
        location: { pathname, query },
        products,
        refinements,
      },
    } = getState()

    const nextPage = parseCurrentPage(products.length)

    const newQuery = {
      ...query,
      category,
      currentPage: nextPage,
      pageSize: productListPageSize,
    }

    if (category) {
      delete newQuery.q
    }

    dispatch(loadingMoreProducts())

    // set the current active refinements in the redux store if not already set
    // so they are available when building the request query to fetch more products
    dispatch(setSeoRefinements(refinements))

    return dispatch(fetchProducts(pathname, newQuery))
      .then(({ body: { products } }) => {
        dispatch(loadingMoreProducts(false))
        // No products were available, don't change the state
        if (!products.length) {
          return null
        }

        dispatch({
          type: 'ADD_TO_PRODUCTS',
          products,
        })
        const location = getState().routing.location
        browserHistory.replace({
          ...location,
          query: {
            ...location.query,
            currentPage: nextPage,
          },
        })
        dispatch(setInfinityPage(nextPage))
        dispatch(setInfinityActive())
        dispatch(updateUniversalVariable())
      })
      .catch((err) => {
        dispatch(loadingMoreProducts(false))
        if (err.response && err.response.body) {
          err.message = err.response.body.message
          dispatch(setGenericError(err))
        } else {
          dispatch(setApiError(err))
        }
      })
  }
}

export function setProductsLocation(location) {
  return {
    type: 'SET_PRODUCTS_LOCATION',
    location,
  }
}

function isSameList({ pathname, query }, newLocation) {
  return (
    pathname === newLocation.pathname &&
    query.q === newLocation.query.q &&
    query.category === newLocation.query.category &&
    query.refinements === newLocation.query.refinements
  )
}

// Burton is incapable of filtering by promotion
// This is a simple HACK to remove the option before setting products
function fixRefinements(code, refinements = []) {
  if (code === 'br') {
    return refinements.filter(
      ({ label }) => label.toLowerCase() !== 'promotion'
    )
  }
  return refinements
}

function setProducts(body) {
  return (dispatch, getState) => {
    const { brandCode } = getState().config
    dispatch({
      type: 'SET_PRODUCTS',
      body: {
        ...body,
        refinements: fixRefinements(brandCode, body.refinements),
      },
    })

    dispatch(setDefaultView(body.defaultImgType))

    dispatch(analytics.setProducts())
  }
}

export function updateProductsLocation(
  { refinements, canonicalUrl, searchTerm },
  location
) {
  return (dispatch) => {
    const pathname = getRouteFromUrl(canonicalUrl)
    // If we have a valid route to navigate to, then use it
    if (!searchTerm && !refinements && pathname) {
      const query = omit(['category', 'q'], location.query)
      // Set the product location, to stop it fetching again when the url updates.
      dispatch(setProductsLocation({ pathname, query }))
      const newLocation = { pathname }
      if (!isEmpty(query)) newLocation.search = joinQuery(query)
      browserHistory.replace(newLocation)
    } else {
      dispatch(setProductsLocation(location))
    }
  }
}

export function removeProducts() {
  return {
    type: 'REMOVE_PRODUCTS',
  }
}

export function removeProductsLocation() {
  return {
    type: 'REMOVE_PRODUCTS_LOCATION',
  }
}

export function getProducts(location) {
  return (dispatch, getState) => {
    const { location: currentLocation, redirectUrl } = getState().products

    if (redirectUrl) processRedirectUrl(redirectUrl)
    if (currentLocation && isSameList(currentLocation, location)) return null

    dispatch(clearInfinityPage())
    dispatch(setProductsLocation(location))
    dispatch(removeProducts())
    dispatch(loadingProducts(splitQuery(location.search).q))

    return dispatch(fetchProducts(location.pathname, location.query))
      .then(({ body }) => {
        if (body.cmsPage && body.cmsPage.seoUrl) {
          dispatch(removeProductsLocation())
          return browserHistory.replace(body.cmsPage.seoUrl)
        } else if (body.redirectUrl) {
          return processRedirectUrl(body.redirectUrl)
        } else if (body.permanentRedirectUrl) {
          dispatch(removeProductsLocation())
          return processRedirectUrl(body.permanentRedirectUrl)
        }
        dispatch(setProducts(body))

        // If we fetched using a search term, but we got back a real category - nav to it so refinements works
        if (path(['query', 'q'], location) && body.canonicalUrl) {
          dispatch(updateProductsLocation(omit(['searchTerm'], body), location))
        }
        return dispatch(setProductListEspots(body.plpContentSlot))
      })
      .catch((err) => {
        dispatch(setApiError(err))
        dispatch(setProducts({ totalProducts: 0 }))
      })
  }
}

// Get products using refinmenets too
export function updateCurrentProducts() {
  return (dispatch, getState) => {
    const {
      products: { categoryTitle },
      routing: { location = {} },
    } = getState()
    const category = getCategoryFromState(getState())

    const { q, searchTerm } = location.query

    let query = { category }

    if (searchTerm) query = location.query
    else if (q) query = { q }

    dispatch(clearInfinityPage())
    dispatch(loadingProducts(categoryTitle))
    return dispatch(fetchProducts(location.pathname, query))
      .then(({ body }) => {
        dispatch(updateProductsLocation(body, location))
        dispatch(setProducts(body))
      })
      .catch((err) => {
        if (err.response && err.response.body) {
          err.message = err.response.body.message
          dispatch(setGenericError(err))
        } else {
          dispatch(setApiError(err))
        }
      })
  }
}

export function fetchCombinedProducts(pathname, query) {
  return (dispatch) => {
    // Calculate the page size and amount of pages needed
    const currentPage = parseInt(query.currentPage, 10)
    const totalItems = currentPage * productListPageSize
    const pageSize = productListPageSize
    let fullPages = Math.ceil(totalItems / pageSize)

    if (!process.browser && fullPages > 3) fullPages = 3 // Defensive SS request
    const promises = range(1, fullPages + 1).map((val) => {
      const pageQuery = {
        ...query,
        currentPage: val,
        pageSize,
      }
      dispatch(setInfinityPage(fullPages))
      return dispatch(fetchProducts(pathname, pageQuery))
    })

    // Combine all the responses into one MEGA RESPONSE
    return Promise.all(promises).then((array) => {
      return array.reduce(
        (prev, { body }) => {
          return Object.assign(body, {
            products: prev.products.concat(body.products),
          })
        },
        { products: [] }
      )
    })
  }
}

export function getServerProducts({ search, pathname }, replace = false) {
  const sanitisedQuery = splitQuery(search)

  if (pathname.startsWith('/search') && !replace) return false

  // Want to send the full query to the server, same as if the user had typed it into the search box
  const query = {
    ...sanitisedQuery,
    q: heDecode(sanitisedQuery.q),
  }
  const pagination = !!query.pagination
  const currentPage = query.currentPage ? parseInt(query.currentPage, 10) : 0

  return (dispatch) => {
    // Set the location so we don't double fetch when landing client side JS
    dispatch(setProductsLocation({ pathname, query: sanitisedQuery }))
    // Set the products loading
    dispatch(loadingProducts(query.q))

    return Promise.resolve()
      .then(() => {
        if (pagination || currentPage < 2) {
          // We only need one page, just fetchProducts and go.
          return dispatch(fetchProducts(pathname, query)).then(
            ({ body }) => body
          )
        } else if (query.category || query.q) {
          // At this point we know we need to get multiple pages
          // If we have a query we can do that immediately
          return dispatch(fetchCombinedProducts(pathname, query))
        }
        return dispatch(
          fetchProducts(pathname, omit(['currentPage', 'refinements'], query))
        ).then(({ body: { breadcrumbs } }) => {
          const category = getCategoryFromBreadcrumbs(breadcrumbs)
          // Now get them all from the category
          return dispatch(
            fetchCombinedProducts(pathname, {
              ...query,
              category,
            })
          )
        })
      })
      .then((body) => {
        const { permanentRedirectUrl } = body
        if (permanentRedirectUrl) {
          const { pathname } = url.parse(permanentRedirectUrl)
          return dispatch(urlRedirectServer({ url: pathname, permanent: true }))
        }

        // Found a CMS page - go to it
        // replace exist when you come here via search (where replace comes from the react router)
        // but if we come here via a plp link then replace does not exist and we need to return so that SSR can to the redirect
        if (body.cmsPage && body.cmsPage.seoUrl) {
          dispatch(urlRedirectServer({ url: body.cmsPage.seoUrl }))
          return replace ? replace() : false
        }
        // If it was not a search (so it looks like a category url) and there was no products Set the page to 404
        if (!query.q && (!body.products || !body.products.length)) {
          dispatch(getContent({ cmsPageName: 'error404' }))
          dispatch(setPageSatusCode(404))
        }

        dispatch(setProducts(body))

        if (body.products && body.products.length === 1 && replace) {
          const newLocation = getRouteFromUrl(body.products[0].sourceUrl)
          return replace(newLocation)
        }
        return (
          dispatch(setProductListEspots(body.plpContentSlot))
            // this stop SSR error on react-router
            .then(() => null)
        )
      })
      .catch((err) => {
        dispatch(setApiError(err))
        dispatch(setProducts({ totalProducts: 0 }))
      })
  }
}

export function showQuickviewModal() {
  return showModal(<ProductQuickview />, { mode: 'plpQuickview' })
}
