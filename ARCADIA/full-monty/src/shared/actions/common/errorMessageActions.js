import { errorReport } from '../../../client/lib/reporter'
import { path } from 'ramda'

function isProduction() {
  return process.env.NODE_ENV === 'production'
}

function serialiseError({ message, stack }) {
  return {
    message,
    stack,
  }
}

function setError(error) {
  let decoratedError
  if (isProduction()) {
    if (error.message) {
      decoratedError = {
        ...error,
        nativeError: {
          message: error.message,
        },
      }
    } else {
      decoratedError = {
        ...error,
        message: error.nativeError,
        nativeError: {
          message: '',
        },
      }
    }
  } else if (error.nativeError) {
    decoratedError = {
      ...error,
      nativeError: serialiseError(error.nativeError),
    }
  } else {
    decoratedError = error
  }

  if (process.browser) {
    if (typeof decoratedError.nativeError !== 'undefined') {
      errorReport('set-error', {
        ...decoratedError,
        message: decoratedError.nativeError.message,
      })
    } else {
      errorReport('set-error', decoratedError)
    }
  }

  return {
    type: 'SET_ERROR',
    error: decoratedError,
  }
}

export function removeError() {
  return {
    type: 'SET_ERROR',
    error: null,
  }
}

export function setCmsError() {
  // In case of CMS error we have to show to the user always the not found page.
  return setError({ statusCode: 404 })
}

function isSessionExpired(error = {}) {
  return (
    path(['response', 'headers', 'session-expired'], error) !== undefined ||
    error.message === 'Must be logged in to perform this action'
  )
}

export function setGenericError(error) {
  return (dispatch) => {
    if (!isSessionExpired(error)) {
      dispatch(
        setError({
          message: error.message,
          isOverlay: true,
          nativeError: error,
          noReload: true,
        })
      )
    }
  }
}

export function setApiError(error) {
  return (dispatch) => {
    if (!isSessionExpired(error)) {
      if (error.response && error.response.body) {
        dispatch(
          setError({
            ...error.response.body,
            isOverlay: true,
          })
        )
      } else {
        dispatch(
          setError({
            message: 'There was a problem, please try again later.',
            isOverlay: true,
            nativeError: error,
          })
        )
      }
    }
  }
}
