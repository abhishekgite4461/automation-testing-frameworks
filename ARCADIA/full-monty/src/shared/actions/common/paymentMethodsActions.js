import { joinQuery } from '../../lib/query-helper'
import { MONTY_API } from '../../middleware/preprocess-redux-api-middleware'

export function getPaymentMethods(req) {
  const { delivery, billing, amount, value } = req
  const queryParams = {
    ...(delivery && { delivery }),
    ...(billing && { billing }),
    ...(value && { value }),
    ...(amount && { amount }),
  }

  let query = joinQuery(queryParams)

  // Fallback in case all of the params passed in are empty strings or undefined
  // This is a temporary solution to prevent the server from dealing with undefined delivery and billing addresses which should never be used.
  // Previously this method sent things like 'billingAddress=undefined' which the server took to be the string, 'undefined' and returned a default list.
  // This should be fixed on the server.
  if (query === '?') query = '?value=all'

  return {
    [MONTY_API]: {
      endpoint: `/payments${query}`,
      method: 'GET',
      overlay: true,
      typesPrefix: 'GET_PAYMENT_METHODS',
    },
  }
}

export function getAllPaymentMethods() {
  return (dispatch) => dispatch(getPaymentMethods({ value: 'all' }))
}

export function removePaymentMethod(body) {
  return {
    [MONTY_API]: {
      endpoint: `/payments`,
      body,
      method: 'DELETE',
      typesPrefix: 'DEL_PAYMENT_METHODS',
    },
  }
}
