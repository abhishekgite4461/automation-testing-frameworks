/* eslint-disable no-unused-vars */
import { getLastPage, pageBasedAnalytics } from '../../lib/analytics/analytics'
import pagesData from '../../lib/analytics/page-data'
import { dispatchQubitEventHook } from '../../lib/analytics/qubit-analytics'
import { isReturningCustomer } from '../../selectors/checkoutSelectors'

export function setAdobeAnalyticsEnv(environment) {
  return {
    type: 'SET_ADOBE_ANALYTICS_ENV',
    environment,
  }
}

export function percentagePageViewed(percentagePageViewed) {
  if (window.s)
    window.s.Util.cookieWrite('mostRecentProp19', percentagePageViewed)

  return {
    type: 'SET_PERCENTAGE_VIEWED',
    percentagePageViewed,
  }
}

export function setInternalNavigation(intNav) {
  return {
    type: 'SET_INT_NAV',
    intNav,
  }
}

export function clearInternalNavigation() {
  return {
    type: 'CLEAR_INT_NAV',
  }
}

export function pageLoaded(pageName, loadTime, sendAdobe) {
  return (dispatch, getState) => {
    if (sendAdobe) {
      const state = getState()
      const { getData = () => ({}) } = pagesData[pageName] || {}
      const pageData = getData(state) || {}

      const analyticsData = {
        ...pageData,
        loadTime,
        lastPage: getLastPage(state.routing.visited),
        percentagePageViewed: state.adobe.percentagePageViewed,
        intNav: state.adobe.intNav,
        buildVersion: state.debug.buildInfo.tag,
      }

      analyticsData.eVar87 = isReturningCustomer(getState())
        ? 'Returning'
        : 'New'

      pageBasedAnalytics(analyticsData)
      if (state.routing.visited.length > 1) dispatchQubitEventHook()
    }
    dispatch(clearInternalNavigation())
    dispatch({
      type: 'PAGE_LOADED',
      payload: {
        pageName,
        loadTime,
      },
    })
    const pageLoadedEvent = new Event('PAGE_LOADED')
    document.dispatchEvent(pageLoadedEvent)
  }
}
