import * as swatchesActions from '../common/swatchesActions'
import * as analytics from '../../lib/analytics'
import { analyticsPlpClickEvent } from '../../analytics/tracking/site-interactions'

function setGrid(columns) {
  return (dispatch) => {
    analyticsPlpClickEvent(`setgrid-${columns}`)
    dispatch({
      type: 'SET_GRID_LAYOUT',
      columns,
    })
    dispatch(analytics.layoutChange())
  }
}

export function setGridLayout(grid) {
  return (dispatch) => {
    dispatch(setGrid(grid))
    dispatch(swatchesActions.resetSwatchesPage())
  }
}
