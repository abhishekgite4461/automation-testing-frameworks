import { updateCurrentProducts } from '../common/productsActions'
import { browserHistory } from 'react-router'
import { analyticsPlpClickEvent } from '../../analytics/tracking/site-interactions'

export function selectSortOption(option) {
  return (dispatch, getState) => {
    const { currentSortOption } = getState().sorting

    if (currentSortOption !== option) {
      analyticsPlpClickEvent(`sortoption-${option.toLowerCase()}`)
      dispatch({
        type: 'SELECT_SORT_OPTION',
        option,
      })

      // Persist the sort in the URL
      const { pathname, query } = getState().routing.location
      browserHistory.replace({
        pathname,
        query: {
          ...query,
          sort: encodeURIComponent(option),
        },
      })

      return dispatch(updateCurrentProducts())
    }
  }
}

export function clearSortOptions() {
  return {
    type: 'CLEAR_SORT_OPTIONS',
  }
}
