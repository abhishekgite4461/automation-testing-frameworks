import configureMockStore from 'test/unit/lib/configure-mock-store'
import { getMockStoreWithInitialReduxState } from 'test/unit/helpers/get-redux-mock-store'
import stores from 'test/mocks/store-locator'
import { maps } from 'test/mocks/google'

import * as actions from '../StoreLocatorActions'
import { setSelectedBrandFulfilmentStore } from '../../common/selectedBrandFulfilmentStoreActions'
import { get } from '../../../lib/api-service'
import { getItem, setStoreCookie } from '../../../../client/lib/cookie'
import { setSelectedStore } from '../../../../client/lib/storage'
import {
  shouldFetchBagStock,
  getSkuList,
  getBasketDetails,
  getAppliedFilters,
} from '../../../lib/store-locator-utilities'
import * as checkoutActions from '../../common/checkoutActions'
import * as productSelectors from '../../../selectors/productSelectors'
import * as storeLocatorSelectors from '../../../selectors/storeLocatorSelectors'
import * as analytics from '../../../analytics'

if (!global.google) {
  global.google = {}
}
global.google.maps = maps

jest.mock('../../common/selectedBrandFulfilmentStoreActions')
jest.mock('../../../analytics')

jest.mock('../../../lib/api-service', () => ({
  get: jest.fn(() => {
    return {
      type: 'GET',
      body: [{}],
    }
  }),
}))

jest.mock('../../../lib/store-locator-utilities', () => ({
  shouldFetchBagStock: jest.fn().mockReturnValue(true),
  getAppliedFilters: jest.fn().mockReturnValue([]),
  getSkuList: jest.fn().mockReturnValue('sku_1,sku_2'),
  getBasketDetails: jest.fn().mockReturnValue('sku1:1,sku2:3'),
}))

jest.mock('../../../../client/lib/cookie', () => ({
  setItem: jest.fn(() => 'TS0001'),
  getItem: jest.fn(() => 'TS0001'),
  setStoreCookie: jest.fn(),
}))

jest.mock('../../../../client/lib/storage', () => ({
  setSelectedStore: jest.fn(),
  loadRecentlyViewedState: jest.fn(() => []),
}))

jest.mock('../../common/checkoutActions', () => ({
  setDeliveryStore: jest.fn(),
}))

describe('Store locator actions', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  afterAll(() => {
    jest.resetAllMocks()
  })

  describe('@setFulfilmentStore', () => {
    it('should update selectedStore', () => {
      const store = configureMockStore({
        storeLocator: {
          selectedStore: { storeId: 'TS0001' },
        },
      })
      expect(store.getState().storeLocator.selectedStore.storeId).toBe('TS0001')
      store.dispatch(actions.setFulfilmentStore({ storeId: 'TS0002' }))
      expect(store.getState().storeLocator.selectedStore.storeId).toBe('TS0002')
    })
  })

  describe('@getSelectedStoreIdFromCookie', () => {
    afterEach(() => {
      jest.clearAllMocks()
    })

    it('sets selected store id from cookie', () => {
      jest.spyOn(actions, 'setSelectedStoreID')

      const dispatch = jest.fn()
      const action = actions.getSelectedStoreIdFromCookie()

      expect(actions.setSelectedStoreID).not.toHaveBeenCalled()

      action(dispatch)

      expect(getItem).toHaveBeenLastCalledWith('WC_pickUpStore')
      expect(actions.setSelectedStoreID).toHaveBeenCalledTimes(1)
      expect(actions.setSelectedStoreID).toHaveBeenLastCalledWith('TS0001')
      expect(getItem).toHaveBeenCalledTimes(1)
    })
  })

  describe('@setFilters', () => {
    it('sets filters for store types', () => {
      const store = configureMockStore({
        storeLocator: {
          selectedStoreId: 'TS0002',
        },
      })
      const filters = {
        today: { applied: true, selected: true, disabled: false },
        brand: { applied: true, selected: true, disabled: false },
        parcel: { applied: true, selected: false, disabled: true },
        other: { applied: true, selected: false, disabled: true },
      }

      store.dispatch(actions.setFilters(filters))

      expect(store.getState().storeLocator.filters).toEqual({
        today: { applied: true, selected: true, disabled: false },
        brand: { applied: true, selected: true, disabled: false },
        parcel: { applied: true, selected: false, disabled: true },
        other: { applied: true, selected: false, disabled: true },
      })
    })
  })

  describe('changeFulfilmentStore', () => {
    it('sets store address and storeId', () => {
      jest.spyOn(actions, 'setSelectedStoreID')
      jest.spyOn(actions, 'setFulfilmentStore')

      const storeObj = {
        storeId: 'TS0001',
        address: {
          line1: 'line1',
          line2: 'line2',
          city: 'city',
          postcode: 'postcode',
        },
      }
      const action = actions.changeFulfilmentStore(storeObj)
      const dispatch = jest.fn()

      expect(actions.setSelectedStoreID).not.toHaveBeenCalled()
      expect(actions.setFulfilmentStore).not.toHaveBeenCalled()
      expect(checkoutActions.setDeliveryStore).not.toHaveBeenCalled()

      action(dispatch)

      expect(setStoreCookie).toHaveBeenCalledTimes(1)
      expect(setStoreCookie).toHaveBeenLastCalledWith(storeObj)
      expect(setSelectedStore).toHaveBeenCalledTimes(1)
      expect(setSelectedStore).toHaveBeenLastCalledWith(storeObj)
      expect(actions.setSelectedStoreID).toHaveBeenCalledTimes(1)
      expect(actions.setSelectedStoreID).toHaveBeenLastCalledWith(
        storeObj.storeId
      )
      expect(actions.setFulfilmentStore).toHaveBeenCalledTimes(1)
      expect(actions.setFulfilmentStore).toHaveBeenLastCalledWith(storeObj)
      expect(checkoutActions.setDeliveryStore).toHaveBeenCalledTimes(1)
      expect(checkoutActions.setDeliveryStore).toHaveBeenLastCalledWith(
        storeObj
      )
    })
  })

  describe('@selectStore', () => {
    it('should dispatch an analytics click event', async () => {
      const productId = 'foo123'
      jest.spyOn(storeLocatorSelectors, 'getStoreByIndex').mockReturnValue({})
      jest.spyOn(productSelectors, 'getCurrentProductId')
      productSelectors.getCurrentProductId.mockReturnValue(productId)

      const fakeAnalyticsAction = Object.freeze({ type: 'FAKE_CLICK_EVENT' })
      analytics.sendAnalyticsClickEvent.mockReturnValue(fakeAnalyticsAction)

      const store = getMockStoreWithInitialReduxState({})
      await store.dispatch(actions.selectStore(123))
      expect(analytics.sendAnalyticsClickEvent).toHaveBeenCalledTimes(1)
      expect(analytics.sendAnalyticsClickEvent).toHaveBeenCalledWith({
        category: analytics.GTM_CATEGORY.PDP,
        action: analytics.GTM_ACTION.IN_STORE_POSTCODE_RESULT,
        label: productId,
      })
      expect(store.getActions()).toEqual(
        expect.arrayContaining([fakeAnalyticsAction])
      )
    })
  })

  describe('@getStoreForModal', () => {
    let dispatch
    let storeLocatorType

    const store = configureMockStore({
      userLocator: {
        selectedPlaceDetails: {
          place_id: 'id',
        },
      },
      storeLocator: {
        filters: {
          today: {
            applied: true,
            selected: true,
            disabled: false,
          },
          brand: {
            applied: true,
            selected: true,
            disabled: false,
          },
          parcel: {
            selected: true,
            applied: true,
            disabled: false,
          },
        },
      },
      features: {
        status: {
          FEATURE_CFSI: true,
        },
      },
      findInStore: {
        activeItem: {
          sku: 'sku',
        },
      },
      config: {
        siteId: 42,
      },
    })

    beforeEach(() => {
      storeLocatorType = 'findInStore'
      dispatch = jest.fn((fn) => Promise.resolve(fn))
      actions.setMarkers = jest.fn()
    })

    it('getStoreModal with `collectFromStore` as locator type queries the store locator with the provided filters as the `types` url query param', () => {
      getAppliedFilters.mockReturnValueOnce(['today', 'brand', 'parcel'])
      storeLocatorType = 'collectFromStore'
      const expectedUrl =
        '/store-locator?latitude=undefined&longitude=undefined&brandPrimaryEStoreId=42&deliverToStore=true&types=today%2Cbrand%2Cparcel&cfsi=true'

      actions.getStoreForModal(storeLocatorType)(dispatch, store.getState)
      expect(dispatch.mock.calls[0][0].name).toBe('updateCurrentLatLngThunk')
      expect(get).toHaveBeenCalledWith(expectedUrl)
    })

    it('getStoreModal with `findInStore` as the locator type queries the store locator with a `types` url query param of `brand` as well as a `sku` query param', () => {
      const expectedUrl =
        '/store-locator?latitude=undefined&longitude=undefined&brandPrimaryEStoreId=42&deliverToStore=true&types=brand&sku=sku&cfsi=true'

      actions.getStoreForModal(storeLocatorType)(dispatch, store.getState)
      expect(get).toHaveBeenCalledWith(expectedUrl)
    })

    it('should dispatch an analytics click event which tracks the product ID', () => {
      const productId = 'foo123'

      jest
        .spyOn(productSelectors, 'getCurrentProductId')
        .mockReturnValue(productId)

      actions.getStoreForModal(storeLocatorType)(dispatch, store.getState)
      expect(analytics.sendAnalyticsClickEvent).toHaveBeenCalledTimes(1)
      expect(analytics.sendAnalyticsClickEvent).toHaveBeenCalledWith({
        category: analytics.GTM_CATEGORY.PDP,
        action: analytics.GTM_ACTION.IN_STORE_POSTCODE_GO,
        label: productId,
      })
    })

    it('loads a store list on a successful response from the store locator query and calls `setMarkers` when stores are present', async () => {
      get.mockReturnValueOnce(Promise.resolve({ body: stores }))

      await actions.getStoreForModal(storeLocatorType)(dispatch, store.getState)
      expect(dispatch.mock.calls[4][0].stores).toEqual(stores)
      expect(actions.setMarkers).toHaveBeenCalled()
    })

    it('loads a store list on a successful response from the store locator query and calls `noStoresFound` when no stores are present', async () => {
      get.mockReturnValueOnce(Promise.resolve({ body: [] }))

      await actions.getStoreForModal(storeLocatorType)(dispatch, store.getState)
      expect(dispatch.mock.calls[4][0].stores).toEqual([])
      expect(actions.setMarkers).not.toHaveBeenCalled()
      expect(dispatch.mock.calls[7][0].type).toBe('NO_STORES_FOUND')
    })

    it('calls dispatch with `getStoresError` if the store locator query promise is rejected', async () => {
      const errorMessage = 'I have been rejected'
      get.mockReturnValueOnce(Promise.reject(errorMessage))

      await actions.getStoreForModal(storeLocatorType)(dispatch, store.getState)
      expect(actions.setMarkers).not.toHaveBeenCalled()
      expect(dispatch.mock.calls[4][0].error).toBe(errorMessage)
      expect(dispatch.mock.calls[5][0].loading).toBe(false)
    })
  })

  describe('@getStoresForCheckoutModal()', () => {
    let dispatch
    let getState

    beforeEach(() => {
      dispatch = jest.fn((fn) => Promise.resolve(fn))
      getState = jest.fn()
    })

    it('should fetch stores for CollectFromStore component', () => {
      getState.mockReturnValue({})

      const thunk = actions.getStoresForCheckoutModal()
      thunk(dispatch, getState)

      expect(dispatch.mock.calls[0][0].name).toBe('updateCurrentLatLngThunk')
      expect(dispatch.mock.calls[1][0]).toEqual({
        type: 'GET_STORES_LOADING',
        loading: true,
      })
      expect(dispatch.mock.calls[2][0]).toEqual({
        type: 'GET',
        body: [{}],
      })
    })

    it('should include basketDetails in the url to fetch stores with stock data if CFSI is ON', () => {
      getState.mockImplementation(() => ({
        features: {
          status: { FEATURE_CFSI: true },
        },
        config: {
          siteId: 1234,
        },
      }))
      const expectedUrl =
        '/store-locator?latitude=undefined&longitude=undefined&brandPrimaryEStoreId=1234&deliverToStore=true&types=&cfsi=true&basketDetails=sku1%3A1%2Csku2%3A3'
      const thunk = actions.getStoresForCheckoutModal()
      thunk(dispatch, getState)

      expect(getBasketDetails).toHaveBeenCalledTimes(1)
      expect(getBasketDetails).toHaveBeenCalledWith({})
      expect(get).toBeCalledWith(expectedUrl)
    })
  })

  describe('@getStores', () => {
    const getState = jest.fn().mockReturnValue({
      features: {
        status: {
          FEATURE_CFSI: true,
        },
      },
      config: {
        siteId: 42,
      },
      routing: {
        location: {
          query: {
            types: 'x,y',
          },
        },
      },
    })
    const dispatch = jest.fn((fn) => Promise.resolve(fn))

    beforeAll(() => {
      jest.spyOn(actions, 'getStores')
    })

    it('calls applyQueryFilters', () => {
      const search = {
        search: 'search',
      }
      const action = actions.getStores(search)

      action(dispatch, getState)

      expect(dispatch).toHaveBeenCalledTimes(2)

      dispatch.mock.calls[0][0](dispatch, getState)

      expect(dispatch).toHaveBeenCalledWith({
        type: 'APPLY_FILTERS',
        filters: ['x', 'y'],
      })
    })

    it('returns if no search', () => {
      const search = { search: false }
      const action = actions.getStores(search)

      action(dispatch, getState)

      expect(dispatch).toHaveBeenCalledTimes(0)
    })
  })

  describe('@getRecentStores', () => {
    beforeEach(() => {
      jest.spyOn(actions, 'getStores')
      actions.getStores.mockImplementation(
        jest.fn(() => {
          return {
            body: [{}],
          }
        })
      )
    })

    it('does not call getStores', () => {
      const search = { search: false }
      const store = configureMockStore()

      store.dispatch(actions.getRecentStores(search))

      expect(actions.getStores).toHaveBeenCalledTimes(0)
    })

    it('calls getStores', () => {
      const search = { search: 'test' }
      const store = configureMockStore()

      expect(actions.getStores).toHaveBeenCalledTimes(0)

      store.dispatch(actions.getRecentStores(search))

      expect(actions.getStores).toHaveBeenCalledTimes(1)
    })
  })

  describe('@getFulfilmentStore', () => {
    let dispatch

    beforeAll(() => {
      dispatch = jest.fn((x) => {
        return Promise.resolve(x)
      })

      actions.getStores.mockReturnValue({
        body: [
          {
            storeId: 'TS0001',
          },
        ],
      })

      actions.setFulfilmentStore = jest.fn()
    })

    it('does not call getStores', () => {
      const search = { search: false }
      const action = actions.getFulfilmentStore(search)

      expect(actions.getStores).toHaveBeenCalledTimes(0)

      action(dispatch)

      expect(actions.getStores).toHaveBeenCalledTimes(0)
      expect(dispatch).toHaveBeenCalledTimes(0)
    })

    it('calls getStores', () => {
      const search = { search: 'test' }
      const action = actions.getFulfilmentStore(search)

      expect.assertions(3)
      expect(actions.getStores).toHaveBeenCalledTimes(0)

      return action(dispatch).then(() => {
        expect(actions.getStores).toHaveBeenCalledTimes(1)
        expect(dispatch).toHaveBeenCalledTimes(2)
      })
    })

    it('calls setFulfilmentStore when getstores return body', () => {
      const search = { search: 'test' }
      const action = actions.getFulfilmentStore(search)

      expect.assertions(4)
      expect(actions.setFulfilmentStore).not.toHaveBeenCalled()

      return action(dispatch).then(() => {
        expect(actions.setFulfilmentStore).toHaveBeenCalledTimes(1)
        expect(actions.setFulfilmentStore).toBeCalledWith({ storeId: 'TS0001' })
        expect(dispatch).toHaveBeenCalledTimes(2)
      })
    })

    it('does not call setFulfilmentStore if no stores array', () => {
      const search = { search: 'test' }
      const action = actions.getFulfilmentStore(search)

      actions.getStores.mockReturnValue({
        body: 'fail',
      })

      expect.assertions(2)
      expect(actions.setFulfilmentStore).not.toHaveBeenCalled()

      return action(dispatch).then(() => {
        expect(actions.setFulfilmentStore).toHaveBeenCalledTimes(0)
      })
    })

    it('calls setFulfilmentStoreSKU when getFulfilmentStore is called with isFFS as true and with a sku', () => {
      const search = { search: 'test' }
      const isFFS = true
      const sku = '90210'
      const action = actions.getFulfilmentStore(search, isFFS, sku)

      actions.getStores.mockReturnValue({
        body: [{ storeId: 'TS0001' }],
      })

      expect.assertions(2)

      return action(dispatch).then(() => {
        expect(dispatch).toHaveBeenCalledTimes(3)
        expect(dispatch).toHaveBeenCalledWith({
          type: 'SET_FULFILMENT_STORE_SKU',
          sku,
        })
      })
    })

    it('calls getStoresError when get stores returns a promise rejecttion', () => {
      actions.getStores.mockReturnValue(Promise.reject('error'))

      const search = { search: 'test' }
      const action = actions.getFulfilmentStore(search)

      expect.assertions(2)

      return action(dispatch).then(() => {
        expect(dispatch).toHaveBeenCalledTimes(2)
        expect(dispatch).toHaveBeenCalledWith({
          type: 'GET_STORES_ERROR',
          error: 'error',
        })
      })
    })
  })

  describe('@storeSearch', () => {
    beforeAll(() => {
      jest.spyOn(actions, 'getStores')
      actions.getStores.mockImplementation(
        jest.fn(() => {
          return {
            body: [{}],
          }
        })
      )
    })

    afterAll(() => {
      jest.clearAllMocks()
    })

    it('does not call getStores', () => {
      const search = { search: false }
      const store = configureMockStore()

      store.dispatch(actions.storeSearch(search))

      expect(actions.getStores).toHaveBeenCalledTimes(0)
    })

    it('calls getStores', () => {
      const search = { search: 'test' }
      const store = configureMockStore()

      expect(actions.getStores).toHaveBeenCalledTimes(0)

      store.dispatch(actions.storeSearch(search))

      expect(actions.getStores).toHaveBeenCalledTimes(1)
    })
  })

  describe('getDeliveryStoreDetails', () => {
    const storeId = 'fake-id'
    const state = {
      selectedBrandFulfilmentStore: {},
      shoppingBag: {
        bag: 'fake-shopping-bag',
      },
      features: {
        status: {
          FEATURE_CFSI: true,
        },
      },
    }
    const getState = jest.fn()
    const dispatch = jest.fn((fn) => Promise.resolve(fn))

    beforeAll(() => {
      jest.spyOn(actions, 'getStores')
    })

    beforeEach(() => {
      getState.mockReturnValue(state)
      actions.getStores.mockReturnValue(
        Promise.resolve({
          body: [{}],
        })
      )
    })

    it('should call shouldFetchBagStock', () => {
      shouldFetchBagStock.mockReturnValueOnce(false)

      const action = actions.getDeliveryStoreDetails(storeId)

      action(dispatch, getState)

      expect(shouldFetchBagStock).toHaveBeenCalledTimes(1)
      expect(shouldFetchBagStock).toHaveBeenLastCalledWith(
        state.shoppingBag.bag,
        state.selectedBrandFulfilmentStore,
        storeId
      )
    })

    it('should call getItem("WC_pickUpStore") if storeId is undefined and should call shouldFetchBagStock with new storeId', () => {
      shouldFetchBagStock.mockReturnValueOnce(false)

      const action = actions.getDeliveryStoreDetails()

      action(dispatch, getState)

      expect(getItem).toHaveBeenCalledTimes(1)
      expect(shouldFetchBagStock).toHaveBeenLastCalledWith(
        state.shoppingBag.bag,
        state.selectedBrandFulfilmentStore,
        'TS0001'
      )
    })

    it('should not dispatch if shouldFetchBagStock return false', () => {
      shouldFetchBagStock.mockReturnValueOnce(false)

      const action = actions.getDeliveryStoreDetails(storeId)

      action(dispatch, getState)

      expect(dispatch).not.toHaveBeenCalled()
    })

    it('should not dispatch if cfsi feature is off', () => {
      const newState = {
        ...state,
        features: {
          status: {
            FEATURE_CFSI: false,
          },
        },
      }
      getState.mockReturnValue(newState)

      const action = actions.getDeliveryStoreDetails(storeId)

      action(dispatch, getState)

      expect(dispatch).not.toHaveBeenCalled()
    })

    it('should dispatch and call getStores if cfsi is on and shouldFetchBagStock is true', () => {
      const action = actions.getDeliveryStoreDetails(storeId)

      action(dispatch, getState)

      expect(actions.getStores).toHaveBeenCalledTimes(1)
      expect(actions.getStores).toHaveBeenLastCalledWith({
        search: `?storeIds=${storeId}&skuList=${getSkuList(
          state.shoppingBag.bag
        )}`,
      })
    })

    it('should dispatch setSelectedBrandFulfilmentStore if getStores resolves with single store array', () => {
      const returnedStore = { storeId: 'TS0001' }
      actions.getStores.mockReturnValueOnce(
        Promise.resolve({
          body: [returnedStore],
        })
      )
      const action = actions.getDeliveryStoreDetails(storeId)

      expect.assertions(3)

      action(dispatch, getState).then(() => {
        expect(dispatch).toHaveBeenCalledTimes(2)
        expect(setSelectedBrandFulfilmentStore).toHaveBeenCalledTimes(1)
        expect(setSelectedBrandFulfilmentStore).toHaveBeenLastCalledWith(
          returnedStore
        )
      })
    })

    it('should not dispatch setSelectedBrandFulfilmentStore if getStores resolves with empty array', () => {
      actions.getStores.mockReturnValue(
        Promise.resolve({
          body: [],
        })
      )

      const action = actions.getDeliveryStoreDetails(storeId)

      expect.assertions(3)

      action(dispatch, getState).then(() => {
        expect(dispatch).toHaveBeenCalledTimes(1)
        expect(dispatch.mock.calls[0][0]).not.toMatchObject({
          type: 'GET_STORES_ERROR',
        })
        expect(setSelectedBrandFulfilmentStore).not.toHaveBeenCalled()
      })
    })

    it('should not dispatch setSelectedBrandFulfilmentStore if getStores resolves with non array', () => {
      actions.getStores.mockReturnValueOnce(
        Promise.resolve({
          body: {},
        })
      )

      const action = actions.getDeliveryStoreDetails(storeId)

      expect.assertions(3)

      action(dispatch, getState).then(() => {
        expect(dispatch).toHaveBeenCalledTimes(1)
        expect(dispatch.mock.calls[0][0]).not.toMatchObject({
          type: 'GET_STORES_ERROR',
        })
        expect(setSelectedBrandFulfilmentStore).not.toHaveBeenCalled()
      })
    })

    it('should not dispatch setSelectedBrandFulfilmentStore if getStores resolves with ', () => {
      const error = 'error message'

      actions.getStores.mockReturnValueOnce(Promise.reject(error))

      const action = actions.getDeliveryStoreDetails(storeId)

      expect.assertions(3)

      action(dispatch, getState).then(() => {
        expect(dispatch).toHaveBeenCalledTimes(2)
        expect(dispatch.mock.calls[1][0]).toMatchObject({
          type: 'GET_STORES_ERROR',
          error,
        })
        expect(setSelectedBrandFulfilmentStore).not.toHaveBeenCalled()
      })
    })
  })

  describe('findStore', () => {
    const dispatch = jest.fn((promise) => promise)
    const getState = jest.fn()
    beforeEach(() => {
      jest.clearAllMocks()
    })
    it('should dispatch getStoresError if there is an error with get', (done) => {
      const state = {
        storeLocator: {
          stores: [],
        },
        features: {
          status: {},
        },
      }
      getState.mockReturnValue(state)
      get.mockImplementation(() => {
        return Promise.reject({})
      })
      actions.findStore({
        address2: 'no match 1',
        address3: 'no match 2',
        address4: 'no match city',
        address5: 'no match postcode',
        country: 'no match country',
      })(dispatch, getState)
      expect(dispatch).toHaveBeenCalled()
      expect(dispatch.mock.calls[0][0].name).toBe('removeMarkersThunk')
      expect(dispatch.mock.calls[1][0].name).toBe('removeLabelsThunk')
      expect(dispatch.mock.calls[2][0]).toEqual({
        type: 'AJAXCOUNTER_INCREMENT',
      })
      expect(dispatch.mock.calls[3][0]).toEqual({
        type: 'AJAXCOUNTER_DECREMENT',
      })
      expect(dispatch.mock.calls[4][0]).toEqual({
        type: 'GET_STORES_LOADING',
        loading: true,
      })
      expect(dispatch.mock.calls[5][0]).toEqual({
        type: 'AJAXCOUNTER_INCREMENT',
      })
      dispatch.mock.calls[6][0].then(null, () => {
        expect(dispatch.mock.calls[7][0]).toEqual({
          type: 'AJAXCOUNTER_DECREMENT',
        })
        expect(dispatch.mock.calls[8][0]).toEqual({
          type: 'GET_STORES_ERROR',
          error: {},
        })
        expect(dispatch.mock.calls[9][0]).toEqual({
          type: 'GET_STORES_LOADING',
          loading: false,
        })
        expect(dispatch.mock.calls[10][0]).toEqual({ type: 'NO_STORES_FOUND' })
        done()
      })
    })
    it('should dispatch noStoresFound if address does not match store from store-locator', (done) => {
      const state = {
        storeLocator: {
          stores: [],
        },
        features: {
          status: {},
        },
      }
      const address = {
        line1: 'fake line 1',
        line2: 'fake line 2',
        city: 'fake city',
        postcode: 'fake postcode',
      }
      getState.mockReturnValue(state)
      get.mockImplementation(() => {
        return Promise.resolve({
          body: [{ address }],
        })
      })
      actions.findStore({
        address2: 'no match 1',
        address3: 'no match 2',
        address4: 'no match city',
        address5: 'no match postcode',
        country: 'no match country',
      })(dispatch, getState)
      expect(dispatch).toHaveBeenCalled()
      expect(dispatch.mock.calls[0][0].name).toBe('removeMarkersThunk')
      expect(dispatch.mock.calls[1][0].name).toBe('removeLabelsThunk')
      expect(dispatch.mock.calls[2][0]).toEqual({
        type: 'AJAXCOUNTER_INCREMENT',
      })
      expect(dispatch.mock.calls[3][0]).toEqual({
        type: 'AJAXCOUNTER_DECREMENT',
      })
      expect(dispatch.mock.calls[4][0]).toEqual({
        type: 'GET_STORES_LOADING',
        loading: true,
      })
      expect(dispatch.mock.calls[5][0]).toEqual({
        type: 'AJAXCOUNTER_INCREMENT',
      })
      dispatch.mock.calls[6][0].then(() => {
        expect(dispatch.mock.calls[7][0]).toEqual({
          type: 'AJAXCOUNTER_DECREMENT',
        })
        expect(dispatch.mock.calls[8][0]).toEqual({ type: 'NO_STORES_FOUND' })
        done()
      })
    })
    it('should dispatch selectStoreByStore if address matches store from store-locator', (done) => {
      const state = {
        storeLocator: {
          stores: [],
        },
        features: {
          status: {},
        },
      }
      const address = {
        line1: 'fake line 1',
        line2: 'fake line 2',
        city: 'fake city',
        postcode: 'fake postcode',
      }
      getState.mockReturnValue(state)
      get.mockImplementation(() => {
        return Promise.resolve({
          body: [{ address }],
        })
      })
      actions.findStore({
        address2: 'fake line 1',
        address3: 'fake line 2',
        address4: 'fake city',
        address5: 'fake postcode',
        country: 'fake country',
      })(dispatch, getState)
      expect(dispatch).toHaveBeenCalled()
      expect(dispatch.mock.calls[0][0].name).toBe('removeMarkersThunk')
      expect(dispatch.mock.calls[1][0].name).toBe('removeLabelsThunk')
      expect(dispatch.mock.calls[2][0]).toEqual({
        type: 'AJAXCOUNTER_INCREMENT',
      })
      expect(dispatch.mock.calls[3][0]).toEqual({
        type: 'AJAXCOUNTER_DECREMENT',
      })
      expect(dispatch.mock.calls[4][0]).toEqual({
        type: 'GET_STORES_LOADING',
        loading: true,
      })
      expect(dispatch.mock.calls[5][0]).toEqual({
        type: 'AJAXCOUNTER_INCREMENT',
      })
      dispatch.mock.calls[6][0].then(() => {
        expect(dispatch.mock.calls[7][0]).toEqual({
          type: 'AJAXCOUNTER_DECREMENT',
        })
        expect(dispatch.mock.calls[8][0].name).toEqual(
          'selectStoreByStoreThunk'
        )
        expect(dispatch.mock.calls[9][0]).toEqual({
          type: 'RECEIVE_STORES',
          stores: [{ address }],
        })
        expect(dispatch.mock.calls[10][0]).toEqual({
          type: 'GET_STORES_LOADING',
          loading: false,
        })
        done()
      })
    })
  })

  describe('setMarkerIcon', () => {
    const dispatchMock = jest.fn()
    const getStateMock = () => ({
      storeLocator: {
        stores: [
          {
            brandName: 'Topshop',
            storeId: 'TS0001',
          },
          {
            brandName: 'Hermes',
            storeId: 'S08313',
          },
        ],
      },
      config: {
        brandName: 'topshop',
        logoVersion: '2',
      },
    })

    it('should set store markers for Arcadia stores', () => {
      const savedWindow = global.window
      global.window.markers = [{ setIcon: jest.fn() }]
      global.window.google.maps = {
        Size: jest.fn(),
        Point: jest.fn(),
      }

      actions.setMarkerIcon(0)(dispatchMock, getStateMock)
      expect(global.window.markers[0].setIcon).toHaveBeenCalledWith({
        anchor: {},
        scaledSize: {},
        size: {},
        url: '/assets/topshop/images/store-marker-icon.svg?version=2',
      })
      global.window = savedWindow
    })

    it('should set store markers for Hermes stores', () => {
      const savedWindow = global.window
      global.window.markers = [{}, { setIcon: jest.fn() }]
      global.window.google.maps = {
        Size: jest.fn(),
        Point: jest.fn(),
      }

      actions.setMarkerIcon(1)(dispatchMock, getStateMock)
      expect(global.window.markers[1].setIcon).toHaveBeenCalledWith({
        anchor: {},
        scaledSize: {},
        size: {},
        url: '/assets/topshop/images/parcelshop-marker-icon.svg?version=2',
      })
      global.window = savedWindow
    })
  })
})
