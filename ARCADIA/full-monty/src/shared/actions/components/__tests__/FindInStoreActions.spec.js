import * as actions from '../FindInStoreActions'
import configureMockStore from '../../../../../test/unit/lib/configure-mock-store'

const snapshot = (action) => expect(action).toMatchSnapshot()
describe('Find in Store actions', () => {
  it('updateFindInStoreActiveItem', () => {
    const store = configureMockStore({})
    const activeItem = { id: 1, quantity: 1 }
    store.dispatch(actions.updateFindInStoreActiveItem(activeItem))
    expect(store.getState().findInStore.activeItem).toEqual(activeItem)
  })
  it("updateFindInStoreActiveItem - doesn't dispatch if no quantity", () => {
    const store = configureMockStore({})
    const activeItem = { id: 1 }
    store.dispatch(actions.updateFindInStoreActiveItem(activeItem))
    expect(store.getState().findInStore.activeItem).toEqual({})
  })

  it('setStoreStockList', () => {
    snapshot(actions.setStoreStockList(true))
  })

  it('setStoreStockListProps', () => {
    snapshot(actions.setStoreStockListProps({}))
  })
})
