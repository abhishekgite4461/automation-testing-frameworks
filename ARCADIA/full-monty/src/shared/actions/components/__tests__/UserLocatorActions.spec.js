import * as actions from '../UserLocatorActions'
import configureMockStore from '../../../../../test/unit/lib/configure-mock-store'
import { maps } from '../../../../../test/mocks/google'
import { browserHistory } from 'react-router'
import { setStoreLocatorQuery } from '../StoreLocatorActions'

jest.mock('react-router', () => ({ browserHistory: { push: jest.fn() } }))

jest.mock('../StoreLocatorActions', () => ({
  setStoreLocatorQuery: jest.fn(() => {
    return {
      type: 'SET_STORE_LOCATOR_QUERY',
    }
  }),
}))

if (!global.google) {
  global.google = {}
}
global.google.maps = maps

const snapshot = (action) => expect(action).toMatchSnapshot()

const getStore = () => ({
  userLocator: {
    selectedPlaceDetails: {
      place_id: {
        placeId: 'id',
      },
    },
  },
  checkout: {
    orderSummary: {
      basket: {
        inventoryPositions: {
          item_1: {
            partNumber: '12345',
            catentry: '987',
            inventory: [{ cutofftime: '2100', quantity: 28 }],
          },
        },
        products: [
          {
            productId: 12345,
            catEntryId: 987,
            quantity: 1,
            sku: '12347',
          },
        ],
      },
    },
  },
  features: {
    status: {
      FEATURE_CFSI: false,
    },
  },
  config: {
    siteId: 42,
  },
})

describe('User locator actions', () => {
  it('fillRecentStores', () => {
    snapshot(actions.fillRecentStores(['Topshop, Oxford Street']))
  })

  it('completeRecentStores', () => {
    snapshot(actions.completeRecentStores(['Topshop, Oxford Street']))
  })

  it('resetRecentStores', () => {
    snapshot(actions.resetRecentStores())
  })

  it('searchStores', () => {
    snapshot(actions.searchStores(false))
  })
})

describe('@fillRecentStores', () => {
  it('sets recent stores', () => {
    const store = configureMockStore({
      userLocator: {
        recentStores: [],
      },
    })
    expect(store.getState().userLocator.recentStores).toEqual([])
    store.dispatch(actions.fillRecentStores(['recent store']))
    expect(store.getState().userLocator.recentStores).toEqual(['recent store'])
  })
})

describe('@resetRecentStores', () => {
  it('set recent stores to empty array', () => {
    const store = configureMockStore({
      userLocator: {
        recentStores: ['test store'],
      },
    })
    expect(store.getState().userLocator.recentStores).toEqual(['test store'])
    store.dispatch(actions.resetRecentStores())
    expect(store.getState().userLocator.recentStores).toEqual([])
  })
})

describe('@completeRecentStores', () => {
  beforeAll(() => {
    jest.spyOn(actions, 'fillRecentStores')
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('calls fillRecentStores', () => {
    const store = configureMockStore({
      config: {
        brandName: 'brandName',
      },
    })
    const searchTerm = [
      {
        name: 'name',
        brandName: 'brandName',
        address: {
          postcode: 'postcode',
        },
      },
    ]
    actions.fillRecentStores.mockReturnValue({
      type: 'FILL_RECENT_STORES',
    })

    store.dispatch(actions.completeRecentStores(searchTerm))

    expect(actions.fillRecentStores).toHaveBeenCalledTimes(1)
  })

  it('does not call fillRecentStores if 400 returned', () => {
    const store = configureMockStore({
      config: {
        brandName: 'brandName',
      },
    })
    const searchTerm = [
      {
        name: 'name',
        brandName: 'brandName',
        address: {
          postcode: 'postcode',
        },
      },
    ]
    actions.fillRecentStores.mockReturnValue({
      type: 'FILL_RECENT_STORES',
    })

    global.google.maps.places.AutocompleteService = () => {
      return {
        getPlacePredictions: (obj, func) => {
          func(
            [
              {
                description: '',
                storeId: '',
              },
            ],
            400
          )
        },
      }
    }

    store.dispatch(actions.completeRecentStores(searchTerm))

    expect(actions.fillRecentStores).toHaveBeenCalledTimes(0)
  })
})

describe('@autocomplete', () => {
  beforeAll(() => {
    jest.spyOn(actions, 'fillPredictions')
    jest.spyOn(actions, 'resetPredictions')
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('calls fillPredictions', () => {
    const store = configureMockStore()
    const searchTerm = 'test'

    actions.fillPredictions.mockReturnValue({
      type: 'FILL_PREDICTIONS',
    })

    global.google.maps.places.AutocompleteService = () => {
      return {
        getPlacePredictions: (obj, func) => {
          func(
            [
              {
                description: '',
                storeId: '',
              },
            ],
            200
          )
        },
      }
    }

    store.dispatch(actions.autocomplete(searchTerm))

    expect(actions.fillPredictions).toHaveBeenCalledTimes(1)
  })

  it('calls resetPredictions', () => {
    const store = configureMockStore()
    const searchTerm = ''

    actions.fillPredictions.mockReturnValue({
      type: 'RESET_PREDICTIONS',
    })

    store.dispatch(actions.autocomplete(searchTerm))

    expect(actions.resetPredictions).toHaveBeenCalledTimes(1)
  })
})

describe('@searchStoresCheckout', () => {
  beforeEach(() => {
    jest.spyOn(actions, 'getPlaceCoordinates')
  })

  afterEach(() => {
    jest.clearAllMocks()
    actions.getPlaceCoordinates.mockRestore()
  })

  it('calls setStoreLocatorQuery', () => {
    const store = configureMockStore({
      userLocator: {
        selectedPlaceDetails: {
          place_id: {
            placeId: 'id',
          },
        },
      },
      features: {
        status: {
          FEATURE_CFSI: true,
        },
      },
      config: {
        siteId: 42,
      },
    })

    expect(setStoreLocatorQuery).toHaveBeenCalledTimes(0)

    store.dispatch(actions.searchStoresCheckout())

    expect(setStoreLocatorQuery).toHaveBeenCalledTimes(1)
  })

  it('calls setStoreLocatorQuery with cfsi false', () => {
    const store = configureMockStore(getStore())
    const expectedQuery = {
      pathname: '/checkout/delivery/collect-from-store',
      query: {
        brand: 42,
        deliverToStore: true,
        latitude: undefined,
        longitude: undefined,
        basketDetails: '',
        types: 'today,brand,parcel,other',
      },
    }

    store.dispatch(actions.searchStoresCheckout())

    expect(browserHistory.push).toBeCalledWith(expectedQuery)
  })

  it('calls setStoreLocatorQuery with cfsi true', () => {
    const storeOptions = getStore()
    storeOptions.features.status.FEATURE_CFSI = true
    const store = configureMockStore(storeOptions)

    const expectedQuery = {
      pathname: '/checkout/delivery/collect-from-store',
      query: {
        brandPrimaryEStoreId: 42,
        deliverToStore: true,
        latitude: undefined,
        longitude: undefined,
        basketDetails: '12347:1',
        types: 'today,brand,parcel,other',
      },
    }

    store.dispatch(actions.searchStoresCheckout())

    expect(browserHistory.push).toBeCalledWith(expectedQuery)
  })

  it('calls getPlaceCoordinates', () => {
    const store = configureMockStore()

    expect(actions.getPlaceCoordinates).toHaveBeenCalledTimes(0)

    store.dispatch(actions.searchStoresCheckout())

    expect(actions.getPlaceCoordinates).toHaveBeenCalledTimes(1)
  })
})

describe('@searchStockInStoresBySelectedPlace', () => {
  beforeAll(() => {
    jest.clearAllMocks()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('calls getPlaceCoordinates', () => {
    const store = configureMockStore()

    jest.spyOn(actions, 'getPlaceCoordinates')

    expect(actions.getPlaceCoordinates).toHaveBeenCalledTimes(0)

    store.dispatch(actions.searchStores('type'))

    expect(actions.getPlaceCoordinates).toHaveBeenCalledTimes(1)
  })
})
