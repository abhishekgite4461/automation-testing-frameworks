import { browserHistory } from 'react-router'

import * as sortSelectorActions from '../sortSelectorActions'
import * as productsActions from '../../common/productsActions'
import { analyticsPlpClickEvent } from '../../../analytics/tracking/site-interactions'

jest.mock('../../../analytics/tracking/site-interactions', () => ({
  analyticsPlpClickEvent: jest.fn(),
}))

describe('Sort Selector Actions', () => {
  describe('selectSortOption', () => {
    const getState = () => ({
      sorting: {
        currentSortOption: 'Newness',
      },
      routing: {
        location: {
          pathname: '/search/',
          query: {
            q: 'jeans',
          },
        },
      },
    })
    const browserHistorySpy = jest
      .spyOn(browserHistory, 'replace')
      .mockImplementation(() => {})
    const updateCurrentProductsSpy = jest.spyOn(
      productsActions,
      'updateCurrentProducts'
    )

    afterEach(() => {
      jest.resetAllMocks()
    })

    afterAll(() => {
      jest.restoreAllMocks()
    })

    it('should do nothing if already sorting by the sort option', () => {
      const dispatchMock = jest.fn()
      const getState = () => ({
        sorting: {
          currentSortOption: 'Newness',
        },
      })
      const action = sortSelectorActions.selectSortOption('Newness')
      action(dispatchMock, getState)
      expect(dispatchMock).not.toHaveBeenCalled()
    })

    it('should sort by option if it’s changed', () => {
      const dispatchMock = jest.fn()
      const mockOption = 'Relevance'
      const action = sortSelectorActions.selectSortOption('Relevance')
      action(dispatchMock, getState)
      expect(analyticsPlpClickEvent).toHaveBeenCalledWith(
        `sortoption-${mockOption.toLowerCase()}`
      )
      expect(dispatchMock).toHaveBeenCalledWith({
        type: 'SELECT_SORT_OPTION',
        option: 'Relevance',
      })
    })

    it('should add sort option to the URL', () => {
      const dispatchMock = jest.fn()
      const action = sortSelectorActions.selectSortOption('Price Ascending')
      action(dispatchMock, getState)
      expect(browserHistorySpy).toHaveBeenCalledWith({
        pathname: '/search/',
        query: {
          q: 'jeans',
          sort: 'Price%20Ascending',
        },
      })
    })

    it('should update current products', () => {
      const updateCurrentProductsAction = {
        type: 'UPDATE_CURRENT_PRODUCTS',
      }
      updateCurrentProductsSpy.mockImplementation(
        () => updateCurrentProductsAction
      )
      const dispatchMock = jest.fn()
      const action = sortSelectorActions.selectSortOption('Relevance')
      action(dispatchMock, getState)
      expect(dispatchMock).toHaveBeenCalledWith(updateCurrentProductsAction)
    })
  })
})
