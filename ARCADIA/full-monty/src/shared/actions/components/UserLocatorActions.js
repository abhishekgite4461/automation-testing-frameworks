import { browserHistory } from 'react-router'
import { closeTopNavMenu } from '../../actions/components/TopNavMenuActions'
import { setFormField } from '../../actions/common/formActions'
import { setGenericError } from '../common/errorMessageActions'
import { localise } from '../../lib/localisation'
import {
  setStoreStockList,
  setStoreStockListProps,
} from '../../actions/components/FindInStoreActions'
import { setStoreLocatorQuery } from './StoreLocatorActions'
import {
  getAppliedFilters,
  getBasketDetails,
} from '../../lib/store-locator-utilities'
import { path } from 'ramda'

let sessionToken = null

function mapSession() {
  if (!sessionToken) {
    sessionToken = new window.google.maps.places.AutocompleteSessionToken()
  }
}

export function resetSearchTerm() {
  return {
    type: 'RESET_SEARCH_TERM',
  }
}

export function fillPredictions(searchTerm, predictions) {
  return {
    type: 'FILL_PREDICTIONS',
    searchTerm,
    predictions,
  }
}

export function resetPredictions() {
  return {
    type: 'RESET_PREDICTIONS',
  }
}

export function setSelectedPlace(selectedPlaceDetails) {
  return {
    type: 'SET_SELECTED_PLACE',
    selectedPlaceDetails,
  }
}

export function resetSelectedPlace() {
  return {
    type: 'RESET_SELECTED_PLACE',
  }
}

function navigateToStoreLocator({ country, latitude, longitude, sku, region }) {
  return (dispatch) => {
    browserHistory.push({
      pathname: '/store-locator',
      query: { country, latitude, longitude, sku, region },
    })
    dispatch(closeTopNavMenu())
  }
}

export function getPlaceCoordinates(placeId, callback) {
  const service = new window.google.maps.places.PlacesService(
    document.createElement('div')
  )
  mapSession()
  service.getDetails(
    {
      placeId,
      sessionToken,
      fields: ['geometry'],
    },
    (predictions, status) => {
      sessionToken = null
      if (status !== window.google.maps.places.PlacesServiceStatus.OK) {
        return (dispatch, getState) => {
          const { language, brandName } = getState().config
          const l = localise.bind(null, language, brandName)
          dispatch(
            setGenericError(`${l`Error with googleGetDetails`}: ${status}`)
          )
        }
      }
      const latitude = predictions.geometry.location.lat()
      const longitude = predictions.geometry.location.lng()
      return callback({ latitude, longitude })
    }
  )
}

function searchStoresBySelectedPlace() {
  return (dispatch, getState) => {
    const {
      selectedPlaceDetails: { place_id: placeId },
    } = getState().userLocator
    const { region } = getState().config
    getPlaceCoordinates(placeId, ({ latitude, longitude }) => {
      dispatch(navigateToStoreLocator({ latitude, longitude, region }))
    })
  }
}

function searchStockInStoresBySelectedPlace() {
  return (dispatch, getState) => {
    const {
      activeItem: { sku },
    } = getState().findInStore
    const {
      selectedPlaceDetails: { place_id: placeId },
    } = getState().userLocator
    const cfsi = getState().features.status.FEATURE_CFSI || false
    const brandParam = cfsi ? `brandPrimaryEStoreId` : `brand`

    exports.getPlaceCoordinates(placeId, ({ latitude, longitude }) => {
      const brand = getState().config.siteId
      const props = {
        location: {
          search: `?${brandParam}=${brand}&latitude=${latitude}&longitude=${longitude}&sku=${sku}`,
          query: {
            latitude,
            longitude,
            brand,
            sku,
          },
        },
      }
      dispatch(setStoreStockListProps(props))
      dispatch(setStoreStockList(true))
    })
  }
}

export function searchStoresCheckout() {
  return (dispatch, getState) => {
    const placeId = getState().userLocator.selectedPlaceDetails.place_id

    exports.getPlaceCoordinates(placeId, ({ latitude, longitude }) => {
      const filters = path(['storeLocator', 'filters'], getState()) || []
      const cfsi =
        path(['features', 'status', 'FEATURE_CFSI'], getState()) || false
      const basket =
        path(['checkout', 'orderSummary', 'basket'], getState()) || {}

      const brandParam = cfsi ? 'brandPrimaryEStoreId' : 'brand'
      const basketDetails = cfsi ? getBasketDetails(basket) : ''

      const query = {
        latitude,
        longitude,
        [brandParam]: getState().config.siteId,
        deliverToStore: true,
        types: getAppliedFilters(filters).join(','),
        basketDetails,
      }

      // @NOTE shouldnt the argument be passed to the action?
      dispatch(setStoreLocatorQuery)

      browserHistory.push({
        pathname: '/checkout/delivery/collect-from-store',
        query: {
          ...getState().routing.location.query,
          ...query,
        },
      })
    })
  }
}

export function searchStores(type) {
  return (dispatch, getState) => {
    if (type) {
      dispatch(searchStockInStoresBySelectedPlace())
    } else {
      const { selectedCountry } = getState().storeLocator
      if (selectedCountry === 'United Kingdom') {
        dispatch(searchStoresBySelectedPlace())
      } else {
        dispatch(navigateToStoreLocator({ country: selectedCountry }))
      }
    }
  }
}

export function fillRecentStores(recentStores) {
  return {
    type: 'FILL_RECENT_STORES',
    recentStores,
  }
}

export function resetRecentStores() {
  return {
    type: 'RESET_RECENT_STORES',
  }
}

export function autocomplete(searchTerm) {
  return (dispatch) => {
    const service = new window.google.maps.places.AutocompleteService()
    if (searchTerm.length >= 3) {
      mapSession()
      service.getPlacePredictions(
        {
          input: searchTerm,
          sessionToken,
          componentRestrictions: { country: 'uk' },
        },
        (predictions, status) => {
          if (status !== window.google.maps.places.PlacesServiceStatus.OK) {
            // error handling
            return
          }
          dispatch(exports.fillPredictions(searchTerm, predictions))
        }
      )
    } else if (searchTerm.length === 0) {
      dispatch(exports.resetPredictions())
    }
  }
}

export function completeRecentStores(searchTerm) {
  return (dispatch, getState) => {
    const service = new window.google.maps.places.AutocompleteService()
    const { brandName } = getState().config
    mapSession()
    searchTerm.forEach((term) => {
      if (
        term.brandName &&
        term.name &&
        term.brandName.toLowerCase() === brandName.toLowerCase()
      ) {
        service.getPlacePredictions(
          {
            sessionToken,
            input: `${term.address.postcode}`,
            componentRestrictions: { country: 'uk' },
          },
          (predictions, status) => {
            if (status !== window.google.maps.places.PlacesServiceStatus.OK) {
              // error handling
              return
            }
            predictions[0].description = `${term.brandName}, ${term.name}`
            predictions[0].storeId = term.storeId
            dispatch(exports.fillRecentStores(predictions))
          }
        )
      }
    })
  }
}

export function getCurrentLocationError() {
  return {
    type: 'GET_CURRENT_LOCATION_ERROR',
  }
}

export function setUserLocatorPending(pending) {
  return {
    type: 'SET_USER_LOCATOR_PENDING',
    pending,
  }
}

export function onGetCurrentPositionSuccess({
  coords: { latitude, longitude },
}) {
  return (dispatch) => {
    const geocoder = new window.google.maps.Geocoder()
    const latlng = { lat: parseFloat(latitude), lng: parseFloat(longitude) }
    geocoder.geocode({ location: latlng }, (results, status) => {
      dispatch(setUserLocatorPending(false))
      if (status === window.google.maps.GeocoderStatus.OK) {
        if (results.length > 0) {
          dispatch(setSelectedPlace(results[0]))
          dispatch(
            setFormField(
              'userLocator',
              'userLocation',
              results[0].description || results[0].formatted_address
            )
          )
        } else {
          // handle no results
        }
      } else {
        dispatch(getCurrentLocationError())
      }
    })
  }
}
