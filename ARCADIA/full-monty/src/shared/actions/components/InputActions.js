export const toggleShowPassword = (id) => ({ type: 'TOGGLE_SHOW_PASSWORD', id })
export const toggleInputActive = (id) => ({ type: 'TOGGLE_INPUT_ACTIVE', id })
