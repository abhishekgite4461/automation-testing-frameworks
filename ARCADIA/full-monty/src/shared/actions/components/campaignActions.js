import { get } from '../../lib/api-service'
import { initCarousel } from '../common/carousel-actions'
import consts from '../../constants/campaignConsts'

export function setCampaignSocialFeed(socialFeed, campaign) {
  return {
    type: 'SET_CAMPAIGN_SOCIAL_FEED',
    socialFeed,
    campaign,
  }
}

export function getCampaignSocialFeed(campaign) {
  return (dispatch) => {
    const url = consts.socialFeedUrls[campaign]
    return dispatch(get(url)).then(({ body }) => {
      dispatch(setCampaignSocialFeed(body, campaign))
      dispatch(initCarousel(campaign, body.length))
    })
  }
}

export function setCampaignBlogFeed(blogFeed, campaign) {
  return {
    type: 'SET_CAMPAIGN_BLOG_FEED',
    blogFeed,
    campaign,
  }
}

export function getCampaignBlogFeed(campaign, carouselName) {
  return (dispatch) => {
    const url = consts.blogFeedUrls[campaign]
    return dispatch(get(url)).then(({ body }) => {
      dispatch(setCampaignBlogFeed(body, campaign))
      if (carouselName) dispatch(initCarousel(carouselName, body.length))
    })
  }
}
