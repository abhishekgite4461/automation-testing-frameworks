import createReducer from '../../lib/create-reducer'

import { UPDATE_LOCATION } from 'react-router-redux'

function emptyBag() {
  return { products: [], orderId: 0 }
}

const recentlyAdded = (state = {}, recentlyAdded) => ({
  ...state,
  recentlyAdded: {
    products: [],
    quantity: 0,
    isMiniBagConfirmShown: false,
    ...state.recentlyAdded,
    ...recentlyAdded,
  },
})

const countItems = (products = []) =>
  products.reduce((acc, product) => acc + (product.quantity || 0), 0)

const initialState = {
  totalItems: 0,
  promotionCode: null,
  currentlyAddingToBag: false,
  miniBagOpen: false,
  autoClose: false,
  promotionCodeConfirmation: false,
  bag: emptyBag(),
  loadingShoppingBag: false,
  ...recentlyAdded(),
}

export default createReducer(initialState, {
  UPDATE_SHOPPING_BAG_BADGE_COUNT: (state, { count }) => ({
    ...state,
    totalItems: count,
  }),
  SET_PROMOTION_CODE: (state, { promotionCode }) => ({
    ...state,
    promotionCode,
  }),
  SET_CURRENTLY_ADDING_TO_BAG: (state, { val }) => ({
    ...state,
    currentlyAddingToBag: val,
  }),
  OPEN_MINI_BAG: (state, { autoClose }) => ({
    ...state,
    miniBagOpen: true,
    autoClose,
  }),
  CLOSE_MINI_BAG: (state) => ({ ...state, miniBagOpen: false }),
  SET_LOADING_SHOPPING_BAG: (state, { isLoading }) => ({
    ...state,
    loadingShoppingBag: isLoading,
  }),
  SET_PROMOTION_CODE_CONFIRMATION: (state, { promotionCodeConfirmation }) => ({
    ...state,
    promotionCodeConfirmation,
  }),
  UPDATE_BAG: (state, { bag }) => ({ ...state, bag }),
  UPDATE_SHOPPING_BAG_PRODUCT: (state, action) => ({
    ...state,
    bag: {
      ...state.bag,
      products: state.bag.products.map((product, index) => {
        return index === action.index
          ? {
              ...product,
              ...action.update,
            }
          : product
      }),
    },
  }),
  EMPTY_SHOPPING_BAG: (state) => ({
    ...state,
    ...recentlyAdded(),
    bag: emptyBag(),
    totalItems: 0,
  }),
  FETCH_ORDER_SUMMARY_SUCCESS: (state, { data: { basket } }) => ({
    ...state,
    ...(!basket && recentlyAdded()),
    bag: basket || emptyBag(),
    totalItems: basket ? countItems(basket.products) : 0,
  }),
  PRODUCTS_ADDED_TO_BAG: (state, { payload }) => recentlyAdded(state, payload),
  SHOW_MINIBAG_CONFIRM: (state, { payload }) =>
    recentlyAdded(state, { isMiniBagConfirmShown: payload }),
  UPDATE_ACTIVE_ITEM: (state) => recentlyAdded(state),
  PRE_CACHE_RESET: () => initialState,
  LOGOUT: (state) => ({ ...initialState, miniBagOpen: state.miniBagOpen }),
  [UPDATE_LOCATION]: (state) => ({ ...state, miniBagOpen: false }),
})
