import createReducer from '../../lib/create-reducer'

export default createReducer(
  { open: false, scrollToTop: false },
  {
    TOGGLE_TOP_NAV_MENU: (state) => ({ ...state, open: !state.open }),
    CLOSE_TOP_NAV_MENU: (state) => ({ ...state, open: false }),
    TOGGLE_SCROLL_TO_TOP: (state) => ({
      ...state,
      scrollToTop: !state.scrollToTop,
    }),
  }
)
