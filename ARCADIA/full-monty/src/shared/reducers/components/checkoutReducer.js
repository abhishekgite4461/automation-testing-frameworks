import createReducer from '../../lib/create-reducer'
import { assoc, assocPath, evolve, path } from 'ramda'

const initialState = {
  orderCompleted: {},
  orderSummary: {},
  orderSummaryError: {},
  orderError: false,
  useDeliveryAsBilling: true,
  verifyPayment: false,
  storeUpdating: false,
  deliveryAndPayment: {
    deliveryEditingEnabled: false,
  },
  savePaymentDetails: false,
  deliveryStore: undefined,
}

const setOrderSummaryField = (state, action) => {
  const transform = {
    orderSummary: {
      [action.field]: () => action.value,
    },
  }

  return evolve(transform, state)
}

const setOrderSummary = (state, payload) => {
  return {
    ...state,
    orderSummary: {
      ...state.orderSummary,
      ...payload,
    },
  }
}

const getSavedAddress = path([
  'DeliveryOptionsDetails',
  'deliveryoptionsform',
  'savedAddresses',
])

export default createReducer(initialState, {
  EMPTY_ORDER_SUMMARY: (state) => ({
    ...state,
    orderSummary: {},
  }),
  FETCH_ORDER_SUMMARY_SUCCESS: (state, { data }) => ({
    ...state,
    orderSummary: data,
    orderCompleted: {},
  }),
  SET_PARTIAL_ORDER_SUMMARY: (state, { data }) => ({
    ...state,
    partialOrderSummary: data,
  }),
  SET_ORDER_SUMMARY_FIELD: (state, action) =>
    setOrderSummaryField(state, action),
  SET_ORDER_SUMMARY_ERROR: (state, { data }) => ({
    ...state,
    orderSummaryError: data,
  }),
  // @NOTE SET_DELIVERY_AS_BILLING_FLAG is meant to replace SET_DELIVERY_ADDRESS_TO_BILLING
  SET_DELIVERY_AS_BILLING_FLAG: (state, { val }) => ({
    ...state,
    useDeliveryAsBilling: val,
  }),
  SET_ORDER_COMPLETED: (state, { data }) => ({
    ...state,
    orderCompleted: data,
  }),
  LOGOUT: () => ({ ...initialState }),
  SET_ORDER_PENDING: (state, { data }) => ({ ...state, verifyPayment: data }),
  SET_ORDER_ERROR: (state, { error }) => ({ ...state, orderError: error }),
  RESET_STORE_DETAILS: (state) => ({
    ...state,
    deliveryStore: undefined,
    orderSummary: {
      ...state.orderSummary,
      storeDetails: undefined,
    },
  }),
  SET_DELIVERY_STORE: (state, { store }) => ({
    ...state,
    deliveryStore: store,
  }),
  UPDATE_ORDER_SUMMARY_PRODUCT: (state, action) => ({
    ...state,
    orderSummary: {
      ...state.orderSummary,
      basket: {
        ...state.orderSummary.basket,
        products: state.orderSummary.basket.products.map((product, index) => {
          return index === action.index
            ? {
                ...product,
                ...action.update,
              }
            : product
        }),
      },
    },
  }),
  SET_STORE_UPDATING: (state, { updating }) => ({
    ...state,
    storeUpdating: updating,
  }),
  DELIVERY_AND_PAYMENT_SET_DELIVERY_EDITING_ENABLED: (state, { enabled }) => {
    return assocPath(
      ['deliveryAndPayment', 'deliveryEditingEnabled'],
      enabled,
      state
    )
  },
  SET_SAVE_PAYMENT_DETAILS_ENABLED: (state, { enabled }) => {
    return assoc('savePaymentDetails', enabled, state)
  },
  ADDRESS_BOOK_SELECT_ADDRESS_API_SUCCESS: (state, { payload }) =>
    setOrderSummary(state, payload),
  ADDRESS_BOOK_CREATE_ADDRESS_API_SUCCESS: (state, { payload }) =>
    setOrderSummary(state, payload),
  ADDRESS_BOOK_DELETE_ADDRESS_API_SUCCESS: (state, { payload }) => {
    return {
      ...state,
      orderSummary: {
        ...state.orderSummary,
        savedAddresses: getSavedAddress(payload),
      },
    }
  },
  REMOVE_UNCACHEABLE: (state) => ({ ...state, checkoutVersion: undefined }),
})
