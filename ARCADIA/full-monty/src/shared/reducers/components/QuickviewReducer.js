import createReducer from '../../lib/create-reducer'

export default createReducer(
  { product: {} },
  {
    SET_PRODUCT_QUICKVIEW: (state, { product }) => ({
      ...state,
      product,
    }),
    SET_PRODUCT_ID_QUICKVIEW: (state, { productId }) => ({
      ...state,
      productId,
    }),
  }
)
