import { omit, path } from 'ramda'
import { UPDATE_LOCATION } from 'react-router-redux'
import createReducer from '../../lib/create-reducer'
import { keyValueDecodeURI } from '../../lib/query-helper'
import { pushFilterUsedEvent } from '../../analytics/tracking/site-interactions'

const handleValueOptionUpdate = (allowMultiple) => (
  state,
  { refinement, option }
) => {
  // Get the current value, or default to an empty one
  const currentValue = state.selectedOptions[refinement] || []

  // Remove the option if we already had it, or add it if we didn't
  let newValue
  const filterUsedData = {
    filterOptionName: refinement,
    filterOptionValue: option,
    filterAction: '',
  }
  if (currentValue.includes(option)) {
    newValue = currentValue.filter((val) => val !== option)
    filterUsedData.filterAction = 'remove'
  } else {
    newValue = allowMultiple ? currentValue.concat(option) : [option]
    filterUsedData.filterAction = 'apply'
  }

  pushFilterUsedEvent(filterUsedData)

  // No options left? remove it entirely.
  if (!newValue.length) {
    return {
      ...state,
      selectedOptions: omit([refinement], state.selectedOptions),
    }
  }

  // Otherwise - set the new value
  return {
    ...state,
    selectedOptions: {
      ...state.selectedOptions,
      [refinement]: newValue,
    },
  }
}

const setRefinementOptions = (state, selectedOptions = {}) => {
  return {
    ...state,
    selectedOptions,
    appliedOptions: selectedOptions,
  }
}

export default createReducer(
  { isShown: false, selectedOptions: {}, appliedOptions: {} },
  {
    OPEN_REFINEMENTS: (state) => ({
      ...state,
      isShown: true,
      appliedOptions: state.selectedOptions,
    }),
    CLOSE_REFINEMENTS: (state) => ({
      ...state,
      isShown: false,
      selectedOptions: state.appliedOptions,
    }),
    APPLY_REFINEMENTS: (state, { seoUrl = '' }) => ({
      ...state,
      appliedOptions: state.selectedOptions,
      seoUrl,
    }),
    UPDATE_OPTION_RANGE: (state, { refinement, option }) => {
      // Simply when its changed keep the new one
      return {
        ...state,
        selectedOptions: {
          ...state.selectedOptions,
          [refinement]: [...option],
        },
      }
    },
    UPDATE_OPTION_VALUE: handleValueOptionUpdate(true),
    UPDATE_OPTION_SINGLE: handleValueOptionUpdate(false),
    REMOVE_OPTION_RANGE: (state, { refinement }) => {
      return {
        ...state,
        selectedOptions: omit([refinement], state.selectedOptions),
      }
    },
    CLEAR_REFINEMENT_OPTIONS: (state) => ({
      ...state,
      selectedOptions: {},
      previousOptions: null,
    }),
    SET_SEO_REFINEMENTS: (state, { refinements = [] }) => {
      const selectedOptions = refinements.reduce(
        (selected, refinement = {}) => {
          const { label, refinementOptions } = refinement
          if (!label || !refinementOptions) return selected

          const selectedValue = refinementOptions
            .filter(({ selectedFlag }) => selectedFlag)
            .map(({ value }) => value)

          return selectedValue.length
            ? { ...selected, [label.toLowerCase()]: selectedValue }
            : selected
        },
        { ...state.selectedOptions }
      )
      return {
        ...state,
        selectedOptions,
      }
    },
    UPDATE_LOCATION_SERVER: (
      state,
      {
        location: {
          query: { refinements = {} },
        },
      }
    ) => setRefinementOptions(state, keyValueDecodeURI(refinements)),
    [UPDATE_LOCATION]: (state, location) => {
      // { payload: { query: { refinements } = {} }, query }
      const refinements = path(['payload', 'query', 'refinements'], location)
      const sort = path(['payload', 'query', 'sort'], location)
      const currentPage = path(['payload', 'query', 'currentPage'], location)

      if (refinements) {
        return setRefinementOptions(state, keyValueDecodeURI(refinements))
      } else if (sort || currentPage) {
        return {
          ...state,
        }
      }
      return {
        ...state,
        selectedOptions: {},
        appliedOptions: {},
        previousOptions: null,
      }
    },
  }
)
