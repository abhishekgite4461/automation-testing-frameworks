import * as campaignUtils from '../../../lib/campaign-utilities'
import createReducer from '../../../lib/create-reducer'

export default function createCampaignReducer(campaignName, schema) {
  const initialState = campaignUtils.defaultSchema(schema)
  const baseReducers = {
    SET_CAMPAIGN_SOCIAL_FEED: campaignUtils.setCampaignSocialFeed(campaignName),
    SET_CAMPAIGN_BLOG_FEED: campaignUtils.setCampaignBlogFeed(campaignName),
  }
  return createReducer(initialState, { ...baseReducers })
}
