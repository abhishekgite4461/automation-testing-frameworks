import { combineReducers } from 'redux'
import createCampaignReducer from './createCampaignReducer'

// LONDON F.W 2016 CAMPAIGN !
export default combineReducers({
  lfw: createCampaignReducer('lfw', ['socialFeed', 'blogFeed']),
})
