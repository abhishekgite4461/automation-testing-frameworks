import createReducer from '../../lib/create-reducer'

export default createReducer(
  {
    searchTerm: '',
    predictions: [],
    selectedPlaceDetails: {},
    getCurrentLocationError: false,
    countries: [],
    pending: false,
  },
  {
    FILL_PREDICTIONS: (state, { searchTerm, predictions }) => ({
      ...state,
      searchTerm,
      predictions,
    }),
    RESET_PREDICTIONS: (state) => ({
      ...state,
      predictions: [],
    }),
    FILL_RECENT_STORES: (state, { recentStores }) => ({
      ...state,
      recentStores,
    }),
    RESET_RECENT_STORES: (state) => ({
      ...state,
      recentStores: [],
    }),
    SET_SELECTED_PLACE: (state, { selectedPlaceDetails }) => ({
      ...state,
      selectedPlaceDetails,
    }),
    RESET_SELECTED_PLACE: (state) => ({
      ...state,
      selectedPlaceDetails: {},
    }),
    GET_CURRENT_LOCATION_ERROR: (state) => ({
      ...state,
      getCurrentLocationError: true,
    }),
    RESET_SEARCH_TERM: (state) => ({
      ...state,
      searchTerm: '',
    }),
    SET_USER_LOCATOR_PENDING: (state, { pending }) => ({
      ...state,
      pending,
    }),
  }
)
