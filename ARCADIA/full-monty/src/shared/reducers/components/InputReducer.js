import createReducer from '../../lib/create-reducer'
import { path } from 'ramda'

export default createReducer(
  {},
  {
    TOGGLE_SHOW_PASSWORD: (state, { id }) => {
      const showPassword = !path([id, 'showPassword'], state)
      const idState = state[id]
        ? { ...state[id], showPassword }
        : { showPassword }
      return {
        ...state,
        [id]: idState,
      }
    },
    TOGGLE_INPUT_ACTIVE: (state, { id }) => {
      const isActive = !path([id, 'isActive'], state)
      const idState = state[id] ? { ...state[id], isActive } : { isActive }
      return {
        ...state,
        [id]: idState,
      }
    },
  }
)
