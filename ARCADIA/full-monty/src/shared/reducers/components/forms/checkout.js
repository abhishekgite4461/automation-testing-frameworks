import { combineReducers } from 'redux'
import createFormReducer from './createFormReducer'

export default combineReducers({
  account: createFormReducer('account', [
    'email',
    'password',
    'passwordConfirm',
    'subscribe',
  ]),
  billingAddress: createFormReducer('billingAddress', [
    'address1',
    'address2',
    'postcode',
    'city',
    'country',
    'county',
    'state',
  ]),
  billingCardDetails: createFormReducer('billingCardDetails', [
    'paymentType',
    'cardNumber',
    'expiryMonth',
    'expiryYear',
    'startMonth',
    'startYear',
    'cvv',
  ]),
  billingDetails: createFormReducer('billingDetails', [
    'title',
    'firstName',
    'lastName',
    'telephone',
  ]),
  billingFindAddress: createFormReducer('billingFindAddress', [
    'postCode',
    'houseNumber',
    'address',
    'message',
    'findAddress',
  ]),
  deliveryInstructions: createFormReducer('deliveryInstructions', [
    'deliveryInstructions',
    'smsMobileNumber',
  ]),
  findAddress: createFormReducer('findAddress', [
    'postCode',
    'houseNumber',
    'address',
    'message',
    'country',
    'findAddress',
    'selectAddress',
  ]),
  order: createFormReducer('order', ['isAcceptedTermsAndConditions']),
  yourAddress: createFormReducer('yourAddress', [
    'address1',
    'address2',
    'postcode',
    'city',
    'country',
    'county',
    'state',
  ]),
  yourDetails: createFormReducer('yourDetails', [
    'title',
    'firstName',
    'lastName',
    'telephone',
  ]),
})
