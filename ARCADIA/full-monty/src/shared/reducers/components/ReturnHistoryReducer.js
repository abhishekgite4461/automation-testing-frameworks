import createReducer from '../../lib/create-reducer'

const initialState = { returns: [] }

export default createReducer(initialState, {
  SET_RETURN_HISTORY_API_SUCCESS: (state, { payload: { orders } }) => ({
    ...state,
    returns: orders,
  }),
  SET_RETURN_HISTORY_API_FAILURE: (state) => ({ ...state, returns: {} }),
  SET_RETURN_HISTORY_DETAILS: (state, { returnDetails }) => ({
    ...state,
    returnDetails,
  }),
  SET_RETURN_HISTORY_DETAILS_API_SUCCESS: (state, { payload }) => ({
    ...state,
    returnDetails: payload,
  }),
  SET_RETURN_HISTORY_DETAILS_API_FAILURE: (state) => ({
    ...state,
    returnDetails: {},
  }),
  LOGOUT: () => initialState,
})
