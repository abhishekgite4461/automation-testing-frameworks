import createReducer from '../../lib/create-reducer'

const initialState = { orders: [], orderDetails: {} }

export default createReducer(initialState, {
  SET_ORDER_HISTORY_API_SUCCESS: (state, { payload: { orders } }) => ({
    ...state,
    orders,
  }),
  SET_ORDER_HISTORY_API_FAILURE: (state) => ({ ...state, orders: {} }),

  SET_ORDER_HISTORY_DETAILS: (state, { orderDetails }) => ({
    ...state,
    orderDetails,
  }),
  SET_ORDER_HISTORY_DETAILS_API_SUCCESS: (state, { payload }) => ({
    ...state,
    orderDetails: payload,
  }),
  SET_ORDER_HISTORY_DETAILS_API_FAILURE: (state) => ({ ...state, orders: {} }),

  LOGOUT: () => initialState,
})
