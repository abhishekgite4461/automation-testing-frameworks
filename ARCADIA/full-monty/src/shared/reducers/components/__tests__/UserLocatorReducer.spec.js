import testReducer from '../UserLocatorReducer'

const snapshot = (reducer) => expect(reducer).toMatchSnapshot()

describe('UserLocator Reducer', () => {
  describe('FILL_RECENT_STORES', () => {
    it('set array of stores', () => {
      snapshot(
        testReducer(
          {},
          {
            type: 'FILL_RECENT_STORES',
            recentStores: [{ storeId: 'TS0001' }],
          }
        )
      )
    })
  })
  describe('RESET_RECENT_STORES', () => {
    it('set recentStores to empty array', () => {
      snapshot(
        testReducer(
          {},
          {
            type: 'RESET_RECENT_STORES',
            recentStores: [],
          }
        )
      )
    })
  })
})
