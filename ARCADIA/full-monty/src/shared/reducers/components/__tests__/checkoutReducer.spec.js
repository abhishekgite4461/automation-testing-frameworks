import { path } from 'ramda'
import { createStore } from 'redux'
import nock from 'nock'
import configureStore from '../../../../shared/lib/configure-store'
import configureMockStore from 'test/unit/lib/configure-mock-store'

import checkoutReducer from '../checkoutReducer'
import {
  setDeliveryEditingEnabled,
  setStoreUpdating,
  setOrderSummaryError,
  setOrderSummary,
  putOrderSummary,
  clearOrderSummaryBasket,
} from '../../../actions/common/checkoutActions'

const createOrderSummaryResponse = () => ({
  ageVerificationDeliveryConfirmationRequired: false,
  basket: {
    ageVerificationRequired: false,
    deliveryOptions: [
      {
        deliveryOptionExternalId: 'retail_store_immediate',
        deliveryOptionId: 51017,
        enabled: false,
        label: 'Collect From Store Today \u00a33.00',
        selected: false,
      },
      {
        deliveryOptionExternalId: 'retail_store_standard',
        deliveryOptionId: 45019,
        enabled: true,
        label: 'Free Collect From Store Standard \u00a30.00',
        selected: true,
      },
      {
        deliveryOptionExternalId: 'retail_store_collection',
        deliveryOptionId: 50517,
        enabled: true,
        label: 'Collect from ParcelShop \u00a34.00',
        selected: false,
      },
      {
        deliveryOptionExternalId: 'n3',
        deliveryOptionId: 28004,
        enabled: true,
        label: 'Express / Nominated Day Delivery \u00a36.00',
        selected: false,
      },
      {
        deliveryOptionExternalId: 'retail_store_express',
        deliveryOptionId: 56018,
        enabled: true,
        label: 'Collect From Store Express \u00a33.00',
        selected: false,
      },
      {
        deliveryOptionExternalId: 's',
        deliveryOptionId: 26504,
        enabled: true,
        label: 'Standard Delivery \u00a34.00',
        selected: false,
      },
    ],
    discounts: [],
    inventoryPositions: {
      item_1: {
        catentryId: '20688467',
        inventorys: [
          {
            cutofftime: '2100',
            expressdates: ['2018-01-10', '2018-01-11'],
            ffmcenterId: 12556,
            quantity: 45,
          },
        ],
        partNumber: '602015000864158',
      },
    },
    orderId: 700381188,
    products: [
      {
        ageVerificationRequired: false,
        assets: [
          {
            assetType: 'IMAGE_SMALL',
            index: 1,
            url:
              'http://media.topshop.com/wcsstore/TopShop/images/catalog/30T08IGRY_small.jpg',
          },
          {
            assetType: 'IMAGE_THUMB',
            index: 1,
            url:
              'http://media.topshop.com/wcsstore/TopShop/images/catalog/30T08IGRY_thumb.jpg',
          },
          {
            assetType: 'IMAGE_NORMAL',
            index: 1,
            url:
              'http://media.topshop.com/wcsstore/TopShop/images/catalog/30T08IGRY_normal.jpg',
          },
          {
            assetType: 'IMAGE_LARGE',
            index: 1,
            url:
              'http://media.topshop.com/wcsstore/TopShop/images/catalog/30T08IGRY_large.jpg',
          },
        ],
        attributes: {},
        bundleProducts: [],
        bundleSlots: [],
        catEntryId: 20688467,
        colourSwatches: [],
        inStock: true,
        isBundleOrOutfit: false,
        items: [],
        lineNumber: '30T08IGRY',
        lowStock: false,
        name: 'TALL Flannel Belted Peg Trousers',
        orderItemId: 7940619,
        productId: 20688452,
        quantity: 1,
        shipModeId: 45019,
        size: '12',
        totalPrice: '40.00',
        tpmLinks: [],
        unitPrice: '40.00',
      },
    ],
    promotions: [],
    restrictedDeliveryItem: false,
    savedProducts: [],
    subTotal: '40.00',
    total: '40.00',
    totalBeforeDiscount: '40.00',
  },
  deliveryInstructions: '',
  deliveryLocations: [
    {
      deliveryLocationType: 'HOME',
      deliveryMethods: [],
      enabled: true,
      label:
        'Home Delivery Standard (UK up to 4 working days; worldwide varies)  Express (UK next or nominated day; worldwide varies)',
      selected: false,
    },
    {
      deliveryLocationType: 'STORE',
      deliveryMethods: [
        {
          additionalDescription: 'Collection date Sunday 14 January 2018',
          cost: '3.00',
          deliveryOptions: [],
          deliveryType: 'STORE_EXPRESS',
          enabled: true,
          label: 'Collect From Store Express',
          selected: false,
          shipModeId: 56018,
        },
        {
          additionalDescription: 'Collection date Sunday 14 January 2018',
          deliveryOptions: [],
          deliveryType: 'STORE_STANDARD',
          enabled: true,
          label: 'Collect From Store Standard',
          selected: true,
          shipCode: 'Retail Store Standard',
          shipModeId: 45019,
        },
      ],
      enabled: true,
      label:
        'Collect from Store Standard (3-7 working days) Express (next day)',
      selected: true,
    },
    {
      deliveryLocationType: 'PARCELSHOP',
      deliveryMethods: [],
      enabled: true,
      label:
        'Collect from ParcelShop - UK only Thousands of local shops open early and late Next Day Delivery',
      selected: false,
    },
  ],
  deliveryStoreCode: 'TM8137',
  estimatedDelivery: ['No later than\u00a0Sunday 14 January 2018'],
  giftCards: [],
  savedAddresses: [],
  shippingCountry: 'United Kingdom',
  smsMobileNumber: '',
  storeDetails: {
    address1: 'c/o Top Shop, 60/64 The Strand',
    address2: '',
    city: 'Strand',
    country: 'United Kingdom',
    postcode: 'WC2N 5LR',
  },
  version: '1.10',
})

describe('checkoutReducer', () => {
  describe('DELIVERY_AND_PAYMENT_SET_DELIVERY_EDITING_ENABLED', () => {
    it('set deliveryEditingEnabled to true when "enabled" = true', () => {
      const store = createStore(checkoutReducer)
      store.dispatch(setDeliveryEditingEnabled(true))
      expect(
        path(['deliveryAndPayment', 'deliveryEditingEnabled'], store.getState())
      ).toEqual(true)
    })
    it('set deliveryEditingEnabled to false when "enabled" = false', () => {
      const store = createStore(checkoutReducer)
      store.dispatch(setDeliveryEditingEnabled(false))
      expect(
        path(['deliveryAndPayment', 'deliveryEditingEnabled'], store.getState())
      ).toEqual(false)
    })
  })
  describe('SET_SAVE_PAYMENT_DETAILS_ENABLED', () => {
    it('set savePaymentDetails to true when "enabled" = true', () => {
      const store = createStore(checkoutReducer)
      store.dispatch(setDeliveryEditingEnabled(true))
      expect(
        path(['deliveryAndPayment', 'deliveryEditingEnabled'], store.getState())
      ).toEqual(true)
    })
    it('set savePaymentDetails to false when "enabled" = false', () => {
      const store = createStore(checkoutReducer)
      store.dispatch(setDeliveryEditingEnabled(false))
      expect(
        path(['deliveryAndPayment', 'deliveryEditingEnabled'], store.getState())
      ).toEqual(false)
    })
  })
  describe('FETCH_ORDER_SUMMARY_SUCCESS', () => {
    it('set orderSummary and orderCompleted to object', () => {
      const store = createStore(checkoutReducer)
      const state = store.getState()
      store.dispatch(clearOrderSummaryBasket())
      expect(path(['orderSummary'], state)).toEqual({})
      expect(path(['orderCompleted'], state)).toEqual({})
    })
  })
  describe('order summary', () => {
    it('sets the order summary error', () => {
      const store = createStore(checkoutReducer)
      const obj = { test: 'test' }
      expect(store.getState().orderSummaryError).toEqual({})
      store.dispatch(setOrderSummaryError(obj))
      expect(store.getState().orderSummaryError).toEqual(obj)
    })

    it("set's the order summary", () => {
      const resp = createOrderSummaryResponse()
      const store = configureStore()
      expect(store.getState().checkout.orderSummary).toEqual({})
      store.dispatch(setOrderSummary(resp))
      expect(store.getState().checkout.orderSummary).toEqual(resp)
    })

    it('putOrderSummary() empties order summary error', (done) => {
      const store = configureMockStore({
        checkout: {
          orderSummaryError: {
            message: 'error',
          },
        },
      })

      nock('http://localhost:3000')
        .put('/api/checkout/order_summary', {})
        .reply(200, {})

      store.subscribeUntilPasses(() => {
        expect(store.getState().checkout.orderSummaryError).toEqual({})
        done()
      })

      store.dispatch(putOrderSummary({}))
    })
  })

  describe('setStoreUpdating()', () => {
    it('sets boolean', () => {
      const store = createStore(checkoutReducer)
      store.dispatch(setStoreUpdating(true))
      expect(store.getState().storeUpdating).toEqual(true)
    })
  })

  describe('ADDRESS_BOOK_SELECT_ADDRESS_API_SUCCESS', () => {
    it('should update deliveryDetails and savedAddress in orderSummary', () => {
      const payload = {
        deliveryDetails: {
          addressDetailsId: 'fake address',
        },
        savedAddresses: [
          {
            id: 123,
          },
        ],
      }
      const actual = checkoutReducer(
        {},
        {
          type: 'ADDRESS_BOOK_SELECT_ADDRESS_API_SUCCESS',
          payload,
        }
      )
      expect(path(['orderSummary', 'deliveryDetails'], actual)).toEqual(
        payload.deliveryDetails
      )
      expect(path(['orderSummary', 'savedAddresses'], actual)).toEqual(
        payload.savedAddresses
      )
    })
  })

  describe('ADDRESS_BOOK_DELETE_ADDRESS_API_SUCCESS', () => {
    it('should update the savedAddresses with the request payload', () => {
      const address1 = 'somewhere'
      const address2 = 'somewhere else'
      const store = configureMockStore({
        checkout: { orderSummary: { savedAddresses: [address1, address2] } },
      })
      const payload = {
        DeliveryOptionsDetails: {
          deliveryoptionsform: {
            savedAddresses: [address1],
          },
        },
      }
      store.dispatch({
        type: 'ADDRESS_BOOK_DELETE_ADDRESS_API_SUCCESS',
        payload,
      })
      const updatedSavedAddresses = store.getState().checkout.orderSummary
        .savedAddresses
      expect(updatedSavedAddresses).toEqual([address1])
    })
  })

  describe('LOGOUT', () => {
    it('LOGOUT should wipe checkout container session state', () => {
      const initialState = checkoutReducer(undefined, {})

      let state = checkoutReducer(initialState, {
        type: 'SET_DELIVERY_STORE',
        store: 'legendary store',
      })
      expect(state).not.toEqual(initialState)

      state = checkoutReducer(state, { type: 'LOGOUT' })
      expect(state).toEqual(initialState)
    })
  })

  describe('EMPTY_ORDER_SUMMARY', () => {
    it('EMPTY_ORDER_SUMMARY should wipe checkout order summary', () => {
      const initialState = {
        somethingElse: 'somethingElse',
        orderSummary: {
          products: [],
        },
      }
      const expectedState = {
        somethingElse: 'somethingElse',
        orderSummary: {},
      }

      const state = checkoutReducer(initialState, {
        type: 'EMPTY_ORDER_SUMMARY',
      })

      expect(state).toEqual(expectedState)
    })
  })
})
