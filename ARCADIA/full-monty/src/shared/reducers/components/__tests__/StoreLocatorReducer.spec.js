import { createStore } from 'redux'
import * as actions from '../../../actions/components/StoreLocatorActions'

import storeLocatorReducer from '../StoreLocatorReducer'

const snapshot = (reducer) => expect(reducer).toMatchSnapshot()

describe('StoreLocator Reducer', () => {
  describe('SET_FULFILMENT_STORE', () => {
    it('single store set in state', () => {
      const store = createStore(storeLocatorReducer)
      const selectedStore = {
        id: 'fake-store-id',
        name: 'Fake Store Name',
      }
      expect(store.getState().selectedStore).toEqual({})
      store.dispatch({
        type: 'SET_FULFILMENT_STORE',
        payload: selectedStore,
      })
      expect(store.getState().selectedStore).toEqual(selectedStore)
    })
  })
  describe('SET_SELECTED_STORE_ID', () => {
    it('store id set in state', () => {
      const store = createStore(storeLocatorReducer)
      const selectedStoreId = 'fake-store-id'
      expect(store.getState().selectedStoreId).not.toBeDefined()
      store.dispatch({
        type: 'SET_SELECTED_STORE_ID',
        selectedStoreId,
      })
      expect(store.getState().selectedStoreId).toEqual(selectedStoreId)
    })
  })
  describe('SELECT_STORE', () => {
    it('set store index', () => {
      const store = createStore(storeLocatorReducer)
      const selectedStoreIndex = 3
      expect(store.getState().selectedStoreIndex).not.toBeDefined()
      store.dispatch({
        type: 'SELECT_STORE',
        index: selectedStoreIndex,
      })
      expect(store.getState().selectedStoreIndex).toEqual(selectedStoreIndex)
    })
  })
  describe('APPLY_FILTERS', () => {
    it('when today filter is applied it disables parcel and others', () => {
      const store = createStore(storeLocatorReducer)
      const filters = ['today']
      store.dispatch({
        type: 'APPLY_FILTERS',
        filters,
      })
      snapshot(store.getState().filters)
    })
    it('when parcel filter is applied it does not disables parcel and others', () => {
      const store = createStore(storeLocatorReducer)
      const filters = ['parcel']
      store.dispatch({
        type: 'APPLY_FILTERS',
        filters,
      })
      snapshot(store.getState().filters)
    })
  })
  describe('SET_FILTERS', () => {
    it('set filters', () => {
      const store = createStore(storeLocatorReducer)
      const filters = {
        today: {
          applied: true,
          selected: true,
          disabled: false,
        },
        brand: {
          applied: false,
          selected: true,
          disabled: false,
        },
        parcel: {
          applied: false,
          selected: true,
          disabled: false,
        },
        other: {
          applied: true,
          selected: true,
          disabled: false,
        },
      }
      store.dispatch(actions.setFilters(filters))
      snapshot(store.getState().filters)
      expect(store.getState().filters).toEqual(filters)
    })
  })
  describe('SET_FULFILMENT_STORE_SKU', () => {
    it('should set selectedStoreSKU', () => {
      const store = createStore(storeLocatorReducer)
      const sku = '90210'
      store.dispatch({
        type: 'SET_FULFILMENT_STORE_SKU',
        sku,
      })
      snapshot(store.getState())
      expect(store.getState().selectedStoreSKU).toBe(sku)
    })
  })
  describe('RESET_STORE_LOCATOR', () => {
    it('should persist filters and selectedStore', () => {
      const initialState = {
        selectedStore: { storeId: 'TM0007' },
        filters: { today: { applied: true } },
      }

      expect(
        storeLocatorReducer(initialState, {
          type: 'RESET_STORE_LOCATOR',
        })
      ).toMatchSnapshot()
    })
  })
})
