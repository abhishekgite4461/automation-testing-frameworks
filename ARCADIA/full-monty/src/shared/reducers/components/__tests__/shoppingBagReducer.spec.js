import testReducer from '../shoppingBagReducer'
import configureMockStore from '../../../../../test/unit/lib/configure-mock-store'
import { UPDATE_LOCATION } from 'react-router-redux'

describe('Shopping Bag Reducer', () => {
  it('Default values', () => {
    const state = configureMockStore().getState()
    expect(state.shoppingBag.totalItems).toBe(0)
    expect(state.shoppingBag.promotionCode).toBe(null)
    expect(state.shoppingBag.currentlyAddingToBag).toBe(false)
    expect(state.shoppingBag.miniBagOpen).toBe(false)
    expect(state.shoppingBag.autoClose).toBe(false)
    expect(state.shoppingBag.promotionCodeConfirmation).toBe(false)
    expect(state.shoppingBag.bag.products).toEqual([])
    expect(state.shoppingBag.bag.orderId).toBe(0)
    expect(state.shoppingBag.loadingShoppingBag).toBe(false)
    expect(state.shoppingBag.recentlyAdded.products).toEqual([])
    expect(state.shoppingBag.recentlyAdded.quantity).toBe(0)
    expect(state.shoppingBag.recentlyAdded.isMiniBagConfirmShown).toBe(false)
  })
  describe('UPDATE_SHOPPING_BAG_BADGE_COUNT', () => {
    it('should update `totalItems`', () => {
      expect(
        testReducer(
          {},
          {
            type: 'UPDATE_SHOPPING_BAG_BADGE_COUNT',
            count: 12,
          }
        )
      ).toEqual({
        totalItems: 12,
      })
    })
  })
  describe('SET_PROMOTION_CODE', () => {
    const promotionCode = 'PROMO101'
    it('should set `promotionCode`', () => {
      expect(
        testReducer(
          {},
          {
            type: 'SET_PROMOTION_CODE',
            promotionCode,
          }
        )
      ).toEqual({
        promotionCode,
      })
    })
  })
  describe('SET_CURRENTLY_ADDING_TO_BAG', () => {
    it('should set `currentlyAddingToBag`', () => {
      expect(
        testReducer(
          {},
          {
            type: 'SET_CURRENTLY_ADDING_TO_BAG',
            val: true,
          }
        )
      ).toEqual({
        currentlyAddingToBag: true,
      })
    })
  })
  describe('OPEN_MINI_BAG', () => {
    it('should open mini bag', () => {
      expect(
        testReducer(
          {},
          {
            type: 'OPEN_MINI_BAG',
          }
        )
      ).toEqual({
        miniBagOpen: true,
      })
    })

    it('should open mini bag with autoClose enabled', () => {
      expect(
        testReducer(
          {},
          {
            type: 'OPEN_MINI_BAG',
            autoClose: true,
          }
        )
      ).toEqual({
        miniBagOpen: true,
        autoClose: true,
      })
    })

    it('should open mini bag with autoClose disabled', () => {
      expect(
        testReducer(
          {},
          {
            type: 'OPEN_MINI_BAG',
            autoClose: false,
          }
        )
      ).toEqual({
        miniBagOpen: true,
        autoClose: false,
      })
    })
  })
  describe('CLOSE_MINI_BAG', () => {
    it('should close mini bag', () => {
      expect(
        testReducer(
          {},
          {
            type: 'CLOSE_MINI_BAG',
          }
        )
      ).toEqual({
        miniBagOpen: false,
      })
    })
  })
  describe('SET_LOADING_SHOPPING_BAG', () => {
    it('should set `loadingShoppingBag`', () => {
      expect(
        testReducer(
          {},
          {
            type: 'SET_LOADING_SHOPPING_BAG',
            isLoading: true,
          }
        )
      ).toEqual({
        loadingShoppingBag: true,
      })
    })
  })
  describe('SET_PROMOTION_CODE_CONFIRMATION', () => {
    it('should set `promotionCodeConfirmation`', () => {
      expect(
        testReducer(
          {},
          {
            type: 'SET_PROMOTION_CODE_CONFIRMATION',
            promotionCodeConfirmation: true,
          }
        )
      ).toEqual({
        promotionCodeConfirmation: true,
      })
    })
  })
  describe('UPDATE_BAG', () => {
    it('should update bag details', () => {
      const bag = {
        products: [
          {
            productId: 30275174,
            catEntryId: 30275208,
            orderItemId: 8800490,
            lineNumber: '35S13MMUS',
            size: '16',
            name: 'Velvet Square Neck Mini Slip Dress',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '26.00',
            totalPrice: '26.00',
            isBundleOrOutfit: false,
          },
        ],
        orderId: 123456,
        subTotal: '26.00',
        total: '30.00',
      }
      expect(
        testReducer(
          {},
          {
            type: 'UPDATE_BAG',
            bag,
          }
        )
      ).toEqual({
        bag,
      })
    })
  })
  describe('UPDATE_SHOPPING_BAG_PRODUCT', () => {
    const bag = {
      products: [
        {
          productId: 30275174,
          size: '16',
          name: 'Velvet Square Neck Mini Slip Dress',
          quantity: 1,
        },
      ],
      orderId: 123456,
    }
    it('should update bag product details if index is equal', () => {
      expect(
        testReducer(
          { bag },
          {
            type: 'UPDATE_SHOPPING_BAG_PRODUCT',
            index: 0,
            update: {
              quantity: 3,
            },
          }
        )
      ).toEqual({
        bag: {
          products: [
            {
              productId: 30275174,
              size: '16',
              name: 'Velvet Square Neck Mini Slip Dress',
              quantity: 3,
            },
          ],
          orderId: 123456,
        },
      })
    })
    it('should not update bag product details if index is not equal', () => {
      expect(
        testReducer(
          { bag },
          {
            type: 'UPDATE_SHOPPING_BAG_PRODUCT',
            index: 13,
            update: {
              quantity: 3,
            },
          }
        )
      ).toEqual({
        bag: {
          products: [
            {
              productId: 30275174,
              size: '16',
              name: 'Velvet Square Neck Mini Slip Dress',
              quantity: 1,
            },
          ],
          orderId: 123456,
        },
      })
    })
  })
  describe('EMPTY_SHOPPING_BAG', () => {
    it('should reset shopping bag details', () => {
      const bag = {
        products: [
          {
            productId: 30275174,
            catEntryId: 30275208,
            orderItemId: 8800490,
            lineNumber: '35S13MMUS',
            size: '16',
            name: 'Velvet Square Neck Mini Slip Dress',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '26.00',
            totalPrice: '26.00',
            isBundleOrOutfit: false,
          },
        ],
        orderId: 123456,
        subTotal: '26.00',
        total: '30.00',
      }
      expect(
        testReducer(
          { bag, totalItems: 1 },
          {
            type: 'EMPTY_SHOPPING_BAG',
          }
        )
      ).toEqual({
        bag: {
          products: [],
          orderId: 0,
        },
        totalItems: 0,
        recentlyAdded: {
          products: [],
          quantity: 0,
          isMiniBagConfirmShown: false,
        },
      })
    })
  })
  describe('FETCH_ORDER_SUMMARY_SUCCESS', () => {
    it('should set details if basket', () => {
      const basket = {
        products: [
          {
            productId: 30275174,
            catEntryId: 30275208,
            orderItemId: 8800490,
            lineNumber: '35S13MMUS',
            size: '16',
            name: 'Velvet Square Neck Mini Slip Dress',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '26.00',
            totalPrice: '26.00',
            isBundleOrOutfit: false,
          },
          {
            quantity: 3,
          },
        ],
        orderId: 123456,
        subTotal: '26.00',
        total: '30.00',
      }
      expect(
        testReducer(
          {
            bag: {
              products: [{}, {}],
            },
            totalItems: 2,
          },
          {
            type: 'FETCH_ORDER_SUMMARY_SUCCESS',
            data: {
              basket,
            },
          }
        )
      ).toEqual({
        bag: basket,
        totalItems: 4,
      })
    })
    it('should reset details if basket not there', () => {
      expect(
        testReducer(
          {
            totalItems: 9,
            bag: {
              products: [{}],
            },
          },
          {
            type: 'FETCH_ORDER_SUMMARY_SUCCESS',
            data: {},
          }
        )
      ).toEqual({
        totalItems: 0,
        bag: {
          products: [],
          orderId: 0,
        },
        recentlyAdded: {
          products: [],
          quantity: 0,
          isMiniBagConfirmShown: false,
        },
      })
    })
  })
  describe('PRODUCTS_ADDED_TO_BAG', () => {
    it('should update `recentlyAdded`', () => {
      const products = [
        {
          productId: 30275174,
          catEntryId: 30275208,
          orderItemId: 8800490,
          lineNumber: '35S13MMUS',
          size: '16',
          name: 'Velvet Square Neck Mini Slip Dress',
          quantity: 1,
          lowStock: false,
          inStock: true,
          unitPrice: '26.00',
          totalPrice: '26.00',
          isBundleOrOutfit: false,
        },
      ]
      expect(
        testReducer(
          {},
          {
            type: 'PRODUCTS_ADDED_TO_BAG',
            payload: {
              products,
              quantity: 2,
            },
          }
        )
      ).toEqual({
        recentlyAdded: {
          isMiniBagConfirmShown: false,
          products,
          quantity: 2,
        },
      })
    })
  })
  describe('SHOW_MINIBAG_CONFIRM', () => {
    it('should update `isMiniBagConfirmShown`', () => {
      const products = [
        {
          productId: 30275174,
          catEntryId: 30275208,
          orderItemId: 8800490,
          lineNumber: '35S13MMUS',
          size: '16',
          name: 'Velvet Square Neck Mini Slip Dress',
          quantity: 1,
          lowStock: false,
          inStock: true,
          unitPrice: '26.00',
          totalPrice: '26.00',
          isBundleOrOutfit: false,
        },
      ]
      expect(
        testReducer(
          {
            recentlyAdded: {
              products,
              quantity: 2,
            },
          },
          {
            type: 'SHOW_MINIBAG_CONFIRM',
            payload: true,
          }
        )
      ).toEqual({
        recentlyAdded: {
          isMiniBagConfirmShown: true,
          products,
          quantity: 2,
        },
      })
    })
  })
  describe('UPDATE_ACTIVE_ITEM', () => {
    it('should update whole `recentlyAdded`', () => {
      const products = [
        {
          productId: 30275174,
          catEntryId: 30275208,
          orderItemId: 8800490,
          lineNumber: '35S13MMUS',
          size: '16',
          name: 'Velvet Square Neck Mini Slip Dress',
          quantity: 1,
          lowStock: false,
          inStock: true,
          unitPrice: '26.00',
          totalPrice: '26.00',
          isBundleOrOutfit: false,
        },
      ]
      expect(
        testReducer(
          {
            recentlyAdded: {
              products,
            },
          },
          {
            type: 'UPDATE_ACTIVE_ITEM',
          }
        )
      ).toEqual({
        recentlyAdded: {
          isMiniBagConfirmShown: false,
          products,
          quantity: 0,
        },
      })
    })
  })
  describe('PRE_CACHE_RESET', () => {
    it('should set initial state', () => {
      const bag = {
        products: [
          {
            productId: 30275174,
            size: '16',
            name: 'Velvet Square Neck Mini Slip Dress',
            quantity: 1,
          },
        ],
        orderId: 123456,
        subTotal: '26.00',
        total: '30.00',
      }
      expect(
        testReducer(
          { bag, miniBagOpen: true, totalItems: 1 },
          {
            type: 'PRE_CACHE_RESET',
          }
        )
      ).toEqual({
        totalItems: 0,
        promotionCode: null,
        currentlyAddingToBag: false,
        miniBagOpen: false,
        autoClose: false,
        promotionCodeConfirmation: false,
        bag: {
          products: [],
          orderId: 0,
        },
        loadingShoppingBag: false,
        recentlyAdded: {
          products: [],
          quantity: 0,
          isMiniBagConfirmShown: false,
        },
      })
    })
  })
  describe('LOGOUT', () => {
    it('should reset mini bag and not change `miniBagOpen` state when logout', () => {
      const bag = {
        products: [
          {
            productId: 30275174,
            size: '16',
            name: 'Velvet Square Neck Mini Slip Dress',
            quantity: 1,
          },
        ],
        orderId: 123456,
        subTotal: '26.00',
        total: '30.00',
      }
      expect(
        testReducer(
          { bag, miniBagOpen: true, totalItems: 1 },
          {
            type: 'LOGOUT',
          }
        )
      ).toEqual({
        totalItems: 0,
        promotionCode: null,
        currentlyAddingToBag: false,
        miniBagOpen: true,
        autoClose: false,
        promotionCodeConfirmation: false,
        bag: {
          products: [],
          orderId: 0,
        },
        loadingShoppingBag: false,
        recentlyAdded: {
          products: [],
          quantity: 0,
          isMiniBagConfirmShown: false,
        },
      })
    })
  })
  describe('`UPDATE_LOCATION`', () => {
    it('should close mini bag when change location', () => {
      const bag = {
        products: [
          {
            productId: 30275174,
            size: '16',
            name: 'Velvet Square Neck Mini Slip Dress',
            quantity: 1,
          },
        ],
        orderId: 123456,
        subTotal: '26.00',
        total: '30.00',
      }
      expect(
        testReducer(
          { bag, miniBagOpen: true, totalItems: 1 },
          {
            type: UPDATE_LOCATION,
          }
        )
      ).toEqual({
        bag,
        miniBagOpen: false,
        totalItems: 1,
      })
    })
  })
})
