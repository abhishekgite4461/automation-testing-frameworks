import reducer from '../QuickviewReducer'
import {
  setProductQuickview,
  setProductIdQuickview,
} from '../../../actions/common/productsActions'

describe('QuickviewReducer', () => {
  const initialState = {
    product: {},
  }

  it('should return the initial state', () => {
    const expectedState = initialState

    expect(reducer(undefined, {})).toEqual(expectedState)
  })

  it('should handle SET_PRODUCT_QUICKVIEW', () => {
    const expectedState = {
      product: {
        someFakeStuff: 'test',
      },
    }

    expect(
      reducer(undefined, setProductQuickview(expectedState.product))
    ).toEqual(expectedState)
  })

  it('should handle SET_PRODUCT_ID_QUICKVIEW', () => {
    const expectedState = {
      ...initialState,
      productId: 'fakeId',
    }

    expect(
      reducer(undefined, setProductIdQuickview(expectedState.productId))
    ).toEqual(expectedState)
  })
})
