import createReducer from '../../lib/create-reducer'

const initialState = { destination: '', language: '' }

export default createReducer(initialState, {
  SET_SHIPPING_DESTINATION: (state, { destination }) => ({
    ...state,
    destination,
  }),
  SET_LANGUAGE: (state, { language }) => ({ ...state, language }),
})
