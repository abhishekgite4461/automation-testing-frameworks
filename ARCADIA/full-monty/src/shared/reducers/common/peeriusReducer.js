import createReducer from '../../lib/create-reducer'
import { omit } from 'ramda'

const baseTrack = {
  lang: null,
  channel: 'mobileweb',
  smartRecs: {
    showAttributes: ['*'],
  },
}
const initialState = {
  dynamic: false,
  smartRecs: null,
  baseTrack,
  track: null,
  domain: null,
}

// Seems like overkill - but allows me to rewire in unit tests.
const isBrowser = () => process.browser

// Peerius checks against the window.PeeriusCallbacks object to obtain all its information
// When updating this reducer, we sync it with the window object
const syncWindow = (state) => {
  if (!isBrowser()) return
  window.PeeriusCallbacks = state
  // If Peerius has loaded, set the extraXml directly
  // This is hack, as once initialised their dynamic script does not update the extraXml property correctly
  // Discussion with peerius ongoing to update their dynamic function to do this internally.
  if (window.Peerius)
    window.Peerius.extraXml = `<_di><json><![CDATA[${JSON.stringify(
      state.track
    )}]]></json></_di>`
}

const loadScript = (domain) =>
  isBrowser() &&
  window.loadScripts &&
  window.loadScripts([
    {
      src: `https://${domain}.peerius.com/tracker/peerius.page`,
    },
  ])

const canDynamicTrack = () =>
  isBrowser() && window.Peerius && typeof window.Peerius.dynamic === 'function'
const canClickTrack = () =>
  isBrowser() &&
  window.Peerius &&
  typeof window.Peerius.smartRecsSendClick === 'function'

const dynamicTrack = () => canDynamicTrack() && window.Peerius.dynamic()
const trackClick = (id) =>
  canClickTrack() && window.Peerius.smartRecsSendClick(id)

export function userAccount(state, { firstName, lastName, email }) {
  return {
    ...state,
    baseTrack: {
      ...state.baseTrack,
      user: {
        name: `${firstName} ${lastName}`,
        email,
      },
    },
  }
}

export default createReducer(initialState, {
  SET_PEERIUS_CALLBACK: (state, { callback }) => {
    const newState = {
      ...state,
      smartRecs: callback,
    }

    syncWindow(newState)

    return newState
  },
  TRACK_PEERIUS: (state, { track }) => {
    const newState = {
      ...state,
      track: {
        ...state.baseTrack,
        ...track,
      },
    }

    syncWindow(newState)

    if (state.dynamic) {
      dynamicTrack()
    } else if (isBrowser()) {
      // If we could dynamic track, but we currently aren't that means we should do next time
      // First track is handled by the library when it loads.
      loadScript(state.domain)
      return { ...newState, dynamic: true }
    }

    return newState
  },
  CLICK_RECOMMENDATION: (state, { id }) => {
    trackClick(id)
    return state
  },
  SET_CONFIG: (state, { config }) => ({
    ...state,
    domain: config.peeriusDomain,
    baseTrack: {
      ...state.baseTrack,
      lang: config.peeriusLang,
    },
  }),
  SET_PEERIUS_DOMAIN: (state, { domain }) => ({
    ...state,
    domain,
  }),

  USER_ACCOUNT: (state, { user }) => userAccount(state, user),
  GET_USER_ACCOUNT_API_SUCCESS: (state, { payload }) =>
    userAccount(state, payload),
  CHANGE_PASSWORD_FORM_API_SUCCESS: (state, { payload }) =>
    userAccount(state, payload),
  CHANGE_SHORT_PROFILE_FORM_API_SUCCESS: (state, { payload }) =>
    userAccount(state, payload),
  UPDATE_USER_ACCOUNT_FORM_API_SUCCESS: (state, { payload }) =>
    userAccount(state, payload),

  PRE_CACHE_RESET: (state) => ({
    ...state,
    baseTrack: omit(['user'], state.baseTrack),
  }),
  LOGOUT: (state) => ({
    ...state,
    baseTrack: omit(['user'], state.baseTrack),
  }),
})
