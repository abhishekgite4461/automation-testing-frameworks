import createReducer from '../../lib/create-reducer'
import { setCacheData } from '../../../client/lib/storage'
import { UPDATE_LOCATION } from 'react-router-redux'

const initialState = {
  user: {},
  forgetPwd: false,
  myCheckoutDetails: {
    editingEnabled: false,
    initialFocus: undefined,
  },
}

function userAccount(state, user) {
  const newState = { ...state, user }
  if (process.browser) setCacheData('account', newState) // TODO get rid of side effect from reducer!
  return newState
}

export default createReducer(initialState, {
  PRE_CACHE_RESET: () => initialState,
  RETRIEVE_CACHED_DATA: (state, { account }) => account || state,
  LOGOUT: () => initialState,

  USER_ACCOUNT: (state, { user }) => userAccount(state, user),
  GET_USER_ACCOUNT_API_SUCCESS: (state, { payload }) =>
    userAccount(state, payload),
  CHANGE_PASSWORD_FORM_API_SUCCESS: (state, { payload }) =>
    userAccount(state, payload),
  CHANGE_SHORT_PROFILE_FORM_API_SUCCESS: (state, { payload }) =>
    userAccount(state, payload),
  UPDATE_USER_ACCOUNT_FORM_API_SUCCESS: (state, { payload }) =>
    userAccount(state, payload),

  SET_FORGET_PASSWORD: (state, { value }) => ({ ...state, forgetPwd: value }),
  TOGGLE_FORGET_PASSWORD: (state) => ({
    ...state,
    forgetPwd: !state.forgetPwd,
  }),

  // MyCheckoutDetails
  SET_MCD_INITIAL_FOCUS: (state, { initialFocus }) => ({
    ...state,
    myCheckoutDetails: {
      ...state.myCheckoutDetails,
      initialFocus,
    },
  }),
  [UPDATE_LOCATION]: (state, { payload: { pathname = '' } }) => {
    switch (pathname) {
      case '/my-account/details':
        return {
          ...state,
          myCheckoutDetails: {
            ...state.myCheckoutDetails,
            editingEnabled: false,
          },
        }
      case '/my-account/details/edit':
        return {
          ...state,
          myCheckoutDetails: {
            ...state.myCheckoutDetails,
            editingEnabled: true,
          },
        }
      default:
        return state
    }
  },
})
