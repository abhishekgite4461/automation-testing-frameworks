import createReducer from '../../lib/create-reducer'

export default createReducer(
  { value: '' },
  {
    SET_JSESSION_ID: (state, { jsessionid }) => ({
      ...state,
      value: jsessionid,
    }),
  }
)
