import createReducer from '../../lib/create-reducer'

export default createReducer(
  {},
  {
    CLEAR_PRODUCT: () => ({}),
    SET_PRODUCT: (currentProduct, { product }) => product,
    UPDATE_SEE_MORE_URL: (currentProduct, { seeMoreLink, seeMoreUrl }) => {
      const seeMoreValue = currentProduct.seeMoreValue.map((seeMore) => {
        return seeMore.seeMoreLink === seeMoreLink
          ? {
              ...seeMore,
              seeMoreUrl,
            }
          : seeMore
      })
      return {
        ...currentProduct,
        seeMoreValue,
      }
    },
  }
)
