import { get } from '../../lib/api-service'
import {
  getBrandConfigByPreferredISO,
  getAllBrandHostnames,
  getEnvDomainsFromConfig,
} from '../../../server/config'
import { SUPPORTED_FEATURES } from '../../constants/features'
import { error } from '../../../server/lib/logger'

const { FEATURE_GEOIP } = SUPPORTED_FEATURES

const desktopMainDevDomain = 'arcadiagroup.ltd.uk'
const mobileMainDevDomain = 'digital.arcadiagroup.co.uk'

const isUsingMobileDomain = (hostname) => /^m\.|\.m\.|-m-/.test(hostname)

const handleDesktopMainDevRedirect = (hostname, userGeoPreference) => {
  const [domain, queryString = ''] = hostname.split('?')
  const newQS = queryString
    ? queryString.replace(
        /prefShipCtry=[A-Z]{2}/,
        `prefShipCtry=${userGeoPreference}`
      )
    : `prefShipCtry=${userGeoPreference}`

  return `${domain}?${newQS}`
}

const handleMobileAWSRedirect = (hostname, redirectDomain) => {
  const [, env, restDomain] = hostname.match(/^([^-]+)-[^.]+(.+)/)
  return `${env}-${redirectDomain.replace(/\./g, '-')}${restDomain}`
}

const handleMobileUnconfiguredAkamaiRedirect = (hostname, redirectDomain) => {
  const [, leftSubdomain] = hostname.match(/^([^.]+)\./)
  return `${leftSubdomain}.${redirectDomain}`
}

export const handleDevRedirect = (
  brandName,
  hostname,
  userGeoPreference,
  redirectDomain
) => {
  const isDesktopMainDev =
    !isUsingMobileDomain(hostname) &&
    hostname.includes(`.${desktopMainDevDomain}`)
  if (isDesktopMainDev)
    return handleDesktopMainDevRedirect(hostname, userGeoPreference)

  const isMobileMainDev = hostname.includes(`.${mobileMainDevDomain}`)
  if (isMobileMainDev) return handleMobileAWSRedirect(hostname, redirectDomain)

  const isMobileUnconfiguredAkamai = !getAllBrandHostnames(
    brandName,
    hostname
  ).includes(hostname)
  if (isMobileUnconfiguredAkamai)
    return handleMobileUnconfiguredAkamaiRedirect(hostname, redirectDomain)

  return redirectDomain
}

export const getURLPath = (url) => {
  const matches = url.match(/(https?:\/\/)?[^/]+\.[^/]+(\.[^/]+)*(\/.*)?$/)
  if (!matches) return undefined
  return matches[3] || '/'
}

export const getUserGeoPreference = (state) => {
  return state.geoIP.storedGeoPreference || state.geoIP.geoISO
}

export const isValidISO2 = (x) => /^[A-Z]{2}$/.test(x)

export const getRedirectURL = (state) => {
  const {
    config: requestedSiteConfig,
    geoIP: { redirectURL, hostname, geoISO },
    features: {
      status: { [FEATURE_GEOIP]: geoIPEnabled },
    },
  } = state

  const geoIPFeatureDisabled = !geoIPEnabled
  const noGeoIPHeader = !geoISO

  if (geoIPFeatureDisabled || noGeoIPHeader) return undefined

  const userGeoPreference = getUserGeoPreference(state)
  if (!isValidISO2(userGeoPreference)) return undefined

  if (redirectURL) return redirectURL

  const userRequestedPreferredSite = requestedSiteConfig.preferredISOs.includes(
    userGeoPreference
  )
  if (userRequestedPreferredSite) return undefined

  const userGeoConfig = getBrandConfigByPreferredISO(
    userGeoPreference,
    requestedSiteConfig.brandCode
  )

  const [mobileDomain, desktopDomain] = getEnvDomainsFromConfig(
    userGeoConfig,
    hostname
  )

  const isMobileDomain = isUsingMobileDomain(hostname)
  // If, for whatever reason, the requested domain is the same as the user's prefered ISO domain
  // then we don't want them to redirect to the same domain. Vicious circle.
  const requestedSiteMatchesRedirectSite =
    (isMobileDomain && mobileDomain === hostname) ||
    (!isMobileDomain && desktopDomain === hostname)
  if (requestedSiteMatchesRedirectSite) return undefined

  let redirectDomain = isMobileDomain ? mobileDomain : desktopDomain
  redirectDomain = handleDevRedirect(
    requestedSiteConfig.brandName,
    hostname,
    userGeoPreference,
    redirectDomain
  )

  return redirectDomain
}

export const geoIPActions = {
  setGeoIPRequestData: ({ hostname, geoISO, storedGeoPreference }) => ({
    type: 'SET_GEOIP_REQUEST_DATA',
    hostname,
    geoISO,
    storedGeoPreference,
  }),
  setGeoIPRedirectInfo: (redirectURL) => {
    return { type: 'SET_GEOIP_REDIRECT_URL', redirectURL }
  },
  setRedirectURLForPDP: (partNumber) => {
    return (dispatch, getState) => {
      if (process.browser || !partNumber) return
      const state = getState()
      const userGeoPreference = getUserGeoPreference(state)
      const redirectURL = getRedirectURL(state)

      if (!redirectURL) return Promise.resolve()

      const { config: requestedSiteConfig } = state
      const dontShowGeoIP = requestedSiteConfig.preferredISOs.includes(
        userGeoPreference
      )
      if (dontShowGeoIP) return Promise.resolve()

      return dispatch(get(`/${userGeoPreference}/products/${partNumber}`))
        .then(({ body: { sourceUrl } }) => {
          const path = getURLPath(sourceUrl)
          if (!path) {
            error('GeoIP', {
              message: 'Invalid `sourceUrl` returned in foreign PDP response',
              sourceUrl,
              request: {
                partNumber,
                iso: userGeoPreference,
              },
              state: {
                geoIPRedirectURL: redirectURL,
              },
            })
            throw new Error()
          }

          return dispatch(
            geoIPActions.setGeoIPRedirectInfo(`${redirectURL}${path}`)
          )
        })
        .catch(() => {
          return dispatch(
            geoIPActions.setGeoIPRedirectInfo(`${redirectURL}/404`)
          )
        })
    }
  },
}

const defaultState = {
  redirectURL: '',
  hostname: '',
  geoISO: '',
  storedGeoPreference: '',
}
const geoIPReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'SET_GEOIP_REDIRECT_URL':
      return {
        ...state,
        redirectURL: action.redirectURL,
      }
    case 'SET_GEOIP_REQUEST_DATA':
      return {
        ...state,
        hostname: action.hostname,
        geoISO: action.geoISO || '',
        storedGeoPreference: action.storedGeoPreference || '',
      }
    default:
      return state
  }
}

export default geoIPReducer
