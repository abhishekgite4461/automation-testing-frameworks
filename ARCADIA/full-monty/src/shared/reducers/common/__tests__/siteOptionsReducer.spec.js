import testReducer from '../siteOptionsReducer'
import configureMockStore from '../../../../../test/unit/lib/configure-mock-store'

describe('Site Options Reducer', () => {
  it('Default values', () => {
    const state = configureMockStore().getState()
    expect(state.siteOptions.deliveryCountries).toEqual([])
    expect(state.siteOptions.USStates).toEqual([])
    expect(state.siteOptions.expiryYears).toEqual([])
    expect(state.siteOptions.expiryMonths).toEqual([])
    expect(state.siteOptions.peakService).toBe(false)
    expect(state.siteOptions.billingCountries).toEqual([])
    expect(state.siteOptions.titles).toEqual([])
    expect(state.siteOptions.currencyCode).toBe('')
    expect(state.siteOptions.version).toBe('')
  })
  describe('SET_SITE_OPTIONS', () => {
    it('should update `siteOptions` without `creditCardOptions`', () => {
      expect(
        testReducer(
          {},
          {
            type: 'SET_SITE_OPTIONS',
            siteOptions: {
              creditCardOptions: 'creditCardOptions',
              deliveryCountries: 'deliveryCountries',
            },
          }
        )
      ).toEqual({
        deliveryCountries: 'deliveryCountries',
      })
    })
    it('should update `siteOptions` if not have `creditCardOptions`', () => {
      expect(
        testReducer(
          {},
          {
            type: 'SET_SITE_OPTIONS',
            siteOptions: {
              deliveryCountries: 'deliveryCountries',
            },
          }
        )
      ).toEqual({
        deliveryCountries: 'deliveryCountries',
      })
    })
  })
})
