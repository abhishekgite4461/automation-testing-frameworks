import mobileHostnameReducer from '../mobileHostnameReducer'

describe('#mobileHostnameReducer', () => {
  it('sets isMobile property as expected', () => {
    expect(
      mobileHostnameReducer(
        {
          isMobile: false,
        },
        {
          type: 'SET_IS_MOBILE_HOSTNAME',
          isMobile: true,
        }
      )
    ).toEqual({
      isMobile: true,
    })

    expect(
      mobileHostnameReducer(
        {
          isMobile: true,
        },
        {
          type: 'SET_IS_MOBILE_HOSTNAME',
          isMobile: false,
        }
      )
    ).toEqual({
      isMobile: false,
    })
  })
})
