import testReducer from '../shippingDestinationReducer'
import configureMockStore from '../../../../../test/unit/lib/configure-mock-store'

describe('Shipping Destination Reducer', () => {
  it('Default values', () => {
    const state = configureMockStore().getState()
    expect(state.shippingDestination.destination).toBe('')
    expect(state.shippingDestination.language).toBe('')
  })
  describe('SET_SHIPPING_DESTINATION', () => {
    it('should set destination', () => {
      expect(
        testReducer(
          { destination: 'USA' },
          {
            type: 'SET_SHIPPING_DESTINATION',
            destination: 'Poland',
          }
        )
      ).toEqual({
        destination: 'Poland',
      })
    })
  })
  describe('SET_LANGUAGE', () => {
    it('should set language', () => {
      expect(
        testReducer(
          { language: 'english' },
          {
            type: 'SET_LANGUAGE',
            language: 'polish',
          }
        )
      ).toEqual({
        language: 'polish',
      })
    })
  })
})
