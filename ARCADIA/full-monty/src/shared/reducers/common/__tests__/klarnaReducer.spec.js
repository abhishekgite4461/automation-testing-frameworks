import reducer from '../klarnaReducer'
import {
  setKlarnaSessionId,
  setKlarnaClientToken,
  setKlarnaAuthorizationToken,
  setKlarnaFormVisibility,
  setLoadFormPending,
  setAuthorizePending,
  setReAuthorizePending,
  setOrderSummaryHash,
  setIsUpdatingKlarnaEmail,
  setKlarnaEmail,
  resetKlarna,
  setKlarnaFormSubmitted,
} from '../../../actions/common/klarnaActions'

describe('klarnaReducer', () => {
  const initialState = {
    sessionId: '',
    clientToken: '',
    loadFormPending: false,
    isUpdatingEmail: false,
    email: '',
    formSubmitted: false,
  }
  const param = 'mock-param'
  const actionCreators = [
    [
      setKlarnaSessionId,
      {
        type: 'SET_KLARNA_SESSION_ID',
        sessionId: param,
      },
    ],
    [
      setKlarnaClientToken,
      {
        type: 'SET_KLARNA_CLIENT_TOKEN',
        clientToken: param,
      },
    ],
    [
      setKlarnaAuthorizationToken,
      {
        type: 'SET_KLARNA_AUTHORIZATION_TOKEN',
        authorizationToken: param,
      },
    ],
    [
      setKlarnaFormVisibility,
      {
        type: 'SET_KLARNA_FORM_VISIBILITY',
        shown: param,
      },
    ],
    [
      setLoadFormPending,
      {
        type: 'SET_KLARNA_LOADFORM_PENDING',
        loadFormPending: param,
      },
    ],
    [
      setAuthorizePending,
      {
        type: 'SET_KLARNA_AUTHORIZE_PENDING',
        authorizePending: param,
      },
    ],
    [
      setReAuthorizePending,
      {
        type: 'SET_KLARNA_REAUTHORIZE_PENDING',
        reAuthorizePending: param,
      },
    ],
    [
      setOrderSummaryHash,
      {
        type: 'SET_ORDER_SUMMARY_HASH',
        orderSummaryHash: param,
      },
    ],
    [
      setIsUpdatingKlarnaEmail,
      {
        type: 'SET_KLARNA_UPDATING_EMAIL',
        isUpdatingEmail: param,
      },
    ],
    [
      setKlarnaEmail,
      {
        type: 'SET_KLARNA_EMAIL',
        email: param,
      },
    ],
    [
      resetKlarna,
      {
        type: 'RESET_KLARNA',
      },
    ],
    [
      setKlarnaFormSubmitted,
      {
        type: 'SET_KLARNA_FORM_SUBMITTED',
        formSubmitted: param,
      },
    ],
  ]

  it('should return the initial state', () => {
    const expectedState = initialState

    expect(reducer(undefined, {})).toEqual(expectedState)
  })

  actionCreators.forEach(([actionCreator, action]) => {
    const { type, ...rest } = action

    it(`should handle ${type}`, () => {
      const expectedState = {
        ...initialState,
        ...rest,
      }

      expect(reducer(initialState, actionCreator(param))).toEqual(expectedState)
    })
  })

  it('should handle PRE_CACHE_RESET', () => {
    const expectedState = initialState

    expect(reducer(undefined, { type: 'PRE_CACHE_RESET' })).toEqual(
      expectedState
    )
  })
})
