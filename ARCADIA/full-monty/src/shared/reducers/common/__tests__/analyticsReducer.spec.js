import testReducer from '../analyticsReducer'

const snapshot = (reducer) => expect(reducer).toMatchSnapshot()

describe('Analytics Reducer', () => {
  describe('SET_ADOBE_ANALYTICS_ENV', () => {
    it('environment set in state', () => {
      snapshot(
        testReducer(
          {},
          {
            type: 'SET_ADOBE_ANALYTICS_ENV',
            environment: 'foo',
          }
        )
      )
    })
  })
  describe('SET_PERCENTAGE_VIEWED', () => {
    it('percentagePageViewed set in state', () => {
      snapshot(
        testReducer(
          {},
          {
            type: 'SET_PERCENTAGE_VIEWED',
            percentagePageViewed: 45,
          }
        )
      )
    })
  })
  describe('SET_INT_NAV', () => {
    it('intNav set in state', () => {
      snapshot(
        testReducer(
          {},
          {
            type: 'SET_INT_NAV',
            intNav: {
              type: 'Global Nav',
              details: 'Global Nav:Dresses',
            },
          }
        )
      )
    })
  })
  describe('CLEAR_INT_NAV', () => {
    it('intNav clear from state', () => {
      snapshot(
        testReducer(
          {},
          {
            type: 'CLEAR_INT_NAV',
          }
        )
      )
    })
  })
})
