import deepFreeze from 'deep-freeze'
import pageDataReducer from '../pageDataReducer'

describe('pageDataReducer', () => {
  describe('SET_PAGE_DATA', () => {
    it('should set title and description', () => {
      const pageData = deepFreeze({
        title: 'title',
        description: 'description',
      })
      expect(
        pageDataReducer(null, {
          type: 'SET_PAGE_DATA',
          pageData,
        })
      ).toEqual(pageData)
    })
    it('should set initial title and description', () => {
      const defaultPageData = deepFreeze({
        title: '',
        description: '',
      })
      expect(pageDataReducer(defaultPageData, {})).toEqual(defaultPageData)
    })
  })
})
