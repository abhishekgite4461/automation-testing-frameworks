import jsessionReducer from '../jsessionidReducer'

describe('#jsessionReducer', () => {
  it('sets jsessionid as expected if jsessionid is provided', () => {
    expect(
      jsessionReducer(undefined, {
        type: 'SET_JSESSION_ID',
        jsessionid: 'jsessionid',
      })
    ).toEqual({ value: 'jsessionid' })
  })
  it('sets jsessionid as expected if jsessionid is not provided or undefined', () => {
    expect(jsessionReducer(undefined, { type: 'SET_JSESSION_ID' })).toEqual({
      value: undefined,
    })
    expect(
      jsessionReducer(undefined, {
        type: 'SET_JSESSION_ID',
        jsessionid: undefined,
      })
    ).toEqual({ value: undefined })
    expect(
      jsessionReducer(undefined, { type: 'SET_JSESSION_ID', jsessionid: '' })
    ).toEqual({ value: '' })
  })
  it('overwrites the previous jsessionid value if jsessionid is provided', () => {
    expect(
      jsessionReducer(
        { value: 'jsessionid1' },
        { type: 'SET_JSESSION_ID', jsessionid: 'jsessionid2' }
      )
    ).toEqual({ value: 'jsessionid2' })
  })
})
