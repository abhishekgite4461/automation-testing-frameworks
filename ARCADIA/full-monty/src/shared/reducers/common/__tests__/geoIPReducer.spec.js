import { sampleOne } from 'testcheck'
import geoIPReducer, {
  geoIPActions,
  getUserGeoPreference,
  getRedirectURL,
  getURLPath,
  isValidISO2,
} from '../geoIPReducer'
import { get } from '../../../lib/api-service'
import { SUPPORTED_FEATURES } from '../../../constants/features'
import { error } from '../../../../server/lib/logger'

import {
  getBrandConfigByPreferredISO,
  getEnvDomainsFromConfig,
} from '../../../../server/config'
import topshop from '../../../../server/config/brands/topshop.json'
import topman from '../../../../server/config/brands/topman.json'
import wallis from '../../../../server/config/brands/wallis.json'
import burton from '../../../../server/config/brands/burton.json'
import dorothyPerkins from '../../../../server/config/brands/dorothy-perkins.json'
import evans from '../../../../server/config/brands/evans.json'
import missSelfidge from '../../../../server/config/brands/miss-selfridge.json'

jest.mock('../../../lib/api-service')
jest.mock('../../../../server/lib/logger')

const { FEATURE_GEOIP } = SUPPORTED_FEATURES

describe('geoIPReducer', () => {
  it('defaultState is correct', () => {
    expect(geoIPReducer(undefined, { type: 'hello' })).toEqual({
      redirectURL: '',
      hostname: '',
      geoISO: '',
      storedGeoPreference: '',
    })
  })

  it('returns the state', () => {
    const state = { redirectURL: 'http://foo.com', userGeoPreference: 'US' }
    expect(geoIPReducer(state, { type: 'FOO' })).toBe(state)
  })

  it('sets the redirect url', () => {
    const newRedirectUrl = 'http://newredirect.url'
    const defaultState = geoIPReducer(undefined, { type: 'hello' })
    expect(
      geoIPReducer(defaultState, {
        type: 'SET_GEOIP_REDIRECT_URL',
        redirectURL: newRedirectUrl,
      })
    ).toEqual({ ...defaultState, redirectURL: newRedirectUrl })
    expect(
      geoIPReducer(
        defaultState,
        geoIPActions.setGeoIPRedirectInfo(newRedirectUrl)
      )
    ).toEqual({ ...defaultState, redirectURL: newRedirectUrl })
  })

  it('sets the request data', () => {
    const defaultState = geoIPReducer(undefined, { type: 'hello' })
    expect(
      geoIPReducer(
        defaultState,
        geoIPActions.setGeoIPRequestData({
          hostname: 'foo',
          geoISO: 'bar',
          storedGeoPreference: 'baz',
        })
      )
    ).toEqual({
      ...defaultState,
      hostname: 'foo',
      geoISO: 'bar',
      storedGeoPreference: 'baz',
    })
  })

  it('sets the request data with bad stuff', () => {
    const defaultState = geoIPReducer(undefined, { type: 'hello' })
    expect(
      geoIPReducer(
        defaultState,
        geoIPActions.setGeoIPRequestData({
          hostname: 'foo',
          geoISO: undefined,
          storedGeoPreference: undefined,
        })
      )
    ).toEqual({
      ...defaultState,
      hostname: 'foo',
      geoISO: '',
      storedGeoPreference: '',
    })
  })
})

describe('geoIP Actions', () => {
  let originalProcessBrowser
  beforeEach(() => {
    jest.clearAllMocks()
    originalProcessBrowser = process.browser
    process.browser = false
  })

  afterEach(() => {
    process.browser = originalProcessBrowser
  })

  it('setGeoIPRedirectInfo action creator', () => {
    const redirectURL = 'http://philip.es'
    expect(geoIPActions.setGeoIPRedirectInfo(redirectURL)).toEqual({
      type: 'SET_GEOIP_REDIRECT_URL',
      redirectURL,
    })
  })

  it('setGeoIPRequestData', () => {
    expect(
      geoIPActions.setGeoIPRequestData({
        hostname: 'foo',
        geoISO: 'GB',
        storedGeoPreference: 'US',
      })
    ).toEqual({
      type: 'SET_GEOIP_REQUEST_DATA',
      hostname: 'foo',
      geoISO: 'GB',
      storedGeoPreference: 'US',
    })
  })

  describe('setRedirectURLForPDP', () => {
    it('only works on the server', async () => {
      process.browser = true
      const dispatch = jest.fn()
      await geoIPActions.setRedirectURLForPDP('123')(dispatch, () => {})
      expect(dispatch).not.toHaveBeenCalled()
      expect(get).not.toHaveBeenCalled()
    })

    it('does nothing if `partNumber` is falsy', async () => {
      const dispatch = jest.fn()
      await geoIPActions.setRedirectURLForPDP(undefined)(dispatch, () => {})
      expect(dispatch).not.toHaveBeenCalled()
      expect(get).not.toHaveBeenCalled()
    })

    it('sets a redirect url for a PDP page', async () => {
      const iso = 'US'
      const partNumber = 'TS26K31NGRY'
      const dispatch = jest.fn((x) => x)
      const getState = () => ({
        config: topshop.find((conf) => conf.storeCode === 'tsuk'),
        geoIP: {
          hostname: 'm.topshop.com',
          geoISO: 'US',
          storedGeoPreference: '',
        },
        features: { status: { [FEATURE_GEOIP]: true } },
      })
      get.mockReturnValueOnce(
        Promise.resolve({
          body: {
            sourceUrl:
              'http://ts.stage.arcadiagroup.ltd.uk/en/tsus/product/old-skool-trainers-by-vans-supplied-by-office-6914580',
          },
        })
      )

      await geoIPActions.setRedirectURLForPDP(partNumber)(dispatch, getState)

      expect(get).toHaveBeenCalledWith(`/${iso}/products/${partNumber}`)
      expect(dispatch).toHaveBeenCalledWith({
        type: 'SET_GEOIP_REDIRECT_URL',
        redirectURL:
          'm.us.topshop.com/en/tsus/product/old-skool-trainers-by-vans-supplied-by-office-6914580',
      })
    })

    it('does not dispatch a request to get a PDP page if the user is already on their preferred site and returns a resolved promise', async () => {
      const partNumber = 'TS26K31NGRY'
      const dispatch = jest.fn((x) => x)
      const getState = () => ({
        config: topshop.find((conf) => conf.storeCode === 'tsus'),
        geoIP: {
          hostname: 'm.topshop.com',
          geoISO: 'US',
          storedGeoPreference: 'US',
        },
        features: { status: { [FEATURE_GEOIP]: true } },
      })

      await geoIPActions.setRedirectURLForPDP(partNumber)(dispatch, getState)

      expect(get).not.toHaveBeenCalled()
      expect(dispatch).not.toHaveBeenCalled()
    })

    it('sets a 404 redirect url for a missing PDP page', async () => {
      const iso = 'FR'
      const partNumber = 'TS26K31NGRY'
      const dispatch = jest.fn((x) => x)
      const getState = () => ({
        config: topshop.find((conf) => conf.storeCode === 'tsuk'),
        geoIP: {
          hostname: 'm.topshop.com',
          geoISO: iso,
          storedGeoPreference: iso,
        },
        features: { status: { [FEATURE_GEOIP]: true } },
      })
      get.mockReturnValueOnce(Promise.reject({ success: false }))

      await geoIPActions.setRedirectURLForPDP(partNumber)(dispatch, getState)

      expect(dispatch).toHaveBeenCalledWith({
        type: 'SET_GEOIP_REDIRECT_URL',
        redirectURL: 'm.fr.topshop.com/404',
      })
    })

    it('does not blow up if returned source url is not a valid url and logs an error', async () => {
      const iso = 'US'
      const partNumber = 'TS26K31NGRY'
      const dispatch = jest.fn((x) => x)
      const getState = () => ({
        config: topshop.find((conf) => conf.storeCode === 'tsuk'),
        geoIP: {
          hostname: 'm.topshop.com',
          geoISO: iso,
          storedGeoPreference: '',
        },
        features: { status: { [FEATURE_GEOIP]: true } },
      })
      get.mockReturnValueOnce(
        Promise.resolve({
          body: {
            sourceUrl: 'smelliestofurls',
          },
        })
      )

      await geoIPActions.setRedirectURLForPDP(partNumber)(dispatch, getState)

      expect(error).toHaveBeenCalledWith('GeoIP', {
        message: 'Invalid `sourceUrl` returned in foreign PDP response',
        sourceUrl: 'smelliestofurls',
        request: {
          partNumber,
          iso,
        },
        state: {
          geoIPRedirectURL: 'm.us.topshop.com',
        },
      })
      expect(dispatch).toHaveBeenCalledWith({
        type: 'SET_GEOIP_REDIRECT_URL',
        redirectURL: 'm.us.topshop.com/404',
      })
    })
  })

  describe('getUserGeoPreference', () => {
    it('gets the `geoISO` from state if `storedGeoPreference` is not set', () => {
      const state = {
        geoIP: {
          geoISO: 'GB',
          storedGeoPreference: '',
        },
      }
      expect(getUserGeoPreference(state)).toBe('GB')
    })

    it('gets the `storedGeoPreference` from state if set', () => {
      const state = {
        geoIP: {
          geoISO: 'GB',
          storedGeoPreference: 'US',
        },
      }
      expect(getUserGeoPreference(state)).toBe('US')
    })
  })

  describe('getRedirectURL', () => {
    const configs = [
      ...topshop,
      ...topman,
      ...wallis,
      ...burton,
      ...dorothyPerkins,
      ...evans,
      ...missSelfidge,
    ]

    /*
     * What lies ahead is some generative testing. See full-monty/docs/testcheck.md for info
     * Don't be afraid, give it a good read first.
     */
    const genCapitalLetter = gen
      .intWithin(65, 90)
      .then((int) => String.fromCharCode(int))
    const combine = (a, b, combinator) =>
      a.then((x) => b.then((y) => combinator(x, y)))
    const gen2CaptialLetters = combine(
      genCapitalLetter,
      genCapitalLetter,
      (a, b) => `${a}${b}`
    )
    const genConfig = ({ not }) => {
      const redirectableConfigs = configs.filter(
        (conf) => !conf.preferredISOs.includes(not)
      )
      return gen
        .intWithin(0, redirectableConfigs.length - 1)
        .then((i) => redirectableConfigs[i])
    }
    const genMainDevEnvDomain = ({ config, hostnameIndex, preferredISO }) =>
      hostnameIndex === 0
        ? gen.oneOf([
            `stage.${config.domains.prod[hostnameIndex]}`,
            `showcase.${config.domains.prod[hostnameIndex]}`,
            `preprod.${config.domains.prod[hostnameIndex]}`,
            `perf.${config.domains.prod[hostnameIndex]}`,
            `integration.${config.domains.prod[hostnameIndex]}`,
            `local.${config.domains.prod[hostnameIndex]}`,
          ])
        : gen.return(
            `${config.brandCode}.stage.arcadiagroup.ltd.uk${
              config.region !== 'uk' ? `?prefShipCtry=${preferredISO}` : ''
            }`
          )
    const genAWSDomain = ({ config, hostnameIndex }) =>
      gen.alphaNumString
        .notEmpty()
        .then(
          (env) =>
            `${env}-${config.domains.prod[hostnameIndex].replace(
              /\./g,
              '-'
            )}.digital.arcadiagroup.co.uk`
        )
    const genConfigAndDomain = ({ domainType, devEnv, header, cookie }) =>
      genConfig({ not: cookie || header }).then((config) => {
        const genHostnameIndex =
          domainType === 'mobile'
            ? gen.return(0)
            : domainType === 'desktop'
              ? gen.return(1)
              : gen.intWithin(0, config.domains.prod.length - 1)

        return genHostnameIndex.then((index) => ({
          config,
          domain:
            devEnv === 'main'
              ? genMainDevEnvDomain({
                  config,
                  hostnameIndex: index,
                  preferredISO: cookie || header,
                })
              : devEnv === 'aws'
                ? genAWSDomain({ config, hostnameIndex: index })
                : config.domains.prod[index],
          header,
          cookie,
        }))
      })

    const genRedirectableISO = (input) => {
      const config = configs
        .filter((conf) => conf.brandCode === input.config.brandCode)
        .find((conf) => conf.domains.prod[0] !== input.config.domains.prod[0])

      return gen
        .intWithin(0, config.preferredISOs.length - 1)
        .then((i) => config.preferredISOs[i])
    }

    const genHeaderOrCookie = () =>
      gen.oneOf([
        gen.object({
          cookie: '',
          header: gen2CaptialLetters,
        }),
        gen.object({
          cookie: gen2CaptialLetters,
          header: gen2CaptialLetters,
        }),
      ])

    const genInput = ({
      domainType = null,
      featureFlag = true,
      headerOverride = null,
      cookieOverride = null,
      redirectURL,
      devEnv = false,
    } = {}) =>
      genHeaderOrCookie().then(({ header, cookie }) =>
        genConfigAndDomain({ domainType, devEnv, cookie, header }).then(
          ({ config, domain, header, cookie }) =>
            gen.object({
              config,
              domain,
              header: headerOverride === null ? header : headerOverride,
              cookie: cookieOverride === null ? cookie : cookieOverride,
              featureFlag,
              redirectURL,
            })
        )
      )

    const testGetRedirectURL = ({
      config,
      domain,
      redirectURL,
      header,
      cookie,
      featureFlag,
    }) => {
      const state = {
        geoIP: {
          redirectURL: redirectURL || '',
          hostname: domain,
          geoISO: header || '',
          storedGeoPreference: cookie || '',
        },
        config,
        features: {
          status: featureFlag ? { [FEATURE_GEOIP]: FEATURE_GEOIP } : {},
        },
      }

      return getRedirectURL(state)
    }

    it('returns `redirectURL` if set', () => {
      const input = sampleOne(genInput())
      input.redirectURL = 'foo'

      expect(testGetRedirectURL(input)).toBe(input.redirectURL)
    })

    const genInput1 = genInput({ featureFlag: false })
    check.it(
      'feature flag disabled never returns a redirect URL',
      genInput1,
      (input) => {
        expect(testGetRedirectURL(input)).toBe(undefined)
      }
    )

    const genDirtyISO = gen.string.suchThat((x) => x !== '' && !isValidISO2(x))
    const genInputDirtyHeader = genInput({
      headerOverride: genDirtyISO,
      cookieOverride: '',
    })
    check.it(
      'dirty inputs provided by akamai as the header value disable the feature',
      genInputDirtyHeader,
      (input) => {
        expect(testGetRedirectURL(input)).toBe(undefined)
      }
    )

    const genInputDirtyCookie = genInput({
      headerOverride: 'GB',
      cookieOverride: genDirtyISO,
    })
    check.it(
      'dirty cookie stored returns no redirectURL',
      genInputDirtyCookie,
      (input) => {
        expect(testGetRedirectURL(input)).toBe(undefined)
      }
    )

    const genInput2 = genInput({ headerOverride: '' })
    check.it(
      'if akamai GeoIP disabled, i.e. geoISO is not set, then never return a redirect URL',
      genInput2,
      (input) => {
        expect(testGetRedirectURL(input)).toBe(undefined)
      }
    )

    const genRedirectURLWithDirtyInputs = gen
      .oneOf([genInputDirtyHeader, genInputDirtyCookie])
      .then((input) => ({ ...input, redirectURL: 'foo' }))
    check.it(
      'redirectURL should not be returned if feature is disabled or no header or dirty data',
      genRedirectURLWithDirtyInputs,
      (input) => {
        expect(testGetRedirectURL(input)).toBe(undefined)
      }
    )

    const genInputHeader = genInput({ cookieOverride: '' }).then((input) =>
      gen.object({ ...input, header: gen.oneOf(input.config.preferredISOs) })
    )
    check.it(
      'first time user requests site of their requesting country',
      genInputHeader,
      (input) => {
        expect(testGetRedirectURL(input)).toBe(undefined)
      }
    )

    const genInputCookie = genInput().then((input) =>
      gen.object({ ...input, cookie: gen.oneOf(input.config.preferredISOs) })
    )
    check.it(
      'user requests preferred site and has previously chosen to stay on it',
      genInputCookie,
      (input) => {
        expect(testGetRedirectURL(input)).toBe(undefined)
      }
    )

    const genInputRedirectableHeader = genInput({ cookieOverride: '' }).then(
      (input) => gen.object({ ...input, header: genRedirectableISO(input) })
    )
    check.it(
      'first time user requesting a foreign site',
      genInputRedirectableHeader,
      (input) => {
        expect(testGetRedirectURL(input)).toEqual(expect.any(String))
      }
    )

    const genInputHeaderOrCookie = (opts) =>
      genInput(opts).then((input) => {
        const genISO = genRedirectableISO(input)

        return gen
          .oneOf([
            gen.object({
              cookie: '',
              header: genISO,
            }),
            gen.object({
              cookie: genISO,
              header: gen2CaptialLetters,
            }),
          ])
          .then(({ cookie, header }) =>
            gen.object({
              ...input,
              header,
              cookie,
            })
          )
      })

    check.it(
      'if requested with "m." domain then `redirectDomain` should be mobile',
      genInputHeaderOrCookie({ domainType: 'mobile' }),
      (input) => {
        const userGeoPreference = input.cookie || input.header
        const preferredSiteConfig = getBrandConfigByPreferredISO(
          userGeoPreference,
          input.config.brandCode
        )
        const [mobileDomain] = getEnvDomainsFromConfig(
          preferredSiteConfig,
          input.domain
        )
        expect(testGetRedirectURL(input)).toBe(mobileDomain)
      }
    )

    check.it(
      'if requested with "www." domain then `redirectDomain` should be desktop',
      genInputHeaderOrCookie({ domainType: 'desktop' }),
      (input) => {
        const userGeoPreference = input.cookie || input.header
        const preferredSiteConfig = getBrandConfigByPreferredISO(
          userGeoPreference,
          input.config.brandCode
        )
        const [, wwwDomain] = getEnvDomainsFromConfig(
          preferredSiteConfig,
          input.domain
        )
        expect(testGetRedirectURL(input)).toBe(wwwDomain)
      }
    )

    it('returns `undefined` when requested site is the same as the user geo', () => {
      const input = {
        config: topshop[0],
        domain: 'm.topshop.com',
        redirectURL: '',
        header: 'GB',
        cookie: '',
        featureFlag: true,
      }
      expect(testGetRedirectURL(input)).toBe(undefined)
    })

    // stage.m.fr.topshop.com
    const genInputWithMainDevMobile = genInput({
      devEnv: 'main',
      domainType: 'mobile',
      cookieOverride: '',
    }).then((input) =>
      gen.object({ ...input, header: genRedirectableISO(input) })
    )
    check.it(
      'if request is from a main dev environment then redirect URL should also be',
      genInputWithMainDevMobile,
      (input) => {
        const env = input.domain.match(/^([^.]+)\./)[1]
        expect(testGetRedirectURL(input)).toMatch(env)
      }
    )

    // ts.stage.arcadiagroup.ltd.uk?prefShipCtry=FR
    const genInputWithMainDevDesktop = genInput({
      devEnv: 'main',
      domainType: 'desktop',
      cookieOverride: '',
    }).then((input) =>
      gen.object({ ...input, header: genRedirectableISO(input) })
    )
    check.it(
      'if request is from a desktop main dev environment then redirect URL should also be',
      genInputWithMainDevDesktop,
      (input) => {
        const env = input.domain.match(/^[a-z]{2}\.([^.]+)/)[1]
        const redirectURL = testGetRedirectURL(input)
        expect(redirectURL).toMatch(env)
        expect(redirectURL).toMatch(
          `?prefShipCtry=${input.cookie || input.header}`
        )
        expect(redirectURL).toMatch('.arcadiagroup.ltd.uk')
      }
    )

    // foo-m-fr-topshop-com.digitial.arcadiagroup.co.uk
    const genInputWithAWSMobile = genInput({
      devEnv: 'aws',
      domainType: 'mobile',
      cookieOverride: '',
    }).then((input) =>
      gen.object({ ...input, header: genRedirectableISO(input) })
    )
    check.it(
      'if request is from an AWS dev environment then redirect URL should also be',
      genInputWithAWSMobile,
      (input) => {
        const env = input.domain.match(/^([^-]+)-/)[1]
        const res = testGetRedirectURL(input)
        expect(res).toMatch(env)
        expect(res).toMatch('.digital.arcadiagroup.co.uk')
      }
    )
  })
})

describe('getURLPath', () => {
  it('returns the url path or `/` when given a url', () => {
    expect(getURLPath('http://m.us.lel.lol.topshop.com/i/am/a/path')).toBe(
      '/i/am/a/path'
    )
    expect(getURLPath('m.us.lel.lol.topshop.com/i/am/a/path')).toBe(
      '/i/am/a/path'
    )
    expect(
      getURLPath('m.us.lel.lol.topshop.com/i/am/a/path#hashstringappeared')
    ).toBe('/i/am/a/path#hashstringappeared')
    expect(
      getURLPath('m.us.lel.lol.topshop.com/i/am/a/path?query=string&key=value')
    ).toBe('/i/am/a/path?query=string&key=value')
    expect(getURLPath('m.us.lel.lol.topshop.co.uk/i/am/a/path')).toBe(
      '/i/am/a/path'
    )
    expect(getURLPath('topshop.com')).toBe('/')
    expect(getURLPath('https://us.subdomain.topshop.com')).toBe('/')
    expect(getURLPath('https://philip.es')).toBe('/')
    expect(getURLPath('topshop.co.uk')).toBe('/')
    expect(getURLPath('topshop.london')).toBe('/')
  })

  it('returns undefined when a non valid url is given', () => {
    expect(getURLPath('smellyurl')).toBe(undefined)
    expect(getURLPath('localhost:8080')).toBe(undefined)
  })
})
