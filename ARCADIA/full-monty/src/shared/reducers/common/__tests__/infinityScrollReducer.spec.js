import testReducer from '../infinityScrollReducer'
import { getMockStoreWithInitialReduxState } from '../../../../../test/unit/helpers/get-redux-mock-store'

describe('Infinity Scroll Reducer', () => {
  it('Default values', () => {
    const state = getMockStoreWithInitialReduxState().getState()
    expect(state.infinityScroll.currentPage).toBe(1)
    expect(state.infinityScroll.isActive).toBe(true)
  })
  describe('NEXT_PAGE_INFINITY', () => {
    it('should increase `currentPage` by 1', () => {
      expect(
        testReducer(
          { currentPage: 1 },
          {
            type: 'NEXT_PAGE_INFINITY',
          }
        )
      ).toEqual({
        currentPage: 2,
      })
    })
  })
  describe('SET_INFINITY_PAGE', () => {
    it('should set `currentPage`', () => {
      expect(
        testReducer(
          { currentPage: 1 },
          {
            type: 'SET_INFINITY_PAGE',
            page: 13,
          }
        )
      ).toEqual({
        currentPage: 13,
      })
    })
  })
  describe('SET_INFINITY_ACTIVE', () => {
    it('should set `isActive` to true', () => {
      expect(
        testReducer(
          { isActive: false },
          {
            type: 'SET_INFINITY_ACTIVE',
          }
        )
      ).toEqual({
        isActive: true,
      })
    })
  })
  describe('SET_INFINITY_INACTIVE', () => {
    it('should set `isActive` to false', () => {
      expect(
        testReducer(
          { isActive: true },
          {
            type: 'SET_INFINITY_INACTIVE',
          }
        )
      ).toEqual({
        isActive: false,
      })
    })
  })
  describe('PRESERVE_SCROLL', () => {
    it('should set `preservedScroll` to value', () => {
      const preservedScroll = 156
      expect(
        testReducer(
          {},
          {
            type: 'PRESERVE_SCROLL',
            preservedScroll,
          }
        )
      ).toEqual({
        preservedScroll,
      })
    })
  })
  describe('CLEAR_PRESERVE_SCROLL', () => {
    it('should set `preservedScroll` to 0', () => {
      expect(
        testReducer(
          {},
          {
            type: 'CLEAR_PRESERVE_SCROLL',
          }
        )
      ).toEqual({
        preservedScroll: 0,
      })
    })
  })
  describe('SET_PRODUCTS', () => {
    it('should set `preservedScroll` to 0 and `isActive` to true', () => {
      const preservedScroll = 156
      expect(
        testReducer(
          { isActive: false, preservedScroll },
          {
            type: 'SET_PRODUCTS',
          }
        )
      ).toEqual({
        preservedScroll: 0,
        isActive: true,
      })
    })
  })
})
