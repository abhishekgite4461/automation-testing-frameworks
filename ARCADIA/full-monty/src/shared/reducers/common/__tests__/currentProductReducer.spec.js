import currentProductReducer from '../currentProduct'

describe('Current Product', () => {
  describe('CLEAR_PRODUCT', () => {
    it('should return an empty object', () => {
      const currentProduct = {
        productId: 30263823,
      }
      const reducer = currentProductReducer(currentProduct, {
        type: 'CLEAR_PRODUCT',
      })
      expect(reducer).toEqual({})
    })
  })

  describe('SET_PRODUCT', () => {
    it('should replace with supplied product', () => {
      const currentProduct = {
        productId: 30263823,
      }
      const product = {
        productId: 30164534,
      }
      const reducer = currentProductReducer(currentProduct, {
        type: 'SET_PRODUCT',
        product,
      })
      expect(reducer).toBe(product)
    })
  })

  describe('UPDATE_SEE_MORE_URL', () => {
    it('should add `seeMoreUrl`s to correct see more links', () => {
      const currentProduct = {
        seeMoreValue: [
          {
            seeMoreLabel: 'Jeans',
            seeMoreLink: '/_/N-2bswZ1xjf',
          },
          {
            seeMoreLabel: 'Spray On Jeans',
            seeMoreLink: '/_/N-2bu3Z1xjf',
          },
        ],
      }

      const reducer = currentProductReducer(currentProduct, {
        type: 'UPDATE_SEE_MORE_URL',
        seeMoreLink: '/_/N-2bu3Z1xjf',
        seeMoreUrl:
          'www.topman.com.arcadiagroup.co.uk/Spray-On-Jeans/_/N-2bu3Z1xjf',
      })
      expect(reducer).toEqual({
        seeMoreValue: [
          {
            seeMoreLabel: 'Jeans',
            seeMoreLink: '/_/N-2bswZ1xjf',
          },
          {
            seeMoreLabel: 'Spray On Jeans',
            seeMoreLink: '/_/N-2bu3Z1xjf',
            seeMoreUrl:
              'www.topman.com.arcadiagroup.co.uk/Spray-On-Jeans/_/N-2bu3Z1xjf',
          },
        ],
      })
    })
  })
})
