import reducer from '../featuresReducer'

jest.mock('../../../constants/features', () => ({
  ALL_FEATURES: {
    FEATURE_A: 'FEATURE_A',
    FEATURE_B: 'FEATURE_B',
    FEATURE_C: 'FEATURE_C',
  },
}))

describe('featuresReducer', () => {
  const initialState = {
    status: {},
    overrides: null,
  }

  it('should return the initial state', () => {
    const expectedState = initialState

    expect(reducer(undefined, {})).toEqual(expectedState)
  })

  it('should handle SET_FEATURES', () => {
    const enabled = ['FEATURE_C']
    const overrides = {
      FEATURE_B: true,
    }
    const expectedState = {
      ...initialState,
      status: {
        FEATURE_A: false,
        FEATURE_B: true,
        FEATURE_C: true,
      },
      overrides,
    }
    const action = {
      type: 'SET_FEATURES',
      enabled,
      overrides,
    }

    expect(reducer(initialState, action)).toEqual(expectedState)
  })

  it('should handle TOGGLE_FEATURE', () => {
    const currentState = {
      status: {
        FEATURE_A: false,
      },
      overrides: {},
    }
    const expectedState = {
      status: {
        FEATURE_A: true,
      },
      overrides: {
        FEATURE_A: true,
      },
    }
    const action = {
      type: 'TOGGLE_FEATURE',
      feature: 'FEATURE_A',
    }

    expect(reducer(currentState, action)).toEqual(expectedState)

    currentState.status.FEATURE_A = true
    expectedState.status.FEATURE_A = false
    expectedState.overrides.FEATURE_A = false

    expect(reducer(currentState, action)).toEqual(expectedState)
  })

  it('should handle REMOVE_OVERRIDES', () => {
    const currentState = {
      overrides: {
        SOME_FEATURE: true,
      },
    }
    const expectedState = {
      overrides: {},
    }
    const action = {
      type: 'REMOVE_OVERRIDES',
    }

    expect(reducer(currentState, action)).toEqual(expectedState)
  })
})
