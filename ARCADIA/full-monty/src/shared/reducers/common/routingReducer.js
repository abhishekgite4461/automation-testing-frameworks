import createReducer from '../../lib/create-reducer'
import { UPDATE_LOCATION } from 'react-router-redux'

const updateLocationUrl = (visited, url) => {
  if (visited.length === 0) return [url]
  return visited[visited.length - 1] !== url ? [...visited, url] : [...visited]
}
const removeFromVisited = (visited, index) => {
  if (index >= visited.length) return visited
  return [...visited.slice(0, index), ...visited.slice(index + 1)]
}

export default createReducer(
  { location: { pathname: '' }, visited: [] },
  {
    [UPDATE_LOCATION]: (state, { payload }) => ({
      ...state,
      visited: updateLocationUrl(state.visited, payload.pathname),
      location: {
        ...state.location,
        ...payload,
      },
    }),
    UPDATE_LOCATION_SERVER: (state, { location }) => ({
      ...state,
      location,
    }),
    URL_REDIRECT_SERVER: (state, { redirect }) => ({
      ...state,
      redirect,
    }),
    SET_PAGE_STATUS_CODE: (state, { statusCode }) => ({
      ...state,
      pageStatusCode: statusCode,
    }),
    REMOVE_FROM_VISITED: (state, { index }) => ({
      ...state,
      visited: removeFromVisited(state.visited, index),
    }),
  }
)
