import createReducer from '../../lib/create-reducer'

const initialState = {
  title: '',
  description: '',
}

export default createReducer(initialState, {
  SET_PAGE_DATA: (state, { pageData: { description, title } }) => ({
    ...state,
    description,
    title,
  }),
})
