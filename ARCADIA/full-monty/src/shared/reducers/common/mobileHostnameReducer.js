import createReducer from '../../lib/create-reducer'

export default createReducer(
  { isMobile: false },
  {
    SET_IS_MOBILE_HOSTNAME: (state, { isMobile }) => ({
      ...state,
      isMobile,
    }),
  }
)
