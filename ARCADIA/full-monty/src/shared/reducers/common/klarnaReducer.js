import createReducer from '../../lib/create-reducer'
import { isStorageSupported } from '../../../client/lib/storage'

const initialState = {
  sessionId: '',
  clientToken: '',
  loadFormPending: false,
  isUpdatingEmail: false,
  email: '',
  formSubmitted: false,
}

export default createReducer(initialState, {
  SET_KLARNA_SESSION_ID: (state, { sessionId }) => ({ ...state, sessionId }),
  SET_KLARNA_CLIENT_TOKEN: (state, { clientToken }) => ({
    ...state,
    clientToken,
  }),
  SET_KLARNA_FORM_VISIBILITY: (state, { shown }) => ({ ...state, shown }),
  SET_KLARNA_AUTHORIZATION_TOKEN: (state, { authorizationToken }) => ({
    ...state,
    authorizationToken,
  }),
  SET_KLARNA_LOADFORM_PENDING: (state, { loadFormPending }) => ({
    ...state,
    loadFormPending,
  }),
  SET_KLARNA_AUTHORIZE_PENDING: (state, { authorizePending }) => ({
    ...state,
    authorizePending,
  }),
  SET_KLARNA_REAUTHORIZE_PENDING: (state, { reAuthorizePending }) => ({
    ...state,
    reAuthorizePending,
  }),
  SET_ORDER_SUMMARY_HASH: (state, { orderSummaryHash }) => {
    if (isStorageSupported() && process.browser) {
      // TODO refactor this, no side effects in reducer!
      if (orderSummaryHash) {
        window.localStorage.setItem('orderHash', orderSummaryHash)
      } else {
        window.localStorage.removeItem('orderHash')
      }
    }
    return {
      ...state,
      orderSummaryHash,
    }
  },
  SET_KLARNA_UPDATING_EMAIL: (state, { isUpdatingEmail }) => ({
    ...state,
    isUpdatingEmail,
  }),
  SET_KLARNA_EMAIL: (state, { email }) => ({ ...state, email }),
  RESET_KLARNA: () => {
    if (isStorageSupported() && process.browser)
      window.localStorage.removeItem('orderHash') // TODO refactor
    return initialState
  },
  SET_KLARNA_FORM_SUBMITTED: (state, { formSubmitted }) => ({
    ...state,
    formSubmitted,
  }),
  PRE_CACHE_RESET: () => initialState,
})
