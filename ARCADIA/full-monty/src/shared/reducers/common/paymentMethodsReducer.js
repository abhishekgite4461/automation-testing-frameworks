import createReducer from '../../lib/create-reducer'

const initialState = []

export default createReducer(initialState, {
  GET_PAYMENT_METHODS_API_SUCCESS: (state, { payload }) => payload,
  GET_PAYMENT_METHODS_API_FAILURE: () => [],
  DEL_PAYMENT_METHODS_API_SUCCESS: (state, { payload }) => payload,
})
