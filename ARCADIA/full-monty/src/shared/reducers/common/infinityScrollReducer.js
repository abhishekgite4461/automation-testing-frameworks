import createReducer from '../../lib/create-reducer'

export default createReducer(
  { currentPage: 1, isActive: true },
  {
    NEXT_PAGE_INFINITY: (state) => ({
      ...state,
      currentPage: state.currentPage + 1,
    }),
    SET_INFINITY_PAGE: (state, action) => ({
      ...state,
      currentPage: action.page,
    }),
    SET_INFINITY_ACTIVE: (state) => ({
      ...state,
      isActive: true,
    }),
    SET_INFINITY_INACTIVE: (state) => ({
      ...state,
      isActive: false,
    }),
    PRESERVE_SCROLL: (state, { preservedScroll }) => ({
      ...state,
      preservedScroll,
    }),
    CLEAR_PRESERVE_SCROLL: (state) => ({
      ...state,
      preservedScroll: 0,
    }),
    SET_PRODUCTS: (state) => ({
      ...state,
      preservedScroll: 0,
      isActive: true,
    }),
  }
)
