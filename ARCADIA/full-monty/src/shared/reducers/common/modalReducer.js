import createReducer from '../../lib/create-reducer'

export default createReducer(
  { open: false, mode: 'normal', type: 'dialog', children: [] },
  {
    TOGGLE_MODAL: (state, { entryPoint, scrollPositionOnOpenModal }) => ({
      ...state,
      open: !state.open,
      entryPoint: state.open ? null : entryPoint,
      scrollPositionOnOpenModal: !state.open
        ? scrollPositionOnOpenModal
        : state.scrollPositionOnOpenModal,
    }),
    SET_MODAL_MODE: (state, { mode }) => ({ ...state, mode }),
    SET_MODAL_TYPE: (state, { modalType }) => ({ ...state, type: modalType }),
    SET_MODAL_CANCELLED: (state, { cancelled }) => ({ ...state, cancelled }),
    SET_MODAL_CHILDREN: (state, { children }) => ({ ...state, children }),
    CLEAR_MODAL_CHILDREN: (state) => ({ ...state, children: [] }),
    OPEN_MODAL: (state, { entryPoint, scrollPositionOnOpenModal }) => ({
      ...state,
      open: true,
      entryPoint,
      scrollPositionOnOpenModal,
    }),
    CLOSE_MODAL: (state, { shouldScrollToPreviousPosition = true }) => {
      if (!state.open) {
        return state
      }
      return {
        ...state,
        open: false,
        entryPoint: null,
        scrollPositionOnOpenModal: shouldScrollToPreviousPosition
          ? state.scrollPositionOnOpenModal
          : null,
      }
    },
    RESET_SCROLL_POSITION_ON_OPEN_MODAL: (state) => ({
      ...state,
      scrollPositionOnOpenModal: null,
    }),
  }
)
