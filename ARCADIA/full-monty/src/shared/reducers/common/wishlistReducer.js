import { identity } from 'ramda'
import createReducer from '../../lib/create-reducer'

// Helper functions
const filterProductList = (productList, productId) =>
  Array.isArray(productList)
    ? productList.filter((item) => item.productId !== productId)
    : productList
const filterItemDetails = (itemDetails, productId) =>
  Array.isArray(itemDetails)
    ? itemDetails.filter(
        (item) => item.parentProductId !== productId.toString()
      )
    : itemDetails

const initialState = {
  itemDetails: null,
}

export default createReducer(initialState, {
  ADD_TO_WISHLIST_SUCCESS: (state, { wishlist }) => ({
    ...state,
    ...wishlist,
  }),
  ADD_TO_WISHLIST_FAILURE: identity,
  GET_ALL_WISHLISTS_SUCCESS: identity,
  GET_WISHLIST_SUCCESS: (state, { wishlist }) => ({
    ...state,
    ...wishlist,
  }),
  GET_WISHLIST_FAILURE: identity,
  REMOVE_ITEM_FROM_WISHLIST: (state, { productId }) => ({
    ...state,
    // @TODO: see if the response from removing an item can include the updated list of items.
    // Ideally the server would return the sole source of truth instead of the store being altered.
    productList: filterProductList(state.productList, productId),
    itemDetails: filterItemDetails(state.itemDetails, productId),
  }),
  WISHLIST_STORE_PRODUCT_ID: (state, { productId }) => ({
    ...state,
    pendingProductId: productId,
  }),
  WISHLIST_DELETE_STORED_PRODUCT_ID: (state) => ({
    ...state,
    pendingProductId: null,
  }),
  CLEAR_WISHLIST: () => ({
    productList: null,
    itemDetails: null,
  }),
  SET_MOVING_PRODUCT_TO_WISHLIST: (state, { productId }) => ({
    ...state,
    movingProductToWishlist: productId,
  }),
  CLEAR_MOVING_PRODUCT_TO_WISHLIST: (state) => ({
    ...state,
    movingProductToWishlist: null,
  }),
})
