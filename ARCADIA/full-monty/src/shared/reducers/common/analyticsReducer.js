import createReducer from '../../lib/create-reducer'

export default createReducer(
  { percentagePageViewed: {} },
  {
    SET_ADOBE_ANALYTICS_ENV: (state, { environment }) => ({
      ...state,
      environment,
    }),
    SET_PERCENTAGE_VIEWED: (state, { percentagePageViewed }) => ({
      ...state,
      percentagePageViewed,
    }),
    SET_INT_NAV: (state, { intNav }) => ({ ...state, intNav }),
    CLEAR_INT_NAV: (state) => ({ ...state, intNav: {} }),
  }
)
