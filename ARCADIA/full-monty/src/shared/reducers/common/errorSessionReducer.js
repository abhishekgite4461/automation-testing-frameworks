import createReducer from '../../lib/create-reducer'

const initialState = { sessionExpired: false }

export default createReducer(initialState, {
  SESSION_EXPIRED: () => ({ sessionExpired: true }),
  RESET_SESSION_EXPIRED: () => initialState,
})
