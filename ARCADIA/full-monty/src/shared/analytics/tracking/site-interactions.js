import dataLayer from '../dataLayer'

export const pushApiResponseEvent = (data) => {
  if (process.browser) {
    dataLayer.push(data, null, 'apiResponse')
  }
}

export const pushClickEvent = (data) => {
  if (process.browser) {
    const base = {
      ec: '',
      ea: '',
      el: window.location.href,
      ev: '',
    }
    dataLayer.push(
      {
        ...base,
        ...data,
      },
      null,
      'clickevent'
    )
  }
}

export const pushErrorMessage = (errorMessage) => {
  if (process.browser) {
    dataLayer.push({ errorMessage }, null, 'errorMessage')
  }
}

export const analyticsGlobalNavClickEvent = (action) => {
  return pushClickEvent({
    ec: 'globalnavigation',
    ea: action,
  })
}

export const analyticsPlpClickEvent = (action) => {
  return pushClickEvent({
    ec: 'plp',
    ea: action,
  })
}

export const analyticsSearchClickEvent = (action) => {
  return pushClickEvent({
    ec: 'search',
    ea: action,
  })
}

export const analyticsPdpClickEvent = (action) => {
  return pushClickEvent({
    ec: 'pdp',
    ea: action,
  })
}

export const analyticsShoppingBagClickEvent = (action) => {
  return pushClickEvent({
    ec: 'shoppingBag',
    ...action,
  })
}

export const analyticsRegisterClickEvent = (action) => {
  return pushClickEvent({
    ec: 'register',
    ...action,
  })
}

export const analyticsErrorEvent = (action) => {
  return dataLayer.push(action, null, 'errorMessage')
}

export const analyticsLoginClickEvent = (action) => {
  return pushClickEvent({
    ec: 'signin',
    ...action,
  })
}

export const analyticsFormErrorEvent = (formId, errorMessage) => {
  analyticsErrorEvent({
    form: formId,
    errorMessage,
  })
}

export const analyticsBagDrawerCheckoutClickEvent = (pageType, action) => {
  return pushClickEvent({
    ec: pageType,
    ea: action,
  })
}

/**
 * see tag manager docs: https://developers.google.com/tag-manager/enhanced-ecommerce#product-clicks
 */
export const pushProductClickEvent = (productObj, list) => {
  if (process.browser && productObj) {
    const { name, id, price, brand, category, position } = productObj
    dataLayer.push(
      {
        ecommerce: {
          click: {
            actionField: list ? { list } : {},
            products: [
              {
                name,
                id,
                price,
                brand,
                category,
                position,
              },
            ],
          },
        },
      },
      'productClickSchema',
      'productClick'
    )
  }
}

export const pushDisplayEvent = (payload, eventName) => {
  if (process.browser) {
    dataLayer.push(payload, null, eventName)
  }
}

export const pushDeliveryOptionChangeEvent = (deliveryOption) => {
  if (process.browser) {
    dataLayer.push({ deliveryOption }, null, 'deliveryOptionChanged')
  }
}

export const pushInputValidationStatus = ({ id, validationStatus }) => {
  if (process.browser) {
    dataLayer.push({ id, validationStatus }, null, 'formValidation')
  }
}

export const pushDeliveryMethodChangeEvent = ({ deliveryMethod }) => {
  if (process.browser) {
    dataLayer.push({ deliveryMethod }, null, 'deliveryMethodChanged')
  }
}

export const pushFilterUsedEvent = (payload) => {
  if (process.browser) {
    dataLayer.push(payload, null, 'filterUsed')
  }
}
