import * as analyticsConstants from '../analytics-constants'

describe('ANALYTICS_ACTION', () => {
  it('should be the correct set of action type strings', () => {
    expect(analyticsConstants.ANALYTICS_ACTION).toEqual({
      SEND_API_RESPONSE_EVENT: 'MONTY/ANALYTICS.SEND_API_RESPONSE_EVENT',
      SEND_CLICK_EVENT: 'MONTY/ANALYTICS.SEND_CLICK_EVENT',
      SEND_ERROR_MESSAGE: 'MONTY/ANALYTICS.SEND_ERROR_MESSAGE',
      SEND_PRODUCT_CLICK_EVENT: 'MONTY/ANALYTICS.SEND_PRODUCT_CLICK_EVENT',
      SEND_DISPLAY_EVENT: 'MONTY/ANALYTICS.SEND_DISPLAY_EVENT',
      SEND_DELIVERY_OPTION_CHANGE_EVENT:
        'MONTY/ANALYTICS.SEND_DELIVERY_OPTION_CHANGE_EVENT',
      SEND_INPUT_VALIDATION_STATUS:
        'MONTY/ANALYTICS.SEND_INPUT_VALIDATION_STATUS',
      SEND_PAYMENT_METHOD_CHANGE_EVENT:
        'MONTY/ANALYTICS.SEND_PAYMENT_METHOD_CHANGE_EVENT',
      SEND_DELIVERY_METHOD_CHANGE_EVENT:
        'MONTY/ANALYTICS.SEND_DELIVERY_METHOD_CHANGE_EVENT',
      SEND_FILTER_USED_EVENT: 'MONTY/ANALYTICS.SEND_FILTER_USED_EVENT',
    })
  })
})

describe('GTM_EVENT', () => {
  it('should be the correct set of values', () => {
    expect(analyticsConstants.GTM_EVENT).toMatchSnapshot()
  })
})

describe('GTM_TRIGGER', () => {
  it('should be the correct set of values', () => {
    expect(analyticsConstants.GTM_TRIGGER).toMatchSnapshot()
  })
})
