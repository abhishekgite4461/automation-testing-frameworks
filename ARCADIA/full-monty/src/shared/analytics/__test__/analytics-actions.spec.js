import { ANALYTICS_ACTION, GTM_ACTION } from '../analytics-constants'
import * as analyticsActions from '../analytics-actions'

describe('Analytics actions', () => {
  describe('sendAnalyticsApiResponseEvent()', () => {
    it('should return the correct action', () => {
      const payload = {
        event: 'apiResponse',
        apiEndpoint: '/getAccount',
        responseCode: 200,
      }

      expect(analyticsActions.sendAnalyticsApiResponseEvent(payload)).toEqual({
        type: ANALYTICS_ACTION.SEND_API_RESPONSE_EVENT,
        payload,
      })
    })
  })
  describe('sendAnalyticsClickEvent()', () => {
    it('should return the correct action', () => {
      const payload = 'foo'

      expect(analyticsActions.sendAnalyticsClickEvent(payload)).toEqual({
        type: ANALYTICS_ACTION.SEND_CLICK_EVENT,
        payload,
      })
    })
  })

  describe('sendAnalyticsDisplayEvent()', () => {
    it('should return the correct action', () => {
      const payload = 'foo'
      const eventName = 'bar'

      expect(
        analyticsActions.sendAnalyticsDisplayEvent(payload, eventName)
      ).toEqual({
        type: ANALYTICS_ACTION.SEND_DISPLAY_EVENT,
        payload,
        eventName,
      })
    })
  })

  describe('sendAnalyticsErrorMessage()', () => {
    it('should return the correct action', () => {
      const errorMessage = 'I am erroneous.'

      expect(analyticsActions.sendAnalyticsErrorMessage(errorMessage)).toEqual({
        type: ANALYTICS_ACTION.SEND_ERROR_MESSAGE,
        errorMessage,
      })
    })
  })

  describe('sendAnalyticsProductClickEvent()', () => {
    it('should return the correct action', () => {
      const payload = { foo: 'bar' }
      expect(analyticsActions.sendAnalyticsProductClickEvent(payload)).toEqual({
        type: ANALYTICS_ACTION.SEND_PRODUCT_CLICK_EVENT,
        payload,
      })
    })
  })

  describe('sendAnalyticsDeliveryOptionChangeEvent()', () => {
    it('should return the correct action', () => {
      const deliveryLocationType = "Carrier pigeon's nest"

      expect(
        analyticsActions.sendAnalyticsDeliveryOptionChangeEvent(
          deliveryLocationType
        )
      ).toEqual({
        type: ANALYTICS_ACTION.SEND_DELIVERY_OPTION_CHANGE_EVENT,
        deliveryLocationType,
      })
    })
  })

  describe('sendAnalyticsValidationState()', () => {
    it('should return the correct action', () => {
      const payload = {
        id: 'some ID',
        validationStatus: 'successful obvs',
      }
      expect(analyticsActions.sendAnalyticsValidationState(payload)).toEqual({
        type: ANALYTICS_ACTION.SEND_INPUT_VALIDATION_STATUS,
        id: 'some ID',
        validationStatus: 'successful obvs',
      })
    })
  })

  describe('sendAnalyticsPaymentMethodChangeEvent()', () => {
    it('should return the correct action', () => {
      const paymentMethod = 'KLRNA'
      expect(
        analyticsActions.sendAnalyticsPaymentMethodChangeEvent(paymentMethod)
      ).toEqual({
        type: ANALYTICS_ACTION.SEND_PAYMENT_METHOD_CHANGE_EVENT,
        eventName: GTM_ACTION.PAYMENT_METHOD_CHANGE,
        payload: { paymentMethod },
      })
    })
  })

  describe('sendAnalyticsDeliveryMethodChangeEvent()', () => {
    it('should return the correct action', () => {
      const deliveryMethod = 'Get it before you even think about it'
      expect(
        analyticsActions.sendAnalyticsDeliveryMethodChangeEvent(deliveryMethod)
      ).toEqual({
        type: ANALYTICS_ACTION.SEND_DELIVERY_METHOD_CHANGE_EVENT,
        deliveryMethod,
      })
    })
  })
})
