import dataLayer, { attachable, gtm as gtmAdapter } from '../dataLayer'
import pageSchema from '../schemas/page'
import userSessionSchema from '../schemas/userSession'
import basketSchema from '../schemas/basket'
import promosSchema from '../schemas/promos'
import { productClickSchema } from '../schemas/product'

describe('dataLayer', () => {
  beforeEach(() => {
    while (dataLayer.length > 0) {
      dataLayer.pop()
    }
  })

  it("`window` mock has global.window.dataLayer = [] for the purposes of these tests (doesn't confirm GTM setup)", () => {
    expect(attachable.dataLayer.push).not.toBeUndefined()
  })

  it('pageSchema added', () => {
    expect(dataLayer.getSchema('pageSchema')).toEqual(pageSchema)
  })

  it('userSessionSchema added', () => {
    expect(dataLayer.getSchema('userSessionSchema')).toEqual(userSessionSchema)
  })

  it('basketSchema added', () => {
    expect(dataLayer.getSchema('basketSchema')).toEqual(basketSchema)
  })

  it('promosSchema added', () => {
    expect(dataLayer.getSchema('promosSchema')).toEqual(promosSchema)
  })

  it('productClickSchema added', () => {
    expect(dataLayer.getSchema('productClickSchema')).toEqual(
      productClickSchema
    )
  })

  it('GTM adapter added', () => {
    expect(dataLayer.getAdapter('gtm')).toEqual(gtmAdapter)
  })

  it('a .push() validating against userSessionSchema populates local and GTM dataLayer', () => {
    dataLayer.push(
      {
        pageType: 'Product Listing',
        pageCategory: 'T-Shirts',
        user: {
          dualRun: 'none',
          loggedIn: 'True',
          id: '123',
        },
      },
      'userSessionSchema',
      'testEvent'
    )

    expect(dataLayer[0]).toEqual({
      platform: 'monty',
      $event: 'testEvent',
      pageType: 'Product Listing',
      pageCategory: 'T-Shirts',
      user: {
        dualRun: 'none',
        loggedIn: 'True',
        id: '123',
      },
    })
    expect(global.window.dataLayer[0]).toEqual({
      platform: 'monty',
      event: 'testEvent',
      pageType: 'Product Listing',
      pageCategory: 'T-Shirts',
      user: {
        dualRun: 'none',
        loggedIn: 'True',
        id: '123',
      },
    })
  })
})
