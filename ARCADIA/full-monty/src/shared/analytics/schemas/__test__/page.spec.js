import dataLayer from '../../dataLayer'
import pageSchema from '../page'

describe('pageSchema', () => {
  it('pageSchema added', () => {
    expect(dataLayer.getSchema('pageSchema')).toBe(pageSchema)
  })

  it('throws if fields missing', () => {
    expect(() => {
      dataLayer.push(
        {
          pageType: 'test',
        },
        'pageSchema'
      )
    }).toThrow()
  })

  it('throws if value format incorrect', () => {
    expect(() => {
      dataLayer.push(
        {
          pageType: 'Product Listing',
          pageCategory: 123,
        },
        'pageSchema'
      )
    }).toThrow()
  })

  it('values are valid', () => {
    expect(() => {
      dataLayer.push(
        {
          pageType: 'Product Listing',
          pageCategory: 'Shirts',
        },
        'pageSchema'
      )
    }).not.toThrow()
  })
})
