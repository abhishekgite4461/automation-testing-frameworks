import dataLayer from '../../dataLayer'
import promosSchema from '../promos'

describe('promosSchema', () => {
  it('promosSchema added', () => {
    expect(dataLayer.getSchema('promosSchema')).toBe(promosSchema)
  })

  it('throws if fields missing', () => {
    expect(() => {
      dataLayer.push(
        {
          ecommerce: {
            promoClick: {
              promotions: {},
            },
          },
        },
        'promosSchema'
      )
    }).toThrow()
  })

  it('does not throw if fields valid', () => {
    expect(() => {
      dataLayer.push(
        {
          ecommerce: {
            promoClick: {
              promotions: [
                {
                  id: '123',
                  name: 'testywesty',
                  creative: undefined,
                  position: undefined,
                },
              ],
            },
          },
        },
        'promosSchema'
      )
    }).not.toThrow()
  })
})
