import { compose, ifElse, path, isEmpty } from 'ramda'
import { isNilOrEmpty } from '../../lib/values'
import { round } from '../../lib/numbers'
import { getMatchingAttribute } from '../../lib/product-utilities'
import {
  getCheckoutPartialOrderSummaryProducts,
  getCheckoutOrderLines,
} from '../../selectors/checkoutSelectors'

const ECMC_PROD_CE3_PRODUCT_TYPE = 'ECMC_PROD_CE3_PRODUCT_TYPE'
const toString = (value) => (value != null ? `${value}` : value)

const safeRoundTo = (precision) =>
  ifElse(
    isNilOrEmpty,
    () => undefined,
    compose(
      (x) => x.toFixed(precision),
      round,
      parseFloat
    )
  )

export const currency = safeRoundTo(2)

export const percentage = safeRoundTo(2)

export const rating = safeRoundTo(1)

const ECMC_PROD_CE3_BRAND = 'ECMC_PROD_CE3_BRAND'

const mapTotalSizes = (items) => {
  if (!Array.isArray(items)) {
    return
  }
  const sizes = items.filter(({ size }) => size)
  return !isEmpty(sizes) ? sizes.length : undefined
}

const mapSizesInStock = (items) => {
  if (!Array.isArray(items)) {
    return
  }
  return items
    .filter(({ size, quantity }) => size && quantity > 0)
    .map(({ size }) => size)
}

const mapReviewRating = (product) => {
  const reviewRating =
    path(['attributes', 'AverageOverallRating'], product) ||
    path(['bazaarVoiceData', 'average'], product)
  return rating(reviewRating)
}

const mapMarkdown = (wasPrice, unitPrice) => {
  if (!wasPrice) {
    return
  }
  const floatWasPrice = parseFloat(wasPrice)
  const markdown = ((floatWasPrice - parseFloat(unitPrice)) / wasPrice) * 100
  return currency(markdown)
}

const mapDepartment = path(['attributes', 'Department'])

export const product = (product = {}) => {
  const wasPrice = product.wasWasPrice || product.wasPrice

  const totalSizes = mapTotalSizes(product.items)

  const sizesInStock = mapSizesInStock(product.items)

  const sizesAvailable =
    totalSizes > 0 && sizesInStock
      ? (sizesInStock.length / totalSizes) * 100
      : undefined

  return {
    id: toString(product.lineNumber),
    ecmcCategory: product.iscmCategory,
    productId: toString(product.productId),
    name: `(${product.lineNumber}) ${product.name}`,
    price: currency(product.unitPrice),
    unitWasPrice: currency(wasPrice),
    unitNowPrice: currency(product.unitPrice),
    markdown: mapMarkdown(wasPrice, product.unitPrice),
    brand: getMatchingAttribute(ECMC_PROD_CE3_BRAND, product.attributes),
    colour: product.colour,
    quantity: toString(product.quantity),
    category: getMatchingAttribute(
      ECMC_PROD_CE3_PRODUCT_TYPE,
      product.attributes
    ),
    department: mapDepartment(product),
    size: toString(product.size),
    totalSizes: toString(totalSizes),
    sizesInStock:
      sizesInStock && !isEmpty(sizesInStock)
        ? sizesInStock.join(',')
        : undefined,
    sizesAvailable: percentage(sizesAvailable),
    reviewRating: mapReviewRating(product),
  }
}

export const getPartialOrderProducts = (state) => {
  const partialSummaryProducts = getCheckoutPartialOrderSummaryProducts(state)
  const orderLines = getCheckoutOrderLines(state)

  return orderLines.map((orderLine) => {
    const relatedBasketItem = partialSummaryProducts.find(
      (x) => x.lineNumber === orderLine.lineNo
    )
    return {
      id: orderLine.lineNo,
      productId: relatedBasketItem
        ? toString(relatedBasketItem.productId)
        : undefined,
      name: `(${orderLine.lineNo}) ${orderLine.name}`,
      price: currency(orderLine.unitPrice),
      markdown: undefined, // Not available! 😞
      unitWasPrice: undefined, // Not available! 😞
      unitNowPrice: currency(orderLine.unitPrice),
      brand: getMatchingAttribute(
        ECMC_PROD_CE3_BRAND,
        relatedBasketItem.attributes
      ),
      colour: orderLine.colour,
      quantity: toString(orderLine.quantity),
      category: getMatchingAttribute(
        ECMC_PROD_CE3_PRODUCT_TYPE,
        relatedBasketItem.attributes
      ),
      size: orderLine.size,
      totalSizes: undefined, // Not available! 😞
      sizesInStock: undefined, // Not available! 😞
      sizesAvailable: undefined, // Not available! 😞
      reviewRating: undefined, // Not available! 😞
    }
  })
}
