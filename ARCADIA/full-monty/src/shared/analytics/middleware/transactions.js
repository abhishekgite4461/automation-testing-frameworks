import { isEmpty } from 'ramda'
import dataLayer from '../dataLayer'
import { addPostDispatchListeners } from './analytics-middleware'
import * as DataMapper from './data-mapper'
import * as Logger from '../../../client/lib/logger'
import {
  getCheckoutOrderError,
  getCheckoutOrderId,
  getCheckoutOrderSummaryPromotions,
  getCheckoutOrderSummaryProducts,
  getCheckoutPartialOrderSummaryProducts,
  getCheckoutOrderLines,
  getCheckoutDeliveryStore,
  getCheckoutOrderCompleted,
  getCheckoutOrderCountry,
} from '../../selectors/checkoutSelectors'
import { isPartialOrderDDP, isDDPUser } from '../../selectors/ddpSelectors'
import { getCurrencyCode } from '../../selectors/configSelectors'

export const transformProduct = (product, orderLines) => {
  const basketProduct = DataMapper.product(product)

  // If the bag the user is checking out with has items prepolulated from a previous session (ScrAPI),
  // we won't have several analaytics fields such as colour, stock and rating (as with basket
  // add/remove). We can at least get colour from checkout.orderCompleted.orderLines
  const orderLine = orderLines.find(
    (orderLine) => basketProduct.lineNumber === orderLine.lineNo
  )
  basketProduct.colour = orderLine ? orderLine.colour : undefined

  return basketProduct
}

export const transformSummary = (state, products) => {
  const deliveryStore = getCheckoutDeliveryStore(state)
  const orderCompleted = getCheckoutOrderCompleted(state)
  const isDdpOrder =
    isPartialOrderDDP(state) || isDDPUser(state) ? 'True' : 'False'

  const deliveryPrice = orderCompleted.deliveryPrice
    ? parseFloat(orderCompleted.deliveryPrice)
    : 0
  const totalOrderPrice = parseFloat(orderCompleted.totalOrderPrice)
  const productsPrice = totalOrderPrice - deliveryPrice
  const totalOrdersDiscount = orderCompleted.totalOrdersDiscount
    ? parseFloat(
        orderCompleted.totalOrdersDiscount
          .replace(/^[^\d]+/, '')
          .replace(/[^\d]+$/, '')
      )
    : 0.0
  const deliveryCountry = getCheckoutOrderCountry(state)
  const productRevenue = products.reduce(
    (acc, product) =>
      acc +
      parseFloat(product.unitWasPrice || product.unitNowPrice) *
        product.quantity,
    0
  )
  const markdown = products.reduce(
    (acc, cur) => acc + parseFloat(cur.unitNowPrice) * cur.quantity,
    0
  )

  const id = orderCompleted.orderId
    ? orderCompleted.orderId.toString()
    : undefined

  return {
    id,
    revenue: DataMapper.currency(totalOrderPrice),
    productRevenue: DataMapper.currency(productRevenue),
    markdownRevenue: DataMapper.currency(markdown),
    paymentType: orderCompleted.paymentDetails
      .map((item) => item.paymentMethod)
      .join(','),
    orderDiscount: DataMapper.currency(
      (totalOrdersDiscount / (productsPrice + totalOrdersDiscount)) * 100
    ),
    discountValue: DataMapper.currency(totalOrdersDiscount),
    orderCountry: deliveryCountry,
    deliveryPrice: DataMapper.currency(deliveryPrice),
    deliveryDiscount: undefined, // no way I can see of getting this from state (using a shipping discount code)
    shippingOption: orderCompleted.deliveryMethod,
    deliverToStore: deliveryStore ? deliveryStore.deliveryStoreCode : undefined,
    ddpOrder: isDdpOrder,
  }
}

export const pageLoadedListener = (action, store) => {
  if (action.payload.pageName !== 'order-completed') {
    return
  }
  const state = store.getState()
  const orderError = getCheckoutOrderError(state)
  const orderId = getCheckoutOrderId(state)
  const currencyCode = getCurrencyCode(state)
  const promotions = getCheckoutOrderSummaryPromotions(state)
  const basketProducts = getCheckoutOrderSummaryProducts(state)
  const partialSummaryProducts = getCheckoutPartialOrderSummaryProducts(state)
  const orderLines = getCheckoutOrderLines(state)

  if (
    orderError ||
    !orderId ||
    (isEmpty(basketProducts) && isEmpty(partialSummaryProducts))
  ) {
    // No products data available on our state! So do nothing.
    return
  }

  const productsData = isEmpty(basketProducts)
    ? // We were likely redirected to the order completed page via a 3rd party,
      // therefore we have to map the product data from the SSR response, which
      // has a limited set of data available to us compared to where no 3rd
      // party redirect may have occurred.
      DataMapper.getPartialOrderProducts(state)
    : // Otherwise we resolve the product data from our basket which will have
      // much more data available.
      basketProducts.map((product) => transformProduct(product, orderLines))

  const data = {
    ecommerce: {
      currencyCode,
      purchase: Object.assign(
        {
          actionField: transformSummary(state, productsData),
          products: productsData,
        },
        // We only add the "coupon" if a promotionCode is being used
        promotions.length > 0 && promotions[0].promotionCode
          ? { coupon: promotions[0].promotionCode }
          : {}
      ),
    },
  }
  dataLayer.push(data, null, 'purchase')
  Logger.info('gtm.transaction', { dataLayer: data })
}

export default () => {
  addPostDispatchListeners('PAGE_LOADED', pageLoadedListener)
}
