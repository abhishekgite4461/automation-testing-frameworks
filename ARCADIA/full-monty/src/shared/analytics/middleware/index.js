import userSessionFactory from './user-session'
import productViewFactory from './product-view'
import checkoutStepsFactory from './checkout-steps'
import transactionsFactory from './transactions'
import pageViewFactory from './page-view'
import basketFactory from './basket'
import productImpressionsFactory from './product-impressions'
import wishlistFactory from './wishlist'
import { setupAnalyticsActionListeners } from './analytics-action-listeners'

export default () => {
  userSessionFactory()
  pageViewFactory()
  productViewFactory()
  checkoutStepsFactory()
  transactionsFactory()
  basketFactory()
  productImpressionsFactory()
  wishlistFactory()
  setupAnalyticsActionListeners()
}
