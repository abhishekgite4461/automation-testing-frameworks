import dataLayer from '../dataLayer'
import { addPostDispatchListeners } from './analytics-middleware'
import * as DataMapper from './data-mapper'
import { getCurrencyCode } from '../../selectors/configSelectors'

let currentPageBatchCount = 1

export const transformProduct = (product) => {
  return {
    ...DataMapper.product(product),
    quantity: '1',
    position: currentPageBatchCount.toString(),
  }
}

export const productImpressions = (products = [], state) => {
  const currencyCode = getCurrencyCode(state)
  dataLayer.push(
    {
      ecommerce: {
        currencyCode,
        impressions: products.map(transformProduct),
      },
    },
    null,
    'impression'
  )
}

export const setProductsListener = (action, store) => {
  currentPageBatchCount = 1
  productImpressions(action.body.products, store.getState())
}

export const addToProductsListener = (action, store) => {
  currentPageBatchCount++
  productImpressions(action.products, store.getState())
}

export default () => {
  addPostDispatchListeners('SET_PRODUCTS', setProductsListener)
  addPostDispatchListeners('ADD_TO_PRODUCTS', addToProductsListener)
}
