import { isEmpty } from 'ramda'
import dataLayer from '../dataLayer'
import { addPostDispatchListeners } from './analytics-middleware'
import { getCurrentProduct } from '../../selectors/productSelectors'
import { getCurrencyCode } from '../../selectors/configSelectors'
import * as DataMapper from './data-mapper'
import { GTM_CATEGORY } from '../'

export const transformProduct = (product) => {
  return {
    ...DataMapper.product(product),
    quantity: '1',
  }
}

const productViewEvent = (product, currencyCode) => {
  dataLayer.push(
    {
      ecommerce: {
        currencyCode,
        detail: {
          products: [transformProduct(product)],
        },
      },
    },
    null,
    'detail'
  )
}

export const productQuickViewListener = (action, store) => {
  if (isEmpty(action.product)) {
    return
  }
  productViewEvent(action.product, getCurrencyCode(store.getState()))
}

export const pageLoadedListener = (action, store) => {
  if (
    action.payload.pageName !== GTM_CATEGORY.PDP &&
    action.payload.pageName !== GTM_CATEGORY.BUNDLE
  ) {
    return
  }
  const state = store.getState()
  productViewEvent(getCurrentProduct(state), getCurrencyCode(state))
}

export default () => {
  addPostDispatchListeners('PAGE_LOADED', pageLoadedListener)
  addPostDispatchListeners('SET_PRODUCT_QUICKVIEW', productQuickViewListener)
}
