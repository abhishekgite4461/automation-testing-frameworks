import { product, getPartialOrderProducts } from '../data-mapper'

describe('Data mapper', () => {
  describe('Product', () => {
    it('fails gracefully if no product is passed', () => {
      const mappedProduct = product()
      expect(mappedProduct).not.toBeUndefined()
    })

    it('maps category', () => {
      const mappedProduct = product({
        attributes: {
          ECMC_PROD_CE3_PRODUCT_TYPE_1: 'Jeans',
        },
      })
      expect(mappedProduct.category).toBe('Jeans')
    })

    it('maps ecmcCategory', () => {
      const iscmCategory = 'EASY'
      const mappedProduct = product({
        iscmCategory,
      })
      expect(mappedProduct.ecmcCategory).toBe(iscmCategory)
    })

    it('does not map category if category key in product attributes is missing', () => {
      const mappedProduct = product({
        attributes: {},
      })
      expect(mappedProduct.category).toBeUndefined()
    })

    it('maps quantity', () => {
      const quantity = 4
      const mappedProduct = product({
        quantity,
      })
      expect(mappedProduct.quantity).toBe(quantity.toString())
    })

    it('maps total size', () => {
      const mappedProduct = product({
        items: [{ size: 1 }, { size: 2 }, {}, { size: 0 }, { size: 4 }],
      })
      expect(mappedProduct.totalSizes).toBe('3')
    })

    it('maps total size with sizes as strings', () => {
      const mappedProduct = product({
        items: [{ size: 'ONE', quantity: 10 }, { size: 'TWO', quantity: 0 }],
      })
      expect(mappedProduct.totalSizes).toBe('2')
    })

    it('does not map total size if items is not an array', () => {
      const mappedProduct = product({
        items: 'whatever',
      })
      expect(mappedProduct.totalSizes).toBeUndefined()
    })

    it('maps sizes in stock', () => {
      const mappedProduct = product({
        items: [
          { size: 1, quantity: 1 },
          { size: 2, quantity: 0 },
          {},
          { size: 0, quantity: 2 },
          { size: 4, quantity: 3 },
        ],
      })
      expect(mappedProduct.sizesInStock).toBe('1,4')
    })

    it('does not map size in stock if items is not an array', () => {
      const mappedProduct = product({
        items: 'whatever',
      })
      expect(mappedProduct.sizesInStock).toBeUndefined()
    })

    it('maps size when size is a number', () => {
      const size = 3
      const mappedProduct = product({
        size,
      })
      expect(mappedProduct.size).toBe(size.toString())
    })

    it('maps size when size is a string', () => {
      const size = '3'
      const mappedProduct = product({
        size,
      })
      expect(mappedProduct.size).toBe(size)
    })

    it('maps review rating from product attributes', () => {
      const mappedProduct = product({
        attributes: {
          AverageOverallRating: '1',
          bazaarVoiceData: {
            average: 5,
          },
        },
      })
      expect(mappedProduct.reviewRating).toBe('1.0')
    })

    it('maps review rating from bazaar', () => {
      const mappedProduct = product({
        bazaarVoiceData: {
          average: 5,
        },
      })
      expect(mappedProduct.reviewRating).toBe('5.0')
    })

    it('maps product id', () => {
      const productId = 32006092
      const mappedProduct = product({
        productId,
      })
      expect(mappedProduct.productId).toBe(productId.toString())
    })

    it('maps line number', () => {
      const lineNumber = '02G22NBLC'
      const mappedProduct = product({
        lineNumber,
      })
      expect(mappedProduct.id).toBe(lineNumber)
    })

    it('maps name', () => {
      const mappedProduct = product({
        lineNumber: '02G22NBLC',
        name: 'product name',
      })
      expect(mappedProduct.name).toBe('(02G22NBLC) product name')
    })

    it('does not map markdown when was price is not available', () => {
      const mappedProduct = product({})
      expect(mappedProduct.markdown).toBeUndefined()
    })

    it('maps markdown', () => {
      const mappedProduct = product({
        wasPrice: 10,
        unitPrice: 3,
      })
      expect(mappedProduct.markdown).toBe('70.00')
    })

    it('maps department', () => {
      const mappedProduct = product({
        attributes: {
          Department: 'Some Product',
        },
      })
      expect(mappedProduct.department).toBe('Some Product')
    })
  })
  describe('getPartialOrderProducts', () => {
    const state = {
      checkout: {
        orderCompleted: {
          orderLines: [
            {
              lineNo: 1,
              name: 'product 1',
              unitPrice: '5.0',
              colour: 'red',
              quantity: 1,
              size: 1,
            },
          ],
        },
        partialOrderSummary: {
          basket: {
            products: [
              {
                lineNumber: 1,
                productId: '12345',
                attributes: {
                  ECMC_PROD_CE3_PRODUCT_TYPE_1: 'Jeans',
                },
              },
            ],
          },
        },
      },
    }
    it('should return partial order products list', () => {
      expect(getPartialOrderProducts(state)).toEqual([
        {
          brand: undefined,
          category: 'Jeans',
          colour: 'red',
          id: 1,
          markdown: undefined,
          name: '(1) product 1',
          price: '5.00',
          productId: '12345',
          quantity: '1',
          reviewRating: undefined,
          size: 1,
          sizesAvailable: undefined,
          sizesInStock: undefined,
          totalSizes: undefined,
          unitNowPrice: '5.00',
          unitWasPrice: undefined,
        },
      ])
    })
  })
})
