import { productQuickViewListener, pageLoadedListener } from '../product-view'
import dataLayer from '../../dataLayer'

jest.mock('../analytics-middleware', () => ({
  addPostDispatchListeners: jest.fn(),
}))

jest.mock('../data-mapper', () => ({
  product: jest.fn(() => {
    return { productId: '1' }
  }),
}))

describe('ProductView analytics', () => {
  const stateMock = {
    config: {
      currencyCode: 'EUR',
    },
    currentProduct: {
      productId: 123,
    },
  }

  const expectedPushData = [
    {
      ecommerce: {
        currencyCode: 'EUR',
        detail: { products: [{ productId: '1', quantity: '1' }] },
      },
    },
    null,
    'detail',
  ]

  beforeEach(() => {
    jest.clearAllMocks()
    jest.spyOn(dataLayer, 'push')
  })

  describe('productQuickViewListener', () => {
    it('should not call dataLayerMock.push when there is no product in action', () => {
      const action = { product: {} }
      productQuickViewListener(action)
      expect(dataLayer.push).not.toHaveBeenCalled()
    })

    it('should call dataLayerMock.push with correct data', () => {
      const action = { product: stateMock.currentProduct }
      const store = {
        getState: () => stateMock,
      }
      productQuickViewListener(action, store)
      expect(dataLayer.push).toHaveBeenCalledTimes(1)
      expect(dataLayer.push).toHaveBeenCalledWith(...expectedPushData)
    })
  })

  describe('pageLoadedListener', () => {
    it('should not call dataLayerMock.push when page is not PDP', () => {
      const action = { payload: { pageName: 'homepage' } }
      pageLoadedListener(action)
      expect(dataLayer.push).not.toHaveBeenCalled()
    })

    it('should call dataLayerMock.push with correct data for pdp', () => {
      const action = { payload: { pageName: 'pdp' } }
      const store = {
        getState: () => stateMock,
      }
      pageLoadedListener(action, store)
      expect(dataLayer.push).toHaveBeenCalledTimes(1)
      expect(dataLayer.push).toHaveBeenCalledWith(...expectedPushData)
    })
    it('should call dataLayerMock.push with correct data for bundle', () => {
      const action = { payload: { pageName: 'bundle' } }
      const store = {
        getState: () => stateMock,
      }
      pageLoadedListener(action, store)
      expect(dataLayer.push).toHaveBeenCalledTimes(1)
      expect(dataLayer.push).toHaveBeenCalledWith(...expectedPushData)
    })
  })
})
