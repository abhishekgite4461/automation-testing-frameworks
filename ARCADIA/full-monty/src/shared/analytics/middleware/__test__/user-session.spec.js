import config, {
  triggered,
  cacheActionListener,
  userSessionStateListener,
} from '../user-session'
import { getItem } from '../../../../client/lib/cookie/utils'

jest.mock('../../storeObserver', () => ({
  addStateListeners: jest.fn(),
}))

jest.mock('../analytics-middleware', () => ({
  addPostDispatchListeners: jest.fn(),
}))

jest.mock('../../dataLayer', () => ({
  push: jest.fn(),
}))

jest.mock('../../../../client/lib/cookie/utils', () => ({
  getItem: jest.fn(),
}))

describe('userSession analytics', () => {
  let dataLayerMock

  beforeEach(() => {
    jest.clearAllMocks()
    getItem.mockImplementation(() => 'none')
    triggered.cacheListener = false
    dataLayerMock = require('../../dataLayer')
  })

  const loggedOutState = {
    account: {
      user: {},
    },
  }

  const loggedInState = {
    account: {
      user: {
        exists: true,
        userTrackingId: 123,
      },
    },
  }

  const loggedInDdpUserState = {
    account: {
      user: {
        exists: true,
        userTrackingId: 123,
        isDDPUser: true,
      },
    },
  }

  describe('userSessionStateListener() triggering behaves as expected', () => {
    describe('cacheActionListener()', () => {
      it('triggers update to dataLayer', () => {
        expect(triggered.cacheListener).toBe(false)

        cacheActionListener(null, {
          getState: () => loggedOutState,
        })

        expect(triggered.cacheListener).toBe(true)

        expect(dataLayerMock.push).toHaveBeenCalledTimes(1)
        const actualDataLayerPushArgs = dataLayerMock.push.mock.calls[0]
        expect(actualDataLayerPushArgs).toEqual([
          {
            user: {
              dualRun: 'none',
              loggedIn: 'False',
              id: undefined,
              ddpUser: undefined,
            },
          },
          'userSessionSchema',
          'userState',
        ])
      })
      it('triggers update to dataLayer for user in Monty AB Test', () => {
        getItem.mockImplementation(() => 'monty')

        expect(triggered.cacheListener).toBe(false)

        cacheActionListener(null, {
          getState: () => loggedOutState,
        })

        expect(triggered.cacheListener).toBe(true)

        expect(dataLayerMock.push).toHaveBeenCalledTimes(1)
        const actualDataLayerPushArgs = dataLayerMock.push.mock.calls[0]
        expect(actualDataLayerPushArgs).toEqual([
          {
            user: {
              dualRun: 'monty',
              loggedIn: 'False',
              id: undefined,
              ddpUser: undefined,
            },
          },
          'userSessionSchema',
          'userState',
        ])
      })
    })

    describe('userSessionStateListener()', () => {
      it('triggers userStateChange event if logged out and then logged in', () => {
        triggered.cacheListener = true

        userSessionStateListener(loggedOutState, loggedInState)

        expect(dataLayerMock.push).toHaveBeenCalledTimes(1)
        const actualDataLayerPushArgs = dataLayerMock.push.mock.calls[0]
        expect(actualDataLayerPushArgs).toEqual([
          {
            user: {
              dualRun: 'none',
              loggedIn: 'True',
              id: '123',
              ddpUser: 'False',
            },
          },
          'userSessionSchema',
          'userState',
        ])
      })

      it('triggers userStateChange event if logged out and then logged in as ddpUser', () => {
        triggered.cacheListener = true

        userSessionStateListener(loggedOutState, loggedInDdpUserState)

        expect(dataLayerMock.push).toHaveBeenCalledTimes(1)
        const actualDataLayerPushArgs = dataLayerMock.push.mock.calls[0]
        expect(actualDataLayerPushArgs).toEqual([
          {
            user: {
              dualRun: 'none',
              loggedIn: 'True',
              id: '123',
              ddpUser: 'True',
            },
          },
          'userSessionSchema',
          'userState',
        ])
      })

      it('triggers userStateChange event if logged in and then logged out', () => {
        triggered.cacheListener = true

        userSessionStateListener(loggedInState, loggedOutState)

        expect(dataLayerMock.push).toHaveBeenCalledTimes(1)
        const actualDataLayerPushArgs = dataLayerMock.push.mock.calls[0]
        expect(actualDataLayerPushArgs).toEqual([
          {
            user: {
              dualRun: 'none',
              loggedIn: 'False',
              id: undefined,
              ddpUser: undefined,
            },
          },
          'userSessionSchema',
          'userState',
        ])
      })

      it('triggers userStateChange event if logged in as ddpUser and then logged out', () => {
        triggered.cacheListener = true

        userSessionStateListener(loggedInDdpUserState, loggedOutState)

        expect(dataLayerMock.push).toHaveBeenCalledTimes(1)
        const actualDataLayerPushArgs = dataLayerMock.push.mock.calls[0]
        expect(actualDataLayerPushArgs).toEqual([
          {
            user: {
              dualRun: 'none',
              loggedIn: 'False',
              id: undefined,
              ddpUser: undefined,
            },
          },
          'userSessionSchema',
          'userState',
        ])
      })

      it('does not push any event if user remains loggedOut', () => {
        userSessionStateListener(loggedOutState, loggedOutState)

        expect(dataLayerMock.push).toHaveBeenCalledTimes(0)
      })

      it('configures listeners correctly', () => {
        config()
        const addStateListeners = require('../../storeObserver')
          .addStateListeners
        expect(addStateListeners).toHaveBeenCalledWith(userSessionStateListener)

        const addPostDispatchListeners = require('../analytics-middleware')
          .addPostDispatchListeners
        expect(addPostDispatchListeners).toHaveBeenCalledWith(
          'PAGE_LOADED',
          cacheActionListener
        )
      })
    })
  })
})
