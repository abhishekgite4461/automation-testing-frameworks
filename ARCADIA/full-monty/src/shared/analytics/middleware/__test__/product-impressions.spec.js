import config, {
  transformProduct,
  setProductsListener,
  addToProductsListener,
} from '../product-impressions'
import dataLayer from '../../dataLayer'

jest.mock('../analytics-middleware', () => ({
  addPostDispatchListeners: jest.fn(),
}))

describe('productImpressions analytics', () => {
  const mockProduct1 = {
    productId: 123456789,
    lineNumber: 'TS09D12MWHT',
    name: 'Long Sleeve Crew Neck Top',
    wasPrice: '20.00',
    unitPrice: '15.00',
    items: [
      {
        size: '8',
        quantity: 2,
      },
      {
        size: '10',
        quantity: 0,
      },
    ],
    bazaarVoiceData: {
      average: '4.0',
    },
  }

  const mockProduct2 = {
    productId: 987654321,
    lineNumber: 'TS32K05MWHT',
    name: 'KLUELESS Studded Mule',
    unitPrice: '62.00',
    items: [],
  }

  beforeEach(() => {
    jest.resetAllMocks()
    jest.spyOn(dataLayer, 'push')
  })

  describe('utilities', () => {
    it('transformProduct()', () => {
      expect(transformProduct(mockProduct1)).toEqual({
        id: 'TS09D12MWHT',
        productId: '123456789',
        name: '(TS09D12MWHT) Long Sleeve Crew Neck Top',
        price: '15.00',
        unitWasPrice: '20.00',
        unitNowPrice: '15.00',
        markdown: '25.00',
        brand: undefined,
        colour: undefined,
        quantity: '1',
        category: undefined,
        size: undefined,
        totalSizes: '2',
        sizesInStock: '8',
        sizesAvailable: '50.00',
        reviewRating: '4.0',
        position: '1',
      })

      expect(transformProduct(mockProduct2)).toEqual({
        id: 'TS32K05MWHT',
        productId: '987654321',
        name: '(TS32K05MWHT) KLUELESS Studded Mule',
        price: '62.00',
        unitWasPrice: undefined,
        unitNowPrice: '62.00',
        markdown: undefined,
        brand: undefined,
        colour: undefined,
        quantity: '1',
        category: undefined,
        size: undefined,
        totalSizes: undefined,
        sizesInStock: undefined,
        sizesAvailable: undefined,
        reviewRating: undefined,
        position: '1',
      })
    })
  })

  describe('dataLayer', () => {
    it('Scenario where PLP or search loads initial batch of products', () => {
      setProductsListener(
        {
          body: {
            products: [mockProduct1],
          },
        },
        {
          getState: () => ({
            config: {
              currencyCode: 'EUR',
            },
            products: {
              products: [mockProduct1],
            },
          }),
        }
      )

      expect(dataLayer.push).toHaveBeenCalledTimes(1)
      const actualDataLayerPushArgs = dataLayer.push.mock.calls[0]
      expect(actualDataLayerPushArgs).toEqual([
        {
          ecommerce: {
            currencyCode: 'EUR',
            impressions: [
              {
                id: 'TS09D12MWHT',
                productId: '123456789',
                name: '(TS09D12MWHT) Long Sleeve Crew Neck Top',
                price: '15.00',
                unitWasPrice: '20.00',
                unitNowPrice: '15.00',
                markdown: '25.00',
                brand: undefined,
                colour: undefined,
                quantity: '1',
                category: undefined,
                size: undefined,
                totalSizes: '2',
                sizesInStock: '8',
                sizesAvailable: '50.00',
                reviewRating: '4.0',
                position: '1',
              },
            ],
          },
        },
        null,
        'impression',
      ])
    })

    it('Scenario where PLP or search loads no products', () => {
      setProductsListener(
        {
          body: {},
        },
        {
          getState: () => ({
            config: {
              currencyCode: 'EUR',
            },
          }),
        }
      )
      expect(dataLayer.push).toHaveBeenCalledTimes(1)
      const actualDataLayerPushArgs = dataLayer.push.mock.calls[0]
      expect(actualDataLayerPushArgs).toEqual([
        {
          ecommerce: {
            currencyCode: 'EUR',
            impressions: [],
          },
        },
        null,
        'impression',
      ])
    })

    it('Scenario where user scrolls down to load more products', () => {
      // initial page of products
      setProductsListener(
        {
          body: {
            products: [mockProduct1],
          },
        },
        {
          getState: () => ({
            config: {
              currencyCode: 'EUR',
            },
            products: {
              products: [mockProduct1],
            },
          }),
        }
      )

      // scroll down triggers additional product to be loaded
      addToProductsListener(
        {
          products: [mockProduct2],
        },
        {
          getState: () => ({
            config: {
              currencyCode: 'EUR',
            },
            products: {
              products: [mockProduct1, mockProduct2],
            },
          }),
        }
      )

      expect(dataLayer.push).toHaveBeenCalledTimes(2)
      const actualDataLayerPushArgs = dataLayer.push.mock.calls[1]
      expect(actualDataLayerPushArgs).toEqual([
        {
          ecommerce: {
            currencyCode: 'EUR',
            impressions: [
              {
                id: 'TS32K05MWHT',
                productId: '987654321',
                name: '(TS32K05MWHT) KLUELESS Studded Mule',
                price: '62.00',
                unitWasPrice: undefined,
                unitNowPrice: '62.00',
                markdown: undefined,
                brand: undefined,
                colour: undefined,
                quantity: '1',
                category: undefined,
                size: undefined,
                totalSizes: undefined,
                sizesInStock: undefined,
                sizesAvailable: undefined,
                reviewRating: undefined,
                position: '2',
              },
            ],
          },
        },
        null,
        'impression',
      ])
    })
  })

  it('listeners configured correctly', () => {
    config()

    const addPostDispatchListeners = require('../analytics-middleware')
      .addPostDispatchListeners
    expect(addPostDispatchListeners).toHaveBeenCalledWith(
      'ADD_TO_PRODUCTS',
      addToProductsListener
    )
    expect(addPostDispatchListeners).toHaveBeenCalledWith(
      'SET_PRODUCTS',
      setProductsListener
    )
  })
})
