import { clone } from 'ramda'
import dataLayer from '../../dataLayer'

describe('transaction analytics', () => {
  beforeEach(() => {
    jest.resetAllMocks()
    jest.spyOn(dataLayer, 'push')
  })

  const mockState = {
    config: {
      currencyCode: 'EUR',
    },
    checkout: {
      orderCompleted: {
        orderId: 1706986,
        deliveryMethod: 'UK Standard up to 4 working days',
        deliveryCost: '4.00',
        deliveryPrice: '4.00',
        totalOrderPrice: '44.8',
        totalOrdersDiscount: '-£7.2',
        orderLines: [
          {
            lineNo: '26M48MIVR',
            colour: 'IVORY',
          },
          {
            lineNo: '42H08KGLD',
            colour: 'GOLD',
          },
        ],
        paymentDetails: [
          {
            paymentMethod: 'Visa',
          },
        ],
      },
      orderSummary: {
        shippingCountry: 'UK',
        basket: {
          products: [
            {
              productId: 123456789,
              orderItemId: 8025765,
              lineNumber: '26M48MIVR',
              size: '6',
              name: 'PETITE Floral Jacquard Midi Wrap Dress',
              quantity: 1,
              wasWasPrice: '49.00',
              unitPrice: '30.00',
              totalPrice: '30.00',
              items: [],
              attributes: {},
            },
            {
              productId: 987654321,
              orderItemId: 8025780,
              lineNumber: '42H08KGLD',
              size: '38',
              name: 'HELD UP Leather Knot Sandals',
              quantity: 1,
              wasPrice: '24.00',
              unitPrice: '18.00',
              totalPrice: '18.00',
              items: [],
              attributes: {},
            },
          ],
        },
      },
      orderError: false,
    },
  }

  const mockDdpOrder = {
    checkout: {
      orderCompleted: {
        orderId: 1111111,
        totalOrderPrice: '30.00',
        deliveryCost: '4.00',
        deliveryPrice: '4.00',
        totalOrdersDiscount: '5.0',
        orderLines: [],
        paymentDetails: [
          {
            paymentMethod: 'Visa',
          },
        ],
      },
      partialOrderSummary: {
        basket: {
          products: [
            {
              lineNumber: 'Subscription-ddp',
              productId: 1234,
            },
          ],
          isDDPOrder: true,
        },
      },
    },
  }

  const mockStateWithPromoCode = clone(mockState)
  mockStateWithPromoCode.checkout.orderSummary.basket.promotions = [
    {
      promotionCode: 'TSDEL0103',
      label: 'Free standard shipping Free collect from store',
    },
  ]

  let transformProduct
  let transformSummary
  let pageLoadedListener

  beforeEach(() => {
    const transactionsModule = require('../transactions')
    pageLoadedListener = transactionsModule.pageLoadedListener
    transformSummary = transactionsModule.transformSummary
    transformProduct = transactionsModule.transformProduct
  })

  describe('utility functions', () => {
    it('transformProduct()', () => {
      const orderLines = mockState.checkout.orderCompleted.orderLines
      const products = mockState.checkout.orderSummary.basket.products.map(
        (product) => transformProduct(product, orderLines)
      )

      expect(products).toEqual([
        {
          id: '26M48MIVR',
          productId: '123456789',
          name: '(26M48MIVR) PETITE Floral Jacquard Midi Wrap Dress',
          price: '30.00',
          unitWasPrice: '49.00',
          unitNowPrice: '30.00',
          markdown: '38.78',
          brand: undefined,
          colour: undefined,
          quantity: '1',
          category: undefined,
          size: '6',
          totalSizes: undefined,
          sizesInStock: undefined,
          sizesAvailable: undefined,
          reviewRating: undefined,
        },
        {
          id: '42H08KGLD',
          productId: '987654321',
          name: '(42H08KGLD) HELD UP Leather Knot Sandals',
          price: '18.00',
          unitWasPrice: '24.00',
          unitNowPrice: '18.00',
          markdown: '25.00',
          brand: undefined,
          colour: undefined,
          quantity: '1',
          category: undefined,
          size: '38',
          totalSizes: undefined,
          sizesInStock: undefined,
          sizesAvailable: undefined,
          reviewRating: undefined,
        },
      ])
    })

    describe('transformSummary()', () => {
      it('produces the expected output given the mock state', () => {
        const orderLines = mockState.checkout.orderCompleted.orderLines
        const products = mockState.checkout.orderSummary.basket.products.map(
          (product) => transformProduct(product, orderLines)
        )
        expect(transformSummary(mockState, products)).toEqual({
          id: '1706986',
          revenue: '44.80',
          productRevenue: '73.00',
          markdownRevenue: '48.00',
          paymentType: 'Visa',
          orderDiscount: '15.00',
          discountValue: '7.20',
          orderCountry: 'UK',
          deliveryPrice: '4.00',
          deliveryDiscount: undefined,
          shippingOption: 'UK Standard up to 4 working days',
          deliverToStore: undefined,
          ddpOrder: 'False',
        })
      })

      it('sets orderCountry correctly when the deliveryDetails.address.country state exists', () => {
        const orderLines = mockState.checkout.orderCompleted.orderLines
        const products = mockState.checkout.orderSummary.basket.products.map(
          (product) => transformProduct(product, orderLines)
        )
        const mockCountry = 'mockCountry'
        expect(
          transformSummary(
            {
              ...mockState,
              checkout: {
                ...mockState.checkout,
                orderSummary: {
                  ...mockState.orderSummary,
                  deliveryDetails: {
                    address: {
                      country: mockCountry,
                    },
                  },
                },
              },
            },
            products
          ).orderCountry
        ).toEqual(mockCountry)
      })

      it('sets orderCountry correctly when the deliveryDetails.country state exists', () => {
        const orderLines = mockState.checkout.orderCompleted.orderLines
        const products = mockState.checkout.orderSummary.basket.products.map(
          (product) => transformProduct(product, orderLines)
        )
        const mockCountry = 'someOtherMockCountry'
        expect(
          transformSummary(
            {
              ...mockState,
              checkout: {
                ...mockState.checkout,
                orderSummary: {
                  ...mockState.orderSummary,
                  deliveryDetails: {
                    country: mockCountry,
                  },
                },
              },
            },
            products
          ).orderCountry
        ).toEqual(mockCountry)
      })

      it('sets deliverToStore correctly when the deliveryStore state exists', () => {
        const mockStateWithDeliveryStore = {
          checkout: {
            ...mockState.checkout,
            deliveryStore: {
              deliveryStoreCode: 'TS0001',
              storeAddress1: '214 Oxford Street',
              storeAddress2: 'Oxford Circus',
              storeCity: 'West End',
              storePostcode: 'W1W 8LG',
            },
          },
        }
        expect(transformSummary(mockStateWithDeliveryStore, [])).toMatchObject({
          deliverToStore: 'TS0001',
        })
      })
    })
  })

  describe('dataLayer push', () => {
    it('pageLoadedListener(): on order completion page so should push to dataLayer', () => {
      pageLoadedListener(
        {
          payload: {
            pageName: 'order-completed',
          },
        },
        { getState: () => mockState }
      )

      expect(dataLayer.push).toHaveBeenCalledTimes(1)
      const actualDataLayerPushArgs = dataLayer.push.mock.calls[0]
      expect(actualDataLayerPushArgs).toEqual([
        {
          ecommerce: {
            currencyCode: 'EUR',
            purchase: {
              actionField: {
                id: '1706986',
                ddpOrder: 'False',
                revenue: '44.80',
                productRevenue: '73.00',
                markdownRevenue: '48.00',
                paymentType: 'Visa',
                orderDiscount: '15.00',
                discountValue: '7.20',
                orderCountry: 'UK',
                deliveryPrice: '4.00',
                deliveryDiscount: undefined,
                shippingOption: 'UK Standard up to 4 working days',
                deliverToStore: undefined,
              },
              products: [
                {
                  id: '26M48MIVR',
                  productId: '123456789',
                  name: '(26M48MIVR) PETITE Floral Jacquard Midi Wrap Dress',
                  price: '30.00',
                  unitWasPrice: '49.00',
                  unitNowPrice: '30.00',
                  markdown: '38.78',
                  brand: undefined,
                  colour: undefined,
                  quantity: '1',
                  category: undefined,
                  size: '6',
                  totalSizes: undefined,
                  sizesInStock: undefined,
                  sizesAvailable: undefined,
                  reviewRating: undefined,
                },
                {
                  id: '42H08KGLD',
                  productId: '987654321',
                  name: '(42H08KGLD) HELD UP Leather Knot Sandals',
                  price: '18.00',
                  unitWasPrice: '24.00',
                  unitNowPrice: '18.00',
                  markdown: '25.00',
                  brand: undefined,
                  colour: undefined,
                  quantity: '1',
                  category: undefined,
                  size: '38',
                  totalSizes: undefined,
                  sizesInStock: undefined,
                  sizesAvailable: undefined,
                  reviewRating: undefined,
                },
              ],
            },
          },
        },
        null,
        'purchase',
      ])
    })

    it('pageLoadedListener(): on order completion page when Promo Code used so should push to dataLayer', () => {
      pageLoadedListener(
        {
          payload: {
            pageName: 'order-completed',
          },
        },
        { getState: () => mockStateWithPromoCode }
      )

      expect(dataLayer.push).toHaveBeenCalledTimes(1)
      const actualDataLayerPushArgs = dataLayer.push.mock.calls[0]
      expect(actualDataLayerPushArgs).toEqual([
        {
          ecommerce: {
            currencyCode: 'EUR',
            purchase: {
              coupon: 'TSDEL0103',
              actionField: {
                id: '1706986',
                ddpOrder: 'False',
                revenue: '44.80',
                productRevenue: '73.00',
                markdownRevenue: '48.00',
                paymentType: 'Visa',
                orderDiscount: '15.00',
                discountValue: '7.20',
                orderCountry: 'UK',
                deliveryPrice: '4.00',
                deliveryDiscount: undefined,
                shippingOption: 'UK Standard up to 4 working days',
                deliverToStore: undefined,
              },
              products: [
                {
                  id: '26M48MIVR',
                  productId: '123456789',
                  name: '(26M48MIVR) PETITE Floral Jacquard Midi Wrap Dress',
                  price: '30.00',
                  unitWasPrice: '49.00',
                  unitNowPrice: '30.00',
                  markdown: '38.78',
                  brand: undefined,
                  colour: undefined,
                  quantity: '1',
                  category: undefined,
                  size: '6',
                  totalSizes: undefined,
                  sizesInStock: undefined,
                  sizesAvailable: undefined,
                  reviewRating: undefined,
                },
                {
                  id: '42H08KGLD',
                  productId: '987654321',
                  name: '(42H08KGLD) HELD UP Leather Knot Sandals',
                  price: '18.00',
                  unitWasPrice: '24.00',
                  unitNowPrice: '18.00',
                  markdown: '25.00',
                  brand: undefined,
                  colour: undefined,
                  quantity: '1',
                  category: undefined,
                  size: '38',
                  totalSizes: undefined,
                  sizesInStock: undefined,
                  sizesAvailable: undefined,
                  reviewRating: undefined,
                },
              ],
            },
          },
        },
        null,
        'purchase',
      ])
    })

    it('pageLoadedListener(): on order completion page when ddpOrder is present, should push to the dataLayer', () => {
      expect(dataLayer.push).toHaveBeenCalledTimes(0)
      pageLoadedListener(
        {
          payload: {
            pageName: 'order-completed',
          },
        },
        {
          getState: () => mockDdpOrder,
        }
      )

      expect(dataLayer.push).toHaveBeenCalledTimes(1)
      const actualDataLayerPushArgs = dataLayer.push.mock.calls[0]
      expect(actualDataLayerPushArgs).toEqual([
        {
          ecommerce: {
            purchase: {
              actionField: {
                ddpOrder: 'True',
                id: '1111111',
                deliveryPrice: '4.00',
                discountValue: '5.00',
                productRevenue: '0.00',
                revenue: '30.00',
                orderDiscount: '16.13',
                markdownRevenue: '0.00',
                paymentType: 'Visa',
              },
              products: [],
            },
          },
        },
        null,
        'purchase',
      ])
    })

    it('pageLoadedListener(): NOT on order completion page so nothing should happen', () => {
      pageLoadedListener(
        {
          payload: {
            pageName: 'not-order-completed',
          },
        },
        { getState: () => mockState }
      )

      expect(dataLayer.push).toHaveBeenCalledTimes(0)
    })

    describe('with existing dataLayer data', () => {
      beforeEach(() => {
        // We simulate existing data on the dataLayer:
        dataLayer[0] = {
          ecommerce: {
            purchase: {},
          },
        }
        dataLayer.length = 1
        pageLoadedListener = require('../transactions').pageLoadedListener
      })

      it('pageLoadedListener(): dataLayer.pushMock should not be called when page is not order-completed', () => {
        pageLoadedListener(
          {
            payload: {
              pageName: 'not-order-completed',
            },
          },
          { getState: () => mockState }
        )

        expect(dataLayer.push).not.toHaveBeenCalled()
      })
    })

    describe('with ssr state', () => {
      const ssrMockState = {
        config: {
          currencyCode: 'EUR',
        },
        checkout: {
          orderCompleted: {
            orderId: 2337940,
            subTotal: '130.00',
            returnPossible: false,
            returnRequested: false,
            deliveryMethod: 'Friday Nominated Day Delivery:',
            deliveryDate: 'Friday 23 March 2018',
            deliveryCost: '6.00',
            deliveryCarrier: 'Home Delivery Network',
            deliveryPrice: '6.00',
            totalOrderPrice: '136.00',
            totalOrdersDiscountLabel: '',
            totalOrdersDiscount: '',
            billingAddress: {
              address1: '2 Carlile Close',
              name: 'Mr 3D. AUTHORISED',
              country: 'United Kingdom',
              address2: 'LONDON',
              address3: 'E3 2ES',
            },
            deliveryAddress: {
              address1: '2 Carlile Close',
              name: 'Mr 3D. AUTHORISED',
              country: 'United Kingdom',
              address2: 'LONDON',
              address3: 'E3 2ES',
            },
            orderLines: [
              {
                lineNo: '02K77LMDT',
                name: 'MOTO Mid Blue Lace Fly Jamie Jeans',
                size: 'W2630',
                colour: 'MID STONE',
                imageUrl:
                  'https://ts.stage.arcadiagroup.ltd.uk/wcsstore/TopShop/images/catalog/TS02K77LMDT_Small_M_1.jpg',
                quantity: 2,
                unitPrice: '42.00',
                discount: '',
                discountPrice: '0.00',
                total: '84.00',
                nonRefundable: false,
              },
              {
                lineNo: '20B03MPPK',
                name: 'OhK! Mini Tissues',
                size: '',
                colour: 'PALE PINK',
                imageUrl:
                  'https://ts.stage.arcadiagroup.ltd.uk/wcsstore/TopShop/images/catalog/TS20B03MPPK_Small_M_1.jpg',
                quantity: 4,
                unitPrice: '1.00',
                discount: '',
                discountPrice: '0.00',
                total: '4.00',
                nonRefundable: false,
              },
              {
                lineNo: '02K06NBLG',
                name: 'MOTO Jamie Skinny Fit Jeans',
                size: 'W3234',
                colour: 'Blue',
                imageUrl:
                  'https://ts.stage.arcadiagroup.ltd.uk/wcsstore/TopShop/images/catalog/TS02K06NBLG_Small_M_1.jpg',
                quantity: 1,
                unitPrice: '42.00',
                discount: '',
                discountPrice: '0.00',
                total: '42.00',
                nonRefundable: false,
              },
            ],
            paymentDetails: [
              {
                paymentMethod: 'Visa',
                cardNumberStar: '************1111',
                totalCost: '£136.00',
              },
            ],
            currencyConversion: {
              currencyRate: 'GBP',
            },
            returning_buyer: false,
            eMarketingEspot1URL: {
              contentText:
                '%3C%21--+New+Customer+Trigger--%3E%0A%3Cscript+type%3D%22text%2Fjavascript%22%3E%0Avar+existingcust+%3D+%22Sale-New%22%0A%3C%2Fscript%3E',
            },
          },
          orderSummary: {},
          orderSummaryError: {},
          orderError: false,
          isDeliverySameAsBilling: true,
          useDeliveryAsBilling: true,
          verifyPayment: false,
          showExpressDeliveryOptionsOnSummary: false,
          editingDetails: false,
          storeUpdating: false,
          deliveryAndPayment: {
            deliveryEditingEnabled: false,
          },
          savePaymentDetails: false,
          partialOrderSummary: {
            basket: {
              orderId: 2337940,
              subTotal: '130.00',
              total: '136.00',
              totalBeforeDiscount: '130.00',
              promotions: [],
              discounts: [],
              products: [
                {
                  lineNumber: '02K77LMDT',
                  productId: 29689232,
                },
                {
                  lineNumber: '20B03MPPK',
                  wasPrice: '2.00',
                  productId: 29832681,
                },
                {
                  lineNumber: '02K06NBLG',
                  productId: 31305407,
                },
              ],
              savedProducts: [],
              inventoryPositions: {
                item_1: {
                  partNumber: '602017001152320',
                  catentryId: '29689275',
                  inventorys: [
                    {
                      cutofftime: '2100',
                      quantity: 10,
                      ffmcenterId: 12556,
                      expressdates: ['2018-03-20', '2018-03-21'],
                    },
                  ],
                  invavls: null,
                },
                item_2: {
                  partNumber: '602017001157334',
                  catentryId: '29832691',
                  inventorys: [
                    {
                      cutofftime: '2100',
                      quantity: 10,
                      ffmcenterId: 12556,
                      expressdates: ['2018-03-20', '2018-03-21'],
                    },
                  ],
                  invavls: null,
                },
                item_3: {
                  partNumber: '602018001209685',
                  catentryId: '31305878',
                  inventorys: [
                    {
                      cutofftime: '2100',
                      quantity: 10,
                      ffmcenterId: 12556,
                      expressdates: ['2018-03-20', '2018-03-21'],
                    },
                  ],
                  invavls: null,
                },
              },
            },
            shippingCountry: 'United Kingdom',
          },
        },
      }

      it('pageLoadedListener(): on order completion page so should push to dataLayer', () => {
        pageLoadedListener(
          {
            payload: {
              pageName: 'order-completed',
            },
          },
          {
            getState: () => ssrMockState,
          }
        )

        expect(dataLayer.push).toHaveBeenCalledTimes(1)
        const actualDataLayerPushArgs = dataLayer.push.mock.calls[0]
        expect(actualDataLayerPushArgs).toEqual([
          {
            ecommerce: {
              currencyCode: 'EUR',
              purchase: {
                actionField: {
                  id: '2337940',
                  ddpOrder: 'False',
                  revenue: '136.00',
                  productRevenue: '130.00',
                  markdownRevenue: '130.00',
                  paymentType: 'Visa',
                  orderDiscount: '0.00',
                  discountValue: '0.00',
                  orderCountry: 'United Kingdom',
                  deliveryPrice: '6.00',
                  deliveryDiscount: undefined,
                  shippingOption: 'Friday Nominated Day Delivery:',
                  deliverToStore: undefined,
                },
                products: [
                  {
                    id: '02K77LMDT',
                    productId: '29689232',
                    name: '(02K77LMDT) MOTO Mid Blue Lace Fly Jamie Jeans',
                    price: '42.00',
                    markdown: undefined,
                    unitWasPrice: undefined,
                    unitNowPrice: '42.00',
                    brand: undefined,
                    colour: 'MID STONE',
                    quantity: '2',
                    category: undefined,
                    size: 'W2630',
                    totalSizes: undefined,
                    sizesInStock: undefined,
                    sizesAvailable: undefined,
                    reviewRating: undefined,
                  },
                  {
                    id: '20B03MPPK',
                    productId: '29832681',
                    name: '(20B03MPPK) OhK! Mini Tissues',
                    price: '1.00',
                    markdown: undefined,
                    unitWasPrice: undefined,
                    unitNowPrice: '1.00',
                    brand: undefined,
                    colour: 'PALE PINK',
                    quantity: '4',
                    category: undefined,
                    size: '',
                    totalSizes: undefined,
                    sizesInStock: undefined,
                    sizesAvailable: undefined,
                    reviewRating: undefined,
                  },
                  {
                    id: '02K06NBLG',
                    productId: '31305407',
                    name: '(02K06NBLG) MOTO Jamie Skinny Fit Jeans',
                    price: '42.00',
                    markdown: undefined,
                    unitWasPrice: undefined,
                    unitNowPrice: '42.00',
                    brand: undefined,
                    colour: 'Blue',
                    quantity: '1',
                    category: undefined,
                    size: 'W3234',
                    totalSizes: undefined,
                    sizesInStock: undefined,
                    sizesAvailable: undefined,
                    reviewRating: undefined,
                  },
                ],
              },
            },
          },
          null,
          'purchase',
        ])
      })
    })
  })
})
