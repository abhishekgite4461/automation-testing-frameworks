import dataLayer from '../../../dataLayer'
import * as gpd from '../get-page-data'
import * as configSelectors from '../../../../selectors/configSelectors'
import * as debugSelectors from '../../../../selectors/debugSelectors'
import * as featureSelectors from '../../../../selectors/featureSelectors'
import * as viewportSelectors from '../../../../selectors/viewportSelectors'
import { pageLoadedListener } from '../page-loaded-listener'

jest.mock('../get-page-data')
jest.mock('../../../../selectors/configSelectors')
jest.mock('../../../../selectors/debugSelectors')
jest.mock('../../../../selectors/featureSelectors')
jest.mock('../../../../selectors/viewportSelectors')

describe('pageLoadedListener()', () => {
  const mockAction = { payload: { pageName: 'fish' } }

  const mockState = Object.freeze({ fake: 'state' })
  const mockStore = {
    getState() {
      return mockState
    },
  }

  beforeEach(() => {
    jest.resetAllMocks()
  })

  it('should retrieve page data from `getPageData()`', () => {
    gpd.getPageData.mockReturnValue({})
    configSelectors.getBrandCode.mockReturnValue('')

    pageLoadedListener(mockAction, mockStore)

    expect(gpd.getPageData).toHaveBeenCalledTimes(1)
    expect(gpd.getPageData).toHaveBeenCalledWith('fish', mockState)
  })

  it('should provide correct data to Google Analytics via `dataLayer.push()`', () => {
    jest.spyOn(dataLayer, 'push')

    gpd.getPageData.mockReturnValue({
      type: 'some type',
      category: 'some category',
      more: 'data',
      and: 'stuff',
    })

    configSelectors.getBrandCode.mockReturnValue('fishfingers')
    debugSelectors.getBuildVersion.mockReturnValue('alpha')
    featureSelectors.getFeaturesStatus.mockReturnValue('fake status')
    viewportSelectors.getViewportMedia.mockReturnValue('fake media')

    pageLoadedListener(mockAction, mockStore)

    expect(dataLayer.push).toHaveBeenCalledTimes(1)
    expect(dataLayer.push).toHaveBeenCalledWith(
      {
        pageType: 'FISHFINGERS:some type',
        pageCategory: 'FISHFINGERS:some category',
        buildVersion: 'alpha',
        features: 'fake status',
        viewport: 'fake media',
        more: 'data',
        and: 'stuff',
      },
      'pageSchema',
      'pageView'
    )
  })

  it('should not invoke `dataLayer.push()` for unknown pages', () => {
    jest.spyOn(dataLayer, 'push')

    gpd.getPageData.mockReturnValue(undefined)

    pageLoadedListener(mockAction, mockStore)

    expect(dataLayer.push).not.toHaveBeenCalled()
  })
})
