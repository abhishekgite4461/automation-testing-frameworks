import dataLayer from '../../dataLayer'
import { getBrandCode } from '../../../selectors/configSelectors'
import { getBuildVersion } from '../../../selectors/debugSelectors'
import { getFeaturesStatus } from '../../../selectors/featureSelectors'
import { getViewportMedia } from '../../../selectors/viewportSelectors'
import { getPageData } from './get-page-data'

export const pageLoadedListener = (action, store) => {
  const state = store.getState()
  const pageData = getPageData(action.payload.pageName, state)

  if (pageData === undefined) {
    return
  }

  const { type, category, ...customData } = pageData
  const upperCaseBrand = getBrandCode(state).toUpperCase()

  const pageType = `${upperCaseBrand}:${type}`
  const pageCategory = `${upperCaseBrand}:${category}`

  dataLayer.push(
    {
      pageType,
      pageCategory,
      buildVersion: getBuildVersion(state),
      features: getFeaturesStatus(state),
      viewport: getViewportMedia(state),
      ...customData,
    },
    'pageSchema',
    'pageView'
  )
}
