import { path } from 'ramda'
import dataLayer from '../dataLayer'
import { addPostDispatchListeners } from './analytics-middleware'
import * as DataMapper from './data-mapper'
import * as Logger from '../../../client/lib/logger'
import { getCheckoutOrderSummaryProducts } from '../../selectors/checkoutSelectors'
import { getCurrencyCode } from '../../selectors/configSelectors'

export const pageLoadedListener = (action, store) => {
  const state = store.getState()
  const bagProducts = getCheckoutOrderSummaryProducts(state)
  const currencyCode = getCurrencyCode(state)
  const checkoutSteps = {
    'checkout-login': 1,
    'delivery-details': 2,
    'billing-details': 3,
    'order-submit-page': 4,
  }
  const checkoutStep = checkoutSteps[action.payload.pageName]

  if (checkoutStep) {
    const data = {
      ecommerce: {
        currencyCode,
        checkout: {
          actionField: {
            step: checkoutStep,
          },
          products: bagProducts.map(DataMapper.product),
        },
      },
    }
    dataLayer.push(data, null, 'checkout')
    Logger.info(`gtm.checkoutstep${checkoutStep}`, { dataLayer: data })
  } else {
    // Ensure step data isn't sent along with pageView data if current page isn't one above
    for (let i = dataLayer.length - 1; i >= 0; i--) {
      if (
        path(['ecommerce', 'checkout', 'actionField', 'step'], dataLayer[i])
      ) {
        dataLayer.push({
          ecommerce: {
            checkout: {
              actionField: {
                step: undefined,
              },
            },
          },
        })
        break
      }
    }
  }
}

export default () => {
  addPostDispatchListeners('PAGE_LOADED', pageLoadedListener)
}
