import { ANALYTICS_ACTION, GTM_ACTION } from './analytics-constants'

export const sendAnalyticsApiResponseEvent = (payload) => ({
  type: ANALYTICS_ACTION.SEND_API_RESPONSE_EVENT,
  payload,
})

export const sendAnalyticsClickEvent = (payload) => ({
  type: ANALYTICS_ACTION.SEND_CLICK_EVENT,
  payload,
})

export const sendAnalyticsErrorMessage = (errorMessage) => ({
  type: ANALYTICS_ACTION.SEND_ERROR_MESSAGE,
  errorMessage,
})

export const sendAnalyticsProductClickEvent = (payload) => ({
  type: ANALYTICS_ACTION.SEND_PRODUCT_CLICK_EVENT,
  payload,
})

export const sendAnalyticsDisplayEvent = (payload, eventName) => ({
  type: ANALYTICS_ACTION.SEND_DISPLAY_EVENT,
  payload,
  eventName,
})

export const sendAnalyticsDeliveryOptionChangeEvent = (
  deliveryLocationType
) => ({
  type: ANALYTICS_ACTION.SEND_DELIVERY_OPTION_CHANGE_EVENT,
  deliveryLocationType,
})

export const sendAnalyticsValidationState = ({ id, validationStatus }) => ({
  type: ANALYTICS_ACTION.SEND_INPUT_VALIDATION_STATUS,
  id,
  validationStatus,
})

export const sendAnalyticsPaymentMethodChangeEvent = (paymentMethod) => ({
  type: ANALYTICS_ACTION.SEND_PAYMENT_METHOD_CHANGE_EVENT,
  eventName: GTM_ACTION.PAYMENT_METHOD_CHANGE,
  payload: { paymentMethod },
})

export const sendAnalyticsDeliveryMethodChangeEvent = (deliveryMethod) => ({
  type: ANALYTICS_ACTION.SEND_DELIVERY_METHOD_CHANGE_EVENT,
  deliveryMethod,
})

export const sendAnalyticsFilterUsedEvent = (payload) => ({
  type: ANALYTICS_ACTION.SEND_FILTER_USED_EVENT,
  payload,
})
