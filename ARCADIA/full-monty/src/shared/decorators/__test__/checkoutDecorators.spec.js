import React, { Component } from 'react'
import { mount } from 'enzyme'
import { createStore } from 'redux'

function mockWhenBasketHasBeenCheckedOut(result) {
  jest.doMock('../../selectors/checkoutSelectors', () => ({
    hasCheckedOut: jest.fn(() => {
      return result
    }),
  }))
}

function mountComponentDecoratedWith(decorator) {
  @decorator
  class SomeComponent extends Component {
    render() {
      return <div>foo</div>
    }
  }
  const store = createStore(() => {}, {})
  return mount(<SomeComponent />, { context: { store } })
}

function testComponentDecoratedWithWhenCheckedOut({
  hasCheckedOut,
  expectToRender,
}) {
  mockWhenBasketHasBeenCheckedOut(hasCheckedOut)
  const { whenCheckedOut } = require('../checkoutDecorators')
  expect(mountComponentDecoratedWith(whenCheckedOut).html()).toBe(
    expectToRender
  )
  jest.resetAllMocks()
}

describe('Checkout Decorators', () => {
  describe('@whenCheckedOut', () => {
    describe('when the basket has been checked out', () => {
      it('returns a component that renders the decorated component', () => {
        testComponentDecoratedWithWhenCheckedOut({
          hasCheckedOut: true,
          expectToRender: '<div>foo</div>',
        })
      })
    })

    describe('when the basket has not been checked out', () => {
      it('returns a component that renders null', () => {
        testComponentDecoratedWithWhenCheckedOut({
          hasCheckedOut: false,
          expectToRender: null,
        })
      })
    })
  })
})
