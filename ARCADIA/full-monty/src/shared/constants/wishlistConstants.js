export const ITEMS_TO_SHOW = 20
export const MAX_WISHLIST_ITEMS = 100
export const MAX_WISHLIST_INFO_MODAL =
  'Your Wish List is full. To add more, you will need to remove some products.'
