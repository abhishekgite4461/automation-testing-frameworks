export const formNames = {
  delivery: {
    address: 'deliveryAddressMCD',
    details: 'deliveryDetailsAddressMCD',
    findAddress: 'deliveryFindAddressMCD',
  },
  billing: {
    address: 'billingAddressMCD',
    details: 'billingDetailsAddressMCD',
    findAddress: 'billingFindAddressMCD',
  },
  payment: {
    paymentCardDetailsMCD: 'paymentCardDetailsMCD',
    paymentCardDetails: 'billingCardDetails',
  },
}
