export const espotGroupId = Object.freeze({
  NAVIGATION: 'navigation',
  HOME: 'home',
  MINI_BAG: 'miniBag',
  MINI_BAG_MIDDLE: 'miniBagMiddle',
  PRODUCTS: 'products',
  PRODUCT: 'product',
  THANKYOU: 'thankyou',
  ORDER_SUMMARY: 'orderSummary',
})

export default Object.freeze({
  [espotGroupId.NAVIGATION]: {
    siteWideHeader: 'headerEspot',
    // NOTE that some brand require brandHeader and global headers to be swapped
    brandHeader: 'montyHeaderEspot',
    global: 'globalEspot',
  },
  [espotGroupId.HOME]: {
    content: 'homePageEspotContent',
    mainBody: 'homeEspot1',
  },
  [espotGroupId.MINI_BAG]: {
    top: 'shoppingBagTopEspot',
    middle: 'Delivery2HOMEEXPRESS',
    bottom: 'shoppingBagTotalEspot',
  },
  [espotGroupId.PRODUCTS]: {
    productList: 'productList',
  },
  [espotGroupId.PRODUCT]: {
    col1pos1: 'CEProductEspotCol1Pos1',
    col1pos2: 'CEProductEspotCol1Pos2',
    col2pos1: 'CEProductEspotCol2Pos1',
    col2pos2: 'CEProductEspotCol2Pos2',
    col2pos4: 'CEProductEspotCol2Pos4',
    content1: 'CE3ContentEspot1',
    klarna1: 'KlarnaPDPEspot1',
    klarna2: 'KlarnaPDPEspot2',
    bundle1: 'BundleProductEspot1',
  },
  [espotGroupId.THANKYOU]: {
    mainBody1: 'eMarketingEspot1URL',
    mainBody2: 'eMarketingEspot2URL',
    mainBody3: 'eMarketingEspot3URL',
    mainBody4: 'eMarketingEspot7URL',
    sideBar1: 'eMarketingEspot4URL',
    sideBar2: 'eMarketingEspot5URL',
    sideBar3: 'eMarketingEspot6URL',
  },
  [espotGroupId.ORDER_SUMMARY]: {
    discountIntro: 'checkoutDiscountIntroEspot',
    toBeDefined: 'toBeDefined',
  },
})
