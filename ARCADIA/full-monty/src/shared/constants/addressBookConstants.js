export const addressBookConstants = {
  formNames: {
    newAddress: {
      address: 'newAddress',
      details: 'newDetails',
      findAddress: 'newFindAddress',
    },
  },
}
