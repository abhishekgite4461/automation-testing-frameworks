/**
 * Payment type strings for supported payment methods.
 */

export const VISA = 'VISA'
export const MASTERCARD = 'MCARD'
export const AMEX = 'AMEX'
export const SWITCH = 'SWTCH'
export const PAYPAL = 'PYPAL'
export const ALIPAY = 'ALIPY'
export const CUPAY = 'CUPAY'
export const SOFORT = 'SOFRT'
export const IDEAL = 'IDEAL'
export const MASTERPASS = 'MPASS'
export const KLARNA = 'KLRNA'
