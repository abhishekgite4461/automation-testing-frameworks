const qasCountries = {
  Canada: 'CAN',
  Denmark: 'DNK',
  Guernsey: 'GGY',
  India: 'IND',
  Indonesia: 'IDN',
  Jersey: 'JEY',
  Malaysia: 'MYS',
  Philippines: 'PHL ',
  Spain: 'ESP ',
  Sweden: 'SWE',
  Turkey: 'TUR',
  'United Kingdom': 'GBR',
}

export default qasCountries
