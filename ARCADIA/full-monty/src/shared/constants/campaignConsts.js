export default {
  socialFeedUrls: {
    lfw: 'https://api-gateway.digital.arcadiagroup.co.uk/social/lfw',
  },
  blogFeedUrls: {
    lfw:
      'https://api-gateway.digital.arcadiagroup.co.uk/social/wordpressgo?feedURL=http://insideout.topshop.com/fashion-week-1/feed?category=fashion-week-1',
  },
}
