import { mockLocalise } from 'test/unit/helpers/test-component'
import {
  productSize,
  noEmoji,
  email,
  password,
  isSameAs,
  isNotSameAs,
  isEqualTo,
  promotionCode,
  smsMobileNumber,
  required,
  ukPhoneNumber,
  usPhoneNumber,
  countryValidate,
} from '../validators'

describe('validators', () => {
  describe('`email`', () => {
    it('returns error in case of email argument as empty string', () => {
      expect(email({ email: '' }, 'email', jest.fn(mockLocalise))).toBe(
        'An email address is required.'
      )
    })
    it('returns error in case of email argument as "abc"', () => {
      expect(email({ email: 'abc' }, 'email', jest.fn(mockLocalise))).toBe(
        'Please enter a valid email address.'
      )
    })
    it('returns error in case of email argument as "abc@abc"', () => {
      expect(email({ email: 'abc@abc' }, 'email', jest.fn(mockLocalise))).toBe(
        'Please enter a valid email address.'
      )
    })
    it('returns error in case of email argument as "abc@abc.abcde"', () => {
      expect(
        email({ email: 'abc@abc.abcde' }, 'email', jest.fn(mockLocalise))
      ).toBe('Please enter a valid email address.')
    })
    it('returns error in case of email argument as "abc@abc.a"', () => {
      expect(
        email({ email: 'abc@abc.a' }, 'email', jest.fn(mockLocalise))
      ).toBe('Please enter a valid email address.')
    })
    it('returns error in case of email argument as "ab+c@abc.com"', () => {
      expect(
        email({ email: 'ab+c@abc.com' }, 'email', jest.fn(mockLocalise))
      ).toBe('Please enter a valid email address.')
    })
    it('returns error in case of email argument as "abc@abc.abc"', () => {
      expect(
        email({ email: 'abc@abc.abc' }, 'email', jest.fn(mockLocalise))
      ).toBe('Please enter a valid email address.')
    })
    it('returns no error in case of email argument as "123@abc.com"', () => {
      expect(
        email({ email: '123@abc.com' }, 'email', jest.fn(mockLocalise))
      ).toBe(null)
    })
    it('returns no error in case of email argument as "abc@abc.com"', () => {
      expect(
        email({ email: 'abc@abc.com' }, 'email', jest.fn(mockLocalise))
      ).toBe(null)
    })
    it('returns no error in case of email argument as "abc@abc.co.uk"', () => {
      expect(
        email({ email: 'abc@abc.co.uk' }, 'email', jest.fn(mockLocalise))
      ).toBe(null)
    })
    it('returns no error in case of email argument as "123abc@123abc.com"', () => {
      expect(
        email({ email: '123abc@123abc.com' }, 'email', jest.fn(mockLocalise))
      ).toBe(null)
    })
    it('returns no error in case of email argument as "123abc@123abc.com.com"', () => {
      expect(
        email(
          { email: '123abc@123abc.com.com' },
          'email',
          jest.fn(mockLocalise)
        )
      ).toBe(null)
    })
  })

  describe('`password`', () => {
    it('password returns error in case of password argument as empty string', () => {
      expect(
        password({ password: '' }, 'password', jest.fn(mockLocalise))
      ).toBe('A password is required.')
    })
    it('password returns error in case of password argument length less than 6 characters', () => {
      expect(
        password({ password: 'abcde' }, 'password', jest.fn(mockLocalise))
      ).toBe('Please enter a password of at least 6 characters.')
    })
    it('password returns no error in case of password argument length at least 6 characters', () => {
      expect(
        password({ password: 'abcdef1' }, 'password', jest.fn(mockLocalise))
      ).toBe(undefined)
    })
    it('password returns error if the password contains no numbers', () => {
      const err = 'Please enter a password containing at least one number'
      expect(
        password({ password: 'abcdef' }, 'password', jest.fn(mockLocalise))
      ).toBe(err)
    })
    it('password returns error if the password contains more than 20 chars', () => {
      const err = 'Please enter a password less than 20 characters.'
      expect(
        password(
          { password: '123456789012345678901' },
          'password',
          jest.fn(mockLocalise)
        )
      ).toBe(err)
    })
  })

  describe('`isSameAs`', () => {
    it('isSameAs returns error in case of non identical arguments', () => {
      const err = 'Please ensure that both passwords match'
      expect(
        isSameAs(
          err,
          'b',
          {
            a: '123',
            b: '234',
          },
          'a'
        )
      ).toBe(err)
    })
    it('isSameAs returns no error in case of identical arguments', () => {
      expect(isSameAs('err', 'b', { a: '123', b: '123' }, 'a')).toBe(undefined)
    })
    it('isSameAs is curried', () => {
      const err = 'Please ensure that both passwords match'
      const data = { a: '123', b: '234' }
      expect(isSameAs(err, 'b', data, 'a')).toBe(isSameAs(err)('b')(data)('a'))
    })
  })

  describe('`isSameAs`', () => {
    it('IsNotSameAs returns nothing if the properties are different', () => {
      const err = 'Password cannot be your email'
      expect(
        isNotSameAs(
          err,
          'b',
          {
            a: '123',
            b: '234',
          },
          'a'
        )
      ).toBe(undefined)
    })

    it('IsNotSameAs returns nothing if the properties are different', () => {
      const err = 'Password cannot be your email'
      expect(
        isNotSameAs(
          err,
          'b',
          {
            a: '123',
            b: '123',
          },
          'a'
        )
      ).toBe(err)
    })

    it('isNotSameAs is curried', () => {
      const err = 'Password cannot be your email'
      const data = { a: '123', b: '134' }
      expect(isNotSameAs(err, 'b', data, 'a')).toBe(
        isNotSameAs(err)('b')(data)('a')
      )
    })
  })

  describe('`isEqualTo`', () => {
    it('isEqualTo returns error if the property is not equal to the value', () => {
      const err = 'Password cannot be your email'
      expect(isEqualTo(err, 321, { a: 123 }, 'a')).toBe(err)
    })

    it('isEqualTo returns undefined if the property is equal to the value', () => {
      const err = 'error message'
      expect(isEqualTo(err, 123, { a: 123 }, 'a')).toBe(undefined)
    })

    it('isEqualTo is curried', () => {
      const err = 'error message'
      expect(isEqualTo(err, 123, { a: 123 }, 'a')).toBe(
        isEqualTo(err)(123)({ a: 123 })('a')
      )
    })
  })

  describe('`promotionCode`', () => {
    it('promotionCode returns an error for input longer than 20 chars', () => {
      const err =
        'The promotion code you have entered has not been recognised. Please confirm the code and try again.'
      expect(
        promotionCode(
          { promotionCode: '123456789012345678901' },
          'promotionCode',
          jest.fn(mockLocalise)
        )
      ).toBe(err)
    })

    it('promotionCode returns undefined for input under 20 chars', () => {
      expect(
        promotionCode(
          { promotionCode: '12345678901234567890' },
          'promotionCode',
          jest.fn(mockLocalise)
        )
      ).toBe(undefined)
    })
  })

  describe('`smsMobileNumber`', () => {
    it('validation for the delivery instructions mobile number tests for a valid UK number with a leading zero', () => {
      expect(
        smsMobileNumber(
          { smsMobileNumber: '07773457453' },
          'smsMobileNumber',
          jest.fn(mockLocalise)
        )
      ).toBe(undefined)
      expect(
        smsMobileNumber(
          { smsMobileNumber: '7773457453' },
          'smsMobileNumber',
          jest.fn(mockLocalise)
        )
      ).toBe('Please enter a valid UK phone number')
    })

    it('smsMobileNumber checks for a valid UK phone number + 0 at beginning', () => {
      expect(
        smsMobileNumber({ a: '7773456453' }, 'a', jest.fn(mockLocalise))
      ).toBe('Please enter a valid UK phone number')
      expect(
        smsMobileNumber({ a: '07773456453' }, 'a', jest.fn(mockLocalise))
      ).toBe(undefined)
    })

    it('smsMobileNumber returns empty string if no value is supplied (not required)', () => {
      expect(smsMobileNumber({ a: null }, 'a', jest.fn(mockLocalise))).toBe('')
    })
  })

  describe('`required`', () => {
    const requireError = 'This field is required'
    it('required returns error in case of argument as empty string', () => {
      expect(required({ a: '' }, 'a', jest.fn(mockLocalise))).toBe(requireError)
    })

    it('required returns NO ERROR if we pass 0 like a number', () => {
      expect(required({ numb: 0 }, 'numb', jest.fn(mockLocalise))).toBe(
        undefined
      )
    })

    it('required return NO error if we pass 0 like a string', () => {
      expect(required({ numbStr: '0' }, 'numbStr', jest.fn(mockLocalise))).toBe(
        undefined
      )
    })

    it('required returns ERROR if given null', () => {
      expect(required({ a: null }, 'a', jest.fn(mockLocalise))).toBe(
        requireError
      )
    })
  })

  describe('`ukPhoneNumber`', () => {
    const validNumberError = 'Please enter a valid phone number'

    it('ukPhoneNumber accepts UK valid numbers (10 digits long)', () => {
      expect(
        ukPhoneNumber({ a: '077 3333 444' }, 'a', jest.fn(mockLocalise))
      ).toBe(undefined)
    })

    it('ukPhoneNumber accepts UK valid numbers (11 digits long)', () => {
      expect(
        ukPhoneNumber({ a: ' 077 3333 4444 ' }, 'a', jest.fn(mockLocalise))
      ).toBe(undefined)
    })

    it('ukPhoneNumber return Error for non UK valid numbers (too short)', () => {
      expect(
        ukPhoneNumber({ a: '077 333891' }, 'a', jest.fn(mockLocalise))
      ).toBe(validNumberError)
    })

    it('ukPhoneNumber return Error for non UK valid numbers (too long)', () => {
      expect(
        ukPhoneNumber({ a: '077 3338 91123' }, 'a', jest.fn(mockLocalise))
      ).toBe(validNumberError)
    })

    it('ukPhoneNumber return Error for non UK valid numbers (not start with 0)', () => {
      expect(
        ukPhoneNumber({ a: '377 3333 444' }, 'a', jest.fn(mockLocalise))
      ).toBe(validNumberError)
    })

    it('ukPhoneNumber returns empty string if no value is given (not required)', () => {
      expect(ukPhoneNumber({ a: null }, 'a', jest.fn(mockLocalise))).toBe('')
    })
  })

  describe('`usPhoneNumber`', () => {
    const validNumberError = 'Please enter a valid phone number'

    it('usPhoneNumber accepts US valid numbers', () => {
      expect(
        usPhoneNumber({ a: '202-555-0117' }, 'a', jest.fn(mockLocalise))
      ).toBe(undefined)
    })

    it('usPhoneNumber return Error for non US valid numbers', () => {
      expect(usPhoneNumber({ a: '202-555' }, 'a', jest.fn(mockLocalise))).toBe(
        validNumberError
      )
    })

    it('usPhoneNumber returns empty string if no value is given (not required)', () => {
      expect(usPhoneNumber({ a: null }, 'a', jest.fn(mockLocalise))).toBe('')
    })
  })

  describe('`noEmoji`', () => {
    it('should error when passed an emoji', () => {
      expect(noEmoji({ wave: '✋' }, 'wave', jest.fn(mockLocalise))).toBe(
        'Please remove all emoji characters'
      )
    })

    it('should return undefined when no emoji is present', () => {
      expect(
        noEmoji({ test: 'test' }, 'test', jest.fn(mockLocalise))
      ).toBeUndefined()
    })
  })

  describe('`productSize`', () => {
    it('should have an error when object is undefined', () => {
      expect(
        productSize(
          { productSize: undefined },
          'productSize',
          jest.fn(mockLocalise)
        )
      ).toBe('Please select your size to continue')
    })

    it('should have an error when object has no size property', () => {
      expect(
        productSize({ productSize: {} }, 'productSize', jest.fn(mockLocalise))
      ).toBe('Please select your size to continue')
    })

    it('should not have an error when the object has a size property', () => {
      expect(
        productSize(
          { productSize: { size: 3 } },
          'productSize',
          jest.fn(mockLocalise)
        )
      ).toBe(undefined)
    })
  })

  describe('`countryValidate`', () => {
    it('countryValidate returns error if the property value is not in the countries array', () => {
      const countries = ['United Kingdom', 'Spain']
      expect(
        countryValidate(
          countries,
          { country: 'Italy' },
          'country',
          jest.fn(mockLocalise)
        )
      ).toBe('Please enter a valid country')
    })

    it('countryValidate returns undefined if the property is equal to the value', () => {
      const countries = ['United Kingdom', 'Spain']
      expect(
        countryValidate(
          countries,
          { country: 'Spain' },
          'country',
          jest.fn(mockLocalise)
        )
      ).toBe(undefined)
    })

    it('countryValidate is curried', () => {
      const countries = ['United Kingdom', 'Spain']
      expect(
        countryValidate(countries, { a: 123 }, 'a', jest.fn(mockLocalise))
      ).toEqual(
        countryValidate(countries)({ a: 123 })('a')(jest.fn(mockLocalise))
      )
    })
  })
})
