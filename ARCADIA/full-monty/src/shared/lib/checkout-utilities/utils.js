import { path, pluck, values } from 'ramda'
import { getDetailsForm } from '../../selectors/checkoutSelectors'

export const fromCookie = (cookie, key) => {
  const name = `${key}=`
  const pairs = cookie.split(';')

  return pairs.reduce((prev, pair) => {
    return pair.indexOf(name) === 0 ? pair.substring(name.length) : prev
  }, '')
}

export const randomize = (array) => {
  const randomArrayIndex = (length) => Math.floor(Math.random() * length)
  return array[randomArrayIndex(array.length)]
}

/**
 * `retry()` is a function that will automatically invoke and re-invoke `fn`.
 *
 * `retry()` will resolve to the value that `fn` is resolved to. However, if
 * `fn` is rejected then `isRetryFn` is invoked with the rejected value of `fn`.
 * The returned value of `isRetryFn` is used to determine if `fn` should
 * or should not be re-invoked.
 *
 * If `isRetryFn` returns truthy then `fn` is re-invoked. If `isRetryFn` returns
 * falsy then `retry()` is rejected to the rejected value of `fn`. If
 * `isRetryFn` is not a function then `fn` is immediately re-invoked.
 *
 * @param {function} fn - a function that returns a Promise
 * @param {function} [isRetryFn] - an escape function that returns Boolean
 * @returns {Promise}
 */
export const retry = (fn, isRetryFn) => {
  return fn()
    .then((result) => result)
    .catch((err) => {
      if (typeof isRetryFn !== 'function') return retry(fn, isRetryFn)
      if (isRetryFn(err)) return retry(fn, isRetryFn)
      throw err
    })
}

/**
 * combineCardOptions is a function that returns an array that combines
 * Card type Payment Methods against other Payment Methods
 *
 * @param {object} [paymentMethods] - an object containing payment methods
 * @returns {Array}
 */
export const combineCardOptions = (paymentMethods) => {
  const cardPaymentOptions = paymentMethods.filter(
    ({ type }) => type === 'CARD'
  )

  const otherPaymentOptions = paymentMethods.filter(
    ({ type }) => type !== 'CARD'
  )

  const combinedCards = cardPaymentOptions.length
    ? [
        {
          value: 'CARD',
          type: 'CARD',
          label: 'Debit and Credit Card',
          description: cardPaymentOptions.map(({ label }) => label).join(', '),
          paymentTypes: cardPaymentOptions,
        },
      ]
    : []

  return [...combinedCards, ...otherPaymentOptions]
}

/**
 * withValue is a helper function that returns a curried function
 *
 * @param {string} [matcher] - a string containing a paymentType Value
 * @returns {function}
 */
export const withValue = (matcher) => ({ value }) => value === matcher

/**
 * standardCardCodesMap maps the correspondence between credit-card-type
 * module card ids and full monty's paymentMethods ids
 */
export const standardCardCodesMap = {
  'master-card': 'MCARD',
  'american-express': 'AMEX',
  maestro: 'SWTCH',
  visa: 'VISA',
}

/**
 * getPaymentTypeFromValidCardTypes is a function that returns a
 * valid Payment Method Identifier used by monty given an array of
 * credit card matchers
 *
 * @param {Array} validCardTypes - an array containing matched card types config
 * @returns {string}
 */
export const getPaymentTypeFromValidCardTypes = (validCardTypes) => {
  const cardCodes = standardCardCodesMap
  const type = path([0, 'type'], validCardTypes)

  return cardCodes[type] ? cardCodes[type] : cardCodes.visa
}

export const getPaymentMethodFromValue = (paymentValue, paymentMethods) => {
  return paymentMethods.find(({ value }) => value === paymentValue)
}

export const isCard = (currPaymentMethodValue, paymentMethods) => {
  const paymentSelected = getPaymentMethodFromValue(
    currPaymentMethodValue,
    paymentMethods
  )
  return !!(
    paymentSelected &&
    (paymentSelected.type === 'CARD' || paymentSelected.type === 'OTHER_CARD')
  )
}

// return true when all the user data are correct, false if one value is null
export const yourDetailsExist = (state) => {
  const yourDetailsState = getDetailsForm('delivery', state)
  const yourDetails = pluck('value', yourDetailsState.fields)
  return values(yourDetails).every((value) => value !== null)
}

const formNames = {
  delivery: {
    address: 'yourAddress',
    details: 'yourDetails',
    findAddress: 'findAddress',
  },
  billing: {
    address: 'billingAddress',
    details: 'billingDetails',
    findAddress: 'billingFindAddress',
  },
}

export const getFormNames = (type) => formNames[type]
