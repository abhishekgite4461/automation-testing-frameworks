import { createVisaForm } from '../create-visa-form'

describe(createVisaForm, () => {
  const vbvForm =
    "<form method='post' action='https://secure-test.worldpay.com/jsp/test/shopper/ThreeDResponseSimulator.jsp?orderCode=2118975_TS_UK_GB_1516719083461'><input type='hidden' name='TermUrl' value='http://local.m.topshop.com:8080/order-complete?paymentMethod=VISA'/><input type='hidden' name='MD' value='MD_226056806225575'/><textarea style='display:none;' name='PaReq'>eJxVUsuOgjAU/RXi2qG0AqK51uCYOC40ZnQzS1KagUQetjCoXz+3CKJndc+5j95HYXnNztafVDot8sWI2s7IWnI4JUrK9VGKWkkOO6l19CutNMYIj/pTOnMCN5hNRxwO4be8cOgqcCxgMyA9xVQlkiivOETistruOR0ApNMgk2q7fnUZfAxxDz+UkdZNoWJO2cT1/GkA5ClBHmWSn4rSOiZFCaSlIIo6r9SNB8wH0hOo1ZknVVXqOSFN09hVUWpMskWRATFOIEPfh9pYGotd05jv7qG7v29v+9PPZBe+YQHEREAcVZIzhwYOtmlRd+7ROXOBtDpEmemCb1YHy2PjmYdLeChQmofCB/GY8bwqgKdQMhf9LD0DeS2LXGIErv1pQyy1MNswyxhbmxU+bxQgwzifX+YoosLNuogJgiH6y7QOUz3FleEwflveECAmlXSXJ93vQOvt1/wDkjy/mQ==&#13&#10</textarea></form><script language='text/javascript' type='text/javascript'>document.getElementsByTagName('form')[0].submit();</script>"

  const originalTermUrl =
    'https://ts.stage.arcadiagroup.ltd.uk/webapp/wcs/stores/servlet/PunchoutPaymentCallBack?orderId=2118975&catalogId=33057&policyId=40006&tran_id=556806&storeId=12556&langId=-1&notifyShopper=0&notifyOrderSubmitted=0'

  it('returns a new Verified By Visa form where id and new "TermUrl" have been added', () => {
    const expectedForm = createVisaForm({ vbvForm, originalTermUrl })
    expect(expectedForm).toMatchSnapshot()

    const regFormId = /id="paymentForm"/
    expect(regFormId.test(expectedForm)).toBeTruthy()

    const expectedTermUrlValue = /name='TermUrl' value='(.*?)'\/>/.exec(
      expectedForm
    )[1]
    const regOrderPath = /\/order-complete\?/
    expect(regOrderPath.test(expectedTermUrlValue)).toBeTruthy()
  })
})
