import {
  assembleAddressPayload,
  assembleFullPayload,
  assembleSessionPayload,
  extractSummary,
  getAddressDetails,
  getDelivery,
  getDeliveryDetails,
  getOrderInfo,
  getOrderLines,
  parseDeliveryDetails,
  splitSelectedDelivery,
  authorizeRequest,
  reAuthorizeRequest,
  loadFormRequest,
  loadScript,
} from '../klarna-utils'
import * as mocks from '../../../../../test/unit/lib/klarna-utils-mocks'
import { reduce, add, filter, pluck } from 'ramda'

describe(assembleAddressPayload.name, () => {
  it('correctly combines the results of the getAddressDetails() calls', () => {
    expect(assembleAddressPayload(mocks.props)).toEqual(
      mocks.expectedAddressDetails
    )
  })
})

describe(assembleSessionPayload.name, () => {
  it('produces the correct payload we need for setting up a Klarna session', () => {
    expect(assembleSessionPayload(mocks.props)).toEqual(
      mocks.expectedSessionPayload
    )
  })
})

describe(assembleFullPayload.name, () => {
  it('generates payload when needUrl and needSession arguments are true', () => {
    expect(assembleFullPayload(true, true, mocks.props)).toEqual(
      mocks.expectedFullPayloadAll
    )
  })

  it('generates payload with no needUrl and needSession argument is true', () => {
    expect(assembleFullPayload(false, true, mocks.props)).toEqual(
      mocks.expectedFullPayloadNoLink
    )
  })

  it('generates payload with both needUrl and needSession arguments set as false', () => {
    expect(assembleFullPayload(false, false, mocks.props)).toEqual(
      mocks.expectedFullPayloadNoSession
    )
  })
})

describe(extractSummary.name, () => {
  it('maps the data correctly for hash creation from props', () => {
    expect(
      extractSummary({
        ...mocks.props,
        orderSummary: mocks.orderSummary,
      })
    ).toEqual(mocks.expectedHashExtract)
  })
})

describe(getAddressDetails.name, () => {
  const {
    billingDetails,
    billingAddress,
    yourDetails,
    yourAddress,
    user,
    klarnaForm,
    config,
  } = mocks
  it('parses the user delivery details and produces the correct output', () => {
    expect(
      getAddressDetails(yourDetails, yourAddress, user, klarnaForm, config)
    ).toEqual(mocks.expectedDeliveryAddressDetails)
  })

  it('parses the user billing details and produces the correct output', () => {
    expect(
      getAddressDetails(
        billingDetails,
        billingAddress,
        user,
        klarnaForm,
        config
      )
    ).toEqual(mocks.expectedBillingAddressDetails)
  })
})

describe(getDelivery.name, () => {
  it('parses the delivery options in basket and returns the correct itemised cost of delivery', () => {
    expect(getDelivery(mocks.shoppingBag.bag)).toEqual(
      mocks.expectedDeliveryOptions
    )
  })
})

describe(getDeliveryDetails.name, () => {
  it('correctly ties splitSelectedDelivery() and parseDeliveryDetails() together and returns parsed, typed result', () => {
    expect(getDeliveryDetails(mocks.getDeliveryDetailsMocks.a.mock)).toEqual(
      mocks.getDeliveryDetailsMocks.a.expected
    )

    expect(getDeliveryDetails(mocks.getDeliveryDetailsMocks.b.mock)).toEqual(
      mocks.getDeliveryDetailsMocks.b.expected
    )
  })
})

describe(getOrderInfo.name, () => {
  it('produces the correct general orderInfo payload', () => {
    expect(getOrderInfo(mocks.config, mocks.shoppingBag.bag)).toEqual(
      mocks.expectedGetOrderInfo
    )
  })
})

describe(getOrderLines.name, () => {
  it('parses the basket and orderSummary and produces the correct itemised payload', () => {
    expect(getOrderLines(mocks.shoppingBag.bag)).toEqual(
      mocks.expectedOrderLines
    )
  })

  it('adds up logically with main totals', () => {
    const actual = getOrderLines(mocks.shoppingBag.bag)
    const bag = mocks.shoppingBag.bag
    const noDiscounts = filter((item) => item.total_amount > 0, actual)

    const noDiscountsTotal = reduce(add, 0, pluck('total_amount', noDiscounts))
    const total = reduce(add, 0, pluck('total_amount', actual))

    expect(noDiscountsTotal).toBe(parseFloat(bag.totalBeforeDiscount) * 100)
    expect(total).toBe(parseFloat(bag.total) * 100)
  })
})

describe(parseDeliveryDetails.name, () => {
  it('will find the correct tokens in the delivery type label (name, price)', () => {
    expect(
      parseDeliveryDetails(mocks.parseDeliveryDetailsMocks.a.mock)
    ).toEqual(mocks.parseDeliveryDetailsMocks.a.expected)

    expect(
      parseDeliveryDetails(mocks.parseDeliveryDetailsMocks.b.mock)
    ).toEqual(mocks.parseDeliveryDetailsMocks.b.expected)
  })
})

describe(splitSelectedDelivery.name, () => {
  it('finds the selected delivery type and splits it into an array', () => {
    expect(
      splitSelectedDelivery(mocks.shoppingBag.bag.deliveryOptions)
    ).toEqual(mocks.expectedSplitSelectedDelivery)
  })
})

describe('Load Klarna', () => {
  const originalDate = Date
  const originalWindow = global.window
  const updateDetails = 'mock updateDetails'
  const mockDate = (date = '2018-01-01T00:00:00') => {
    const constantDate = new Date(date)

    global.Date = () => {
      return constantDate
    }
  }
  const restoreGlobals = () => {
    global.Date = originalDate
    global.window = originalWindow
  }

  global.window.loadScript = jest.fn()

  beforeEach(() => {
    jest.resetModules()
    jest.resetAllMocks()
    mockDate()

    global.window = { ...originalWindow }

    window.loadScript.mockImplementation(({ onload }) => {
      onload()
      return Promise.resolve()
    })
  })

  afterEach(restoreGlobals)

  describe(loadScript.name, () => {
    it('should call loadScript', () => {
      return loadScript().then(() => {
        expect(window.loadScript).toHaveBeenCalledTimes(1)
        expect(window.loadScript).toBeCalledWith(
          expect.objectContaining({
            isAsync: true,
            src: 'https://credit.klarnacdn.net/lib/v1/api.js?1514764800000',
          })
        )
      })
    })
  })

  describe(authorizeRequest.name, () => {
    const authorize = jest.fn()
    const result = {
      show_form: 'mock show_form',
      approved: 'mock approved',
    }

    it('should call window.Klarna.Credit.authorize', () => {
      authorize.mockImplementation((updateDetails, callback) => {
        callback(result)
      })

      global.window.Klarna = {
        Credit: {
          authorize,
        },
      }

      return authorizeRequest(updateDetails).then(() => {
        expect(authorize.mock.calls[0][0]).toEqual(updateDetails)
      })
    })

    it('should call window.Klarna.Credit.authorize and fail', () => {
      const result = 'wrong result'

      authorize.mockImplementation((updateDetails, callback) => {
        callback(result)
      })

      global.window.Klarna = {
        Credit: {
          authorize,
        },
      }

      return authorizeRequest(updateDetails).catch(() => {
        expect(authorize).toHaveBeenCalledTimes(1)
        expect(authorize.mock.calls[0][0]).toEqual(updateDetails)
      })
    })

    it('should call loadScript', () => {
      window.Klarna = undefined

      authorize.mockImplementation((updateDetails, callback) => {
        callback(result)
      })
      window.loadScript.mockImplementation(({ onload }) => {
        global.window.Klarna = {
          Credit: {
            authorize,
          },
        }

        onload()
      })

      return authorizeRequest(updateDetails).catch(() => {
        expect(window.loadScript).toHaveBeenCalledTimes(1)
        expect(authorize).toHaveBeenCalledTimes(1)
      })
    })
  })

  describe(reAuthorizeRequest.name, () => {
    const reauthorize = jest.fn()
    const result = {
      show_form: 'mock show_form',
      approved: 'mock approved',
    }

    it('should call window.Klarna.Credit.reauthorize', () => {
      reauthorize.mockImplementation((updateDetails, callback) => {
        callback(result)
      })

      global.window.Klarna = {
        Credit: {
          reauthorize,
        },
      }

      return reAuthorizeRequest(updateDetails).then(() => {
        expect(reauthorize.mock.calls[0][0]).toEqual(updateDetails)
      })
    })

    it('should call window.Klarna.Credit.reauthorize and fail', () => {
      const result = 'wrong result'

      reauthorize.mockImplementation((updateDetails, callback) => {
        callback(result)
      })

      global.window.Klarna = {
        Credit: {
          reauthorize,
        },
      }

      return reAuthorizeRequest(updateDetails).catch(() => {
        expect(reauthorize).toHaveBeenCalledTimes(1)
        expect(reauthorize.mock.calls[0][0]).toEqual(updateDetails)
      })
    })

    it('should call loadScript', () => {
      window.Klarna = undefined

      reauthorize.mockImplementation((updateDetails, callback) => {
        callback(result)
      })
      window.loadScript.mockImplementation(({ onload }) => {
        global.window.Klarna = {
          Credit: {
            reauthorize,
          },
        }

        onload()
      })

      return reAuthorizeRequest(updateDetails).catch(() => {
        expect(window.loadScript).toHaveBeenCalledTimes(1)
        expect(reauthorize).toHaveBeenCalledTimes(1)
      })
    })
  })

  describe(loadFormRequest.name, () => {
    const client_token = 'mock client_token'
    const container = 'mock client_token'
    const load = jest.fn()
    const result = {
      show_form: 'mock show_form',
      approved: 'mock approved',
    }

    it('should call window.Klarna.Credit.load', () => {
      load.mockImplementation((p, updateDetails, callback) => {
        callback(result)
      })

      global.window.Klarna = {
        Credit: {
          load,
        },
      }

      return loadFormRequest(client_token, container, updateDetails).then(
        () => {
          expect(load).toHaveBeenCalledTimes(1)
          expect(load.mock.calls[0][0]).toEqual({ client_token, container })
        }
      )
    })

    it('should call window.Klarna.Credit.load and fail', () => {
      const result = 'wrong result'

      load.mockImplementation((p, updateDetails, callback) => {
        callback(result)
      })

      global.window.Klarna = {
        Credit: {
          load,
        },
      }

      return loadFormRequest(client_token, container, updateDetails).catch(
        () => {
          expect(load).toHaveBeenCalledTimes(1)
          expect(load.mock.calls[0][0]).toEqual({ client_token, container })
        }
      )
    })

    it('should call loadScript', () => {
      window.Klarna = undefined

      load.mockImplementation((p, updateDetails, callback) => {
        callback(result)
      })
      window.loadScript.mockImplementation(({ onload }) => {
        global.window.Klarna = {
          Credit: {
            load,
          },
        }

        onload()
      })

      return loadFormRequest(client_token, container, updateDetails).catch(
        () => {
          expect(window.loadScript).toHaveBeenCalledTimes(1)
          expect(load).toHaveBeenCalledTimes(1)
        }
      )
    })
  })
})
