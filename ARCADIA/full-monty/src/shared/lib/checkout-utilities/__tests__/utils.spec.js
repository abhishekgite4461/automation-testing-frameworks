import { isCard, getPaymentMethodFromValue, yourDetailsExist } from '../utils'
import { paymentMethods } from 'test/unit/lib/checkout-mocks'

const mockFnisCard = jest.fn().mockImplementation(isCard)
const mockFnGetPay = jest.fn().mockImplementation(getPaymentMethodFromValue)

describe('Checkout utils', () => {
  it('isCard return true if receive visa credit card', () => {
    const test1 = mockFnisCard('VISA', paymentMethods)
    expect(mockFnisCard).toHaveBeenCalledTimes(1)
    expect(test1).toBe(true)
  })

  it('getPaymentMethodFromValue return visa details', () => {
    const test1 = mockFnGetPay('VISA', paymentMethods)
    expect(test1.type).toBe('CARD')
    expect(test1.label).toBe('Visa')
    expect(mockFnisCard).toHaveBeenCalledTimes(1)
  })
  describe('yourDetailsExist', () => {
    const fakeForms = {
      forms: {
        checkout: {
          yourDetails: {
            fields: {
              title: { value: '' },
              firstName: { value: null },
              lastName: { value: null },
              telephone: { value: null },
            },
          },
        },
      },
    }

    const trueForms = {
      forms: {
        checkout: {
          yourDetails: {
            fields: {
              title: { value: 'dr' },
              firstName: { value: 'Enzo' },
              lastName: { value: 'dono' },
              telephone: { value: '' },
            },
          },
        },
      },
    }
    it('should return false if yourDetails does not exist ', () => {
      expect(yourDetailsExist(fakeForms)).toBeFalsy()
    })
    it('should return true if yourDetails does exist ', () => {
      expect(yourDetailsExist(trueForms)).toBeTruthy()
    })
  })
})
