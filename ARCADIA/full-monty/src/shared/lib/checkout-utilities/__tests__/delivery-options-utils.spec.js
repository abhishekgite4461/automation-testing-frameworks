import {
  findSelected,
  isStoreDeliveryLocation,
  isParcelShopDeliveryLocation,
  isStoreOrParcelDeliveryLocation,
  getDefaultDeliveryType,
  getDeliveryTypeFromBasket,
} from '../delivery-options-utils'

describe('findSelected', () => {
  it('returns undefined if selected element can not be found', () => {
    const array = []
    expect(findSelected(array)).toBeUndefined()
  })
  it('returns the selected object from array', () => {
    const array = [{}, { selected: true }, {}]
    expect(findSelected(array)).toBe(array[1])
  })
})

describe('isStoreDeliveryLocation(location)', () => {
  it('returns true if the location type is `STORE`', () => {
    const location = { deliveryLocationType: 'STORE' }
    expect(isStoreDeliveryLocation(location)).toBe(true)
  })

  it('returns false if the location type is not `STORE`', () => {
    const location = { deliveryLocationType: 'HOME' }
    expect(isStoreDeliveryLocation(location)).toBe(false)
  })
})

describe('isParcelShopDeliveryLocation(location)', () => {
  it('returns true if the location type is `PARCELSHOP`', () => {
    const location = { deliveryLocationType: 'PARCELSHOP' }
    expect(isParcelShopDeliveryLocation(location)).toBe(true)
  })

  it('returns false if the location type is not `PARCELSHOP`', () => {
    const location = { deliveryLocationType: 'HOME' }
    expect(isParcelShopDeliveryLocation(location)).toBe(false)
  })
})

describe('isStoreOrParcelDeliveryLocation(location)', () => {
  it('returns true if the location type is `STORE`', () => {
    const location = { deliveryLocationType: 'STORE' }
    expect(isStoreOrParcelDeliveryLocation(location)).toBe(true)
  })

  it('returns true if the location type is `PARCELSHOP`', () => {
    const location = { deliveryLocationType: 'PARCELSHOP' }
    expect(isStoreOrParcelDeliveryLocation(location)).toBe(true)
  })

  it('returns false if the location type is not `HOME`', () => {
    const location = { deliveryLocationType: 'HOME' }
    expect(isStoreOrParcelDeliveryLocation(location)).toBe(false)
  })
})

describe('getDefaultDeliveryType(locationType)', () => {
  it('returns STORE_STANDARD when locationType is STORE', () => {
    expect(getDefaultDeliveryType('STORE')).toBe('STORE_STANDARD')
  })
  it('returns PARCELSHOP_COLLECTION when locationType is PARCELSHOP', () => {
    expect(getDefaultDeliveryType('PARCELSHOP')).toBe('PARCELSHOP_COLLECTION')
  })
  it('returns HOME_STANDARD otherwise', () => {
    expect(getDefaultDeliveryType('')).toBe('HOME_STANDARD')
  })
})

describe('getDeliveryTypeFromBasket(option)', () => {
  it('returns STORE_STANDARD when label contains Store Standard', () => {
    expect(
      getDeliveryTypeFromBasket({
        label: 'Free Collect From Store Standard £0.00',
      })
    ).toBe('STORE_STANDARD')
  })
  it('returns STORE_EXPRESS when label contains Store Express or Collect In Store Exp', () => {
    expect(
      getDeliveryTypeFromBasket({ label: 'Collect From Store Express £3.00' })
    ).toBe('STORE_EXPRESS')
    expect(
      getDeliveryTypeFromBasket({
        label: 'Collect In Store Exp Next Day(Excl Sun) £2.95',
      })
    ).toBe('STORE_EXPRESS')
  })
  it('returns STORE_IMMEDIATE when label contains Store Today', () => {
    expect(
      getDeliveryTypeFromBasket({ label: 'Collect From Store Today £3.00' })
    ).toBe('STORE_IMMEDIATE')
  })
  it('returns PARCELSHOP_COLLECTION when label contains ParcelShop', () => {
    expect(
      getDeliveryTypeFromBasket({ label: 'Collect from ParcelShop £4.00' })
    ).toBe('PARCELSHOP_COLLECTION')
  })
  it('returns null if option does not have a label or label does not match any of the above', () => {
    expect(getDeliveryTypeFromBasket({})).toBe(null)
    expect(getDeliveryTypeFromBasket({ label: 'A-label' })).toBe(null)
  })
})
