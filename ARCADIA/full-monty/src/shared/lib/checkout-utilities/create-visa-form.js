const createVisaForm = function createVisaForm({ vbvForm, originalTermUrl }) {
  const regForm = /\('form'\)(.*?).submit/
  const regPost = /<form(.*?)method/
  const regUrl = /name='TermUrl' value='(.*?)'\/>/
  const originalUrl = originalTermUrl.split('?')[1]
  const orderCompletePath = /order-complete/.exec(vbvForm)[0]

  let newForm = vbvForm.replace(
    regUrl.exec(vbvForm)[1],
    `${window.location.protocol}//${
      window.location.host
    }/${orderCompletePath}?${originalUrl}`
  )
  newForm = newForm.replace(regForm.exec(newForm)[1], '[2]')
  newForm = newForm.replace(regPost.exec(newForm)[1], ' id="paymentForm" ')
  return newForm
}

export { createVisaForm }
