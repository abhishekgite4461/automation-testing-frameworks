import * as faker from './faker'
import { randomize } from './utils'

const createAccount = (email, password = 'password100', subscribe = true) => {
  return { email, password, passwordConfirm: password, subscribe }
}

const createAuthentication = (email, password = 'password100') => {
  return { username: email, password }
}

const createBasketItem = (product) => {
  const items = product.items.filter((item) => item.quantity > 0)
  const item = randomize(items)

  return {
    productId: product.productId,
    sku: item.sku,
    quantity: Math.floor(0.75 * Math.random() * item.quantity) + 1,
  }
}

function getSelectedDeliveryLocation(deliveryLocations) {
  const options = { deliveryType: '', shipModeId: 0, cost: 0, label: '' }

  const location = deliveryLocations.find((item) => item.selected)
  if (!location) return options
  options.deliveryMethod = location.deliveryLocationType
  options.deliveryType = location.deliveryLocationType

  if (location.deliveryMethods && location.deliveryMethods.length === 0) {
    return options
  }

  const method = location.deliveryMethods.find((item) => item.selected)
  // @note: this happening when parcelshop item are not available we need to be full defensive here
  if (!method) return options
  options.deliveryType = method.deliveryType
  options.shipModeId = method.shipModeId
  options.cost = method.cost || 0
  options.label = method.label
  options.additionalDescription = method.additionalDescription

  if (method.deliveryOptions && method.deliveryOptions.length === 0) {
    return options
  }

  const option = method.deliveryOptions.find((item) => item.selected)
  // @note: this should be impossible, but we need to be full defensive here
  // we return back the basic option if no deliveryOptions is selected
  if (!option) return { deliveryType: '', shipModeId: 0, cost: 0, label: '' }
  options.shipModeId = option.shipModeId
  options.nominatedDate = option.nominatedDate
  options.cost = parseFloat(method.cost, 10) === 0 ? 0 : option.price || 0
  options.dayText = option.dayText
  options.dateText = option.dateText
  options.nominatedDate = option.nominatedDate

  return options
}

export const selectedDeliveryLocationSafe = (deliveryLocations) => {
  if (!deliveryLocations) return {}

  return getSelectedDeliveryLocation(deliveryLocations)
}

export const selectedDeliveryLocation = (deliveryLocations) => {
  if (!deliveryLocations) {
    throw new Error(
      `Unexpected value, deliveryLocations:${deliveryLocations}. Consider HTTP GET to '/order-summary'.`
    )
  }

  return getSelectedDeliveryLocation(deliveryLocations)
}

const orderDeliveryOption = (orderSummary) => {
  const { basket, deliveryLocations, shippingCountry } = orderSummary

  return {
    ...selectedDeliveryLocation(deliveryLocations),
    shippingCountry,
    orderId: basket.orderId,
  }
}

const accountLogin = (email, password = 'password100') => {
  return { email, password }
}

const combineNameAndPhone = ({ title, firstName, lastName }, telephone) => {
  return { title, firstName, lastName, telephone }
}

export const payload = {
  createAccount,
  createAuthentication,
  createBasketItem,
  updateOrderSummary: orderDeliveryOption,
}

export const partial = {
  accountCreate: createAccount,
  accountLogin,
  nameAndPhone: combineNameAndPhone,
  deliveryNameAndPhone: combineNameAndPhone,
  orderDeliveryOption,
}

const generateSchema = (isShortSchema = true, authenticationType) => {
  const base = {
    smsMobileNumber: '',
    remoteIpAddress: '',
    cardCvv: 0,
    orderDeliveryOption: {},
    deliveryInstructions: '',
    returnUrl: 'http://0.0.0.0:8080/order/summary?complete=true',
  }

  const extra = {
    deliveryAddress: {},
    billingDetails: { nameAndPhone: {}, address: {} },
    creditCard: {},
    deliveryNameAndPhone: {},
  }

  if (isShortSchema) {
    return { ...base, cardNumberHash: '' }
  }

  switch (authenticationType) {
    case 'new':
      return { ...base, ...extra, accountCreate: {} }
    case 'existing':
      return { ...base, ...extra, accountLogin: {} }
    default:
      return { ...base, ...extra }
  }
}

const populateSchema = (orderSchema, orderSummary, account) => {
  /* eslint-disable no-param-reassign */

  orderSchema.smsMobileNumber = faker.telephone()
  orderSchema.remoteIpAddress = faker.ipAddress()
  orderSchema.orderDeliveryOption = partial.orderDeliveryOption(orderSummary)

  if (Object.prototype.hasOwnProperty.call(orderSchema, 'cardNumberHash')) {
    if (!account) {
      throw new Error('Expected an Account to have been provided.')
    }

    const card = faker.creditCard(account.creditCard.type)
    orderSchema.cardCvv = card.cardCvv
    orderSchema.cardNumberHash = account.creditCard.cardNumberHash

    return Promise.resolve(orderSchema)
  }

  return faker
    .profile()
    .then(({ address, email, name, telephone }) => {
      const { creditCard, cardCvv } = faker.creditCard()
      const nameAndPhone = partial.nameAndPhone(name, telephone)

      orderSchema.deliveryAddress = address
      orderSchema.billingDetails = { address, nameAndPhone }
      orderSchema.creditCard = creditCard
      orderSchema.cardCvv = cardCvv
      orderSchema.deliveryNameAndPhone = nameAndPhone

      return { orderSchema, email }
    })
    .then(({ orderSchema: schema, email }) => {
      if (!account) {
        return { schema, email }
      }

      if (account.deliveryDetails.addressDetailsId > -1) {
        schema.deliveryAddress = account.deliveryDetails.address
        schema.deliveryNameAndPhone = account.deliveryDetails.nameAndPhone
      }

      if (account.billingDetails.addressDetailsId > -1) {
        schema.billingDetails.address = account.billingDetails.address
        schema.billingDetails.nameAndPhone = account.billingDetails.nameAndPhone
      }

      return { schema, email: account.email }
    })
    .then(({ schema, email }) => {
      if (Object.prototype.hasOwnProperty.call(schema, 'accountCreate')) {
        schema.accountCreate = partial.accountCreate(email)
      }

      if (Object.prototype.hasOwnProperty.call(schema, 'accountLogin')) {
        schema.accountLogin = partial.accountLogin(email)
      }

      return schema
    })

  /* eslint-enable no-param-reassign */
}

const verifiedByVisa = (orderPayload, isAuthorised = true) => {
  const vbvName = {
    firstName: '3D.',
    lastName: isAuthorised ? 'AUTHORISED' : 'REFUSED',
  }

  try {
    return {
      ...orderPayload,
      billingDetails: {
        ...orderPayload.billingDetails,
        nameAndPhone: {
          ...orderPayload.billingDetails.nameAndPhone,
          ...vbvName,
        },
      },
    }
  } catch (e) {
    throw e
  }
}

const paypalSchema = (orderPayload) => {
  const paypalData = {
    expiryYear: '2017',
    cardNumber: '0',
    type: 'PYPAL',
    expiryMonth: '01',
  }

  return {
    ...orderPayload,
    creditCard: paypalData,
    cardCvv: '0',
  }
}

export const orderPayloadHelpers = {
  generateSchema,
  populateSchema,
  verifiedByVisa,
  paypalSchema,
}
