import request from 'superagent'
import querystring from 'querystring'
import { randomize, retry } from './utils'

const _getRequest = (url) => {
  return request
    .get(url)
    .set('Content-Type', 'application/json')
    .accept('application/json')
}

const _getTitle = (gender) => {
  const titles = {
    male: ['Mr', 'Dr'],
    female: ['Dr', 'Ms', 'Miss', 'Mrs'],
  }
  return randomize(titles[gender])
}

export const name = () => {
  const url = 'http://uinames.com/api/'
  const query = { amount: 25, region: 'england', gender: 'random' }

  return new Promise((resolve, reject) => {
    _getRequest(`${url}?${querystring.stringify(query)}`).end((err, resp) => {
      if (err) {
        reject(err)
      } else if (resp.statusCode === 200) {
        const user = randomize(resp.body)
        resolve({
          title: _getTitle(user.gender),
          firstName: user.name,
          lastName: user.surname,
        })
      } else {
        reject(resp)
      }
    })
  })
}

const _makeEmail = (firstName, lastName) => {
  return `${firstName}.${lastName}.${Date.now()}@test.com`.toLowerCase()
}

export const credentials = (password = 'password100') => {
  return name().then(({ firstName, lastName }) => ({
    username: _makeEmail(firstName, lastName),
    password,
  }))
}

const _getRandomPostcode = () => {
  const cacheBuster = Date.now()
  return new Promise((resolve, reject) => {
    _getRequest(`http://postcodes.io/random/postcodes?q=${cacheBuster}`).end(
      (err, resp) => {
        if (err) {
          reject(err)
        } else if (resp.statusCode === 200) {
          resolve({ postcode: resp.body.result.postcode })
        } else {
          reject(resp)
        }
      }
    )
  })
}

const _getRandomAddress = (postcode) => {
  const url = 'https://qas.api.arcadiagroup.co.uk/qas/json'
  const query = { method: 'search', country: 'GBR', postcode }

  return new Promise((resolve, reject) => {
    _getRequest(`${url}?${querystring.stringify(query)}`).end((err, resp) => {
      if (err) {
        reject(err)
      } else if (resp.statusCode === 200) {
        const addresses = JSON.parse(resp.text).picklistList

        if (addresses.length === 0) {
          reject(resp)
        } else {
          resolve(randomize(addresses))
        }
      } else {
        reject(resp)
      }
    })
  })
}

const _getNormalizedAddress = (moniker) => {
  const url = 'https://qas.api.arcadiagroup.co.uk/qas/json'
  const query = { method: 'address', country: 'GBR', moniker }

  return new Promise((resolve, reject) => {
    _getRequest(`${url}?${querystring.stringify(query)}`).end((err, resp) => {
      if (err) {
        reject(err)
      } else if (resp.statusCode === 200) {
        const raw = JSON.parse(resp.text)
        resolve({
          address1: raw.addressLine1,
          address2: raw.addressLine2,
          city: raw.townCity,
          country: raw.country,
          postcode: raw.postcode,
        })
      } else {
        reject(resp)
      }
    })
  })
}

/**
 * When the returned Promise is resolved successful the payload will contain a
 * randomly selected, well formatted, and full UK residential address.
 *
 * @returns {Promise}
 */
export const address = () => {
  /**
   * A function that returns a Promise that should resolve to a randomly
   * selected, well formatted, and full UK residential address.
   *
   * Internally, _getAddress will use isValidFormat to determine if the obtained
   * candidate-address is correctly formatted if not. If isValidFormat deems a
   * candidate-address to be malformed then the returned Promise of _getAddress
   * will resolve unsuccessful.
   *
   * @returns {Promise}
   * @private
   */
  const _getAddress = () => {
    const isValidFormat = (line) => {
      return !!(/^[\d]+?[a-zA-Z]? /g.test(line) && !line.includes(','))
    }

    return new Promise((resolve, reject) => {
      _getRandomPostcode()
        .then(({ postcode }) => _getRandomAddress(postcode))
        .then(({ moniker }) => _getNormalizedAddress(moniker))
        .then(
          (_address) =>
            isValidFormat(_address.address1)
              ? resolve(_address)
              : reject(_address)
        )
        .catch(reject)
    })
  }

  return retry(_getAddress)
}

export const telephone = (prefix = '07') => {
  return (
    prefix +
    Math.random()
      .toFixed(11 - prefix.length)
      .slice(2)
  )
}

export const creditCard = (type = 'VISA', isExpired = false) => {
  const getCreditCard = (cardNumber, cvv = 123) => {
    const year = (new Date().getFullYear() + (isExpired ? -1 : +1)).toString()
    return {
      creditCard: { expiryYear: year, cardNumber, type, expiryMonth: '09' },
      cardCvv: cvv,
    }
  }

  switch (type) {
    case 'PYPAL':
      return getCreditCard('0', 0)
    case 'AMEX':
      return getCreditCard('343434343434343', 1234)
    case 'MCARD':
      return getCreditCard('1001280000000005')
    case 'SWTCH':
      return getCreditCard('6759820000000019')
    case 'VISA':
      return getCreditCard('4444333322221152')
    default:
      throw new Error(`Unknown Credit Card type, "${type}"`)
  }
}

export const profile = () => {
  return Promise.all([name(), address()]).then((details) => ({
    name: details[0],
    email: _makeEmail(details[0].firstName, details[0].lastName),
    telephone: telephone(),
    address: details[1],
  }))
}

const random = (min = 1, max = 255) => {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

export const ipAddress = () => {
  return `${random(1, 223)}.${random()}.${random()}.${random(1, 254)}`
}
