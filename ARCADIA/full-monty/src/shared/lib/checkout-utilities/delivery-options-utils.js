import { __, find, path, pathOr, prop, test } from 'ramda'

export function matchesParcelShopLabel({ label }) {
  return label.includes('ParcelShop')
}

export function isSelected({ selected }) {
  return selected
}

export const findSelected = find(prop('selected'))

export function isStoreDeliveryLocation(location) {
  return path(['deliveryLocationType'], location) === 'STORE'
}

export function isParcelShopDeliveryLocation(location) {
  return path(['deliveryLocationType'], location) === 'PARCELSHOP'
}

export function isStoreOrParcelDeliveryLocation(location) {
  return (
    isStoreDeliveryLocation(location) || isParcelShopDeliveryLocation(location)
  )
}

export function getDefaultDeliveryType(locationType) {
  if (locationType === 'STORE') return 'STORE_STANDARD'
  if (locationType === 'PARCELSHOP') return 'PARCELSHOP_COLLECTION'
  return 'HOME_STANDARD'
}

// @NOTE No need to get the type for HOME delivery since it is populated by default
export function getDeliveryTypeFromBasket(option) {
  // (Regex) => boolean
  const testOpt = test(__, pathOr('', ['label'], option))

  if (testOpt(/Store.*Standard/i)) {
    return 'STORE_STANDARD'
  } else if (
    testOpt(/Store.*Express/i) ||
    testOpt(/Collect.*In.*Store.*Exp.*Next/i)
  ) {
    return 'STORE_EXPRESS'
  } else if (testOpt(/Store.*Today/i)) {
    return 'STORE_IMMEDIATE'
  } else if (testOpt(/ParcelShop/i)) {
    return 'PARCELSHOP_COLLECTION'
  }
  return null
}
