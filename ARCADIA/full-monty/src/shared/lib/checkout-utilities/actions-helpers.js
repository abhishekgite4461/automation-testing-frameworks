export function getDefaultAddress() {
  return {
    address1: null,
    address2: null,
    city: null,
    state: null,
    country: null,
    postcode: null,
  }
}

export function getDefaultNameAndPhone() {
  return {
    title: '',
    firstName: null,
    lastName: null,
    telephone: null,
  }
}

export function getDefaultPaymentOptions(
  paymentOptions,
  expiryMonths,
  expiryYears
) {
  const currentMonth = new Date().getMonth()
  const paymentType = paymentOptions.length ? paymentOptions[0].value : null
  return {
    paymentType,
    cardNumber: '',
    expiryMonth: expiryMonths[currentMonth],
    expiryYear: expiryYears[0],
    startMonth: '',
    startYear: '',
    cvv: '',
  }
}

export function isUserCreditCard(user) {
  // note: if `user === null` then (typeof user === 'object') returns true
  return !!(typeof user === 'object' && user.creditCard && user.creditCard.type)
}

export function outOfStockMultiLang(message) {
  return (
    message.includes('out of stock') ||
    message.includes('plus en stock') ||
    message.includes('nicht auf Lager')
  )
}
