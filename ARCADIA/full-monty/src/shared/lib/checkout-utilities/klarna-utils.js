import { pluck, pick, prop, map, compose, omit } from 'ramda'
import md5 from 'md5'
import diacritics from 'diacritics'

// HACK: For some reason WCS is sending pence to Klarna,
// so to match up we need to multiply all amounts by 100

// Needs config object from the store
export const getOrderInfo = (config, basket) => {
  const { currencyCode, lang, locale } = config
  return {
    purchase_country: locale,
    purchase_currency: currencyCode,
    locale: `${lang}-${locale}`,
    order_amount: parseFloat(basket.total) * 100,
  }
}

export const splitSelectedDelivery = (deliveryOptions) => {
  const selectedDelivery = deliveryOptions.filter((item) => item.selected)[0]
  return selectedDelivery ? selectedDelivery.label.split(' ') : null
}

export const parseDeliveryDetails = (split) => {
  const deliveryName =
    split && split.filter((item) => !/\d+[.,]\d+|[£$€]/.test(item)).join(' ')
  const priceWithCurrency =
    split && split.find((item) => /\d+[.,]\d+/.test(item))
  const priceString = /(\d+[.,]\d+)/.exec(priceWithCurrency)[1]
  return { deliveryName, priceString }
}

export const getDeliveryDetails = (deliveryOptions) => {
  const split = splitSelectedDelivery(deliveryOptions)
  const { deliveryName, priceString } = parseDeliveryDetails(split)
  const deliveryPrice = priceString
    ? parseFloat(priceString.replace(/,/g, '.'))
    : null
  return { deliveryName, deliveryPrice }
}

export const getDelivery = (basket) => {
  const { deliveryName, deliveryPrice } = getDeliveryDetails(
    basket.deliveryOptions
  )
  if (!deliveryName || !deliveryPrice) return null

  return {
    name: deliveryName,
    quantity: 1,
    unit_price: deliveryPrice * 100,
    total_amount: deliveryPrice * 100,
  }
}

const getDiscounts = (basket) => {
  const { discounts } = basket
  return discounts.map((item) => {
    return {
      name: 'discount',
      quantity: 1,
      unit_price: parseFloat(item.value) * -100,
      total_amount: parseFloat(item.value) * -100,
    }
  })
}

export const getOrderLines = (basket) => {
  const getProducts = () => {
    return basket.products.map((item) => {
      return {
        name: item.name,
        quantity: item.quantity,
        unit_price: parseFloat(item.unitPrice) * 100,
        total_amount: parseFloat(item.totalPrice) * 100,
        image_url: item.assets[0] ? item.assets[0].url : '',
      }
    })
  }
  const orderLines = [...getProducts(), ...getDiscounts(basket)]
  const delivery = getDelivery(basket)
  if (delivery) orderLines.push(delivery)
  return orderLines
}

const getRegion = (county, state) => {
  if (county) return county
  if (state) return state
  return ''
}

const getEmail = (user, form) => {
  if (form.fields.email.value) return form.fields.email.value
  if (user.email) return user.email
  return ''
}

export const getAddressDetails = (
  details,
  address,
  user,
  form,
  config,
  orderSummary
) => {
  const detailsValues = pluck('value', details.fields)
  const addressValues = pluck('value', address.fields)
  const { locale } = config
  const storeDetails = orderSummary ? orderSummary.storeDetails : undefined

  return {
    given_name: detailsValues.firstName,
    family_name: detailsValues.lastName,
    title: detailsValues.title,
    email: getEmail(user, form),
    phone: detailsValues.telephone,
    street_address: storeDetails
      ? storeDetails.address1
      : addressValues.address1,
    street_address2: storeDetails
      ? storeDetails.address2
      : addressValues.address2,
    postal_code: storeDetails ? storeDetails.postcode : addressValues.postcode,
    city: storeDetails ? storeDetails.city : addressValues.city,
    region: storeDetails
      ? ''
      : getRegion(addressValues.county, addressValues.state),
    country: locale.toUpperCase(),
  }
}

const removeDiacriticsDeep = (object) => {
  // eslint-disable-next-line no-restricted-syntax
  for (const key in object) {
    if (Object.prototype.hasOwnProperty.call(object, key)) {
      const value = object[key]
      if (typeof value === 'object') {
        object[key] = removeDiacriticsDeep(value)
      } else if (typeof value === 'string') {
        object[key] = diacritics.remove(value)
      }
    }
  }
  return object
}

export function assembleSessionPayload({ config, shoppingBag }) {
  return {
    ...getOrderInfo(config, shoppingBag.bag),
    order_lines: getOrderLines(shoppingBag.bag),
  }
}

export function assembleAddressPayload(props) {
  const {
    billingDetails,
    billingAddress,
    yourDetails,
    yourAddress,
    user,
    klarnaForm,
    config,
    orderSummary,
  } = props
  return removeDiacriticsDeep({
    billing_address: getAddressDetails(
      billingDetails,
      billingAddress,
      user,
      klarnaForm,
      config
    ),
    shipping_address: getAddressDetails(
      yourDetails,
      yourAddress,
      user,
      klarnaForm,
      config,
      orderSummary
    ),
  })
}

export function assembleFullPayload(
  needUrl = false,
  needSession = true,
  props
) {
  const { shoppingBag, orderCompletePath } = props
  const sessionPayload = needSession ? assembleSessionPayload(props) : {}
  return {
    ...sessionPayload,
    ...assembleAddressPayload(props),
    merchant_urls: needUrl
      ? {
          confirmation: `${window.location.protocol}//${
            window.location.host
          }/${orderCompletePath}?klarnaOrderId=${shoppingBag.bag.orderId}`,
        }
      : undefined,
  }
}

export function hash(orderSummary) {
  const string = JSON.stringify(orderSummary)
  return md5(string)
}

export function extractSummary(props) {
  const {
    shoppingBag,
    orderSummary: { storeDetails },
  } = props
  const fieldNames = [
    'yourDetails',
    'yourAddress',
    'billingAddress',
    'billingDetails',
    'billingCardDetails',
  ]
  const getForms = compose(
    map(map(prop('value'))),
    pluck('fields'),
    pick(fieldNames)
  )
  const fields = {
    ...shoppingBag,
    ...omit(['billingDetails', 'deliveryDetails'], props.orderSummary),
  }
  const discounts = getDiscounts(shoppingBag.bag)
  return { ...getForms(props), ...fields, discounts, storeDetails }
}

export function hashOrderSummary(props) {
  return hash(extractSummary(props))
}

export function loadScript() {
  if (document) {
    return new Promise((resolve) => {
      const time = new Date().getTime()
      const src = `https://credit.klarnacdn.net/lib/v1/api.js?${time}`

      window.loadScript({ src, isAsync: true, onload: resolve })
    })
  }
  return Promise.reject()
}

export function authorizeRequest(updateDetails) {
  const promisify = () => {
    return new Promise((resolve, reject) => {
      window.Klarna.Credit.authorize(updateDetails, (res, err) => {
        if (err) return reject(false)
        return !res.show_form && !res.approved ? reject(res) : resolve(res)
      })
    })
  }

  if (!window.Klarna) {
    return loadScript().then(() => {
      return promisify()
    })
  }
  return promisify()
}

export const reAuthorizeRequest = (updateDetails) => {
  const promisify = () => {
    return new Promise((resolve, reject) => {
      window.Klarna.Credit.reauthorize(updateDetails, (res) => {
        if (!res.show_form && !res.approved) {
          reject(res)
        } else {
          resolve(res)
        }
      })
    })
  }

  if (!window.Klarna) {
    return loadScript().then(() => {
      return promisify()
    })
  }
  return promisify()
}

export const loadFormRequest = (client_token, container, updateDetails) => {
  const promisify = () => {
    return new Promise((resolve, reject) => {
      window.Klarna.Credit.load(
        { client_token, container },
        updateDetails,
        (res, err) => {
          if (err) return reject(err)
          return !res.show_form ? reject(res) : resolve(res)
        }
      )
    })
  }

  if (!window.Klarna) {
    return loadScript().then(() => {
      return promisify()
    })
  }
  return promisify()
}
