import urlp from 'url'
import { browserHistory } from 'react-router'
import { changeURL, getHostname, isProductionBrandHost } from './window'
import {
  ATT_BADGE,
  ATT_BANNER,
  PROMO_BANNER,
} from '../constants/productAssetTypes'
import { productListPageSize } from '../../server/api/mapping/constants/plp'

const mobileAssetType = (additionalAssetType) => ({ assetType }) =>
  assetType === `${additionalAssetType}_MOBILE`

const mobileAttBadge = mobileAssetType(ATT_BADGE)
const mobileAttBanner = mobileAssetType(ATT_BANNER)
const mobilePromoBanner = mobileAssetType(PROMO_BANNER)

const processRedirectUrl = (url) => {
  if (process.browser) {
    if (url.indexOf('/') === 0) {
      // relative url
      browserHistory.replace(url)
    } else {
      const urlObject = urlp.parse(url)
      if (isProductionBrandHost(getHostname(), urlObject.hostname)) {
        // internal redirect
        browserHistory.replace(urlObject.path)
      } else {
        browserHistory.goBack()
        changeURL(url)
      }
    }
  }
}

const isLowStockProduct = (quantity, stockThreshold = 0) => {
  if (stockThreshold === 0) stockThreshold = 3 // Client patch in case of missed configuration
  return quantity <= stockThreshold
}

const parseCurrentPage = (productsLength) =>
  productsLength ? Math.ceil(productsLength / productListPageSize) + 1 : 1

export {
  mobileAttBadge,
  mobileAttBanner,
  mobilePromoBanner,
  processRedirectUrl,
  isLowStockProduct,
  parseCurrentPage,
}
