export function getViewport() {
  const w = Math.max(
    document.documentElement.clientWidth,
    window.innerWidth || 0
  )
  const h = Math.max(
    document.documentElement.clientHeight,
    window.innerHeight || 0
  )

  return { width: w, height: h }
}

export function touchDetection() {
  // navigator.maxTouchPoints works on IE10/11 and Surface
  return (
    typeof window !== 'undefined' &&
    ('ontouchstart' in window || navigator.maxTouchPoints > 0)
  )
}

export function getTotalDocumentHeight() {
  let body = document.querySelector('.Main-body')

  if (!body) {
    body = document.body
  }

  return Math.max(body.scrollHeight, body.offsetHeight, body.clientHeight)
}

export function getScrollTop() {
  return window.pageYOffset || document.body.scrollTop
}

export function getWindowHeight() {
  return window.innerHeight
}

export function getParams() {
  const documentHeight = getTotalDocumentHeight()
  const scrollTop = getScrollTop()
  const windowHeight = getWindowHeight()

  return {
    documentHeight,
    scrollTop,
    windowHeight,
  }
}
