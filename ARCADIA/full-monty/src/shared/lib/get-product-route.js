// remove the domain, if applicable
export const getRouteFromUrl = (url = '') => {
  const urlMatch = url.match(/^(?:.*\/\/[^/]+)?(\/.*)/, '')
  return urlMatch ? urlMatch[1] : null
}

export const removeQueryFromPathname = (url = '') => {
  return url.split('?')[0]
}

const CURRENT_PATH_REGEX = /\w((\/\w*){2})/

export function getProductRouteFromId(pathname, id, localise) {
  const urlMatch = pathname && id && pathname.match(CURRENT_PATH_REGEX)
  return urlMatch && urlMatch.length > 1
    ? `${urlMatch[1]}/${localise('product')}/${id}`
    : null
}

export function getProductRouteFromParams(...args) {
  return `/${args.join('/')}`
}
