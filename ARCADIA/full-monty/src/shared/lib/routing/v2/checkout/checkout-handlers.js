import { all, isEmpty, isNil, map, pathOr, prop } from 'ramda'
import { requiresAuth } from '../auth/auth-handlers'
import { isAuth } from '../../v1/auth/auth-helpers'
import { hasDeliveryAndCC } from '../../v1/checkout/checkout-helpers'
import {
  isReturningCustomer,
  selectedDeliveryLocationTypeEquals,
} from '../../../../selectors/checkoutSelectors'
import {
  setDeliveryAsBillingFlag,
  setSavePaymentDetailsEnabled,
  loadFulfilmentStoreFromCookie,
} from '../../../../actions/common/checkoutActions'
import { resetFormDirty } from '../../../../actions/common/formActions'
import { getItem } from '../../../../../client/lib/cookie'
import { isFeatureSavePaymentDetailsEnabled } from '../../../../selectors/featureSelectors'
import { getSelectedBrandFulfilmentStore } from '../../../../reducers/common/selectedBrandFulfilmentStore'
import { yourDetailsExist } from '../../../checkout-utilities/utils'

export const isYourDetailsFormEmpty = (state) => {
  const yourDetailsFields = pathOr(
    {},
    ['forms', 'checkout', 'yourDetails', 'fields'],
    state
  )
  const yourDetailsValues = map(prop('value'), yourDetailsFields)
  return all(isEmpty, yourDetailsValues)
}

export const fillYourDetailsForm = (state, dispatch) => {
  const userNameAndPhone = pathOr(
    {},
    ['account', 'user', 'deliveryDetails', 'nameAndPhone'],
    state
  )
  dispatch(resetFormDirty('yourDetails', userNameAndPhone))
}

export const isYourAddressFormEmpty = (state) => {
  const yourAddressFields = pathOr(
    {},
    ['forms', 'checkout', 'yourAddress', 'fields'],
    state
  )
  const yourAddressValues = map(prop('value'), yourAddressFields)
  return all(isEmpty, yourAddressValues)
}

export const fillYourAddressForm = (state, dispatch) => {
  const userAddress = pathOr(
    {},
    ['account', 'user', 'deliveryDetails', 'address'],
    state
  )
  dispatch(resetFormDirty('yourAddress', userAddress))
}

export const redirectToDeliveryPayment = (state, dispatch, replace) => {
  if (selectedDeliveryLocationTypeEquals(state, 'HOME')) {
    dispatch(setDeliveryAsBillingFlag(true))
    if (isYourDetailsFormEmpty(state)) {
      fillYourDetailsForm(state, dispatch)
    }
    if (isYourAddressFormEmpty(state)) {
      fillYourAddressForm(state, dispatch)
    }
  }
  replace('/checkout/delivery-payment')
}

export const shouldLoadFulfilmentStoreFromCookie = (state) => {
  const deliveryStoreDetails = getSelectedBrandFulfilmentStore(state)
  const pickUpStoreId = getItem('WC_pickUpStore')
  return isEmpty(deliveryStoreDetails) && !isNil(pickUpStoreId)
}

export const redirectToDelivery = (state, nextState, dispatch, replace) => {
  if (shouldLoadFulfilmentStoreFromCookie(state)) {
    dispatch(loadFulfilmentStoreFromCookie())
  }
  replace(`/checkout/delivery${pathOr('', ['location', 'search'], nextState)}`)
}

export const checkoutRedirect = (
  { getState, dispatch },
  nextState,
  replace
) => {
  const state = getState()

  switch (true) {
    case !isAuth(state):
      replace('/checkout/login')
      break
    case isReturningCustomer(state):
      redirectToDeliveryPayment(state, dispatch, replace)
      break
    default:
      redirectToDelivery(state, nextState, dispatch, replace)
  }
}

export const paymentRedirect = ({ getState }, nextState, replace) => {
  return process.browser
    ? requiresAuth({ getState }, nextState, replace)
    : replace('/checkout/delivery')
}

export const summaryRedirect = ({ getState }, nextState, replace) => {
  const state = getState()
  const user = state.account.user

  switch (true) {
    case !isAuth(state):
      replace('/checkout/login')
      break
    case !process.browser && !hasDeliveryAndCC(user):
      replace('/checkout/delivery')
      break
    default:
  }
}

const resetSavePaymentDetails = function resetSavePaymentDetails(
  state,
  dispatch
) {
  if (isFeatureSavePaymentDetailsEnabled(state)) {
    dispatch(setSavePaymentDetailsEnabled(true))
  }
}

const onEnterPayment = function onEnterPayment(
  { getState, dispatch },
  nextState,
  replace
) {
  const state = getState()

  if (isAuth(state)) {
    if (yourDetailsExist(state)) {
      resetSavePaymentDetails(state, dispatch)
    } else {
      replace('/checkout/delivery')
    }
  } else {
    replace('/checkout/login')
  }
}

export { onEnterPayment, resetSavePaymentDetails }
