jest.mock('../../../v1/checkout/checkout-handlers.js', () => ({
  checkoutRedirect: (a, b, replace) => {
    replace()
  },
}))
import { requiresAuth, requiresNotAuth } from '../auth-handlers'

const getState = () => ({
  auth: {
    authentication: 'authenticated',
  },
})

const getFalseState = () => ({
  auth: {
    authentication: false,
  },
})

describe('Auth Routing V2 Handlers', () => {
  describe('requiresAuth', () => {
    it('should do nothing if logged', () => {
      const replace = jest.fn()
      requiresAuth({ getState }, null, replace)
      expect(replace).toHaveBeenCalledTimes(0)
    })

    it('should redirect to /checkout/login if not logged in', () => {
      const replace = jest.fn()
      requiresAuth(
        {
          getState: getFalseState,
        },
        null,
        replace
      )
      expect(replace).toHaveBeenCalledTimes(1)
      expect(replace).lastCalledWith('/checkout/login')
    })
  })

  describe('requiresNotAuth', () => {
    it('should call checkoutRedirect if logged in', () => {
      const replace = jest.fn()
      requiresNotAuth({ getState }, null, replace)
      expect(replace).toHaveBeenCalledTimes(1)
    })

    it('should do nothing if not logged in', () => {
      const replace = jest.fn()
      requiresNotAuth({ getState: getFalseState }, null, replace)
      expect(replace).toHaveBeenCalledTimes(0)
    })
  })
})
