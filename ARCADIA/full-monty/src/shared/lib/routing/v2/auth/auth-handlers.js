import { isAuth } from '../../v1/auth/auth-helpers'
import { checkoutRedirect } from '../../v1/checkout/checkout-handlers'

export const requiresAuth = ({ getState }, nextState, replace) => {
  if (isAuth(getState())) return
  replace('/checkout/login')
}

export const requiresNotAuth = ({ getState }, nextState, replace) => {
  if (isAuth(getState())) {
    checkoutRedirect({ getState }, nextState, replace)
  }
}
