jest.mock('../../checkout/checkout-handlers.js', () => ({
  checkoutRedirect: (a, b, replace) => {
    replace()
  },
}))
import {
  checkAuthentication,
  requiresAuthOrAnonymous,
  requiresNotAuthOrAnon,
} from '../auth-handlers'

const getState = () => ({
  auth: {
    authentication: 'authenticated',
  },
})

const getFalseState = () => ({
  auth: {
    authentication: false,
  },
  routing: {
    location: {
      query: {
        isAnonymous: null,
      },
    },
  },
})

describe('Auth Routing Handlers', () => {
  describe('checkAuthentication', () => {
    it('should redirect to /my-account if logged in and going to /login', () => {
      const replace = jest.fn()
      const nextState = {
        location: {
          pathname: '/login',
        },
      }
      checkAuthentication({ getState }, nextState, replace)
      expect(replace).toHaveBeenCalledTimes(1)
      expect(replace).lastCalledWith(
        {
          nextPathname: '/login',
        },
        '/my-account',
        undefined
      )
    })

    it('should redirect to /login if not logged in and going to /my-account', () => {
      const replace = jest.fn()
      const nextState = {
        location: {
          pathname: '/my-account',
        },
      }
      checkAuthentication(
        {
          getState: getFalseState,
        },
        nextState,
        replace
      )
      expect(replace).toHaveBeenCalledTimes(1)
      expect(replace).lastCalledWith(
        {
          nextPathname: '/my-account',
        },
        '/login',
        undefined
      )
    })

    it('should do nothing if destination is not login or my-account', () => {
      const replace = jest.fn()
      const nextState = {
        location: {
          pathname: '/nothing',
        },
      }
      checkAuthentication(
        {
          getState: getFalseState,
        },
        nextState,
        replace
      )
      expect(replace).toHaveBeenCalledTimes(0)
    })
  })

  describe('requiresAuthOrAnonymous', () => {
    it('should do nothing if logged in or anonymous', () => {
      const replace = jest.fn()
      requiresAuthOrAnonymous({ getState }, null, replace)
      expect(replace).toHaveBeenCalledTimes(0)
    })

    it('should redirect to /checkout/login if not logged in or anonymous', () => {
      const replace = jest.fn()
      requiresAuthOrAnonymous(
        {
          getState: getFalseState,
        },
        null,
        replace
      )
      expect(replace).toHaveBeenCalledTimes(1)
      expect(replace).lastCalledWith('/checkout/login')
    })
  })

  describe('requiresNotAuthOrAnon', () => {
    it('should call checkoutRedirect if logged in', () => {
      const replace = jest.fn()
      requiresNotAuthOrAnon({ getState }, null, replace)
      expect(replace).toHaveBeenCalledTimes(1)
    })

    it('should do nothing if not logged in', () => {
      const replace = jest.fn()
      requiresNotAuthOrAnon({ getState: getFalseState }, null, replace)
      expect(replace).toHaveBeenCalledTimes(0)
    })
  })
})
