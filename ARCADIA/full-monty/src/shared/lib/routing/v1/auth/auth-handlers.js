import { isAuth, isAuthOrAnonymous } from './auth-helpers'
import { checkoutRedirect } from '../checkout/checkout-handlers'

export const checkAuthentication = ({ getState }, nextState, replace) => {
  const authenticated = isAuth(getState())

  let path
  switch (true) {
    case nextState.location.pathname === '/login' && authenticated:
      path = '/my-account'
      break
    case nextState.location.pathname.indexOf('/my-account') !== -1 &&
      !authenticated:
      path = '/login'
      break
    default:
      return
  }

  replace(
    {
      nextPathname: nextState.location.pathname,
    },
    path,
    nextState.location.query
  )
}

export const requiresAuthOrAnonymous = ({ getState }, nextState, replace) => {
  if (isAuthOrAnonymous(getState())) return
  replace('/checkout/login')
}

export const requiresNotAuthOrAnon = ({ getState }, nextState, replace) => {
  if (isAuthOrAnonymous(getState())) {
    checkoutRedirect({ getState }, nextState, replace)
  }
}
