export const isAuth = (state) => !!state.auth.authentication

export const isAnonymous = (state) =>
  state.routing.location.query.isAnonymous === 'true'

export const isAuthOrAnonymous = (state) => isAuth(state) || isAnonymous(state)
