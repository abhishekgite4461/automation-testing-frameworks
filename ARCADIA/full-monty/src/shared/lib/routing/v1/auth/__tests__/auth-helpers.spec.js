import { clone } from 'ramda'
import { isAuth, isAnonymous, isAuthOrAnonymous } from '../auth-helpers'

const state = {
  auth: {
    authentication: 'authenticated',
  },
  routing: {
    location: {
      query: {
        isAnonymous: 'true',
      },
    },
  },
}

const falseAuthState = (state) => {
  const falseAuthState = clone(state)
  falseAuthState.auth.authentication = null
  return falseAuthState
}

const falseAnonState = (state) => {
  const falseAnonState = clone(state)
  falseAnonState.routing.location.query.isAnonymous = null
  return falseAnonState
}

describe('Auth Routing Helpers', () => {
  describe('isAuth', () => {
    it('should return true if authentication is truthy', () => {
      expect(isAuth(state)).toBe(true)
    })

    it('should return false if authentication is falsy', () => {
      const falseState = falseAuthState(state)
      expect(isAuth(falseState)).toBe(false)
    })
  })

  describe('isAnonymous', () => {
    it('should return true if queryParams is "true"', () => {
      expect(isAnonymous(state)).toBe(true)
    })

    it('should otherwise return false', () => {
      const falseState = falseAnonState(state)
      expect(isAnonymous(falseState)).toBe(false)
    })
  })

  describe('isAuthOrAnonymous', () => {
    it('should return false if not authenticated or anonymous', () => {
      const neitherState = falseAuthState(falseAnonState(state))
      expect(isAuthOrAnonymous(neitherState)).toBe(false)
    })

    it('should otherwise return true', () => {
      expect(isAuthOrAnonymous(state)).toBe(true)
      const authState = falseAuthState(state)
      expect(isAuthOrAnonymous(authState)).toBe(true)
      const anonState = falseAnonState(state)
      expect(isAuthOrAnonymous(anonState)).toBe(true)
    })
  })
})
