import { clone } from 'ramda'
import {
  isCFS,
  hasDeliveryAddress,
  hasCreditCard,
  hasDetails,
  klarnaIsDefaultPaymentType,
  hasDeliveryAndCC,
} from '../checkout-helpers'

const deliveryLocations = [
  {
    selected: true,
    deliveryLocationType: 'STORE',
  },
]

const user = {
  deliveryDetails: {
    addressDetailsId: 1,
  },
  billingDetails: {
    addressDetailsId: 1,
  },
  hasBillingDetails: true,
  hasDeliveryDetails: true,
  creditCard: {
    cardNumberHash: true,
  },
}

describe('Checkout Routing Helpers', () => {
  describe('isCFS', () => {
    it('should return true if there is a matching deliveryLocation', () => {
      expect(isCFS(deliveryLocations)).toBe(true)
    })
  })

  describe('hasDeliveryAddress', () => {
    it('should return whether user has a delivery address', () => {
      expect(hasDeliveryAddress(user)).toBe(true)
    })
  })

  describe('hasCreditCard', () => {
    it('should return whether user has a credit card', () => {
      expect(hasCreditCard(user)).toBe(true)
    })
  })

  describe('hasDetails', () => {
    it('should return false if details is empty or if addressDetailsId !== -1', () => {
      expect(hasDetails()).toBe(false)
      expect(
        hasDetails(
          {
            empty: false,
          },
          true
        )
      ).toBe(true)
    })
  })

  describe('klarnaIsDefaultPaymentType', () => {
    it('should return true if users default payment method is Klarna', () => {
      const klarnaUser = clone(user)
      delete klarnaUser.creditCard
      expect(klarnaIsDefaultPaymentType(klarnaUser)).toBe(true)
      expect(klarnaIsDefaultPaymentType({})).toBe(false)
    })
  })

  describe('hasDeliveryAndCC', () => {
    it('should return true if user has both delivery and credit card details', () => {
      expect(hasDeliveryAndCC(user)).toBe(true)
    })
  })
})
