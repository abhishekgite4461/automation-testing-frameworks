import { any, both, either, propEq, all, isEmpty, equals } from 'ramda'

export const isCFS = any(
  both(
    propEq('selected', true),
    either(
      propEq('deliveryLocationType', 'STORE'),
      propEq('deliveryLocationType', 'PARCELSHOP')
    )
  )
)

export const hasDeliveryAddress = ({
  deliveryDetails = {},
  hasDeliveryDetails,
}) => {
  return deliveryDetails.addressDetailsId !== -1 || hasDeliveryDetails
}

export const hasCreditCard = ({
  creditCard: cc = {},
  hasCardNumberHash,
  hasPayPal,
}) => {
  return !!(
    cc.cardNumberHash ||
    ['PAYPAL', 'KLRNA'].includes(cc.type) ||
    hasCardNumberHash ||
    hasPayPal
  )
}

export const hasDetails = (details = {}, detailExists = false) => {
  return isEmpty(details) ? detailExists : details.addressDetailsId !== -1
}

export const klarnaIsDefaultPaymentType = (user) => {
  const {
    hasBillingDetails,
    hasDeliveryDetails,
    billingDetails,
    deliveryDetails,
  } = user

  return all(equals(true), [
    !hasCreditCard(user),
    hasDetails(billingDetails, hasBillingDetails),
    hasDetails(deliveryDetails, hasDeliveryDetails),
  ])
}

export const hasDeliveryAndCC = (user) =>
  hasDeliveryAddress(user) && hasCreditCard(user)
