import { path } from 'ramda'
import { requiresAuthOrAnonymous } from '../auth/auth-handlers'
import { isAuth, isAnonymous, isAuthOrAnonymous } from '../auth/auth-helpers'
import {
  klarnaIsDefaultPaymentType,
  hasDeliveryAddress,
  hasCreditCard,
  isCFS,
  hasDeliveryAndCC,
} from './checkout-helpers'

export const checkoutRedirect = ({ getState }, nextState, replace) => {
  const state = getState()
  const user = state.account.user
  const deliveryLocations = path(
    ['checkout', 'orderSummary', 'deliveryLocations'],
    state
  )

  switch (true) {
    case !isAuth(state) && !isAnonymous(state):
      replace('/checkout/login')
      break
    case deliveryLocations && isCFS(deliveryLocations):
      replace('/checkout/delivery')
      break
    case klarnaIsDefaultPaymentType(user):
      replace('/checkout/payment')
      break
    case hasDeliveryAddress(user) && hasCreditCard(user):
      replace('/checkout/summary')
      break
    default:
      replace('/checkout/delivery')
  }
}

export const paymentRedirect = ({ getState }, nextState, replace) => {
  return process.browser
    ? requiresAuthOrAnonymous({ getState }, nextState, replace)
    : replace('/checkout/delivery')
}

export const summaryRedirect = ({ getState }, nextState, replace) => {
  const state = getState()
  const user = state.account.user

  switch (true) {
    case !isAuthOrAnonymous(state):
      replace('/checkout/login')
      break
    case !process.browser && !hasDeliveryAndCC(user):
      replace('/checkout/delivery')
      break
    default:
  }
}
