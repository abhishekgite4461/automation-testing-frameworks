import {
  checkoutRedirect,
  paymentRedirect,
  summaryRedirect,
} from '../checkout-handlers'

const getState = () => ({
  account: {
    user: {
      billingDetails: {},
    },
  },
  checkout: {},
  auth: {
    authentication: false,
  },
  routing: {
    location: {
      query: {
        isAnonymous: 'false',
      },
    },
  },
})

const checkoutStoreDeliveryLocationType = () => ({
  checkout: {
    orderSummary: {
      deliveryLocations: [
        {
          deliveryLocationType: 'HOME',
        },
        {
          deliveryLocationType: 'STORE',
          selected: true,
        },
      ],
    },
  },
})

const checkoutParcelDeliveryLocationType = () => ({
  checkout: {
    orderSummary: {
      deliveryLocations: [
        {
          deliveryLocationType: 'HOME',
        },
        {
          deliveryLocationType: 'PARCELSHOP',
          selected: true,
        },
      ],
    },
  },
})

const replace = jest.fn()

describe('Checkout Routing Handlers', () => {
  beforeEach(() => jest.resetAllMocks())

  describe('checkoutRedirect', () => {
    it('should redirect to /checkout/login if not logged in or anonymous', () => {
      checkoutRedirect({ getState }, null, replace)
      expect(replace).toHaveBeenCalledTimes(1)
      expect(replace).lastCalledWith('/checkout/login')
    })

    it('should redirect to /checkout/delivery if logged in and delivery location type is "STORE"', () => {
      const state = {
        ...getState(),
        ...checkoutStoreDeliveryLocationType(),
        auth: {
          authentication: true,
        },
      }

      checkoutRedirect({ getState: () => state }, null, replace)
      expect(replace).toHaveBeenCalledTimes(1)
      expect(replace).lastCalledWith('/checkout/delivery')
    })

    it('should redirect to /checkout/delivery if logged in and the delivery location type is "PARCELSHOP"', () => {
      const state = {
        ...getState(),
        ...checkoutParcelDeliveryLocationType(),
        auth: {
          authentication: true,
        },
      }

      checkoutRedirect({ getState: () => state }, null, replace)
      expect(replace).toHaveBeenCalledTimes(1)
      expect(replace).lastCalledWith('/checkout/delivery')
    })

    it('should redirect to /checkout/delivery if anonymous and using CFS', () => {
      const state = {
        ...getState(),
        ...checkoutStoreDeliveryLocationType(),
        routing: {
          location: {
            query: {
              isAnonymous: 'true',
            },
          },
        },
      }

      checkoutRedirect({ getState: () => state }, null, replace)
      expect(replace).toHaveBeenCalledTimes(1)
      expect(replace).lastCalledWith('/checkout/delivery')
    })

    it('should redirect to /checkout/delivery if logged in with no address', () => {
      const state = getState()
      state.auth.authentication = true
      checkoutRedirect({ getState: () => state }, null, replace)
      expect(replace).toHaveBeenCalledTimes(1)
      expect(replace).lastCalledWith('/checkout/delivery')
    })

    it('should redirect to /checkout/delivery if v1 returning logged in user', () => {
      const state = getState()
      state.auth.authentication = true
      state.account.user.billingDetails.addressDetailsId = 3
      checkoutRedirect({ getState: () => state }, null, replace)
      expect(replace).toHaveBeenCalledTimes(1)
      expect(replace).lastCalledWith('/checkout/delivery')
    })

    it('should redirect to /checkout/login if v1 returning user', () => {
      const state = getState()
      state.account.user.billingDetails.addressDetailsId = 3
      checkoutRedirect({ getState: () => state }, null, replace)
      expect(replace).toHaveBeenCalledTimes(1)
      expect(replace).lastCalledWith('/checkout/login')
    })
  })

  describe('paymentRedirect', () => {
    global.process.browser = true
    it('should redirect to /checkout/login when logged out', () => {
      paymentRedirect({ getState }, null, replace)
      expect(replace).toHaveBeenCalledTimes(1)
      expect(replace).lastCalledWith('/checkout/login')
    })

    it('should do nothing when logged in', () => {
      const state = getState()
      state.auth.authentication = true
      paymentRedirect({ getState: () => state }, null, replace)
      expect(replace).toHaveBeenCalledTimes(0)
    })
  })

  describe('summaryRedirect', () => {
    it('should redirect to /checkout/login when logged out', () => {
      summaryRedirect({ getState }, null, replace)
      expect(replace).toHaveBeenCalledTimes(1)
      expect(replace).lastCalledWith('/checkout/login')
    })

    it('should redirect to /checkout/delivery when logged in without delivery and CC', () => {
      const state = getState()
      state.auth.authentication = true
      global.process.browser = false
      summaryRedirect({ getState: () => state }, null, replace)
      expect(replace).toHaveBeenCalledTimes(1)
      expect(replace).lastCalledWith('/checkout/delivery')
    })
  })
})
