import bindHandlers from '../bind-handlers'
import * as authHandlers from './auth/auth-handlers'
import * as searchHandlers from './search/search-handlers'

const routeHandlers = {
  ...authHandlers,
  ...searchHandlers,
}

export default bindHandlers(routeHandlers)
