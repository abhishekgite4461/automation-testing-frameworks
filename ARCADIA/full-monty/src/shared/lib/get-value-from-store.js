import R from 'ramda'

let storeRef

export function makeReferenceToStore(store) {
  storeRef = store
}

export function getValueFromStore(path) {
  return R.path(path)(storeRef.getState())
}

export function isLoggedIn() {
  return R.path(['auth', 'authentication'])(storeRef.getState())
}

export function dispatch(action) {
  return storeRef.dispatch(action)
}
