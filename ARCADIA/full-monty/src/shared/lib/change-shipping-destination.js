import qs from 'querystring'
import { changeURL, getProtocol } from './window'
import { euCountries } from '../constants/languages'
import { all, is, contains, path } from 'ramda'
import { getStoreId } from '../selectors/configSelectors'
import { getShoppingBagOrderId } from '../selectors/checkoutSelectors'
import { isFeatureTransferBasketEnabled } from '../selectors/featureSelectors'

export const getTransferBasketParameters = function getTransferBasketParameters(
  state
) {
  if (!isFeatureTransferBasketEnabled(state)) {
    return {} /* eslint-disable  no-else-return */
  } else {
    const transferStoreID = getStoreId(state)
    const transferOrderID = getShoppingBagOrderId(state)
    const isValidOrderId = (orderId) => orderId > 0
    return all(is(Number))([transferStoreID, transferOrderID]) &&
      isValidOrderId(transferOrderID)
      ? {
          transferStoreID,
          transferOrderID,
        }
      : {}
  }
}

export const isEUCountry = (euCountries, shippingDestination) =>
  contains(shippingDestination, euCountries)

export const findHostnameByDefaultLanguage = (
  langHostnames,
  selectedLanguage
) => {
  const country = Object.keys(langHostnames).find((country) => {
    return langHostnames[country].defaultLanguage === selectedLanguage
  })
  return country ? langHostnames[country] : langHostnames.default
}

export const findShippingDestination = (
  langHostnames,
  shippingDestination,
  selectedLanguage
) => {
  return isEUCountry(euCountries, shippingDestination)
    ? selectedLanguage === 'English'
      ? langHostnames.default
      : findHostnameByDefaultLanguage(langHostnames, selectedLanguage)
    : langHostnames.nonEU
}

export const findRedirectUrl = (
  langHostnames,
  shippingDestination,
  selectedLanguage
) => {
  const destination = path([shippingDestination], langHostnames)

  return destination && destination.defaultLanguage === selectedLanguage
    ? destination
    : findShippingDestination(
        langHostnames,
        shippingDestination,
        selectedLanguage
      )
}

export const getShippingDestinationRedirectURL = (
  shippingDestination,
  langHostnames,
  selectedLanguage,
  hostname = ''
) => {
  const env = hostname.split('.')[0]
  const redirectURL = findRedirectUrl(
    langHostnames,
    shippingDestination,
    selectedLanguage
  )

  switch (env) {
    case 'integration':
    case 'preprod':
    case 'showcase':
    case 'stage':
      return `${env}.${redirectURL.hostname}`

    default:
      switch (process.env.NODE_ENV) {
        case 'development': {
          // Get port number when working locally
          const port = window.location.port || '8080'
          return `${env}.${redirectURL.hostname}:${port}`
        }
        default:
          // production
          return redirectURL.hostname
      }
  }
}

export function shippingDestinationRedirect(
  shippingDestination,
  langHostnames,
  selectedLanguage,
  optionalParameters = {},
  hostname = ''
) {
  const destinationURL = getShippingDestinationRedirectURL(
    shippingDestination,
    langHostnames,
    selectedLanguage,
    hostname
  )
  const params = {
    currentCountry: shippingDestination,
    ...optionalParameters,
  }

  changeURL(`${getProtocol()}//${destinationURL}?${qs.stringify(params)}`)
}
