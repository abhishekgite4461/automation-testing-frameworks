import { getLanguageCodeByName } from '../language'

describe('Language Utilities', () => {
  describe(getLanguageCodeByName.name, () => {
    it('should return the iso code of the language that bears the specified name', () => {
      expect(getLanguageCodeByName('English')).toBe('en')
    })

    it('should return undefined if no language was found', () => {
      expect(getLanguageCodeByName('FuzzleBot')).toBeUndefined()
    })
  })
})
