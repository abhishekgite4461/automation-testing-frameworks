import { isMobileHostname } from '../hostname'

describe('#isMobileHostname', () => {
  it('returns false if the argument is not a string or empty string', () => {
    expect(isMobileHostname()).toEqual(false)
    expect(isMobileHostname('')).toEqual(false)
  })
  it('returns true for hostnames starting with m. or local.m.', () => {
    expect(isMobileHostname('m.whatever.com')).toEqual(true)
    expect(isMobileHostname('local.m.whatever.com')).toEqual(true)
  })
  it('returns false for hostnames starting with www. or local.www.', () => {
    expect(isMobileHostname('www.whatever.com')).toEqual(false)
    expect(isMobileHostname('local.www.whatever.com')).toEqual(false)
  })
})
