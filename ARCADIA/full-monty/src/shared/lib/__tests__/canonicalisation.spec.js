import { getCanonicalHostname } from '../canonicalisation'

describe('canonicalisation', () => {
  describe('#getCanonicalHostname', () => {
    it('returns empty string for empty arguments', () => {
      expect(getCanonicalHostname('')).toEqual('')
    })

    it('returns the parameter value for unrecognized URLs', () => {
      expect(getCanonicalHostname('www.google.it')).toEqual('www.google.it')
    })

    it('returns "http://www.topshop.com" for argument "m.topshop.com"', () => {
      expect(getCanonicalHostname('m.topshop.com')).toEqual(
        'http://www.topshop.com'
      )
    })

    it('returns "http://fr.topshop.com" for argument "m.fr.topshop.com"', () => {
      expect(getCanonicalHostname('m.fr.topshop.com')).toEqual(
        'http://fr.topshop.com'
      )
    })

    it('returns "http://www.dorothyperkins.com" for argument "m.dorothyperkins.com"', () => {
      expect(getCanonicalHostname('m.dorothyperkins.com')).toEqual(
        'http://www.dorothyperkins.com'
      )
    })
  })
})
