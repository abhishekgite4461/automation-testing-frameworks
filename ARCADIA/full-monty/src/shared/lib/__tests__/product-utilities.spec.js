import { checkIfOneSizedItem, getMatchingAttribute } from '../product-utilities'

describe('product-utilities', () => {
  describe('getMatchingAttribute', () => {
    it('should return undefined value when object is not valid', () => {
      expect(getMatchingAttribute('freddieMercury', undefined)).toEqual(
        undefined
      )

      expect(getMatchingAttribute('freddieMercury', null)).toEqual(undefined)

      expect(getMatchingAttribute('freddieMercury', 0)).toEqual(undefined)

      expect(
        getMatchingAttribute('freddieMercury', 'trying to be an object')
      ).toEqual(undefined)
    })
    it('should be undefined if object is not passed', () => {
      expect(getMatchingAttribute(123)).toBeUndefined()
    })
    it('should return matching value for given key', () => {
      expect(getMatchingAttribute('brianMay', { brianMay: 321 })).toEqual(321)
    })
    it('should return undefined value for given key that does not exist', () => {
      expect(getMatchingAttribute('freddieMercury', { brianMay: 321 })).toEqual(
        undefined
      )
    })
  })

  describe('checkIfOneSizedItem', () => {
    it('should be false if items.length === 0', () => {
      expect(checkIfOneSizedItem([])).toBe(false)
    })
    it('should be false if items.length > 0', () => {
      expect(checkIfOneSizedItem([{ size: 1 }, { size: 2 }])).toBe(false)
    })
    it('should be false if items.length === 1 and size is not "ONE" or "000"', () => {
      expect(checkIfOneSizedItem([{ size: 1 }])).toBe(false)
    })
    it('should be false if items.length === 1 and size is "000"', () => {
      expect(checkIfOneSizedItem([{ size: '000' }])).toBe(true)
    })
    it('should be false if items.length === 1 and size is "ONE"', () => {
      expect(checkIfOneSizedItem([{ size: 'ONE' }])).toBe(true)
    })
  })
})
