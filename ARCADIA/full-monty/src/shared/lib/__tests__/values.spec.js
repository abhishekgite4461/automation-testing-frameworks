import * as ValueUtils from '../values'

describe('values', () => {
  describe('isNilOrEmpty', () => {
    it('returns true if given value is undefined', () => {
      expect(ValueUtils.isNilOrEmpty(undefined)).toBe(true)
    })

    it('returns true if given value is null', () => {
      expect(ValueUtils.isNilOrEmpty(null)).toBe(true)
    })

    it('returns true if given value is an empty object', () => {
      expect(ValueUtils.isNilOrEmpty({})).toBe(true)
    })

    it('returns true if given value is an empty array', () => {
      expect(ValueUtils.isNilOrEmpty([])).toBe(true)
    })

    it('returns true if given value is an empty string', () => {
      expect(ValueUtils.isNilOrEmpty('')).toBe(true)
    })

    it('returns false for valid values', () => {
      expect(ValueUtils.isNilOrEmpty({ foo: 'bar' })).toBe(false)
      expect(ValueUtils.isNilOrEmpty(['foo'])).toBe(false)
      expect(ValueUtils.isNilOrEmpty('foo')).toBe(false)
      expect(ValueUtils.isNilOrEmpty(() => undefined)).toBe(false)
      expect(ValueUtils.isNilOrEmpty(1)).toBe(false)
    })
  })
})
