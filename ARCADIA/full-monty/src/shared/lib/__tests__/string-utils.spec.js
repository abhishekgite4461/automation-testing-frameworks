global.process.browser = true

import {
  capitalize,
  titleCase,
  camelCaseify,
  trimFromFileExtension,
} from '../string-utils'

describe('String Utils', () => {
  describe('capitalize', () => {
    it('capitalises the first letter of a word', () => {
      expect(capitalize('test')).toBe('Test')
    })

    it('returns an empty string if passed a non string', () => {
      expect(capitalize(0)).toBe('')
    })

    it('returns an empty string if passed an empty string', () => {
      expect(capitalize('')).toBe('')
    })
  })

  describe('titleCase', () => {
    it('returns a sentence in title case', () => {
      expect(titleCase('this is a test')).toBe('This Is A Test')
    })
  })

  describe('camelCaseify', () => {
    it('should convert string with spaces to camel case', () => {
      expect(camelCaseify('hello world')).toBe('helloWorld')
    })

    it('should convert string with multiple spaces to camel case', () => {
      expect(camelCaseify('hello world and goodbye')).toBe(
        'helloWorldAndGoodbye'
      )
    })

    it('should remove non-alphanumeric characters', () => {
      expect(camelCaseify('hello world & goodbye1!')).toBe('helloWorldGoodbye1')
    })

    it('should remove underscore characters', () => {
      expect(camelCaseify('hello world__goodbye')).toBe('helloWorldGoodbye')
    })

    it('should handle one letter words', () => {
      expect(camelCaseify('hello world s and b')).toBe('helloWorldSAndB')
    })

    it('should handle nothing supplied', () => {
      expect(camelCaseify()).toBeUndefined()
    })
  })

  describe('trimFromFileExtension', () => {
    const extension = 'jpg'

    it('should return empty string when empty string passed in', () => {
      const string = ''
      expect(trimFromFileExtension(string, extension)).toEqual(string)
    })

    it('should return same string when no extension specified', () => {
      const string = 'http://www.google.com/assets/foo.jpg'
      expect(trimFromFileExtension(string)).toEqual(string)
    })

    it('should return the same string when no extension found', () => {
      const string = 'http://www.google.com/assets/jpg/other-page-url'
      expect(trimFromFileExtension(string, extension)).toEqual(string)
    })

    it('should remove file extension from the end', () => {
      const string = 'http://www.google.com/assets/foo.jpg'
      expect(trimFromFileExtension(string, extension)).toBe(
        'http://www.google.com/assets/foo'
      )
    })

    it('should remove everything starting from file extension', () => {
      const string = 'http://www.google.com/assets/foo.jpg?$2col$'
      expect(trimFromFileExtension(string, extension)).toEqual(
        'http://www.google.com/assets/foo'
      )
    })
  })
})
