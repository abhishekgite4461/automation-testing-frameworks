import { pick, evolve, map, clone } from 'ramda'

import { createOrder, fixTotal, giftCardCoversTotal } from '../checkout'
import * as mocks from '../../../../test/unit/lib/checkout-mocks'

const INPUT_FIELDS = pick(
  [
    'billingCardDetails',
    'orderSummary',
    'yourDetails',
    'yourAddress',
    'deliveryInstructions',
    'billingDetails',
    'billingAddress',
    'credentials',
    'auth',
    'user',
  ],
  mocks
)

const cleanFields = (fields) =>
  map(
    evolve({ fields: map((field) => ({ ...field, isDirty: false })) }),
    fields
  )

describe('Checkout', () => {
  describe(createOrder.name, () => {
    it('should create a shortOrder if the user already has checkout details', () => {
      const input = cleanFields(INPUT_FIELDS)
      const order = createOrder({
        ...input,
        paymentMethods: mocks.paymentMethods,
        orderCompletePath: 'order-complete-v1',
      })
      expect(order).toEqual(mocks.shortOrder)
    })

    it('should create a guestOrder if the user is not authenticated', () => {
      const order = createOrder({
        ...INPUT_FIELDS,
        auth: {
          authenticated: false,
        },
        user: {
          exists: true,
          email: '.TEMP.-something',
        },
        paymentMethods: mocks.paymentMethods,
        orderCompletePath: 'order-complete-v1',
      })
      expect(order).toEqual(mocks.guestOrder)
    })

    it('should add sections for details that has been modified', () => {
      const order = createOrder({
        ...INPUT_FIELDS,
        paymentMethods: mocks.paymentMethods,
        orderCompletePath: 'order-complete-v1',
      })
      expect(order).toEqual(mocks.fullOrder)
    })

    it('should set default shippingCountry if country has not been selected as delivery', () => {
      const input = clone(INPUT_FIELDS)
      input.yourAddress.fields.country.value = ''
      input.orderSummary.shippingCountry = 'United Kingdom'
      const order = createOrder({
        ...input,
        paymentMethods: mocks.paymentMethods,
      })
      expect(order.orderDeliveryOption.shippingCountry).toEqual(
        'United Kingdom'
      )
    })

    it("should set CVV as '0' if giftCard covers the basket total", () => {
      const input = clone(INPUT_FIELDS)
      input.orderSummary.basket.total = '0.00'
      input.orderSummary.giftCards = [
        {
          giftCardId: '6646689',
          giftCardNumber: 'XXXX XXXX XXXX 0830',
          balance: '26.95',
          amountUsed: '26.95',
          remainingBalance: '257.15',
        },
      ]
      const order = createOrder({
        ...input,
        paymentMethods: mocks.paymentMethods,
      })
      expect(order.cardCvv).toEqual('0')
    })

    it("should set CVV as '0' if there is no CVV in the billingDetails", () => {
      const input = clone(INPUT_FIELDS)
      input.billingCardDetails.fields.cvv = ''
      const order = createOrder({
        ...input,
        paymentMethods: mocks.paymentMethods,
      })
      expect(order.cardCvv).toEqual('0')
    })

    describe('Payment Section', () => {
      let dateMock

      beforeEach(() => {
        dateMock = jest.spyOn(global, 'Date').mockImplementation(() => {
          return {
            getFullYear: () => 2020,
          }
        })
      })

      afterEach(() => {
        dateMock.mockRestore()
      })

      it('should return dummy card info if not a card payment', () => {
        const input = clone(INPUT_FIELDS)
        input.billingCardDetails.fields.paymentType.value = 'PYPAL'
        const order = createOrder({
          ...input,
          paymentMethods: mocks.paymentMethods,
        })
        expect(order.creditCard).toEqual({
          expiryYear: '2021',
          expiryMonth: '1',
          cardNumber: '0',
          type: 'PYPAL',
        })
      })

      it('should return cardNumberHash if fields aren’t dirty', () => {
        const input = cleanFields(INPUT_FIELDS)
        const order = createOrder({
          ...input,
          paymentMethods: mocks.paymentMethods,
        })
        expect(order.cardNumberHash).toBe('card number hash')
      })

      it('should use cardNumberHash from user’s account if none in order summary', () => {
        const input = cleanFields(INPUT_FIELDS)
        delete input.orderSummary.cardNumberHash
        const order = createOrder({
          ...input,
          paymentMethods: mocks.paymentMethods,
          user: {
            creditCard: {
              cardNumberHash: '123456qwerty',
            },
          },
        })
        expect(order.cardNumberHash).toBe('123456qwerty')
      })

      it('should add the selected savePaymentDetails property when the feature SAVE_PAYMENT_DETAILS is enabled', () => {
        const input = cleanFields(INPUT_FIELDS)
        const order = createOrder({
          ...input,
          savePaymentDetails: true,
          featureSavePaymentDetailsEnabled: true,
          paymentMethods: mocks.paymentMethods,
        })
        expect(order.save_details).toEqual(true)
      })

      it('should not add the savePaymentDetails property when the feature SAVE_PAYMENT_DETAILS is disabled', () => {
        const input = cleanFields(INPUT_FIELDS)
        const order = createOrder({
          ...input,
          savePaymentDetails: false,
          featureSavePaymentDetailsEnabled: false,
          paymentMethods: mocks.paymentMethods,
        })
        expect(order.save_details).toBeUndefined()
      })

      it('should contain returnUrl with order-complete-v2 when checkoutV2 is enabled', () => {
        const input = clone(INPUT_FIELDS)
        const order = createOrder({
          ...input,
          paymentMethods: mocks.paymentMethods,
          orderCompletePath: 'order-complete-v2',
        })
        expect(order.returnUrl).toEqual(
          expect.stringContaining('/order-complete-v2')
        )
      })
    })

    it('should provide placeholder postcode when one is not provided (i.e is not required)', () => {
      const mockAddress = {
        fields: {
          address1: { isDirty: true, value: 'delivery address1' },
          address2: { isDirty: true, value: 'delivery address2' },
          city: { isDirty: true, value: 'delivery city' },
          county: { isDirty: true, value: 'delivery county' },
          postcode: { isDirty: true, value: '' },
          country: { isDirty: true, value: 'delivery country' },
        },
      }
      const order = createOrder({
        ...INPUT_FIELDS,
        yourAddress: mockAddress,
        billingAddress: mockAddress,
        paymentMethods: mocks.paymentMethods,
      })

      expect(order.deliveryAddress.postcode).toEqual('0')
      expect(order.billingDetails.address.postcode).toEqual('0')
    })
  })

  describe(fixTotal.name, () => {
    const shippingCost = 4
    const subTotal = 10
    const discounts = [1, 3]
    it('returns the total (incl.delivery) if the shippingCost is positive', () => {
      expect(fixTotal(subTotal, shippingCost, [])).toEqual(14)
    })
    it('returns the subTotal (before delivery) if the shipping cost is 0', () => {
      expect(fixTotal(subTotal, 0, [])).toEqual(subTotal)
    })
    it('returns the subTotal (before delivery) - discounts if the shipping cost is 0 and a discount has been applied', () => {
      const discount = 3
      expect(fixTotal(subTotal, 0, [discount])).toEqual(subTotal - discount)
    })
    it('returns the subTotal (before delivery) - discounts if the shipping cost is 0 and discounts have been applied', () => {
      expect(fixTotal(subTotal, 0, discounts)).toEqual(subTotal - 4)
    })
    it('handles euro commas', () => {
      expect(fixTotal('7,50', '5,00', ['0,75'])).toEqual(11.75)
    })
  })

  describe(giftCardCoversTotal.name, () => {
    it('should return false if params are not valid', () => {
      expect(giftCardCoversTotal()).toBe(false)
      expect(giftCardCoversTotal(null, null)).toBe(false)
      expect(giftCardCoversTotal({}, '')).toBe(false)
    })

    it('should return false if params giftCards is empty', () => {
      expect(giftCardCoversTotal([], '10.00')).toBe(false)
    })

    it('should return true if there is at least one giftCard and total is 0', () => {
      const giftCard = {
        giftCardId: '6646689',
        giftCardNumber: 'XXXX XXXX XXXX 0830',
        balance: '26.95',
        amountUsed: '26.95',
        remainingBalance: '257.15',
      }

      expect(giftCardCoversTotal([giftCard], '0.00')).toBe(true)
    })
  })
})
