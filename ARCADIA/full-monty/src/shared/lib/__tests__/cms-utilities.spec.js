import React from 'react'
import cmsUtilities, { fixCmsUrl } from '../cms-utilities'
import { dispatch } from '../get-value-from-store'
import {
  setProductIdQuickview,
  showSizeGuide,
  setSizeGuide,
} from '../../actions/common/productsActions'
import { showModal } from '../../actions/common/modalActions'
import { addDDPToBag } from '../../actions/common/ddpActions'
import ProductQuickview from '../../components/containers/ProductQuickview/ProductQuickview'
import { deepEqual } from 'assert'
import { browserHistory } from 'react-router'

jest.mock('../get-value-from-store')
jest.mock('../../actions/common/productsActions')
jest.mock('../../actions/common/modalActions')
jest.mock('../../actions/common/ddpActions', () => ({
  addDDPToBag: jest.fn(),
}))
jest.mock('react-router', () => ({
  browserHistory: {
    push: jest.fn(),
  },
}))

const unmounterMock = jest.fn()

const notRealFunction = jest.fn()

const sandboxMockFull = {
  unmounter: unmounterMock,
  toBeMapped: [],
}

const sandboxMockEmpty = {
  notRealFunction,
}

const id = '12345'
const props = { prop1: 'hello', prop2: 56 }

describe('cms-utilities', () => {
  beforeEach(() => {
    // using global but in actual code is on window as it is client side code
    global.sandbox = undefined
    sandboxMockFull.toBeMapped = []
    jest.resetAllMocks()
  })

  describe('fixCmsUrl', () => {
    it('should return empty string if no url provided', () => {
      expect(fixCmsUrl()).toBe('')
    })

    it('should add / if not present at the start of url', () => {
      expect(fixCmsUrl('url')).toBe('/url')
    })

    it('should not add / if present at the start of url already', () => {
      expect(fixCmsUrl('/url')).toBe('/url')
    })
  })

  describe('unmountPreviousSandboxDOMNode', () => {
    // needed as the unmounter can be called with no parameters
    it('calls the unmounter on the sandbox with no parameters', () => {
      global.sandbox = sandboxMockFull
      expect(unmounterMock).not.toBeCalled()
      cmsUtilities.unmountPreviousSandboxDOMNode()
      expect(unmounterMock).toHaveBeenCalledTimes(1)
    })
    it('calls the unmounter on the sandbox with the correct parameters', () => {
      global.sandbox = sandboxMockFull
      expect(unmounterMock).not.toBeCalled()
      cmsUtilities.unmountPreviousSandboxDOMNode(id)
      expect(unmounterMock).toHaveBeenCalledTimes(1)
      expect(unmounterMock).lastCalledWith(id)
    })
    it('does not call the unmounter on the sandbox, if the sandbox is not present', () => {
      expect(unmounterMock).not.toBeCalled()
      cmsUtilities.unmountPreviousSandboxDOMNode(id)
      expect(unmounterMock).not.toBeCalled()
    })
    it('does not call the unmounter on the sandbox, if the unmounter is not present', () => {
      global.sandbox = sandboxMockEmpty
      expect(unmounterMock).not.toBeCalled()
      expect(notRealFunction).not.toBeCalled()
      cmsUtilities.unmountPreviousSandboxDOMNode(id)
      expect(unmounterMock).not.toBeCalled()
      expect(notRealFunction).not.toBeCalled()
    })
  })
  describe('updateNewSandBox', () => {
    it('adds an item to the toBeMapped array on the sandbox when the sandbox is undefined', () => {
      expect(global.sandbox).toBeUndefined()
      cmsUtilities.updateNewSandBox(id, props)
      expect(global.sandbox).toBeDefined()
      expect(global.sandbox.toBeMapped.length).toBe(1)
      expect(global.sandbox.toBeMapped[0]).toEqual({ id, props })
    })
    it('adds an item to the toBeMapped array on the sandbox when the sandbox is defined', () => {
      global.sandbox = sandboxMockFull
      cmsUtilities.updateNewSandBox(id, props)
      expect(global.sandbox.toBeMapped.length).toBe(1)
      expect(global.sandbox.toBeMapped[0]).toEqual({ id, props })
    })
  })

  describe('globally available sandbox functions', () => {
    beforeEach(() => {
      cmsUtilities.updateNewSandBox(id, props)
    })

    const setupMock = (mockFn) => ({
      whenCalledWith: (...expectedArgs) => ({
        returns: (returnValue) => {
          mockFn.mockImplementation((...args) => {
            try {
              deepEqual(expectedArgs, args)
              return returnValue
            } catch (e) {} // eslint-disable-line no-empty
          })
        },
      }),
    })

    describe('sandbox attribute list', () => {
      it('should match a given pattern', () => {
        expect(global.sandbox).toEqual({
          addDDPToBasket: expect.any(Function),
          applyBundleMapping: expect.any(Function),
          historyPush: expect.any(Function),
          jsBundles: {},
          mapped: [],

          openQuickViewProduct: expect.any(Function),
          openSizeGuideDrawer: expect.any(Function),
          toBeMapped: expect.any(Array),
        })
      })
    })
    describe('openQuickViewProduct', () => {
      it('should call document dispatch with correct product id, and type', () => {
        const productId = 321
        setupMock(setProductIdQuickview)
          .whenCalledWith(productId)
          .returns('return1')
        setupMock(showModal)
          .whenCalledWith(<ProductQuickview />, { mode: 'sandboxQuickview' })
          .returns('return2')

        global.sandbox.openQuickViewProduct(productId)

        expect(dispatch).toHaveBeenCalledTimes(2)
        expect(dispatch).toHaveBeenCalledWith('return1')
        expect(dispatch).toHaveBeenCalledWith('return2')
      })
    })

    describe('openSizeGuideDrawer', () => {
      it('should call document historyPush with correct type (mobile)', () => {
        global.sandbox.openSizeGuideDrawer('Trousers')

        const thunk = dispatch.mock.calls[0][0]
        const mockStore = () => ({ viewport: { media: 'mobile' } })

        expect(browserHistory.push).not.toHaveBeenCalled()

        thunk(dispatch, mockStore)

        expect(browserHistory.push).toHaveBeenCalledTimes(1)
        expect(browserHistory.push).toHaveBeenCalledWith('/size-guide/Trousers')

        expect(dispatch).toHaveBeenCalledTimes(1)
      })
      it('should call document dispatches with correct type (desktop)', () => {
        setupMock(setSizeGuide)
          .whenCalledWith('Trousers')
          .returns('return1')
        setupMock(showSizeGuide)
          .whenCalledWith()
          .returns('return2')

        expect(dispatch).not.toHaveBeenCalled()

        global.sandbox.openSizeGuideDrawer('Trousers')

        const thunk = dispatch.mock.calls[0][0]
        const mockStore = () => ({ viewport: { media: 'desktop' } })

        expect(dispatch).toHaveBeenCalledTimes(1)

        thunk(dispatch, mockStore)

        expect(dispatch).toHaveBeenCalledTimes(3)
        expect(dispatch).toHaveBeenCalledWith('return1')
        expect(dispatch).toHaveBeenCalledWith('return2')
      })
    })

    describe('addDDPToBasket', () => {
      it('should dispatch an addToBag action with given sku', () => {
        const sku = '00000000321'
        const addDDPToBagMock = () => ({
          type: 'ADD_DDP_TO_BAG_MOCK',
        })

        addDDPToBag.mockImplementationOnce(addDDPToBagMock)

        expect(dispatch).not.toHaveBeenCalled()

        global.sandbox.addDDPToBasket(sku)

        expect(dispatch).toHaveBeenCalledTimes(1)
        expect(dispatch).toHaveBeenCalledWith(addDDPToBagMock())
        expect(addDDPToBag).toHaveBeenCalledWith(sku)
      })
    })
  })
})
