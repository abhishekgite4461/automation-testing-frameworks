import {
  findHostnameByDefaultLanguage,
  findRedirectUrl,
  findShippingDestination,
  getShippingDestinationRedirectURL,
  getTransferBasketParameters,
  isEUCountry,
  shippingDestinationRedirect,
} from '../change-shipping-destination'
import { changeURL } from '../window'
import { mockLangHostnames } from '../../../../test/mocks/change-shipping-destination'
import qs from 'querystring'
import { euCountries } from '../../constants/languages'

jest.mock('../window', () => ({
  changeURL: jest.fn(),
  getOrigin: () => 'https://www.topshop.com',
  getProtocol: () => 'https:',
}))

jest.mock('../../../client/lib/cookie', () => ({
  setItem: jest.fn(),
}))

const langHostnames = {
  France: { hostname: 'm.fr.topshop.com', defaultLanguage: 'French' },
  Germany: { hostname: 'm.de.topshop.com', defaultLanguage: 'German' },
  Indonesia: { hostname: 'm.topshop.com', defaultLanguage: 'English' },
  Malaysia: { hostname: 'm.my.topshop.com', defaultLanguage: 'English' },
  Singapore: { hostname: 'm.sg.topshop.com', defaultLanguage: 'English' },
  Thailand: { hostname: 'm.th.topshop.com', defaultLanguage: 'English' },
  'United Kingdom': { hostname: 'm.topshop.com', defaultLanguage: 'English' },
  'United States': {
    hostname: 'm.us.topshop.com',
    defaultLanguage: 'English',
  },
  default: { hostname: 'm.eu.topshop.com', defaultLanguage: 'English' },
  nonEU: { hostname: 'm.topshop.com', defaultLanguage: 'English' },
}

describe('change-shipping-destination', () => {
  beforeEach(() => jest.clearAllMocks())
  global.process.env.NODE_ENV = 'production'

  describe(shippingDestinationRedirect.name, () => {
    it('redirects to the expected', () => {
      shippingDestinationRedirect('Ireland', mockLangHostnames, 'English')
      expect(changeURL).toHaveBeenLastCalledWith(
        'https://m.eu.topshop.com?currentCountry=Ireland'
      )
      shippingDestinationRedirect('Italy', mockLangHostnames, 'English')
      expect(changeURL).toHaveBeenLastCalledWith(
        'https://m.eu.topshop.com?currentCountry=Italy'
      )
      shippingDestinationRedirect(
        'United Kingdom',
        mockLangHostnames,
        'English'
      )
      expect(changeURL).toHaveBeenLastCalledWith(
        'https://m.topshop.com?currentCountry=United%20Kingdom'
      )
      shippingDestinationRedirect('United States', mockLangHostnames, 'English')
      expect(changeURL).toHaveBeenLastCalledWith(
        'https://m.us.topshop.com?currentCountry=United%20States'
      )
      shippingDestinationRedirect('France', mockLangHostnames, 'French')
      expect(changeURL).toHaveBeenLastCalledWith(
        'https://m.fr.topshop.com?currentCountry=France'
      )
      shippingDestinationRedirect('France', mockLangHostnames, 'German')
      expect(changeURL).toHaveBeenLastCalledWith(
        'https://m.de.topshop.com?currentCountry=France'
      )
      shippingDestinationRedirect('Germany', mockLangHostnames, 'German')
      expect(changeURL).toHaveBeenLastCalledWith(
        'https://m.de.topshop.com?currentCountry=Germany'
      )
      shippingDestinationRedirect('Germany', mockLangHostnames, 'French')
      expect(changeURL).toHaveBeenLastCalledWith(
        'https://m.fr.topshop.com?currentCountry=Germany'
      )
      shippingDestinationRedirect('Jamaica', mockLangHostnames, 'English')
      expect(changeURL).toHaveBeenLastCalledWith(
        'https://m.topshop.com?currentCountry=Jamaica'
      )
      shippingDestinationRedirect('Italy', mockLangHostnames, 'English', {
        optionalParam: 'optionalValue',
      })
      expect(changeURL).toHaveBeenLastCalledWith(
        'https://m.eu.topshop.com?currentCountry=Italy&optionalParam=optionalValue'
      )
    })
  })

  describe('shipping redirect rules', () => {
    const siteKeys = Object.keys(mockLangHostnames)
    const numSites = siteKeys.length
    const genSiteCountry = gen
      .intWithin(0, numSites - 1)
      .then((i) => siteKeys[i])

    check.it(
      'if selected language = default language then redirect to destination site',
      genSiteCountry,
      (country) => {
        jest.clearAllMocks()
        const lang = mockLangHostnames[country].defaultLanguage
        shippingDestinationRedirect(country, mockLangHostnames, lang)
        expect(changeURL).toHaveBeenLastCalledWith(
          `https://${mockLangHostnames[country].hostname}?${qs.stringify({
            currentCountry: country,
          })}`
        )
      }
    )

    describe('if selected shipping destination is a EU country with selectedLanguage !== defaultLanguage', () => {
      it('redirects to EU default site when selectedLanguage is English', () => {
        shippingDestinationRedirect('France', mockLangHostnames, 'English')
        expect(changeURL).toHaveBeenLastCalledWith(
          'https://m.eu.topshop.com?currentCountry=France'
        )

        shippingDestinationRedirect(
          'Liechtenstein',
          mockLangHostnames,
          'English'
        )
        expect(changeURL).toHaveBeenLastCalledWith(
          'https://m.eu.topshop.com?currentCountry=Liechtenstein'
        )
      })

      it('redirects to custom Language site if exists', () => {
        shippingDestinationRedirect('France', mockLangHostnames, 'German')
        expect(changeURL).toHaveBeenLastCalledWith(
          'https://m.de.topshop.com?currentCountry=France'
        )

        shippingDestinationRedirect('Germany', mockLangHostnames, 'French')
        expect(changeURL).toHaveBeenLastCalledWith(
          'https://m.fr.topshop.com?currentCountry=Germany'
        )
      })
    })

    it('redirect to non-eu site none of the other rules is satisfied', () => {
      shippingDestinationRedirect('Vanuatu', mockLangHostnames, 'German')
      expect(changeURL).toHaveBeenLastCalledWith(
        'https://m.topshop.com?currentCountry=Vanuatu'
      )
    })
  })

  describe(getTransferBasketParameters.name, () => {
    const config = { siteId: 1234 }
    const shoppingBag = {
      bag: {
        orderId: 5678,
      },
    }
    const features = {
      status: {
        FEATURE_TRANSFER_BASKET: true,
      },
    }
    it(
      'returns a map containing the transfer basket parameters' +
        'when the TRANSFER_BASKET feature flag is enabled and the order id is valid',
      () => {
        const state = {
          config,
          shoppingBag,
          features,
        }
        expect(getTransferBasketParameters(state)).toEqual({
          transferStoreID: 1234,
          transferOrderID: 5678,
        })
      }
    )
    describe('returns empty object when', () => {
      it('the TRANSFER_BASKET feature flag is disabled', () => {
        const state = {
          config,
          features,
        }
        expect(getTransferBasketParameters(state)).toEqual({})
      })
      it('the order id is not valid', () => {
        const state = {
          config,
          features,
        }
        expect(getTransferBasketParameters(state)).toEqual({})
      })
    })
  })

  describe('@menthods', () => {
    describe('isEUCountry()', () => {
      it('should return true if it is an EU country', () => {
        const country = 'Spain'
        expect(isEUCountry(euCountries, country)).toBeTruthy()
      })

      it('should return false if it is not an EU country', () => {
        const country = 'Trinidad'
        expect(isEUCountry(euCountries, country)).toBeFalsy()
      })
    })

    describe('findHostnameByDefaultLanguage()', () => {
      it('should return French hostname and language', () => {
        const language = 'French'
        const expected = {
          defaultLanguage: 'French',
          hostname: 'm.fr.topshop.com',
        }
        expect(findHostnameByDefaultLanguage(langHostnames, language)).toEqual(
          expected
        )
      })

      it('should return German hostname and language', () => {
        const language = 'German'
        const expected = {
          defaultLanguage: 'German',
          hostname: 'm.de.topshop.com',
        }
        expect(findHostnameByDefaultLanguage(langHostnames, language)).toEqual(
          expected
        )
      })

      it('should return international hostname and English language', () => {
        const language = 'English'
        const expected = {
          defaultLanguage: 'English',
          hostname: 'm.topshop.com',
        }
        expect(findHostnameByDefaultLanguage(langHostnames, language)).toEqual(
          expected
        )
      })

      it('should return to a default option "m.eu.topshop" if language is not supported', () => {
        const language = 'Spanish'
        const expected = {
          defaultLanguage: 'English',
          hostname: 'm.eu.topshop.com',
        }
        expect(findHostnameByDefaultLanguage(langHostnames, language)).toEqual(
          expected
        )
      })
    })

    describe('findShippingDestination()', () => {
      it('should return m.fr.topshop hostname if language is French', () => {
        const shippingDestination = 'Belgium'
        const selectedLanguage = 'French'
        const expected = {
          hostname: 'm.fr.topshop.com',
          defaultLanguage: 'French',
        }
        expect(
          findShippingDestination(
            langHostnames,
            shippingDestination,
            selectedLanguage
          )
        ).toEqual(expected)
      })

      it('should return m.de.topshop hostname if language is German', () => {
        const shippingDestination = 'Belgium'
        const selectedLanguage = 'German'
        const expected = {
          hostname: 'm.de.topshop.com',
          defaultLanguage: 'German',
        }
        expect(
          findShippingDestination(
            langHostnames,
            shippingDestination,
            selectedLanguage
          )
        ).toEqual(expected)
      })

      it('should return m.eu.topshop hostname if destination is an EU country and language is English', () => {
        const shippingDestination = 'Belgium'
        const selectedLanguage = 'English'
        const expected = {
          hostname: 'm.eu.topshop.com',
          defaultLanguage: 'English',
        }
        expect(
          findShippingDestination(
            langHostnames,
            shippingDestination,
            selectedLanguage
          )
        ).toEqual(expected)
      })

      it('should return m.topshop hostname if destination is a non EU country', () => {
        const shippingDestination = 'Japan'
        const selectedLanguage = 'English'
        const expected = {
          hostname: 'm.topshop.com',
          defaultLanguage: 'English',
        }
        expect(
          findShippingDestination(
            langHostnames,
            shippingDestination,
            selectedLanguage
          )
        ).toEqual(expected)
      })
    })

    describe('findRedirectUrl()', () => {
      it('should return', () => {
        const shippingDestination = 'Belgium'
        const selectedLanguage = 'French'
        const expected = {
          hostname: 'm.fr.topshop.com',
          defaultLanguage: 'French',
        }
        expect(
          findRedirectUrl(langHostnames, shippingDestination, selectedLanguage)
        ).toEqual(expected)
      })
    })

    describe('getShippingDestinationRedirectURL()', () => {
      beforeEach(() => {
        global.process.env.NODE_ENV = ''
      })

      it('should return a redirect URL for production', () => {
        global.process.env.NODE_ENV = 'production'
        const shippingDestination = 'Belgium'
        const selectedLanguage = 'French'
        const location = 'm.fr.topshop.com'
        const expected = 'm.fr.topshop.com'
        expect(
          getShippingDestinationRedirectURL(
            shippingDestination,
            langHostnames,
            selectedLanguage,
            location
          )
        ).toEqual(expected)
      })

      it('should return a redirect URL for development', () => {
        global.process.env.NODE_ENV = 'development'
        const shippingDestination = 'Belgium'
        const selectedLanguage = 'French'
        const location = 'local.m.fr.topshop.com'
        const expected = 'local.m.fr.topshop.com:8080'
        expect(
          getShippingDestinationRedirectURL(
            shippingDestination,
            langHostnames,
            selectedLanguage,
            location
          )
        ).toEqual(expected)
      })

      it('should return a redirect URL for showcase', () => {
        global.process.env.NODE_ENV = 'showcase'
        const shippingDestination = 'Belgium'
        const selectedLanguage = 'French'
        const location = 'showcase.m.fr.topshop.com'
        const expected = 'showcase.m.fr.topshop.com'
        expect(
          getShippingDestinationRedirectURL(
            shippingDestination,
            langHostnames,
            selectedLanguage,
            location
          )
        ).toEqual(expected)
      })
    })
  })
})
