import { get, post, put, del } from '../api-service'
import configureStore from '../configure-store'
import { setItem, getItem, hasItem } from '../../../client/lib/cookie/utils'
import superagent from '../superagent'
import { sendAnalyticsApiResponseEvent } from '../../analytics'
import * as logger from '../../../client/lib/logger'
import { addToCookiesString } from '../cookie'
import * as hostnameModule from '../hostname'

jest.mock('../superagent', () => jest.fn())
jest.mock('../../../client/lib/cookie/utils')
jest.mock('../../../client/lib/logger', () => ({
  info: jest.fn(),
  error: jest.fn(),
  generateTransactionId: () => 1000000,
}))
jest.mock('../cacheable-urls', () => ({
  getCacheExpiration: () => 'date',
}))
jest.mock('../cookie', () => ({
  extractCookie: () => 'cookie',
  addToCookiesString: jest.fn(),
}))
jest.mock('../../actions/common/jsessionidActions', () => ({
  setJsessionid: () => {
    return 'SET_JSESSION_ID'
  },
}))
jest.mock('../../actions/common/errorMessageActions', () => ({
  setApiError: () => 'SET_API_ERROR',
  setCmsError: () => 'SET_CMS_ERROR',
}))
jest.mock('../../analytics')
jest.mock('../../actions/common/sessionActions', () => ({
  sessionExpired: () => 'SESSION_EXPIRED',
}))

describe('api-service', () => {
  let dispatchMock
  let dispatchParameterMock
  let requestMock
  const state = {
    config: {
      brandCode: 'abc',
      region: 'def',
    },
    viewport: {
      media: 'desktop',
    },
    auth: {
      token: 'token',
    },
    hostname: {
      isMobile: false,
    },
  }
  const setRequestMock = (
    body = {},
    response = {},
    headers = {},
    statusCode = 200,
    url = '',
    method
  ) => {
    if (statusCode !== 200) {
      requestMock = Promise.reject({
        body,
        response,
        headers,
        statusCode,
        method,
      })
    } else {
      requestMock = Promise.resolve({
        body,
        response,
        headers,
        statusCode,
        method,
      })
    }
    requestMock.headers = []
    requestMock.set = jest.fn((name, value) => {
      requestMock.headers.push(name, value)
    })
    requestMock.cache = jest.fn()
    requestMock.url = url
    requestMock.send = () => requestMock
    requestMock.method = method

    superagent.mockImplementation(() => requestMock)
  }

  beforeAll(() => {
    jest.resetAllMocks()

    dispatchParameterMock = jest.fn()
    dispatchMock = jest.fn((func) => {
      return func(dispatchParameterMock)
    })
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  afterAll(() => {
    jest.resetAllMocks()
  })

  describe('#get', () => {
    afterAll(() => {
      global.process.env.USE_NEW_HANDLER = null
      global.process.browser = false
    })

    describe('superagent', () => {
      let getStateMock

      beforeAll(() => {
        setRequestMock()

        getStateMock = jest.fn(() => ({
          ...state,
          jsessionid: { value: 'jsessionid' },
        }))
        global.process.browser = false
        global.process.env.USE_NEW_HANDLER = 'true'
      })

      it('should be called with the method GET and a composed url if the path does not contain https', () => {
        const httpRequest = get('/whatever')

        httpRequest(dispatchMock, getStateMock, {})

        expect(superagent).toHaveBeenCalledWith('GET', ':3000/api/whatever')
      })

      it('should be called with the method GET and with the initial path if the path contain https', () => {
        const httpRequest = get('https://whatever')

        httpRequest(dispatchMock, getStateMock, {})

        expect(superagent).toHaveBeenCalledWith('GET', 'https://whatever')
      })
    })

    describe('headers', () => {
      describe('not in browser', () => {
        let getStateMock

        beforeAll(() => {
          setRequestMock()
          addToCookiesString.mockImplementation((newValue, exitingValue) => {
            return `${exitingValue};${newValue}`
          })
        })

        beforeEach(() => {
          getStateMock = jest.fn(() => ({
            ...state,
            jsessionid: { value: 'jsessionid' },
          }))
          global.process.browser = false
          global.process.env.USE_NEW_HANDLER = 'true'
        })

        describe('BRAND-CODE header', () => {
          it('should be set when the path does not include "api-gateway"', () => {
            const httpRequest = get('/whatever')

            httpRequest(dispatchMock, getStateMock, {})

            expect(requestMock.set).toHaveBeenCalledWith(
              'BRAND-CODE',
              state.config.brandCode + state.config.region
            )
          })

          it('should not be set when the path includes "api-gateway"', () => {
            const httpRequest = get('/api-gateway')

            httpRequest(dispatchMock, getStateMock, {})

            expect(requestMock.set).not.toHaveBeenCalledWith(
              'BRAND-CODE',
              state.config.brandCode + state.config.region
            )
          })
        })

        describe('monty-mobile-hostname header', () => {
          it('should be set with the value of the state property hostname.isMobile', () => {
            const httpRequest = get('/whatever')

            httpRequest(dispatchMock, getStateMock, {})

            expect(requestMock.set).toHaveBeenCalledWith(
              'monty-mobile-hostname',
              state.hostname.isMobile
            )
          })
        })

        describe('deviceType header', () => {
          it('should be set', () => {
            const httpRequest = get('/whatever')

            httpRequest(dispatchMock, getStateMock, {})

            expect(addToCookiesString).toHaveBeenCalledWith(
              'deviceType=desktop',
              'token=token;jsessionid=jsessionid'
            )
            expect(requestMock.set).toHaveBeenCalledWith(
              'Cookie',
              'token=token;jsessionid=jsessionid;deviceType=desktop'
            )
          })
        })

        describe('Cookie', () => {
          describe('"jsessionid" has a value', () => {
            describe('Env variable USE_NEW_HANDLER is "true"', () => {
              it('sets the Cookie', () => {
                const httpRequest = get('/whatever')

                httpRequest(dispatchMock, getStateMock, {})

                expect(requestMock.set).toHaveBeenCalledTimes(4)
                expect(addToCookiesString).toHaveBeenCalledWith(
                  'jsessionid=jsessionid',
                  'token=token'
                )
                expect(requestMock.set).toHaveBeenCalledWith(
                  'Cookie',
                  'token=token;jsessionid=jsessionid;deviceType=desktop'
                )
              })
            })

            describe('Env variable USE_NEW_HANDLER is "false"', () => {
              it('does not set the jsessionid in the Cookie', () => {
                global.process.env.USE_NEW_HANDLER = 'false'
                const httpRequest = get('/whatever')

                httpRequest(dispatchMock, getStateMock, {})

                expect(requestMock.set).toHaveBeenCalledTimes(4)
                expect(addToCookiesString).not.toHaveBeenCalledWith(
                  'jsessionid=jsessionid',
                  'token=token'
                )
                expect(requestMock.set).toHaveBeenCalledWith(
                  'Cookie',
                  'token=token;deviceType=desktop'
                )
              })
            })

            describe('Auth token is not set', () => {
              it('does not set the token in the Cookie', () => {
                getStateMock.mockReturnValueOnce({
                  ...state,
                  auth: {},
                  jsessionid: { value: 'jsessionid' },
                })
                const httpRequest = get('/whatever')

                httpRequest(dispatchMock, getStateMock, {})

                expect(requestMock.set).toHaveBeenCalledTimes(4)
                expect(requestMock.set).toHaveBeenCalledWith(
                  'Cookie',
                  ';jsessionid=jsessionid;deviceType=desktop'
                )
              })
            })
          })

          describe('No "jsessionid"', () => {
            describe('Env variable USE_NEW_HANDLER is "true"', () => {
              it('does not set the jsessionid in the Cookie', () => {
                getStateMock.mockReturnValueOnce({
                  ...state,
                  jsessionid: { value: '' },
                })
                const httpRequest = get('/whatever')

                httpRequest(dispatchMock, getStateMock, {})

                expect(requestMock.set).toHaveBeenCalledTimes(4)
                expect(addToCookiesString).not.toHaveBeenCalledWith(
                  'jsessionid=jsessionid',
                  'token=token'
                )
                expect(requestMock.set).toHaveBeenCalledWith(
                  'Cookie',
                  'token=token;deviceType=desktop'
                )
              })
            })
          })
        })
      })

      describe('in browser', () => {
        let getStateMock

        beforeAll(() => {
          setRequestMock()

          getStateMock = jest.fn(() => ({
            ...state,
            jsessionid: { value: 'jsessionid' },
          }))
          global.process.browser = true
          global.process.env.USE_NEW_HANDLER = 'true'
        })

        it('should set the cache expiration', () => {
          const httpRequest = get('/whatever')

          httpRequest(dispatchMock, getStateMock, {})

          expect(requestMock.cache).toHaveBeenCalledWith('date')
        })

        describe('monty-mobile-hostname header', () => {
          it('should be set with the value returned by the function "isMobileHostname"', () => {
            hostnameModule.isMobileHostname = jest.fn(() => true)
            const httpRequest = get('/whatever')

            httpRequest(dispatchMock, getStateMock, {})

            expect(requestMock.set).toHaveBeenCalledWith(
              'monty-mobile-hostname',
              true
            )
          })
        })
      })

      describe('X-TRACE-ID', () => {
        beforeAll(() => {
          setRequestMock()
        })

        it('uses `traceId2` from client cookies', async () => {
          global.process.browser = true
          hasItem.mockImplementationOnce((key) => key === 'traceId2')
          getItem.mockImplementationOnce(
            (key) => (key === 'traceId2' ? '456R789' : '')
          )

          const store = configureStore({})

          await store.dispatch(get('/foo'))

          expect(requestMock.set).toHaveBeenCalledWith('X-TRACE-ID', '456R789')
          expect(setItem).toHaveBeenCalledWith('traceId2', '456R789')
        })

        it('uses `traceId` from the reduxContext for SSR', async () => {
          global.process.browser = false
          const store = configureStore({}, { traceId: '123R456' })

          await store.dispatch(get('/foo'))

          expect(requestMock.set).toHaveBeenCalledWith('X-TRACE-ID', '123R456')
          expect(setItem).toHaveBeenCalledWith('traceId2', '123R456')
        })

        it('fallback to generating a new `traceId` and sets cookie client side', async () => {
          global.process.browser = true
          hasItem.mockImplementationOnce(() => false)
          jest.spyOn(Date, 'now').mockImplementation(() => 98765)
          jest.spyOn(Math, 'random').mockImplementation(() => 0.123456)
          const store = configureStore({})

          await store.dispatch(get('/foo'))

          expect(requestMock.set).toHaveBeenCalledWith(
            'X-TRACE-ID',
            '98765R123456'
          )
          expect(setItem).toHaveBeenCalledWith('traceId2', '98765R123456')
        })

        it('fallback to generating a new `traceId` on the server', async () => {
          global.process.browser = false
          jest.spyOn(Date, 'now').mockImplementation(() => 98765)
          jest.spyOn(Math, 'random').mockImplementation(() => 0.123456)
          const store = configureStore({}, { traceId: '' })

          await store.dispatch(get('/foo'))

          expect(requestMock.set).toHaveBeenCalledWith(
            'X-TRACE-ID',
            '98765R123456'
          )
        })
      })
    })

    describe('response handler', () => {
      let getStateMock

      beforeEach(() => {
        getStateMock = jest.fn(() => ({
          ...state,
          jsessionid: { value: 'jsessionid' },
        }))
        global.process.env.USE_NEW_HANDLER = 'true'
      })

      describe('not in browser', () => {
        beforeAll(() => {
          global.process.browser = false
        })

        describe('the API returns a 504 error code', () => {
          describe('the url contains "/api/cms/"', () => {
            beforeAll(() => {
              setRequestMock(
                {},
                {
                  headers: {
                    'set-cookie': 'cookie',
                  },
                },
                {},
                504,
                '/api/cms/'
              )
            })

            it('should dispatch a CMS Error', async () => {
              const httpRequest = get('/whatever')

              try {
                await httpRequest(dispatchMock, getStateMock)
              } catch (e) {
                expect(dispatchParameterMock).toHaveBeenCalledWith(
                  'SET_JSESSION_ID'
                )
                expect(dispatchParameterMock).toHaveBeenCalledWith(
                  'SET_CMS_ERROR'
                )
              }

              expect.assertions(2)
            })
          })

          describe('the url does not contain "/api/cms/" and the error response status code is 500', () => {
            beforeAll(() => {
              setRequestMock(
                {},
                {
                  statusCode: 500,
                  headers: {
                    'set-cookie': 'cookie',
                  },
                },
                {},
                504,
                ''
              )
            })

            it('should dispatch an API Error', async () => {
              const httpRequest = get('/whatever')

              try {
                await httpRequest(dispatchMock, getStateMock)
              } catch (e) {
                expect(dispatchParameterMock).toHaveBeenCalledWith(
                  'SET_JSESSION_ID'
                )
                expect(dispatchParameterMock).toHaveBeenCalledWith(
                  'SET_API_ERROR'
                )
              }

              expect.assertions(2)
            })
          })

          describe('the url does not contain "/api/cms/" and the error response status code is 404', () => {
            beforeAll(() => {
              setRequestMock(
                {},
                {
                  statusCode: 404,
                  headers: {
                    'set-cookie': 'cookie',
                  },
                },
                {},
                504,
                ''
              )
            })

            it('should dispatch an API Error', async () => {
              const httpRequest = get('/whatever')

              try {
                await httpRequest(dispatchMock, getStateMock)
              } catch (e) {
                expect(dispatchParameterMock).toHaveBeenCalledWith(
                  'SET_JSESSION_ID'
                )
                expect(dispatchParameterMock).toHaveBeenCalledWith(
                  'SET_API_ERROR'
                )
              }

              expect.assertions(2)
            })
          })

          describe('the error body contains validation errors', () => {
            beforeAll(() => {
              setRequestMock(
                {},
                {
                  headers: {
                    'set-cookie': 'cookie',
                  },
                  body: {
                    originalMessage: 'Validation error',
                    validationErrors: [
                      { message: 'error1' },
                      { message: 'error2' },
                    ],
                  },
                },
                {},
                504,
                ''
              )
            })

            it('should set the first validation message in the error body', async () => {
              const httpRequest = get('/whatever')

              try {
                await httpRequest(dispatchMock, getStateMock)
              } catch (e) {
                expect(e.response.body.message).toEqual('error1')
              }

              expect.assertions(1)
            })
          })
        })
      })

      describe('in browser', () => {
        beforeAll(() => {
          global.process.browser = true
        })

        afterAll(() => {
          global.process.browser = false
        })

        describe('the session has expired', () => {
          beforeAll(() => {
            setRequestMock(
              {},
              {
                headers: {
                  'session-expired': 'true',
                },
              },
              {},
              504
            )
          })

          it('should dispatch Session Expired', async () => {
            const httpRequest = get('/whatever')

            try {
              await httpRequest(dispatchMock, getStateMock)
            } catch (e) {
              expect(dispatchParameterMock).toHaveBeenCalledWith(
                'SESSION_EXPIRED'
              )
              expect(logger.info).toHaveBeenCalledWith('api-service', {
                transactionId: 1000000,
                loggerMessage: 'session-expired',
              })
            }

            expect.assertions(2)
          })
        })
      })
    })
  })

  describe('#post', () => {
    let getStateMock

    beforeAll(() => {
      setRequestMock()

      getStateMock = jest.fn(() => ({
        ...state,
        jsessionid: { value: 'jsessionid' },
      }))
      global.process.browser = false
      global.process.env.USE_NEW_HANDLER = 'true'
    })

    describe('superagent', () => {
      it('should be called with the method POST and a composed url if the path does not contain https', () => {
        const httpRequest = post('/whatever')

        httpRequest(dispatchMock, getStateMock, {})

        expect(superagent).toHaveBeenCalledWith('POST', ':3000/api/whatever')
      })
    })

    describe('response handler', () => {
      describe('the API returns a 504 error code', () => {
        describe('the url contains "/api/cms/"', () => {
          beforeAll(() => {
            setRequestMock(
              {},
              {
                headers: {
                  'set-cookie': 'cookie',
                },
              },
              {},
              504,
              '/api/cms/'
            )

            global.process.browser = false
          })

          it('should not dispatch a CMS Error when the handlerError flag is not set', async () => {
            const httpRequest = post('/whatever')

            try {
              await httpRequest(dispatchMock, getStateMock)
            } catch (e) {
              expect(dispatchParameterMock).toHaveBeenCalledWith(
                'SET_JSESSION_ID'
              )
              expect(dispatchParameterMock).not.toHaveBeenCalledWith(
                'SET_CMS_ERROR'
              )
            }

            expect.assertions(2)
          })

          it('should dispatch a CMS Error when the handlerError flag is set to true', async () => {
            const httpRequest = post('/whatever', {}, true)

            try {
              await httpRequest(dispatchMock, getStateMock)
            } catch (e) {
              expect(dispatchParameterMock).toHaveBeenCalledWith(
                'SET_JSESSION_ID'
              )
              expect(dispatchParameterMock).toHaveBeenCalledWith(
                'SET_CMS_ERROR'
              )
            }

            expect.assertions(2)
          })
        })
      })
    })
  })

  describe('#put', () => {
    let getStateMock

    beforeAll(() => {
      setRequestMock()

      getStateMock = jest.fn(() => ({
        ...state,
        jsessionid: { value: 'jsessionid' },
      }))
      global.process.browser = false
      global.process.env.USE_NEW_HANDLER = 'true'
    })

    describe('superagent', () => {
      it('should be called with the method PUT and a composed url if the path does not contain https', () => {
        const httpRequest = put('/whatever')

        httpRequest(dispatchMock, getStateMock, {})

        expect(superagent).toHaveBeenCalledWith('PUT', ':3000/api/whatever')
      })
    })
  })

  describe('#deleted', () => {
    let getStateMock

    beforeAll(() => {
      setRequestMock()

      getStateMock = jest.fn(() => ({
        ...state,
        jsessionid: { value: 'jsessionid' },
      }))
      global.process.browser = false
      global.process.env.USE_NEW_HANDLER = 'true'
    })

    describe('superagent', () => {
      it('should be called with the method DELETE and a composed url if the path does not contain https', () => {
        const httpRequest = del('/whatever')

        httpRequest(dispatchMock, getStateMock, {})

        expect(superagent).toHaveBeenCalledWith('DELETE', ':3000/api/whatever')
      })
    })
  })

  describe('on Checkout page', () => {
    let getStateMock
    let oldWindowLocation

    beforeEach(() => {
      getStateMock = jest.fn(() => ({
        ...state,
        jsessionid: { value: 'jsessionid' },
      }))
    })

    beforeAll(() => {
      global.process.browser = true
      global.process.env.USE_NEW_HANDLER = 'true'
      oldWindowLocation = window.location
      delete window.location
      Object.defineProperty(window, 'location', {
        value: {
          pathname: '/checkout/login',
        },
      })
    })

    afterAll(() => {
      window.location = oldWindowLocation
      global.process.browser = false
      global.process.env.USE_NEW_HANDLER = null
    })

    describe('on successful status', () => {
      it('should dispatch analytics event, on success', async () => {
        setRequestMock(
          {},
          {
            headers: {
              'set-cookie': 'cookie',
            },
            body: {},
          },
          {},
          200,
          'http://m.topshop.com/api/getAccount',
          'GET'
        )
        const httpRequest = get('/whatever')

        try {
          await httpRequest(dispatchMock, getStateMock)
        } catch (e) {} // eslint-disable-line no-empty
        expect(sendAnalyticsApiResponseEvent).toHaveBeenCalledWith({
          apiEndpoint: 'api/getAccount',
          apiMethod: 'GET',
          responseCode: 200,
        })
      })
      it('should dispatch analytics event, on error', async () => {
        setRequestMock(
          {},
          {
            headers: {
              'set-cookie': 'cookie',
            },
            body: {},
          },
          {},
          518,
          'http://m.topshop.com/api/getAccount',
          'GET'
        )
        const httpRequest = get('/whatever')

        try {
          await httpRequest(dispatchMock, getStateMock)
        } catch (e) {
          expect(sendAnalyticsApiResponseEvent).toHaveBeenCalledWith({
            apiEndpoint: 'api/getAccount',
            apiMethod: 'GET',
            responseCode: 518,
          })
        }
      })
    })
  })
})
