import {
  processRedirectUrl,
  isLowStockProduct,
  parseCurrentPage,
} from '../products-utils'

jest.mock('react-router', () => ({
  browserHistory: {
    goBack: jest.fn(),
    replace: jest.fn(),
  },
}))
import { browserHistory } from 'react-router'

jest.mock('../window', () => ({
  getHostname: jest.fn(),
  changeURL: jest.fn(),
  isProductionBrandHost: jest.fn(),
}))
import { getHostname, changeURL, isProductionBrandHost } from '../window'

describe('Product Utils', () => {
  describe('HTML Utils', () => {
    const browser = global.process.browser

    afterEach(() => {
      global.process.browser = browser
      jest.resetAllMocks()
    })

    it('handles relative URL', () => {
      global.process.browser = true
      processRedirectUrl('/bbb')
      expect(browserHistory.replace).toHaveBeenCalledWith('/bbb')
    })

    it('handles locals URL', () => {
      getHostname.mockImplementation(() => 'aaa.com')
      isProductionBrandHost.mockImplementation(() => true)
      global.process.browser = true
      processRedirectUrl('http://aaa.com/bbb')
      expect(isProductionBrandHost).toHaveBeenCalledTimes(1)
      expect(browserHistory.replace).toHaveBeenCalledWith('/bbb')
    })

    it('handles external URL', () => {
      getHostname.mockImplementation(() => 'aaa.com')
      global.process.browser = true
      processRedirectUrl('http://ccc.com/bbb')
      expect(browserHistory.goBack).toHaveBeenCalledTimes(1)
      expect(changeURL).toHaveBeenCalledWith('http://ccc.com/bbb')
    })
  })
  describe('isLowStock', () => {
    it('should return false if quantity greather than stockThreshold', () => {
      expect(isLowStockProduct(15, 5)).toBe(false)
    })
    it('should return true if quantity less or equal than stockThreshold', () => {
      expect(isLowStockProduct(5, 5)).toBe(true)
    })
    it('should return true if quantity less or equal than undefined stockThreshold', () => {
      expect(isLowStockProduct(3)).toBe(true)
    })
  })
  describe('parseCurrentPage', () => {
    it('should return 1 if no product length is supplied', () => {
      expect(parseCurrentPage(undefined)).toEqual(1)
    })
    it('should return 2 if the product length is 24 or less', () => {
      expect(parseCurrentPage(8)).toEqual(2)
      expect(parseCurrentPage(10)).toEqual(2)
      expect(parseCurrentPage(16)).toEqual(2)
      expect(parseCurrentPage(20)).toEqual(2)
    })
    it('should return 3 if the product length is more than 24 and less than 48', () => {
      expect(parseCurrentPage(25)).toEqual(3)
      expect(parseCurrentPage(28)).toEqual(3)
      expect(parseCurrentPage(36)).toEqual(3)
      expect(parseCurrentPage(48)).toEqual(3)
    })
    it('should return 4 if the product length is more than 48 and less than 72', () => {
      expect(parseCurrentPage(49)).toEqual(4)
      expect(parseCurrentPage(56)).toEqual(4)
      expect(parseCurrentPage(62)).toEqual(4)
      expect(parseCurrentPage(72)).toEqual(4)
    })
  })
})
