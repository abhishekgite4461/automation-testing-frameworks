import { isDeliveryOptionDisabled } from '../is-delivery-disabled'

describe('isDeliveryOptionDisabled', () => {
  const deliveryDayUnavailable = {
    CFSiDay: 'Monday',
    expressDeliveryDay: false,
    homeExpressDeliveryDay: false,
    parcelCollectDay: false,
  }
  const deliveryDayAvailable = {
    CFSiDay: 'today',
    expressDeliveryDay: true,
    homeExpressDeliveryDay: true,
    parcelCollectDay: true,
  }
  describe('returns true if delivery option is store collection today and CFSi is', () => {
    const option = 'Collect from Store Today'
    it('not today returns true', () => {
      expect(isDeliveryOptionDisabled(option, deliveryDayUnavailable)).toBe(
        true
      )
    })
    it('today returns false', () => {
      expect(isDeliveryOptionDisabled(option, deliveryDayAvailable)).toBe(false)
    })
  })
  describe('if delivery option is express delivery and home express delivery is', () => {
    const option = 'Express Delivery'
    it('false returns true', () => {
      expect(isDeliveryOptionDisabled(option, deliveryDayUnavailable)).toBe(
        true
      )
    })
    it('true returns false', () => {
      expect(isDeliveryOptionDisabled(option, deliveryDayAvailable)).toBe(false)
    })
  })
  describe('if delivery option is "Next or Named Day Delivery" and home express delivery is', () => {
    const option = 'Next or Named Day Delivery'
    it('false returns true', () => {
      expect(isDeliveryOptionDisabled(option, deliveryDayUnavailable)).toBe(
        true
      )
    })
    it('true returns false', () => {
      expect(isDeliveryOptionDisabled(option, deliveryDayAvailable)).toBe(false)
    })
  })
  describe('if delivery option is express collect from store and express delivery is', () => {
    const option = 'Collect from Store Express'
    it('false returns true', () => {
      expect(isDeliveryOptionDisabled(option, deliveryDayUnavailable)).toBe(
        true
      )
    })
    it('true returns false', () => {
      expect(isDeliveryOptionDisabled(option, deliveryDayAvailable)).toBe(false)
    })
  })
  describe('if delivery option is express collect from store on Burton brand and express delivery is', () => {
    const option = 'Collect In Store Exp Next Day(Excl Sun)'
    it('false returns true', () => {
      expect(isDeliveryOptionDisabled(option, deliveryDayUnavailable)).toBe(
        true
      )
    })
    it('true returns false', () => {
      expect(isDeliveryOptionDisabled(option, deliveryDayAvailable)).toBe(false)
    })
  })
  describe('if delivery option is parcel shop and parcel collect is', () => {
    const option = 'Collect from ParcelShop'
    it('false returns true', () => {
      expect(isDeliveryOptionDisabled(option, deliveryDayUnavailable)).toBe(
        true
      )
    })
    it('true returns false', () => {
      expect(isDeliveryOptionDisabled(option, deliveryDayAvailable)).toBe(false)
    })
  })
  it('returns false if the delivery option is not recognized', () => {
    const option = 'Random Delivery Option'
    expect(isDeliveryOptionDisabled(option, deliveryDayAvailable)).toBe(false)
  })
})
