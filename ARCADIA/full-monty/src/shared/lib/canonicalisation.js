import canonicalisationMap from '../../../src/shared/constants/canonicalisationMap'

/**
 * @param url: m.topshop.com | m.burton.co.uk | ...
 * @returns {string} http://www.topshop.com | http://www.burton.co.uk | ...
 */
export const getCanonicalHostname = (url) => canonicalisationMap[url] || url
