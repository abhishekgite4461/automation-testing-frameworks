import { __, test } from 'ramda'

export function isDeliveryOptionDisabled(opt, deliveryDayAvailability) {
  const testOpt = test(__, opt)
  if (testOpt(/(?:Store.*Today)/i)) {
    return deliveryDayAvailability.CFSiDay !== 'today'
  } else if (
    testOpt(/(?:Express.*Delivery)/i) ||
    testOpt(/(?:Next.*Named.*Day.*Delivery)/i)
  ) {
    return !deliveryDayAvailability.homeExpressDeliveryDay
  } else if (testOpt(/(?:ParcelShop)/i)) {
    return !deliveryDayAvailability.parcelCollectDay
  } else if (
    testOpt(/(?:Store.*Express)/i) ||
    testOpt(/(?:Collect.*In.*Store.*Exp.*Next)/i)
  ) {
    return !deliveryDayAvailability.expressDeliveryDay
  }
  return false
}
