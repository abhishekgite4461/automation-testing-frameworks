export function defaultSchema() {
  return {
    socialFeed: [],
    blogFeed: [],
  }
}

export function setCampaignSocialFeed() {
  return (state, { socialFeed }) => ({ ...state, socialFeed })
}

export function setCampaignBlogFeed() {
  return (state, { blogFeed }) => ({ ...state, blogFeed })
}
