// Consult peerius documentation for the data to send
const createRefcode = (brandCode, lineNumber) => {
  // API can't be relied upon, sometimes the brand code is there, sometimes it isn't
  const code = brandCode && brandCode.toUpperCase()
  const product = lineNumber && lineNumber.toUpperCase()
  return product && product.substring(0, 2) === code ? product : code + product
}

export const home = () => ({})

export const product = ({ lineNumber, brandCode }) => ({
  refCode: createRefcode(brandCode, lineNumber),
})

export const category = ({ breadcrumbs }) => {
  return (
    (breadcrumbs &&
      breadcrumbs
        .filter(({ category }) => !!category) // Only care about breadcrumb categories
        .map((crumb) => crumb.label)
        .join('>')) ||
    null
  )
}

export const basket = ({ currency, bagProducts, brandCode }) =>
  bagProducts && bagProducts.length
    ? {
        currency,
        items: bagProducts.map(({ lineNumber, quantity, unitPrice }) => ({
          refCode: createRefcode(brandCode, lineNumber),
          qty: quantity,
          price: unitPrice,
        })),
      }
    : null

export const checkout = (props) => {
  const { subTotal, shipping, total } = props
  return {
    ...basket(props),
    subtotal: subTotal,
    shipping,
    total,
  }
}

export const order = (props) => {
  const { orderNo, orderLines } = props
  return {
    ...checkout({
      ...props,
      bagProducts: orderLines.map(({ lineNo, quantity, unitPrice }) => ({
        lineNumber: lineNo,
        quantity,
        unitPrice,
      })),
    }),
    orderNo,
  }
}

export const searchResults = ({ term, products, brandCode }) => ({
  term,
  results: products.map(({ lineNumber }) => ({
    refCode: createRefcode(brandCode, lineNumber),
  })),
})

export const brand = () => ({})

export const attribute = () => ({})

export const other = () => ({})
