import { isNil, isEmpty } from 'ramda'

// :: Any -> boolean
export const isNilOrEmpty = (x) => isNil(x) || isEmpty(x)
