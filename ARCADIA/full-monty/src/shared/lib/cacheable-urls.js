// 10 minute default cache expiration timeout
const defaultTTL = 600

// Expiration values in seconds
const timeouts = {
  scrAPI: [
    {
      matcher: /^\/.+\/site-options$/,
      expiration: process.env.CACHE_TTL_SCRAPI_SITE_OPTIONS || defaultTTL,
    },
    {
      matcher: /^\/.+\/navigation\/.+$/,
      expiration: process.env.CACHE_TTL_SCRAPI_NAVIGATION || defaultTTL,
    },
    {
      matcher: /^\/.+\/products?[?/].+$/,
      expiration: process.env.CACHE_TTL_SCRAPI_PRODUCTS || defaultTTL,
    },
    {
      matcher: /^\/.+\/cms\/.+$/,
      expiration: process.env.CACHE_TTL_SCRAPI_CMS || defaultTTL,
    },
  ],
  hapi: [
    {
      matcher: /(^\/$|\/\?)/,
      expiration: process.env.CACHE_TTL_MONTY_HOMEPAGE || defaultTTL,
    },
    {
      matcher: /^.+\/product\/.+$/,
      expiration: process.env.CACHE_TTL_MONTY_PDP || defaultTTL,
    },
    {
      matcher: /^.+\/category\/.+$/,
      expiration: process.env.CACHE_TTL_MONTY_CATEGORY || defaultTTL,
    },
    {
      matcher: /^\/api\/cms\/.+$/,
      expiration: process.env.CACHE_TTL_MONTY_CMS || defaultTTL,
    },
    {
      matcher: /^\/cmscontent.*$/,
      expiration: process.env.CACHE_TTL_MONTY_CMS || defaultTTL,
    },
    {
      // never cache keep-alive requests!
      matcher: /^\/api\/keep-alive$/,
      expiration: 0,
    },
    {
      matcher: /^\/api\/navigation\/.+$/,
      expiration: process.env.CACHE_TTL_MONTY_NAVIGATION || defaultTTL,
    },
    {
      matcher: /^\/api\/products?[?/].+$/,
      expiration: process.env.CACHE_TTL_MONTY_PRODUCTS || defaultTTL,
    },
    {
      matcher: /^\/api\/site-options$/,
      expiration: process.env.CACHE_TTL_MONTY_SITE_OPTIONS || defaultTTL,
    },
    {
      matcher: /^\/api\/stores-countries\?brand=.+$/,
      expiration: process.env.CACHE_TTL_MONTY_STORE_COUNTRIES || defaultTTL,
    },
    // Waiting on calculations for how much we want to cache / poll features then this can be reduced
    {
      matcher: /^\/api\/features$/,
      expiration: process.env.FEATURES_CACHE_EXPIRATION || 3600,
    },
    {
      matcher: /^\/api\/features\/[^/]+$/,
      expiration: 600,
    },
    {
      matcher: /^\/assets\/.+\/images\/.+$/,
      expiration: 24 * 60 * 60,
      etag: 'hash',
    },
    /**
     TODO: update the public folder, move all images into the image sub-folder and make sure webpack works properly
     Images on main folders (expire in 1 day)
     */
    {
      matcher: /^\/assets\/.+\/*.(gif|jpg|jpeg|svg|png)/,
      expiration: 24 * 60 * 60,
      etag: 'hash',
    },
    {
      matcher: /^\/assets\/.+\/vendors\/.+$/,
      expiration: 24 * 60 * 60,
    },
    {
      matcher: /^\/assets\/.+\/fonts\/.+$/,
      expiration: 365 * 24 * 60 * 60,
      etag: 'hash',
    },
    /**
      TODO: update the public folder, move all fonts into the font sub-folder and make sure webpack works properly
      Fonts on main folders (expire in 1 year)
     */
    {
      matcher: /^\/assets\/.+\/*.(woff|eot|woff2|ttf)/,
      expiration: 365 * 24 * 60 * 60,
      etag: 'hash',
    },
    {
      matcher: /^\/assets\/common\/.*$/,
      expiration: 365 * 24 * 60 * 60,
    },
    {
      matcher: /^\/assets\/.+\/styles.+\.css$/,
      expiration: 365 * 24 * 60 * 60,
    },
    {
      matcher: /^\/assets\/content\/cms\/.+$/,
      expiration: 7 * 24 * 60 * 60,
    },
  ],
}

export function getCacheSettings(api, path) {
  return timeouts[api].find((item) => item.matcher.test(path))
}

export function getCacheExpiration(api, path) {
  const config = getCacheSettings(api, path)
  if (config && typeof config.expiration === 'string')
    config.expiration = parseInt(config.expiration, 0)
  return config ? config.expiration : 0
}

export function getEtagMethod(api, path) {
  const config = getCacheSettings(api, path)
  return config && config.etag ? config.etag : false
}
