/* This is needed since the API returns keynames with appended numbers. These need to be ignored to get the data. */
export const getMatchingAttribute = (key, object = {}) => {
  if (!object) return undefined
  const attributeKey = Object.keys(object).find((objectKey) =>
    objectKey.includes(key)
  )
  return object[attributeKey]
}

export const checkIfOneSizedItem = (items) =>
  items.length === 1 && (items[0].size === 'ONE' || items[0].size === '000')
