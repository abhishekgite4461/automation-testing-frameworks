import aggregateRatingSchema from './aggregateRatingSchema'
import offerSchema from './offerSchema'

// http://schema.org/Product

export default function productSchema(
  {
    name,
    unitPrice,
    description,
    assets,
    colour,
    productId,
    attributes: { AverageOverallRating, NumReviews },
  },
  brand,
  url,
  priceCurrency
) {
  return {
    '@context': 'http://schema.org',
    '@type': 'Product',
    brand,
    name,
    offers: offerSchema({ price: unitPrice, priceCurrency }),
    url,
    description,
    image: assets && assets.length && assets[0] && assets[0].url,
    color: colour,
    productID: productId,
    aggregateRating: aggregateRatingSchema(AverageOverallRating, NumReviews),
  }
}
