// http://schema.org/Offer

export default function productSchema({ price, priceCurrency }) {
  return {
    '@context': 'http://schema.org',
    '@type': 'Offer',
    price,
    priceCurrency,
  }
}
