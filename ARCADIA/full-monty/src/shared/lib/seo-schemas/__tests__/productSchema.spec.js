import productSchema from '../productSchema'

describe('productSchema', () => {
  it('should contain correct props', () => {
    const json = productSchema(
      {
        name: 'name',
        unitPrice: 'unitPrice',
        description: 'description',
        assets: [{ url: 'image' }],
        colour: 'colour',
        productId: 'productId',
        attributes: {
          AverageOverallRating: 'AverageOverallRating',
          NumReviews: 'NumReviews',
        },
      },
      'topshop',
      'm.topshop.com',
      'GBP'
    )
    expect(json).toEqual({
      '@type': 'Product',
      '@context': 'http://schema.org',
      brand: 'topshop',
      color: 'colour',
      description: 'description',
      image: 'image',
      name: 'name',
      productID: 'productId',
      url: 'm.topshop.com',
      offers: {
        '@context': 'http://schema.org',
        '@type': 'Offer',
        price: 'unitPrice',
        priceCurrency: 'GBP',
      },
      aggregateRating: {
        '@context': 'http://schema.org',
        '@type': 'AggregateRating',
        bestRating: 5,
        ratingValue: 'AverageOverallRating',
        reviewCount: 'NumReviews',
        worstRating: 1,
      },
    })
  })
})
