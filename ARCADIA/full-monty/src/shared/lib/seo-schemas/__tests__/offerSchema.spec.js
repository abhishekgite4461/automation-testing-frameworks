import offerSchema from '../offerSchema'

describe('offerSchema', () => {
  it('should contain correct props', () => {
    const json = offerSchema({
      price: '24.00',
      priceCurrency: 'GBP',
    })
    expect(json['@context']).toBe('http://schema.org')
    expect(json['@type']).toBe('Offer')
    expect(json.price).toBe('24.00')
    expect(json.priceCurrency).toBe('GBP')
  })
})
