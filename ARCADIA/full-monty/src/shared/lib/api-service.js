import superagent from './superagent'
import { getCacheExpiration } from './cacheable-urls'
import { setApiError, setCmsError } from '../actions/common/errorMessageActions'
import { sessionExpired } from '../actions/common/sessionActions'
import { setJsessionid } from '../actions/common/jsessionidActions'
import { sendAnalyticsApiResponseEvent } from '../analytics'
import { getItem, hasItem, setItem } from '../../client/lib/cookie/utils'
import ramda from 'ramda'
import { extractCookie, addToCookiesString } from './cookie'
import * as logger from '../../client/lib/logger'
import { isMobileHostname } from './hostname'

const prefix = process.browser
  ? `${window.location.protocol}//${window.location.host}`
  : `${process.env.CORE_API_HOST || ''}:${process.env.CORE_API_PORT || 3000}`

export function getCodeFromState(state) {
  const {
    config: { brandCode, region },
  } = state
  return brandCode + region
}

const analyticsHandler = (dispatch, { url, method }, res) => {
  if (process.browser && /\/checkout\//.test(window.location.pathname)) {
    dispatch(
      sendAnalyticsApiResponseEvent({
        apiEndpoint: url
          .split('/')
          .slice(3)
          .join('/')
          .split('?')[0],
        apiMethod: method,
        responseCode: res.statusCode,
      })
    )
  }
  return res
}

const requestWithInspection = (dispatch, request, transactionId) => {
  const inspectSession = (resp) => {
    if (process.browser) {
      if (resp && !resp.cached && resp.headers['session-expired']) {
        logger.info('api-service', {
          transactionId,
          loggerMessage: 'session-expired',
        })

        dispatch(sessionExpired())
      }
    } else {
      const responseCookies = ramda.pathOr([], ['headers', 'set-cookie'], resp)
      const responseJsessionId = extractCookie('jsessionid', responseCookies)

      if (responseJsessionId) {
        dispatch(setJsessionid(responseJsessionId))
      }
    }
    return resp
  }

  return request.then(inspectSession, (err) => {
    inspectSession(err.response)
    throw err
  })
}

function handleResponse(request, handleError = true, transactionId) {
  return (dispatch) => {
    const requestPromise = (retried) => {
      return requestWithInspection(dispatch, request, transactionId).catch(
        (error) => {
          return (error.status === 504 || error.statusCode === 504) && !retried
            ? requestPromise(true)
            : Promise.reject(error)
        }
      )
    }

    const requestWithAnalytics = requestPromise()
      .then((res) => analyticsHandler(dispatch, request, res))
      .catch((error) => {
        throw analyticsHandler(dispatch, request, error)
      })

    return handleError
      ? requestWithAnalytics.catch((error) => {
          if (request.url.includes('/api/cms/')) {
            dispatch(setCmsError(error))
          } else if (
            error.response &&
            (error.response.statusCode === 500 ||
              error.response.statusCode === 404)
          ) {
            dispatch(setApiError(error))
          }

          // todo: refactor once UI components support array of server-side validation messages
          if (
            error.response &&
            error.response.body &&
            error.response.body.originalMessage &&
            error.response.body.originalMessage === 'Validation error' &&
            Array.isArray(error.response.body.validationErrors) &&
            error.response.body.validationErrors.length
          ) {
            const validationError = error.response.body.validationErrors[0]
            error.response.body.message = validationError.message
          }

          throw error
        })
      : requestWithAnalytics
  }
}

const generateTraceId = () =>
  `${Date.now()}R${Math.floor(Math.random() * 1000000, 0)}`

const getTraceId = (reduxContext) => {
  if (hasItem('traceId2')) return getItem('traceId2')

  if (!process.browser && reduxContext.traceId) return reduxContext.traceId

  return generateTraceId()
}

const setTraceId = (request, traceId) => {
  setItem('traceId2', traceId)
  request.set('X-TRACE-ID', traceId)
}

const setUserGeo = (request, { xUserGeo }) => {
  if (xUserGeo) request.set('X-USER-GEO', xUserGeo)
}

const httpRequest = (path, data = {}, handleError = true, method = 'GET') => {
  const transactionId = logger.generateTransactionId()
  return (dispatch, getState, reduxContext = {}) => {
    const state = getState()
    const url = path.includes('https') ? path : `${prefix}/api${path}`

    const request = superagent(method, url)

    if (!path.includes('api-gateway')) {
      request.set('BRAND-CODE', getCodeFromState(state))
    }

    const token = ramda.path(['auth', 'token'], state)
    const jsessionid = ramda.path(['jsessionid', 'value'], state)
    const traceId = getTraceId(reduxContext)

    logger.info('api-service', {
      loggerMessage: 'HTTP PARAMS',
      transactionId,
      url,
      token,
      jsessionid,
      traceId,
    })

    let hostnameIsMobile

    if (!process.browser) {
      let cookies = ''

      if (token) {
        // if request happens as result of dispatching 'needs' in a component, there's no cookie set
        cookies = `token=${token}`
      }
      if (jsessionid && process.env.USE_NEW_HANDLER === 'true') {
        // WCS Api: in this case a server side rendering is happening and the browser sent a jsessionid cookie,
        // in server-side-renderer the jsessionid value cookie value is saved in the Store and here we are
        // taking that value from the Store and setting a cookie to the request that will be sent to monty hapi API (e.g.: /site-options).
        cookies = addToCookiesString(`jsessionid=${jsessionid}`, cookies)
      }

      // Retrieving from the state the device type set by server-side-renderer and setting a cookie that
      // will be used in coreApi to send to WCS data about the device making the request.
      const deviceType = ramda.path(['viewport', 'media'], state)
      cookies = addToCookiesString(`deviceType=${deviceType}`, cookies)

      request.set('Cookie', cookies)

      hostnameIsMobile = ramda.path(['hostname', 'isMobile'], state)
      setUserGeo(request, reduxContext)
    } else {
      hostnameIsMobile = isMobileHostname(window.location.host)
    }

    // [MJI-1084] This is needed to be able to mark the WCS orders as orders coming from www. or m.
    request.set('monty-mobile-hostname', hostnameIsMobile)

    setTraceId(request, traceId)
    switch (method.toUpperCase()) {
      case 'GET':
        request.cache(process.browser && getCacheExpiration('hapi', path))
        return dispatch(handleResponse(request, handleError, transactionId))
      case 'POST':
      case 'PUT':
      case 'DELETE':
        return dispatch(
          handleResponse(request.send(data), handleError),
          transactionId
        )
      default:
        throw new Error(`Unsupported method: ${method}`)
    }
  }
}

export function get(url, handleError = true) {
  return httpRequest(url, null, handleError)
}

export function post(url, data, handleError = false) {
  return httpRequest(url, data, handleError, 'POST')
}

export function put(url, data, handleError = true) {
  return httpRequest(url, data, handleError, 'PUT')
}

export function del(url, data) {
  return httpRequest(url, data, false, 'DELETE')
}
