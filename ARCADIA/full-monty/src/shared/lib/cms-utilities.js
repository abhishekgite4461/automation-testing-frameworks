import React from 'react'
import { browserHistory } from 'react-router'
import { assocPath, path } from 'ramda'
import { dispatch } from './get-value-from-store'
import {
  setProductIdQuickview,
  showSizeGuide,
  setSizeGuide,
} from '../actions/common/productsActions'
import { addDDPToBag } from '../actions/common/ddpActions'
import { showModal } from '../actions/common/modalActions'
import ProductQuickview from '../components/containers/ProductQuickview/ProductQuickview'

// adds/removes properties from window.sandbox object.
const setDataOnWindow = (data, path) => {
  window.sandbox = path ? assocPath(path, data, window.sandbox) : data
}

function unmountPreviousSandboxDOMNode(id) {
  if (window.sandbox && window.sandbox.unmounter) {
    window.sandbox.unmounter(id)
  }
}

function mapMountedSandboxDOMNodeToBundle() {
  if (
    !window.sandbox ||
    typeof window.sandbox.applyBundleMapping !== 'function'
  )
    return

  window.sandbox.applyBundleMapping()
}

function applyBundleMapping() {
  if (!Array.isArray(this.toBeMapped) || !this.toBeMapped.length) return // there is nothing to map

  const allItems = this.toBeMapped.filter((item) => {
    const bundle = item.props.bundle
    const func = this.jsBundles[bundle]
    if (func) {
      const componentDivId = `Sandbox-${item.id}`
      func(componentDivId, item.props) // render(<Comp {...item.props}/>, document.getElementById(`Sandbox-${item.id}`))

      window.sandbox.mapped = [...window.sandbox.mapped, componentDivId] // we will use this to unmount this component (avoid memot) when another one is mounted e.g.: e-receipts -> students

      return false
    }
    return true
  })
  setDataOnWindow(allItems, ['toBeMapped'])
}

const historyPush = (url) => browserHistory.push(url)

const openQuickViewProduct = (productId) => {
  dispatch(setProductIdQuickview(productId))
  dispatch(showModal(<ProductQuickview />, { mode: 'sandboxQuickview' }))
}

// @TODO: Remove this API as soon as a SideBar has been implemented inside Monty-CMS.
const openSizeGuideDrawer = (sizeGuideType) => {
  dispatch((dispatch, getState) => {
    const state = getState()
    const {
      viewport: { media },
    } = state
    if (media === 'mobile') {
      historyPush(`/size-guide/${sizeGuideType}`)
    } else {
      dispatch(setSizeGuide(sizeGuideType))
      dispatch(showSizeGuide())
    }
  })
}

const addDDPToBasket = (skuId) => {
  dispatch(addDDPToBag(skuId))
}

// Initialize window.sandbox.
const checkCmsInit = () => {
  if (!window.sandbox || !window.sandbox.applyBundleMapping) {
    const dataSetByBundles = path(['sandbox', 'jsBundles'], window)
    setDataOnWindow({
      jsBundles: {
        ...dataSetByBundles,
      },
      toBeMapped: [],
      mapped: [],
      applyBundleMapping,
      historyPush,
      openQuickViewProduct,
      openSizeGuideDrawer,
      addDDPToBasket,
    })
  }
}

const addElementToQueue = (id, props) => {
  const newItem = { id, props }
  const currentMapping = window.sandbox.toBeMapped
  const newMapping = currentMapping ? [...currentMapping, newItem] : [newItem]
  setDataOnWindow(newMapping, ['toBeMapped'])
}

const updateNewSandBox = (id, props) => {
  checkCmsInit()
  addElementToQueue(id, props)
}

// TODO remove this once WCS fixes URLs https://arcadiagroup.atlassian.net/browse/DES-3766
export const fixCmsUrl = (responsiveCMSUrl) => {
  if (!responsiveCMSUrl) {
    return ''
  }

  return responsiveCMSUrl.startsWith('/')
    ? responsiveCMSUrl
    : `/${responsiveCMSUrl}`
}

export default {
  updateNewSandBox,
  unmountPreviousSandboxDOMNode,
  mapMountedSandboxDOMNodeToBundle,
}
