import { createStore, applyMiddleware, compose } from 'redux'
import { syncHistory } from 'react-router-redux'
import { browserHistory } from 'react-router'
import { apiMiddleware } from 'redux-api-middleware'
import { montyApiMiddleware } from '../middleware/preprocess-redux-api-middleware'
import localiseMiddleware from '../middleware/localise-middleware'
import createSagaMiddleware, { END } from 'redux-saga'
import ReduxAsyncQueue from 'redux-async-queue'
import thunkMiddleware from 'redux-thunk'
import reducer from './combine-reducers'
import rootSaga from '../sagas'
import analyticsMiddleware from '../analytics/middleware/analytics-middleware'
import analyticsStoreObserver from '../analytics/storeObserver'

const sagaMiddleware = createSagaMiddleware()

let middleware = [
  montyApiMiddleware,
  apiMiddleware,
  thunkMiddleware,
  ReduxAsyncQueue,
  sagaMiddleware,
  localiseMiddleware,
  analyticsMiddleware,
]

if (process.browser) {
  const reduxRouterMiddleware = syncHistory(browserHistory)
  middleware = [reduxRouterMiddleware, ...middleware]
}

/* ::
type ReduxContext = {
  traceId: string
}
*/

const finalCreateStore = (
  reducer,
  initialState,
  reduxContext = {} /* : ReduxContext */
) => {
  const finalMiddlwares = middleware.map(
    (m) =>
      m === thunkMiddleware
        ? thunkMiddleware.withExtraArgument(reduxContext)
        : m
  )

  return compose(
    applyMiddleware(...finalMiddlwares),
    typeof window === 'object' && typeof window.devToolsExtension === 'function'
      ? window.devToolsExtension()
      : (f) => f
  )(createStore)(reducer, initialState)
}

function createChromeExtBridge(store) {
  if (store && typeof window === 'object' && window.__CKE_REGISTER_REDUX__) {
    window.__CKE_REGISTER_REDUX__(store)
  }
}

export default function configureStore(initialState = {}, reduxContext) {
  const store = finalCreateStore(reducer, initialState, reduxContext)

  analyticsStoreObserver(store)
  store.close = () => store.dispatch(END) // End Sagas
  store.sagaPromise = sagaMiddleware.run(rootSaga)

  createChromeExtBridge(store)

  return store
}
