/**
 * Given a hostname string it returns true if the hostname starts with "m." or "local.m."
 * false otherwise.
 *
 * @param {String} hostname
 * @return {Boolean}
 */
export function isMobileHostname(hostname) {
  return (
    typeof hostname === 'string' &&
    (hostname.startsWith('m.') || hostname.startsWith('local.m.'))
  )
}
