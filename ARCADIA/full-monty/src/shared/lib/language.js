import { allLanguages } from '../constants/languages'

export function getLanguageCodeByName(name) {
  const match = allLanguages.find((o) => o.value === name)
  return match ? match.isoCode : undefined
}
