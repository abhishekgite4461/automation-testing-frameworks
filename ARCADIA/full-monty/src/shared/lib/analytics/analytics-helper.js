import navigationConsts from '../../constants/navigation'
import { setInternalNavigation } from '../../actions/common/analyticsActions'

function getNodesCategories(categoryHistory) {
  return categoryHistory.reduce((string, category) => {
    return `${string}:${category.label}`
  }, '')
}

export function setAnalyticsIntnav(leafLabel, categoryType) {
  return (dispatch, getState) => {
    const type =
      navigationConsts.PRODUCT_CATEGORIES_GROUP_TYPE === categoryType
        ? 'Global Nav'
        : navigationConsts.HELP_AND_INFO_GROUP_TYPE === categoryType
          ? 'Footer'
          : null
    if (type) {
      const { categoryHistory } = getState().navigation
      dispatch(
        setInternalNavigation({
          type,
          details: `${type}${getNodesCategories(categoryHistory)}:${leafLabel}`,
        })
      )
    }
  }
}
