let hasLoaded = false

function createS() {
  const s = new window.AppMeasurement()
  window.s = s
}

export function getHasLoaded() {
  return hasLoaded
}

export function createAnalyticsObject() {
  if (window.s) {
    delete window.s
  }
  createS()
}

export function setAnalyticsObject(fn, data, isTrackLink) {
  // if not loaded then wait for load
  if (!window.AppMeasurement) {
    const scriptTag = document.getElementById('analyticsScript')
    if (scriptTag) {
      scriptTag.addEventListener('load', () => {
        createS()
        hasLoaded = true
        fn(data, isTrackLink)
      })
    }
  } else {
    hasLoaded = true
    fn(data, isTrackLink)
  }
}
