import { analyticsTl } from '../../lib/analytics/analytics'
import { updateUniversalVariable } from '../../lib/analytics/qubit-analytics'
import { isProductViewSelected } from '../../selectors/productViewsSelectors'

function mapProductObject(product, state) {
  const {
    productId,
    seoUrl,
    name,
    unitPrice,
    wasPrice,
    wasWasPrice,
    rrp,
    lineNumber,
    assets,
  } = product
  const {
    config: { currencyCode },
  } = state
  const imageAsset =
    assets && assets.find((asset) => asset.assetType === 'IMAGE_LARGE')

  return {
    id: productId ? productId.toString() : '',
    url: seoUrl,
    name,
    image_url: imageAsset ? imageAsset.url : undefined,
    sku_code: lineNumber,
    unit_sale_price: unitPrice ? parseFloat(unitPrice) : 0,
    unit_price:
      rrp || wasWasPrice || wasPrice || unitPrice
        ? parseFloat(rrp || wasWasPrice || wasPrice || unitPrice)
        : 0,
    currency: currencyCode,
  }
}

export function updateUVProductsList(state) {
  const {
    products: { products, categoryTitle, totalProducts },
    grid: { columns },
    sorting: { currentSortOption },
    refinements: { appliedOptions },
  } = state
  const items = products
    ? products.map((product) => mapProductObject(product, state))
    : []
  return {
    items,
    query: categoryTitle,
    result_count: totalProducts,
    layout: columns,
    sort_by: currentSortOption || 'Relevance',
    filters: appliedOptions,
  }
}

// adobe analytics data is moved to ./page-data/plp.js
export function setProducts() {
  return (dispatch) => {
    dispatch(updateUniversalVariable())
  }
}

export function layoutChange() {
  return (dispatch, getState) => {
    const state = getState()
    const productViewSelected = isProductViewSelected(state)
    const selectedViewLabel = productViewSelected
      ? 'Product view'
      : 'Outfit view'
    analyticsTl({ prop23: `Layout:${state.grid.columns}:${selectedViewLabel}` })
    dispatch(updateUniversalVariable())
  }
}
