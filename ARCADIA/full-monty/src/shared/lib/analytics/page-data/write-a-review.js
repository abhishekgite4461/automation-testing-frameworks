const getProductString = (recentlyViewedItem) => {
  const { name, lineNumber } = recentlyViewedItem
  return `(${lineNumber})${name}`
}

export default (state) => {
  const { recentlyViewed } = state
  return {
    pageName: 'Bazaar Voice',
    pageType: 'Category',
    firstCategory: 'Bazaar Voice',
    events: 'event27',
    eVar24: 'Bazaarvoice^RatingsAndReviews^Action^Write^Default',
    products:
      recentlyViewed.length > 0
        ? getProductString(recentlyViewed[0])
        : undefined,
  }
}
