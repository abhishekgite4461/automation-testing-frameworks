import orderCompleted from '../order-completed-v2'

// mocks
import {
  orderCompletedTwoProducts,
  orderCompletedApplyOrderDiscount,
  orderCompletedApplyDeliveryDiscount,
  orderCompletedCollectFromStoreStandard,
  orderCompletedCollectFromStoreExpress,
  orderCompletedDiscountedItem,
  orderCompletedMarkedDownItems,
} from '../../../../../../test/mocks/checkout/orderCompleted/orderCompleted-mocks'

describe('[Adobe Analytics] Order Completed v2', () => {
  // state for scenario 1
  const state = orderCompletedTwoProducts

  it('should return `undefined` if no `partialOrderSummary`', () => {
    const emptyPartialOrderSummaryState = {
      checkout: {
        partialOrderSummary: {},
      },
    }
    expect(orderCompleted(emptyPartialOrderSummaryState)).toBeUndefined()
  })
  it('should return `undefined` if no `orderCompleted`', () => {
    const emptyOrderCompletedState = {
      checkout: {
        orderCompleted: {},
      },
    }
    expect(orderCompleted(emptyOrderCompletedState)).toBeUndefined()
  })

  describe('Scenario 1. Page Load', () => {
    it('should return basic data (pageName, channel [firstCategory] and prop3 [granularPageName] and eVar40', () => {
      expect(orderCompleted(state)).toEqual(
        expect.objectContaining({
          pageName: 'Order Confirmed',
          granularPageName: 'Order Confirmed',
          firstCategory: 'Checkout',
          channel: 'Checkout',
          prop3: 'Order Confirmed',
          eVar40: 1638681,
        })
      )
    })
    it('should set `isCheckout` to `true`', () => {
      expect(orderCompleted(state).isCheckout).toBe(true)
    })
    it('should set page load `events`', () => {
      expect(orderCompleted(state).events).toBe(
        'event3,event69,purchase,event58,event66,event71=1.00,event7,event11'
      )
    })

    describe('Products', () => {
      const product1 = orderCompleted(state)
        .products.split(',')
        .shift()

      it('should add each product', () => {
        const products = orderCompleted(state).products.split(',')
        expect(products).toContain(
          ';(32J07MPNK)JETSET V-Cut Slingback Shoes;1;42.00;event58=42.00|event66=42.00;eVar35=35|eVar36=PINK'
        )
        expect(products).toContain(
          ';(42V02MBLK)GEORGIA Mules;1;26.00;event58=26.00|event66=26.00;eVar35=35|eVar36=BLACK'
        )
      })
      it('should add the delivery method', () => {
        expect(
          orderCompleted(state)
            .products.split(',')
            .pop()
        ).toContain(';UK Standard up to 4 days;;;event3=4.00|event69=4.00;')
      })
      it('should add `(Product ID)<Product Description>;Quantity;Total Cost` to `products`', () => {
        expect(product1).toContain(
          ';(32J07MPNK)JETSET V-Cut Slingback Shoes;1;42.00;'
        )
      })
      it('should add `event58=<Total Was Price>` and `event66=<Total Now Price>` to `products`', () => {
        expect(product1).toContain(';event58=42.00|event66=42.00;')
      })
      it('should add `eVar35=<size>` and `eVar36=<colour>` to `products`', () => {
        expect(product1).toContain(';eVar35=35|eVar36=PINK')
      })
    })

    describe('Delivery option returned as a Product', () => {
      const deliveryProduct = orderCompleted(state)
        .products.split(',')
        .pop()

      it('should add `<delivery type>` as `Product ID`', () => {
        expect(deliveryProduct).toContain(';UK Standard up to 4 days;')
      })
      it('should add `event3=<Delivery Price>` and `event69=<Local Delivery Price>`', () => {
        expect(deliveryProduct).toContain(';event3=4.00|event69=4.00;')
      })
    })
  })

  describe('Scenario 2. Order Discount (eg. apply discount code: TESTUK5OFF)', () => {
    // journey: buy Apply off £5 pound discount - Order Discount
    const state = orderCompletedApplyOrderDiscount

    it('should add event6 and event68 to page load `events`', () => {
      expect(orderCompleted(state).events).toContain('event6')
      expect(orderCompleted(state).events).toContain('event68')
    })
    it('should add `eVar12=<description of discount>`', () => {
      expect(orderCompleted(state).eVar12).toBe('£5 off')
    })

    describe('Products', () => {
      it('should add each product', () => {
        const products = orderCompleted(state).products.split(',')
        expect(products).toContain(
          ';(10S76MRED)Rocket Man Tea Dress;2;92.00;event58=92.00|event6=3.77|event66=92.00|event68=3.77;eVar35=12|eVar36=RED'
        )
        expect(products).toContain(
          ';(27V01LBLE)Bonded Lace Wrap Midi;3;30.00;event58=180.00|event6=1.23|event66=30.00|event68=1.23;eVar35=10|eVar36=BLUE'
        )
      })
      it('should include `events`: `event58<Was Price>`,`event66=<Total Now Price>`', () => {
        const product1 = orderCompleted(state)
          .products.split(',')
          .shift()
        expect(product1).toContain(';event58=92.00|event6=3.77|event66=92.00')
      })
    })
  })

  describe('Scenario 3. Delivery Discount (eg. Free standard shipping any spend - TSDEL0103)', () => {
    // journey: buy item for less than £50 and apply Free standard shipping promotion code: TSDEL0103
    const state = orderCompletedApplyDeliveryDiscount

    describe('Delivery option returned as a Product', () => {
      it('should add `UK Standard up to 4 days` as `Product ID`', () => {
        expect(
          orderCompleted(state)
            .products.split(',')
            .pop()
        ).toContain(';UK Standard up to 4 days;;;')
      })

      it('should add `event3=0.00` and `event69=0.00`', () => {
        expect(
          orderCompleted(state)
            .products.split(',')
            .pop()
        ).toContain(';event3=0.00|event69=0.00;')
      })
    })
  })

  describe('Scenario 4. Standard Delivery to a store', () => {
    // journey: buy item and select Collect From Store and Collect From Store Standard
    const state = orderCompletedCollectFromStoreStandard

    describe('Delivery option returned as a Product', () => {
      const deliveryProduct = orderCompleted(state)
        .products.split(',')
        .pop()
      it('should add `Collect From Store Standard` as `Product ID`', () => {
        expect(deliveryProduct).toContain(';Collect From Store Standard;')
      })
      it('should add `event3=<Delivery Price>` and `event69=<Local Delivery Price>`', () => {
        expect(deliveryProduct).toContain(';event3=0.00|event69=0.00;')
      })
    })
  })

  describe('Scenario 5. Express Delivery to a store', () => {
    // journey: but item and select Collect From Store and Collect From Store Express
    const state = orderCompletedCollectFromStoreExpress

    describe('Delivery option returned as a Product', () => {
      const deliveryProduct = orderCompleted(state)
        .products.split(',')
        .pop()
      it('should add `Collect From Store Express` as `Product ID`', () => {
        expect(deliveryProduct).toContain(';Collect From Store Express;')
      })
      it('should add `event3=<Delivery Price>` and `event69=<Local Delivery Price>`', () => {
        expect(deliveryProduct).toContain(';event3=3.00|event69=3.00;')
      })
    })
  })

  describe('Scenario 6. Discounted Item (eg. 3 for £8 ancke socks promotion)', () => {
    // journey: buy 3 for £8 pound socks (Crop Ribbed Glitter Socks)
    const state = orderCompletedDiscountedItem

    describe('Products', () => {
      const product1 = orderCompleted(state)
        .products.split(',')
        .shift()

      it('should include `events`: `event58<Was Price>`,`event66=<Total Now Price>`,`event5=<Item Discount>`,`event67=<Local Item Discount>`', () => {
        expect(product1).toContain(
          ';event5=2.50|event58=10.50|event66=10.50|event67=2.50;'
        )
      })
      it('should include `eVars`: `eVar12=<description of discount>`, `eVar35=<Size>` and `eVar36=<Colour>`', () => {
        expect(product1).toContain(
          ';eVar12=3 for £8 Ankle Socks Promotion|eVar35=ONE|eVar36=BLUE'
        )
      })
    })

    describe('Events', () => {
      const events = orderCompleted(state).events.split(',')

      it('should include `events`: ``', () => {
        expect(events).toEqual([
          'event3',
          'event69',
          'purchase',
          'event58',
          'event66',
          'event71=1.00',
          'event7',
          'event11',
          'event5',
          'event67',
        ])
      })
    })
  })

  describe('Scenario 7. Marked Down Item(s) (eg. WAS/NOW Price)', () => {
    // journey: buy a Marked Down Item (Bonded Lace Wrap Midi)
    const state = orderCompletedMarkedDownItems

    describe('Products', () => {
      const product1 = orderCompleted(state)
        .products.split(',')
        .shift()

      it('should include `events`: `event58<Was Price>`,`event66=<Total Now Price>`', () => {
        expect(product1).toContain(';event58=180.00|event66=30.00;')
      })
    })

    describe('Events', () => {
      const events = orderCompleted(state).events.split(',')

      it('should include `events`: ``', () => {
        expect(events).toEqual([
          'event3',
          'event69',
          'purchase',
          'event58',
          'event66',
          'event71=1.00',
          'event7',
          'event11',
        ])
      })
    })
  })

  it('should set the member ID (`userTrackingId`) as `eVar6`', () => {
    const data = orderCompleted({
      ...state,
      account: {
        user: {
          userTrackingId: 1234567,
        },
      },
    })
    expect(data.eVar6).toBe(1234567)
  })

  it('should set the payment method as `eVar16`', () => {
    const data = orderCompleted(state)
    expect(data.eVar16).toBe('Visa')
  })

  it('should set the delivery type as `eVar17`', () => {
    const data = orderCompleted(state)
    expect(data.eVar17).toBe('UK Standard up to 4 days')
  })

  it('should set the total order cost as `eVar71`', () => {
    const data = orderCompleted(state)
    expect(data.eVar71).toBe('64.00')
  })
})
