import {
  singleProductStandardDeliveryOrder,
  standardStoreDeliveryOrder,
  expressStoreDeliveryOrder,
  orderWithDiscountCode,
  deliveryDiscountOrder,
  multiBuyDiscountOrder,
  markedDownItemOrder,
  customerRegistersDuringCheckout,
  orderFromFrance,
} from './mockdata/completed-orders'

import orderCompletedResolver from '../order-completed'

const expectEventStringToInclude = (
  expectedElements,
  delimitedString,
  delimeter
) => {
  const expected = expect.arrayContaining(expectedElements)
  const stringAsArray = delimitedString.split(delimeter)

  expect(stringAsArray).toEqual(expected)
  expect(stringAsArray).toHaveLength(expectedElements.length)
}

const expectCommonFields = (result, expectations) => {
  expect(result.pageName).toEqual('Order Confirmed')
  expect(result.eVar16).toEqual('Visa')
  expect(result.eVar17).toEqual(
    expectations.shippingOptions
      ? expectations.shippingOptions
      : 'UK Standard up to 4 days'
  )
  expect(result.currencyCode).toEqual(
    expectations.currencyCode ? expectations.currencyCode : 'GBP'
  )

  expectEventStringToInclude(expectations.events, result.events, ',')
}

const account = {
  user: {
    email: 'monty@test.com',
    userTrackingId: 2246675,
  },
}

const forms = {
  bundlesAddToBag: {},
  checkout: {
    account: {
      fields: {
        email: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        password: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        passwordConfirm: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        subscribe: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
      },
      isLoading: false,
      errors: {},
      message: {},
    },
    billingAddress: {
      fields: {
        address1: {
          value: '86 Tirrington',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        address2: {
          value: 'Bretton',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        city: {
          value: 'PETERBOROUGH',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        state: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        country: {
          value: 'United Kingdom',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        postcode: {
          value: 'PE3 9XT',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        county: {
          value: null,
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
      },
      isLoading: false,
      errors: {},
      message: {},
    },
    billingCardDetails: {
      fields: {
        paymentType: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        cardNumber: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        expiryMonth: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        expiryYear: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        startMonth: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        startYear: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        cvv: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
      },
      isLoading: false,
      errors: {},
      message: {},
    },
    billingDetails: {
      fields: {
        title: {
          value: 'Ms',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        firstName: {
          value: 'Jane',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        lastName: {
          value: 'Doe',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        telephone: {
          value: '01234567890',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
      },
      isLoading: false,
      errors: {},
      message: {},
    },
    billingFindAddress: {
      fields: {
        postCode: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        houseNumber: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        address: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        message: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        findAddress: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
      },
      isLoading: false,
      errors: {},
      message: {},
    },
    deliveryInstructions: {
      fields: {
        deliveryInstructions: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        smsMobileNumber: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
      },
      isLoading: false,
      errors: {},
      message: {},
    },
    findAddress: {
      fields: {
        postCode: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        houseNumber: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        address: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        message: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        country: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        findAddress: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
      },
      isLoading: false,
      errors: {},
      message: {},
    },
    order: {
      fields: {
        isAcceptedTermsAndConditions: {
          value: true,
          isDirty: true,
          isTouched: false,
          isFocused: false,
        },
      },
      isLoading: false,
      errors: {},
      message: {},
    },
    yourAddress: {
      fields: {
        address1: {
          value: '86 Tirrington',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        address2: {
          value: 'Bretton',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        city: {
          value: 'PETERBOROUGH',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        state: {
          value: '',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        country: {
          value: 'United Kingdom',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        postcode: {
          value: 'PE3 9XT',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        county: {
          value: null,
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
      },
      isLoading: false,
      errors: {},
      message: {},
    },
    yourDetails: {
      fields: {
        title: {
          value: 'Ms',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        firstName: {
          value: 'Jane',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        lastName: {
          value: 'Doe',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
        telephone: {
          value: '01234567890',
          isDirty: false,
          isTouched: false,
          isFocused: false,
        },
      },
      isLoading: false,
      errors: {},
      message: {},
    },
  },
  changePassword: {
    fields: {
      email: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      oldPassword: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      newPassword: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      newPasswordConfirm: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
    },
    isLoading: false,
    errors: {},
    message: {},
  },
  cmsForm: {
    fields: {},
    isLoading: false,
    errors: {},
    message: {},
  },
  customerDetails: {
    fields: {
      title: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      firstName: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      lastName: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      telephone: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      country: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      postcode: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      address1: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      address2: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      city: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      state: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      type: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      cardNumber: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      expiryMonth: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      expiryYear: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
    },
    isLoading: false,
    errors: {},
    message: {},
    modalOpen: false,
  },
  customerShortProfile: {
    fields: {
      title: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      firstName: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      lastName: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      email: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
    },
    isLoading: false,
    errors: {},
    message: {},
  },
  forgetPassword: {
    fields: {
      email: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
    },
    isLoading: false,
    errors: {},
    message: {},
  },
  login: {
    fields: {
      email: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      password: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
    },
    isLoading: false,
    errors: {},
    message: {},
  },
  notifyStock: {
    fields: {
      firstName: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      surname: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      email: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      state: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
    },
    isLoading: false,
    errors: {},
    message: {},
  },
  promotionCode: {
    fields: {
      promotionCode: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
    },
    isLoading: false,
    errors: {},
    message: {},
    isVisible: true,
  },
  register: {
    fields: {
      email: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      password: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      passwordConfirm: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      subscribe: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
    },
    isLoading: false,
    errors: {},
    message: {},
  },
  search: {
    fields: {
      searchTerm: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
    },
    isLoading: false,
    errors: {},
    message: {},
  },
  storeDelivery: {
    fields: {
      postcode: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
    },
    isLoading: false,
    errors: {},
    message: {},
  },
  userLocator: {
    fields: {
      userLocation: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
    },
    isLoading: false,
    errors: {},
    message: {},
  },
  giftCard: {
    fields: {
      giftCardNumber: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      pin: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
    },
    isLoading: false,
    errors: {},
    message: {},
    banner: false,
  },
  klarna: {
    fields: {
      email: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
    },
    isLoading: false,
    errors: {},
    message: {},
  },
}

describe('order with single product for standard free home delivery', () => {
  it('should have the correct analytics', () => {
    const result = orderCompletedResolver({
      account,
      forms,
      checkout: singleProductStandardDeliveryOrder(),
    })

    const expectedEvents = [
      'event3',
      'event58',
      'event66',
      'event69',
      'purchase',
      'event71=1.00',
      'event7',
      'event11',
    ]
    expectCommonFields(result, { events: expectedEvents })
    expect(result.products).toEqual(
      ';(19K15MBLK)Classic Beret Hat;1;12.00;event58=12.00|event66=12.00;eVar35=ONE|eVar36=BLACK,;UK Standard up to 4 days;;;event3=4.00|event69=4.00;'
    )
  })
})

describe('order with discount code applied', () => {
  it('should have the correct analytics', () => {
    const result = orderCompletedResolver({
      account,
      forms,
      checkout: orderWithDiscountCode(),
    })

    const expectedEvents = [
      'event3',
      'event58',
      'event66',
      'event69',
      'purchase',
      'event71=1.00',
      'event6',
      'event68',
      'event7',
      'event11',
    ]
    expectCommonFields(result, { events: expectedEvents })
    expect(result.products).toEqual(
      ';(19K15MPPK)Classic Beret;1;12.00;event58=12.00|event66=12.00|event6=5.00|event68=5.00;eVar35=ONE|eVar36=PALE PINK,;UK Standard up to 4 days;;;event3=4.00|event69=4.00;'
    )
    expect(result.eVar12).toEqual('£5 off')
  })
})

describe('order with standard store delivery', () => {
  it('should have the correct analytics', () => {
    const result = orderCompletedResolver({
      account,
      forms,
      checkout: standardStoreDeliveryOrder(),
    })

    const expectedEvents = [
      'event3',
      'event58',
      'event66',
      'event69',
      'purchase',
      'event71=1.00',
      'event7',
      'event11',
    ]
    expectCommonFields(result, {
      events: expectedEvents,
      shippingOptions: 'Collect From Store Standard',
    })
    expect(result.products).toEqual(
      ';(20L10LMVE)Lip Kit in Clearly Misunderstood;1;18.00;event58=18.00|event66=18.00;eVar35=ONE|eVar36=MAUVE,;Collect From Store Standard;;;event3=0.00|event69=0.00;'
    )
  })
})

describe('order with express store delivery', () => {
  it('should have the correct analytics', () => {
    const result = orderCompletedResolver({
      account,
      forms,
      checkout: expressStoreDeliveryOrder(),
    })

    const expectedEvents = [
      'event3',
      'event58',
      'event66',
      'event69',
      'purchase',
      'event71=1.00',
      'event7',
      'event11',
    ]
    expectCommonFields(result, {
      events: expectedEvents,
      shippingOptions: 'Collect From Store Express',
    })
    expect(result.products).toEqual(
      ';(19K15MPPK)Classic Beret;1;12.00;event58=12.00|event66=12.00;eVar35=ONE|eVar36=PALE PINK,;Collect From Store Express;;;event3=3.00|event69=3.00;'
    )
  })
})

// todo: 'event59' - no flag to detect delivery discount
xdescribe('order with delivery discount', () => {
  it('should have the correct analytics', () => {
    const result = orderCompletedResolver({
      account,
      forms,
      checkout: deliveryDiscountOrder(),
    })

    const expectedEvents = [
      'event3',
      'event58',
      'event66',
      'event69',
      'purchase',
      'event71=1.00',
      'event7',
      'event11',
    ]
    expectCommonFields(result, { events: expectedEvents })
    expect(result.products).toEqual(
      ';(11P15MBLK)Collarless Biker Jacket;1;49.00;event58=49.00|event66=49.00;eVar35=8|eVar36=BLACK,;(42H21MBLE)HERRING Embellished Goldfish Sliders;1;26.00;event58=26.00|event66=26.00;eVar35=37|eVar36=BURGUNDY;;(29T33KBLK)Logo Boyfriend T-Shirt by Ivy Park;1;18.00;event58=18.00|event66=18.00;eVar35=XL|eVar36=LIGHT GREY M;;UK Standard up to 4 days;;;event3=0.00|event69=0.00;'
    )
  })
})

describe('order with multi-buy discount', () => {
  it('should have the correct analytics', () => {
    const result = orderCompletedResolver({
      account,
      forms,
      checkout: multiBuyDiscountOrder(),
    })

    const expectedEvents = [
      'event3',
      'event58',
      'event66',
      'event69',
      'purchase',
      'event71=1.00',
      'event5',
      'event67',
      'event7',
      'event11',
    ]
    expectCommonFields(result, { events: expectedEvents })
    expect(result.products).toEqual(
      ';(08C09LGRY)Grey Sporty Tube Ankle Socks;1;3.50;event58=3.50|event66=3.50|event5=0.84|event67=0.84;eVar35=ONE|eVar36=GREY,;(08L14LBGD)Cropped Rib Glitter Ankle Socks;1;3.50;event58=3.50|event66=3.50|event5=0.83|event67=0.83;eVar35=ONE|eVar36=Nude,;(08C06LOGE)Orange Sporty Tubes Ankle Socks;1;3.50;event58=3.50|event66=3.50|event5=0.83|event67=0.83;eVar35=ONE|eVar36=ORANGE,;UK Standard up to 4 days;;;event3=4.00|event69=4.00;'
    )
    expect(result.eVar12).toEqual('3 for £8 Ankle Socks Promotion')
  })
})

describe('order with marked down item', () => {
  it('should have the correct analytics', () => {
    const result = orderCompletedResolver({
      account,
      forms,
      checkout: markedDownItemOrder(),
    })

    const expectedEvents = [
      'event3',
      'event58',
      'event66',
      'event69',
      'purchase',
      'event71=1.00',
      'event7',
      'event11',
    ]
    expectCommonFields(result, { events: expectedEvents })
    expect(result.products).toEqual(
      ';(24W09MPNK)Tapestry Tote Bag;1;15.00;event58=45.00|event66=15.00;eVar35=ONE|eVar36=PINK,;UK Standard up to 4 days;;;event3=4.00|event69=4.00;'
    )
  })
})

describe('order where customer registers during checkout', () => {
  it('should have the correct analytics', () => {
    const result = orderCompletedResolver(customerRegistersDuringCheckout())

    const expectedEvents = [
      'event3',
      'event58',
      'event66',
      'event69',
      'purchase',
      'event71=1.00',
      'event7',
      'event9',
      'event11',
    ]
    expectCommonFields(result, { events: expectedEvents })
    expect(result.products).toEqual(
      ';(19K15MBLK)Classic Beret Hat;1;12.00;event58=12.00|event66=12.00;eVar35=ONE|eVar36=BLACK,;UK Standard up to 4 days;;;event3=4.00|event69=4.00;'
    )
  })
})

describe('order from France', () => {
  it('should have the correct analytics', () => {
    const result = orderCompletedResolver({
      account,
      forms,
      checkout: orderFromFrance(),
    })

    const expectedEvents = [
      'event3',
      'event58',
      'event66',
      'event69',
      'purchase',
      'event71=1.2;',
      'event7',
      'event11',
    ]
    expectCommonFields(result, {
      events: expectedEvents,
      currencyCode: 'EUR',
      shippingOptions: 'Livraison standard',
    })
    expect(result.products).toEqual(
      ';(19E03MBLK)Bonnet côtelé avec pompon arc-en-ciel;1;20.00;event58=20.00|event66=20.00;eVar35=ONE|eVar36=NOIR,;Livraison standard;;;event3=5.00|event69=5.00;'
    )
  })
})
