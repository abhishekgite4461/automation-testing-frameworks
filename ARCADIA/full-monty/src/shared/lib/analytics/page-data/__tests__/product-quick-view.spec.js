import bundleMockData from '../../../../../../test/mocks/bundleMocks'
import quickViewProductWithReviewsState from '../../../../../../test/mocks/quickview/product-with-reviews'
import quickViewProductWithoutReviewsState from '../../../../../../test/mocks/quickview/product-no-reviews'

import productQuickViewAnalyticsDataResolver from '../product-quick-view'

describe('productQuickViewAnalyticsDataResolver', () => {
  const notAddedToBagShoppingBagState = {}

  describe('@pageView', () => {
    describe('@product', () => {
      it('w/reviews', () => {
        const actual = productQuickViewAnalyticsDataResolver({
          quickview: quickViewProductWithReviewsState,
          shoppingBag: notAddedToBagShoppingBagState,
        })
        expect(actual).toMatchObject({
          pageName: 'Product Quickview',
          firstCategory: 'Product Quickview',
          granularPageName:
            'Prod QView:(12X34LPNK)3-Stripe Crew Sweatshirt by adidas Originals',
          eVar13:
            'Prod QView:(12X34LPNK)3-Stripe Crew Sweatshirt by adidas Originals',
          events: 'prodView,event2,event60,event108',
          products:
            ';(12X34LPNK)3-Stripe Crew Sweatshirt by adidas Originals;;;event60=1.00;eVar42=5.0|eVar60=6|eVar61=6|eVar62=100|eVar68=45.00|eVar69=0|eVar78=45.00',
        })
      })
      it('wo/reviews', () => {
        const actual = productQuickViewAnalyticsDataResolver({
          quickview: quickViewProductWithoutReviewsState,
          shoppingBag: notAddedToBagShoppingBagState,
        })
        expect(actual).toMatchObject({
          pageName: 'Product Quickview',
          firstCategory: 'Product Quickview',
          granularPageName:
            'Prod QView:(12X39LMUL)Butterfly Print Trefoil Playsuit by adidas Originals',
          eVar13:
            'Prod QView:(12X39LMUL)Butterfly Print Trefoil Playsuit by adidas Originals',
          events: 'prodView,event2,event60',
          products:
            ';(12X39LMUL)Butterfly Print Trefoil Playsuit by adidas Originals;;;event60=1.00;eVar42=null|eVar60=6|eVar61=6|eVar62=100|eVar68=48.00|eVar69=0|eVar78=48.00',
        })
      })
    })

    describe('@bundles', () => {
      it('wo/reviews', () => {
        const actual = productQuickViewAnalyticsDataResolver({
          quickview: { product: bundleMockData },
          shoppingBag: notAddedToBagShoppingBagState,
        })
        expect(actual).toMatchObject({
          pageName: 'Product Quickview',
          firstCategory: 'Product Quickview',
          granularPageName:
            'Prod QView:(BUNDLE_SS16_02T01GBLK_3)3 Piece Black Essential Tailored Fit Suit',
          eVar13:
            'Prod QView:(BUNDLE_SS16_02T01GBLK_3)3 Piece Black Essential Tailored Fit Suit',
          events: 'prodView,event2,event60',
          products:
            ';(BUNDLE_SS16_02T01GBLK_3)3 Piece Black Essential Tailored Fit Suit;;;event60=1.00;eVar42=null|eVar60=49|eVar61=49|eVar62=100|eVar68=107.00|eVar69=0|eVar78=107.00',
        })
      })
    })
  })
})
