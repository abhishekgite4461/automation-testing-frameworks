import getMrCMSPageData from '../mr-cms'

describe('MR CMS Page Data', () => {
  const state = {
    sandbox: {
      pages: {
        '/e-receipt': {
          props: {
            data: {
              pageName: 'E Receipt',
            },
          },
        },
      },
    },
    routing: {
      location: {
        pathname: '/e-receipt',
      },
    },
  }

  it('should get data from sandbox pages, based on pathname', () => {
    expect(getMrCMSPageData(state)).toEqual(
      expect.objectContaining({
        firstCategory: 'E Receipt',
        granularPageName: 'E Receipt',
        pageName: 'E Receipt',
      })
    )
  })

  it('should handle pages which are URI encoded', () => {
    const uriEncodedState = {
      sandbox: {
        pages: {
          '%2Fe-receipt': {
            props: {
              data: {
                pageName: 'E Receipt',
              },
            },
          },
        },
      },
      routing: {
        location: {
          pathname: '/e-receipt',
        },
      },
    }

    expect(getMrCMSPageData(uriEncodedState).pageName).toBe('E Receipt')
  })

  it('should return a `pageType` of `Content`', () => {
    expect(getMrCMSPageData(state).pageType).toBe('Content')
  })

  it('should remove `mob - ` prefix from the `pageName`', () => {
    const prefixedState = {
      sandbox: {
        pages: {
          '/e-receipt': {
            props: {
              data: {
                pageName: 'mob - E Receipt',
              },
            },
          },
        },
      },
      routing: {
        location: {
          pathname: '/e-receipt',
        },
      },
    }

    expect(getMrCMSPageData(prefixedState).pageName).toBe('E Receipt')
  })

  it('should return undefined if it can’t find the page', () => {
    const badState = {
      sandbox: {
        pages: {
          '/e-receipt': {
            props: {
              data: {
                pageName: 'E Receipt',
              },
            },
          },
        },
      },
      routing: {
        location: {
          pathname: '/privacy-and-cookies',
        },
      },
    }

    expect(getMrCMSPageData(badState)).toBeUndefined()
  })

  it('should return pageName if page is termsAndConditions', () => {
    const stateWithNotFound = {
      sandbox: {
        pages: {
          termsAndConditions: {
            props: {
              data: {
                pageName: 'T&C',
              },
            },
          },
        },
      },
      routing: {
        location: {
          pathname: '/helpandsomething/tandc-25141',
        },
      },
    }

    expect(getMrCMSPageData(stateWithNotFound).pageName).toBe('T&C')
  })

  it('should return pageName if page is notFound', () => {
    const stateWithNotFound = {
      sandbox: {
        pages: {
          notFound: {
            props: {
              data: {
                pageName: '404 - Not found',
              },
            },
          },
        },
      },
      routing: {
        location: {
          pathname: '/somerandomurl',
        },
      },
    }

    expect(getMrCMSPageData(stateWithNotFound).pageName).toBe('404 - Not found')
  })

  it('should return pageName if page is size-guide', () => {
    const stateWithSizeGuide = {
      sandbox: {
        pages: {
          IvyPark: {
            props: {
              data: {
                pageName: 'Size guide - IvyPark',
              },
            },
          },
        },
      },
      routing: {
        location: {
          pathname: '/size-guide/IvyPark',
        },
      },
    }

    expect(getMrCMSPageData(stateWithSizeGuide).pageName).toBe(
      'Size guide - IvyPark'
    )
  })
})
