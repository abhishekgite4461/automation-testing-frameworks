export function singleProductStandardDeliveryOrder() {
  return {
    orderCompleted: {
      orderId: 1815780,
      subTotal: '',
      returnPossible: false,
      returnRequested: false,
      deliveryMethod: 'UK Standard up to 4 days',
      deliveryDate: 'Wednesday 20 September 2017',
      deliveryCost: '4.00',
      deliveryCarrier: 'Parcelnet',
      deliveryPrice: '4.00',
      totalOrderPrice: '16.00',
      totalOrdersDiscountLabel: '',
      totalOrdersDiscount: '',
      billingAddress: {
        name: 'Ms Jane Doe',
        address1: '86 Tirrington',
        address2: 'Bretton',
        address3: 'PETERBOROUGH',
        address4: 'PE3 9XT',
        country: 'United Kingdom',
      },
      deliveryAddress: {
        name: 'Ms Jane Doe',
        address1: '86 Tirrington',
        address2: 'Bretton',
        address3: 'PETERBOROUGH',
        address4: 'PE3 9XT',
        country: 'United Kingdom',
      },
      orderLines: [
        {
          lineNo: '19K15MBLK',
          name: 'Classic Beret Hat',
          size: 'ONE',
          colour: 'BLACK',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Small_F_1.jpg',
          quantity: 1,
          unitPrice: '12.00',
          discount: '',
          total: '12.00',
          nonRefundable: false,
        },
      ],
      paymentDetails: [
        {
          paymentMethod: 'Visa',
          cardNumberStar: '************4444',
          totalCost: '£16.00',
        },
      ],
      currencyConversion: {
        currencyRate: 'GBP',
      },
    },
    orderSummary: {
      basket: {
        orderId: 1815780,
        subTotal: '12.00',
        total: '16.00',
        totalBeforeDiscount: '12.00',
        deliveryOptions: [
          {
            deliveryOptionId: 47524,
            label: 'Collect from ParcelShop £4.00',
            selected: false,
          },
          {
            deliveryOptionId: 45020,
            label: 'Collect From Store Express £3.00',
            selected: false,
          },
          {
            deliveryOptionId: 28005,
            label: 'Express / Nominated Day Delivery £6.00',
            selected: false,
          },
          {
            deliveryOptionId: 45019,
            label: 'Free Collect From Store Standard £0.00',
            selected: false,
          },
          {
            deliveryOptionId: 26504,
            label: 'UK Standard up to 4 days £4.00',
            selected: true,
          },
        ],
        promotions: [],
        discounts: [],
        products: [
          {
            productId: 29370388,
            catEntryId: 29370437,
            orderItemId: 8225124,
            shipModeId: 26504,
            lineNumber: '19K15MBLK',
            size: 'ONE',
            name: 'Classic Beret Hat',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '12.00',
            totalPrice: '12.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
        savedProducts: [],
        ageVerificationRequired: false,
        restrictedDeliveryItem: false,
        inventoryPositions: {
          item_1: {
            partNumber: '602017001141910',
            catentryId: '29370437',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 9,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
        },
      },
      deliveryLocations: [
        {
          deliveryLocationType: 'HOME',
          label:
            'Home Delivery Standard (UK up to 4 days; worldwide varies)  Express (UK next or nominated day; worldwide varies)',
          selected: true,
          deliveryMethods: [
            {
              shipModeId: 26504,
              shipCode: 'S',
              deliveryType: 'HOME_STANDARD',
              label: 'UK Standard up to 4 days',
              additionalDescription: 'Up to 4 days',
              cost: '4.00',
              selected: true,
              deliveryOptions: [],
            },
            {
              deliveryType: 'HOME_EXPRESS',
              label: 'Express / Nominated day delivery',
              additionalDescription: '',
              selected: false,
              deliveryOptions: [
                {
                  shipModeId: 28005,
                  dayText: 'Thu',
                  dateText: '14 Sep',
                  nominatedDate: '2017-09-14',
                  price: '6.00',
                  selected: true,
                },
                {
                  shipModeId: 28006,
                  dayText: 'Fri',
                  dateText: '15 Sep',
                  nominatedDate: '2017-09-15',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28007,
                  dayText: 'Sat',
                  dateText: '16 Sep',
                  nominatedDate: '2017-09-16',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28002,
                  dayText: 'Mon',
                  dateText: '18 Sep',
                  nominatedDate: '2017-09-18',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28003,
                  dayText: 'Tue',
                  dateText: '19 Sep',
                  nominatedDate: '2017-09-19',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28004,
                  dayText: 'Wed',
                  dateText: '20 Sep',
                  nominatedDate: '2017-09-20',
                  price: '6.00',
                  selected: false,
                },
              ],
            },
          ],
        },
        {
          deliveryLocationType: 'STORE',
          label:
            'Collect from Store Standard (3-7 working days) Express (next day)',
          selected: false,
          deliveryMethods: [],
        },
      ],
      giftCards: [],
      creditCard: {
        type: 'VISA',
        cardNumberStar: '************4444',
      },
      cardNumberHash: 'qdVaNzj/8PFhXEw3GAWQ23mrDgSc058p',
      deliveryInstructions: '',
      smsMobileNumber: '',
      shippingCountry: 'United Kingdom',
      billingDetails: {
        nameAndPhone: {
          title: 'Ms',
          firstName: 'Jane',
          lastName: 'Doe',
          telephone: '',
        },
        address: {
          address1: '86 Tirrington',
          address2: 'Bretton',
          city: 'PETERBOROUGH',
          state: '',
          country: 'United Kingdom',
          postcode: 'PE3 9XT',
        },
      },
      deliveryDetails: {
        addressDetailsId: 2218366,
        nameAndPhone: {
          title: 'Ms',
          firstName: 'Jane',
          lastName: 'Doe',
          telephone: '',
        },
        address: {
          address1: '86 Tirrington',
          address2: 'Bretton',
          city: 'PETERBOROUGH',
          state: '',
          country: 'United Kingdom',
          postcode: 'PE3 9XT',
        },
      },
      savedAddresses: [],
      ageVerificationDeliveryConfirmationRequired: false,
      estimatedDelivery: ['No later than Tuesday 19 September 2017'],
      version: '1.7',
    },
    orderSummaryError: {},
    orderError: false,
    isDeliverySameAsBilling: true,
    useDeliveryAsBilling: true,
    verifyPayment: false,
    showExpressDeliveryOptionsOnSummary: false,
    editingDetails: false,
    storeUpdating: false,
    partialOrderSummary: {
      basket: {
        orderId: 1815780,
        subTotal: '12.00',
        total: '16.00',
        totalBeforeDiscount: '12.00',
        deliveryOptions: [
          {
            deliveryOptionId: 47524,
            label: 'Collect from ParcelShop £4.00',
            selected: false,
          },
          {
            deliveryOptionId: 45020,
            label: 'Collect From Store Express £3.00',
            selected: false,
          },
          {
            deliveryOptionId: 28005,
            label: 'Express / Nominated Day Delivery £6.00',
            selected: false,
          },
          {
            deliveryOptionId: 45019,
            label: 'Free Collect From Store Standard £0.00',
            selected: false,
          },
          {
            deliveryOptionId: 26504,
            label: 'UK Standard up to 4 days £4.00',
            selected: true,
          },
        ],
        promotions: [],
        discounts: [],
        products: [
          {
            productId: 29370388,
            catEntryId: 29370437,
            orderItemId: 8225124,
            shipModeId: 26504,
            lineNumber: '19K15MBLK',
            size: 'ONE',
            name: 'Classic Beret Hat',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '12.00',
            totalPrice: '12.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
        savedProducts: [],
        ageVerificationRequired: false,
        restrictedDeliveryItem: false,
        inventoryPositions: {
          item_1: {
            partNumber: '602017001141910',
            catentryId: '29370437',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 9,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
        },
      },
    },
  }
}

export function standardStoreDeliveryOrder() {
  return {
    orderCompleted: {
      orderId: 1813387,
      subTotal: '',
      returnPossible: false,
      returnRequested: false,
      deliveryMethod: 'Collect From Store Standard',
      deliveryDate: '',
      deliveryCost: '',
      deliveryCarrier: 'Retail Store Standard',
      deliveryPrice: '0.00',
      totalOrderPrice: '18.00',
      totalOrdersDiscountLabel: '',
      totalOrdersDiscount: '',
      billingAddress: {
        name: 'Ms Jane Doe',
        address1: '86 Tirrington',
        address2: 'Bretton',
        address3: 'PETERBOROUGH',
        address4: 'PE3 9XT',
        country: 'United Kingdom',
      },
      deliveryAddress: {
        name: 'Ms Jane Doe',
        address1: 'Topshop - Peterborough',
        address2: '21/23 Queensgate Centre',
        address3: 'Peterborough',
        address4: 'PE1 1NH',
        country: 'United Kingdom',
      },
      orderLines: [
        {
          lineNo: '20L10LMVE',
          name: 'Lip Kit in Clearly Misunderstood',
          size: 'ONE',
          colour: 'MAUVE',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS20L10LMVE_Small_F_1.jpg',
          quantity: 1,
          unitPrice: '18.00',
          discount: '',
          total: '18.00',
          nonRefundable: false,
        },
      ],
      paymentDetails: [
        {
          paymentMethod: 'Visa',
          cardNumberStar: '************4444',
          totalCost: '£18.00',
        },
      ],
      currencyConversion: {
        currencyRate: 'GBP',
      },
    },
    orderSummary: {
      basket: {
        orderId: 1813387,
        subTotal: '18.00',
        total: '18.00',
        totalBeforeDiscount: '18.00',
        deliveryOptions: [
          {
            deliveryOptionId: 28004,
            label: 'Express / Nominated Day Delivery £6.00',
            selected: false,
          },
          {
            deliveryOptionId: 26504,
            label: 'UK Standard up to 4 days £4.00',
            selected: false,
          },
          {
            deliveryOptionId: 45019,
            label: 'Free Collect From Store Standard £0.00',
            selected: true,
          },
          {
            deliveryOptionId: 47524,
            label: 'Collect from ParcelShop £4.00',
            selected: false,
          },
          {
            deliveryOptionId: 45020,
            label: 'Collect From Store Express £3.00',
            selected: false,
          },
        ],
        promotions: [],
        discounts: [],
        products: [
          {
            productId: 27802219,
            catEntryId: 27802224,
            orderItemId: 8222361,
            shipModeId: 26504,
            lineNumber: '20L10LMVE',
            size: 'ONE',
            name: 'Lip Kit in Clearly Misunderstood',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '18.00',
            totalPrice: '18.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS20L10LMVE_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS20L10LMVE_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS20L10LMVE_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS20L10LMVE_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
        savedProducts: [],
        ageVerificationRequired: false,
        restrictedDeliveryItem: false,
        inventoryPositions: {
          item_1: {
            partNumber: '602017001076273',
            catentryId: '27802224',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 7,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
        },
      },
      deliveryLocations: [
        {
          deliveryLocationType: 'HOME',
          label:
            'Home Delivery Standard (UK up to 4 days; worldwide varies)  Express (UK next or nominated day; worldwide varies)',
          selected: false,
          deliveryMethods: [],
        },
        {
          deliveryLocationType: 'STORE',
          label:
            'Collect from Store Standard (3-7 working days) Express (next day)',
          selected: true,
          deliveryMethods: [
            {
              shipModeId: 45020,
              deliveryType: 'STORE_EXPRESS',
              label: 'Collect From Store Express',
              additionalDescription:
                'Collection date Wednesday 13 September 2017',
              cost: '3.00',
              selected: false,
              deliveryOptions: [],
            },
            {
              shipModeId: 45019,
              shipCode: 'Retail Store Standard',
              deliveryType: 'STORE_STANDARD',
              label: 'Collect From Store Standard',
              additionalDescription:
                'Collection date Saturday 16 September 2017',
              selected: true,
              deliveryOptions: [],
            },
          ],
        },
      ],
      deliveryStoreCode: 'TS0176',
      giftCards: [],
      creditCard: {
        type: 'VISA',
        cardNumberStar: '************4444',
      },
      cardNumberHash: 'qdVaNzj/8PFhXEw3GAWQ23mrDgSc058p',
      deliveryInstructions: '',
      smsMobileNumber: '',
      shippingCountry: 'United Kingdom',
      billingDetails: {
        nameAndPhone: {
          title: 'Ms',
          firstName: 'Jane',
          lastName: 'Doe',
          telephone: '',
        },
        address: {
          address1: '86 Tirrington',
          address2: 'Bretton',
          city: 'PETERBOROUGH',
          state: '',
          country: 'United Kingdom',
          postcode: 'PE3 9XT',
        },
      },
      deliveryDetails: {
        nameAndPhone: {
          title: 'Ms',
          firstName: 'Jane',
          lastName: 'Doe',
          telephone: '',
        },
        address: {
          address1: 'Topshop - Peterborough',
          address2: '',
          city: '21/23 Queensgate Centre',
          state: 'Peterborough',
          country: 'United Kingdom',
          postcode: 'PE1 1NH',
        },
      },
      storeDetails: {
        address1: '21/23 Queensgate Centre',
        address2: '',
        city: 'Peterborough',
        country: 'United Kingdom',
        postcode: 'PE1 1NH',
      },
      savedAddresses: [],
      ageVerificationDeliveryConfirmationRequired: false,
      estimatedDelivery: ['No later than Saturday 16 September 2017'],
      version: '1.7',
    },
    orderSummaryError: {},
    orderError: false,
    isDeliverySameAsBilling: true,
    useDeliveryAsBilling: true,
    verifyPayment: false,
    showExpressDeliveryOptionsOnSummary: false,
    editingDetails: false,
    storeUpdating: false,
    partialOrderSummary: {
      basket: {
        orderId: 1813387,
        subTotal: '18.00',
        total: '18.00',
        totalBeforeDiscount: '18.00',
        deliveryOptions: [
          {
            deliveryOptionId: 28004,
            label: 'Express / Nominated Day Delivery £6.00',
            selected: false,
          },
          {
            deliveryOptionId: 26504,
            label: 'UK Standard up to 4 days £4.00',
            selected: false,
          },
          {
            deliveryOptionId: 45019,
            label: 'Free Collect From Store Standard £0.00',
            selected: true,
          },
          {
            deliveryOptionId: 47524,
            label: 'Collect from ParcelShop £4.00',
            selected: false,
          },
          {
            deliveryOptionId: 45020,
            label: 'Collect From Store Express £3.00',
            selected: false,
          },
        ],
        promotions: [],
        discounts: [],
        products: [
          {
            productId: 27802219,
            catEntryId: 27802224,
            orderItemId: 8222361,
            shipModeId: 26504,
            lineNumber: '20L10LMVE',
            size: 'ONE',
            name: 'Lip Kit in Clearly Misunderstood',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '18.00',
            totalPrice: '18.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS20L10LMVE_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS20L10LMVE_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS20L10LMVE_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS20L10LMVE_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
        savedProducts: [],
        ageVerificationRequired: false,
        restrictedDeliveryItem: false,
        inventoryPositions: {
          item_1: {
            partNumber: '602017001076273',
            catentryId: '27802224',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 7,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
        },
      },
      deliveryStoreCode: 'TS0176',
    },
    deliveryStore: {
      deliveryStoreCode: 'TS0176',
      storeAddress1: '21/23 Queensgate Centre',
      storeAddress2: '',
      storeCity: 'Peterborough',
      storePostcode: 'PE1 1NH',
    },
  }
}

export function expressStoreDeliveryOrder() {
  return {
    orderCompleted: {
      orderId: 1813512,
      subTotal: '',
      returnPossible: false,
      returnRequested: false,
      deliveryMethod: 'Collect From Store Express',
      deliveryDate: '',
      deliveryCost: '3.00',
      deliveryCarrier: 'Retail Store Express',
      deliveryPrice: '3.00',
      totalOrderPrice: '15.00',
      totalOrdersDiscountLabel: '',
      totalOrdersDiscount: '',
      billingAddress: {
        name: 'Ms Jane Doe',
        address1: '86 Tirrington',
        address2: 'Bretton',
        address3: 'PETERBOROUGH',
        address4: 'PE3 9XT',
        country: 'United Kingdom',
      },
      deliveryAddress: {
        name: 'Ms Jane Doe',
        address1: 'OUTFIT - Peterborough Serp Outfit',
        address2: 'c/o Outfit, Unit 21',
        address3: 'Serpentine Green',
        address4: 'Peterborough',
        address5: 'PE7 8BE',
        country: 'United Kingdom',
      },
      orderLines: [
        {
          lineNo: '19K15MPPK',
          name: 'Classic Beret',
          size: 'ONE',
          colour: 'PALE PINK',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Small_F_1.jpg',
          quantity: 1,
          unitPrice: '12.00',
          discount: '',
          total: '12.00',
          nonRefundable: false,
        },
      ],
      paymentDetails: [
        {
          paymentMethod: 'Visa',
          cardNumberStar: '************4444',
          totalCost: '£15.00',
        },
      ],
      currencyConversion: {
        currencyRate: 'GBP',
      },
    },
    orderSummary: {
      basket: {
        orderId: 1813512,
        subTotal: '12.00',
        total: '15.00',
        totalBeforeDiscount: '12.00',
        deliveryOptions: [
          {
            deliveryOptionId: 26504,
            label: 'UK Standard up to 4 days £4.00',
            selected: false,
          },
          {
            deliveryOptionId: 45020,
            label: 'Collect From Store Express £3.00',
            selected: true,
          },
          {
            deliveryOptionId: 45019,
            label: 'Free Collect From Store Standard £0.00',
            selected: false,
          },
          {
            deliveryOptionId: 47524,
            label: 'Collect from ParcelShop £4.00',
            selected: false,
          },
          {
            deliveryOptionId: 28004,
            label: 'Express / Nominated Day Delivery £6.00',
            selected: false,
          },
        ],
        promotions: [],
        discounts: [],
        products: [
          {
            productId: 29370510,
            catEntryId: 29370546,
            orderItemId: 8222492,
            shipModeId: 26504,
            lineNumber: '19K15MPPK',
            size: 'ONE',
            name: 'Classic Beret',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '12.00',
            totalPrice: '12.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
        savedProducts: [],
        ageVerificationRequired: false,
        restrictedDeliveryItem: false,
        inventoryPositions: {
          item_1: {
            partNumber: '602017001141912',
            catentryId: '29370546',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 9,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
        },
      },
      deliveryLocations: [
        {
          deliveryLocationType: 'HOME',
          label:
            'Home Delivery Standard (UK up to 4 days; worldwide varies)  Express (UK next or nominated day; worldwide varies)',
          selected: false,
          deliveryMethods: [],
        },
        {
          deliveryLocationType: 'STORE',
          label:
            'Collect from Store Standard (3-7 working days) Express (next day)',
          selected: true,
          deliveryMethods: [
            {
              shipModeId: 45020,
              shipCode: 'Retail Store Express',
              deliveryType: 'STORE_EXPRESS',
              label: 'Collect From Store Express',
              additionalDescription:
                'Collection date Wednesday 13 September 2017',
              cost: '3.00',
              selected: true,
              deliveryOptions: [],
            },
            {
              shipModeId: 45019,
              deliveryType: 'STORE_STANDARD',
              label: 'Collect From Store Standard',
              additionalDescription:
                'Collection date Saturday 16 September 2017',
              selected: false,
              deliveryOptions: [],
            },
          ],
        },
      ],
      deliveryStoreCode: 'OU0110',
      giftCards: [],
      creditCard: {
        type: 'VISA',
        cardNumberStar: '************4444',
      },
      cardNumberHash: 'qdVaNzj/8PFhXEw3GAWQ23mrDgSc058p',
      deliveryInstructions: '',
      smsMobileNumber: '',
      shippingCountry: 'United Kingdom',
      billingDetails: {
        nameAndPhone: {
          title: 'Ms',
          firstName: 'Jane',
          lastName: 'Doe',
          telephone: '',
        },
        address: {
          address1: '86 Tirrington',
          address2: 'Bretton',
          city: 'PETERBOROUGH',
          state: '',
          country: 'United Kingdom',
          postcode: 'PE3 9XT',
        },
      },
      deliveryDetails: {
        nameAndPhone: {
          title: 'Ms',
          firstName: 'Jane',
          lastName: 'Doe',
          telephone: '',
        },
        address: {
          address1: 'OUTFIT - Peterborough Serp Outfit',
          address2: 'c/o Outfit, Unit 21',
          city: 'Serpentine Green',
          state: 'Peterborough',
          country: 'United Kingdom',
          postcode: 'PE7 8BE',
        },
      },
      storeDetails: {
        address1: 'c/o Outfit, Unit 21',
        address2: 'Serpentine Green',
        city: 'Peterborough',
        country: 'United Kingdom',
        postcode: 'PE7 8BE',
      },
      savedAddresses: [],
      ageVerificationDeliveryConfirmationRequired: false,
      estimatedDelivery: ['No later than Wednesday 13 September 2017'],
      version: '1.7',
    },
    orderSummaryError: {},
    orderError: false,
    isDeliverySameAsBilling: true,
    useDeliveryAsBilling: true,
    verifyPayment: false,
    showExpressDeliveryOptionsOnSummary: false,
    editingDetails: false,
    storeUpdating: false,
    partialOrderSummary: {
      basket: {
        orderId: 1813512,
        subTotal: '12.00',
        total: '15.00',
        totalBeforeDiscount: '12.00',
        deliveryOptions: [
          {
            deliveryOptionId: 26504,
            label: 'UK Standard up to 4 days £4.00',
            selected: false,
          },
          {
            deliveryOptionId: 45020,
            label: 'Collect From Store Express £3.00',
            selected: true,
          },
          {
            deliveryOptionId: 45019,
            label: 'Free Collect From Store Standard £0.00',
            selected: false,
          },
          {
            deliveryOptionId: 47524,
            label: 'Collect from ParcelShop £4.00',
            selected: false,
          },
          {
            deliveryOptionId: 28004,
            label: 'Express / Nominated Day Delivery £6.00',
            selected: false,
          },
        ],
        promotions: [],
        discounts: [],
        products: [
          {
            productId: 29370510,
            catEntryId: 29370546,
            orderItemId: 8222492,
            shipModeId: 26504,
            lineNumber: '19K15MPPK',
            size: 'ONE',
            name: 'Classic Beret',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '12.00',
            totalPrice: '12.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
        savedProducts: [],
        ageVerificationRequired: false,
        restrictedDeliveryItem: false,
        inventoryPositions: {
          item_1: {
            partNumber: '602017001141912',
            catentryId: '29370546',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 9,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
        },
      },
      deliveryStoreCode: 'OU0110',
    },
    deliveryStore: {
      deliveryStoreCode: 'OU0110',
      storeAddress1: 'c/o Outfit, Unit 21',
      storeAddress2: 'Serpentine Green',
      storeCity: 'Peterborough',
      storePostcode: 'PE7 8BE',
    },
  }
}

export function orderWithDiscountCode() {
  return {
    orderCompleted: {
      orderId: 1813796,
      subTotal: '12.00',
      returnPossible: false,
      returnRequested: false,
      deliveryMethod: 'UK Standard up to 4 days',
      deliveryDate: 'Tuesday 19 September 2017',
      deliveryCost: '4.00',
      deliveryCarrier: 'Parcelnet',
      deliveryPrice: '4.00',
      totalOrderPrice: '11.00',
      totalOrdersDiscountLabel: '£5 off',
      totalOrdersDiscount: '-£5.00',
      billingAddress: {
        name: 'Ms Jane Doe',
        address1: '86 Tirrington',
        address2: 'Bretton',
        address3: 'PETERBOROUGH',
        address4: 'PE3 9XT',
        country: 'United Kingdom',
      },
      deliveryAddress: {
        name: 'Ms Jane Doe',
        address1: '86 Tirrington',
        address2: 'Bretton',
        address3: 'PETERBOROUGH',
        address4: 'PE3 9XT',
        country: 'United Kingdom',
      },
      orderLines: [
        {
          lineNo: '19K15MPPK',
          name: 'Classic Beret',
          size: 'ONE',
          colour: 'PALE PINK',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Small_F_1.jpg',
          quantity: 1,
          unitPrice: '12.00',
          discount: '',
          total: '12.00',
          nonRefundable: false,
        },
      ],
      paymentDetails: [
        {
          paymentMethod: 'Visa',
          cardNumberStar: '************4444',
          totalCost: '£11.00',
        },
      ],
      currencyConversion: {
        currencyRate: 'GBP',
      },
    },
    orderSummary: {
      basket: {
        orderId: 1813796,
        subTotal: '12.00',
        total: '11.00',
        totalBeforeDiscount: '12.00',
        deliveryOptions: [
          {
            deliveryOptionId: 47524,
            label: 'Collect from ParcelShop £4.00',
            selected: false,
          },
          {
            deliveryOptionId: 26504,
            label: 'UK Standard up to 4 days £4.00',
            selected: true,
          },
          {
            deliveryOptionId: 45020,
            label: 'Collect From Store Express £3.00',
            selected: false,
          },
          {
            deliveryOptionId: 28004,
            label: 'Express / Nominated Day Delivery £6.00',
            selected: false,
          },
          {
            deliveryOptionId: 45019,
            label: 'Free Collect From Store Standard £0.00',
            selected: false,
          },
        ],
        promotions: [
          {
            promotionCode: 'TESTUK5OFF',
            label: '£5 off',
          },
        ],
        discounts: [
          {
            label: '£5 off',
            value: '5.00',
          },
        ],
        products: [
          {
            productId: 29370510,
            catEntryId: 29370546,
            orderItemId: 8222798,
            shipModeId: 26504,
            lineNumber: '19K15MPPK',
            size: 'ONE',
            name: 'Classic Beret',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '12.00',
            totalPrice: '12.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
        savedProducts: [],
        ageVerificationRequired: false,
        restrictedDeliveryItem: false,
        inventoryPositions: {
          item_1: {
            partNumber: '602017001141912',
            catentryId: '29370546',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 8,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
        },
      },
      deliveryLocations: [
        {
          deliveryLocationType: 'HOME',
          label:
            'Home Delivery Standard (UK up to 4 days; worldwide varies)  Express (UK next or nominated day; worldwide varies)',
          selected: true,
          deliveryMethods: [
            {
              shipModeId: 26504,
              shipCode: 'S',
              deliveryType: 'HOME_STANDARD',
              label: 'UK Standard up to 4 days',
              additionalDescription: 'Up to 4 days',
              cost: '4.00',
              selected: true,
              deliveryOptions: [],
            },
            {
              deliveryType: 'HOME_EXPRESS',
              label: 'Express / Nominated day delivery',
              additionalDescription: '',
              selected: false,
              deliveryOptions: [
                {
                  shipModeId: 28004,
                  dayText: 'Wed',
                  dateText: '13 Sep',
                  nominatedDate: '2017-09-13',
                  price: '6.00',
                  selected: true,
                },
                {
                  shipModeId: 28005,
                  dayText: 'Thu',
                  dateText: '14 Sep',
                  nominatedDate: '2017-09-14',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28006,
                  dayText: 'Fri',
                  dateText: '15 Sep',
                  nominatedDate: '2017-09-15',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28007,
                  dayText: 'Sat',
                  dateText: '16 Sep',
                  nominatedDate: '2017-09-16',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28002,
                  dayText: 'Mon',
                  dateText: '18 Sep',
                  nominatedDate: '2017-09-18',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28003,
                  dayText: 'Tue',
                  dateText: '19 Sep',
                  nominatedDate: '2017-09-19',
                  price: '6.00',
                  selected: false,
                },
              ],
            },
          ],
        },
        {
          deliveryLocationType: 'STORE',
          label:
            'Collect from Store Standard (3-7 working days) Express (next day)',
          selected: false,
          deliveryMethods: [],
        },
      ],
      giftCards: [],
      creditCard: {
        type: 'VISA',
        cardNumberStar: '************4444',
      },
      cardNumberHash: 'qdVaNzj/8PFhXEw3GAWQ23mrDgSc058p',
      deliveryInstructions: '',
      smsMobileNumber: '',
      shippingCountry: 'United Kingdom',
      billingDetails: {
        nameAndPhone: {
          title: 'Ms',
          firstName: 'Jane',
          lastName: 'Doe',
          telephone: '',
        },
        address: {
          address1: '86 Tirrington',
          address2: 'Bretton',
          city: 'PETERBOROUGH',
          state: '',
          country: 'United Kingdom',
          postcode: 'PE3 9XT',
        },
      },
      deliveryDetails: {
        addressDetailsId: 2224720,
        nameAndPhone: {
          title: 'Ms',
          firstName: 'Jane',
          lastName: 'Doe',
          telephone: '',
        },
        address: {
          address1: '86 Tirrington',
          address2: 'Bretton',
          city: 'PETERBOROUGH',
          state: '',
          country: 'United Kingdom',
          postcode: 'PE3 9XT',
        },
      },
      savedAddresses: [],
      ageVerificationDeliveryConfirmationRequired: false,
      estimatedDelivery: ['No later than Monday 18 September 2017'],
      version: '1.7',
    },
    orderSummaryError: {},
    orderError: false,
    isDeliverySameAsBilling: true,
    useDeliveryAsBilling: true,
    verifyPayment: false,
    showExpressDeliveryOptionsOnSummary: false,
    editingDetails: false,
    storeUpdating: false,
    partialOrderSummary: {
      basket: {
        orderId: 1813796,
        subTotal: '12.00',
        total: '11.00',
        totalBeforeDiscount: '12.00',
        deliveryOptions: [
          {
            deliveryOptionId: 47524,
            label: 'Collect from ParcelShop £4.00',
            selected: false,
          },
          {
            deliveryOptionId: 26504,
            label: 'UK Standard up to 4 days £4.00',
            selected: true,
          },
          {
            deliveryOptionId: 45020,
            label: 'Collect From Store Express £3.00',
            selected: false,
          },
          {
            deliveryOptionId: 28004,
            label: 'Express / Nominated Day Delivery £6.00',
            selected: false,
          },
          {
            deliveryOptionId: 45019,
            label: 'Free Collect From Store Standard £0.00',
            selected: false,
          },
        ],
        promotions: [
          {
            promotionCode: 'TESTUK5OFF',
            label: '£5 off',
          },
        ],
        discounts: [
          {
            label: '£5 off',
            value: '5.00',
          },
        ],
        products: [
          {
            productId: 29370510,
            catEntryId: 29370546,
            orderItemId: 8222798,
            shipModeId: 26504,
            lineNumber: '19K15MPPK',
            size: 'ONE',
            name: 'Classic Beret',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '12.00',
            totalPrice: '12.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
        savedProducts: [],
        ageVerificationRequired: false,
        restrictedDeliveryItem: false,
        inventoryPositions: {
          item_1: {
            partNumber: '602017001141912',
            catentryId: '29370546',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 8,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
        },
      },
    },
  }
}

export function deliveryDiscountOrder() {
  return {
    orderCompleted: {
      orderId: 1808580,
      subTotal: '93.00',
      returnPossible: false,
      returnRequested: false,
      deliveryMethod: 'UK Standard up to 4 days',
      deliveryDate: 'Monday 18 September 2017',
      deliveryCost: '',
      deliveryCarrier: 'Parcelnet',
      deliveryPrice: '0.00',
      totalOrderPrice: '93.00',
      totalOrdersDiscountLabel: '',
      totalOrdersDiscount: '',
      billingAddress: {
        name: 'Ms Jane Doe',
        address1: '86 Tirrington',
        address2: 'Bretton',
        address3: 'PETERBOROUGH',
        address4: 'PE3 9XT',
        country: 'United Kingdom',
      },
      deliveryAddress: {
        name: 'Ms Jane Doe',
        address1: '86 Tirrington',
        address2: 'Bretton',
        address3: 'PETERBOROUGH',
        address4: 'PE3 9XT',
        country: 'United Kingdom',
      },
      orderLines: [
        {
          lineNo: '11P15MBLK',
          name: 'Collarless Biker Jacket',
          size: '8',
          colour: 'BLACK',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS11P15MBLK_Small_F_1.jpg',
          quantity: 1,
          unitPrice: '49.00',
          discount: '',
          total: '49.00',
          nonRefundable: false,
        },
        {
          lineNo: '42H21MBLE',
          name: 'HERRING Embellished Goldfish Sliders',
          size: '37',
          colour: 'BURGUNDY',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS42H21MBLE_Small_F_1.jpg',
          quantity: 1,
          unitPrice: '26.00',
          discount: '',
          total: '26.00',
          nonRefundable: false,
        },
        {
          lineNo: '29T33KBLK',
          name: 'Logo Boyfriend T-Shirt by Ivy Park',
          size: 'XL',
          colour: 'LIGHT GREY M',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS29T33KBLK_Small_F_1.jpg',
          quantity: 1,
          unitPrice: '18.00',
          discount: '',
          total: '18.00',
          nonRefundable: false,
        },
      ],
      paymentDetails: [
        {
          paymentMethod: 'Visa',
          cardNumberStar: '************4444',
          totalCost: '£93.00',
        },
      ],
      currencyConversion: {
        currencyRate: 'GBP',
      },
    },
    orderSummary: {
      basket: {
        orderId: 1808580,
        subTotal: '93.00',
        total: '93.00',
        totalBeforeDiscount: '93.00',
        deliveryOptions: [
          {
            deliveryOptionId: 28004,
            label: 'Express / Nominated Day Delivery £6.00',
            selected: false,
          },
          {
            deliveryOptionId: 26504,
            label: 'Free UK Standard up to 4 days £0.00',
            selected: true,
          },
          {
            deliveryOptionId: 45019,
            label: 'Free Collect From Store Standard £0.00',
            selected: false,
          },
          {
            deliveryOptionId: 45020,
            label: 'Collect From Store Express £3.00',
            selected: false,
          },
          {
            deliveryOptionId: 49517,
            label: 'Collect From Store Today £3.00',
            selected: false,
          },
          {
            deliveryOptionId: 47524,
            label: 'Collect from ParcelShop £4.00',
            selected: false,
          },
        ],
        promotions: [],
        discounts: [],
        products: [
          {
            productId: 29172364,
            catEntryId: 29172472,
            orderItemId: 8206394,
            shipModeId: 26504,
            lineNumber: '11P15MBLK',
            size: '8',
            name: 'Collarless Biker Jacket',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '49.00',
            totalPrice: '49.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS11P15MBLK_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS11P15MBLK_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS11P15MBLK_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS11P15MBLK_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
          {
            productId: 29175324,
            catEntryId: 29175360,
            orderItemId: 8210343,
            shipModeId: 26504,
            lineNumber: '42H21MBLE',
            size: '37',
            name: 'HERRING Embellished Goldfish Sliders',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '26.00',
            totalPrice: '26.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS42H21MBLE_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS42H21MBLE_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS42H21MBLE_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS42H21MBLE_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
          {
            productId: 28838266,
            catEntryId: 28838342,
            orderItemId: 8210360,
            shipModeId: 26504,
            lineNumber: '29T33KBLK',
            size: 'XL',
            name: 'Logo Boyfriend T-Shirt by Ivy Park',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '18.00',
            totalPrice: '18.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29T33KBLK_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29T33KBLK_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29T33KBLK_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29T33KBLK_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
        savedProducts: [],
        ageVerificationRequired: false,
        restrictedDeliveryItem: false,
        inventoryPositions: {
          item_1: {
            partNumber: '602017001133641',
            catentryId: '29172472',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 10,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
          item_2: {
            partNumber: '602017001133912',
            catentryId: '29175360',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 10,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
          item_3: {
            partNumber: '602017001119724',
            catentryId: '28838342',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 10,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
        },
      },
      deliveryLocations: [
        {
          deliveryLocationType: 'HOME',
          label:
            'Home Delivery Standard (UK up to 4 days; worldwide varies)  Express (UK next or nominated day; worldwide varies)',
          selected: true,
          deliveryMethods: [
            {
              shipModeId: 26504,
              shipCode: 'S',
              deliveryType: 'HOME_STANDARD',
              label: 'UK Standard up to 4 days',
              additionalDescription: 'Up to 4 days',
              selected: true,
              deliveryOptions: [],
            },
            {
              deliveryType: 'HOME_EXPRESS',
              label: 'Express / Nominated day delivery',
              additionalDescription: '',
              selected: false,
              deliveryOptions: [
                {
                  shipModeId: 28004,
                  dayText: 'Wed',
                  dateText: '13 Sep',
                  nominatedDate: '2017-09-13',
                  price: '6.00',
                  selected: true,
                },
                {
                  shipModeId: 28005,
                  dayText: 'Thu',
                  dateText: '14 Sep',
                  nominatedDate: '2017-09-14',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28006,
                  dayText: 'Fri',
                  dateText: '15 Sep',
                  nominatedDate: '2017-09-15',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28007,
                  dayText: 'Sat',
                  dateText: '16 Sep',
                  nominatedDate: '2017-09-16',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28002,
                  dayText: 'Mon',
                  dateText: '18 Sep',
                  nominatedDate: '2017-09-18',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28003,
                  dayText: 'Tue',
                  dateText: '19 Sep',
                  nominatedDate: '2017-09-19',
                  price: '6.00',
                  selected: false,
                },
              ],
            },
          ],
        },
        {
          deliveryLocationType: 'STORE',
          label:
            'Collect from Store Standard (3-7 working days) Express (next day)',
          selected: false,
          deliveryMethods: [],
        },
      ],
      giftCards: [],
      deliveryInstructions: '',
      smsMobileNumber: '',
      shippingCountry: 'United Kingdom',
      savedAddresses: [],
      ageVerificationDeliveryConfirmationRequired: false,
      estimatedDelivery: ['No later than Monday 18 September 2017'],
      version: '1.7',
      deliveryDetails: {
        address1: '86 Tirrington',
        address2: 'Bretton',
        city: 'PETERBOROUGH',
        country: 'United Kingdom',
        postcode: 'PE3 9XT',
        state: null,
      },
    },
    orderSummaryError: {},
    orderError: false,
    isDeliverySameAsBilling: true,
    useDeliveryAsBilling: true,
    verifyPayment: false,
    showExpressDeliveryOptionsOnSummary: false,
    editingDetails: false,
    storeUpdating: false,
    partialOrderSummary: {
      basket: {
        orderId: 1808580,
        subTotal: '93.00',
        total: '93.00',
        totalBeforeDiscount: '93.00',
        deliveryOptions: [
          {
            deliveryOptionId: 28004,
            label: 'Express / Nominated Day Delivery £6.00',
            selected: false,
          },
          {
            deliveryOptionId: 26504,
            label: 'Free UK Standard up to 4 days £0.00',
            selected: true,
          },
          {
            deliveryOptionId: 45019,
            label: 'Free Collect From Store Standard £0.00',
            selected: false,
          },
          {
            deliveryOptionId: 45020,
            label: 'Collect From Store Express £3.00',
            selected: false,
          },
          {
            deliveryOptionId: 49517,
            label: 'Collect From Store Today £3.00',
            selected: false,
          },
          {
            deliveryOptionId: 47524,
            label: 'Collect from ParcelShop £4.00',
            selected: false,
          },
        ],
        promotions: [],
        discounts: [],
        products: [
          {
            productId: 29172364,
            catEntryId: 29172472,
            orderItemId: 8206394,
            shipModeId: 26504,
            lineNumber: '11P15MBLK',
            size: '8',
            name: 'Collarless Biker Jacket',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '49.00',
            totalPrice: '49.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS11P15MBLK_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS11P15MBLK_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS11P15MBLK_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS11P15MBLK_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
          {
            productId: 29175324,
            catEntryId: 29175360,
            orderItemId: 8210343,
            shipModeId: 26504,
            lineNumber: '42H21MBLE',
            size: '37',
            name: 'HERRING Embellished Goldfish Sliders',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '26.00',
            totalPrice: '26.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS42H21MBLE_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS42H21MBLE_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS42H21MBLE_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS42H21MBLE_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
          {
            productId: 28838266,
            catEntryId: 28838342,
            orderItemId: 8210360,
            shipModeId: 26504,
            lineNumber: '29T33KBLK',
            size: 'XL',
            name: 'Logo Boyfriend T-Shirt by Ivy Park',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '18.00',
            totalPrice: '18.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29T33KBLK_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29T33KBLK_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29T33KBLK_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29T33KBLK_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
        savedProducts: [],
        ageVerificationRequired: false,
        restrictedDeliveryItem: false,
        inventoryPositions: {
          item_1: {
            partNumber: '602017001133641',
            catentryId: '29172472',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 10,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
          item_2: {
            partNumber: '602017001133912',
            catentryId: '29175360',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 10,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
          item_3: {
            partNumber: '602017001119724',
            catentryId: '28838342',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 10,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
        },
      },
    },
  }
}

export function multiBuyDiscountOrder() {
  return {
    orderCompleted: {
      orderId: 1815642,
      subTotal: '8.00',
      returnPossible: false,
      returnRequested: false,
      deliveryMethod: 'UK Standard up to 4 days',
      deliveryDate: 'Tuesday 19 September 2017',
      deliveryCost: '4.00',
      deliveryCarrier: 'Parcelnet',
      deliveryPrice: '4.00',
      totalOrderPrice: '12.00',
      totalOrdersDiscountLabel: '',
      totalOrdersDiscount: '',
      billingAddress: {
        name: 'Ms Jane Doe',
        address1: '86 Tirrington',
        address2: 'Bretton',
        address3: 'PETERBOROUGH',
        address4: 'PE3 9XT',
        country: 'United Kingdom',
      },
      deliveryAddress: {
        name: 'Ms Jane Doe',
        address1: '86 Tirrington',
        address2: 'Bretton',
        address3: 'PETERBOROUGH',
        address4: 'PE3 9XT',
        country: 'United Kingdom',
      },
      orderLines: [
        {
          lineNo: '08C09LGRY',
          name: 'Grey Sporty Tube Ankle Socks',
          size: 'ONE',
          colour: 'GREY',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS08C09LGRY_Small_F_1.jpg',
          quantity: 1,
          unitPrice: '3.50',
          discount: '3 for £8 Ankle Socks Promotion',
          discountPrice: '.84',
          total: '2.66',
          nonRefundable: false,
        },
        {
          lineNo: '08L14LBGD',
          name: 'Cropped Rib Glitter Ankle Socks',
          size: 'ONE',
          colour: 'Nude',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS08L14LBGD_Small_F_1.jpg',
          quantity: 1,
          unitPrice: '3.50',
          discount: '3 for £8 Ankle Socks Promotion',
          discountPrice: '.83',
          total: '2.67',
          nonRefundable: false,
        },
        {
          lineNo: '08C06LOGE',
          name: 'Orange Sporty Tubes Ankle Socks',
          size: 'ONE',
          colour: 'ORANGE',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS08C06LOGE_Small_F_1.jpg',
          quantity: 1,
          unitPrice: '3.50',
          discount: '3 for £8 Ankle Socks Promotion',
          discountPrice: '.83',
          total: '2.67',
          nonRefundable: false,
        },
      ],
      paymentDetails: [
        {
          paymentMethod: 'Visa',
          cardNumberStar: '************4444',
          totalCost: '£12.00',
        },
      ],
      currencyConversion: {
        currencyRate: 'GBP',
      },
    },
    orderSummary: {
      basket: {
        orderId: 1815642,
        subTotal: '8.00',
        total: '12.00',
        totalBeforeDiscount: '9.00',
        deliveryOptions: [
          {
            deliveryOptionId: 28005,
            label: 'Express / Nominated Day Delivery £6.00',
            selected: false,
          },
          {
            deliveryOptionId: 45020,
            label: 'Collect From Store Express £3.00',
            selected: false,
          },
          {
            deliveryOptionId: 26504,
            label: 'UK Standard up to 4 days £4.00',
            selected: true,
          },
          {
            deliveryOptionId: 47524,
            label: 'Collect from ParcelShop £4.00',
            selected: false,
          },
          {
            deliveryOptionId: 45019,
            label: 'Free Collect From Store Standard £0.00',
            selected: false,
          },
        ],
        promotions: [],
        discounts: [],
        products: [
          {
            productId: 27984088,
            catEntryId: 27984111,
            orderItemId: 8224835,
            shipModeId: 26504,
            lineNumber: '08C09LGRY',
            size: 'ONE',
            name: 'Grey Sporty Tube Ankle Socks',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '3.50',
            totalPrice: '2.66',
            discountText: '3 for £8 Ankle Socks Promotion',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C09LGRY_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C09LGRY_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C09LGRY_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C09LGRY_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
          {
            productId: 29435579,
            catEntryId: 29435580,
            orderItemId: 8224839,
            shipModeId: 26504,
            lineNumber: '08L14LBGD',
            size: 'ONE',
            name: 'Cropped Rib Glitter Ankle Socks',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '3.50',
            totalPrice: '2.67',
            discountText: '3 for £8 Ankle Socks Promotion',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08L14LBGD_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08L14LBGD_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08L14LBGD_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08L14LBGD_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
          {
            productId: 27953779,
            catEntryId: 27953782,
            orderItemId: 8224842,
            shipModeId: 26504,
            lineNumber: '08C06LOGE',
            size: 'ONE',
            name: 'Orange Sporty Tubes Ankle Socks',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '3.50',
            totalPrice: '2.67',
            discountText: '3 for £8 Ankle Socks Promotion',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C06LOGE_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C06LOGE_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C06LOGE_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C06LOGE_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
        savedProducts: [],
        ageVerificationRequired: false,
        restrictedDeliveryItem: false,
        inventoryPositions: {
          item_1: {
            partNumber: '602017001083637',
            catentryId: '27984111',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 10,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
          item_2: {
            partNumber: '602017001144113',
            catentryId: '29435580',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 10,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
          item_3: {
            partNumber: '602017001082251',
            catentryId: '27953782',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 10,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
        },
      },
      deliveryLocations: [
        {
          deliveryLocationType: 'HOME',
          label:
            'Home Delivery Standard (UK up to 4 days; worldwide varies)  Express (UK next or nominated day; worldwide varies)',
          selected: true,
          deliveryMethods: [
            {
              shipModeId: 26504,
              shipCode: 'S',
              deliveryType: 'HOME_STANDARD',
              label: 'UK Standard up to 4 days',
              additionalDescription: 'Up to 4 days',
              cost: '4.00',
              selected: true,
              deliveryOptions: [],
            },
            {
              deliveryType: 'HOME_EXPRESS',
              label: 'Express / Nominated day delivery',
              additionalDescription: '',
              selected: false,
              deliveryOptions: [
                {
                  shipModeId: 28005,
                  dayText: 'Thu',
                  dateText: '14 Sep',
                  nominatedDate: '2017-09-14',
                  price: '6.00',
                  selected: true,
                },
                {
                  shipModeId: 28006,
                  dayText: 'Fri',
                  dateText: '15 Sep',
                  nominatedDate: '2017-09-15',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28007,
                  dayText: 'Sat',
                  dateText: '16 Sep',
                  nominatedDate: '2017-09-16',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28002,
                  dayText: 'Mon',
                  dateText: '18 Sep',
                  nominatedDate: '2017-09-18',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28003,
                  dayText: 'Tue',
                  dateText: '19 Sep',
                  nominatedDate: '2017-09-19',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28004,
                  dayText: 'Wed',
                  dateText: '20 Sep',
                  nominatedDate: '2017-09-20',
                  price: '6.00',
                  selected: false,
                },
              ],
            },
          ],
        },
        {
          deliveryLocationType: 'STORE',
          label:
            'Collect from Store Standard (3-7 working days) Express (next day)',
          selected: false,
          deliveryMethods: [],
        },
      ],
      giftCards: [],
      creditCard: {
        type: 'VISA',
        cardNumberStar: '************4444',
      },
      cardNumberHash: 'qdVaNzj/8PFhXEw3GAWQ23mrDgSc058p',
      deliveryInstructions: '',
      smsMobileNumber: '',
      shippingCountry: 'United Kingdom',
      billingDetails: {
        nameAndPhone: {
          title: 'Ms',
          firstName: 'Jane',
          lastName: 'Doe',
          telephone: '',
        },
        address: {
          address1: '86 Tirrington',
          address2: 'Bretton',
          city: 'PETERBOROUGH',
          state: '',
          country: 'United Kingdom',
          postcode: 'PE3 9XT',
        },
      },
      deliveryDetails: {
        addressDetailsId: 2218366,
        nameAndPhone: {
          title: 'Ms',
          firstName: 'Jane',
          lastName: 'Doe',
          telephone: '',
        },
        address: {
          address1: '86 Tirrington',
          address2: 'Bretton',
          city: 'PETERBOROUGH',
          state: '',
          country: 'United Kingdom',
          postcode: 'PE3 9XT',
        },
      },
      savedAddresses: [],
      ageVerificationDeliveryConfirmationRequired: false,
      estimatedDelivery: ['No later than Tuesday 19 September 2017'],
      version: '1.7',
    },
    orderSummaryError: {},
    orderError: false,
    isDeliverySameAsBilling: true,
    useDeliveryAsBilling: true,
    verifyPayment: false,
    showExpressDeliveryOptionsOnSummary: false,
    editingDetails: false,
    storeUpdating: false,
    partialOrderSummary: {
      basket: {
        orderId: 1815642,
        subTotal: '8.00',
        total: '12.00',
        totalBeforeDiscount: '9.00',
        deliveryOptions: [
          {
            deliveryOptionId: 28005,
            label: 'Express / Nominated Day Delivery £6.00',
            selected: false,
          },
          {
            deliveryOptionId: 45020,
            label: 'Collect From Store Express £3.00',
            selected: false,
          },
          {
            deliveryOptionId: 26504,
            label: 'UK Standard up to 4 days £4.00',
            selected: true,
          },
          {
            deliveryOptionId: 47524,
            label: 'Collect from ParcelShop £4.00',
            selected: false,
          },
          {
            deliveryOptionId: 45019,
            label: 'Free Collect From Store Standard £0.00',
            selected: false,
          },
        ],
        promotions: [],
        discounts: [],
        products: [
          {
            productId: 27984088,
            catEntryId: 27984111,
            orderItemId: 8224835,
            shipModeId: 26504,
            lineNumber: '08C09LGRY',
            size: 'ONE',
            name: 'Grey Sporty Tube Ankle Socks',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '3.50',
            totalPrice: '2.66',
            discountText: '3 for £8 Ankle Socks Promotion',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C09LGRY_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C09LGRY_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C09LGRY_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C09LGRY_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
          {
            productId: 29435579,
            catEntryId: 29435580,
            orderItemId: 8224839,
            shipModeId: 26504,
            lineNumber: '08L14LBGD',
            size: 'ONE',
            name: 'Cropped Rib Glitter Ankle Socks',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '3.50',
            totalPrice: '2.67',
            discountText: '3 for £8 Ankle Socks Promotion',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08L14LBGD_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08L14LBGD_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08L14LBGD_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08L14LBGD_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
          {
            productId: 27953779,
            catEntryId: 27953782,
            orderItemId: 8224842,
            shipModeId: 26504,
            lineNumber: '08C06LOGE',
            size: 'ONE',
            name: 'Orange Sporty Tubes Ankle Socks',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '3.50',
            totalPrice: '2.67',
            discountText: '3 for £8 Ankle Socks Promotion',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C06LOGE_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C06LOGE_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C06LOGE_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C06LOGE_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
        savedProducts: [],
        ageVerificationRequired: false,
        restrictedDeliveryItem: false,
        inventoryPositions: {
          item_1: {
            partNumber: '602017001083637',
            catentryId: '27984111',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 10,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
          item_2: {
            partNumber: '602017001144113',
            catentryId: '29435580',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 10,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
          item_3: {
            partNumber: '602017001082251',
            catentryId: '27953782',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 10,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
        },
      },
    },
  }
}

export function markedDownItemOrder() {
  return {
    orderCompleted: {
      orderId: 1816292,
      subTotal: '',
      returnPossible: false,
      returnRequested: false,
      deliveryMethod: 'UK Standard up to 4 days',
      deliveryDate: 'Wednesday 20 September 2017',
      deliveryCost: '4.00',
      deliveryCarrier: 'Parcelnet',
      deliveryPrice: '4.00',
      totalOrderPrice: '19.00',
      totalOrdersDiscountLabel: '',
      totalOrdersDiscount: '',
      billingAddress: {
        name: 'Ms Jane Doe',
        address1: '86 Tirrington',
        address2: 'Bretton',
        address3: 'PETERBOROUGH',
        address4: 'PE3 9XT',
        country: 'United Kingdom',
      },
      deliveryAddress: {
        name: 'Ms Jane Doe',
        address1: '86 Tirrington',
        address2: 'Bretton',
        address3: 'PETERBOROUGH',
        address4: 'PE3 9XT',
        country: 'United Kingdom',
      },
      orderLines: [
        {
          lineNo: '24W09MPNK',
          name: 'Tapestry Tote Bag',
          size: 'ONE',
          colour: 'PINK',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS24W09MPNK_Small_F_1.jpg',
          quantity: 1,
          unitPrice: '15.00',
          discount: '',
          total: '15.00',
          nonRefundable: false,
        },
      ],
      paymentDetails: [
        {
          paymentMethod: 'Visa',
          cardNumberStar: '************4444',
          totalCost: '£19.00',
        },
      ],
      currencyConversion: {
        currencyRate: 'GBP',
      },
    },
    orderSummary: {
      basket: {
        orderId: 1816292,
        subTotal: '15.00',
        total: '19.00',
        totalBeforeDiscount: '.00',
        deliveryOptions: [
          {
            deliveryOptionId: 47524,
            label: 'Collect from ParcelShop £4.00',
            selected: false,
          },
          {
            deliveryOptionId: 45020,
            label: 'Collect From Store Express £3.00',
            selected: false,
          },
          {
            deliveryOptionId: 45019,
            label: 'Free Collect From Store Standard £0.00',
            selected: false,
          },
          {
            deliveryOptionId: 26504,
            label: 'UK Standard up to 4 days £4.00',
            selected: true,
          },
          {
            deliveryOptionId: 28005,
            label: 'Express / Nominated Day Delivery £6.00',
            selected: false,
          },
        ],
        promotions: [],
        discounts: [],
        products: [
          {
            productId: 28775493,
            catEntryId: 28775507,
            orderItemId: 8225822,
            shipModeId: 26504,
            lineNumber: '24W09MPNK',
            size: 'ONE',
            name: 'Tapestry Tote Bag',
            quantity: 1,
            lowStock: false,
            inStock: true,
            wasWasPrice: '45.00',
            unitPrice: '15.00',
            totalPrice: '15.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS24W09MPNK_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS24W09MPNK_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS24W09MPNK_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS24W09MPNK_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
        savedProducts: [],
        ageVerificationRequired: false,
        restrictedDeliveryItem: false,
        inventoryPositions: {
          item_1: {
            partNumber: '602017001116968',
            catentryId: '28775507',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 10,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
        },
      },
      deliveryLocations: [
        {
          deliveryLocationType: 'HOME',
          label:
            'Home Delivery Standard (UK up to 4 days; worldwide varies)  Express (UK next or nominated day; worldwide varies)',
          selected: true,
          deliveryMethods: [
            {
              shipModeId: 26504,
              shipCode: 'S',
              deliveryType: 'HOME_STANDARD',
              label: 'UK Standard up to 4 days',
              additionalDescription: 'Up to 4 days',
              cost: '4.00',
              selected: true,
              deliveryOptions: [],
            },
            {
              deliveryType: 'HOME_EXPRESS',
              label: 'Express / Nominated day delivery',
              additionalDescription: '',
              selected: false,
              deliveryOptions: [
                {
                  shipModeId: 28005,
                  dayText: 'Thu',
                  dateText: '14 Sep',
                  nominatedDate: '2017-09-14',
                  price: '6.00',
                  selected: true,
                },
                {
                  shipModeId: 28006,
                  dayText: 'Fri',
                  dateText: '15 Sep',
                  nominatedDate: '2017-09-15',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28007,
                  dayText: 'Sat',
                  dateText: '16 Sep',
                  nominatedDate: '2017-09-16',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28002,
                  dayText: 'Mon',
                  dateText: '18 Sep',
                  nominatedDate: '2017-09-18',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28003,
                  dayText: 'Tue',
                  dateText: '19 Sep',
                  nominatedDate: '2017-09-19',
                  price: '6.00',
                  selected: false,
                },
                {
                  shipModeId: 28004,
                  dayText: 'Wed',
                  dateText: '20 Sep',
                  nominatedDate: '2017-09-20',
                  price: '6.00',
                  selected: false,
                },
              ],
            },
          ],
        },
        {
          deliveryLocationType: 'STORE',
          label:
            'Collect from Store Standard (3-7 working days) Express (next day)',
          selected: false,
          deliveryMethods: [],
        },
      ],
      giftCards: [],
      creditCard: {
        type: 'VISA',
        cardNumberStar: '************4444',
      },
      cardNumberHash: 'qdVaNzj/8PFhXEw3GAWQ23mrDgSc058p',
      deliveryInstructions: '',
      smsMobileNumber: '',
      shippingCountry: 'United Kingdom',
      billingDetails: {
        nameAndPhone: {
          title: 'Ms',
          firstName: 'Jane',
          lastName: 'Doe',
          telephone: '',
        },
        address: {
          address1: '86 Tirrington',
          address2: 'Bretton',
          city: 'PETERBOROUGH',
          state: '',
          country: 'United Kingdom',
          postcode: 'PE3 9XT',
        },
      },
      deliveryDetails: {
        addressDetailsId: 2218366,
        nameAndPhone: {
          title: 'Ms',
          firstName: 'Jane',
          lastName: 'Doe',
          telephone: '',
        },
        address: {
          address1: '86 Tirrington',
          address2: 'Bretton',
          city: 'PETERBOROUGH',
          state: '',
          country: 'United Kingdom',
          postcode: 'PE3 9XT',
        },
      },
      savedAddresses: [],
      ageVerificationDeliveryConfirmationRequired: false,
      estimatedDelivery: ['No later than Tuesday 19 September 2017'],
      version: '1.7',
    },
    orderSummaryError: {},
    orderError: false,
    isDeliverySameAsBilling: true,
    useDeliveryAsBilling: true,
    verifyPayment: false,
    showExpressDeliveryOptionsOnSummary: false,
    editingDetails: false,
    storeUpdating: false,
    partialOrderSummary: {
      basket: {
        orderId: 1816292,
        subTotal: '15.00',
        total: '19.00',
        totalBeforeDiscount: '.00',
        deliveryOptions: [
          {
            deliveryOptionId: 47524,
            label: 'Collect from ParcelShop £4.00',
            selected: false,
          },
          {
            deliveryOptionId: 45020,
            label: 'Collect From Store Express £3.00',
            selected: false,
          },
          {
            deliveryOptionId: 45019,
            label: 'Free Collect From Store Standard £0.00',
            selected: false,
          },
          {
            deliveryOptionId: 26504,
            label: 'UK Standard up to 4 days £4.00',
            selected: true,
          },
          {
            deliveryOptionId: 28005,
            label: 'Express / Nominated Day Delivery £6.00',
            selected: false,
          },
        ],
        promotions: [],
        discounts: [],
        products: [
          {
            productId: 28775493,
            catEntryId: 28775507,
            orderItemId: 8225822,
            shipModeId: 26504,
            lineNumber: '24W09MPNK',
            size: 'ONE',
            name: 'Tapestry Tote Bag',
            quantity: 1,
            lowStock: false,
            inStock: true,
            wasWasPrice: '45.00',
            unitPrice: '15.00',
            totalPrice: '15.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS24W09MPNK_Thumb_F_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS24W09MPNK_Small_F_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS24W09MPNK_2col_F_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS24W09MPNK_Zoom_F_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
        savedProducts: [],
        ageVerificationRequired: false,
        restrictedDeliveryItem: false,
        inventoryPositions: {
          item_1: {
            partNumber: '602017001116968',
            catentryId: '28775507',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 10,
                ffmcenterId: 12556,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
        },
      },
    },
  }
}

export function customerRegistersDuringCheckout() {
  return {
    accordions: {},
    account: {
      user: {
        exists: true,
        email: 'rodderstestxx@test.com',
        title: 'Ms',
        firstName: 'Jane',
        lastName: 'Doe',
        userTrackingId: 2296610,
        basketItemCount: 0,
        creditCard: {
          type: 'VISA',
          cardNumberHash: 'qdVaNzj/8PFhXEw3GAWQ23mrDgSc058p',
          cardNumberStar: '************4444',
          expiryMonth: '09',
          expiryYear: '2019',
        },
        deliveryDetails: {
          addressDetailsId: 2246907,
          nameAndPhone: {
            title: 'Ms',
            firstName: 'Jane',
            lastName: 'Doe',
            telephone: '01234567890',
          },
          address: {
            address1: '86 Tirrington',
            address2: 'Bretton',
            city: 'PETERBOROUGH',
            state: '',
            country: 'United Kingdom',
            postcode: 'PE3 9XT',
          },
        },
        billingDetails: {
          addressDetailsId: 2246910,
          nameAndPhone: {
            title: 'Ms',
            firstName: 'Jane',
            lastName: 'Doe',
            telephone: '01234567890',
          },
          address: {
            address1: '86 Tirrington',
            address2: 'Bretton',
            city: 'PETERBOROUGH',
            state: '',
            country: 'United Kingdom',
            postcode: 'PE3 9XT',
          },
        },
        version: '1.7',
      },
      forgetPwd: true,
    },
    adobe: {
      pageLoadTime: 0,
    },
    auth: {
      authentication: true,
      loading: false,
    },
    backToTop: {
      isVisible: false,
    },
    breadCrumbs: {
      isHidden: false,
    },
    campaigns: {
      lfw: {
        socialFeed: [],
        blogFeed: [],
      },
    },
    carousel: {
      productDetail: {
        direction: 'right',
        previous: -1,
        current: 0,
        max: 2,
        zoom: 1,
        panX: 0,
        panY: 0,
      },
    },
    changePassword: {
      success: false,
    },
    checkout: {
      orderCompleted: {
        orderId: 1816564,
        subTotal: '',
        returnPossible: false,
        returnRequested: false,
        deliveryMethod: 'UK Standard up to 4 days',
        deliveryDate: 'Wednesday 20 September 2017',
        deliveryCost: '4.00',
        deliveryCarrier: 'Parcelnet',
        deliveryPrice: '4.00',
        totalOrderPrice: '16.00',
        totalOrdersDiscountLabel: '',
        totalOrdersDiscount: '',
        billingAddress: {
          name: 'Ms Jane Doe',
          address1: '86 Tirrington',
          address2: 'Bretton',
          address3: 'PETERBOROUGH',
          address4: 'PE3 9XT',
          country: 'United Kingdom',
        },
        deliveryAddress: {
          name: 'Ms Jane Doe',
          address1: '86 Tirrington',
          address2: 'Bretton',
          address3: 'PETERBOROUGH',
          address4: 'PE3 9XT',
          country: 'United Kingdom',
        },
        orderLines: [
          {
            lineNo: '19K15MBLK',
            name: 'Classic Beret Hat',
            size: 'ONE',
            colour: 'BLACK',
            imageUrl:
              '//media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Small_F_1.jpg',
            quantity: 1,
            unitPrice: '12.00',
            discount: '',
            total: '12.00',
            nonRefundable: false,
          },
        ],
        paymentDetails: [
          {
            paymentMethod: 'Visa',
            cardNumberStar: '************4444',
            totalCost: '£16.00',
          },
        ],
        currencyConversion: {
          currencyRate: 'GBP',
        },
      },
      orderSummary: {
        basket: {
          orderId: 1816564,
          subTotal: '12.00',
          total: '16.00',
          totalBeforeDiscount: '12.00',
          deliveryOptions: [
            {
              deliveryOptionId: 45019,
              label: 'Free Collect From Store Standard £0.00',
              selected: false,
            },
            {
              deliveryOptionId: 28005,
              label: 'Express / Nominated Day Delivery £6.00',
              selected: false,
            },
            {
              deliveryOptionId: 47524,
              label: 'Collect from ParcelShop £4.00',
              selected: false,
            },
            {
              deliveryOptionId: 26504,
              label: 'UK Standard up to 4 days £4.00',
              selected: true,
            },
            {
              deliveryOptionId: 45020,
              label: 'Collect From Store Express £3.00',
              selected: false,
            },
          ],
          promotions: [],
          discounts: [],
          products: [
            {
              productId: 29370388,
              catEntryId: 29370437,
              orderItemId: 8226197,
              shipModeId: 26504,
              lineNumber: '19K15MBLK',
              size: 'ONE',
              name: 'Classic Beret Hat',
              quantity: 1,
              lowStock: false,
              inStock: true,
              unitPrice: '12.00',
              totalPrice: '12.00',
              assets: [
                {
                  assetType: 'IMAGE_SMALL',
                  index: 1,
                  url:
                    'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Thumb_F_1.jpg',
                },
                {
                  assetType: 'IMAGE_THUMB',
                  index: 1,
                  url:
                    'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Small_F_1.jpg',
                },
                {
                  assetType: 'IMAGE_NORMAL',
                  index: 1,
                  url:
                    'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_2col_F_1.jpg',
                },
                {
                  assetType: 'IMAGE_LARGE',
                  index: 1,
                  url:
                    'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Zoom_F_1.jpg',
                },
              ],
              items: [],
              bundleProducts: [],
              attributes: {},
              colourSwatches: [],
              tpmLinks: [],
              bundleSlots: [],
              ageVerificationRequired: false,
              isBundleOrOutfit: false,
            },
          ],
          savedProducts: [],
          ageVerificationRequired: false,
          restrictedDeliveryItem: false,
          inventoryPositions: {
            item_1: {
              partNumber: '602017001141910',
              catentryId: '29370437',
              inventorys: [
                {
                  cutofftime: '2100',
                  quantity: 10,
                  ffmcenterId: 12556,
                  expressdates: ['2017-09-13', '2017-09-14'],
                },
              ],
            },
          },
        },
        deliveryLocations: [
          {
            deliveryLocationType: 'HOME',
            label:
              'Home Delivery Standard (UK up to 4 days; worldwide varies)  Express (UK next or nominated day; worldwide varies)',
            selected: true,
            deliveryMethods: [
              {
                shipModeId: 26504,
                shipCode: 'S',
                deliveryType: 'HOME_STANDARD',
                label: 'UK Standard up to 4 days',
                additionalDescription: 'Up to 4 days',
                cost: '4.00',
                selected: true,
                deliveryOptions: [],
              },
              {
                deliveryType: 'HOME_EXPRESS',
                label: 'Express / Nominated day delivery',
                additionalDescription: '',
                selected: false,
                deliveryOptions: [
                  {
                    shipModeId: 28005,
                    dayText: 'Thu',
                    dateText: '14 Sep',
                    nominatedDate: '2017-09-14',
                    price: '6.00',
                    selected: true,
                  },
                  {
                    shipModeId: 28006,
                    dayText: 'Fri',
                    dateText: '15 Sep',
                    nominatedDate: '2017-09-15',
                    price: '6.00',
                    selected: false,
                  },
                  {
                    shipModeId: 28007,
                    dayText: 'Sat',
                    dateText: '16 Sep',
                    nominatedDate: '2017-09-16',
                    price: '6.00',
                    selected: false,
                  },
                  {
                    shipModeId: 28002,
                    dayText: 'Mon',
                    dateText: '18 Sep',
                    nominatedDate: '2017-09-18',
                    price: '6.00',
                    selected: false,
                  },
                  {
                    shipModeId: 28003,
                    dayText: 'Tue',
                    dateText: '19 Sep',
                    nominatedDate: '2017-09-19',
                    price: '6.00',
                    selected: false,
                  },
                  {
                    shipModeId: 28004,
                    dayText: 'Wed',
                    dateText: '20 Sep',
                    nominatedDate: '2017-09-20',
                    price: '6.00',
                    selected: false,
                  },
                ],
              },
            ],
          },
          {
            deliveryLocationType: 'STORE',
            label:
              'Collect from Store Standard (3-7 working days) Express (next day)',
            selected: false,
            deliveryMethods: [],
          },
        ],
        giftCards: [],
        deliveryInstructions: '',
        smsMobileNumber: '',
        shippingCountry: 'United Kingdom',
        savedAddresses: [],
        ageVerificationDeliveryConfirmationRequired: false,
        estimatedDelivery: ['No later than Tuesday 19 September 2017'],
        version: '1.7',
        deliveryDetails: {
          address1: '86 Tirrington',
          address2: 'Bretton',
          city: 'PETERBOROUGH',
          country: 'United Kingdom',
          postcode: 'PE3 9XT',
          state: null,
        },
      },
      orderSummaryError: {},
      orderError: false,
      isDeliverySameAsBilling: true,
      useDeliveryAsBilling: true,
      verifyPayment: false,
      showExpressDeliveryOptionsOnSummary: false,
      editingDetails: false,
      storeUpdating: false,
      partialOrderSummary: {
        basket: {
          orderId: 1816564,
          subTotal: '12.00',
          total: '16.00',
          totalBeforeDiscount: '12.00',
          deliveryOptions: [
            {
              deliveryOptionId: 45019,
              label: 'Free Collect From Store Standard £0.00',
              selected: false,
            },
            {
              deliveryOptionId: 28005,
              label: 'Express / Nominated Day Delivery £6.00',
              selected: false,
            },
            {
              deliveryOptionId: 47524,
              label: 'Collect from ParcelShop £4.00',
              selected: false,
            },
            {
              deliveryOptionId: 26504,
              label: 'UK Standard up to 4 days £4.00',
              selected: true,
            },
            {
              deliveryOptionId: 45020,
              label: 'Collect From Store Express £3.00',
              selected: false,
            },
          ],
          promotions: [],
          discounts: [],
          products: [
            {
              productId: 29370388,
              catEntryId: 29370437,
              orderItemId: 8226197,
              shipModeId: 26504,
              lineNumber: '19K15MBLK',
              size: 'ONE',
              name: 'Classic Beret Hat',
              quantity: 1,
              lowStock: false,
              inStock: true,
              unitPrice: '12.00',
              totalPrice: '12.00',
              assets: [
                {
                  assetType: 'IMAGE_SMALL',
                  index: 1,
                  url:
                    'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Thumb_F_1.jpg',
                },
                {
                  assetType: 'IMAGE_THUMB',
                  index: 1,
                  url:
                    'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Small_F_1.jpg',
                },
                {
                  assetType: 'IMAGE_NORMAL',
                  index: 1,
                  url:
                    'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_2col_F_1.jpg',
                },
                {
                  assetType: 'IMAGE_LARGE',
                  index: 1,
                  url:
                    'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Zoom_F_1.jpg',
                },
              ],
              items: [],
              bundleProducts: [],
              attributes: {},
              colourSwatches: [],
              tpmLinks: [],
              bundleSlots: [],
              ageVerificationRequired: false,
              isBundleOrOutfit: false,
            },
          ],
          savedProducts: [],
          ageVerificationRequired: false,
          restrictedDeliveryItem: false,
          inventoryPositions: {
            item_1: {
              partNumber: '602017001141910',
              catentryId: '29370437',
              inventorys: [
                {
                  cutofftime: '2100',
                  quantity: 10,
                  ffmcenterId: 12556,
                  expressdates: ['2017-09-13', '2017-09-14'],
                },
              ],
            },
          },
        },
      },
    },
    cms: {
      pages: {
        mobileTacticalMessageESpotPos1: {
          pageId: 116708,
          pageName: 'Mobile Tactical Message eSpot',
          baseline: '76',
          contentPath: '/cms/pages/json/json-0000116708/json-0000116708.json',
          seoUrl: '',
          pageData: [
            {
              type: 'imagelist',
              data: {
                options: {},
                columns: 1,
                assets: [
                  {
                    target: '',
                    alt: 'Express Delivery',
                    link:
                      '/en/tsuk/category/uk-delivery-4043283/home?TS=1421171569402&amp;cat2=2141530&amp;intcmpid=sitewide_mobile_WK3_LFW',
                    source:
                      'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/json/json-0000116708/images/MOBILElfw.svg',
                  },
                ],
              },
            },
          ],
          version: '1.7',
        },
        navSocial: {
          pageId: 116706,
          pageName: 'mob - Menu Navigation - Social Icons',
          baseline: '6',
          contentPath: '/cms/pages/json/json-0000116706/json-0000116706.json',
          seoUrl: '',
          pageData: [
            {
              type: 'imagelist',
              data: {
                options: {},
                columns: 5,
                assets: [
                  {
                    target: '_blank',
                    alt: 'Instagram',
                    link: 'https://www.instagram.com/topshop/',
                    source:
                      'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/json/json-0000116706/images/Instagram_Icon_Mobile.svg',
                  },
                  {
                    target: '_blank',
                    alt: 'Facebook',
                    link: 'http://www.facebook.com/Topshop',
                    source:
                      'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/json/json-0000116706/images/Facebook_Icon.svg',
                  },
                  {
                    target: '_blank',
                    alt: 'Twitter',
                    link: 'http://www.twitter.com/topshop',
                    source:
                      'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/json/json-0000116706/images/Twitter_Icon.svg',
                  },
                  {
                    target: '_blank',
                    alt: 'Pinterest',
                    link: 'http://pinterest.com/topshop/',
                    source:
                      'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/json/json-0000116706/images/Pinterest_Icon.svg',
                  },
                  {
                    target: '_blank',
                    alt: 'Snapchat',
                    link: 'https://www.snapchat.com/add/topshop',
                    source:
                      'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/json/json-0000116706/images/Snapchat_Icon.svg',
                  },
                ],
              },
            },
          ],
          version: '1.7',
        },
        mobilePLPESpotPos1: {
          pageId: 116870,
          pageName: 'Mobile PLP POS1 eSpot',
          baseline: '31',
          contentPath: '/cms/pages/json/json-0000116870/json-0000116870.json',
          seoUrl: '',
          pageData: [
            {
              type: 'imagelist',
              data: {
                options: {},
                columns: 1,
                assets: [
                  {
                    target: '',
                    alt: 'Delivery - Find Out More',
                    link:
                      '/en/tsuk/category/uk-delivery-4043283/home?TS=1421171569402&amp;cat2=2141530&amp;intcmpid=mobile_PLP_Delivery',
                    source:
                      'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/json/json-0000116870/images/NEW_monty-plp_.svg',
                  },
                ],
              },
            },
          ],
          version: '1.7',
        },
        mobilePDPESpotPos1: {
          error: 'Error retrieving CMS content',
        },
        mobilePDPESpotPos2: {
          pageId: 116710,
          pageName: 'Mobile PDP POS1 eSpot',
          baseline: '35',
          contentPath: '/cms/pages/json/json-0000116710/json-0000116710.json',
          seoUrl: '',
          pageData: [
            {
              type: 'imagelist',
              data: {
                options: {},
                columns: 1,
                assets: [
                  {
                    target: '',
                    alt: 'Free Express Delivery - Find Out More',
                    link:
                      '/en/tsuk/category/uk-delivery-4043283/home?TS=1421171569402&amp;cat2=2141530&amp;intcmpid=W_ESPOT_PDP_DELIVERY_LFW',
                    source:
                      'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/json/json-0000116710/images/newMonty-PDP-UK.svg',
                  },
                ],
              },
            },
          ],
          version: '1.7',
        },
        mobileShoppingBagESpotPos1: {
          pageId: 116871,
          pageName: 'Mobile ShoppingBag POS1 eSpot',
          baseline: '42',
          contentPath: '/cms/pages/json/json-0000116871/json-0000116871.json',
          seoUrl: '',
          pageData: [
            {
              type: 'imagelist',
              data: {
                options: {},
                columns: 1,
                assets: [
                  {
                    target: '',
                    alt: 'Free Express Delivery - Find Out More',
                    link:
                      '/en/tsuk/category/uk-delivery-4043283/home?TS=1421171569402&amp;cat2=2141530intcmpid=W_SHOPPINGBAG_LFW_DELIVERY',
                    source:
                      'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/json/json-0000116871/images/newMonty-PDP-UK.svg',
                  },
                ],
              },
            },
          ],
          version: '1.7',
        },
        mobileCheckoutESpotPos1: {
          pageId: 116874,
          pageName: 'Mobile Checkout POS1 eSpot',
          baseline: '44',
          contentPath: '/cms/pages/json/json-0000116874/json-0000116874.json',
          seoUrl: '',
          pageData: [
            {
              type: 'imagelist',
              data: {
                options: {},
                columns: 1,
                assets: [
                  {
                    target: '',
                    alt: 'Free Express Delivery - Find Out More',
                    link:
                      '/en/tsuk/category/uk-delivery-4043283/home?TS=1421171569402&amp;cat2=2141530&amp;intcmpid=W_ESPOT_CHECKOUT_LFW_DELIVERY',
                    source:
                      'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/json/json-0000116874/images/LFWUKCheckout-UK.svg',
                  },
                ],
              },
            },
          ],
          version: '1.7',
        },
        mobileOrderConfirmationESpotPos1: {
          pageId: 116875,
          pageName: 'Mobile OrderConf POS1 eSpot',
          baseline: '4',
          contentPath: '/cms/pages/json/json-0000116875/json-0000116875.json',
          seoUrl: '/en/tsuk/category/home-654321/home',
          pageData: [
            {
              type: 'imagelist',
              data: {
                columns: 1,
                assets: [
                  {
                    target: '',
                    alt: 'Free Shipping - Find out more',
                    link:
                      '/en/tsuk/category/uk-delivery-4043283/home?TS=1421171569402&amp;intcmpid=mobile_OrderConf_Shipping',
                    source:
                      'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/json/json-0000116875/images/ESPOTS_PDP_UK.jpg',
                  },
                ],
              },
            },
          ],
          version: '1.7',
        },
      },
      showTacticalMessage: false,
    },
    config: {
      paymentSchema: ['type', 'cardNumber', 'expiryMonth', 'expiryYear'],
      customerDetailsSchema: [
        'title',
        'firstName',
        'lastName',
        'telephone',
        'country',
        'postcode',
        'address1',
        'address2',
        'city',
        'state',
        'type',
        'cardNumber',
        'expiryMonth',
        'expiryYear',
      ],
      checkoutAddressFormRules: {
        Albania: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Andorra: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Anguilla: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Argentina: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Armenia: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Australia: {
          pattern: '^[0-9]{4}$',
          stateFieldType: 'input',
          postcodeRequired: true,
          postcodeLabel: 'Postcode',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Austria: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Azerbaijan: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Bangladesh: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Barbados: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Belgium: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Bermuda: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Bolivia: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'Bosnia and Herzegovina': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Brazil: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Brunei: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Bulgaria: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Cambodia: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Canada: {
          pattern: '^[\\w ]+$',
          stateFieldType: 'input',
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'Cayman Islands': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Chile: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        China: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Colombia: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Croatia: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'Czech Republic': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Denmark: {
          pattern: '^[^\\s]*[0-9]{3,4}$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Estonia: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'Falkland Islands': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'Faroe Islands': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Finland: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        France: {
          pattern: '^[0-9]{5}$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postcode',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'French Guiana': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Germany: {
          pattern: '^[0-9]{5}$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postcode',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Greece: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Greenland: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Guadeloupe: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Guernsey: {
          pattern:
            '^[Gg][Yy]([0-9]|10)[ ]?[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postcode',
          premisesRequired: false,
          premisesLabel: 'House name or number',
        },
        'Holy See': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Hungary: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        India: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Index Number',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Indonesia: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Ireland: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: false,
          postcodeLabel: 'Town or Locality',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Italy: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Japan: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Jersey: {
          pattern:
            '^[Jj][Ee]{1,2}[0-5][ ]?[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postcode',
          premisesRequired: false,
          premisesLabel: 'House name or number',
        },
        Kazakhstan: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'Korea South': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Kyrgyzstan: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Latvia: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Libya: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Liechtenstein: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Lithuania: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Luxembourg: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Madagascar: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Malaysia: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postcode',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Maldives: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'Marshall Islands': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Mexico: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Moldova: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Monaco: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Montenegro: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Morocco: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Nepal: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Netherlands: {
          pattern: '^[0-9]{4}[\\s][a-zA-Z]{2}$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'New Caledonia': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'New Zealand': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Niger: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Norway: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Oman: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Pakistan: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Palau: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Philippines: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Zip Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Poland: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Portugal: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'Puerto Rico': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Romania: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'S Georgia & S Sandwich Islands': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'Saint Pierre and Miquelon': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Samoa: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'San Marino': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Serbia: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Singapore: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Slovakia: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'South Africa': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Spain: {
          pattern: '^([a-zA-Z]{2}|[0-9]{2})[0-9]{3}$',
          stateFieldType: 'input',
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'Sri Lanka': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'St Vincent and the Grenadines': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'Svalbard and Jan Mayen': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Sweden: {
          pattern: '^[1-9][0-9]{2}[\\s][0-9]{2}$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Switzerland: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Taiwan: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Thailand: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Togo: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Tunisia: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Turkey: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postcode',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Turkmenistan: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Ukraine: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'United Kingdom': {
          pattern:
            '^(([gG][iI][rR] {0,}0[aA]{2})|(([aA][sS][cC][nN]|[sS][tT][hH][lL]|[tT][dD][cC][uU]|[bB][bB][nN][dD]|[bB][iI][qQ][qQ]|[fF][iI][qQ][qQ]|[pP][cC][rR][nN]|[sS][iI][qQ][qQ]|[iT][kK][cC][aA]) {0,}1[zZ]{2})|((([a-pr-uwyzA-PR-UWYZ][a-hk-yxA-HK-XY]?[0-9][0-9]?)|(([a-pr-uwyzA-PR-UWYZ][0-9][a-hjkstuwA-HJKSTUW])|([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y][0-9][abehmnprv-yABEHMNPRV-Y]))) {0,}[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}))$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postcode',
          premisesRequired: false,
          premisesLabel: 'House number',
        },
        'United States': {
          pattern: '^[0-9]{5}(-[0-9]{4}){0,1}$',
          stateFieldType: 'SELECT',
          postcodeRequired: true,
          postcodeLabel: 'Zip Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Uruguay: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        Vietnam: {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'Virgin Islands British': {
          pattern: '^[\\w ]+$',
          stateFieldType: false,
          postcodeRequired: true,
          postcodeLabel: 'Postal Code',
          premisesRequired: true,
          premisesLabel: 'Street Address',
        },
        'Hong Kong S.A.R. of China': {
          pattern: '.',
          stateFieldType: false,
          postcodeRequired: false,
          postcodeLabel: 'Town or Locality',
          premisesRequired: false,
          premisesLabel: 'Street Address',
        },
      },
      qasCountries: {
        Canada: 'CAN',
        Denmark: 'DNK',
        Guernsey: 'GGY',
        India: 'IND',
        Indonesia: 'IDN',
        Jersey: 'JEY',
        Malaysia: 'MYS',
        Philippines: 'PHL ',
        Spain: 'ESP ',
        Sweden: 'SWE',
        Turkey: 'TUR',
        'United Kingdom': 'GBR',
      },
      analyticsId: 'arcadiatsmobile',
      googleTagManagerId: 'GTM-W3T7B3Z',
      stagingReportSuiteId: 'arcadiatsstage',
      prodReportSuiteId: 'arcadiatsrollup-prod',
      brandName: 'topshop',
      siteId: 12556,
      storeCode: 'tsuk',
      brandCode: 'ts',
      region: 'uk',
      brandId: 1,
      name: 'Topshop UK',
      lang: 'en',
      locale: 'gb',
      currencyCode: 'GBP',
      currencySymbol: '£',
      country: 'United Kingdom',
      defaultLanguage: 'English',
      bazaarVoiceId: '6025-en_gb',
      hostnames: ['m.topshop.com'],
      adobeTrackingServer: 'sstats.topshop.com',
      mediaHostname: 'media.topshop.com',
      peeriusDomain: 'topshop',
      peeriusLang: 'en-gb',
      opentagRef: 'topshopuk',
      language: 'en-gb',
      googleSiteVerification: 'zQ2HIlAlAghVTHgdRcdJNUd4gDlQUNMekCVJpvi65iU',
      langHostnames: {
        'United Kingdom': {
          hostname: 'm.topshop.com',
          defaultLanguage: 'English',
        },
        'United States': {
          hostname: 'm.us.topshop.com',
          defaultLanguage: 'English',
        },
        Germany: {
          hostname: 'm.de.topshop.com',
          defaultLanguage: 'German',
        },
        France: {
          hostname: 'm.fr.topshop.com',
          defaultLanguage: 'French',
        },
        default: {
          hostname: 'm.eu.topshop.com',
          defaultLanguage: 'English',
        },
        Singapore: {
          hostname: 'm.topshop.com',
          defaultLanguage: 'English',
        },
        Malaysia: {
          hostname: 'm.topshop.com',
          defaultLanguage: 'English',
        },
        Thailand: {
          hostname: 'm.topshop.com',
          defaultLanguage: 'English',
        },
        Indonesia: {
          hostname: 'm.topshop.com',
          defaultLanguage: 'English',
        },
        nonEU: {
          hostname: 'm.topshop.com',
          defaultLanguage: 'English',
        },
      },
      isDaylightSavingTime: true,
      assets: {
        css: {
          'topshop/custom/lfw16/blogfeed.css':
            '/assets/topshop/custom/lfw16/blogfeed.css',
          'topshop/custom/lfw16/lfw16.css':
            '/assets/topshop/custom/lfw16/lfw16.css',
          'topshop/custom/lfw16/newsletter.css':
            '/assets/topshop/custom/lfw16/newsletter.css',
          'topshop/custom/lfw16/showspace.css':
            '/assets/topshop/custom/lfw16/showspace.css',
          'topshop/custom/lfw16/tabtitle.css':
            '/assets/topshop/custom/lfw16/tabtitle.css',
          'topman/styles.css': '/assets/topman/styles.css',
          'topshop/styles.css': '/assets/topshop/styles.css',
          'burton/styles.css': '/assets/burton/styles.css',
          'wallis/styles.css': '/assets/wallis/styles.css',
          'evans/styles.css': '/assets/evans/styles.css',
          'missselfridge/styles.css': '/assets/missselfridge/styles.css',
          'dorothyperkins/styles.css': '/assets/dorothyperkins/styles.css',
          'topman/styles-tablet.css': '/assets/topman/styles-tablet.css',
          'topman/styles-laptop.css': '/assets/topman/styles-laptop.css',
          'topman/styles-desktop.css': '/assets/topman/styles-desktop.css',
          'topshop/styles-tablet.css': '/assets/topshop/styles-tablet.css',
          'topshop/styles-laptop.css': '/assets/topshop/styles-laptop.css',
          'topshop/styles-desktop.css': '/assets/topshop/styles-desktop.css',
          'burton/styles-tablet.css': '/assets/burton/styles-tablet.css',
          'burton/styles-laptop.css': '/assets/burton/styles-laptop.css',
          'burton/styles-desktop.css': '/assets/burton/styles-desktop.css',
          'wallis/styles-tablet.css': '/assets/wallis/styles-tablet.css',
          'wallis/styles-laptop.css': '/assets/wallis/styles-laptop.css',
          'wallis/styles-desktop.css': '/assets/wallis/styles-desktop.css',
          'evans/styles-tablet.css': '/assets/evans/styles-tablet.css',
          'evans/styles-laptop.css': '/assets/evans/styles-laptop.css',
          'evans/styles-desktop.css': '/assets/evans/styles-desktop.css',
          'missselfridge/styles-tablet.css':
            '/assets/missselfridge/styles-tablet.css',
          'missselfridge/styles-laptop.css':
            '/assets/missselfridge/styles-laptop.css',
          'missselfridge/styles-desktop.css':
            '/assets/missselfridge/styles-desktop.css',
          'dorothyperkins/styles-tablet.css':
            '/assets/dorothyperkins/styles-tablet.css',
          'dorothyperkins/styles-laptop.css':
            '/assets/dorothyperkins/styles-laptop.css',
          'dorothyperkins/styles-desktop.css':
            '/assets/dorothyperkins/styles-desktop.css',
        },
        js: {
          'common/0.js': '/assets/common/0.js',
          'common/1.js': '/assets/common/1.js',
          'common/bundle.js': '/assets/common/bundle.js',
          'common/service-desk.js': '/assets/common/service-desk.js',
          'common/vendor.js': '/assets/common/vendor.js',
        },
        chunks: {
          0: 'common/0.js',
          1: 'common/1.js',
          2: 'common/2.js',
          3: 'common/3.js',
          4: 'common/4.js',
          5: 'common/5.js',
          6: 'common/6.js',
          7: 'common/7.js',
          8: 'common/8.js',
          9: 'common/9.js',
          10: 'common/10.js',
          11: 'common/11.js',
        },
      },
    },
    currentProduct: {},
    debug: {},
    errorMessage: null,
    errorSession: {
      sessionExpired: false,
      msg: '',
    },
    features: {
      status: {
        FEATURE_SWATCHES: true,
        PASSWORD_SHOW_TOGGLE: false,
        FEATURE_PUDO: true,
        FEATURE_ORDER_HISTORY_MSG: false,
        FEATURE_HEADER_BIG: true,
        FEATURE_PDP_QUANTITY: true,
        FEATURE_RESPONSIVE: true,
        FEATURE_NEW_CHECKOUT: false,
        FEATURE_STORE_FINDER_HEADER_WITH_COUNTRY_SELECTOR: true,
        FEATURE_CVV_HELP: false,
        FEATURE_CFSI: false,
      },
      overrides: {
        FEATURE_RESPONSIVE: true,
      },
    },
    findAddress: {
      isManual: false,
      monikers: [],
    },
    pageCache: {
      retrieved: false,
    },
    findInStore: {
      activeItem: {
        quantity: 10,
        size: 'ONE',
        sku: '602017001141910',
        selected: false,
      },
      storeListOpen: false,
      storeLocatorProps: {},
    },
    forms: {
      bundlesAddToBag: {},
      checkout: {
        account: {
          fields: {
            email: {
              value: 'rodderstestxx@test.com',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
            password: {
              value: 'rodderstestxx1',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
            passwordConfirm: {
              value: 'rodderstestxx1',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
            subscribe: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
          },
          isLoading: false,
          errors: {},
          message: null,
          user: null,
        },
        billingAddress: {
          fields: {
            address1: {
              value: '86 Tirrington',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
            address2: {
              value: 'Bretton',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
            city: {
              value: 'PETERBOROUGH',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
            country: {
              value: 'United Kingdom',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
            postcode: {
              value: 'PE3 9XT',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
            state: {
              value: null,
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
          },
          isLoading: false,
          errors: {},
          message: {},
        },
        billingCardDetails: {
          fields: {
            paymentType: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            cardNumber: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            expiryMonth: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            expiryYear: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            startMonth: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            startYear: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            cvv: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
          },
          isLoading: false,
          errors: {},
          message: {},
        },
        billingDetails: {
          fields: {
            title: {
              value: 'Ms',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
            firstName: {
              value: 'Jane',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
            lastName: {
              value: 'Doe',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
            telephone: {
              value: '01234567890',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
          },
          isLoading: false,
          errors: {},
          message: {},
        },
        billingFindAddress: {
          fields: {
            postCode: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            houseNumber: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            address: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            message: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            findAddress: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
          },
          isLoading: false,
          errors: {},
          message: {},
        },
        deliveryInstructions: {
          fields: {
            deliveryInstructions: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            smsMobileNumber: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
          },
          isLoading: false,
          errors: {},
          message: {},
        },
        findAddress: {
          fields: {
            postCode: {
              value: 'PE39XT',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
            houseNumber: {
              value: '86',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
            address: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            message: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            country: {
              value: 'United Kingdom',
              isDirty: true,
              isTouched: false,
              isFocused: false,
            },
            findAddress: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
          },
          isLoading: false,
          errors: {},
          message: {
            type: 'error',
            message: '',
          },
        },
        order: {
          fields: {
            isAcceptedTermsAndConditions: {
              value: true,
              isDirty: true,
              isTouched: false,
              isFocused: false,
            },
          },
          isLoading: false,
          errors: {},
          message: {},
        },
        yourAddress: {
          fields: {
            address1: {
              value: '86 Tirrington',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            address2: {
              value: 'Bretton',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            city: {
              value: 'PETERBOROUGH',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            country: {
              value: 'United Kingdom',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            postcode: {
              value: 'PE3 9XT',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            state: {
              value: null,
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
          },
          isLoading: false,
          errors: {},
          message: {},
        },
        yourDetails: {
          fields: {
            title: {
              value: 'Ms',
              isDirty: true,
              isTouched: false,
              isFocused: false,
            },
            firstName: {
              value: 'Jane',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
            lastName: {
              value: 'Doe',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
            telephone: {
              value: '01234567890',
              isDirty: true,
              isTouched: true,
              isFocused: false,
            },
          },
          isLoading: false,
          errors: {},
          message: {},
        },
      },
      changePassword: {
        fields: {
          email: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          oldPassword: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          newPassword: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          newPasswordConfirm: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
        },
        isLoading: false,
        errors: {},
        message: {},
      },
      cmsForm: {
        fields: {},
        isLoading: false,
        errors: {},
        message: {},
      },
      customerDetails: {
        fields: {
          title: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          firstName: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          lastName: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          telephone: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          country: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          postcode: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          address1: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          address2: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          city: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          state: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          type: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          cardNumber: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          expiryMonth: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          expiryYear: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
        },
        isLoading: false,
        errors: {},
        message: {},
        modalOpen: false,
      },
      customerShortProfile: {
        fields: {
          title: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          firstName: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          lastName: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          email: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
        },
        isLoading: false,
        errors: {},
        message: {},
      },
      forgetPassword: {
        fields: {
          email: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
        },
        isLoading: false,
        errors: {},
        message: {},
      },
      login: {
        fields: {
          email: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          password: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
        },
        isLoading: false,
        errors: {},
        message: {},
      },
      notifyStock: {
        fields: {
          firstName: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          surname: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          email: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          state: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
        },
        isLoading: false,
        errors: {},
        message: {},
      },
      promotionCode: {
        fields: {
          promotionCode: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
        },
        isLoading: false,
        errors: {},
        message: {},
        isVisible: true,
      },
      register: {
        fields: {
          email: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          password: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          passwordConfirm: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          subscribe: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
        },
        isLoading: false,
        errors: {},
        message: {},
      },
      search: {
        fields: {
          searchTerm: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
        },
        isLoading: false,
        errors: {},
        message: {},
      },
      storeDelivery: {
        fields: {
          postcode: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
        },
        isLoading: false,
        errors: {},
        message: {},
      },
      userLocator: {
        fields: {
          userLocation: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
        },
        isLoading: false,
        errors: {},
        message: {},
      },
      giftCard: {
        fields: {
          giftCardNumber: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
          pin: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
        },
        isLoading: false,
        errors: {},
        message: {},
        banner: false,
      },
      klarna: {
        fields: {
          email: {
            value: '',
            isDirty: false,
            isTouched: false,
            isFocused: false,
          },
        },
        isLoading: false,
        errors: {},
        message: {},
      },
    },
    plpContainer: {
      refreshPlp: true,
    },
    grid: {
      options: {
        default: [2, 3, 4],
        mobile: [1, 2, 3],
      },
      columns: 2,
    },
    infinityScroll: {
      currentPage: 1,
      isActive: true,
      preservedScroll: 250,
    },
    input: {
      firstName5: {
        isActive: false,
      },
      lastName6: {
        isActive: false,
      },
      telephone7: {
        isActive: false,
      },
      postCode9: {
        isActive: false,
      },
      houseNumber10: {
        isActive: false,
      },
      cardNumber29: {
        isActive: false,
      },
      cvv30: {
        isActive: false,
      },
      email34: {
        isActive: false,
      },
      password35: {
        isActive: false,
      },
      passwordConfirm36: {
        isActive: false,
      },
    },
    klarna: {
      sessionId: '',
      clientToken: '',
      loadFormPending: false,
      isUpdatingEmail: false,
      email: '',
      formSubmitted: false,
      authorizationToken: null,
    },
    loaderOverlay: {
      visible: false,
      ajaxCounter: 0,
    },
    localisation: {
      dictionary: {
        ' to view where we can leave your order if you are not in.':
          ' to view where we can leave your order if you are not in.',
        '| sec| min| hour| day| week| month| year|||':
          '| sec| min| hour| day| week| month| year|||',
        '| secs| mins| hours| days| weeks| months| years|||':
          '| secs| mins| hours| days| weeks| months| years|||',
        '/home': '/home',
        '& up': '& up',
        '${} out of 5 stars': '${} out of 5 stars',
        '${} as part of an outfit': '${} as part of an outfit',
        '${} has been removed from your bag':
          '${} has been removed from your bag',
        '${} added to bag': '${} added to bag',
        '${} characters remaining': '${} characters remaining',
        '07123 123123': '07123 123123',
        'A 15 digit card number is required':
          'A 15 digit card number is required',
        'A 16 digit card number is required':
          'A 16 digit card number is required',
        'A 3 digit CVV is required': 'A 3 digit CVV is required',
        'A 4 digit CVV is required': 'A 4 digit CVV is required',
        '3 digits required': '3 digits required',
        '4 digits required': '4 digits required',
        'A password is required.': 'A password is required.',
        'A phone number is required': 'A phone number is required',
        'Add All to Bag': 'Add All to Bag',
        'Add to bag': 'Add to bag',
        'Add your name or change your email address':
          'Add your name or change your email address',
        Address: 'Address',
        'Address Line 1': 'Address Line 1',
        'Address Line 2': 'Address Line 2',
        'An account with that email address already exists.':
          'An account with that email address already exists.',
        'An error has occured. Please try again.':
          'An error has occured. Please try again.',
        'An email address is required.': 'An email address is required.',
        Apply: 'Apply',
        APPLY: 'APPLY',
        'Apply card': 'Apply card',
        'Are you sure you want to remove ${} from your bag?':
          'Are you sure you want to remove ${} from your bag?',
        'Are you sure you want to remove all items from your bag?':
          'Are you sure you want to remove all items from your bag?',
        'Available in store by': 'Available in store by',
        'Back to checkout': 'Back to Checkout',
        'Back to My Account': 'Back to My Account',
        'Back to My Orders': 'Back to My Orders',
        'Back to shopping': 'Back to shopping',
        'Back to top': 'Back to top',
        Billing: 'Billing',
        'Billing address': 'Billing address',
        'Billing country': 'Billing country',
        'Billing Details': 'Billing Details',
        'Billing Options': 'Billing Options',
        'Bundle Code': 'Bundle Code',
        'Buy Now': 'Buy It Now',
        'Try before you buy': 'Try before you buy',
        'Call ${}': 'Call ${}',
        "Can't find country?": "Can't find country?",
        Cancel: 'Cancel',
        'Card Number': 'Card Number',
        Carrier: 'Carrier',
        'Carousel Image': 'Carousel Image',
        category: 'category',
        Change: 'Change',
        'Change shop': 'Change shop',
        'Change store': 'Change store',
        'Change delivery method': 'Change delivery method',
        'Change my details': 'Change my details',
        'Change password again?': 'Change password again?',
        'Change payment details': 'Change payment details',
        'Change shipping destination': 'Change shipping destination',
        'Change your account password': 'Change your account password',
        'change-your-shipping-destination': 'change-your-shipping-destination',
        'Check out with your PayPal account':
          'Check out with your PayPal account',
        'Check out with your IDEAL account':
          'Check out with your IDEAL account',
        'Check out with your SOFORT account':
          'Check out with your SOFORT account',
        'Check your saved delivery and payment details':
          'Check your saved delivery and payment details',
        'Checkout now': 'Checkout now',
        'Check eligibility': 'Check eligibility',
        'Choose country': 'Choose country',
        'Choose from': 'Choose from',
        Clear: 'Clear',
        'Clear all': 'Clear all',
        'clear filters': 'clear filters',
        'Clear form': 'Clear form',
        'Click here': 'Click here',
        'Click to select your address': 'Click to select your address',
        'Close filters modal': 'Close filters modal',
        'Close menu': 'Close menu',
        'Close modal (can also press escape key to close).':
          'Close modal (can also press escape key to close).',
        'Closed now.': 'Closed now.',
        'Closed today.': 'Closed today.',
        'Closing soon at': 'Closing soon at',
        Code: 'Code',
        'Collect from': 'Collect from',
        Colour: 'Colour',
        Confirm: 'Confirm',
        'Confirm email address': 'Confirm email address',
        'Confirm password': 'Confirm password',
        'Confirm new password': 'Confirm new password',
        'Confirm Password': 'Confirm Password',
        'CONFIRMATION DETAILS': 'CONFIRMATION DETAILS',
        'Confirmation details': 'Confirmation details',
        'Confirmation email': 'Confirmation email',
        'Contact information': 'Contact information',
        'Content is loading, please wait.': 'Content is loading, please wait.',
        'Continue shopping': 'Continue shopping',
        'Continue Shopping': 'Continue Shopping',
        'Continue to checkout': 'Continue to checkout',
        'Could not update customer details.':
          'Could not update customer details.',
        Country: 'Country',
        'Current password': 'Current password',
        CVV: 'CVV',
        'CVV/Security No.': 'CVV/Security No.',
        'Create a password to manage and track your order':
          'Create a password to manage and track your order',
        'Create password': 'Create password',
        'Debit and Credit Card': 'Debit and Credit Card',
        Delete: 'Delete',
        'Deliver to another store': 'Deliver to another store',
        'Deliver to another shop': 'Deliver to another shop',
        'Delivered to store by': 'Delivered to store by',
        Delivery: 'Delivery',
        'Delivery address': 'Delivery address',
        'Delivery and returns Information': 'Delivery and returns Information',
        'Delivery country': 'Delivery country',
        'Delivery Date': 'Delivery Date',
        'Delivery Details': 'Delivery Details',
        'DELIVERY INSTRUCTIONS': 'DELIVERY INSTRUCTIONS',
        'Delivery Instructions': 'Delivery Instructions',
        'Delivery Options': 'Delivery Options',
        'Delivery Type': 'Delivery Type',
        'Display product list items in ${} Columns':
          'Display product list items in ${} Columns',
        'Displaying your last 20 orders. For your full order history, please contact customer service at ${}.':
          'Displaying your last 20 orders. For your full order history, please contact customer service at ${}.',
        'Displaying your last 20 returns. For your full return history, please contact customer service at ${}.':
          'Displaying your last 20 returns. For your full return history, please contact customer service at ${}.',
        Doe: 'Doe',
        'Double tap for large view': 'Double tap for large view',
        'E-mail me when back in stock': 'E-mail me when back in stock',
        'E-receipts': 'E-receipts',
        Edit: 'Edit',
        'Eg. ***': 'Eg. ***',
        'Eg. 214': 'Eg. 214',
        'Eg. W1T 3NL': 'Eg. W1T 3NL',
        Email: 'Email',
        'Email address': 'Email address',
        'Empty bag': 'Empty bag',
        'Enter address manually': 'Enter address manually',
        'Enter your mobile number to receive SMS updates on your delivery.':
          'Enter your mobile number to receive SMS updates on your delivery.',
        'Enter your promotion code': 'Enter your promotion code',
        Error: 'Error',
        'Error with googleGetDetails': 'Error with googleGetDetails',
        'Estimated delivery is no later than':
          'Estimated delivery is no later than',
        'Example: Doe': 'Example: Doe',
        'Example: John': 'Example: John',
        'Example: XXX': 'Example: XXX',
        'Example: XXXX': 'Example: XXXX',
        'Example: XXXXXXX': 'Example : XXXXXXX',
        'example@domain.com': 'example@domain.com',
        'Expiry Date': 'Expiry Date',
        'Expiry: month': 'Expiry: month',
        'Expiry: year': 'Expiry: year',
        Filter: 'Filter',
        'Find a store': 'Find a store',
        'Find Address': 'Find Address',
        'FIND ADDRESS': 'FIND ADDRESS',
        'Find in store': 'Find in store',
        'First Name': 'First Name',
        'Follow Us': 'Follow Us',
        'For international customers, please call ${}.':
          'For international customers, please call ${}.',
        'Forgot your password?': 'Forgot your password?',
        'Forgotten your password?': 'Forgotten your password?',
        Free: 'Free',
        'Free Shipping*': 'Free Shipping*',
        friday: 'Friday',
        From: 'From',
        'Get Directions': 'Get Directions',
        'Get my current location': 'Get my current location',
        'Get your goods today and pay 3 months later':
          'Get your goods today and pay 3 months later',
        'Gift card': 'Gift card',
        'Gift Card': 'Gift Card',
        'Gift card number': 'Gift card number',
        'Gift Card Number': 'Gift Card Number',
        'Giftcard number needs to be 16 characters long.':
          'Giftcard number needs to be 16 characters long.',
        'Giftcard PIN needs to be 4 characters long.':
          'Giftcard PIN needs to be 4 characters long.',
        Go: 'Go',
        'Go back': 'Go back',
        'Go to Checkout': 'Go to Checkout',
        'GO TO CHECKOUT': 'GO TO CHECKOUT',
        'Go to checkout': 'Go to checkout',
        'Help & Information': 'Help & Information',
        Hide: 'Hide',
        Home: 'Home',
        homepage: 'homepage',
        'House name or number': 'House name or number',
        'House number': 'House number',
        'I accept the': 'I accept the',
        "I'm looking for a store in": "I'm looking for a store in",
        'Image carousel, press Enter to cycle through the images, press the Z key to zoom in and out of an image. Once zoomed in, use WASD keys to pan around image.':
          'Image carousel, press Enter to cycle through the images, press the Z key to zoom in and out of an image. Once zoomed in, use WASD keys to pan around image.',
        'In Stock': 'In Stock',
        'including delivery': 'including delivery',
        'Item code': 'Item code',
        'Item price': 'Item price',
        'item(s) added to bag': 'item(s) added to bag',
        John: 'John',
        kilometres: 'kilometres',
        'Last Name': 'Last Name',
        left: 'left',
        'Left arrow': 'Left arrow',
        'Let us know where we can leave your order if you are not in.':
          'Let us know where we can leave your order if you are not in.',
        Loading: 'Loading',
        'loading products': 'loading products',
        Login: 'Login',
        'logout worked but the api sent back errors':
          'logout worked but the api sent back errors',
        'Low stock': 'Low stock',
        'Manage your E-receipts here': 'Manage your E-receipts here',
        'Max. 30 characters': 'Max. 30 characters',
        'Max. 30 characters ${} remaining': 'Max. 30 characters ${} remaining',
        miles: 'miles',
        Mobile: 'Mobile',
        'MOBILE NUMBER': 'MOBILE NUMBER',
        'Mobile Number': 'Mobile Number',
        monday: 'Monday',
        'Monday to Friday': 'Monday to Friday',
        'MY ACCOUNT': 'MY ACCOUNT',
        'My account': 'My account',
        'My Bag': 'My Bag',
        'My checkout details': 'My checkout details',
        'My details': 'My details',
        'My Order': 'My Order',
        'My orders': 'My orders',
        'My returns': 'My returns',
        'My wishlists': ' My wishlists',
        'My password': 'My password',
        'my-account': 'my-account',
        'New Customer?': 'New Customer?',
        'New password': 'New password',
        Next: 'Next',
        'Not available at this store': 'Not available at this store',
        'Notify me when available': 'Notify me when available',
        Now: 'Now',
        'On the Blog': 'On the Blog',
        'One Column': 'One Column',
        'Only digits allowed': 'Only digits allowed',
        'Only ${} left in stock': 'Only ${} left in stock',
        'Open categories menu': 'Open categories menu',
        'Open search bar to search for products':
          'Open search bar to search for products',
        'Open today until': 'Open today until',
        'Opening hours': 'Opening hours',
        'Opens today at': 'Opens today at',
        'Opens tomorrow at': 'Opens tomorrow at',
        'Order and Pay Now': 'Order and Pay Now',
        'Order details': 'Order details',
        'Order date': 'Order date',
        'Order not found': 'Order not found',
        'Order Number': 'Order Number',
        'Order summary': 'Order Summary',
        'Order Summary and Confirmation': 'Order Summary and Confirmation',
        'Out of stock': 'Out of stock',
        Ok: 'Ok',
        outfit: 'outfit',
        'Outfit View': 'Outfit View',
        Password: 'Password',
        'Password cannot be your email': 'Password cannot be your email',
        'Passwords must have at least six characters including one digit.':
          'Passwords must have at least six characters including one digit.',
        'Pay via PayPal': 'Pay via PayPal',
        'Pay via Klarna': 'Try before you buy',
        'PAYMENT DETAILS': 'PAYMENT DETAILS',
        'Payment details': 'Payment details',
        'Payment type': 'Payment type',
        Payment: 'Payment',
        PIN: 'PIN',
        'Please choose your shipping destination':
          'Please choose your shipping destination',
        'Please click on FIND ADDRESS': 'Please click on FIND ADDRESS',
        'Please collect within 10 days': 'Please collect within 10 days',
        'Please ensure that both passwords match':
          'Please ensure that both passwords match',
        'Please ensure that both passwords match.':
          'Please ensure that both passwords match.',
        'Please ensure that your password does not contain your email address.':
          'Please ensure that your password does not contain your email address.',
        'Please enter your password or try another email address.':
          'Please enter your password or try another email address.',
        'Please enter a password containing at least one number':
          'Please enter a password containing at least one number',
        'Please enter a password less than 20 characters.':
          'Please enter a password less than 20 characters.',
        'Please enter a password of at least 6 characters.':
          'Please enter a password of at least 6 characters.',
        'Please enter a valid email address.':
          'Please enter a valid email address.',
        'Please enter a valid gift card number':
          'Please enter a valid gift card number',
        'Please enter a valid phone number':
          'Please enter a valid phone number',
        'Please enter a valid post code': 'Please enter a valid post code',
        'Please enter a valid UK phone number':
          'Please enter a valid UK phone number',
        'PLEASE NOTE: If you have items in your basket they will be transferred, but will be subject to local pricing and promotions. Some items may not be suitable for sale in your selected region and will be removed.':
          'PLEASE NOTE: If you have items in your basket they will be transferred, but will be subject to local pricing and promotions. Some items may not be suitable for sale in your selected region and will be removed.',
        'Please note, for security reasons you will need to update your payment details each time you update your delivery information.':
          'Please note, for security reasons you will need to update your payment details each time you update your delivery information.',
        'Please note, you can update your billing details when you check out.':
          'Please note, you can update your billing details when you check out.',
        'Please provide your login details or try an other email address':
          'Please provide your login details or try an other email address',
        'Please select': 'Please select',
        'Please select a nominated delivery date:':
          'Please select a nominated delivery date:',
        'Please select a valid expiry date':
          'Please select a valid expiry date',
        'Please select your address...': 'Please select your address...',
        'Please select your country': 'Please select your country',
        'Please select your size to continue':
          'Please select your size to continue',
        'Please sign me up for emails': 'Please sign me up for emails',
        'Postal Code': 'Postal Code',
        'Postal Index Number': 'Postal Index Number',
        Postcode: 'Postcode',
        Previous: 'Previous',
        Price: 'Price',
        'Primary Phone Number': 'Primary Phone Number',
        Proceed: 'Proceed',
        'Proceed to Payment': 'Proceed to Payment',
        product: 'product',
        'Product Code': 'Product Code',
        'Product details': 'Product details',
        'Product information loading': 'Product information loading',
        'Product View': 'Product View',
        'Promo & Student Codes': 'Promo & Student Codes',
        'Promotion Code or Discount': 'Promotion Code or Discount',
        'Promotion Code or Student discount':
          'Promotion Code or Student discount',
        'Promotion or Student Discount': 'Promotion or Student Discount',
        'Provide us with your email address and we’ll let you know when the item becomes available.':
          'Provide us with your email address and we’ll let you know when the item becomes available.',
        'Provide us with your email address and we’ll let you know when this item arrives on':
          'Provide us with your email address and we’ll let you know when this item arrives on',
        Quantity: 'Quantity',
        'Read reviews': 'Read reviews',
        'Read More': 'Read More',
        'Recently viewed': 'Recently viewed',
        Register: 'Register',
        'Register and Pay Now': 'Register and Pay Now',
        'Register and Pay via PayPal': 'Register and Pay via PayPal',
        'Register and Pay Later': 'Register and Pay Later',
        Reload: 'Reload',
        Remove: 'Remove',
        'Remove this gift card': 'Remove this gift card',
        'Reset Form': 'Reset Form',
        'Reset password': 'Reset password',
        results: 'results',
        'REVIEW ORDER AND CONFIRM': 'REVIEW ORDER AND CONFIRM',
        'Review order and confirm': 'Review order and confirm',
        right: 'right',
        RRP: 'RRP',
        'Same as delivery address': 'Same as delivery address',
        Saturday: 'Saturday',
        Save: 'Save',
        'SAVE CHANGES': 'SAVE CHANGES',
        'Save email address': 'Save email address',
        'Save for later': 'Save for later',
        Search: 'Search',
        'Search - ${}': 'Search - ${}',
        'Search again': 'Search again',
        'Search for a product...': 'Search for a product...',
        'Search for store...': 'Search for store...',
        'Search, e.g. denim jacket': 'Search, e.g. denim jacket',
        'See full details': 'See full details',
        'See More': 'See More',
        'See Prev': 'See Prev',
        'Secure checkout': 'Secure checkout',
        'Select a size': 'Select a size',
        'Select size': 'Select size',
        'Select store': 'Select store',
        'Select your items': 'Select your items',
        'Select your preferred delivery option':
          'Select your preferred delivery option',
        'Session Expired': 'Session Expired',
        'Shop By Category': 'Shop By Category',
        Show: 'Show',
        'Show more': 'Show more',
        'Sign In': 'Sign In',
        'Sign in or Register': 'Sign in or Register',
        'Sign out': 'Sign out',
        'sign-in-or-register': 'sign-in-or-register',
        'Sign In to My Account or Proceed as Guest':
          'Sign In to My Account or Proceed as Guest',
        'Sign Up for news straight to your inbox':
          'Sign Up for news straight to your inbox',
        'Similar product recommendations, press the S key to stop automatic cycling. Press D to cycle forwards through the products, or A to cycle backwards':
          'Similar product recommendations, press the S key to stop automatic cycling. Press D to cycle forwards through the products, or A to cycle backwards',
        Size: 'Size',
        size: 'size',
        'Size guide': 'Size guide',
        'Skip to content': 'Skip to content',
        'Sorry your search didn’t match any products.':
          'Sorry your search didn’t match any products.',
        "Sorry, there's been an error with loading the page. Please try again later":
          "Sorry, there's been an error with loading the page. Please try again later",
        'Sorry, this item is out of stock': 'Sorry, this item is out of stock',
        "Sorry, we couldn't find any locations matching your search. Please enter another location and try again.":
          "Sorry, we couldn't find any locations matching your search. Please enter another location and try again.",
        'Sort product list by': 'Sort product list by',
        'Standard Delivery': 'Standard Delivery',
        stars: 'stars',
        State: 'State',
        Status: 'Status',
        'Street Address': 'Street Address',
        'Store Locator': 'Store Locator',
        'Store opening times': 'Store opening times',
        Submit: 'Submit',
        Submitted: 'Submitted',
        Submitting: 'Submitting',
        Subtotal: 'Subtotal',
        'Subtotal before Delivery': 'Subtotal before Delivery',
        Summary: 'Summary',
        Sunday: 'Sunday',
        Surname: 'Surname',
        tcs: 'tcs',
        'Terms & Conditions': 'Terms & Conditions',
        'Thank you for submitting your details.':
          'Thank you for submitting your details.',
        'Thank you for updating your details.':
          'Thank you for updating your details.',
        'Thank You For Your Order': 'Thank You For Your Order',
        'Thank you, your gift card has been added.':
          'Thank you, your gift card has been added.',
        'The buy now, pay later payment option is not available for this order. Please select an alternative payment option.':
          'The buy now, pay later payment option is not available for this order. Please select an alternative payment option.',
        'The promotion code you have entered has not been recognised. Please confirm the code and try again.':
          'The promotion code you have entered has not been recognised. Please confirm the code and try again.',
        'There are no additional delivery options available':
          'There are no additional delivery options available',
        'There was an error submitting the form. Please try again.':
          'There was an error submitting the form. Please try again.',
        'There were no orders found': 'There were no orders found',
        'There are no returns found': 'There are no returns found',
        "There's been a temporary issue. Please confirm your order again.":
          "There's been a temporary issue. Please confirm your order again.",
        'This field is required': 'This field is required',
        'This is the product page for: ${}':
          'This is the product page for: ${}',
        'This is the quick product view page for: ${}':
          'This is the quick product view page for: ${}',
        'This is the filters modal. Select any required filters and press apply to return to a filtered product list.':
          'This is the filters modal. Select any required filters and press apply to return to a filtered product list.',
        'Three Columns': 'Three Columns',
        thursday: 'Thursday',
        Title: 'Title',
        Total: 'Total',
        'Total left to pay': 'Total left to pay',
        'TOPSHOP UNIQUE SHOW STARTS IN': 'TOPSHOP UNIQUE SHOW STARTS IN',
        'Total cost': 'Total cost',
        'Total Cost': 'Total Cost',
        'Total cost incl. delivery': 'Total cost incl. delivery',
        'Town or Locality': 'Town or Locality',
        'Town/City': 'Town/City',
        'Track my order': 'Track my order',
        'Track your current orders and view your order history':
          'Track your current orders and view your order history',
        'Tracking number': 'Tracking number',
        'Try another email address': 'Try another email address',
        'Two Columns': 'Two Columns',
        tuesday: 'Tuesday',
        'Unable to find order, please check your account':
          'Unable to find order, please check your account',
        'Unfortunately your session has timed out.':
          'Unfortunately your session has timed out.',
        'Update details': 'Update details',
        'Use for Billing Address': 'Use for Billing Address',
        'Use our quick checkout process': 'Use our quick checkout process',
        VERIFY: 'VERIFY',
        'View bag': 'View bag',
        'View in maps': 'View in maps',
        'View next image': 'View next image',
        'View previous image': 'View previous image',
        'View product information': 'View product information',
        'View shopping cart': 'View shopping cart',
        Was: 'Was',
        'We are unable to find your address at the moment. Please enter your address manually.':
          'We are unable to find your address at the moment. Please enter your address manually.',
        'We ship to over 100 countries': 'We ship to over 100 countries',
        "We'll email you when it's ready to collect.":
          "We'll email you when it's ready to collect.",
        wednesday: 'Wednesday',
        "What's a CVV?": "What's a CVV?",
        'Which size are you looking for?': 'Which size are you looking for?',
        'Where do you want to collect from?':
          'Where do you want to collect from?',
        'Why not try?': 'Why not try?',
        'You have items in your bag that are no longer available. Please review your bag before payment.':
          'You have items in your bag that are no longer available. Please review your bag before payment.',
        'You have items in your shopping bag from a previous visit.':
          'You have items in your shopping bag from a previous visit.',
        'You have items in your shopping bag from a previous visit. Please review the items in your bag before continuing to payment.':
          'You have items in your shopping bag from a previous visit. Please review the items in your bag before continuing to payment.',
        'You have out of stock item(s) in your bag. Please remove them before you continue your checkout.':
          'You have out of stock item(s) in your bag. Please remove them before you continue your checkout.',
        'You must accept the Terms and Conditions.':
          'You must accept the Terms and Conditions.',
        'You successfully logged in with ${}':
          'You successfully logged in with ${}',
        'You have added the maximum number of gift cards for this order.':
          'You have added the maximum number of gift cards for this order.',
        'You will be asked to log onto ${} to confirm your order':
          'You will be asked to log onto ${} to confirm your order',
        "You'll get an order confirmation email shortly.":
          "You'll get an order confirmation email shortly.",
        "You'll have a chance to review your order and check your delivery & billing information on the next screen before ordering.":
          "You'll have a chance to review your order and check your delivery & billing information on the next screen before ordering",
        "You'll have a chance to review your order, add promotion codes, and check your delivery & billing information on the next screen before ordering.":
          "You'll have a chance to review your order, add promotion codes, and check your delivery & billing information on the next screen before ordering.",
        'Your bag': 'Your bag',
        'Your billing address is the address at which the card you are paying with is registered to. Please enter this as it appears on your bank or credit card statement.':
          'Your billing address is the address at which the card you are paying with is registered to. Please enter this as it appears on your bank or credit card statement.',
        'Your Delivery Details': 'Your Delivery Details',
        'Your Details': 'Your Details',
        'Your discount': 'Your discount',
        'Your item has been added to your bag':
          'Your item has been added to your bag',
        "Your new password can't be the same as your previous password.":
          "Your new password can't be the same as your previous password.",
        'Your order': 'Your order',
        'Your Order is now complete. Redirecting to your Order History ...':
          'Your Order is now complete. Redirecting to your Order History ...',
        'Your Order is Complete': 'Your Order is Complete',
        'Your order will be delivered by': 'Your order will be delivered by',
        'Your order will be delivered on': 'Your order will be delivered on',
        'Your order will be delivered to': 'Your order will be delivered to',
        'Your order will arrive': 'Your order will arrive',
        'Your password has been successfully changed.':
          'Your password has been successfully changed.',
        "Your payment type has expired. Please update your payment details from the 'Change' option above.":
          "Your payment type has expired. Please update your payment details from the 'Change' option above.",
        'Your profile details have been successfully updated.':
          'Your profile details have been successfully updated.',
        'Your promo code has been applied': 'Your promo code has been applied',
        'Your shopping bag is currently empty.':
          'Your shopping bag is currently empty.',
        'Zip Code': 'Postcode',
        'THE RUNWAY': 'THE RUNWAY',
        BACKSTAGE: 'BACKSTAGE',
        'THE MARKET': 'THE MARKET',
        'TOPSHOP UNIQUE SHOP': 'TOPSHOP UNIQUE SHOP',
        'The catwalk runs directly through Spitafield’s market – with room to house over 700 fashion editors and celebrities on the front row. Want to watch too? You can tune in to the livestream at 2pm BST.':
          'The catwalk runs directly through Spitafield’s market – with room to house over 700 fashion editors and celebrities on the front row. Want to watch too? You can tune in to the livestream at 2pm BST.',
        'Hair, make-up, final touches to the styling… there’s a lot that goes into making the Topshop Unique show look this good. Add us @topshop_snaps to get an insider look at the backstage action.':
          'Hair, make-up, final touches to the styling… there’s a lot that goes into making the Topshop Unique show look this good. Add us @topshop_snaps to get an insider look at the backstage action.',
        'We’ve curated exclusive market stalls open to all. Get your jeans customised, shop catwalk beauty looks and more…':
          'We’ve curated exclusive market stalls open to all. Get your jeans customised, shop catwalk beauty looks and more…',
        'Open to the public from 3pm, this market stall will sell pieces from the collection straight after the show.':
          'Open to the public from 3pm, this market stall will sell pieces from the collection straight after the show.',
        'Find out more': 'Find out more',
        'http://insideout.topshop.com/2016/08/topshop-uniques-new-home-old-spitalfields-market':
          'http://insideout.topshop.com/2016/08/topshop-uniques-new-home-old-spitalfields-market',
        '/runwaytoretail': '/runwaytoretail',
        '//topshoplive.akamaized.net/ss2017/ts_unique/?dev=1&stage=1&display=mobile&l=uk':
          '//topshoplive.akamaized.net/ss2017/ts_unique/?dev=1&stage=1&display=mobile&l=uk',
        'One sized item': 'One sized item',
        'Please remove all emoji characters':
          'Please remove all emoji characters',
        'Your order will be delivered no later than':
          'Your order will be delivered no later than',
      },
    },
    modal: {
      open: false,
      mode: 'normal',
      type: 'dialog',
      children: [],
      entryPoint: null,
    },
    navigation: {
      menuLinks: [
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 1,
          label: 'Shop By Category',
          categoryId: 3370518,
          categoryFilter: '3370518',
          seoUrl: '/en/tsuk/category/shop-by-category-5634548',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 1,
          label: 'New In Fashion',
          categoryId: 277012,
          categoryFilter: '208491,277012',
          seoUrl:
            '/en/tsuk/category/new-in-this-week-2169932/new-in-fashion-6367514',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 2,
          label: 'New In Dresses',
          categoryId: 2724496,
          categoryFilter: '208491,2724496',
          seoUrl:
            '/en/tsuk/category/new-in-this-week-2169932/new-in-dresses-4938909',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 3,
          label: 'New In Lingerie & Swim',
          categoryId: 3454190,
          categoryFilter: '208491,3454190',
          seoUrl:
            '/en/tsuk/category/new-in-this-week-2169932/new-in-lingerie-swim-6354962',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 4,
          label: 'New In Shoes',
          categoryId: 1780842,
          categoryFilter: '208491,1780842',
          seoUrl:
            '/en/tsuk/category/new-in-this-week-2169932/new-in-shoes-3105666',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 5,
          label: 'New In Bags & Accessories',
          categoryId: 1780853,
          categoryFilter: '208491,1780853',
          seoUrl:
            '/en/tsuk/category/new-in-this-week-2169932/new-in-bags-accessories-3105675',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 6,
          label: 'New In Petite, Tall & Maternity',
          categoryId: 3429092,
          categoryFilter: '208491,3429092',
          seoUrl:
            '/en/tsuk/category/new-in-this-week-2169932/new-in-petite-tall-maternity-6044985',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 7,
          label: 'New In Brands',
          categoryId: 2290040,
          categoryFilter: '208491,2290040',
          seoUrl:
            '/en/tsuk/category/new-in-this-week-2169932/new-in-brands-4293441',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 1,
          label: 'Dresses',
          categoryId: 208523,
          categoryFilter: '203984,208523',
          seoUrl: '/en/tsuk/category/clothing-427/dresses-442',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 2,
          label: 'Jeans',
          categoryId: 208527,
          categoryFilter: '203984,208527',
          seoUrl: '/en/tsuk/category/clothing-427/jeans-446',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 3,
          label: 'Shirts & Blouses',
          categoryId: 2512228,
          categoryFilter: '203984,2512228',
          seoUrl: '/en/tsuk/category/clothing-427/shirts-blouses-4650801',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 4,
          label: 'Jumpers & Cardigans',
          categoryId: 208525,
          categoryFilter: '203984,208525',
          seoUrl: '/en/tsuk/category/clothing-427/jumpers-cardigans-6924635',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 5,
          label: 'T-Shirts',
          categoryId: 3492547,
          categoryFilter: '203984,3492547',
          seoUrl: '/en/tsuk/category/clothing-427/t-shirts-6864659',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 6,
          label: 'Camis & Vests',
          categoryId: 3492548,
          categoryFilter: '203984,3492548',
          seoUrl: '/en/tsuk/category/clothing-427/camis-vests-6864668',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 7,
          label: 'Bodysuits',
          categoryId: 3492533,
          categoryFilter: '203984,3492533',
          seoUrl: '/en/tsuk/category/clothing-427/bodysuits-6864633',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 8,
          label: 'Jackets & Coats',
          categoryId: 208526,
          categoryFilter: '203984,208526',
          seoUrl: '/en/tsuk/category/clothing-427/jackets-coats-2390889',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 9,
          label: 'Denim',
          categoryId: 525523,
          categoryFilter: '203984,525523',
          seoUrl: '/en/tsuk/category/clothing-427/denim-4889473',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 10,
          label: 'Hoodies & Sweats',
          categoryId: 3492546,
          categoryFilter: '203984,3492546',
          seoUrl: '/en/tsuk/category/clothing-427/hoodies-sweats-6864676',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 11,
          label: 'Trousers & Leggings',
          categoryId: 208528,
          categoryFilter: '203984,208528',
          seoUrl: '/en/tsuk/category/clothing-427/trousers-leggings-4075710',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 12,
          label: 'Playsuits & Jumpsuits',
          categoryId: 208531,
          categoryFilter: '203984,208531',
          seoUrl: '/en/tsuk/category/clothing-427/playsuits-jumpsuits-2159081',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 13,
          label: 'Shorts',
          categoryId: 208529,
          categoryFilter: '203984,208529',
          seoUrl: '/en/tsuk/category/clothing-427/shorts-448',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 14,
          label: 'Skirts',
          categoryId: 208530,
          categoryFilter: '203984,208530',
          seoUrl: '/en/tsuk/category/clothing-427/skirts-449',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 15,
          label: 'Swimwear & Beachwear',
          categoryId: 208534,
          categoryFilter: '203984,208534',
          seoUrl: '/en/tsuk/category/clothing-427/swimwear-beachwear-3163078',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 16,
          label: 'Lingerie & Nightwear',
          categoryId: 3497086,
          categoryFilter: '203984,3497086',
          seoUrl: '/en/tsuk/category/clothing-427/lingerie-nightwear-6914422',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 17,
          label: 'Socks & Tights',
          categoryId: 208532,
          categoryFilter: '203984,208532',
          seoUrl: '/en/tsuk/category/clothing-427/socks-tights-3283666',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 18,
          label: 'Topshop Unique',
          categoryId: 2205494,
          categoryFilter: '203984,2205494',
          seoUrl: '/en/tsuk/category/clothing-427/topshop-unique-6924716',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 19,
          label: 'Topshop Boutique',
          categoryId: 208521,
          categoryFilter: '203984,208521',
          seoUrl: '/en/tsuk/category/clothing-427/topshop-boutique-6924724',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 20,
          label: 'Petite',
          categoryId: 208536,
          categoryFilter: '203984,208536',
          seoUrl: '/en/tsuk/category/clothing-427/petite-455',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 21,
          label: 'Tall',
          categoryId: 208535,
          categoryFilter: '203984,208535',
          seoUrl: '/en/tsuk/category/clothing-427/tall-454',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 22,
          label: 'Maternity',
          categoryId: 208537,
          categoryFilter: '203984,208537',
          seoUrl: '/en/tsuk/category/clothing-427/maternity-456',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 1,
          label: 'Shop All Jeans',
          categoryId: 3496082,
          categoryFilter: '3493110,3496082',
          seoUrl: '/en/tsuk/category/jeans-6877054/shop-all-jeans-6906718',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 2,
          label: 'Shop All Denim',
          categoryId: 3496110,
          categoryFilter: '3493110,3496110',
          seoUrl: '/en/tsuk/category/jeans-6877054/shop-all-denim-6906760',
          redirectionUrl: '/en/tsuk/category/clothing-427/denim-4889473',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 3,
          label: 'Jamie Jeans',
          categoryId: 3496007,
          categoryFilter: '3493110,3496007',
          seoUrl: '/en/tsuk/category/jeans-6877054/jamie-jeans-6906622',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 4,
          label: 'Joni Jeans',
          categoryId: 3496006,
          categoryFilter: '3493110,3496006',
          seoUrl: '/en/tsuk/category/jeans-6877054/joni-jeans-6906608',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 5,
          label: 'Leigh Jeans',
          categoryId: 3496048,
          categoryFilter: '3493110,3496048',
          seoUrl: '/en/tsuk/category/jeans-6877054/leigh-jeans-6906654',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 6,
          label: 'Sidney Jeans',
          categoryId: 3496038,
          categoryFilter: '3493110,3496038',
          seoUrl: '/en/tsuk/category/jeans-6877054/sidney-jeans-6906638',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 7,
          label: 'Mom Jeans',
          categoryId: 3496011,
          categoryFilter: '3493110,3496011',
          seoUrl: '/en/tsuk/category/jeans-6877054/mom-jeans-6906630',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 8,
          label: 'Hayden Jeans',
          categoryId: 3496079,
          categoryFilter: '3493110,3496079',
          seoUrl: '/en/tsuk/category/jeans-6877054/hayden-jeans-6906694',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 9,
          label: 'Baxter Jeans',
          categoryId: 3496052,
          categoryFilter: '3493110,3496052',
          seoUrl: '/en/tsuk/category/jeans-6877054/baxter-jeans-6906662',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 10,
          label: 'Lucas Jeans',
          categoryId: 3496063,
          categoryFilter: '3493110,3496063',
          seoUrl: '/en/tsuk/category/jeans-6877054/lucas-jeans-6906686',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 11,
          label: 'Orson Jeans',
          categoryId: 3496069,
          categoryFilter: '3493110,3496069',
          seoUrl: '/en/tsuk/category/jeans-6877054/orson-jeans-6906670',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 12,
          label: 'Dree Jeans',
          categoryId: 3496067,
          categoryFilter: '3493110,3496067',
          seoUrl: '/en/tsuk/category/jeans-6877054/dree-jeans-6906678',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 13,
          label: 'Straight Leg Jeans',
          categoryId: 3496099,
          categoryFilter: '3493110,3496099',
          seoUrl: '/en/tsuk/category/jeans-6877054/straight-leg-jeans-6906746',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 14,
          label: 'Skinny Jeans',
          categoryId: 3496096,
          categoryFilter: '3493110,3496096',
          seoUrl: '/en/tsuk/category/jeans-6877054/skinny-jeans-6906726',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 15,
          label: 'Crop Jeans',
          categoryId: 3496035,
          categoryFilter: '3493110,3496035',
          seoUrl: '/en/tsuk/category/jeans-6877054/crop-jeans-6906646',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 16,
          label: 'Flared Jeans',
          categoryId: 3497032,
          categoryFilter: '3493110,3497032',
          seoUrl: '/en/tsuk/category/jeans-6877054/flared-jeans-6908704',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 17,
          label: 'Wide Leg Jeans',
          categoryId: 3496088,
          categoryFilter: '3493110,3496088',
          seoUrl: '/en/tsuk/category/jeans-6877054/wide-leg-jeans-6906703',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 18,
          label: 'Ripped Jeans',
          categoryId: 3496186,
          categoryFilter: '3493110,3496186',
          seoUrl: '/en/tsuk/category/jeans-6877054/ripped-jeans-6906866',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 19,
          label: 'Embroidered Jeans',
          categoryId: 3496183,
          categoryFilter: '3493110,3496183',
          seoUrl: '/en/tsuk/category/jeans-6877054/embroidered-jeans-6906894',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 20,
          label: 'Tall Jeans',
          categoryId: 3496154,
          categoryFilter: '3493110,3496154',
          seoUrl: '/en/tsuk/category/jeans-6877054/tall-jeans-6906816',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 21,
          label: 'Petite Jeans',
          categoryId: 3496160,
          categoryFilter: '3493110,3496160',
          seoUrl: '/en/tsuk/category/jeans-6877054/petite-jeans-6906842',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 22,
          label: 'Maternity Jeans',
          categoryId: 3496187,
          categoryFilter: '3493110,3496187',
          seoUrl: '/en/tsuk/category/jeans-6877054/maternity-jeans-6906874',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 23,
          label: 'The Jeans Fit Guide',
          categoryId: 3496168,
          categoryFilter: '3493110,3496168',
          seoUrl: '/en/tsuk/category/jeans-6877054/the-jeans-fit-guide-6906850',
          redirectionUrl:
            '/en/tsuk/category/jeans-fit-guide-6338428/home?TS=1487259856055',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 24,
          label: 'The Denim Shorts Fit Guide',
          categoryId: 3496177,
          categoryFilter: '3493110,3496177',
          seoUrl:
            '/en/tsuk/category/jeans-6877054/the-denim-shorts-fit-guide-6906858',
          redirectionUrl:
            '/en/tsuk/category/shorts-fit-guide-6445047/home?TS=1490611612567',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 1,
          label: 'Shop All Shoes',
          categoryId: 331499,
          categoryFilter: '208492,331499',
          seoUrl: '/en/tsuk/category/shoes-430/shop-all-shoes-6909322',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 2,
          label: 'Heels',
          categoryId: 208542,
          categoryFilter: '208492,208542',
          seoUrl: '/en/tsuk/category/shoes-430/heels-458',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 3,
          label: 'Boots',
          categoryId: 3339357,
          categoryFilter: '208492,3339357',
          seoUrl: '/en/tsuk/category/shoes-430/boots-6909314',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 4,
          label: 'Trainers',
          categoryId: 3216551,
          categoryFilter: '208492,3216551',
          seoUrl: '/en/tsuk/category/shoes-430/trainers-5399321',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 5,
          label: 'Sliders',
          categoryId: 3462494,
          categoryFilter: '208492,3462494',
          seoUrl: '/en/tsuk/category/shoes-430/sliders-6467817',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 6,
          label: 'Flats',
          categoryId: 208543,
          categoryFilter: '208492,208543',
          seoUrl: '/en/tsuk/category/shoes-430/flats-459',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 7,
          label: 'Loafers',
          categoryId: 3212595,
          categoryFilter: '208492,3212595',
          seoUrl: '/en/tsuk/category/shoes-430/loafers-5928805',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 8,
          label: 'Sandals',
          categoryId: 3212506,
          categoryFilter: '208492,3212506',
          seoUrl: '/en/tsuk/category/shoes-430/sandals-5388227',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 9,
          label: 'Ankle Boots',
          categoryId: 2759103,
          categoryFilter: '208492,2759103',
          seoUrl: '/en/tsuk/category/shoes-430/ankle-boots-4979409',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 10,
          label: 'Over The Knee Boots',
          categoryId: 2751541,
          categoryFilter: '208492,2751541',
          seoUrl: '/en/tsuk/category/shoes-430/over-the-knee-boots-6909328',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 11,
          label: 'Metallic Shoes',
          categoryId: 3496999,
          categoryFilter: '208492,3496999',
          seoUrl: '/en/tsuk/category/shoes-430/metallic-shoes-6909333',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 12,
          label: 'Going Out Shoes',
          categoryId: 2751577,
          categoryFilter: '208492,2751577',
          seoUrl: '/en/tsuk/category/shoes-430/going-out-shoes-4967245',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 13,
          label: 'OFFICE at Topshop',
          categoryId: 3438529,
          categoryFilter: '208492,3438529',
          seoUrl: '/en/tsuk/category/shoes-430/office-at-topshop-6909341',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 1,
          label: 'Shop All Accessories',
          categoryId: 3497014,
          categoryFilter: '204484,3497014',
          seoUrl:
            '/en/tsuk/category/bags-accessories-1702216/shop-all-accessories-6909359',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 2,
          label: 'Bags & Purses',
          categoryId: 208548,
          categoryFilter: '204484,208548',
          seoUrl: '/en/tsuk/category/bags-accessories-1702216/bags-purses-462',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 3,
          label: 'Sunglasses',
          categoryId: 208555,
          categoryFilter: '204484,208555',
          seoUrl: '/en/tsuk/category/bags-accessories-1702216/sunglasses-468',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 4,
          label: 'Jewellery',
          categoryId: 208556,
          categoryFilter: '204484,208556',
          seoUrl: '/en/tsuk/category/bags-accessories-1702216/jewellery-469',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 5,
          label: 'Watches',
          categoryId: 1500007,
          categoryFilter: '204484,1500007',
          seoUrl: '/en/tsuk/category/bags-accessories-1702216/watches-2497426',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 6,
          label: 'Hats',
          categoryId: 208549,
          categoryFilter: '204484,208549',
          seoUrl: '/en/tsuk/category/bags-accessories-1702216/hats-463',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 7,
          label: 'Scarves',
          categoryId: 208552,
          categoryFilter: '204484,208552',
          seoUrl: '/en/tsuk/category/bags-accessories-1702216/scarves-465',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 8,
          label: 'Belts',
          categoryId: 208553,
          categoryFilter: '204484,208553',
          seoUrl: '/en/tsuk/category/bags-accessories-1702216/belts-466',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 9,
          label: 'Socks & Tights',
          categoryId: 1006498,
          categoryFilter: '204484,1006498',
          seoUrl:
            '/en/tsuk/category/bags-accessories-1702216/socks-tights-3283675',
          redirectionUrl: '/en/tsuk/category/clothing-427/tights-socks-451',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 10,
          label: 'Hair Accessories',
          categoryId: 208551,
          categoryFilter: '204484,208551',
          seoUrl:
            '/en/tsuk/category/bags-accessories-1702216/hair-accessories-464',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 11,
          label: 'Books & Stationery',
          categoryId: 3497040,
          categoryFilter: '204484,3497040',
          seoUrl:
            '/en/tsuk/category/bags-accessories-1702216/books-stationery-6934143',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 12,
          label: 'iPhone Accessories',
          categoryId: 2223012,
          categoryFilter: '204484,2223012',
          seoUrl:
            '/en/tsuk/category/bags-accessories-1702216/iphone-accessories-4582259',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 13,
          label: 'Gifts & Novelty',
          categoryId: 422493,
          categoryFilter: '204484,422493',
          seoUrl:
            '/en/tsuk/category/bags-accessories-1702216/gifts-novelty-837',
          redirectionUrl:
            '/en/tsuk/category/bags-accessories-1702216/gifts-novelty/N-7ttZqncZdgl?Nrpp=20&siteId=%2F12556&No=0',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 1,
          label: 'Shop All Beauty',
          categoryId: 648991,
          categoryFilter: '208495,648991',
          seoUrl: '/en/tsuk/category/beauty-3326659/shop-all-beauty-6909070',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 2,
          label: 'Eyes',
          categoryId: 208566,
          categoryFilter: '208495,208566',
          seoUrl: '/en/tsuk/category/beauty-3326659/eyes-470',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 3,
          label: 'Lips',
          categoryId: 208567,
          categoryFilter: '208495,208567',
          seoUrl: '/en/tsuk/category/beauty-3326659/lips-471',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 4,
          label: 'Face',
          categoryId: 208568,
          categoryFilter: '208495,208568',
          seoUrl: '/en/tsuk/category/beauty-3326659/face-1906567',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 5,
          label: 'Nails',
          categoryId: 208569,
          categoryFilter: '208495,208569',
          seoUrl: '/en/tsuk/category/beauty-3326659/nails-473',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 6,
          label: 'Face Masks & Skincare',
          categoryId: 3496992,
          categoryFilter: '208495,3496992',
          seoUrl:
            '/en/tsuk/category/beauty-3326659/face-masks-skincare-6909177',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 7,
          label: 'Glitter & Body Jewels',
          categoryId: 3497037,
          categoryFilter: '208495,3497037',
          seoUrl:
            '/en/tsuk/category/beauty-3326659/glitter-body-jewels-6908996',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 8,
          label: 'Hair Accessories',
          categoryId: 3497066,
          categoryFilter: '208495,3497066',
          seoUrl: '/en/tsuk/category/beauty-3326659/hair-accessories-6911871',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 9,
          label: 'Tools & Accessories',
          categoryId: 3497002,
          categoryFilter: '208495,3497002',
          seoUrl: '/en/tsuk/category/beauty-3326659/tools-accessories-6909009',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 1,
          label: 'Clothing Brands',
          categoryId: 2252527,
          categoryFilter: '2244743,2252527',
          seoUrl: '/en/tsuk/category/brands-4210405/clothing-brands-4224272',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 2,
          label: 'Shoe Brands',
          categoryId: 2250618,
          categoryFilter: '2244743,2250618',
          seoUrl: '/en/tsuk/category/brands-4210405/shoe-brands-4217917',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 3,
          label: 'Accessories Brands',
          categoryId: 2252229,
          categoryFilter: '2244743,2252229',
          seoUrl: '/en/tsuk/category/brands-4210405/accessories-brands-4218529',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 4,
          label: 'Beauty Brands',
          categoryId: 2258006,
          categoryFilter: '2244743,2258006',
          seoUrl: '/en/tsuk/category/brands-4210405/beauty-brands-4233544',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 5,
          label: 'Ivy Park',
          categoryId: 3291092,
          categoryFilter: '2244743,3291092',
          seoUrl: '/en/tsuk/category/brands-4210405/ivy-park-5834347',
          redirectionUrl:
            '/en/tsuk/category/clothing-427/ivy-park-5463599?cat1=203984&cat2=3281520',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 6,
          label: 'adidas',
          categoryId: 3406848,
          categoryFilter: '2244743,3406848',
          seoUrl: '/en/tsuk/category/brands-4210405/adidas-6008354',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 7,
          label: 'Calvin Klein',
          categoryId: 3291029,
          categoryFilter: '2244743,3291029',
          seoUrl: '/en/tsuk/category/brands-4210405/calvin-klein-5755401',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 8,
          label: 'FILA',
          categoryId: 3470499,
          categoryFilter: '2244743,3470499',
          seoUrl: '/en/tsuk/category/brands-4210405/fila-6515109',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 9,
          label: 'Reebok',
          categoryId: 3497097,
          categoryFilter: '2244743,3497097',
          seoUrl: '/en/tsuk/category/brands-4210405/reebok-6914481',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 10,
          label: 'Motel',
          categoryId: 2524395,
          categoryFilter: '2244743,2524395',
          seoUrl: '/en/tsuk/category/brands-4210405/motel-4669336',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 11,
          label: 'Skinnydip',
          categoryId: 3054569,
          categoryFilter: '2244743,3054569',
          seoUrl: '/en/tsuk/category/brands-4210405/skinnydip-5755425',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 12,
          label: 'Olivia Burton Watches',
          categoryId: 3054544,
          categoryFilter: '2244743,3054544',
          seoUrl:
            '/en/tsuk/category/brands-4210405/olivia-burton-watches-6914504',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 13,
          label: 'Converse from OFFICE',
          categoryId: 3054533,
          categoryFilter: '2244743,3054533',
          seoUrl:
            '/en/tsuk/category/brands-4210405/converse-from-office-5755416',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 14,
          label: 'Quay Sunglasses',
          categoryId: 3054557,
          categoryFilter: '2244743,3054557',
          seoUrl: '/en/tsuk/category/brands-4210405/quay-sunglasses-5755424',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 1,
          label: 'Shop All Sale',
          categoryId: 398526,
          categoryFilter: '217217,398526',
          seoUrl: '/en/tsuk/category/sale-6923952/shop-all-sale-6912866',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 2,
          label: 'Clothing',
          categoryId: 3482514,
          categoryFilter: '217217,3482514',
          seoUrl: '/en/tsuk/category/sale-6923952/clothing-6712290',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 3,
          label: 'Swimwear',
          categoryId: 3491995,
          categoryFilter: '217217,3491995',
          seoUrl: '/en/tsuk/category/sale-6923952/swimwear-6854808',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 4,
          label: 'Shoes',
          categoryId: 3483025,
          categoryFilter: '217217,3483025',
          seoUrl: '/en/tsuk/category/sale-6923952/shoes-6712298',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 5,
          label: 'Accessories',
          categoryId: 3483026,
          categoryFilter: '217217,3483026',
          seoUrl: '/en/tsuk/category/sale-6923952/accessories-6712306',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 6,
          label: 'Multi-Buy',
          categoryId: 397534,
          categoryFilter: '217217,397534',
          seoUrl: '/en/tsuk/category/sale-6923952/multi-buy-6924772',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 7,
          label: 'Seasonal Offers',
          categoryId: 847049,
          categoryFilter: '217217,847049',
          seoUrl: '/en/tsuk/category/sale-6923952/seasonal-offers-6367796',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 8,
          label: 'Student Discount',
          categoryId: 3498635,
          categoryFilter: '217217,3498635',
          seoUrl: '/en/tsuk/category/sale-6923952/student-discount-6925053',
          redirectionUrl:
            '/en/tsuk/category/topshop-students-student-discount-2316596/home?TS=1392132268317',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 1,
          label: 'Fashion Stories',
          categoryId: 3452041,
          categoryFilter: '2091635,3452041',
          seoUrl: '/en/tsuk/category/style-6317120/fashion-stories-6310478',
          redirectionUrl:
            '/en/tsuk/category/magazine-3934038/home?TS=1490286326161',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 2,
          label: 'Blog Stories',
          categoryId: 3452112,
          categoryFilter: '2091635,3452112',
          seoUrl: '/en/tsuk/category/style-6317120/blog-stories-6319501',
          redirectionUrl: 'http://insideout.topshop.com',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 3,
          label: 'My Topshop Wardrobe',
          categoryId: 3452053,
          categoryFilter: '2091635,3452053',
          seoUrl: '/en/tsuk/category/style-6317120/my-topshop-wardrobe-6310494',
          redirectionUrl:
            '/en/tsuk/category/new-in-this-week-2169932/my-topshop-wardrobe-4674321/home?cat1=208491&cat2=2532995',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 4,
          label: 'Book a Personal Shopper',
          categoryId: 3452548,
          categoryFilter: '2091635,3452548',
          seoUrl:
            '/en/tsuk/category/style-6317120/book-a-personal-shopper-6310520',
          redirectionUrl:
            '/en/tsuk/category/topshop-personal-shopping-4886705/home?TS=1444832379447&cat2=2665003',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 5,
          label: 'Topshop at LFW',
          categoryId: 3452565,
          categoryFilter: '2091635,3452565',
          seoUrl: '/en/tsuk/category/style-6317120/topshop-at-lfw-6310567',
          redirectionUrl:
            '/en/tsuk/category/london-fashion-week-unique-show-4706872/home?TS=1441825748288',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 1,
          label: 'Store Locator',
          categoryId: 3339351,
          categoryFilter: '3339350,3339351',
          seoUrl:
            '/en/tsuk/category/find-a-store-5555722/store-locator-5555730',
          redirectionUrl: '/storelocator',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 1,
          label: 'Sign In or Register',
          categoryId: 3376494,
          categoryFilter: '3370531,3376494',
          seoUrl:
            '/en/tsuk/category/your-details-5655887/sign-in-or-register-5649535',
          redirectionUrl: '/login',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 2,
          label: 'My Account',
          categoryId: 3370549,
          categoryFilter: '3370531,3370549',
          seoUrl: '/en/tsuk/category/your-details-5655887/my-account-5634587',
          redirectionUrl: '/login',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 3,
          label: 'My Topshop Wardrobe',
          categoryId: 3376633,
          categoryFilter: '3370531,3376633',
          seoUrl:
            '/en/tsuk/category/your-details-5655887/my-topshop-wardrobe-5651536',
          redirectionUrl:
            '/en/tsuk/category/style-quiz-4459746/home?TS=1465549195413',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 4,
          label: 'E-Receipts',
          categoryId: 3376506,
          categoryFilter: '3370531,3376506',
          seoUrl: '/en/tsuk/category/your-details-5655887/e-receipts-5651062',
          redirectionUrl:
            '/en/tsuk/category/topshop-receipt-4957579/home?TS=1466070101673',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 5,
          label: 'Change Your Shipping Destination',
          categoryId: 3376507,
          categoryFilter: '3370531,3376507',
          seoUrl:
            '/en/tsuk/category/your-details-5655887/change-your-shipping-destination-5651100',
          redirectionUrl: '/change-shipping-destination',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 1,
          label: 'Student Discount',
          categoryId: 3376530,
          categoryFilter: '3370510,3376530',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/student-discount-5651267',
          redirectionUrl:
            '/en/tsuk/category/topshop-students-student-discount-2316596/home?TS=1465546794890',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 2,
          label: 'Gift Cards',
          categoryId: 3376543,
          categoryFilter: '3370510,3376543',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/gift-cards-5651287',
          redirectionUrl:
            '/en/tsuk/category/topshop-gift-card-247/home?TS=1466070708078',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 3,
          label: 'Topshop Card',
          categoryId: 3376546,
          categoryFilter: '3370510,3376546',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/topshop-card-5651295',
          redirectionUrl:
            '/en/tsuk/category/topshop-card-20/home?TS=1466071847952',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 4,
          label: 'Personal Shopping',
          categoryId: 3376549,
          categoryFilter: '3370510,3376549',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/personal-shopping-5651309',
          redirectionUrl:
            '/en/tsuk/category/topshop-personal-shopping-4886705/home?TS=1465547040401',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 5,
          label: 'Sign Up to Style Notes',
          categoryId: 3376563,
          categoryFilter: '3370510,3376563',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/sign-up-to-style-notes-5651318',
          redirectionUrl:
            '/en/tsuk/category/sign-up-to-style-notes-3176160/home?TS=1466666505349',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 6,
          label: 'Delivery',
          categoryId: 3376567,
          categoryFilter: '3370510,3376567',
          seoUrl: '/en/tsuk/category/help-information-5634539/delivery-5651326',
          redirectionUrl:
            '/en/tsuk/category/uk-delivery-4043283/home?TS=1421171569402&cat2=2141530',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 7,
          label: 'Returns',
          categoryId: 3376568,
          categoryFilter: '3370510,3376568',
          seoUrl: '/en/tsuk/category/help-information-5634539/returns-5651334',
          redirectionUrl:
            '/en/tsuk/category/returns-4043372/home?TS=1421172442418',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 8,
          label: 'Help',
          categoryId: 3376578,
          categoryFilter: '3370510,3376578',
          seoUrl: '/en/tsuk/category/help-information-5634539/help-5651342',
          redirectionUrl:
            'http://help.topshop.com/system/templates/selfservice/topshop/#!portal/403700000001048?LANGUAGE=en&COUNTRY=uk',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 9,
          label: 'Contact Us',
          categoryId: 3376590,
          categoryFilter: '3370510,3376590',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/contact-us-5651350',
          redirectionUrl:
            'http://help.topshop.com/system/templates/selfservice/topshop/#!portal/403700000001048',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 10,
          label: 'Careers & Opportunities',
          categoryId: 3376603,
          categoryFilter: '3370510,3376603',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/careers-opportunities-5651359',
          redirectionUrl: 'http://www.careers.topshop.com/',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 11,
          label: 'Size Guide & Washcare',
          categoryId: 3376616,
          categoryFilter: '3370510,3376616',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/size-guide-washcare-5651368',
          redirectionUrl:
            '/en/tsuk/category/size-guide-6849801/home?TS=1501776779937',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 12,
          label: 'Topshop at Oxford Circus',
          categoryId: 3472804,
          categoryFilter: '3370510,3472804',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/topshop-at-oxford-circus-6580256',
          redirectionUrl:
            '/en/tsuk/category/topshop-at-oxford-circus-6530258/home?TS=1493046708959',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 13,
          label: 'Privacy policy & cookies',
          categoryId: 3401135,
          categoryFilter: '3370510,3401135',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/privacy-policy-cookies-5755498',
          redirectionUrl: '/en/tsuk/category/privacy-cookies-157/home',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 14,
          label: 'Accessibility',
          categoryId: 3401127,
          categoryFilter: '3370510,3401127',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/accessibility-5755393',
          redirectionUrl: '/en/tsuk/category/accessibility-8/home',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 15,
          label: 'T&Cs',
          categoryId: 3376620,
          categoryFilter: '3370510,3376620',
          seoUrl: '/en/tsuk/category/help-information-5634539/tcs-5651376',
          redirectionUrl: '/en/tsuk/category/terms-conditions-19/home',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 16,
          label: 'Modern Slavery Act',
          categoryId: 3456093,
          categoryFilter: '3370510,3456093',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/modern-slavery-act-6372576',
          redirectionUrl:
            'https://www.arcadiagroup.co.uk/modernslaverystatement',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 17,
          label: 'Switch to Full Website',
          categoryId: 3376623,
          categoryFilter: '3370510,3376623',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/switch-to-full-website-5651384',
          redirectionUrl: '/switch-to-full-homepage',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 1,
          label: 'Follow Us',
          categoryId: 3370545,
          categoryFilter: '3370536,3370545',
          seoUrl: '/en/tsuk/category/follow-us-5634571/follow-us-5634579',
          redirectionUrl: '/cms/page/name/navSocial',
          navigationEntries: [],
        },
      ],
      categoryHistory: [],
      productCategories: [
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 2,
          label: 'New In',
          categoryId: 208491,
          categoryFilter: '208491',
          seoUrl: '/en/tsuk/category/new-in-this-week-2169932',
          redirectionUrl:
            '/en/tsuk/category/new-in-this-week-2169932/new-in-fashion-6367514/N-8d7Zdgl?No=0&Nrpp=20&siteId=%2F12556',
          navigationEntries: [
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 1,
              label: 'New In Fashion',
              categoryId: 277012,
              categoryFilter: '208491,277012',
              seoUrl:
                '/en/tsuk/category/new-in-this-week-2169932/new-in-fashion-6367514',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 2,
              label: 'New In Dresses',
              categoryId: 2724496,
              categoryFilter: '208491,2724496',
              seoUrl:
                '/en/tsuk/category/new-in-this-week-2169932/new-in-dresses-4938909',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 3,
              label: 'New In Lingerie & Swim',
              categoryId: 3454190,
              categoryFilter: '208491,3454190',
              seoUrl:
                '/en/tsuk/category/new-in-this-week-2169932/new-in-lingerie-swim-6354962',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 4,
              label: 'New In Shoes',
              categoryId: 1780842,
              categoryFilter: '208491,1780842',
              seoUrl:
                '/en/tsuk/category/new-in-this-week-2169932/new-in-shoes-3105666',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 5,
              label: 'New In Bags & Accessories',
              categoryId: 1780853,
              categoryFilter: '208491,1780853',
              seoUrl:
                '/en/tsuk/category/new-in-this-week-2169932/new-in-bags-accessories-3105675',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 6,
              label: 'New In Petite, Tall & Maternity',
              categoryId: 3429092,
              categoryFilter: '208491,3429092',
              seoUrl:
                '/en/tsuk/category/new-in-this-week-2169932/new-in-petite-tall-maternity-6044985',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 7,
              label: 'New In Brands',
              categoryId: 2290040,
              categoryFilter: '208491,2290040',
              seoUrl:
                '/en/tsuk/category/new-in-this-week-2169932/new-in-brands-4293441',
              navigationEntries: [],
            },
          ],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 3,
          label: 'Clothing',
          categoryId: 203984,
          categoryFilter: '203984',
          seoUrl: '/en/tsuk/category/clothing-427',
          navigationEntries: [
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 1,
              label: 'Dresses',
              categoryId: 208523,
              categoryFilter: '203984,208523',
              seoUrl: '/en/tsuk/category/clothing-427/dresses-442',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 2,
              label: 'Jeans',
              categoryId: 208527,
              categoryFilter: '203984,208527',
              seoUrl: '/en/tsuk/category/clothing-427/jeans-446',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 3,
              label: 'Shirts & Blouses',
              categoryId: 2512228,
              categoryFilter: '203984,2512228',
              seoUrl: '/en/tsuk/category/clothing-427/shirts-blouses-4650801',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 4,
              label: 'Jumpers & Cardigans',
              categoryId: 208525,
              categoryFilter: '203984,208525',
              seoUrl:
                '/en/tsuk/category/clothing-427/jumpers-cardigans-6924635',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 5,
              label: 'T-Shirts',
              categoryId: 3492547,
              categoryFilter: '203984,3492547',
              seoUrl: '/en/tsuk/category/clothing-427/t-shirts-6864659',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 6,
              label: 'Camis & Vests',
              categoryId: 3492548,
              categoryFilter: '203984,3492548',
              seoUrl: '/en/tsuk/category/clothing-427/camis-vests-6864668',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 7,
              label: 'Bodysuits',
              categoryId: 3492533,
              categoryFilter: '203984,3492533',
              seoUrl: '/en/tsuk/category/clothing-427/bodysuits-6864633',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 8,
              label: 'Jackets & Coats',
              categoryId: 208526,
              categoryFilter: '203984,208526',
              seoUrl: '/en/tsuk/category/clothing-427/jackets-coats-2390889',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 9,
              label: 'Denim',
              categoryId: 525523,
              categoryFilter: '203984,525523',
              seoUrl: '/en/tsuk/category/clothing-427/denim-4889473',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 10,
              label: 'Hoodies & Sweats',
              categoryId: 3492546,
              categoryFilter: '203984,3492546',
              seoUrl: '/en/tsuk/category/clothing-427/hoodies-sweats-6864676',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 11,
              label: 'Trousers & Leggings',
              categoryId: 208528,
              categoryFilter: '203984,208528',
              seoUrl:
                '/en/tsuk/category/clothing-427/trousers-leggings-4075710',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 12,
              label: 'Playsuits & Jumpsuits',
              categoryId: 208531,
              categoryFilter: '203984,208531',
              seoUrl:
                '/en/tsuk/category/clothing-427/playsuits-jumpsuits-2159081',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 13,
              label: 'Shorts',
              categoryId: 208529,
              categoryFilter: '203984,208529',
              seoUrl: '/en/tsuk/category/clothing-427/shorts-448',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 14,
              label: 'Skirts',
              categoryId: 208530,
              categoryFilter: '203984,208530',
              seoUrl: '/en/tsuk/category/clothing-427/skirts-449',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 15,
              label: 'Swimwear & Beachwear',
              categoryId: 208534,
              categoryFilter: '203984,208534',
              seoUrl:
                '/en/tsuk/category/clothing-427/swimwear-beachwear-3163078',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 16,
              label: 'Lingerie & Nightwear',
              categoryId: 3497086,
              categoryFilter: '203984,3497086',
              seoUrl:
                '/en/tsuk/category/clothing-427/lingerie-nightwear-6914422',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 17,
              label: 'Socks & Tights',
              categoryId: 208532,
              categoryFilter: '203984,208532',
              seoUrl: '/en/tsuk/category/clothing-427/socks-tights-3283666',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 18,
              label: 'Topshop Unique',
              categoryId: 2205494,
              categoryFilter: '203984,2205494',
              seoUrl: '/en/tsuk/category/clothing-427/topshop-unique-6924716',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 19,
              label: 'Topshop Boutique',
              categoryId: 208521,
              categoryFilter: '203984,208521',
              seoUrl: '/en/tsuk/category/clothing-427/topshop-boutique-6924724',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 20,
              label: 'Petite',
              categoryId: 208536,
              categoryFilter: '203984,208536',
              seoUrl: '/en/tsuk/category/clothing-427/petite-455',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 21,
              label: 'Tall',
              categoryId: 208535,
              categoryFilter: '203984,208535',
              seoUrl: '/en/tsuk/category/clothing-427/tall-454',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 22,
              label: 'Maternity',
              categoryId: 208537,
              categoryFilter: '203984,208537',
              seoUrl: '/en/tsuk/category/clothing-427/maternity-456',
              navigationEntries: [],
            },
          ],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 4,
          label: 'Jeans',
          categoryId: 3493110,
          categoryFilter: '3493110',
          seoUrl: '/en/tsuk/category/jeans-6877054',
          redirectionUrl:
            '/en/tsuk/category/denim-update-6917944/home?TS=1504007617660',
          navigationEntries: [
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 1,
              label: 'Shop All Jeans',
              categoryId: 3496082,
              categoryFilter: '3493110,3496082',
              seoUrl: '/en/tsuk/category/jeans-6877054/shop-all-jeans-6906718',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 2,
              label: 'Shop All Denim',
              categoryId: 3496110,
              categoryFilter: '3493110,3496110',
              seoUrl: '/en/tsuk/category/jeans-6877054/shop-all-denim-6906760',
              redirectionUrl: '/en/tsuk/category/clothing-427/denim-4889473',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 3,
              label: 'Jamie Jeans',
              categoryId: 3496007,
              categoryFilter: '3493110,3496007',
              seoUrl: '/en/tsuk/category/jeans-6877054/jamie-jeans-6906622',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 4,
              label: 'Joni Jeans',
              categoryId: 3496006,
              categoryFilter: '3493110,3496006',
              seoUrl: '/en/tsuk/category/jeans-6877054/joni-jeans-6906608',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 5,
              label: 'Leigh Jeans',
              categoryId: 3496048,
              categoryFilter: '3493110,3496048',
              seoUrl: '/en/tsuk/category/jeans-6877054/leigh-jeans-6906654',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 6,
              label: 'Sidney Jeans',
              categoryId: 3496038,
              categoryFilter: '3493110,3496038',
              seoUrl: '/en/tsuk/category/jeans-6877054/sidney-jeans-6906638',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 7,
              label: 'Mom Jeans',
              categoryId: 3496011,
              categoryFilter: '3493110,3496011',
              seoUrl: '/en/tsuk/category/jeans-6877054/mom-jeans-6906630',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 8,
              label: 'Hayden Jeans',
              categoryId: 3496079,
              categoryFilter: '3493110,3496079',
              seoUrl: '/en/tsuk/category/jeans-6877054/hayden-jeans-6906694',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 9,
              label: 'Baxter Jeans',
              categoryId: 3496052,
              categoryFilter: '3493110,3496052',
              seoUrl: '/en/tsuk/category/jeans-6877054/baxter-jeans-6906662',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 10,
              label: 'Lucas Jeans',
              categoryId: 3496063,
              categoryFilter: '3493110,3496063',
              seoUrl: '/en/tsuk/category/jeans-6877054/lucas-jeans-6906686',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 11,
              label: 'Orson Jeans',
              categoryId: 3496069,
              categoryFilter: '3493110,3496069',
              seoUrl: '/en/tsuk/category/jeans-6877054/orson-jeans-6906670',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 12,
              label: 'Dree Jeans',
              categoryId: 3496067,
              categoryFilter: '3493110,3496067',
              seoUrl: '/en/tsuk/category/jeans-6877054/dree-jeans-6906678',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 13,
              label: 'Straight Leg Jeans',
              categoryId: 3496099,
              categoryFilter: '3493110,3496099',
              seoUrl:
                '/en/tsuk/category/jeans-6877054/straight-leg-jeans-6906746',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 14,
              label: 'Skinny Jeans',
              categoryId: 3496096,
              categoryFilter: '3493110,3496096',
              seoUrl: '/en/tsuk/category/jeans-6877054/skinny-jeans-6906726',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 15,
              label: 'Crop Jeans',
              categoryId: 3496035,
              categoryFilter: '3493110,3496035',
              seoUrl: '/en/tsuk/category/jeans-6877054/crop-jeans-6906646',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 16,
              label: 'Flared Jeans',
              categoryId: 3497032,
              categoryFilter: '3493110,3497032',
              seoUrl: '/en/tsuk/category/jeans-6877054/flared-jeans-6908704',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 17,
              label: 'Wide Leg Jeans',
              categoryId: 3496088,
              categoryFilter: '3493110,3496088',
              seoUrl: '/en/tsuk/category/jeans-6877054/wide-leg-jeans-6906703',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 18,
              label: 'Ripped Jeans',
              categoryId: 3496186,
              categoryFilter: '3493110,3496186',
              seoUrl: '/en/tsuk/category/jeans-6877054/ripped-jeans-6906866',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 19,
              label: 'Embroidered Jeans',
              categoryId: 3496183,
              categoryFilter: '3493110,3496183',
              seoUrl:
                '/en/tsuk/category/jeans-6877054/embroidered-jeans-6906894',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 20,
              label: 'Tall Jeans',
              categoryId: 3496154,
              categoryFilter: '3493110,3496154',
              seoUrl: '/en/tsuk/category/jeans-6877054/tall-jeans-6906816',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 21,
              label: 'Petite Jeans',
              categoryId: 3496160,
              categoryFilter: '3493110,3496160',
              seoUrl: '/en/tsuk/category/jeans-6877054/petite-jeans-6906842',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 22,
              label: 'Maternity Jeans',
              categoryId: 3496187,
              categoryFilter: '3493110,3496187',
              seoUrl: '/en/tsuk/category/jeans-6877054/maternity-jeans-6906874',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 23,
              label: 'The Jeans Fit Guide',
              categoryId: 3496168,
              categoryFilter: '3493110,3496168',
              seoUrl:
                '/en/tsuk/category/jeans-6877054/the-jeans-fit-guide-6906850',
              redirectionUrl:
                '/en/tsuk/category/jeans-fit-guide-6338428/home?TS=1487259856055',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 24,
              label: 'The Denim Shorts Fit Guide',
              categoryId: 3496177,
              categoryFilter: '3493110,3496177',
              seoUrl:
                '/en/tsuk/category/jeans-6877054/the-denim-shorts-fit-guide-6906858',
              redirectionUrl:
                '/en/tsuk/category/shorts-fit-guide-6445047/home?TS=1490611612567',
              navigationEntries: [],
            },
          ],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 5,
          label: 'Shoes',
          categoryId: 208492,
          categoryFilter: '208492',
          seoUrl: '/en/tsuk/category/shoes-430',
          navigationEntries: [
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 1,
              label: 'Shop All Shoes',
              categoryId: 331499,
              categoryFilter: '208492,331499',
              seoUrl: '/en/tsuk/category/shoes-430/shop-all-shoes-6909322',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 2,
              label: 'Heels',
              categoryId: 208542,
              categoryFilter: '208492,208542',
              seoUrl: '/en/tsuk/category/shoes-430/heels-458',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 3,
              label: 'Boots',
              categoryId: 3339357,
              categoryFilter: '208492,3339357',
              seoUrl: '/en/tsuk/category/shoes-430/boots-6909314',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 4,
              label: 'Trainers',
              categoryId: 3216551,
              categoryFilter: '208492,3216551',
              seoUrl: '/en/tsuk/category/shoes-430/trainers-5399321',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 5,
              label: 'Sliders',
              categoryId: 3462494,
              categoryFilter: '208492,3462494',
              seoUrl: '/en/tsuk/category/shoes-430/sliders-6467817',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 6,
              label: 'Flats',
              categoryId: 208543,
              categoryFilter: '208492,208543',
              seoUrl: '/en/tsuk/category/shoes-430/flats-459',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 7,
              label: 'Loafers',
              categoryId: 3212595,
              categoryFilter: '208492,3212595',
              seoUrl: '/en/tsuk/category/shoes-430/loafers-5928805',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 8,
              label: 'Sandals',
              categoryId: 3212506,
              categoryFilter: '208492,3212506',
              seoUrl: '/en/tsuk/category/shoes-430/sandals-5388227',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 9,
              label: 'Ankle Boots',
              categoryId: 2759103,
              categoryFilter: '208492,2759103',
              seoUrl: '/en/tsuk/category/shoes-430/ankle-boots-4979409',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 10,
              label: 'Over The Knee Boots',
              categoryId: 2751541,
              categoryFilter: '208492,2751541',
              seoUrl: '/en/tsuk/category/shoes-430/over-the-knee-boots-6909328',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 11,
              label: 'Metallic Shoes',
              categoryId: 3496999,
              categoryFilter: '208492,3496999',
              seoUrl: '/en/tsuk/category/shoes-430/metallic-shoes-6909333',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 12,
              label: 'Going Out Shoes',
              categoryId: 2751577,
              categoryFilter: '208492,2751577',
              seoUrl: '/en/tsuk/category/shoes-430/going-out-shoes-4967245',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 13,
              label: 'OFFICE at Topshop',
              categoryId: 3438529,
              categoryFilter: '208492,3438529',
              seoUrl: '/en/tsuk/category/shoes-430/office-at-topshop-6909341',
              navigationEntries: [],
            },
          ],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 6,
          label: 'Bags & Accessories',
          categoryId: 204484,
          categoryFilter: '204484',
          seoUrl: '/en/tsuk/category/bags-accessories-1702216',
          navigationEntries: [
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 1,
              label: 'Shop All Accessories',
              categoryId: 3497014,
              categoryFilter: '204484,3497014',
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/shop-all-accessories-6909359',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 2,
              label: 'Bags & Purses',
              categoryId: 208548,
              categoryFilter: '204484,208548',
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/bags-purses-462',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 3,
              label: 'Sunglasses',
              categoryId: 208555,
              categoryFilter: '204484,208555',
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/sunglasses-468',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 4,
              label: 'Jewellery',
              categoryId: 208556,
              categoryFilter: '204484,208556',
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/jewellery-469',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 5,
              label: 'Watches',
              categoryId: 1500007,
              categoryFilter: '204484,1500007',
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/watches-2497426',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 6,
              label: 'Hats',
              categoryId: 208549,
              categoryFilter: '204484,208549',
              seoUrl: '/en/tsuk/category/bags-accessories-1702216/hats-463',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 7,
              label: 'Scarves',
              categoryId: 208552,
              categoryFilter: '204484,208552',
              seoUrl: '/en/tsuk/category/bags-accessories-1702216/scarves-465',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 8,
              label: 'Belts',
              categoryId: 208553,
              categoryFilter: '204484,208553',
              seoUrl: '/en/tsuk/category/bags-accessories-1702216/belts-466',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 9,
              label: 'Socks & Tights',
              categoryId: 1006498,
              categoryFilter: '204484,1006498',
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/socks-tights-3283675',
              redirectionUrl: '/en/tsuk/category/clothing-427/tights-socks-451',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 10,
              label: 'Hair Accessories',
              categoryId: 208551,
              categoryFilter: '204484,208551',
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hair-accessories-464',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 11,
              label: 'Books & Stationery',
              categoryId: 3497040,
              categoryFilter: '204484,3497040',
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/books-stationery-6934143',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 12,
              label: 'iPhone Accessories',
              categoryId: 2223012,
              categoryFilter: '204484,2223012',
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/iphone-accessories-4582259',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 13,
              label: 'Gifts & Novelty',
              categoryId: 422493,
              categoryFilter: '204484,422493',
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/gifts-novelty-837',
              redirectionUrl:
                '/en/tsuk/category/bags-accessories-1702216/gifts-novelty/N-7ttZqncZdgl?Nrpp=20&siteId=%2F12556&No=0',
              navigationEntries: [],
            },
          ],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 7,
          label: 'Beauty',
          categoryId: 208495,
          categoryFilter: '208495',
          seoUrl: '/en/tsuk/category/beauty-3326659',
          navigationEntries: [
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 1,
              label: 'Shop All Beauty',
              categoryId: 648991,
              categoryFilter: '208495,648991',
              seoUrl:
                '/en/tsuk/category/beauty-3326659/shop-all-beauty-6909070',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 2,
              label: 'Eyes',
              categoryId: 208566,
              categoryFilter: '208495,208566',
              seoUrl: '/en/tsuk/category/beauty-3326659/eyes-470',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 3,
              label: 'Lips',
              categoryId: 208567,
              categoryFilter: '208495,208567',
              seoUrl: '/en/tsuk/category/beauty-3326659/lips-471',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 4,
              label: 'Face',
              categoryId: 208568,
              categoryFilter: '208495,208568',
              seoUrl: '/en/tsuk/category/beauty-3326659/face-1906567',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 5,
              label: 'Nails',
              categoryId: 208569,
              categoryFilter: '208495,208569',
              seoUrl: '/en/tsuk/category/beauty-3326659/nails-473',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 6,
              label: 'Face Masks & Skincare',
              categoryId: 3496992,
              categoryFilter: '208495,3496992',
              seoUrl:
                '/en/tsuk/category/beauty-3326659/face-masks-skincare-6909177',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 7,
              label: 'Glitter & Body Jewels',
              categoryId: 3497037,
              categoryFilter: '208495,3497037',
              seoUrl:
                '/en/tsuk/category/beauty-3326659/glitter-body-jewels-6908996',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 8,
              label: 'Hair Accessories',
              categoryId: 3497066,
              categoryFilter: '208495,3497066',
              seoUrl:
                '/en/tsuk/category/beauty-3326659/hair-accessories-6911871',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 9,
              label: 'Tools & Accessories',
              categoryId: 3497002,
              categoryFilter: '208495,3497002',
              seoUrl:
                '/en/tsuk/category/beauty-3326659/tools-accessories-6909009',
              navigationEntries: [],
            },
          ],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 8,
          label: 'Brands',
          categoryId: 2244743,
          categoryFilter: '2244743',
          seoUrl: '/en/tsuk/category/brands-4210405',
          navigationEntries: [
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 1,
              label: 'Clothing Brands',
              categoryId: 2252527,
              categoryFilter: '2244743,2252527',
              seoUrl:
                '/en/tsuk/category/brands-4210405/clothing-brands-4224272',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 2,
              label: 'Shoe Brands',
              categoryId: 2250618,
              categoryFilter: '2244743,2250618',
              seoUrl: '/en/tsuk/category/brands-4210405/shoe-brands-4217917',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 3,
              label: 'Accessories Brands',
              categoryId: 2252229,
              categoryFilter: '2244743,2252229',
              seoUrl:
                '/en/tsuk/category/brands-4210405/accessories-brands-4218529',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 4,
              label: 'Beauty Brands',
              categoryId: 2258006,
              categoryFilter: '2244743,2258006',
              seoUrl: '/en/tsuk/category/brands-4210405/beauty-brands-4233544',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 5,
              label: 'Ivy Park',
              categoryId: 3291092,
              categoryFilter: '2244743,3291092',
              seoUrl: '/en/tsuk/category/brands-4210405/ivy-park-5834347',
              redirectionUrl:
                '/en/tsuk/category/clothing-427/ivy-park-5463599?cat1=203984&cat2=3281520',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 6,
              label: 'adidas',
              categoryId: 3406848,
              categoryFilter: '2244743,3406848',
              seoUrl: '/en/tsuk/category/brands-4210405/adidas-6008354',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 7,
              label: 'Calvin Klein',
              categoryId: 3291029,
              categoryFilter: '2244743,3291029',
              seoUrl: '/en/tsuk/category/brands-4210405/calvin-klein-5755401',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 8,
              label: 'FILA',
              categoryId: 3470499,
              categoryFilter: '2244743,3470499',
              seoUrl: '/en/tsuk/category/brands-4210405/fila-6515109',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 9,
              label: 'Reebok',
              categoryId: 3497097,
              categoryFilter: '2244743,3497097',
              seoUrl: '/en/tsuk/category/brands-4210405/reebok-6914481',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 10,
              label: 'Motel',
              categoryId: 2524395,
              categoryFilter: '2244743,2524395',
              seoUrl: '/en/tsuk/category/brands-4210405/motel-4669336',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 11,
              label: 'Skinnydip',
              categoryId: 3054569,
              categoryFilter: '2244743,3054569',
              seoUrl: '/en/tsuk/category/brands-4210405/skinnydip-5755425',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 12,
              label: 'Olivia Burton Watches',
              categoryId: 3054544,
              categoryFilter: '2244743,3054544',
              seoUrl:
                '/en/tsuk/category/brands-4210405/olivia-burton-watches-6914504',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 13,
              label: 'Converse from OFFICE',
              categoryId: 3054533,
              categoryFilter: '2244743,3054533',
              seoUrl:
                '/en/tsuk/category/brands-4210405/converse-from-office-5755416',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 14,
              label: 'Quay Sunglasses',
              categoryId: 3054557,
              categoryFilter: '2244743,3054557',
              seoUrl:
                '/en/tsuk/category/brands-4210405/quay-sunglasses-5755424',
              navigationEntries: [],
            },
          ],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 9,
          label: 'Sale',
          categoryId: 217217,
          categoryFilter: '217217',
          seoUrl: '/en/tsuk/category/sale-6923952',
          navigationEntries: [
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 1,
              label: 'Shop All Sale',
              categoryId: 398526,
              categoryFilter: '217217,398526',
              seoUrl: '/en/tsuk/category/sale-6923952/shop-all-sale-6912866',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 2,
              label: 'Clothing',
              categoryId: 3482514,
              categoryFilter: '217217,3482514',
              seoUrl: '/en/tsuk/category/sale-6923952/clothing-6712290',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 3,
              label: 'Swimwear',
              categoryId: 3491995,
              categoryFilter: '217217,3491995',
              seoUrl: '/en/tsuk/category/sale-6923952/swimwear-6854808',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 4,
              label: 'Shoes',
              categoryId: 3483025,
              categoryFilter: '217217,3483025',
              seoUrl: '/en/tsuk/category/sale-6923952/shoes-6712298',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 5,
              label: 'Accessories',
              categoryId: 3483026,
              categoryFilter: '217217,3483026',
              seoUrl: '/en/tsuk/category/sale-6923952/accessories-6712306',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 6,
              label: 'Multi-Buy',
              categoryId: 397534,
              categoryFilter: '217217,397534',
              seoUrl: '/en/tsuk/category/sale-6923952/multi-buy-6924772',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 7,
              label: 'Seasonal Offers',
              categoryId: 847049,
              categoryFilter: '217217,847049',
              seoUrl: '/en/tsuk/category/sale-6923952/seasonal-offers-6367796',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 8,
              label: 'Student Discount',
              categoryId: 3498635,
              categoryFilter: '217217,3498635',
              seoUrl: '/en/tsuk/category/sale-6923952/student-discount-6925053',
              redirectionUrl:
                '/en/tsuk/category/topshop-students-student-discount-2316596/home?TS=1392132268317',
              navigationEntries: [],
            },
          ],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
          index: 10,
          label: 'Style',
          categoryId: 2091635,
          categoryFilter: '2091635',
          seoUrl: '/en/tsuk/category/style-6317120',
          navigationEntries: [
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 1,
              label: 'Fashion Stories',
              categoryId: 3452041,
              categoryFilter: '2091635,3452041',
              seoUrl: '/en/tsuk/category/style-6317120/fashion-stories-6310478',
              redirectionUrl:
                '/en/tsuk/category/magazine-3934038/home?TS=1490286326161',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
              index: 2,
              label: 'Blog Stories',
              categoryId: 3452112,
              categoryFilter: '2091635,3452112',
              seoUrl: '/en/tsuk/category/style-6317120/blog-stories-6319501',
              redirectionUrl: 'http://insideout.topshop.com',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 3,
              label: 'My Topshop Wardrobe',
              categoryId: 3452053,
              categoryFilter: '2091635,3452053',
              seoUrl:
                '/en/tsuk/category/style-6317120/my-topshop-wardrobe-6310494',
              redirectionUrl:
                '/en/tsuk/category/new-in-this-week-2169932/my-topshop-wardrobe-4674321/home?cat1=208491&cat2=2532995',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 4,
              label: 'Book a Personal Shopper',
              categoryId: 3452548,
              categoryFilter: '2091635,3452548',
              seoUrl:
                '/en/tsuk/category/style-6317120/book-a-personal-shopper-6310520',
              redirectionUrl:
                '/en/tsuk/category/topshop-personal-shopping-4886705/home?TS=1444832379447&cat2=2665003',
              navigationEntries: [],
            },
            {
              navigationEntryType: 'NAV_ENTRY_TYPE_CATEGORY',
              index: 5,
              label: 'Topshop at LFW',
              categoryId: 3452565,
              categoryFilter: '2091635,3452565',
              seoUrl: '/en/tsuk/category/style-6317120/topshop-at-lfw-6310567',
              redirectionUrl:
                '/en/tsuk/category/london-fashion-week-unique-show-4706872/home?TS=1441825748288',
              navigationEntries: [],
            },
          ],
        },
      ],
      userDetailsGroup: [
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 1,
          label: 'Sign out',
          categoryId: 3376494,
          categoryFilter: '3370531,3376494',
          seoUrl: '/logout',
          redirectionUrl: '/login',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 2,
          label: 'My Account',
          categoryId: 3370549,
          categoryFilter: '3370531,3370549',
          seoUrl: '/en/tsuk/category/your-details-5655887/my-account-5634587',
          redirectionUrl: '/login',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 3,
          label: 'My Topshop Wardrobe',
          categoryId: 3376633,
          categoryFilter: '3370531,3376633',
          seoUrl:
            '/en/tsuk/category/your-details-5655887/my-topshop-wardrobe-5651536',
          redirectionUrl:
            '/en/tsuk/category/style-quiz-4459746/home?TS=1465549195413',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 4,
          label: 'E-Receipts',
          categoryId: 3376506,
          categoryFilter: '3370531,3376506',
          seoUrl: '/en/tsuk/category/your-details-5655887/e-receipts-5651062',
          redirectionUrl:
            '/en/tsuk/category/topshop-receipt-4957579/home?TS=1466070101673',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 5,
          label: 'Change Your Shipping Destination',
          categoryId: 3376507,
          categoryFilter: '3370531,3376507',
          seoUrl:
            '/en/tsuk/category/your-details-5655887/change-your-shipping-destination-5651100',
          redirectionUrl: '/change-shipping-destination',
          navigationEntries: [],
        },
      ],
      helpAndInfoGroup: [
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 1,
          label: 'Student Discount',
          categoryId: 3376530,
          categoryFilter: '3370510,3376530',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/student-discount-5651267',
          redirectionUrl:
            '/en/tsuk/category/topshop-students-student-discount-2316596/home?TS=1465546794890',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 2,
          label: 'Gift Cards',
          categoryId: 3376543,
          categoryFilter: '3370510,3376543',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/gift-cards-5651287',
          redirectionUrl:
            '/en/tsuk/category/topshop-gift-card-247/home?TS=1466070708078',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 3,
          label: 'Topshop Card',
          categoryId: 3376546,
          categoryFilter: '3370510,3376546',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/topshop-card-5651295',
          redirectionUrl:
            '/en/tsuk/category/topshop-card-20/home?TS=1466071847952',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 4,
          label: 'Personal Shopping',
          categoryId: 3376549,
          categoryFilter: '3370510,3376549',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/personal-shopping-5651309',
          redirectionUrl:
            '/en/tsuk/category/topshop-personal-shopping-4886705/home?TS=1465547040401',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 5,
          label: 'Sign Up to Style Notes',
          categoryId: 3376563,
          categoryFilter: '3370510,3376563',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/sign-up-to-style-notes-5651318',
          redirectionUrl:
            '/en/tsuk/category/sign-up-to-style-notes-3176160/home?TS=1466666505349',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 6,
          label: 'Delivery',
          categoryId: 3376567,
          categoryFilter: '3370510,3376567',
          seoUrl: '/en/tsuk/category/help-information-5634539/delivery-5651326',
          redirectionUrl:
            '/en/tsuk/category/uk-delivery-4043283/home?TS=1421171569402&cat2=2141530',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 7,
          label: 'Returns',
          categoryId: 3376568,
          categoryFilter: '3370510,3376568',
          seoUrl: '/en/tsuk/category/help-information-5634539/returns-5651334',
          redirectionUrl:
            '/en/tsuk/category/returns-4043372/home?TS=1421172442418',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 8,
          label: 'Help',
          categoryId: 3376578,
          categoryFilter: '3370510,3376578',
          seoUrl: '/en/tsuk/category/help-information-5634539/help-5651342',
          redirectionUrl:
            'http://help.topshop.com/system/templates/selfservice/topshop/#!portal/403700000001048?LANGUAGE=en&COUNTRY=uk',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 9,
          label: 'Contact Us',
          categoryId: 3376590,
          categoryFilter: '3370510,3376590',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/contact-us-5651350',
          redirectionUrl:
            'http://help.topshop.com/system/templates/selfservice/topshop/#!portal/403700000001048',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 10,
          label: 'Careers & Opportunities',
          categoryId: 3376603,
          categoryFilter: '3370510,3376603',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/careers-opportunities-5651359',
          redirectionUrl: 'http://www.careers.topshop.com/',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 11,
          label: 'Size Guide & Washcare',
          categoryId: 3376616,
          categoryFilter: '3370510,3376616',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/size-guide-washcare-5651368',
          redirectionUrl:
            '/en/tsuk/category/size-guide-6849801/home?TS=1501776779937',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 12,
          label: 'Topshop at Oxford Circus',
          categoryId: 3472804,
          categoryFilter: '3370510,3472804',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/topshop-at-oxford-circus-6580256',
          redirectionUrl:
            '/en/tsuk/category/topshop-at-oxford-circus-6530258/home?TS=1493046708959',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 13,
          label: 'Privacy policy & cookies',
          categoryId: 3401135,
          categoryFilter: '3370510,3401135',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/privacy-policy-cookies-5755498',
          redirectionUrl: '/en/tsuk/category/privacy-cookies-157/home',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 14,
          label: 'Accessibility',
          categoryId: 3401127,
          categoryFilter: '3370510,3401127',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/accessibility-5755393',
          redirectionUrl: '/en/tsuk/category/accessibility-8/home',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 15,
          label: 'T&Cs',
          categoryId: 3376620,
          categoryFilter: '3370510,3376620',
          seoUrl: '/en/tsuk/category/help-information-5634539/tcs-5651376',
          redirectionUrl: '/en/tsuk/category/terms-conditions-19/home',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 16,
          label: 'Modern Slavery Act',
          categoryId: 3456093,
          categoryFilter: '3370510,3456093',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/modern-slavery-act-6372576',
          redirectionUrl:
            'https://www.arcadiagroup.co.uk/modernslaverystatement',
          navigationEntries: [],
        },
        {
          navigationEntryType: 'NAV_ENTRY_TYPE_LABEL',
          index: 17,
          label: 'Switch to Full Website',
          categoryId: 3376623,
          categoryFilter: '3370510,3376623',
          seoUrl:
            '/en/tsuk/category/help-information-5634539/switch-to-full-website-5651384',
          redirectionUrl: '/switch-to-full-homepage',
          navigationEntries: [],
        },
      ],
      error: {},
    },
    orderHistory: {
      orders: [],
      orderDetails: {},
    },
    peerius: {
      dynamic: true,
      baseTrack: {
        lang: 'en-gb',
        channel: 'mobileweb',
        smartRecs: {
          showAttributes: ['*'],
        },
        user: {
          name: 'Jane Doe',
          email: 'rodderstestxx@test.com',
        },
      },
      track: {
        lang: 'en-gb',
        channel: 'mobileweb',
        smartRecs: {
          showAttributes: ['*'],
        },
        user: {
          name: 'Jane Doe',
          email: 'rodderstestxx@test.com',
        },
        type: 'order',
        order: {
          currency: 'GBP',
          items: [
            {
              refCode: 'TS19K15MBLK',
              qty: 1,
              price: '12.00',
            },
          ],
          subtotal: '12.00',
          shipping: '4.00',
          total: '16.00',
          orderNo: 1816564,
        },
      },
      domain: 'uat',
    },
    productDetail: {
      activeItem: {
        quantity: 10,
        size: 'ONE',
        sku: '602017001141910',
        selected: false,
      },
      selectedOosItem: {},
      showError: false,
      selectedQuantity: 1,
    },
    products: {
      refinements: [
        {
          label: 'Colour',
          refinementOptions: [
            {
              type: 'VALUE',
              label: 'black',
              value: 'black',
              count: 35,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/black/N-7vpZdeoZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'blue',
              value: 'blue',
              count: 27,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/blue/N-7vpZdepZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'brown',
              value: 'brown',
              count: 6,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/brown/N-7vpZdeqZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'camel',
              value: 'camel',
              count: 5,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/camel/N-7vpZderZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'cream',
              value: 'cream',
              count: 3,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/cream/N-7vpZdesZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'green',
              value: 'green',
              count: 6,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/green/N-7vpZdetZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'grey',
              value: 'grey',
              count: 19,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/grey/N-7vpZdeuZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'metallic',
              value: 'metallic',
              count: 2,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/metallic/N-7vpZdevZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'multi',
              value: 'multi',
              count: 4,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/multi/N-7vpZdewZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'nude',
              value: 'nude',
              count: 13,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/nude/N-7vpZdexZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'orange',
              value: 'orange',
              count: 2,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/orange/N-7vpZdeyZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'pink',
              value: 'pink',
              count: 13,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/pink/N-7vpZdezZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'purple',
              value: 'purple',
              count: 6,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/purple/N-7vpZdf0Zdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'red',
              value: 'red',
              count: 9,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/red/N-7vpZdf1Zdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'white',
              value: 'white',
              count: 6,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/white/N-7vpZdf2Zdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'yellow',
              value: 'yellow',
              count: 1,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/yellow/N-7vpZdf3Zdgl?Nrpp=20&siteId=%2F12556',
            },
          ],
        },
        {
          label: 'Size',
          refinementOptions: [
            {
              type: 'VALUE',
              label: 'one size',
              value: 'one size',
              count: 12,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/one-size/N-7vpZdozZdgl?Nrpp=20&siteId=%2F12556',
            },
          ],
        },
        {
          label: 'Price',
          refinementOptions: [
            {
              type: 'RANGE',
              minValue: '1',
              maxValue: '50',
            },
          ],
        },
        {
          label: 'Bag Style',
          refinementOptions: [
            {
              type: 'VALUE',
              label: 'backpacks',
              value: 'backpacks',
              count: 1,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/backpacks/N-7vpZqdxZdgl?Nrpp=20&siteId=%2F12556',
            },
          ],
        },
        {
          label: 'Hat Style',
          refinementOptions: [
            {
              type: 'VALUE',
              label: 'baseball caps',
              value: 'baseball caps',
              count: 90,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/baseball-caps/N-7vpZ1xprZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'beanies',
              value: 'beanies',
              count: 35,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/beanies/N-7vpZr95Zdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'berets',
              value: 'berets',
              count: 2,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/berets/N-7vpZ1ygjZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'fedora hats',
              value: 'fedora hats',
              count: 18,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/fedora-hats/N-7vpZr96Zdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'felt hats',
              value: 'felt hats',
              count: 1,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/felt-hats/N-7vpZ1y0kZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'straw hats',
              value: 'straw hats',
              count: 4,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/straw-hats/N-7vpZrtkZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'visors',
              value: 'visors',
              count: 2,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/visors/N-7vpZ20efZdgl?Nrpp=20&siteId=%2F12556',
            },
          ],
        },
        {
          label: 'Brands',
          refinementOptions: [
            {
              type: 'VALUE',
              label: 'adidas',
              value: 'adidas',
              count: 12,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/adidas/N-7vpZ21o4Zdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'herschel',
              value: 'herschel',
              count: 1,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/herschel/N-7vpZ25nkZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'hype',
              value: 'hype',
              count: 5,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/hype/N-7vpZ2411Zdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'ivy park',
              value: 'ivy park',
              count: 7,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/ivy-park/N-7vpZ21okZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'new balance',
              value: 'new balance',
              count: 6,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/new-balance/N-7vpZ248kZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'new era',
              value: 'new era',
              count: 24,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/new-era/N-7vpZ21owZdgl?Nrpp=20&siteId=%2F12556',
            },
            {
              type: 'VALUE',
              label: 'tommy hilfiger',
              value: 'tommy hilfiger',
              count: 3,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/tommy-hilfiger/N-7vpZ21tcZdgl?Nrpp=20&siteId=%2F12556',
            },
          ],
        },
        {
          label: 'Accessories',
          refinementOptions: [
            {
              type: 'VALUE',
              label: 'one size',
              value: 'one size',
              count: 145,
              seoUrl:
                '/en/tsuk/category/bags-accessories-1702216/hats-463/one-size/N-7vpZdfvZdgl?Nrpp=20&siteId=%2F12556',
            },
          ],
        },
      ],
      sortOptions: [
        {
          label: 'Best Match',
          value: 'Relevance',
        },
        {
          label: 'Newest',
          value: 'Newness',
        },
        {
          label: 'Price - Low To High',
          value: 'Price Ascending',
        },
        {
          label: 'Price - High To Low',
          value: 'Price Descending',
        },
      ],
      breadcrumbs: [
        {
          label: 'Home',
        },
        {
          label: 'Bags & Accessories',
          category: '204484',
        },
        {
          label: 'Hats',
          category: '204484,208549',
        },
      ],
      products: [
        {
          productId: 29370388,
          lineNumber: 'TS19K15MBLK',
          name: 'Classic Beret Hat',
          unitPrice: '12.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/classic-beret-hat-6972944',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/333333.jpg',
              swatchProduct: {
                productId: 29370388,
                grouping: 'TS19K15MBLK',
                lineNumber: '19K15MBLK',
                name: 'Classic Beret Hat',
                shortDescription: 'Classic Beret Hat',
                unitPrice: '12.00',
                wasPrice: '12.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/classic-beret-hat-6972944?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/e0b8b8.jpg',
              swatchProduct: {
                productId: 29370510,
                grouping: 'TS19K15MPPK',
                lineNumber: '19K15MPPK',
                name: 'Classic Beret',
                shortDescription: 'Classic Beret',
                unitPrice: '12.00',
                wasPrice: '12.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/classic-beret-6957392?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_4col_M_1.jpg',
                  },
                ],
              },
            },
          ],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 29370510,
          lineNumber: 'TS19K15MPPK',
          name: 'Classic Beret',
          unitPrice: '12.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/classic-beret-6957392',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/e0b8b8.jpg',
              swatchProduct: {
                productId: 29370510,
                grouping: 'TS19K15MPPK',
                lineNumber: '19K15MPPK',
                name: 'Classic Beret',
                shortDescription: 'Classic Beret',
                unitPrice: '12.00',
                wasPrice: '12.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/classic-beret-6957392?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/333333.jpg',
              swatchProduct: {
                productId: 29370388,
                grouping: 'TS19K15MBLK',
                lineNumber: '19K15MBLK',
                name: 'Classic Beret Hat',
                shortDescription: 'Classic Beret Hat',
                unitPrice: '12.00',
                wasPrice: '12.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/classic-beret-hat-6972944?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_4col_M_1.jpg',
                  },
                ],
              },
            },
          ],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 29235330,
          lineNumber: 'TS19E12MBLK',
          name: 'Double Pom Beanie',
          unitPrice: '14.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/double-pom-beanie-6844665',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 29370178,
          lineNumber: 'TS19E03MBLK',
          name: 'Rainbow Pom Pom Ribbed Beanie Hat',
          unitPrice: '14.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/rainbow-pom-pom-ribbed-beanie-hat-6956135',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/000000.jpg',
              swatchProduct: {
                productId: 29370178,
                grouping: 'TS19E03MBLK',
                lineNumber: '19E03MBLK',
                name: 'Rainbow Pom Pom Ribbed Beanie Hat',
                shortDescription: 'Rainbow Pom Pom Ribbed Beanie Hat',
                unitPrice: '14.00',
                wasPrice: '14.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/rainbow-pom-pom-ribbed-beanie-hat-6956135?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/52527a.jpg',
              swatchProduct: {
                productId: 28979493,
                grouping: 'TS19E03MGRY',
                lineNumber: '19E03MGRY',
                name: 'Rainbow Pom Pom Beanie Hat',
                shortDescription: 'Rainbow Pom Pom Beanie Hat',
                unitPrice: '14.00',
                wasPrice: '14.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/rainbow-pom-pom-beanie-hat-6858666?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MGRY_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MGRY_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MGRY_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MGRY_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MGRY_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MGRY_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MGRY_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MGRY_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MGRY_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MGRY_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MGRY_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MGRY_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MGRY_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MGRY_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MGRY_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MGRY_4col_M_1.jpg',
                  },
                ],
              },
            },
          ],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 29573702,
          lineNumber: 'TS29A92NCHI',
          name: 'Velvet Backless Cap by Ivy Park',
          unitPrice: '20.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/velvet-backless-cap-by-ivy-park-6954024',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29A92NCHI_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29A92NCHI_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29A92NCHI_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29A92NCHI_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_PROMO_GRAPHIC',
              index: 1,
              url:
                'http://ts.stage.arcadiagroup.ltd.uk/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/static/static-0000109335/images/Banner_Mobile_comingsoon_uk.svg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29A92NCHI_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29A92NCHI_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29A92NCHI_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29A92NCHI_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29A92NCHI_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29A92NCHI_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29A92NCHI_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29A92NCHI_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29A92NCHI_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29A92NCHI_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29A92NCHI_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS29A92NCHI_4col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_BANNER_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/static/static-0000109335/images/Banner_Small_comingsoon_uk.svg',
            },
            {
              assetType: 'IMAGE_BANNER_MEDIUM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/static/static-0000109335/images/Banner_Medium_comingsoon_uk.svg',
            },
            {
              assetType: 'IMAGE_BANNER_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/static/static-0000109335/images/Banner_Small_comingsoon_uk.svg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 29370304,
          lineNumber: 'TS19K01KRST',
          name: 'Felt Floppy Hat',
          unitPrice: '25.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/felt-floppy-hat-6915237',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K01KRST_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K01KRST_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K01KRST_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K01KRST_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K01KRST_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K01KRST_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K01KRST_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K01KRST_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K01KRST_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K01KRST_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K01KRST_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K01KRST_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K01KRST_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K01KRST_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K01KRST_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K01KRST_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 27414108,
          lineNumber: 'TS19E13LLGY',
          name: 'Pom Pom Beanie Hat',
          unitPrice: '14.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/pom-pom-beanie-hat-6911601',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/d6c2d6.jpg',
              swatchProduct: {
                productId: 27414108,
                grouping: 'TS19E13LLGY',
                lineNumber: '19E13LLGY',
                name: 'Pom Pom Beanie Hat',
                shortDescription: 'Pom Pom Beanie Hat',
                unitPrice: '14.00',
                wasPrice: '14.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/pom-pom-beanie-hat-6911601?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/ffffff.jpg',
              swatchProduct: {
                productId: 27414259,
                grouping: 'TS19E13LNUD',
                lineNumber: '19E13LNUD',
                name: 'Pom Pom Beanie Hat',
                shortDescription: 'Pom Pom Beanie Hat',
                unitPrice: '14.00',
                wasPrice: '14.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/pom-pom-beanie-hat-6911609?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_4col_M_1.jpg',
                  },
                ],
              },
            },
          ],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 27414259,
          lineNumber: 'TS19E13LNUD',
          name: 'Pom Pom Beanie Hat',
          unitPrice: '14.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/pom-pom-beanie-hat-6911609',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/ffffff.jpg',
              swatchProduct: {
                productId: 27414259,
                grouping: 'TS19E13LNUD',
                lineNumber: '19E13LNUD',
                name: 'Pom Pom Beanie Hat',
                shortDescription: 'Pom Pom Beanie Hat',
                unitPrice: '14.00',
                wasPrice: '14.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/pom-pom-beanie-hat-6911609?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LNUD_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/d6c2d6.jpg',
              swatchProduct: {
                productId: 27414108,
                grouping: 'TS19E13LLGY',
                lineNumber: '19E13LLGY',
                name: 'Pom Pom Beanie Hat',
                shortDescription: 'Pom Pom Beanie Hat',
                unitPrice: '14.00',
                wasPrice: '14.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/pom-pom-beanie-hat-6911601?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E13LLGY_4col_M_1.jpg',
                  },
                ],
              },
            },
          ],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 29235690,
          lineNumber: 'TS19K11MBLK',
          name: 'Velvet Baker Boy Hat',
          unitPrice: '14.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/velvet-baker-boy-hat-6908805',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K11MBLK_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K11MBLK_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K11MBLK_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K11MBLK_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K11MBLK_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K11MBLK_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K11MBLK_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K11MBLK_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K11MBLK_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K11MBLK_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K11MBLK_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K11MBLK_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K11MBLK_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K11MBLK_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K11MBLK_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K11MBLK_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 29235361,
          lineNumber: 'TS19E12MRED',
          name: 'Double Pom Pom Beanie Hat',
          unitPrice: '14.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/double-pom-pom-beanie-hat-6906571',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MRED_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MRED_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MRED_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MRED_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MRED_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MRED_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MRED_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MRED_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MRED_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MRED_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MRED_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MRED_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MRED_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MRED_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MRED_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MRED_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 28979725,
          lineNumber: 'TS19H06MPNK',
          name: 'Faux Fur Cap',
          unitPrice: '18.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/faux-fur-cap-6894440',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/f5a3be.jpg',
              swatchProduct: {
                productId: 28979725,
                grouping: 'TS19H06MPNK',
                lineNumber: '19H06MPNK',
                name: 'Faux Fur Cap',
                shortDescription: 'Faux Fur Cap',
                unitPrice: '18.00',
                wasPrice: '18.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/faux-fur-cap-6894440?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/cccccc.jpg',
              swatchProduct: {
                productId: 28979648,
                grouping: 'TS19H06MGRY',
                lineNumber: '19H06MGRY',
                name: 'Faux Fur Cap',
                shortDescription: 'Faux Fur Cap',
                unitPrice: '18.00',
                wasPrice: '18.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/faux-fur-cap-6894432?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_4col_M_1.jpg',
                  },
                ],
              },
            },
          ],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 28979648,
          lineNumber: 'TS19H06MGRY',
          name: 'Faux Fur Cap',
          unitPrice: '18.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/faux-fur-cap-6894432',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/cccccc.jpg',
              swatchProduct: {
                productId: 28979648,
                grouping: 'TS19H06MGRY',
                lineNumber: '19H06MGRY',
                name: 'Faux Fur Cap',
                shortDescription: 'Faux Fur Cap',
                unitPrice: '18.00',
                wasPrice: '18.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/faux-fur-cap-6894432?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MGRY_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/f5a3be.jpg',
              swatchProduct: {
                productId: 28979725,
                grouping: 'TS19H06MPNK',
                lineNumber: '19H06MPNK',
                name: 'Faux Fur Cap',
                shortDescription: 'Faux Fur Cap',
                unitPrice: '18.00',
                wasPrice: '18.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/faux-fur-cap-6894440?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H06MPNK_4col_M_1.jpg',
                  },
                ],
              },
            },
          ],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 28979599,
          lineNumber: 'TS19H05MTAN',
          name: 'Faux Fur Cap',
          unitPrice: '18.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/faux-fur-cap-6902428',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H05MTAN_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H05MTAN_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H05MTAN_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H05MTAN_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H05MTAN_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H05MTAN_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H05MTAN_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H05MTAN_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H05MTAN_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H05MTAN_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H05MTAN_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H05MTAN_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H05MTAN_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H05MTAN_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H05MTAN_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19H05MTAN_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 29356571,
          lineNumber: 'TS19E21LBLK',
          name: 'Pom Pom Beanie Hat',
          unitPrice: '14.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/pom-pom-beanie-hat-6899259',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E21LBLK_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E21LBLK_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E21LBLK_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E21LBLK_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E21LBLK_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E21LBLK_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E21LBLK_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E21LBLK_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E21LBLK_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E21LBLK_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E21LBLK_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E21LBLK_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E21LBLK_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E21LBLK_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E21LBLK_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E21LBLK_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 28767784,
          lineNumber: 'TS19E01MCAM',
          name: 'Ribbed Turn Up Beanie Hat',
          unitPrice: '12.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/ribbed-turn-up-beanie-hat-6891161',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/c28b70.jpg',
              swatchProduct: {
                productId: 28767784,
                grouping: 'TS19E01MCAM',
                lineNumber: '19E01MCAM',
                name: 'Ribbed Turn Up Beanie Hat',
                shortDescription: 'Ribbed Turn Up Beanie Hat',
                unitPrice: '12.00',
                wasPrice: '12.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/ribbed-turn-up-beanie-hat-6891161?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/ffffff.jpg',
              swatchProduct: {
                productId: 28767817,
                grouping: 'TS19E01MMNT',
                lineNumber: '19E01MMNT',
                name: 'Ribbed Turn Up Beanie',
                shortDescription: 'Ribbed Turn Up Beanie',
                unitPrice: '12.00',
                wasPrice: '12.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/ribbed-turn-up-beanie-6891149?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/ebb447.jpg',
              swatchProduct: {
                productId: 28767847,
                grouping: 'TS19E01MOGE',
                lineNumber: '19E01MOGE',
                name: 'Ribbed Turn Up Beanie',
                shortDescription: 'Ribbed Turn Up Beanie',
                unitPrice: '12.00',
                wasPrice: '12.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/ribbed-turn-up-beanie-6891155?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_4col_M_1.jpg',
                  },
                ],
              },
            },
          ],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 28767847,
          lineNumber: 'TS19E01MOGE',
          name: 'Ribbed Turn Up Beanie',
          unitPrice: '12.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/ribbed-turn-up-beanie-6891155',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/ebb447.jpg',
              swatchProduct: {
                productId: 28767847,
                grouping: 'TS19E01MOGE',
                lineNumber: '19E01MOGE',
                name: 'Ribbed Turn Up Beanie',
                shortDescription: 'Ribbed Turn Up Beanie',
                unitPrice: '12.00',
                wasPrice: '12.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/ribbed-turn-up-beanie-6891155?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/c28b70.jpg',
              swatchProduct: {
                productId: 28767784,
                grouping: 'TS19E01MCAM',
                lineNumber: '19E01MCAM',
                name: 'Ribbed Turn Up Beanie Hat',
                shortDescription: 'Ribbed Turn Up Beanie Hat',
                unitPrice: '12.00',
                wasPrice: '12.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/ribbed-turn-up-beanie-hat-6891161?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/ffffff.jpg',
              swatchProduct: {
                productId: 28767817,
                grouping: 'TS19E01MMNT',
                lineNumber: '19E01MMNT',
                name: 'Ribbed Turn Up Beanie',
                shortDescription: 'Ribbed Turn Up Beanie',
                unitPrice: '12.00',
                wasPrice: '12.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/ribbed-turn-up-beanie-6891149?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_4col_M_1.jpg',
                  },
                ],
              },
            },
          ],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 28767817,
          lineNumber: 'TS19E01MMNT',
          name: 'Ribbed Turn Up Beanie',
          unitPrice: '12.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/ribbed-turn-up-beanie-6891149',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/ffffff.jpg',
              swatchProduct: {
                productId: 28767817,
                grouping: 'TS19E01MMNT',
                lineNumber: '19E01MMNT',
                name: 'Ribbed Turn Up Beanie',
                shortDescription: 'Ribbed Turn Up Beanie',
                unitPrice: '12.00',
                wasPrice: '12.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/ribbed-turn-up-beanie-6891149?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MMNT_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/c28b70.jpg',
              swatchProduct: {
                productId: 28767784,
                grouping: 'TS19E01MCAM',
                lineNumber: '19E01MCAM',
                name: 'Ribbed Turn Up Beanie Hat',
                shortDescription: 'Ribbed Turn Up Beanie Hat',
                unitPrice: '12.00',
                wasPrice: '12.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/ribbed-turn-up-beanie-hat-6891161?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MCAM_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/ebb447.jpg',
              swatchProduct: {
                productId: 28767847,
                grouping: 'TS19E01MOGE',
                lineNumber: '19E01MOGE',
                name: 'Ribbed Turn Up Beanie',
                shortDescription: 'Ribbed Turn Up Beanie',
                unitPrice: '12.00',
                wasPrice: '12.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/ribbed-turn-up-beanie-6891155?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E01MOGE_4col_M_1.jpg',
                  },
                ],
              },
            },
          ],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 28010343,
          lineNumber: 'TS19E11LDKG',
          name: 'Ribbed Slouch Beanie',
          unitPrice: '12.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/ribbed-slouch-beanie-6884126',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E11LDKG_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E11LDKG_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E11LDKG_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E11LDKG_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E11LDKG_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E11LDKG_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E11LDKG_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E11LDKG_4col_F_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 29176844,
          lineNumber: 'TS19E05MCRM',
          name: 'Oversized Pom Pom Beanie Hat',
          unitPrice: '14.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/oversized-pom-pom-beanie-hat-6872333',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/ffffff.jpg',
              swatchProduct: {
                productId: 29176844,
                grouping: 'TS19E05MCRM',
                lineNumber: '19E05MCRM',
                name: 'Oversized Pom Pom Beanie Hat',
                shortDescription: 'Oversized Pom Pom Beanie Hat',
                unitPrice: '14.00',
                wasPrice: '14.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/oversized-pom-pom-beanie-hat-6872333?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCRM_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/cccccc.jpg',
              swatchProduct: {
                productId: 28979546,
                grouping: 'TS19E05MCHR',
                lineNumber: '19E05MCHR',
                name: 'Big Knit Pompom Beanie Hat',
                shortDescription: 'Big Knit Pompom Beanie Hat',
                unitPrice: '14.00',
                wasPrice: '14.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/big-knit-pompom-beanie-hat-6838434?bi=0&ps=20',
                rating: '1.0',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCHR_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCHR_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCHR_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCHR_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCHR_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCHR_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCHR_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCHR_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCHR_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCHR_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCHR_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCHR_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCHR_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCHR_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCHR_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MCHR_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/d6c2c2.jpg',
              swatchProduct: {
                productId: 28979553,
                grouping: 'TS19E05MFUS',
                lineNumber: '19E05MFUS',
                name: 'Big Knit Pompom Beanie Hat',
                shortDescription: 'Big Knit Pompom Beanie Hat',
                unitPrice: '14.00',
                wasPrice: '14.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/big-knit-pompom-beanie-hat-6838439?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MFUS_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MFUS_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MFUS_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MFUS_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MFUS_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MFUS_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MFUS_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MFUS_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MFUS_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MFUS_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MFUS_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MFUS_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MFUS_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MFUS_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MFUS_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MFUS_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/cccccc.jpg',
              swatchProduct: {
                productId: 28979562,
                grouping: 'TS19E05MYLW',
                lineNumber: '19E05MYLW',
                name: 'Big Knitted Pompom Beanie Hat',
                shortDescription: 'Big Knitted Pompom Beanie Hat',
                unitPrice: '14.00',
                wasPrice: '14.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/big-knitted-pompom-beanie-hat-6840031?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MYLW_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MYLW_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MYLW_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MYLW_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MYLW_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MYLW_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MYLW_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MYLW_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MYLW_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MYLW_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MYLW_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MYLW_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MYLW_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MYLW_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MYLW_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E05MYLW_4col_M_1.jpg',
                  },
                ],
              },
            },
          ],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
        {
          productId: 28889389,
          lineNumber: 'TS19E10MCRM',
          name: 'Ribbed Fisherman Hat',
          unitPrice: '12.00',
          seoUrl:
            'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/product/ribbed-fisherman-hat-6870683',
          assets: [
            {
              assetType: 'IMAGE_SMALL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_Thumb_F_1.jpg',
            },
            {
              assetType: 'IMAGE_THUMB',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_Small_F_1.jpg',
            },
            {
              assetType: 'IMAGE_NORMAL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_LARGE',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_Zoom_F_1.jpg',
            },
          ],
          additionalAssets: [
            {
              assetType: 'IMAGE_ZOOM',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_Zoom_F_1.jpg',
            },
            {
              assetType: 'IMAGE_2COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_2col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_3COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_3col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_4COL',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_4col_F_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_SMALL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_Thumb_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_THUMB',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_Small_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_NORMAL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_LARGE',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_Large_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_ZOOM',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_Zoom_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_2COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_2col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_3COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_3col_M_1.jpg',
            },
            {
              assetType: 'IMAGE_OUTFIT_4COL',
              index: 2,
              url:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_4col_M_1.jpg',
            },
          ],
          items: [],
          bundleProducts: [],
          attributes: {},
          colourSwatches: [
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/d6cfc2.jpg',
              swatchProduct: {
                productId: 28889389,
                grouping: 'TS19E10MCRM',
                lineNumber: '19E10MCRM',
                name: 'Ribbed Fisherman Hat',
                shortDescription: 'Ribbed Fisherman Hat',
                unitPrice: '12.00',
                wasPrice: '12.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/ribbed-fisherman-hat-6870683?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MCRM_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/ffbb33.jpg',
              swatchProduct: {
                productId: 28889095,
                grouping: 'TS19E10MOGE',
                lineNumber: '19E10MOGE',
                name: 'Fisherman Hat',
                shortDescription: 'Fisherman Hat',
                unitPrice: '12.00',
                wasPrice: '12.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/fisherman-hat-6738505?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MOGE_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MOGE_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MOGE_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MOGE_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MOGE_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MOGE_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MOGE_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MOGE_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MOGE_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MOGE_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MOGE_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MOGE_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MOGE_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MOGE_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MOGE_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MOGE_4col_M_1.jpg',
                  },
                ],
              },
            },
            {
              colourName: '',
              imageUrl:
                'http://media.topshop.com/wcsstore/TopShop/images/catalog/swatch/477eeb.jpg',
              swatchProduct: {
                productId: 28979538,
                grouping: 'TS19E10MBLE',
                lineNumber: '19E10MBLE',
                name: 'Ribbed Fisherman Hat',
                shortDescription: 'Ribbed Fisherman Hat',
                unitPrice: '12.00',
                wasPrice: '12.00',
                productUrl:
                  '/en/tsuk/product/bags-accessories-1702216/hats-463/ribbed-fisherman-hat-6842362?bi=0&ps=20',
                rating: '',
                assets: [
                  {
                    assetType: 'IMAGE_SMALL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MBLE_Thumb_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_THUMB',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MBLE_Small_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_NORMAL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MBLE_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_LARGE',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MBLE_Zoom_F_1.jpg',
                  },
                ],
                additionalAssets: [
                  {
                    assetType: 'IMAGE_ZOOM',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MBLE_Zoom_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_2COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MBLE_2col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_3COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MBLE_3col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_4COL',
                    index: 1,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MBLE_4col_F_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_SMALL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MBLE_Thumb_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_THUMB',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MBLE_Small_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_NORMAL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MBLE_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_LARGE',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MBLE_Large_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_ZOOM',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MBLE_Zoom_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_2COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MBLE_2col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_3COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MBLE_3col_M_1.jpg',
                  },
                  {
                    assetType: 'IMAGE_OUTFIT_4COL',
                    index: 2,
                    url:
                      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E10MBLE_4col_M_1.jpg',
                  },
                ],
              },
            },
          ],
          tpmLinks: [],
          bundleSlots: [],
          ageVerificationRequired: false,
          isBundleOrOutfit: false,
        },
      ],
      totalProducts: '157',
      categoryTitle: 'Hats',
      categoryDescription:
        'Shop new season hats at Topshop. From the trending sporty-feel cap to textured fedoras and floppy hats. Order online and get free delivery on orders over £50.',
      isLoadingAll: false,
      isLoadingMore: false,
      location: {
        pathname: '/en/tsuk/category/bags-accessories-1702216/hats-463',
        search: '',
        hash: '',
        state: null,
        action: 'PUSH',
        key: 'd8hmx6',
        query: {},
        $searchBase: {
          search: '',
          searchBase: '',
        },
      },
      categoryRefinement: {
        refinementOptions: [],
      },
      canonicalUrl:
        'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/category/bags-accessories-1702216/hats-463/N-7vpZdgl',
      cmsPage: {
        pageId: 80907,
        pageName: 'categoryheader-0000080907',
        baseline: '18',
        revision: '0',
        lastPublished: '2017-03-13 16:07:45.951508',
        contentPath:
          'cms/pages/categoryheader/categoryheader-0000080907/categoryheader-0000080907.html',
        seoUrl: '',
        mobileCMSUrl: '',
      },
      version: '1.7',
      description: 'undefined',
    },
    returnHistory: {
      returns: [],
    },
    productsSearch: {
      open: false,
    },
    productViews: {
      productIsActive: true,
    },
    quickview: {
      product: {},
    },
    quiz: {
      points: 0,
      page: 0,
    },
    recentlyViewed: [
      {
        productId: 29235330,
        name: 'Double Pom Beanie',
        imageUrl:
          'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E12MBLK_Small_F_1.jpg',
      },
      {
        productId: 28775493,
        name: 'Tapestry Tote Bag',
        imageUrl:
          'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS24W09MPNK_Small_F_1.jpg',
      },
      {
        productId: 29370178,
        name: 'Rainbow Pom Pom Ribbed Beanie Hat',
        imageUrl:
          'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19E03MBLK_Small_F_1.jpg',
      },
      {
        productId: 29370388,
        name: 'Classic Beret Hat',
        imageUrl:
          'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MBLK_Small_F_1.jpg',
      },
      {
        productId: 27953779,
        name: 'Orange Sporty Tubes Ankle Socks',
        imageUrl:
          'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C06LOGE_Small_F_1.jpg',
      },
      {
        productId: 29435579,
        name: 'Cropped Rib Glitter Ankle Socks',
        imageUrl:
          'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08L14LBGD_Small_F_1.jpg',
      },
      {
        productId: 27984088,
        name: 'Grey Sporty Tube Ankle Socks',
        imageUrl:
          'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS08C09LGRY_Small_F_1.jpg',
      },
      {
        productId: 28889501,
        name: 'Flat Lens Round Sunglasses',
        imageUrl:
          'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS22P14LGLD_Small_F_1.jpg',
      },
      {
        productId: 29370510,
        name: 'Classic Beret',
        imageUrl:
          'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS19K15MPPK_Small_F_1.jpg',
      },
      {
        productId: 29433090,
        name: 'TALL Hold Power Joni Jeans',
        imageUrl:
          'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS30A31MBLK_Small_F_1.jpg',
      },
    ],
    recommendations: {
      recommendations: null,
      position: 0,
      sizeLimit: 5,
    },
    refinements: {
      isShown: false,
      selectedOptions: {},
      appliedOptions: {},
      previousOptions: null,
    },
    routing: {
      location: {
        pathname: '/order-complete',
        search: '',
        hash: '',
        state: null,
        action: 'PUSH',
        key: 'eewniv',
        hostname: 'local.m.topshop.com',
        protocol: 'https:',
        query: {},
        $searchBase: {
          search: '',
          searchBase: '',
        },
      },
      visited: [
        '/',
        '/en/tsuk/category/bags-accessories-1702216/hats-463',
        '/en/tsuk/product/double-pom-beanie-6844665',
        '/en/tsuk/category/bags-accessories-1702216/hats-463',
        '/en/tsuk/product/bags-accessories-1702216/hats-463/classic-beret-hat-6972944',
        '/checkout',
        '/checkout/login',
        '/checkout/delivery',
        '/checkout/payment',
        '/checkout/summary',
        '/order-complete',
      ],
    },
    shippingDestination: {
      destination: 'United Kingdom',
    },
    shoppingBag: {
      totalItems: 0,
      promotionCode: null,
      currentlyAddingToBag: false,
      miniBagOpen: false,
      promotionCodeConfirmation: false,
      bag: {
        products: [],
        orderId: 0,
      },
      loadingShoppingBag: false,
      recentlyAdded: {
        product: null,
        quantity: 0,
        isMiniBagConfirmShown: false,
      },
    },
    siteOptions: {
      deliveryCountries: [
        'Albania',
        'Anguilla',
        'Armenia',
        'Australia',
        'Azerbaijan',
        'Bahamas',
        'Bangladesh',
        'Barbados',
        'Bermuda',
        'Bolivia',
        'Bosnia and Herzegovina',
        'Brunei',
        'Bulgaria',
        'Cambodia',
        'Canada',
        'Cayman Islands',
        'Central African Republic',
        'China',
        'Colombia',
        'Cook Islands',
        'Croatia',
        'Czech Republic',
        'Denmark',
        'Djibouti',
        'Eritrea',
        'Falkland Islands',
        'Faroe Islands',
        'French Guiana',
        'Gambia',
        'Greenland',
        'Guadeloupe',
        'Guernsey',
        'Guyana',
        'Hong Kong S.A.R. of China',
        'Hungary',
        'India',
        'Indonesia',
        'Jamaica',
        'Japan',
        'Jersey',
        'Kazakhstan',
        'Kenya',
        'Kiribati',
        'Korea South',
        'Kyrgyzstan',
        'Lithuania',
        'Macau S.A.R. of China',
        'Madagascar',
        'Maldives',
        'Marshall Islands',
        'Mexico',
        'Moldova',
        'Morocco',
        'Nepal',
        'New Caledonia',
        'New Zealand',
        'Niger',
        'Pakistan',
        'Panama',
        'Poland',
        'Puerto Rico',
        'Romania',
        'Saint Kitts and Nevis',
        'Saint Lucia',
        'Saint Pierre and Miquelon',
        'Samoa',
        'Sao Tome and Principe',
        'Serbia',
        'Seychelles',
        'S Georgia & S Sandwich Islands',
        'Solomon Islands',
        'South Africa',
        'Sri Lanka',
        'St Vincent and the Grenadines',
        'Suriname',
        'Sweden',
        'Taiwan',
        'Togo',
        'Tunisia',
        'Turkey',
        'Turkmenistan',
        'Tuvalu',
        'United Kingdom',
        'Vanuatu',
        'Vietnam',
        'Virgin Islands British',
      ],
      USStates: [
        'AL',
        'AK',
        'AZ',
        'AR',
        'CA',
        'CO',
        'CT',
        'DE',
        'DC',
        'FL',
        'GA',
        'HI',
        'ID',
        'IL',
        'IN',
        'IA',
        'KS',
        'KY',
        'LA',
        'ME',
        'MD',
        'MA',
        'MI',
        'MN',
        'MS',
        'MO',
        'MT',
        'NE',
        'NV',
        'NH',
        'NJ',
        'NM',
        'NY',
        'NC',
        'ND',
        'OH',
        'OK',
        'OR',
        'PA',
        'RI',
        'SC',
        'SD',
        'TN',
        'TX',
        'UT',
        'VT',
        'VA',
        'WA',
        'WV',
        'WI',
        'WY',
      ],
      expiryYears: [
        '2017',
        '2018',
        '2019',
        '2020',
        '2021',
        '2022',
        '2023',
        '2024',
        '2025',
        '2026',
        '2027',
        '2028',
      ],
      expiryMonths: [
        '01',
        '02',
        '03',
        '04',
        '05',
        '06',
        '07',
        '08',
        '09',
        '10',
        '11',
        '12',
      ],
      peakService: false,
      billingCountries: [
        'Albania',
        'Andorra',
        'Anguilla',
        'Argentina',
        'Armenia',
        'Australia',
        'Austria',
        'Azerbaijan',
        'Bahamas',
        'Bangladesh',
        'Barbados',
        'Belgium',
        'Bermuda',
        'Bolivia',
        'Bosnia and Herzegovina',
        'Brunei',
        'Bulgaria',
        'Cambodia',
        'Canada',
        'Cayman Islands',
        'Central African Republic',
        'Chile',
        'China',
        'Colombia',
        'Cook Islands',
        'Croatia',
        'Czech Republic',
        'Denmark',
        'Djibouti',
        'Eritrea',
        'Estonia',
        'Falkland Islands',
        'Faroe Islands',
        'Finland',
        'France',
        'French Guiana',
        'French Southern Territories',
        'Gambia',
        'Germany',
        'Greece',
        'Greenland',
        'Guadeloupe',
        'Guernsey',
        'Guyana',
        'Holy See',
        'Hong Kong S.A.R. of China',
        'Hungary',
        'India',
        'Indonesia',
        'Ireland',
        'Italy',
        'Jamaica',
        'Japan',
        'Jersey',
        'Kazakhstan',
        'Kenya',
        'Kiribati',
        'Korea South',
        'Kyrgyzstan',
        'Latvia',
        'Libya',
        'Liechtenstein',
        'Lithuania',
        'Luxembourg',
        'Macau S.A.R. of China',
        'Madagascar',
        'Malaysia',
        'Maldives',
        'Marshall Islands',
        'Mexico',
        'Moldova',
        'Monaco',
        'Montenegro',
        'Morocco',
        'Nepal',
        'Netherlands',
        'New Caledonia',
        'New Zealand',
        'Niger',
        'Norway',
        'Pakistan',
        'Palau',
        'Panama',
        'Philippines',
        'Poland',
        'Portugal',
        'Puerto Rico',
        'Romania',
        'S Georgia & S Sandwich Islands',
        'Saint Kitts and Nevis',
        'Saint Lucia',
        'Saint Pierre and Miquelon',
        'Samoa',
        'San Marino',
        'Sao Tome and Principe',
        'Serbia',
        'Seychelles',
        'Singapore',
        'Slovakia',
        'Solomon Islands',
        'South Africa',
        'Spain',
        'Sri Lanka',
        'St Vincent and the Grenadines',
        'Suriname',
        'Svalbard and Jan Mayen',
        'Sweden',
        'Switzerland',
        'Taiwan',
        'Togo',
        'Tunisia',
        'Turkey',
        'Turkmenistan',
        'Tuvalu',
        'Ukraine',
        'United Kingdom',
        'United States',
        'Vanuatu',
        'Vietnam',
        'Virgin Islands British',
      ],
      titles: ['Mr', 'Mrs', 'Ms', 'Miss', 'Dr'],
      currencyCode: 'GBP',
      version: '1.7',
    },
    paymentMethods: [
      {
        value: 'VISA',
        type: 'CARD',
        label: 'Visa',
        description: 'Pay with VISA',
        icon: 'icon_visa.gif',
        validation: {
          cardNumber: {
            length: 16,
            message: 'A 16 digit card number is required',
          },
          cvv: {
            length: 3,
            message: '3 digits required',
          },
          expiryDate: 'MM/YY',
          startDate: null,
        },
      },
      {
        value: 'MCARD',
        type: 'CARD',
        label: 'MasterCard',
        description: 'Pay with MasterCard',
        icon: 'icon_mastercard.gif',
        validation: {
          cardNumber: {
            length: 16,
            message: 'A 16 digit card number is required',
          },
          cvv: {
            length: 3,
            message: '3 digits required',
          },
          expiryDate: 'MM/YY',
          startDate: null,
        },
      },
      {
        value: 'AMEX',
        type: 'CARD',
        label: 'American Express',
        description: 'Pay with American Express',
        icon: 'icon_amex.gif',
        validation: {
          cardNumber: {
            length: 15,
            message: 'A 15 digit card number is required',
          },
          cvv: {
            length: 4,
            message: '4 digits required',
          },
          expiryDate: 'MM/YY',
          startDate: null,
        },
      },
      {
        value: 'SWTCH',
        type: 'CARD',
        label: 'Switch/Maestro',
        description: 'Pay with Switch / Maestro',
        icon: 'icon_switch.gif',
        validation: {
          cardNumber: {
            length: 16,
            message: 'A 16 digit card number is required',
          },
          cvv: {
            length: 3,
            message: '3 digits required',
          },
          expiryDate: 'MM/YY',
          startDate: null,
        },
      },
      {
        value: 'ACCNT',
        type: 'OTHER_CARD',
        label: 'Account Card',
        description: 'Pay with Account Card',
        icon: 'icon_account-card.gif',
        validation: {
          cardNumber: {
            length: 16,
            message: 'A 16 digit card number is required',
          },
          cvv: {
            length: 3,
            message: '3 digits required',
          },
          expiryDate: 'MM/YY',
          startDate: null,
        },
      },
      {
        value: 'PYPAL',
        type: 'OTHER',
        label: 'Paypal',
        description: 'Check out with your PayPal account',
        icon: 'icon_paypal.gif',
      },
      {
        value: 'MPASS',
        type: 'OTHER',
        label: 'MasterPass',
        description: 'Check out with you MasterPass account',
        icon: 'icon_masterpass.gif',
      },
    ],
    sorting: {
      currentSortOption: null,
    },
    storeLocator: {
      currentLat: 51.515615,
      currentLng: -0.1432734,
      currentZoom: 14,
      filters: {
        today: {
          applied: false,
          selected: false,
          disabled: false,
        },
        brand: {
          applied: false,
          selected: false,
          disabled: false,
        },
        parcel: {
          selected: true,
          applied: true,
          disabled: false,
        },
        other: {
          applied: false,
          selected: false,
          disabled: false,
        },
      },
      loading: false,
      mapExpanded: false,
      noStoresFound: false,
      stores: [],
      selectedStore: [],
      countries: [
        'Armenia',
        'Australia',
        'Bahrain',
        'Brazil',
        'Canada',
        'Channel Islands',
        'Chile',
        'Cyprus',
        'Czech Republic',
        'Denmark',
        'Egypt',
        'Eire',
        'France',
        'Georgia',
        'Germany',
        'Gibraltar',
        'Hong Kong',
        'Indonesia',
        'Isle Of Man',
        'Israel',
        'Kazakhstan',
        'Kuwait',
        'Malaysia',
        'Malta',
        'Netherlands',
        'New Zealand',
        'Peru',
        'Philippines',
        'Portugal',
        'Qatar',
        'Russia',
        'Saudi Arabia',
        'Singapore',
        'Slovenia',
        'South Africa',
        'Thailand',
        'Ukraine',
        'United Arab Emirates',
        'United Kingdom',
        'United States',
        'Vietnam',
      ],
    },
    swatches: {
      maxSwatches: 4,
      products: {},
    },
    topNavMenu: {
      open: false,
      scrollToTop: false,
    },
    userLocator: {
      searchTerm: '',
      predictions: [],
      selectedPlaceDetails: {},
      getCurrentLocationError: false,
      countries: [],
      pending: false,
    },
    viewport: {
      height: 608,
      width: 409,
      pageHeight: 0,
      iosAgent: false,
      media: 'mobile',
      loadedMedia: ['mobile', 'desktop'],
      touch: false,
    },
    sandbox: {
      pages: {},
    },
  }
}

export function orderFromFrance() {
  return {
    orderCompleted: {
      orderId: 1816058,
      subTotal: '',
      returnPossible: false,
      returnRequested: false,
      deliveryMethod: 'Livraison standard',
      deliveryDate: 'jeudi 21 septembre 2017',
      deliveryCost: '5.00',
      deliveryCarrier: 'Livraison standard',
      deliveryPrice: '5.00',
      totalOrderPrice: '25.00',
      totalOrdersDiscountLabel: '',
      totalOrdersDiscount: '',
      billingAddress: {
        name: 'Mlle Jane Doe',
        address1: '17 avenue Jules Ferry',
        address2: 'Provence-Alpes-Cote d',
        address3: '83140 SIX-FOURS-LES-PLAGES',
        country: 'France',
      },
      deliveryAddress: {
        name: 'Mlle Jane Doe',
        address1: '17 avenue Jules Ferry',
        address2: 'Provence-Alpes-Cote d',
        address3: '83140 SIX-FOURS-LES-PLAGES',
        country: 'France',
      },
      orderLines: [
        {
          lineNo: '19E03MBLK',
          name: 'Bonnet côtelé avec pompon arc-en-ciel',
          size: 'ONE',
          colour: 'NOIR',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShopFR/images/catalog/TS19E03MBLK_Small_M_1.jpg',
          quantity: 1,
          unitPrice: '20.00',
          discount: '',
          total: '20.00',
          nonRefundable: false,
        },
      ],
      paymentDetails: [
        {
          paymentMethod: 'Visa',
          cardNumberStar: '************4444',
          totalCost: '25,00 €',
        },
      ],
      currencyConversion: {
        currencyCode: '1.2;',
        currencyRate: 'EUR',
      },
    },
    orderSummary: {
      basket: {
        orderId: 1816058,
        subTotal: '20.00',
        total: '30.00',
        totalBeforeDiscount: '.00',
        deliveryOptions: [
          {
            deliveryOptionId: 33124,
            label: 'Livraison standard 5,00 €',
            selected: true,
          },
          {
            deliveryOptionId: 42042,
            label: 'Livraison Express 3 10,00 €',
            selected: false,
          },
        ],
        promotions: [],
        discounts: [],
        products: [
          {
            productId: 29370253,
            catEntryId: 29370294,
            orderItemId: 8225458,
            shipModeId: 33124,
            lineNumber: '19E03MBLK',
            size: 'ONE',
            name: 'Bonnet côtelé avec pompon arc-en-ciel',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '20.00',
            totalPrice: '20.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShopFR/images/catalog/TS19E03MBLK_Thumb_M_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShopFR/images/catalog/TS19E03MBLK_Small_M_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShopFR/images/catalog/TS19E03MBLK_2col_M_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShopFR/images/catalog/TS19E03MBLK_Zoom_M_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
        savedProducts: [],
        ageVerificationRequired: false,
        restrictedDeliveryItem: false,
        inventoryPositions: {
          item_1: {
            partNumber: '602017001141908',
            catentryId: '29370294',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 7,
                ffmcenterId: 13057,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
        },
      },
      deliveryLocations: [
        {
          deliveryLocationType: 'HOME',
          label: 'HOME',
          selected: true,
          deliveryMethods: [
            {
              shipModeId: 33124,
              deliveryType: 'HOME_STANDARD',
              label: 'Livraison standard',
              additionalDescription: "jusqu'à 5 jours ouvrés",
              cost: '5.00',
              selected: true,
              deliveryOptions: [],
            },
            {
              shipModeId: 42042,
              deliveryType: 'HOME_EXPRESS',
              label: 'Livraison Express 3',
              additionalDescription: '3 Jours',
              cost: '10.00',
              selected: false,
              deliveryOptions: [],
            },
          ],
        },
      ],
      giftCards: [],
      deliveryInstructions: '',
      smsMobileNumber: '',
      shippingCountry: 'France',
      savedAddresses: [],
      ageVerificationDeliveryConfirmationRequired: false,
      estimatedDelivery: ['Pas plus tard que mercredi 20 septembre 2017'],
      version: '1.7',
    },
    orderSummaryError: {},
    orderError: false,
    isDeliverySameAsBilling: true,
    useDeliveryAsBilling: true,
    verifyPayment: false,
    showExpressDeliveryOptionsOnSummary: false,
    editingDetails: false,
    storeUpdating: false,
    partialOrderSummary: {
      basket: {
        orderId: 1816058,
        subTotal: '20.00',
        total: '30.00',
        totalBeforeDiscount: '.00',
        deliveryOptions: [
          {
            deliveryOptionId: 33124,
            label: 'Livraison standard 5,00 €',
            selected: true,
          },
          {
            deliveryOptionId: 42042,
            label: 'Livraison Express 3 10,00 €',
            selected: false,
          },
        ],
        promotions: [],
        discounts: [],
        products: [
          {
            productId: 29370253,
            catEntryId: 29370294,
            orderItemId: 8225458,
            shipModeId: 33124,
            lineNumber: '19E03MBLK',
            size: 'ONE',
            name: 'Bonnet côtelé avec pompon arc-en-ciel',
            quantity: 1,
            lowStock: false,
            inStock: true,
            unitPrice: '20.00',
            totalPrice: '20.00',
            assets: [
              {
                assetType: 'IMAGE_SMALL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShopFR/images/catalog/TS19E03MBLK_Thumb_M_1.jpg',
              },
              {
                assetType: 'IMAGE_THUMB',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShopFR/images/catalog/TS19E03MBLK_Small_M_1.jpg',
              },
              {
                assetType: 'IMAGE_NORMAL',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShopFR/images/catalog/TS19E03MBLK_2col_M_1.jpg',
              },
              {
                assetType: 'IMAGE_LARGE',
                index: 1,
                url:
                  'http://media.topshop.com/wcsstore/TopShopFR/images/catalog/TS19E03MBLK_Zoom_M_1.jpg',
              },
            ],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
        savedProducts: [],
        ageVerificationRequired: false,
        restrictedDeliveryItem: false,
        inventoryPositions: {
          item_1: {
            partNumber: '602017001141908',
            catentryId: '29370294',
            inventorys: [
              {
                cutofftime: '2100',
                quantity: 7,
                ffmcenterId: 13057,
                expressdates: ['2017-09-13', '2017-09-14'],
              },
            ],
          },
        },
      },
    },
  }
}
