import {
  getDataForStoreFinder,
  getDataForCollectFromStore,
  getDataForFindInStore,
} from '../store-locator'

const createState = () => {
  return {
    userLocator: {
      selectedPlaceDetails: {
        description: 'Oxford Street',
      },
      searchTerm: 'Oxfor',
    },
    storeLocator: {
      stores: [{}],
      selectedCountry: 'Poland',
    },
  }
}
describe('Store locator analytics data', () => {
  describe('getDataForCollectFromStore', () => {
    it('should return data for collect from store', () => {
      const state = createState()
      expect(getDataForCollectFromStore(state)).toEqual({
        pageName: 'Store Finder',
        firstCategory: 'Store Finder',
        prop9: 'Oxford Street',
        prop29: 1,
        events: 'event25',
        granularPageName: 'Store Finder Checkout',
        prop30: 'United Kingdom',
        isCheckout: true,
      })
    })
  })
  describe('getDataForFindInStore', () => {
    it('should return data for find in store', () => {
      const state = createState()
      expect(getDataForFindInStore(state)).toEqual({
        pageName: 'Store Finder',
        firstCategory: 'Store Finder',
        prop9: 'Oxford Street',
        prop29: 1,
        events: 'event25',
        granularPageName: 'Store Finder Product',
        prop30: 'United Kingdom',
      })
    })
  })
  describe('getDataForStoreFinder', () => {
    it('should return data for store locator landing', () => {
      const state = {
        storeLocator: {
          stores: [],
          selectedCountry: 'United Kingdom',
        },
      }
      expect(getDataForStoreFinder(state)).toEqual({
        pageName: 'Store Finder',
        firstCategory: 'Store Finder',
        granularPageName: 'Store Finder Landing',
      })
    })
    it('should return data for store locator without selectedPlaceDetails description', () => {
      const state = {
        userLocator: {
          selectedPlaceDetails: {},
          searchTerm: 'Oxfor',
        },
        storeLocator: {
          stores: [{}],
          selectedCountry: 'Poland',
        },
      }
      expect(getDataForStoreFinder(state)).toEqual({
        pageName: 'Store Finder',
        firstCategory: 'Store Finder',
        prop9: 'Oxfor',
        prop29: 1,
        events: 'event25',
        granularPageName: 'Store Finder',
        prop30: 'Poland',
      })
    })
  })
})
