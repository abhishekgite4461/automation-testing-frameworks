import getPLPPageData from '../plp'

const createState = () => {
  return {
    routing: {
      visited: [],
    },
    sorting: {
      currentSortOption: null,
    },
    grid: {
      options: {
        default: [2, 3, 4],
        mobile: [1, 2, 3],
      },
      columns: 2,
    },
    config: {
      brandCode: 'ts',
    },
    refinements: {
      selectedOptions: {
        fabric: ['faux fur'],
        price: [43, 125],
      },
    },
    products: {
      breadcrumbs: [
        {
          label: 'Home',
        },
        {
          label: 'Clothing',
          category: '203984',
        },
        {
          label: 'Denim',
          category: '203984,525523',
        },
      ],
      products: [{}, {}],
      totalProducts: '2',
    },
    productViews: {
      productIsActive: true,
    },
  }
}

describe('PLP Page Data', () => {
  describe('No search term', () => {
    it('should return undefined if no breadcrumbs', () => {
      const state = createState()
      state.products.breadcrumbs = []
      expect(getPLPPageData(state)).toBeUndefined()
    })

    it('should return correct data if there are breadcrumbs', () => {
      const state = createState()
      expect(getPLPPageData(state)).toEqual({
        pageName: 'Category Display',
        granularPageName: 'Category:Clothing > Denim',
        firstCategory: 'Clothing',
        prop2: 2,
        eVar4: 'TS:Category:Clothing > Denim',
        prop23: 'Layout:2:Product view (Default)',
        prop27: 'Best Match (Default)',
        prop7: 'TS:Fabric:faux fur:Price:43,125',
      })
    })

    it('should return correct data (with prop2 to zero) if there are breadcrumbs and no products', () => {
      const state = createState()
      state.products.products = []
      state.products.totalProducts = null
      expect(getPLPPageData(state)).toEqual({
        eVar4: 'TS:Category:Clothing > Denim',
        firstCategory: 'Clothing',
        granularPageName: 'Category:Clothing > Denim',
        pageName: 'Category Display',
        prop2: 'zero',
        prop23: 'Layout:2:Product view (Default)',
        prop27: 'Best Match (Default)',
        prop7: 'TS:Fabric:faux fur:Price:43,125',
      })
    })

    it('should return correct data (with uppecased category) if breadcrumbs category name contains -', () => {
      const state = createState()
      state.products.breadcrumbs = [
        {
          label: 'Home',
        },
        {
          label: 'new-category',
          category: '203984',
        },
      ]
      expect(getPLPPageData(state)).toEqual({
        eVar4: 'TS:Category:New-Category',
        firstCategory: 'new-category',
        granularPageName: 'Category:New-Category',
        pageName: 'Category Display',
        prop2: 2,
        prop23: 'Layout:2:Product view (Default)',
        prop27: 'Best Match (Default)',
        prop7: 'TS:Fabric:faux fur:Price:43,125',
      })
    })

    it('should return correct data if breadcrumbs category name contains -', () => {
      const state = createState()
      state.products.breadcrumbs = [
        {
          label: 'Home',
        },
        {
          label: 'new-',
          category: '203984',
        },
      ]
      expect(getPLPPageData(state)).toEqual({
        eVar4: 'TS:Category:New-',
        firstCategory: 'new-',
        granularPageName: 'Category:New-',
        pageName: 'Category Display',
        prop2: 2,
        prop23: 'Layout:2:Product view (Default)',
        prop27: 'Best Match (Default)',
        prop7: 'TS:Fabric:faux fur:Price:43,125',
      })
    })

    it('should return correct data (with prop2 to zero) if there are breadcrumbs and no products', () => {
      const state = createState()
      state.products.products = []
      state.products.totalProducts = null
      expect(getPLPPageData(state)).toEqual({
        eVar4: 'TS:Category:Clothing > Denim',
        firstCategory: 'Clothing',
        granularPageName: 'Category:Clothing > Denim',
        pageName: 'Category Display',
        prop2: 'zero',
        prop23: 'Layout:2:Product view (Default)',
        prop27: 'Best Match (Default)',
        prop7: 'TS:Fabric:faux fur:Price:43,125',
      })
    })

    it('should return correct data (with uppecased category) if breadcrumbs category name contains -', () => {
      const state = createState()
      state.products.breadcrumbs = [
        {
          label: 'Home',
        },
        {
          label: 'new-category',
          category: '203984',
        },
      ]
      expect(getPLPPageData(state)).toEqual({
        eVar4: 'TS:Category:New-Category',
        firstCategory: 'new-category',
        granularPageName: 'Category:New-Category',
        pageName: 'Category Display',
        prop2: 2,
        prop23: 'Layout:2:Product view (Default)',
        prop27: 'Best Match (Default)',
        prop7: 'TS:Fabric:faux fur:Price:43,125',
      })
    })

    it('should return correct data if breadcrumbs category name contains -', () => {
      const state = createState()
      state.products.breadcrumbs = [
        {
          label: 'Home',
        },
        {
          label: 'new-',
          category: '203984',
        },
      ]
      expect(getPLPPageData(state)).toEqual({
        eVar4: 'TS:Category:New-',
        firstCategory: 'new-',
        granularPageName: 'Category:New-',
        pageName: 'Category Display',
        prop2: 2,
        prop23: 'Layout:2:Product view (Default)',
        prop27: 'Best Match (Default)',
        prop7: 'TS:Fabric:faux fur:Price:43,125',
      })
    })
  })

  describe('With search term', () => {
    it('should return undefined if there is one product', () => {
      const state = createState()
      state.products.searchTerm = 'jeans'
      state.products.products = [{}]
      expect(getPLPPageData(state)).toBeUndefined()
    })

    it('should return undefined if there are no products', () => {
      const state = createState()
      state.products.searchTerm = 'jeans'
      state.products.products = null
      expect(getPLPPageData(state)).toBeUndefined()
    })

    it('should return correct data if there are products', () => {
      const state = createState()
      state.products.searchTerm = 'jeans'
      expect(getPLPPageData(state)).toEqual({
        firstCategory: 'Search',
        granularPageName: 'Search',
        pageName: 'Category Display',
        eVar1: 'jeans',
        eVar4: 'TS:Search',
        events: null,
        prop1: 'jeans',
        prop2: 2,
        prop23: 'Layout:2:Product view (Default)',
        prop27: 'Best Match (Default)',
        prop7: 'TS:Fabric:faux fur:Price:43,125',
      })
    })

    it('should return correct data (with prop2 to zero) if no products', () => {
      const state = createState()
      state.products.searchTerm = 'jeans'
      state.products.products = []
      state.products.totalProducts = null
      expect(getPLPPageData(state)).toEqual({
        eVar1: 'jeans',
        eVar4: 'TS:Search',
        events: null,
        firstCategory: 'Search',
        granularPageName: 'Search',
        pageName: 'Category Display',
        prop1: 'jeans',
        prop2: 'zero',
        prop23: 'Layout:2:Product view (Default)',
        prop27: 'Best Match (Default)',
        prop7: 'TS:Fabric:faux fur:Price:43,125',
      })
    })

    it('should return correct data (with prop2 to zero) if no products', () => {
      const state = createState()
      state.products.searchTerm = 'jeans'
      state.products.products = []
      state.products.totalProducts = null
      expect(getPLPPageData(state)).toEqual({
        eVar1: 'jeans',
        eVar4: 'TS:Search',
        events: null,
        firstCategory: 'Search',
        granularPageName: 'Search',
        pageName: 'Category Display',
        prop1: 'jeans',
        prop2: 'zero',
        prop23: 'Layout:2:Product view (Default)',
        prop27: 'Best Match (Default)',
        prop7: 'TS:Fabric:faux fur:Price:43,125',
      })
    })

    it('should return correct data if there are products but no refinements', () => {
      const state = createState()
      state.products.searchTerm = 'jeans'
      state.refinements.selectedOptions = {}
      expect(getPLPPageData(state)).toEqual({
        firstCategory: 'Search',
        granularPageName: 'Search',
        pageName: 'Category Display',
        eVar1: 'jeans',
        eVar4: 'TS:Search',
        events: 'event1',
        prop1: 'jeans',
        prop2: 2,
        prop23: 'Layout:2:Product view (Default)',
        prop27: 'Best Match (Default)',
        prop7: '',
      })
    })
  })
})
