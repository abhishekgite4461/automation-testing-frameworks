import pdpDataResolver from '../pdp'
import bundleMockData from '../../../../../../test/mocks/bundleMocks'
import productDetailsMockData from '../../../../../../test/mocks/product-detail'

jest.mock('../../product-referrals-helper', () => ({
  popReferrerProduct: jest.fn(),
  REFERRER_TYPES: {
    RECOMMENDED: 'RECOMMENDED',
    RECENTLY_VIEWED: 'RECENTLY_VIEWED',
  },
}))

const createStateForBundle = () => ({
  routing: {},
  currentProduct: bundleMockData,
})

const createStateForProduct = (
  currentProductOverrides = {},
  customState = {}
) => ({
  routing: {
    visited: ['/en/tsuk/product/petite-jacquard-bardot-dress-6787286'],
  },
  products: {
    products: null,
    searchTerm: null,
  },
  currentProduct: Object.assign(
    {},
    productDetailsMockData,
    currentProductOverrides
  ),
  ...customState,
})

describe('PDP Page Data', () => {
  afterEach(() => {
    jest.resetAllMocks()
  })

  describe('Bundle', () => {
    it('should return proper data for bundles', () => {
      const state = createStateForBundle()
      expect(pdpDataResolver(state)).toEqual({
        pageName: 'Product Display',
        products: [
          ';(02T01GBLK)Black Essential Tailored Fit Suit Jacket;;;event60=1.00;eVar42=null|eVar60=22|eVar61=22|eVar62=100|eVar68=49.00|eVar69=0|eVar78=49.00',
          ';(02T02GBLK)Black Essential Tailored Fit Suit Trousers;;;event60=1.00;eVar42=null|eVar60=22|eVar61=22|eVar62=100|eVar68=30.00|eVar69=0|eVar78=30.00',
          ';(02S03HBLK)Black Essential Waistcoat;;;event60=1.00;eVar42=null|eVar60=5|eVar61=5|eVar62=100|eVar68=28.00|eVar69=0|eVar78=28.00',
        ],
        granularPageName:
          'Prod Detail:(BUNDLE_SS16_02T01GBLK_3)3 Piece Black Essential Tailored Fit Suit',
        events: 'prodView,event2,event60',
        firstCategory: 'Product',
      })
    })
  })

  describe('Product', () => {
    it('should return proper data for productDetail', () => {
      const state = createStateForProduct()
      expect(pdpDataResolver(state)).toEqual({
        pageName: 'Product Display',
        products:
          ';(07L04JWNA)Tencel Buffalo Duster Coat;;;event60=0.86;eVar42=3|eVar60=7|eVar61=6|eVar62=86|eVar68=85.00|eVar69=0|eVar78=85.00',
        granularPageName: 'Prod Detail:(07L04JWNA)Tencel Buffalo Duster Coat',
        firstCategory: 'Product',
        events: 'prodView,event2,event60=0.86,event108=3',
      })
    })

    it('should return proper data for productDetail when search', () => {
      const state = {
        ...createStateForProduct(),
        products: {
          products: [{}],
          searchTerm: 'PETITE Jacquard Bardot Dress',
        },
        routing: {
          visited: [
            '/',
            '/search/',
            '/en/tsuk/product/petite-jacquard-bardot-dress-6787286',
          ],
        },
      }
      expect(pdpDataResolver(state)).toEqual({
        pageName: 'Product Display',
        products:
          ';(07L04JWNA)Tencel Buffalo Duster Coat;;;event60=0.86;eVar42=3|eVar60=7|eVar61=6|eVar62=86|eVar68=85.00|eVar69=0|eVar78=85.00',
        granularPageName: 'Prod Detail:(07L04JWNA)Tencel Buffalo Duster Coat',
        firstCategory: 'Product',
        events: 'prodView,event2,event60=0.86,event108=3,event1',
        eVar1: 'petite jacquard bardot dress',
        prop1: 'petite jacquard bardot dress',
        prop2: 1,
      })
    })

    it('should return the expected eVar69 value for price differences', () => {
      expect(
        pdpDataResolver(
          createStateForProduct({
            wasWasPrice: 30,
            unitPrice: 10,
          })
        ).products
      ).toContain('eVar69=66.67')

      expect(
        pdpDataResolver(
          createStateForProduct({
            wasWasPrice: 30,
            wasPrice: 20,
            unitPrice: 10,
          })
        ).products
      ).toContain('eVar69=66.67')

      expect(
        pdpDataResolver(
          createStateForProduct({
            wasWasPrice: 20,
            wasPrice: 30,
            unitPrice: 10,
          })
        ).products
      ).toContain('eVar69=50.00')
    })
  })

  describe('Reffered', () => {
    const productReferralsHelperMock = require('../../product-referrals-helper')
    const { popReferrerProduct, REFERRER_TYPES } = productReferralsHelperMock

    it('should set event130 and eVar86 for a RECOMMENDATION referral', () => {
      popReferrerProduct.mockImplementation((referralType) => {
        if (referralType !== REFERRER_TYPES.RECOMMENDED) {
          return null
        }
        return {
          fromProductId: 1,
          fromProductLineNumber: '1337PROD',
          fromProductName: 'Pink Braces',
        }
      })

      const data = pdpDataResolver(createStateForProduct())

      expect(data.eVar86).toEqual('(1337PROD) Pink Braces')
      expect(data.events).toContain('event130')
      expect(popReferrerProduct.mock.calls.length).toBe(2)
    })

    it('should set event129 and eVar86 for a RECENTLY_VIEWED referral', () => {
      popReferrerProduct.mockImplementation((referralType) => {
        if (referralType !== REFERRER_TYPES.RECENTLY_VIEWED) {
          return null
        }
        return {
          fromProductId: 1,
          fromProductLineNumber: '1337PROD',
          fromProductName: 'Pink Braces',
        }
      })

      const data = pdpDataResolver(createStateForProduct())

      expect(data.eVar86).toEqual('(1337PROD) Pink Braces')
      expect(data.events).toContain('event129')
      expect(popReferrerProduct.mock.calls.length).toBe(2)
    })

    it('should set event129 when referred from the /order-complete route', () => {
      const data = pdpDataResolver(
        createStateForProduct(null, {
          routing: {
            visited: [
              '/order-complete',
              '/en/tsuk/product/6787286',
              '/en/tsuk/product/petite-jacquard-bardot-dress-6787286',
            ],
          },
        })
      )
      expect(data.events).toContain('event129')
    })
  })
})
