import recentlyReviewedData from '../../../../../../test/mocks/recently-viewed'
import writeReviewAnalyticsDataResolver from '../write-a-review'

describe('writeReviewAnalyticsDataResolver', () => {
  const state = {
    recentlyViewed: recentlyReviewedData,
  }

  it('should resolve the expected data', () => {
    const actual = writeReviewAnalyticsDataResolver(state)
    expect(actual).toEqual({
      pageName: 'Bazaar Voice',
      pageType: 'Category',
      firstCategory: 'Bazaar Voice',
      products: `(${recentlyReviewedData[0].lineNumber})${
        recentlyReviewedData[0].name
      }`,
      events: 'event27',
      eVar24: 'Bazaarvoice^RatingsAndReviews^Action^Write^Default',
    })
  })
})
