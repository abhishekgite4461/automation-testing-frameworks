import getCMSPageData from '../cms'

describe('CMS Page Data', () => {
  const state = {
    cms: {
      pages: {
        '/e-receipt': {
          pageName: 'E Receipt',
        },
      },
    },
    routing: {
      location: {
        pathname: '/e-receipt',
      },
    },
  }

  it('should get data from sandbox pages, based on pathname', () => {
    expect(getCMSPageData(state)).toEqual(
      expect.objectContaining({
        pageName: 'E Receipt',
        granularPageName: 'E Receipt',
        firstCategory: 'E Receipt',
      })
    )
  })

  it('should handle pages which are URI encoded', () => {
    const uriEncodedState = {
      cms: {
        pages: {
          '%2Fe-receipt': {
            pageName: 'E Receipt',
          },
        },
      },
      routing: {
        location: {
          pathname: '/e-receipt',
        },
      },
    }

    expect(getCMSPageData(uriEncodedState).pageName).toBe('E Receipt')
  })

  it('should return a `pageType` of `Content`', () => {
    expect(getCMSPageData(state).pageType).toBe('Content')
  })

  it('should remove `mob - ` prefix from the `pageName`', () => {
    const prefixedState = {
      cms: {
        pages: {
          '/e-receipt': {
            pageName: 'mob - E Receipt',
          },
        },
      },
      routing: {
        location: {
          pathname: '/e-receipt',
        },
      },
    }

    expect(getCMSPageData(prefixedState).pageName).toBe('E Receipt')
  })
})
