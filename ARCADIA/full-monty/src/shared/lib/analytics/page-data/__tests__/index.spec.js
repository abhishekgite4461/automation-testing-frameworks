import pagesData from '../index'

describe('Pages dictionary', () => {
  it('should add isCheckout value to order summary data', () => {
    const orderCompletedMock = require('../order-completed')
    orderCompletedMock.default = jest
      .fn()
      .mockReturnValue({ pageName: 'Order Completed' })
    const orderCompletedData = pagesData['order-completed'].getData()
    expect(orderCompletedData).toEqual({
      pageName: 'Order Completed',
      isCheckout: true,
    })
  })
})
