import { stripCommas } from '../analytics'
import { popReferrerProduct, REFERRER_TYPES } from '../product-referrals-helper'
import { getProductAnalyticsData, getBundleAnalyticsData } from './helpers'

function bundle(state) {
  const { currentProduct } = state
  const { name, lineNumber } = currentProduct
  const safeName = stripCommas(name)

  return {
    pageName: 'Product Display',
    granularPageName: `Prod Detail:(${lineNumber})${safeName}`,
    firstCategory: 'Product',
    products: getBundleAnalyticsData(currentProduct),
    events: 'prodView,event2,event60',
  }
}

function productDetail(state) {
  const {
    currentProduct,
    routing: { visited },
    products: { products, searchTerm },
  } = state
  const { name, lineNumber, attributes } = currentProduct
  const safeName = stripCommas(name)
  const productAnalyticsData = getProductAnalyticsData(state.currentProduct)

  // The last page was a search PLP and there was 1 product, and a search term,
  // which means we were redirected here
  const redirectedFromSearch =
    visited.length > 1 &&
    visited[visited.length - 2] === '/search/' &&
    products &&
    products.length === 1 &&
    searchTerm

  return {
    products: productAnalyticsData.string,
    pageName: 'Product Display',
    granularPageName: `Prod Detail:(${lineNumber})${safeName}`,
    firstCategory: 'Product',
    events: [
      'prodView',
      'event2',
      `event60=${productAnalyticsData.events.event60}`,
    ]
      .concat(
        attributes && attributes.AverageOverallRating
          ? [`event108=${attributes.AverageOverallRating}`]
          : []
      )
      .concat(redirectedFromSearch ? ['event1'] : [])
      .join(','),
    prop2: redirectedFromSearch ? 1 : undefined,
    prop1: redirectedFromSearch ? searchTerm.toLowerCase() : undefined,
    eVar1: redirectedFromSearch ? searchTerm.toLowerCase() : undefined,
  }
}

function resolveEVar86(referral) {
  const { fromProductLineNumber, fromProductName } = referral
  return `(${fromProductLineNumber}) ${fromProductName}`
}

export default (state) => {
  const {
    currentProduct: { isBundleOrOutfit },
  } = state
  const typeBasedAnalytics = isBundleOrOutfit
    ? bundle(state)
    : productDetail(state)

  const recommendedReferred = popReferrerProduct(REFERRER_TYPES.RECOMMENDED)
  const recentlyViewedReferred = popReferrerProduct(
    REFERRER_TYPES.RECENTLY_VIEWED
  )

  // At the moment you can only be referred to a product from the order complete
  // page via the RecentlyViewed component, so it _should_ be okay to assume
  // that this is an event130.
  const referredFromOrderCompletePage =
    state.routing.visited &&
    state.routing.visited.length >= 3 &&
    state.routing.visited[state.routing.visited.length - 3].includes(
      '/order-complete'
    )

  return Object.assign(typeBasedAnalytics, {
    eVar86:
      recommendedReferred || recentlyViewedReferred
        ? resolveEVar86(recommendedReferred || recentlyViewedReferred)
        : undefined,
    events: typeBasedAnalytics.events
      .concat(recommendedReferred ? ',event130' : '')
      .concat(
        recentlyViewedReferred || referredFromOrderCompletePage
          ? ',event129'
          : ''
      ),
  })
}
