import { flatten } from 'ramda'
import { stripCommas } from '../analytics'

export function getProductAnalyticsData(product) {
  const { name, lineNumber, attributes, items, unitPrice } = product

  const safeName = stripCommas(name)
  const wasPrice = product.wasWasPrice || product.wasPrice
  const priceDifference = wasPrice
    ? ((1 - unitPrice / wasPrice) * 100).toFixed(2)
    : 0
  const sizes = items ? items.length : 0
  const sizesInStock = items
    ? items.filter(({ quantity }) => quantity > 0).length
    : 0
  const decSizesInStock = (sizesInStock / sizes).toFixed(2)
  const percSizesInStock = decSizesInStock * 100
  const averageOverallRating =
    attributes && attributes.AverageOverallRating
      ? attributes.AverageOverallRating
      : 'null'

  const evars = {
    eVar42: averageOverallRating,
    eVar60: sizes,
    eVar61: sizesInStock,
    eVar62: percSizesInStock,
    eVar68: wasPrice || unitPrice,
    eVar69: priceDifference,
    eVar78: unitPrice,
  }

  const events = {
    event60: decSizesInStock,
  }

  // Converts the events into format:
  // key=value|key=value|key=value
  const eventsString = Object.keys(events)
    .map((x) => `${x}=${events[x]}`)
    .join('|')

  // Converts the evars into format:
  // key=value|key=value|key=value
  const evarsString = Object.keys(evars)
    .map((x) => `${x}=${evars[x]}`)
    .join('|')

  const string = `;(${lineNumber})${safeName};;;${eventsString};${evarsString}`

  return { evars, events, string }
}

export function getBundleAnalyticsData(bundle) {
  const { bundleSlots } = bundle
  const items = bundleSlots.filter((slot) => slot.products.length)
  return flatten(
    items.map(({ products }) =>
      products.map((p) => getProductAnalyticsData(p).string)
    )
  )
}
