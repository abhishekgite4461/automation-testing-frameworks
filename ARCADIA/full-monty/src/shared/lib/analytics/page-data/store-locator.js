function getGeneralData(state) {
  const { selectedPlaceDetails, searchTerm } = state.userLocator
  return {
    pageName: 'Store Finder',
    firstCategory: 'Store Finder',
    prop9: selectedPlaceDetails.description
      ? selectedPlaceDetails.description
      : searchTerm,
    prop29: state.storeLocator.stores.length,
    prop30: state.storeLocator.selectedCountry,
    events: 'event25',
  }
}

export function getDataForStoreFinder(state) {
  if (state.storeLocator.stores.length === 0) {
    return {
      pageName: 'Store Finder',
      firstCategory: 'Store Finder',
      granularPageName: 'Store Finder Landing',
    }
  }

  return {
    ...getGeneralData(state),
    granularPageName: 'Store Finder',
  }
}

export function getDataForCollectFromStore(state) {
  return {
    ...getGeneralData(state),
    granularPageName: 'Store Finder Checkout',
    prop30: 'United Kingdom',
    isCheckout: true,
  }
}

export function getDataForFindInStore(state) {
  return {
    ...getGeneralData(state),
    granularPageName: 'Store Finder Product',
    prop30: 'United Kingdom',
  }
}
