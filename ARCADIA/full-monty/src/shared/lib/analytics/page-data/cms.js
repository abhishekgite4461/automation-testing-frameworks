const getPageName = (pages, cmsPageName) => {
  return (
    (pages[cmsPageName] &&
      pages[cmsPageName].pageName &&
      pages[cmsPageName].pageName.replace(/^mob - /, '')) ||
    ''
  )
}

export default (state) => {
  const {
    cms: { pages },
    routing: {
      location: { pathname },
    },
  } = state
  const pageName =
    getPageName(pages, pathname) ||
    getPageName(pages, window.encodeURIComponent(pathname))

  return {
    pageName,
    pageType: 'Content',
    granularPageName: pageName,
    firstCategory: pageName,
  }
}
