import { path } from 'ramda'

const objectToAnalyticsString = (obj) => {
  return Object.keys(obj)
    .sort((keyOne, keyTwo) => keyOne.localeCompare(keyTwo))
    .reduce((currentStrings, key) => {
      return currentStrings.concat(`${key}=${obj[key]}`)
    }, [])
    .join('|')
}

// replacement for isEmpty ramda
const isEmptyObject = (obj) =>
  !obj || (Object.keys(obj).length === 0 && obj.constructor === Object)
const isEmptyString = (s) => typeof s === 'string' && s === ''

const productToString = ({
  product,
  quantity,
  cost,
  events = {},
  eVars = {},
} = {}) => {
  // Abode Analytics Syntax of the Products Variable:
  // "Category;Product;Quantity;Price;eventN=X[|eventN2=X2];eVarN=merch_category[|eVarN2=merch_category2]"
  // note that category is empty, that's why the first semicolon
  return `;${[
    product.replace(',', ''),
    quantity,
    cost ? (cost * quantity).toFixed(2) : cost,
    objectToAnalyticsString(events),
    objectToAnalyticsString(eVars),
  ].join(';')}`
}

export default (state) => {
  const {
    account: { user = {} } = {},
    checkout: { orderCompleted },
  } = state
  const basket = path(['checkout', 'partialOrderSummary', 'basket'], state)
  if (isEmptyObject(orderCompleted)) return

  const {
    currencyConversion,
    orderId,
    orderLines,
    deliveryPrice,
    deliveryMethod,
    subTotal,
    totalOrderPrice,
    totalOrdersDiscountLabel,
    totalOrderDiscount,
    paymentDetails: [{ paymentMethod }] = [{}],
  } = orderCompleted

  const data = {
    pageName: 'Order Confirmed',
    granularPageName: 'Order Confirmed',
    firstCategory: 'Checkout',
    channel: 'Checkout',
    isCheckout: true,
    prop3: 'Order Confirmed',
    eVar6: user.userTrackingId,
    eVar16: paymentMethod,
    eVar17: orderCompleted.deliveryMethod,
    eVar40: orderId,
    eVar71: (parseFloat(totalOrderPrice) - parseFloat(deliveryPrice)).toFixed(
      2
    ),
  }

  const events = [
    'event3',
    'event69',
    'purchase',
    'event58',
    'event66',
    `event71=${
      currencyConversion && currencyConversion.currencyCode
        ? currencyConversion.currencyCode
        : '1.00'
    }`,
    'event7', // confirm & pay - allways on if the user is in Thank You page
    'event11', // successful login
  ]

  // Scenario 2. if Order Discount applied
  const orderHasDiscount =
    !isEmptyString(totalOrdersDiscountLabel) &&
    !isEmptyString(totalOrderDiscount)
  if (orderHasDiscount) {
    events.push('event6', 'event68')
    data.eVar12 = totalOrdersDiscountLabel
  }

  const products = orderLines
    .map(
      ({
        name,
        colour,
        total,
        size,
        quantity,
        discount,
        unitPrice: nowPrice,
        lineNo,
      }) => {
        if (name.toLowerCase().includes('delivery')) return undefined

        // Scenario 7. Marked Down Item
        const basketProduct =
          basket &&
          basket.products &&
          basket.products.find(({ lineNumber }) => lineNumber === lineNo)
        const wasPrice =
          (basketProduct &&
            (basketProduct.wasWasPrice || basketProduct.wasPrice)) ||
          nowPrice
        const productEvents = {
          event58: parseFloat(parseFloat(wasPrice) * quantity).toFixed(2), // wasPrice
          event66: parseFloat(parseFloat(nowPrice) * quantity).toFixed(2), // nowPrice
        }
        const productEVars = {
          eVar35: size,
          eVar36: colour,
        }

        // Scenario 6. Discounted Item
        if (!isEmptyString(discount)) {
          const discountPrice = (
            parseFloat(nowPrice) * quantity -
            parseFloat(total)
          ).toFixed(2)
          productEvents.event5 = discountPrice
          productEvents.event67 = discountPrice
          productEVars.eVar12 = discount // eVar12 = Item and Order Level Discount Label
          if (!events.includes('event5'))
            ['event5', 'event67'].map((event) => events.push(event))
        }

        if (orderHasDiscount) {
          const productPriceRatio = parseFloat(total) / parseFloat(subTotal)
          const orderPriceAfterDeliveryPrice =
            parseFloat(subTotal) + parseFloat(deliveryPrice)
          const overallDiscountForBag =
            orderPriceAfterDeliveryPrice - parseFloat(totalOrderPrice)
          const orderDiscountForItem = parseFloat(
            productPriceRatio * overallDiscountForBag
          ).toFixed(2)
          productEvents.event6 = orderDiscountForItem
          productEvents.event68 = orderDiscountForItem
        }

        return productToString({
          product: `(${lineNo})${name}`,
          quantity,
          cost: nowPrice,
          events: productEvents,
          eVars: productEVars,
        })
      }
    )
    // filter out empty elements
    .filter((value) => value)

  const deliveryProductName = orderLines.find(({ name }) =>
    name.toLowerCase().includes('delivery')
  )
  products.push(
    productToString({
      product: deliveryProductName || deliveryMethod,
      events: {
        event3: deliveryPrice,
        event69: deliveryPrice,
      },
    })
  )

  return {
    ...data,
    events: events.join(','),
    products: products.join(','),
  }
}
