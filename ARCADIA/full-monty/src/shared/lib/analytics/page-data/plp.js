import { getAnalyticsBrandCode } from '../../analytics/analytics'
import { capitalize } from '../../string-utils'

const currentSortSelector = (sort) => {
  if (sort === null || sort === undefined) return 'Best Match (Default)'
  else if (sort === 'Relevance') return 'Best Match'
  return sort
}

export default (state) => {
  const view = state.productViews.productIsActive
    ? 'Product view'
    : 'Outfit view'
  const {
    breadcrumbs,
    searchTerm,
    totalProducts,
    products = [],
  } = state.products
  const refinements = state.refinements.selectedOptions
  const analyticsBrandCode = getAnalyticsBrandCode(state.config.brandCode)

  const refinementsString = Object.keys(refinements).reduce((string, key) => {
    const capitalizeKey = capitalize(key)
    if (key === 'price') {
      return `${string}:${capitalizeKey}:${refinements[key].join(',')}`
    }
    return `${string}:${refinements[key]
      .map((val) => `${capitalizeKey}:${val}`)
      .join(':')}`
  }, '')
  const prop7 = refinementsString && `${analyticsBrandCode}${refinementsString}`
  const sort = currentSortSelector(
    state.sorting && state.sorting.currentSortOption
  )

  if (searchTerm) {
    // Is search page

    if (!products || products.length === 1) return // Do not track if its one product, the redirected pdp will do that.
    return {
      pageName: 'Category Display',
      granularPageName: 'Search',
      prop23: `Layout:${state.grid.columns}:${view} (Default)`,
      prop27: sort,
      eVar1: searchTerm.toLowerCase(),
      prop1: searchTerm.toLowerCase(),
      prop2: parseInt(totalProducts, 10) || products.length || 'zero',
      eVar4: `${analyticsBrandCode}:Search`,
      prop7,
      firstCategory: 'Search',
      events: prop7 ? null : 'event1',
    }
  } else if (breadcrumbs && breadcrumbs.length) {
    // Is category page

    // the categories had to be changed to camel case as this was one of the requirements
    // e.g new-line becomes New-Line
    const formatCategory = (category) => {
      return category
        .split('-')
        .map((s) => {
          // defensive code for leading `-` in `s`
          return s.length > 0 ? s[0].toUpperCase() + s.slice(1) : s
        })
        .join('-')
    }
    const categories =
      breadcrumbs &&
      breadcrumbs
        .filter(({ category }) => category) // Only care about categories
        .map((crumb) => formatCategory(crumb.label))
        .join(' > ')
    const firstCategory = breadcrumbs && breadcrumbs[1].label

    return {
      pageName: 'Category Display',
      granularPageName: `Category:${categories}`,
      prop23: `Layout:${state.grid.columns}:${view} (Default)`,
      prop27: sort,
      prop2: parseInt(totalProducts, 10) || products.length || 'zero',
      eVar4: `${analyticsBrandCode}:Category:${categories}`,
      prop7,
      firstCategory,
    }
  }
}
