import getCMSPageData from './cms'
import getMrCMSPageData from './mr-cms'
import getOrderCompletedPageData from './order-completed'
import getPDPPageData from './pdp'
import getPLPPageData from './plp'
import {
  getDataForStoreFinder,
  getDataForCollectFromStore,
  getDataForFindInStore,
} from './store-locator'
import getWriteReviewData from './write-a-review'
import getProductQuickViewData from './product-quick-view'

function getOrderCompletedFullPageData(state) {
  return {
    ...getOrderCompletedPageData(state),
    isCheckout: true,
  }
}

export default {
  'my-account': {
    getData: () => ({
      pageName: 'My Account',
      granularPageName: 'My Account',
      firstCategory: 'Account',
    }),
  },
  'write-a-review': {
    getData: getWriteReviewData,
  },
  'change-password': {
    getData: () => ({
      pageName: 'My Account > My Password',
      granularPageName: 'My Account > My Password',
      firstCategory: 'Account',
    }),
  },
  'my-checkout-details': {
    getData: () => ({
      pageName: 'My Account > My Checkout Details',
      granularPageName: 'My Account > My Checkout Details',
      firstCategory: 'Account',
    }),
  },
  'my-details': {
    getData: () => ({
      pageName: 'My Account > My Details',
      granularPageName: 'My Account > My Details',
      firstCategory: 'Account',
    }),
  },
  'my-orders': {
    getData: () => ({
      pageName: 'My Account > My Orders',
      granularPageName: 'My Account > My Orders',
      firstCategory: 'Account',
    }),
  },
  'order-details': {
    getData: () => ({
      pageName: 'My Account > My Order Details',
      granularPageName: 'My Account > My Order Details',
      firstCategory: 'Account',
    }),
  },
  'change-shipping-destination': {
    getData: () => ({
      pageName: 'Change shipping',
      granularPageName: 'Change shipping',
      firstCategory: 'Change shipping',
    }),
  },
  'delivery-details': {
    getData: () => ({
      pageName: 'Delivery Details',
      granularPageName: 'Delivery Details',
      firstCategory: 'Checkout',
      isCheckout: true,
    }),
  },
  'billing-details': {
    getData: () => ({
      pageName: 'Billing Details',
      granularPageName: 'Billing Details',
      firstCategory: 'Checkout',
      isCheckout: true,
    }),
  },
  'delivery-payment': {
    getData: () => ({
      pageName: 'Delivery and Payment',
      granularPageName: 'Delivery and Payment',
      firstCategory: 'Checkout',
      isCheckout: true,
    }),
  },
  'order-submit-page': {
    getData: () => ({
      pageName: 'Order Submit Form',
      granularPageName: 'Order Submit Form',
      firstCategory: 'Checkout',
      isCheckout: true,
    }),
  },
  'checkout-login': {
    getData: () => ({
      pageName: 'Checkout/Logon',
      granularPageName: 'Checkout/Logon',
      firstCategory: 'Checkout',
      isCheckout: true,
    }),
  },
  'register-login': {
    getData: () => ({
      pageName: 'Register/Logon',
      granularPageName: 'Register/Logon',
      firstCategory: 'Account',
    }),
  },
  'style-adviser': {
    getData: () => ({
      pageName: 'Style Adviser',
      pageType: 'Content',
    }),
  },
  home: {
    getData: () => ({
      pageName: 'Home Page',
      pageType: 'Home Page',
      granularPageName: 'Home Page',
      firstCategory: 'Home Page',
    }),
  },
  plp: {
    getData: getPLPPageData,
  },
  pdp: {
    getData: getPDPPageData,
  },
  'cms-pages': {
    getData: getCMSPageData,
  },
  'mrCms-pages': {
    getData: getMrCMSPageData,
  },
  'store-locator': {
    getData: getDataForStoreFinder,
  },
  'collect-from-store': {
    getData: getDataForCollectFromStore,
  },
  'find-in-store': {
    getData: getDataForFindInStore,
  },
  'order-completed': {
    getData: getOrderCompletedFullPageData,
  },
  'product-quick-view': {
    getData: getProductQuickViewData,
  },
}
