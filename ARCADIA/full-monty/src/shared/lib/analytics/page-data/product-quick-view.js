import { getProductAnalyticsData } from './helpers'

const productHasReviews = (product) =>
  product.attributes != null &&
  product.attributes.NumReviews != null &&
  parseInt(product.attributes.NumReviews, 10) > 0

export default (state) => {
  const { quickview } = state
  const { product } = quickview
  const { lineNumber, name } = product

  const granularPageName = `Prod QView:(${lineNumber})${name}`
  const pageName = 'Product Quickview'

  return {
    pageName,
    firstCategory: pageName,
    granularPageName,
    eVar13: granularPageName,
    events: [
      'prodView',
      'event2',
      'event60',
      productHasReviews(product) ? 'event108' : undefined,
    ]
      .filter((x) => x != null)
      .join(','),
    products: getProductAnalyticsData(product).string,
  }
}
