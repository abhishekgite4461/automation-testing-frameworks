import { isEmpty } from 'ramda'

import { stripCommas } from '../analytics'

export default (state) => {
  const {
    checkout: { partialOrderSummary, orderCompleted },
    forms: {
      checkout: {
        account: {
          fields: { passwordConfirm: passwordConfirmFormField },
        },
      },
    },
    account: {
      user: { email, userTrackingId },
    },
  } = state

  if (isEmpty(partialOrderSummary) || isEmpty(orderCompleted)) {
    return
  }

  const { deliveryStoreCode, basket } = partialOrderSummary
  const {
    currencyConversion,
    deliveryAddress: { country },
    orderId,
    orderLines,
    deliveryPrice,
    totalOrdersDiscountLabel,
    paymentDetails,
    deliveryMethod,
    subTotal,
    totalOrderPrice,
  } = orderCompleted

  const events = [
    'event3',
    'event7',
    'event58',
    'event66',
    'event69',
    'purchase',
    `event71=${
      currencyConversion && currencyConversion.currencyCode
        ? currencyConversion.currencyCode
        : '1.00'
    }`,
    'event11',
  ]

  if (passwordConfirmFormField.isTouched) {
    events.push('event9')
  }

  const joinedProducts = orderLines.reduce((currentProductsString, product) => {
    const {
      name,
      colour,
      total,
      size,
      quantity,
      discount,
      unitPrice,
      lineNo,
    } = product
    const basketProduct =
      basket &&
      basket.products &&
      basket.products.find(({ lineNumber }) => lineNumber === lineNo)
    let wasPrice = basketProduct.wasWasPrice || basketProduct.wasPrice
    wasPrice = isEmpty(wasPrice) ? undefined : parseInt(wasPrice, 10)
    const nonDiscountedTotalPrice = parseFloat(
      parseFloat(unitPrice) * quantity
    ).toFixed(2)
    let productEvents = [
      `event58=${
        wasPrice
          ? parseFloat(wasPrice * quantity).toFixed(2)
          : parseFloat(unitPrice * quantity).toFixed(2)
      }`,
      `event66=${nonDiscountedTotalPrice}`,
    ]

    const productEvars = [`eVar35=${size}`, `eVar36=${colour}`].join('|')

    if (!isEmpty(discount)) {
      const discountPrice = parseFloat(
        (parseFloat(unitPrice) * quantity).toFixed(2) - parseFloat(total)
      ).toFixed(2)
      productEvents.push(`event5=${discountPrice}`)
      productEvents.push(`event67=${discountPrice}`)
      if (!events.includes('event5'))
        ['event5', 'event67'].map((event) => events.push(event))
    }
    if (basket && basket.discounts && basket.discounts[0]) {
      const productPriceRatio = parseFloat(total) / parseFloat(subTotal)
      const orderPriceAfterDeliveryPrice =
        parseFloat(subTotal) + parseFloat(deliveryPrice)
      const overallDiscountForBag =
        orderPriceAfterDeliveryPrice - parseFloat(totalOrderPrice)
      const orderDiscountForItem = parseFloat(
        productPriceRatio * overallDiscountForBag
      ).toFixed(2)
      productEvents.push(`event6=${orderDiscountForItem}`)
      productEvents.push(`event68=${orderDiscountForItem}`)
      if (!events.includes('event6'))
        ['event6', 'event68'].map((event) => events.push(event))
    }
    productEvents = productEvents.join('|')

    const safeName = stripCommas(name)
    return safeName.toLowerCase().includes('delivery')
      ? currentProductsString
      : currentProductsString.concat(
          `;(${lineNo})${safeName};${quantity};${nonDiscountedTotalPrice};${productEvents};${productEvars},`
        )
  }, '')

  const deliveryProductName = orderLines.find(({ name }) =>
    name.toLowerCase().includes('delivery')
  )
  const deliveryProductString = `;${
    deliveryProductName ? stripCommas(deliveryProductName) : deliveryMethod
  };;;event3=${deliveryPrice}|event69=${deliveryPrice};`

  const products = joinedProducts.concat(deliveryProductString)
  const paymentMethods = paymentDetails.reduce(
    (currentPayments, { paymentMethod }) => {
      if (
        !currentPayments.length &&
        paymentMethod === 'Giftcard' &&
        paymentDetails.length === 1
      )
        return 'Giftcard'
      else if (currentPayments.length > 0 && paymentMethod === 'Giftcard') {
        return currentPayments.concat(' / Giftcard')
      }
      return currentPayments.concat(paymentMethod)
    },
    ''
  )

  const data = {
    pageName: 'Order Confirmed',
    granularPageName: 'Order Confirmed',
    firstCategory: 'Checkout',
    currencyCode: currencyConversion ? currencyConversion.currencyRate : 'GBP',
    eVar7: email.toLowerCase(),
    eVar6: userTrackingId,
    eVar9: country,
    eVar12: totalOrdersDiscountLabel || orderLines[0].discount,
    eVar16: paymentMethods,
    eVar17: deliveryMethod,
    eVar40: orderId,
    eVar71: parseFloat(
      parseFloat(totalOrderPrice) - parseFloat(deliveryPrice)
    ).toFixed(2),
    products,
    events: events.join(','),
    isCheckout: true,
  }
  if (deliveryStoreCode && deliveryStoreCode.includes('TS0001'))
    data.eVar34 = `${deliveryStoreCode}Oxford Circus`
  else if (deliveryStoreCode) data.eVar34 = deliveryStoreCode

  return data
}
