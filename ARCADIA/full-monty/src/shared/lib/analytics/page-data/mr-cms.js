import { path, omit } from 'ramda'

const getFixedCmsPageName = (pageName) => {
  // CMS currently returns e.g. "mob - Page Title"
  return typeof pageName === 'string' ? pageName.replace(/^mob - /, '') : ''
}

const getPageName = (pages, pathname) => {
  // for size guide on PDP - key in pages is dynamic
  const sizeGuidePageName = /\/size-guide/.test(pathname)
    ? Object.keys(omit(['navSocial'], pages))
    : ''
  const pageData =
    pages[pathname] ||
    pages.termsAndConditions ||
    pages.notFound ||
    pages[sizeGuidePageName]
  const pageName = path(['props', 'data', 'pageName'], pageData)
  return getFixedCmsPageName(pageName)
}

export default (state) => {
  const {
    sandbox: { pages },
    routing: {
      location: { pathname },
    },
  } = state
  const pageName =
    getPageName(pages, pathname) ||
    getPageName(pages, window.encodeURIComponent(pathname))
  if (!pageName) return

  return {
    pageName,
    pageType: 'Content',
    granularPageName: pageName,
    firstCategory: pageName,
  }
}
