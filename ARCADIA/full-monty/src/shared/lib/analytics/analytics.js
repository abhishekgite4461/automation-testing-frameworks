import { compose, fromPairs, map, split, drop, path } from 'ramda'
import { isLoggedIn } from '../get-value-from-store'
import { padWithZerosToTwo } from '../generic-utils'
import {
  setAnalyticsObject,
  createAnalyticsObject,
  getHasLoaded,
} from './getAnalyticsObject'
import {
  setItem as setCookie,
  getItem as getCookie,
} from '../../../client/lib/cookie'
import {
  addPageSpecificAnalyticsKeys,
  getBasicPageData,
  addInternalNav,
} from './analytics-utils'
import * as Logger from '../../../client/lib/logger'
import { getDRETValue } from '../cookie'

// Adobe analytics functions

export const getAnalyticsBrandCode = (brand) => {
  const brandProp = brand.toUpperCase()
  return brandProp === 'BR' ? 'BM' : brandProp
}

export function getLastPage(visited) {
  return visited.length > 1 ? visited[visited.length - 2] : ''
}

export function getDate() {
  const now = new Date()

  const isDaylightSavingTime =
    window.__INITIAL_STATE__.config.isDaylightSavingTime
  if (isDaylightSavingTime) now.setUTCHours(now.getUTCHours() + 1)

  const date = padWithZerosToTwo(now.getUTCDate())
  const month = padWithZerosToTwo(now.getUTCMonth() + 1)
  const year = now.getUTCFullYear()
  const hours = padWithZerosToTwo(now.getUTCHours())
  const minutes = padWithZerosToTwo(now.getUTCMinutes())
  const dateMonthYear = `${date}-${month}-${year}`
  const day = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'][now.getDay()]
  const time = `${hours}:${minutes}`

  return `${time} | ${day} | ${dateMonthYear}`
}

// Temporary Adobe events passed to logging
function logAdobeEvent(data) {
  const adobeLoggedData = Object.assign({}, data)
  if (
    adobeLoggedData.channel &&
    adobeLoggedData.channel.toUpperCase().indexOf('CHECKOUT') !== -1
  ) {
    try {
      Logger.info('adobe:event:checkout', { data: adobeLoggedData })
    } catch (event) {
      Logger.info('adobe:event:error', { data: 'Logging failed' })
      throw new Error('Adobe logging failure')
    }
  }
}

// sending adobe analytics
function sendAnalytics(data, s, isTrackLink) {
  if (!process.browser) return

  const {
    config: { analyticsId, stagingReportSuiteId, prodReportSuiteId },
    adobe: { environment },
  } = window.__INITIAL_STATE__

  data.account =
    environment === 'production' ? prodReportSuiteId : stagingReportSuiteId
  data.prop66 = analyticsId
  data.eVar66 = 'D=c66'

  Object.assign(s, data)
  logAdobeEvent(data)

  if (isTrackLink) {
    // if linkTrackVars is null (or an empty string), all Analytics variables that are defined for the current page are tracked
    const linkTrackVars = `pageName,${Object.keys(data).join(',')}`
    s.linkTrackVars = linkTrackVars
    // @NOTE Commented out for GTM-156
    // s.tl(true, 'o', 'event')
  } else {
    // @NOTE Commented out for GTM-156
    // s.t()
  }
}

// containing arcadia specyfic global adobe analytics, should be called on every page
export function pageBasedAnalytics(data, isTrackLink) {
  if (!process.browser) return

  if (getHasLoaded() === false) {
    setAnalyticsObject(pageBasedAnalytics.bind(this), data, isTrackLink)
    return
  }
  createAnalyticsObject()

  const {
    config: { currencyCode, adobeTrackingServer, brandCode },
  } = window.__INITIAL_STATE__

  const localS = addPageSpecificAnalyticsKeys(data)

  const getQueries = compose(fromPairs, map(split('=')), split('&'), drop(1))
  const queries = getQueries(
    window.location.search.replace(/&amp(;|%3B)/g, '&')
  )
  const marketingCampaignId = queries.cmpid
  const internalCampaignId = queries.intcmpid
  const analyticsBrandCode = getAnalyticsBrandCode(brandCode)
  const dualRunGroup = getCookie('dual-run') || 'none'
  const { pageName, granularPageName, channel } = getBasicPageData(
    data,
    analyticsBrandCode
  )
  const sourceValue = getCookie('source')
  const dretValue = getDRETValue({ cookies: window.document.cookie })

  if (marketingCampaignId) localS.campaign = marketingCampaignId
  if (process.env.NODE_ENV === 'production') {
    localS.trackingServerSecure = adobeTrackingServer
    localS.trackingServer = adobeTrackingServer
  }
  localS.currencyCode = currencyCode

  localS.prop3 = granularPageName
  localS.eVar13 = 'D=c3'

  localS.prop4 = isLoggedIn() ? 'logged-in' : 'not logged-in'
  localS.eVar14 = 'D=c4'

  localS.prop8 = getDate()
  localS.eVar19 = 'D=c8'
  localS.prop16 = data.loadTime
  localS.prop18 = getCookie('previousGranluarPageName')
  localS.prop19 = path(['percentagePageViewed', 'value'], data)
  if (data.percentagePageViewed && data.percentagePageViewed.loadedProducts) {
    localS.prop45 = path(['percentagePageViewed', 'loadedProducts'], data)
  }

  localS.prop50 = window.location.href
  localS.prop51 = window.location.search.substr(1)
  localS.prop59 = window.s.c_r('s_vi')
  localS.eVar59 = 'D=c59'

  localS.eVar64 = dretValue
  localS.prop63 = dretValue

  localS.eVar79 = `Mobile ${
    typeof window.version.version === 'string'
      ? window.version.version.split(' ')[0]
      : ''
  }`

  localS.eVar85 = data.checkout
  localS.prop62 = data.checkout

  localS.channel = channel
  localS.pageName = pageName
  localS.eVar95 = getCookie('traceId2')

  localS.eVar96 = sourceValue
  localS.prop61 = sourceValue

  localS.eVar97 = data.buildVersion
  localS.prop60 = data.buildVersion

  localS.referrer = data.lastPage
    ? window.location.origin + data.lastPage
    : document.referrer

  // Added for dual dualRun
  localS.eVar54 = dualRunGroup
  localS.prop55 = dualRunGroup

  Object.assign(localS, addInternalNav(data.intNav, internalCampaignId))

  setCookie('previousGranluarPageName', localS.prop3)

  sendAnalytics(localS, window.s, isTrackLink)
}

export function genericEventBasedCallback(eventNo) {
  return () => ({
    events: eventNo,
  })
}

// sending event specyfic adobe analytics, side wide tracking
export function eventBasedAnalytics(data) {
  if (!process.browser) return

  if (!window.s) {
    if (getHasLoaded() === false) {
      setAnalyticsObject(eventBasedAnalytics.bind(this), data)
      return
    }
    createAnalyticsObject()
  }
  // We do this in order to give our e2e tests the ability to read this value.
  if (window.s)
    window.s.Util.cookieWrite('mostRecentAnalyticsEventData', data.events)

  sendAnalytics(data, window.s, true)
}

export function analyticsTl(data) {
  if (!process.browser) return

  if (!window.s) {
    if (getHasLoaded() === false) {
      setAnalyticsObject(analyticsTl.bind(this), data)
      return
    }
    createAnalyticsObject()
  }
  sendAnalytics(data, window.s, true)
}

// Adobe analytics helpers
const strip = (chr, str) => {
  const re = new RegExp(`${chr}`, 'gi')
  return typeof str === 'string' ? str.replace(re, '') : str
}

// Adobe Analytics considers a ',' as a separator between values. See CHAN-637.
export const stripCommas = (str) => strip(',', str)
