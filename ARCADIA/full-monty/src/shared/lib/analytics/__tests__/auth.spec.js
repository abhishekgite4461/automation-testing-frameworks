import { authEventAnalytics, logoutEventAnalytics } from '../auth'

jest.mock('../../../lib/analytics/qubit-analytics', () => ({
  updateUniversalVariable: jest.fn(),
}))
import { updateUniversalVariable } from '../../../lib/analytics/qubit-analytics'
import * as analytics from '../analytics'

const dispatch = jest.fn()
analytics.eventBasedAnalytics = jest.fn()

describe('Auth events handler', () => {
  beforeEach(() => {
    analytics.eventBasedAnalytics.mockReset()
    dispatch.mockReset()
    updateUniversalVariable.mockReset()
  })
  describe('authEventAnalytics', () => {
    it('should call eventBasedAnalytics with correct data when unsuccessful login', () => {
      const getState = jest.fn(() => ({ account: { user: {} } }))
      expect(analytics.eventBasedAnalytics).not.toHaveBeenCalled()
      authEventAnalytics({ username: 'name@exmaple.com' }, ['event57'])(
        dispatch,
        getState
      )
      expect(analytics.eventBasedAnalytics).toHaveBeenCalledTimes(1)
      expect(analytics.eventBasedAnalytics).toHaveBeenCalledWith({
        events: 'event57',
      })
    })
    it('should call eventBasedAnalytics with correct data when successful login (and no userTrackingId and subscriptionId)', () => {
      const getState = jest.fn(() => ({ account: { user: {} } }))
      expect(analytics.eventBasedAnalytics).not.toHaveBeenCalled()
      authEventAnalytics({ username: 'name@exmaple.com' }, ['event11'])(
        dispatch,
        getState
      )
      expect(analytics.eventBasedAnalytics).toHaveBeenCalledTimes(1)
      expect(analytics.eventBasedAnalytics).toHaveBeenCalledWith({
        events: 'event11',
        eVar7: 'name@exmaple.com',
      })
    })
    it('should call eventBasedAnalytics with correct data when events not event11 or event9', () => {
      const getState = jest.fn(() => ({ account: { user: {} } }))
      expect(analytics.eventBasedAnalytics).not.toHaveBeenCalled()
      authEventAnalytics({ username: 'name@exmaple.com' }, ['event111'])(
        dispatch,
        getState
      )
      expect(analytics.eventBasedAnalytics).toHaveBeenCalledTimes(1)
      expect(analytics.eventBasedAnalytics).toHaveBeenCalledWith({
        events: 'event111',
      })
    })
    it('should call eventBasedAnalytics with correct data when successful login', () => {
      const userTrackingId = 1111111
      const subscriptionId = 2222222
      const getState = jest.fn(() => ({
        account: {
          user: {
            userTrackingId,
            subscriptionId,
          },
        },
      }))
      expect(analytics.eventBasedAnalytics).not.toHaveBeenCalled()
      authEventAnalytics({ username: 'name@exmaple.com' }, ['event11'])(
        dispatch,
        getState
      )
      expect(analytics.eventBasedAnalytics).toHaveBeenCalledTimes(1)
      expect(analytics.eventBasedAnalytics).toHaveBeenCalledWith({
        events: 'event11',
        eVar7: 'name@exmaple.com',
        eVar6: userTrackingId,
        eVar5: subscriptionId,
      })
    })
    it('should call eventBasedAnalytics with correct data when successful registration', () => {
      const userTrackingId = 1111111
      const subscriptionId = 2222222
      const getState = jest.fn(() => ({
        account: {
          user: {
            userTrackingId,
            subscriptionId,
          },
        },
      }))
      expect(analytics.eventBasedAnalytics).not.toHaveBeenCalled()
      authEventAnalytics({ username: 'name@exmaple.com' }, ['event9'])(
        dispatch,
        getState
      )
      expect(analytics.eventBasedAnalytics).toHaveBeenCalledTimes(1)
      expect(analytics.eventBasedAnalytics).toHaveBeenCalledWith({
        events: 'event9',
        eVar7: 'name@exmaple.com',
        eVar6: userTrackingId,
        eVar5: subscriptionId,
      })
    })
    it('should call eventBasedAnalytics with correct data when successful registration (many events)', () => {
      const userTrackingId = 1111111
      const subscriptionId = 2222222
      const getState = jest.fn(() => ({
        account: {
          user: {
            userTrackingId,
            subscriptionId,
          },
        },
      }))
      expect(analytics.eventBasedAnalytics).not.toHaveBeenCalled()
      authEventAnalytics({ username: 'name@exmaple.com' }, [
        'event9',
        'event4',
      ])(dispatch, getState)
      expect(analytics.eventBasedAnalytics).toHaveBeenCalledTimes(1)
      expect(analytics.eventBasedAnalytics).toHaveBeenCalledWith({
        events: 'event9,event4',
        eVar7: 'name@exmaple.com',
        eVar6: userTrackingId,
        eVar5: subscriptionId,
      })
    })
    it('should fire updateUniversalVariable', () => {
      const getState = jest.fn(() => ({ account: { user: {} } }))
      authEventAnalytics({ username: 'name@exmaple.com' }, [
        'event9',
        'event4',
      ])(dispatch, getState)
      expect(dispatch).toHaveBeenCalledTimes(1)
      expect(updateUniversalVariable).toHaveBeenCalledTimes(1)
    })
  })
  describe('logoutEventAnalytics', () => {
    it('should call eventBasedAnalytics with correct data', () => {
      expect(analytics.eventBasedAnalytics).not.toHaveBeenCalled()
      logoutEventAnalytics()(dispatch)
      expect(analytics.eventBasedAnalytics).toHaveBeenCalledTimes(1)
      expect(analytics.eventBasedAnalytics).toHaveBeenCalledWith({
        events: 'event114',
      })
    })
    it('should fire updateUniversalVariable', () => {
      const getState = jest.fn(() => ({ account: { user: {} } }))
      authEventAnalytics({ username: 'name@exmaple.com' }, [
        'event9',
        'event4',
      ])(dispatch, getState)
      expect(dispatch).toHaveBeenCalledTimes(1)
      expect(updateUniversalVariable).toHaveBeenCalledTimes(1)
    })
  })
})
