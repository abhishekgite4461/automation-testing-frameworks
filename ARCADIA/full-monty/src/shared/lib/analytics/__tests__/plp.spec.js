import { setProducts, layoutChange, updateUVProductsList } from '../plp'
import { clone } from 'ramda'
import {
  config,
  products,
  sorting,
  grid,
  refinements,
  productViews,
  routing,
} from './plpData'
import * as analyticsFunction from '../analytics'
import * as qubitAnalyticsFunctions from '../qubit-analytics'

describe('setProductAnalytics', () => {
  let state
  let dispatch
  let getState

  const stateObject = {
    config,
    products,
    sorting,
    refinements,
    productViews,
    grid,
    routing,
  }

  beforeEach(() => {
    state = clone(stateObject)
    dispatch = jest.fn()
    getState = jest.fn(() => state)
  })

  afterEach(() => {
    state = null
    jest.resetAllMocks()
  })

  describe('Qubit analytics', () => {
    let updateUniversalVariableSpy
    beforeEach(() => {
      updateUniversalVariableSpy = jest.spyOn(
        qubitAnalyticsFunctions,
        'updateUniversalVariable'
      )
    })
    afterEach(() => {
      updateUniversalVariableSpy.mockReset()
      updateUniversalVariableSpy.mockRestore()
      jest.resetAllMocks()
    })
    it('should call updateUniversalVariable', () => {
      const setProductsResult = setProducts()

      expect(updateUniversalVariableSpy).not.toHaveBeenCalled()
      setProductsResult(dispatch, getState)
      expect(updateUniversalVariableSpy).toHaveBeenCalledTimes(1)
    })
  })
})

describe('layoutChange', () => {
  let updateUniversalVariableSpy
  beforeEach(() => {
    updateUniversalVariableSpy = jest.spyOn(
      qubitAnalyticsFunctions,
      'updateUniversalVariable'
    )
    analyticsFunction.analyticsTl = jest.fn()
  })
  afterEach(() => {
    updateUniversalVariableSpy.mockReset()
    updateUniversalVariableSpy.mockRestore()
    jest.resetAllMocks()
  })
  it('should call analyticsTl with product view if productIsActive', () => {
    const dispatch = layoutChange()
    const dispatchFunction = jest.fn()
    const getState = jest.fn(() => ({
      productViews: {
        selectedViewType: 'Product',
      },
      grid: {
        columns: 2,
      },
    }))
    expect(updateUniversalVariableSpy).not.toHaveBeenCalled()
    expect(analyticsFunction.analyticsTl).not.toHaveBeenCalled()

    dispatch(dispatchFunction, getState)

    expect(dispatchFunction).toHaveBeenCalledTimes(1)
    expect(updateUniversalVariableSpy).toHaveBeenCalledTimes(1)
    expect(analyticsFunction.analyticsTl).toHaveBeenCalledTimes(1)
    expect(analyticsFunction.analyticsTl).toHaveBeenCalledWith({
      prop23: 'Layout:2:Product view',
    })
  })
  it('should call analyticsTl with outfit view if not productIsActive', () => {
    const dispatch = layoutChange()
    const dispatchFunction = jest.fn()
    const getState = jest.fn(() => ({
      productViews: {
        selectedViewType: 'Outfit',
      },
      grid: {
        columns: 2,
      },
    }))
    expect(updateUniversalVariableSpy).not.toHaveBeenCalled()
    expect(analyticsFunction.analyticsTl).not.toHaveBeenCalled()

    dispatch(dispatchFunction, getState)

    expect(dispatchFunction).toHaveBeenCalledTimes(1)
    expect(updateUniversalVariableSpy).toHaveBeenCalledTimes(1)
    expect(analyticsFunction.analyticsTl).toHaveBeenCalledTimes(1)
    expect(analyticsFunction.analyticsTl).toHaveBeenCalledWith({
      prop23: 'Layout:2:Outfit view',
    })
  })
})

describe('updateUVProductsList and mapProductObject', () => {
  const state = {
    products: {
      categoryTitle: 'category-title',
      totalProducts: 4,
    },
    grid: { columns: 2 },
    sorting: { currentSortOption: 'sort-option' },
    refinements: { appliedOptions: 'some-refinements' },
    config: { currencyCode: 'PLN' },
  }
  it('should return correct values', () => {
    const result = updateUVProductsList(state)
    expect(result.items).toEqual([])
    expect(result.query).toBe('category-title')
    expect(result.result_count).toBe(4)
    expect(result.sort_by).toBe('sort-option')
    expect(result.filters).toBe('some-refinements')
  })
  it('should return no sort_by when no currentSortOption', () => {
    state.sorting.currentSortOption = null
    const result = updateUVProductsList(state)
    expect(result.sort_by).toBe('Relevance')
  })
  it('should return items', () => {
    state.products.products = [
      {
        productId: 123,
        seoUrl: 'some-url',
        name: 'some-name',
        unitPrice: 1.34,
        rrp: 3.5,
        lineNumber: 5,
      },
    ]
    const result = updateUVProductsList(state)
    expect(result.items[0].id).toBe('123')
    expect(result.items[0].url).toBe('some-url')
    expect(result.items[0].name).toBe('some-name')
    expect(result.items[0].image_url).toBeUndefined()
    expect(result.items[0].sku_code).toBe(5)
    expect(result.items[0].unit_sale_price).toBe(1.34)
    expect(result.items[0].unit_price).toBe(3.5)
    expect(result.items[0].currency).toBe('PLN')
  })
  it('should return items', () => {
    state.products.products = [
      {
        seoUrl: 'some-url',
        name: 'some-name',
        lineNumber: 5,
      },
    ]
    const result = updateUVProductsList(state)
    expect(result.items[0].id).toBe('')
    expect(result.items[0].url).toBe('some-url')
    expect(result.items[0].name).toBe('some-name')
    expect(result.items[0].image_url).toBeUndefined()
    expect(result.items[0].sku_code).toBe(5)
    expect(result.items[0].unit_sale_price).toBe(0)
    expect(result.items[0].unit_price).toBe(0)
    expect(result.items[0].currency).toBe('PLN')
  })
  it('should return items with image_url', () => {
    state.products.products = [
      {
        seoUrl: 'some-url',
        name: 'some-name',
        lineNumber: 5,
        assets: [
          {
            assetType: 'SOME-TYPE',
            url: 'first-asset-url',
          },
          {
            assetType: 'IMAGE_LARGE',
            url: 'second-asset-url',
          },
        ],
      },
    ]
    const result = updateUVProductsList(state)
    expect(result.items[0].image_url).toBe('second-asset-url')
  })
})
