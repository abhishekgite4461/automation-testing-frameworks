import {
  pageBasedAnalytics,
  eventBasedAnalytics,
  genericEventBasedCallback,
  getLastPage,
  getAnalyticsBrandCode,
  getDate,
  analyticsTl,
} from '../analytics'

import { info } from '../../../../client/lib/logger'

jest.mock('../../../../client/lib/logger')

jest.mock('../../get-value-from-store', () => ({
  isLoggedIn: jest.fn(() => true),
}))
jest.mock('../getAnalyticsObject', () => ({
  getHasLoaded: jest.fn(() => true),
  createAnalyticsObject: jest.fn(() => {
    global.s = {
      c_r: jest.fn(() => 'sample visitor id'),
      t: jest.fn(),
      tl: jest.fn(),
      Util: {
        cookieWrite: jest.fn(),
      },
      w: global.window,
    }
  }),
  setAnalyticsObject: jest.fn(),
}))
jest.mock('../../../../client/lib/cookie', () => ({
  getItem: jest.fn((key) => {
    if (key === 'previousGranluarPageName') return 'sample prev page name'
    if (key === 'traceId2') return 'AAAAAAA'
    if (key === 'source') return 'CoreAPI'
  }),
  setItem: jest.fn(),
}))
import { setItem } from '../../../../client/lib/cookie'

jest.mock('../analytics-utils')
import {
  getBasicPageData,
  addPageSpecificAnalyticsKeys,
  addInternalNav,
} from '../analytics-utils'

jest.mock('../../cookie', () => ({
  getDRETValue: jest.fn(),
}))
import { getDRETValue } from '../../cookie'

describe('analytics', () => {
  const nowMock = jest.spyOn(window, 'Date')

  beforeEach(() => {
    nowMock.mockImplementation(() => {
      return {
        getUTCHours: jest.fn(() => 13),
        getUTCDate: jest.fn(() => 17),
        getUTCMonth: jest.fn(() => 9),
        getUTCFullYear: jest.fn(() => 2017),
        getUTCMinutes: jest.fn(() => 4),
        getDay: jest.fn(() => 2),
      }
    })
    jest.useFakeTimers()
    getBasicPageData.mockImplementation(() => {
      return {
        pageName: 'TS:some page name',
        granularPageName: 'TS:some granular page name',
        channel: 'TS:some category',
      }
    })
    addPageSpecificAnalyticsKeys.mockImplementation(() => {
      return {}
    })
    global.__INITIAL_STATE__ = {
      config: {
        analyticsId: 'analytics id',
        stagingReportSuiteId: 'stagingId',
        prodReportSuiteId: 'prodId',
        currencyCode: 'PLN',
        adobeTrackingServer: 'server',
        brandCode: 'TS',
      },
      adobe: {
        environment: 'production',
      },
    }
    global.version = {
      version: 'sampleVersion',
    }
    global.process.browser = true
  })

  afterEach(() => {
    nowMock.mockClear()
    jest.clearAllTimers()
    jest.resetAllMocks()
  })

  describe('pageBasedAnalytics', () => {
    beforeEach(() => {
      addPageSpecificAnalyticsKeys.mockImplementation(() => {
        return {
          eVar100: 'some value',
        }
      })
    })

    it('should send analytics with proper data', () => {
      const loadTimeValue = 30
      const data = {
        loadTime: loadTimeValue,
        percentagePageViewed: {
          value: 86,
          loadedProducts: undefined,
        },
        buildVersion: 'buildVersion',
        pageName: 'some page name',
        granularPageName: 'some granular page name',
        firstCategory: 'some category',
        eVar100: 'some value',
      }

      pageBasedAnalytics(data, true)
      expect(setItem).toHaveBeenCalledTimes(1)
      expect(setItem).toHaveBeenCalledWith(
        'previousGranluarPageName',
        'TS:some granular page name'
      )

      expect(global.s.channel).toBe('TS:some category')
      expect(global.s.currencyCode).toBe('PLN')
      expect(global.s.eVar100).toBe('some value')
      expect(global.s.eVar13).toBe('D=c3')
      expect(global.s.eVar14).toBe('D=c4')
      expect(global.s.eVar19).toBe('D=c8')
      expect(global.s.eVar59).toBe('D=c59')
      expect(global.s.eVar79).toBe('Mobile sampleVersion')
      expect(global.s.pageName).toBe('TS:some page name')
      expect(global.s.prop16).toBe(loadTimeValue)
      expect(global.s.prop18).toBe('sample prev page name')
      expect(global.s.prop19).toBe(86)
      expect(global.s.prop3).toBe('TS:some granular page name')
      expect(global.s.prop4).toBe('logged-in')
      expect(global.s.prop50).toBe('about:blank')
      expect(global.s.prop51).toBe('')
      expect(global.s.eVar95).toBe('AAAAAAA')
      expect(global.s.eVar96).toBe('CoreAPI')
      expect(global.s.prop61).toBe('CoreAPI')
      expect(global.s.eVar97).toBe('buildVersion')
      expect(global.s.prop60).toBe('buildVersion')
      expect(global.s.prop59).toBe('sample visitor id')
      expect(global.s.prop8).toBe('13:04 | Tue | 17-10-2017')
      expect(global.s.eVar64).toBe(undefined)
      expect(global.s.prop63).toBe(undefined)
    })
    it('should call logger for Checkout channel', () => {
      addPageSpecificAnalyticsKeys.mockImplementationOnce((data) => data)
      getBasicPageData.mockImplementationOnce(() => {
        return {
          pageName: 'TS:some page name',
          granularPageName: 'TS:some granular page name',
          channel: 'TS:Checkout',
        }
      })
      pageBasedAnalytics(
        {
          foo: 'bar',
        },
        true
      )
      expect(info).toHaveBeenCalledTimes(1)
      expect(info.mock.calls[0][1].data.foo).toBe('bar')
    })
    it('should not call logger for any other channel', () => {
      addPageSpecificAnalyticsKeys.mockImplementationOnce((data) => data)
      getBasicPageData.mockImplementationOnce(() => {
        return {
          pageName: 'TS:some page name',
          granularPageName: 'TS:some granular page name',
          channel: 'TS:Other',
        }
      })
      pageBasedAnalytics(
        {
          foo: 'bar',
        },
        true
      )
      expect(info).toHaveBeenCalledTimes(0)
    })
    it('should send analytics with data specific to production', () => {
      global.process.env.NODE_ENV = 'production'
      const loadTimeValue = 30
      const data = {
        loadTime: loadTimeValue,
        percentagePageViewed: {
          value: 86,
          loadedProducts: undefined,
        },
        pageName: 'some page name',
        granularPageName: 'some granular page name',
        firstCategory: 'some category',
      }

      pageBasedAnalytics(data, true)
      expect(global.s.trackingServer).toBe('server')
      expect(global.s.trackingServerSecure).toBe('server')
    })
    it('should call analytics with data specific to percentagePageViewed for PLP', () => {
      const loadTimeValue = 30
      const data = {
        loadTime: loadTimeValue,
        percentagePageViewed: {
          value: 50,
          loadedProducts: 60,
        },
        pageName: 'some page name',
        granularPageName: 'some granular page name',
        firstCategory: 'some category',
      }
      pageBasedAnalytics(data, true)
      expect(global.s.prop19).toBe(50)
      expect(global.s.prop45).toBe(60)
    })
    it('should call analytics with checkout version if defined', () => {
      const loadTimeValue = 30
      const data = {
        loadTime: loadTimeValue,
        percentagePageViewed: {
          value: 50,
          loadedProducts: 60,
        },
        checkout: 'V1',
        pageName: 'some page name',
        granularPageName: 'some granular page name',
        firstCategory: 'some category',
      }
      pageBasedAnalytics(data, true)
      expect(global.s.eVar85).toBe('V1')
      expect(global.s.prop62).toBe('V1')
    })
    it('should call analytics with internal nav if defined', () => {
      addInternalNav.mockImplementation(() => {
        return {
          eVar3: 'Global Nav',
          eVar58: 'Global Nav:Clothing:Dresses',
        }
      })
      const loadTimeValue = 30
      const data = {
        loadTime: loadTimeValue,
        percentagePageViewed: {
          value: 50,
          loadedProducts: 60,
        },
        intNav: {
          type: 'Global Nav',
          details: 'Global Nav:Clothing:Dresses',
        },
        pageName: 'some page name',
        granularPageName: 'some granular page name',
        firstCategory: 'some category',
      }
      pageBasedAnalytics(data, true)
      expect(global.s.eVar3).toBe('Global Nav')
      expect(global.s.eVar58).toBe('Global Nav:Clothing:Dresses')
    })
    it("should push eVar64 and prop63 'OLD' value for DRET test", () => {
      getDRETValue.mockReturnValue('OLD')
      pageBasedAnalytics({}, true)
      expect(global.s.eVar64).toBe('OLD')
      expect(global.s.prop63).toBe('OLD')
    })
    it("should push eVar64 and prop63 'NEW' value for DRET test", () => {
      getDRETValue.mockReturnValue('NEW')
      pageBasedAnalytics({}, true)
      expect(global.s.eVar64).toBe('NEW')
      expect(global.s.prop63).toBe('NEW')
    })
  })

  describe('sendAnalytics', () => {
    const data = {
      pageName: 'sample page',
      events: 'event111',
    }
    it('should pass prodReportSuiteId if env is prod', () => {
      pageBasedAnalytics(data, true)
      expect(global.s.account).toBe('prodId')
      expect(global.s.prop66).toBe('analytics id')
      expect(global.s.eVar66).toBe('D=c66')
    })
    it('should pass stagingReportSuiteId if env is not prod', () => {
      global.window.__INITIAL_STATE__.adobe.environment = 'stage'
      pageBasedAnalytics(data, true)
      expect(global.s.account).toBe('stagingId')
    })
    it('should pass data props to sObject', () => {
      pageBasedAnalytics(data, true)
      expect(global.s.pageName).toBe('TS:some page name')
    })
    it('should no longer call tl', () => {
      const data = {
        events: 'event111',
        eVar123: 'sth',
      }
      expect(global.s.tl).not.toHaveBeenCalled()
      expect(global.s.t).not.toHaveBeenCalled()
      pageBasedAnalytics(data, true)
      expect(global.s.t).not.toHaveBeenCalled()
      expect(global.s.tl).toHaveBeenCalledTimes(0)
    })
    it('should no longer call t', () => {
      expect(global.s.tl).not.toHaveBeenCalled()
      expect(global.s.t).not.toHaveBeenCalled()
      pageBasedAnalytics(data, false)
      expect(global.s.tl).not.toHaveBeenCalled()
      expect(global.s.t).toHaveBeenCalledTimes(0)
    })
  })

  describe('eventBasedAnalytics', () => {
    it('should call sendAnalytics', () => {
      const data = { events: 'event130' }
      eventBasedAnalytics(data)
      expect(global.s.events).toBe('event130')
    })
  })

  describe('analyticsTl', () => {
    it('should call sendAnalytics', () => {
      const data = { prop199: 'someprop' }
      analyticsTl(data)
      expect(global.s.prop199).toBe('someprop')
    })
  })

  describe('genericEventBasedCallback', () => {
    it('should call with correct blah', () => {
      const eventNo = 'event111111'
      const callback = genericEventBasedCallback('event111111')

      expect(callback()).toEqual({
        events: eventNo,
      })
    })
  })

  describe('getLastPage', () => {
    it('should return empty string when visited is empty', () => {
      const visited = []
      expect(getLastPage(visited)).toBe('')
    })
    it('should return previous page if there are any', () => {
      const visited = ['first-page', 'second-page']
      expect(getLastPage(visited)).toBe('first-page')
    })
    it('should return empty string if there are only one page in history', () => {
      const visited = ['first-page']
      expect(getLastPage(visited)).toBe('')
    })
  })

  describe('getAnalyticsBrandCode', () => {
    it('should return uppercased brandCode', () => {
      const brandCode = getAnalyticsBrandCode('ts')
      expect(brandCode).toBe('TS')
    })
    it('should return BM brandCode if br was passed', () => {
      const brandCode = getAnalyticsBrandCode('br')
      expect(brandCode).toBe('BM')
    })
  })

  describe('getDate', () => {
    it('should return date in right format', () => {
      expect(getDate()).toBe('13:04 | Tue | 17-10-2017')
    })
    it('should return date in right format for isDaylightSavingTime', () => {
      global.__INITIAL_STATE__.config.isDaylightSavingTime = true
      nowMock.mockImplementation(() => {
        let hours = 13
        return {
          setUTCHours: jest.fn((h) => {
            hours = h
          }),
          getUTCHours: jest.fn(() => hours),
          getUTCDate: jest.fn(() => 17),
          getUTCMonth: jest.fn(() => 9),
          getUTCFullYear: jest.fn(() => 2017),
          getUTCMinutes: jest.fn(() => 4),
          getDay: jest.fn(() => 2),
        }
      })
      expect(getDate()).toBe('14:04 | Tue | 17-10-2017')
    })
  })
})
