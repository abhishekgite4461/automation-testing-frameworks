import { setAnalyticsObject } from '../getAnalyticsObject'

describe('getAnalyticsObject', () => {
  describe('setAnalyticsObject', () => {
    let originalGetElementById

    beforeAll(() => {
      originalGetElementById = global.document.getElementById
    })

    afterAll(() => {
      global.document.getElementById = originalGetElementById
    })

    it('calls back when global.window.AppMeasurement already exists', () => {
      global.AppMeasurement = {}
      const cb = jest.fn()

      setAnalyticsObject(cb, 'foo', 'bar')

      expect(cb.mock.calls.length).toBe(1)
      expect(cb.mock.calls[0][0]).toEqual('foo')
      expect(cb.mock.calls[0][1]).toEqual('bar')
    })

    it('loads the analytics script when global.window.AppMeasurement does not exist', () => {
      global.AppMeasurement = undefined
      const cb = jest.fn()
      const addEventListenerMock = jest.fn()
      const getElementByIdMock = jest.fn(() => ({
        addEventListener: addEventListenerMock,
      }))
      global.document.getElementById = getElementByIdMock

      setAnalyticsObject(cb, 'foo', 'bar')

      // should have attached a load event to the analytics script:
      expect(getElementByIdMock.mock.calls.length).toBe(1)
      expect(getElementByIdMock.mock.calls[0][0]).toEqual('analyticsScript')
      expect(addEventListenerMock.mock.calls.length).toBe(1)
      expect(addEventListenerMock.mock.calls[0][0]).toBe('load')
      const eventListenerLoadCb = addEventListenerMock.mock.calls[0][1]
      expect(typeof eventListenerLoadCb).toBe('function')

      // Ok, so now we want to manually fire the callback that was provided
      // to the event listener for the script, but before we do we need
      // to mock the global.window.AppMeasurement that should exist for the
      // callback to have executed.

      const sMock = {}
      global.AppMeasurement = jest.fn(() => sMock)

      // manually fire the event listener callback:
      eventListenerLoadCb()

      // our "s" instance should have been created:
      expect(global.s).toBe(sMock)

      // the fn callback should have executed:
      expect(cb.mock.calls.length).toBe(1)
      expect(cb.mock.calls[0][0]).toEqual('foo')
      expect(cb.mock.calls[0][1]).toEqual('bar')
    })
  })
})
