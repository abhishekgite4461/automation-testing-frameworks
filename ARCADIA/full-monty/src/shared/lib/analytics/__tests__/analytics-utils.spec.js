import {
  addPageSpecificAnalyticsKeys,
  getBasicPageData,
  addInternalNav,
  getInternalNav,
} from '../analytics-utils'

describe('addPageSpecificAnalyticsKeys', () => {
  it('should return object without not required keys', () => {
    const data = {
      pageName: 'pageName',
      granularPageName: 'granularPageName',
      eVar34: 'some value',
    }
    const result = addPageSpecificAnalyticsKeys(data)
    expect(result).toEqual({ eVar34: 'some value' })
  })
})

describe('getBasicPageData', () => {
  it('should return pageName as pageName with brandcode', () => {
    const pageName = 'sample page name'
    const result = getBasicPageData({ pageName }, 'TS')
    expect(result.pageName).toBe('TS:sample page name')
  })
  it('should return granularPageName as granularPageName if defined', () => {
    const granularPageName = 'sample granular page name'
    const result = getBasicPageData({ granularPageName }, 'TS')
    expect(result.granularPageName).toBe('TS:sample granular page name')
  })
  it('should return granularPageName as first and second category if granularPageName not defined', () => {
    const pageType = 'sample page type'
    const firstCategory = 'sample first category'
    const secondCategory = 'sample second category'
    const result = getBasicPageData(
      {
        pageType,
        firstCategory,
        secondCategory,
      },
      'TS'
    )
    expect(result.granularPageName).toBe(
      'TS:sample page type:sample first category > sample second category'
    )
  })
  it('should return proper granularPageName if pageType not defined', () => {
    const firstCategory = 'sample first category'
    const secondCategory = 'sample second category'
    const result = getBasicPageData(
      {
        firstCategory,
        secondCategory,
      },
      'TS'
    )
    expect(result.granularPageName).toBe(
      'TS:sample first category > sample second category'
    )
  })
  it('should return proper granularPageName if firstCategory not defined', () => {
    const pageType = 'sample page type'
    const secondCategory = 'sample second category'
    const result = getBasicPageData(
      {
        pageType,
        secondCategory,
      },
      'TS'
    )
    expect(result.granularPageName).toBe(
      'TS:sample page type > sample second category'
    )
  })
  it('should return proper granularPageName if firstCategory not defined', () => {
    const pageType = 'sample page type'
    const firstCategory = 'sample first category'
    const result = getBasicPageData(
      {
        pageType,
        firstCategory,
      },
      'TS'
    )
    expect(result.granularPageName).toBe(
      'TS:sample page type:sample first category'
    )
  })
  it('should return channel as first if defined', () => {
    const firstCategory = 'sample first category'
    const result = getBasicPageData(
      {
        firstCategory,
      },
      'TS'
    )
    expect(result.channel).toBe('TS:sample first category')
  })
  it('should return channel as empty string if first category not defined', () => {
    const result = getBasicPageData({}, 'TS')
    expect(result.channel).toBe('')
  })
})

describe('addInternalNav', () => {
  it('should return proper values for Global Nav', () => {
    const intNav = {
      type: 'Global Nav',
      details: 'Global Nav:Clothing:Dresses',
    }
    const result = addInternalNav(intNav)
    expect(result.eVar3).toBe('Global Nav')
    expect(result.prop53).toBe('D=v3')
    expect(result.prop54).toBe('D=v58')
    expect(result.eVar58).toBe('Global Nav:Clothing:Dresses')
    expect(result.eVar2).toBeUndefined()
  })
  it('should return proper values for Footer', () => {
    const intNav = {
      type: 'Footer',
      details: 'Footer:Gift Card',
    }
    const result = addInternalNav(intNav)
    expect(result.eVar3).toBe('Footer')
    expect(result.prop53).toBe('D=v3')
    expect(result.prop54).toBe('D=v58')
    expect(result.eVar58).toBe('Footer:Gift Card')
    expect(result.eVar2).toBeUndefined()
  })
  it('should return proper values for Search', () => {
    const intNav = {
      type: 'Search',
    }
    const result = addInternalNav(intNav)
    expect(result.eVar3).toBe('Search')
    expect(result.prop53).toBe('D=v3')
    expect(result.prop54).toBe('D=v58')
    expect(result.eVar58).toBe('D=v3')
    expect(result.eVar2).toBeUndefined()
  })
  it('should return proper values for Internal Campaign', () => {
    const internalCampaignId = 'Some internal campaign'
    const result = addInternalNav(null, internalCampaignId)
    expect(result.eVar3).toBe('Internal Campaign')
    expect(result.prop53).toBe('D=v3')
    expect(result.prop54).toBe('D=v58')
    expect(result.eVar58).toBe('D=v3')
    expect(result.eVar2).toBe(internalCampaignId)
  })
  it('should return empty object if no intNav', () => {
    const result = addInternalNav()
    expect(result).toEqual({})
  })
  it('should return empty object if intNav is Empty', () => {
    const result = addInternalNav({})
    expect(result).toEqual({})
  })
})

describe('getInternalNav', () => {
  it('should return proper values for Global Nav', () => {
    const intNav = {
      type: 'Global Nav',
      details: 'Global Nav:Clothing:Dresses',
    }
    const result = getInternalNav(intNav)
    expect(result.eVar3).toBe('Global Nav')
    expect(result.prop53).toBe('D=v3')
    expect(result.prop54).toBe('D=v58')
    expect(result.eVar58).toBe('Global Nav:Clothing:Dresses')
  })
  it('should return proper values for Footer', () => {
    const intNav = {
      type: 'Footer',
      details: 'Footer:Gift Card',
    }
    const result = getInternalNav(intNav)
    expect(result.eVar3).toBe('Footer')
    expect(result.prop53).toBe('D=v3')
    expect(result.prop54).toBe('D=v58')
    expect(result.eVar58).toBe('Footer:Gift Card')
  })
  it('should return proper values for Search', () => {
    const intNav = {
      type: 'Search',
    }
    const result = getInternalNav(intNav)
    expect(result.eVar3).toBe('Search')
    expect(result.prop53).toBe('D=v3')
    expect(result.prop54).toBe('D=v58')
    expect(result.eVar58).toBe('D=v3')
  })
  it('should return proper values for Internal Campaign', () => {
    const intNav = {
      type: 'Internal Campaign',
    }
    const result = getInternalNav(intNav)
    expect(result.eVar3).toBe('Internal Campaign')
    expect(result.prop53).toBe('D=v3')
    expect(result.prop54).toBe('D=v58')
    expect(result.eVar58).toBe('D=v3')
  })
})
