import { bagChange, viewBag } from '../shoppingBag'

jest.mock('../analytics', () => ({
  eventBasedAnalytics: jest.fn(),
  stripCommas: jest.fn().mockImplementation((val) => val),
}))
import { eventBasedAnalytics } from '../analytics'

describe('Shopping Bag Analytics', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  describe('bagChange', () => {
    const product = {
      lineNumber: 'LAN7654',
      size: '14',
      unitPrice: '15.95',
      quantity: 1,
      name: 'Super cool top',
    }

    it('should call eventBasedAnalytics with correct data when adding to bag (`scAdd`)', () => {
      bagChange(product, 'scAdd')

      expect(eventBasedAnalytics).toHaveBeenCalledWith({
        events: 'scAdd',
        products:
          ';(LAN7654)Super cool top;;;event76=1|event77=15.95;eVar35=14',
      })
    })

    it('should call eventBasedAnalytics with correct data when removing from bag (`scRemove`)', () => {
      bagChange(product, 'scRemove')

      expect(eventBasedAnalytics).toHaveBeenCalledWith({
        events: 'scRemove',
        products:
          ';(LAN7654)Super cool top;;;event112=1|event113=15.95;eVar35=14',
      })
    })

    it('should call eventBasedAnalytics with correct data when changing bag (`event30`)', () => {
      bagChange(product, 'event30')

      expect(eventBasedAnalytics).toHaveBeenCalledWith({
        events: 'event30',
        products: ';(LAN7654)Super cool top;;;;',
      })
    })

    it('should call eventBasedAnalytics with discount eVars if product is discounted', () => {
      const discountedProduct = {
        ...product,
        wasWasPrice: '19.99',
      }
      bagChange(discountedProduct, 'scAdd')

      expect(eventBasedAnalytics.mock.calls[0][0].products).toBe(
        ';(LAN7654)Super cool top;;;event76=1|event77=15.95;eVar35=14|eVar68=19.99|eVar69=20.21|eVar78=15.95'
      )
    })
  })

  describe('viewBag', () => {
    const bag = {
      products: [
        {
          lineNumber: 'LKS72615',
          name: 'first cool product',
        },
      ],
    }

    it('should call eventBasedAnalytics with correct data when bag is open', () => {
      expect(eventBasedAnalytics).not.toHaveBeenCalled()
      viewBag(bag)
      expect(eventBasedAnalytics).toHaveBeenCalledTimes(1)
      expect(eventBasedAnalytics).toHaveBeenCalledWith({
        events: 'scView',
        products: [';(LKS72615)first cool product'],
      })
    })
  })
})
