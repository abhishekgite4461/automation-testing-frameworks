import { setAnalyticsIntnav } from '../analytics-helper'

jest.mock('../../../actions/common/analyticsActions', () => ({
  setInternalNavigation: jest.fn(),
}))
import { setInternalNavigation } from '../../../actions/common/analyticsActions'

describe('analytics-helpers', () => {
  describe('setAnalyticsIntnav', () => {
    const productsCategoriesName = 'productCategories'
    const footerCategoriesName = 'helpAndInfoGroup'
    beforeEach(() => {
      jest.resetAllMocks()
    })
    it('should call setInternalNavigation with proper params for Global Nav', () => {
      const state = {
        navigation: {
          categoryHistory: [{ label: 'Clothing' }],
        },
      }
      const dispatch = jest.fn((val) => val)
      const getState = jest.fn(() => state)
      const action = setAnalyticsIntnav('Dresses', productsCategoriesName)
      action(dispatch, getState)
      expect(setInternalNavigation).toHaveBeenCalledTimes(1)
      expect(setInternalNavigation).toHaveBeenCalledWith({
        type: 'Global Nav',
        details: 'Global Nav:Clothing:Dresses',
      })
    })
    it('should call setInternalNavigation with proper params for Global Nav with many categories', () => {
      const state = {
        navigation: {
          categoryHistory: [{ label: 'Clothing' }, { label: 'Dresses' }],
        },
      }
      const dispatch = jest.fn((val) => val)
      const getState = jest.fn(() => state)
      const action = setAnalyticsIntnav('Casual', productsCategoriesName)
      action(dispatch, getState)
      expect(setInternalNavigation).toHaveBeenCalledTimes(1)
      expect(setInternalNavigation).toHaveBeenCalledWith({
        type: 'Global Nav',
        details: 'Global Nav:Clothing:Dresses:Casual',
      })
    })
    it('should call setInternalNavigation with proper params for Footer', () => {
      const state = {
        navigation: {
          categoryHistory: [],
        },
      }
      const dispatch = jest.fn((val) => val)
      const getState = jest.fn(() => state)
      const action = setAnalyticsIntnav('Gift Card', footerCategoriesName)
      action(dispatch, getState)
      expect(setInternalNavigation).toHaveBeenCalledTimes(1)
      expect(setInternalNavigation).toHaveBeenCalledWith({
        type: 'Footer',
        details: 'Footer:Gift Card',
      })
    })
  })
})
