const productViews = { productIsActive: true }
const refinements = {
  selectedOptions: {
    fabric: ['faux fur'],
    price: [43, 125],
  },
}
const sorting = { currentSortOption: null }
const grid = { options: { default: [2, 3, 4], mobile: [1, 2, 3] }, columns: 2 }
const routing = {
  visited: [],
}
const products = {
  breadcrumbs: [
    {
      label: 'Home',
    },
    {
      label: 'Clothing',
      category: '203984',
    },
    {
      label: 'Denim',
      category: '203984,525523',
    },
  ],
  products: [{}, {}],
  totalProducts: '2',
}
const config = {
  brandCode: 'ts',
}
export { routing, sorting, grid, config, refinements, products, productViews }
