import React from 'react'
import { shallow } from 'enzyme'
import {
  decorator,
  popReferrerProduct,
  REFERRER_TYPES,
} from '../product-referrals-helper'
import productDataMock from '../../../../../test/mocks/product-detail'

jest.mock('../../../../client/lib/cookie', () => ({
  getItem: jest.fn(),
  setItem: jest.fn(),
}))

describe('product-refferals-helper', () => {
  afterEach(() => {
    jest.resetAllMocks()
  })

  describe('decorator', () => {
    const ComponentToDecorate = () => <div />
    const DecoratedComponent = decorator(REFERRER_TYPES.RECENTLY_VIEWED)(
      ComponentToDecorate
    )

    const toProductId = 1337

    it('should throw an error when an invalid type is provided', () => {
      const thunk = () => decorator('INVALID_TYPE')
      expect(thunk).toThrow(/Invalid referral type/)
    })

    it('should not provide a "registerReferral" when no "referringProduct" is provided', () => {
      const passedDownProps = shallow(<DecoratedComponent />)
        .find(ComponentToDecorate)
        .props()
      expect(passedDownProps.registerReferral).toBeUndefined()
    })

    it('should throw an error when no "toProductId" is provided', () => {
      const registerRefferal = shallow(
        <DecoratedComponent referringProduct={productDataMock} />
      )
        .find(ComponentToDecorate)
        .props().registerReferral
      expect(() => registerRefferal()).toThrow(
        /must provide the id of the target product/
      )
    })

    it('should store the product using the analytics cookie util when "registerReferral" is called', () => {
      const { setItem: setItemMock } = require('../../../../client/lib/cookie')

      const registerRefferal = shallow(
        <DecoratedComponent referringProduct={productDataMock} />
      )
        .find(ComponentToDecorate)
        .props().registerReferral

      registerRefferal(toProductId)

      expect(setItemMock.mock.calls.length).toBe(1)
      expect(setItemMock.mock.calls[0][0]).toEqual(
        `${REFERRER_TYPES.RECENTLY_VIEWED}-product-referral`
      )
      expect(setItemMock.mock.calls[0][1]).toEqual(
        JSON.stringify({
          fromProductId: productDataMock.productId,
          fromProductLineNumber: productDataMock.lineNumber,
          fromProductName: productDataMock.name,
          toProductId,
        })
      )
    })
  })

  describe('popReferrerProduct', () => {
    it('should throw an error when an invalid type is provided', () => {
      const thunk = () => popReferrerProduct('INVALID_TYPE')
      expect(thunk).toThrow(/Invalid referral type/)
    })

    it('should return the product using the analytics cookie util and nullify the cookie', () => {
      const {
        setItem: setItemMock,
        getItem: getItemMock,
      } = require('../../../../client/lib/cookie')

      getItemMock.mockReturnValueOnce(JSON.stringify(productDataMock))

      const actualResult = popReferrerProduct(REFERRER_TYPES.RECOMMENDED)

      expect(getItemMock.mock.calls.length).toBe(1)
      expect(getItemMock.mock.calls[0][0]).toEqual(
        `${REFERRER_TYPES.RECOMMENDED}-product-referral`
      )

      expect(setItemMock.mock.calls.length).toBe(1)
      expect(setItemMock.mock.calls[0][0]).toEqual(
        `${REFERRER_TYPES.RECOMMENDED}-product-referral`
      )
      expect(setItemMock.mock.calls[0][1]).toEqual('')

      expect(actualResult).toEqual(productDataMock)
    })
  })
})
