/**
 * These are a set of helpers with a decorator to aid in tracking a referral
 * from one product to another (e.g. click through from a "why not try").
 * The product referral is then recorded within the adobe analytics data helper
 * for the PDP.
 */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { values, equals, isNil, isEmpty } from 'ramda'
import { getItem, setItem } from '../../../client/lib/cookie'

const isNullOrEmpty = (x) => isNil(x) || isEmpty(x)

export const REFERRER_TYPES = {
  RECOMMENDED: 'RECOMMENDED',
  RECENTLY_VIEWED: 'RECENTLY_VIEWED',
}

// :: string -> bool
const isValidReferrerType = (type) =>
  !isNullOrEmpty(type) && values(REFERRER_TYPES).find(equals(type)) != null

// :: string -> string
const cookieNameForType = (type) => `${type}-product-referral`

// :: (Object, string) -> void
const setReferral = (refferal, type) => {
  setItem(cookieNameForType(type), refferal)
}

// :: (Object, string) -> void
const pushReferrerProduct = (referral, type) => {
  setReferral(JSON.stringify(referral), type)
}

// :: string -> ?Object
export const popReferrerProduct = (referralType) => {
  if (!isValidReferrerType(referralType)) {
    throw new Error('Invalid referral type specified')
  }

  const product = getItem(cookieNameForType(referralType))
  setReferral('', referralType)
  return isNullOrEmpty(product) ? undefined : JSON.parse(product)
}

// :: string -> Component -> Component
export const decorator = (referralType) => {
  if (!isValidReferrerType(referralType)) {
    throw new Error('Invalid referral type specified')
  }

  return (WrappedComponent) =>
    class ProductReferrals extends Component {
      static displayName = `ProductReferrals(${WrappedComponent.displayName ||
        WrappedComponent.name ||
        'Component'})`

      static WrappedComponent = WrappedComponent

      static propTypes = {
        referringProduct: PropTypes.object,
      }

      constructor(props, context) {
        super(props, context)
        this.referralType = referralType
      }

      render() {
        const { referringProduct, ...otherProps } = this.props

        const passThroughProps = Object.assign(
          {},
          otherProps,
          referringProduct == null
            ? {}
            : {
                registerReferral: (toProductId) => {
                  if (isNullOrEmpty(toProductId)) {
                    throw new Error(
                      'You must provide the id of the target product'
                    )
                  }
                  pushReferrerProduct(
                    {
                      fromProductId: referringProduct.productId,
                      fromProductLineNumber: referringProduct.lineNumber,
                      fromProductName: referringProduct.name,
                      toProductId,
                    },
                    referralType
                  )
                },
              }
        )

        return <WrappedComponent {...passThroughProps} />
      }
    }
}
