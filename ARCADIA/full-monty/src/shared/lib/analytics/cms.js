import { analyticsTl } from '../../lib/analytics/analytics'

/* This EventListener works when we receive some message from iFrame (ecmc template)
 * TODO: refactor this code for some more reusable logic
 */
export function cmsIframePostMessage(brandName, { data, origin }) {
  const validOrigin = new RegExp(`${brandName}\\.(com|co\\.uk|eu|us|fr|de)$`)
  if (
    validOrigin.test(origin) &&
    typeof data === 'string' &&
    !data.includes('-un')
  ) {
    // Send on analytics only when is a message for select
    analyticsTl({ prop3: `gift-finder-mob-${data}` })
  }
}
