import {
  updateUVProductsList,
  updateUVProductDetail,
  updateUVTransaction,
} from './index'
import { getMatchingAttribute } from '../product-utilities'
import { localise } from '../localisation'
import { compose, map, curry, reject } from 'ramda'

export function dispatchQubitEventHook() {
  document.dispatchEvent(new Event('qubitPageview'))
}

function getPageBreadcrumb(pageType, { products, currentProduct }) {
  if (pageType === 'category') {
    const { breadcrumbs } = products
    if (!breadcrumbs) return undefined
    const rejectHome = reject((cat) => cat.label === 'Home')
    const mapBreadcrumb = map((cat) => cat.label)
    return compose(mapBreadcrumb, rejectHome)(breadcrumbs)
  } else if (pageType === 'product') {
    const { attributes, name } = currentProduct
    const productBreadcrumb = []
    const prodType =
      attributes && getMatchingAttribute('PROD_PRODUCT_TYPE', attributes)
    if (prodType) {
      productBreadcrumb.push(prodType)
    }
    const prodSubType =
      attributes && getMatchingAttribute('CE3_PRODUCT_TYPE', attributes)
    if (prodSubType) {
      productBreadcrumb.push(prodSubType)
    }
    productBreadcrumb.push(name)
    return productBreadcrumb
  }
}

function getPageType(path, state) {
  const { language, brand } = state.config
  const curreiedLocalise = curry(localise)
  const l = curreiedLocalise(language, brand)
  const page = {}
  if (path === '/') {
    page.type = 'home'
  } else if (path.startsWith('/order-complete')) {
    page.type = 'confirmation'
  } else if (
    path.startsWith('/search') ||
    (new RegExp(`^/.+/${l('category')}/`).test(path) &&
      !new RegExp(`^/.+/${l('category')}/(your-details|help-information)`).test(
        path
      ))
  ) {
    page.type = 'category'
    page.breadcrumb = getPageBreadcrumb(page.type, state)
  } else if (new RegExp(`^/.+/${l('product')}/`).test(path)) {
    page.type = 'product'
    page.breadcrumb = getPageBreadcrumb(page.type, state)
  } else if (new RegExp('^/checkout/').test(path)) {
    page.type = 'checkout'
  } else {
    page.type = 'content'
  }
  return page
}

function mapBasketProducts(product, { config }) {
  const {
    productId,
    name,
    quantity = 0,
    size,
    wasPrice,
    unitPrice,
    totalPrice = 0,
    lineNumber,
    assets,
  } = product
  const { currencyCode, brandCode } = config
  const imageAsset =
    assets && assets.find((asset) => asset.assetType === 'IMAGE_LARGE')
  return {
    product: {
      id: productId ? productId.toString() : '',
      name,
      url: `${window.location.protocol}//${
        window.location.host
      }/search?q=${brandCode.toUpperCase()}${lineNumber}`,
      currency: currencyCode,
      unit_sale_price: unitPrice ? parseFloat(unitPrice) : 0,
      unit_price: wasPrice || unitPrice ? parseFloat(wasPrice || unitPrice) : 0,
      size,
      sku_code: `${brandCode.toUpperCase()}${lineNumber}`,
      image_url: imageAsset ? imageAsset.url : undefined,
    },
    quantity,
    subtotal: wasPrice || unitPrice ? (wasPrice || unitPrice) * quantity : 0,
    total_discount:
      wasPrice || unitPrice
        ? (wasPrice || unitPrice) * quantity - totalPrice
        : 0,
  }
}

function updateUVBasket(state) {
  const {
    shoppingBag: { bag },
    config: { currencyCode },
  } = state
  const { orderId, subTotal, total, products, promotions, discounts } = bag
  const vouchers = promotions
    ? promotions.map(({ promotionCode }) => {
        return promotionCode
      })
    : []
  const voucherDiscounts = discounts
    ? discounts.reduce((totalDiscount, { value }) => {
        return totalDiscount + parseFloat(value)
      }, 0)
    : 0
  return {
    id: orderId ? orderId.toString() : '',
    subtotal: subTotal ? parseFloat(subTotal) : 0,
    total: total ? parseFloat(total) : 0,
    subtotal_include_tax: true,
    currency: currencyCode,
    vouchers,
    voucher_discount: voucherDiscounts,
    line_items: products.map((product) => mapBasketProducts(product, state)),
  }
}

function checkTransactedUser({
  hasBillingDetails,
  hasPayPal,
  hasCardNumberHash,
  hasDeliveryDetails,
  deliveryDetails = {},
  billingDetails = {},
  creditCard = {},
}) {
  const { cardNumberHash, type } = creditCard
  const deliveryAddress = deliveryDetails.addressDetailsId
  const billingAddress = billingDetails.addressDetailsId
  return (
    hasBillingDetails ||
    hasDeliveryDetails ||
    hasPayPal ||
    hasCardNumberHash ||
    cardNumberHash ||
    type === 'PYPAL' ||
    (deliveryAddress && deliveryAddress !== -1) ||
    (billingAddress && billingAddress !== -1)
  )
}

function getUVUser(state) {
  const {
    config: { language },
    account: {
      user: {
        email,
        userTrackingId,
        subscriptionId,
        firstName,
        lastName,
        title,
      },
      user,
    },
  } = state

  return {
    user_id: userTrackingId ? userTrackingId.toString() : '',
    email,
    name: firstName ? `${title} ${firstName} ${lastName}` : undefined,
    subscription_id: subscriptionId ? subscriptionId.toString() : '',
    language,
    has_transacted: checkTransactedUser(user),
  }
}

export function updateUniversalVariable() {
  return (dispatch, getState) => {
    if (!process.browser) return
    const state = getState()
    const {
      config: { language },
      auth: { authentication },
      routing,
    } = state
    const {
      location: { pathname },
    } = routing
    const version = '1.2.1'
    const basket = updateUVBasket(state)
    const user = !authentication ? { language } : getUVUser(state)
    const page = getPageType(pathname, state)
    const UVData = {}
    switch (page.type) {
      case 'category':
        UVData.listing = updateUVProductsList(state)
        break
      case 'product':
        UVData.product = updateUVProductDetail(state)
        break
      case 'confirmation':
        UVData.transaction = updateUVTransaction(state)
        break
      default:
        break
    }
    window.universal_variable = { version, basket, user, page, ...UVData }
  }
}
