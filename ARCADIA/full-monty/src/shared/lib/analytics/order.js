import { updateUniversalVariable } from './qubit-analytics'
import { isEmpty, path } from 'ramda'

export function order() {
  return (dispatch) => {
    // dispatchQubitEventHook()
    dispatch(updateUniversalVariable())
  }
}

function formatUVAddress({ name, address1, address2, address3, country }) {
  return {
    name,
    address: address1,
    city: address2,
    postcode: address3,
    country,
  }
}

function formatUVLineItems(orderLines, { config, checkout }) {
  const {
    partialOrderSummary: { basket },
  } = checkout
  const { currencyCode, brandCode } = config
  return orderLines.map((lineItem) => {
    const {
      name,
      quantity,
      unitPrice,
      lineNo,
      colour,
      size,
      imageUrl,
    } = lineItem
    const basketItem =
      basket &&
      basket.products &&
      basket.products.find(({ lineNumber }) => lineNumber === lineNo)
    const { productId = '' } = basketItem || {}
    const wasPrice =
      basketItem && basketItem.wasPrice && !isEmpty(basketItem.wasPrice)
        ? parseFloat(basketItem.wasPrice)
        : undefined
    const subtotal = wasPrice
      ? parseFloat(wasPrice * quantity)
      : parseFloat(unitPrice * quantity)
    const discount = subtotal - parseFloat(unitPrice * quantity)
    return {
      product: {
        id: productId ? productId.toString() : '',
        name,
        url: `${window.location.protocol}//${
          window.location.host
        }/search?q=${brandCode.toUpperCase()}${lineNo}`,
        currency: currencyCode,
        unit_sale_price: unitPrice ? parseFloat(unitPrice) : 0,
        unit_price:
          wasPrice || unitPrice ? parseFloat(wasPrice || unitPrice) : 0,
        color: colour,
        size,
        sku_code: `${brandCode.toUpperCase()}${lineNo}`,
        image_url: imageUrl,
      },
      quantity,
      subtotal,
      total_discount: discount,
    }
  })
}

export function updateUVTransaction(state) {
  const { currencyCode } = state.config
  const {
    orderCompleted,
    partialOrderSummary: { basket },
  } = state.checkout
  const {
    orderId,
    orderLines,
    paymentDetails,
    deliveryMethod,
    deliveryPrice,
    totalOrderPrice,
    billingAddress,
    deliveryAddress,
    totalOrdersDiscount,
  } = orderCompleted

  const subtotal =
    orderLines &&
    orderLines.reduce((subtotal, product) => {
      const { unitPrice, quantity, lineNo } = product
      let wasPrice =
        basket &&
        basket.products &&
        basket.products.find(({ lineNumber }) => lineNumber === lineNo).wasPrice
      wasPrice = isEmpty(wasPrice) ? undefined : parseFloat(wasPrice)
      subtotal += wasPrice
        ? parseFloat(wasPrice * quantity)
        : parseFloat(unitPrice * quantity)
      return subtotal
    }, 0)

  const paymentMethods =
    paymentDetails &&
    paymentDetails.reduce((currentPayments, { paymentMethod }) => {
      if (
        !currentPayments.length &&
        paymentMethod === 'Giftcard' &&
        paymentDetails.length === 1
      )
        return 'Giftcard'
      else if (currentPayments.length > 0 && paymentMethod === 'Giftcard') {
        return currentPayments.concat(' / Giftcard')
      }
      return currentPayments.concat(paymentMethod)
    }, '')

  const promotions = path(
    ['checkout', 'partialOrderSummary', 'basket', 'promotions'],
    state
  )
  const vouchers =
    promotions &&
    promotions.map((promotion) => {
      return promotion.promotionCode
    })

  return {
    order_id: orderId ? orderId.toString() : '',
    returning: false,
    currency: currencyCode,
    payment_type: paymentMethods,
    subtotal,
    shipping_cost: deliveryPrice ? parseFloat(deliveryPrice) : 0,
    shipping_method: deliveryMethod,
    total: totalOrderPrice ? parseFloat(totalOrderPrice) : 0,
    delivery: deliveryAddress && formatUVAddress(deliveryAddress),
    billing: billingAddress && formatUVAddress(billingAddress),
    line_items: orderLines && formatUVLineItems(orderLines, state),
    voucher_discount: totalOrdersDiscount || 0,
    vouchers,
  }
}
