import { test, pickBy, values, path, isEmpty, propEq, find } from 'ramda'

function formatBundleProducts(
  bundleProducts,
  { config: { currencyCode }, forms: { bundlesAddToBag } }
) {
  return bundleProducts.map(
    ({
      productId,
      name,
      colour,
      attributes,
      description,
      unitPrice,
      wasPrice,
      items,
      grouping,
      assets,
    }) => {
      const brandKey = (val, key) => {
        return test(/BRANDS/)(key)
      }
      const brands = values(pickBy(brandKey, attributes))
      const categoryKey = (val, key) => {
        return test(/CE3_PRODUCT_TYPE/)(key)
      }
      const categories = values(pickBy(categoryKey, attributes))

      const sku =
        bundlesAddToBag &&
        path([productId, 'fields', 'size', 'value'], bundlesAddToBag)
      const item =
        sku && !isEmpty(sku) ? find(propEq('sku', sku), items) : undefined
      const imageAsset =
        assets && assets.find((asset) => asset.assetType === 'IMAGE_LARGE')
      return {
        id: productId ? productId.toString() : '',
        name,
        image_url: imageAsset ? imageAsset.url : undefined,
        category: categories.length ? categories[0] : undefined,
        manufacturer: brands.length ? brands[0] : undefined,
        description,
        currency: currencyCode,
        unit_sale_price: unitPrice ? parseFloat(unitPrice) : 0,
        unit_price:
          wasPrice || unitPrice ? parseFloat(wasPrice || unitPrice) : 0,
        sku_code: grouping,
        stock: item && item.quantity,
        color: colour,
        size: item && item.size,
      }
    }
  )
}

export function updateUVProductDetail(state) {
  const {
    config: { currencyCode },
    currentProduct: {
      productId,
      sourceUrl,
      name,
      colour,
      unitPrice,
      rrp,
      wasPrice,
      wasWasPrice,
      description,
      isBundleOrOutfit,
      bundleProducts,
      attributes,
      grouping,
      assets,
    },
    productDetail: { activeItem },
  } = state
  const brandKey = (val, key) => {
    return test(/BRANDS/)(key)
  }
  const brands = values(pickBy(brandKey, attributes))
  const categoryKey = (val, key) => {
    return test(/CE3_PRODUCT_TYPE/)(key)
  }
  const categories = values(pickBy(categoryKey, attributes))
  const { size, quantity } = activeItem

  const bundle = isBundleOrOutfit
    ? formatBundleProducts(bundleProducts, state)
    : undefined
  const imageAsset =
    assets && assets.find((asset) => asset.assetType === 'IMAGE_LARGE')

  return {
    id: productId ? productId.toString() : '',
    url: sourceUrl,
    image_url: imageAsset ? imageAsset.url : undefined,
    name,
    category: categories.length ? categories[0] : undefined,
    manufacturer: brands.length ? brands[0] : undefined,
    description,
    linked_products: bundle,
    currency: currencyCode,
    unit_sale_price: unitPrice ? parseFloat(unitPrice) : 0,
    unit_price:
      rrp || wasWasPrice || wasPrice || unitPrice
        ? parseFloat(rrp || wasWasPrice || wasPrice || unitPrice)
        : 0,
    sku_code: grouping,
    stock: !isBundleOrOutfit ? quantity : undefined,
    color: !isBundleOrOutfit ? colour : undefined,
    size: !isBundleOrOutfit ? size : undefined,
  }
}
