import { omit, isEmpty, contains } from 'ramda'

// adds in specific keys not required in pageBasedAnalytics function
export function addPageSpecificAnalyticsKeys(data) {
  const basicPageAnalyticsKeys = [
    'pageName',
    'granularPageName',
    'pageType',
    'firstCategory',
    'loadTime',
    'percentagePageViewed',
    'intNav',
    'checkout',
    'isCheckout',
    'lastPage',
  ]
  const omitted = omit(basicPageAnalyticsKeys)
  return Object.assign({}, omitted(data))
}

export function getBasicPageData(data, analyticsBrandCode) {
  const pageName = `${analyticsBrandCode}:${data.pageName}`
  const firstCategory = data.firstCategory ? `:${data.firstCategory}` : ''
  const secondCategory = data.secondCategory ? ` > ${data.secondCategory}` : ''
  const granularPageName = data.granularPageName
    ? `${analyticsBrandCode}:${data.granularPageName}`
    : `${analyticsBrandCode}${
        data.pageType ? `:${data.pageType}` : ''
      }${firstCategory}${secondCategory}`
  const channel = firstCategory ? `${analyticsBrandCode}${firstCategory}` : ''

  return {
    pageName,
    granularPageName,
    channel,
  }
}

export function getInternalNav(intNav) {
  const eVar58 = contains(intNav.type, ['Global Nav', 'Footer'])
    ? intNav.details
    : 'D=v3'
  return {
    eVar3: intNav.type,
    prop53: 'D=v3',
    prop54: 'D=v58',
    eVar58,
  }
}

export function addInternalNav(intNav, internalCampaignId) {
  if (internalCampaignId) {
    return {
      ...getInternalNav({ type: 'Internal Campaign' }),
      eVar2: internalCampaignId,
    }
  }
  if (intNav && !isEmpty(intNav)) return getInternalNav(intNav)

  return {}
}
