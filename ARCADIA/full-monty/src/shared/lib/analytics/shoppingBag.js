import { eventBasedAnalytics, stripCommas } from '../../lib/analytics/analytics'

// TODO: make these functions reusable
const objectToAnalyticsString = (obj) => {
  return Object.keys(obj)
    .sort((keyOne, keyTwo) => keyOne.localeCompare(keyTwo))
    .reduce((currentStrings, key) => {
      return currentStrings.concat(`${key}=${obj[key]}`)
    }, [])
    .join('|')
}

const createAnalyticsProduct = ({
  category = '',
  product = '',
  quantity = '',
  cost = '',
  events = {},
  eVars = {},
} = {}) => {
  // Abode Analytics Syntax of the Products Variable:
  // "Category;Product;Quantity;Price;eventN=X[|eventN2=X2];eVarN=merch_category[|eVarN2=merch_category2]"
  return [
    category,
    stripCommas(product),
    quantity,
    cost,
    objectToAnalyticsString(events),
    objectToAnalyticsString(eVars),
  ].join(';')
}

export function bagChange(item, event) {
  const { lineNumber, size, unitPrice, wasWasPrice, quantity, name } = item
  const product = {
    product: `(${lineNumber})${name}`,
  }
  if (event !== 'event30') {
    product.eVars = {
      eVar35: size,
    }
    const [quantityEventName, priceEventName] =
      event === 'scRemove' ? ['event112', 'event113'] : ['event76', 'event77']
    product.events = {
      [quantityEventName]: quantity,
      [priceEventName]: unitPrice,
    }
    if (wasWasPrice) {
      const priceChange = wasWasPrice - unitPrice
      product.eVars = {
        ...product.eVars,
        eVar68: wasWasPrice,
        eVar69: ((priceChange / wasWasPrice) * 100).toFixed(2),
        eVar78: unitPrice,
      }
    }
  }

  eventBasedAnalytics({
    events: event,
    products: createAnalyticsProduct(product),
  })
}

export function viewBag(bag) {
  const { products = [] } = bag
  eventBasedAnalytics({
    events: 'scView',
    products: products.map(({ name, lineNumber }) => {
      return `;(${lineNumber})${stripCommas(name)}`
    }),
  })
}
