import { eventBasedAnalytics } from '../../lib/analytics/analytics'
import { updateUniversalVariable } from '../../lib/analytics/qubit-analytics'

export function authEventAnalytics({ username, email }, events) {
  return (dispatch, getState) => {
    const state = getState()
    const {
      account: {
        user: { userTrackingId, subscriptionId },
      },
    } = state

    const authAnalyticsData = {
      events: events.join(','),
    }

    // upon successful login (event11) and registration (event9)
    if (/(event11|event9)(,|$)/.test(events)) {
      authAnalyticsData.eVar7 = (username || email).toLowerCase()
      if (userTrackingId) authAnalyticsData.eVar6 = userTrackingId
      if (subscriptionId) authAnalyticsData.eVar5 = subscriptionId
    }

    eventBasedAnalytics(authAnalyticsData)
    dispatch(updateUniversalVariable())
  }
}

export function logoutEventAnalytics() {
  return (dispatch) => {
    eventBasedAnalytics({
      events: 'event114',
    })
    dispatch(updateUniversalVariable())
  }
}
