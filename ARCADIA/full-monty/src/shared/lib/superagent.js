const superagent = require('superagent')

let cache = { storage: 'session', expiration: 600, verbose: false }
const defaults = {
  cacheWhenEmpty: false,
  expiration: 900,
  pruneOptions: ['arcadia-api-key', 'arcadia-session-key', 'authjwt'],
}

if (!process.browser && process.env.REDIS_URL) {
  // Only include redis cache if in server context
  // relies on webpack ignore plugin to remove from bundle.js
  const CsRedis = require('cache-service-redis')

  cache = new CsRedis({ redisUrl: process.env.REDIS_URL })

  cache.db.on('error', (error) => {
    throw error
  })
} else {
  cache = new (require('cache-service-cache-module'))(cache)
}

const _origGet = cache.get
cache.get = function _get(key, callback) {
  _origGet.call(cache, key, (error, response) => {
    if (response) {
      if (!process.browser && process.env.REDIS_URL) {
        require('../../server/lib/newrelic.js').addCustomParameter(
          'elasticacheHit',
          'true'
        )
      }
      response.cached = true
    }
    callback(error, response)
  })
}

const _origSet = cache.set
cache.set = function _set(key, response, expiration, refresh, callback) {
  const { body, text, statusCode, status, ok } = response
  // body.success and body.statusCode are scrAPI specific error flags that are usually absent in successful response
  if (
    expiration < 0 &&
    body.success !== false &&
    (!body.statusCode || body.statusCode === 200)
  ) {
    const headers = Object.assign({}, response.headers)
    delete headers['arcadia-api-key']
    delete headers['arcadia-session-key']
    delete headers.authjwt

    _origSet.call(
      cache,
      key,
      { body, text, headers, statusCode, status, ok },
      -expiration,
      refresh,
      callback
    )
  } else {
    setImmediate(callback)
  }
}

superagent.Request.prototype.cache = function _cache(cacheable, secs) {
  if (typeof cacheable === 'number') {
    secs = cacheable
    cacheable = !!secs
  }
  if (cacheable) {
    this.expiration(secs ? -secs : -900)
  }
  return this
}

require('superagent-cache')(superagent, cache, defaults)

export default superagent
