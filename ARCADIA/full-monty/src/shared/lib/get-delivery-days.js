import {
  path,
  compose,
  contains,
  both,
  find,
  propEq,
  is,
  isEmpty,
  propSatisfies,
} from 'ramda'

const isNonEmptyArray = (x) => Array.isArray(x) && !isEmpty(x)

const isNonEmptyObject = (x) =>
  is(Object, x) && !isEmpty(x) && !Array.isArray(x)

const getDayNames = () => [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
]

const getDayFromDateString = (d) => {
  const dayNames = getDayNames()
  const date = new Date(d)
  return !isNaN(date.getDay()) && dayNames[date.getDay()]
}

export const getCurrentDate = () => new Date()

export const generateCutoffTimestamp = (date, cutoffTime) => {
  const [hours, minutes] = Array.isArray(cutoffTime)
    ? cutoffTime
    : cutoffTime.split(':')
  const cutoffDate = new Date(
    date.getFullYear(),
    date.getMonth(),
    date.getDate(),
    hours,
    minutes
  )
  return cutoffDate.getTime()
}

export const isTimePast = (cutoffTime) => {
  if (!cutoffTime) return true
  const now = new Date()
  const time = generateCutoffTimestamp(now, cutoffTime)
  return !isNaN(time) && now.getTime() >= time
}

export const formatDateAsYMD = (date) => {
  return [
    date.getFullYear(),
    `0${date.getMonth() + 1}`.slice(-2),
    `0${date.getDate()}`.slice(-2),
  ].join('-')
}

export const getNumberOfDaysFromNow = (day) => {
  if (day === 'today' || !day) return 0
  const dayNames = getDayNames()
  const dayIndex = dayNames.findIndex((d) => day === d)
  const todayIndex = new Date().getDay()
  const calcDay = dayIndex - todayIndex
  return calcDay < 0 ? dayNames.length + 1 + calcDay : calcDay
}

export const getStockFromStockList = (sku, store) =>
  compose(
    both(Array.isArray, compose(path(['stock']), find(propEq('sku', sku)))),
    path(['stockList'])
  )(store)

export const getDeliveryDay = (inventory, quantity) => {
  const {
    expressdates: [expressDate0, expressDate1] = [],
    cutofftime: cutoffTime,
    quantity: stockQuantity,
  } = inventory

  const notPassedCutoff =
    cutoffTime &&
    !exports.isTimePast([cutoffTime.slice(0, -2), cutoffTime.slice(-2)])
  const stockExists = stockQuantity && stockQuantity >= quantity

  if (expressDate0 && stockExists && notPassedCutoff)
    return getDayFromDateString(expressDate0)
  if (expressDate1 && stockExists) return getDayFromDateString(expressDate1)
  return ''
}

export const getDCDeliveryDay = ({ inventoryPositions, quantity }) => {
  return compose(isNonEmptyArray, path(['inventorys']))(inventoryPositions)
    ? exports.getDeliveryDay(inventoryPositions.inventorys[0], quantity)
    : ''
}

export const getExpressDeliveryDay = ({ inventoryPositions, quantity }) => {
  const expressAvailability =
    compose(isNonEmptyArray, path(['invavls']))(inventoryPositions) &&
    inventoryPositions.invavls.find(
      propSatisfies(contains('EXPRESS'), 'stlocIdentifier')
    )

  return expressAvailability
    ? exports.getDeliveryDay(expressAvailability, quantity)
    : ''
}

export const isCFSiToday = (product = {}, store = {}) => {
  if (!path(['quantity'], product) || !path(['sku'], product) || !store)
    return false

  const { sku, quantity = 1 } = product
  const dateString = formatDateAsYMD(exports.getCurrentDate())
  const notPassedCutoff =
    !!store.cfsiPickCutOffTime && !exports.isTimePast(store.cfsiPickCutOffTime)
  const availableToday =
    !!store.cfsiAvailableOn && store.cfsiAvailableOn.includes(dateString)
  const stock = store.stock || exports.getStockFromStockList(sku, store) || 0

  return availableToday && notPassedCutoff && parseInt(stock, 10) >= quantity
}

/**
 * Function that returns product delivery availability
 *
 * @param {Object} product mapped product with its inventory details, refer to ProductInventory schema
 * @param {Object} store
 * @returns {Object}
 *
 *  Delivery Precedence
 *  StoreExpress - store with express, DC
 *  Home express - DC, store express
 *  ParcelShop - from DC
 */
export function getFulfilmentDetails(product = {}, store = {}) {
  if (!isNonEmptyObject(product)) return null

  const DCDeliveryDay = exports.getDCDeliveryDay(product) // express from DC (warehouse)
  const expressDeliveryDay =
    exports.getExpressDeliveryDay(product) || DCDeliveryDay // express from store inventory
  const CFSiDay = exports.isCFSiToday(product, store)
    ? 'today'
    : expressDeliveryDay

  return {
    CFSiDay,
    expressDeliveryDay,
    homeExpressDeliveryDay: DCDeliveryDay || expressDeliveryDay,
    parcelCollectDay: DCDeliveryDay,
  }
}
