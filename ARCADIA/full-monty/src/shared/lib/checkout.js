import {
  omit,
  pick,
  prop,
  pluck,
  compose,
  any,
  values,
  sum,
  pathOr,
} from 'ramda'
import { getPaymentMethodFromValue, isCard } from './checkout-utilities/utils'
import { formatForRadix } from './price'

const ADDRESS_FIELDS = [
  'address1',
  'address2',
  'city',
  'state',
  'country',
  'postcode',
]
const NAME_AND_PHONE_FIELDS = ['firstName', 'lastName', 'telephone', 'title']
const CREDIT_CARD_FIELDS = ['expiryYear', 'expiryMonth', 'cardNumber', 'type']

// For countries that do not require a postcode (i.e Ireland) we need to pass at the very least a truthy string to WCS in order for
// it to not kick up a fuss about validation. Hence adding a string of '0' at the very least when formatting the address
const formatAddress = (data) =>
  pick(ADDRESS_FIELDS, {
    ...data,
    state: data.county || data.state,
    postcode: data.postcode || '0',
  })
const formatNameAndPhone = (data) =>
  pick(NAME_AND_PHONE_FIELDS, {
    ...data,
    telephone: data.telephone.replace(/\D/g, ''),
  })
const formatCreditCard = (data) =>
  pick(CREDIT_CARD_FIELDS, { ...data, type: data.paymentType })
const extractFormData = compose(pluck('value'), prop('fields'))

export const giftCardCoversTotal = (giftCards, total) => {
  return (
    !!giftCards &&
    !!total &&
    giftCards.length > 0 &&
    (parseFloat(total) === 0 || parseInt(total, 10) === 0)
  )
}

export const isCheckoutProfile = ({
  deliveryDetails = {},
  billingDetails = {},
  creditCard = {},
}) => {
  const { cardNumberHash, type } = creditCard
  if (cardNumberHash) return true
  if (type === 'PYPAL') return true
  const deliveryAddress = deliveryDetails.addressDetailsId
  const billingAddress = billingDetails.addressDetailsId
  if (
    (deliveryAddress && deliveryAddress !== -1) ||
    (billingAddress && billingAddress !== -1)
  )
    return true
  return false
}

export const createOrderDeliveryOption = (orderSummary, yourAddress) => {
  const selectedDeliveryMethod = orderSummary.deliveryLocations
    .find(prop('selected'))
    .deliveryMethods.find(prop('selected'))

  // because of inconsistent data structure in ordersummary
  const { shipModeId, nominatedDate } =
    selectedDeliveryMethod.deliveryOptions &&
    selectedDeliveryMethod.deliveryOptions.length
      ? selectedDeliveryMethod.deliveryOptions.find(prop('selected'))
      : selectedDeliveryMethod

  const payload = {
    orderId: orderSummary.basket.orderId,
    shippingCountry:
      yourAddress.fields.country.value || orderSummary.shippingCountry,
    shipCode: selectedDeliveryMethod.shipCode,
    deliveryType: selectedDeliveryMethod.deliveryType,
    shipModeId,
    nominatedDate,
  }

  if (orderSummary.deliveryStoreCode) {
    payload.deliveryStoreCode = orderSummary.deliveryStoreCode
  }

  return payload
}

const createBaseOrder = ({
  orderSummary,
  billingCardDetails: cardDetailsForm,
  yourAddress,
  deliveryInstructions,
  orderCompletePath,
}) => {
  const billingCardDetails = extractFormData(cardDetailsForm)
  const {
    location: { protocol, host },
  } = window
  const giftCards = pathOr(null, ['giftCards'], orderSummary)
  const total = pathOr(null, ['basket', 'total'], orderSummary)

  return {
    smsMobileNumber: deliveryInstructions.fields.smsMobileNumber.value,
    remoteIpAddress: '127.0.0.1', // refer to the scrAPI documentation. (none of us know why this is needed)
    cardCvv: giftCardCoversTotal(giftCards, total)
      ? '0'
      : billingCardDetails.cvv || '0',
    orderDeliveryOption: createOrderDeliveryOption(orderSummary, yourAddress),
    deliveryInstructions:
      deliveryInstructions.fields.deliveryInstructions.value,
    returnUrl: `${protocol}//${host}/${orderCompletePath}?paymentMethod=${
      billingCardDetails.paymentType
    }`,
  }
}

const deliverySections = ({ yourAddress, yourDetails, orderSummary }) => {
  const deliveryFormFields = {
    ...yourDetails.fields,
    ...yourAddress.fields,
  }
  // If the user has modified the data we need to add the new data to the order
  // Because we are not "submitting" the delivery form data, we need to see if it has been changed in the form.
  const isDirty = any(prop('isDirty'), values(deliveryFormFields))
  const deliveryInfo = pluck('value', deliveryFormFields) // form data as: {field: value}

  return isDirty
    ? {
        deliveryAddress:
          orderSummary.storeDetails || formatAddress(deliveryInfo),
        deliveryNameAndPhone: formatNameAndPhone(deliveryInfo),
      }
    : {}
}

const billingSection = ({ billingAddress, billingDetails }) => {
  const billingFormFields = {
    ...billingAddress.fields,
    ...billingDetails.fields,
  }
  // If the user has modified the data we need to add the new data to the order
  // as with delivery data, we use the form data if the form fields has been changed.
  const isDirty = any(prop('isDirty'), values(billingFormFields))
  const billingInfo = pluck('value', billingFormFields)
  return isDirty
    ? {
        billingDetails: {
          address: formatAddress(billingInfo),
          nameAndPhone: formatNameAndPhone(billingInfo),
        },
      }
    : {}
}

const paymentSection = ({
  billingCardDetails,
  orderSummary,
  paymentMethods,
  user,
}) => {
  const {
    giftCards,
    basket: { total },
    cardNumberHash,
  } = orderSummary
  if (giftCardCoversTotal(giftCards, total)) return undefined
  // If the user has modified the data we need to add the new data to the order
  const isDirty = any(
    prop('isDirty'),
    values(omit(['cvv'], billingCardDetails.fields))
  )
  if (isDirty) {
    const cardInfo = pluck('value', billingCardDetails.fields)
    const paymentTypeValue = getPaymentMethodFromValue(
      cardInfo.paymentType,
      paymentMethods
    )
    if (!isCard(paymentTypeValue.value, paymentMethods)) {
      // TODO : check if Field (Year - Month) is today OR future
      cardInfo.expiryYear = (new Date().getFullYear() + 1).toString() // API requires this, but don't use it when paying with paypal
      cardInfo.expiryMonth = '1'
      cardInfo.cardNumber = '0'
      cardInfo.cvv = '0'
    }
    return { creditCard: formatCreditCard(cardInfo) }
  }

  return {
    // NOTE: if first order is store delivery, then there is no cardNumberHash in the order summary - ¯\_(ツ)_/¯
    cardNumberHash: cardNumberHash || user.creditCard.cardNumberHash,
  }
}

const savePaymentDetailsSection = (
  featureSavePaymentDetailsEnabled,
  savePaymentDetails
) => {
  return featureSavePaymentDetailsEnabled
    ? { save_details: savePaymentDetails }
    : {}
}

const isTempUser = (user) => /\.TEMP.*/.test(user.email)

const accountSection = ({ auth: { authentication }, credentials, user }) => {
  let userAuth = {}
  if (!authentication || isTempUser(user)) {
    if (user && user.exists && !isTempUser(user)) {
      userAuth = {
        accountLogin: {
          password: credentials.fields.password.value,
          email: credentials.fields.email.value,
        },
      }
    } else {
      userAuth = { accountCreate: pluck('value', credentials.fields) }
    }
  }
  return userAuth
}

export const createOrder = (arg) => {
  const {
    billingCardDetails,
    yourDetails,
    yourAddress,
    billingAddress,
    billingDetails,
    deliveryInstructions,
    orderSummary,
    credentials,
    auth,
    user,
    paymentMethods,
    featureSavePaymentDetailsEnabled,
    savePaymentDetails,
    orderCompletePath,
  } = arg
  return {
    ...createBaseOrder({
      billingCardDetails,
      orderSummary,
      yourAddress,
      deliveryInstructions,
      orderCompletePath,
    }),
    ...deliverySections({ yourAddress, yourDetails, orderSummary }),
    ...billingSection({ billingAddress, billingDetails }),
    ...paymentSection({
      billingCardDetails,
      orderSummary,
      paymentMethods,
      user,
    }),
    ...accountSection({ auth, credentials, user }),
    ...savePaymentDetailsSection(
      featureSavePaymentDetailsEnabled,
      savePaymentDetails
    ),
  }
}

/**
 * Calculate the shopping bag total
 * FIX: the 'total' calculated we receive from the backend is not always updated
 */
export const fixTotal = (subTotal, shippingCost = 0, discounts = []) => {
  // handle commas in european money
  const discountsConverted = discounts.map((x) => formatForRadix(x))
  return (
    parseFloat(formatForRadix(subTotal)) +
    parseFloat(formatForRadix(shippingCost)) -
    sum(discountsConverted.map(parseFloat))
  )
}
