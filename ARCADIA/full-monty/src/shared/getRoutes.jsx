import React from 'react'
import { Route, IndexRoute, Redirect } from 'react-router'

// selectors
import {
  isFeatureMyCheckoutDetailsEnabled,
  isFeatureWishlistEnabled,
  isFeaturePLPRedirectIfOneProductEnabled,
  isFeatureUnifiedLoginRegisterEnabled,
} from './selectors/featureSelectors'

// components
import Main from './components/containers/Main/Main'
import PlpContainer from './components/containers/PLP/PlpContainer'
import PdpContainer from './components/containers/PdpContainer/PdpContainer'
import BazaarVoiceReview from './components/common/BazaarVoice/BazaarVoiceReview/BazaarVoiceReview'
import SignIn from './components/containers/SignIn/SignIn'
import SignOut from './components/containers/SignOut/SignOut'
import MyAccount from './components/containers/MyAccount/MyAccount'
import MyAccountSubcategory from './components/containers/MyAccountSubcategory/MyAccountSubcategory'
import ChangePassword from './components/containers/ChangePassword/ChangePassword'
import MyCheckoutDetails from './components/containers/MyCheckoutDetails/MyCheckoutDetails'
// eslint-disable-next-line import/no-named-as-default
import ResetPassword from './components/containers/ResetPassword/ResetPassword'
import CustomerDetails from './components/containers/CustomerDetails/CustomerDetails'
import CustomerShortProfile from './components/containers/CustomerShortProfile/CustomerShortProfile'
import OrderHistoryList from './components/containers/OrderHistory/OrderHistoryList'
import ReturnHistoryList from './components/containers/ReturnHistory/ReturnHistoryList'
import OrderHistoryDetails from './components/containers/OrderHistoryDetails/OrderHistoryDetails'
import ReturnHistoryDetails from './components/containers/ReturnHistory/ReturnHistoryDetails'
import StoreLocator from './components/common/StoreLocator/StoreLocator'
import FixedHeightPage from './components/containers/FixedHeightPage/FixedHeightPage'
import Dressipi from './components/containers/Dressipi/Dressipi'
import ChangeShippingDestination from './components/containers/ChangeShippingDestination/ChangeShippingDestination'
import * as checkout from './components/containers/CheckoutV2'
import UI from './components/containers/UI/UI'
import CmsForm from './components/containers/CmsForm/CmsForm'
import Home from './components/containers/Home/Home'
import routeHandlers from './lib/routing/v1/state-binding'
import routeHandlersV2 from './lib/routing/v2/state-binding'
import CmsWrapper from './components/containers/CmsWrapper/CmsWrapper'
import SandBoxPage from './components/containers/SandBoxPage/SandBoxPage'
import EReceipt from './components/containers/EReceipt/EReceipt'
import WrappedWishlistPageContainer from './components/containers/Wishlist/WishlistPageContainer'
import LoginRegisterContainer from './components/containers/LoginRegisterContainer/LoginRegisterContainer'
import ForgotPassword from './components/containers/LoginRegister/ForgotPassword'
import RegisterSuccess from './components/containers/LoginRegister/RegisterSuccess'

const { checkAuthentication, searchRedirect } = routeHandlers
const {
  checkoutRedirect: checkoutRedirectV2,
  requiresNotAuth,
  requiresAuth,
  onEnterPayment,
} = routeHandlersV2

export default function getRoutes(context, { l }) {
  const { getState } = context
  const state = getState()

  /**
   * Encode URI but decode ['/', ':']
   * @param {string} path
   */
  const encode = (path) =>
    encodeURIComponent(path)
      .replace(/%2F/g, '/')
      .replace(/%3A/g, ':')

  /**
   * Add route for path, component and props, but if path is not encoded, add that route too
   * @param {string} path
   * @param {object} component
   * @param {object} props
   */
  const route = (path, component, props = {}) => {
    const routes = [
      <Route path={path} component={component} key={path} {...props} />,
    ]
    const encoded = encode(path)
    if (path !== encoded) {
      routes.push(
        <Route path={encoded} component={component} key={path} {...props} />
      )
    }
    return routes
  }

  /**
   * Add a redirect from both the path and the encoded path
   * @param {string} path
   * @param {string} to
   */
  const redirect = (path, to) => {
    const encoded = encode(path)
    const redirects = [<Redirect from={path} to={to} key={path} />]
    if (path !== encoded) {
      redirects.push(<Redirect from={encoded} to={to} key={path} />)
    }
    return redirects
  }

  /**
   * Return Array<Array<Redirect>> with redirect being `[localised category]/[path]** /home`
   * @param {Array<string>} paths
   */
  const redirectToStoreLocatorFrom = (...paths) =>
    paths.map((path) =>
      redirect(`/**/**/${l`category`}/${path}**/home`, '/store-locator')
    )

  const checkoutFlow = (context) => {
    const loginContainer = isFeatureUnifiedLoginRegisterEnabled(
      context.getState()
    )
      ? LoginRegisterContainer
      : checkout.LoginContainer
    return [
      <Route
        key="checkout"
        path="checkout"
        component={checkout.CheckoutContainer}
      >
        <IndexRoute onEnter={checkoutRedirectV2(context)} />
        <Route
          path="login"
          component={loginContainer}
          onEnter={requiresNotAuth(context)}
        />
        <Route
          path="delivery"
          component={checkout.DeliveryContainer}
          onEnter={requiresAuth(context)}
        />
        <Route
          path="delivery/collect-from-store"
          component={checkout.DeliveryCollectFromStoreContainer}
          onEnter={requiresAuth(context)}
        />
        <Route
          path="payment"
          component={checkout.PaymentContainer}
          onEnter={onEnterPayment(context)}
        />
        <Route
          path="delivery-payment"
          component={checkout.DeliveryPaymentContainer}
          onEnter={onEnterPayment(context)}
        />
      </Route>,
    ]
  }

  const wishlist = (state) => {
    const featureWishlistEnabled = isFeatureWishlistEnabled(state)
    return featureWishlistEnabled ? (
      <Route
        path="/wishlist"
        component={WrappedWishlistPageContainer}
        contentType="page"
      />
    ) : null
  }

  /* TODO: Create a URL strategy for handling localised CMS content */
  return (
    <Route name="main" component={Main} path="/">
      <IndexRoute cmsPageName="home" component={Home} cacheable />
      {redirectToStoreLocatorFrom(
        'store-finder',
        'store-locator',
        'find-a-store'
      )}
      {route(l`change-your-shipping-destination`, ChangeShippingDestination)}
      {/* TODO: Create a URL strategy for handling localised CMS content */}
      {/* Can be removed when t&c's are all republished and requests come back with seoUrls */}
      {route(
        `/**/**/${l`category`}/help-information**/${l`tcs`}**`,
        SandBoxPage,
        {
          cmsPageName: 'termsAndConditions',
          cacheable: true,
        }
      )}
      {route(`/lfw16`, CmsWrapper, {
        cmsPageName: 'lfw16',
        contentType: 'page',
      })}
      {route(`/cms/${l`tcs`}`, SandBoxPage, {
        cmsPageName: 'termsAndConditions',
        contentType: 'page',
      })}
      {route(
        `/**/**/${l`category`}/help-information**/style-adviser**`,
        Dressipi,
        { cacheable: true }
      )}
      {route(
        `/**/**/${l`category`}/help-information**/my-topshop-wardrobe**`,
        Dressipi,
        { cacheable: true }
      )}
      {route(
        `/**/**/${l`category`}/help-information**/${l`change-your-shipping-destination`}**`,
        ChangeShippingDestination,
        { cacheable: true }
      )}
      {route(
        `/**/**/${l`category`}/help-information**/:hygieneType-**`,
        SandBoxPage,
        { cacheable: true }
      )}
      {/* CMS content accessible from outside the application http://localhost:8080/en/tsuk/category/topshop-gift-card-247/home?TS=1466070708078 */}
      {route(
        `/**/**/${l`category`}(/**)(/**)/my-topshop-wardrobe**`,
        Dressipi,
        { cacheable: true }
      )}
      {route(`/**/**/${l`category`}(/**)(/**)style-adviser**`, Dressipi, {
        cacheable: true,
      })}
      {route(`/**/**/${l`category`}/**(/**)${l`/home`}`, SandBoxPage, {
        cacheable: true,
        contentType: 'page',
      })}
      {redirect(
        `/**/**/${l`category`}/your-details**/${l`sign-in-or-register`}**`,
        '/login'
      )}
      {redirect(
        `/**/**/${l`category`}/your-details**/${l`my-account`}**`,
        '/login'
      )}
      {route(
        `/**/**/${l`category`}/your-details**/${l`change-your-shipping-destination`}**`,
        ChangeShippingDestination
      )}
      {route(
        `/**/**/${l`category`}/your-details**/:hygieneType-**`,
        SandBoxPage,
        {
          cacheable: true,
        }
      )}
      <Route
        path="login"
        component={
          isFeatureUnifiedLoginRegisterEnabled(context.getState())
            ? LoginRegisterContainer
            : SignIn
        }
        onEnter={checkAuthentication(context)}
      />
      <Route path="logout" component={SignOut} />
      {checkoutFlow(context)}
      {route(`/**/**/${l`category`}/**(/**):param`, PlpContainer, {
        cacheable: true,
        redirectIfOneProduct: isFeaturePLPRedirectIfOneProductEnabled(
          context.getState()
        ),
      })}
      {route(`/**/**/${l`product`}/:identifier`, PdpContainer, {
        cacheable: true,
      })}
      {route(`/**/**/${l`product`}/**/:identifier`, PdpContainer, {
        cacheable: true,
      })}
      {route(`/**/**/${l`product`}/**/**/:identifier`, PdpContainer, {
        cacheable: true,
      })}
      {route(`/search`, PlpContainer, {
        cacheable: true,
        redirectIfOneProduct: isFeaturePLPRedirectIfOneProductEnabled(
          context.getState()
        ),
        onEnter: searchRedirect(context),
      })}
      <Route path="style-adviser" component={Dressipi} />
      <Route path="review/*" component={BazaarVoiceReview} />
      <Route
        path="reset-password"
        component={() => <ChangePassword resetPassword />}
      />
      <Route path="reset-password-link" component={ResetPassword} />
      <Route
        path="my-account"
        component={MyAccount}
        onEnter={checkAuthentication(context)}
      />
      <Route
        path="my-account"
        component={MyAccountSubcategory}
        onEnter={checkAuthentication(context)}
      >
        <Route path="my-password" component={ChangePassword} />
        <Route
          path="details(/edit)"
          component={
            isFeatureMyCheckoutDetailsEnabled(context.getState())
              ? MyCheckoutDetails
              : CustomerDetails
          }
        />
        <Route path="my-details" component={CustomerShortProfile} />
        <Route path="order-history" component={OrderHistoryList} />
        <Route path="order-history/:param" component={OrderHistoryDetails} />
        <Route path="return-history" component={ReturnHistoryList} />
        <Route
          path="return-history/:param/:id"
          component={ReturnHistoryDetails}
        />
        <Route path="e-receipts" component={EReceipt} />
      </Route>
      <Route path="/form/:cmsFormName" component={CmsForm} />
      <Route path="/store-locator" component={FixedHeightPage}>
        <IndexRoute component={StoreLocator} />
      </Route>
      <Route path="ui" component={UI} />
      <Route
        path="/size-guide/:cmsPageName"
        component={SandBoxPage}
        contentType="page"
      />
      {wishlist(state)}
      <Route path="/cms-preview/*" component={SandBoxPage} />
      <Route
        key="order-complete"
        path="order-complete"
        component={checkout.OrderComplete}
      />
      <Route
        path="/webapp/wcs/stores/servlet/TopCategoriesDisplay"
        component={Home}
      />
      <Route
        path="/webapp/wcs/stores/servlet/ProductDisplay"
        component={PdpContainer}
      />
      <Route
        path="/webapp/wcs/stores/servlet/ResetPasswordLink"
        component={ResetPassword}
      />
      <Route path="/forgot-password" component={ForgotPassword} />
      <Route path="/register-success" component={RegisterSuccess} />
      <Route
        path="/webapp/wcs/stores/servlet/CatalogNavigationSearchResultCmd"
        component={PlpContainer}
      />
      <Route
        path="*"
        cmsPageName="notFound"
        component={SandBoxPage}
        contentType="page"
      />
    </Route>
  )
}
