import { getPaymentMethodFormValidationSchema } from '../paymentFormValidationSchema'

describe('Payment Form Validation Schema functions', () => {
  describe(getPaymentMethodFormValidationSchema.name, () => {
    const state = {
      forms: {
        checkout: {
          billingCardDetails: {
            fields: {
              paymentType: {
                value: 'VISA',
              },
            },
          },
        },
      },
      paymentMethods: [
        { type: 'CARD', value: 'VISA' },
        { type: 'CARD', value: 'AMEX' },
      ],
    }
    it('returns validation schema for VISA', () => {
      const result = getPaymentMethodFormValidationSchema(state)
      expect(result).toHaveProperty('cardNumber')
      expect(result).toHaveProperty('expiryMonth')
      expect(
        result.cardNumber(
          { cardNumber: '1234123412341234' },
          'cardNumber',
          (v) => v
        )
      ).toBeNull()
      expect(
        result.expiryMonth({ expiryMonth: 10, expiryYear: 2076 })
      ).toBeNull()
    })
    it('returns error message when card number is not 16 digits', () => {
      const result = getPaymentMethodFormValidationSchema(state)
      expect(
        result.cardNumber({ cardNumber: '1234' }, 'cardNumber', (v) => v)
      ).toBe('A 16 digit card number is required')
    })
    it('returns error message when expiryMonth is expired', () => {
      const result = getPaymentMethodFormValidationSchema(state)
      expect(result.expiryMonth({ expiryMonth: 10, expiryYear: 2016 })).toBe(
        'Please select a valid expiry date'
      )
    })
    it('returns undefined when type card does not exists', () => {
      const state = {
        forms: {
          checkout: {
            billingCardDetails: {
              fields: {
                paymentType: {
                  value: 'NOEXISTS',
                },
              },
            },
          },
        },
      }
      const result = getPaymentMethodFormValidationSchema(state)
      expect(result).toEqual({})
    })
  })
})
