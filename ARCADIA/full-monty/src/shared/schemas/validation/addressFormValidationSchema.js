import {
  regexValidate,
  requiredCurried,
  countryValidate,
} from '../../lib/validator/validators'

export const regionalPhoneValidation = (country) => {
  switch (country) {
    case 'United Kingdom':
      return ['ukPhoneNumber']
    case 'United States':
      return ['usPhoneNumber']
    default:
      return ['numbersOnly']
  }
}

export const getYourDetailsSchema = (country) => {
  return {
    title: 'required',
    firstName: ['noEmoji', 'required'],
    lastName: ['noEmoji', 'required'],
    telephone: ['required', ...regionalPhoneValidation(country), 'noEmoji'],
  }
}

const postCodeValidation = (rules) => {
  return rules && !!rules.postcodeRequired && !!rules.pattern
    ? [
        'required',
        regexValidate('Please enter a valid post code', rules.pattern),
      ]
    : []
}

export const getYourAddressSchema = (rules, countriesByAddressType) => ({
  address1: ['required', 'noEmoji'],
  address2: 'noEmoji',
  postcode: [...postCodeValidation(rules), 'noEmoji'],
  city: ['required', 'noEmoji'],
  country: ['required', countryValidate(countriesByAddressType)],
})

export const getFindAddressSchema = (rules, options = {}) => {
  const { hasFoundAddresses, hasSelectedAddress } = options
  return {
    postcode: [...postCodeValidation(rules), 'noEmoji'],
    houseNumber: 'noEmoji',
    findAddress: hasFoundAddresses
      ? []
      : requiredCurried('Please click on FIND ADDRESS'),
    selectAddress: hasSelectedAddress
      ? []
      : requiredCurried('Please select an address'),
  }
}
