// constants
import paymentRules from '../../constants/paymentValidationRules'

// lib
import { cardExpiry, hasLength } from '../../lib/validator/validators'

// selectors
import { getSelectedPaymentMethodValue } from '../../selectors/common/accountSelectors'
import { getPaymentMethodTypeByValue } from '../../selectors/paymentMethodSelectors'

export const getPaymentMethodFormValidationSchema = (state) => {
  const value = getSelectedPaymentMethodValue(state)
  const type = getPaymentMethodTypeByValue(state, value, 'OTHER')
  return type !== 'CARD'
    ? {}
    : {
        cardNumber: paymentRules[value]
          ? hasLength(
              paymentRules[value].cardNumber.message,
              paymentRules[value].cardNumber.length
            )
          : '',
        expiryMonth: cardExpiry(
          `Please select a valid expiry date`,
          new Date()
        ),
      }
}
