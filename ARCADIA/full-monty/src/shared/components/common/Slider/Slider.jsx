import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'

const addEvent = (node, handler, eventName) =>
  node.addEventListener(eventName, handler)

const removeEvent = (node, handler, eventName) =>
  node.removeEventListener(eventName, handler)

@connect(({ viewport, refinements }) => ({
  viewportWidth: viewport.width,
  appliedRefinements: refinements.appliedOptions,
}))
export default class Slider extends Component {
  static propTypes = {
    minValue: PropTypes.number.isRequired,
    maxValue: PropTypes.number.isRequired,
    onChangeFinished: PropTypes.func,
  }

  static contextTypes = {
    p: PropTypes.func,
  }

  constructor(props) {
    super(props)

    this.state = {
      activeHandle: null,
      minHandle: {
        left: 0,
        value: props.minValue,
      },
      maxHandle: {
        left: 0,
        value: props.maxValue,
      },
    }

    this.slider = null
    this.iconWidth = 0
    this.height = 0
    this.width = 0
    this.left = 0
    this.offsetAllowance = 0
    this.handlerStartOffset = 0
    this.onPressEventHandlers = {
      minHandle: this.getOnPressHandlers('minHandle'),
      maxHandle: this.getOnPressHandlers('maxHandle'),
    }
    this.eventNames = {
      moving: ['touchmove', 'mousemove'],
      released: ['touchend', 'touchcancel', 'mouseup'],
    }
  }

  componentWillReceiveProps(nextProps) {
    const { left, width } = this.slider.getBoundingClientRect()
    const { minValue, maxValue } = this.props
    const { minHandle, maxHandle } = this.state
    const hasChanges =
      minValue !== nextProps.minValue ||
      maxValue !== nextProps.maxValue ||
      this.width !== width ||
      this.left !== left

    if (hasChanges) {
      const { appliedRefinements } = this.props
      let min = nextProps.minValue
      let max = nextProps.maxValue
      if (appliedRefinements && appliedRefinements.price) {
        min = Math.max(nextProps.minValue, minHandle.value)
        max = Math.min(nextProps.maxValue, maxHandle.value)
      }

      this.width = width
      this.left = left
      const newMinMax = {
        minValue: nextProps.minValue,
        maxValue: nextProps.maxValue,
      }
      this.setState({
        minHandle: {
          value: min,
          left: this.getHandleLeft('minHandle', min, newMinMax),
        },
        maxHandle: {
          value: max,
          left: this.getHandleLeft('maxHandle', max, newMinMax),
        },
      })
    }
  }

  onSliderMount = (slider) => {
    if (slider) {
      const { left, height, width } = slider.getBoundingClientRect()
      const { maxHandle } = this.state

      this.slider = slider
      this.left = left
      this.width = width
      this.height = height
      this.offsetAllowance = height / 2 + 100
      this.iconWidth = this.icon.getBoundingClientRect().width

      this.setState({
        maxHandle: {
          value: maxHandle.value,
          left: this.getHandleLeft('maxHandle', maxHandle.value),
        },
      })
    }

    return this
  }

  onHandlePress = ({ currentTarget, ...event }, activeHandle) => {
    const pageX = this.getHandlerPosition(event).pageX
    const left = currentTarget
      .getElementsByClassName('Slider-icon')[0]
      .getBoundingClientRect().left
    this.handlerStartOffset = pageX - left
    this.setState({ activeHandle })
    this.eventNames.moving.forEach(
      addEvent.bind(null, document, this.onHandleMove)
    )
    this.eventNames.released.forEach(
      addEvent.bind(null, document, this.onHandleRelease)
    )

    return this
  }

  onHandleMove = (event) => {
    const { activeHandle } = this.state
    const { pageX } = this.getHandlerPosition(event)
    const handleLeft = pageX - this.handlerStartOffset - this.left
    const value = this.getHandleValue(activeHandle, handleLeft)
    const left = this.getHandleLeft(activeHandle, value)
    this.setState({
      [activeHandle]: { left, value },
    })

    return this
  }

  onHandleRelease = () => {
    const { activeHandle, minHandle, maxHandle } = this.state

    if (activeHandle) {
      this.setState({ activeHandle: null })
      this.props.onChangeFinished(minHandle.value, maxHandle.value)
    }
    this.eventNames.moving.forEach(
      removeEvent.bind(null, document, this.onHandleMove)
    )
    this.eventNames.released.forEach(
      removeEvent.bind(null, document, this.onHandleRelease)
    )

    return this
  }

  getAvailableWidth = () => this.width - this.iconWidth * 2

  getHandleValue = (handle, left) => {
    const { minHandle, maxHandle } = this.state
    const { minValue, maxValue } = this.props

    const isMinHandle = handle === 'minHandle'
    const minLeft = isMinHandle ? 0 : minHandle.left + this.iconWidth
    const maxLeft = (isMinHandle ? maxHandle.left : this.width) - this.iconWidth
    const inRangeLeft =
      Math.min(maxLeft, Math.max(minLeft, left)) +
      (isMinHandle ? 0 : -this.iconWidth)
    const percentage = inRangeLeft / this.getAvailableWidth()
    const exactValue = percentage * (maxValue - minValue) + minValue

    return Math.round(exactValue)
  }

  getHandleLeft = (handle, value, newMinMax) => {
    let { minValue, maxValue } = this.props
    if (newMinMax) {
      minValue = newMinMax.minValue
      maxValue = newMinMax.maxValue
    }
    const percentage = (value - minValue) / (maxValue - minValue)

    return (
      percentage * this.getAvailableWidth() +
      (handle === 'minHandle' ? 0 : this.iconWidth)
    )
  }

  getHandlerPosition = ({ changedTouches, pageX, pageY }) =>
    changedTouches ? changedTouches[0] : { pageX, pageY }

  getOnPressHandlers = (handle) => ({
    onTouchStart: (event) => this.onHandlePress(event, handle),
    onMouseDown: (event) => this.onHandlePress(event, handle),
  })

  renderHandle = (handle) => (
    <button
      {...this.onPressEventHandlers[handle]}
      ref={(element) => {
        this[handle] = element
      }}
      className={`Slider-handle Slider-handle--${handle} ${
        this.state.activeHandle === handle ? 'is-active' : ''
      }`}
      style={{
        transform: `translateX(${this.state[handle].left}px)`,
      }}
    >
      <span
        ref={(icon) => {
          this.icon = icon
        }}
        className="Slider-icon"
      />
    </button>
  )

  renderLabel = (handle) => (
    <span className={`Slider-label Slider-label--${handle}`}>
      {this.context.p(this.state[handle].value.toFixed(2))}
    </span>
  )

  render = () => (
    <div ref={this.onSliderMount} className="Slider">
      {this.renderLabel('minHandle')}
      {this.renderLabel('maxHandle')}
      {this.renderHandle('minHandle')}
      {this.renderHandle('maxHandle')}
      <div
        className="Slider-bar"
        style={{
          paddingLeft: `${this.state.minHandle.left + this.iconWidth / 2}px`,
          paddingRight: `${this.width -
            this.state.maxHandle.left -
            this.iconWidth / 2}px`,
        }}
      />
    </div>
  )
}
