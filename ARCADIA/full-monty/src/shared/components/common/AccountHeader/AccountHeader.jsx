import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Link } from 'react-router'

export default class AccountHeader extends Component {
  static propTypes = {
    label: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    onLinkClick: PropTypes.func,
    classNames: PropTypes.string,
  }

  render() {
    const { link, label, title, classNames, onLinkClick } = this.props
    const classes = classNames
      ? `AccountHeader-row ${classNames}`
      : 'AccountHeader-row'

    return (
      <section className="AccountHeader">
        <div className="AccountHeader-row">
          <Link to={link} onClick={onLinkClick} className="NoLink">
            <p className="AccountHeader-back">{label}</p>
          </Link>
        </div>
        <div className={classes}>
          <h1 className="AccountHeader-title">{title}</h1>
        </div>
      </section>
    )
  }
}
