import testComponentHelper from 'test/unit/helpers/test-component'
import AccountHeader from '../AccountHeader'
import { Link } from 'react-router'

describe('<AccountHeader />', () => {
  const renderComponent = testComponentHelper(AccountHeader)
  const initialProps = {
    link: '/example',
    label: 'Back to My Account',
    title: 'My title',
  }

  describe('@renders', () => {
    it('in default state. There is a header with a left arrow that takes the user back to My Account', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })
    it('should render with custom classNames', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        classNames: 'classyMcClassFace',
      })
      expect(wrapper.find('.classyMcClassFace').isEmpty()).toBe(false)
    })
  })

  describe('@function as prop', () => {
    it('when onLinkClick is defined it should be triggered when Link clicked', () => {
      const props = {
        ...initialProps,
        onLinkClick: jest.fn(),
      }
      const { wrapper } = renderComponent(props)
      const link = wrapper.find(Link)
      link.prop('onClick')()
      expect(props.onLinkClick).toHaveBeenCalledTimes(1)
    })
    it('when onLinkClick is NOT defined it should NOT be triggered when Link clicked', () => {
      const { wrapper } = renderComponent(initialProps)
      const link = wrapper.find(Link)
      expect(link.prop('onClick')).toBeUndefined()
    })
  })
})
