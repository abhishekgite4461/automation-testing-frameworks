import PropTypes from 'prop-types'
import { compose } from 'ramda'
import React from 'react'
import { connect } from 'react-redux'

import {
  decorator as productReferrals,
  REFERRER_TYPES,
} from '../../../lib/analytics/product-referrals-helper'
import { brandCode } from '../../../../server/api/mapping/constants/product'

// actions
import { clickRecommendation } from '../../../actions/components/RecommendationsActions'

// components
import ProductCarousel from '../ProductCarousel/ProductCarousel'

import {
  sendAnalyticsProductClickEvent,
  GTM_LIST_TYPES,
} from '../../../analytics'

const Recommendations = (
  {
    recommendations,
    registerReferral,
    clickRecommendation,
    brand,
    sendAnalyticsProductClickEvent,
  },
  { l }
) => {
  const content = () => {
    switch (brand) {
      case brandCode.br:
        return `Recommended For You`
      case brandCode.dp:
      case brandCode.tm:
        return l`you may also like`
      case brandCode.ms:
        return l`you might also like`
      case brandCode.ev:
      case brandCode.ts:
      case brandCode.wl:
      default:
        return l`Why not try?`
    }
  }

  const carouselProducts = recommendations.map(
    ({ productId, title: name, img, amplienceUrl, prices }) => {
      const currency = Object.keys(prices)[0]

      return {
        productId,
        name,
        imageUrl: img,
        amplienceUrl,
        unitPrice: prices[currency].unitPrice.toString(),
        salePrice:
          prices[currency].salePrice && prices[currency].salePrice.toString(),
      }
    }
  )

  const getRecommendationByProductId = (productId) => {
    return recommendations.find(
      (recommendation) => recommendation.productId === productId
    )
  }

  const logAnalyticsClickEvent = (productId) => {
    const { title: name, position, prices } = getRecommendationByProductId(
      productId
    )
    const currency = Object.keys(prices)[0]
    const price = prices[currency].salePrice
      ? prices[currency].salePrice.toString()
      : prices[currency].unitPrice.toString()

    sendAnalyticsProductClickEvent({
      listType: GTM_LIST_TYPES.PDP_RECOMMENDED_PRODUCTS,
      name,
      id: productId,
      price,
      position,
    })
  }

  const productLinkClickHandler = (productId) => {
    clickRecommendation(productId)
    registerReferral(productId)
    logAnalyticsClickEvent(productId)
  }

  return carouselProducts.length ? (
    <div className="Recommendations">
      <h3 className="Recommendations-header">{content()}</h3>
      <ProductCarousel
        isImageFallbackEnabled
        products={carouselProducts}
        onProductLinkClick={productLinkClickHandler}
        hideProductName
        hideQuickView={false}
      />
    </div>
  ) : null
}

Recommendations.propTypes = {
  recommendations: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      url: PropTypes.string,
      img: PropTypes.string,
      prices: PropTypes.objectOf(
        PropTypes.shape({
          unitPrice: PropTypes.number,
          salePrice: PropTypes.number,
        })
      ),
    })
  ),
  registerReferral: PropTypes.func,
  brand: PropTypes.string,
  // actions
  clickRecommendation: PropTypes.func.isRequired,
}

Recommendations.defaultProps = {
  recommendations: [],
  registerReferral: () => {},
}

Recommendations.contextTypes = {
  l: PropTypes.func,
}

export default compose(
  productReferrals(REFERRER_TYPES.RECOMMENDED),
  connect(
    (state) => ({
      recommendations: state.recommendations.recommendations,
      brand: state.config.brandName,
    }),
    { clickRecommendation, sendAnalyticsProductClickEvent }
  )
)(Recommendations)

export { Recommendations as WrappedRecommendations }
