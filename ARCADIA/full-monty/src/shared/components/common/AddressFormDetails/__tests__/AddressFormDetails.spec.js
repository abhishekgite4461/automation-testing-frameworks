import testComponentHelper from 'test/unit/helpers/test-component'
import AddressFormDetails from '../AddressFormDetails'

describe('<AddressFormDetails />', () => {
  const renderComponent = testComponentHelper(
    AddressFormDetails.WrappedComponent
  )
  const initialProps = {
    addressType: 'delivery',
    form: {
      fields: {
        title: {
          value: 'Mr',
        },
        firstName: {
          value: 'John',
        },
        lastName: {
          value: 'Smith',
        },
        telephone: {
          value: '07123123123',
        },
      },
    },
    formName: 'yourDetails',
    titles: ['Mr', 'Mrs', 'Ms', 'Miss', 'Dr'],
    setFormField: jest.fn(),
    setAndValidateFormField: jest.fn(),
    touchedFormField: jest.fn(),
    validateForm: jest.fn(),
    clearFormErrors: jest.fn(),
    validationSchema: {
      title: 'required',
    },
  }

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })

    it('with a label', () => {
      expect(
        renderComponent({
          ...initialProps,
          label: 'Your Delivery Details',
        }).getTree()
      ).toMatchSnapshot()
    })

    it('with a type', () => {
      expect(
        renderComponent({
          ...initialProps,
          type: 'storeDelivery',
        }).getTree()
      ).toMatchSnapshot()
    })

    it('with errors', () => {
      expect(
        renderComponent({
          ...initialProps,
          errors: {
            firstName: 'An email address is required.',
            lastName: 'A password is required.',
          },
        }).getTree()
      ).toMatchSnapshot()
    })

    it('with a h4', () => {
      expect(
        renderComponent({
          ...initialProps,
          h4: true,
        }).getTree()
      ).toMatchSnapshot()
    })
  })

  describe('@events', () => {
    beforeEach(() => {
      jest.clearAllMocks()
    })

    describe('title Select', () => {
      it('should call setAndValidateFormField on change', () => {
        const event = {
          target: {
            value: 'Dr',
          },
        }
        const { wrapper, instance } = renderComponent(initialProps)
        wrapper.find('Connect(Select) [name="title"]').prop('onChange')(event)
        expect(instance.props.setAndValidateFormField).toHaveBeenLastCalledWith(
          initialProps.formName,
          'title',
          'Dr',
          initialProps.validationSchema.title
        )
      })

      it('should send analytics `event131` event when focus leaves', () => {
        const sendEventAnalyticsMock = jest.fn()
        const { wrapper } = renderComponent({
          ...initialProps,
          sendEventAnalytics: sendEventAnalyticsMock,
        })
        wrapper.find('Connect(Select) [name="title"]').prop('onChange')({
          target: {},
        })
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event131',
        })
      })
    })

    describe('firstName Input', () => {
      it('should call setFormField when prop setField is called', () => {
        const { wrapper, instance } = renderComponent({
          ...initialProps,
          validationSchema: {
            firstName: ['required'],
          },
        })
        wrapper.find('Connect(Input) [name="firstName"]').prop('setField')(
          'firstName'
        )({ target: { value: 'Foo' } })
        expect(instance.props.setAndValidateFormField).toHaveBeenLastCalledWith(
          initialProps.formName,
          'firstName',
          'Foo',
          ['required']
        )
      })

      it('should call touchedFormField when prop touchedField is called', () => {
        const { wrapper, instance } = renderComponent(initialProps)
        wrapper.find('Connect(Input) [name="firstName"]').prop('touchedField')(
          'firstName'
        )()
        expect(instance.props.touchedFormField).toHaveBeenLastCalledWith(
          initialProps.formName,
          'firstName'
        )
      })

      it('should send analytics `event132` event when focus leaves', () => {
        const sendEventAnalyticsMock = jest.fn()
        const { wrapper } = renderComponent({
          ...initialProps,
          sendEventAnalytics: sendEventAnalyticsMock,
        })
        wrapper.find('Connect(Input) [name="firstName"]').prop('touchedField')(
          'firstName'
        )()
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event132',
        })
      })
    })

    describe('lastName Input', () => {
      it('should call setFormField when prop setField is called', () => {
        const { wrapper, instance } = renderComponent({
          ...initialProps,
          validationSchema: {
            lastName: ['required'],
          },
        })
        wrapper.find('Connect(Input) [name="lastName"]').prop('setField')(
          'lastName'
        )({ target: { value: 'Bar' } })
        expect(instance.props.setAndValidateFormField).toHaveBeenLastCalledWith(
          initialProps.formName,
          'lastName',
          'Bar',
          ['required']
        )
      })

      it('should call touchedFormField when prop touchedField is called', () => {
        const { wrapper, instance } = renderComponent(initialProps)
        wrapper.find('Connect(Input) [name="lastName"]').prop('touchedField')(
          'lastName'
        )()
        expect(instance.props.touchedFormField).toHaveBeenLastCalledWith(
          initialProps.formName,
          'lastName'
        )
      })

      it('should send analytics `event133` event when focus leaves', () => {
        const sendEventAnalyticsMock = jest.fn()
        const { wrapper } = renderComponent({
          ...initialProps,
          sendEventAnalytics: sendEventAnalyticsMock,
        })
        wrapper.find('Connect(Input) [name="lastName"]').prop('touchedField')(
          'lastName'
        )()
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event133',
        })
      })
    })

    describe('telephone Input', () => {
      it('should call setFormField when prop setField is called', () => {
        const { wrapper, instance } = renderComponent({
          ...initialProps,
          validationSchema: {
            telephone: ['required'],
          },
        })
        wrapper.find('Connect(Input) [name="telephone"]').prop('setField')(
          'telephone'
        )({ target: { value: '018118181' } })
        expect(instance.props.setAndValidateFormField).toHaveBeenLastCalledWith(
          initialProps.formName,
          'telephone',
          '018118181',
          ['required']
        )
      })

      it('should call touchedFormField when prop touchedField is called', () => {
        const { wrapper, instance } = renderComponent(initialProps)
        wrapper.find('Connect(Input) [name="telephone"]').prop('touchedField')(
          'telephone'
        )()
        expect(instance.props.touchedFormField).toHaveBeenLastCalledWith(
          initialProps.formName,
          'telephone'
        )
      })

      it('should send analytics `event134` event when focus leaves', () => {
        const sendEventAnalyticsMock = jest.fn()
        const { wrapper } = renderComponent({
          ...initialProps,
          sendEventAnalytics: sendEventAnalyticsMock,
        })
        wrapper.find('Connect(Input) [name="telephone"]').prop('touchedField')(
          'telephone'
        )()
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event134',
        })
      })
    })
  })
})
