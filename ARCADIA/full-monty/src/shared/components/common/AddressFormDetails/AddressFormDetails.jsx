import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { path } from 'ramda'

// lib
import { eventBasedAnalytics } from '../../../lib/analytics/analytics'

// actions
import {
  setFormField,
  setAndValidateFormField,
  touchedFormField,
  validateForm,
  clearFormErrors,
} from '../../../actions/common/formActions'

// schemas
import { getYourDetailsSchema } from '../../../schemas/validation/addressFormValidationSchema'

// components
import Input from '../../common/FormComponents/Input/Input'
import Select from '../../common/FormComponents/Select/Select'

export const mapStateToProps = (
  state,
  { addressType, country, formNames, getAddressFormFromState }
) => {
  const formName = formNames.details
  const form = getAddressFormFromState(addressType, formName, state)
  return {
    form,
    titles: state.siteOptions.titles,
    formName,
    errors: form.errors,
    validationSchema: getYourDetailsSchema(country),
    // NOTE: rather hacky way of determining whether the schemas have updated (as can't do a `===` on them)
    // think about using something like https://github.com/reactjs/reselect
    schemaHash: [country].join(':'),
  }
}

export const mapDispatchToProps = {
  setFormField,
  setAndValidateFormField,
  touchedFormField,
  validateForm,
  clearFormErrors,
}

class AddressFormDetails extends Component {
  static propTypes = {
    addressType: PropTypes.oneOf(['delivery', 'billing']).isRequired,
    autofocus: PropTypes.bool,
    label: PropTypes.string,
    titleHidden: PropTypes.bool,
    h4: PropTypes.bool,
    isDesktopMulticolumStyle: PropTypes.bool,
    form: PropTypes.object.isRequired,
    formName: PropTypes.string.isRequired,
    titles: PropTypes.array.isRequired,
    errors: PropTypes.object,
    validationSchema: PropTypes.object,
    events: PropTypes.objectOf(PropTypes.string),
    // functions
    setAndValidateFormField: PropTypes.func.isRequired,
    touchedFormField: PropTypes.func.isRequired,
    validateForm: PropTypes.func.isRequired,
    clearFormErrors: PropTypes.func.isRequired,
    sendEventAnalytics: PropTypes.func,
  }

  static defaultProps = {
    autofocus: false,
    label: '',
    titlesHidden: false,
    h4: false,
    isDesktopMulticolumStyle: false,
    errors: {},
    validationSchema: {},
    events: {
      title: 'event131',
      firstName: 'event132',
      lastName: 'event133',
      telephone: 'event134',
    },
    sendEventAnalytics: eventBasedAnalytics,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentDidMount() {
    this.validateDetails()
  }

  componentDidUpdate(prevProps) {
    if (prevProps.schemaHash !== this.props.schemaHash) {
      this.validateDetails()
    }
  }

  componentWillUnmount() {
    const { formName, clearFormErrors } = this.props
    clearFormErrors(formName)
  }

  setTitleField = ({ target: { value: title } = {} } = {}) => {
    const {
      events,
      formName,
      setAndValidateFormField,
      sendEventAnalytics,
      validationSchema,
    } = this.props
    setAndValidateFormField(formName, 'title', title, validationSchema.title)
    sendEventAnalytics({
      events: events.title,
    })
  }

  setAndValidateDetailsField = (fieldName) => ({
    target: { value } = {},
  } = {}) => {
    const { formName, validationSchema, setAndValidateFormField } = this.props
    setAndValidateFormField(
      formName,
      fieldName,
      value,
      validationSchema[fieldName]
    )
  }

  validateDetails() {
    const { formName, validationSchema, validateForm } = this.props
    return validateForm(formName, validationSchema)
  }

  touchDetailsField = (fieldName) => () => {
    const {
      events,
      formName,
      touchedFormField,
      sendEventAnalytics,
    } = this.props
    if (fieldName in events) {
      sendEventAnalytics({
        events: events[fieldName],
      })
    }
    touchedFormField(formName, fieldName)
  }

  render() {
    const { l } = this.context
    const {
      autofocus,
      form,
      label,
      titles,
      errors,
      addressType,
      titleHidden,
      isDesktopMulticolumStyle,
      h4,
    } = this.props
    const isIOS =
      process.browser &&
      !!(
        navigator.userAgent.match(/iPhone/i) ||
        navigator.userAgent.match(/iP[ao]d/i)
      )

    return (
      <section
        className={`AddressFormDetails AddressFormDetails--${addressType} ${
          isDesktopMulticolumStyle ? 'AddressFormDetails--multiColumn' : ''
        }`}
        aria-label={`${addressType} Details`}
      >
        {!titleHidden && (
          <header className="AddressFormDetails-header">
            {h4 ? (
              <h4 className="AddressFormDetails-headerText">{label}</h4>
            ) : (
              <h3 className="AddressFormDetails-headerText">{label}</h3>
            )}
          </header>
        )}
        <div className="AddressFormDetails-row AddressFormDetails-titleRow">
          <Select
            autofocus={autofocus && !isIOS}
            className="AddressFormDetails-title"
            label={l`Title`}
            disabled={form.success}
            name="title"
            value={path(['fields', 'title', 'value'], form)}
            firstDisabled={l`Please select`}
            onChange={this.setTitleField}
            options={titles}
            isRequired
            errors={errors}
            field={path(['fields', 'title'], form)}
          />
          <div className="AddressFormDetails-col">&nbsp;</div>
        </div>
        <div className="AddressFormDetails-row">
          <Input
            className="AddressFormDetails-firstName"
            isDisabled={form.success}
            field={path(['fields', 'firstName'], form)}
            name="firstName"
            errors={errors}
            label={l`First Name`}
            placeholder={l`First Name`}
            setField={this.setAndValidateDetailsField}
            touchedField={this.touchDetailsField}
            isRequired
          />
          <Input
            isDisabled={form.success}
            className="AddressFormDetails-lastName"
            field={path(['fields', 'lastName'], form)}
            name="lastName"
            errors={errors}
            label={l`Surname`}
            placeholder={l`Surname`}
            setField={this.setAndValidateDetailsField}
            touchedField={this.touchDetailsField}
            isRequired
          />
        </div>
        <div className="AddressFormDetails-row AddressFormDetails-telephoneRow">
          <Input
            className="AddressFormDetails-telephone"
            isDisabled={form.success}
            field={path(['fields', 'telephone'], form)}
            name="telephone"
            type="tel"
            errors={errors}
            label={l`Primary Phone Number`}
            placeholder={l`07123 123123`}
            setField={this.setAndValidateDetailsField}
            touchedField={this.touchDetailsField}
            isRequired
          />
          <div className="AddressFormDetails-col">&nbsp;</div>
        </div>
      </section>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddressFormDetails)

export { AddressFormDetails as WrappedAddressFormDetails }
