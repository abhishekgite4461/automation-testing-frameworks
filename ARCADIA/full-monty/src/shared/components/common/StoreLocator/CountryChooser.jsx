import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { path } from 'ramda'

import storeLocatorConsts from '../../../constants/storeLocator'

import Select from '../FormComponents/Select/Select'
import { selectCountry } from '../../../actions/components/StoreLocatorActions'

@connect(
  (state) => ({
    countries: state.storeLocator.countries,
    isMobile: state.viewport.media === 'mobile',
    selectedCountry: state.storeLocator.selectedCountry,
  }),
  { selectCountry }
)
export default class CountryChooser extends Component {
  static propTypes = {
    selectCountry: PropTypes.func.isRequired,
    className: PropTypes.string,
    countries: PropTypes.array,
    isMobile: PropTypes.bool,
    name: PropTypes.string,
    selectedCountry: PropTypes.string,
    isLandingPage: PropTypes.bool,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  static defaultProps = {
    className: '',
    countries: [],
    name: 'CountrySelect',
  }

  onCountryChange = (event) => {
    const { selectCountry, selectedCountry } = this.props
    const value = path(['target', 'value'], event)
    if (selectedCountry !== value) {
      selectCountry(value)
    }
  }

  createOptions = (countries, label, defaultCountry) => {
    return [
      { label, disabled: true },
      { label: defaultCountry, value: defaultCountry },
    ].concat(countries.map((country) => ({ value: country, label: country })))
  }

  render() {
    const { l } = this.context
    const label = l`Choose country`
    const {
      className,
      countries,
      isMobile,
      name,
      selectedCountry = label,
      isLandingPage,
    } = this.props
    const { defaultCountry } = storeLocatorConsts

    return (
      <Select
        className={`CountryChooser ${
          isMobile ? 'Select--link' : ''
        } ${className}`}
        name={name}
        options={this.createOptions(countries, label, defaultCountry)}
        onChange={this.onCountryChange}
        label={isLandingPage ? l`Store Locator` : l`I'm looking for a store in`}
        hideLabel={isLandingPage}
        value={selectedCountry}
      />
    )
  }
}
