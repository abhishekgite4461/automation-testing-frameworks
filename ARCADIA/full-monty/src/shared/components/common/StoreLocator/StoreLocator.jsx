import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { equals, filter, isEmpty, pathOr } from 'ramda'
import animate from 'amator'
import Helmet from 'react-helmet'

import { closeModal, showModal } from '../../../actions/common/modalActions'
import { selectDeliveryStore } from '../../../actions/common/checkoutActions'
import {
  clearFilters,
  collapseMap,
  getStores,
  selectStore,
  deselectStore,
  resetStoreLocator,
  resizeMap,
  setMarkers,
  storeSearch,
} from '../../../actions/components/StoreLocatorActions'
import { searchStores } from '../../../actions/components/UserLocatorActions'

import FeatureCheck from '../../common/FeatureCheck/FeatureCheck'
import Loader from '../../common/Loader/Loader'
import UserLocator from '../../common/UserLocator/UserLocator'
import UserLocatorInput from '../../common/UserLocatorInput/UserLocatorInput'

import GoogleMap from './GoogleMap'
import getFilterLabel from './get-filter-label'
import Store from './Store'
import StoreLocatorFilters from './StoreLocatorFilters'
import analyticsDecorator from '../../../../client/lib/analytics/analytics-decorator'
import { GTM_CATEGORY } from '../../../../shared/analytics'

@analyticsDecorator(GTM_CATEGORY.STORE_LOCATOR, { isAsync: true })
@connect(
  (state) => ({
    brandName: state.config.brandName,
    filters: state.storeLocator.filters,
    isStoresLoading: state.storeLocator.loading,
    isHeaderEnabled:
      state.features.status.FEATURE_STORE_FINDER_HEADER_WITH_COUNTRY_SELECTOR,
    mapExpanded: state.storeLocator.mapExpanded,
    modalOpen: state.modal.open,
    selectedStoreIndex: state.storeLocator.selectedStoreIndex,
    stores: state.storeLocator.stores,
    isMobile: state.viewport.media === 'mobile',
    CFSI: state.features.status.FEATURE_CFSI,
  }),
  {
    collapseMap,
    getStores,
    selectStore,
    deselectStore,
    resizeMap,
    clearFilters,
    resetStoreLocator,
    setMarkers,
    closeModal,
    showModal,
    selectDeliveryStore,
    storeSearch,
    searchStores,
  }
)
export default class StoreLocator extends Component {
  static propTypes = {
    clearFilters: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    collapseMap: PropTypes.func.isRequired,
    deselectStore: PropTypes.func.isRequired,
    resetStoreLocator: PropTypes.func.isRequired,
    resizeMap: PropTypes.func.isRequired,
    searchStores: PropTypes.func.isRequired,
    selectDeliveryStore: PropTypes.func.isRequired,
    selectStore: PropTypes.func.isRequired,
    setMarkers: PropTypes.func.isRequired,
    showModal: PropTypes.func.isRequired,

    brandName: PropTypes.string.isRequired,
    filters: PropTypes.object.isRequired,
    isStoresLoading: PropTypes.bool.isRequired,
    isMobile: PropTypes.bool.isRequired,
    mapExpanded: PropTypes.bool.isRequired,
    location: PropTypes.object.isRequired,
    stores: PropTypes.array.isRequired,

    selectedStoreIndex: PropTypes.number,
    storeLocatorType: PropTypes.string,
    modalOpen: PropTypes.bool,
    isHeaderEnabled: PropTypes.bool,
    storeSearch: PropTypes.func,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentWillMount() {
    const { stores, storeSearch, location, setMarkers } = this.props
    storeSearch(location)
    if (process.browser) {
      if (!isEmpty(stores)) {
        setMarkers()
      }
      if (this.hideMap()) window.map = null
    }
  }

  componentWillReceiveProps(nextProps) {
    const { query: nextQuery } = nextProps.location
    const { clearFilters, modalOpen } = this.props
    if (this.shouldPerformSearch(nextQuery)) {
      this.props.storeSearch(nextProps.location)
    }
    if (this.hideMap()) {
      window.map = null
    }
    if (!nextProps.modalOpen && modalOpen) {
      clearFilters()
    }
  }

  componentDidUpdate(prevProps) {
    const { mapExpanded, resizeMap, selectedStoreIndex } = this.props
    if (this.hideMap()) window.map = null
    if (prevProps.mapExpanded !== mapExpanded) {
      // wait untill DOM transition has been completed
      setTimeout(resizeMap, 300)
    }
    if (
      prevProps.selectedStoreIndex !== selectedStoreIndex &&
      selectedStoreIndex !== undefined
    ) {
      setTimeout(this.scrollToSelectedStore, 300)
    }
  }

  componentWillUnmount() {
    this.props.resetStoreLocator()
  }

  onUserLocatorInputSubmit = (event) => {
    const { searchStores } = this.props
    event.preventDefault()
    searchStores()
  }

  getFiltersInfo = () => {
    const { filters, brandName, CFSI } = this.props
    const appliedFilters = filter(({ applied }) => applied, filters)

    return Object.keys(appliedFilters).length === Object.keys(filters).length
      ? 'Showing all store types'
      : `Showing ${Object.keys(appliedFilters)
          .map((filter) => getFilterLabel(filter, brandName, CFSI))
          .join(', ')}`
  }

  scrollToSelectedStore = () => {
    const offsetTop =
      this.selectedStore.getClientRects()[0].top -
      this.resultsContainer.getClientRects()[0].top
    const scrollTop = this.resultsContainer.scrollTop + offsetTop
    animate(
      this.resultsContainer,
      {
        scrollTop,
      },
      {
        duration: 100,
      }
    )
  }

  showFilters = (event) => {
    const { showModal, closeModal } = this.props
    event.stopPropagation()
    showModal(<StoreLocatorFilters onApply={closeModal} />, { mode: 'rollBig' })
  }

  shouldPerformSearch(nextQuery) {
    const latitude = pathOr('', ['latitude'], nextQuery)
    const longitude = pathOr('', ['longitude'], nextQuery)
    const country = pathOr('', ['country'], nextQuery)
    return (!isEmpty(latitude) && !isEmpty(longitude)) || !isEmpty(country)
      ? !equals(this.props.location.query, nextQuery)
      : false
  }

  hideMap = () => {
    const {
      location: {
        query: { latitude, longitude },
      },
    } = this.props
    return (!latitude && latitude !== 0) || (!longitude && longitude !== 0)
  }

  renderShowFiltersButton() {
    return (
      this.props.storeLocatorType === 'collectFromStore' && (
        <FeatureCheck flag="FEATURE_PUDO">
          <button
            className="StoreLocator-showFiltersButton"
            onClick={this.showFilters}
          >
            {this.context.l`Filter`}
          </button>
        </FeatureCheck>
      )
    )
  }

  renderFooterMapExpanded() {
    const { collapseMap } = this.props
    const { l } = this.context
    return (
      <div // eslint-disable-line jsx-a11y/no-static-element-interactions
        className="StoreLocator-footer"
        onClick={collapseMap}
      >
        {this.renderShowFiltersButton()}
        <div className="StoreLocator-footerText">{l`Show list`}</div>
      </div>
    )
  }

  renderFooterMapCollapsed() {
    return (
      <FeatureCheck flag="FEATURE_PUDO">
        <div // eslint-disable-line jsx-a11y/no-static-element-interactions
          className="StoreLocator-footer"
          onClick={this.showFilters}
        >
          {this.renderShowFiltersButton()}
          <div className="StoreLocator-footerText">{this.getFiltersInfo()}</div>
        </div>
      </FeatureCheck>
    )
  }

  renderFooter = () => {
    const { isMobile, mapExpanded } = this.props
    if (!isMobile) return null
    return mapExpanded
      ? this.renderFooterMapExpanded()
      : this.renderFooterMapCollapsed()
  }

  renderGoogleMap = () => {
    const { mapExpanded, isHeaderEnabled, isMobile } = this.props
    return (
      !this.hideMap() && (
        <div
          className={`StoreLocator-googleMapContainer${
            mapExpanded ? ' StoreLocator-googleMapContainer--expanded' : ''
          }${
            !this.isCfs && isHeaderEnabled
              ? ' StoreLocator-googleMapContainer--withEnabledHeader'
              : ''
          }`}
          key="StoreLocator-GoogleMap"
        >
          {isMobile &&
            !(isHeaderEnabled || this.isCfs) && (
              <form
                onSubmit={this.onUserLocatorInputSubmit}
                className="StoreLocator-userLocatorInputContainer"
              >
                <UserLocatorInput selectedCountry="United Kingdom" />
              </form>
            )}
          <GoogleMap className="StoreLocator-googleMap" borderless />
        </div>
      )
    )
  }

  renderStoresList = () => {
    const {
      selectedStoreIndex,
      selectStore,
      location,
      stores,
      deselectStore,
      storeLocatorType,
      selectDeliveryStore,
      isStoresLoading,
      isMobile,
      isHeaderEnabled,
    } = this.props

    if (isEmpty(stores)) return null
    const { latitude, longitude } = location.query
    const searchCoordinates = { latitude, longitude }
    const googleMap = this.renderGoogleMap()
    const fullHeightModifier = !googleMap
      ? ' StoreLocator-resultsContainer--fullHeight'
      : ''
    const storeLocType = storeLocatorType || 'storeSearch'
    const storeList = (
      <div
        key="storeList"
        ref={(element) => {
          this.resultsContainer = element
        }}
        className={`StoreLocator-resultsContainer${fullHeightModifier}`}
      >
        {stores.map((store, index) => {
          const selected = selectedStoreIndex === index
          return (
            <Store
              parentElementRef={(store) => {
                if (selected) {
                  this.selectedStore = store
                }
              }}
              storeLocatorType={storeLocType}
              key={store.storeId}
              storeDetails={store}
              onHeaderClick={
                selected ? deselectStore : () => selectStore(index)
              }
              onSelectClick={() => selectDeliveryStore(store)}
              selected={selected}
              directionsFrom={searchCoordinates}
            />
          )
        })}
      </div>
    )
    return (
      <div
        className={`StoreLocator-fullHeightContainer ${
          isHeaderEnabled && !this.isCfs && !this.hideMap()
            ? 'StoreLocator-fullHeightContainer--withEnabledHeader'
            : ''
        }`}
        // Change opacity to keep same map size, so there is no need to resize or reinitialise
        // the map every time a new filter is selected
        style={{ opacity: isStoresLoading || isEmpty(stores) ? 0 : 1 }}
      >
        {isMobile ? [googleMap, storeList] : [storeList, googleMap]}
      </div>
    )
  }

  renderUserLocator = () => (
    <div className="StoreLocator-userLocatorContainer">
      <UserLocator locator="TopNavMenu" landingPage />
    </div>
  )

  renderNoResultsFound = () => {
    const { l } = this.context
    const { storeLocatorType } = this.props
    return (
      <div className="StoreLocator-userLocatorContainer">
        <div className="StoreLocator-noSearchResults">
          <p>
            {l`Sorry, we couldn't find any locations matching your search. Please enter another location or modify your filters.`}
          </p>
          <UserLocator
            storeLocatorType={storeLocatorType}
            locator="StoreLocator-noResults"
            landingPage
          />
        </div>
      </div>
    )
  }

  get isCfs() {
    return this.props.storeLocatorType === 'collectFromStore'
  }

  get isLandingPage() {
    const { isMobile, isHeaderEnabled, stores } = this.props
    return !this.isCfs && (isEmpty(stores) || !isMobile || isHeaderEnabled)
  }

  shouldShowStoreLocator() {
    const { isMobile, stores, isHeaderEnabled } = this.props
    return (
      !this.isCfs &&
      ((isEmpty(stores) && !this.isValidSearch) ||
        (!isEmpty(stores) && (!isMobile || isHeaderEnabled)))
    )
  }

  get isValidSearch() {
    const {
      location: {
        query: { latitude, longitude, country },
      },
    } = this.props
    return (latitude && longitude) || (country && country !== '')
  }

  renderStoreLocator = () => {
    const { l } = this.context
    const { mapExpanded, storeLocatorType, stores } = this.props

    return (
      <div
        className={`StoreLocator${this.isLandingPage ? ` is-landing` : ''}${
          storeLocatorType ? ` StoreLocator--${storeLocatorType}` : ''
        }`}
      >
        <Helmet title={l`Store Locator`} />
        {(!this.isValidSearch || this.shouldShowStoreLocator()) &&
          this.renderUserLocator()}
        {this.isValidSearch && isEmpty(stores) && this.renderNoResultsFound()}
        {!isEmpty(stores) && this.renderStoresList()}
        {(this.isCfs || mapExpanded) && this.renderFooter()}
      </div>
    )
  }

  render() {
    const { isStoresLoading } = this.props
    return isStoresLoading ? <Loader /> : this.renderStoreLocator()
  }
}
