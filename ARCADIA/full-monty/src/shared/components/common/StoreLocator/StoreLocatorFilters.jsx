import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { values, equals } from 'ramda'
import * as storeLocatorActions from '../../../actions/components/StoreLocatorActions'
import Checkbox from '../../common/FormComponents/Checkbox/Checkbox'
import Button from '../../common/Button/Button'
import getFilterLabel from './get-filter-label'
import { getSelectedFilters } from '../../../lib/store-locator-utilities'
import { isCFSIEspotEnabled } from '../../../../shared/selectors/espotSelectors'

@connect(
  (state) => ({
    filters: state.storeLocator.filters,
    filtersErrorDisplayed: state.storeLocator.filtersErrorDisplayed,
    brandName: state.config.brandName,
    isMobile: state.viewport.media === 'mobile',
    isCFSIEspotEnabled: isCFSIEspotEnabled(state),
  }),
  { ...storeLocatorActions }
)
export default class StoreLocatorFilters extends Component {
  static propTypes = {
    setFilterSelected: PropTypes.func,
    applySelectedFilters: PropTypes.func,
    filters: PropTypes.object,
    onApply: PropTypes.func,
    showFiltersError: PropTypes.func,
    hideFiltersError: PropTypes.func,
    filtersErrorDisplayed: PropTypes.bool,
    brandName: PropTypes.string,
    applyOnChange: PropTypes.bool,
    isMobile: PropTypes.bool,
    setFilters: PropTypes.func,
    isCFSIEspotEnabled: PropTypes.bool,
  }

  componentDidUpdate(prevProps) {
    const { filters, applyOnChange } = this.props
    if (
      !equals(
        getSelectedFilters(prevProps.filters),
        getSelectedFilters(filters)
      ) &&
      applyOnChange
    )
      this.onSubmit()
  }

  onSubmit = (event) => {
    if (event) event.preventDefault()
    const {
      filters,
      applySelectedFilters,
      onApply,
      showFiltersError,
    } = this.props
    const hasSelectedFilters = values(filters).some(({ selected }) => selected)
    if (hasSelectedFilters) {
      applySelectedFilters()
      onApply()
    } else {
      showFiltersError()
    }
  }

  onFilterChange = (index, selected) => {
    const {
      hideFiltersError,
      setFilterSelected,
      isCFSIEspotEnabled,
    } = this.props
    hideFiltersError()
    if (isCFSIEspotEnabled) {
      this.disableFilters(index, selected)
    }
    setFilterSelected(index, selected)
  }

  disableFilters = (filterClicked, selected) => {
    const { filters, setFilters } = this.props
    Object.keys(filters).forEach((filter) => {
      if (filterClicked === 'today') {
        const isFilterSelected = selected
        if ((filter === 'parcel' || filter === 'other') && isFilterSelected) {
          filters[filter].disabled = true
          filters[filter].selected = false
          filters.brand.selected = true
        } else {
          filters[filter].disabled = false
        }
      } else if (filterClicked === 'brand') {
        if (!selected) {
          filters.today.selected = false
          filters[filter].disabled = false
        }
      }
    })
    setFilters(filters)
  }

  render() {
    const { filters, brandName, isMobile, isCFSIEspotEnabled } = this.props
    return (
      <form className="StoreLocatorFilters" onSubmit={this.onSubmit}>
        <h3 className="StoreLocatorFilters-header">Filter</h3>
        <ul className="StoreLocatorFilters-list">
          {Object.keys(filters).map((filter) => {
            const label = getFilterLabel(filter, brandName, isCFSIEspotEnabled)
            return label ? (
              <li key={filter} className="StoreLocatorFilters-listItem">
                <Checkbox
                  className={'StoreLocatorFilters-checkbox'}
                  checked={{ value: filters[filter].selected }}
                  isDisabled={filters[filter].disabled}
                  name={label}
                  onChange={(event) =>
                    this.onFilterChange(filter, event.target.checked)
                  }
                  reverse={isMobile}
                >
                  {label}
                </Checkbox>
              </li>
            ) : null
          })}
        </ul>
        {this.props.filtersErrorDisplayed && (
          <p className="StoreLocatorFilters-error">
            You have to select at least one filter
          </p>
        )}
        <Button className="StoreLocatorFilters-applyButton" type="submit">
          Apply
        </Button>
      </form>
    )
  }
}
