import testComponentHelper from 'test/unit/helpers/test-component'
import GoogleMap from './GoogleMap'

describe('<GoogleMap/>', () => {
  const renderComponent = testComponentHelper(GoogleMap.WrappedComponent)
  jest.useFakeTimers()

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent({}).getTree()).toMatchSnapshot()
    })
    it('should have passed className', () => {
      const { wrapper } = renderComponent({ className: 'sampleClassName' })
      expect(wrapper.find('.GoogleMap').hasClass('sampleClassName')).toBe(true)
    })
    it('should `borderless` class if prop passed', () => {
      const { wrapper } = renderComponent({ borderless: true })
      expect(wrapper.find('.GoogleMap').hasClass('GoogleMap--borderless')).toBe(
        true
      )
    })
  })

  describe('@lifecycle', () => {
    describe('componentDidMount', () => {
      it('should call `initMapWhenGoogleMapsAvailable`', () => {
        const { instance } = renderComponent({
          initMapWhenGoogleMapsAvailable: jest.fn(),
        })
        instance.componentDidMount()
        jest.runAllTimers()
        expect(
          instance.props.initMapWhenGoogleMapsAvailable
        ).toHaveBeenCalledTimes(1)
      })
    })
  })
})
