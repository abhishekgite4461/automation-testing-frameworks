const MONTHS = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
]

export function getTimeInMinutes(hours, minutes) {
  return parseInt(hours, 10) * 60 + parseInt(minutes, 10)
}

function getOrdinalDay(number) {
  const suffix =
    {
      1: 'st',
      2: 'nd',
      3: 'rd',
      21: 'st',
      22: 'nd',
      23: 'rd',
      31: 'st',
    }[number] || 'th'

  return `${number}${suffix}`
}

export function getEnglishDate(dateString) {
  const today = new Date(new Date().toDateString())
  const tomorrow = new Date(
    new Date(new Date().setDate(today.getDate() + 1)).toDateString()
  )
  const date = new Date(new Date(dateString).toDateString())
  const englishDate = `${getOrdinalDay(date.getDate())} ${
    MONTHS[date.getMonth()]
  }`
  const dateTime = date.getTime()

  if (dateTime === today.getTime()) {
    return `today, ${englishDate}`
  }

  if (dateTime === tomorrow.getTime()) {
    return `tomorrow, ${englishDate}`
  }

  return englishDate
}

export function getEarliestDate(dates) {
  const earliestDate = dates.find(({ availableUntil }) => {
    return new Date(availableUntil) > Date.now()
  })
  return earliestDate && earliestDate.collectFrom
}
