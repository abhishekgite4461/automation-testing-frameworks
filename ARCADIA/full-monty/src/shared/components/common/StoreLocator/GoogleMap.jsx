import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import { initMapWhenGoogleMapsAvailable } from '../../../actions/components/StoreLocatorActions'

@connect(null, { initMapWhenGoogleMapsAvailable })
export default class GoogleMap extends Component {
  static propTypes = {
    borderless: PropTypes.bool,
    initMapWhenGoogleMapsAvailable: PropTypes.func,
    className: PropTypes.string,
  }

  static defaultProps = {
    className: '',
  }

  componentDidMount() {
    // Issue: Map tiles not loading consistently
    // Cause: Modal animation
    // Fix: Load google maps after the modal animation

    // TODO: Look for a better solution
    setTimeout(() => {
      this.props.initMapWhenGoogleMapsAvailable()
    }, 500)
  }

  render() {
    const { borderless, className } = this.props
    const classNames = classnames('GoogleMap', className, {
      'GoogleMap--borderless': borderless,
    })
    return (
      <div className={classNames}>
        <div className="GoogleMap-map" />
      </div>
    )
  }
}
