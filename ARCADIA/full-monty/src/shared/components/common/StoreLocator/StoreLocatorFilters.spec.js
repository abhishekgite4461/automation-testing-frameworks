import testComponentHelper from 'test/unit/helpers/test-component'
import StoreLocatorFilters from './StoreLocatorFilters'

describe('<StoreLocatorFilters />', () => {
  const renderComponent = testComponentHelper(
    StoreLocatorFilters.WrappedComponent
  )
  const mockStorefilters = {
    today: {
      applied: true,
      selected: true,
      disabled: false,
    },
    brand: {
      applied: true,
      selected: true,
      disabled: false,
    },
    parcel: {
      applied: true,
      selected: true,
      disabled: false,
    },
    other: {
      applied: true,
      selected: true,
      disabled: false,
    },
  }

  const noSelectedStorefilters = {
    today: {
      applied: true,
      selected: false,
      disabled: false,
    },
    brand: {
      applied: true,
      selected: false,
      disabled: false,
    },
    parcel: {
      applied: true,
      selected: false,
      disabled: false,
    },
    other: {
      applied: true,
      selected: false,
      disabled: false,
    },
  }

  const initialProps = {
    setFilterSelected: jest.fn(),
    applySelectedFilters: jest.fn(),
    onApply: jest.fn(),
    showFiltersError: jest.fn(),
    hideFiltersError: jest.fn(),
    filtersErrorDisplayed: false,
    brandName: 'Topshop',
    applyOnChange: true,
    isMobile: true,
    setFilters: jest.fn(),
  }

  beforeEach(() => jest.resetAllMocks())

  describe('@render', () => {
    it('default with CFSI not enabled', () => {
      expect(
        renderComponent({
          ...initialProps,
          filters: mockStorefilters,
        }).getTree()
      ).toMatchSnapshot()
    })

    it('default with CFSI enabled', () => {
      expect(
        renderComponent({
          ...initialProps,
          filters: mockStorefilters,
          isCFSIEspotEnabled: true,
        }).getTree()
      ).toMatchSnapshot()
    })

    it('check filter length if cfsi is false', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        filters: mockStorefilters,
      })
      expect(wrapper.find('.StoreLocatorFilters-listItem').length).toBe(3)
    })

    it('check filter length if cfsi is true', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        isCFSIEspotEnabled: true,
        filters: mockStorefilters,
      })
      expect(wrapper.find('.StoreLocatorFilters-listItem').length).toBe(4)
    })
  })

  describe('@events', () => {
    it('set today filter to be true', () => {
      const { instance } = renderComponent({
        ...initialProps,
        isCFSIEspotEnabled: true,
        filters: mockStorefilters,
      })
      instance.disableFilters('today', true)
      expect(instance.props.filters.parcel.disabled).toBe(true)
      expect(instance.props.filters.other.disabled).toBe(true)
      expect(instance.props.filters.brand.selected).toBe(true)
    })

    it('set today filter to be false', () => {
      const { instance } = renderComponent({
        ...initialProps,
        isCFSIEspotEnabled: true,
        filters: mockStorefilters,
      })
      instance.disableFilters('today', false)
      expect(instance.props.filters.parcel.disabled).toBe(false)
      expect(instance.props.filters.other.disabled).toBe(false)
      expect(instance.props.filters.brand.selected).toBe(true)
    })

    it('set brand filter to be false', () => {
      const { instance } = renderComponent({
        ...initialProps,
        isCFSIEspotEnabled: true,
        filters: mockStorefilters,
      })
      instance.disableFilters('brand', false)
      expect(instance.props.filters.today.selected).toBe(false)
      expect(instance.props.filters.other.disabled).toBe(false)
      expect(instance.props.filters.brand.disabled).toBe(false)
      expect(instance.props.filters.parcel.disabled).toBe(false)
    })

    it('simulate change event for checkboxes', () => {
      const { wrapper, instance } = renderComponent({
        ...initialProps,
        isCFSIEspotEnabled: true,
        filters: mockStorefilters,
      })

      const event = {
        target: {
          checked: true,
        },
      }

      wrapper
        .find('.StoreLocatorFilters-listItem')
        .first()
        .children()
        .simulate('change', event)
      expect(instance.props.filters.today.selected).toBe(false)
    })
  })

  describe('@onSubmit', () => {
    let evt

    beforeEach(() => {
      evt = {
        preventDefault: jest.fn(),
      }
    })

    it('should call preventDefault', () => {
      const { instance } = renderComponent({
        ...initialProps,
        filters: mockStorefilters,
      })
      expect(evt.preventDefault).not.toHaveBeenCalled()
      instance.onSubmit(evt)
      expect(evt.preventDefault).toHaveBeenCalledTimes(1)
      expect(instance.props.applySelectedFilters).toHaveBeenCalledTimes(1)
      expect(instance.props.onApply).toHaveBeenCalledTimes(1)
    })

    it('expect filter errors to show if no filters selected', () => {
      const { instance } = renderComponent({
        ...initialProps,
        filters: noSelectedStorefilters,
      })
      expect(evt.preventDefault).not.toHaveBeenCalled()
      instance.onSubmit(evt)
      expect(instance.props.showFiltersError).toHaveBeenCalledTimes(1)
    })
  })
})
