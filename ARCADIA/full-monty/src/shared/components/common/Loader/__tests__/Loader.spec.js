import testComponentHelper from 'test/unit/helpers/test-component'

import Loader from '../Loader'

describe('<Loader />', () => {
  const renderComponent = testComponentHelper(Loader)

  describe('@renders', () => {
    it('should render in default state', () => {
      const { getTree } = renderComponent()
      expect(getTree()).toMatchSnapshot()
    })

    it('should be able to change `fillColor`', () => {
      const { wrapper } = renderComponent({
        fillColor: '#ffffff',
      })
      expect(wrapper.find({ fill: '#ffffff' }).isEmpty()).toBe(false)
    })

    it('should add supplied `className`', () => {
      const { wrapper } = renderComponent({
        className: 'MyClass',
      })
      expect(wrapper.find('.Loader').hasClass('MyClass')).toBe(true)
    })

    it('should add ‘Loader--button’ and ‘Loader-image--button’ classes only, if `isButton` is `true`', () => {
      const { wrapper } = renderComponent({
        isButton: true,
      })
      expect(wrapper.find('.Loader--button').isEmpty()).toBe(false)
      expect(wrapper.find('.Loader').isEmpty()).toBe(true)
      expect(wrapper.find('.Loader-image--button').isEmpty()).toBe(false)
      expect(wrapper.find('.Loader-image').isEmpty()).toBe(true)
    })
  })
})
