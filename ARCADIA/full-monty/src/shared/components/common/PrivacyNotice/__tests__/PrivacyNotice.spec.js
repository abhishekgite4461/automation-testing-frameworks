import testComponentHelper from 'test/unit/helpers/test-component'
import PrivacyNotice from '../PrivacyNotice'

describe('<PrivacyNotice />', () => {
  const initialProps = {
    brandName: 'missselfridge',
    region: 'uk',
  }
  const renderComponent = testComponentHelper(PrivacyNotice.WrappedComponent)

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })

    it('renders with a different region, for example de', () => {
      const props = {
        ...initialProps,
        region: 'de',
      }
      expect(renderComponent(props).getTree()).toMatchSnapshot()
    })
    it('should not render if region is us', () => {
      const props = {
        ...initialProps,
        region: 'us',
      }
      expect(renderComponent(props).getTree()).toMatchSnapshot()
    })

    it('renders with a different brand, for example topshop', () => {
      const props = {
        ...initialProps,
        brandName: 'topshop',
      }
      expect(renderComponent(props).getTree()).toMatchSnapshot()
    })

    it('renders with different a region and brand, for example de and topshop', () => {
      const props = {
        ...initialProps,
        region: 'de',
        brandName: 'topshop',
      }
      expect(renderComponent(props).getTree()).toMatchSnapshot()
    })

    it('defaults to no href if there is no brand for a given region, for example burton in de', () => {
      const props = {
        ...initialProps,
        brandName: 'burton',
        region: 'de',
      }
      expect(renderComponent(props).getTree()).toMatchSnapshot()
    })
  })
})
