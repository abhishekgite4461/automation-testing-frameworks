import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { path } from 'ramda'

// selectors
import {
  getBrandName,
  getRegion,
} from '../../../selectors/common/configSelectors'

const PrivacyPolicyUrls = require('./PrivacyPolicyUrls.json')

// NOTE: this will be refactor when will have PrivacyPolicy Endpoint
const getPrivacyPolicyUrlForRegionAndBrand = (brandName, region) => {
  return path([brandName, region], PrivacyPolicyUrls)
}

@connect(
  (state) => ({
    brandName: getBrandName(state),
    region: getRegion(state),
  }),
  {}
)
export default class PrivacyNotice extends PureComponent {
  static contextTypes = {
    l: PropTypes.func,
  }

  static propTypes = {
    brandName: PropTypes.string.isRequired,
    region: PropTypes.string.isRequired,
  }

  render() {
    const { l } = this.context
    const { brandName, region } = this.props
    if (region === 'us') return null
    return (
      <div className="PrivacyNotice">
        {l`privacyNoticeBeforeLink`}
        <Link
          to={getPrivacyPolicyUrlForRegionAndBrand(brandName, region)}
        >{l`privacyNoticeUrlText`}</Link>
        .
      </div>
    )
  }
}
