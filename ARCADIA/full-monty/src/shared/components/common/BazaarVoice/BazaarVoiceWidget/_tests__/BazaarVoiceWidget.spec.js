import testComponentHelper from 'test/unit/helpers/test-component'

import BazaarVoiceWidget from '../BazaarVoiceWidget'

describe('<BazaarVoiceWidget />', () => {
  const renderComponent = testComponentHelper(
    BazaarVoiceWidget.WrappedComponent
  )

  describe('@renders', () => {
    it('should render in default state', () => {
      const { getTree } = renderComponent()
      expect(getTree()).toMatchSnapshot()
    })

    it('should not render summary container if `containerOnly` is `true`', () => {
      const { wrapper } = renderComponent({
        containerOnly: true,
      })
      expect(wrapper.find('#BVRRSummaryContainer').isEmpty()).toBe(true)
    })

    it('should not render container if `summaryOnly` is `true`', () => {
      const { wrapper } = renderComponent({
        summaryOnly: true,
      })
      expect(wrapper.find('#BVRRContainer').isEmpty()).toBe(true)
    })
  })
})
