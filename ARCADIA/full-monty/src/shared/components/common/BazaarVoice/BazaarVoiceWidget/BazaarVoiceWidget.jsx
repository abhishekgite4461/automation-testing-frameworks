import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import loadBazaarVoiceApi from '../../../../lib/load-bazaar-voice-api'
import animate from 'amator'

@connect((state) => ({
  location: state.routing.location,
  brandName: state.config.brandName,
  bazaarVoiceId: state.config.bazaarVoiceId,
}))
export default class BazaarVoiceWidget extends Component {
  static propTypes = {
    lineNumber: PropTypes.string,
    productId: PropTypes.number,
    brandName: PropTypes.string,
    bazaarVoiceId: PropTypes.string,
    location: PropTypes.object,
    summaryOnly: PropTypes.bool,
    containerOnly: PropTypes.bool,
    className: PropTypes.string,
  }

  static defaultProps = {
    className: '',
  }

  componentDidMount() {
    this.loadBazaar()
  }

  componentDidUpdate(prevProps) {
    if (
      (this.props &&
        prevProps &&
        this.props.summaryOnly !== prevProps.summaryOnly) ||
      this.props.containerOnly !== prevProps.containerOnly
    ) {
      this.loadBazaar()
    }
  }

  shouldScrollToReviews = () =>
    this.props.location && this.props.location.hash === '#BVReviews'

  scrollToBazaar = (id) => {
    const body = document.body
    const element = document.getElementById(id)

    const bodyRect = body.getBoundingClientRect()
    const elemRect = element.getBoundingClientRect()
    const offset = elemRect.top - bodyRect.top

    animate(
      body,
      { scrollTop: offset },
      {
        duration: 100,
      }
    )
  }

  loadBazaar = () => {
    const { brandName, bazaarVoiceId, lineNumber, productId } = this.props
    loadBazaarVoiceApi(brandName, bazaarVoiceId, () => {
      // eslint-disable-next-line no-undef
      $BV.configure('global', {
        submissionContainerUrl: `${window.location.protocol}//${
          window.location.host
        }/review/${productId}`,
        submissionReturnUrl: window.location.href,
      })
      // eslint-disable-next-line no-undef
      $BV.ui('rr', 'show_reviews', {
        productId: lineNumber,
      })
    }).then(() => {
      if (this.shouldScrollToReviews()) {
        this.scrollToBazaar('BVRRContainer')
      }
    })
  }

  render() {
    const { className, containerOnly, summaryOnly } = this.props
    return (
      <div className={`BazaarVoice ${className}`}>
        {!containerOnly && <div id="BVRRSummaryContainer" />}
        {!summaryOnly && <div id="BVRRContainer" />}
      </div>
    )
  }
}
