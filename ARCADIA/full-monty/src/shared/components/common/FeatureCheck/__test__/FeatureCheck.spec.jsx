import { mount } from 'enzyme'
import React from 'react'
import FeatureCheck from '../FeatureCheck'
import configureMockStore from '../../../../../../test/unit/lib/configure-mock-store'

const mountOptions = (enabled) => ({
  context: {
    store: configureMockStore({
      features: { status: { TESTING_FLAG: enabled } },
    }),
  },
})

describe('<FeatureCheck />', () => {
  const render = () => (
    <FeatureCheck flag="TESTING_FLAG">
      <div className="children" />
    </FeatureCheck>
  )

  it('should render children if flag is enabled', () => {
    const wrapper = mount(render(), mountOptions(true))
    expect(wrapper.find('.children')).toHaveLength(1)
  })

  it('should render children if flag is disabled', () => {
    const wrapper = mount(render(), mountOptions(false))
    expect(wrapper.find('.children')).toHaveLength(0)
  })
})
