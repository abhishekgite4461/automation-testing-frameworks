import { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { pathOr } from 'ramda'

// :: State -> MappedState
const mapState = pathOr({}, ['features', 'status'])

@connect(mapState, {})
export default class FeatureCheck extends Component {
  static propTypes = {
    flag: PropTypes.string,
    children: PropTypes.node,
  }

  render() {
    const { flag, children } = this.props
    return this.props[flag] ? children : null
  }
}
