import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { isEmpty } from 'ramda'
import classnames from 'classnames'
import QubitReact from 'qubit-react/wrapper'
import {
  sendAnalyticsDisplayEvent,
  GTM_EVENT,
  GTM_TRIGGER,
} from '../../../analytics'
import { analyticsPdpClickEvent } from '../../../analytics/tracking/site-interactions'

import { isMobile } from '../../../selectors/viewportSelectors'

// actions
import {
  addToBag,
  openMiniBag,
} from '../../../actions/common/shoppingBagActions'

// components
import Button from '../Button/Button'
import AddToBagModal from '../AddToBagModal/AddToBagModal'
import InlineConfirm from './InlineConfirm'
import DeliveryCutoffMessage from '../../common/DeliveryCutoffMessage/DeliveryCutoffMessage'

class AddToBag extends Component {
  static propTypes = {
    productId: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
      .isRequired,
    sku: PropTypes.string,
    bundleItems: PropTypes.arrayOf(
      PropTypes.shape({
        productId: PropTypes.number,
        sku: PropTypes.string,
      })
    ),
    className: PropTypes.string,
    deliveryMessage: PropTypes.string,
    quantity: PropTypes.number,
    shouldShowInlineConfirm: PropTypes.bool,
    shouldShowMiniBagConfirm: PropTypes.bool,
    isMobile: PropTypes.bool,
    shouldAddToBag: PropTypes.func,
    dispatchEvent: PropTypes.func,
    // actions
    addToBag: PropTypes.func.isRequired,
    openMiniBag: PropTypes.func.isRequired,
    addToBagIsReady: PropTypes.bool.isRequired,
    sendAnalyticsDisplayEvent: PropTypes.func.isRequired,
  }

  static defaultProps = {
    className: '',
    sku: undefined,
    bundleItems: undefined,
    deliveryMessage: '',
    quantity: 1,
    shouldShowInlineConfirm: false,
    shouldShowMiniBagConfirm: false,
    isMobile: true,
    shouldAddToBag: () => true,
    dispatchEvent:
      typeof window !== 'undefined'
        ? document.dispatchEvent.bind(document)
        : () => {},
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  state = {
    showInlineConfirm: false,
  }

  addToBag = () => {
    const {
      productId,
      sku,
      quantity,
      bundleItems,
      isMobile,
      shouldShowInlineConfirm,
      shouldShowMiniBagConfirm,
      shouldAddToBag,
      dispatchEvent,
      addToBag,
      openMiniBag,
      sendAnalyticsDisplayEvent,
    } = this.props

    this.setState({ showInlineConfirm: false })

    if (!shouldAddToBag({ sku, bundleItems })) return

    dispatchEvent(new Event('addToBag'))

    analyticsPdpClickEvent(`addtobag`)

    return addToBag(
      productId,
      sku,
      quantity,
      <AddToBagModal />,
      bundleItems
    ).then((res) => {
      if (res instanceof Error) return

      if (shouldShowInlineConfirm) this.setState({ showInlineConfirm: true })

      if (shouldShowMiniBagConfirm && !isMobile) {
        setTimeout(() => {
          openMiniBag(true)
          sendAnalyticsDisplayEvent(
            {
              bagDrawerTrigger: GTM_TRIGGER.PRODUCT_ADDED_TO_BAG,
            },
            GTM_EVENT.BAG_DRAWER_DISPLAYED
          )
        }, 100)
      }
    })
  }

  render() {
    const { l } = this.context
    const {
      isMobile,
      bundleItems,
      deliveryMessage,
      className,
      addToBagIsReady,
    } = this.props
    const buttonClassNames = classnames({ 'is-active': addToBagIsReady })

    return (
      <QubitReact id="qubit-pdp-add-to-bag">
        <div className={`AddToBag ${className}`}>
          <Button className={buttonClassNames} clickHandler={this.addToBag}>
            {!bundleItems ? l`Add to bag` : l`Add All to Bag`}
          </Button>
          {!isMobile && this.state.showInlineConfirm && <InlineConfirm />}
          <DeliveryCutoffMessage message={deliveryMessage} />
        </div>
      </QubitReact>
    )
  }
}

export default connect(
  (state, { quantity, addToBagIsReady }) => ({
    quantity:
      quantity !== undefined ? quantity : state.productDetail.selectedQuantity,
    isMobile: isMobile(state),
    addToBagIsReady:
      addToBagIsReady || !isEmpty(state.productDetail.activeItem),
  }),
  {
    addToBag,
    openMiniBag,
    sendAnalyticsDisplayEvent,
  }
)(AddToBag)

export { AddToBag as WrappedAddToBag }
