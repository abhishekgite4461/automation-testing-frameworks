import React from 'react'

import testComponentHelper from 'test/unit/helpers/test-component'
import { mockStoreCreator } from 'test/unit/helpers/get-redux-mock-store'

import AddToBag, { WrappedAddToBag } from '../AddToBag'
import AddToBagModal from '../../AddToBagModal/AddToBagModal'
import InlineConfirm from '../InlineConfirm'
import Button from '../../Button/Button'
import DeliveryCutoffMessage from '../../../common/DeliveryCutoffMessage/DeliveryCutoffMessage'
import * as siteInteractions from '../../../../analytics/tracking/site-interactions'
import { GTM_TRIGGER, GTM_EVENT } from '../../../../analytics'

jest.spyOn(siteInteractions, 'analyticsPdpClickEvent')

const noop = () => {}

describe('<AddToBag />', () => {
  const requiredProps = {
    productId: 29434263,
    addToBag: () => Promise.resolve(),
    addToBagIsReady: false,
    showMiniBagConfirm: noop,
    openMiniBag: noop,
    sendAnalyticsDisplayEvent: jest.fn(),
  }

  const renderComponent = testComponentHelper(WrappedAddToBag)

  describe('@connected', () => {
    it('should get `quantity` from `productDetail.selectedQuantity` in state', () => {
      const store = mockStoreCreator({
        productDetail: {
          selectedQuantity: 2,
        },
      })
      const { wrapper } = testComponentHelper(AddToBag)({
        store,
        ...requiredProps,
      })
      expect(wrapper.prop('quantity')).toBe(2)
    })
    it('should get `addToBagIsReady` from `addToBagIsReady` in state if its true', () => {
      const store = mockStoreCreator({
        productDetail: {
          activeItem: {},
        },
      })
      const { wrapper } = testComponentHelper(AddToBag)({
        store,
        ...requiredProps,
        addToBagIsReady: true,
      })
      expect(wrapper.prop('addToBagIsReady')).toBe(true)
    })
    it('should get set `addToBagIsReady` to true id `addToBagIsReady` is false but `productDetail.activeItem` is not empty ', () => {
      const store = mockStoreCreator({
        productDetail: {
          activeItem: { sku: 'sku294u982345' },
        },
      })
      const { wrapper } = testComponentHelper(AddToBag)({
        store,
        ...requiredProps,
      })
      expect(wrapper.prop('addToBagIsReady')).toBe(true)
    })

    it('should take supplied `quantity` prop over state', () => {
      const store = mockStoreCreator({
        productDetail: {
          selectedQuantity: 2,
        },
      })
      const { wrapper } = testComponentHelper(AddToBag)({
        ...requiredProps,
        quantity: 4,
        store,
      })
      expect(wrapper.prop('quantity')).toBe(4)
    })
  })

  describe('@renders', () => {
    it('should render default state', () => {
      const { getTree } = renderComponent(requiredProps)
      expect(getTree()).toMatchSnapshot()
    })

    it('should render `InlineConfirm` if `showInlineConfirm` state is `true` and `isMobile` is false', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        isMobile: false,
      })
      wrapper.setState({
        showInlineConfirm: true,
      })
      expect(wrapper.find(InlineConfirm).exists()).toBeTruthy()
    })

    it('Button component should render "Add All to Bag" when `bundleItems` is truthy (empty array)', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        bundleItems: [],
      })
      expect(
        wrapper
          .find(Button)
          .render()
          .text()
      ).toBe('Add All to Bag')
    })

    it('Button component should render without is-active class', () => {
      const { wrapper } = renderComponent({ ...requiredProps })
      expect(wrapper.find(Button).hasClass('is-active')).toBe(false)
    })

    it('Button component should render with is-active class', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        addToBagIsReady: true,
      })
      expect(wrapper.find(Button).hasClass('is-active')).toBe(true)
    })

    it('Button component should render "Add All to Bag" when `bundleItems` is truthy (array with items)', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        bundleItems: [
          {
            productId: 29752415,
            sku: '602017001154368',
          },
          {
            productId: 29750936,
            sku: '602017001154352',
          },
        ],
      })
      expect(
        wrapper
          .find(Button)
          .render()
          .text()
      ).toBe('Add All to Bag')
    })

    it('Button component should render "Add to bag" when `bundleItems` is falsey', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
      })
      expect(
        wrapper
          .find(Button)
          .render()
          .text()
      ).toBe('Add to bag')
    })

    it('should pass `deliveryMessage` prop to <DeliveryCutoffMessage />', () => {
      const deliveryMessage = 'Order in 9 hrs 23 mins for next day delivery'
      const { wrapper } = renderComponent({
        ...requiredProps,
        deliveryMessage,
      })
      expect(wrapper.find(DeliveryCutoffMessage).prop('message')).toBe(
        deliveryMessage
      )
    })
  })

  describe('@events', () => {
    describe('on click of ‘Add to Bag’ button', () => {
      it('should set `showInlineConfirm` state to `false`', () => {
        const { wrapper } = renderComponent(requiredProps)
        wrapper.find('Button').prop('clickHandler')()
        expect(wrapper.state('showInlineConfirm')).toBe(false)
      })

      it('should call `shouldAddToBag` with the `sku` and `bundleItems`', () => {
        const shouldAddToBagMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          sku: '602017001144095',
          bundleItems: [
            {
              productId: 29752415,
              sku: '602017001154368',
            },
          ],
          shouldAddToBag: shouldAddToBagMock,
        })
        wrapper.find('Button').prop('clickHandler')()
        expect(shouldAddToBagMock).toHaveBeenCalledWith({
          sku: '602017001144095',
          bundleItems: [
            {
              productId: 29752415,
              sku: '602017001154368',
            },
          ],
        })
      })

      it('should not call `addToBag` if `shouldAddToBag` returns `false`', () => {
        const addToBagMock = jest.fn(() => Promise.resolve())
        const { wrapper } = renderComponent({
          ...requiredProps,
          shouldAddToBag: () => false,
          addToBag: addToBagMock,
        })
        wrapper.find('Button').prop('clickHandler')()
        expect(addToBagMock).not.toHaveBeenCalled()
      })

      it('should call `dispatchEvent` prop with `addToBag` event and sends the analytics click event with `addtobag`', () => {
        const dispatchEventMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          sku: '602017001144094',
          dispatchEvent: dispatchEventMock,
        })
        wrapper.find('Button').prop('clickHandler')()
        expect(dispatchEventMock.mock.calls[0][0].type).toBe('addToBag')
        expect(siteInteractions.analyticsPdpClickEvent).toHaveBeenCalledWith(
          'addtobag'
        )
      })

      it('should call `addToBag` prop with `productId`, `sku` and `quantity`', () => {
        const addToBagMock = jest.fn(() => Promise.resolve())
        const { wrapper } = renderComponent({
          ...requiredProps,
          sku: '602017001144094',
          quantity: 5,
          addToBag: addToBagMock,
        })
        wrapper.find('Button').prop('clickHandler')()
        const [productIdArg, skuArg, quantityArg] = addToBagMock.mock.calls[0]
        expect(productIdArg).toBe(29434263)
        expect(skuArg).toBe('602017001144094')
        expect(quantityArg).toBe(5)
      })

      it('should pass correct successHTML to `addToBag`', () => {
        const addToBagMock = jest.fn(() => Promise.resolve())
        const additionalProps = {
          sku: '602017001144094',
          quantity: 1,
          bundleItems: undefined,
          addToBag: addToBagMock,
        }
        const { wrapper } = renderComponent({
          ...requiredProps,
          ...additionalProps,
        })
        wrapper.find('Button').prop('clickHandler')()
        const args = addToBagMock.mock.calls[0]
        expect(args[3]).toEqual(<AddToBagModal />)
      })

      it('should call `addToBag` prop with `bundleItems`', () => {
        const addToBagMock = jest.fn(() => Promise.resolve())
        const { wrapper } = renderComponent({
          ...requiredProps,
          bundleItems: [
            {
              productId: 29752415,
              sku: '602017001154368',
            },
          ],
          addToBag: addToBagMock,
        })
        wrapper.find('Button').prop('clickHandler')()
        const args = addToBagMock.mock.calls[0]
        expect(args[4]).toEqual([
          {
            productId: 29752415,
            sku: '602017001154368',
          },
        ])
      })

      describe('on `addToBag` fulfilled', () => {
        beforeEach(jest.useFakeTimers)

        it('should set `showInlineConfirm` state to `true` if `shouldShowInlineConfirm` prop is `true`', () => {
          const addToBagMock = jest.fn(() => Promise.resolve())
          const { wrapper } = renderComponent({
            ...requiredProps,
            sku: '602017001144094',
            shouldShowInlineConfirm: true,
            addToBag: addToBagMock,
          })

          return wrapper
            .find('Button')
            .prop('clickHandler')()
            .then(() => {
              expect(wrapper.state('showInlineConfirm')).toBe(true)
            })
        })

        it('should open mini bag with a 100ms delay if `shouldShowMiniBagConfirm` prop is `true`', () => {
          const addToBagMock = jest.fn(() => Promise.resolve())
          const openMiniBagMock = jest.fn()
          const sendAnalyticsDisplayEventMock = jest.fn()
          const { wrapper } = renderComponent({
            ...requiredProps,
            sku: '602017001144094',
            shouldShowMiniBagConfirm: true,
            addToBag: addToBagMock,
            isMobile: false,
            openMiniBag: openMiniBagMock,
            sendAnalyticsDisplayEvent: sendAnalyticsDisplayEventMock,
          })

          return wrapper
            .find('Button')
            .prop('clickHandler')()
            .then(() => {
              expect(openMiniBagMock).not.toHaveBeenCalled()
              expect(setTimeout).toHaveBeenCalledWith(expect.any(Function), 100)
              jest.runAllTimers()
              expect(openMiniBagMock).toHaveBeenCalledTimes(1)
              expect(openMiniBagMock).toHaveBeenCalledWith(true)
              expect(sendAnalyticsDisplayEventMock).toHaveBeenCalledWith(
                {
                  bagDrawerTrigger: GTM_TRIGGER.PRODUCT_ADDED_TO_BAG,
                },
                GTM_EVENT.BAG_DRAWER_DISPLAYED
              )
            })
        })

        it('should not open mini bag when on mobile', () => {
          const addToBagMock = jest.fn(() => Promise.resolve())
          const openMiniBagMock = jest.fn()
          const sendAnalyticsDisplayEventMock = jest.fn()

          const { wrapper } = renderComponent({
            ...requiredProps,
            sku: '602017001144094',
            shouldShowInlineConfirm: true,
            shouldShowMiniBagConfirm: true,
            addToBag: addToBagMock,
            openMiniBag: openMiniBagMock,
            sendAnalyticsDisplayEvent: sendAnalyticsDisplayEventMock,
            isMobile: true,
          })

          return wrapper
            .find('Button')
            .prop('clickHandler')()
            .then(() => {
              expect(setTimeout).not.toHaveBeenCalled()
              jest.runAllTimers()
              expect(openMiniBagMock).not.toHaveBeenCalled()
              expect(sendAnalyticsDisplayEventMock).not.toHaveBeenCalled()
            })
        })

        it('should do nothing if `addToBag` fulfills with an `Error`', () => {
          const addToBagMock = jest.fn(() => Promise.resolve(new Error()))
          const { wrapper } = renderComponent({
            ...requiredProps,
            sku: '602017001144094',
            shouldShowInlineConfirm: true,
            shouldShowMiniBagConfirm: true,
            addToBag: addToBagMock,
          })

          return wrapper
            .find('Button')
            .prop('clickHandler')()
            .then(() => {
              expect(wrapper.state('showInlineConfirm')).toBe(false)
              expect(setTimeout).not.toHaveBeenCalled()
            })
        })
      })
    })
  })
})
