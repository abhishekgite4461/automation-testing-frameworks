import React from 'react'
import PropTypes from 'prop-types'

const WishListIcon = ({ isSelected = false, modifier }) => (
  <i
    className={`WishListIcon${isSelected ? ' is-selected' : ''}${
      modifier ? ` WishListIcon--${modifier}` : ''
    }`}
  />
)

WishListIcon.propTypes = {
  isSelected: PropTypes.bool,
  modifier: PropTypes.oneOf(['plp', 'pdp', 'quickview', 'bundle', 'minibag']),
}

export default WishListIcon
