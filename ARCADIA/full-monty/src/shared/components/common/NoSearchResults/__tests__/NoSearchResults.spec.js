import testComponentHelper from 'test/unit/helpers/test-component'
import NoSearchResults from '../NoSearchResults'
import Button from '../../Button/Button'

describe('<NoSearchResults/>', () => {
  const mobileNonFilterProps = {
    toggleProductsSearchBar: jest.fn(),
    filterType: false,
    clearRefinements: jest.fn(),
    applyRefinements: jest.fn(),
    isMobile: true,
  }
  const mobileFilterProps = {
    ...mobileNonFilterProps,
    filterType: true,
  }
  const nonMobileProps = {
    ...mobileNonFilterProps,
    isMobile: false,
  }

  const renderComponent = testComponentHelper(NoSearchResults.WrappedComponent)

  beforeEach(() => {
    jest.resetAllMocks()
  })

  describe('@render', () => {
    it('in default state', () => {
      expect(renderComponent(mobileNonFilterProps).getTree()).toMatchSnapshot()
    })

    it('has filterType', () => {
      expect(renderComponent(mobileFilterProps).getTree()).toMatchSnapshot()
    })

    it('has no button when desktop', () => {
      expect(renderComponent(nonMobileProps).getTree()).toMatchSnapshot()
    })

    it('calls continueOption method via render method', () => {
      const { instance } = renderComponent(mobileNonFilterProps)

      jest.spyOn(instance, 'continueOption')

      expect(instance.continueOption).not.toBeCalled()
      instance.render()
      expect(instance.continueOption).toHaveBeenCalledTimes(1)
    })
  })

  describe('@events', () => {
    it('should call clearAndApplyRefinements', () => {
      const { wrapper } = renderComponent(mobileFilterProps)
      expect(mobileFilterProps.clearRefinements).not.toHaveBeenCalled()
      expect(mobileFilterProps.applyRefinements).not.toHaveBeenCalled()
      wrapper
        .find(Button)
        .props()
        .clickHandler()
      expect(mobileFilterProps.clearRefinements).toHaveBeenCalled()
      expect(mobileFilterProps.applyRefinements).toHaveBeenCalled()
    })

    it('should call toggleProductsSearchBar', () => {
      const { wrapper } = renderComponent(mobileNonFilterProps)

      expect(mobileNonFilterProps.toggleProductsSearchBar).not.toBeCalled()
      wrapper
        .find(Button)
        .props()
        .clickHandler()
      expect(
        mobileNonFilterProps.toggleProductsSearchBar
      ).toHaveBeenCalledTimes(1)
      expect(
        mobileNonFilterProps.toggleProductsSearchBar.mock.calls[0][0]
      ).toBeUndefined()
    })
  })

  describe('instance methods', () => {
    describe('clearAndApplyRefinements', () => {
      it('should call clearRefinements and applyRefinements', () => {
        const instance = renderComponent(mobileNonFilterProps).instance

        expect(mobileNonFilterProps.clearRefinements).not.toBeCalled()
        expect(mobileNonFilterProps.applyRefinements).not.toBeCalled()
        instance.clearAndApplyRefinements()
        expect(mobileNonFilterProps.clearRefinements).toHaveBeenCalledTimes(1)
        expect(mobileNonFilterProps.applyRefinements).toHaveBeenCalledTimes(1)
      })
    })
  })
})
