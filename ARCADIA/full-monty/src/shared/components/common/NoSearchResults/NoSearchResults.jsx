import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Button from '../Button/Button'
import Helmet from 'react-helmet'
import Recommendations from '../Recommendations/Recommendations'
import Image from '../Image/Image'
import { toggleProductsSearchBar } from '../../../actions/components/search-bar-actions'
import {
  clearRefinements,
  applyRefinements,
} from '../../../actions/components/refinementsActions'
import { connect } from 'react-redux'

@connect(
  (state) => ({
    isMobile: state.viewport.media === 'mobile',
  }),
  { toggleProductsSearchBar, clearRefinements, applyRefinements }
)
export default class NoSearchResults extends Component {
  static propTypes = {
    toggleProductsSearchBar: PropTypes.func,
    filterType: PropTypes.bool,
    clearRefinements: PropTypes.func,
    applyRefinements: PropTypes.func,
    isMobile: PropTypes.bool,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  clearAndApplyRefinements = () => {
    const { clearRefinements, applyRefinements } = this.props
    clearRefinements()
    applyRefinements()
  }

  continueOption = () => {
    const { toggleProductsSearchBar, filterType, isMobile } = this.props
    const { l } = this.context

    return isMobile ? (
      filterType ? (
        <Button
          className="NoSearchResults-clearfilters"
          clickHandler={this.clearAndApplyRefinements}
        >{l`clear filters`}</Button>
      ) : (
        <Button
          className="NoSearchResults-searchAgain"
          clickHandler={toggleProductsSearchBar}
        >{l`Search again`}</Button>
      )
    ) : null
  }

  render() {
    const { l } = this.context
    const { filterType } = this.props

    return (
      <div
        className={`NoSearchResults ${
          filterType ? 'NoSearchResults--filterType' : ''
        }`}
      >
        <Helmet title={l`Sorry your search didn’t match any products.`} />
        <Image
          className="NoSearchResults-errorImage"
          alt={l`Sorry your search didn’t match any products.`}
          src={'/assets/{brandName}/images/error.svg'}
        />
        <p className="NoSearchResults-message">{l`Sorry your search didn’t match any products.`}</p>
        {this.continueOption()}
        <Recommendations />
      </div>
    )
  }
}
