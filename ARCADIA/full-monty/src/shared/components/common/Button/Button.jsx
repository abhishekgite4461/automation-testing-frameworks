import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

export default class Button extends Component {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
    isDisabled: PropTypes.bool,
    isActive: PropTypes.bool,
    clickHandler: PropTypes.func,
    type: PropTypes.string,
  }

  handleClick = (e) => {
    const { isDisabled, clickHandler } = this.props
    if (!isDisabled && clickHandler) clickHandler(e)
  }

  render() {
    const { isDisabled, isActive, type, className } = this.props
    const classNames = classnames('Button', className, {
      'is-active': isActive,
    })
    return (
      <button
        className={classNames}
        type={type || 'button'}
        role="button"
        disabled={isDisabled}
        aria-disabled={isDisabled}
        aria-hidden={isDisabled}
        onClick={this.handleClick}
      >
        {this.props.children}
      </button>
    )
  }
}
