import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

import { getWishlistItemCount } from '../../../selectors/wishlistSelectors'
import { isUserAuthenticated } from '../../../selectors/userSelectors'
import { isFeatureWishlistEnabled } from '../../../selectors/featureSelectors'

class WishlistHeaderLink extends Component {
  static propTypes = {
    label: PropTypes.string,
    className: PropTypes.string,
    isAuthenticated: PropTypes.bool,
    isWishlistEnabled: PropTypes.bool,
    itemsCount: PropTypes.number,
  }

  static defaultProps = {
    label: undefined,
    className: '',
    isAuthenticated: false,
    isWishlistEnabled: false,
    itemsCount: 0,
  }

  render() {
    const {
      label,
      isAuthenticated,
      isWishlistEnabled,
      itemsCount,
      className,
    } = this.props
    const isSelected = isAuthenticated && itemsCount > 0

    return isWishlistEnabled ? (
      <Link
        className={`WishlistHeaderLink${className ? ` ${className}` : ``}`}
        to="/wishlist"
      >
        {label && <span className="WishlistHeaderLink-label">{label}</span>}
        <div
          className={`WishlistHeaderLink-icon${
            isSelected ? ' is-selected' : ''
          }`}
        />
      </Link>
    ) : null
  }
}

const mapStateToProps = (state) => ({
  isAuthenticated: isUserAuthenticated(state),
  isWishlistEnabled: isFeatureWishlistEnabled(state),
  itemsCount: getWishlistItemCount(state),
})

export default connect(mapStateToProps)(WishlistHeaderLink)
