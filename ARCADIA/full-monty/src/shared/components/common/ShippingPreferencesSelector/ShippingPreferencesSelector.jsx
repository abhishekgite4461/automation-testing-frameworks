import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import countryList from 'country-list'
import Select from '../FormComponents/Select/Select'
import Button from '../Button/Button'
import * as ShippingDestinationActions from '../../../actions/common/shippingDestinationActions'
import { defaultLanguages, languageOptions } from '../../../constants/languages'
import shippingCountries from '../../../constants/shippingCountries'
import {
  getBrandCode,
  getDefaultLanguage,
} from '../../../selectors/configSelectors'
import { getShippingDestination } from '../../../selectors/shippingDestinationSelectors'
import { error } from '../../../../client/lib/logger'
import GeoIPPixelCookies from '../GeoIP/GeoIPPixelCookies'

export const cantFindCountryHelpLinks = {
  ts:
    'http://help.topshop.com/system/templates/selfservice/topshop/#!portal/403700000001048/article/Prod-2224/Do-you-provide-International-delivery',
  tm:
    'http://help.topman.com/system/templates/selfservice/topman/#!portal/403700000001052/article/Prod-2434/Do-you-provide-International-delivery',
  dp:
    'http://help.dorothyperkins.com/system/templates/selfservice/dorothyperkins/#!portal/403700000001066/article/Prod-2614/Do-you-provide-International-delivery',
  wl:
    'http://help.wallis.co.uk/system/templates/selfservice/wallis/#!portal/403700000001010/article/Prod-2067/Do-you-provide-International-delivery',
  ev:
    'http://help.evans.co.uk/system/templates/selfservice/evans/#!portal/403700000001062/article/Prod-2725/Do-you-provide-International-delivery',
  br:
    'http://help.burton.co.uk/system/templates/selfservice/burton/#!portal/403700000001071/article/Prod-2463/Do-you-provide-International-delivery',
  ms:
    'http://help.missselfridge.com/system/templates/selfservice/missselfridge/#!portal/403700000001057/article/Prod-2599/Do-you-provide-International-delivery',
}

const countries = countryList()

class ShippingPreferencesSelector extends Component {
  static propTypes = {
    brandCode: PropTypes.string.isRequired,
    currentShippingCountry: PropTypes.string.isRequired,
    defaultLanguage: PropTypes.string.isRequired,
    geoIPEnabled: PropTypes.bool,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  state = {
    selectedLanguage: this.props.defaultLanguage,
    selectedShippingCountry: this.props.currentShippingCountry,
    shouldSetGeoIPCookies: false,
  }

  get language() {
    return this.state.selectedLanguage || defaultLanguages[this.shippingCountry]
  }

  get resolvedLanguage() {
    return this.language || this.props.defaultLanguage
  }

  get languages() {
    return languageOptions[this.props.brandCode]
  }

  get shippingCountry() {
    return (
      this.state.selectedShippingCountry || this.props.currentShippingCountry
    )
  }

  get cantFindCountryHelpLink() {
    return cantFindCountryHelpLinks[this.props.brandCode]
  }

  get showLanguageSelector() {
    return defaultLanguages[this.shippingCountry] != null
  }

  handleGeoIPCookieError = (err) => {
    const { selectedShippingCountry, selectedLanguage } = this.state
    const { currentShippingCountry } = this.props

    error(`GeoIP pixel error when setting cookie for all hostnames`, {
      error: new Error(
        'GeoIP pixel error when setting cookie for all hostnames'
      ),
      message: JSON.stringify({
        message: 'GeoIP pixel error when setting cookie for all hostnames',
        sourceURL: window.location.href,
        geoPixelURL: err.nativeEvent.currentTarget.currentSrc,
        selectedShippingCountry,
        selectedLanguage,
        currentShippingCountry,
      }),
    })
  }

  handleGeoIPCookieSuccess = () => {
    this.props.redirect(this.shippingCountry, this.resolvedLanguage)
  }

  handleFormSubmit = (e) => {
    e.preventDefault()
    if (this.props.geoIPEnabled) {
      this.setState({ shouldSetGeoIPCookies: true })
    } else {
      this.handleGeoIPCookieSuccess()
    }
  }

  handleCountrySelectorChange = (e) => {
    this.setState({ selectedShippingCountry: e.target.value })
  }

  handleLanguageSelectorChange = (e) => {
    this.setState({ selectedLanguage: e.target.value })
  }

  renderSubmitButton = (l) => {
    return (
      <div className="ShippingPreferencesSelector-inputGroupButton">
        <Button
          type="submit"
          className="ShippingPreferencesSelector-submitButton"
        >
          {l`Go`}
        </Button>
      </div>
    )
  }

  render() {
    const { l } = this.context
    const { shouldSetGeoIPCookies } = this.state

    return (
      <section className="ShippingPreferencesSelector">
        <header className="ShippingPreferencesSelector-header">
          <div className="ShippingPreferencesSelector-subHeading">
            {l`We ship to over 100 countries`}
          </div>
        </header>

        <form
          className="ShippingPreferencesSelector-form"
          onSubmit={this.handleFormSubmit}
        >
          <div className="ShippingPreferencesSelector-dropdowns">
            <div className="ShippingPreferencesSelector-inputGroup">
              <div className="ShippingPreferencesSelector-inputGroupSelect">
                <Select
                  className="ShippingPreferencesSelector-countrySelector"
                  name="country"
                  options={shippingCountries}
                  value={this.shippingCountry}
                  label={l`Please choose your shipping destination`}
                  onChange={this.handleCountrySelectorChange}
                />
              </div>
              {!this.showLanguageSelector && this.renderSubmitButton(l)}
            </div>
            <a
              className="ShippingPreferencesSelector-cantFindCountryHelpLink"
              href={this.cantFindCountryHelpLink}
            >
              {l`Can't find country?`}
            </a>
            {this.showLanguageSelector && (
              <div className="ShippingPreferencesSelector-inputGroup">
                <div className="ShippingPreferencesSelector-inputGroupSelect">
                  <Select
                    className="ShippingPreferencesSelector-languageSelector"
                    name="language"
                    options={this.languages}
                    value={this.language}
                    label={l`Choose your language`}
                    onChange={this.handleLanguageSelectorChange}
                  />
                </div>
                {this.renderSubmitButton(l)}
              </div>
            )}
          </div>
        </form>

        <div className="ShippingPreferencesSelector-note">
          {l(
            'PLEASE NOTE: If you have items in your basket they will be ' +
              'transferred, but will be subject to local pricing and ' +
              'promotions. Some items may not be suitable for sale in your ' +
              'selected region and will be removed.'
          )}
        </div>
        {shouldSetGeoIPCookies && (
          <GeoIPPixelCookies
            cookieValue={countries.getCode(this.shippingCountry)}
            handleSuccess={this.handleGeoIPCookieSuccess}
            handleError={this.handleGeoIPCookieError}
          />
        )}
      </section>
    )
  }
}

export default connect(
  (state) => ({
    brandCode: getBrandCode(state),
    currentShippingCountry: getShippingDestination(state),
    defaultLanguage: getDefaultLanguage(state),
    geoIPEnabled: state.features.status.FEATURE_GEOIP,
  }),
  {
    redirect: ShippingDestinationActions.redirect,
  }
)(ShippingPreferencesSelector)

export { ShippingPreferencesSelector as WrappedShippingPreferencesSelector }
