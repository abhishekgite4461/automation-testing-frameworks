import { map } from 'ramda'
import testComponentHelper, {
  renderConnectedComponentProps,
} from '../../../../../test/unit/helpers/test-component'
import ShippingPreferencesSelector, {
  WrappedShippingPreferencesSelector,
  cantFindCountryHelpLinks,
} from './ShippingPreferencesSelector'
import {
  getBrandCode,
  getDefaultLanguage,
} from '../../../selectors/configSelectors'
import { getDeliveryCountries } from '../../../selectors/siteOptionsSelectors'
import { getShippingDestination } from '../../../selectors/shippingDestinationSelectors'
import GeoIPPixelCookies from '../GeoIP/GeoIPPixelCookies'

jest.mock('../../../selectors/siteOptionsSelectors')
jest.mock('../../../selectors/configSelectors')
jest.mock('../../../lib/change-shipping-destination')
jest.mock('../../../selectors/shippingDestinationSelectors')

const mockProps = {
  brandCode: 'ts',
  currentShippingCountry: 'France',
  defaultLanguage: 'English',
  redirect: jest.fn(),
  geoIPEnabled: true,
}

function renderComponent(props) {
  const component = testComponentHelper(
    WrappedShippingPreferencesSelector,
    {},
    {
      mockBrowserEventListening: false,
    }
  )(props)

  const $ = {
    countrySelector: '.ShippingPreferencesSelector-countrySelector',
    languageSelector: '.ShippingPreferencesSelector-languageSelector',
    submitButton: '.ShippingPreferencesSelector-submitButton',
    form: '.ShippingPreferencesSelector-form',
  }

  return {
    ...component,
    ...map((selector) => () => component.wrapper.find(selector), $),
    getCountrySelector() {
      return component.wrapper.find($.countrySelector)
    },
  }
}

function MockEvent(value) {
  return {
    preventDefault() {},
    target: {
      value,
    },
  }
}

describe('ShippingPreferencesSelector', () => {
  beforeEach(() => jest.clearAllMocks())

  describe('@renders', () => {
    describe('the country selector', () => {
      it('is populated with the specified shipping countries', () => {
        const { countrySelector } = renderComponent(mockProps)
        expect(countrySelector().prop('options')).toMatchSnapshot()
      })

      describe('when no country has been selected', () => {
        it('is rendered with the current shipping country as selected', () => {
          const { countrySelector } = renderComponent(mockProps)
          expect(countrySelector().prop('value')).toBe(
            mockProps.currentShippingCountry
          )
        })
      })

      describe('when a country has been selected', () => {
        it('is rendered with the selected shipping country as the selected option', () => {
          const {
            countrySelector,
            getCountrySelector,
            wrapper,
          } = renderComponent(mockProps)
          countrySelector().prop('onChange')(MockEvent('Germany'))
          wrapper.update()
          expect(getCountrySelector().prop('value')).toBe('Germany')
        })
      })
    })

    describe('the language selector', () => {
      describe('when the selected country is flagged to show languages', () => {
        it('is rendered with the specified brand languages ans options', () => {
          const { languageSelector } = renderComponent(mockProps)
          expect(languageSelector()).toHaveLength(1)
        })

        describe('when no language has been explicitely selected', () => {
          it('is rendered with the default language selected', () => {
            const props = { ...mockProps, currentShippingCountry: 'Germany' }
            const { languageSelector } = renderComponent(props)
            expect(
              languageSelector()
                .first()
                .prop('value')
            ).toEqual('English')
          })
        })

        describe('when a language has been explicitely selected', () => {
          it('is rendered with the selected language as the selected option', () => {
            const props = { ...mockProps, currentShippingCountry: 'Germany' }
            const { languageSelector, wrapper } = renderComponent(props)
            languageSelector().prop('onChange')(MockEvent('French'))
            wrapper.update()

            expect(
              languageSelector()
                .first()
                .prop('value')
            ).toEqual('French')
          })
        })
      })

      describe('when the selected country is flagged to not show languages', () => {
        it('does not render the language selector', () => {
          const props = { ...mockProps, currentShippingCountry: 'South Africa' }
          const { languageSelector } = renderComponent(props)
          expect(languageSelector()).toHaveLength(0)
        })
      })
    })

    describe('the `cant find country` help link', () => {
      it('links to the appropriate brand help link', () => {
        const { wrapper } = renderComponent(mockProps)
        const actual = wrapper
          .find('.ShippingPreferencesSelector-cantFindCountryHelpLink')
          .prop('href')
        expect(actual).toEqual(cantFindCountryHelpLinks[mockProps.brandCode])
      })
    })
  })

  describe('@events', () => {
    describe('when the submit button is clicked', () => {
      describe('and both a country and language has been selected', () => {
        it('calls the `redirect` action, sets the state and renders the GeoIPPixelCookies', () => {
          const {
            form,
            countrySelector,
            languageSelector,
            wrapper,
          } = renderComponent(mockProps)

          countrySelector()
            .first()
            .prop('onChange')(MockEvent('France'))
          languageSelector().prop('onChange')(MockEvent('French'))
          form().prop('onSubmit')({ preventDefault() {} })
          wrapper.update()

          expect(wrapper.state().shouldSetGeoIPCookies).toBe(true)
          expect(wrapper.find(GeoIPPixelCookies).length).toBe(1)
        })
      })

      describe('and only a country has been selected', () => {
        it('calls the `redirect` action with the selected country and the default language', () => {
          const { form, countrySelector, wrapper } = renderComponent(mockProps)

          countrySelector().prop('onChange')(MockEvent('Germany'))
          form().prop('onSubmit')({ preventDefault() {} })
          wrapper.update()

          expect(wrapper.state().shouldSetGeoIPCookies).toBe(true)
          expect(wrapper.find(GeoIPPixelCookies).length).toBe(1)
        })
      })

      it('no geoIp pixel when geoIP feature is disabled', () => {
        const { form, countrySelector, wrapper } = renderComponent({
          ...mockProps,
          geoIPEnabled: false,
        })

        countrySelector().prop('onChange')(MockEvent('Germany'))

        form().prop('onSubmit')({ preventDefault() {} })
        expect(wrapper.find(GeoIPPixelCookies).length).toBe(0)
      })
    })
  })

  describe('@connected', () => {
    it('wraps the ShippingPreferencesSelector', () => {
      expect(ShippingPreferencesSelector.WrappedComponent).toBe(
        WrappedShippingPreferencesSelector
      )
    })

    describe('mapping to props', () => {
      const countries = ['United Kindom', 'South Africa']
      const state = {
        features: {
          status: {
            FEATURE_GEOIP: false,
          },
        },
      }

      beforeEach(() => {
        getBrandCode.mockReturnValue('foo')
        getDeliveryCountries.mockReturnValue(countries)
        getShippingDestination.mockReturnValue('United Kingdom')
        getDefaultLanguage.mockReturnValue('English')
      })

      describe('prop: brandCode', () => {
        it('is correctly mapped', () => {
          const { brandCode } = renderConnectedComponentProps(
            ShippingPreferencesSelector,
            state
          )
          expect(brandCode).toBe('foo')
        })
      })
    })
  })
})
