import Swatch from '../Swatch'
import testComponentHelper from 'test/unit/helpers/test-component'
import { Link } from 'react-router'

import { dispatchQubitEventHook } from '../../../../lib/analytics/qubit-analytics'

jest.mock('../../../../lib/analytics/qubit-analytics', () => ({
  dispatchQubitEventHook: jest.fn(),
}))

const renderComponent = testComponentHelper(Swatch)

const onSelectSpy = jest.fn()

const props = {
  imageUrl: 'imageUrl',
  colourName: 'colourName',
  index: 0,
  selected: true,
  swatchProduct: {
    productId: '123456',
  },
  onSelect: onSelectSpy,
  seoUrl: 'seo-url',
}

beforeEach(() => {
  onSelectSpy.mockReset()
})

describe('<Swatch />', () => {
  describe('@render', () => {
    it('renders in the default state', () => {
      const { getTree } = renderComponent()
      expect(getTree()).toMatchSnapshot()
    })

    it('renders a `Link` to the productId', () => {
      const { wrapper } = renderComponent({ ...props })
      expect(
        wrapper
          .find('.Swatch-link')
          .first()
          .prop('to')
      ).toBe('seo-url')
    })

    it('renders an image', () => {
      const { wrapper } = renderComponent({ ...props })
      expect(
        wrapper
          .find('.Swatch-linkImage')
          .first()
          .prop('src')
      ).toBe(props.imageUrl)
      expect(
        wrapper
          .find('.Swatch-linkImage')
          .first()
          .prop('alt')
      ).toBe(props.colourName)
    })

    it('applies selected class when selected', () => {
      const { wrapper } = renderComponent({ ...props })
      expect(wrapper.hasClass('is-selected')).toBe(true)
    })
  })

  describe('@events', () => {
    it('`onSelectSpy` is called when `<Link />` is clicked and `onSelect` prop is passed into component', () => {
      const { wrapper } = renderComponent({ ...props })
      const onClickObject = { preventDefault: () => {} }

      wrapper.find(Link).prop('onClick')(onClickObject)

      expect(onSelectSpy).toHaveBeenCalledTimes(1)
      expect(onSelectSpy).toHaveBeenCalledWith(onClickObject, props.index)
      expect(dispatchQubitEventHook).toHaveBeenCalledTimes(1)
    })
  })
})
