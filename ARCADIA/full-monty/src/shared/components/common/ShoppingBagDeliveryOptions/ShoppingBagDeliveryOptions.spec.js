import testComponentHelper from 'test/unit/helpers/test-component'
import ShoppingBagDeliveryOptions from './ShoppingBagDeliveryOptions'
import Select from '../FormComponents/Select/Select'

describe('<ShoppingBagDeliveryOptions />', () => {
  const renderComponent = testComponentHelper(ShoppingBagDeliveryOptions)
  const initialProps = {
    changeDeliveryType: jest.fn(),
    shoppingBag: {
      bag: {
        deliveryOptions: [
          {
            deliveryOptionId: 45020,
            label: 'Collect From Store Express £3.00',
            selected: false,
            enabled: true,
          },
          {
            deliveryOptionId: 51017,
            label: 'Collect From Store Today £3.00',
            selected: false,
            enabled: true,
          },
          {
            deliveryOptionId: 26504,
            label: 'Standard Delivery £4.00',
            selected: true,
            enabled: true,
          },
          {
            deliveryOptionId: 45019,
            label: 'Free Collect From Store Standard £0.00',
            selected: false,
            enabled: true,
          },
          {
            deliveryOptionId: 28002,
            label: 'Next or Named Day Delivery £6.00',
            selected: false,
            enabled: true,
          },
          {
            deliveryOptionId: 50517,
            label: 'Collect from ParcelShop £4.00',
            selected: false,
            enabled: true,
          },
        ],
      },
    },
    isCFSIEnabled: false,
    isPUDOEnabled: true,
    deliveryDayAvailability: {
      CFSiDay: 'today',
      expressDeliveryDay: true,
      homeExpressDeliveryDay: true,
      parcelCollectDay: true,
    },
  }
  describe('@renders', () => {
    it('in default state', () => {
      const { wrapper, getTree } = renderComponent(initialProps)
      expect(wrapper.find(Select).prop('options').length).toBe(5)
      expect(getTree()).toMatchSnapshot()
    })
    it('should display collect today when feature CFSi is true', () => {
      const { wrapper, getTree } = renderComponent({
        ...initialProps,
        isCFSIEnabled: true,
      })
      expect(wrapper.find(Select).prop('options').length).toBe(6)
      expect(getTree()).toMatchSnapshot()
    })
    it('should not display ParcelShop option when PUDO feature is disabled', () => {
      const { wrapper, getTree } = renderComponent({
        ...initialProps,
        isPUDOEnabled: false,
      })
      expect(wrapper.find(Select).prop('options').length).toBe(4)
      expect(getTree()).toMatchSnapshot()
    })
  })
  describe('instance methods', () => {
    describe('selectedValue()', () => {
      it('should return selected deliveryOptionId if any value is selected', () => {
        const { instance } = renderComponent(initialProps)
        expect(
          instance.selectedValue(instance.props.shoppingBag.bag.deliveryOptions)
        ).toBe('26504')
      })
    })
  })
  describe('@events', () => {
    describe('onChange Select', () => {
      it('should call changedeliveryType() with the changed value', () => {
        const event = {
          target: {
            value:
              initialProps.shoppingBag.bag.deliveryOptions[2].deliveryOptionId,
          },
        }
        const { wrapper, instance } = renderComponent(initialProps)
        expect(instance.props.changeDeliveryType).not.toHaveBeenCalled()
        wrapper.find(Select).simulate('change', event)
        expect(instance.props.changeDeliveryType).toHaveBeenCalledTimes(1)
        expect(instance.props.changeDeliveryType).toHaveBeenLastCalledWith({
          deliveryOptionId: event.target.value,
        })
      })
    })
  })
  describe('@functions', () => {
    describe('@manipulateData', () => {
      const deliveryDayAvailability = {
        CFSiDay: 'Monday',
        expressDeliveryDay: false,
        homeExpressDeliveryDay: false,
        parcelCollectDay: false,
      }
      const { instance } = renderComponent({
        ...initialProps,
        isCFSIEnabled: true,
        isPUDOEnabled: true,
        deliveryDayAvailability,
      })
      it('store collection today option is disabled if CFSi is not today', () => {
        const option = [
          {
            label: 'Collect from Store Today',
            deliveryOptionId: 'id',
          },
        ]
        const expected = [
          {
            value: 'id',
            disabled: true,
            label: 'Collect from Store Today',
          },
        ]
        expect(instance.manipulateData(option)).toEqual(expected)
      })
      it('express delivery option is disabled if home express delivery is false', () => {
        const option = [
          {
            label: 'Express Delivery',
            deliveryOptionId: 'id',
          },
        ]
        const expected = [
          {
            value: 'id',
            disabled: true,
            label: 'Express Delivery',
          },
        ]
        expect(instance.manipulateData(option)).toEqual(expected)
      })
      it('express collect from store option is disabled if express delivery is false', () => {
        const option = [
          {
            label: 'Collect from Store Express',
            deliveryOptionId: 'id',
          },
        ]
        const expected = [
          {
            value: 'id',
            disabled: true,
            label: 'Collect from Store Express',
          },
        ]
        expect(instance.manipulateData(option)).toEqual(expected)
      })
      it('parcel shop option is disabled if parcel collect is false', () => {
        const option = [
          {
            label: 'Collect from ParcelShop',
            deliveryOptionId: 'id',
          },
        ]
        const expected = [
          {
            value: 'id',
            disabled: true,
            label: 'Collect from ParcelShop',
          },
        ]
        expect(instance.manipulateData(option)).toEqual(expected)
      })
      it('should disable delivery option if enabled flag is false', () => {
        const option = [
          {
            deliveryOptionId: 28002,
            label: 'Next or Named Day Delivery £6.00',
            enabled: false,
          },
        ]
        const expected = [
          {
            value: 28002,
            disabled: true,
            label: 'Next or Named Day Delivery £6.00',
          },
        ]
        expect(instance.manipulateData(option)).toEqual(expected)
      })
    })
  })
})
