import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Select from '../FormComponents/Select/Select'
import { isDeliveryOptionDisabled } from '../../../lib/is-delivery-disabled'

export default class ShoppingBagDeliveryOptions extends Component {
  static propTypes = {
    shoppingBag: PropTypes.object.isRequired,
    changeDeliveryType: PropTypes.func.isRequired,
    isCFSIEnabled: PropTypes.bool.isRequired,
    isPUDOEnabled: PropTypes.bool.isRequired,
    deliveryDayAvailability: PropTypes.object,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  onChangeHandler = (e) => {
    const { changeDeliveryType } = this.props
    const deliveryOptionId = e.target.value
    changeDeliveryType({ deliveryOptionId })
  }

  selectedValue = (deliveryOptions) => {
    const selectedDeliveryOption = deliveryOptions.find(
      (option) => option.selected === true
    )
    // scrAPI returns no selected option in EU
    return (
      selectedDeliveryOption &&
      selectedDeliveryOption.deliveryOptionId.toString()
    )
  }

  // @NOTE good candidate for a selector function
  manipulateData(options) {
    const { isCFSIEnabled, isPUDOEnabled, deliveryDayAvailability } = this.props
    return options.reduce((result, opt) => {
      if (
        (!isCFSIEnabled && opt.label.search(/(?:Store.*Today)/) !== -1) ||
        (!isPUDOEnabled && opt.label.search(/(?:ParcelShop.*)/) !== -1)
      ) {
        return result
      }
      return [
        ...result,
        {
          value: opt.deliveryOptionId,
          label: opt.label,
          disabled:
            isCFSIEnabled && opt.enabled
              ? isDeliveryOptionDisabled(opt.label, deliveryDayAvailability)
              : !opt.enabled,
        },
      ]
    }, [])
  }

  render() {
    const { l } = this.context
    const {
      shoppingBag: { bag },
    } = this.props
    return (
      <div>
        {bag.deliveryOptions.length ? (
          <div className="ShoppingBagDeliveryOptions-select">
            <Select
              className="ShoppingBagDeliveryOptions-selectMenu"
              name="miniBagDeliveryType"
              value={this.selectedValue(bag.deliveryOptions)}
              onChange={this.onChangeHandler}
              options={this.manipulateData(bag.deliveryOptions)}
            />
          </div>
        ) : (
          <p className="ShoppingBagDeliveryOptions-message">
            {l`There are no additional delivery options available`}
          </p>
        )}
      </div>
    )
  }
}
