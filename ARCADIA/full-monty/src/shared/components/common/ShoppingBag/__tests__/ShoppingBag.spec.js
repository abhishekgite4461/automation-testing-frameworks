import React from 'react'
import { shallow } from 'enzyme'
import configureStore from 'redux-mock-store'
import ShoppingBag from '../ShoppingBag'
import testComponentHelper from 'test/unit/helpers/test-component'
import OrderProducts from '../../OrderProducts/OrderProducts'
import CmsWrapper from '../../../containers/CmsWrapper/CmsWrapper'

const renderComponent = testComponentHelper(ShoppingBag.WrappedComponent)

describe('<ShoppingBag />', () => {
  describe('Connected component', () => {
    it('should receive the correct props', () => {
      const initialState = {
        routing: {
          location: {
            pathname: '/checkout',
          },
        },
      }
      const container = shallow(
        <ShoppingBag store={configureStore()(initialState)} />
      )

      expect(container).toBeTruthy()
      expect(container.prop('inCheckout')).toBe(true)
    })
  })

  describe('Render', () => {
    it('renders OrderProducts', () => {
      const props = {
        className: 'class',
        bagProducts: [],
        scrollMinibag: () => {},
        inCheckout: false,
      }
      const { wrapper } = renderComponent(props)
      const OrderProductsComponent = wrapper.find(OrderProducts)
      expect(OrderProductsComponent).toHaveLength(1)
      expect(OrderProductsComponent.prop('allowMoveToWishlist')).toEqual(true)
    })

    it('renders CmsWrapper when not in checkout', () => {
      const props = {
        bagProducts: [],
        scrollMinibag: () => {},
        inCheckout: false,
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find(CmsWrapper)).toHaveLength(1)
    })

    it('does not render CmsWrapper when in checkout', () => {
      const props = {
        bagProducts: [],
        scrollMinibag: () => {},
        inCheckout: true,
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find(CmsWrapper)).toHaveLength(0)
    })
  })
})
