import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as ModalActions from '../../../actions/common/modalActions'
import OrderProducts from '../OrderProducts/OrderProducts'
import CmsWrapper from '../../containers/CmsWrapper/CmsWrapper'
import { getShoppingBagProductsWithInventory } from '../../../selectors/inventorySelectors'
import espots from '../../../constants/espotsMobile'

import cmsConsts from '../../../constants/cmsConsts'

// Qubit Wrapper
import QubitReact from 'qubit-react/wrapper'

@connect(
  (state) => ({
    bagProducts: getShoppingBagProductsWithInventory(state),
    inCheckout: state.routing.location.pathname.startsWith('/checkout'),
  }),
  { ...ModalActions }
)
export default class ShoppingBag extends Component {
  static propTypes = {
    bagProducts: PropTypes.array,
    className: PropTypes.string,
    inCheckout: PropTypes.bool,
    scrollMinibag: PropTypes.func,
  }

  render() {
    const { className, bagProducts, scrollMinibag, inCheckout } = this.props
    return (
      <QubitReact id="qubit-shoppingbagdelivery">
        <div className={`ShoppingBag${className ? ` ${className}` : ''}`}>
          <OrderProducts
            products={bagProducts}
            scrollOnEdit={scrollMinibag}
            allowEmptyBag={!inCheckout}
            allowMoveToWishlist
            shouldProductsLinkToPdp
          />
          {!this.props.inCheckout && (
            <CmsWrapper
              params={{
                cmsPageName: espots.shoppingBag[0],
                contentType: cmsConsts.ESPOT_CONTENT_TYPE,
              }}
            />
          )}
        </div>
      </QubitReact>
    )
  }
}
