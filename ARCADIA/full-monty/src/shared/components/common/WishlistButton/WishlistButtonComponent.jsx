import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import WishListIcon from '../WishList/WishListIcon'

class WishlistButtonComponent extends Component {
  static propTypes = {
    isSelected: PropTypes.bool,
    modifier: PropTypes.oneOf(['plp', 'pdp', 'quickview', 'minibag', 'bundle']),
    onClick: PropTypes.func,
    renderRemoveFromWishlistText: PropTypes.func,
    renderAddToWishlistText: PropTypes.func,
  }

  render() {
    const {
      modifier,
      renderRemoveFromWishlistText,
      renderAddToWishlistText,
      isSelected,
      onClick,
    } = this.props
    return (
      <button
        className={classNames(
          `WishlistButton`,
          modifier ? `WishlistButton--${modifier}` : null
        )}
        onClick={onClick}
      >
        <div>
          <span className="WishlistButton-icon">
            <WishListIcon modifier={modifier} isSelected={isSelected} />
          </span>
          {isSelected &&
            renderRemoveFromWishlistText &&
            renderRemoveFromWishlistText()}
          {!isSelected && renderAddToWishlistText && renderAddToWishlistText()}
        </div>
      </button>
    )
  }
}

export default WishlistButtonComponent
