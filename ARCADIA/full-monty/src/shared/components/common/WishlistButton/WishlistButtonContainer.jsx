import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import WishlistButtonComponent from './WishlistButtonComponent'

import WishlistLoginModal from '../../containers/Wishlist/WishlistLoginModal'

import {
  triggerWishlistLoginModal,
  addToWishlist,
  removeProductFromWishlist,
} from '../../../actions/common/wishlistActions'

class WishlistButtonContainer extends Component {
  static propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    isAddedToWishlist: PropTypes.bool.isRequired,
    modifier: PropTypes.oneOf(['plp', 'pdp', 'quickview', 'bundle', 'minibag'])
      .isRequired,
    productId: PropTypes.number.isRequired,
    addToWishlist: PropTypes.func.isRequired,
    triggerWishlistLoginModal: PropTypes.func.isRequired,
    removeProductFromWishlist: PropTypes.func.isRequired,
    getDisplaySelectedOverride: PropTypes.func,
    onClickPreHook: PropTypes.func,
    afterAddToWishlist: PropTypes.func,
    onAuthenticationPreHook: PropTypes.func,
    onCancelLogin: PropTypes.func,
    renderRemoveFromWishlistText: PropTypes.func,
    renderAddToWishlistText: PropTypes.func,
  }

  static defaultProps = {
    isAuthenticated: false,
    isAddedToWishlist: false,
    onClickPreHook: null,
    getDisplaySelectedOverride: null,
    removeProductFromWishlistOverride: null,
    afterAddToWishlist: null,
    onAuthenticationPreHook: null,
    onCancelLogin: null,
  }

  // Local property to avoid multiple requests
  pendingRequest = false

  // @NOTE isAddedToWishlist will be used only to initialize the 'selected' state
  constructor(props) {
    super(props)
    this.state = {
      isInWishlist: props.isAddedToWishlist,
    }
  }

  componentWillReceiveProps(nextProps) {
    // @NOTE Syncing local state `isInWishlist` with `isAddedToWishlist`
    if (
      this.props.isAddedToWishlist !== nextProps.isAddedToWishlist &&
      nextProps.isAddedToWishlist !== this.state.isInWishlist
    ) {
      this.setState({
        isInWishlist: nextProps.isAddedToWishlist,
      })
    }
  }

  handleWishlistClick = (e) => {
    e.preventDefault()
    const { isInWishlist } = this.state
    const {
      productId,
      isAuthenticated,
      triggerWishlistLoginModal,
      modifier,
      onAuthenticationPreHook,
      onClickPreHook,
      afterAddToWishlist,
      onCancelLogin,
    } = this.props

    if (this.pendingRequest) return
    if (onClickPreHook) onClickPreHook()

    if (isAuthenticated) {
      return isInWishlist
        ? this.removeFromWishlistOptimistic()
        : this.addToWishlistOptimistic()
    }

    const actions = []
    if (onAuthenticationPreHook) actions.push(onAuthenticationPreHook())

    actions.push(
      triggerWishlistLoginModal(
        productId,
        WishlistLoginModal,
        modifier,
        afterAddToWishlist,
        onCancelLogin
      )
    )
    return Promise.all(actions)
  }

  addToWishlistOptimistic = () => {
    const {
      productId,
      addToWishlist,
      modifier,
      afterAddToWishlist,
    } = this.props

    this.pendingRequest = true
    this.setState({ isInWishlist: true })
    return addToWishlist(productId, modifier)
      .then(() => {
        if (afterAddToWishlist) return afterAddToWishlist()
      })
      .catch(() => {
        this.setState({ isInWishlist: false })
      })
      .then(() => {
        this.pendingRequest = false
      })
  }

  removeFromWishlistOptimistic = () => {
    const { productId, modifier, removeProductFromWishlist } = this.props

    this.pendingRequest = true
    this.setState({ isInWishlist: false })
    return removeProductFromWishlist(productId, modifier)
      .catch(() => {
        this.setState({ isInWishlist: true })
      })
      .then(() => {
        this.pendingRequest = false
      })
  }

  getDisplaySelected = () => {
    // logic to determine if this component should display as 'selected'
    // defaults to 'is it in the wishlist', but MinibagWishlistButton provides its own logic for this
    const { getDisplaySelectedOverride } = this.props
    if (getDisplaySelectedOverride) return getDisplaySelectedOverride()
    return this.state.isInWishlist
  }

  render() {
    const {
      modifier,
      renderAddToWishlistText,
      renderRemoveFromWishlistText,
    } = this.props
    return (
      <WishlistButtonComponent
        onClick={this.handleWishlistClick}
        isSelected={this.getDisplaySelected()}
        modifier={modifier}
        renderAddToWishlistText={renderAddToWishlistText}
        renderRemoveFromWishlistText={renderRemoveFromWishlistText}
      />
    )
  }
}

const mapDispatchToProps = {
  triggerWishlistLoginModal,
  addToWishlist,
  removeProductFromWishlist,
}
export default connect(
  () => ({}),
  mapDispatchToProps
)(WishlistButtonContainer)
