import testComponentHelper from 'test/unit/helpers/test-component'

import WishlistButton from '../WishlistButton'

describe('<WishlistButton />', () => {
  const modifier = 'pdp'
  const productId = 12345
  const defaultProps = {
    modifier,
    productId,
    addToWishlist: jest.fn(),
    triggerWishlistLoginModal: jest.fn(),
    removeProductFromWishlist: jest.fn(),
  }

  const renderComponent = testComponentHelper(WishlistButton.WrappedComponent)

  beforeEach(() => {
    jest.clearAllMocks()
  })

  describe('@renders', () => {
    it('should render default state', () => {
      const { getTree } = renderComponent({
        ...defaultProps,
      })
      expect(getTree()).toMatchSnapshot()
    })
  })
})
