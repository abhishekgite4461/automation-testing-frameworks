import React from 'react'
import testComponentHelper from 'test/unit/helpers/test-component'

import WishlistButtonComponent from '../WishlistButtonComponent'

describe('<WishlistButtonComponent />', () => {
  const productId = 123456
  const defaultProps = {
    productId,
    isSelected: false,
    onClick: jest.fn(),
  }

  const renderComponent = testComponentHelper(WishlistButtonComponent)

  beforeEach(() => {
    jest.clearAllMocks()
  })

  describe('@renders', () => {
    it('should render default state', () => {
      const { getTree } = renderComponent({
        ...defaultProps,
      })
      expect(getTree()).toMatchSnapshot()
    })
    it('should render with plp modifier', () => {
      const { getTree } = renderComponent({
        ...defaultProps,
        modifier: 'plp',
      })
      expect(getTree()).toMatchSnapshot()
    })
    it('should render with pdp modifier', () => {
      const { getTree } = renderComponent({
        ...defaultProps,
        modifier: 'pdp',
      })
      expect(getTree()).toMatchSnapshot()
    })
    it('should render with quickview modifier', () => {
      const { getTree } = renderComponent({
        ...defaultProps,
        modifier: 'quickview',
      })
      expect(getTree()).toMatchSnapshot()
    })
    it('should render with minibag modifier', () => {
      const { getTree } = renderComponent({
        ...defaultProps,
        modifier: 'minibag',
      })
      expect(getTree()).toMatchSnapshot()
    })
    it('should render with isSelected=true', () => {
      const { getTree } = renderComponent({
        ...defaultProps,
        isSelected: true,
      })
      expect(getTree()).toMatchSnapshot()
    })
    it('should render renderAddToWishlistText', () => {
      const { getTree } = renderComponent({
        ...defaultProps,
        isSelected: false,
        renderAddToWishlistText: () => <span>Add to wishlist</span>,
        renderRemoveFromWishlistText: () => <span>Remove from wishlist</span>,
      })
      expect(getTree()).toMatchSnapshot()
    })
    it('should render renderRemoveFromWishlistText', () => {
      const { getTree } = renderComponent({
        ...defaultProps,
        isSelected: true,
        renderAddToWishlistText: () => <span>Add to wishlist</span>,
        renderRemoveFromWishlistText: () => <span>Remove from wishlist</span>,
      })
      expect(getTree()).toMatchSnapshot()
    })
  })

  describe('@events', () => {
    describe('on clicking the wishlist button', () => {
      it('should call the onClick callback', async () => {
        const { instance, wrapper } = renderComponent({
          ...defaultProps,
        })

        expect(instance.props.onClick).toHaveBeenCalledTimes(0)
        await wrapper.find('.WishlistButton').simulate('click')
        expect(instance.props.onClick).toHaveBeenCalledTimes(1)
      })
    })
  })
})
