import testComponentHelper from 'test/unit/helpers/test-component'
import React from 'react'
import deepFreeze from 'deep-freeze'

import Product from '../Product'
import ProductQuickview from '../../../containers/ProductQuickview/ProductQuickview'
import ProductQuickViewButton from '../../ProductQuickViewButton/ProductQuickViewButton'
import HistoricalPrice from '../../HistoricalPrice/HistoricalPrice'
import ProductImages from '../../ProductImages/ProductImages'

import * as viewHelpers from '../../../../lib/viewHelper'
import {
  assetsMock,
  attributeBadgesMock,
  attributeBannersMock,
  promoBannersMock,
} from '../../../../../../test/mocks/product-assets-mocks'

import productSwatchPromoMock from '../../../../../../test/mocks/product-swatch-promo'

const touchDetectionSpy = jest.spyOn(viewHelpers, 'touchDetection')

describe('<Product/>', () => {
  const renderComponent = testComponentHelper(Product.WrappedComponent)
  const initialProps = deepFreeze({
    productId: 24393842,
    swatchProducts: {},
    grid: 1,
    name: 'productName',
    productUrl: '/uk/testUrl',
    assets: [],
    isMobile: true,
    isDesktop: false,
    showNewBanners: true,
    sizes: {
      mobile: 1,
      tablet: 1,
      desktop: 1,
    },
    selectedProductSwatches: {},
  })

  describe('@renders', () => {
    beforeEach(() => {
      touchDetectionSpy.mockImplementation(() => false)
    })

    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })

    it('with className', () => {
      expect(
        renderComponent({ ...initialProps, className: 'myClassName' }).getTree()
      ).toMatchSnapshot()
    })

    it('with className for touch device', () => {
      const { wrapper } = renderComponent({ ...initialProps })
      expect(wrapper.hasClass('Product--isTouch')).toBe(false)
    })

    it('with className for non touch devices', () => {
      touchDetectionSpy.mockImplementation(() => true)
      const { wrapper } = renderComponent({ ...initialProps })
      expect(wrapper.hasClass('Product--isTouch')).toBe(true)
    })

    it('with rating', () => {
      const props = {
        rating: 3,
        bazaarVoiceData: {
          average: '4.32',
        },
      }

      expect(
        renderComponent({ ...initialProps, ...props }).getTree()
      ).toMatchSnapshot()
    })

    it('with bazaarVoiceData.average and no rating', () => {
      expect(
        renderComponent({
          ...initialProps,
          bazaarVoiceData: { average: '4.32' },
        }).getTree()
      ).toMatchSnapshot()
    })

    it('with showProductView boolean', () => {
      expect(
        renderComponent({ ...initialProps, showProductView: true }).getTree()
      ).toMatchSnapshot()
    })

    it('with assets', () => {
      const props = {
        assets: [
          {
            assetType: 'IMAGE_SMALL',
            index: 1,
            url:
              'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS09D31JAQA_Thumb_F_1.jpg',
          },
          {
            assetType: 'IMAGE_THUMB',
            index: 1,
            url:
              'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS09D31JAQA_Small_F_1.jpg',
          },
        ],
      }

      expect(
        renderComponent({ ...initialProps, ...props }).getTree()
      ).toMatchSnapshot()
    })

    it('with additionalAssets', () => {
      const props = {
        additionalAssets: [
          {
            assetType: 'IMAGE_ZOOM',
            index: 1,
            url:
              'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS09D31JAQA_Zoom_F_1.jpg',
          },
          {
            assetType: 'IMAGE_2COL',
            index: 1,
            url:
              'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS09D31JAQA_2col_F_1.jpg',
          },
        ],
      }

      expect(
        renderComponent({ ...initialProps, ...props }).getTree()
      ).toMatchSnapshot()
    })

    it('with unitPrice', () => {
      expect(
        renderComponent({ ...initialProps, unitPrice: '30.00' }).getTree()
      ).toMatchSnapshot()
    })

    it('with wasPrice', () => {
      expect(
        renderComponent({ ...initialProps, wasPrice: '25.00' }).getTree()
      ).toMatchSnapshot()
    })

    it('with wasWasPrice', () => {
      expect(
        renderComponent({ ...initialProps, wasWasPrice: '35.00' }).getTree()
      ).toMatchSnapshot()
    })

    it('with rrp', () => {
      expect(
        renderComponent({ ...initialProps, rrp: '40.00' }).getTree()
      ).toMatchSnapshot()
    })

    it('without swatches (do not render measure)', () => {
      expect(
        renderComponent({ ...initialProps, colourSwatches: null }).wrapper.find(
          'Measure'
        ).length
      ).toBe(0)
    })

    it('with 1 swatch (do not render measure)', () => {
      expect(
        renderComponent({
          ...initialProps,
          colourSwatches: new Array(1),
        }).wrapper.find('Measure').length
      ).toBe(0)
    })
    it('with at least 2 swatches (render measure)', () => {
      expect(
        renderComponent({
          ...initialProps,
          colourSwatches: new Array(2),
        }).wrapper.find('Measure').length
      ).toBe(1)
    })

    it('should render ProductImages with correct sizes prop', () => {
      const { wrapper } = renderComponent(initialProps)
      expect(wrapper.find(ProductImages).prop('sizes')).toEqual(
        initialProps.sizes
      )
    })

    it('should be able to not show quickview option', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        showPrice: false,
      })
      expect(wrapper.find(HistoricalPrice).isEmpty()).toBe(true)
    })

    it('should render if `isBundleOrOutfit` and `isMobile` are both `false`', () => {
      expect(
        renderComponent({
          ...initialProps,
          isBundleOrOutfit: false,
          isMobile: false,
        }).getTree()
      ).toMatchSnapshot()
    })

    it('should add a className to center align price', () => {
      const { instance } = renderComponent(initialProps)
      expect(instance.addClassNamesToHistoricalPrice(1, true)).toContain(
        'HistoricalPrice--center'
      )
    })

    it('should render ProductAttributeBanner', () => {
      const component = renderComponent({
        ...initialProps,
        additionalAssets: attributeBannersMock,
      })
      expect(component.getTree()).toMatchSnapshot()
    })

    it('should render ProductPromoBanner', () => {
      const component = renderComponent({
        ...initialProps,
        additionalAssets: promoBannersMock,
      })
      expect(component.getTree()).toMatchSnapshot()
    })

    it('should not render Banners in 3 column view for mobile viewport', () => {
      expect(
        renderComponent({
          ...initialProps,
          isMobile: true,
          grid: 3,
          additionalAssets: [...attributeBannersMock, ...promoBannersMock],
        }).getTree()
      ).toMatchSnapshot()
    })

    it('should hide product meta', () => {
      expect(
        renderComponent({ ...initialProps, hideProductMeta: true }).getTree()
      ).toMatchSnapshot()
    })

    it('should hide product name', () => {
      expect(
        renderComponent({ ...initialProps, hideProductName: true }).getTree()
      ).toMatchSnapshot()
    })

    it('should hide quick view icon', () => {
      expect(
        renderComponent({ ...initialProps, hideQuickView: true }).getTree()
      ).toMatchSnapshot()
    })

    it('when is bundle', () => {
      expect(
        renderComponent({ ...initialProps, isBundleOrOutfit: true }).getTree()
      ).toMatchSnapshot()
    })

    describe('with Wishlist Feature Flag enabled', () => {
      it('displays wishlist button', () => {
        expect(
          renderComponent({
            ...initialProps,
            isFeatureWishlistEnabled: true,
          }).getTree()
        ).toMatchSnapshot()
      })

      it('does not show wishlist button for bundles', () => {
        expect(
          renderComponent({
            ...initialProps,
            isFeatureWishlistEnabled: true,
            isBundleOrOutfit: true,
          }).getTree()
        ).toMatchSnapshot()
      })

      it('does not show wishlist button for carousel items', () => {
        expect(
          renderComponent({
            ...initialProps,
            isFeatureWishlistEnabled: true,
            isCarouselItem: true,
          }).getTree()
        ).toMatchSnapshot()
      })
    })
  })

  describe('@instance methods', () => {
    describe('getters', () => {
      describe('attributeBanner()', () => {
        it('returns undefined if no attribute banner is found in additionalAssets', () => {
          const { instance } = renderComponent(initialProps)
          expect(instance.attributeBanner).toBe(undefined)
        })

        it('returns attribute banner mobile size if found in additionalAssets', () => {
          const { instance } = renderComponent({
            ...initialProps,
            additionalAssets: attributeBannersMock,
          })
          expect(instance.attributeBanner).toEqual({
            assetType: 'IMAGE_BANNER_MOBILE',
            index: 1,
            url:
              'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color8/cms/pages/static/static-0000109785/images/Banner_Mobile_Petite3.png',
          })
        })

        it('returns badge banner mobile size if there is no attribute banner in additionalAssets', () => {
          const { instance } = renderComponent({
            ...initialProps,
            additionalAssets: attributeBadgesMock,
          })
          expect(instance.attributeBanner).toEqual({
            assetType: 'IMAGE_BADGE_MOBILE',
            index: 1,
            url:
              'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color8/cms/pages/static/static-0000109785/images/Badge_Mobile_Petite3.png',
          })
        })
      })

      describe('promoBanner()', () => {
        it('returns undefined if no promo banner is found in assets or additionalAssets', () => {
          const { instance } = renderComponent(initialProps)
          expect(instance.promoBanner).toBe(undefined)
        })

        // @NOTE to be deleted
        describe('when using ScrAPI', () => {
          it('returns undefined when IMAGE_PROMO_GRAPHIC indexes an attribute banner', () => {
            const { instance } = renderComponent({
              ...initialProps,
              assets: [
                {
                  assetType: 'IMAGE_PROMO_GRAPHIC',
                  index: 1,
                  url:
                    'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/static/static-0000109335/images/Banner_Mobile_promo_basicjersey_uk.svg',
                },
              ],
            })
            expect(instance.promoBanner).toBe(undefined)
          })

          it('returns promoBanner', () => {
            const { instance } = renderComponent({
              ...initialProps,
              assets: assetsMock,
            })
            expect(instance.promoBanner).toEqual({
              assetType: 'IMAGE_PROMO_GRAPHIC',
              index: 1,
              url:
                'http://media.topshop.com/wcsstore/Topshop/images/category_icons/promo_code_627587_mobile.png',
            })
          })

          it('returns undefined when a discounted swatch product is chosen', () => {
            const { instance } = renderComponent({
              ...initialProps,
              ...productSwatchPromoMock,
              productId: '27980029',
              swatchProducts: { '27980029': { selected: 4 } },
            })

            expect(instance.promoBanner).toBe(undefined)
          })

          it('returns promoBanner when a swatch product is not discounted', () => {
            const { instance } = renderComponent({
              ...initialProps,
              ...productSwatchPromoMock,
              productId: '27980029',
              swatchProducts: { '27980029': { selected: 0 } },
            })

            expect(instance.promoBanner).toEqual({
              assetType: 'IMAGE_PROMO_GRAPHIC_MOBILE',
              index: 1,
              url:
                'http://media.burton.co.uk/wcsstore/Burton/images/category_icons/promo_code_560596_mobile.png',
            })
          })
        })

        it('returns mobile size promoBanner if found when using coreAPI', () => {
          const { instance } = renderComponent({
            ...initialProps,
            additionalAssets: promoBannersMock,
            grid: 4,
          })
          expect(instance.promoBanner).toEqual({
            assetType: 'IMAGE_PROMO_GRAPHIC_MOBILE',
            index: 1,
            url:
              'http://media.topshop.com/wcsstore/Topshop/images/category_icons/promo_code_627587_mobile.png',
          })
        })
      })
    })
    describe('renderSwatches', () => {
      it('default case', () => {
        const { instance } = renderComponent(initialProps)
        expect(
          testComponentHelper(
            instance.renderSwatches(
              5,
              'colourSwatches',
              'parentProductId',
              'parentProductUrl',
              'name'
            )
          )().getTree()
        ).toMatchSnapshot()
      })
      it('with componentWidth', () => {
        const { instance } = renderComponent(initialProps)
        expect(
          testComponentHelper(
            instance.renderSwatches(
              5,
              'colourSwatches',
              'parentProductId',
              'parentProductUrl',
              'name'
            )
          )({ componentWidth: 500 }).getTree()
        ).toMatchSnapshot()
      })
    })
    describe('calSwatchWidth', () => {
      it('return the correct swatch size for mobile', () => {
        const SWATCH_WIDTH = 34
        const isDesktop = false
        const { instance } = renderComponent({
          ...initialProps,
          additionalAssets: attributeBadgesMock,
          grid: 2,
        })
        expect(instance.calSwatchWidth(isDesktop)).toEqual(SWATCH_WIDTH)
      })
    })
    describe('calMaxNumOfSwatches', () => {
      it('returns correct number of swatches to fit inside a parent element', () => {
        const PARENT_ELEMENT_WIDTH = 140
        const isDesktop = false
        const { instance } = renderComponent({
          ...initialProps,
          additionalAssets: attributeBadgesMock,
          grid: 2,
        })
        expect(
          instance.calMaxNumOfSwatches(PARENT_ELEMENT_WIDTH, isDesktop)
        ).toEqual(4)
      })
    })
  })

  describe('@events', () => {
    describe('on quick view button click with the selected swatch', () => {
      it('should open quick view with product id if no swatch was previously selected', () => {
        const productId = 123456
        const setProductQuickviewMock = jest.fn()
        const setProductIdQuickviewMock = jest.fn()
        const showModalMock = jest.fn()
        const { wrapper } = renderComponent({
          ...initialProps,
          productId,
          isMobile: false,
          setProductQuickview: setProductQuickviewMock,
          setProductIdQuickview: setProductIdQuickviewMock,
          showModal: showModalMock,
          showProductView: true,
        })
        wrapper.find(ProductQuickViewButton).prop('onClick')()

        expect(setProductQuickviewMock).toHaveBeenCalledWith({})
        expect(setProductIdQuickviewMock).toHaveBeenCalledWith(productId)
        expect(showModalMock).toHaveBeenCalledWith(<ProductQuickview />, {
          mode: 'plpQuickview',
        })
      })

      it('should open quick view with swatch id if a swatch was previously selected', () => {
        const productId = 123456
        const swatchId = 998877
        const setProductQuickviewMock = jest.fn()
        const setProductIdQuickviewMock = jest.fn()
        const showModalMock = jest.fn()
        const { wrapper } = renderComponent({
          ...initialProps,
          productId,
          isMobile: false,
          setProductQuickview: setProductQuickviewMock,
          setProductIdQuickview: setProductIdQuickviewMock,
          showModal: showModalMock,
          showProductView: true,
          selectedProductSwatches: {
            [productId]: swatchId,
          },
        })
        wrapper.find(ProductQuickViewButton).prop('onClick')()

        expect(setProductQuickviewMock).toHaveBeenCalledWith({})
        expect(setProductIdQuickviewMock).toHaveBeenCalledWith(swatchId)
        expect(showModalMock).toHaveBeenCalledWith(<ProductQuickview />, {
          mode: 'plpQuickview',
        })
      })
    })

    describe('on product link click', () => {
      it('should call preserveScroll with current global.window.scrollY (0)', () => {
        const { instance, wrapper } = renderComponent({
          ...initialProps,
          preserveScroll: jest.fn(),
        })

        wrapper
          .find('.Product-link')
          .first()
          .simulate('click')

        expect(instance.props.preserveScroll).toHaveBeenCalledTimes(1)
        expect(instance.props.preserveScroll).lastCalledWith(0)
      })

      it('should call `onLinkClick` with product ID', () => {
        const onLinkClickMock = jest.fn()
        const { wrapper } = renderComponent({
          ...initialProps,
          preserveScroll: () => {},
          onLinkClick: onLinkClickMock,
        })
        wrapper.find('.Product-link').prop('onClick')()
        expect(onLinkClickMock).toHaveBeenCalledWith(24393842)
      })
    })
  })
})
