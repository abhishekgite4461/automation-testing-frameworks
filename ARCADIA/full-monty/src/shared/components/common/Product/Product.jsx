import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { divide, isNil } from 'ramda'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import classnames from 'classnames'
import QubitReact from 'qubit-react/wrapper'

import ProductImages from '../ProductImages/ProductImages'
import ProductAttributeBanner from '../ProductAttributeBanner/ProductAttributeBanner'
import ProductPromoBanner from '../ProductPromoBanner/ProductPromoBanner'
import RatingImage from '../RatingImage/RatingImage'
import HistoricalPrice from '../HistoricalPrice/HistoricalPrice'
import ProductQuickview from '../../containers/ProductQuickview/ProductQuickview'
import ProductQuickViewButton from '../ProductQuickViewButton/ProductQuickViewButton'
import WishlistButton from '../WishlistButton/WishlistButton'

import Swatches from '../Swatches/Swatches'

import { PROMO_BANNER } from '../../../constants/productAssetTypes'
import { imageSizesPropTypes } from '../../../constants/amplience'

import {
  mobileAttBadge,
  mobileAttBanner,
  mobilePromoBanner,
} from '../../../lib/products-utils'
import { touchDetection } from '../../../lib/viewHelper'
import { getRouteFromUrl } from '../../../lib/get-product-route'
import { Measure } from '../../../lib'
import { removeQuery } from '../../../lib/query-helper'

import { preserveScroll } from '../../../actions/common/infinityScrollActions'
import * as modalActions from '../../../actions/common/modalActions'
import * as productsActions from '../../../actions/common/productsActions'

import { isMobile, isDesktop } from '../../../selectors/viewportSelectors'
import { getSelectedProductSwatches } from '../../../selectors/productSelectors'
import { isProductViewSelected } from '../../../selectors/productViewsSelectors'
import {
  isFeatureWishlistEnabled,
  isFeatureLogBadAttributeBannersEnabled,
} from '../../../selectors/featureSelectors'

@connect(
  (state) => ({
    swatchProducts: state.swatches.products,
    isFeatureWishlistEnabled: isFeatureWishlistEnabled(state),
    isMobile: isMobile(state),
    isDesktop: isDesktop(state),
    selectedProductSwatches: getSelectedProductSwatches(state),
    productViewSelected: isProductViewSelected(state),
    isFeatureLogBadAttributeBannersEnabled: isFeatureLogBadAttributeBannersEnabled(
      state
    ),
  }),
  { ...modalActions, preserveScroll, ...productsActions }
)
export default class Product extends Component {
  constructor(props) {
    super(props)

    this.isTouchEnabled = touchDetection()
    this.state = {
      swatchSize: {
        desktop: {
          width: 28, // This should match $SwatchSizeDesktop in Swatch.css
          padding: 10, // This should match $SwatchMarginDesktop in Swatch.css
        },
        mobile: {
          width: 22, // This should match $SwatchSizeMobile Swatch.css
          padding: 6, // This should match $SwatchMarginMobile in Swatch.css
        },
      },
    }
  }

  static propTypes = {
    lazyLoad: PropTypes.bool,
    isImageFallbackEnabled: PropTypes.bool,
    productId: PropTypes.number,
    swatchProducts: PropTypes.object.isRequired,
    grid: PropTypes.number.isRequired,
    productUrl: PropTypes.string.isRequired,
    showPrice: PropTypes.bool,
    isFeatureWishlistEnabled: PropTypes.bool,
    isMobile: PropTypes.bool.isRequired,
    isDesktop: PropTypes.bool.isRequired,
    hideProductMeta: PropTypes.bool,
    hideProductName: PropTypes.bool,
    hideQuickView: PropTypes.bool,
    bazaarVoiceData: PropTypes.object,
    className: PropTypes.string,
    assets: PropTypes.array, //eslint-disable-line
    wasPrice: PropTypes.string,
    wasWasPrice: PropTypes.string,
    rrp: PropTypes.string,
    additionalAssets: PropTypes.array,
    colourSwatches: PropTypes.array,
    rating: PropTypes.number,
    selectedProductSwatches: PropTypes.object.isRequired,
    preserveScroll: PropTypes.func,
    showModal: PropTypes.func,
    setProductIdQuickview: PropTypes.func,
    setProductQuickview: PropTypes.func,
    onLinkClick: PropTypes.func,
    productViewSelected: PropTypes.bool,
    sizes: imageSizesPropTypes.isRequired,
    isCarouselItem: PropTypes.bool,
  }

  static defaultProps = {
    isImageFallbackEnabled: false,
    lazyLoad: false,
    assets: [],
    additionalAssets: [],
    showPrice: true,
    hideProductMeta: false,
    hideProductName: false,
    hideQuickView: false,
    isCarouselItem: false,
    onLinkClick: () => {},
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  // @NOTE using only mobile sizes for banners
  // we treat both image_banners and image_badges as attribute banners
  // however, banners always take precedence
  get attributeBanner() {
    const { additionalAssets } = this.getProductInfo()

    const attributeBanner = additionalAssets.find(mobileAttBanner)
    const attributeBadge = additionalAssets.find(mobileAttBadge)

    return attributeBanner || attributeBadge
  }

  // @NOTE use only mobile sizes for promo banners
  get promoBanner() {
    const { assets, additionalAssets } = this.getProductInfo()

    // @NOTE scrAPIPromoBanner to be DELETED once rollout is complete
    // ScrAPI indexes the promo banners as IMAGE_PROMO_GRAPHIC on the assets property
    // Need to validate that asset url is an actual promoBanner after verifying
    // strange behaviour on scrAPI where the promoBanner was indexed by copying
    // the mobile attribute banner
    const scrAPIPromoBanner = assets.find(({ assetType, url }) => {
      return assetType === PROMO_BANNER && url.includes('/promo_code_') // <= MonkeyPatch alert!
    })

    const coreAPIPromoBanner = additionalAssets.find(mobilePromoBanner)

    return scrAPIPromoBanner || coreAPIPromoBanner
  }

  getProductInfo() {
    const {
      colourSwatches,
      productId,
      wasPrice,
      wasWasPrice,
      rrp,
      additionalAssets,
      rating,
      bazaarVoiceData,
      swatchProducts,
    } = this.props
    const swatchProduct = swatchProducts[productId] || {}

    // Get the current index of the swatch - default 0
    const swatchIndex = swatchProduct.selected || 0

    // If there are swatches, and there is one at the index we expect
    if (colourSwatches && colourSwatches[swatchIndex]) {
      // Overwrite props with swatchProduct info
      const data = {
        ...colourSwatches[swatchIndex].swatchProduct,
        parentProductId: productId,
      }

      // MONKEY PATCH
      // If we are on the first swatch product, we can use the parents value for wasPrice and wasWasPrice
      if (wasPrice && !swatchIndex) {
        data.rrp = rrp
        data.wasPrice = wasPrice
        data.wasWasPrice = wasWasPrice
        data.additionalAssets = additionalAssets
      }

      return data
    }

    // MONKEY PATCH #2
    // If the API is returning bazaarVoiceData instead of rating, copy accross the average value
    if (!rating && bazaarVoiceData && bazaarVoiceData.average) {
      return {
        ...this.props,
        rating: bazaarVoiceData.average,
      }
    }

    // No swatches, or bad index - just use the props
    return this.props
  }

  linkClickHandler = () => {
    const { productId, preserveScroll, onLinkClick } = this.props
    const scrollYPos = window.scrollY || window.pageYOffset
    preserveScroll(scrollYPos)
    onLinkClick(productId)
  }

  openQuickView = () => {
    const {
      selectedProductSwatches,
      productId,
      setProductIdQuickview,
      setProductQuickview,
      showModal,
    } = this.props
    // We explicitly set the product quick view state to an empty object here
    // in order to aid our analytics decorator on ProductQuickView to know
    // that a product is being quick-viewed. This helps with cases where you
    // quick view the same product sequentially.
    setProductQuickview({})
    const quickviewProductId = selectedProductSwatches[productId] || productId
    setProductIdQuickview(quickviewProductId)

    showModal(<ProductQuickview />, { mode: 'plpQuickview' })
  }

  addClassNamesToHistoricalPrice(colIndex, centerAlign) {
    const className = 'HistoricalPrice'
    const classNames = [`${className}--product`, `${className}--col${colIndex}`]

    if (centerAlign) {
      classNames.push(`${className}--center`)
    }
    return classNames.join(' ')
  }

  calSwatchWidth = (isDesktop) => {
    const { desktop, mobile } = this.state.swatchSize

    return isDesktop
      ? desktop.width + desktop.padding * 2
      : mobile.width + mobile.padding * 2
  }

  // Calculate the maximum number of swatches that can fit into the parent container
  calMaxNumOfSwatches = (width, isDesktop) => {
    const SWATCH_ELEMENT_WIDTH = this.calSwatchWidth(isDesktop)
    // Minimum number of swatches to render
    const MIN_NUM_OF_SWATCHES = 1

    return isNil(width)
      ? MIN_NUM_OF_SWATCHES
      : // Divide parent container width into SWATCH_ELEMENT_WIDTH to get Max number of swatches
        Math.floor(divide(width, SWATCH_ELEMENT_WIDTH))
  }

  renderSwatches = (
    grid,
    colourSwatches,
    parentProductId,
    parentProductUrl,
    name
  ) => ({ componentWidth }) => {
    const { isDesktop } = this.props
    const maxSwatches = this.calMaxNumOfSwatches(componentWidth, isDesktop)

    return (
      <div className={`Product-swatches Product-swatches--col${grid}`}>
        <Swatches
          swatches={colourSwatches}
          maxSwatches={maxSwatches}
          productId={parentProductId}
          productUrl={parentProductUrl}
          name={name}
        />
      </div>
    )
  }

  render() {
    const {
      lazyLoad,
      className,
      productViewSelected,
      grid,
      colourSwatches,
      productUrl: parentProductUrl,
      showPrice,
      isFeatureWishlistEnabled,
      isMobile,
      hideProductMeta,
      hideProductName,
      hideQuickView,
      sizes,
      isCarouselItem,
      isImageFallbackEnabled,
      isFeatureLogBadAttributeBannersEnabled,
    } = this.props

    const {
      name,
      unitPrice,
      assets,
      productId,
      productBaseImageUrl,
      outfitBaseImageUrl,
      parentProductId,
      rating,
      wasPrice,
      wasWasPrice,
      rrp,
      additionalAssets,
      productUrl,
      isBundleOrOutfit,
    } = this.getProductInfo()

    const productClassName = classnames(
      'Product',
      `Product--col${grid}`,
      className,
      {
        'Product--isTouch': this.isTouchEnabled,
      }
    )
    const productRoute =
      removeQuery(productUrl) || getRouteFromUrl(parentProductUrl)
    const shouldRenderBanners = !isMobile || !(grid === 3)
    const isWishlistButtonEnabled =
      isFeatureWishlistEnabled && !(isBundleOrOutfit || isCarouselItem)
    return (
      <div className={productClassName}>
        <div>
          <QubitReact id="qubit-PLP-product-tile" productId={productId}>
            <Link
              className="Product-link"
              to={productRoute}
              onClick={this.linkClickHandler}
            >
              <div className="Product-images">
                {isWishlistButtonEnabled && (
                  <WishlistButton productId={productId} modifier="plp" />
                )}
                <ProductImages
                  isImageFallbackEnabled={isImageFallbackEnabled}
                  lazyLoad={lazyLoad}
                  productId={productId}
                  grid={grid}
                  assets={assets}
                  productBaseImageUrl={productBaseImageUrl}
                  outfitBaseImageUrl={outfitBaseImageUrl}
                  sizes={sizes}
                  additionalAssets={additionalAssets}
                  showProductView={productViewSelected}
                  productDescription={name}
                />
              </div>
              {shouldRenderBanners &&
                this.attributeBanner && (
                  <QubitReact
                    id="Qubit-Product-AttributeProductBanners"
                    productId={productId}
                  >
                    <ProductAttributeBanner
                      src={this.attributeBanner.url}
                      productURL={productUrl}
                      isFeatureLogBadAttributeBannersEnabled={
                        isFeatureLogBadAttributeBannersEnabled
                      }
                    />
                  </QubitReact>
                )}
            </Link>
          </QubitReact>
        </div>
        {!hideProductMeta && (
          <QubitReact id={`PLP-Trending-Product-${productId}`}>
            <div className="Product-meta">
              {shouldRenderBanners &&
                this.promoBanner && (
                  <QubitReact
                    id="Qubit-Product-PromoProductBanners"
                    productId={productId}
                  >
                    <ProductPromoBanner src={this.promoBanner.url} />
                  </QubitReact>
                )}

              <div className={`Product-info Product-info--col${grid}`}>
                {!hideProductName && (
                  <header className="Product-name">
                    <Link
                      className="Product-nameLink"
                      to={productRoute}
                      onClick={this.linkClickHandler}
                    >
                      {name}
                    </Link>
                  </header>
                )}
                {showPrice && (
                  <HistoricalPrice
                    className={this.addClassNamesToHistoricalPrice(
                      grid,
                      hideQuickView
                    )}
                    price={unitPrice}
                    wasPrice={wasPrice}
                    wasWasPrice={wasWasPrice}
                    rrp={rrp}
                  />
                )}
                {rating && (
                  <RatingImage
                    rating={rating}
                    className="Product-ratingImage"
                  />
                )}
                {!hideQuickView &&
                  !isBundleOrOutfit &&
                  !isMobile && (
                    <ProductQuickViewButton onClick={this.openQuickView} />
                  )}
              </div>
            </div>
          </QubitReact>
        )}
        {colourSwatches &&
          colourSwatches.length > 1 && (
            <Measure>
              {this.renderSwatches(
                grid,
                colourSwatches,
                parentProductId,
                parentProductUrl,
                name
              )}
            </Measure>
          )}
      </div>
    )
  }
}
