import AddressPreview from '../AddressPreview'
import testComponentHelper from 'test/unit/helpers/test-component'

const defaultProps = () => {
  return {
    address: {
      address1: 'Flat 46 Sheppard House',
      address2: '120 Oxford Street',
      city: 'London',
      country: 'United Kingdom',
      postcode: 'E2 7AB',
      state: '',
    },
    details: {
      title: 'Mr',
      firstName: 'John',
      lastName: 'Doe',
      telephone: '12345678',
    },
    onChangeButtonClick: jest.fn(),
    brandName: 'topshop',
  }
}

describe(AddressPreview.name, () => {
  const renderComponent = testComponentHelper(AddressPreview)

  describe('@renders', () => {
    it('should render the address details', () => {
      expect(renderComponent(defaultProps()).getTree()).toMatchSnapshot()
    })
  })

  describe('@events', () => {
    describe('AddressPreview-button', () => {
      it('calls onClickChangeButton on click', () => {
        const { instance, wrapper } = renderComponent(defaultProps())
        expect(instance.props.onChangeButtonClick).not.toBeCalled()
        wrapper
          .find('.Button')
          .first()
          .simulate('click')
        expect(instance.props.onChangeButtonClick).toHaveBeenCalledTimes(1)
      })
    })
  })
})
