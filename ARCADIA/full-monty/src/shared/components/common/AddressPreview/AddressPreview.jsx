import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

export default class AddressPreview extends PureComponent {
  static propTypes = {
    address: PropTypes.shape({
      address1: PropTypes.string,
      address2: PropTypes.string,
      city: PropTypes.string,
      country: PropTypes.string,
      postcode: PropTypes.string,
      state: PropTypes.string,
    }),
    details: PropTypes.shape({
      firstName: PropTypes.string,
      lastName: PropTypes.string,
      title: PropTypes.string,
    }),
    onChangeButtonClick: PropTypes.func.isRequired,
    headingIncluded: PropTypes.bool,
    headingTextTranslated: PropTypes.string,
  }

  static defaultProps = {
    headingIncluded: false,
    headingTextTranslated: 'Billing details',
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  renderDetails = (items) => {
    return items.map(([key, value]) => (
      <div key={key}>
        <dt className="AddressPreviewV1-detailsTerm">{key}</dt>
        <dd className="AddressPreviewV1-detailsDescription">{value}</dd>
      </div>
    ))
  }

  render() {
    const { l } = this.context
    const {
      onChangeButtonClick,
      address,
      details,
      headingIncluded,
      headingTextTranslated,
    } = this.props
    const { address1, address2, city, country, postcode, state } = address
    const { firstName, lastName, title } = details
    const detailsItems = [
      ['Title', `${title} ${firstName} ${lastName}`],
      ['Address Line 1', address1],
      ['Address Line 2', address2],
      ['City', city],
      ['Postcode', postcode],
      ['Country', country],
      ['State', state],
    ]

    return (
      <div className="AddressPreviewV1">
        {headingIncluded && (
          <h4 className="AddressPreviewV1-header">{headingTextTranslated}</h4>
        )}
        <dl className="AddressPreview-details">
          {this.renderDetails(detailsItems)}
        </dl>
        <button
          className="Button Button--secondary AddressPreviewV1-button"
          onClick={onChangeButtonClick}
        >
          {l`Change`}
        </button>
      </div>
    )
  }
}
