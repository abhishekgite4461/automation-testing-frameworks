import React, { Component } from 'react'
import PropTypes from 'prop-types'
import QubitReact from 'qubit-react/wrapper'
import cn from 'classnames'
import removeHttp from '../../../lib/remove-http'
import ResponsiveImage from '../ResponsiveImage/ResponsiveImage'
import { imageSizesPropTypes } from '../../../constants/amplience'

const IMAGE_PREFERENCE_ORDER = ['IMAGE_NORMAL', 'IMAGE_LARGE', 'IMAGE_THUMB']
const IMAGE_OUTFIT_REGEX = /IMAGE_OUTFIT/g
const IMAGE_NOT_AVAILABLE_URL =
  '../../../../../assets/common/images/image-not-available.png'

export default class ProductImages extends Component {
  static propTypes = {
    lazyLoad: PropTypes.bool,
    isImageFallbackEnabled: PropTypes.bool,
    assets: PropTypes.array.isRequired,
    productBaseImageUrl: PropTypes.string.isRequired,
    outfitBaseImageUrl: PropTypes.string,
    additionalAssets: PropTypes.array,
    showProductView: PropTypes.bool,
    productDescription: PropTypes.string,
    sizes: imageSizesPropTypes.isRequired,
  }

  static defaultProps = {
    isImageFallbackEnabled: false,
    lazyLoad: false,
    outfitBaseImageUrl: '',
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  constructor(props) {
    super(props)
    this.state = {
      hovered: false,
      loadStates: {
        product: {
          loaded: false,
          errored: false,
        },
        outfit: {
          loaded: false,
          errored: false,
        },
      },
    }
  }

  hasOutfitImage(additionalAssets = []) {
    return additionalAssets.some(({ assetType }) =>
      assetType.match(IMAGE_OUTFIT_REGEX)
    )
  }

  getImageUrl = (assets) => {
    if (!assets.length) {
      return IMAGE_NOT_AVAILABLE_URL
    }

    const image =
      IMAGE_PREFERENCE_ORDER.reduce((selected, targetType) => {
        return (
          selected || assets.find(({ assetType }) => assetType === targetType)
        )
      }, null) || assets[0]

    return removeHttp(image.url)
  }

  getOutfitImage = (url) => {
    const { additionalAssets } = this.props
    return this.hasOutfitImage(additionalAssets)
      ? url.replace(/_F_|_P_/i, '_M_')
      : url
  }

  handleOnLoad = (which) => {
    this.setState({
      loadStates: {
        ...this.state.loadStates,
        [which]: {
          loaded: true,
          errored: false,
        },
      },
    })
  }

  handleOnError = (which) => {
    this.setState({
      loadStates: {
        ...this.state.loadStates,
        [which]: {
          loaded: false,
          errored: true,
        },
      },
    })
  }

  renderProductImage = (url) => {
    if (this.state.loadStates.product.errored) {
      return (
        <ResponsiveImage
          isImageFallbackEnabled
          className="ProductImages-image ProductImages-image--showing"
          src={IMAGE_NOT_AVAILABLE_URL}
        />
      )
    }
    const {
      lazyLoad,
      productBaseImageUrl,
      isImageFallbackEnabled,
      sizes,
      productDescription,
    } = this.props

    const showing = this.state.loadStates.product.loaded
    return (
      <ResponsiveImage
        className={cn([
          'ProductImages-image',
          {
            'ProductImages-image--showing': showing,
          },
        ])}
        isImageFallbackEnabled={isImageFallbackEnabled}
        lazyLoad={lazyLoad}
        amplienceUrl={productBaseImageUrl}
        sizes={sizes}
        src={url}
        alt={productDescription}
        aria-hidden={!showing}
        useProgressiveJPG
        onLoad={() => this.handleOnLoad('product')}
        onError={() => this.handleOnError('product')}
      />
    )
  }

  renderOutfitImage = (url) => {
    if (this.state.loadStates.outfit.errored) {
      return (
        <ResponsiveImage
          isImageFallbackEnabled
          className="ProductImages-image ProductImages-image--showing"
          src={IMAGE_NOT_AVAILABLE_URL}
        />
      )
    }
    const {
      lazyLoad,
      outfitBaseImageUrl,
      sizes,
      productDescription,
    } = this.props
    const { l } = this.context

    const showing = this.state.loadStates.outfit.loaded
    return (
      <ResponsiveImage
        lazyLoad={lazyLoad}
        className={cn([
          'ProductImages-image',
          {
            'ProductImages-image--showing': showing,
          },
        ])}
        amplienceUrl={outfitBaseImageUrl}
        sizes={sizes}
        src={url}
        alt={l`${productDescription} as part of an outfit`}
        aria-hidden={!showing}
        useProgressiveJPG
        onLoad={() => this.handleOnLoad('outfit')}
        onError={() => this.handleOnError('outfit')}
      />
    )
  }

  shouldShowProduct = () => {
    const { additionalAssets, showProductView } = this.props
    // if there's no outfit image, always show the product
    if (!this.hasOutfitImage(additionalAssets)) {
      return true
    }
    return (
      (showProductView && !this.state.hovered) ||
      (!showProductView && this.state.hovered)
    )
  }

  render() {
    const { assets } = this.props

    const url = this.getImageUrl(assets)
    const showProduct = this.shouldShowProduct()

    return (
      <div
        className="ProductImages"
        onMouseEnter={() => this.setState({ hovered: true })}
        onMouseLeave={() => this.setState({ hovered: false })}
      >
        {showProduct ? (
          <QubitReact id={`PLP-Social-Proof-${this.props.productId}`}>
            {this.renderProductImage(url)}
          </QubitReact>
        ) : (
          <QubitReact id={`PLP-Social-Proof-Hover-${this.props.productId}`}>
            {this.renderOutfitImage(this.getOutfitImage(url))}
          </QubitReact>
        )}
      </div>
    )
  }
}
