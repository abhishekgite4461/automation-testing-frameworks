import QubitReact from 'qubit-react/wrapper'
import testComponentHelper from 'test/unit/helpers/test-component'
import ProductImages from '../ProductImages'
import setupConnectionType from '../../../../../../test/unit/helpers/setup-connection-type'

jest.mock('../../../../lib/viewHelper', () => ({
  touchDetection: jest.fn(() => false),
}))

const productImage =
  '//media.topshop.com/wcsstore/TopShop/images/catalog/TS09D31JAQA_2col_F_1.jpg'
const outfitImage =
  '//media.topshop.com/wcsstore/TopShop/images/catalog/TS09D31JAQA_2col_M_1.jpg'

const assets = [
  {
    assetType: 'IMAGE_SMALL',
    index: 1,
    url:
      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS09D31JAQA_Thumb_F_1.jpg',
  },
  {
    assetType: 'IMAGE_THUMB',
    index: 1,
    url:
      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS09D31JAQA_Small_F_1.jpg',
  },
  {
    assetType: 'IMAGE_NORMAL',
    index: 1,
    url: `http:${productImage}`,
  },
  {
    assetType: 'IMAGE_LARGE',
    index: 1,
    url:
      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS09D31JAQA_Zoom_F_1.jpg',
  },
]

const additionalAssets = [
  {
    assetType: 'IMAGE_ZOOM',
    index: 1,
    url:
      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS09D31JAQA_Zoom_F_1.jpg',
  },
  {
    assetType: 'IMAGE_2COL',
    index: 1,
    url:
      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS09D31JAQA_2col_F_1.jpg',
  },
  {
    assetType: 'IMAGE_OUTFIT_SMALL',
    index: 2,
    url:
      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS09D31JAQA_Thumb_M_1.jpg',
  },
  {
    assetType: 'IMAGE_OUTFIT_THUMB',
    index: 2,
    url:
      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS09D31JAQA_Small_M_1.jpg',
  },
  {
    assetType: 'IMAGE_OUTFIT_NORMAL',
    index: 2,
    url: `http:${outfitImage}`,
  },
  {
    assetType: 'IMAGE_OUTFIT_LARGE',
    index: 2,
    url:
      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS09D31JAQA_Large_M_1.jpg',
  },
  {
    assetType: 'IMAGE_OUTFIT_ZOOM',
    index: 2,
    url:
      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS09D31JAQA_Zoom_M_1.jpg',
  },
  {
    assetType: 'IMAGE_OUTFIT_2COL',
    index: 2,
    url:
      'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS09D31JAQA_2col_M_1.jpg',
  },
]

const getResponsiveImage = (w) => w.find(QubitReact).props().children
const isShowingProductImage = (w) => {
  const hasCorrectImgSrc = getResponsiveImage(w).props.src === productImage
  const hasCorrectQubitId =
    w.find(QubitReact).props().id === 'PLP-Social-Proof-100'

  return hasCorrectImgSrc && hasCorrectQubitId
}
const isShowingOutfitImage = (w) => {
  const hasCorrectImgSrc = getResponsiveImage(w).props.src === outfitImage
  const hasCorrectQubitId =
    w.find(QubitReact).props().id === 'PLP-Social-Proof-Hover-100'

  return hasCorrectImgSrc && hasCorrectQubitId
}

/**
 * Test that ResponsiveImage is called correctly rather than testing it's behaviour
 * @param  {ShallowWrapper} wrapper
 * @param  {Object} partialProps
 * @return {undefined}
 */
const expectResponsiveImageWithProps = (wrapper, partialProps) => {
  const respImgProps = getResponsiveImage(wrapper).props
  expect(respImgProps).toEqual(expect.objectContaining(partialProps))
}

describe('<ProductImages/>', () => {
  const props = {
    assets,
    additionalAssets,
    showProductView: true,
    productDescription: 'This is a jeans',
    grid: 3,
    productBaseImageUrl: '//product-image.jpg',
    outfitBaseImageUrl: '//outfit-image.jpg',
    sizes: {
      mobile: 1,
      tablet: 1,
      desktop: 1,
    },
    productId: '100',
    isImageFallbackEnabled: false,
    lazyLoad: true,
  }

  const renderComponent = testComponentHelper(ProductImages)

  it('`showProduct` shows the product image', () => {
    const { wrapper } = renderComponent(props)

    expect(isShowingProductImage(wrapper)).toBeTruthy()
    expectResponsiveImageWithProps(wrapper, {
      isImageFallbackEnabled: props.isImageFallbackEnabled,
      lazyLoad: props.lazyLoad,
      amplienceUrl: props.productBaseImageUrl,
      sizes: props.sizes,
      alt: props.productDescription,
    })
  })

  it('`!showProduct` shows the outfit image', () => {
    const { wrapper } = renderComponent({
      ...props,
      showProductView: false,
    })
    expect(isShowingOutfitImage(wrapper)).toBeTruthy()
  })

  it('hovering on the product image shows the outfit image', () => {
    const { wrapper } = renderComponent(props)

    wrapper.setState({ hovered: true })

    expect(isShowingOutfitImage(wrapper)).toBeTruthy()
  })

  it('hovering on product image and no outfit image still shows product image', () => {
    const { wrapper } = renderComponent({ ...props, additionalAssets: [] })

    wrapper.setState({ hovered: true })

    expect(isShowingProductImage(wrapper)).toBeTruthy()
  })

  it('hovering on outfit image shows the product image', () => {
    const { wrapper } = renderComponent({
      ...props,
      showProductView: false,
    })

    wrapper.setState({ hovered: true })

    expect(isShowingProductImage(wrapper)).toBeTruthy()
  })

  it('shows product image if there are no `additionalAssets` (ignores `showProduct` value)', () => {
    const ps = {
      ...props,
      additionalAssets: [],
      showProduct: false,
    }

    expect(isShowingProductImage(renderComponent(ps).wrapper)).toBeTruthy()
    expect(
      isShowingProductImage(
        renderComponent({ ...ps, showProduct: true }).wrapper
      )
    ).toBeTruthy()
  })

  it('if no assets then show "image not unavailable" image ', () => {
    const { wrapper } = renderComponent({ ...props, assets: [] })

    // TODO why relative path and not absolute?
    expect(getResponsiveImage(wrapper).props.src).toBe(
      '../../../../../assets/common/images/image-not-available.png'
    )
  })

  it("hides image before it's loaded so that it can transition the images appearence", () => {
    setupConnectionType('4g')
    const { wrapper } = renderComponent(props)

    expect(wrapper.find('.ProductImages-image').length).toBe(1)
    expect(wrapper.find('.ProductImages-image--showing').length).toBe(0)
  })
})
