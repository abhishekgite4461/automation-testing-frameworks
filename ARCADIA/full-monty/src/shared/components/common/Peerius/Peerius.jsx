import { Component } from 'react'
import PropTypes from 'prop-types'
import * as peeriusActions from '../../../actions/common/peeriusActions'
import { connect } from 'react-redux'
import * as peeriusTracking from '../../../lib/peerius-tracking'
import { isEmpty } from 'ramda'

@connect(
  (state) => ({
    brandCode: state.config.brandCode,
  }),
  { ...peeriusActions }
)
export default class Peerius extends Component {
  static propTypes = {
    type: PropTypes.string, // eslint-disable-line react/no-unused-prop-types
    trackPeerius: PropTypes.func, // eslint-disable-line react/no-unused-prop-types
  }

  componentWillMount = () => this.track(this.props)

  componentWillReceiveProps = (nextProps) => this.track(nextProps)

  track = (props) => {
    if (!process.browser) return

    const { type, trackPeerius } = props
    const tracking = peeriusTracking[type](props)

    if (tracking) {
      const data = { type: type.toLowerCase() }
      if (!isEmpty(tracking)) data[type] = tracking
      trackPeerius(data)
    }
  }

  render = () => null
}
