import React from 'react'
import Button from '../../Button/Button'
import InputError from '../InputError/InputError'
import PropTypes from 'prop-types'

const FormButton = (
  {
    name,
    error = '',
    isDisabled = false,
    type = 'button',
    onClick,
    children,
    touched = false,
  },
  { l }
) => {
  const componentClassName = `FormComponent-${name} ButtonContainer ButtonContainer-${name}`

  return (
    <div className={componentClassName}>
      {!isDisabled && touched && error ? (
        <InputError name={name} className="Input-validationMessage">
          {l(error)}
        </InputError>
      ) : null}
      <Button isDisabled={isDisabled} type={type} clickHandler={onClick}>
        {children}
      </Button>
    </div>
  )
}

FormButton.propTypes = {
  name: PropTypes.string.isRequired,
  error: PropTypes.string,
  isDisabled: PropTypes.bool,
  type: PropTypes.string,
  onClick: PropTypes.func,
}

FormButton.contextTypes = {
  l: PropTypes.func,
}

export default FormButton
