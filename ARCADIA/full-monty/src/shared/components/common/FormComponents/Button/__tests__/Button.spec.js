import testComponentHelper from 'test/unit/helpers/test-component'

import Button from '../../../Button/Button'
import InputError from '../../InputError/InputError'
import FormButton from '../Button'

describe('FormComponents', () => {
  describe('<Button />', () => {
    const renderComponent = testComponentHelper(FormButton)

    it('should render button wrapped', () => {
      const props = {
        name: 'arcadia',
        isDisabled: true,
        type: 'submit',
        onClick: jest.fn(),
      }
      const { wrapper } = renderComponent({
        ...props,
        children: 'Click Here',
      })

      expect(
        wrapper.find('div').find({
          className:
            'FormComponent-arcadia ButtonContainer ButtonContainer-arcadia',
        })
      ).toHaveLength(1)

      expect(
        wrapper.find(Button).find({
          isDisabled: props.isDisabled,
          type: props.type,
          clickHandler: props.onClick,
        })
      ).toHaveLength(1)
      expect(wrapper.find(InputError)).toHaveLength(0)
    })

    it('should pass custom classNames', () => {
      const props = { name: 'submitButton' }
      const { wrapper } = renderComponent({
        ...props,
        children: 'Click Here',
      })

      expect(
        wrapper.find('div').find({
          className:
            'FormComponent-submitButton ButtonContainer ButtonContainer-submitButton',
        })
      ).toHaveLength(1)
    })

    it('should display <InputError />', () => {
      const props = {
        name: 'submit',
        error: 'This is an error message',
        touched: true,
      }
      const { wrapper } = renderComponent({
        ...props,
        children: 'Click Here',
      })

      expect(
        wrapper.find(InputError).find({
          className: 'Input-validationMessage',
          name: 'submit',
          children: props.error,
        })
      ).toHaveLength(1)
    })
  })
})
