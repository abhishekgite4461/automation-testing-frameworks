import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { path, pathOr } from 'ramda'
import classnames from 'classnames'

import { PrivacyGuard } from '../../../../lib'
import Image from '../../Image/Image'
import InputError from '../InputError/InputError'
import FeatureCheck from '../../FeatureCheck/FeatureCheck'
import * as inputActions from '../../../../actions/components/InputActions'
import { eventBasedAnalytics } from '../../../../lib/analytics/analytics'
import PreloadImages from '../../PreloadImages/PreloadImages'
import {
  sendAnalyticsValidationState,
  GTM_VALIDATION_STATES,
} from '../../../../analytics'
import { isInCheckout as checkIfInCheckout } from '../../../../selectors/routingSelectors'

let inputId = 0

@connect(
  (state) => ({
    inputState: state.input,
    isInCheckout: checkIfInCheckout(state),
  }),
  { ...inputActions, sendAnalyticsValidationState },
  (stateProps, dispatchProps, ownProps) => ({
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
  })
)
export default class Input extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    autocomplete: PropTypes.string,
    autofocus: PropTypes.bool,
    className: PropTypes.string,
    errors: PropTypes.object,
    field: PropTypes.object,
    id: PropTypes.string,
    inputState: PropTypes.object,
    isDisabled: PropTypes.bool,
    isFocused: PropTypes.bool,
    suppressValidationIcon: PropTypes.bool,
    isRequired: PropTypes.bool,
    label: PropTypes.string,
    maxLength: PropTypes.number,
    placeholder: PropTypes.string,
    type: PropTypes.string,
    value: PropTypes.string,
    privacyProtected: PropTypes.bool,
    pattern: PropTypes.string,
    isInCheckout: PropTypes.bool,
    // functions
    setField: PropTypes.func.isRequired,
    touchedField: PropTypes.func.isRequired,
    analyticsOnChange: PropTypes.func,
    toggleInputActive: PropTypes.func,
    toggleShowPassword: PropTypes.func,
    onBlur: PropTypes.func,
    sendAnalyticsValidationState: PropTypes.func.isRequired,
    messagePosition: PropTypes.oneOf(['default', 'bottom']),
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  static defaultProps = {
    className: '',
    id: undefined,
    pattern: undefined,
    suppressValidationIcon: false,
    field: {},
    onBlur: () => {},
    errors: null,
    isInCheckout: false,
    messagePosition: 'default',
  }

  constructor(props) {
    super(props)
    this.id = props.name + inputId++
    this.analyticEventsValue = null
    this.blurred = false
  }

  componentDidMount() {
    // in case input triggered a change event before react was loaded
    this.syncDomValue()

    const isIOS = !!(
      navigator.userAgent.match(/iPhone/i) ||
      navigator.userAgent.match(/iP[ao]d/i)
    )
    if (isIOS) {
      // IOS doesn't trigger react onChange event on autocomplete
      this.inputField.addEventListener('change', this.syncDomValue)
      // IOS doesn't trigger change event on autofill
      this.syncDomValueInterval = setInterval(this.syncDomValue, 100)
    }
  }

  componentDidUpdate(prevProps) {
    const { isFocused, analyticsOnChange } = this.props
    if (prevProps.isFocused !== isFocused && isFocused) {
      this.setFocus()
    }

    const currentValue = path(['field', 'value'], prevProps)
    /**
     * we only want to send analytics events when: blurred, no error, value exists and is a new value
     * need to do here as the errors do not appear until after blur
     * need a flag to show a blur has occurred as componentDidUpdate is frequently called
     */
    if (
      analyticsOnChange &&
      this.blurred &&
      !this.getErrorDomEle() &&
      currentValue &&
      currentValue !== this.analyticEventsValue
    ) {
      this.analyticEventsValue = currentValue
      eventBasedAnalytics(analyticsOnChange())
    }

    this.blurred = false
  }

  componentWillUnmount() {
    this.inputField.removeEventListener('change', this.onChange)
    window.clearInterval(this.syncDomValueInterval)
  }

  setFocus = () => {
    if (!this.inputField) return
    this.inputField.focus()
  }

  syncDomValue = (event) => {
    const { value, name, setField } = this.props
    const domValue = event ? event.target.value : this.inputField.value
    if (domValue) {
      window.clearInterval(this.syncDomValueInterval)
      if (domValue !== value) {
        setField(name)({
          target: {
            value: domValue,
          },
        })
      }
    }
  }

  getErrorDomEle() {
    const { name } = this.props
    return document.getElementById(`${name}-error`)
  }

  handleBlur = (ev) => {
    const {
      field,
      name,
      touchedField,
      onBlur,
      errors,
      isInCheckout,
    } = this.props
    const isTouched = pathOr(false, ['isTouched'], field)
    if (!isTouched) {
      touchedField(name)()
    }
    this.props.toggleInputActive(this.id)
    onBlur(ev)
    if (errors && isInCheckout) {
      this.props.sendAnalyticsValidationState({
        id: this.inputField.id,
        validationStatus: errors[name]
          ? GTM_VALIDATION_STATES.FAILURE
          : GTM_VALIDATION_STATES.SUCCESS,
      })
    }
    this.blurred = true
  }

  handleFocus = () => {
    this.props.toggleInputActive(this.id)
  }

  togglePassword = (e) => {
    e.preventDefault()
    this.props.toggleShowPassword(this.id)
    this.setFocus()
  }

  renderValidationSuccessImage = () => {
    const {
      field,
      errors,
      name,
      value = '',
      suppressValidationIcon,
    } = this.props
    const inputValue = pathOr(value, ['value'], field)
    const isTouched = pathOr(false, ['isTouched'], field)
    return (
      errors &&
      !errors[name] &&
      isTouched &&
      !suppressValidationIcon &&
      inputValue !== '' && (
        <span className="Input-validateIcon Input-validateSuccess">
          <Image
            className={'Image--width100'}
            src="/assets/{brandName}/images/validate-success.svg"
          />
        </span>
      )
    )
  }

  renderValidationErrorImage = () => {
    const { field, errors, name, suppressValidationIcon } = this.props
    const isTouched = pathOr(false, ['isTouched'], field)
    return (
      errors &&
      errors[name] &&
      !suppressValidationIcon &&
      isTouched && (
        <span className="Input-validateIcon Input-validateFail">
          <Image
            className={'Image--width100'}
            src="/assets/{brandName}/images/validate-fail.svg"
          />
        </span>
      )
    )
  }

  renderValidationMessage = (modifier) => {
    const { field, errors, name } = this.props
    const isTouched = pathOr(false, ['isTouched'], field)
    const { l } = this.context
    const classNames = classnames('Input-validationMessage', {
      [`Input-validationMessage--${modifier}`]: modifier,
    })
    return (
      errors &&
      errors[name] &&
      isTouched && (
        <InputError className={classNames} name={name}>
          {l(errors[name])}
        </InputError>
      )
    )
  }

  renderInput = () => {
    const {
      field,
      errors,
      label,
      placeholder,
      type,
      name,
      autocomplete,
      value = '',
      setField,
      isDisabled,
      autofocus,
      maxLength,
      inputState,
      privacyProtected,
      pattern,
      id,
    } = this.props
    const showPassword =
      type === 'password' && path([this.id, 'showPassword'], inputState)
    const inputType = showPassword ? 'text' : type || 'text'
    const inputValue = pathOr(value, ['value'], field)
    // NOTE: give a default pattern if number type, so iOS displays the numeric keyboard on focus
    const inputPattern = pattern || (type === 'number' ? '[0-9]*' : undefined)
    return (
      <PrivacyGuard noProtection={!(showPassword || privacyProtected)}>
        <input
          id={id || `${name}-${inputType}`}
          className={`Input-field Input-field-${name}`}
          placeholder={placeholder}
          type={inputType}
          name={name}
          autoComplete={autocomplete}
          value={inputValue}
          onChange={setField(name)}
          onBlur={this.handleBlur}
          onFocus={this.handleFocus}
          disabled={isDisabled}
          ref={(node) => {
            this.inputField = node
          }}
          autoFocus={autofocus}
          maxLength={maxLength}
          aria-invalid={!!(errors && errors[name])}
          aria-label={label}
          aria-describedby={`${name}-error`}
          pattern={inputPattern}
        />
      </PrivacyGuard>
    )
  }

  render() {
    const {
      field,
      errors,
      label,
      type,
      name,
      value = '',
      isDisabled,
      isRequired,
      inputState,
      className,
      messagePosition,
    } = this.props
    const showPassword =
      type === 'password' && path([this.id, 'showPassword'], inputState)
    const inputType = showPassword ? 'text' : type || 'text'
    const isActive = path([this.id, 'isActive'], inputState)
    const inputValue = pathOr(value, ['value'], field)
    const containerClassName = `${name ? ` FormComponent-${name}` : ''}`
    const isTouched = pathOr(false, ['isTouched'], field)
    const isError = errors && errors[name] && isTouched
    return (
      <div
        className={`${containerClassName} Input Input-${name}${
          isDisabled ? ' is-disabled' : ''
        }${isTouched && inputValue !== '' ? ' is-touched' : ''}${
          isActive ? ' is-active' : ''
        }${isError ? ' is-erroring' : ''} ${className}`}
      >
        <div className="Input-head">
          <label className="Input-label" htmlFor={`${name}-${inputType}`}>
            {label}
            {isRequired && <span className="Input-required">*</span>}
          </label>
          {messagePosition === 'default' && this.renderValidationMessage()}
        </div>
        <div className="Input-row">
          {this.props.type === 'password' ? (
            /* eslint-disable jsx-a11y/no-static-element-interactions */
            <FeatureCheck flag="PASSWORD_SHOW_TOGGLE">
              <PreloadImages
                sources={[
                  '/assets/common/images/password_showing.svg',
                  '/assets/common/images/password_hidden.svg',
                ]}
                render={() => (
                  <div
                    className="Input-toggleButton"
                    onClick={this.togglePassword}
                  >
                    {showPassword ? (
                      <Image
                        className={
                          'Input-revealPasswordImage Input-revealPasswordImage-showing'
                        }
                        src="/assets/common/images/password_showing.svg"
                      />
                    ) : (
                      <Image
                        className={
                          'Input-revealPasswordImage Input-revealPasswordImage-hidden'
                        }
                        src="/assets/common/images/password_hidden.svg"
                      />
                    )}
                  </div>
                )}
              />
            </FeatureCheck>
          ) : /* eslint-enable jsx-a11y/no-static-element-interactions */
          null}
          <div className="Input-container">
            {this.renderInput()}
            {this.renderValidationSuccessImage()}
            {this.renderValidationErrorImage()}
            {messagePosition === 'bottom' &&
              this.renderValidationMessage(messagePosition)}
          </div>
        </div>
      </div>
    )
  }
}
