import testComponentHelper from 'test/unit/helpers/test-component'

import ContentOverlay from '../ContentOverlay'

const noop = () => {}

describe('<ContentOverlay/>', () => {
  const requiredProps = {
    toggleTopNavMenu: noop,
    toggleProductsSearchBar: noop,
    closeMiniBag: noop,
    setTimeout: noop,
    hideSizeGuide: noop,
    sizeGuideOpen: true,
    onModalCancelled: noop,
    onClick: jest.fn(),
  }
  const renderComponent = testComponentHelper(ContentOverlay.WrappedComponent)

  describe('@renders', () => {
    it('should render default state', () => {
      const { getTree } = renderComponent(requiredProps)
      expect(getTree()).toMatchSnapshot()
    })

    it('should have `ContentOverlay--modalOpen` class if `modalOpen` prop is `true`', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        modalOpen: true,
      })
      expect(
        wrapper.find('.ContentOverlay').hasClass('ContentOverlay--modalOpen')
      ).toBe(true)
    })

    it('should not have `is-hidden` class if `showOverlay` prop is `true`', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        showOverlay: true,
      })
      expect(wrapper.find('.ContentOverlay').hasClass('is-hidden')).toBe(false)
    })
  })

  describe('@events', () => {
    describe('onClick', () => {
      it('should not call `onClick` prop when overlay is closed', () => {
        const { wrapper } = renderComponent(requiredProps)
        wrapper.find('div').simulate('click')
        expect(requiredProps.onClick).not.toHaveBeenCalled()
      })

      describe('if `showOverlay` prop is `true', () => {
        it('should call `onClick` prop', () => {
          const { wrapper } = renderComponent({
            ...requiredProps,
            showOverlay: true,
          })
          wrapper.find('div').simulate('click')
          expect(requiredProps.onClick).toHaveBeenCalled()
        })

        it('should stop event propagation', () => {
          const stopPropagationMock = jest.fn()
          const { wrapper } = renderComponent({
            ...requiredProps,
            showOverlay: true,
          })
          wrapper.find('.ContentOverlay').prop('onClick')({
            stopPropagation: stopPropagationMock,
          })
          expect(stopPropagationMock).toHaveBeenCalled()
        })

        describe('`toggleTopNavMenu`', () => {
          it('should not be called by default', () => {
            const setTimeoutMock = jest.fn((fn) => fn())
            const toggleTopNavMenuMock = jest.fn()
            const { wrapper } = renderComponent({
              ...requiredProps,
              showOverlay: true,
              setTimeout: setTimeoutMock,
              toggleTopNavMenu: toggleTopNavMenuMock,
            })
            wrapper.find('.ContentOverlay').prop('onClick')()
            expect(toggleTopNavMenuMock).not.toHaveBeenCalled()
          })

          it('should be called after `10` milliseconds if `topNavMenuOpen` prop is `true`', () => {
            const setTimeoutMock = jest.fn((fn) => fn())
            const toggleTopNavMenuMock = jest.fn()
            const { wrapper } = renderComponent({
              ...requiredProps,
              showOverlay: true,
              topNavMenuOpen: true,
              setTimeout: setTimeoutMock,
              toggleTopNavMenu: toggleTopNavMenuMock,
            })
            wrapper.find('.ContentOverlay').prop('onClick')()
            expect(setTimeoutMock.mock.calls[0][1]).toBe(10)
            expect(toggleTopNavMenuMock).toHaveBeenCalled()
          })
        })

        describe('`toggleProductsSearchBar`', () => {
          it('should not be called by default', () => {
            const toggleProductsSearchBarMock = jest.fn()
            const { wrapper } = renderComponent({
              ...requiredProps,
              showOverlay: true,
              toggleProductsSearchBar: toggleProductsSearchBarMock,
            })
            wrapper.find('.ContentOverlay').prop('onClick')()
            expect(toggleProductsSearchBarMock).not.toHaveBeenCalled()
          })

          it('should be called if `productsSearchOpen` prop is `true`', () => {
            const toggleProductsSearchBarMock = jest.fn()
            const { wrapper } = renderComponent({
              ...requiredProps,
              showOverlay: true,
              productsSearchOpen: true,
              toggleProductsSearchBar: toggleProductsSearchBarMock,
            })
            wrapper.find('.ContentOverlay').prop('onClick')()
            expect(toggleProductsSearchBarMock).toHaveBeenCalled()
          })
        })

        describe('clearMovingProductToWishlist', () => {
          it('should not be called by default', () => {
            const clearMovingProductToWishlist = jest.fn()
            const { wrapper } = renderComponent({
              ...requiredProps,
              showOverlay: true,
              clearMovingProductToWishlist,
              isMovingProductToWishlist: false,
            })
            wrapper.find('.ContentOverlay').prop('onClick')()
            expect(clearMovingProductToWishlist).not.toHaveBeenCalled()
          })

          it('should be called if `isMovingProductToWishlist` is true', () => {
            const clearMovingProductToWishlist = jest.fn()
            const { wrapper } = renderComponent({
              ...requiredProps,
              showOverlay: true,
              clearMovingProductToWishlist,
              isMovingProductToWishlist: true,
            })
            wrapper.find('.ContentOverlay').prop('onClick')()
            expect(clearMovingProductToWishlist).toHaveBeenCalled()
          })
        })

        describe('`sizeGuide`', () => {
          it('should not be called by default', () => {
            const setTimeoutMock = jest.fn((fn) => fn())
            const hideSizeGuideMock = jest.fn()
            const { wrapper } = renderComponent({
              ...requiredProps,
              showOverlay: true,
              sizeGuideOpen: false,
              setTimeout: setTimeoutMock,
              hideSizeGuide: hideSizeGuideMock,
            })
            wrapper.find('.ContentOverlay').prop('onClick')()
            expect(hideSizeGuideMock).not.toHaveBeenCalled()
          })

          it('should be called after `10` milliseconds if `topNavMenuOpen` prop is `true`', () => {
            const setTimeoutMock = jest.fn((fn) => fn())
            const hideSizeGuideMock = jest.fn()
            const { wrapper } = renderComponent({
              ...requiredProps,
              showOverlay: true,
              sizeGuideOpen: true,
              setTimeout: setTimeoutMock,
              hideSizeGuide: hideSizeGuideMock,
            })
            wrapper.find('.ContentOverlay').prop('onClick')()
            expect(setTimeoutMock.mock.calls[0][1]).toBe(10)
            expect(hideSizeGuideMock).toHaveBeenCalled()
          })
        })

        describe('closeMiniBag', () => {
          it('should be called after `10` milliseconds by default', () => {
            const setTimeoutMock = jest.fn((fn) => fn())
            const closeMiniBagMock = jest.fn()
            const { wrapper } = renderComponent({
              ...requiredProps,
              showOverlay: true,
              setTimeout: setTimeoutMock,
              closeMiniBag: closeMiniBagMock,
            })
            wrapper.find('.ContentOverlay').prop('onClick')()
            expect(setTimeoutMock.mock.calls[0][1]).toBe(10)
            expect(closeMiniBagMock).toHaveBeenCalled()
          })

          it('should not be called after if `modalOpen` is `true`', () => {
            const setTimeoutMock = jest.fn((fn) => fn())
            const closeMiniBagMock = jest.fn()
            const { wrapper } = renderComponent({
              ...requiredProps,
              showOverlay: true,
              modalOpen: true,
              setTimeout: setTimeoutMock,
              closeMiniBag: closeMiniBagMock,
            })
            wrapper.find('.ContentOverlay').prop('onClick')()
            expect(closeMiniBagMock).not.toHaveBeenCalled()
          })
        })

        describe('setModalCancelled', () => {
          it('should be called if `modalOpen` is true', () => {
            const setModalCancelledMock = jest.fn()
            const { wrapper } = renderComponent({
              ...requiredProps,
              showOverlay: true,
              modalOpen: true,
              setModalCancelled: setModalCancelledMock,
            })
            wrapper.find('.ContentOverlay').prop('onClick')()
            expect(setModalCancelledMock).toHaveBeenCalledWith(true)
          })
        })
      })
    })
  })
})
