import PropTypes from 'prop-types'
import { compose } from 'ramda'
import React from 'react'
import { connect } from 'react-redux'

import {
  decorator as productReferrals,
  REFERRER_TYPES,
} from '../../../lib/analytics/product-referrals-helper'
import { deleteRecentlyViewedProduct } from '../../../actions/common/recentlyViewedActions'
import ProductCarousel from '../ProductCarousel/ProductCarousel'
import { getRecentlyViewedProductsWithAmplienceUrl } from '../../../selectors/recentlyViewedSelectors'
import { getBrandName } from '../../../selectors/common/configSelectors'
import {
  sendAnalyticsProductClickEvent,
  GTM_LIST_TYPES,
} from '../../../analytics'

const RecentlyViewed = (props, { l }) => {
  const {
    recentlyViewed,
    currentProductId,
    registerReferral,
    deleteRecentlyViewedProduct,
    sendAnalyticsProductClickEvent,
    brand,
  } = props
  const carouselProducts = recentlyViewed.filter(
    ({ productId }) => productId !== currentProductId
  )

  const handleLinkClick = (productId) => {
    registerReferral(productId)

    const index = carouselProducts.findIndex(
      (product) => product.productId === productId
    )
    const {
      name,
      productId: id,
      unitPrice: price,
      iscmCategory: category,
    } = carouselProducts[index]
    sendAnalyticsProductClickEvent({
      listType: GTM_LIST_TYPES.PDP_RECENTLY_VIEWED,
      name,
      id,
      price,
      brand,
      category,
      position: index + 1,
    })
  }

  return carouselProducts.length ? (
    <div className="RecentlyViewed">
      <h3 className="RecentlyViewed-header">{l`Recently viewed`}</h3>
      <ProductCarousel
        products={carouselProducts}
        hideProductMeta
        onProductLinkClick={handleLinkClick}
        onProductRemove={deleteRecentlyViewedProduct}
      />
    </div>
  ) : null
}

RecentlyViewed.propTypes = {
  recentlyViewed: PropTypes.arrayOf(
    PropTypes.shape({
      productId: PropTypes.number,
      name: PropTypes.string,
      imageUrl: PropTypes.string,
      amplienceUrl: PropTypes.string,
    })
  ),
  currentProductId: PropTypes.number,
  registerReferral: PropTypes.func,
  brand: PropTypes.string.isRequired,
  // actions
  deleteRecentlyViewedProduct: PropTypes.func.isRequired,
  sendAnalyticsProductClickEvent: PropTypes.func.isRequired,
}

RecentlyViewed.defaultProps = {
  recentlyViewed: [],
  currentProductId: undefined,
  registerReferral: () => {},
}

RecentlyViewed.contextTypes = {
  l: PropTypes.func,
}

export default compose(
  productReferrals(REFERRER_TYPES.RECENTLY_VIEWED),
  connect(
    (state) => ({
      recentlyViewed: getRecentlyViewedProductsWithAmplienceUrl(state),
      brand: getBrandName(state),
    }),
    { deleteRecentlyViewedProduct, sendAnalyticsProductClickEvent }
  )
)(RecentlyViewed)

export { RecentlyViewed as WrappedRecentlyViewed }
