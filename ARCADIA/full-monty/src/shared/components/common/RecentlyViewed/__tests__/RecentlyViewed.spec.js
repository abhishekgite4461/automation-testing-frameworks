import testComponentHelper from 'test/unit/helpers/test-component'

import RecentlyViewed, { WrappedRecentlyViewed } from '../RecentlyViewed'
import ProductCarousel from '../../ProductCarousel/ProductCarousel'

describe('<RecentlyViewed />', () => {
  const singleRecentlyViewed = [
    {
      productId: 30348471,
      name: 'MOTO Indigo Holding Power Jeans',
      amplienceUrl: 'http://www.images.com',
      imageUrl:
        'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS02P03MIND_Small_F_1.jpg',
      unitPrice: '40.00',
      iscmCategory: 'JEAN',
    },
  ]
  const recentlyViewed = [
    {
      productId: 30348471,
      name: 'MOTO Indigo Holding Power Jeans',
      amplienceUrl: 'http://www.images.com',
      imageUrl:
        'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS02P03MIND_Small_F_1.jpg',
      unitPrice: '40.00',
      iscmCategory: 'JEAN',
    },
    {
      productId: 29434229,
      name: 'Marble Reversible Crop Bikini Top',
      amplienceUrl: 'http://www.images.com',
      imageUrl:
        'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS03P13MLIL_Small_F_1.jpg',
      unitPrice: '10.00',
      iscmCategory: 'SWIM',
    },
  ]
  const requiredProps = {
    deleteRecentlyViewedProduct: () => {},
    sendAnalyticsProductClickEvent: () => {},
    brand: 'topshop',
  }
  const renderComponent = testComponentHelper(WrappedRecentlyViewed)

  beforeEach(jest.resetAllMocks)

  describe('@decorators', () => {
    describe('productReferrals', () => {
      it('should have a `registerReferral` prop if `referringProduct` is supplied', () => {
        const { wrapper } = testComponentHelper(RecentlyViewed)({
          referringProduct: {},
        })
        expect(wrapper.prop('registerReferral')).toBeDefined()
      })
    })
  })

  describe('@renders', () => {
    it('should render nothing in default state', () => {
      const { wrapper } = renderComponent(requiredProps)
      expect(wrapper.isEmptyRender()).toBe(true)
    })

    describe('if `recentlyViewed` prop supplied', () => {
      it('should render recently viewed items', () => {
        const { getTree } = renderComponent({
          ...requiredProps,
          recentlyViewed,
        })
        expect(getTree()).toMatchSnapshot()
      })

      it('should omit currently viewed product', () => {
        const { wrapper } = renderComponent({
          ...requiredProps,
          recentlyViewed,
          currentProductId: recentlyViewed[1].productId,
        })
        expect(wrapper.find(ProductCarousel).prop('products')).toEqual([
          recentlyViewed[0],
        ])
      })

      it('should render nothing if the single `recentlyViewed` is currently view product', () => {
        const { wrapper } = renderComponent({
          ...requiredProps,
          recentlyViewed: singleRecentlyViewed,
          currentProductId: 30348471,
        })
        expect(wrapper.isEmptyRender()).toBe(true)
      })
    })
  })

  describe('@events', () => {
    describe('on product link click', () => {
      it('should call `registerReferral`', () => {
        const registerReferralMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          recentlyViewed,
          registerReferral: registerReferralMock,
        })
        wrapper.find(ProductCarousel).prop('onProductLinkClick')(30348471)
        expect(registerReferralMock).toHaveBeenCalledWith(30348471)
      })

      it('should track the click event with the correct product info', () => {
        const trackingSpy = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          recentlyViewed,
          sendAnalyticsProductClickEvent: trackingSpy,
        })
        const index = 1
        wrapper.find(ProductCarousel).prop('onProductLinkClick')(
          recentlyViewed[index].productId
        )
        const { name, productId, unitPrice, iscmCategory } = recentlyViewed[
          index
        ]
        expect(trackingSpy).toHaveBeenCalledWith({
          listType: 'PDP Recently Viewed',
          name,
          id: productId,
          price: unitPrice,
          category: iscmCategory,
          position: index + 1,
          brand: requiredProps.brand,
        })
      })
    })

    describe('on product remove', () => {
      it('should call `deleteRecentlyViewedProduct`', () => {
        const deleteRecentlyViewedProductMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          recentlyViewed,
          deleteRecentlyViewedProduct: deleteRecentlyViewedProductMock,
        })
        wrapper.find(ProductCarousel).prop('onProductRemove')(1247392)
        expect(deleteRecentlyViewedProductMock).toHaveBeenCalledWith(1247392)
      })
    })
  })
})
