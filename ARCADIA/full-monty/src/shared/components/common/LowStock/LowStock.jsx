import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class LowStock extends Component {
  static propTypes = {
    activeItem: PropTypes.object.isRequired,
    stockThreshold: PropTypes.number.isRequired,
  }

  static defaultProps = {
    stockThreshold: 3,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  render() {
    const { l } = this.context
    const {
      activeItem: { quantity },
      stockThreshold,
    } = this.props
    const lowStockValue = stockThreshold === 0 ? 3 : stockThreshold

    if (!quantity || quantity === 0 || quantity > lowStockValue) {
      return null
    }

    return <div className="LowStock">{l`Only ${quantity} left in stock`}</div>
  }
}
