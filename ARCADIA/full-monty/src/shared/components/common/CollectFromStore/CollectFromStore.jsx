import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { isEmpty } from 'ramda'

import UserLocatorInput from '../UserLocatorInput/UserLocatorInput'
import GoogleMap from '../StoreLocator/GoogleMap'
import StoreList from '../StoreLocator/StoreList'

import { getStoresForCheckoutModal } from '../../../actions/components/StoreLocatorActions'
import {
  selectDeliveryStore,
  setStoreUpdating,
} from '../../../actions/common/checkoutActions'
import { closeModal } from '../../../actions/common/modalActions'

import { getAppliedFilters } from '../../../lib/store-locator-utilities'
import storeLocatorConsts from '../../../constants/storeLocator'

@connect(
  (state) => ({
    stores: state.storeLocator.stores,
    storeQuery: state.storeLocator.query,
    filters: state.storeLocator.filters,
    isStoresLoading: state.storeLocator.loading,
    isMobile: state.viewport.media === 'mobile',
    selectedPlaceDetails: state.userLocator.selectedPlaceDetails,
  }),
  {
    getStoresForCheckoutModal,
    selectDeliveryStore,
    setStoreUpdating,
    closeModal,
  }
)
export default class CollectFromStore extends Component {
  static propTypes = {
    isMobile: PropTypes.bool.isRequired,
    stores: PropTypes.array.isRequired,
    isStoresLoading: PropTypes.bool.isRequired,
    filters: PropTypes.object.isRequired,
    storeQuery: PropTypes.object,
    selectedPlaceDetails: PropTypes.object,
    getStoresForCheckoutModal: PropTypes.func,
    selectDeliveryStore: PropTypes.func,
    closeModal: PropTypes.func,
    setStoreUpdating: PropTypes.func,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentWillMount() {
    const {
      storeQuery,
      filters,
      getStoresForCheckoutModal,
      selectedPlaceDetails,
    } = this.props
    if (
      storeQuery !== undefined &&
      getAppliedFilters(filters).join(',') !== storeQuery.types &&
      !isEmpty(selectedPlaceDetails)
    )
      getStoresForCheckoutModal('collectFromStore')
  }

  componentWillReceiveProps(nextProps) {
    const { isMobile, closeModal } = this.props
    if (nextProps.isMobile && isMobile !== nextProps.isMobile) closeModal()
  }

  componentWillUnmount() {
    const { setStoreUpdating } = this.props
    setStoreUpdating(false)
  }

  onSelectDeliveryStore = (store) => {
    const { selectDeliveryStore, closeModal } = this.props
    selectDeliveryStore(store)
    closeModal()
  }

  searchStores = (e) => {
    const { getStoresForCheckoutModal } = this.props
    if (e) e.preventDefault()
    getStoresForCheckoutModal()
  }

  render() {
    const { stores, isStoresLoading } = this.props
    const { l } = this.context
    return (
      <div className="CollectFromStore">
        <h3 className="CollectFromStore-title">{l`Where do you want to collect from?`}</h3>
        <div className="CollectFromStore-row">
          <div className="CollectFromStore-columnLeft">
            <form
              className="CollectFromStore-search"
              onSubmit={this.searchStores}
            >
              <UserLocatorInput
                selectedCountry={storeLocatorConsts.defaultCountry}
              />
            </form>
            {// @NOTE user will be blocked if search has no results
            // @TODO implement a no results view
            stores.length && !isStoresLoading ? (
              <StoreList
                name="CollectFromStore"
                storeType="collectFromStore"
                hasFilters
                fetchStores={this.searchStores}
                selectDeliveryStore={this.onSelectDeliveryStore}
              />
            ) : null}
          </div>
          <div className="CollectFromStore-columnRight">
            <GoogleMap />
          </div>
        </div>
        {!stores.length || isStoresLoading ? (
          <div className="CollectFromStore-espotContainer" />
        ) : null}
      </div>
    )
  }
}
