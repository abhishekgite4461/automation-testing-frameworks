import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

import FeatureCheck from '../../common/FeatureCheck/FeatureCheck'
import { getRouteFromUrl } from '../../../lib/get-product-route'

export default class ProductsBreadCrumbs extends React.Component {
  static propTypes = {
    breadcrumbs: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string.isRequired,
        url: PropTypes.string,
      })
    ),
  }

  static defaultProps = {
    breadcrumbs: [],
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  render() {
    const { l } = this.context
    const { breadcrumbs } = this.props
    if (breadcrumbs.length === 0) {
      return null
    }
    return (
      <FeatureCheck flag="FEATURE_DESKTOP_DISPLAY_PRODUCTS_BREADCRUMB">
        <ol className="ProductsBreadCrumbs">
          {breadcrumbs.map(({ label, url }) => {
            const breadcrumbLabel = label === 'Home' ? l`Home` : label
            return (
              <li className="ProductsBreadCrumbs-item" key={label}>
                {url ? (
                  <Link to={getRouteFromUrl(url)}>{breadcrumbLabel}</Link>
                ) : (
                  breadcrumbLabel
                )}
              </li>
            )
          })}
        </ol>
      </FeatureCheck>
    )
  }
}
