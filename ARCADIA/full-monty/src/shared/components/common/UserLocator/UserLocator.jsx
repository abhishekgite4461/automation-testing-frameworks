import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Image from '../Image/Image'
import storeLocatorConsts from '../../../constants/storeLocator'
import Button from '../Button/Button'
import CountryChooser from '../StoreLocator/CountryChooser'
import UserLocatorInput from '../UserLocatorInput/UserLocatorInput'
import {
  getCurrentLocationError,
  searchStores,
  searchStoresCheckout,
  setSelectedPlace,
  resetPredictions,
  resetSearchTerm,
  resetSelectedPlace,
} from '../../../actions/components/UserLocatorActions'
import { selectCountry } from '../../../actions/components/StoreLocatorActions'
import { setFormField } from '../../../actions/common/formActions'

@connect(
  (state) => ({
    getCurrentLocationError: state.userLocator.getCurrentLocationError,
    selectedCountry: state.storeLocator.selectedCountry,
    isMobile: state.viewport.media === 'mobile',
  }),
  {
    getCurrentLocationError,
    searchStores,
    searchStoresCheckout,
    setSelectedPlace,
    resetPredictions,
    resetSearchTerm,
    resetSelectedPlace,
    setFormField,
    selectCountry,
  }
)
export default class UserLocator extends Component {
  static propTypes = {
    selectedCountry: PropTypes.string,
    storeLocatorType: PropTypes.string,
    landingPage: PropTypes.bool,
    searchStores: PropTypes.func,
    searchStoresCheckout: PropTypes.func,
    resetPredictions: PropTypes.func,
    resetSearchTerm: PropTypes.func,
    resetSelectedPlace: PropTypes.func,
    setFormField: PropTypes.func,
    selectCountry: PropTypes.func,
    isMobile: PropTypes.bool,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentWillMount() {
    const { landingPage, selectedCountry, selectCountry } = this.props

    if (landingPage && !selectedCountry) {
      selectCountry(storeLocatorConsts.defaultCountry)
    }
  }

  componentWillReceiveProps({ selectedCountry }) {
    if (selectedCountry && selectedCountry !== this.props.selectedCountry) {
      this.searchStoresByCountry(selectedCountry)
    }
  }

  clearSearchField = () => {
    const {
      setFormField,
      resetPredictions,
      resetSearchTerm,
      resetSelectedPlace,
    } = this.props
    setFormField('userLocator', 'userLocation', '')
    resetPredictions()
    resetSearchTerm()
    resetSelectedPlace()
  }

  submitHandler = (e) => {
    if (e) e.preventDefault()
    this.searchStores()
  }

  searchStores = () => {
    const { storeLocatorType, searchStoresCheckout, searchStores } = this.props
    if (storeLocatorType === 'collectFromStore') {
      searchStoresCheckout()
    } else {
      searchStores()
    }
  }

  searchStoresByCountry = (country) => {
    const { landingPage, selectCountry } = this.props
    selectCountry(country)
    if (landingPage && country !== storeLocatorConsts.defaultCountry) {
      // Avoiding the scenario where the User selects UK and a place in the UK,
      // then he selects US and then again UK seeing the search field already
      // filled with the previous data.
      this.clearSearchField()
      this.searchStores()
    }
  }

  get showInput() {
    const { selectedCountry } = this.props
    return selectedCountry === storeLocatorConsts.defaultCountry
  }

  render() {
    const { l } = this.context
    const {
      isMobile,
      selectedCountry,
      landingPage, // TODO: remove when there is no store finder any more in the hamburger menu,
    } = this.props
    return (
      <form className="UserLocator" onSubmit={this.submitHandler}>
        <div className="UserLocator-countryContainer">
          {!landingPage && (
            <Image
              className="UserLocator-mapMarker"
              alt="Store locator map marker"
              src={'/assets/{brandName}/images/map-marker.svg'}
            />
          )}
          <div
            className={`${landingPage ? 'UserLocator-container' : ''}`}
            style={{
              marginBottom: isMobile && this.showInput ? 15 : 0,
            }}
          >
            {landingPage && (
              <h1 className="UserLocator-storeFinderText">{l`Store Locator`}</h1>
            )}
            <CountryChooser
              isLandingPage={landingPage}
              name={landingPage ? 'StoreFinderCountrySelect' : 'CountrySelect'}
            />
          </div>
          {!landingPage &&
            selectedCountry &&
            !this.showInput && (
              <Button
                title={l`Go`}
                type="submit"
                className="UserLocator-goButton UserLocator-goButton--country"
              >
                {l`Go`}
              </Button>
            )}
        </div>
        <UserLocatorInput selectedCountry={selectedCountry} />
      </form>
    )
  }
}
