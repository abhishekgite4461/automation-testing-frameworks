import testComponentHelper from 'test/unit/helpers/test-component'
import UserLocator from './UserLocator'

import storeLocatorConsts from '../../../constants/storeLocator'

const initialProps = {
  getCurrentLocationError: jest.fn(),
  searchStores: jest.fn(),
  searchStoresCheckout: jest.fn(),
  setSelectedPlace: jest.fn(),
  resetPredictions: jest.fn(),
  resetSearchTerm: jest.fn(),
  resetSelectedPlace: jest.fn(),
  setFormField: jest.fn(),
  selectCountry: jest.fn(),
}

const renderComponent = testComponentHelper(UserLocator.WrappedComponent)

describe('<UserLocator />', () => {
  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })
    it('when selectedCountry is the defaultCountry and it is not the landing page', () => {
      expect(
        renderComponent({
          ...initialProps,
          selectedCountry: storeLocatorConsts.defaultCountry,
        }).getTree()
      ).toMatchSnapshot()
    })
    it('when selectedCountry is not the defaultCountry and it is not the landing page', () => {
      expect(
        renderComponent({
          ...initialProps,
          selectedCountry: 'Portugal',
        }).getTree()
      ).toMatchSnapshot()
    })
    it('when selectedCountry is the defaultCountry and it is the landing page', () => {
      expect(
        renderComponent({
          ...initialProps,
          selectedCountry: storeLocatorConsts.defaultCountry,
          landingPage: true,
        }).getTree()
      ).toMatchSnapshot()
    })
    it('when selectedCountry is not the defaultCountry and it is the landing page', () => {
      expect(
        renderComponent({
          ...initialProps,
          selectedCountry: 'Portugal',
          landingPage: true,
        }).getTree()
      ).toMatchSnapshot()
    })
  })

  describe('@lifecycle', () => {
    beforeEach(() => {
      jest.resetAllMocks()
    })
    describe('componentWillMount', () => {
      it('should call selectCountry if on landing page and no selectedCountry', () => {
        const { instance } = renderComponent({
          ...initialProps,
          landingPage: true,
        })
        expect(instance.props.selectCountry).toHaveBeenCalledTimes(1)
        expect(instance.props.selectCountry).toHaveBeenCalledWith(
          storeLocatorConsts.defaultCountry
        )
      })

      it('should NOT call selectCountry if on landing page and selectedCountry', () => {
        const { instance } = renderComponent({
          ...initialProps,
          landingPage: true,
          selectedCountry: storeLocatorConsts.defaultCountry,
        })
        expect(instance.props.selectCountry).not.toHaveBeenCalled()
      })
      it('should NOT call selectCountry if not on landing page', () => {
        const { instance } = renderComponent({
          ...initialProps,
          landingPage: false,
          selectedCountry: storeLocatorConsts.defaultCountry,
        })
        expect(instance.props.selectCountry).not.toHaveBeenCalled()
      })
    })
    describe('componentWillReceiveProps', () => {
      it('should call searchStoresByCountry method if selectedCountry in nextProps', () => {
        const { instance } = renderComponent(initialProps)
        instance.searchStoresByCountry = jest.fn()
        const newCountry = 'France'

        expect(instance.searchStoresByCountry).not.toHaveBeenCalled()
        instance.componentWillReceiveProps({ selectedCountry: newCountry })
        expect(instance.searchStoresByCountry).toHaveBeenCalledTimes(1)
        expect(instance.searchStoresByCountry).toHaveBeenCalledWith(newCountry)
      })

      it('should not call searchStoresByCountry if selectedCountry remains the same', () => {
        const { instance } = renderComponent({
          ...initialProps,
          selectedCountry: storeLocatorConsts.defaultCountry,
        })
        instance.searchStoresByCountry = jest.fn()
        instance.componentWillReceiveProps({
          selectCountry: storeLocatorConsts.defaultCountry,
        })
        expect(instance.searchStoresByCountry).not.toHaveBeenCalled()
      })
    })
  })

  describe('@instance methods', () => {
    beforeEach(() => {
      jest.resetAllMocks()
    })

    describe('clearSearchField', () => {
      it('should call the following the 5 methods', () => {
        const { instance } = renderComponent(initialProps)

        expect(instance.props.setFormField).not.toHaveBeenCalled()
        expect(instance.props.resetPredictions).not.toHaveBeenCalled()
        expect(instance.props.resetSearchTerm).not.toHaveBeenCalled()
        expect(instance.props.resetSelectedPlace).not.toHaveBeenCalled()

        instance.clearSearchField()

        expect(instance.props.setFormField).toHaveBeenCalledTimes(1)
        expect(instance.props.setFormField).toHaveBeenCalledWith(
          'userLocator',
          'userLocation',
          ''
        )
        expect(instance.props.resetPredictions).toHaveBeenCalledTimes(1)
        expect(instance.props.resetSearchTerm).toHaveBeenCalledTimes(1)
        expect(instance.props.resetSelectedPlace).toHaveBeenCalledTimes(1)
      })
    })

    describe('searchStoresByCountry', () => {
      it('should call selectCountry', () => {
        const { instance } = renderComponent(initialProps)
        expect(instance.props.selectCountry).not.toHaveBeenCalled()
        instance.searchStoresByCountry('somecountry')
        expect(instance.props.selectCountry).toHaveBeenCalledTimes(1)
        expect(instance.props.selectCountry).toHaveBeenCalledWith('somecountry')
      })
      it('should not call clearSearchField and searchStores if on landingPage and with defaultCountry', () => {
        const { instance } = renderComponent({
          ...initialProps,
          landingPage: true,
        })
        instance.clearSearchField = jest.fn()
        instance.searchStores = jest.fn()
        instance.searchStoresByCountry(storeLocatorConsts.defaultCountry)
        expect(instance.clearSearchField).not.toHaveBeenCalled()
        expect(instance.searchStores).not.toHaveBeenCalled()
      })
      it('should not call clearSearchField and searchStores if on landingPage and not with defaultCountry', () => {
        const { instance } = renderComponent({
          ...initialProps,
          landingPage: true,
        })
        instance.clearSearchField = jest.fn()
        instance.searchStores = jest.fn()
        expect(instance.clearSearchField).not.toHaveBeenCalled()
        expect(instance.searchStores).not.toHaveBeenCalled()
        instance.searchStoresByCountry('somecountry')
        expect(instance.clearSearchField).toHaveBeenCalledTimes(1)
        expect(instance.searchStores).toHaveBeenCalledTimes(1)
      })
      it('should call clearSearchField and searchStores if not on landingPage', () => {
        const { instance } = renderComponent({
          ...initialProps,
          landingPage: false,
        })
        instance.clearSearchField = jest.fn()
        instance.searchStores = jest.fn()
        instance.searchStoresByCountry(storeLocatorConsts.defaultCountry)
        expect(instance.clearSearchField).not.toHaveBeenCalled()
        expect(instance.searchStores).not.toHaveBeenCalled()
      })
    })
    describe('submitHandler', () => {
      it('should call preventDefault and should call searchStores', () => {
        const { instance } = renderComponent(initialProps)
        const event = { preventDefault: jest.fn() }
        instance.searchStores = jest.fn()
        expect(event.preventDefault).not.toHaveBeenCalled()
        expect(instance.searchStores).not.toHaveBeenCalled()
        instance.submitHandler(event)
        expect(event.preventDefault).toHaveBeenCalledTimes(1)
        expect(instance.searchStores).toHaveBeenCalledTimes(1)
      })
    })
    describe('searchStores', () => {
      it('if storeLocatorType === collectFromStore should call searchStoresCheckout and not searchStores', () => {
        const { instance } = renderComponent({
          ...initialProps,
          storeLocatorType: 'collectFromStore',
        })
        expect(instance.props.searchStoresCheckout).not.toHaveBeenCalled()
        instance.searchStores()
        expect(instance.props.searchStoresCheckout).toHaveBeenCalledTimes(1)
        expect(instance.props.searchStores).not.toHaveBeenCalled()
      })
      it('if storeLocatorType !== collectFromStore should call searchStores and not searchStoresCheckout', () => {
        const { instance } = renderComponent({
          ...initialProps,
          storeLocatorType: 'sometype',
        })
        expect(instance.props.searchStores).not.toHaveBeenCalled()
        instance.searchStores()
        expect(instance.props.searchStoresCheckout).not.toHaveBeenCalled()
        expect(instance.props.searchStores).toHaveBeenCalledTimes(1)
      })
    })
  })
})
