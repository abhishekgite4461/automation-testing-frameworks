import testComponentHelper from 'test/unit/helpers/test-component'
import AccountItem from './AccountItem'

describe('<AccountItem />', () => {
  const initialProps = {
    description: 'link to homepage',
    link: '/homepage',
    title: 'To Home page',
  }
  const renderComponent = testComponentHelper(AccountItem)
  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })
  })
})
