import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Link } from 'react-router'

export default class AccountItem extends Component {
  static propTypes = {
    description: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  }

  render() {
    const { title, link, description } = this.props

    return (
      <li className={'AccountItem'}>
        <Link to={link} className="NoLink AccountItem-link">
          <h3 className="AccountItem-title">{title}</h3>
          <p className="AccountItem-description">{description}</p>
        </Link>
      </li>
    )
  }
}
