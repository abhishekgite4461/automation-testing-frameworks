import testComponentHelper from 'test/unit/helpers/test-component'
import BackToTop from './BackToTop'
import { scrollToTop } from '../../../lib/scroll-helper'

global.window.addEventListener = jest.fn()
global.window.removeEventListener = jest.fn()
global.document.getElementsByClassName = jest.fn()
jest.mock('../../../lib/scroll-helper', () => ({
  scrollToTop: jest.fn(),
}))

describe('<BackToTop />', () => {
  const renderComponent = testComponentHelper(BackToTop.WrappedComponent)
  const initialProps = {
    setVisible: jest.fn(),
  }
  describe('@renders', () => {
    it('in default state', () => {
      const { wrapper, getTree } = renderComponent(initialProps)
      expect(getTree()).toMatchSnapshot()
      expect(wrapper.find('.BackToTop-returnButton').length).toBe(1)
      expect(
        wrapper.find('.BackToTop-returnButton').hasClass('is-visible')
      ).toBe(false)
      expect(wrapper.find('.BackToTop-arrow').length).toBe(1)
      expect(wrapper.find('.BackToTop-label').length).toBe(1)
      expect(wrapper.find('.BackToTop-content').length).toBe(1)
    })
    it('in visible state', () => {
      const { wrapper, getTree } = renderComponent({
        ...initialProps,
        hasPassedThreshold: true,
      })
      expect(getTree()).toMatchSnapshot()
      expect(wrapper.find('.BackToTop-returnButton').length).toBe(1)
      expect(
        wrapper.find('.BackToTop-returnButton').hasClass('is-visible')
      ).toBe(true)
    })
  })
  describe('@events', () => {
    describe('on click button', () => {
      it('should call scrollToTop with 200', () => {
        const { wrapper } = renderComponent(initialProps)
        expect(scrollToTop).not.toHaveBeenCalled()
        wrapper.find('Button').prop('clickHandler')()
        expect(scrollToTop).toHaveBeenCalledTimes(1)
        expect(scrollToTop).toHaveBeenLastCalledWith(200)
      })
    })
  })
})
