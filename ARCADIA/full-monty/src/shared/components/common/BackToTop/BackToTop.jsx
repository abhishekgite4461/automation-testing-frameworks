import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { scrollToTop } from '../../../lib/scroll-helper'
import Button from '../Button/Button'
import { withWindowScroll } from '../../containers/WindowEventProvider/withWindowScroll'

class BackToTop extends Component {
  static propTypes = {
    hasPassedThreshold: PropTypes.bool,
  }

  static defaultProps = {
    hasPassedThreshold: false,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  render() {
    const { l } = this.context
    const visibleClass = this.props.hasPassedThreshold ? 'is-visible' : ''

    return (
      <div className="BackToTop">
        <Button
          clickHandler={() => scrollToTop(200)}
          className={`BackToTop-returnButton ${visibleClass}`}
        >
          <span className="BackToTop-content">
            <span className="BackToTop-arrow" />
            <span className="BackToTop-label">{l`Back to top`}</span>
          </span>
        </Button>
      </div>
    )
  }
}

export default withWindowScroll({ scrollPastThreshold: 0.9 })(BackToTop)
