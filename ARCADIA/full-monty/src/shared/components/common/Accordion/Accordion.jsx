import classnames from 'classnames'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { path, equals } from 'ramda'
import keys from '../../../constants/keyboardKeys'
import Loader from '../Loader/Loader'

export default class Accordion extends Component {
  static propTypes = {
    accordionName: PropTypes.string,
    expanded: PropTypes.bool,
    showLoader: PropTypes.bool,
    header: PropTypes.node.isRequired,
    children: PropTypes.node,
    className: PropTypes.string,
    noContentPadding: PropTypes.bool,
    noContentBorderTop: PropTypes.bool,
    noHeaderPadding: PropTypes.bool,
    onAccordionToggle: PropTypes.func,
    analyticsOnToggle: PropTypes.func,
    noExpandedHeaderBackground: PropTypes.bool,
    arrowStyle: PropTypes.oneOf(['primary', 'secondary']),
    arrowPosition: PropTypes.oneOf(['left', 'right']),
  }

  static defaultProps = {
    accordionName: '',
    expanded: false,
    showLoader: false,
    className: '',
    noContentBorderTop: false,
    noExpandedHeaderBackground: false,
    arrowStyle: 'primary',
    arrowPosition: 'left',
    children: null,
    onAccordionToggle: () => {},
    analyticsOnToggle: () => {},
  }

  constructor(props) {
    super(props)
    this.state = {
      expanded: props.expanded,
      expandedHeight: 0,
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.expanded !== this.state.expanded) {
      this.setState({ expanded: nextProps.expanded })
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.state.expanded !== nextState.expanded ||
      this.state.expandedHeight !== nextState.expandedHeight ||
      !equals(this.props.children, nextProps.children) ||
      (this.props.showLoader && !nextProps.showLoader)
    )
  }

  componentDidMount() {
    this.updateExpandedHeight()
  }

  componentDidUpdate() {
    this.updateExpandedHeight()
  }

  updateExpandedHeight = () => {
    const newHeight = path(['scrollHeight'], this.accordionWrapper)
    if (newHeight === undefined) {
      return
    }

    if (newHeight !== this.state.expandedHeight) {
      this.setState({ expandedHeight: newHeight })
    }
  }

  toggle = () => {
    const {
      accordionName,
      children,
      onAccordionToggle,
      analyticsOnToggle,
    } = this.props
    const notExpanded = !this.state.expanded

    if (!children) {
      return null
    }

    if (notExpanded) {
      analyticsOnToggle()
    }

    onAccordionToggle(accordionName, notExpanded)

    this.setState((state) => ({ expanded: !state.expanded }))
  }

  onKeyDown = ({ keyCode }) => {
    if (keyCode === keys.ENTER || keyCode === keys.SPACE) {
      this.toggle()
    }
  }

  render() {
    const {
      children,
      className,
      header,
      noContentPadding,
      noHeaderPadding,
      noContentBorderTop,
      noExpandedHeaderBackground,
      arrowStyle,
      arrowPosition,
      showLoader,
    } = this.props
    const { expanded } = this.state
    const classNames = classnames('Accordion', className, {
      'is-expanded': expanded,
    })
    const headerClassNames = classnames('Accordion-header', {
      'Accordion-header--noExpandedBackground': noExpandedHeaderBackground,
      'is-padded': !noHeaderPadding,
    })
    const iconClassNames = classnames(
      'Accordion-icon',
      `Accordion-icon--${arrowStyle}`,
      `Accordion-icon--${arrowPosition}`,
      {
        'Accordion-icon--hidden': !children,
      }
    )

    return (
      <article
        className={classNames}
        ref={(accordion) => {
          this.accordion = accordion
        }}
      >
        <div // eslint-disable-line jsx-a11y/no-static-element-interactions
          className={headerClassNames}
          onClick={this.toggle}
          onKeyDown={this.onKeyDown}
          style={children ? {} : { cursor: 'auto' }}
        >
          <div className={iconClassNames} />
          <header
            className="Accordion-title"
            role="button"
            tabIndex="0"
            aria-pressed={expanded}
          >
            {header}
          </header>
        </div>
        <div
          className="Accordion-wrapper"
          style={{ maxHeight: expanded ? this.state.expandedHeight : 0 }}
          ref={(accordionWrapper) => {
            this.accordionWrapper = accordionWrapper
          }}
        >
          {showLoader && <Loader />}
          {children && (
            <div
              className={classnames('Accordion-content', {
                'is-padded': !noContentPadding,
                'is-visible': !showLoader,
                'Accordion-content--borderTop': !noContentBorderTop,
              })}
            >
              {children}
            </div>
          )}
        </div>
      </article>
    )
  }
}
