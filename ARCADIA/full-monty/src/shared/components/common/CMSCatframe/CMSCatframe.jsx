import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'

const encode = (path) => path.replace(/%2F/g, '/').replace(/%3A/g, ':')
const getDesktopLegacyWidth = (brand) => {
  if (brand === 'topman') return 1024
  else if (brand === 'topshop') return 1200
  return 990
}
const DESKTOP_WIDTH = 1200
const DEFAULT_HEIGHT = 20000

@connect((state) => ({
  viewportWidth: state.viewport.width,
  apiEnvironment: state.debug.environment,
  storeCode: state.config.storeCode,
  location: state.routing.location,
  brandName: state.config.brandName,
  seoUrlRefinement: state.refinements.seoUrl,
  canonicalUrl: state.products.canonicalUrl,
}))
export default class CMSCatframe extends Component {
  static propTypes = {
    location: PropTypes.object,
    viewportWidth: PropTypes.number,
    apiEnvironment: PropTypes.string,
    storeCode: PropTypes.string,
    brandName: PropTypes.string,
    defaultWidth: PropTypes.number,
    defaultHeight: PropTypes.number,
    seoUrlRefinement: PropTypes.string,
    canonicalUrl: PropTypes.string,
  }

  static defaultProps = {
    defaultWidth: DESKTOP_WIDTH,
    defaultHeight: DEFAULT_HEIGHT,
  }

  constructor(props) {
    super(props)
    this.state = {
      width: null,
      scale: 1,
      left: 0,
      top: 0,
      height: this.props.defaultHeight,
    }
  }

  componentDidMount() {
    window.addEventListener('message', this.manageIframePostMessage)
    // need this delay as there is a minor delay in setting the viewportWidth in Main.jsx
    const x = setTimeout(() => {
      const { viewportWidth } = this.props
      this.fixDimensions(viewportWidth)
      clearTimeout(x)
    }, 100)
  }

  componentWillReceiveProps({ viewportWidth }) {
    const { viewportWidth: oldWidth } = this.props
    if (viewportWidth !== oldWidth) {
      this.fixDimensions(viewportWidth)
    }
  }

  componentWillUnmount() {
    window.removeEventListener('message', this.manageIframePostMessage)
  }

  manageIframePostMessage = (messageEvent) => {
    const { viewportWidth } = this.props
    if (
      messageEvent.data &&
      messageEvent.data.IframeLoaded &&
      messageEvent.data.height
    ) {
      this.fixDimensions(viewportWidth, messageEvent.data.height)
    } else if (messageEvent.data && messageEvent.data.iframeTitle) {
      this.setState({
        title: messageEvent.data.iframeTitle,
      })
    }
  }

  fixDimensions = (viewportWidth, realHeight) => {
    const { brandName } = this.props
    if (!realHeight) {
      const node =
        this.iframeNode &&
        this.iframeNode.contentWindow.document.getElementById(
          'monty_legacy_cms_content_wrapper'
        )
      realHeight = node ? node.offsetHeight : this.props.defaultHeight
    }
    const scaledWidth =
      viewportWidth <= getDesktopLegacyWidth(brandName)
        ? getDesktopLegacyWidth(brandName)
        : viewportWidth
    const scale = viewportWidth / scaledWidth
    const scaledHeight = realHeight * scale
    this.setState({
      width: scaledWidth,
      scale,
      left: (viewportWidth - scaledWidth) / 2,
      height: realHeight,
      top: (scaledHeight - realHeight) / 2,
    })
  }

  render() {
    const {
      location,
      viewportWidth,
      storeCode,
      apiEnvironment,
      defaultHeight,
      defaultWidth,
      seoUrlRefinement,
      canonicalUrl,
    } = this.props
    const { width, scale, left, top, height, title } = this.state
    const pathname = seoUrlRefinement || canonicalUrl || location.pathname

    const seoUrl = `/cmsfeaturecontent?apiEnvironment=${apiEnvironment ||
      'prod'}&storeCode=${storeCode}&pathname=${encode(
      pathname
    )}&categoryOnly=true&render404=false`

    const margin =
      viewportWidth > defaultWidth ? (defaultWidth - viewportWidth) / 2 : 0
    const style = {
      width: `${width || viewportWidth}px`,
      transform: `scale(${scale})`,
      left: `${left}px`,
      top: `${top}px`,
      height: `${height}px`,
    }
    const containerStyle = {
      width: `${viewportWidth}px`,
      height: `${height * scale}px`,
      marginLeft: `${margin}px`,
      marginRight: `${margin}px`,
    }
    return (
      <div className="CMSCatframe-container" style={containerStyle}>
        {title && <Helmet title={title} />}
        <iframe
          id="CMSCatframe"
          ref={(el) => {
            this.iframeNode = el
          }}
          src={seoUrl}
          height={`${defaultHeight}px`}
          className="CMSCatframe-frame"
          frameBorder="0"
          style={style}
        />
      </div>
    )
  }
}
