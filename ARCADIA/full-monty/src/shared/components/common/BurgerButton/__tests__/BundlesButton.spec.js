import testComponentHelper from 'test/unit/helpers/test-component'

import BurgerButton from '../BurgerButton'

describe('<BurgerButton />', () => {
  const renderComponent = testComponentHelper(BurgerButton)

  describe('@renders', () => {
    it('should render in default state', () => {
      const { getTree } = renderComponent()
      expect(getTree()).toMatchSnapshot()
    })

    it('should add `is-arrow` class if `isArrow` prop is `true`', () => {
      const { wrapper } = renderComponent({
        isArrow: true,
      })
      expect(wrapper.find('.BurgerButton').hasClass('is-arrow')).toBe(true)
    })
  })
})
