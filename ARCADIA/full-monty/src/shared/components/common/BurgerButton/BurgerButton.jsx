import PropTypes from 'prop-types'
import React, { Component } from 'react'

export default class BurgerButton extends Component {
  static propTypes = {
    isArrow: PropTypes.bool,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  render() {
    const { l } = this.context
    return (
      <button
        title={l`Open categories menu`}
        aria-label={l`Open categories menu`}
        className={`BurgerButton${this.props.isArrow ? ' is-arrow' : ''}`}
      >
        <span className="BurgerButton-bar" />
        <span className="BurgerButton-bar" />
        <span className="BurgerButton-bar" />
      </button>
    )
  }
}
