import PropTypes from 'prop-types'
import React from 'react'

// components
import Input from '../FormComponents/Input/Input'
import Select from '../FormComponents/Select/Select'

const ManualAddress = (
  {
    addressForm,
    onBlur,
    country,
    canFindAddress,
    formNames,
    isDesktopMulticolumStyle,
    isFindAddressVisible,
    postCodeRules,
    usStates,
    setAndValidateAddressField,
    touchField,
    handleClearForm,
    handleSwitchToFindAddress,
  },
  { l }
) => {
  return country && country !== 'default' && isFindAddressVisible === false ? (
    <section
      className={`ManualAddress ${
        isDesktopMulticolumStyle ? 'ManualAddress--multiColumn' : ''
      }`}
      aria-label="Address"
    >
      <div className="ManualAddress-row">
        <Input
          className="ManualAddress-address1"
          label={l`Address Line 1`}
          field={addressForm.fields.address1}
          setField={setAndValidateAddressField}
          touchedField={touchField(formNames.address)}
          placeholder={l`Address Line 1`}
          name="address1"
          errors={addressForm.errors}
          isRequired
        />
        <Input
          className="ManualAddress-address2"
          label={l`Address Line 2`}
          field={addressForm.fields.address2}
          setField={setAndValidateAddressField}
          touchedField={touchField(formNames.address)}
          placeholder={l`Address Line 2`}
          name="address2"
          errors={addressForm.errors}
        />
      </div>
      {postCodeRules.stateFieldType ? (
        postCodeRules.stateFieldType === 'input' ? (
          <Input
            label={l`State`}
            field={addressForm.fields.state}
            setField={setAndValidateAddressField}
            touchedField={touchField(formNames.address)}
            placeholder="State"
            name="state"
          />
        ) : (
          <Select
            label={l`State`}
            name="state"
            value={addressForm.fields.state.value}
            onChange={setAndValidateAddressField('state')}
            options={usStates}
            defaultValue={addressForm.fields.state.value || usStates[0]}
          />
        )
      ) : null}
      <div className="ManualAddress-row">
        <Input
          className="ManualAddress-postcode"
          label={l(postCodeRules.postcodeLabel || 'Postcode')}
          field={addressForm.fields.postcode}
          onBlur={onBlur('postcode')}
          setField={setAndValidateAddressField}
          touchedField={touchField(formNames.address)}
          placeholder={l(postCodeRules.postcodeLabel || 'Postcode')}
          name="postcode"
          errors={addressForm.errors}
          isRequired={postCodeRules.postcodeRequired}
        />
        <Input
          className="ManualAddress-city"
          label={l`Town/City`}
          field={addressForm.fields.city}
          setField={setAndValidateAddressField}
          touchedField={touchField(formNames.address)}
          placeholder={l`Town/City`}
          name="city"
          errors={addressForm.errors}
          isRequired
        />
      </div>
      <div className="ManualAddress-linkWrapper">
        <button
          type="reset"
          onClick={handleClearForm}
          className="ManualAddress-link ManualAddress-link--left"
        >
          {l`Clear form`}
        </button>
        {canFindAddress && (
          <button
            type="button"
            onClick={handleSwitchToFindAddress}
            className="ManualAddress-link ManualAddress-link--right"
          >
            {l`Find Address`}
          </button>
        )}
      </div>
    </section>
  ) : null
}

ManualAddress.propTypes = {
  addressForm: PropTypes.object.isRequired,
  country: PropTypes.string.isRequired,
  canFindAddress: PropTypes.bool,
  formNames: PropTypes.object.isRequired,
  isDesktopMulticolumStyle: PropTypes.bool,
  isFindAddressVisible: PropTypes.bool,
  postCodeRules: PropTypes.object,
  usStates: PropTypes.arrayOf(PropTypes.string),
  setAndValidateAddressField: PropTypes.func.isRequired,
  touchField: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  handleClearForm: PropTypes.func.isRequired,
  handleSwitchToFindAddress: PropTypes.func.isRequired,
}

ManualAddress.contextTypes = {
  l: PropTypes.func,
}

ManualAddress.defaultProps = {
  country: 'United Kingdom',
  canFindAddress: false,
  isDesktopMulticolumStyle: false,
  isFindAddressVisible: true,
  postCodeRules: {},
  onBlur: () => null,
}

export default ManualAddress
