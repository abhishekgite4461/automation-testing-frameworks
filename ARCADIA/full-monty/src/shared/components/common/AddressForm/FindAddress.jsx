import PropTypes from 'prop-types'
import React from 'react'
import { path, pathOr } from 'ramda'

// components
import Input from '../FormComponents/Input/Input'
import Select from '../FormComponents/Select/Select'
import Button from '../FormComponents/Button/Button'
import Message from '../FormComponents/Message/Message'

const FindAddress = (props, { l }) => {
  const {
    addressForm,
    addressType,
    onBlur,
    country,
    countries,
    detailsForm,
    findAddressForm,
    formNames,
    isDesktopMulticolumStyle,
    isFindAddressVisible,
    monikers,
    postCodeRules,
    titleHidden,
    setAndValidateDetailsField,
    setAndValidateFindAddressField,
    touchField,
    setCountry,
    handleFindAddressRequest,
    handleAddressChange,
    handleSwitchToManualAddress,
  } = props
  return (
    <section
      className={`FindAddressV1 ${
        isDesktopMulticolumStyle ? 'FindAddressV1--multiColumn' : ''
      }`}
      aria-label="Find Address"
    >
      {!titleHidden && <h3 className="FindAddressV1-heading">{l`Address`}</h3>}
      <div className="FindAddressV1-row">
        {/* HACK: Telephone field is normally part of DetailsForm,
          To make it responsive we need to bring it in here as well and show/hide it
          props to make the model bindings work are brought in */
        isDesktopMulticolumStyle ? (
          <Input
            className="FindAddressV1-telephone"
            isDisabled={detailsForm.success}
            field={pathOr({}, ['fields', 'telephone'], detailsForm)}
            name="telephone"
            type="tel"
            errors={detailsForm.errors}
            label={l`Primary Phone Number`}
            placeholder={l`07123 123123`}
            setField={setAndValidateDetailsField}
            touchedField={touchField(formNames.details)}
            isRequired
          />
        ) : null}
        <Select
          className="FindAddressV1-country"
          label={
            addressType === 'billing' ? l`Billing country` : l`Delivery country`
          }
          field={addressForm.fields.country}
          options={[
            { label: l`Please select your country`, value: '', disabled: true },
            ...countries.map((country) => ({ label: country, value: country })),
          ]}
          name="country"
          errors={addressForm.errors}
          value={country}
          onChange={setCountry}
          isRequired
          noTranslate
        />
      </div>
      {isFindAddressVisible && (
        <div className="FindAddressV1-form">
          <div className="FindAddressV1-row">
            <Input
              className="FindAddressV1-postcode"
              field={pathOr({}, ['fields', 'postcode'], findAddressForm)}
              name="postcode"
              label={l(postCodeRules.postcodeLabel || 'Postcode')}
              errors={findAddressForm.errors}
              placeholder={l`Eg. W1T 3NL`}
              onBlur={onBlur('postcode')}
              setField={setAndValidateFindAddressField}
              touchedField={touchField(formNames.findAddress)}
              isRequired={postCodeRules.postcodeRequired}
            />
            <Input
              className="FindAddressV1-houseNumber"
              field={pathOr({}, ['fields', 'houseNumber'], findAddressForm)}
              name="houseNumber"
              label={l(postCodeRules.premisesLabel || 'House number')}
              errors={findAddressForm.errors}
              placeholder={l`Eg. 214`}
              setField={setAndValidateFindAddressField}
              touchedField={touchField(formNames.findAddress)}
            />
          </div>
          <Button
            onClick={handleFindAddressRequest}
            className="FindAddressV1-button"
            name="findAddress"
            isDisabled={!!path(['errors', 'postcode'], findAddressForm)}
            error={path(['errors', 'findAddress'], findAddressForm)}
            touched={path(
              ['fields', 'findAddress', 'isTouched'],
              findAddressForm
            )}
          >
            {l`Find Address`}
          </Button>
          <Message
            message={path(['message', 'message'], findAddressForm)}
            type="error"
          />
          <Select
            label={l`Click to select your address`}
            onChange={handleAddressChange}
            className={`FindAddressV1-selectAddress${
              monikers && monikers.length > 0 ? '' : ' is-hidden'
            }`}
            name="selectAddress"
            firstDisabled={l`Please select your address...`}
            options={
              monikers && monikers.length
                ? monikers.map(({ address, moniker }) => ({
                    label: address,
                    value: moniker,
                  }))
                : []
            }
            field={path(['fields', 'selectAddress'], findAddressForm)}
            errors={findAddressForm.errors}
            isRequired
          />
          <button
            type="button"
            onClick={handleSwitchToManualAddress}
            className="FindAddressV1-link"
          >
            {l`Enter address manually`}
          </button>
        </div>
      )}
    </section>
  )
}

FindAddress.propTypes = {
  addressForm: PropTypes.object.isRequired,
  addressType: PropTypes.oneOf(['delivery', 'billing']).isRequired,
  country: PropTypes.string.isRequired,
  countries: PropTypes.array,
  detailsForm: PropTypes.object.isRequired,
  findAddressForm: PropTypes.object.isRequired,
  formNames: PropTypes.object.isRequired,
  isDesktopMulticolumStyle: PropTypes.bool,
  isFindAddressVisible: PropTypes.bool,
  monikers: PropTypes.array,
  onBlur: PropTypes.func.isRequired,
  postCodeRules: PropTypes.object,
  titleHidden: PropTypes.bool,
  setAndValidateDetailsField: PropTypes.func.isRequired,
  setAndValidateFindAddressField: PropTypes.func.isRequired,
  touchField: PropTypes.func.isRequired,
  setCountry: PropTypes.func.isRequired,
  handleFindAddressRequest: PropTypes.func.isRequired,
  handleAddressChange: PropTypes.func.isRequired,
  handleSwitchToManualAddress: PropTypes.func.isRequired,
}

FindAddress.contextTypes = {
  l: PropTypes.func,
}

FindAddress.defaultProps = {
  country: 'United Kingdom',
  countries: [],
  isDesktopMulticolumStyle: false,
  isFindAddressVisible: true,
  monikers: [],
  postCodeRules: {},
  onBlur: () => null,
  titleHidden: false,
}

export default FindAddress
