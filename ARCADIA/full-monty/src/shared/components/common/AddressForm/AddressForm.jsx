import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { path, pathOr } from 'ramda'

// actions
import {
  setFormField,
  setAndValidateFormField,
  validateForm,
  clearFormErrors,
  clearFormFieldError,
  touchedFormField,
  resetForm,
} from '../../../actions/common/formActions'
import {
  findAddress,
  findExactAddressByMoniker,
} from '../../../actions/common/findAddressActions'
import { validateDDPForCountry } from '../../../actions/common/ddpActions'

// libs
import {
  getYourAddressSchema,
  getFindAddressSchema,
  getYourDetailsSchema,
} from '../../../schemas/validation/addressFormValidationSchema'
import { eventBasedAnalytics } from '../../../lib/analytics/analytics'

// selectors
import {
  getPostCodeRules,
  getCountryCodeFromQAS,
  getCountriesByAddressType,
} from '../../../selectors/common/configSelectors'
import { isInCheckout } from '../../../selectors/routingSelectors'

// components
import FindAddress from './FindAddress'
import ManualAddress from './ManualAddress'

// component helper
export function getHasSelectedAddress(form) {
  return !!path(['fields', 'address1', 'value'], form)
}
export function gethasFoundAddress(form) {
  return !path(['fields', 'findAddress', 'value'], form)
}

export const mapStateToProps = (
  state,
  {
    addressType,
    country,
    formNames,
    getAddressFormFromState,
    isFindAddressVisible,
  }
) => {
  const postCodeRules = getPostCodeRules(state, country)
  const addressForm = getAddressFormFromState(
    addressType,
    formNames.address,
    state
  )
  const findAddressForm = getAddressFormFromState(
    addressType,
    formNames.findAddress,
    state
  )
  const hasFoundAddress = gethasFoundAddress(findAddressForm)
  const hasSelectedAddress = getHasSelectedAddress(addressForm)
  const countries = getCountriesByAddressType(state, addressType)

  return {
    addressForm,
    addressValidationSchema: getYourAddressSchema(postCodeRules, countries),
    country,
    countries,
    countryCode: getCountryCodeFromQAS(state, country),
    canFindAddress: !!path(['config', 'qasCountries', country], state),
    detailsForm: getAddressFormFromState(addressType, formNames.details, state),
    detailsValidationSchema: getYourDetailsSchema(country),
    findAddressForm,
    findAddressValidationSchema: isFindAddressVisible
      ? getFindAddressSchema(postCodeRules, {
          hasFoundAddress,
          hasSelectedAddress,
        })
      : {},
    formNames,
    isFindAddressVisible,
    postCodeRules,
    // NOTE: rather hacky way of determining whether the schemas have updated (as can't do a `===` on them)
    // think about using something like https://github.com/reactjs/reselect
    schemaHash: isFindAddressVisible
      ? [
          country,
          isFindAddressVisible,
          hasFoundAddress,
          hasSelectedAddress,
        ].join(':')
      : [country, isFindAddressVisible].join(':'),
    usStates: pathOr([], ['siteOptions', 'USStates'], state),
    isCheckout: isInCheckout(state),
  }
}

export const mapDispatchToProps = {
  clearFormErrors,
  clearFormFieldError,
  findAddress,
  findExactAddressByMoniker,
  resetForm,
  setFormField,
  setAndValidateFormField,
  touchedFormField,
  validateForm,
  validateDDPForCountry,
}

class AddressForm extends Component {
  static propTypes = {
    addressValidationSchema: PropTypes.object,
    country: PropTypes.string.isRequired,
    countryCode: PropTypes.string,
    detailsValidationSchema: PropTypes.object,
    events: PropTypes.objectOf(PropTypes.string),
    findAddressForm: PropTypes.object.isRequired,
    findAddressValidationSchema: PropTypes.object,
    formNames: PropTypes.object.isRequired,
    schemaHash: PropTypes.string,
    // functions
    onSelectCountry: PropTypes.func,
    sendEventAnalytics: PropTypes.func,
    clearFormErrors: PropTypes.func.isRequired,
    clearFormFieldError: PropTypes.func.isRequired,
    findAddress: PropTypes.func.isRequired,
    findExactAddressByMoniker: PropTypes.func.isRequired,
    resetForm: PropTypes.func.isRequired,
    setFormField: PropTypes.func.isRequired,
    setAndValidateFormField: PropTypes.func.isRequired,
    touchedFormField: PropTypes.func.isRequired,
    validateForm: PropTypes.func.isRequired,
    validateDDPForCountry: PropTypes.func.isRequired,
    isCheckout: PropTypes.bool.isRequired,
  }

  static defaultProps = {
    addressValidationSchema: {},
    findAddressValidationSchema: {},
    country: 'United Kingdom',
    countryCode: null,
    countries: [],
    canFindAddress: false,
    detailsValidationSchema: {},
    events: {
      address1: 'event136',
      country: 'event135',
      findAddress: 'event121',
      manualAddress: 'event122',
      postCode: 'event137',
    },
    isFindAddressVisible: true,
    postCodeRules: {},
    schemaHash: '',
    usStates: [],
    onSelectCountry: () => {},
    sendEventAnalytics: eventBasedAnalytics,
    validateDDPForCountry: () => {},
    isCheckout: false,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  constructor(props) {
    super(props)
    this.state = {
      monikers: [],
    }
  }

  componentDidMount() {
    this.validateForms()
  }

  componentDidUpdate(prevProps) {
    if (prevProps.schemaHash !== this.props.schemaHash) {
      this.validateForms()
    }
  }

  componentWillUnmount() {
    this.clearErrors()
  }

  clearForms = () => {
    const { country, formNames, resetForm } = this.props
    // reset the address inputs
    resetForm(formNames.address, {
      address1: '',
      address2: '',
      postcode: '',
      city: '',
      county: '',
      state: '',
      country,
      isManual: true,
    })
    resetForm(formNames.findAddress, {
      houseNumber: '',
      message: '',
      findAddress: '',
      selectAddress: '',
      postcode: '',
    })
  }

  clearErrors() {
    const { formNames, clearFormErrors } = this.props
    clearFormErrors(formNames.findAddress)
    clearFormErrors(formNames.details)
    clearFormErrors(formNames.address)
  }

  setCountry = (ev) => {
    const {
      formNames,
      events,
      onSelectCountry,
      setFormField,
      sendEventAnalytics,
      addressType,
      validateDDPForCountry,
      isCheckout,
    } = this.props
    const {
      target: { value: country },
    } = ev

    if (isCheckout && addressType === 'delivery') validateDDPForCountry(country)

    setFormField(formNames.address, 'state', '')
    onSelectCountry(country)
    setFormField(formNames.address, 'country', country)
    sendEventAnalytics({
      events: events.country,
    })
  }

  setAndValidateFindAddressField = (fieldName) => ({ target: { value } }) => {
    const {
      formNames,
      setAndValidateFormField,
      findAddressValidationSchema,
    } = this.props
    return setAndValidateFormField(
      formNames.findAddress,
      fieldName,
      value,
      findAddressValidationSchema[fieldName]
    )
  }

  setAndValidateDetailsField = (fieldName) => ({ target: { value } }) => {
    const {
      formNames,
      detailsValidationSchema,
      setAndValidateFormField,
    } = this.props
    return setAndValidateFormField(
      formNames.details,
      fieldName,
      value,
      detailsValidationSchema[fieldName]
    )
  }

  setAndValidateAddressField = (fieldName) => ({ target: { value } }) => {
    const {
      formNames,
      addressValidationSchema,
      setAndValidateFormField,
    } = this.props
    return setAndValidateFormField(
      formNames.address,
      fieldName,
      value,
      addressValidationSchema[fieldName]
    )
  }

  validateForms() {
    const {
      formNames,
      addressValidationSchema,
      detailsValidationSchema,
      findAddressValidationSchema,
      validateForm,
    } = this.props
    validateForm(formNames.findAddress, findAddressValidationSchema)
    validateForm(formNames.details, detailsValidationSchema)
    validateForm(formNames.address, addressValidationSchema)
  }

  touchField = (formName) => (fieldName) => () => {
    const {
      events,
      formNames,
      sendEventAnalytics,
      touchedFormField,
    } = this.props
    if (formName !== formNames.details) {
      if (fieldName in events) {
        sendEventAnalytics({
          events: events[fieldName],
        })
      }
    }
    return touchedFormField(formName, fieldName)
  }

  handleFindAddressRequest = () => {
    const {
      formNames,
      events,
      countryCode,
      findAddressForm,
      clearFormFieldError,
      findAddress,
      sendEventAnalytics,
    } = this.props
    sendEventAnalytics({
      events: events.findAddress,
    })
    return findAddress({
      data: {
        country: countryCode,
        postcode: findAddressForm.fields.postcode.value,
        address: findAddressForm.fields.houseNumber.value,
      },
      formNames,
    }).then((monikers) => {
      if (monikers) {
        this.setState({
          monikers,
        })
        clearFormFieldError(formNames.findAddress, 'findAddress')
      }
    })
  }

  handleAddressChange = ({ target: { selectedIndex } }) => {
    const { countryCode, formNames, findExactAddressByMoniker } = this.props
    const { monikers } = this.state
    findExactAddressByMoniker({
      country: countryCode,
      moniker: monikers[selectedIndex - 1].moniker,
      formNames,
    })
  }

  handleSwitchToFindAddress = (event) => {
    const { formNames, setFormField } = this.props
    event.preventDefault()
    this.clearForms()
    this.clearErrors()
    setFormField(formNames.address, 'isManual', false)
    this.setState({
      monikers: [],
    })
  }

  handleSwitchToManualAddress = (event) => {
    const { events, formNames, setFormField, sendEventAnalytics } = this.props
    event.preventDefault()
    setFormField(formNames.address, 'isManual', true)
    sendEventAnalytics({
      events: events.manualAddress,
    })
  }

  handleClearForm = (event) => {
    event.preventDefault()
    this.clearForms()
    this.validateForms()
  }

  trimOnBlur = (formName, validationSchema) => (fieldName) => ({
    target: { value },
  }) => {
    const { setAndValidateFormField } = this.props
    return setAndValidateFormField(
      formName,
      fieldName,
      value.trim(),
      validationSchema[fieldName]
    )
  }

  render() {
    const {
      formNames,
      findAddressValidationSchema,
      addressValidationSchema,
    } = this.props
    return (
      <section>
        <FindAddress
          {...this.props}
          onBlur={this.trimOnBlur(
            formNames.findAddress,
            findAddressValidationSchema
          )}
          monikers={this.state.monikers}
          setAndValidateDetailsField={this.setAndValidateDetailsField}
          setAndValidateFindAddressField={this.setAndValidateFindAddressField}
          setAndValidateAddressField={this.setAndValidateAddressField}
          touchField={this.touchField}
          setCountry={this.setCountry}
          handleFindAddressRequest={this.handleFindAddressRequest}
          handleAddressChange={this.handleAddressChange}
          handleSwitchToManualAddress={this.handleSwitchToManualAddress}
        />
        <ManualAddress
          {...this.props}
          onBlur={this.trimOnBlur(formNames.address, addressValidationSchema)}
          setAndValidateAddressField={this.setAndValidateAddressField}
          touchField={this.touchField}
          handleClearForm={this.handleClearForm}
          handleSwitchToFindAddress={this.handleSwitchToFindAddress}
        />
      </section>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddressForm)

export { AddressForm as WrappedAddressForm }
