import testComponentHelper from 'test/unit/helpers/test-component'
import FindAddress from '../FindAddress'

// constants
import checkoutAddressFormRules from '../../../../../shared/constants/checkoutAddressFormRules'

describe('<FindAddress />', () => {
  const requiredProps = {
    addressForm: {
      fields: {},
      errors: {},
      message: {},
    },
    addressType: 'billing',
    country: 'United Kingdom',
    countries: [],
    detailsForm: {
      fields: {},
      errors: {},
      message: {},
    },
    findAddressForm: {
      fields: {},
      errors: {},
      message: {},
    },
    formNames: {
      address: 'billingAddressMCD',
      details: 'billingDetailsAddressMCD',
      findAddress: 'billingFindAddressMCD',
    },
    // non req
    isFindAddressVisible: true,
    monikers: [],
    postCodeRules: checkoutAddressFormRules['United Kingdom'],
    titleHidden: true,
    // functions
    setAndValidateDetailsField: () => {},
    setAndValidateFindAddressField: () => {},
    setAndValidateAddressField: () => {},
    touchField: () => {},
    setCountry: () => {},
    handleFindAddressRequest: () => {},
    handleAddressChange: () => {},
    handleSwitchToManualAddress: () => {},
  }
  const renderComponent = testComponentHelper(FindAddress)

  describe('@renders', () => {
    it('should render default state', () => {
      expect(renderComponent(requiredProps).getTree()).toMatchSnapshot()
    })

    it('should not render Find Address Form when isFindAddressVisible is false', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        isFindAddressVisible: false,
      })
      expect(wrapper.find('.FindAddressV1-form').length).toBe(0)
    })

    it('should render Find Address Form when isFindAddressVisible is true', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        isFindAddressVisible: true,
      })
      expect(wrapper.find('.FindAddressV1-form').length).toBe(1)
    })

    it('hides the title when titleHidden property is true', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        titleHidden: true,
      })
      expect(wrapper.find('.FormAddress-heading').length).toBe(0)
    })

    it('should render correct (hidden) the select with no moniker or null (defensive js)', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        monikers: null,
      })
      expect(wrapper.find('.is-hidden').length).toBe(1)
    })
  })

  describe('@events and functions', () => {
    it('should trigger handleAddressChange', () => {
      const handleAddressChangeMock = jest.fn()
      const { wrapper } = renderComponent({
        ...requiredProps,
        monikers: [
          {
            moniker: '111 Something Close',
          },
        ],
        handleAddressChange: handleAddressChangeMock,
      })
      wrapper.find('.FindAddressV1-selectAddress').prop('onChange')({
        target: { selectedIndex: 1 },
      })
      expect(handleAddressChangeMock).toHaveBeenCalledTimes(1)
    })

    it('should trigger setAndValidateDetailsField', () => {
      const setAndValidateDetailsFieldMock = jest.fn()
      const { wrapper } = renderComponent({
        ...requiredProps,
        isDesktopMulticolumStyle: true,
        setAndValidateDetailsField: setAndValidateDetailsFieldMock,
      })
      wrapper.find('.FindAddressV1-telephone').prop('setField')()
      expect(setAndValidateDetailsFieldMock).toHaveBeenCalledTimes(1)
    })

    it('should trigger setAndValidateFindAddressField', () => {
      const setAndValidateFindAddressFieldMock = jest.fn()
      const { wrapper } = renderComponent({
        ...requiredProps,
        setAndValidateFindAddressField: setAndValidateFindAddressFieldMock,
      })
      wrapper.find('.FindAddressV1-houseNumber').prop('setField')()
      expect(setAndValidateFindAddressFieldMock).toHaveBeenCalledTimes(1)
    })

    it('should trigger setAndValidateAddressField', () => {
      const setAndValidateFindAddressFieldMock = jest.fn()
      const { wrapper } = renderComponent({
        ...requiredProps,
        setAndValidateFindAddressField: setAndValidateFindAddressFieldMock,
      })
      wrapper.find('.FindAddressV1-postcode').prop('setField')()
      expect(setAndValidateFindAddressFieldMock).toHaveBeenCalledTimes(1)
    })

    it('should trigger touchField (postcode, telephone)', () => {
      const formTouchFieldMock = jest.fn()
      const touchFieldMock = () => formTouchFieldMock
      const { wrapper } = renderComponent({
        ...requiredProps,
        isDesktopMulticolumStyle: true,
        touchField: touchFieldMock,
      })
      wrapper.find('.FindAddressV1-postcode').prop('touchedField')(
        requiredProps.formNames.findAddress
      )
      expect(formTouchFieldMock).toHaveBeenCalledTimes(1)
      expect(formTouchFieldMock).toHaveBeenCalledWith(
        requiredProps.formNames.findAddress
      )
      wrapper.find('.FindAddressV1-telephone').prop('touchedField')(
        requiredProps.formNames.details
      )
      expect(formTouchFieldMock).toHaveBeenCalledTimes(2)
      expect(formTouchFieldMock).toHaveBeenCalledWith(
        requiredProps.formNames.details
      )
    })

    it('should trigger touchField (houseNumber)', () => {
      const findAddressFormTouchFieldMock = jest.fn()
      const touchFieldMock = () => findAddressFormTouchFieldMock
      const { wrapper } = renderComponent({
        ...requiredProps,
        touchField: touchFieldMock,
      })
      wrapper.find('.FindAddressV1-houseNumber').prop('touchedField')(
        requiredProps.formNames.findAddress
      )
      expect(findAddressFormTouchFieldMock).toHaveBeenCalledTimes(1)
      expect(findAddressFormTouchFieldMock).toHaveBeenCalledWith(
        requiredProps.formNames.findAddress
      )
    })

    it('should trigger setCountry', () => {
      const setCountryMock = jest.fn()
      const { wrapper } = renderComponent({
        ...requiredProps,
        setCountry: setCountryMock,
      })
      wrapper.find('.FindAddressV1-country').prop('onChange')()
      expect(setCountryMock).toHaveBeenCalledTimes(1)
    })

    it('should trigger handleFindAddressRequest', () => {
      const handleFindAddressRequestMock = jest.fn()
      const { wrapper } = renderComponent({
        ...requiredProps,
        handleFindAddressRequest: handleFindAddressRequestMock,
      })
      wrapper.find('.FindAddressV1-button').prop('onClick')()
      expect(handleFindAddressRequestMock).toHaveBeenCalledTimes(1)
    })

    it('should trigger handleSwitchToManualAddress', () => {
      const handleSwitchToManualAddressMock = jest.fn()
      const { wrapper } = renderComponent({
        ...requiredProps,
        handleSwitchToManualAddress: handleSwitchToManualAddressMock,
      })
      wrapper.find('.FindAddressV1-link').prop('onClick')()
      expect(handleSwitchToManualAddressMock).toHaveBeenCalledTimes(1)
    })

    it('should trigger onBlur', () => {
      const onBlurField = jest.fn()
      const onBlur = jest.fn(() => onBlurField)
      const { wrapper } = renderComponent({
        ...requiredProps,
        onBlur,
      })
      expect(onBlur).toHaveBeenCalledTimes(1)
      expect(onBlur).toHaveBeenCalledWith('postcode')
      expect(onBlurField).not.toHaveBeenCalled()
      wrapper.find('.FindAddressV1-postcode').prop('onBlur')()
      expect(onBlurField).toHaveBeenCalledTimes(1)
    })
  })
})
