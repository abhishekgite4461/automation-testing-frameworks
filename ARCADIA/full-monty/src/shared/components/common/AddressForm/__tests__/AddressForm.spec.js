import {
  buildComponentRender,
  shallowRender,
  mountRender,
  withStore,
} from 'test/unit/helpers/test-component'
import AddressForm, {
  mapStateToProps,
  mapDispatchToProps,
  getHasSelectedAddress,
  gethasFoundAddress,
} from '../AddressForm'
import FindAddress from '../FindAddress'
import ManualAddress from '../ManualAddress'

// constants
import checkoutAddressFormRules from '../../../../../shared/constants/checkoutAddressFormRules'
import qasCountries from '../../../../../shared/constants/qasCountries'

// mocks
import myAccountMock from '../../../../../../test/mocks/myAccount-response.json'
import myCheckoutDetailsMocks from '../../../../../../test/mocks/forms/myCheckoutDetailsFormsMocks'
import configMock from '../../../../../../test/mocks/config'
import siteOptionsMock from '../../../../../../test/mocks/siteOptions'

// selectors
import {
  getFormNames,
  getMCDAddressForm,
} from '../../../../selectors/common/accountSelectors'

import { compose } from 'ramda'

beforeEach(() => jest.clearAllMocks())

describe('<AddressForm />', () => {
  const requiredProps = {
    addressType: 'billing',
    country: 'United Kingdom',
    findAddressForm: {
      fields: {},
      errors: {},
      message: {},
    },
    addressForm: {
      fields: {},
      errors: {},
      message: {},
    },
    detailsForm: {
      fields: {},
      errors: {},
      message: {},
    },
    formNames: {
      address: 'billingAddress',
      details: 'billingDetails',
      findAddress: 'billingFindAddress',
    },
    postCodeRules: checkoutAddressFormRules['United Kingdom'],
    countryCode: qasCountries['United Kingdom'],
    // non req
    canFindAddress: true,
    isFindAddressVisible: false,
    titleHidden: true,
    // functions
    onSelectCountry: () => {},
    sendEventAnalytics: () => {},
    clearFormErrors: () => {},
    clearFormFieldError: () => {},
    findAddress: () => {},
    findExactAddressByMoniker: () => {},
    resetForm: () => {},
    setFormField: () => {},
    setAndValidateFormField: jest.fn(),
    touchedFormField: () => {},
    validateForm: () => {},
    isCheckout: false,
  }
  const renderComponent = buildComponentRender(
    shallowRender,
    AddressForm.WrappedComponent
  )

  describe('@renders', () => {
    it('should render default state', () => {
      expect(renderComponent(requiredProps).getTree()).toMatchSnapshot()
    })
  })

  describe('@lifecycle', () => {
    describe('constructor', () => {
      it('should init monikers in the internal state', () => {
        const { instance } = renderComponent(requiredProps)
        expect(instance.state.monikers).toEqual([])
      })
    })

    describe('@componentDidMount', () => {
      it('should validate forms', () => {
        const validateFormMock = jest.fn()
        const findAddressValidationSchema = {}
        const detailsValidationSchema = {}
        const addressValidationSchema = {}
        const { instance } = renderComponent({
          ...requiredProps,
          findAddressValidationSchema,
          detailsValidationSchema,
          addressValidationSchema,
          validateForm: validateFormMock,
        })
        instance.componentDidMount()
        expect(validateFormMock).toHaveBeenCalledWith(
          requiredProps.formNames.findAddress,
          findAddressValidationSchema
        )
        expect(validateFormMock).toHaveBeenCalledWith(
          requiredProps.formNames.details,
          detailsValidationSchema
        )
        expect(validateFormMock).toHaveBeenCalledWith(
          requiredProps.formNames.address,
          addressValidationSchema
        )
      })
    })

    describe('@componentDidUpdate', () => {
      it('should validate if `schemaHash` changes', () => {
        const validateFormMock = jest.fn()
        const findAddressValidationSchema = {}
        const detailsValidationSchema = {}
        const addressValidationSchema = {}
        const { instance } = renderComponent({
          ...requiredProps,
          schemaHash: '12345',
          findAddressValidationSchema,
          detailsValidationSchema,
          validateForm: validateFormMock,
        })
        instance.componentDidUpdate({ schemaHash: '67890' })
        expect(validateFormMock).toHaveBeenCalledWith(
          requiredProps.formNames.findAddress,
          findAddressValidationSchema
        )
        expect(validateFormMock).toHaveBeenCalledWith(
          requiredProps.formNames.details,
          detailsValidationSchema
        )
        expect(validateFormMock).toHaveBeenCalledWith(
          requiredProps.formNames.address,
          addressValidationSchema
        )
      })
    })

    describe('@componentWillUnmount', () => {
      it('should clear errors', () => {
        const clearFormErrorsMock = jest.fn()
        const { instance } = renderComponent({
          ...requiredProps,
          clearFormErrors: clearFormErrorsMock,
        })
        instance.componentWillUnmount()
        expect(clearFormErrorsMock).toHaveBeenCalledWith(
          requiredProps.formNames.findAddress
        )
        expect(clearFormErrorsMock).toHaveBeenCalledWith(
          requiredProps.formNames.details
        )
        expect(clearFormErrorsMock).toHaveBeenCalledWith(
          requiredProps.formNames.address
        )
      })
    })
  })

  describe('@events and functions', () => {
    describe('Select Address', () => {
      it('calls findExactAddressByMoniker', () => {
        const findExactAddressByMonikerMock = jest.fn()
        const { instance, wrapper } = renderComponent({
          ...requiredProps,
          countryCode: 'GBP',
          findExactAddressByMoniker: findExactAddressByMonikerMock,
        })
        instance.setState({
          monikers: [
            {
              moniker: '111 Something Close',
            },
          ],
        })
        wrapper.find(FindAddress).prop('handleAddressChange')({
          target: { selectedIndex: 1 },
        })
        expect(findExactAddressByMonikerMock).toHaveBeenCalledWith({
          country: 'GBP',
          moniker: '111 Something Close',
          formNames: requiredProps.formNames,
        })
      })
    })

    describe('Find Address Request', () => {
      const findAddressMock = jest.fn()
      const sendEventAnalyticsMock = jest.fn()
      const clearFormFieldErrorMock = jest.fn()
      findAddressMock.mockImplementation(() => {
        return Promise.resolve([
          {
            moniker: '111 Something Close',
          },
        ])
      })
      const { instance, wrapper } = renderComponent({
        ...requiredProps,
        countryCode: 'GBP',
        findAddressForm: {
          fields: {
            houseNumber: {
              value: '35',
            },
            postcode: {
              value: 'NW1 7HQ',
            },
          },
        },
        findAddress: findAddressMock,
        sendEventAnalytics: sendEventAnalyticsMock,
        clearFormFieldError: clearFormFieldErrorMock,
      })
      it('calls findAddress when handleFindAddressRequest invoked', () => {
        wrapper.find(FindAddress).prop('handleFindAddressRequest')()
        expect(findAddressMock).toHaveBeenCalledWith({
          data: {
            country: 'GBP',
            postcode: 'NW1 7HQ',
            address: '35',
          },
          formNames: requiredProps.formNames,
        })
      })
      it('calls sendEventAnalytics when handleFindAddressRequest invoked', () => {
        wrapper.find(FindAddress).prop('handleFindAddressRequest')()
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event121',
        })
      })
      it('calls clearFormFieldError asynchronously when handleFindAddressRequest invoked', () => {
        return wrapper
          .find(FindAddress)
          .prop('handleFindAddressRequest')()
          .then(() => {
            expect(clearFormFieldErrorMock).toHaveBeenCalledWith(
              requiredProps.formNames.findAddress,
              'findAddress'
            )
          })
      })
      it('should setState with monikers asynchronously', () => {
        return wrapper
          .find(FindAddress)
          .prop('handleFindAddressRequest')()
          .then(() => {
            expect(instance.state.monikers).toEqual([
              { moniker: '111 Something Close' },
            ])
          })
      })
    })

    describe('Select Country', () => {
      it('change Country call setFieldCountry update', () => {
        const sendEventAnalyticsMock = jest.fn()
        const setFormFieldMock = jest.fn()
        const onSelectCountryMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          setFormField: setFormFieldMock,
          onSelectCountry: onSelectCountryMock,
          sendEventAnalytics: sendEventAnalyticsMock,
        })
        wrapper.find(FindAddress).prop('setCountry')({
          target: { value: 'France' },
        })
        expect(onSelectCountryMock).toHaveBeenCalledWith('France')
        expect(setFormFieldMock).toHaveBeenCalledWith(
          requiredProps.formNames.address,
          'country',
          'France'
        )
        expect(setFormFieldMock).toHaveBeenCalledWith(
          requiredProps.formNames.address,
          'state',
          ''
        )
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event135',
        })
      })

      it('should call `validateDDPForCountry` if adding new delivery address in checkout', () => {
        const validateDDPForCountryMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          validateDDPForCountry: validateDDPForCountryMock,
          addressType: 'delivery',
          isCheckout: true,
        })
        wrapper.find(FindAddress).prop('setCountry')({
          target: { value: 'France' },
        })

        expect(validateDDPForCountryMock).toHaveBeenCalledWith('France')
      })

      it('should not call `validateDDPForCountry` if adding new address other than delivery', () => {
        const validateDDPForCountryMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          validateDDPForCountry: validateDDPForCountryMock,
          addressType: 'billing',
          isCheckout: true,
        })
        wrapper.find(FindAddress).prop('setCountry')({
          target: { value: 'France' },
        })

        expect(validateDDPForCountryMock).not.toHaveBeenCalled()
      })

      it('should not call `validateDDPForCountry` if adding new delivery address anywhere other than checkout', () => {
        const validateDDPForCountryMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          validateDDPForCountry: validateDDPForCountryMock,
          addressType: 'delivery',
          isCheckout: false,
        })
        wrapper.find(FindAddress).prop('setCountry')({
          target: { value: 'France' },
        })

        expect(validateDDPForCountryMock).not.toHaveBeenCalled()
      })
    })

    describe('Enter Address Manually', () => {
      const sendEventAnalyticsMock = jest.fn()
      const setFormFieldMock = jest.fn()

      const render = compose(
        mountRender,
        withStore({ features: { status: '' } })
      )
      const mountRenderComponent = buildComponentRender(
        render,
        AddressForm.WrappedComponent
      )

      const { wrapper } = mountRenderComponent({
        ...requiredProps,
        isFindAddressVisible: true,
        setFormField: setFormFieldMock,
        sendEventAnalytics: sendEventAnalyticsMock,
      })

      it('should setFormField (addressForm) with isManual to True when the switch to manual address button is clicked', () => {
        wrapper
          .find(FindAddress)
          .find('button')
          .at(1)
          .simulate('click', { preventDefault: jest.fn() })
        expect(setFormFieldMock).toHaveBeenCalledWith(
          'billingAddress',
          'isManual',
          true
        )
      })

      it('should send analytics event `event122` on click of the switch to manual address button', () => {
        wrapper
          .find(FindAddress)
          .find('button')
          .at(1)
          .simulate('click', { preventDefault: jest.fn() })
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event122',
        })
      })
    })

    describe('Set and Validate', () => {
      it('should set and validate field on change', () => {
        const setAndValidateFormFieldMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          detailsValidationSchema: {
            telephone: ['required'],
          },
          findAddressValidationSchema: {},
          setAndValidateFormField: setAndValidateFormFieldMock,
        })
        wrapper.find(FindAddress).prop('setAndValidateDetailsField')(
          'telephone'
        )({
          target: {
            value: '0818118181',
          },
        })
        expect(setAndValidateFormFieldMock).toHaveBeenCalledWith(
          requiredProps.formNames.details,
          'telephone',
          '0818118181',
          ['required']
        )
        wrapper.find(FindAddress).prop('setAndValidateFindAddressField')('')({
          target: {
            value: '',
          },
        })
        expect(setAndValidateFormFieldMock).toHaveBeenCalledTimes(2)
        wrapper.find(FindAddress).prop('setAndValidateAddressField')('')({
          target: {
            value: '',
          },
        })
        expect(setAndValidateFormFieldMock).toHaveBeenCalledTimes(3)
        wrapper.find(ManualAddress).prop('setAndValidateAddressField')('')({
          target: {
            value: '',
          },
        })
        expect(setAndValidateFormFieldMock).toHaveBeenCalledTimes(4)
      })
    })

    describe('Touched Field', () => {
      it('should be able to touch field', () => {
        const touchedFormFieldMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          touchedFormField: touchedFormFieldMock,
        })
        wrapper.find(FindAddress).prop('touchField')(
          requiredProps.formNames.details
        )('telephone')()
        expect(touchedFormFieldMock).toHaveBeenCalledWith(
          requiredProps.formNames.details,
          'telephone'
        )
        wrapper.find(ManualAddress).prop('touchField')(
          requiredProps.formNames.address
        )('address1')()
        expect(touchedFormFieldMock).toHaveBeenCalledWith(
          requiredProps.formNames.address,
          'address1'
        )
      })
    })

    describe('Delivery Country', () => {
      it('should send analytics event `event135` on change', () => {
        const sendEventAnalyticsMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          sendEventAnalytics: sendEventAnalyticsMock,
        })
        wrapper.find(FindAddress).prop('setCountry')({
          target: {},
        })
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event135',
        })
      })
    })

    describe('Postcode', () => {
      it('should send analytics event `event137` when focus leaves (FindAddress and AddressForm)', () => {
        const sendEventAnalyticsMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          sendEventAnalytics: sendEventAnalyticsMock,
        })
        wrapper.find(FindAddress).prop('touchField')(
          requiredProps.formNames.address
        )('postCode')()
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event137',
        })
      })
      it('should send analytics event `event137` when focus leaves (ManualAddress and address form)', () => {
        const sendEventAnalyticsMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          sendEventAnalytics: sendEventAnalyticsMock,
        })
        wrapper.find(FindAddress).prop('touchField')(
          requiredProps.formNames.address
        )('postCode')()
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event137',
        })
      })
    })

    describe('On "Clear Form" button click', () => {
      const render = compose(
        mountRender,
        withStore({ features: { status: '' } })
      )
      const mountRenderComponent = buildComponentRender(
        render,
        AddressForm.WrappedComponent
      )
      it('should reset forms', () => {
        const resetFormMock = jest.fn()
        const { wrapper } = mountRenderComponent({
          ...requiredProps,
          ...{
            addressForm: {
              fields: {
                address1: {
                  value: '',
                },
                address2: {
                  value: '',
                },
                postcode: {
                  value: '',
                },
                city: {
                  value: '',
                },
              },
            },
          },
          resetForm: resetFormMock,
        })
        wrapper
          .find(ManualAddress)
          .find('button')
          .first()
          .simulate('click', { preventDefault: jest.fn() })

        expect(resetFormMock).toHaveBeenCalledWith(
          requiredProps.formNames.address,
          {
            address1: '',
            address2: '',
            city: '',
            country: 'United Kingdom',
            county: '',
            postcode: '',
            state: '',
            isManual: true,
          }
        )
        expect(resetFormMock).toHaveBeenCalledWith(
          requiredProps.formNames.findAddress,
          {
            houseNumber: '',
            message: '',
            findAddress: '',
            selectAddress: '',
            postcode: '',
          }
        )
      })

      it('should validate address form', () => {
        const addressValidationSchema = {}
        const validateFormMock = jest.fn()
        const { wrapper } = mountRenderComponent({
          ...requiredProps,
          ...{
            addressForm: {
              fields: {
                address1: {
                  value: '',
                },
                address2: {
                  value: '',
                },
                postcode: {
                  value: '',
                },
                city: {
                  value: '',
                },
              },
            },
          },
          validateForm: validateFormMock,
          addressValidationSchema,
        })
        wrapper
          .find(ManualAddress)
          .find('button')
          .first()
          .simulate('click', { preventDefault: jest.fn() })
        expect(validateFormMock).toHaveBeenCalledWith(
          requiredProps.formNames.address,
          addressValidationSchema
        )
      })
    })

    describe('On "Find Address" button click', () => {
      it('should reset form to display Find Address', () => {
        const render = compose(
          mountRender,
          withStore({ features: { status: '' } })
        )
        const setFormFieldMock = jest.fn()
        const mountRenderComponent = buildComponentRender(
          render,
          AddressForm.WrappedComponent
        )
        const { instance, wrapper } = mountRenderComponent({
          ...requiredProps,
          ...{
            addressForm: {
              fields: {
                address1: {
                  value: '',
                },
                address2: {
                  value: '',
                },
                postcode: {
                  value: '',
                },
                city: {
                  value: '',
                },
              },
            },
          },
          canFindAddress: true,
          setFormField: setFormFieldMock,
        })
        instance.setState({
          monikers: [
            {
              moniker: '111 Something Close',
            },
          ],
        })
        expect(instance.state.monikers).toEqual([
          {
            moniker: '111 Something Close',
          },
        ])
        wrapper
          .find(ManualAddress)
          .find('button')
          .at(1)
          .simulate('click', { preventDefault: jest.fn() })
        expect(setFormFieldMock).toHaveBeenCalledWith(
          'billingAddress',
          'isManual',
          false
        )
        expect(instance.state.monikers).toEqual([])
      })
    })

    describe('First Line of Address', () => {
      it('should send analytics `event136` event when focus leaves', () => {
        const sendEventAnalyticsMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          touchedFormField: () => () => () => {},
          sendEventAnalytics: sendEventAnalyticsMock,
        })
        wrapper.find(ManualAddress).prop('touchField')(
          requiredProps.formNames.address
        )('address1')()
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event136',
        })
      })
    })

    describe('trimOnBlur', () => {
      it('should update FindAddress postcode field with trimmed content', () => {
        const { wrapper } = renderComponent({
          ...requiredProps,
          findAddressValidationSchema: {
            postcode: 'findaddress-postcodevalidation',
          },
        })
        wrapper.find(FindAddress).prop('onBlur')('postcode')({
          target: { value: ' abc ' },
        })
        expect(requiredProps.setAndValidateFormField).toHaveBeenCalledWith(
          requiredProps.formNames.findAddress,
          'postcode',
          'abc',
          'findaddress-postcodevalidation'
        )
      })
      it('should update ManualAddress postcode field with trimmed content', () => {
        const { wrapper } = renderComponent({
          ...requiredProps,
          addressValidationSchema: {
            postcode: 'address-postcodevalidation',
          },
        })
        wrapper.find(ManualAddress).prop('onBlur')('postcode')({
          target: { value: ' abc ' },
        })
        expect(requiredProps.setAndValidateFormField).toHaveBeenCalledWith(
          requiredProps.formNames.address,
          'postcode',
          'abc',
          'address-postcodevalidation'
        )
      })
    })
  })

  describe('@component helpers', () => {
    describe('getHasSelectedAddress', () => {
      it('should return true when address selected', () => {
        const selectedAddress = getHasSelectedAddress({
          fields: {
            address1: {
              value: 'Address 1',
            },
          },
        })
        expect(selectedAddress).toBe(true)
      })
      it('should return false when NO address selected', () => {
        const selectedAddress = getHasSelectedAddress({
          fields: {
            address1: {
              value: '',
            },
          },
        })
        expect(selectedAddress).toBe(false)
      })
    })

    describe('gethasFoundAddress', () => {
      it('should return true when findAddress has NO value', () => {
        const selectedAddress = gethasFoundAddress({
          fields: {
            findAddress: {
              value: '',
            },
          },
        })
        expect(selectedAddress).toBe(true)
      })
      it('should return false when NO value in findAddres', () => {
        const selectedAddress = gethasFoundAddress({
          fields: {
            findAddress: {
              value: 'touched',
            },
          },
        })
        expect(selectedAddress).toBe(false)
      })
    })
  })

  describe('@connected component', () => {
    const state = {
      config: configMock,
      account: {
        user: myAccountMock,
        myCheckoutDetails: {
          editingEnabled: false,
        },
      },
      forms: {
        account: {
          myCheckoutDetails: myCheckoutDetailsMocks,
        },
      },
      siteOptions: {
        ...siteOptionsMock,
        USStates: ['CA', 'AL'],
      },
    }
    const addressType = 'delivery'
    const formNames = getFormNames(addressType)
    const initialProps = {
      addressType: 'delivery',
      country: 'United Kingdom',
      formNames,
      getAddressFormFromState: getMCDAddressForm,
      isFindAddressVisible: true,
    }

    it('should wrap `AdressForm` component', () => {
      expect(AddressForm.WrappedComponent.name).toBe('AddressForm')
    })

    describe('mapDispatchToProps', () => {
      it('should return `clearFormErrors` action', () => {
        const { clearFormErrors } = mapDispatchToProps
        expect(clearFormErrors.name).toBe('clearFormErrors')
      })
      it('should return `clearFormFieldError` action', () => {
        const { clearFormFieldError } = mapDispatchToProps
        expect(clearFormFieldError.name).toBe('clearFormFieldError')
      })
      it('should return `findAddress` action', () => {
        const { findAddress } = mapDispatchToProps
        expect(findAddress.name).toBe('findAddress')
      })
      it('should return `findExactAddressByMoniker` action', () => {
        const { findExactAddressByMoniker } = mapDispatchToProps
        expect(findExactAddressByMoniker.name).toBe('findExactAddressByMoniker')
      })
      it('should return `resetForm` action', () => {
        const { resetForm } = mapDispatchToProps
        expect(resetForm.name).toBe('resetForm')
      })
      it('should return `setFormField` action', () => {
        const { setFormField } = mapDispatchToProps
        expect(setFormField.name).toBe('setFormField')
      })
      it('should return `setAndValidateFormField` action', () => {
        const { setAndValidateFormField } = mapDispatchToProps
        expect(setAndValidateFormField.name).toBe('setAndValidateFormField')
      })
      it('should return `touchedFormField` action', () => {
        const { touchedFormField } = mapDispatchToProps
        expect(touchedFormField.name).toBe('touchedFormField')
      })
      it('should return `validateForm` action', () => {
        const { validateForm } = mapDispatchToProps
        expect(validateForm.name).toBe('validateForm')
      })
    })

    describe('mapStateToProps', () => {
      it('should get addressForm fields from state (delivery) from state', () => {
        const { addressForm } = mapStateToProps(state, initialProps)
        expect(addressForm.fields).toBeDefined()
        expect(addressForm.fields).toEqual(
          expect.objectContaining({
            address1: {
              value: '2 Britten Close',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
          })
        )
      })
      it('should get addressForm fields from state (billing) from state', () => {
        const addressType = 'billing'
        const formNames = getFormNames(addressType)
        const { addressForm } = mapStateToProps(state, {
          ...initialProps,
          addressType,
          formNames,
        })
        expect(addressForm.fields).toBeDefined()
        expect(addressForm.fields).toEqual(
          expect.objectContaining({
            address1: {
              value: '35 Britten Close',
              isDirty: true,
              isTouched: false,
              isFocused: false,
            },
          })
        )
      })
      it('should return addressValidationSchema', () => {
        const { addressValidationSchema } = mapStateToProps(state, {
          ...initialProps,
        })
        expect(addressValidationSchema).toEqual({
          address1: ['required', 'noEmoji'],
          address2: 'noEmoji',
          postcode: ['required', expect.any(Function), 'noEmoji'],
          city: ['required', 'noEmoji'],
          country: ['required', expect.any(Function)],
        })
      })
      it('should get country from state from state', () => {
        const { country } = mapStateToProps(state, initialProps)
        expect(country).toBe('United Kingdom')
      })
      it('should return default country if no state (United Kingdom)', () => {
        const { country } = mapStateToProps({}, initialProps)
        expect(country).toBe('United Kingdom')
      })
      it('should get countries from config', () => {
        const { countries } = mapStateToProps(state, initialProps)
        expect(countries).toEqual(
          expect.arrayContaining([
            'Albania',
            'United Kingdom',
            'Virgin Islands British',
          ])
        )
      })
      it('should get countryCode from config', () => {
        const { countryCode } = mapStateToProps(state, initialProps)
        expect(countryCode).toEqual('GBR')
      })
      it('should return canFindAddress', () => {
        const { canFindAddress } = mapStateToProps(state, initialProps)
        expect(canFindAddress).toEqual(true)
      })
      it('should return canFindAddress', () => {
        const { canFindAddress } = mapStateToProps(
          {
            ...state,
            config: {},
          },
          initialProps
        )
        expect(canFindAddress).toEqual(false)
      })
      it('should get detailsForm from state (delivery)', () => {
        const { detailsForm } = mapStateToProps(state, initialProps)
        expect(detailsForm.fields).toBeDefined()
        expect(detailsForm.fields).toEqual(
          expect.objectContaining({
            title: {
              value: 'Mr',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
          })
        )
      })
      it('should get detailsForm from state (billing)', () => {
        const addressType = 'billing'
        const formNames = getFormNames(addressType)
        const { detailsForm } = mapStateToProps(state, {
          ...initialProps,
          addressType,
          formNames,
        })
        expect(detailsForm.fields).toBeDefined()
        expect(detailsForm.fields).toEqual(
          expect.objectContaining({
            firstName: {
              value: 'Jose Billing',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
          })
        )
      })
      it('should get detailsValidationSchema from state', () => {
        const { detailsValidationSchema } = mapStateToProps(state, initialProps)
        expect(detailsValidationSchema).toEqual({
          title: 'required',
          firstName: ['noEmoji', 'required'],
          lastName: ['noEmoji', 'required'],
          telephone: ['required', 'ukPhoneNumber', 'noEmoji'],
        })
      })
      it('should get findAddressForm from state', () => {
        const { findAddressForm } = mapStateToProps(state, initialProps)
        expect(findAddressForm).toEqual({
          fields: {
            postCode: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            houseNumber: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
            findAddress: {
              value: '',
              isDirty: false,
              isTouched: false,
              isFocused: false,
            },
          },
          isLoading: false,
          errors: {},
          message: {},
        })
      })
      it('should get findAddressValidationSchema from state', () => {
        const { findAddressValidationSchema } = mapStateToProps(
          state,
          initialProps
        )
        expect(findAddressValidationSchema).toEqual({
          postcode: ['required', expect.any(Function), 'noEmoji'],
          houseNumber: 'noEmoji',
          findAddress: expect.any(Function),
          selectAddress: [],
        })
      })
      it('should get findAddressValidationSchema emtpy when isFindAddressVisible is false', () => {
        const { findAddressValidationSchema } = mapStateToProps(state, {
          ...initialProps,
          isFindAddressVisible: false,
        })
        expect(findAddressValidationSchema).toEqual({})
      })
      it('should get formNames from constants', () => {
        const { formNames } = mapStateToProps(state, initialProps)
        expect(formNames).toEqual({
          address: 'deliveryAddressMCD',
          details: 'deliveryDetailsAddressMCD',
          findAddress: 'deliveryFindAddressMCD',
        })
      })
      it('should return isFindAddressVisible', () => {
        const { isFindAddressVisible } = mapStateToProps(state, initialProps)
        expect(isFindAddressVisible).toEqual(true)
      })
      it('should get postCodeRules from state', () => {
        const { postCodeRules } = mapStateToProps(state, initialProps)
        expect(postCodeRules).toEqual({
          pattern: {},
          stateFieldType: false,
          postcodeRequired: true,
          premisesRequired: false,
          premisesLabel: 'House number',
        })
      })
      it('should get schemaHash from state', () => {
        const { schemaHash } = mapStateToProps(state, initialProps)
        expect(schemaHash).toEqual('United Kingdom:true:true:true')
      })
      it('should get usStates from state', () => {
        const { usStates } = mapStateToProps(state, initialProps)
        expect(usStates).toEqual(['CA', 'AL'])
      })
    })
  })
})
