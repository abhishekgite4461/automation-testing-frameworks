import testComponentHelper from 'test/unit/helpers/test-component'
import ManualAddress from '../ManualAddress'
import Select from '../../FormComponents/Select/Select'

// constants
import checkoutAddressFormRules from '../../../../../shared/constants/checkoutAddressFormRules'

describe('<ManualAddress />', () => {
  const requiredProps = {
    addressForm: {
      fields: {},
      errors: {},
      message: {},
    },
    country: 'United Kingdom',
    canFindAddress: true,
    formNames: {
      address: 'deliveryAddressMCD',
      details: 'deliveryDetailsAddressMCD',
      findAddress: 'deliveryFindAddressMCD',
    },
    isFindAddressVisible: false,
    postCodeRules: checkoutAddressFormRules['United Kingdom'],
    usStates: ['AL', 'CA'],
    // functions
    setAndValidateAddressField: () => {},
    touchField: () => {},
    handleClearForm: () => {},
    handleSwitchToFindAddress: () => {},
  }
  const renderComponent = testComponentHelper(ManualAddress)

  describe('@renders', () => {
    it('should render default state', () => {
      expect(renderComponent(requiredProps).getTree()).toMatchSnapshot()
    })

    it('should not render Find Address Form when isFindAddressVisible is true', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        isFindAddressVisible: true,
      })
      expect(wrapper.find('.ManualAddress').length).toBe(0)
    })

    it('should render Find Address Form when isFindAddressVisible is false', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        isFindAddressVisible: false,
      })
      expect(wrapper.find('.ManualAddress').length).toBe(1)
    })

    it('should not render Find Address button when canFindAddress = false', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        canFindAddress: false,
      })
      expect(wrapper.find('.ManualAddress-link--right').length).toBe(0)
    })

    it('should render Find Address button when canFindAddress = true', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        canFindAddress: true,
      })
      expect(wrapper.find('.ManualAddress-link--right').length).toBe(1)
    })
  })

  describe('@events and functions', () => {
    it('should trigger setAndValidateAddressField', () => {
      const setAndValidateAddressFieldMock = jest.fn()
      const { wrapper } = renderComponent({
        ...requiredProps,
        setAndValidateAddressField: setAndValidateAddressFieldMock,
      })
      wrapper.find('.ManualAddress-address1').prop('setField')()
      expect(setAndValidateAddressFieldMock).toHaveBeenCalledTimes(1)
      wrapper.find('.ManualAddress-address2').prop('setField')()
      expect(setAndValidateAddressFieldMock).toHaveBeenCalledTimes(2)
      wrapper.find('.ManualAddress-postcode').prop('setField')()
      expect(setAndValidateAddressFieldMock).toHaveBeenCalledTimes(3)
      wrapper.find('.ManualAddress-city').prop('setField')()
      expect(setAndValidateAddressFieldMock).toHaveBeenCalledTimes(4)
    })

    it('should trigger setAndValidateAddressField on state field type = select', () => {
      const selectOnChangeMock = jest.fn()
      const setAndValidateAddressFieldMock = () => selectOnChangeMock
      const { wrapper } = renderComponent({
        ...requiredProps,
        country: 'United States',
        postCodeRules: checkoutAddressFormRules['United States'],
        addressForm: {
          fields: {
            state: {
              value: 'CA',
            },
          },
        },
        setAndValidateAddressField: setAndValidateAddressFieldMock,
      })
      const selectComponent = wrapper.find(Select).find({ name: 'state' })
      selectComponent.prop('onChange')()
      expect(selectOnChangeMock).toHaveBeenCalledTimes(1)
    })

    it('should trigger setAndValidateAddressField on state field type = input', () => {
      const setAndValidateAddressFieldMock = jest.fn()
      const { wrapper } = renderComponent({
        ...requiredProps,
        country: 'Spain',
        postCodeRules: checkoutAddressFormRules.Spain,
        setAndValidateAddressField: setAndValidateAddressFieldMock,
      })
      const selectComponent = wrapper.find('Connect(Input) [name="state"]')
      selectComponent.prop('setField')()
      expect(setAndValidateAddressFieldMock).toHaveBeenCalledTimes(1)
    })

    it('should trigger touchField (address1, postCode)', () => {
      const addressFormTouchFieldMock = jest.fn()
      const touchFieldMock = () => addressFormTouchFieldMock
      const { wrapper } = renderComponent({
        ...requiredProps,
        country: 'Spain',
        postCodeRules: checkoutAddressFormRules.Spain,
        touchField: touchFieldMock,
      })
      wrapper.find('.ManualAddress-address1').prop('touchedField')(
        requiredProps.formNames.address
      )
      expect(addressFormTouchFieldMock).toHaveBeenCalledTimes(1)
      expect(addressFormTouchFieldMock).toHaveBeenCalledWith(
        requiredProps.formNames.address
      )
      wrapper.find('.ManualAddress-postcode').prop('touchedField')(
        requiredProps.formNames.address
      )
      expect(addressFormTouchFieldMock).toHaveBeenCalledTimes(2)
      expect(addressFormTouchFieldMock).toHaveBeenCalledWith(
        requiredProps.formNames.address
      )
      wrapper.find('Connect(Input) [name="state"]').prop('touchedField')(
        requiredProps.formNames.address
      )
      expect(addressFormTouchFieldMock).toHaveBeenCalledTimes(3)
    })

    it('should trigger handleClearForm', () => {
      const handleClearFormMock = jest.fn()
      const { wrapper } = renderComponent({
        ...requiredProps,
        handleClearForm: handleClearFormMock,
      })
      wrapper.find('.ManualAddress-link--left').prop('onClick')()
      expect(handleClearFormMock).toHaveBeenCalledTimes(1)
    })

    it('should trigger handleSwitchToFindAddress', () => {
      const handleSwitchToFindAddressMock = jest.fn()
      const { wrapper } = renderComponent({
        ...requiredProps,
        canFindAddress: true,
        handleSwitchToFindAddress: handleSwitchToFindAddressMock,
      })
      wrapper.find('.ManualAddress-link--right').prop('onClick')()
      expect(handleSwitchToFindAddressMock).toHaveBeenCalledTimes(1)
    })

    it('should trigger onBlur', () => {
      const onBlurField = jest.fn()
      const onBlur = jest.fn(() => onBlurField)
      const { wrapper } = renderComponent({
        ...requiredProps,
        onBlur,
      })
      expect(onBlur).toHaveBeenCalledTimes(1)
      expect(onBlur).toHaveBeenCalledWith('postcode')
      expect(onBlurField).not.toHaveBeenCalled()
      wrapper.find('.ManualAddress-postcode').prop('onBlur')()
      expect(onBlurField).toHaveBeenCalledTimes(1)
    })
  })
})
