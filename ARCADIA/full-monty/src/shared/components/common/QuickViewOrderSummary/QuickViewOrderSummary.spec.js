import testComponentHelper from 'test/unit/helpers/test-component'
import QuickViewOrderSummary from './QuickViewOrderSummary'

jest.mock('../../../lib/analytics/analytics', () => ({
  eventBasedAnalytics: jest.fn(),
}))
import { eventBasedAnalytics } from '../../../lib/analytics/analytics'

describe('<QuickViewOrderSummary/>', () => {
  const initialProps = {
    openMiniBag: jest.fn(),
    l: jest.fn(),
  }

  const renderComponent = testComponentHelper(QuickViewOrderSummary)

  describe('@renders', () => {
    it('should render correct default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })
  })

  describe('onClick', () => {
    beforeEach(() => {
      jest.resetAllMocks()
    })
    it('calls eventBasedAnalytics() and openMiniBag()', () => {
      const { instance } = renderComponent(initialProps)
      expect(eventBasedAnalytics).not.toBeCalled()
      expect(instance.props.openMiniBag).not.toBeCalled()
      instance.onClick()
      expect(eventBasedAnalytics).toHaveBeenCalledTimes(1)
      expect(eventBasedAnalytics).toHaveBeenCalledWith({
        events: 'event124',
      })
      expect(instance.props.openMiniBag).toHaveBeenCalledTimes(1)
    })
  })
})
