import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Button from '../Button/Button'

import { eventBasedAnalytics } from '../../../lib/analytics/analytics'

export default class QuickViewOrderSummary extends Component {
  static propTypes = {
    l: PropTypes.func,
    openMiniBag: PropTypes.func,
  }

  onClick = () => {
    const { openMiniBag } = this.props
    eventBasedAnalytics({
      events: 'event124',
    })
    if (openMiniBag) openMiniBag()
  }

  render() {
    const { l } = this.props
    return (
      <Button
        className="QuickViewOrderSummary-button"
        clickHandler={this.onClick}
      >
        {l`Order summary`}
      </Button>
    )
  }
}
