import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import url from 'url'
import {
  getRedirectURL,
  getUserGeoPreference,
  getURLPath,
} from '../../../reducers/common/geoIPReducer'
import { getBrandConfigByPreferredISO } from '../../../../server/config'
import { closeModal } from '../../../actions/common/modalActions'
import { sendGeoIPEvent } from '../../../actions/common/googleAnalyticsActions'
import { error, info } from '../../../../client/lib/logger'
import Button from '../Button/Button'
import GeoIPPixelCookies from './GeoIPPixelCookies'

export const addClientURLPartsToLink = (link) => {
  const { port = null, protocol = null } = window.location
  link = protocol ? `${protocol}//${link}` : link

  const parsedLink = url.parse(link)
  delete parsedLink.host
  delete parsedLink.href
  parsedLink.port = port
  return url.format(parsedLink)
}

@connect(
  (state) => {
    const redirectURL = getRedirectURL(state)
    const userGeoPreference = getUserGeoPreference(state)
    const showFootnote =
      redirectURL && /^\/(\?|#|$)/.test(getURLPath(redirectURL))

    return {
      redirectURL,
      currentSiteRegion: state.config.region,
      currentSiteISO: state.config.preferredISOs[0],
      userRegionPreference: getBrandConfigByPreferredISO(
        userGeoPreference,
        state.config.brandCode
      ).region,
      userGeoPreference,
      brandName: state.config.brandName,
      showFootnote,
      geoIPReduxState: state.geoIP,
      currentSiteLanguage: state.config.lang,
    }
  },
  { closeModal, sendGeoIPEvent }
)
export default class GeoIPModal extends React.Component {
  static propTypes = {
    redirectURL: PropTypes.string.isRequired,
    userGeoPreference: PropTypes.string.isRequired,
    l: PropTypes.func.isRequired,
    handleClose: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    showFootnote: PropTypes.bool.isRequired,
    geoIPReduxState: PropTypes.object.isRequired,
    currentSiteISO: PropTypes.string.isRequired,
    currentSiteRegion: PropTypes.string,
    userRegionPreference: PropTypes.string,
    handleRemoveCloseHandler: PropTypes.func,
    sendGeoIPEvent: PropTypes.func.isRequired,
  }

  static defaultProps = {
    currentSiteRegion: 'uk',
    userRegionPreference: 'uk',
  }

  constructor(props) {
    super(props)

    this.state = {
      cookieValue: null,
      shouldRedirect: false,
      shouldSetGeoIPCookies: false,
    }
  }

  /**
   * Logic is here in the componentWillMount in order pass the closer handler to
   * the parent Modal component as well as the separate ContentOverlay component;
   * following the observer pattern. We do this as it is bad practice and an anti-pattern
   * to store functions in the redux state, coupled with the way the Modal and
   * ContentOverlay components have been written and separated out. Ultimately
   * we should look into rewriting the Modal logic to accommodate this new use case:
   * requiring some extra logic to be handled upon closing the Modal.
   */
  componentWillMount() {
    const {
      currentSiteISO,
      userGeoPreference,
      currentSiteLanguage,
      sendGeoIPEvent,
    } = this.props

    sendGeoIPEvent(
      `Geo IP modal shown on ${currentSiteISO} for redirection to ${userGeoPreference}`,
      'Modal shown',
      `Modal language: ${currentSiteLanguage}`
    )

    this.props.handleClose(() => {
      if (this.finishedGeoPixel)
        throw new Error('Cannot call handleClose callback multiple times')

      return new Promise((resolve) => {
        this.finishedGeoPixel = resolve
        sendGeoIPEvent(
          `Geo IP modal dismissed on ${currentSiteISO} with prompt for redirection to ${userGeoPreference}`,
          'Modal dismissed',
          'Modal redirected: false'
        )
        this.setGeoIPCookies({
          cookieValue: this.props.currentSiteISO,
          shouldRedirect: false,
        })()
      })
    })
  }

  componentWillUnmount() {
    this.props.handleRemoveCloseHandler()
  }

  setGeoIPCookies = ({ cookieValue, shouldRedirect }) => () => {
    this.setState({
      shouldSetGeoIPCookies: true,
      shouldRedirect,
      cookieValue,
    })
  }

  handleGeoIPCookieError = (err) => {
    const { geoIPReduxState } = this.props
    const { shouldRedirect, cookieValue } = this.state

    error('GeoIP', {
      message: 'GeoIP pixel error when setting cookie for all hostnames',
      sourceURL: window.location.href,
      geoPixelURL: err.nativeEvent.currentTarget.currentSrc,
      newGeoPreference: cookieValue,
      userRequestedRedirect: shouldRedirect,
      geoIPReduxState,
    })
  }

  handleGeoIPCookieSuccess = () => {
    const { shouldRedirect, cookieValue } = this.state
    const {
      redirectURL,
      geoIPReduxState,
      currentSiteISO,
      closeModal,
      userGeoPreference,
      sendGeoIPEvent,
    } = this.props

    if (shouldRedirect) {
      info('GeoIP', {
        message: 'User has been redirected to their preferred site',
        sourceURL: window.location.href,
        newGeoPreference: cookieValue,
        geoIPReduxState,
        redirectURL,
      })
      sendGeoIPEvent(
        `Geo IP modal redirected from ${currentSiteISO} to ${userGeoPreference}`,
        'Modal redirection',
        'Modal redirected: true'
      )
      return window.location.assign(addClientURLPartsToLink(redirectURL))
    }
    info('GeoIP', {
      message: 'User has chosen to stay on the requested site',
      sourceURL: window.location.href,
      newGeoPreference: currentSiteISO,
      geoIPReduxState,
      redirectURL,
    })
    sendGeoIPEvent(
      `Geo IP modal closed on ${currentSiteISO} for redirection to ${userGeoPreference}`,
      'Modal redirection',
      'Modal redirected: false'
    )
    if (this.finishedGeoPixel) {
      this.finishedGeoPixel()
    } else {
      closeModal()
    }
  }

  render() {
    const {
      currentSiteRegion,
      userRegionPreference,
      userGeoPreference,
      currentSiteISO,
      l,
      showFootnote,
    } = this.props
    const { shouldSetGeoIPCookies, cookieValue } = this.state

    return (
      <div className="GeoIP" data-id="GeoIPModal">
        <h3 className="GeoIP-title">{l`Country preferences`}</h3>
        <p className="GeoIP-info">{l`You are viewing the website for ${l(
          currentSiteRegion
        )}. Would you like to view the website for ${l(
          userRegionPreference
        )} instead?`}</p>
        <Button
          className="GeoIP-redirectURL Button--primary"
          clickHandler={this.setGeoIPCookies({
            cookieValue: userGeoPreference,
            shouldRedirect: true,
          })}
        >{l`Yes, redirect me`}</Button>
        <Button
          clickHandler={this.setGeoIPCookies({
            cookieValue: currentSiteISO,
            shouldRedirect: false,
          })}
          className="GeoIP-remain Button--secondary"
        >{l`No, I want to stay on the ${l(currentSiteRegion)} website`}</Button>
        {showFootnote && (
          <p
            data-id="GeoIPModal-Footnote"
            className="GeoIP-footnote"
          >{l`NOTE: If you choose to be redirected, you will be taken to the home page.`}</p>
        )}
        {shouldSetGeoIPCookies && (
          <GeoIPPixelCookies
            handleSuccess={this.handleGeoIPCookieSuccess}
            handleError={this.handleGeoIPCookieError}
            cookieValue={cookieValue}
          />
        )}
      </div>
    )
  }
}
