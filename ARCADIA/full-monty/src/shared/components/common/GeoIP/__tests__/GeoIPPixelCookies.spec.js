import { JSDOM } from 'jsdom'
import GeoIPPixelCookies from '../GeoIPPixelCookies'
import createRender from 'test/unit/helpers/test-component'
import { getAllBrandHostnames } from '../../../../../server/config'

jest.mock('../../../../../client/lib/logger')

const render = createRender(GeoIPPixelCookies.WrappedComponent)
const props = {
  brandName: 'topshop',
  cookieValue: 'US',
  handleError: jest.fn(),
  handleSuccess: jest.fn(),
  geoIPReduxState: {
    hostname: 'local.m.us.topshop.com',
    geoISO: 'GB',
    storedGeoPreference: '',
  },
}
const topshopHostnames = getAllBrandHostnames(
  'topshop',
  props.geoIPReduxState.hostname
)

describe('GeoIPPixelCookies', () => {
  let wrapper

  beforeEach(() => {
    ;({ wrapper } = render(props))
    jest.clearAllMocks()
  })
  it('does not call the handleSuccess prop until images have loaded', () => {
    delete global.window
    global.window = new JSDOM(``, {
      url: `http://${props.geoIPReduxState.hostname}`,
    }).window
    ;({ wrapper } = render(props))

    expect(wrapper.find('img').length).toBe(topshopHostnames.length)

    wrapper.find('img').forEach((img, i) => {
      expect(img.props().style.visibility).toBe('hidden')
      expect(img.props().style.position).toBe('absolute')
      expect(img.props().style.left).toBe('-1000px')
      expect(img.props().src).toBe(
        `http://local.${topshopHostnames[i]}/api/geo-ip-pixel/${
          props.cookieValue
        }`
      )
      img.props().onLoad()
    })
    expect(props.handleSuccess).toHaveBeenCalled()
  })

  it('calls the handlError prop when loading geo Pixel images and there is an error', () => {
    expect(wrapper.find('img').length).toBe(topshopHostnames.length)

    wrapper
      .find('img')
      .first()
      .props()
      .onError({
        nativeEvent: {
          currentTarget: {
            currentSrc: window.location.href,
          },
        },
      })
    expect(props.handleError).toHaveBeenCalled()
  })
})
