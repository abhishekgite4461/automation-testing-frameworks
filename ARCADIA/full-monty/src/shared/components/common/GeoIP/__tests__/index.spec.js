import R from 'ramda'
import GeoIP from '../'
import createRender, {
  buildComponentRender,
  mountRender,
  withStore,
} from 'test/unit/helpers/test-component'
import { JSDOM } from 'jsdom'
import { SUPPORTED_FEATURES } from '../../../../constants/features'
import topshop from '../../../../../server/config/brands/topshop.json'
import { getAllBrandHostnames } from '../../../../../server/config'
import { info, error } from '../../../../../client/lib/logger'

jest.mock('../../../../../client/lib/cookie/utils')
jest.mock('../../../../../client/lib/logger')

const { FEATURE_GEOIP } = SUPPORTED_FEATURES
const render = createRender(GeoIP.WrappedComponent)
const mount = (state = {}) =>
  buildComponentRender(
    R.compose(
      mountRender,
      withStore({
        geoIP: {
          hostname: 'm.us.topshop.com',
          geoISO: 'AZ',
          storedGeoPreference: 'FR',
        },
        config: topshop.find((conf) => conf.region === 'us'),
        features: {
          status: {
            [FEATURE_GEOIP]: true,
          },
        },
        ...state,
      })
    ),
    GeoIP.WrappedComponent
  )

const props = {
  redirectURL: 'm.topshop.com',
  currentSiteRegion: 'us',
  currentSiteISO: 'US',
  userRegionPreference: 'uk',
  userGeoPreference: 'GB',
  brandName: 'topshop',
  l: (xs, ...ys) =>
    ys.length
      ? R.zip(
          xs,
          ys.concat(Array(xs.length - ys.length)).fill('', ys.length, xs.length)
        )
          .map(([a, b]) => a + b)
          .join('')
      : Array.isArray(xs)
        ? xs[0]
        : xs,
  closeModal: jest.fn(),
  geoIPReduxState: {
    hostname: 'm.us.topshop.com',
    geoISO: 'GB',
    storedGeoPreference: '',
  },
  showFootnote: true,
  handleClose: jest.fn(),
  handleRemoveCloseHandler: jest.fn(),
  sendGeoIPEvent: jest.fn(),
  currentSiteLanguage: 'en',
}
const topshopHostnames = getAllBrandHostnames(
  'topshop',
  props.geoIPReduxState.hostname
)

describe('GeoIP Modal', () => {
  let wrapper

  beforeEach(() => {
    delete global.window
    global.window = new JSDOM(``, { url: 'http://local.m.topshop.com' }).window
    ;({ wrapper } = render(props))
    jest.clearAllMocks()
  })

  it('renders a redirect button', () => {
    const wrapperLink = wrapper.find('Button').first()
    expect(wrapperLink.length).toBe(1)
    expect(wrapperLink.dive().text()).toBe('Yes, redirect me')
  })

  it('renders an informative message', () => {
    const wrapperText = wrapper.find('p').first()
    expect(wrapperText.length).toBe(1)
    expect(wrapperText.text()).toBe(
      'You are viewing the website for us. Would you like to view the website for uk instead?'
    )
  })

  it('renders a button to close the modal and let the user stay where they are', () => {
    const wrapperButton = wrapper.find('Button').last()
    expect(wrapperButton.length).toBe(1)
    expect(wrapperButton.dive().text()).toBe(
      'No, I want to stay on the us website'
    )
  })

  it('displays a message describing what will happen should a user redirect', () => {
    const wrapperFootnote = wrapper.find('[data-id="GeoIPModal-Footnote"]')
    expect(wrapperFootnote.length).toBe(1)
    expect(wrapperFootnote.text()).toBe(
      'NOTE: If you choose to be redirected, you will be taken to the home page.'
    )
  })

  it('does not display the modal footnote if redirecting to a PDP page', () => {
    ;({ wrapper } = render({ ...props, showFootnote: false }))
    const wrapperFootnote = wrapper.find('[data-id="GeoIPModal-Footnote"]')
    expect(wrapperFootnote.length).toBe(0)
  })

  it('renders a heading', () => {
    const wrapperHeading = wrapper.find('h3')
    expect(wrapperHeading.length).toBe(1)
    expect(wrapperHeading.text()).toBe('Country preferences')
  })

  it('does not redirect a user until all images have loaded', () => {
    const { wrapper: geoIP } = mount({})({
      l: () => {},
      handleClose: () => {},
      ...props,
    })

    jest.spyOn(window.location, 'assign')
    const originalHref = window.location.href
    window.location.href = `https://${props.geoIPReduxState.hostname}`
    expect(props.sendGeoIPEvent).toHaveBeenCalledWith(
      'Geo IP modal shown on US for redirection to GB',
      'Modal shown',
      `Modal language: ${props.currentSiteLanguage}`
    )

    const wrapperRedirectButton = geoIP.find('Button').first()
    wrapperRedirectButton.simulate('click')

    geoIP.find('img').forEach((img, i) => {
      expect(window.location.assign).not.toHaveBeenCalled()
      expect(img.props().style.visibility).toBe('hidden')
      expect(img.props().style.position).toBe('absolute')
      expect(img.props().style.left).toBe('-1000px')
      expect(img.props().src).toBe(
        `http://${topshopHostnames[i]}/api/geo-ip-pixel/${
          props.userGeoPreference
        }`
      )
      img.props().onLoad()
    })
    expect(info).toHaveBeenCalledWith('GeoIP', {
      message: 'User has been redirected to their preferred site',
      sourceURL: window.location.href,
      newGeoPreference: props.userGeoPreference,
      geoIPReduxState: props.geoIPReduxState,
      redirectURL: props.redirectURL,
    })
    expect(props.sendGeoIPEvent).toHaveBeenCalledWith(
      'Geo IP modal redirected from US to GB',
      'Modal redirection',
      'Modal redirected: true'
    )
    expect(window.location.assign).toHaveBeenCalledWith(
      `http://${props.redirectURL}/`
    )

    window.location.assign.mockRestore()
    window.location.href = originalHref
  })

  it('logs an error when loading geo Pixel images and there is an error', () => {
    const geoIPProps = {
      redirectURL: 'https://m.fr.topshop.com',
      hostname: 'm.us.topshop.com',
      geoISO: 'AZ',
      storedGeoPreference: 'FR',
    }

    const { wrapper: geoIP } = mount({
      geoIP: geoIPProps,
    })({ l: () => {}, handleClose: () => {}, ...props })

    jest.spyOn(window.location, 'assign')

    const wrapperRedirectButton = geoIP.find('Button').first()
    wrapperRedirectButton.simulate('click')
    expect(geoIP.find('img').length).toBe(topshopHostnames.length)

    geoIP
      .find('img')
      .first()
      .props()
      .onError({
        nativeEvent: {
          currentTarget: {
            currentSrc: window.location.href,
          },
        },
      })
    expect(error).toHaveBeenCalled()
    expect(window.location.assign).not.toHaveBeenCalled()

    window.location.assign.mockRestore()
  })

  it('sets the cookies to the current site ISO if user selects to stay', () => {
    const geoIPProps = {
      ...props,
      redirectURL: 'stage.m.us.topshop.com',
      geoIPReduxState: {
        ...props.geoIPReduxState,
        hostname: 'stage.m.topshop.com',
      },
    }

    const { wrapper: geoIP } = mount()({
      l: () => {},
      handleClose: () => {},
      ...geoIPProps,
    })

    jest.spyOn(window.location, 'assign')
    window.location.href = `https://${geoIPProps.hostname}`

    const button = geoIP.find('Button').last()
    button.simulate('click')
    expect(geoIP.find('img').length).toBe(topshopHostnames.length)

    const imgs = geoIP.find('img')

    imgs.forEach((img, i) => {
      expect(window.location.assign).not.toHaveBeenCalled()
      expect(img.props().style.visibility).toBe('hidden')
      expect(img.props().style.position).toBe('absolute')
      expect(img.props().style.left).toBe('-1000px')
      expect(img.props().src).toBe(
        `http://${topshopHostnames[i]}/api/geo-ip-pixel/${props.currentSiteISO}`
      )
      img.props().onLoad()
    })
    expect(props.sendGeoIPEvent).toHaveBeenCalledWith(
      'Geo IP modal closed on US for redirection to GB',
      'Modal redirection',
      'Modal redirected: false'
    )
    expect(info).toHaveBeenCalledWith('GeoIP', {
      message: 'User has chosen to stay on the requested site',
      sourceURL: window.location.href,
      newGeoPreference: geoIPProps.currentSiteISO,
      geoIPReduxState: geoIPProps.geoIPReduxState,
      redirectURL: geoIPProps.redirectURL,
    })
    expect(window.location.assign).not.toHaveBeenCalled()
    expect(geoIPProps.closeModal).toHaveBeenCalled()

    window.location.assign.mockRestore()
  })

  it('sets the cookies to the current site ISO if user closes modal', () => {
    let promise
    const handleClose = (handler) => {
      promise = handler()
    }
    const geoIPProps = {
      ...props,
      redirectURL: 'stage.m.us.topshop.com',
      geoIPReduxState: {
        ...props.geoIPReduxState,
        hostname: 'stage.m.topshop.com',
      },
    }

    const { wrapper: geoIP } = mount()({
      l: () => {},
      ...geoIPProps,
      handleClose,
    })
    expect(geoIP.find('img').length).toBe(topshopHostnames.length)

    geoIP.find('img').forEach((img, i) => {
      expect(img.props().style.visibility).toBe('hidden')
      expect(img.props().style.position).toBe('absolute')
      expect(img.props().style.left).toBe('-1000px')
      expect(img.props().src).toBe(
        `http://${topshopHostnames[i]}/api/geo-ip-pixel/${props.currentSiteISO}`
      )
      img.props().onLoad()
    })

    return promise
  })

  it('sets the cookies on a mobile main dev domain', () => {
    const geoIPProps = {
      ...props,
      redirectURL: 'foobar.m.us.topshop.com',
      geoIPReduxState: {
        ...props.geoIPReduxState,
        hostname: 'foobar.m.topshop.com',
      },
    }
    const { wrapper: geoIP } = mount({ geoIP: geoIPProps.geoIPReduxState })({
      l: () => {},
      handleClose: () => {},
      ...geoIPProps,
    })

    geoIP
      .find('Button')
      .last()
      .simulate('click')

    expect(geoIP.find('img').length).toBe(topshopHostnames.length)

    geoIP.find('img').forEach((img, i) => {
      expect(img.props().src).toBe(
        `http://foobar.${topshopHostnames[i]}/api/geo-ip-pixel/${
          props.currentSiteISO
        }`
      )
    })
  })

  it('sets the cookies on a mobile AWS domain', () => {
    const geoIPProps = {
      ...props,
      redirectURL: 'foo-m-us-topshop-com.digital.arcadiagroup.co.uk',
      geoIPReduxState: {
        ...props.geoIPReduxState,
        hostname: 'foo-m-topshop-com.digital.arcadiagroup.co.uk',
      },
    }
    const { wrapper: geoIP } = mount({ geoIP: geoIPProps.geoIPReduxState })({
      l: () => {},
      handleClose: () => {},
      ...geoIPProps,
    })

    geoIP
      .find('Button')
      .last()
      .simulate('click')

    expect(geoIP.find('img').length).toBe(topshopHostnames.length)

    geoIP.find('img').forEach((img, i) => {
      expect(img.props().src).toBe(
        `http://foo-${topshopHostnames[i].replace(
          /\./g,
          '-'
        )}.digital.arcadiagroup.co.uk/api/geo-ip-pixel/${props.currentSiteISO}`
      )
    })
  })

  it('adds port number if used', () => {
    delete global.window
    global.window = new JSDOM(``, {
      url: 'http://local.m.topshop.com:8080',
    }).window
    jest.spyOn(window.location, 'assign')

    const geoIPProps = {
      ...props,
      redirectURL: 'local.m.us.topshop.com?foo=bar',
      geoIPReduxState: {
        ...props.geoIPReduxState,
        hostname: 'local.m.topshop.com',
      },
    }
    const { wrapper: geoIP } = mount({ geoIP: geoIPProps.geoIPReduxState })({
      l: () => {},
      handleClose: () => {},
      ...geoIPProps,
    })
    geoIP
      .find('Button')
      .first()
      .simulate('click')
    geoIP.find('img').forEach((img, i) => {
      expect(img.props().src).toBe(
        `http://local.${topshopHostnames[i]}:8080/api/geo-ip-pixel/${
          props.userGeoPreference
        }`
      )
      img.props().onLoad()
    })

    expect(window.location.assign).toHaveBeenCalledWith(
      'http://local.m.us.topshop.com:8080/?foo=bar'
    )
  })

  it('if handleClose callback is called multiple times, throw an error', () => {
    const handleClose = (handler) => {
      handler()
      handler()
    }
    expect(() => render({ ...props, handleClose })).toThrow(
      'Cannot call handleClose callback multiple times'
    )
  })

  it('removes handleClose callback when the GeoIP Modal unmounts', () => {
    wrapper.unmount()
    expect(props.handleRemoveCloseHandler).toHaveBeenCalled()
  })
})

describe('mapStateToProps', () => {
  it('passes the right props to GeoIP', () => {
    const geoIPProps = {
      redirectURL: 'm.fr.topshop.com',
      currentSiteRegion: 'us',
      currentSiteISO: 'US',
      userRegionPreference: 'fr',
      userGeoPreference: 'FR',
      brandName: 'topshop',
      showFootnote: true,
      geoIPReduxState: {
        hostname: 'm.us.topshop.com',
        geoISO: 'AZ',
        storedGeoPreference: 'FR',
      },
      currentSiteLanguage: 'en',
      sendGeoIPEvent: () => {},
    }
    const { wrapper } = mount()({
      l: () => {},
      handleClose: () => {},
      ...geoIPProps,
    })

    expect(wrapper.find(GeoIP.WrappedComponent).props()).toMatchObject({
      redirectURL: 'm.fr.topshop.com',
      currentSiteRegion: 'us',
      currentSiteISO: 'US',
      userRegionPreference: 'fr',
      userGeoPreference: 'FR',
      brandName: 'topshop',
      showFootnote: true,
      geoIPReduxState: {
        hostname: 'm.us.topshop.com',
        geoISO: 'AZ',
        storedGeoPreference: 'FR',
      },
      currentSiteLanguage: 'en',
    })
  })

  it('passes the right props to GeoIP for PDP case', () => {
    const geoIPProps = {
      redirectURL: 'm.fr.topshop.com/fr/tsfr/clothing/robe-1',
      currentSiteRegion: 'us',
      currentSiteISO: 'US',
      userRegionPreference: 'fr',
      userGeoPreference: 'FR',
      brandName: 'topshop',
      showFootnote: false,
      geoIPReduxState: {
        redirectURL: 'm.fr.topshop.com/fr/tsfr/clothing/robe-1',
        hostname: 'm.us.topshop.com/en/tsus/clothing/dress-1',
        geoISO: 'AZ',
        storedGeoPreference: 'FR',
      },
      currentSiteLanguage: 'en',
      sendGeoIPEvent: () => {},
    }
    const { wrapper } = mount()({
      l: () => {},
      handleClose: () => {},
      ...geoIPProps,
    })

    expect(wrapper.find(GeoIP.WrappedComponent).props()).toMatchObject({
      redirectURL: 'm.fr.topshop.com/fr/tsfr/clothing/robe-1',
      currentSiteRegion: 'us',
      currentSiteISO: 'US',
      userRegionPreference: 'fr',
      userGeoPreference: 'FR',
      brandName: 'topshop',
      showFootnote: false,
      geoIPReduxState: {
        redirectURL: 'm.fr.topshop.com/fr/tsfr/clothing/robe-1',
        hostname: 'm.us.topshop.com/en/tsus/clothing/dress-1',
        geoISO: 'AZ',
        storedGeoPreference: 'FR',
      },
      currentSiteLanguage: 'en',
    })
  })
})
