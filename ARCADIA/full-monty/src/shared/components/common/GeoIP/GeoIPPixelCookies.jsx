import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { getAllBrandHostnames } from '../../../../server/config'
import { handleDevRedirect } from '../../../reducers/common/geoIPReducer'
import { addClientURLPartsToLink } from './'

@connect((state) => ({
  brandName: state.config.brandName,
  geoIPReduxState: state.geoIP,
}))
class GeoIPPixelCookies extends React.Component {
  static propTypes = {
    brandName: PropTypes.string.isRequired,
    cookieValue: PropTypes.string.isRequired,
    handleSuccess: PropTypes.func.isRequired,
    handleError: PropTypes.func.isRequired,
  }

  onAllImageResponse(numImages) {
    const { handleSuccess, handleError } = this.props
    let i = numImages
    return (err) => {
      if (err) {
        handleError(err)
      }

      --i
      if (i <= 0) {
        handleSuccess()
      }
    }
  }

  render() {
    const {
      brandName,
      geoIPReduxState: { hostname: currentHostname },
      cookieValue,
    } = this.props
    const hostnames = getAllBrandHostnames(brandName, currentHostname)
    const onEvent = this.onAllImageResponse(hostnames.length)

    return (
      <div>
        {hostnames.map((hostname) => (
          <img
            alt=""
            style={{
              visibility: 'hidden',
              position: 'absolute',
              left: '-1000px',
            }}
            key={hostname}
            src={`${addClientURLPartsToLink(
              handleDevRedirect(
                brandName,
                currentHostname,
                cookieValue,
                hostname
              )
            )}api/geo-ip-pixel/${cookieValue}`}
            onLoad={() => onEvent()}
            onError={(err) => onEvent(err)}
          />
        ))}
      </div>
    )
  }
}

export default GeoIPPixelCookies
