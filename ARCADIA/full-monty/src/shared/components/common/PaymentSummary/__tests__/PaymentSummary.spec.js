import React from 'react'
import { mount } from 'enzyme'
import PaymentSummary from '../PaymentSummary'

const context = {
  l: (value) => {
    return value
  },
  p: (value) => {
    return `£${value}`
  },
}

const props = {
  totalOrderPrice: '52.00',
  payments: [
    { method: 'Visa', cardNumber: '************1111', price: '52.00' },
    { method: 'Gift Card', cardNumber: '************1234', price: '10.00' },
  ],
}

const elements = {
  heading: '.qa-heading',
  tableHead: '.qa-thead',
  tableBody: '.qa-tbody',
  tableFoot: '.qa-tfoot',
  tableRow: '.qa-row',
  tableColumn: '.qa-column',
}

function getTableRowData(wrapper, selector) {
  const data = {}
  wrapper.find(`${selector} ${elements.tableRow}`).forEach((node) => {
    const columns = node.find(elements.tableColumn)
    data[columns.at(0).text()] = columns.at(1).text()
  })
  return data
}

describe('<PaymentSummary />', () => {
  it('should render as expected when all props have been specified', () => {
    const wrapper = mount(<PaymentSummary {...props} />, { context })
    const paymentSummary = {
      heading: wrapper
        .find(elements.heading)
        .at(0)
        .text(),
      table: {
        summary: getTableRowData(wrapper, elements.tableHead),
        paymentMethods: getTableRowData(wrapper, elements.tableBody),
      },
    }
    const expectedPaymentSummary = {
      heading: 'Payment details',
      table: {
        summary: {
          'Total Cost': '£52.00',
        },
        paymentMethods: {
          'Visa **** **** **** 1111': '£52.00',
          'Gift Card **** **** **** 1234': '£10.00',
        },
      },
    }
    expect(paymentSummary).toEqual(expectedPaymentSummary)
  })
})
