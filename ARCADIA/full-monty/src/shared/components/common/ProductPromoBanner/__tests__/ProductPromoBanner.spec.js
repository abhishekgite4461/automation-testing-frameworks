import testComponentHelper from 'test/unit/helpers/test-component'
import ProductPromoBanner from '../ProductPromoBanner'

describe('<ProductPromoBanner/>', () => {
  const renderComponent = testComponentHelper(ProductPromoBanner)

  describe('@renders', () => {
    it('in default state', () => {
      expect(
        renderComponent({
          url:
            'http://media.topshop.com/wcsstore/Wallis/images/category_icons/promo_code_627587_small.png',
        }).getTree()
      ).toMatchSnapshot()
    })
  })
})
