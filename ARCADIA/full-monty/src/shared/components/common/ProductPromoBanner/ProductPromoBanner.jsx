import React from 'react'
import Image from '../Image/Image'

function ProductPromoBanner({ src }) {
  return <Image className={`ProductPromoBanner`} src={src} />
}

export default ProductPromoBanner
