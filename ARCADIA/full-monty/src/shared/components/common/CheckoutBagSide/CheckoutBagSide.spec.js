import testComponentHelper from 'test/unit/helpers/test-component'
import CheckoutBagSide from './CheckoutBagSide'
import Espot from '../../containers/Espot/Espot'

jest.mock('../../../lib/checkout-utilities/reshaper', () => ({
  selectedDeliveryLocation: jest.fn((deliveryLocations) => ({
    deliveryType: deliveryLocations[0].type,
    label: 'My shipping inf',
    additionalDescription: 'my addi info',
    shipModeId: 12,
  })),
}))

describe('<CheckoutBagSide />', () => {
  const renderComponent = testComponentHelper(CheckoutBagSide.WrappedComponent)
  const basicProps = {
    orderSummary: {
      basket: {
        subTotal: '10.00',
        total: '12.00',
        totalBeforeDiscount: '8.00',
        products: [],
        inventoryPositions: [],
      },
      deliveryLocations: [
        {
          type: 'HOME_DELIVERY',
          deliveryLocationType: 'HOME',
          selected: true,
        },
      ],
      estimatedDelivery: 'Today',
      storeDetails: { details: 'details' },
    },
    yourAddress: {
      fields: [{ value: 'My address1' }, { value: 'My address2' }],
    },
    yourDetails: { fields: [{ value: 'My details' }] },
    subTotal: '10.00',
    totalShoppingBagItems: 1,
  }
  describe('@renders', () => {
    it('in initial state', () => {
      expect(renderComponent().getTree()).toMatchSnapshot()
    })
    it('in isHomeDelivery state', () => {
      expect(renderComponent(basicProps).getTree()).toMatchSnapshot()
    })
    it('in NOT isHomeDelivery state', () => {
      const newOrderSummary = {
        ...basicProps.orderSummary,
        deliveryLocations: [
          {
            type: 'STORE_EXPRESS',
            deliveryLocationType: 'STORE',
            selected: true,
          },
        ],
      }
      expect(
        renderComponent({
          ...basicProps,
          orderSummary: newOrderSummary,
        }).getTree()
      ).toMatchSnapshot()
    })
    it('in className state', () => {
      expect(
        renderComponent({ ...basicProps, className: 'MyClassName' }).getTree()
      ).toMatchSnapshot()
    })
    it('in orderProducts state', () => {
      expect(
        renderComponent({
          ...basicProps,
          orderProducts: [{ productId: 'myProductId' }],
        }).getTree()
      ).toMatchSnapshot()
    })
    it('in scrollMinibag, inCheckout=true, canModify=false state', () => {
      expect(
        renderComponent({
          ...basicProps,
          scrollMinibag: (scrolled) => scrolled,
          inCheckout: true,
          canModify: false,
        }).getTree()
      ).toMatchSnapshot()
    })
    it('in path="/checkout" state', () => {
      expect(
        renderComponent({ ...basicProps, path: '/checkout' }).getTree()
      ).toMatchSnapshot()
    })
    it('in path="/delivery" state', () => {
      expect(
        renderComponent({ ...basicProps, path: '/delivery' }).getTree()
      ).toMatchSnapshot()
    })
    it('in path="/checkout/delivery" state', () => {
      expect(
        renderComponent({ ...basicProps, path: '/checkout/delivery' }).getTree()
      ).toMatchSnapshot()
    })
    it('in path="/checkout/delivery-payment" state', () => {
      expect(
        renderComponent({
          ...basicProps,
          path: '/checkout/delivery-payment',
        }).getTree()
      ).toMatchSnapshot()
    })
    it('apply the drawer CSS modifier if the prop "drawer" is true', () => {
      expect(
        renderComponent({ ...basicProps, drawer: true }).wrapper.hasClass(
          'CheckoutBagSide--drawer'
        )
      ).toBe(true)
    })

    describe('header', () => {
      it('should not render the totalShoppingBagItems if the value is 0', () => {
        const { wrapper } = renderComponent({
          ...basicProps,
          totalShoppingBagItems: 0,
        })
        expect(wrapper.find('h3.CheckoutBagSide-title').text()).not.toMatch(
          `(0)`
        )
      })

      it('should render the totalShoppingBagItems', () => {
        const values = [1, 6, 24]
        values.forEach((totalShoppingBagItems) => {
          const { wrapper } = renderComponent({
            ...basicProps,
            totalShoppingBagItems,
          })
          expect(wrapper.find('h3.CheckoutBagSide-title').text()).toMatch(
            `(${totalShoppingBagItems})`
          )
        })
      })
    })

    describe('espots should', () => {
      it('render when order is completed', () => {
        const { wrapper, getTreeFor } = renderComponent({
          ...basicProps,
          orderCompleted: { orderId: '1234' },
        })
        expect(wrapper.find(Espot).map(getTreeFor)).toMatchSnapshot()
      })

      it('not render when order is not completed', () => {
        const { wrapper } = renderComponent({
          ...basicProps,
          orderCompleted: false,
        })
        expect(wrapper.find(Espot).length).toBe(0)
      })
    })

    describe('with orderSummary discounts', () => {
      const newOrderSummary = {
        ...basicProps.orderSummary,
        basket: {
          ...basicProps.orderSummary.basket,
          discounts: [
            {
              label: 'Topshop Card- £5 Welcome offer',
              value: '5.00',
            },
          ],
        },
      }

      it('should render discounts with showDiscounts prop', () => {
        const component = renderComponent({
          ...basicProps,
          orderSummary: newOrderSummary,
          showDiscounts: true,
          discountInfo: [
            {
              label: 'Topshop Card- £5 Welcome offer',
              value: '5.00',
            },
          ],
        })

        expect(
          component.wrapper.find('SimpleTotals').prop('discounts').length
        ).toBe(1)
        expect(component.getTree()).toMatchSnapshot()
      })

      it('should not render discounts without showDiscounts prop', () => {
        const component = renderComponent({
          ...basicProps,
          orderSummary: newOrderSummary,
        })

        expect(
          component.wrapper.find('SimpleTotals').prop('discounts').length
        ).toBe(0)
        expect(component.getTree()).toMatchSnapshot()
      })
    })
  })
})
