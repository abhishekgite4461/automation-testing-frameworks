import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { pathOr, pluck, path } from 'ramda'
import * as checkoutActions from '../../../actions/common/checkoutActions'
import OrderProducts from '../OrderProducts/OrderProducts'
import Espot from '../../containers/Espot/Espot'
import SimpleTotals from '../../common/SimpleTotals/SimpleTotals'
import DeliveryEstimate from '../../common/DeliveryEstimate/DeliveryEstimate'
import { selectedDeliveryLocation } from '../../../lib/checkout-utilities/reshaper'
import {
  extractDiscountInfo,
  getSubTotal,
} from '../../../selectors/checkoutSelectors'
import { getShoppingBagTotalItems } from '../../../selectors/shoppingBagSelectors'
import { getShoppingBagProductsWithInventory } from '../../../selectors/inventorySelectors'
import constants from '../../../../shared/constants/espotsDesktop'

@connect(
  (state) => ({
    inCheckout: state.routing.location.pathname.startsWith('/checkout'),
    yourDetails: state.forms.checkout.yourDetails,
    yourAddress: state.forms.checkout.yourAddress,
    pathName: state.routing.location.pathname,
    orderCompleted: path(['checkout', 'orderCompleted'], state),
    bagProducts: getShoppingBagProductsWithInventory(state),
    discountInfo: extractDiscountInfo(state),
    subTotal: getSubTotal(state),
    totalShoppingBagItems: getShoppingBagTotalItems(state),
  }),
  { ...checkoutActions }
)
export default class CheckoutBagSide extends Component {
  static propTypes = {
    className: PropTypes.string,
    pathName: PropTypes.string,
    canModify: PropTypes.bool,
    drawer: PropTypes.bool,
    inCheckout: PropTypes.bool,
    orderCompleted: PropTypes.object,
    showDiscounts: PropTypes.bool,
    orderProducts: PropTypes.array,
    orderSummary: PropTypes.object,
    bagProducts: PropTypes.array,
    yourAddress: PropTypes.object,
    yourDetails: PropTypes.object,
    scrollMinibag: PropTypes.func,
    discountInfo: PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.number,
        label: PropTypes.string,
      })
    ),
    subTotal: PropTypes.string,
    totalShoppingBagItems: PropTypes.number.isRequired,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  static defaultProps = {
    canModify: true,
    showDiscounts: false,
  }

  getDetailsInfo() {
    const { yourDetails } = this.props
    const details = pluck('value', yourDetails.fields)
    return details
  }

  getAddressInfo() {
    const { yourAddress } = this.props
    const address = pluck('value', yourAddress.fields)
    return { ...this.getDetailsInfo(), ...address }
  }

  getShippingInfo() {
    const { orderSummary, orderCompleted } = this.props
    const { deliveryMethod, deliveryCost, deliveryPrice } = orderCompleted || {}
    return orderSummary && orderSummary.deliveryLocations
      ? {
          ...selectedDeliveryLocation(orderSummary.deliveryLocations),
          estimatedDelivery: orderSummary.estimatedDelivery,
        }
      : { cost: deliveryCost || deliveryPrice, label: deliveryMethod }
  }

  getDeliveryEstimate() {
    const { pathName, orderSummary } = this.props
    const shippingInfo = this.getShippingInfo()
    if (orderSummary) {
      const isHomeDelivery = /HOME/.test(shippingInfo.deliveryType)
      const address = isHomeDelivery
        ? this.getAddressInfo()
        : { ...this.getDetailsInfo(), ...orderSummary.storeDetails }
      const shouldDisplayAddress = /\/checkout\/payment/.test(pathName)
      const deliveryType =
        orderSummary.deliveryLocations &&
        orderSummary.deliveryLocations.find(({ selected }) => selected)
          .deliveryLocationType
      return (
        <DeliveryEstimate
          className="checkoutBagSide"
          shippingInfo={shippingInfo}
          address={address}
          shouldDisplayAddress={shouldDisplayAddress}
          deliveryType={deliveryType}
        />
      )
    }
  }

  render() {
    const { l } = this.context
    const {
      bagProducts,
      canModify,
      className,
      drawer,
      inCheckout,
      orderCompleted,
      orderProducts,
      orderSummary,
      scrollMinibag,
      showDiscounts,
      subTotal,
      totalShoppingBagItems,
    } = this.props
    const discountInfo = showDiscounts ? this.props.discountInfo : []
    const isOrderCompleted =
      typeof orderCompleted === 'object' &&
      Object.keys(orderCompleted).length > 0

    if (
      (typeof orderSummary !== 'object' ||
        Object.keys(orderSummary).length === 0) &&
      (typeof orderCompleted !== 'object' ||
        Object.keys(orderCompleted).length === 0)
    )
      return null

    const products = orderProducts || bagProducts
    const inventoryPositions = pathOr(
      [],
      ['basket', 'inventoryPositions'],
      orderSummary
    )
    const shippingInfo = this.getShippingInfo()

    // we can use orderSummary as it's only for DeliveryEstimate Component which is rendered on !orderCompleted
    const deliveryEstimate = this.getDeliveryEstimate()

    // Used for Simple Totals
    const priceInfo = { subTotal }

    return (
      <div
        className={`CheckoutBagSide${className ? ` ${className}` : ''}${
          drawer ? ' CheckoutBagSide--drawer' : ''
        }`}
      >
        <h3 className="CheckoutBagSide-title">
          {isOrderCompleted ? l`Your order` : l`My Bag`}
          {totalShoppingBagItems ? ` (${totalShoppingBagItems})` : null}
        </h3>
        {isOrderCompleted && (
          <Espot
            identifier={constants.thankyou.sideBar1}
            className="CheckoutBagSide-espot"
          />
        )}
        <OrderProducts
          className="OrderProducts--checkoutBagSide"
          canModify={canModify}
          products={products}
          scrollOnEdit={scrollMinibag}
          allowEmptyBag={!inCheckout}
          scrollable
          drawer={drawer}
          inventoryPositions={inventoryPositions}
        />
        {deliveryEstimate}
        {isOrderCompleted && (
          <Espot
            identifier={constants.thankyou.sideBar2}
            className="CheckoutBagSide-espot"
          />
        )}
        {isOrderCompleted && (
          <Espot
            identifier={constants.thankyou.sideBar3}
            className="CheckoutBagSide-espot"
          />
        )}
        <SimpleTotals
          shippingInfo={shippingInfo}
          priceInfo={priceInfo}
          discounts={discountInfo}
        />
      </div>
    )
  }
}
