import renderComponentHelper from '../../../../../../test/unit/helpers/test-component'
import FitAttributes from '../FitAttributes'
import { Link } from 'react-router'

const renderComponent = renderComponentHelper(FitAttributes)

const initialProps = {
  fitAttributes: [
    {
      catentryId: '20434610',
      TPMName: 'Tall',
      isTPMActive: true,
      TPMUrl: '/en/tsuk/product/moto-star-stud-denim-borg-jacket-7196268',
    },
    {
      catentryId: '20906473',
      TPMName: 'Petite',
      isTPMActive: false,
      TPMUrl: '/en/tsuk/product/moto-knot-tie-jumpsuit-7178618',
    },
    {
      catentryId: '20865068',
      TPMName: 'Maternity',
      isTPMActive: false,
      TPMUrl: '/en/tsuk/product/moto-side-striped-denim-skirt-7216592',
    },
  ],
}

describe('<FitAttributes/>', () => {
  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })

    describe('not quickview', () => {
      it('renders the correct amount of fit attribute links', () => {
        const { wrapper } = renderComponent(initialProps)
        expect(wrapper.find('.FitAttributes-link').length).toBe(3)
      })

      it('first `Link` item has class of `FitAttributes--stateActive`', () => {
        const { wrapper } = renderComponent(initialProps)
        expect(
          wrapper
            .find(Link)
            .first()
            .hasClass('FitAttributes--stateActive')
        ).toBe(true)
      })

      it('only one `Link` item has class of `FitAttributes--stateActive`', () => {
        const { wrapper } = renderComponent(initialProps)
        expect(wrapper.find('.FitAttributes--stateActive').length).toBe(1)
      })
    })
    describe('quickview', () => {
      it('renders the correct amount of fit attribute buttons', () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          isQuickview: true,
        })
        expect(wrapper.find('.FitAttributes-link').length).toBe(3)
      })

      it('first item has class of `FitAttributes--stateActive`', () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          isQuickview: true,
        })
        expect(
          wrapper
            .find('.FitAttributes-link')
            .first()
            .hasClass('FitAttributes--stateActive')
        ).toBe(true)
      })

      it('only one item has class of `FitAttributes--stateActive`', () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          isQuickview: true,
        })
        expect(wrapper.find('.FitAttributes--stateActive').length).toBe(1)
      })
    })
  })
  describe('@events', () => {
    it('Quickview - should not call onClick when is active', () => {
      const { instance, wrapper } = renderComponent({
        ...initialProps,
        isQuickview: true,
        onClick: jest.fn(),
      })
      wrapper
        .find('.FitAttributes-link')
        .first()
        .simulate('click')
      expect(instance.props.onClick).not.toHaveBeenCalled()
    })
    it('Quickview - should call onClick when is not active', () => {
      const { instance, wrapper } = renderComponent({
        ...initialProps,
        isQuickview: true,
        onClick: jest.fn(),
      })
      wrapper
        .find('.FitAttributes-link')
        .at(1)
        .simulate('click')
      expect(instance.props.onClick).toHaveBeenCalledTimes(1)
      expect(instance.props.onClick).toHaveBeenCalledWith('20906473')
    })
  })
})
