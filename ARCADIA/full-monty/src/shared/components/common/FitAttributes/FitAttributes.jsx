import PropTypes from 'prop-types'
import React from 'react'
import { Link } from 'react-router'
import classNames from 'classnames'

import FeatureCheck from '../FeatureCheck/FeatureCheck'

const FitAttributes = ({ fitAttributes, isQuickview, onClick }) => (
  <FeatureCheck flag="FEATURE_SHOW_FIT_ATTRIBUTE_LINKS">
    <div className="FitAttributes">
      {fitAttributes.map((fitAttribute) => {
        const element = isQuickview ? (
          <button
            onClick={() =>
              !fitAttribute.isTPMActive && onClick(fitAttribute.catentryId)
            }
            key={fitAttribute.catentryId}
            className={classNames('FitAttributes-link', {
              'FitAttributes--stateActive': fitAttribute.isTPMActive,
            })}
          >
            {fitAttribute.TPMName}
          </button>
        ) : (
          <Link
            className={classNames('FitAttributes-link', {
              'FitAttributes--stateActive': fitAttribute.isTPMActive,
            })}
            to={fitAttribute.TPMUrl}
            key={fitAttribute.catentryId}
          >
            {fitAttribute.TPMName}
          </Link>
        )
        return element
      })}
    </div>
  </FeatureCheck>
)

FitAttributes.propTypes = {
  fitAttributes: PropTypes.array.isRequired,
  isQuickview: PropTypes.bool,
  onClick: PropTypes.func,
}

FitAttributes.defaultProps = {
  isQuickview: false,
  onClick: () => {},
}

export default FitAttributes
