import testComponentHelper from 'test/unit/helpers/test-component'
import SortSelector from '../SortSelector'

const renderComponent = testComponentHelper(SortSelector.WrappedComponent)

const props = {
  sortOptions: [
    { value: 'test', label: 'test' },
    { value: 'test', label: 'test' },
  ],
  location: {
    pathname: 'david/',
    search: '?q=d&jim=div',
    query: { sort: 'test' },
  },
  visited: ['one'],
  currentSortOption: 'my sort option',
  selectSortOption: jest.fn(),
}

describe('<SortSelector />', () => {
  describe('@renders', () => {
    it('renders in default state', () => {
      expect(renderComponent().getTree()).toMatchSnapshot()
    })

    it('`Select` child component has correct props', () => {
      expect(renderComponent(props).getTree()).toMatchSnapshot()
    })

    it('`selectSortOption` is called when `SortSelector` is clicked', () => {
      const { wrapper } = renderComponent(props)
      const value = 'test value'
      wrapper.find('.SortSelector').prop('onChange')({
        target: {
          value,
        },
      })
      expect(props.selectSortOption).toHaveBeenCalledTimes(1)
      expect(props.selectSortOption).toHaveBeenCalledWith(value)
    })
  })
})
