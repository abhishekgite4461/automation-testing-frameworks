import PropTypes from 'prop-types'
import React, { Component } from 'react'
import * as actions from '../../../actions/components/sortSelectorActions'
import { connect } from 'react-redux'
import Select from '../../../components/common/FormComponents/Select/Select'

@connect(
  (state) => ({
    sortOptions: state.products.sortOptions,
    currentSortOption: state.sorting.currentSortOption,
  }),
  actions
)
class SortSelector extends Component {
  static propTypes = {
    sortOptions: PropTypes.array,
    selectSortOption: PropTypes.func,
    currentSortOption: PropTypes.string,
    className: PropTypes.string,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  onSelectChange = (e) => {
    const { selectSortOption } = this.props
    const {
      target: { value },
    } = e

    selectSortOption(value)
  }

  render() {
    const { currentSortOption, sortOptions, className } = this.props
    const { l } = this.context

    // Default sorting is by relevance
    const value = currentSortOption || 'Relevance'

    return (
      <Select
        className={`SortSelector Select--sort ${className || ''}`}
        onChange={this.onSelectChange}
        options={sortOptions}
        name="sortSelector"
        value={value}
        label={l`Sort product list by`}
        hideLabel
      />
    )
  }
}

export default SortSelector
