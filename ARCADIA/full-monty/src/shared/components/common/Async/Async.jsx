import PropTypes from 'prop-types'
import React, { Component } from 'react'

const ignoredServerRequire = process.browser
  ? () => {}
  : require('../../../../server/lib/ignored-server-require').default

/*
  Use as follow in any component:
  <Async id="debug" getFile={() => System.import('common/DebugMenu/Debug.jsx')} />

  id: The id is used to pickup the html from the server rendering before client async kicks in. There is a small window between client side initial render and retrieving the async component where the client does not have anything to render and thus will throw an error or displays nothing.

  getFile: use the exact systemjs import line and replace the file name with your file start from the root of the components folder.

  All additional props will be passed to the component loaded in asynchronously
*/

export default class Async extends Component {
  static propTypes = {
    getFile: PropTypes.func.isRequired,
    id: PropTypes.string,
  }

  state = {}

  componentWillMount() {
    if (process.browser) {
      const node = document.getElementById(this.props.id)
      if (node) this.setState({ Html: node.innerHTML })
    }
    this.setModule()
  }

  setModule = () => {
    const { getFile } = this.props
    if (process.browser) {
      getFile().then((mod) => {
        this.setContent(mod.default)
        this.setState({ Html: '' })
      })
    } else {
      const filePath = this.stripFileFromFunction(getFile)
      this.setContent(ignoredServerRequire(filePath).default)
    }
  }

  setContent = (module) => {
    this.setState({ Content: module })
  }

  stripFileFromFunction = (func) => {
    return func
      .toString()
      .replace(/\n|\r/g, '')
      .replace(/.*\.import\('(.*)'\).*/, '$1')
  }

  render() {
    const { id } = this.props
    const { Content, Html } = this.state

    // eslint-disable-next-line react/no-danger
    if (Html) return <div id={id} dangerouslySetInnerHTML={{ __html: Html }} />

    return <div id={id}>{Content ? <Content {...this.props} /> : []}</div>
  }
}
