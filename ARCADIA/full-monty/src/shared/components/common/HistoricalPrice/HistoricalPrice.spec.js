import renderComponentHelper from 'test/unit/helpers/test-component'

import HistoricalPrice from './HistoricalPrice'
import Price from '../Price/Price'
import isWasPrice from '../../../lib/is-was-price'

jest.mock('../../../lib/is-was-price')

const renderComponent = renderComponentHelper(HistoricalPrice)

describe('<HistoricalPrice/>', () => {
  describe('@renders', () => {
    beforeEach(() => jest.resetAllMocks())

    const defaultProps = {
      price: '29.00',
    }

    it('renders a regular price', () => {
      const { wrapper } = renderComponent(defaultProps)
      expect(wrapper.find(Price).props().price).toBe('29.00')
    })

    it('renders `was` price', () => {
      isWasPrice.mockImplementation(() => true)
      const props = {
        ...defaultProps,
        wasPrice: '39.00',
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find('.HistoricalPrice-was')).toHaveLength(1)
    })

    it('renders was price', () => {
      isWasPrice.mockImplementation(() => false)
      const { wrapper } = renderComponent(defaultProps)
      expect(wrapper.find('.HistoricalPrice-was')).toHaveLength(0)
    })

    it('renders a custom className', () => {
      const props = {
        ...defaultProps,
        className: 'HistoricalPrice-test',
      }
      const { wrapper } = renderComponent(props)
      expect(
        wrapper.find('.HistoricalPrice').hasClass('HistoricalPrice-test')
      ).toBe(true)
    })

    it('renders rrp and price when providing price and rrp prices', () => {
      const props = {
        ...defaultProps,
        rrp: '69.00',
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find('.HistoricalPrice-rrp')).toHaveLength(1)
      expect(wrapper.find(Price).get(0).props.price).toBe('69.00')
      expect(wrapper.find(Price).get(1).props.price).toBe('29.00')
    })

    it('renders price when providing price, wasWasPrice and no wasPrice', () => {
      isWasPrice
        .mockImplementationOnce(() => false)
        .mockImplementationOnce(() => true)
      const props = {
        ...defaultProps,
        wasWasPrice: '39.00',
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find('.HistoricalPrice-promotion')).toHaveLength(1)
      expect(wrapper.find(Price).get(0).props.price).toBe('39.00')
      expect(wrapper.find(Price).get(1).props.price).toBe('29.00')
    })

    it('renders price when providing price, wasPrice and no wasWasPrice', () => {
      isWasPrice.mockImplementation(() => true)
      const props = {
        ...defaultProps,
        wasPrice: '39.00',
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find('.HistoricalPrice-was')).toHaveLength(1)
      expect(wrapper.find('.HistoricalPrice-promotion')).toHaveLength(1)
      expect(wrapper.find(Price).get(0).props.price).toBe('39.00')
      expect(wrapper.find(Price).get(1).props.price).toBe('29.00')
    })

    it('renders price when providing price, wasPrice and wasWasPrice', () => {
      isWasPrice.mockImplementation(() => true)
      const props = {
        ...defaultProps,
        wasPrice: '49.00',
        wasWasPrice: '59.00',
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find('.HistoricalPrice-was')).toHaveLength(1)
      expect(wrapper.find('.HistoricalPrice-wasWas')).toHaveLength(1)
      expect(wrapper.find('.HistoricalPrice-promotion')).toHaveLength(1)
      expect(wrapper.find(Price).get(0).props.price).toBe('59.00')
      expect(wrapper.find(Price).get(1).props.price).toBe('49.00')
      expect(wrapper.find(Price).get(2).props.price).toBe('29.00')
    })

    it('renders price when providing price, wasPrice, wasWasPrice and rrp', () => {
      isWasPrice.mockImplementation(() => true)
      const props = {
        ...defaultProps,
        wasPrice: '49.00',
        wasWasPrice: '59.00',
        rrp: '79.00',
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find('.HistoricalPrice-rrp')).toHaveLength(1)
      expect(wrapper.find('.HistoricalPrice-was')).toHaveLength(1)
      expect(wrapper.find('.HistoricalPrice-wasWas')).toHaveLength(1)
      expect(wrapper.find('.HistoricalPrice-promotion')).toHaveLength(1)
      expect(wrapper.find(Price).get(0).props.price).toBe('79.00')
      expect(wrapper.find(Price).get(1).props.price).toBe('59.00')
      expect(wrapper.find(Price).get(2).props.price).toBe('49.00')
      expect(wrapper.find(Price).get(3).props.price).toBe('29.00')
    })
  })
})
