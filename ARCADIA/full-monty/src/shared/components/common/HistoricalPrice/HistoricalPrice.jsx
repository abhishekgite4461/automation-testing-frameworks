import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Price from '../Price/Price'
import isWasPrice from '../../../lib/is-was-price'

export default class HistoricalPrice extends Component {
  static propTypes = {
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    wasPrice: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    wasWasPrice: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    rrp: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    className: PropTypes.string,
  }

  static contextTypes = {
    l: PropTypes.func,
    p: PropTypes.func,
  }

  setAllPrices = () => {
    const { l } = this.context
    const { price, wasPrice, wasWasPrice, rrp } = this.props
    const retail = rrp && (
      <span key={0} className="HistoricalPrice-old HistoricalPrice-rrp">
        {l`RRP`} <Price price={rrp} />{' '}
      </span>
    )
    const wasWas = wasWasPrice && (
      <span key={1} className="HistoricalPrice-old HistoricalPrice-wasWas">
        {l`Was`} <Price price={wasWasPrice} />{' '}
      </span>
    )
    const was = isWasPrice(wasPrice, price) && (
      <span key={2} className="HistoricalPrice-old HistoricalPrice-was">
        {l`Was`} <Price price={wasPrice} />{' '}
      </span>
    )
    const promotion = isWasPrice(wasPrice || wasWasPrice, price) ? (
      <span key={3} className="HistoricalPrice-promotion">
        {l`Now`} <Price key={5} price={price} />
      </span>
    ) : (
      [
        <span key={4} className="HistoricalPrice-label">
          {l`Price`}:{' '}
        </span>,
        <Price key={5} price={price} />,
      ]
    )

    if (wasPrice || wasWasPrice || rrp) {
      return [retail, wasWas, was, promotion]
    }
    return [
      retail,
      <span key={4} className="HistoricalPrice-label">
        {l`Price`}:{' '}
      </span>,
      <Price key={5} price={price} />,
    ]
  }

  render() {
    const { className } = this.props
    return (
      <span className={`HistoricalPrice${className ? ` ${className}` : ''}`}>
        {this.setAllPrices()}
      </span>
    )
  }
}
