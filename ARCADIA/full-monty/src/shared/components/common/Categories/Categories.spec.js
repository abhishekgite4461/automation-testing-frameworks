import React from 'react'
import { mount } from 'enzyme'
import testComponentHelper from 'test/unit/helpers/test-component'
import Categories from './Categories'
import mockProductsCategories from 'test/mocks/topnavmenu-prods-group'
import mockHelpAndInfoMenuItems from 'test/mocks/help-and-info-menu-items'
import ListItemLink from '../ListItemLink/ListItemLink'
import { analyticsGlobalNavClickEvent } from '../../../analytics/tracking/site-interactions'

jest.mock('../../../analytics/tracking/site-interactions', () => ({
  analyticsGlobalNavClickEvent: jest.fn(),
}))

describe('<Categories/>', () => {
  const initialProps = {}
  const renderComponent = testComponentHelper(Categories.WrappedComponent)

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })
  })

  describe('selectMenu', () => {
    it('call clear categories if leaf', () => {
      const menuItem = {
        label: 'Dresses',
      }
      const { instance } = renderComponent({
        navigation: {
          categoryType: [],
        },
        type: 'categoryType',
        setAnalyticsIntnav: jest.fn(),
        resetCategoryHistory: jest.fn(),
        clearInfinityPage: jest.fn(),
        toggleTopNavMenu: jest.fn(),
        toggleScrollToTop: jest.fn(),
      })
      instance.selectMenu({}, menuItem)
      expect(instance.props.setAnalyticsIntnav).toHaveBeenCalledTimes(1)
      expect(instance.props.setAnalyticsIntnav).toHaveBeenCalledWith(
        'Dresses',
        'categoryType'
      )
      expect(instance.props.resetCategoryHistory).toHaveBeenCalledTimes(1)
      expect(instance.props.clearInfinityPage).toHaveBeenCalledTimes(1)
      expect(instance.props.toggleTopNavMenu).toHaveBeenCalledTimes(1)
      expect(instance.props.toggleScrollToTop).toHaveBeenCalledTimes(1)
      expect(analyticsGlobalNavClickEvent).toHaveBeenCalledWith(
        menuItem.label.toLowerCase()
      )
    })
    it('call push to categories if category', () => {
      const menuItem = {
        navigationEntries: [{}],
      }
      const { instance } = renderComponent({
        ...initialProps,
        pushCategoryHistory: jest.fn(),
        setAnalyticsIntnav: jest.fn(),
        toggleScrollToTop: jest.fn(),
      })
      instance.selectMenu({}, menuItem)
      expect(instance.props.setAnalyticsIntnav).not.toHaveBeenCalled()
      expect(instance.props.toggleScrollToTop).toHaveBeenCalledTimes(1)
      expect(instance.props.pushCategoryHistory).toHaveBeenCalledTimes(1)
      expect(instance.props.pushCategoryHistory).toHaveBeenCalledWith(menuItem)
    })
  })

  it('<Categories /> selectCategory action is dispatched', () => {
    const { WrappedComponent } = Categories
    const setCategoryNameSpy = jest.fn()
    const props = {
      toggleTopNavMenu: () => {},
      pushCategoryHistory: setCategoryNameSpy,
      type: 'productCategories',
      navigation: mockProductsCategories,
      toggleScrollToTop: () => {},
    }
    const wrapper = mount(<WrappedComponent {...props} />)
    const node = wrapper
      .find('.Categories-listItem')
      .find('.ListItemLink')
      .first()
    node.simulate('click')
    expect(setCategoryNameSpy).toHaveBeenCalled()
  })

  it('<Categories /> displays sub categories', () => {
    const { WrappedComponent } = Categories
    const props = {
      toggleTopNavMenu: () => {},
      pushCategoryHistory: jest.fn(),
      type: 'productCategories',
      navigation: mockProductsCategories,
      toggleScrollToTop: () => {},
    }
    const wrapper = mount(<WrappedComponent {...props} />)
    expect(
      wrapper
        .find('.Categories-listItem')
        .first()
        .text()
    ).toBe('New In')
  })

  it('<Categories /> displays help and info menu items', () => {
    const { WrappedComponent } = Categories
    const props = {
      toggleTopNavMenu: () => {},
      pushCategoryHistory: jest.fn(),
      type: 'helpAndInfoGroup',
      navigation: mockHelpAndInfoMenuItems,
      toggleScrollToTop: () => {},
    }
    const wrapper = mount(<WrappedComponent {...props} />)
    expect(wrapper.find('.Categories-listItem').hostNodes().length).toBe(
      mockHelpAndInfoMenuItems.helpAndInfoGroup.length
    )
    expect(
      wrapper
        .find('.Categories-listItem')
        .hostNodes()
        .first()
        .text()
    ).toBe('Delivery')
  })

  it(
    '<Categories /> "Delivery", "Return" and "Help" links have absolute redirectionUrl and they have ' +
      'the property "target" set to open a new tab',
    () => {
      const { WrappedComponent } = Categories
      const props = {
        toggleTopNavMenu: () => {},
        pushCategoryHistory: jest.fn(),
        type: 'helpAndInfoGroup',
        navigation: mockHelpAndInfoMenuItems,
        toggleScrollToTop: () => {},
      }
      const wrapper = mount(<WrappedComponent {...props} />)
      expect(
        wrapper
          .find('.Categories-listItem')
          .hostNodes()
          .at(0)
          .text()
      ).toBe('Delivery')
      expect(
        wrapper
          .find('.Categories-listItem')
          .hostNodes()
          .at(0)
          .find('Link')
          .prop('to')
      ).toBe(
        'http://help.topshop.com/' +
          'system/templates/selfservice/topshop/#!portal/403700000001009/topic/403700000001078/Delivery?LANGUAGE=en&COUNTRY=uk'
      )
      expect(
        wrapper
          .find('.Categories-listItem')
          .hostNodes()
          .at(0)
          .find('a')
          .prop('target')
      ).toBe('_blank')

      expect(
        wrapper
          .find('.Categories-listItem')
          .hostNodes()
          .at(1)
          .text()
      ).toBe('Returns')
      expect(
        wrapper
          .find('.Categories-listItem')
          .hostNodes()
          .at(1)
          .find('Link')
          .prop('to')
      ).toBe(
        'http://help.topshop.com/' +
          'system/templates/selfservice/topshop/#!portal/403700000001009/topic/403700000001087/Returns-Online'
      )
      expect(
        wrapper
          .find('.Categories-listItem')
          .hostNodes()
          .at(1)
          .find('a')
          .prop('target')
      ).toBe('_blank')

      expect(
        wrapper
          .find('.Categories-listItem')
          .hostNodes()
          .at(4)
          .text()
      ).toBe('Help')
      expect(
        wrapper
          .find('.Categories-listItem')
          .hostNodes()
          .at(4)
          .find('Link')
          .prop('to')
      ).toBe(
        'http://help.topshop.com/' +
          'system/templates/selfservice/topshop/#!portal/403700000001009?LANGUAGE=en&COUNTRY=us'
      )
      expect(
        wrapper
          .find('.Categories-listItem')
          .hostNodes()
          .at(4)
          .find('a')
          .prop('target')
      ).toBe('_blank')
    }
  )

  it('<Categories /> "Ts&Cs" link does not have the property "target" set to open new tab', () => {
    const { WrappedComponent } = Categories
    const props = {
      toggleTopNavMenu: () => {},
      pushCategoryHistory: jest.fn(),
      type: 'helpAndInfoGroup',
      navigation: mockHelpAndInfoMenuItems,
      toggleScrollToTop: () => {},
    }
    const wrapper = mount(<WrappedComponent {...props} />)
    expect(
      wrapper
        .find('.Categories-listItem')
        .hostNodes()
        .at(5)
        .text()
    ).toBe('T&Cs')
    expect(
      wrapper
        .find('.Categories-listItem')
        .at(5)
        .find('a')
        .prop('target')
    ).toBe(undefined)
  })

  it('does not render "Switch to Full Desktop" menu item', () => {
    const { wrapper } = renderComponent({
      type: 'foo',
      navigation: {
        foo: [
          {
            label: 'Switch to Full Desktop',
            navigationEntries: [],
            redirectionUrl: '/switch-to-full-homepage',
          },
        ],
      },
    })

    expect(wrapper.find(ListItemLink).length).toBe(0)
  })
})
