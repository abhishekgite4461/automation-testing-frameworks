import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import * as topNavMenuActions from '../../../actions/components/TopNavMenuActions'
import * as navigationActions from '../../../actions/common/navigationActions'
import * as infinityScrollActions from '../../../actions/common/infinityScrollActions'
import ListItemLink from '../ListItemLink/ListItemLink'
import { setAnalyticsIntnav } from '../../../lib/analytics/analytics-helper'
import { analyticsGlobalNavClickEvent } from '../../../analytics/tracking/site-interactions'

@connect(
  (state) => ({
    navigation: state.navigation,
  }),
  {
    ...topNavMenuActions,
    ...navigationActions,
    ...infinityScrollActions,
    setAnalyticsIntnav,
  }
)
export default class Categories extends Component {
  static propTypes = {
    toggleScrollToTop: PropTypes.func,
    toggleTopNavMenu: PropTypes.func,
    currentCatId: PropTypes.number,
    pushCategoryHistory: PropTypes.func,
    type: PropTypes.string,
    navigation: PropTypes.object,
    resetCategoryHistory: PropTypes.func,
    clearInfinityPage: PropTypes.func,
  }

  getMenuItems = () =>
    this.props.type ? this.props.navigation[this.props.type] : []

  selectMenu = (evt, menuItem) => {
    this.props.toggleScrollToTop()
    if (menuItem.navigationEntries && menuItem.navigationEntries.length > 0) {
      this.props.pushCategoryHistory(menuItem)
    } else {
      this.props.setAnalyticsIntnav(menuItem.label, this.props.type)
      this.props.resetCategoryHistory()
      this.props.clearInfinityPage()
      this.props.toggleTopNavMenu()
    }
    analyticsGlobalNavClickEvent(menuItem.label && menuItem.label.toLowerCase())
  }

  showCategories = (menuItems, parent) => {
    let listItems = []
    menuItems.forEach((item) => {
      if (item.label)
        listItems.push(this.categoryItemTemplate(item, '', parent))
      if (item.navigationEntries && item.navigationEntries.length > 0) {
        const children = this.showCategories(
          item.navigationEntries,
          item.categoryId
        )
        listItems = [...listItems, children]
      }
    })
    return listItems
  }

  categoryItemTemplate = (menuItem, children, parent) => {
    const { currentCatId } = this.props
    const subCategories = menuItem.navigationEntries

    const isTopLevelMenuItem =
      Array.isArray(menuItem.navigationEntries) &&
      menuItem.navigationEntries.length > 0
    const isExternalUrl =
      menuItem.redirectionUrl && menuItem.redirectionUrl.startsWith('http')

    let catPropsChildren = [menuItem.label]
    if (subCategories.length) {
      catPropsChildren = catPropsChildren.concat(
        <span className="Categories-arrow" />
      )
    }

    const catProps = {
      className: `ListItemLink${
        (!parent && currentCatId === null) || currentCatId === parent
          ? ' is-active'
          : ''
      }`,
      onClick: (e) => this.selectMenu(e, menuItem),
      children: catPropsChildren,
    }

    // In case of absolute "redirectionUrl" as absolute URL from navigation response then
    // we want to by-pass the application routes and redirect and open a new tab. An
    // example is given by "Delivery", "Return" and "Help" menu links.
    const destination = isExternalUrl
      ? menuItem.redirectionUrl
      : menuItem.seoUrl
    if (isExternalUrl) catProps.target = '_blank'

    if (menuItem.redirectionUrl === '/switch-to-full-homepage') return null

    return (
      <ListItemLink
        key={menuItem.categoryId}
        className={`Categories-listItem Categories-listItem${
          menuItem.categoryId
        }`}
      >
        {isTopLevelMenuItem ? (
          <button {...catProps} />
        ) : (
          <Link {...catProps} to={destination} />
        )}
      </ListItemLink>
    )
  }

  render() {
    return <div>{this.showCategories(this.getMenuItems())}</div>
  }
}
