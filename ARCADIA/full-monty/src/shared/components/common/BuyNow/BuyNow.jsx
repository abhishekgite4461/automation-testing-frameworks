import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { equals } from 'ramda'
import Button from '../Button/Button'
import * as ShoppingBagActions from '../../../actions/common/shoppingBagActions'
import { updateShowItemsError } from '../../../actions/common/productsActions'
import { analyticsPdpClickEvent } from '../../../analytics/tracking/site-interactions'

@connect(
  (state) => ({
    modal: state.modal,
    selectedQuantity: state.productDetail.selectedQuantity,
  }),
  { ...ShoppingBagActions, updateShowItemsError }
)
export default class BuyNow extends Component {
  static propTypes = {
    productId: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
      .isRequired,
    activeItem: PropTypes.object.isRequired,
    isDisabled: PropTypes.bool,
    className: PropTypes.string,
    selectedQuantity: PropTypes.number,
    hideButton: PropTypes.bool,
    addToBag: PropTypes.func,
    updateShowItemsError: PropTypes.func,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  static defaultProps = {
    selectedQuantity: 1,
    className: '',
  }

  buyNow = () => {
    const {
      productId,
      activeItem,
      selectedQuantity,
      updateShowItemsError,
      addToBag,
    } = this.props
    const { sku } = activeItem
    if (equals(activeItem, {})) return updateShowItemsError()
    addToBag(productId, sku, selectedQuantity)
    analyticsPdpClickEvent(`buynow-${productId}`)
  }

  render() {
    const { l } = this.context
    const { hideButton, isDisabled, className } = this.props
    if (hideButton) return null
    return (
      <Button
        className={`BuyNow Button Button--secondary ${className}`}
        isDisabled={isDisabled}
        clickHandler={this.buyNow}
      >
        {l`Buy Now`}
      </Button>
    )
  }
}
