import testComponentHelper from 'test/unit/helpers/test-component'
import BuyNow from '../BuyNow'
import { analyticsPdpClickEvent } from '../../../../analytics/tracking/site-interactions'

jest.mock('../../../../analytics/tracking/site-interactions', () => ({
  analyticsPdpClickEvent: jest.fn(),
}))

describe('<BuyNow />', () => {
  const initProps = {
    productId: 'fake-id',
    activeItem: {},
    addToBag: jest.fn(),
    updateShowItemsError: jest.fn(),
  }
  const renderComponent = testComponentHelper(BuyNow.WrappedComponent)

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initProps).getTree()).toMatchSnapshot()
    })
    it('in hidden state', () => {
      expect(
        renderComponent({
          ...initProps,
          hideButton: true,
        }).getTree()
      ).toMatchSnapshot()
    })
    it('in disabled state', () => {
      expect(
        renderComponent({
          ...initProps,
          isDisabled: true,
        }).getTree()
      ).toMatchSnapshot()
    })
    it('with className', () => {
      expect(
        renderComponent({
          ...initProps,
          className: 'fake-class',
        }).getTree()
      ).toMatchSnapshot()
    })
  })

  describe('@events', () => {
    describe('button click', () => {
      beforeEach(jest.resetAllMocks)
      it('when activeItem={} it should should call updateShowItemsError and should not call addToBag', () => {
        const { instance, wrapper } = renderComponent(initProps)
        expect(instance.props.updateShowItemsError).not.toHaveBeenCalled()
        wrapper
          .find('Button')
          .props()
          .clickHandler()
        expect(instance.props.updateShowItemsError).toHaveBeenCalledTimes(1)
        expect(instance.props.addToBag).not.toHaveBeenCalled()
      })
      it('when activeItem is not {} it should should not call updateShowItemsError and should call addToBag with productId, sku, selectedQuantity', () => {
        const activeItem = {
          sku: 'fake-sku',
        }
        const { instance, wrapper } = renderComponent({
          ...initProps,
          activeItem,
        })
        expect(instance.props.addToBag).not.toHaveBeenCalled()
        wrapper
          .find('Button')
          .props()
          .clickHandler()
        expect(instance.props.addToBag).toHaveBeenCalledTimes(1)
        expect(instance.props.addToBag).toHaveBeenLastCalledWith(
          instance.props.productId,
          activeItem.sku,
          instance.props.selectedQuantity
        )
        expect(instance.props.updateShowItemsError).not.toHaveBeenCalled()
        expect(analyticsPdpClickEvent).toHaveBeenCalledWith(
          `buynow-${instance.props.productId}`
        )
      })
    })
  })
})
