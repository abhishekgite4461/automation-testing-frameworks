import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'
import { path } from 'ramda'

import storeHostMap from '../../../../server/api/hostsConfig/store_host_map.json'

const getDesktopLegacyWidth = (brand) => {
  if (brand === 'topman') return 1024
  else if (brand === 'topshop') return 1200
  return 990
}

@connect((state) => ({
  viewportWidth: state.viewport.width,
  viewportHeight: state.viewport.height,
  apiEnvironment: state.debug.environment,
  storeCode: state.config.storeCode,
  location: state.routing.location,
  brandName: state.config.brandName,
}))
export default class CMSIframe extends Component {
  static propTypes = {
    location: PropTypes.object,
    viewportWidth: PropTypes.number,
    viewportHeight: PropTypes.number,
    apiEnvironment: PropTypes.string,
    storeCode: PropTypes.string,
    brandName: PropTypes.string,
  }

  constructor(props) {
    super(props)
    this.DEFAULT_HEIGHT = 20000
    this.state = {
      width: null,
      scale: 1,
      left: 0,
      top: 0,
      height: this.DEFAULT_HEIGHT,
    }
  }

  componentDidMount() {
    window.addEventListener('message', this.manageIframePostMessage)
    // need this delay as there is a minor delay in setting the viewportWidth in Main.jsx
    const x = setTimeout(() => {
      const { viewportWidth } = this.props
      this.fixDimensions(viewportWidth)
      clearTimeout(x)
    }, 100)
  }

  componentWillReceiveProps({ viewportWidth }) {
    const { viewportWidth: oldWidth } = this.props
    if (viewportWidth !== oldWidth) {
      this.fixDimensions(viewportWidth)
    }
  }

  componentWillUnmount() {
    window.removeEventListener('message', this.manageIframePostMessage)
  }

  getDefaultHeight = () => {
    return this.iframeNode
      ? this.props.viewportHeight - this.iframeNode.getBoundingClientRect().top
      : this.DEFAULT_HEIGHT
  }

  manageIframePostMessage = (messageEvent) => {
    const { viewportWidth } = this.props
    if (
      messageEvent.data &&
      messageEvent.data.IframeLoaded &&
      messageEvent.data.height
    ) {
      this.fixDimensions(viewportWidth, messageEvent.data.height)
    } else if (messageEvent.data && messageEvent.data.iframeTitle) {
      this.setState({
        title: messageEvent.data.iframeTitle,
      })
    }
  }

  fixDimensions = (viewportWidth, realHeight) => {
    const { brandName } = this.props
    if (!realHeight) {
      const node =
        this.iframeNode &&
        this.iframeNode.contentWindow.document.getElementById(
          'monty_legacy_cms_content_wrapper'
        )
      realHeight = node ? node.offsetHeight : this.getDefaultHeight()
    }
    const scaledWidth =
      viewportWidth <= getDesktopLegacyWidth(brandName)
        ? getDesktopLegacyWidth(brandName)
        : viewportWidth
    const scale = viewportWidth / scaledWidth
    const scaledHeight = realHeight * scale
    this.setState({
      width: scaledWidth,
      scale,
      left: (viewportWidth - scaledWidth) / 2,
      height: realHeight,
      top: (scaledHeight - realHeight) / 2,
    })
  }

  render() {
    const { location, viewportWidth, storeCode, apiEnvironment } = this.props
    const { width, scale, left, top, height, title } = this.state

    const encodedPathname = encodeURIComponent(
      location.pathname + location.search
    )
    const seoUrl = `/cmsfeaturecontent?apiEnvironment=${apiEnvironment ||
      'prod'}&storeCode=${storeCode}&pathname=${encodedPathname}`
    const host =
      path([apiEnvironment || 'prod', storeCode], storeHostMap) ||
      `${location.protocol}://${location.hostname}`

    const helmetProps = {
      link: [
        {
          href: `${host}${location.pathname}`,
          rel: 'canonical',
        },
      ],
      title,
    }

    const style = {
      width: `${width || viewportWidth}px`,
      transform: `scale(${scale})`,
      left: `${left}px`,
      top: `${top}px`,
      height: `${height}px`,
    }
    const containerStyle = {
      width: `${viewportWidth}px`,
      height: `${height * scale}px`,
    }

    return (
      <div className="CMSIframe-container" style={containerStyle}>
        <Helmet {...helmetProps} />
        <iframe
          id="CMSIframe"
          ref={(el) => {
            this.iframeNode = el
          }}
          src={seoUrl}
          height={`${this.DEFAULT_HEIGHT}px`}
          className="CMSIframe-frame"
          frameBorder="0"
          style={style}
        />
      </div>
    )
  }
}
