import testComponentHelper from 'test/unit/helpers/test-component'
import CMSIframe from '../CMSIframe'

jest.useFakeTimers()

describe('<CMSIframe> ', () => {
  const renderComponent = testComponentHelper(CMSIframe.WrappedComponent, {
    disableLifecycleMethods: true,
  })

  const initProps = {
    viewportWidth: 1300,
    viewportHeight: 2000,
    apiEnvironment: 'prod',
    storeCode: 'tsuk',
    brandName: 'topshop',
    location: {
      pathname: '/a/fake/location',
      hostname: 'www.topshop.com',
      search: '',
    },
  }

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initProps).getTree()).toMatchSnapshot()
    })
    it('with the correct styles', () => {
      const { wrapper, instance } = renderComponent(initProps)
      const iframeProps = wrapper.find('#CMSIframe').prop('style')
      expect(iframeProps).toEqual({
        height: '20000px',
        left: '0px',
        top: '0px',
        transform: 'scale(1)',
        width: '1300px',
      })
      const containerProps = wrapper.find('.CMSIframe-container').prop('style')
      expect(containerProps).toEqual({
        height: `${instance.DEFAULT_HEIGHT}px`,
        width: '1300px',
      })
    })
    it('should set the reference', () => {
      const { wrapper, instance } = renderComponent(initProps)
      const iframeRef = { id: 'iframe-ref' }
      wrapper
        .find('iframe')
        .getElement()
        .ref(iframeRef)
      expect(instance.iframeNode).toEqual(iframeRef)
    })
    it('should encode url with query parmas as well', () => {
      const { wrapper, getTree } = renderComponent({
        ...initProps,
        location: {
          pathname: '/a/fake/location',
          hostname: 'www.topshop.com',
          search: '?who=el&page=xyz+abc',
        },
      })
      expect(getTree()).toMatchSnapshot()
      const iframeSrc = wrapper.find('iframe').prop('src')
      expect(iframeSrc).toEqual(
        '/cmsfeaturecontent?apiEnvironment=prod&storeCode=tsuk&pathname=%2Fa%2Ffake%2Flocation%3Fwho%3Del%26page%3Dxyz%2Babc'
      )
    })
  })

  describe('@lifecycle', () => {
    describe('constructor', () => {
      it('should set the default height', () => {
        const { instance } = renderComponent(initProps)
        expect(instance.DEFAULT_HEIGHT).toBe(20000)
      })
      it('should set the state', () => {
        const { instance } = renderComponent(initProps)
        expect(instance.state).toEqual({
          width: null,
          scale: 1,
          left: 0,
          top: 0,
          height: 20000,
        })
      })
    })
    describe('componentDidMount', () => {
      const addEventListenerSpy = jest.spyOn(window, 'addEventListener')
      const { instance, wrapper } = renderComponent(initProps)
      const iframeRef = {
        contentWindow: {
          document: {
            getElementById: jest.fn(() => ({
              offsetHeight: 150,
            })),
          },
        },
        getBoundingClientRect: jest.fn(() => ({ top: 100 })),
      }
      wrapper
        .find('iframe')
        .getElement()
        .ref(iframeRef)
      instance.componentDidMount()
      it('should add an event listener for message', () => {
        expect(addEventListenerSpy).toHaveBeenCalledTimes(1)
        expect(addEventListenerSpy).toHaveBeenLastCalledWith(
          'message',
          instance.manageIframePostMessage
        )
      })
      it('should call a setTimeout once', () => {
        expect(setTimeout).toHaveBeenCalledTimes(1)
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 100)
      })
      it('should call fixDimensions when timer runs down and should clear timer', () => {
        const fixDimensionsSpy = jest.spyOn(instance, 'fixDimensions')
        expect(fixDimensionsSpy).not.toHaveBeenCalled()
        expect(clearTimeout).not.toHaveBeenCalled()
        jest.runAllTimers()
        expect(fixDimensionsSpy).toHaveBeenCalledTimes(1)
        expect(fixDimensionsSpy).toHaveBeenLastCalledWith(1300)
        expect(clearTimeout).toHaveBeenCalledTimes(1)
        expect(clearTimeout).toHaveBeenLastCalledWith(1)
      })
    })
    describe('componentWillReceiveProps', () => {
      const { instance, wrapper } = renderComponent(initProps)
      const iframeRef = {
        contentWindow: {
          document: {
            getElementById: jest.fn(() => ({
              offsetHeight: 150,
            })),
          },
        },
        getBoundingClientRect: jest.fn(() => ({ top: 100 })),
      }
      wrapper
        .find('iframe')
        .getElement()
        .ref(iframeRef)
      beforeEach(jest.clearAllMocks)
      it('should call fixDimensions with the new viewportWidth if the viewportWidth is different', () => {
        const fixDimensionsSpy = jest.spyOn(instance, 'fixDimensions')
        instance.componentWillReceiveProps({ viewportWidth: 1400 })
        expect(fixDimensionsSpy).toHaveBeenCalledTimes(1)
        expect(fixDimensionsSpy).toHaveBeenLastCalledWith(1400)
      })
      it('should not call fixDimensions if the viewportWidth has not changed', () => {
        const fixDimensionsSpy = jest.spyOn(instance, 'fixDimensions')
        instance.componentWillReceiveProps({ viewportWidth: 1300 })
        expect(fixDimensionsSpy).not.toHaveBeenCalled()
      })
    })
    describe('componentWillUnmount', () => {
      it('should remove an event listener for message', () => {
        const addEventListenerSpy = jest.spyOn(window, 'removeEventListener')
        const { instance, wrapper } = renderComponent(initProps)
        wrapper.unmount()
        expect(addEventListenerSpy).toHaveBeenCalledTimes(1)
        expect(addEventListenerSpy).toHaveBeenLastCalledWith(
          'message',
          instance.manageIframePostMessage
        )
      })
    })
  })

  describe('@instance methods', () => {
    describe('fixDimensions', () => {
      beforeEach(jest.clearAllMocks)
      const { wrapper, instance } = renderComponent(initProps)
      it('should call getElementById on iframe window if realHeight is not defined', () => {
        const iframeNode = {
          contentWindow: {
            document: {
              getElementById: jest.fn(() => ({
                offsetHeight: 150,
              })),
            },
          },
        }
        wrapper
          .find('iframe')
          .getElement()
          .ref(iframeNode)
        expect(
          iframeNode.contentWindow.document.getElementById
        ).not.toHaveBeenCalled()
        instance.fixDimensions(1200)
        expect(
          iframeNode.contentWindow.document.getElementById
        ).toHaveBeenCalledTimes(1)
        expect(
          iframeNode.contentWindow.document.getElementById
        ).toHaveBeenLastCalledWith('monty_legacy_cms_content_wrapper')
      })
      it('if realHeight is not passed and monty_legacy_cms_content_wrapper exist then it should set state.height as offsetHeight+5', () => {
        const iframeNode = {
          contentWindow: {
            document: {
              getElementById: jest.fn(() => ({
                offsetHeight: 150,
              })),
            },
          },
        }
        wrapper
          .find('iframe')
          .getElement()
          .ref(iframeNode)
        instance.fixDimensions(1200)
        expect(instance.state.height).toBe(150)
      })
      it('if realHeight is not passed and monty_legacy_cms_content_wrapper doesnt exist then it should set state.height as DEFAULT_HEIGHT', () => {
        const iframeNode = {
          contentWindow: {
            document: {
              getElementById: jest.fn(() => null),
            },
          },
          getBoundingClientRect: jest.fn(() => ({ top: 100 })),
        }
        wrapper
          .find('iframe')
          .getElement()
          .ref(iframeNode)
        instance.fixDimensions(1200)
        expect(instance.state.height).toBe(1900)
      })
      it('if viewportWidth < 990, it should set scalled width as 990 and scale as < 1 for brand not topman or topshop', () => {
        const { instance } = renderComponent({
          ...initProps,
          brandName: 'burton',
          brandCode: 'bruk',
        })
        instance.fixDimensions(900, 500)
        expect(instance.state).toEqual({
          width: 990,
          scale: 0.9090909090909091,
          left: -45,
          height: 500,
          top: -22.727272727272748,
        })
      })
      it('if viewportWidth < 1200, it should set scalled width as 1200 and scale as < 1 for topshop', () => {
        instance.fixDimensions(1100, 500)
        expect(instance.state).toEqual({
          width: 1200,
          scale: 0.9166666666666666,
          left: -50,
          height: 500,
          top: -20.833333333333343,
        })
      })
      it('if viewportWidth < 1024, it should set scalled width as 1024 and scale as < 1 for topman', () => {
        const { instance } = renderComponent({
          ...initProps,
          brandName: 'topman',
          brandCode: 'tmuk',
        })
        instance.fixDimensions(1000, 500)
        expect(instance.state).toEqual({
          width: 1024,
          scale: 0.9765625,
          left: -12,
          height: 500,
          top: -5.859375,
        })
      })
      it('if viewportWidth > 990, it should set scalled width as viewportWidth and scale as 1', () => {
        instance.fixDimensions(1500, 500)
        expect(instance.state).toEqual({
          width: 1500,
          scale: 1,
          left: 0,
          height: 500,
          top: 0,
        })
      })
    })
    describe('manageIframePostMessage', () => {
      it('should call fixDimensions, if data.IframeLoaded=true and height', () => {
        const { instance } = renderComponent(initProps)
        const messageEvent = {
          data: {
            IframeLoaded: true,
            height: 120,
          },
        }
        instance.fixDimensions = jest.fn()
        expect(instance.fixDimensions).not.toHaveBeenCalled()
        instance.manageIframePostMessage(messageEvent)
        expect(instance.fixDimensions).toHaveBeenCalledTimes(1)
        expect(instance.fixDimensions).toHaveBeenLastCalledWith(1300, 120)
      })
      it('should not call fixDimensions, if data.IframeLoaded=false and height', () => {
        const { instance } = renderComponent(initProps)
        const messageEvent = {
          data: {
            IframeLoaded: false,
            height: 120,
          },
        }
        instance.fixDimensions = jest.fn()
        instance.manageIframePostMessage(messageEvent)
        expect(instance.fixDimensions).not.toHaveBeenCalled()
      })
      it('should update the state.title if data.iframeTitle is defined', () => {
        const { instance } = renderComponent(initProps)
        const messageEvent = {
          data: {
            iframeTitle: 'Mocked Title',
          },
        }
        expect(instance.state).toEqual({
          width: null,
          scale: 1,
          left: 0,
          top: 0,
          height: 20000,
        })
        instance.manageIframePostMessage(messageEvent)
        expect(instance.state).toEqual({
          width: null,
          scale: 1,
          left: 0,
          top: 0,
          height: 20000,
          title: 'Mocked Title',
        })
      })
    })
  })
})
