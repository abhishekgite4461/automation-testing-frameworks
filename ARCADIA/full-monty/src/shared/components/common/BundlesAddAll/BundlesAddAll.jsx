import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'

import { scrollElementIntoView } from '../../../lib/scroll-helper'
import { getSelectedSKUs } from '../../../selectors/productSelectors'

// actions
import { setFormMessage } from '../../../actions/common/formActions'

// components
import AddToBag from '../../common/AddToBag/AddToBag'

const mapStateToProps = (state) => ({
  selectedSKUs: getSelectedSKUs(state),
})

const mapDispatchToProps = { setFormMessage }

class BundlesAddAll extends Component {
  static propTypes = {
    productId: PropTypes.number.isRequired,
    bundleProductIds: PropTypes.arrayOf(PropTypes.number).isRequired,
    selectedSKUs: PropTypes.objectOf(PropTypes.string),
    deliveryMessage: PropTypes.string,
    getElement: PropTypes.func,
    scrollToElement: PropTypes.func,
    // actions
    setFormMessage: PropTypes.func.isRequired,
  }

  static defaultProps = {
    selectedSKUs: {},
    deliveryMessage: '',
    getElement:
      typeof window !== 'undefined'
        ? document.querySelector.bind(document)
        : () => {},
    scrollToElement: scrollElementIntoView,
  }

  getUnselectedProducts = () => {
    const { bundleProductIds, selectedSKUs } = this.props
    return bundleProductIds.filter(
      (bundleProductId) => !(bundleProductId in selectedSKUs)
    )
  }

  shouldAddToBag = () => {
    const { setFormMessage, getElement, scrollToElement } = this.props
    const unselectedProducts = this.getUnselectedProducts()

    if (unselectedProducts.length === 0) return true

    unselectedProducts.forEach((productId) => {
      setFormMessage(
        'bundlesAddToBag',
        'Please select your size to continue',
        productId
      )
    })
    // scroll to first, failing, size dropdown
    const element = getElement(`[data-product-id="${unselectedProducts[0]}"]`)
    scrollToElement(element, 400, 200)
  }

  render() {
    const {
      productId,
      deliveryMessage,
      bundleProductIds,
      selectedSKUs,
    } = this.props
    const bundleItems = bundleProductIds.reduce(
      (bundleItems, bundleProductId) => {
        if (bundleProductId in selectedSKUs) {
          return [
            ...bundleItems,
            { productId: bundleProductId, sku: selectedSKUs[bundleProductId] },
          ]
        }
        return bundleItems
      },
      []
    )

    return (
      <AddToBag
        productId={productId}
        bundleItems={bundleItems}
        quantity={bundleItems.length}
        message={deliveryMessage}
        shouldShowInlineConfirm
        shouldShowMiniBagConfirm
        shouldAddToBag={this.shouldAddToBag}
        addToBagIsReady={this.getUnselectedProducts().length === 0}
      />
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BundlesAddAll)

export {
  BundlesAddAll as WrappedBundlesAddAll,
  mapStateToProps,
  mapDispatchToProps,
}
