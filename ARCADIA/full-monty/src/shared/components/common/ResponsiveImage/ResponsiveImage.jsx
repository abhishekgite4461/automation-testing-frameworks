import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import Image from '../Image/Image'
import {
  IMAGE_FORMAT,
  IMAGE_SIZES_RANGE,
  PROGRESSIVE_JPG_PARAM,
} from '../../../constants/amplience'
import screenSizes from '../../../constants/responsive'
import { isAmplienceFeatureEnabled } from '../../../selectors/featureSelectors'

const ResponsiveImage = ({
  isAmplienceEnabled,
  amplienceUrl,
  sizes,
  isImageFallbackEnabled,
  useProgressiveJPG,
  ...props
}) => {
  if (!isAmplienceEnabled || (!amplienceUrl && isImageFallbackEnabled)) {
    return <Image {...props} />
  }

  const baseUrl = `${amplienceUrl}.${IMAGE_FORMAT}`
  const jpgFormat = `${PROGRESSIVE_JPG_PARAM}=${useProgressiveJPG}`
  const srcSet = IMAGE_SIZES_RANGE.map(
    (size) => `${baseUrl}?$w${size}$&${jpgFormat} ${size}w`
  ).join(', ')

  const imgSizesAttr = `
    (max-width: ${screenSizes.mobile.max}px) ${sizes.mobile}px,
    (max-width: ${screenSizes.tablet.max}px) ${sizes.tablet}px,
    ${sizes.desktop}px
  `

  return <Image {...props} src={baseUrl} sizes={imgSizesAttr} srcSet={srcSet} />
}

ResponsiveImage.propTypes = {
  ...Image.PropTypes,
  isImageFallbackEnabled: PropTypes.bool,
  isAmplienceEnabled: PropTypes.bool.isRequired,
  amplienceUrl: PropTypes.string,
  sizes: PropTypes.shape({
    mobile: PropTypes.number,
    tablet: PropTypes.number,
    desktop: PropTypes.number,
  }),
}

ResponsiveImage.defaultProps = {
  isImageFallbackEnabled: false,
  amplienceUrl: '',
  // TODO: review this as it's designed this must have a default as size are passed down as null if not defined
  sizes: {
    mobile: 200,
    tablet: 400,
    desktop: 800,
  },
  useProgressiveJPG: false,
}

const mapStateToProps = (state) => ({
  isAmplienceEnabled: isAmplienceFeatureEnabled(state),
})

export default connect(mapStateToProps)(ResponsiveImage)
