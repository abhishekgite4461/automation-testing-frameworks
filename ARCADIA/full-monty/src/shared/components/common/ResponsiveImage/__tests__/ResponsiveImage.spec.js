import testComponentHelper from 'test/unit/helpers/test-component'
import React from 'react'
import deepFreeze from 'deep-freeze'
import configureStore from 'redux-mock-store'
import { shallow } from 'enzyme'

import ResponsiveImage from '../ResponsiveImage'
import Image from '../../Image/Image'

jest.mock('../../../../constants/amplience', () => ({
  IMAGE_FORMAT: 'jpg',
  IMAGE_SIZES_RANGE: ['1', '2'],
  PROGRESSIVE_JPG_PARAM: 'fmt.jpeg.interlaced',
}))

describe('<ResponsiveImage />', () => {
  const props = deepFreeze({
    isAmplienceEnabled: true,
    amplienceUrl: 'http://images.com',
    sizes: {
      mobile: 1,
      tablet: 2,
      desktop: 3,
    },
  })

  const renderComponent = testComponentHelper(ResponsiveImage.WrappedComponent)

  describe('Connected component', () => {
    it('should receive the correct props', () => {
      const container = shallow(
        <ResponsiveImage
          store={configureStore()({
            features: { status: { FEATURE_USE_AMPLIENCE: true } },
          })}
        />
      )

      expect(container).toBeTruthy()
      expect(container.prop('isAmplienceEnabled')).toBe(true)
    })
  })

  describe('@render', () => {
    it('renders correctly', () => {
      const { getTree } = renderComponent(props)
      expect(getTree()).toMatchSnapshot()
    })

    it('should render Image with srcSet and sizes props if isAmplienceEnabled is true', () => {
      const { wrapper } = renderComponent(props)
      expect(wrapper.find(Image).prop('srcSet')).toBe(
        'http://images.com.jpg?$w1$&fmt.jpeg.interlaced=false 1w, http://images.com.jpg?$w2$&fmt.jpeg.interlaced=false 2w'
      )
      expect(wrapper.find(Image).prop('sizes')).toMatchSnapshot()
    })

    it('should render Image without srcSet and sizes props if isAmplienceEnabled is false', () => {
      const { wrapper } = renderComponent({
        ...props,
        isAmplienceEnabled: false,
      })
      expect(wrapper.find(Image).prop('srcSet')).toBeFalsy()
      expect(wrapper.find(Image).prop('sizes')).toBeFalsy()
    })

    it('should fallback to normal image if isImageFallbackEnabled is true', () => {
      const fallbackImageSrc = 'image-url'
      const { wrapper } = renderComponent({
        ...props,
        isAmplienceEnabled: true,
        amplienceUrl: '',
        src: fallbackImageSrc,
        isImageFallbackEnabled: true,
      })
      expect(wrapper.find(Image).prop('srcSet')).toBeFalsy()
      expect(wrapper.find(Image).prop('sizes')).toBeFalsy()
      expect(wrapper.find(Image).prop('src')).toBe(fallbackImageSrc)
    })

    it('should not fallback to image if isImageFallbackEnabled is false and amplienceUrl is empty', () => {
      const { wrapper } = renderComponent({
        ...props,
        isAmplienceEnabled: true,
        amplienceUrl: '',
        isImageFallbackEnabled: false,
      })
      expect(wrapper.find(Image).prop('srcSet')).toBeTruthy()
      expect(wrapper.find(Image).prop('sizes')).toBeTruthy()
    })

    it('should provide appropriate query params to image url if useProgressiveJPG is true', () => {
      const { wrapper } = renderComponent({
        ...props,
        isAmplienceEnabled: true,
        useProgressiveJPG: true,
      })

      expect(wrapper.find(Image).prop('srcSet')).toBe(
        'http://images.com.jpg?$w1$&fmt.jpeg.interlaced=true 1w, http://images.com.jpg?$w2$&fmt.jpeg.interlaced=true 2w'
      )
    })
  })
})
