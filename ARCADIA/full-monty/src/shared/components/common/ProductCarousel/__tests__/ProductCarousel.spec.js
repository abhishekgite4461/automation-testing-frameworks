import { compose } from 'ramda'
import testComponentHelper, {
  mountRender,
  withStore,
  buildComponentRender,
} from 'test/unit/helpers/test-component'

import ProductCarousel, {
  WrappedProductCarousel,
  Arrow,
} from '../ProductCarousel'
import ProductCarouselItem from '../../ProductCarousel/ProductCarouselItem'

import * as viewportSelectors from '../../../../selectors/viewportSelectors'

jest.mock('../../../../selectors/viewportSelectors')

describe('<ProductCarousel />', () => {
  const generateProducts = (length) =>
    [...Array(length)].map(() => ({
      productId: 1708381,
      name: "Nails In Nice'N'Neutral",
      amplienceUrl: 'url',
      imageUrl:
        'http://media.topshop.com/wcsstore/TopShop/images/catalog/TS20N01WNUD_2col_F_1.jpg',
      unitPrice: '5',
      salePrice: '2',
    }))

  const renderComponent = testComponentHelper(WrappedProductCarousel)

  const initialProps = {
    carouselSize: 2,
  }

  beforeEach(() => {
    jest.resetAllMocks()
  })

  describe('@connected', () => {
    const initialState = {}
    const render = compose(
      mountRender,
      withStore(initialState)
    )
    const renderComponent = buildComponentRender(render, ProductCarousel)
    it('should render correctly', () => {
      const { getTree } = renderComponent()
      expect(getTree()).toMatchSnapshot()
    })
    describe('mapStateToProps', () => {
      it('should pass correct carousel size for mobile devices', () => {
        viewportSelectors.isMobile.mockReturnValueOnce(true)
        const { instance } = renderComponent()
        expect(instance.stateProps.carouselSize).toBe(2)
      })
      it('should pass correct carousel size for non-mobile devices', () => {
        viewportSelectors.isMobile.mockReturnValueOnce(false)
        const { instance } = renderComponent()
        expect(instance.stateProps.carouselSize).toBe(5)
      })
    })
  })

  describe('@renders', () => {
    it('should render in empty state', () => {
      const { wrapper } = renderComponent(initialProps)
      expect(wrapper.isEmptyRender()).toBe(true)
    })

    it('should render supplied products', () => {
      const { getTree } = renderComponent({
        ...initialProps,
        products: generateProducts(2),
      })
      expect(getTree()).toMatchSnapshot()
    })

    it('should double the products if there are exactly one more than the carousel size', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        products: generateProducts(3),
      })
      expect(wrapper.find(ProductCarouselItem).length).toBe(6)
    })

    describe('if number of products is greater than the carousel size', () => {
      it('should render <Arrow />s', () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          products: generateProducts(3),
        })
        expect(
          wrapper
            .find(Arrow)
            .find({ direction: 'left' })
            .isEmpty()
        ).toBe(false)
        expect(
          wrapper
            .find(Arrow)
            .find({ direction: 'right' })
            .isEmpty()
        ).toBe(false)
      })

      it('should supply correct `style` to <ProductCarouselItem />', () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          products: generateProducts(3),
        })
        expect(
          wrapper
            .find(ProductCarouselItem)
            .first()
            .prop('style')
        ).toEqual({
          width: '50%',
          transform: `translate(0%)`,
          zIndex: '1',
        })
      })

      it('should supply `amplienceUrl` to <ProductCarouselItem />', () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          products: generateProducts(1),
        })
        expect(
          wrapper
            .find(ProductCarouselItem)
            .first()
            .prop('amplienceUrl')
        ).toEqual('url')
      })

      it('should hide <ProductCarouselItem /> items outside of the carousel window view', () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          products: generateProducts(3),
        })
        expect(
          wrapper
            .find(ProductCarouselItem)
            .at(2)
            .prop('style').zIndex
        ).toBe('0')
      })
    })
  })

  describe('@events', () => {
    it('should decrease position on forward arrow click', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        products: generateProducts(7),
      })
      wrapper.find({ direction: 'right' }).prop('onClick')()
      expect(wrapper.state('position')).toBe(0)
    })

    it('should increase position on backward arrow click', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        products: generateProducts(7),
      })
      wrapper.find({ direction: 'left' }).prop('onClick')()
      expect(wrapper.state('position')).toBe(2)
    })

    it('should wrap position around if moving past the end of the carousel', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        products: generateProducts(7),
      })
      const clickRightArrow = wrapper
        .find({ direction: 'right' })
        .prop('onClick')
      clickRightArrow()
      clickRightArrow()
      expect(wrapper.state('position')).toBe(6)
    })

    it('should move carousel forward on backward swipe', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        products: generateProducts(7),
      })
      const swipe = wrapper.find('Hammer').prop('onSwipe')
      swipe({ direction: 2 })
      expect(wrapper.state('position')).toBe(0)
    })

    it('should move backward on forward swipe', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        products: generateProducts(7),
      })
      const swipe = wrapper.find('Hammer').prop('onSwipe')
      swipe({ direction: 4 })
      expect(wrapper.state('position')).toBe(2)
    })
  })
})

describe('<Arrow />', () => {
  const renderComponent = testComponentHelper(Arrow)

  describe('@renders', () => {
    it('should render correctly', () => {
      expect(
        renderComponent({ direction: 'forward', onClick: jest.fn() }).getTree()
      ).toMatchSnapshot()
    })
  })

  describe('@events', () => {
    it('should call `onClick` on button click', () => {
      const onClickMock = jest.fn()
      const { wrapper } = renderComponent({
        direction: 'forward',
        onClick: onClickMock,
      })
      const event = {}
      wrapper.find('button').prop('onClick')(event)
      expect(onClickMock).toHaveBeenCalledWith(event)
    })
  })
})
