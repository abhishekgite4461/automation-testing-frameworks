import PropTypes from 'prop-types'
import { mathMod } from 'ramda'
import React, { Component } from 'react'
import Hammer from 'react-hammerjs'
import { connect } from 'react-redux'

import { isMobile } from '../../../selectors/viewportSelectors'

const CAROUSEL_SIZE_MOBILE = 2
const CAROUSEL_SIZE_DESKTOP = 5

// components
import ProductCarouselItem from './ProductCarouselItem'

const Arrow = ({ direction, onClick }) => {
  return (
    <button
      className={`ProductCarousel-arrow ProductCarousel-arrow--${direction}`}
      onClick={onClick}
      aria-hidden
    />
  )
}

Arrow.propTypes = {
  direction: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}

const doubleArray = (array) => array.concat(array)

class ProductCarousel extends Component {
  static propTypes = {
    isImageFallbackEnabled: PropTypes.bool,
    carouselSize: PropTypes.number.isRequired,
    products: PropTypes.arrayOf(
      PropTypes.shape({
        productId: PropTypes.number,
        name: PropTypes.string,
        price: PropTypes.string,
        unitPrice: PropTypes.string,
        salePrice: PropTypes.string,
        url: PropTypes.string,
        imageUrl: PropTypes.string,
        amplienceUrl: PropTypes.string,
      })
    ),
    canRemoveProduct: PropTypes.bool,
    hideProductMeta: PropTypes.bool,
    hideProductName: PropTypes.bool,
    hideQuickView: PropTypes.bool,
    hidePrice: PropTypes.bool,
    onProductRemove: PropTypes.func,
    onProductLinkClick: PropTypes.func,
  }

  static defaultProps = {
    isImageFallbackEnabled: false,
    products: [],
    canRemoveProduct: false,
    onProductRemove: () => {},
    onProductLinkClick: () => {},
    hideProductMeta: false,
    hideProductName: false,
    hideQuickView: false,
    hidePrice: false,
  }

  state = {
    position: 1,
  }

  handleSwipe = ({ direction } = {}) => {
    const BACKWARD = 2
    const FORWARD = 4

    if (direction === BACKWARD) {
      this.moveCarouselForward()
    } else if (direction === FORWARD) {
      this.moveCarouselBackward()
    }
  }

  moveCarouselForward = () => {
    this.moveCarousel('forward')
  }

  moveCarouselBackward = () => {
    this.moveCarousel('backward')
  }

  moveCarousel(direction) {
    const { position } = this.state
    const newPostion = direction === 'forward' ? position - 1 : position + 1
    this.setState({
      position: mathMod(newPostion, this.getCarouselItems().length),
    })
  }

  getCarouselItems() {
    const { products, carouselSize } = this.props
    // if there is exactly one more item than the carousel size, 'double' the items (to avoid the wrapping effect)
    return products.length === carouselSize + 1
      ? doubleArray(products)
      : products
  }

  renderProductCarouselItem = (product, index) => {
    const {
      canRemoveProduct,
      onProductRemove,
      onProductLinkClick,
      hideProductMeta,
      hideProductName,
      hideQuickView,
      hidePrice,
      carouselSize,
      isImageFallbackEnabled,
    } = this.props

    const carouselItems = this.getCarouselItems()
    const style = { width: `${100 / carouselSize}%` }

    if (carouselItems.length > carouselSize) {
      const carouselPosition = mathMod(
        this.state.position + index,
        carouselItems.length
      )

      const isVisible = carouselPosition > 0 && carouselPosition <= carouselSize

      const translate = (carouselPosition - index - 1) * 100
      style.transform = `translate(${translate}%)`
      style.zIndex = isVisible ? '1' : '0'
    }

    return (
      <ProductCarouselItem
        isImageFallbackEnabled={isImageFallbackEnabled}
        key={index} // eslint-disable-line react/no-array-index-key
        lazyLoad
        productId={product.productId}
        name={product.name}
        price={product.price}
        unitPrice={product.unitPrice}
        salePrice={product.salePrice}
        url={product.url}
        imageUrl={product.imageUrl}
        amplienceUrl={product.amplienceUrl}
        style={style}
        onLinkClick={onProductLinkClick}
        canRemoveProduct={canRemoveProduct}
        onProductRemove={onProductRemove}
        hideProductMeta={hideProductMeta}
        hideProductName={hideProductName}
        hideQuickView={hideQuickView}
        hidePrice={hidePrice}
      />
    )
  }

  render() {
    const carouselItems = this.getCarouselItems()
    const isCarousel = carouselItems.length > this.props.carouselSize

    return carouselItems.length ? (
      <div className="ProductCarousel">
        {isCarousel && (
          <Arrow direction="left" onClick={this.moveCarouselBackward} />
        )}
        <Hammer onSwipe={this.handleSwipe}>
          <div className="ProductCarousel-container">
            {carouselItems.map(this.renderProductCarouselItem)}
          </div>
        </Hammer>
        {isCarousel && (
          <Arrow direction="right" onClick={this.moveCarouselForward} />
        )}
      </div>
    ) : null
  }
}

export default connect((state) => ({
  carouselSize: isMobile(state) ? CAROUSEL_SIZE_MOBILE : CAROUSEL_SIZE_DESKTOP,
}))(ProductCarousel)

export { ProductCarousel as WrappedProductCarousel, Arrow }
