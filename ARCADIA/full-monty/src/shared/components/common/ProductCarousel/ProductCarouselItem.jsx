import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'

import { getProductRouteFromParams } from '../../../lib/get-product-route'
import { clearProduct } from '../../../actions/common/productsActions'
import { IMAGE_SIZES } from '../../../constants/amplience'
import Product from '../Product/Product'

const ProductCarouselItem = (
  {
    lazyLoad,
    productId,
    name,
    price,
    unitPrice,
    salePrice,
    url,
    imageUrl,
    amplienceUrl,
    style,
    canRemoveProduct,
    storeCode,
    region,
    clearProduct,
    onProductRemove,
    onLinkClick,
    hideProductMeta,
    hideProductName,
    hideQuickView,
    hidePrice,
    isImageFallbackEnabled,
  },
  { l }
) => {
  const linkClickHandler = () => {
    clearProduct()
    onLinkClick(productId)
  }

  return (
    <div className="ProductCarouselItem" style={style}>
      <Product
        isImageFallbackEnabled={isImageFallbackEnabled}
        lazyLoad={lazyLoad}
        grid={1}
        sizes={IMAGE_SIZES.smallProduct}
        name={name}
        productId={productId}
        productUrl={
          url ||
          (productId
            ? getProductRouteFromParams(
                region,
                storeCode,
                l`product`,
                productId
              )
            : '')
        }
        assets={imageUrl ? [{ url: imageUrl }] : []}
        productBaseImageUrl={amplienceUrl}
        unitPrice={salePrice || unitPrice || price}
        wasPrice={unitPrice}
        showPrice={(!!unitPrice || !!salePrice || !!price) && !hidePrice}
        onLinkClick={linkClickHandler}
        showProductView={false}
        hideProductMeta={hideProductMeta}
        hideProductName={hideProductName}
        hideQuickView={hideQuickView}
        isCarouselItem
      />
      {canRemoveProduct && (
        <button
          className="ProductCarouselItem-delete Button Button--secondary"
          onClick={onProductRemove.bind(null, productId)} // eslint-disable-line react/jsx-no-bind
        >
          <span className="screen-reader-text">
            Remove product from carousel
          </span>
        </button>
      )}
    </div>
  )
}

ProductCarouselItem.propTypes = {
  isImageFallbackEnabled: PropTypes.bool,
  lazyLoad: PropTypes.bool,
  productId: PropTypes.number,
  name: PropTypes.string,
  price: PropTypes.string,
  unitPrice: PropTypes.string,
  salePrice: PropTypes.string,
  url: PropTypes.string,
  imageUrl: PropTypes.string,
  amplienceUrl: PropTypes.string,
  style: PropTypes.objectOf(PropTypes.string),
  canRemoveProduct: PropTypes.bool,
  hideProductMeta: PropTypes.bool,
  hideProductName: PropTypes.bool,
  hideQuickView: PropTypes.bool,
  hidePrice: PropTypes.bool,
  storeCode: PropTypes.string,
  region: PropTypes.string,
  onLinkClick: PropTypes.func,
  onProductRemove: PropTypes.func,
  // actions
  clearProduct: PropTypes.func.isRequired,
}

ProductCarouselItem.defaultProps = {
  isImageFallbackEnabled: false,
  lazyLoad: false,
  productId: undefined,
  name: undefined,
  price: undefined,
  unitPrice: undefined,
  salePrice: undefined,
  url: undefined,
  imageUrl: undefined,
  amplienceUrl: '',
  style: {},
  canRemoveProduct: false,
  hideProductMeta: false,
  hideProductName: false,
  hideQuickView: false,
  hidePrice: false,
  storeCode: undefined,
  region: undefined,
  onLinkClick: () => {},
  onProductRemove: () => {},
}

ProductCarouselItem.contextTypes = {
  l: PropTypes.func,
}

export default connect(
  (state) => ({
    storeCode: state.config.storeCode,
    region: state.config.region,
  }),
  { clearProduct }
)(ProductCarouselItem)

export { ProductCarouselItem as WrappedProductCarouselItem }
