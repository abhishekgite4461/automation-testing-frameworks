import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as actions from '../../../actions/components/campaignActions'
import { connect } from 'react-redux'
import Hammer from 'react-hammerjs'

@connect(({ campaigns }) => ({ lfwAssets: campaigns.lfw.blogFeed }), actions)
export default class CarouselSocial extends Component {
  static propTypes = {
    name: PropTypes.string,
    carousel: PropTypes.object,
    lfwAssets: PropTypes.array,
    assets: PropTypes.array,
    onClick: PropTypes.func,
    backCarousel: PropTypes.func,
    forwardCarousel: PropTypes.func,
    getCampaignBlogFeed: PropTypes.func,
    handleSwipe: PropTypes.func,
    mode: PropTypes.string,
    type: PropTypes.string,
    template: PropTypes.string,
    className: PropTypes.string,
    autoplay: PropTypes.bool,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentWillMount() {
    if (process.browser) {
      const { template, getCampaignBlogFeed } = this.props
      if (template === 'lfwcarouselblog') getCampaignBlogFeed('lfw', 'lfw')
    }
  }

  componentDidMount() {
    this.carouselAutoPlayInterval = setInterval(this.autoplay, 4000)
  }

  componentWillUnmount() {
    clearInterval(this.carouselAutoPlayInterval)
  }

  autoplay = () => {
    const { autoplay, handleSwipe, assets } = this.props
    if (autoplay && assets.length > 1) handleSwipe({ direction: 2 })
  }

  renderTweet = ({ origin, text, created_at: createdAt }) => {
    return (
      <div className="Carousel-item--tweet">
        <h3 className="Carousel-socialOrigin">
          <img
            alt="Twitter icon"
            className="Carousel-socialIcon"
            src="/assets/common/images/twitter.svg"
          />{' '}
          <span className="Carousel-socialType">{`${origin}`}</span>{' '}
          {`${createdAt}`}
        </h3>
        <p className="Carousel-text--social">{text}</p>
      </div>
    )
  }

  renderInsta = (
    { origin, text, media_url: mediaUrl, created_at: createdAt },
    i
  ) => {
    const { l } = this.context
    return (
      <div className="Carousel-item--insta">
        <h3 className="Carousel-socialOrigin">
          <img
            alt="Instagram icon"
            className="Carousel-socialIcon"
            src="/assets/common/images/instagram.svg"
          />{' '}
          <span className="Carousel-socialType">{`${origin}`}</span>{' '}
          {`${createdAt}`}
        </h3>
        <img
          className="Carousel-image--insta"
          src={mediaUrl}
          ref={`CarouselImage${i}`}
          draggable="false"
          alt={`${l`Carousel Image`} ${i}`}
        />
        {text !== '' ? <p className="Carousel-text--social">{text}</p> : ''}
      </div>
    )
  }

  renderSnap = ({ origin }) => {
    return (
      <div className="Carousel-item--snap">
        <h3 className="Carousel-socialOrigin">
          <img
            alt="Snapchat icon"
            className="Carousel-socialIcon"
            src="/assets/common/images/snapchat.svg"
          />{' '}
          <span className="Carousel-socialType">{`${origin}`}</span>
        </h3>
        <img
          className="Carousel-text--social"
          src={'/assets/custom/lfw16/images/snapchatImage.jpg'}
          alt="Follow Topshop_Snaps on Snapchat"
        />
      </div>
    )
  }

  renderAsset = (asset) => {
    return {
      instagram: this.renderInsta.bind(null, asset),
      twitter: this.renderTweet.bind(null, asset),
      snapchat: this.renderSnap.bind(null, asset),
    }[asset.origin]()
  }

  renderBlogAsset = ({ image, title, link }) => {
    const { l } = this.context
    return (
      <div className="Carousel-item--blogPost">
        <a className="Carousel-link" href={link}>
          <img alt="Blog Carousel" className="Carousel-blogImage" src={image} />
          <h2 className="Carousel-text--blog">{title}</h2>
          <div className="Carousel-item--readmore">
            {l`Read More`}{' '}
            <img
              alt="Read more arrow"
              className="Carousel-item--arrow"
              src={'/assets/custom/lfw16/images/ctaarrow.svg'}
            />
          </div>
        </a>
      </div>
    )
  }

  render() {
    const { l } = this.context
    const {
      assets,
      lfwAssets,
      template,
      backCarousel,
      forwardCarousel,
      carousel,
      name,
      handleSwipe,
      mode,
      onClick = () => {},
      className,
      type,
    } = this.props

    const { current, previous, direction } = carousel[name]

    if (assets && assets instanceof Array) {
      if (assets[0] && assets[0].origin !== 'snapchat')
        assets.unshift({ origin: 'snapchat' })
    }

    const typeClassName =
      type === 'blog' ? 'Carousel-item--blog' : 'Carousel-item--social'
    const typeOfAssets = template === 'lfwcarouselblog' ? lfwAssets : assets

    const listItems =
      typeOfAssets instanceof Array
        ? typeOfAssets.map((asset, i) => {
            const selected = i === current
            return (
              <li
                role="presentation"
                key={`i${name}${asset.url}`}
                className={`Carousel-item ${typeClassName} Carousel-${direction}Item${
                  selected ? ' is-selected' : ''
                }${previous === i ? ' is-previous' : ''}`}
                onClick={() => onClick(i)}
              >
                {type === 'blog'
                  ? this.renderBlogAsset(asset)
                  : this.renderAsset(asset)}
              </li>
            )
          })
        : [] // make defensive in case of error on microservices

    return (
      <div
        className={`Carousel${
          type !== 'blog' ? ` Carousel--${mode}` : ` Carousel--${type}`
        } ${className}`}
      >
        {template === 'lfwcarouselblog' ? (
          <h5 className="Carousel-logo">
            <img
              alt="Blog icon"
              className="Carousel-blogIcon"
              src={'/assets/custom/lfw16/images/blog.svg'}
            />
            <span className="Carousel-title">{l`On the Blog`}</span>
          </h5>
        ) : (
          ''
        )}
        <Hammer onSwipe={(e) => handleSwipe(e)}>
          <div className="Carousel-images Carousel-images--social">
            <ul
              className={`Carousel-list ${
                type === 'blog'
                  ? 'Carousel-list--blog'
                  : 'Carousel-list--social'
              }`}
            >
              {listItems.slice(current)}
              {listItems.slice(0, current)}
            </ul>
          </div>
        </Hammer>

        {typeOfAssets.length > 4 && type !== 'blog' ? (
          <button
            key="right"
            className={`Carousel-arrow Carousel-arrow--socialRight`}
            onClick={() => forwardCarousel(name)}
          >{l`right`}</button>
        ) : (
          [
            <button
              key="left"
              className={`Carousel-arrow Carousel-arrow--left`}
              onClick={() => backCarousel(name)}
            >{l`left`}</button>,
            <button
              key="right"
              className={`Carousel-arrow Carousel-arrow--right`}
              onClick={() => forwardCarousel(name)}
            >{l`right`}</button>,
          ]
        )}
      </div>
    )
  }
}
