import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { setFormField } from '../../../actions/common/formActions'
import {
  resetPredictions,
  setSelectedPlace,
  autocomplete,
  getCurrentLocationError,
  setUserLocatorPending,
  onGetCurrentPositionSuccess,
  resetSearchTerm,
  resetSelectedPlace,
} from '../../../actions/components/UserLocatorActions'
import storeLocatorConsts from '../../../constants/storeLocator'
import Button from '../Button/Button'

@connect(
  (state) => ({
    pending: state.userLocator.pending,
    selectedPlaceDetails: state.userLocator.selectedPlaceDetails,
    predictions: state.userLocator.predictions,
    userLocatorSearch: state.forms.userLocator.fields.userLocation.value,
  }),
  {
    setFormField,
    resetPredictions,
    setSelectedPlace,
    autocomplete,
    getCurrentLocationError,
    setUserLocatorPending,
    onGetCurrentPositionSuccess,
    resetSearchTerm,
    resetSelectedPlace,
  }
)
export default class UserLocatorInput extends Component {
  static propTypes = {
    autocomplete: PropTypes.func,
    getCurrentLocationError: PropTypes.func,
    onGetCurrentPositionSuccess: PropTypes.func,
    pending: PropTypes.bool,
    predictions: PropTypes.array,
    resetPredictions: PropTypes.func,
    resetSearchTerm: PropTypes.func,
    resetSelectedPlace: PropTypes.func,
    selectedCountry: PropTypes.string,
    selectedPlaceDetails: PropTypes.object,
    setFormField: PropTypes.func,
    setSelectedPlace: PropTypes.func,
    setUserLocatorPending: PropTypes.func,
    submitAlwaysEnabled: PropTypes.bool,
    userLocatorSearch: PropTypes.string,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentWillReceiveProps(props) {
    if (props.predictions.length && !this.props.predictions.length) {
      document.addEventListener('click', this.onDocumentClick)
    } else if (!props.predictions.length) {
      document.removeEventListener('click', this.onDocumentClick)
    }
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.onDocumentClick)
  }

  onDocumentClick = () => {
    this.props.resetPredictions()
    document.removeEventListener('click', this.onDocumentClick)
  }

  onGetCurrentPositionError = () => {
    if (this.input) this.input.focus()
    this.props.getCurrentLocationError()
    this.props.setUserLocatorPending(false)
  }

  getCurrentLocation = (e) => {
    e.preventDefault()
    if (window.navigator.geolocation) {
      this.props.setUserLocatorPending(true)
      window.navigator.geolocation.getCurrentPosition(
        this.props.onGetCurrentPositionSuccess,
        this.onGetCurrentPositionError
      )
    }
  }

  setSelectedPlace = (place) => {
    const { resetPredictions, setSelectedPlace, setFormField } = this.props
    resetPredictions()
    setSelectedPlace(place)
    setFormField(
      'userLocator',
      'userLocation',
      place.description || place.formatted_address
    )

    if (this.input) this.input.blur()
  }

  autocomplete = (e) => {
    const { resetSelectedPlace, setFormField, autocomplete } = this.props
    let searchValue = e.target.value
    if (storeLocatorConsts.ukPostCodeRegEx.test(searchValue)) {
      searchValue = searchValue.toUpperCase()
    }
    resetSelectedPlace()
    setFormField('userLocator', 'userLocation', searchValue)
    autocomplete(searchValue)
  }

  clearSearchField = (e) => {
    const {
      setFormField,
      resetPredictions,
      resetSearchTerm,
      resetSelectedPlace,
    } = this.props
    if (e) e.preventDefault()
    setFormField('userLocator', 'userLocation', '')
    resetPredictions()
    resetSearchTerm()
    resetSelectedPlace()
  }

  render() {
    const { l } = this.context
    const {
      pending,
      predictions,
      selectedCountry,
      selectedPlaceDetails,
      submitAlwaysEnabled,
      userLocatorSearch,
    } = this.props
    const isPredictionsListOpen = predictions.length
      ? ' is-predictionsListOpen'
      : ''
    const showInput = selectedCountry === storeLocatorConsts.defaultCountry
    const label = l`Enter town/postcode`

    return (
      <div className="UserLocatorInput">
        <div
          className={`UserLocatorInput-inputContainer${
            showInput ? ' is-visible' : ''
          }${isPredictionsListOpen}`}
        >
          <div className="UserLocatorInput-queryInput">
            <label className="screen-reader-text" htmlFor="UserLocatorInput">
              {label}
            </label>
            <input
              id="UserLocatorInput"
              ref={(input) => {
                this.input = input
              }}
              className="UserLocatorInput-inputField"
              type="search"
              name="searchTerm"
              value={userLocatorSearch}
              placeholder={label}
              onChange={this.autocomplete}
              autoComplete="off"
            />
            {userLocatorSearch && (
              <button
                title={l`Clear`}
                type="button"
                className={`UserLocatorInput-rightIcon UserLocatorInput-clearButton`}
                onClick={this.clearSearchField}
              >
                <span className="screen-reader-text">{l`Clear`}</span>
              </button>
            )}
            {!userLocatorSearch && (
              <button
                title={l`Get my current location`}
                type="button"
                className={`UserLocatorInput-rightIcon UserLocatorInput-currentLocationButton${
                  pending ? ' is-pending' : ''
                }`}
                onClick={this.getCurrentLocation}
              >
                <span className="screen-reader-text">{l`Get my current location`}</span>
              </button>
            )}
          </div>
          <Button
            title={l`Go`}
            type="submit"
            className="UserLocator-goButton"
            isDisabled={!submitAlwaysEnabled && !selectedPlaceDetails.place_id}
          >
            {l`Go`}
          </Button>
          {predictions.length > 0 && (
            <ul className="UserLocatorInput-predictionsList">
              {predictions.map((prediction) => (
                <li
                  role="presentation"
                  key={prediction.place_id}
                  className="UserLocatorInput-predictionsListItem"
                  onClick={() => this.setSelectedPlace(prediction)}
                >
                  <button>{prediction.description}</button>
                </li>
              ))}
            </ul>
          )}
        </div>
      </div>
    )
  }
}
