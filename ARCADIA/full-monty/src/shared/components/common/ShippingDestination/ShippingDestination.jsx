import classnames from 'classnames'
import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import { isEmpty } from 'ramda'

import { getCurrencySymbol } from '../../../selectors/configSelectors'
import { getShippingDestination } from '../../../selectors/shippingDestinationSelectors'
import { camelCaseify } from '../../../lib/string-utils'
import {
  getShippingDestinationDetails,
  saveShippingDestinationDetails,
} from '../../../../client/lib/storage'

// actions
import { showModal } from '../../../actions/common/modalActions'
import { setShippingDestination } from '../../../actions/common/shippingDestinationActions'

// components
import ShippingPreferencesSelectorModal from '../ShippingPreferencesSelectorModal/ShippingPreferencesSelectorModal'

@connect(
  (state) => ({
    currencySymbol: getCurrencySymbol(state),
    shippingDestination: getShippingDestination(state),
    currentCountry: state.routing.location.query.currentCountry,
    defaultCountry: state.config.country,
  }),
  (dispatch) => ({
    setShippingDestination: (country) =>
      dispatch(setShippingDestination(country)),
    onShippingDestinationChange: () =>
      dispatch(showModal(<ShippingPreferencesSelectorModal />)),
  })
)
export default class ShippingDestination extends React.Component {
  static propTypes = {
    currencySymbol: PropTypes.string.isRequired,
    shippingDestination: PropTypes.string,
    className: PropTypes.string,
    modifier: PropTypes.string,
    text: PropTypes.string,
    currencySymbolPosition: PropTypes.oneOf(['left', 'right', 'hide']),
    currencySymbolStyle: PropTypes.oneOf(['standard', 'bracketed']),
    displayCountry: PropTypes.bool,
    onShippingDestinationChange: PropTypes.func.isRequired,
    currentCountry: PropTypes.string,
  }

  static defaultProps = {
    shippingDestination: undefined,
    modifier: undefined,
    className: '',
    text: undefined,
    currencySymbolPosition: 'left',
    currencySymbolStyle: 'standard',
    displayCountry: false,
    currentCountry: '',
  }

  componentDidMount() {
    const {
      defaultCountry,
      currentCountry,
      setShippingDestination,
    } = this.props

    const shippingDestination = getShippingDestinationDetails()
    const country = isEmpty(currentCountry)
      ? isEmpty(shippingDestination)
        ? defaultCountry
        : shippingDestination
      : currentCountry

    if (!isEmpty(currentCountry))
      saveShippingDestinationDetails({ destination: country })

    setShippingDestination(country)
  }

  render() {
    const {
      className,
      currencySymbol,
      shippingDestination,
      modifier,
      text,
      currencySymbolPosition,
      currencySymbolStyle,
      displayCountry,
      onShippingDestinationChange,
    } = this.props

    const classNames = classnames('ShippingDestination', className, {
      [`ShippingDestination--${modifier}`]: modifier,
    })
    const currencySymbolClassNames = classnames(
      'ShippingDestination-currencySymbol',
      {
        [`ShippingDestination-currencySymbol--${currencySymbolStyle}`]:
          currencySymbolStyle !== 'standard',
      }
    )
    const flagClassNames = classnames('ShippingDestination-flag', {
      [`ShippingDestination-flag--${camelCaseify(
        shippingDestination
      )}`]: shippingDestination,
    })

    return (
      <button className={classNames} onClick={onShippingDestinationChange}>
        {text && <span className="ShippingDestination-text">{text}</span>}
        {displayCountry &&
          shippingDestination && (
            <span className="ShippingDestination-country">
              {shippingDestination}
            </span>
          )}
        {currencySymbolPosition === 'left' && (
          <span className={currencySymbolClassNames}>{currencySymbol}</span>
        )}
        {shippingDestination && <span className={flagClassNames} />}
        {currencySymbolPosition === 'right' && (
          <span className={currencySymbolClassNames}>{currencySymbol}</span>
        )}
      </button>
    )
  }
}
