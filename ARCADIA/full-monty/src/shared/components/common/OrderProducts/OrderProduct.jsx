import React, { Component } from 'react'
import { connect } from 'react-redux'
import { equals, keys, pick } from 'ramda'
import PropTypes from 'prop-types'
import QubitReact from 'qubit-react/wrapper'
import { Link } from 'react-router'

import ResponsiveImage from '../ResponsiveImage/ResponsiveImage'
import HistoricalPrice from '../HistoricalPrice/HistoricalPrice'
import { IMAGE_SIZES } from '../../../constants/amplience'
import { getRouteFromUrl } from '../../../lib/get-product-route'
import {
  sendAnalyticsClickEvent,
  GTM_ACTION,
  GTM_CATEGORY,
  GTM_LABEL,
} from '../../../analytics'

class OrderProduct extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    assets: PropTypes.array.isRequired,
    baseImageUrl: PropTypes.string,
    inStock: PropTypes.bool.isRequired,
    lowStock: PropTypes.bool.isRequired,
    productId: PropTypes.number,
    imageUrl: PropTypes.string,
    size: PropTypes.string,
    quantity: PropTypes.number,
    unitPrice: PropTypes.string,
    wasPrice: PropTypes.string,
    wasWasPrice: PropTypes.string,
    rrp: PropTypes.string,
    children: PropTypes.node,
    isDDPProduct: PropTypes.bool,
    sourceUrl: PropTypes.string,
    shouldLinkToPdp: PropTypes.bool,
    sendAnalyticsClickEvent: PropTypes.func.isRequired,
  }

  static defaultProps = {
    baseImageUrl: '',
    sourceUrl: '',
    shouldLinkToPdp: false,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  shouldComponentUpdate(nextProps) {
    // todo: refactor when header is simplified .. DES-292
    const compare = pick(
      [
        'name',
        'imageUrl',
        'lowStock',
        'inStock',
        'size',
        'quantity',
        'unitPrice',
        'wasPrice',
        'wasWasPrice',
        'rrp',
        'children',
      ],
      nextProps
    )
    const current = pick(keys(compare), this.props)
    return !equals(compare, current)
  }

  renderImage = () => {
    const { name, assets, baseImageUrl, imageUrl } = this.props
    return (
      <ResponsiveImage
        alt={name}
        className="OrderProducts-productImage"
        amplienceUrl={baseImageUrl}
        sizes={IMAGE_SIZES.miniBag}
        src={
          assets
            ? assets.find((asset) => asset.assetType === 'IMAGE_THUMB').url
            : imageUrl
        }
      />
    )
  }

  renderHeader = () => {
    const { name } = this.props
    return <header className="OrderProducts-productName">{name}</header>
  }

  trackLinkClick = () => {
    const { sendAnalyticsClickEvent } = this.props
    sendAnalyticsClickEvent({
      category: GTM_CATEGORY.SHOPPING_BAG,
      action: GTM_ACTION.CLICKED,
      label: GTM_LABEL.PRODUCT_DETAILS,
    })
  }

  wrapInPdpLink = (element) => {
    const { shouldLinkToPdp, sourceUrl } = this.props
    if (shouldLinkToPdp && sourceUrl) {
      return (
        <Link to={getRouteFromUrl(sourceUrl)} onClick={this.trackLinkClick}>
          {element}
        </Link>
      )
    }
    return element
  }

  render() {
    const {
      inStock,
      lowStock,
      productId,
      size,
      quantity,
      unitPrice,
      wasPrice,
      wasWasPrice,
      rrp,
      children,
      isDDPProduct,
    } = this.props
    const { l } = this.context
    return (
      <QubitReact id="qubit-OrderProducts-media" productId={productId}>
        <div className="OrderProducts-media">
          <QubitReact id="qubit-OrderProducts-mediaLeft" productId={productId}>
            <div className="OrderProducts-mediaLeft">
              {this.wrapInPdpLink(this.renderImage())}
            </div>
          </QubitReact>
          <div className="OrderProducts-mediaBody">
            {this.wrapInPdpLink(this.renderHeader())}
            <QubitReact
              id="qubit-OrderProducts-mediaBody"
              productId={productId}
            >
              <div>
                {!isDDPProduct && (
                  <p className="OrderProducts-row OrderProducts-productSize">
                    <span className="OrderProducts-label">{`${quantity ||
                      1} x ${
                      size
                        ? `${l`size`} ${decodeURIComponent(size)}`
                        : l`One sized item`
                    }`}</span>
                    {lowStock === true && (
                      <span className="OrderProducts-productLowStock">{l`Low stock`}</span>
                    )}
                    {inStock === false && (
                      <span className="OrderProducts-productOutOfStock">{l`Out of stock`}</span>
                    )}
                  </p>
                )}
                <p className="OrderProducts-row OrderProducts-price">
                  <HistoricalPrice
                    className="HistoricalPrice--orderProducts"
                    price={unitPrice}
                    wasPrice={wasPrice}
                    wasWasPrice={wasWasPrice}
                    rrp={rrp}
                  />
                </p>
                {children}
              </div>
            </QubitReact>
          </div>
        </div>
      </QubitReact>
    )
  }
}

export default connect(
  null,
  { sendAnalyticsClickEvent }
)(OrderProduct)
