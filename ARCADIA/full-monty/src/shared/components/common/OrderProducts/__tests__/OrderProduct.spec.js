import QubitReact from 'qubit-react/wrapper'
import { Link } from 'react-router'

import { getRouteFromUrl } from '../../../../lib/get-product-route'
import { GTM_CATEGORY, GTM_ACTION, GTM_LABEL } from '../../../../analytics'
import testComponentHelper from 'test/unit/helpers/test-component'

import OrderProduct from '../OrderProduct'
import ResponsiveImage from '../../ResponsiveImage/ResponsiveImage'

describe('<OrderProduct/>', () => {
  const render = testComponentHelper(OrderProduct.WrappedComponent)

  const initialProps = {
    name: 'Floral Embroidered PJ Set',
    assets: [
      {
        assetType: 'IMAGE_THUMB',
        index: 1,
        url:
          'http://media.topshop.com/wcsstore/TopShop/images/catalog/01W16JCRM_thumb.jpg',
      },
    ],
    inStock: true,
    lowStock: false,
    isDDPProduct: false,
    sendAnalyticsClickEvent: jest.fn(),
  }

  describe('@renders', () => {
    it('in default state', () => {
      const { getTree } = render(initialProps)
      expect(getTree()).toMatchSnapshot()
    })

    it('displays the low stock message', () => {
      const { wrapper } = render({
        ...initialProps,
        lowStock: true,
      })
      expect(wrapper.find('.OrderProducts-productLowStock').text()).toBe(
        'Low stock'
      )
    })

    it('does not display the low stock message if isDDPProduct is set to true', () => {
      const { wrapper } = render({
        ...initialProps,
        lowStock: true,
        isDDPProduct: true,
      })
      expect(wrapper.find('.OrderProducts-productLowStock')).toHaveLength(0)
    })

    it('does not display product size if isDDPProduct is set to true', () => {
      const { wrapper } = render({
        ...initialProps,
        isDDPProduct: true,
      })
      expect(wrapper.find('.OrderProducts-productSize')).toHaveLength(0)
    })

    it('display product size if isDDPProduct is set to false', () => {
      const { wrapper } = render({
        ...initialProps,
        isDDPProduct: false,
      })
      expect(wrapper.find('.OrderProducts-productSize')).toHaveLength(1)
    })

    it('displays the out of stock message', () => {
      const { wrapper } = render({
        ...initialProps,
        inStock: false,
      })
      expect(wrapper.find('.OrderProducts-productOutOfStock').text()).toBe(
        'Out of stock'
      )
    })

    it('does not display the out of stock message if isDDPProduct is set to true', () => {
      const { wrapper } = render({
        ...initialProps,
        inStock: false,
        isDDPProduct: true,
      })
      expect(wrapper.find('.OrderProducts-productOutOfStock')).toHaveLength(0)
    })

    describe('ResponsiveImage', () => {
      it('should render ResponsiveImage', () => {
        const props = {
          ...initialProps,
          baseImageUrl: 'url',
        }
        const { wrapper } = render(props)
        const responsiveImage = wrapper.find(ResponsiveImage)
        expect(responsiveImage).toHaveLength(1)
        expect(responsiveImage.prop('amplienceUrl')).toBe(props.baseImageUrl)
      })

      describe('if shouldLinkToPdp prop true', () => {
        describe('if sourceUrl prop is provided', () => {
          it('should render ResponsiveImage inside of a Link', () => {
            const props = {
              ...initialProps,
              baseImageUrl: 'url',
              shouldLinkToPdp: true,
              sourceUrl:
                'http://ts-acc1.acc.digital.arcadiagroup.co.uk/en/tsuk/product/mid-blue-jamie-jeans-7947602',
            }
            const { wrapper } = render(props)
            const responsiveImage = wrapper.find(ResponsiveImage)
            expect(responsiveImage.parent().is(Link)).toBe(true)
            const link = wrapper.find('.OrderProducts-mediaLeft').find(Link)
            expect(link.prop('to')).toEqual(getRouteFromUrl(props.sourceUrl))
          })

          it('should dispatch GTM action if link is clicked', () => {
            const props = {
              ...initialProps,
              baseImageUrl: 'url',
              shouldLinkToPdp: true,
              sourceUrl:
                'http://ts-acc1.acc.digital.arcadiagroup.co.uk/en/tsuk/product/mid-blue-jamie-jeans-7947602',
            }
            const { wrapper } = render(props)
            expect(initialProps.sendAnalyticsClickEvent).not.toHaveBeenCalled()

            const responsiveImage = wrapper.find(ResponsiveImage)
            responsiveImage.parent().simulate('click')
            expect(initialProps.sendAnalyticsClickEvent).toHaveBeenCalledTimes(
              1
            )
            expect(initialProps.sendAnalyticsClickEvent).toHaveBeenCalledWith({
              category: GTM_CATEGORY.SHOPPING_BAG,
              action: GTM_ACTION.CLICKED,
              label: GTM_LABEL.PRODUCT_DETAILS,
            })
          })
        })

        describe('if sourceUrl prop is not provided', () => {
          it('should NOT render ResponsiveImage inside of a Link', () => {
            const props = {
              ...initialProps,
              baseImageUrl: 'url',
              shouldLinkToPdp: false,
              sourceUrl: undefined,
            }
            const { wrapper } = render(props)
            const responsiveImage = wrapper.find(ResponsiveImage)
            expect(responsiveImage.parent().is('div')).toBe(true)
          })
        })
      })

      describe('if sourceUrl prop not provided', () => {
        it('should NOT render ResponsiveImage inside of a Link', () => {
          const props = {
            ...initialProps,
            baseImageUrl: 'url',
            shouldLinkToPdp: false,
            sourceUrl:
              'http://ts-acc1.acc.digital.arcadiagroup.co.uk/en/tsuk/product/mid-blue-jamie-jeans-7947602',
          }
          const { wrapper } = render(props)
          const responsiveImage = wrapper.find(ResponsiveImage)
          expect(responsiveImage.parent().is('div')).toBe(true)
        })
      })
    })

    describe('header', () => {
      it('should render a header ', () => {
        const props = {
          ...initialProps,
          baseImageUrl: 'url',
        }
        const { wrapper } = render(props)
        const header = wrapper.find('header.OrderProducts-productName')
        expect(header).toHaveLength(1)
      })

      describe('if shouldLinkToPdp prop true', () => {
        describe('if sourceUrl prop is provided', () => {
          it('render header inside of a Link', () => {
            const props = {
              ...initialProps,
              baseImageUrl: 'url',
              shouldLinkToPdp: true,
              sourceUrl:
                'http://ts-acc1.acc.digital.arcadiagroup.co.uk/en/tsuk/product/mid-blue-jamie-jeans-7947602',
            }
            const { wrapper } = render(props)
            const header = wrapper.find('header.OrderProducts-productName')
            expect(header.parent().is(Link)).toBe(true)
            const link = wrapper.find('.OrderProducts-mediaBody').find(Link)
            expect(link.prop('to')).toEqual(getRouteFromUrl(props.sourceUrl))
          })
        })

        describe('if sourceUrl prop is not provided', () => {
          it('should not render header inside of Link', () => {
            const props = {
              ...initialProps,
              baseImageUrl: 'url',
              shouldLinkToPdp: false,
              sourceUrl: undefined,
            }
            const { wrapper } = render(props)
            const header = wrapper.find('header.OrderProducts-productName')
            expect(header.parent().is('div')).toBe(true)
          })
        })
      })

      describe('if shouldLinkToPdp prop is false', () => {
        it('should not render header inside of Link', () => {
          const props = {
            ...initialProps,
            baseImageUrl: 'url',
            shouldLinkToPdp: false,
            sourceUrl:
              'http://ts-acc1.acc.digital.arcadiagroup.co.uk/en/tsuk/product/mid-blue-jamie-jeans-7947602',
          }
          const { wrapper } = render(props)
          const header = wrapper.find('header.OrderProducts-productName')
          expect(header.parent().is('div')).toBe(true)
        })
      })
    })

    it('should render ‘media’ and ‘media-body’ <QubitReact /> components with `productId`', () => {
      const props = {
        ...initialProps,
        productId: 92498234,
      }
      const { wrapper } = render(props)
      const mediaQubitReact = wrapper
        .find(QubitReact)
        .find({ id: 'qubit-OrderProducts-media' })
      const mediaLeftQubitReact = wrapper
        .find(QubitReact)
        .find({ id: 'qubit-OrderProducts-mediaLeft' })
      expect(mediaQubitReact.prop('productId')).toBe(92498234)
      expect(mediaLeftQubitReact.prop('productId')).toBe(92498234)
    })
  })
})
