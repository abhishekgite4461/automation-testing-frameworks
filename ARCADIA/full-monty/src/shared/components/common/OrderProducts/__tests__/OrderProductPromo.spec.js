import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import React from 'react'

import OrderProductPromo from '../OrderProductPromo'

describe('<OrderProductPromo/>', () => {
  describe('@renders', () => {
    it('renders a link when "promotionDisplayURL" and "promotionDisplayURL" are defined', () => {
      const props = {
        product: {
          promotionDisplayURL:
            'http://www.validpromo.example.org/promoId=3x8socks',
          promoTitle: '3 for £8 Socks Promotion',
        },
      }
      const wrapper = shallow(<OrderProductPromo {...props} />)
      expect(toJson(wrapper)).toMatchSnapshot()
    })
    it('renders a message when "discountText" is defined', () => {
      const props = {
        product: {
          discountText: '3 for £8 Socks Promotion',
        },
      }
      const wrapper = shallow(<OrderProductPromo {...props} />)
      expect(toJson(wrapper)).toMatchSnapshot()
    })

    describe('returns null', () => {
      it('when the "promotionDisplayURL" is undefined', () => {
        const props = {
          product: {
            promoTitle: '3 for £8 Socks Promotion',
          },
        }
        const wrapper = shallow(<OrderProductPromo {...props} />)
        expect(toJson(wrapper)).toMatchSnapshot()
      })
      it('when the "promoTitle" is undefined', () => {
        const props = {
          product: {
            promotionDisplayURL:
              'http://www.validpromo.example.org/promoId=3x8socks',
          },
        }
        const wrapper = shallow(<OrderProductPromo {...props} />)
        expect(toJson(wrapper)).toMatchSnapshot()
      })
    })
  })
})
