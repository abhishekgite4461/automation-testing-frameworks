import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Image from '../Image/Image'
import * as ShoppingBagActions from '../../../actions/common/shoppingBagActions'
import * as ModalActions from '../../../actions/common/modalActions'
import {
  isFeatureWishlistEnabled,
  isFeatureEnhancedMessagingEnabled,
} from '../../../selectors/featureSelectors'
import { getExpressDeliveryAvailableForProducts } from '../../../selectors/enhancedMessagingSelectors'
import OrderProductNotification from '../OrderProductNotification/OrderProductNotification'
import { getOrderProductsBasket } from '../../../selectors/productSelectors'
import MinibagWishlistButton from '../WishlistButton/MinibagWishlistButton'
import Button from '../Button/Button'
import OrderProduct from './OrderProduct'
import Price from '../Price/Price'
import Select from '../FormComponents/Select/Select'
import { findIndex, path, pathOr, propEq, range, min } from 'ramda'
import { getFulfilmentDetails } from '../../../lib/get-delivery-days'
import OrderProductPromo from './OrderProductPromo'
import { deliveryDays } from '../../../lib/get-basket-availability'
import { analyticsShoppingBagClickEvent } from '../../../analytics/tracking/site-interactions'
import {
  sendAnalyticsClickEvent,
  GTM_ACTION,
  GTM_CATEGORY,
} from '../../../analytics'

@connect(
  (state) => ({
    orderId: state.shoppingBag.bag.orderId,
    selectedStore: state.selectedBrandFulfilmentStore,
    CFSi: state.features.status.FEATURE_CFSI,
    orderSummary: state.checkout.orderSummary,
    isFeatureWishlistEnabled: isFeatureWishlistEnabled(state),
    isFeatureEnhancedMessagingEnabled: isFeatureEnhancedMessagingEnabled(state),
    productsBasket: getOrderProductsBasket(state),
    getExpressDeliveryAvailableForProducts: getExpressDeliveryAvailableForProducts(
      state
    ),
  }),
  { ...ShoppingBagActions, ...ModalActions, sendAnalyticsClickEvent }
)
export default class OrderProducts extends Component {
  static propTypes = {
    products: PropTypes.array,
    orderId: PropTypes.number,
    onUpdateProduct: PropTypes.func,
    deleteFromBag: PropTypes.func,
    fetchProductItemSizesAndQuantities: PropTypes.func,
    persistShoppingBagProduct: PropTypes.func,
    showModal: PropTypes.func,
    closeModal: PropTypes.func,
    scrollOnEdit: PropTypes.func,
    updateShoppingBagProduct: PropTypes.func,
    canModify: PropTypes.bool,
    oosOnly: PropTypes.bool,
    allowEmptyBag: PropTypes.bool,
    scrollable: PropTypes.bool,
    className: PropTypes.string,
    selectedStore: PropTypes.object,
    CFSi: PropTypes.bool,
    isFeatureWishlistEnabled: PropTypes.bool,
    isFeatureEnhancedMessagingEnabled: PropTypes.bool,
    closeMiniBag: PropTypes.func,
    openMiniBag: PropTypes.func,
    allowMoveToWishlist: PropTypes.bool.isRequired,
    sendAnalyticsClickEvent: PropTypes.func.isRequired,
    productsBasket: PropTypes.object.isRequired,
    shouldProductsLinkToPdp: PropTypes.bool,
    getExpressDeliveryAvailableForProducts: PropTypes.array,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  static defaultProps = {
    onUpdateProduct: () => {},
    canModify: true,
    className: '',
    allowMoveToWishlist: false,
    shouldProductsLinkToPdp: false,
    getExpressDeliveryAvailableForProducts: [],
  }

  products = []

  componentDidUpdate(prevProps) {
    const prevProducts = prevProps.products
    const { products, scrollOnEdit, canModify } = this.props

    if (canModify && scrollOnEdit && products.length === prevProducts.length) {
      prevProducts.forEach((bagProduct, i) => {
        const refProduct = this.products[i]
        if (!bagProduct.editing && products[i].editing)
          scrollOnEdit(refProduct.offsetTop + refProduct.offsetHeight)
      })
    }

    if (this.editSizeQtyForm) this.editSizeQtyForm.focus()
  }

  getOptionLabel = (item) => {
    const { l } = this.context
    if (item.quantity === 0) {
      return `${item.size}: ${l`Out of stock`}`
    } else if (item.quantity <= 3) {
      return `${item.size}: ${l`Low stock`}`
    }
    return `${item.size}`
  }

  getSizeOptions = (bagProduct) => {
    if (bagProduct.size) {
      return bagProduct.items.map((item) => {
        return {
          value: item.size,
          label: this.getOptionLabel(item),
          disabled: item.quantity === 0,
        }
      })
    }
    return []
  }

  getQuantityOptions = (bagProduct) => {
    if (bagProduct.size) {
      const sizeIndex = findIndex(
        propEq('size', bagProduct.sizeSelected || bagProduct.size),
        bagProduct.items
      )
      const item = bagProduct.items[sizeIndex]
      const ranges = range(1, (item && item.quantity + 1) || 10)
      const result = ranges.map((currentQuantity) => {
        return {
          value: currentQuantity,
          label: currentQuantity,
          disabled: false,
        }
      })
      return result
    }
    return []
  }

  editProduct = (index) => {
    this.updateProduct(index, { editing: true })
    this.props.fetchProductItemSizesAndQuantities(index)
  }

  sizeChangeHandler = (evt, productIndex, bagProduct) => {
    const {
      target: {
        value: size,
        options: { selectedIndex: newSizeIndex },
      },
    } = evt

    const correctedQuantity = this.correctQuantitySelected(
      bagProduct,
      newSizeIndex
    )

    this.updateProduct(productIndex, {
      catEntryIdToAdd: newSizeIndex,
      sizeSelected: size,
      quantitySelected: correctedQuantity.correctedQuantity,
      selectedQuantityWasCorrected:
        correctedQuantity.selectedQuantityWasCorrected,
    })

    analyticsShoppingBagClickEvent({
      ea: 'sizeUpdate',
      el: bagProduct.productId,
    })
  }

  quantityChangeHandler = (productIndex, evt) => {
    const { value } = evt.target
    this.updateProduct(productIndex, {
      quantitySelected: value,
      selectedQuantityWasCorrected: false,
    })
  }

  updateProduct = (index, update) => {
    this.props.onUpdateProduct(index, update)
    this.props.updateShoppingBagProduct(index, update)
  }

  doDeleteFromBag = (
    bagProduct,
    closeConfirmationModal,
    successModalText = null
  ) => () => {
    const { deleteFromBag, orderId, closeModal } = this.props
    const actions = []
    if (closeConfirmationModal) actions.push(closeModal())
    actions.push(deleteFromBag(orderId, bagProduct, successModalText))
    return Promise.all(actions)
  }

  confirmDeleteFromBag = (bagProduct) => () => {
    const { l } = this.context
    const modalHTML = (
      <div className="OrderProducts-modal">
        <p>{l`Are you sure you want to remove ${
          bagProduct.name
        } from your bag?`}</p>
        <Button
          className="OrderProducts-deleteButton"
          clickHandler={this.doDeleteFromBag(bagProduct, true)}
        >{l`Delete`}</Button>
      </div>
    )
    this.props.showModal(modalHTML, { type: 'alertdialog' })
  }

  getCollectionDetails(CFSiDay) {
    const {
      selectedStore,
      orderSummary: { deliveryLocations },
    } = this.props
    const selectedDeliveryOption =
      deliveryLocations && deliveryLocations.find(({ selected }) => selected)

    if (
      !path(['brandName'], selectedStore) ||
      (deliveryLocations &&
        selectedDeliveryOption.deliveryLocationType !== 'STORE')
    ) {
      return false
    }
    if (CFSiDay === 'today') {
      return (
        <p className="OrderProducts-collectionDetails">
          In stock at {selectedStore.brandName} {selectedStore.name}
        </p>
      )
    } else if (CFSiDay) {
      return (
        <p className="OrderProducts-collectionDetails">
          Available {CFSiDay} from {selectedStore.brandName}{' '}
          {selectedStore.name} with Express Delivery
        </p>
      )
    }
  }

  afterAddToWishlist = (bagProduct) => () => {
    const { l } = this.context
    const { openMiniBag } = this.props
    return Promise.all([
      this.doDeleteFromBag(
        bagProduct,
        false,
        l`${bagProduct.name} has been moved to your Wish List`
      )(),
      openMiniBag(),
    ])
  }

  renderRemoveFromWishlistText = () => {
    const { l } = this.context
    return (
      <span className="OrderProducts-removeFromWishlistLabel">
        {l`Removed`}
      </span>
    )
  }

  renderAddToWishlistText = () => {
    const { l } = this.context
    return (
      <span className="OrderProducts-addToWishlistLabel">
        {l`Move to wishlist`}
      </span>
    )
  }

  // Corrects the quantitySelected property in the event that the maximum quantity for a given size is too high
  correctQuantitySelected = (product, newSizeIndex) => {
    const quantityOfCurrentSize = product.quantity
    const quantityLastSelectedInUI = product.quantitySelected
    const maxQuantityForNewSize = path(
      ['items', newSizeIndex, 'quantity'],
      product
    )
    const quantityUsedForCalculation =
      quantityLastSelectedInUI || quantityOfCurrentSize
    const correctedQuantity = min(
      quantityUsedForCalculation,
      maxQuantityForNewSize
    )

    return {
      correctedQuantity: correctedQuantity.toString(),
      selectedQuantityWasCorrected:
        quantityUsedForCalculation !== correctedQuantity,
    }
  }

  saveBag(evt, productIndex) {
    this.props.persistShoppingBagProduct(productIndex)
    this.updateProduct(productIndex, { selectedQuantityWasCorrected: false })
  }

  render() {
    const { l } = this.context
    const {
      oosOnly,
      canModify,
      allowEmptyBag,
      scrollable,
      products: propsProducts,
      className,
      selectedStore,
      CFSi,
      drawer,
      isFeatureWishlistEnabled,
      isFeatureEnhancedMessagingEnabled,
      closeMiniBag,
      allowMoveToWishlist,
      sendAnalyticsClickEvent,
      productsBasket,
      shouldProductsLinkToPdp,
      getExpressDeliveryAvailableForProducts,
    } = this.props
    const products = Array.isArray(propsProducts) ? propsProducts : []
    const currentProducts = oosOnly
      ? products.filter((product) => !product.inStock || product.lowStock)
      : products
    const canDeleteItem = allowEmptyBag || currentProducts.length > 1
    const shouldScroll = !drawer && scrollable && currentProducts.length > 3
    const shouldRenderFFSDetails = !oosOnly && CFSi
    const basketExpressAvailability = deliveryDays(
      productsBasket,
      selectedStore
    ).expressDeliveryDay
    return (
      <div
        className={`OrderProducts ${className}${
          shouldScroll ? ' is-scrollable' : ''
        }`}
      >
        <div className="OrderProducts-wrapper">
          {currentProducts.map((bagProduct, i) => {
            const CFSiDay =
              shouldRenderFFSDetails &&
              !bagProduct.isDDPProduct &&
              basketExpressAvailability
                ? pathOr(
                    '',
                    ['CFSiDay'],
                    getFulfilmentDetails(bagProduct, selectedStore)
                  )
                : ''
            const availabilityForExpressOnProduct = getExpressDeliveryAvailableForProducts.find(
              (inventoryProduct) =>
                inventoryProduct.catEntryId === bagProduct.catEntryId
            )
            const isStandardDeliveryOnly =
              availabilityForExpressOnProduct &&
              !availabilityForExpressOnProduct.available

            return (
              <div
                key={i} // eslint-disable-line react/no-array-index-key
                className="OrderProducts-product"
                ref={(div) => {
                  this.products[i] = div
                }}
              >
                <OrderProduct
                  {...bagProduct}
                  shouldLinkToPdp={shouldProductsLinkToPdp}
                >
                  {!!CFSiDay && this.getCollectionDetails(CFSiDay)}
                  {!oosOnly && (
                    <p className="OrderProducts-row OrderProducts-productSubtotal">
                      <span className="OrderProducts-label OrderProducts-total">
                        {l`Total`}:
                      </span>
                      <Price
                        className="OrderProducts-price"
                        price={bagProduct.totalPrice || bagProduct.total}
                      />
                    </p>
                  )}
                  {<OrderProductPromo product={bagProduct} />}
                  <div className="OrderProducts-editContainer">
                    {!oosOnly &&
                      canModify &&
                      !bagProduct.editing &&
                      !bagProduct.isDDPProduct && (
                        <button
                          className="OrderProducts-editText"
                          onClick={() => {
                            this.editProduct(i)
                            sendAnalyticsClickEvent({
                              category: GTM_CATEGORY.BAG_DRAWER,
                              action: GTM_ACTION.BAG_DRAW_EDIT,
                              label: bagProduct.productId,
                            })
                          }}
                          role="button"
                        >
                          <span className="OrderProducts-editLabel">
                            {l`Edit`}
                          </span>
                        </button>
                      )}
                    {canModify &&
                      canDeleteItem && (
                        <button
                          className="OrderProducts-deleteText"
                          onClick={this.confirmDeleteFromBag(bagProduct)}
                        >
                          <span className="OrderProducts-deleteLabel">
                            {l`Remove`}
                          </span>
                          <Image
                            alt={l`Remove`}
                            className="OrderProducts-deleteIcon"
                            src={'/assets/{brandName}/images/trashcan-icon.svg'}
                          />
                        </button>
                      )}
                  </div>
                  {isFeatureWishlistEnabled &&
                    allowMoveToWishlist && (
                      <div className="OrderProducts-movetowishlistContainer">
                        <MinibagWishlistButton
                          productId={bagProduct.productId}
                          renderRemoveFromWishlistText={
                            this.renderRemoveFromWishlistText
                          }
                          renderAddToWishlistText={this.renderAddToWishlistText}
                          afterAddToWishlist={this.afterAddToWishlist(
                            bagProduct
                          )}
                          onAuthenticationPreHook={closeMiniBag}
                        />
                      </div>
                    )}
                </OrderProduct>
                {isFeatureEnhancedMessagingEnabled &&
                  isStandardDeliveryOnly && (
                    <OrderProductNotification
                      message={l`Only available using our standard delivery options`}
                    />
                  )}
                {bagProduct.editing ? (
                  <form
                    ref={(form) => {
                      this.editSizeQtyForm = form
                    }}
                    tabIndex="0"
                  >
                    <div className="OrderProducts-formItem">
                      <Select
                        className="OrderProducts-sizes"
                        onChange={(evt) =>
                          this.sizeChangeHandler(evt, i, bagProduct)
                        }
                        options={this.getSizeOptions(bagProduct)}
                        label={l`Size`}
                        name="bagItemSize"
                        value={bagProduct.sizeSelected || bagProduct.size}
                      />
                    </div>
                    <div className="OrderProducts-formItem">
                      <Select
                        className="OrderProducts-quantities"
                        onChange={(evt) => this.quantityChangeHandler(i, evt)}
                        options={this.getQuantityOptions(bagProduct)}
                        label={l`Quantity`}
                        name="bagItemQuantity"
                        value={
                          bagProduct.quantitySelected || bagProduct.quantity
                        }
                      />
                    </div>
                    {/* TODO: THIS MESSAGE BOX HAS BEEN HIDDEN VIA CSS UNTIL THE DESIGNS ARE FINALISED */}
                    {bagProduct.selectedQuantityWasCorrected && (
                      <div className="OrderProducts-infoBox">
                        We have changed the quantity of this item as it is low
                        in stock
                      </div>
                    )}
                    <div className="OrderProducts-inlineButtons">
                      <Button
                        className="OrderProducts-saveButton Button--secondary Button--halfWidth"
                        clickHandler={(evt) => this.saveBag(evt, i)}
                      >
                        {l`Save`}
                      </Button>
                      <Button
                        className="OrderProducts-cancelButton Button--secondary Button--halfWidth"
                        clickHandler={() =>
                          this.updateProduct(i, { editing: false })
                        }
                      >
                        {l`Cancel`}
                      </Button>
                    </div>
                  </form>
                ) : (
                  ''
                )}
              </div>
            )
          })}
        </div>
      </div>
    )
  }
}
