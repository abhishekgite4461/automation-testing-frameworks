import React from 'react'
import PropTypes from 'prop-types'
import * as AccountActions from '../../../actions/common/accountActions'
import { getAllPaymentMethods } from '../../../actions/common/paymentMethodsActions'
import { connect } from 'react-redux'
import { paymentMethod } from '../../../constants/propTypes/paymentMethods'

// Libs
import { pathOr } from 'ramda'

export const mapStateToProps = (state) => ({
  user: state.account.user,
  visited: state.routing.visited,
  paymentMethods: state.paymentMethods,
})

@connect(
  mapStateToProps,
  {
    ...AccountActions,
    getAllPaymentMethods,
  }
)
export default class MyAccountSubcategory extends React.Component {
  static propTypes = {
    children: PropTypes.object,
    user: PropTypes.object,
    getAccount: PropTypes.func,
    visited: PropTypes.array,
    paymentMethods: PropTypes.arrayOf(paymentMethod),
    getAllPaymentMethods: PropTypes.func.isRequired,
  }

  componentDidMount() {
    const {
      visited,
      user,
      getAccount,
      paymentMethods,
      getAllPaymentMethods,
    } = this.props
    const isLoggedIn = pathOr(false, ['exists'], user)
    if (visited.length > 1 && !isLoggedIn) getAccount()
    if (!paymentMethods.length) getAllPaymentMethods()
  }

  static needs = [AccountActions.getAccount, getAllPaymentMethods]

  render() {
    const { children } = this.props
    return <div className="MyAccountSubcategory">{children}</div>
  }
}
