import testComponentHelper from 'test/unit/helpers/test-component'
import { WrappedForgetPassword as ForgetPassword } from '../ForgetPassword'
import Accordion from '../../../common/Accordion/Accordion'

const renderComponent = testComponentHelper(ForgetPassword)

describe('<ForgetPasswordForm />', () => {
  const props = {}

  beforeEach(() => {
    jest.resetAllMocks()
  })

  describe('@render', () => {
    it('renders', () => {
      expect(renderComponent(props).getTree()).toMatchSnapshot()
    })

    it('renders with the provided "className"', () => {
      const { wrapper } = renderComponent({ ...props, className: 'foo' })
      expect(wrapper.prop('className')).toBe('ForgetPassword foo')
    })
  })

  describe('@lifecycle', () => {
    describe('constructor', () => {
      it('should set initial state for accordion to be contracted', () => {
        const { instance } = renderComponent(props)
        expect(instance.state.isAccordionExpanded).toBe(false)
      })
    })
  })

  describe('@events', () => {
    describe('Accordion', () => {
      describe('onAccordionToggle', () => {
        it('should toggle instance state of accordion', () => {
          const { instance, wrapper } = renderComponent(props)
          wrapper.find(Accordion).prop('onAccordionToggle')()
          expect(instance.state.isAccordionExpanded).toBe(true)
          wrapper.find(Accordion).prop('onAccordionToggle')()
          expect(instance.state.isAccordionExpanded).toBe(false)
        })
      })
    })
  })
})
