import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { isEmpty, prop, map, path } from 'ramda'

import { validate } from '../../../lib/validator'

import Button from '../../common/Button/Button'
import Input from '../../common/FormComponents/Input/Input'
import Message from '../../common/FormComponents/Message/Message'

const validationSchema = { email: 'email' }

class ForgetPasswordForm extends Component {
  static propTypes = {
    forgetPasswordForm: PropTypes.object,
    forgetPwdRequest: PropTypes.func,
    isFeatureDesktopResetPasswordEnabled: PropTypes.bool,
    resetForm: PropTypes.func,
    resetPasswordLinkRequest: PropTypes.func,
    setForgetPassword: PropTypes.func,
    setFormField: PropTypes.func,
    setFormMessage: PropTypes.func,
    touchedFormField: PropTypes.func,
    className: PropTypes.string,
  }

  static defaultProps = {
    className: '',
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentWillUnmount() {
    this.resetForm()
  }

  submitHandler = (e) => {
    e.preventDefault()
    const {
      forgetPwdRequest,
      forgetPasswordForm,
      isFeatureDesktopResetPasswordEnabled,
      resetPasswordLinkRequest,
    } = this.props
    const data = { email: forgetPasswordForm.fields.email.value }
    if (isFeatureDesktopResetPasswordEnabled) {
      resetPasswordLinkRequest(data)
    } else {
      forgetPwdRequest(data)
    }
  }

  resetForm = () => {
    this.props.setForgetPassword(false)
    this.props.setFormMessage('forgetPassword', {})
    this.props.resetForm('forgetPassword', {
      email: '',
    })
  }

  render() {
    const { l } = this.context
    const {
      forgetPasswordForm,
      setFormField,
      touchedFormField,
      className,
    } = this.props
    const { email } = forgetPasswordForm.fields

    const setField = (field) => (e) =>
      setFormField('forgetPassword', field, e.target.value)
    const touchedField = (field) => () =>
      touchedFormField('forgetPassword', field)
    const errors = validate(
      validationSchema,
      map(prop('value'), forgetPasswordForm.fields),
      l
    )
    return (
      <form className={className} onSubmit={this.submitHandler}>
        <Input
          field={email}
          name="email"
          type="email"
          errors={errors}
          label={l`Email address`}
          placeholder={l`example@domain.com`}
          setField={setField}
          touchedField={touchedField}
        />
        <Button
          className="ForgetPassword-button"
          type="submit"
          isDisabled={!isEmpty(errors)}
        >
          {l`Reset password`}
        </Button>
        <Message
          message={path(['message', 'message'], forgetPasswordForm)}
          type={path(['message', 'type'], forgetPasswordForm)}
        />
      </form>
    )
  }
}

export default ForgetPasswordForm
