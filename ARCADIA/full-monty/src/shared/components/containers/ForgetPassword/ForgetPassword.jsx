import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import * as featureSelectors from '../../../selectors/featureSelectors'
import * as AccountActions from '../../../actions/common/accountActions'
import * as FormActions from '../../../actions/common/formActions'

import Accordion from '../../common/Accordion/Accordion'
import ForgetPasswordForm from './ForgetPasswordForm'

class ForgetPassword extends Component {
  static propTypes = {
    className: PropTypes.string,
    forgetPasswordForm: PropTypes.object,
    forgetPwdRequest: PropTypes.func,
    isFeatureDesktopResetPasswordEnabled: PropTypes.bool,
    resetForm: PropTypes.func,
    resetPasswordLinkRequest: PropTypes.func,
    setForgetPassword: PropTypes.func,
    setFormField: PropTypes.func,
    setFormMessage: PropTypes.func,
    touchedFormField: PropTypes.func,
  }

  static defaultProps = {
    className: '',
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  state = {
    isAccordionExpanded: false,
  }

  onAccordionToggle = () => {
    this.setState((state) => ({
      isAccordionExpanded: !state.isAccordionExpanded,
    }))
  }

  render() {
    const { l } = this.context
    const { className, ...props } = this.props
    return (
      <section className={`ForgetPassword ${className}`}>
        <Accordion
          expanded={this.state.isAccordionExpanded}
          className="ForgetPassword-accordion"
          header={l`Forgotten your password?`}
          accordionName="forgetPassword"
          onAccordionToggle={this.onAccordionToggle}
          noContentPadding
        >
          <ForgetPasswordForm className={'ForgetPassword-form'} {...props} />
        </Accordion>
      </section>
    )
  }
}

export default connect(
  (state) => ({
    forgetPwd: state.account.forgetPwd,
    forgetPasswordForm: state.forms.forgetPassword,
    isFeatureDesktopResetPasswordEnabled: featureSelectors.isFeatureDesktopResetPasswordEnabled(
      state
    ),
  }),
  { ...AccountActions, ...FormActions }
)(ForgetPassword)

export { ForgetPassword as WrappedForgetPassword }
