import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link, browserHistory } from 'react-router'

import { getRouteFromUrl } from '../../../lib/get-product-route'
import analyticsDecorator from '../../../../client/lib/analytics/analytics-decorator'
import { GTM_CATEGORY } from '../../../analytics'

import * as productsActions from '../../../actions/common/productsActions'
import { toggleModal } from '../../../actions/common/modalActions'

import AccessibleText from '../../common/AccessibleText/AccessibleText'
import AddToBag from '../../common/AddToBag/AddToBag'
import Button from '../../common/Button/Button'
import ProductQuantity from '../../common/ProductQuantity/ProductQuantity'
import ProductSizes from '../../common/ProductSizes/ProductSizes'
import HistoricalPrice from '../../common/HistoricalPrice/HistoricalPrice'
import Loader from '../../common/Loader/Loader'
import RatingImage from '../../common/RatingImage/RatingImage'
import Swatches from '../../common/Swatches/Swatches'
import Message from '../../common/FormComponents/Message/Message'
import ProductMedia from '../../common/ProductMedia/ProductMedia'
import LowStock from '../../common/LowStock/LowStock'
import FitAttributes from '../../common/FitAttributes/FitAttributes'
import WishlistButton from '../../common/WishlistButton/WishlistButton'

import { enableSizeGuideButtonAsSizeTile } from '../../../selectors/brandConfigSelectors'
import { getMaximumNumberOfSizeTiles } from '../../../selectors/configSelectors'
import { isFeatureWishlistEnabled } from '../../../selectors/featureSelectors'

import { heDecode } from '../../../lib/html-entities'

@analyticsDecorator(GTM_CATEGORY.PRODUCT_QUICK_VIEW, {
  isAsync: true,
  suppressPageTypeTracking: true,
})
@connect(
  (state) => ({
    activeItem: state.productDetail.activeItem,
    showItemError: state.productDetail.showError,
    product: state.quickview.product,
    productId: state.quickview.productId,
    pathname: state.routing.location.pathname,
    maximumNumberOfSizeTiles: getMaximumNumberOfSizeTiles(state),
    enableSizeGuideButtonAsSizeTile: enableSizeGuideButtonAsSizeTile(state),
    isFeatureWishlistEnabled: isFeatureWishlistEnabled(state),
  }),
  { ...productsActions, toggleModal }
)
export default class ProductQuickview extends Component {
  static propTypes = {
    activeItem: PropTypes.object.isRequired,
    showItemError: PropTypes.bool.isRequired,
    getProduct: PropTypes.func.isRequired,
    product: PropTypes.object.isRequired,
    productId: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
      .isRequired,
    toggleModal: PropTypes.func.isRequired,
    setProductIdQuickview: PropTypes.func,
    updateShowItemsError: PropTypes.func.isRequired,
    maximumNumberOfSizeTiles: PropTypes.number.isRequired,
    enableSizeGuideButtonAsSizeTile: PropTypes.bool.isRequired,
    updateActiveItem: PropTypes.func.isRequired,
    showQuickviewModal: PropTypes.func.isRequired,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  shouldAddToBag = () => {
    const { activeItem, updateShowItemsError } = this.props
    if (activeItem.sku) return true
    updateShowItemsError()
  }

  getQuickviewProduct = (productId) => {
    const { setProductIdQuickview } = this.props
    setProductIdQuickview(productId)
  }

  componentWillMount() {
    const { getProduct, productId } = this.props
    getProduct({ identifier: productId }, true)
  }

  componentDidMount() {
    this.focus({})
  }

  componentWillReceiveProps(nextProps) {
    const { getProduct, productId } = this.props
    if (productId !== nextProps.productId) {
      getProduct({ identifier: nextProps.productId }, true)
    }
  }

  componentDidUpdate(prevProps) {
    this.focus(prevProps)
  }

  componentWillUnmount() {
    this.props.updateActiveItem({})
  }

  onSwatchSelect = (e, name, selectedId) => {
    e.preventDefault()
    this.props.setProductIdQuickview(selectedId)
  }

  focus({ product: prevProduct = {} }) {
    const { product = {}, productId } = this.props
    if (
      product.productId &&
      product.productId === productId &&
      (!prevProduct.productId || prevProduct.productId !== productId) &&
      this.intro
    ) {
      // If we have a product, and it's the right product, and previously it was not there or was a different product
      // And we have a ref to the title - focus it.
      this.intro.focus()
    }
  }

  checkIfOSS(items) {
    return !items || !items.length || !items.some((item) => item.quantity > 0)
  }

  renderAddToBag = (isOSS) => {
    const { product, activeItem } = this.props
    const { productId } = product
    return !isOSS ? (
      <AddToBag
        key="add-to-bag"
        productId={productId}
        sku={activeItem.sku}
        shouldShowInlineConfirm
        shouldAddToBag={this.shouldAddToBag}
      />
    ) : null
  }

  renderCallToActions = (isOSS) => {
    const { product, isFeatureWishlistEnabled, showQuickviewModal } = this.props
    const { productId } = product

    return (
      <div className={'ProductQuickview-secondaryButtonGroup'}>
        {this.renderAddToBag(isOSS)}
        {isFeatureWishlistEnabled && (
          <WishlistButton
            productId={productId}
            modifier="quickview"
            afterAddToWishlist={showQuickviewModal}
          />
        )}
      </div>
    )
  }

  directToDetails = () => {
    const { toggleModal, product } = this.props
    const { sourceUrl } = product

    toggleModal()
    browserHistory.push(getRouteFromUrl(sourceUrl))
  }

  render() {
    const {
      toggleModal,
      product,
      activeItem,
      showItemError,
      maximumNumberOfSizeTiles,
      enableSizeGuideButtonAsSizeTile,
    } = this.props
    const {
      colourSwatches,
      productId,
      unitPrice,
      wasPrice,
      wasWasPrice,
      rrp,
      name,
      items,
      assets,
      amplienceAssets,
      seoUrl,
      sourceUrl,
      stockThreshold,
      attributes,
      description,
      tpmLinks,
    } = product
    const { l } = this.context

    const ratingValue =
      product.ratingValue ||
      (product.attributes && product.attributes.AverageOverallRating)
    const isOSS = this.checkIfOSS(items)

    const shouldShowSizeDropdown =
      items && items.length && items.length > maximumNumberOfSizeTiles

    // omiting bullet points list in product description - in QA short one is
    const shortDescription =
      description && description.replace(/&lt;ul&gt;.*&lt;\/ul&gt;/g, '')

    if (
      !productId ||
      parseInt(productId, 10) !== parseInt(this.props.productId, 10)
    ) {
      return (
        <div aria-live="assertive">
          <AccessibleText
            data-modal-focus
          >{l`Product information loading`}</AccessibleText>
          <Loader />
        </div>
      )
    }

    const getProductSizesClass = () => {
      if (shouldShowSizeDropdown) {
        return 'ProductSizes--sizeGuideDropdown'
      }
      return enableSizeGuideButtonAsSizeTile
        ? 'ProductSizes--sizeGuideButtonAsSizeTile'
        : 'ProductSizes--sizeGuideBox'
    }

    return (
      <div className="ProductQuickview" key={productId}>
        <AccessibleText
          ref={(intro) => {
            this.intro = intro
          }}
          data-modal-focus
        >{l`This is the quick product view page for: ${name}`}</AccessibleText>
        <div className="ProductQuickview-left">
          <ProductMedia
            name="productQuickview"
            productId={productId}
            assets={assets}
            enableVideo
            amplienceAssets={amplienceAssets}
          />
        </div>
        <div className="ProductQuickview-right">
          <h1 className="ProductQuickview-title">{name}</h1>
          <HistoricalPrice
            className="HistoricalPrice--quickview"
            currency={'\u00A3'}
            price={unitPrice}
            wasPrice={wasPrice}
            wasWasPrice={wasWasPrice}
            rrp={rrp}
          />
          {ratingValue > 0 && (
            <div className="ProductQuickview-ratings">
              <RatingImage
                className="ProductQuickview-ratingsImage"
                rating={ratingValue}
              />
              <Link
                to={`${getRouteFromUrl(sourceUrl)}#BVReviews`}
                onClick={toggleModal}
              >{l`Read reviews`}</Link>
            </div>
          )}
          {tpmLinks &&
            tpmLinks.length > 0 && (
              <FitAttributes
                fitAttributes={tpmLinks[0]}
                isQuickview
                onClick={this.getQuickviewProduct}
              />
            )}
          <Swatches
            swatches={colourSwatches}
            maxSwatches={5}
            productId={productId}
            seoUrl={seoUrl}
            name={name}
            selectedId={productId}
            onSelect={this.onSwatchSelect}
            showAllColours
            pageClass="pdp"
          />
          <div className="ProductQuickview-sizeAndQuantity">
            <ProductSizes
              key="product-sizes"
              label={l`Select size`}
              items={items}
              className={`ProductSizes--pdp ${getProductSizesClass()}`}
              stockThreshold={stockThreshold}
              notifyEmail={
                attributes.NotifyMe === 'Y' ||
                attributes.EmailBackInStock === 'Y'
              }
              activeItem={activeItem}
            />
            <LowStock
              activeItem={activeItem}
              stockThreshold={product.stockThreshold}
            />
            {!isOSS && (
              <ProductQuantity key="product-quantity" activeItem={activeItem} />
            )}
          </div>
          {showItemError && (
            <Message
              key="error-message"
              message={l`Please select your size to continue`}
              type="error"
            />
          )}
          {this.renderCallToActions(isOSS)}
          <div className="ProductQuickview-productDescription">
            <h4 className="ProductQuickview-productDescriptionTitle">{l`Product details`}</h4>
            <div
              className="ProductQuickview-productDescriptionContent"
              // eslint-disable-next-line react/no-danger
              dangerouslySetInnerHTML={{ __html: heDecode(shortDescription) }}
            />
          </div>
          <Button
            className="ProductQuickview-link Button--secondary"
            clickHandler={this.directToDetails}
          >
            {l`See full details`}
          </Button>
        </div>
      </div>
    )
  }
}
