import testComponentHelper from 'test/unit/helpers/test-component'
import Drawer from '../Drawer'

describe('<Drawer></Drawer>', () => {
  const mockProps = {
    isOpen: false,
    children: 'Hello World!',
    isScrollable: false,
  }
  const renderComponent = testComponentHelper(Drawer)

  describe('@render', () => {
    it('is closed', () => {
      expect(renderComponent(mockProps).getTree()).toMatchSnapshot()
    })
    it('is open', () => {
      expect(
        renderComponent({ ...mockProps, isOpen: true }).getTree()
      ).toMatchSnapshot()
    })
    it('is open and scrollable', () => {
      expect(
        renderComponent({
          ...mockProps,
          isOpen: true,
          isScrollable: true,
        }).getTree()
      ).toMatchSnapshot()
    })
    it('is open and not scrollable', () => {
      expect(
        renderComponent({
          ...mockProps,
          isOpen: false,
          isScrollable: false,
        }).getTree()
      ).toMatchSnapshot()
    })
  })

  describe('on componentDidUpdate', () => {
    const add = jest.fn()
    const remove = jest.fn()
    const originalAdd = global.document.body.classList.add
    const originalRemove = global.document.body.classList.remove

    beforeAll(() => {
      global.document.body.classList.add = add
      global.document.body.classList.remove = remove
    })

    beforeEach(() => {
      jest.resetAllMocks()
    })

    afterAll(() => {
      global.document.body.classList.add = originalAdd
      global.document.body.classList.remove = originalRemove
    })

    it('should add not-scrollable class to body', () => {
      const { instance } = renderComponent({ ...mockProps, isOpen: true })
      instance.componentDidUpdate()
      expect(remove).toHaveBeenCalledTimes(0)
      expect(add).toHaveBeenCalledTimes(1)
      expect(add).toHaveBeenCalledWith('not-scrollable')
    })

    it('should remove not-scrollable class from body', () => {
      const { instance } = renderComponent({ ...mockProps, isOpen: false })
      instance.componentDidUpdate()
      expect(add).toHaveBeenCalledTimes(0)
      expect(remove).toHaveBeenCalledTimes(1)
      expect(remove).toHaveBeenCalledWith('not-scrollable')
    })
  })
})
