import PropTypes from 'prop-types'
import React, { Component } from 'react'

export default class Drawer extends Component {
  static defaultProps = {
    isScrollable: false,
  }

  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
    isScrollable: PropTypes.bool,
  }

  componentDidUpdate() {
    if (this.props.isOpen) {
      global.document.body.classList.add('not-scrollable')
    } else {
      global.document.body.classList.remove('not-scrollable')
    }
  }

  render() {
    const { isOpen, isScrollable, children } = this.props
    return (
      <div
        className={`Drawer ${isScrollable ? 'is-scrollable' : ''} ${
          isOpen ? 'is-open' : ''
        }`}
      >
        {children}
      </div>
    )
  }
}
