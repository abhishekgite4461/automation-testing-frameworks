import PropTypes from 'prop-types'
import { path, equals, compose } from 'ramda'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { getProductRouteFromId } from '../../../lib/get-product-route'
import { GTM_CATEGORY } from '../../../../shared/analytics'
import cmsConsts from '../../../constants/cmsConsts'
import espots from '../../../constants/espotsMobile'
import constants from '../../../constants/espotsDesktop'

import {
  closeModal,
  setModalChildren,
  setModalMode,
  showModal,
  toggleModal,
} from '../../../actions/common/modalActions'
import * as FindInStoreActions from '../../../actions/components/FindInStoreActions'
import * as recentlyViewedActions from '../../../actions/common/recentlyViewedActions'
import * as storeLocatorActions from '../../../actions/components/StoreLocatorActions'
import { removeFromVisited } from '../../../actions/common/routingActions'
import { updateShowItemsError } from '../../../actions/common/productsActions'
import { setCarouselIndex } from '../../../actions/common/carousel-actions'

import ProductMedia from '../../common/ProductMedia/ProductMedia'
import FeatureCheck from '../../common/FeatureCheck/FeatureCheck'
import HistoricalPrice from '../../common/HistoricalPrice/HistoricalPrice'
import ProductSizes from '../../common/ProductSizes/ProductSizes'
import FitAttributes from '../../common/FitAttributes/FitAttributes'
import Accordion from '../../common/Accordion/Accordion'
import Swatches from '../../common/Swatches/Swatches'
import Peerius from '../../common/Peerius/Peerius'
import Recommendations from '../../common/Recommendations/Recommendations'
import RecentlyViewed from '../../common/RecentlyViewed/RecentlyViewed'
import ShopTheLook from '../../common/ShopTheLook/ShopTheLook'
import NotifyProduct from '../../common/NotifyProduct/NotifyProduct'
import BazaarVoiceWidget from '../../common/BazaarVoice/BazaarVoiceWidget/BazaarVoiceWidget'
import AddToBag from '../../common/AddToBag/AddToBag'
import BuyNow from '../../common/BuyNow/BuyNow'
import Message from '../../common/FormComponents/Message/Message'
import SizeGuide from '../../common/SizeGuide/SizeGuide'
import ProductQuantity from '../../common/ProductQuantity/ProductQuantity'
import FindInStore from '../../common/FindInStore/FindInStore'
import FindInStoreButton from '../../common/FindInStoreButton/FindInStoreButton'
import SandBox from '../../containers/SandBox/SandBox'
import Espot from '../Espot/Espot'
import ProductDescription from '../../common/ProductDescription/ProductDescription'
import ProductDescriptionExtras from '../../common/ProductDescriptionExtras/ProductDescriptionExtras'
import FulfilmentInfo from '../../common/FulfilmentInfo/FulfilmentInfo'
import ProductCarouselThumbnails from '../ProductCarouselThumbnails/ProductCarouselThumbnails'
import { Column } from '../../common/Grid'
import LowStock from '../../common/LowStock/LowStock'
import WishlistButton from '../../common/WishlistButton/WishlistButton'
import { analyticsPdpClickEvent } from '../../../analytics/tracking/site-interactions'
import analyticsDecorator from '../../../../client/lib/analytics/analytics-decorator'

// Qubit Wrapper
import QubitReact from 'qubit-react/wrapper'

// selectors
import {
  getActiveProductWithInventory,
  checkIfOSS,
} from '../../../selectors/inventorySelectors'
import { isFeatureWishlistEnabled } from '../../../selectors/featureSelectors'
import { isCFSIEspotEnabled } from '../../../selectors/espotSelectors'

import { isMobile } from '../../../selectors/viewportSelectors'
import {
  enableSizeGuideButtonAsSizeTile,
  enableDropdownForLongSizes,
} from '../../../selectors/brandConfigSelectors'
import { getMaximumNumberOfSizeTiles } from '../../../selectors/configSelectors'

class ProductDetail extends Component {
  static propTypes = {
    activeItem: PropTypes.object.isRequired,
    isMobile: PropTypes.bool.isRequired,
    location: PropTypes.object.isRequired,
    product: PropTypes.object.isRequired,
    siteId: PropTypes.number.isRequired,
    activeProductWithInventory: PropTypes.object,
    visitedLength: PropTypes.number,
    showItemError: PropTypes.bool,
    sandboxPages: PropTypes.object,
    region: PropTypes.string.isRequired,
    selectedOosItem: PropTypes.object,
    setModalChildren: PropTypes.func,
    setModalMode: PropTypes.func,
    setStoreStockList: PropTypes.func,
    showModal: PropTypes.func,
    storeListOpen: PropTypes.bool,
    toggleModal: PropTypes.func,
    updateFindInStoreActiveItem: PropTypes.func,
    extractRecentlyDataFromProduct: PropTypes.func,
    isFeatureCarouselThumbnailEnabled: PropTypes.bool.isRequired,
    updateShowItemsError: PropTypes.func.isRequired,
    maximumNumberOfSizeTiles: PropTypes.number.isRequired,
    enableSizeGuideButtonAsSizeTile: PropTypes.bool.isRequired,
    enableDropdownForLongSizes: PropTypes.bool.isRequired,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  shouldAddToBag = () => {
    const { activeItem, updateShowItemsError } = this.props
    if (activeItem && activeItem.sku) return true
    updateShowItemsError()
  }

  constructor(props) {
    super(props)
    this.state = {
      showProductVideo: false,
    }
    // redirecting from bundles - removing extra entry from visited
    this.shouldRemoveIfRedirectionFromBundles = true
  }

  componentDidMount() {
    const { product, extractRecentlyDataFromProduct } = this.props
    extractRecentlyDataFromProduct(product)
  }

  componentWillReceiveProps(nextProps) {
    const {
      activeItem,
      closeModal,
      isMobile,
      updateFindInStoreActiveItem,
    } = this.props

    if (!equals(activeItem, nextProps.activeItem)) {
      updateFindInStoreActiveItem(nextProps.activeItem)
    }

    if (!isMobile && nextProps.isMobile) {
      closeModal()
    }
  }

  componentDidUpdate(prevProps) {
    const {
      product,
      extractRecentlyDataFromProduct,
      visitedLength,
      removeFromVisited,
    } = this.props

    if (!equals(prevProps.product, product)) {
      extractRecentlyDataFromProduct(product)
    }

    // redirecting from bundles - removing extra entry from visited
    if (
      this.shouldRemoveIfRedirectionFromBundles &&
      visitedLength > prevProps.visitedLength
    ) {
      removeFromVisited(visitedLength - 2)
      this.shouldRemoveIfRedirectionFromBundles = false
    }
  }

  clickCarouselThumbs = (index) => {
    const { setCarouselIndex } = this.props
    this.setVideoEnabled(false)
    setCarouselIndex('productDetail', index)
  }

  onSwatchSelect = (e, name, selectedId) => {
    e.preventDefault()
    const {
      location: { pathname },
    } = this.props
    const { l } = this.context

    browserHistory.replace({
      pathname: getProductRouteFromId(pathname, selectedId, l),
    })
  }

  clickFindInStore = (e) => {
    if (e) e.preventDefault()
    const {
      isMobile,
      location,
      setModalChildren,
      setModalMode,
      showModal,
      toggleModal,
      storeListOpen,
      setStoreStockList,
      product,
    } = this.props
    analyticsPdpClickEvent(`findinstore-${product.productId}`)
    if (isMobile) {
      if (storeListOpen) setStoreStockList(!storeListOpen)
      browserHistory.push(`${location.pathname}#`)
      setModalChildren(
        <FindInStore
          className={`ProductDetail-findInStore`}
          product={this.props.product}
          siteId={this.props.siteId}
        />
      )
      setModalMode('rollFull')
      toggleModal()
    } else {
      showModal(
        <FindInStore
          className={`ProductDetail-findInStore`}
          product={this.props.product}
          siteId={this.props.siteId}
        />,
        { mode: 'storeLocator' }
      )
    }
  }

  setVideoEnabled = (enabled = !this.state.showProductVideo) => {
    this.setState({
      showProductVideo: enabled,
    })
  }

  getFindInStoreButton(params) {
    const { l } = this.context
    return (
      <a // eslint-disable-line jsx-a11y/href-no-hash
        className="FulfilmentInfo-findInStoreLink"
        href="#"
        onClick={this.clickFindInStore}
      >
        {l(params.text)}
      </a>
    )
  }

  renderFulfilmentDetails() {
    const { siteId, activeProductWithInventory } = this.props
    if (!activeProductWithInventory) return null
    return (
      <FulfilmentInfo
        siteId={siteId}
        getFindInStoreButton={this.getFindInStoreButton}
        activeProduct={activeProductWithInventory}
      />
    )
  }

  renderMobileSizeGuideButton() {
    const {
      isMobile,
      product: { items, attributes },
    } = this.props
    if (!isMobile) return null
    return <SizeGuide items={items} attributes={attributes} />
  }

  renderSizeGuideButtonAsSizeTile(shouldShowSizeDropdown) {
    const {
      isMobile,
      enableSizeGuideButtonAsSizeTile,
      product: { items, attributes },
    } = this.props
    if (isMobile || !enableSizeGuideButtonAsSizeTile || shouldShowSizeDropdown)
      return null

    return (
      <SizeGuide
        items={items}
        attributes={attributes}
        openDrawer
        displayAsBox
        className="SizeGuide--sizeGuideButtonAsSizeTile"
      />
    )
  }

  hasItemsBiggerSizes = () =>
    this.props.enableDropdownForLongSizes &&
    Boolean(
      this.props.product.items.find(
        ({ size }) => String(size).trim().length > 2
      )
    )

  getDeliveryError = (deliveryEspot) =>
    !deliveryEspot ||
    deliveryEspot.error ||
    !deliveryEspot.pageName ||
    deliveryEspot.pageName.toLowerCase().includes('error')

  isDeliveryEspotEmpty = (deliveryEspot) =>
    !deliveryEspot || !deliveryEspot.pageData || !deliveryEspot.pageData.length

  showSizeDropdown() {
    const {
      maximumNumberOfSizeTiles,
      product: { items },
    } = this.props
    return items.length > maximumNumberOfSizeTiles || this.hasItemsBiggerSizes()
  }

  getProductSizesClass() {
    const { enableSizeGuideButtonAsSizeTile } = this.props
    if (this.showSizeDropdown()) {
      return 'ProductSizes--sizeGuideDropdown'
    }
    return enableSizeGuideButtonAsSizeTile
      ? 'ProductSizes--sizeGuideButtonAsSizeTile'
      : 'ProductSizes--sizeGuideBox'
  }

  renderFitAttributes() {
    const {
      product: { tpmLinks },
    } = this.props
    if (!tpmLinks || tpmLinks.length === 0) return null
    return <FitAttributes fitAttributes={tpmLinks[0]} />
  }

  renderSizeGuideButton(shouldShowSizeDropdown) {
    const {
      isMobile,
      enableSizeGuideButtonAsSizeTile,
      product: { items, attributes },
    } = this.props

    if (
      isMobile ||
      (enableSizeGuideButtonAsSizeTile && !shouldShowSizeDropdown)
    )
      return null

    return (
      <div
        className={`ProductDetail-sizeGuide ${
          shouldShowSizeDropdown ? 'ProductDetail-sizeGuide--narrow' : ''
        }`}
      >
        <SizeGuide
          items={items}
          attributes={attributes}
          openDrawer
          displayAsBox={!shouldShowSizeDropdown}
        />
      </div>
    )
  }

  renderAddtoBagButton(isOSS) {
    if (isOSS) return null

    const {
      activeItem,
      product: { productId, deliveryMessage },
    } = this.props
    return (
      <AddToBag
        productId={productId}
        sku={activeItem.sku}
        deliveryMessage={deliveryMessage}
        shouldShowInlineConfirm
        shouldShowMiniBagConfirm
        shouldAddToBag={this.shouldAddToBag}
      />
    )
  }

  renderBazaarVoiceWidget() {
    const {
      isMobile,
      product: { productId, lineNumber },
    } = this.props

    if (isMobile) return null

    return (
      <BazaarVoiceWidget
        className="ProductDetail-writeReview"
        lineNumber={lineNumber}
        productId={productId}
        summaryOnly
      />
    )
  }

  renderProductQuantity(isOSS) {
    if (isOSS) return null
    const { activeItem } = this.props
    return <ProductQuantity activeItem={activeItem} />
  }

  renderItemError() {
    const { l } = this.context
    const { showItemError } = this.props
    if (!showItemError) return null
    return (
      <Message
        message={l`Please select your size to continue`}
        type={'error'}
      />
    )
  }

  renderFindInStoreButton(type, isOSS) {
    const { CFSi, region } = this.props
    if (CFSi && isOSS) return null
    return (
      <FindInStoreButton
        type={type}
        region={region}
        onClick={this.clickFindInStore}
      />
    )
  }

  renderFulfilmentDetailsDesktop() {
    const { isMobile, isCFSIEspotEnabled } = this.props
    if (isMobile || !isCFSIEspotEnabled) return null
    return this.renderFulfilmentDetails()
  }

  renderFulfilmentDetailsMobile() {
    const { isMobile, isCFSIEspotEnabled } = this.props
    if (!isMobile || !isCFSIEspotEnabled) return null
    return this.renderFulfilmentDetails()
  }

  renderCallToActionButtons(isOSS) {
    const {
      isFeatureWishlistEnabled,
      activeItem,
      region,
      product: { productId, name, items, stockThreshold, attributes },
    } = this.props

    const regionIncludesUK = region.includes('uk')
    const addToBagButton = this.renderAddtoBagButton(isOSS)
    const buyNowButtonClass = `ProductDetail-buyNowButton ${
      !regionIncludesUK ? 'Button--fullWidth' : ''
    }`

    return (
      <div className="ProductDetail-ctas">
        {isFeatureWishlistEnabled ? (
          <div className="ProductDetail-secondaryButtonGroup">
            {addToBagButton}
            <WishlistButton productId={productId} modifier="pdp" />
          </div>
        ) : (
          addToBagButton
        )}
        <NotifyProduct
          productId={productId}
          productTitle={name}
          sizes={items}
          stockThreshold={stockThreshold}
          backInStock={attributes.EmailBackInStock === 'Y'}
          notifyMe={attributes.NotifyMe === 'Y'}
        />
        {this.renderFindInStoreButton('desktop', isOSS)}
        {this.renderFulfilmentDetailsDesktop()}
        <div className="ProductDetail-row">
          <BuyNow
            className={buyNowButtonClass}
            name={name}
            activeItem={activeItem}
            productId={productId}
            hideButton={isOSS}
          />
          {this.renderFindInStoreButton('mobile', isOSS)}
        </div>
      </div>
    )
  }

  renderCmsContent() {
    const { isMobile } = this.props
    if (!isMobile) return null
    return (
      <div className="row">
        <div className="col-12">
          <div className="ProductDetail-cmsContent">
            <SandBox
              cmsPageName={espots.pdp[1]}
              contentType={cmsConsts.ESPOT_CONTENT_TYPE}
              isInPageContent
            />
          </div>
        </div>
      </div>
    )
  }

  renderAccordion() {
    const { l } = this.context
    const { isMobile, sandboxPages } = this.props

    if (!isMobile) return null

    const deliveryEspot = path([espots.pdp[0], 'props', 'data'], sandboxPages)
    const deliveryEspotError = this.getDeliveryError(deliveryEspot)
    const deliveryEspotEmpty = this.isDeliveryEspotEmpty(deliveryEspot)

    return (
      <div
        ref={(accordionParent) => {
          this.accordionParent = accordionParent
        }}
        className="row"
      >
        <div className="col-12">
          <Accordion
            header={l`Delivery and returns Information`}
            className={`ProductDetail-deliveryInfo ${
              deliveryEspotError || deliveryEspotEmpty ? 'is-hidden' : ''
            }`}
            accordionName="deliveryInfo"
            scrollToElement={this.accordionParent}
          >
            <SandBox
              cmsPageName={espots.pdp[0]}
              contentType={cmsConsts.ESPOT_CONTENT_TYPE}
              isInPageContent
            />
          </Accordion>
        </div>
      </div>
    )
  }

  render() {
    const { l } = this.context
    const {
      activeItem,
      selectedOosItem,
      isFeatureCarouselThumbnailEnabled,
      product: {
        productId,
        unitPrice,
        wasPrice,
        wasWasPrice,
        rrp,
        name,
        description,
        assets,
        amplienceAssets = {},
        items,
        stockThreshold,
        lineNumber,
        attributes,
        colourSwatches,
        seoUrl,
        colour,
        bundleDisplayURL,
        shopTheLookProducts,
        seeMoreValue,
      },
    } = this.props

    const isOSS = checkIfOSS(items)

    const referringProduct = {
      productId,
      lineNumber,
      name,
    }
    const shouldShowSizeDropdown = this.showSizeDropdown()

    // TODO refactor and improve the size tiles / size dropdown / size guide code here and make it consistent with quickview and bundle

    const mobileSizeGuideButton = this.renderMobileSizeGuideButton()
    const sizeGuideButtonAsSizeTile = this.renderSizeGuideButtonAsSizeTile(
      shouldShowSizeDropdown
    )

    return (
      <div className="ProductDetail" key={productId}>
        <Peerius type="product" lineNumber={lineNumber} />
        <div className="row ProductDetail-columnContainer">
          <Column
            responsive={`col-md-6 ${
              isFeatureCarouselThumbnailEnabled ? 'col-lg-7' : 'col-lg-6'
            }`}
            className="ProductDetail-mediaColumn"
          >
            <div className="ProductDetails-mediaContainer">
              <div
                className={`Carousel-container ${
                  isFeatureCarouselThumbnailEnabled
                    ? ' Carousel-container--thumbnailEnabled'
                    : ''
                }`}
              >
                <FeatureCheck flag="FEATURE_PRODUCT_CAROUSEL_THUMBNAIL">
                  <ProductCarouselThumbnails
                    maxVisible={5}
                    setCarouselIndex={this.clickCarouselThumbs}
                    className="ProductCarouselThumbnails"
                    amplienceImages={amplienceAssets.images}
                    thumbs={assets.filter(
                      (asset) => asset.assetType === 'IMAGE_SMALL'
                    )}
                  />
                </FeatureCheck>
                <div className="ProductDetail-media">
                  <ProductMedia
                    name="productDetail"
                    productId={productId}
                    amplienceAssets={amplienceAssets}
                    assets={assets}
                    enableVideo
                    enableImageOverlay
                  />
                </div>
              </div>
            </div>
            <Espot
              identifier={constants.product.col1pos1}
              className="Espot-productEspot ProductDetail-espot"
            />
            <Espot
              identifier={constants.product.col1pos2}
              className="Espot-productEspot ProductDetail-espot"
            />
          </Column>
          <Column
            responsive={`col-md-6 ${
              isFeatureCarouselThumbnailEnabled ? 'col-lg-5' : 'col-lg-6'
            }`}
            className="ProductDetail-details"
          >
            <QubitReact id="qubit-pdp-ProductDetail">
              <div>
                <h1 className="ProductDetail-title">{name}</h1>
                <QubitReact id="qubit-pdp-HistoricalPrice">
                  <div className="ProductDetail-priceWrapper">
                    <HistoricalPrice
                      className="HistoricalPrice--pdp"
                      currency={'\u00A3'}
                      price={unitPrice}
                      wasPrice={wasPrice}
                      wasWasPrice={wasWasPrice}
                      rrp={rrp}
                    />
                  </div>
                </QubitReact>
                {this.renderBazaarVoiceWidget()}
                <div className="ProductDetail-topGroupRightInner">
                  <Espot
                    identifier={constants.product.col2pos1}
                    className="Espot-productEspot ProductDetail-espot"
                  />
                  <Swatches
                    swatches={colourSwatches}
                    maxSwatches={5}
                    productId={productId}
                    seoUrl={seoUrl}
                    name={name}
                    selectedId={productId}
                    onSelect={this.onSwatchSelect}
                    showAllColours
                    pageClass="pdp"
                  />
                  {this.renderFitAttributes()}

                  <div className="ProductDetail-sizeAndQuantity">
                    <ProductSizes
                      label={l`Select size`}
                      items={items}
                      stockThreshold={stockThreshold}
                      notifyEmail={
                        attributes.NotifyMe === 'Y' ||
                        attributes.EmailBackInStock === 'Y'
                      }
                      hideIfOSS={attributes.NotifyMe === 'Y'}
                      activeItem={activeItem}
                      selectedOosItem={selectedOosItem}
                      className={`ProductSizes--pdp ${this.getProductSizesClass()}`}
                      sizeGuideButton={sizeGuideButtonAsSizeTile}
                    />
                    {this.renderSizeGuideButton(shouldShowSizeDropdown)}

                    <LowStock
                      activeItem={activeItem}
                      stockThreshold={stockThreshold}
                    />

                    {this.renderProductQuantity(isOSS)}
                  </div>

                  {mobileSizeGuideButton}
                  {this.renderItemError()}
                  {this.renderCallToActionButtons(isOSS)}

                  <Espot
                    identifier={constants.product.col2pos2}
                    className="Espot-productEspot Espot--medium ProductDetail-espot"
                    qubitid="qubit-pdp-EspotCol2Pos2"
                  />
                  <Espot
                    identifier={constants.product.col2pos4}
                    className="Espot-productEspot Espot--medium ProductDetail-espot"
                  />
                </div>
              </div>
            </QubitReact>
          </Column>
        </div>
        <div className="row">
          <div className="col-12">
            <ProductDescription
              description={description}
              seeMoreValue={seeMoreValue}
            >
              <ProductDescriptionExtras
                attributes={[
                  { label: l`Colour`, value: colour },
                  { label: l`Product Code`, value: lineNumber },
                ]}
              />
            </ProductDescription>
          </div>
        </div>
        {this.renderFulfilmentDetailsMobile()}
        <div className="row">
          <div className="col-12">
            <Espot
              identifier={constants.product.content1}
              className="ProductDetail-espot"
            />
          </div>
        </div>
        {this.renderAccordion()}
        {this.renderCmsContent()}
        <div className="row ProductDetail-bazaarVoice ProductDetail-bvRow">
          <div className="col-12">
            <BazaarVoiceWidget
              lineNumber={lineNumber}
              productId={productId}
              containerOnly
            />
          </div>
        </div>

        <FeatureCheck flag="FEATURE_SHOP_THE_LOOK">
          <ShopTheLook
            shopTheLookProducts={shopTheLookProducts}
            bundleURL={bundleDisplayURL}
          />
        </FeatureCheck>
        <Recommendations />
        <RecentlyViewed currentProductId={referringProduct.productId} />
      </div>
    )
  }
}

export default compose(
  analyticsDecorator(GTM_CATEGORY.PDP, { isAsync: true }),
  connect(
    (state) => ({
      activeItem: state.productDetail.activeItem,
      activeProductWithInventory: getActiveProductWithInventory(state),
      showItemError: state.productDetail.showError,
      modal: state.modal,
      siteId: state.config.siteId,
      selectedOosItem: state.productDetail.selectedOosItem,
      storeListOpen: state.findInStore.storeListOpen,
      region: state.config.region,
      sandboxPages: state.sandbox.pages,
      isMobile: isMobile(state),
      CFSi: state.features.status.FEATURE_CFSI,
      isCFSIEspotEnabled: isCFSIEspotEnabled(state),
      isFeatureCarouselThumbnailEnabled: Boolean(
        state.features.status.FEATURE_PRODUCT_CAROUSEL_THUMBNAIL
      ),
      visitedLength: state.routing.visited.length,
      maximumNumberOfSizeTiles: getMaximumNumberOfSizeTiles(state),
      enableSizeGuideButtonAsSizeTile: enableSizeGuideButtonAsSizeTile(state),
      enableDropdownForLongSizes: enableDropdownForLongSizes(state),
      isFeatureWishlistEnabled: isFeatureWishlistEnabled(state),
    }),
    {
      ...storeLocatorActions,
      ...FindInStoreActions,
      ...recentlyViewedActions,
      closeModal,
      setModalChildren,
      setModalMode,
      showModal,
      toggleModal,
      removeFromVisited,
      setCarouselIndex,
      updateShowItemsError,
    }
  )
)(ProductDetail)

export { ProductDetail as WrappedProductDetail }
