import { clone } from 'ramda'
import React from 'react'
import { shallow } from 'enzyme'
import testComponentHelper, {
  until,
} from '../../../../../../test/unit/helpers/test-component'
import productMockData from '../../../../../../test/mocks/product-detail'
import { forAnalyticsDecorator as createMockStoreForAnalytics } from 'test/unit/helpers/mock-store'

import ProductDetail, { WrappedProductDetail } from '../ProductDetail'
import ProductDescription from '../../../common/ProductDescription/ProductDescription'

import ProductSizes from '../../../common/ProductSizes/ProductSizes'
import SizeGuide from '../../../common/SizeGuide/SizeGuide'
import AddToBag from '../../../common/AddToBag/AddToBag'
import FitAttributes from '../../../common/FitAttributes/FitAttributes'
import Message from '../../../common/FormComponents/Message/Message'
import ProductCarouselThumbnails from '../../ProductCarouselThumbnails/ProductCarouselThumbnails'
import ProductMedia from '../../../common/ProductMedia/ProductMedia'
import { analyticsPdpClickEvent } from '../../../../analytics/tracking/site-interactions'

const productMock = {
  ...productMockData,
  amplienceAssets: {
    images: ['http://www.images.com/root'],
    video: 'some video',
  },
}

jest.mock('../../../../analytics/tracking/site-interactions', () => ({
  analyticsPdpClickEvent: jest.fn(),
}))

jest.mock('react-router', () => ({
  browserHistory: {
    push: jest.fn(),
  },
}))
jest.mock('../../../../lib/product-utilities', () => ({
  getMatchingAttribute: jest.fn(),
}))

import FindInStore from '../../../common/FindInStore/FindInStore'

jest.mock('../../../../lib/get-product-route', () => ({
  getProductRouteFromId: jest.fn(),
}))
jest.mock('react-router', () => ({
  browserHistory: {
    replace: jest.fn(),
    push: jest.fn(),
  },
}))

import { getProductRouteFromId } from '../../../../lib/get-product-route'
import { browserHistory } from 'react-router'
import WishlistButton from '../../../common/WishlistButton/WishlistButton'

describe('</ProductDetail>', () => {
  const renderComponent = testComponentHelper(WrappedProductDetail)

  const initialProps = {
    maximumNumberOfSizeTiles: 8,
    siteId: 12556,
    product: productMock,
    activeItem: productMock.items[0],
    location: { pathname: '...' },
    region: 'uk',
    visited: [],
    sandboxPages: {
      mobileESpotPDPPos1: {},
    },
    enableDropdownForLongSizes: false,
    enableSizeGuideButtonAsSizeTile: false,
    isFeatureWishlistEnabled: false,
    isFeatureCarouselThumbnailEnabled: false,
    isMobile: true,
    showItemError: false,
    scrollToTopFeature: false,
    analyticsProductDetail: jest.fn(),
    updateFindInStoreActiveItem: jest.fn(),
    closeModal: jest.fn(),
    showModal: jest.fn(),
    setModalChildren: jest.fn(),
    toggleModal: jest.fn(),
    setModalMode: jest.fn(),
    deleteRecentlyViewedProduct: jest.fn(),
    setStoreStockList: jest.fn(),
    extractRecentlyDataFromProduct: jest.fn(),
    updateShowItemsError: jest.fn(),
  }

  const itemLongSize = {
    sku: '609',
    size: 'Size 6 years',
    quantity: 1,
  }

  const e = {
    preventDefault: () => {},
  }
  // TODO Add missing test cases for snapshots
  describe('@renders', () => {
    it('should have all required components / elements, should have code display the correct text, should have the right information, should tracks data with peerius', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })

    it('should render desktop when isMobile is false', () => {
      expect(
        renderComponent({
          ...initialProps,
          isMobile: false,
        }).getTree()
      ).toMatchSnapshot()
    })

    it('should pass data to ProductSizes when selectedOosItem is defined', () => {
      expect(
        renderComponent({
          ...initialProps,
          selectedOosItem: {},
        }).getTree()
      ).toMatchSnapshot()
    })

    describe('AmplienceAssets', () => {
      it('should pass amplience images to ProductCarouselThumbnails', () => {
        const { wrapper } = renderComponent(initialProps)
        expect(
          wrapper.find(ProductCarouselThumbnails).prop('amplienceImages')
        ).toBe(productMock.amplienceAssets.images)
      })

      it('should pass amplience urls to ProductMedia', () => {
        const { wrapper } = renderComponent(initialProps)
        expect(wrapper.find(ProductMedia).prop('amplienceAssets')).toBe(
          productMock.amplienceAssets
        )
      })
    })

    describe('<ProductDescription />', () => {
      it('should pass seeMoreValue to ProductDescription', () => {
        const seeMoreValue = [
          {
            seeMoreLabel: 'Jeans',
          },
          {
            seeMoreLabel: 'Spray On Jeans',
            seeMoreUrl: '/_/N-2bu3Z1xjf',
          },
        ]
        const { wrapper } = renderComponent({
          ...initialProps,
          product: {
            ...productMock,
            seeMoreValue,
          },
        })
        expect(wrapper.find(ProductDescription).prop('seeMoreValue')).toEqual(
          seeMoreValue
        )
      })
    })

    describe('SizeBlock', () => {
      it('should show with correct props when isMobile is true and number of size tiles less than maximum', () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          isMobile: true,
          maximumNumberOfSizeTiles: 8,
        })

        expect(wrapper.find(ProductSizes).prop('className')).toBe(
          'ProductSizes--pdp ProductSizes--sizeGuideBox'
        )

        expect(
          wrapper
            .find(SizeGuide)
            .at(0)
            .prop('displayAsBox')
        ).toBeFalsy()
        expect(
          wrapper
            .find(SizeGuide)
            .at(1)
            .prop('displayAsBox')
        ).toBeFalsy()
      })

      it('should show with correct props when isMobile is true and number of size tiles greater than maximum', () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          isMobile: true,
          maximumNumberOfSizeTiles: 2,
        })

        expect(wrapper.find(ProductSizes).prop('className')).toBe(
          'ProductSizes--pdp ProductSizes--sizeGuideDropdown'
        )

        expect(wrapper.find(SizeGuide).prop('displayAsBox')).toBeFalsy()
      })
      it('should show with correct props when desktop and number of size tiles less than maximum', () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          isMobile: false,
          maximumNumberOfSizeTiles: 8,
        })

        expect(wrapper.find(ProductSizes).prop('className')).toBe(
          'ProductSizes--pdp ProductSizes--sizeGuideBox'
        )

        expect(wrapper.find(SizeGuide).prop('displayAsBox')).toBeTruthy()
      })
      it('should show with correct props when desktop and number of size tiles greater than maximum', () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          isMobile: false,
          maximumNumberOfSizeTiles: 2,
        })

        expect(wrapper.find(ProductSizes).prop('className')).toBe(
          'ProductSizes--pdp ProductSizes--sizeGuideDropdown'
        )

        expect(
          wrapper
            .find(SizeGuide)
            .at(0)
            .prop('displayAsBox')
        ).toBeFalsy()
        expect(
          wrapper
            .find(SizeGuide)
            .at(1)
            .prop('displayAsBox')
        ).toBeFalsy()
      })
    })

    it('should add a class to the carousel container when the carousel thumbnail feature is enabled', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        isFeatureCarouselThumbnailEnabled: true,
      })

      expect(
        wrapper
          .find('.Carousel-container')
          .hasClass('Carousel-container--thumbnailEnabled')
      ).toEqual(true)
    })

    it('should not add a class to the carousel container when the carousel thumbnail feature is disabled', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        isFeatureCarouselThumbnailEnabled: false,
      })

      expect(
        wrapper
          .find('.Carousel-container')
          .hasClass('Carousel-container--thumbnailEnabled')
      ).toEqual(false)
    })

    it('should hide the buy now / and add to bag buttons when it is out of stock', () => {
      expect(
        renderComponent({
          ...initialProps,
          product: {
            ...productMock,
            items: productMock.items.filter((i) => i.quantity === 0),
          },
          selectedOosItem: {},
        }).getTree()
      ).toMatchSnapshot()
    })

    it('should hide the size guide when there is only one size available', () => {
      const items = [
        {
          sku: '602016000925081',
          size: 'ONE',
          quantity: 10,
          selected: false,
          wcsSizeADValueId: '10',
        },
      ]
      expect(
        renderComponent({
          ...initialProps,
          product: {
            ...productMock,
            items,
          },
          selectedOosItem: {},
        }).getTree()
      ).toMatchSnapshot()
    })

    it('with valid sandboxPages ', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        sandboxPages: {
          mobilePDPESpotPos1: {
            props: {
              data: {
                pageName: 'fake-espot',
                pageData: ['espot'],
              },
            },
          },
        },
      })
      expect(
        wrapper.find('.ProductDetail-deliveryInfo').prop('className')
      ).toBe('ProductDetail-deliveryInfo ')
    })
    it('with sandboxPages having no mobilePDPESpotPos1 ', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        sandboxPages: {},
      })
      expect(
        wrapper.find('.ProductDetail-deliveryInfo').prop('className')
      ).toBe('ProductDetail-deliveryInfo is-hidden')
    })
    it('with sandboxPages having mobilePDPESpotPos1 as error ', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        sandboxPages: {
          mobilePDPESpotPos1: {
            props: {
              data: {
                error: true,
              },
            },
          },
        },
      })
      expect(
        wrapper.find('.ProductDetail-deliveryInfo').prop('className')
      ).toBe('ProductDetail-deliveryInfo is-hidden')
    })
    it('with sandboxPages having mobilePDPESpotPos1 with no pageName ', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        sandboxPages: {
          mobilePDPESpotPos1: {
            props: {
              data: {
                pageData: ['espot'],
              },
            },
          },
        },
      })
      expect(
        wrapper.find('.ProductDetail-deliveryInfo').prop('className')
      ).toBe('ProductDetail-deliveryInfo is-hidden')
    })
    it('with sandboxPages having mobilePDPESpotPos1 with pageName having error ', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        sandboxPages: {
          mobilePDPESpotPos1: {
            props: {
              data: {
                pageName: 'is an error',
                pageData: ['espot'],
              },
            },
          },
        },
      })
      expect(
        wrapper.find('.ProductDetail-deliveryInfo').prop('className')
      ).toBe('ProductDetail-deliveryInfo is-hidden')
    })
    it('with sandboxPages having mobilePDPESpotPos1 with pageData missing ', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        sandboxPages: {
          mobilePDPESpotPos1: {
            props: {
              data: {
                pageName: 'fake-espot',
                pageData: undefined,
              },
            },
          },
        },
      })
      expect(
        wrapper.find('.ProductDetail-deliveryInfo').prop('className')
      ).toBe('ProductDetail-deliveryInfo is-hidden')
    })
    it('with sandboxPages having mobilePDPESpotPos1 with pageData empty array ', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        sandboxPages: {
          mobilePDPESpotPos1: {
            props: {
              data: {
                pageName: 'fake-espot',
                pageData: [],
              },
            },
          },
        },
      })
      expect(
        wrapper.find('.ProductDetail-deliveryInfo').prop('className')
      ).toBe('ProductDetail-deliveryInfo is-hidden')
    })
    describe('with activeProductWithInventory', () => {
      it('shows FulfilmentInfo when in mobile and isCFSIEspotEnabled is true', () => {
        const { getTree } = renderComponent({
          ...initialProps,
          isCFSIEspotEnabled: true,
          isMobile: true,
          activeProductWithInventory: { sku: '602016000925078' },
        })
        expect(getTree()).toMatchSnapshot()
      })
      it('shows FulfilmentInfo when in desktop and isCFSIEspotEnabled is true', () => {
        const { getTree } = renderComponent({
          ...initialProps,
          isCFSIEspotEnabled: true,
          activeProductWithInventory: { sku: '602016000925078' },
        })
        expect(getTree()).toMatchSnapshot()
      })
    })

    it('should not render `AddToBag` if none of the product‘s items have a quantity', () => {
      const props = clone(initialProps)
      props.product.items = [{ quantity: 0 }, { quantity: 0 }]
      const { wrapper } = renderComponent(props)
      expect(wrapper.find(AddToBag).exists()).toBeFalsy()
    })

    it('should render a Wishlist icon when the wishlist feature flag is enabled', () => {
      const props = {
        ...initialProps,
        isFeatureWishlistEnabled: true,
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find(WishlistButton).length).toBe(1)
    })

    it('should pass `product.deliveryMessage` prop to <AddToBag >', () => {
      const deliveryMessage = 'Order in 9 hrs 23 mins for next day delivery'
      const product = {
        ...initialProps.product,
        deliveryMessage,
      }
      const { wrapper } = renderComponent({
        ...initialProps,
        product,
      })
      expect(wrapper.find(AddToBag).prop('deliveryMessage')).toBe(
        deliveryMessage
      )
    })
    it('`FitAttributes` should have correct `fitAttributes` prop', () => {
      const { wrapper } = renderComponent(initialProps)
      expect(wrapper.find(FitAttributes).prop('fitAttributes')).toBe(
        initialProps.product.tpmLinks[0]
      )
    })
    it('should render error message', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        showItemError: true,
      })

      const messageNode = wrapper.find(Message)
      expect(messageNode.exists()).toBeTruthy()
      expect(messageNode.prop('type')).toBe('error')
    })
  })

  describe('@lifeCycles', () => {
    describe('constructor', () => {
      it('should set initial values', () => {
        const visitedLength = 2
        const { instance } = renderComponent({ ...initialProps, visitedLength })
        expect(instance.shouldRemoveIfRedirectionFromBundles).toBe(true)
      })
    })
    describe('componentDidMount', () => {
      it('should call extractRecentlyDataFromProduct', () => {
        const { instance } = renderComponent(initialProps)
        expect(
          instance.props.extractRecentlyDataFromProduct
        ).not.toHaveBeenCalled()
        instance.componentDidMount()
        expect(
          instance.props.extractRecentlyDataFromProduct
        ).toHaveBeenCalledTimes(1)
        expect(
          instance.props.extractRecentlyDataFromProduct
        ).toHaveBeenLastCalledWith(instance.props.product)
      })
    })

    describe('componentWillReceiveProps', () => {
      it('should not call updateFindInStoreActiveItem if activeItem has not changed', () => {
        const { instance } = renderComponent(initialProps)
        instance.componentWillReceiveProps(instance.props)
        expect(
          instance.props.updateFindInStoreActiveItem
        ).not.toHaveBeenCalled()
      })

      it('should call updateFindInStoreActiveItem activeItem prop has changed', () => {
        const { instance } = renderComponent(initialProps)
        expect(
          instance.props.updateFindInStoreActiveItem
        ).not.toHaveBeenCalled()
        instance.componentWillReceiveProps({
          ...instance.props,
          activeItem: { sku: 'new' },
        })
        expect(
          instance.props.updateFindInStoreActiveItem
        ).toHaveBeenCalledTimes(1)
      })

      it('should call closeModal in case we decrease the size of the window to make isMobile true', () => {
        const renderedComponent = renderComponent({
          ...initialProps,
          closeModal: jest.fn(),
          isMobile: false,
        })
        const { instance } = renderedComponent
        expect(instance.props.closeModal).not.toHaveBeenCalled()
        instance.componentWillReceiveProps({
          isMobile: true,
        })
        expect(instance.props.closeModal).toHaveBeenCalledTimes(1)
      })
    })

    describe('componentDidUpdate', () => {
      it('should call extractRecentlyDataFromProduct', () => {
        jest.clearAllMocks()
        const { instance } = renderComponent(initialProps)
        expect(
          instance.props.extractRecentlyDataFromProduct
        ).not.toHaveBeenCalled()
        instance.componentDidUpdate({ visitedLength: 3 })
        expect(
          instance.props.extractRecentlyDataFromProduct
        ).toHaveBeenCalledTimes(1)
        expect(
          instance.props.extractRecentlyDataFromProduct
        ).toHaveBeenLastCalledWith(instance.props.product)
      })
      it('should remove extra entry in visited if reditection from bundles', () => {
        const visitedLength = 3
        const removeFromVisited = jest.fn()
        const { instance } = renderComponent({
          ...initialProps,
          visitedLength,
          removeFromVisited,
        })
        expect(instance.props.removeFromVisited).not.toHaveBeenCalled()
        const prevProps = {
          ...initialProps,
          visitedLength: 2,
          removeFromVisited,
        }
        instance.componentDidUpdate(prevProps)
        expect(instance.props.removeFromVisited).toHaveBeenCalledTimes(1)
        expect(instance.props.removeFromVisited).toHaveBeenCalledWith(1)
        expect(instance.shouldRemoveIfRedirectionFromBundles).toBe(false)
      })
      it('should not remove extra entry in visited if not reditection from bundles', () => {
        const visitedLength = 2
        const removeFromVisited = jest.fn()
        const { instance } = renderComponent({
          ...initialProps,
          visitedLength,
          removeFromVisited,
        })
        instance.shouldRemoveIfRedirectionFromBundles = false
        expect(instance.props.removeFromVisited).not.toHaveBeenCalled()
        const prevProps = {
          ...initialProps,
          visitedLength: 2,
          removeFromVisited,
        }
        instance.componentDidUpdate(prevProps)
        expect(instance.props.removeFromVisited).not.toHaveBeenCalled()
        expect(instance.shouldRemoveIfRedirectionFromBundles).toBe(false)
      })
    })
  })

  describe('@functions', () => {
    beforeEach(() => {
      jest.clearAllMocks()
    })

    describe('clickCarouselThumbs', () => {
      it('should set video enabled to false, and change the carousel index to the one given', () => {
        const setVideoEnabled = jest.fn()
        const setCarouselIndex = jest.fn()
        const props = { ...initialProps, setCarouselIndex }
        const wrapper = shallow(<WrappedProductDetail {...props} />, {
          context: { l: jest.fn() },
        })
        wrapper.instance().setVideoEnabled = setVideoEnabled

        wrapper.instance().clickCarouselThumbs(321)

        expect(setVideoEnabled).toHaveBeenCalledWith(false)
        expect(setCarouselIndex).toHaveBeenCalledWith('productDetail', 321)
      })
    })

    it('@clickFindInStore for mobile', () => {
      const isMobile = true
      const location = {
        pathname: 'prod',
      }
      const storeListOpen = true

      const { instance } = renderComponent({
        ...initialProps,
        isMobile,
        location,
        storeListOpen,
      })
      expect(instance.props.setStoreStockList).toHaveBeenCalledTimes(0)
      expect(instance.props.setModalChildren).toHaveBeenCalledTimes(0)
      expect(instance.props.setModalMode).toHaveBeenCalledTimes(0)
      expect(instance.props.toggleModal).toHaveBeenCalledTimes(0)
      expect(instance.props.showModal).toHaveBeenCalledTimes(0)
      instance.clickFindInStore(e)
      expect(instance.props.setStoreStockList).toHaveBeenCalledTimes(1)
      expect(instance.props.setModalChildren).toHaveBeenCalledTimes(1)
      expect(instance.props.setModalMode).toHaveBeenCalledTimes(1)
      expect(instance.props.toggleModal).toHaveBeenCalledTimes(1)
      expect(instance.props.showModal).toHaveBeenCalledTimes(0)
      expect(analyticsPdpClickEvent).toHaveBeenCalledWith(
        `findinstore-${instance.props.product.productId}`
      )
    })
    it('@clickFindInStore for desktop', () => {
      const isMobile = false
      const location = {
        pathname: 'prod',
      }
      const storeListOpen = true

      const { instance } = renderComponent({
        ...initialProps,
        isMobile,
        location,
        storeListOpen,
      })
      expect(instance.props.setStoreStockList).toHaveBeenCalledTimes(0)
      expect(instance.props.setModalChildren).toHaveBeenCalledTimes(0)
      expect(instance.props.setModalMode).toHaveBeenCalledTimes(0)
      expect(instance.props.toggleModal).toHaveBeenCalledTimes(0)
      expect(instance.props.showModal).toHaveBeenCalledTimes(0)
      instance.clickFindInStore(e)
      expect(instance.props.setStoreStockList).toHaveBeenCalledTimes(0)
      expect(instance.props.setModalChildren).toHaveBeenCalledTimes(0)
      expect(instance.props.setModalMode).toHaveBeenCalledTimes(0)
      expect(instance.props.toggleModal).toHaveBeenCalledTimes(0)
      expect(instance.props.showModal).toHaveBeenCalledTimes(1)
      expect(analyticsPdpClickEvent).toHaveBeenCalledWith(
        `findinstore-${instance.props.product.productId}`
      )
    })
    it('@getFindInStoreButton to link', () => {
      const { instance } = renderComponent(initialProps)
      const params = {
        type: 'link',
      }
      instance.getFindInStoreButton(params)
      expect(instance.getFindInStoreButton(params)).toMatchSnapshot()
    })
    describe('@hasItemsBiggerSizes', () => {
      it('should return true when item size has more than 2 chars and enableDropdownForLongSizes is true', () => {
        const { instance } = renderComponent({
          ...initialProps,
          enableDropdownForLongSizes: true,
          product: {
            ...initialProps.product,
            items: [itemLongSize],
          },
        })
        expect(instance.hasItemsBiggerSizes()).toBe(true)
      })
      it('should return false when item size has more than 2 chars and enableDropdownForLongSizes is false', () => {
        const { instance } = renderComponent({
          ...initialProps,
          enableDropdownForLongSizes: false,
          product: {
            ...initialProps.product,
            items: [itemLongSize],
          },
        })
        expect(instance.hasItemsBiggerSizes()).toBe(false)
      })
      it('should return false when item size has less than 2  and enableDropdownForLongSizes is true', () => {
        const { instance } = renderComponent({
          ...initialProps,
          enableDropdownForLongSizes: true,
        })
        expect(instance.hasItemsBiggerSizes()).toBe(false)
      })
    })
    it('@renderFindInStoreButton to button', () => {
      const { instance } = renderComponent(initialProps)
      const params = {
        type: 'button',
      }
      expect(instance.renderFindInStoreButton(params)).toMatchSnapshot()
    })
    describe('@setVideoEnabled', () => {
      const { instance } = renderComponent(initialProps)
      it('Default', () => {
        expect(instance.state.showProductVideo).toBe(false)
      })
      it('With argument set', () => {
        instance.setVideoEnabled(true)
        expect(instance.state.showProductVideo).toBe(true)
        instance.setVideoEnabled(false)
        expect(instance.state.showProductVideo).toBe(false)
      })
      it('No argument, toggling', () => {
        instance.setVideoEnabled()
        expect(instance.state.showProductVideo).toBe(true)
        instance.setVideoEnabled()
        expect(instance.state.showProductVideo).toBe(false)
      })
    })
    describe('@renderFulfilmentDetails', () => {
      it('should not render FulfilmentInfo if activeProductWithInventory is null', () => {
        const { instance } = renderComponent({ ...initialProps })

        expect(instance.renderFulfilmentDetails()).toMatchSnapshot()
      })
      it('should render FulfilmentInfo', () => {
        const { instance } = renderComponent({
          ...initialProps,
          activeProductWithInventory: { sku: '602016000925078' },
        })
        expect(instance.renderFulfilmentDetails()).toMatchSnapshot()
      })
    })
  })

  describe('@events', () => {
    describe('onSelect swatches', () => {
      const renderedComponent = renderComponent(initialProps)
      const name = 'fake-name'
      const selectedId = 'fake-id'
      const event = {
        preventDefault: jest.fn(),
      }
      beforeAll(() => {
        renderedComponent.wrapper
          .find('Swatches')
          .simulate('select', event, name, selectedId)
      })
      it('should call e.preventDefault()', () => {
        expect(event.preventDefault).toHaveBeenCalledTimes(1)
      })
      it('should call browserHistory.replace', () => {
        expect(browserHistory.replace).toHaveBeenCalledTimes(1)
      })
      it('should call getProductRouteFromId', () => {
        const { instance } = renderedComponent
        expect(getProductRouteFromId).toHaveBeenCalledTimes(1)
        expect(getProductRouteFromId).toHaveBeenLastCalledWith(
          instance.props.location.pathname,
          selectedId,
          instance.context.l
        )
      })
    })
    describe('clickHandler findInStoreButton', () => {
      describe('on mobile', () => {
        let renderedComponent
        beforeAll(() => {
          renderedComponent = renderComponent({
            ...initialProps,
            isMobile: true,
            region: 'uk',
          })
          renderedComponent.wrapper
            .find('FindInStoreButton [type="mobile"]')
            .props()
            .onClick()
        })
        it('should call setModalChildren', () => {
          const { instance } = renderedComponent
          expect(instance.props.setModalChildren).toHaveBeenCalledTimes(1)
          expect(instance.props.setModalChildren).toHaveBeenLastCalledWith(
            <FindInStore
              className="ProductDetail-findInStore"
              product={instance.props.product}
              siteId={instance.props.siteId}
            />
          )
        })
        it('should call setModalMode', () => {
          const { instance } = renderedComponent
          expect(instance.props.setModalMode).toHaveBeenCalledTimes(1)
          expect(instance.props.setModalMode).toHaveBeenLastCalledWith(
            'rollFull'
          )
        })
        it('should call toggleModal', () => {
          const { instance } = renderedComponent
          expect(instance.props.toggleModal).toHaveBeenCalledTimes(1)
        })
        it('should not call setStoreStockList if storeListOpen is false', () => {
          const { instance } = renderedComponent
          expect(instance.props.setStoreStockList).not.toHaveBeenCalled()
        })
        it('should call browserHistory.push', () => {
          const { instance } = renderedComponent
          expect(browserHistory.push).toHaveBeenCalledTimes(1)
          expect(browserHistory.push).toHaveBeenLastCalledWith(
            `${instance.props.location.pathname}#`
          )
        })
        it('should call setStoreStockList if storeListOpen is true', () => {
          const { instance, wrapper } = renderComponent({
            ...initialProps,
            isMobile: true,
            region: 'uk',
            storeListOpen: true,
          })
          wrapper
            .find('FindInStoreButton [type="mobile"]')
            .props()
            .onClick()
          expect(instance.props.setStoreStockList).toHaveBeenCalledTimes(1)
          expect(instance.props.setStoreStockList).toHaveBeenLastCalledWith(
            !instance.props.setStoreStockList
          )
        })
      })

      describe('on desktop', () => {
        let renderedComponent
        beforeAll(() => {
          renderedComponent = renderComponent({
            ...initialProps,
            isMobile: false,
            region: 'uk',
          })
          renderedComponent.wrapper
            .find('FindInStoreButton [type="mobile"]')
            .props()
            .onClick()
        })
        it('should call showModal', () => {
          const { instance } = renderedComponent
          expect(instance.props.showModal).toHaveBeenCalledTimes(1)
          expect(instance.props.showModal).toHaveBeenLastCalledWith(
            <FindInStore
              className="ProductDetail-findInStore"
              product={instance.props.product}
              siteId={instance.props.siteId}
            />,
            { mode: 'storeLocator' }
          )
        })
      })
    })

    describe('on shouldAddToBag', () => {
      it('should return `true` if there is an `activeItem` and it has an `sku`', () => {
        const { wrapper } = renderComponent(initialProps)
        expect(wrapper.find(AddToBag).prop('shouldAddToBag')()).toBe(true)
      })

      it('should call `updateShowItemsError` if there isn‘t an `activeItem`', () => {
        const updateShowItemsErrorMock = jest.fn()
        const { wrapper } = renderComponent({
          ...initialProps,
          activeItem: {},
          updateShowItemsError: updateShowItemsErrorMock,
        })
        wrapper.find(AddToBag).prop('shouldAddToBag')()
        expect(updateShowItemsErrorMock).toHaveBeenCalled()
      })
    })
  })

  describe('@decorator', () => {
    describe('analytics decorator', () => {
      it('wraps the component with the correct analytics property', () => {
        const mockStore = createMockStoreForAnalytics({ preloadedState: {} })
        const shallowOptions = { context: { store: mockStore } }

        const wrapper = shallow(<ProductDetail />, shallowOptions)

        expect(ProductDetail.displayName).toMatch(/AnalyticsDecorator/)
        const analyticsDecorator = until(
          wrapper,
          'AnalyticsDecorator',
          shallowOptions
        )
        const analyticsInstance = analyticsDecorator.instance()
        expect(analyticsInstance.pageType).toBe('pdp')
        expect(analyticsInstance.isAsync).toBe(true)
      })
    })
  })
})
