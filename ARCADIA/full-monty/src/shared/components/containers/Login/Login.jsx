import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { validate } from '../../../lib/validator'
import {
  sendAnalyticsClickEvent,
  sendAnalyticsErrorMessage,
  ANALYTICS_ERROR,
  GTM_CATEGORY,
  GTM_ACTION,
} from '../../../analytics'
import {
  setFormField,
  touchedFormField,
  resetForm,
  setFormMessage,
  touchedMultipleFormFields,
  focusedFormField,
} from '../../../actions/common/formActions'
import { loginRequest } from '../../../actions/common/authActions'
import Button from '../../common/Button/Button'
import Input from '../../common/FormComponents/Input/Input'
import Message from '../../common/FormComponents/Message/Message'
import { prop, mapObjIndexed, path, isEmpty } from 'ramda'

@connect(
  (state) => ({
    loginForm: state.forms.login,
  }),
  {
    setFormField,
    touchedFormField,
    resetForm,
    sendAnalyticsClickEvent,
    sendAnalyticsErrorMessage,
    setFormMessage,
    touchedMultipleFormFields,
    focusedFormField,
    loginUser: loginRequest,
  }
)
export default class Login extends Component {
  static propTypes = {
    loginForm: PropTypes.object.isRequired,
    loginUser: PropTypes.func.isRequired,
    setFormField: PropTypes.func.isRequired,
    touchedFormField: PropTypes.func.isRequired,
    resetForm: PropTypes.func.isRequired,
    setFormMessage: PropTypes.func.isRequired,
    touchedMultipleFormFields: PropTypes.func.isRequired,
    focusedFormField: PropTypes.func.isRequired,
    getNextRoute: PropTypes.func,
    className: PropTypes.string,
    successCallback: PropTypes.func,
    errorCallback: PropTypes.func,
    formName: PropTypes.string,
  }

  static defaultProps = {
    className: '',
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentDidUpdate(prevProps) {
    const { focusedFormField, loginForm } = this.props
    const { isFocused } = loginForm.fields.password
    const wasFocused = prevProps.loginForm.fields.password.isFocused
    if (isFocused && wasFocused !== isFocused) {
      focusedFormField('login', 'password', false)
    }
  }

  componentWillUnmount() {
    this.resetForm()
  }

  onSubmit = (e) => {
    e.preventDefault()
    const {
      touchedMultipleFormFields,
      loginUser,
      getNextRoute = () => '/my-account',
      loginForm,
      successCallback,
      errorCallback,
      formName,
      sendAnalyticsClickEvent,
      sendAnalyticsErrorMessage,
    } = this.props
    touchedMultipleFormFields('login', Object.keys(loginForm.fields))
    if (Object.keys(this.getErrors()).length) {
      sendAnalyticsErrorMessage(ANALYTICS_ERROR.LOGIN_FAILED)
      return
    }

    sendAnalyticsClickEvent({
      category: GTM_CATEGORY.SIGN_IN,
      action: GTM_ACTION.SUBMIT,
      label: 'sign-in-button',
    })

    const { email, password } = loginForm.fields
    loginUser({
      credentials: {
        username: email.value,
        password: password.value,
      },
      getNextRoute,
      successCallback,
      errorCallback,
      formName,
    })
  }

  getErrors = () => {
    const validationSchema = { email: 'email', password: 'password' }
    return validate(
      validationSchema,
      mapObjIndexed(prop('value'), this.props.loginForm.fields),
      this.context.l
    )
  }

  resetForm = () => {
    const { resetForm, setFormMessage } = this.props

    setFormMessage('login', {})
    resetForm('login', {
      email: '',
      password: '',
    })
  }

  render() {
    const { l } = this.context
    const { loginForm, setFormField, touchedFormField, className } = this.props
    const { email, password } = loginForm.fields
    const isPasswordFocused = password.isFocused
    const errors = this.getErrors()
    const message = path(['message', 'message'], loginForm)
    const setField = (field) => (e) =>
      setFormField('login', field, e.target.value)
    const touchedField = (field) => () => touchedFormField('login', field)

    return (
      <section className={`Login ${className}`}>
        <h3 className="Login-header">{l`Sign In`}</h3>

        <form className="Login-form" onSubmit={this.onSubmit} method="POST">
          <Input
            id="Login-email"
            field={email}
            name="email"
            type="email"
            errors={errors}
            label={l`Email address`}
            placeholder={l`example@domain.com`}
            setField={setField}
            touchedField={touchedField}
            isRequired
          />

          <Input
            id="Login-password"
            field={password}
            name="password"
            type="password"
            errors={errors}
            label={l`Password`}
            setField={setField}
            touchedField={touchedField}
            isFocused={isPasswordFocused}
            isRequired
          />

          <Button
            className="Login-submitButton"
            isActive={isEmpty(errors)}
            type="submit"
          >{l`Sign In`}</Button>
          {message && (
            <Message
              message={message}
              type={path(['message', 'type'], loginForm)}
            />
          )}
        </form>
      </section>
    )
  }
}
