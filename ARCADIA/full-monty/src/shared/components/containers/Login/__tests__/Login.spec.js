import { clone } from 'ramda'
import deepFreeze from 'deep-freeze'
import testComponentHelper from 'test/unit/helpers/test-component'
import Login from '../Login'
import {
  GTM_CATEGORY,
  GTM_ACTION,
  ANALYTICS_ERROR,
} from '../../../../analytics/analytics-constants'

const setPassword = (password) => ({
  loginForm: {
    fields: {
      password: {
        isFocused: password,
      },
    },
  },
})

describe('<Login />', () => {
  beforeEach(() => {
    jest.resetAllMocks()
  })

  const initialProps = deepFreeze({
    loginForm: {
      fields: {
        email: {
          value: 'elroy@arcadia.com',
        },
        password: {
          value: 'password1',
        },
      },
    },
    loginUser: jest.fn(),
    sendAnalyticsClickEvent: jest.fn(),
    sendAnalyticsErrorMessage: jest.fn(),
    setFormField: jest.fn(),
    touchedFormField: jest.fn(),
    resetForm: jest.fn(),
    setFormMessage: jest.fn(),
    touchedMultipleFormFields: jest.fn(),
    focusedFormField: jest.fn(),
    getNextRoute: jest.fn(),
  })
  const loginFormWithError = deepFreeze({
    ...initialProps,
    loginForm: {
      ...initialProps.loginForm,
      message: {
        message: 'this is a fake Message',
        type: 'normal',
      },
    },
  })
  const renderComponent = testComponentHelper(Login.WrappedComponent)

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })

    it('in error state', () => {
      expect(
        renderComponent({
          ...initialProps,
          loginForm: {
            fields: {
              email: {},
              password: {},
            },
          },
        }).getTree()
      ).toMatchSnapshot()
    })

    it('when password isfocused', () => {
      expect(
        renderComponent({
          ...initialProps,
          loginForm: {
            fields: {
              email: {},
              password: {
                isFocused: true,
              },
            },
          },
        }).getTree()
      ).toMatchSnapshot()
    })

    it('with loginform having message', () => {
      expect(renderComponent(loginFormWithError).getTree()).toMatchSnapshot()
    })
  })

  describe('@lifecycle', () => {
    describe('componentDidUpdate', () => {
      beforeEach(() => jest.resetAllMocks())
      it('should call focusedFormField("login", "password", false) when the password field is focused and was not focused', () => {
        const { instance } = renderComponent({
          ...initialProps,
          ...setPassword(true),
        })
        expect(instance.props.focusedFormField).not.toHaveBeenCalled()
        instance.componentDidUpdate(setPassword(false))
        expect(instance.props.focusedFormField).toHaveBeenCalledTimes(1)
        expect(instance.props.focusedFormField).toHaveBeenLastCalledWith(
          'login',
          'password',
          false
        )
      })

      it('should not call focusedFormField() when the password field is not focused', () => {
        const { instance } = renderComponent({
          ...initialProps,
          ...setPassword(false),
        })
        instance.componentDidUpdate(setPassword(true))
        expect(instance.props.focusedFormField).not.toHaveBeenCalled()
      })

      it('should not call focusedFormField() when the password field is focused and was also focused', () => {
        const { instance } = renderComponent({
          ...initialProps,
          ...setPassword(true),
        })
        instance.componentDidUpdate(setPassword(true))
        expect(instance.props.focusedFormField).not.toHaveBeenCalled()
      })
    })

    describe('@componentWillUnmount', () => {
      it('should call setFormMessage and resetForm', () => {
        const { wrapper, instance } = renderComponent(initialProps)
        expect(instance.props.setFormMessage).not.toHaveBeenCalled()
        expect(instance.props.resetForm).not.toHaveBeenCalled()
        wrapper.unmount()
        expect(instance.props.setFormMessage).toHaveBeenCalledTimes(1)
        expect(instance.props.setFormMessage).toHaveBeenLastCalledWith(
          'login',
          {}
        )
        expect(instance.props.resetForm).toHaveBeenCalledTimes(1)
        expect(instance.props.resetForm).toHaveBeenLastCalledWith('login', {
          email: '',
          password: '',
        })
      })
    })
  })

  describe('@events', () => {
    let renderedComponent
    const event = {
      target: {
        value: 123,
      },
      preventDefault: jest.fn(),
    }
    beforeEach(() => {
      renderedComponent = renderComponent(initialProps)
      jest.resetAllMocks()
    })

    describe('email Input', () => {
      const field = 'email'
      it('should call setFormField when prop setField is called', () => {
        const { wrapper, instance } = renderedComponent
        expect(instance.props.setFormField).not.toHaveBeenCalled()
        wrapper.find(`Connect(Input) [name="${field}"]`).prop('setField')(
          field
        )(event)
        expect(instance.props.setFormField).toHaveBeenCalledTimes(1)
        expect(instance.props.setFormField).toHaveBeenLastCalledWith(
          'login',
          field,
          event.target.value
        )
      })

      it('should call touchedFormField when prop touchedField is called', () => {
        const { wrapper, instance } = renderedComponent
        expect(instance.props.touchedFormField).not.toHaveBeenCalled()
        wrapper.find(`Connect(Input) [name="${field}"]`).prop('touchedField')(
          field
        )(event)
        expect(instance.props.touchedFormField).toHaveBeenCalledTimes(1)
        expect(instance.props.touchedFormField).toHaveBeenLastCalledWith(
          'login',
          field
        )
      })
    })

    describe('password Input', () => {
      const field = 'password'
      it('should call setFormField when prop setField is called', () => {
        const { wrapper, instance } = renderedComponent
        expect(instance.props.setFormField).not.toHaveBeenCalled()
        wrapper.find(`Connect(Input) [name="${field}"]`).prop('setField')(
          field
        )(event)
        expect(instance.props.setFormField).toHaveBeenCalledTimes(1)
        expect(instance.props.setFormField).toHaveBeenLastCalledWith(
          'login',
          field,
          event.target.value
        )
      })

      it('should call touchedFormField when prop touchedField is called', () => {
        const { wrapper, instance } = renderedComponent
        expect(instance.props.touchedFormField).not.toHaveBeenCalled()
        wrapper.find(`Connect(Input) [name="${field}"]`).prop('touchedField')(
          field
        )(event)
        expect(instance.props.touchedFormField).toHaveBeenCalledTimes(1)
        expect(instance.props.touchedFormField).toHaveBeenLastCalledWith(
          'login',
          field
        )
      })
    })

    describe('form submit', () => {
      it('should call e.preventDefault() ', () => {
        const { wrapper } = renderedComponent
        expect(event.preventDefault).not.toHaveBeenCalled()
        wrapper.find('form').simulate('submit', event)
        expect(event.preventDefault).toHaveBeenCalledTimes(1)
      })

      it('should call touchedMultipleFormFields', () => {
        const { instance, wrapper } = renderedComponent
        expect(instance.props.touchedMultipleFormFields).not.toHaveBeenCalled()
        wrapper.find('form').simulate('submit', event)
        expect(instance.props.touchedMultipleFormFields).toHaveBeenCalledTimes(
          1
        )
        expect(
          instance.props.touchedMultipleFormFields
        ).toHaveBeenLastCalledWith('login', ['email', 'password'])
      })

      it('call only sendAnalyticsErrorMessage() when there are no input values', () => {
        const props = clone(initialProps)
        props.loginForm.fields.email = ''
        props.loginForm.fields.password = ''
        const { wrapper, instance } = renderComponent(props)
        expect(instance.props.sendAnalyticsClickEvent).not.toHaveBeenCalled()
        wrapper.find('form').simulate('submit', event)
        expect(instance.props.sendAnalyticsClickEvent).not.toHaveBeenCalled()
        expect(instance.props.sendAnalyticsErrorMessage).toHaveBeenCalled()
        expect(instance.props.sendAnalyticsErrorMessage).toHaveBeenCalledWith(
          ANALYTICS_ERROR.LOGIN_FAILED
        )
      })

      it('should call sendAnalayticsErrorMessage() when user submits form with incorrect validation', () => {
        const props = clone(initialProps)
        props.loginForm.fields.email = 'test@arcadiagroup.co.uk'
        props.loginForm.fields.password = ''
        const { wrapper, instance } = renderComponent(props)
        expect(instance.props.sendAnalyticsClickEvent).not.toHaveBeenCalled()
        wrapper.find('form').simulate('submit', event)
        expect(instance.props.sendAnalyticsClickEvent).not.toHaveBeenCalled()
        expect(instance.props.sendAnalyticsErrorMessage).toHaveBeenCalled()
        expect(instance.props.sendAnalyticsErrorMessage).toHaveBeenCalledWith(
          ANALYTICS_ERROR.LOGIN_FAILED
        )
      })

      it('should call analytics methods when there are input values', () => {
        const { wrapper, instance } = renderComponent(loginFormWithError)
        const prevProps = {
          loginForm: { fields: { password: { isFocused: true } } },
        }
        instance.componentDidUpdate(prevProps)
        wrapper.find('form').simulate('submit', event)
        expect(instance.props.sendAnalyticsClickEvent).toHaveBeenCalledWith({
          category: GTM_CATEGORY.SIGN_IN,
          action: GTM_ACTION.SUBMIT,
          label: 'sign-in-button',
        })
      })

      it('should call sendAnalyticsClickEvent() when user logs in', () => {
        const { wrapper, instance } = renderComponent(initialProps)
        expect(instance.props.sendAnalyticsClickEvent).not.toHaveBeenCalled()
        wrapper.find('form').simulate('submit', event)
        expect(instance.props.sendAnalyticsClickEvent).toHaveBeenCalledTimes(1)
      })

      it('should call loginUser if no errors', () => {
        const { instance, wrapper } = renderedComponent
        instance.getErrors = jest.fn(() => ({}))
        expect(instance.props.loginUser).not.toHaveBeenCalled()
        wrapper.find('form').simulate('submit', event)
        expect(instance.props.loginUser).toHaveBeenCalledTimes(1)
        expect(instance.props.loginUser).toHaveBeenLastCalledWith({
          credentials: {
            username: instance.props.loginForm.fields.email.value,
            password: instance.props.loginForm.fields.password.value,
          },
          getNextRoute: instance.props.getNextRoute,
          successCallback: instance.props.successCallback,
        })
      })

      it('should not call loginUser if errors', () => {
        const { instance, wrapper } = renderedComponent
        instance.getErrors = jest.fn(() => ({
          email: 'has errors',
        }))
        wrapper.find('form').simulate('submit', event)
        expect(instance.props.loginUser).not.toHaveBeenCalled()
      })
    })
  })
})
