import React, { Component } from 'react'
import PropTypes from 'prop-types'
import DetailsFields from '../DetailsFields/DetailsFields'
import PaymentDetails from '../PaymentDetails/PaymentDetails'
import {
  curry,
  path,
  pick,
  mapObjIndexed,
  isEmpty,
  pluck,
  omit,
  map,
} from 'ramda'
import Button from '../../common/Button/Button'
import Message from '../../common/FormComponents/Message/Message'
import AccountHeader from '../../common/AccountHeader/AccountHeader'
import { connect } from 'react-redux'
import * as FormActions from '../../../actions/common/formActions'
import * as AccountActions from '../../../actions/common/accountActions'
import * as ModalActions from '../../../actions/common/modalActions'
import { validate } from '../../../lib/validator/index'
import {
  cardExpiry,
  hasLength,
  regexValidate,
} from '../../../lib/validator/validators'
import { scrollElementIntoView } from '../../../lib/scroll-helper'
import detailsSchema from '../../../constants/customerDetailsSchema'
import { browserHistory } from 'react-router'
import paymentRules from '../../../constants/paymentValidationRules'
import { regionalPhoneValidation } from '../CheckoutV2/shared/validationSchemas'
import analyticsDecorator from '../../../../client/lib/analytics/analytics-decorator'

@analyticsDecorator('my-checkout-details', { isAsync: false })
@connect(
  (state) => ({
    user: state.account.user,
    paymentMethods: state.paymentMethods,
    customerDetailsForm: state.forms.customerDetails,
    rules: state.config.checkoutAddressFormRules,
  }),
  { ...FormActions, ...AccountActions, ...ModalActions }
)
export default class CustomerDetails extends Component {
  static propTypes = {
    user: PropTypes.object,
    customerDetailsForm: PropTypes.object,
    setFormField: PropTypes.func,
    touchedFormField: PropTypes.func,
    resetForm: PropTypes.func,
    resetFormPartial: PropTypes.func,
    updateAccount: PropTypes.func,
    rules: PropTypes.object,
    showModal: PropTypes.func,
    closeModal: PropTypes.func,
    closeCustomerDetailsModal: PropTypes.func,
    paymentMethods: PropTypes.array,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentWillReceiveProps(nextProps) {
    const { showModal } = this.props
    const { l } = this.context

    const confirmHTML = (
      <div className="CustomerDetails-modal">
        <h3>{l`Thank you for updating your details.`}</h3>
        <p
        >{l`Please note, you can update your billing details when you check out.`}</p>
        <Button
          className="CustomerDetails-emptyBag"
          clickHandler={this.confirmChange}
        >{l`Back to My Account`}</Button>
      </div>
    )

    if (nextProps.customerDetailsForm.modalOpen) {
      showModal(confirmHTML)
    }
  }
  componentWillUnmount() {
    const { resetForm, customerDetailsForm } = this.props
    resetForm('customerDetails', map(() => '', customerDetailsForm.fields))
  }

  assemble = () => {
    const { customerDetailsForm, user, paymentMethods } = this.props
    const paymentType = path(['fields', 'type', 'value'], customerDetailsForm)
    const creditCard = pick(
      ['expiryYear', 'expiryMonth', 'type', 'cardNumber'],
      { ...user.creditCard, ...pluck('value', customerDetailsForm.fields) }
    )

    // **** IF `OTHER` TYPE PAYMENT SELECTED: RESET CREDIT CARD VALUES TO FAKE DEFAULT **** //
    const paymentSelected = paymentMethods.find(
      ({ value }) => value === paymentType
    )
    if (paymentSelected && paymentSelected.type === 'OTHER') {
      const date = new Date()
      creditCard.cardNumber = '0'
      creditCard.expiryMonth = date.getMonth() + 1
      creditCard.expiryYear = date.getFullYear()
    }

    const nameAndPhoneFields = detailsSchema.slice(0, 4)
    const addressFields = detailsSchema.slice(4, 10)

    const nameAndPhone = {
      ...pluck('value', pick(nameAndPhoneFields, customerDetailsForm.fields)),
      telephone: customerDetailsForm.fields.telephone.value.replace(/\D/g, ''),
    }
    const address = pluck(
      'value',
      pick(addressFields, customerDetailsForm.fields)
    )

    // This was more elaborate with a section (billing, delivery) being passed to DetailsField.jsx
    // the schema we are working off off also had two kinds of fields e.g. billing-telephone + delivery-telephone
    // Note that as the API currently doesn't properly allow for the independent update of these,
    // We are simply passing the delivery details to both delivery and billing right now
    return {
      billingDetails: {
        nameAndPhone,
        address,
      },
      deliveryDetails: {
        nameAndPhone,
        address,
      },
      creditCard,
    }
  }

  scrollToFirstError(name) {
    if (process.browser) {
      const el =
        document.querySelector(`.CustomerDetails .Input-${name}`) ||
        document.querySelector(`.CustomerDetails select[name=${name}]`)
      scrollElementIntoView(el, 400, 20)
    }
  }

  submitHandler = (validationSchema, errors) => {
    const { updateAccount, touchedFormField } = this.props
    if (!isEmpty(errors)) {
      this.scrollToFirstError(Object.keys(errors)[0])
      Object.keys(validationSchema).forEach((name) =>
        touchedFormField('customerDetails', name)
      )
      return
    }
    updateAccount(this.assemble())
  }

  confirmChange = () => {
    const { closeModal, closeCustomerDetailsModal } = this.props
    closeModal()
    closeCustomerDetailsModal()
    browserHistory.push('/my-account')
  }

  validationRule() {
    const { rules, customerDetailsForm } = this.props
    const country = customerDetailsForm.fields.country.value
    return rules[country] ? rules[country].pattern : undefined
  }

  stateField() {
    const { rules, customerDetailsForm } = this.props
    const country = customerDetailsForm.fields.country.value
    return rules[country]
  }

  postCodeValidate(rule) {
    const { l } = this.context
    return rule
      ? regexValidate(l`Please enter a valid post code`, rule)
      : 'required'
  }

  render() {
    const { l } = this.context
    const {
      setFormField,
      touchedFormField,
      resetForm,
      resetFormPartial,
      user,
      customerDetailsForm,
      paymentMethods,
    } = this.props
    const setField = curry((formName, field, e) =>
      setFormField(formName, field, e.target.value)
    )
    const touchedField = curry((formName, field) => () =>
      touchedFormField(formName, field)
    )

    const customerDetailsFields =
      this.stateField() && this.stateField().stateFieldType
        ? omit(['address2'], customerDetailsForm.fields)
        : omit(['address2', 'state'], customerDetailsForm.fields)
    const validationSchemaDetails = mapObjIndexed((field, key) => {
      if (/postcode/.test(key))
        return this.postCodeValidate(this.validationRule())
      if (/telephone/.test(key))
        return regionalPhoneValidation(customerDetailsForm.fields.country.value)
      return 'required'
    }, customerDetailsFields)

    const type = customerDetailsForm.fields.type.value
    const validationSchemaPayment = {
      cardNumber: paymentRules[type]
        ? hasLength(
            paymentRules[type].cardNumber.message,
            paymentRules[type].cardNumber.length
          )
        : '',
      expiryMonth: cardExpiry(l`Please select a valid expiry date`, new Date()),
    }
    const validationSchema = {
      ...validationSchemaDetails,
      ...validationSchemaPayment,
    }
    const errors = validate(
      validationSchema,
      pluck('value', customerDetailsForm.fields),
      l
    )

    return (
      <section className="CustomerDetails">
        <AccountHeader
          link="/my-account"
          label={l`Back to My Account`}
          title={l`My Delivery & Payment Details`}
        />
        <form className="MyAccount-form MyAccount-wrapper">
          <DetailsFields
            user={user}
            setField={setField('customerDetails')}
            touchedField={touchedField('customerDetails')}
            resetForm={resetForm}
            form={customerDetailsForm}
            errors={errors}
            state={this.stateField()}
            privacyProtected
          />
          <PaymentDetails
            paymentMethods={paymentMethods}
            user={user}
            setField={setField('customerDetails')}
            touchedField={touchedField('customerDetails')}
            resetFormPartial={resetFormPartial}
            form={customerDetailsForm}
            errors={errors}
            privacyProtected
          />
          <Button
            clickHandler={() => this.submitHandler(validationSchema, errors)}
          >
            {l`SAVE CHANGES`}
          </Button>

          <Message
            message={path(['message', 'message'], customerDetailsForm)}
            type={path(['message', 'type'], customerDetailsForm)}
          />
        </form>
      </section>
    )
  }
}
