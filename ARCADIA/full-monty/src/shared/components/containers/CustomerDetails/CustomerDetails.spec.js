import testComponentHelper, {
  analyticsDecoratorHelper,
} from 'test/unit/helpers/test-component'

import CustomerDetails from './CustomerDetails'

describe('<CustomerDetails />', () => {
  const renderComponent = testComponentHelper(
    CustomerDetails.WrappedComponent.WrappedComponent
  )

  const props = {
    customerDetailsForm: {
      fields: {
        country: {
          value: 'United Kingdom',
        },
        type: {
          value: 'VISA',
        },
        cardNumber: {
          value: '123456789',
        },
        cvv: {
          value: '123',
        },
      },
    },
    rules: {
      'United Kingdom': {
        stateFieldType: true,
      },
    },
  }

  describe('@decorators', () => {
    analyticsDecoratorHelper(CustomerDetails, 'my-checkout-details', {
      componentName: 'CustomerDetails',
      isAsync: false,
      redux: true,
    })
  })

  describe('@renders', () => {
    it('should render correct default state', () => {
      expect(renderComponent(props).getTree()).toMatchSnapshot()
    })

    it('should contain a form', () => {
      expect(renderComponent(props).wrapper.find('form').length).toBe(1)
    })

    it('should contain a message tag', () => {
      expect(renderComponent(props).wrapper.find('Message').length).toBe(1)
    })
  })
})
