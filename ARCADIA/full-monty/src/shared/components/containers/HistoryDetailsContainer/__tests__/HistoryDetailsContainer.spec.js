import testComponentHelper from 'test/unit/helpers/test-component'
import HistoryDetailsContainer from '../HistoryDetailsContainer'

const orderLines = [
  {
    lineNo: '02J50LBLK',
    name: 'MOTO Hook & Eye Joni Jeans',
    size: 'W2430',
    colour: 'BLACK',
    imageUrl:
      'http://ts.stage.arcadiagroup.ltd.uk/wcsstore/TopShop/images/catalog/TS02J50LBLK_Small_F_1.jpg',
    quantity: 1,
    unitPrice: '52.00',
    discount: '',
    total: '52.00',
    nonRefundable: false,
  },
  {
    lineNo: '26B07MOCH',
    name: 'PETITE Knot Front Top',
    size: '4',
    colour: 'Ochre',
    imageUrl:
      'http://ts.stage.arcadiagroup.ltd.uk/wcsstore/TopShop/images/catalog/TS26B07MOCH_Small_F_1.jpg',
    quantity: 1,
    unitPrice: '29.00',
    discount: '',
    total: '29.00',
    nonRefundable: false,
  },
]

const paymentDetails = [
  {
    paymentMethod: 'Visa',
    cardNumberStar: '************1111',
    totalCost: '£174.00',
  },
]

const address = {
  name: 'Ms elroy antao',
  address1: 'Wallis',
  address2: '216 Oxford Street',
  address3: 'LONDON',
  address4: 'W1C 1DB',
  country: 'United Kingdom',
}

describe('<HistoryDetailsContainer />', () => {
  const initialProps = {
    paramId: 'fake-id',
    orderDetails: {},
  }
  const renderComponent = testComponentHelper(HistoryDetailsContainer)

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })
    it('with no orderDetails - orderId it should display paramId', () => {
      const { wrapper, getTreeFor } = renderComponent(initialProps)
      expect(
        getTreeFor(wrapper.find('.HistoryDetailsContainer-orderNumber'))
      ).toMatchSnapshot()
    })
    it('with no orderDetails - orderStatus it should not render', () => {
      const { wrapper, getTreeFor } = renderComponent({
        ...initialProps,
      })
      expect(
        getTreeFor(wrapper.find('.HistoryDetailsContainer-orderStatusLabel'))
      ).toBe(null)
      expect(
        getTreeFor(wrapper.find('.HistoryDetailsContainer-orderStatus'))
      ).toBe(null)
    })
    it('with orderDetails - orderId it should display orderId', () => {
      const { wrapper, getTreeFor } = renderComponent({
        ...initialProps,
        orderDetails: {
          orderId: 'fake-order-id',
        },
      })
      expect(
        getTreeFor(wrapper.find('.HistoryDetailsContainer-orderNumber'))
      ).toMatchSnapshot()
    })
    it('with orderDetails - orderStatus it should display orderStatus', () => {
      const { wrapper, getTreeFor } = renderComponent({
        ...initialProps,
        orderDetails: {
          status: 'Your order has been cancelled',
        },
      })
      expect(
        getTreeFor(wrapper.find('.HistoryDetailsContainer-orderStatus'))
      ).toMatchSnapshot()
    })
    it('with orderLines', () => {
      const { wrapper, getTreeFor } = renderComponent({
        ...initialProps,
        orderDetails: {
          orderId: 'fake-order-id',
          orderLines,
        },
      })
      expect(
        getTreeFor(wrapper.find('.HistoryDetailsContainer-list'))
      ).toMatchSnapshot()
    })
    it('with orderLines as []', () => {
      const { wrapper, getTreeFor } = renderComponent({
        ...initialProps,
        orderDetails: {
          orderId: 'fake-order-id',
          orderLines: [],
        },
      })
      expect(
        getTreeFor(wrapper.find('.HistoryDetailsContainer-notFound'))
      ).toMatchSnapshot()
    })
    it('with totalOrderPrice and deliveryPrice', () => {
      const { wrapper, getTreeFor } = renderComponent({
        ...initialProps,
        orderDetails: {
          totalOrderPrice: '75',
          deliveryPrice: '4',
        },
      })
      expect(
        getTreeFor(wrapper.find('OrderHistoryDetailsSummary'))
      ).toMatchSnapshot()
    })
    it('with paymentDetails and billingAddress', () => {
      const { wrapper, getTreeFor } = renderComponent({
        ...initialProps,
        orderDetails: {
          billingAddress: address,
          paymentDetails,
        },
      })
      expect(
        getTreeFor(wrapper.find('OrderHistoryDetailsAddress[type="billing"]'))
      ).toMatchSnapshot()
    })
    it('with paymentDetails and deliveryAddress', () => {
      const { wrapper, getTreeFor } = renderComponent({
        ...initialProps,
        orderDetails: {
          deliveryAddress: address,
          paymentDetails,
          deliveryMethod: 'UK Standard up to 4 working days',
        },
      })
      expect(
        getTreeFor(wrapper.find('OrderHistoryDetailsAddress[type="shipping"]'))
      ).toMatchSnapshot()
    })
    it('with paymentDetails', () => {
      const { wrapper, getTreeFor } = renderComponent({
        ...initialProps,
        orderDetails: {
          paymentDetails,
        },
      })
      expect(
        getTreeFor(wrapper.find('OrderHistoryDetailsPayment'))
      ).toMatchSnapshot()
    })
    it('with paymentDetails and deliveryMethod, deliveryDate, deliveryCarrier', () => {
      const { wrapper, getTreeFor } = renderComponent({
        ...initialProps,
        orderDetails: {
          paymentDetails,
          deliveryMethod: 'UK Standard up to 4 working days',
          deliveryDate: 'Monday 17 July 2017',
          deliveryCarrier: 'Parcelnet',
        },
      })
      expect(
        getTreeFor(wrapper.find('OrderHistoryDetailsDelivery'))
      ).toMatchSnapshot()
    })
  })
})
