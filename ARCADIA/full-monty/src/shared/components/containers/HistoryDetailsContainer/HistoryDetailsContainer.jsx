import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { isEmpty } from 'ramda'

import OrderHistoryDetailsElement from './OrderHistoryDetailsElement'
import OrderHistoryDetailsDelivery from './OrderHistoryDetailsDelivery'
import OrderHistoryDetailsAddress from './OrderHistoryDetailsAddress'
import OrderHistoryDetailsPayment from './OrderHistoryDetailsPayment'
import OrderHistoryDetailsSummary from './OrderHistoryDetailsSummary'

import { PrivacyGuard } from '../../../lib'

export default class HistoryDetailsContainer extends Component {
  static propTypes = {
    orderDetails: PropTypes.object.isRequired,
    paramId: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
      .isRequired,
  }
  static contextTypes = {
    l: PropTypes.func,
  }
  render() {
    const {
      paramId,
      orderDetails: {
        orderId,
        orderLines,
        paymentDetails,
        deliveryMethod,
        deliveryDate,
        deliveryCarrier,
        deliveryAddress,
        billingAddress,
        deliveryPrice,
        totalOrderPrice,
        status,
      },
    } = this.props
    const { l } = this.context

    const match = deliveryCarrier && /(\w+).*\s(\d+)/g.exec(deliveryCarrier)
    const trackingNumber = match && match[2]

    return (
      <div className="HistoryDetailsContainer">
        <section className="HistoryDetailsContainer-header">
          <div className="HistoryDetailsContainer-headerCol">
            <p className="HistoryDetailsContainer-orderNumberLabel">
              {l`Order Number`}:
            </p>
            {status && (
              <p className="HistoryDetailsContainer-orderStatusLabel">
                {l`Order status`}:
              </p>
            )}
          </div>
          <div className="HistoryDetailsContainer-headerCol">
            <p className="HistoryDetailsContainer-orderNumber">
              <PrivacyGuard>
                <strong>{orderId || paramId}</strong>
              </PrivacyGuard>
            </p>
            {status && (
              <p className="HistoryDetailsContainer-orderStatus">
                <PrivacyGuard>
                  <strong>{status}</strong>
                </PrivacyGuard>
              </p>
            )}
          </div>
        </section>
        {orderLines && !isEmpty(orderLines) ? (
          <section className="HistoryDetailsContainer-list">
            {orderLines.map((item, key) => (
              <OrderHistoryDetailsElement
                key={key} // eslint-disable-line react/no-array-index-key
                imageUrl={item.imageUrl}
                productName={item.name}
                productCode={item.lineNo}
                size={item.size}
                price={item.unitPrice}
                wasPrice={item.wasPrice}
                total={item.total}
                quantity={item.quantity}
                isDDPItem={item.isDDPItem}
                colour={item.colour}
              />
            ))}
          </section>
        ) : (
          <section className="HistoryDetailsContainer-notFound">{l`Order not found`}</section>
        )}
        {totalOrderPrice && (
          <OrderHistoryDetailsSummary
            deliveryPrice={deliveryPrice}
            totalOrderPrice={totalOrderPrice}
          />
        )}
        {paymentDetails &&
          !isEmpty(paymentDetails) && (
            <div>
              <OrderHistoryDetailsAddress
                className="OrderHistoryDetailsAddress--delivery"
                address={deliveryAddress}
                deliveryMethod={deliveryMethod}
                type="shipping"
              />
              <OrderHistoryDetailsDelivery
                deliveryMethod={deliveryMethod}
                deliveryDate={deliveryDate}
                deliveryCarrier={deliveryCarrier}
                trackingNumber={trackingNumber}
              />
              <OrderHistoryDetailsAddress
                className="OrderHistoryDetailsAddress--billing"
                address={billingAddress}
                type="billing"
              />
              <OrderHistoryDetailsPayment paymentDetails={paymentDetails} />
            </div>
          )}
      </div>
    )
  }
}
