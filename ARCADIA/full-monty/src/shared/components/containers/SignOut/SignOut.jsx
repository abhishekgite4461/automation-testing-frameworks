import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as authActions from '../../../actions/common/authActions'
import Loader from '../../common/Loader/Loader'

@connect(null, { ...authActions })
export default class SignOut extends Component {
  static propTypes = {
    logoutRequest: PropTypes.func,
  }

  componentWillMount() {
    this.props.logoutRequest()
  }

  render() {
    return <Loader />
  }
}
