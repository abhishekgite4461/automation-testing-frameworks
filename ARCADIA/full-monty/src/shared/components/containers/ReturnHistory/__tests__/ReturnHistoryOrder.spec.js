import { range } from 'ramda'
import testComponentHelper from 'test/unit/helpers/test-component'
import Image from '../../../common/Image/Image'
import Price from '../../../common/Price/Price'
import { PrivacyGuard } from '../../../../lib'
import ReturnHistoryOrder from '../ReturnHistoryOrder'

describe('ReturnHistoryOrder', () => {
  const initialProps = {
    lineNo: '19M43JRED',
    name: 'Pizza Princess Padded Sticker',
    size: 'ONE',
    colour: 'RED',
    imageUrl:
      'http://ts.stage.arcadiagroup.ltd.uk/wcsstore/TopShop/images/catalog/TS19M43JRED_Small_F_1.jpg',
    returnQuantity: 1,
    returnReason: 'Colour not as description',
    unitPrice: '',
    discount: '',
    total: '0.50',
    nonRefundable: false,
  }

  const renderComponent = testComponentHelper(ReturnHistoryOrder)

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })
    it('with discount', () => {
      expect(
        renderComponent({
          ...initialProps,
          discount: '5',
        }).getTree()
      ).toMatchSnapshot()
    })
  })

  describe('@dom', () => {
    let currentWrapped

    beforeEach(() => {
      const { wrapper } = renderComponent(initialProps)
      currentWrapped = wrapper
    })

    describe('Image', () => {
      it('should have an Image component', () => {
        expect(currentWrapped.find(Image).length).toBe(1)
      })

      it('should have an Image component with the correct props', () => {
        const image = currentWrapped.find(Image)

        expect(image.prop('src')).toBe(initialProps.imageUrl)
        expect(image.prop('className')).toBe('ReturnHistoryOrder-image')
        expect(image.prop('alt')).toBe(initialProps.name)
      })
    })

    describe('Heading', () => {
      it('should have an h2 tag', () => {
        expect(currentWrapped.find('h2').length).toBe(1)
      })

      it('should have an h2 with correct classNames', () => {
        expect(currentWrapped.find('h2').prop('className')).toBe(
          'ReturnHistoryOrder-text ReturnHistoryOrder-productName'
        )
      })

      it('should have an h2 with correct text', () => {
        expect(currentWrapped.find('h2').text()).toBe(initialProps.name)
      })
    })

    describe('Content is all there', () => {
      describe('All content is available', () => {
        it('should have the correct amount of paragraphs', () => {
          expect(currentWrapped.find('p').length).toEqual(6)
        })

        it('should have the correct amount of spans', () => {
          expect(currentWrapped.find('span').length).toEqual(6)
        })

        it('should have 2 price components', () => {
          expect(currentWrapped.find(Price).length).toEqual(2)
        })

        it('should have 4 PrivacyGuard components', () => {
          expect(currentWrapped.find(PrivacyGuard).length).toEqual(4)
        })

        it('should have correct number of spacing divs', () => {
          const divs = currentWrapped.find('div')
          expect(divs.length).toEqual(5)
          range(1, 5).forEach((index) =>
            expect(divs.at(index).prop('className')).toBe(
              'ReturnHistoryOrder-cont'
            )
          )
        })
      })

      describe('Some content missing', () => {
        it('should have a reduce amount of paragraphs', () => {
          currentWrapped.setProps({ lineNo: false })
          expect(currentWrapped.find('p').length).toEqual(5)
        })
      })
    })
  })

  describe('@instance', () => {
    let currentInstance

    beforeEach(() => {
      const { instance } = renderComponent(initialProps)
      currentInstance = instance
    })

    describe('@createOptionsForDisplay', () => {
      let returned

      beforeEach(() => {
        returned = currentInstance.createOptionsForDisplay()
      })

      it('should return an array', () => {
        expect(Array.isArray(returned)).toBe(true)
      })

      it('should return an array of arrays', () => {
        expect(returned.length).toBe(4)
        returned.forEach((arr) => {
          expect(Array.isArray(arr)).toBe(true)
        })
      })

      it('should have the correct values', () => {
        const expectedValues = [
          initialProps.lineNo,
          initialProps.returnQuantity,
          initialProps.size,
          initialProps.total,
          0,
          0.5,
        ]

        expectedValues.forEach((item, i) => {
          expect(item.value).toEqual(expectedValues[i].value)
        })
      })
    })
  })
})
