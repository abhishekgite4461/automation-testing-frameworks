import testComponentHelper from 'test/unit/helpers/test-component'
import ReturnHistoryDetailsSummary from '../ReturnHistoryDetailsSummary'

describe('ReturnHistoryDetailsSummary', () => {
  const renderComponent = testComponentHelper(ReturnHistoryDetailsSummary)
  const initialProps = {
    totalOrderPrice: '0.50',
  }

  describe('@defaults', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })
  })
})
