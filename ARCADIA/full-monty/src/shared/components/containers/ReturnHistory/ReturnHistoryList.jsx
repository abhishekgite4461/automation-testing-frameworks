import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import OrderListContainer from '../OrderListContainer/OrderListContainer'

import { returnHistoryRequest } from '../../../actions/common/accountActions'
import { numbers } from '../../../constants/customerCareNumbers'

import { TYPE } from '../OrderListContainer/types'

@connect(
  (state) => ({
    brandCode: state.config.brandCode,
    region: state.config.region,
    returns: state.returnHistory.returns,
    visited: state.routing.visited,
    orderHistoryMessageFeature: state.features.status.FEATURE_ORDER_HISTORY_MSG,
  }),
  { returnHistoryRequest }
)
export default class ReturnHistoryList extends Component {
  static propTypes = {
    brandCode: PropTypes.string.isRequired,
    orderHistoryMessageFeature: PropTypes.bool.isRequired,
    region: PropTypes.string.isRequired,
    returnHistoryRequest: PropTypes.func.isRequired,
    visited: PropTypes.array.isRequired,
    returns: PropTypes.array,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentDidMount() {
    const { returnHistoryRequest } = this.props
    // first fetch is performed by server-side render
    if (this.shouldRequestReturnList()) returnHistoryRequest()
  }

  /*
    Try Avoid call multiple time this API
   */
  shouldRequestReturnList = () => {
    const { visited, returns } = this.props
    return visited.length > 1 && returns.length === 0
  }

  /*
    sorts from newest to oldest
   */
  sortOrders(list) {
    return [...list].sort((a, b) => new Date(b.date) - new Date(a.date))
  }

  static needs = [returnHistoryRequest]

  renderReturnLimitMessage() {
    const { l } = this.context
    const { brandCode, region } = this.props
    const brandNumbers = numbers[brandCode]
    const customerCareNumber = brandNumbers[region] || brandNumbers.intl
    const limitMessage = l`Displaying your last 20 returns. For your full return history, please contact customer service at ${customerCareNumber}.`
    return (
      <p className="ReturnHistoryList-orderLimitMessage">
        {(brandNumbers[region] || (region === 'eu' && brandNumbers.intl)) &&
          limitMessage}
        {region !== 'eu' &&
          brandNumbers.intl &&
          ` ${l`For international customers, please call ${
            brandNumbers.intl
          }.`}`}
      </p>
    )
  }

  render() {
    const { l } = this.context
    const { returns, orderHistoryMessageFeature } = this.props
    const hasReturns = Array.isArray(returns) && returns.length > 0

    const returnLimitMessage =
      hasReturns && orderHistoryMessageFeature
        ? this.renderReturnLimitMessage()
        : null

    return (
      <OrderListContainer
        className="ReturnHistoryList"
        header={returnLimitMessage}
        notFoundMessage={l`There are no returns found`}
        orders={this.sortOrders(returns)}
        title={l`My returns`}
        type={TYPE.RETURN}
      />
    )
  }
}
