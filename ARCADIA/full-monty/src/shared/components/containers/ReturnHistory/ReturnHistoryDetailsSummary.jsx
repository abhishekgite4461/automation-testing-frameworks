import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Price from '../../common/Price/Price'

export default class ReturnHistoryDetailsSummary extends Component {
  static propTypes = {
    totalOrderPrice: PropTypes.string.isRequired,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  render() {
    const { l } = this.context
    const { totalOrderPrice } = this.props

    return (
      <div className="ReturnHistoryDetailsSummary">
        <div className="ReturnHistoryDetailsSummary-container">
          <p className="ReturnHistoryDetailsSummary-title">
            <strong>{l`Total credit returned`}</strong>
          </p>
          <p className="ReturnHistoryDetailsSummary-value">
            <Price price={totalOrderPrice} privacyProtected />
          </p>
        </div>
      </div>
    )
  }
}
