import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import AccountItem from '../../common/AccountItem/AccountItem'
import { getAccount } from '../../../actions/common/accountActions'
import { connect } from 'react-redux'
import { isCheckoutProfile } from '../../../lib/checkout'
import analyticsDecorator from '../../../../client/lib/analytics/analytics-decorator'

// Libs
import { pathOr } from 'ramda'

@analyticsDecorator('my-account')
@connect(
  (state) => ({
    isMobile: state.viewport.media === 'mobile',
    user: state.account.user,
    visited: state.routing.visited,
    region: state.config.region,
  }),
  {
    getAccount,
  }
)
export default class MyAccount extends Component {
  static propTypes = {
    region: PropTypes.string.isRequired,
    user: PropTypes.object,
    visited: PropTypes.array,
    getAccount: PropTypes.func,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  static needs = [getAccount]

  componentDidMount() {
    const { visited, user, getAccount } = this.props

    const isLoggedIn = pathOr(false, ['exists'], user)
    if (visited.length > 1 && !isLoggedIn) getAccount()
  }

  /**
   *
   * @param {array} listItems
   * @param {string} alignment
   * @returns {object} react component
   */
  renderList(listItems, alignment = 'left') {
    const classNames = `MyAccount-list MyAccount-list${alignment}`
    return (
      <ul className={classNames} tabIndex="-1">
        {listItems.map((entry, index) => (
          <AccountItem
            key={index} // eslint-disable-line react/no-array-index-key
            title={entry.title}
            description={entry.description}
            link={entry.link}
          />
        ))}
      </ul>
    )
  }

  /**
   * @function - if isCheckoutProfile > true, we return array of all elements
   * other wise we return defaults
   * @returns {array}
   */
  getMenuEntriesLeft() {
    const { l } = this.context
    const { user } = this.props
    const defaults = [
      {
        title: l`My details`,
        description: l`Add your name or change your email address`,
        link: '/my-account/my-details',
      },
      {
        title: l`My password`,
        description: l`Change your account password`,
        link: '/my-account/my-password',
      },
    ]
    const checkoutDetails = {
      title: l`My Delivery & Payment Details`,
      description: l`Check your saved delivery and payment details`,
      link: '/my-account/details',
    }
    return isCheckoutProfile(user) ? defaults.concat(checkoutDetails) : defaults
  }

  getMenuEntriesRight() {
    const { region, user } = this.props
    const { l } = this.context
    const menuList = [
      {
        title: l`My orders`,
        description: l`Track your current orders and view your order history`,
        link: '/my-account/order-history',
      },
    ]

    const myReturns = {
      title: l`My Returns`,
      description: l`Track your current returns and view your return history`,
      link: '/my-account/return-history',
    }
    if (isCheckoutProfile(user)) menuList.push(myReturns)

    const eReceiptLink = {
      title: l`E-receipts`,
      description: l`Manage your E-receipts here`,
      link: '/my-account/e-receipts',
    }
    if (region === 'uk') menuList.push(eReceiptLink)
    return menuList
  }

  render() {
    const { l } = this.context
    const { isMobile } = this.props
    const listItemsLeft = this.getMenuEntriesLeft()
    const listItemsRight = this.getMenuEntriesRight()
    const showHeadingClass = isMobile ? 'screen-reader-text' : ''
    return (
      <div className="MyAccount">
        <Helmet title={l`My account`} />
        <div className="MyAccount-inner">
          <h1 className={showHeadingClass}>{l`My account`}</h1>
          {this.renderList(listItemsLeft)}
          {this.renderList(listItemsRight, 'right')}
        </div>
      </div>
    )
  }
}
