import testComponentHelper, {
  analyticsDecoratorHelper,
} from 'test/unit/helpers/test-component'

jest.mock('../../../../actions/common/accountActions.js', () => ({
  getAccount: jest.fn(),
}))

import MyAccount from '../MyAccount'
import * as accountActions from '../../../../actions/common/accountActions'
import * as checkout from '../../../../lib/checkout'

describe('<MyAccount />', () => {
  let isCheckoutProfileReturn = true
  const isCheckoutProfileSpy = jest
    .spyOn(checkout, 'isCheckoutProfile')
    .mockImplementation(() => isCheckoutProfileReturn)
  const getAccountSpy = jest.spyOn(accountActions, 'getAccount')
  const renderComponent = testComponentHelper(
    MyAccount.WrappedComponent.WrappedComponent
  )
  const initialProps = {
    getAccount: getAccountSpy,
    user: {},
    visited: ['/my-account'],
    isMobile: true,
    region: 'fake-region',
  }

  beforeEach(() => {
    jest.resetAllMocks()
    isCheckoutProfileSpy.mockReset()
    isCheckoutProfileSpy.mockRestore()
    getAccountSpy.mockReset()
    getAccountSpy.mockRestore()
    isCheckoutProfileReturn = true
  })

  describe('@renders', () => {
    it('in default state (mobile)', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })

    it('is not mobile', () => {
      expect(
        renderComponent({
          ...initialProps,
          isMobile: false,
        }).getTree()
      ).toMatchSnapshot()
    })

    it('has isCheckoutProfile returns true', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })

    it('has checkOutProfile returns false', () => {
      isCheckoutProfileReturn = false
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })

    it('should show e-receipts when region is uk', () => {
      expect(
        renderComponent({
          ...initialProps,
          region: 'uk',
        }).getTree()
      ).toMatchSnapshot()
    })
  })

  describe('@lifecycle methods', () => {
    describe('@componentDidMount', () => {
      it('should NOT call getAccount method if visited length < 1', () => {
        const { instance } = renderComponent(initialProps)
        expect(getAccountSpy).toHaveBeenCalledTimes(0)
        instance.componentDidMount()
        expect(getAccountSpy).toHaveBeenCalledTimes(0)
      })

      it('should call getAccount method if visited length > 1 and user NOT logged in', () => {
        const { instance } = renderComponent({
          ...initialProps,
          visited: ['/abc', 'cde'],
        })
        expect(getAccountSpy).toHaveBeenCalledTimes(0)
        instance.componentDidMount()
        expect(getAccountSpy).toHaveBeenCalledTimes(1)
      })

      it('should NOT call getAccount method if visited length > 1 BUT user already logged in', () => {
        const { instance } = renderComponent({
          ...initialProps,
          user: { exists: true },
          visited: ['/abc', 'cde'],
        })
        expect(getAccountSpy).toHaveBeenCalledTimes(0)
        instance.componentDidMount()
        expect(getAccountSpy).toHaveBeenCalledTimes(0)
      })
    })
  })

  describe('@Needs', () => {
    it('should call getAccount()', () => {
      expect(getAccountSpy).toHaveBeenCalledTimes(0)
      MyAccount.WrappedComponent.needs[0]()
      expect(getAccountSpy).toHaveBeenCalledTimes(1)
    })
  })

  describe('@instance methods', () => {
    describe('getMenuEntriesLeft', () => {
      it('should return 3 items if isCheckoutProfile returns true', () => {
        // problem in instance that the spy above does not work
        jest.spyOn(checkout, 'isCheckoutProfile').mockImplementation(() => true)
        const { instance } = renderComponent(initialProps)
        const result = instance.getMenuEntriesLeft(true)
        expect(result.length).toBe(3)
        expect(instance.getMenuEntriesRight(true).length).toBe(2)
      })

      it('should return 2 items if isCheckoutProfile returns false', () => {
        isCheckoutProfileReturn = false
        const { instance } = renderComponent(initialProps)
        const result = instance.getMenuEntriesLeft(true)
        expect(result.length).toBe(2)
      })
    })
  })

  describe('@decorators', () => {
    analyticsDecoratorHelper(MyAccount, 'my-account', {
      componentName: 'MyAccount',
      isAsync: false,
      redux: true,
    })
  })
})
