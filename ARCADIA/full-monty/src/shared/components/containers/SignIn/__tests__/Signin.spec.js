import React from 'react'
import PropTypes from 'prop-types'
import SignIn from '../SignIn'
import testComponentHelper, {
  analyticsDecoratorHelper,
} from 'test/unit/helpers/test-component'
import { shallow } from 'enzyme'

jest.mock('../../../../lib/query-helper', () => ({
  splitQuery: (search) => ({
    return: search.split('&')[0].split('=')[1],
  }),
}))

describe('<Signin />', () => {
  const initialProps = {
    location: {
      hostname: 'm.topshop.com',
      pathname: '/login',
      search: '',
    },
  }
  const renderComponent = testComponentHelper(
    SignIn.WrappedComponent.WrappedComponent
  )

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })
    it('with search having return param', () => {
      expect(
        renderComponent({
          ...initialProps,
          location: {
            hostname: 'm.topshop.com',
            pathname: '/login',
            search:
              'return=https://m.topshop.com/fakeurl-this_should_be_the_next_route',
          },
        }).getTree()
      ).toMatchSnapshot()
    })
  })

  describe('@decorators', () => {
    analyticsDecoratorHelper(SignIn, 'register-login', {
      componentName: 'SignIn',
      redux: true,
    })
  })

  describe('mapStateToProps', () => {
    it('builds props from state', () => {
      const store = {
        subscribe: () => {},
        dispatch: () => {},
        getState: () => ({
          config: {},
          routing: {
            location: {
              hostname: 'm.topshop.com',
              pathname: '/login',
              search: '',
            },
            visited: ['/'],
          },
          loaderOverlay: {
            visible: false,
          },
          products: [],
          account: {},
          forms: {
            register: {
              fields: [],
            },
          },
        }),
      }

      const wrapper = shallow(<SignIn store={store} />, {
        context: {
          l: () => {},
          store,
        },
        childContextTypes: {
          l: PropTypes.func,
          store: PropTypes.object,
        },
      })

      expect(
        wrapper
          .dive()
          .dive()
          .props()
      ).toMatchObject({
        location: {
          hostname: 'm.topshop.com',
          pathname: '/login',
          search: '',
        },
      })
    })
  })
})
