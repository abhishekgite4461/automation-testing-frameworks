import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'
import Login from '../Login/Login'
import Register from '../Register/Register'
import { splitQuery } from '../../../lib/query-helper'
import ForgetPassword from '../ForgetPassword/ForgetPassword'
import { slice, join, split, compose } from 'ramda'
import { getCanonicalHostname } from '../../../../shared/lib/canonicalisation'
import analyticsDecorator from '../../../../client/lib/analytics/analytics-decorator'
import { GTM_CATEGORY } from '../../../../shared/analytics'

@analyticsDecorator(GTM_CATEGORY.REGISTER_LOGIN)
@connect((state) => ({
  location: state.routing.location,
}))
export default class SignIn extends Component {
  static propTypes = {
    location: PropTypes.shape({
      hostname: PropTypes.string.isRequired,
      pathname: PropTypes.string,
      search: PropTypes.string,
    }).isRequired,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  getNextRoute = () => {
    const {
      location: { search },
    } = this.props
    const bazaarvoiceUri = splitQuery(search).return
    return (
      bazaarvoiceUri &&
      compose(
        join('/'),
        slice(3, Infinity),
        split('/')
      )(bazaarvoiceUri)
    )
  }

  render() {
    const { l } = this.context
    const {
      location: { hostname, pathname },
    } = this.props

    return (
      <section className="SignIn">
        <Helmet
          title={l`Sign In`}
          link={[
            {
              rel: 'canonical',
              href: getCanonicalHostname(hostname) + pathname,
            },
          ]}
        />
        <div className="SignIn-loginSection">
          <Login getNextRoute={this.getNextRoute} className="Login--signin" />
          <ForgetPassword className="ForgetPassword--signin" />
        </div>
        <div className="SignIn-registerSection">
          <Register
            getNextRoute={this.getNextRoute}
            className="Register--signin"
          />
        </div>
      </section>
    )
  }
}
