import testComponentHelper from 'test/unit/helpers/test-component'
import generateMockFormProps from 'test/unit/helpers/form-props-helper'
import {
  initialMiniBagProps as initialProps,
  initialMiniBagFormProps as initialFormProps,
} from './miniBagMocks'
import {
  GTM_CATEGORY,
  GTM_ACTION,
} from '../../../../analytics/analytics-constants'

jest.mock('react-router', () => ({
  browserHistory: {
    push: jest.fn(),
  },
}))

import { browserHistory } from 'react-router'
import MiniBag from '../MiniBag'

describe('<MiniBag />', () => {
  const renderComponent = testComponentHelper(MiniBag.WrappedComponent)
  const generateProps = (newFormProps) => ({
    ...initialProps,
    ...generateMockFormProps(initialFormProps, newFormProps),
  })

  describe('@renders', () => {
    describe('in default state', () => {
      it('should display an empty bag', () => {
        expect(
          renderComponent({
            bagProducts: [],
            sendAnalyticsClickEvent: () => {},
          }).getTree()
        ).toMatchSnapshot()
      })

      it('should display products added to basket', () => {
        expect(renderComponent(generateProps()).getTree()).toMatchSnapshot()
      })

      it('should display number of products in header', () => {
        const { wrapper } = renderComponent(generateProps())
        expect(wrapper.find('.MiniBag-header').text()).toMatch('My Bag (1)')
      })

      it('should not track with <Peerius /> while is closed', () => {
        expect(
          renderComponent({
            ...generateProps(),
            miniBagOpen: false,
          }).getTree()
        ).toMatchSnapshot()
      })

      it('when CFSi is true and not in checkout', () => {
        expect(
          renderComponent({
            ...generateProps(),
            inCheckout: false,
            CFSi: true,
          }).getTree()
        ).toMatchSnapshot()
      })
    })

    describe('in other states', () => {
      it('should display discounts when available', () => {
        expect(
          renderComponent({
            ...generateProps(),
            bagDiscounts: [
              { label: 'test promotions code discount', value: '2.00' },
            ],
          }).getTree()
        ).toMatchSnapshot()
      })

      it('should render <Loader /> while loading products', () => {
        expect(
          renderComponent({
            ...generateProps(),
            loadingShoppingBag: true,
          }).getTree()
        ).toMatchSnapshot()
      })

      it('should display PromotionCode if not in checkout', () => {
        expect(
          renderComponent({
            ...generateProps(),
            inCheckout: false,
          }).getTree()
        ).toMatchSnapshot()
      })

      it('should not display PromotionCode if in checkout', () => {
        expect(
          renderComponent({
            ...generateProps(),
            inCheckout: true,
          }).getTree()
        ).toMatchSnapshot()
      })

      describe('in Checkout', () => {
        it('should not render <ShoppingBagDeliveryOptions />', () => {
          expect(
            renderComponent({
              ...generateProps(),
              inCheckout: true,
            }).getTree()
          ).toMatchSnapshot()
        })

        it('should render Delivery estimate in Payment page for New Checkout Workflow', () => {
          expect(
            renderComponent({
              ...generateProps(initialFormProps),
              inCheckout: true,
              pathname: '/checkout/payment',
            }).getTree()
          ).toMatchSnapshot()
        })
      })
    })
  })

  describe('@events', () => {
    const autoCloseDelayLength = 3000

    beforeEach(() => {
      jest.resetAllMocks()
      jest.useFakeTimers()
    })

    describe('on close button click', () => {
      it('should close the minibag', () => {
        const { wrapper } = renderComponent(initialProps)
        expect(initialProps.closeMiniBag).not.toHaveBeenCalled()
        wrapper.find('.MiniBag-closeButton').simulate('click')
        expect(initialProps.closeMiniBag).toHaveBeenCalled()
      })

      it('should fire a GTM click event', () => {
        const { wrapper } = renderComponent(initialProps)
        expect(initialProps.closeMiniBag).not.toHaveBeenCalled()
        wrapper.find('.MiniBag-closeButton').simulate('click')
        expect(initialProps.sendAnalyticsClickEvent).toHaveBeenCalled()
        expect(initialProps.sendAnalyticsClickEvent).toHaveBeenCalledWith({
          category: GTM_CATEGORY.SHOPPING_BAG,
          action: GTM_ACTION.CLICKED,
          label: 'close-button',
        })
      })
    })

    describe('On "Continue shopping" button click', () => {
      it('should close mini bag', () => {
        const closeMiniBagMock = jest.fn()
        const { wrapper } = renderComponent({
          bagProducts: [],
          closeMiniBag: closeMiniBagMock,
          sendAnalyticsClickEvent: () => {},
        })
        expect(closeMiniBagMock).toHaveBeenCalledTimes(0)
        wrapper
          .find('.Button--primary')
          .props()
          .clickHandler()
        expect(closeMiniBagMock).toHaveBeenCalledTimes(1)
        jest.advanceTimersByTime(autoCloseDelayLength)
        expect(setInterval).not.toHaveBeenCalled()
      })
    })

    describe('On "Checkout Now" button click', () => {
      it('should redirect to checkout and close mini bag', () => {
        const generatedProps = generateProps()
        const { wrapper } = renderComponent(generatedProps)
        expect(browserHistory.push).toHaveBeenCalledTimes(0)
        expect(generatedProps.closeMiniBag).toHaveBeenCalledTimes(0)
        expect(generatedProps.sendAnalyticsClickEvent).toHaveBeenCalledTimes(0)
        wrapper
          .find('.MiniBag-continueButton')
          .first()
          .props()
          .clickHandler()
        expect(browserHistory.push).toHaveBeenCalledWith('/checkout')
        expect(browserHistory.push).toHaveBeenCalledTimes(1)
        expect(generatedProps.closeMiniBag).toHaveBeenCalledTimes(1)
        expect(generatedProps.sendAnalyticsClickEvent).toHaveBeenCalledTimes(1)
        expect(generatedProps.sendAnalyticsClickEvent).toHaveBeenCalledWith({
          category: GTM_CATEGORY.SHOPPING_BAG,
          action: GTM_ACTION.CLICKED,
          label: 'checkout-button',
        })

        jest.advanceTimersByTime(autoCloseDelayLength)
        expect(setInterval).not.toHaveBeenCalled()
      })
    })

    describe('On "Back to checkout" button click', () => {
      it('should close mini bag', () => {
        const { wrapper } = renderComponent({
          ...generateProps(initialFormProps),
          inCheckout: true,
        })
        expect(initialProps.closeMiniBag).toHaveBeenCalledTimes(0)
        wrapper
          .find('.MiniBag-backToCheckout')
          .props()
          .clickHandler()
        expect(initialProps.closeMiniBag).toHaveBeenCalledTimes(1)
      })
    })

    describe('auto close mini bag', () => {
      it('should not close automatically if autoClose prop is false', () => {
        const { wrapper } = renderComponent({
          ...generateProps(initialFormProps),
          miniBagOpen: false,
        })
        expect(initialProps.closeMiniBag).toHaveBeenCalledTimes(0)
        expect(setInterval).not.toHaveBeenCalled()
        wrapper.setProps({ miniBagOpen: true })
        jest.advanceTimersByTime(autoCloseDelayLength)
        expect(setInterval).not.toHaveBeenCalled()
      })

      it('should close after 3 seconds when not interacted with', () => {
        const { wrapper } = renderComponent({
          ...generateProps(initialFormProps),
          miniBagOpen: false,
          autoClose: true,
        })
        expect(initialProps.closeMiniBag).toHaveBeenCalledTimes(0)
        expect(setInterval).not.toHaveBeenCalled()
        wrapper.setProps({ miniBagOpen: true })
        expect(setInterval).toHaveBeenCalledWith(
          expect.any(Function),
          autoCloseDelayLength
        )
        jest.advanceTimersByTime(autoCloseDelayLength)
        expect(initialProps.closeMiniBag).toHaveBeenCalledTimes(1)
        jest.advanceTimersByTime(autoCloseDelayLength)
        expect(setInterval).toHaveBeenCalledTimes(1)
      })

      it('should not close automatically when is just being hovered', () => {
        const { wrapper } = renderComponent({
          ...generateProps(initialFormProps),
          miniBagOpen: false,
          autoClose: true,
        })
        expect(initialProps.closeMiniBag).toHaveBeenCalledTimes(0)
        expect(setInterval).not.toHaveBeenCalled()
        wrapper.setProps({ miniBagOpen: true })
        expect(setInterval).toHaveBeenCalledWith(
          expect.any(Function),
          autoCloseDelayLength
        )
        wrapper.prop('onMouseEnter')()
        jest.advanceTimersByTime(autoCloseDelayLength)
        expect(initialProps.closeMiniBag).not.toHaveBeenCalled()
      })

      it('should close automatically on mouse out when has just been hovered', () => {
        const { wrapper } = renderComponent({
          ...generateProps(initialFormProps),
          miniBagOpen: false,
          autoClose: true,
        })
        expect(initialProps.closeMiniBag).toHaveBeenCalledTimes(0)
        expect(setInterval).not.toHaveBeenCalled()
        wrapper.setProps({ miniBagOpen: true })
        expect(setInterval).toHaveBeenCalledWith(
          expect.any(Function),
          autoCloseDelayLength
        )
        expect(setInterval).toHaveBeenCalledTimes(1)
        wrapper.prop('onMouseEnter')()
        jest.advanceTimersByTime(autoCloseDelayLength)
        expect(initialProps.closeMiniBag).not.toHaveBeenCalled()
        wrapper.prop('onMouseLeave')()
        jest.advanceTimersByTime(autoCloseDelayLength)
        expect(initialProps.closeMiniBag).toHaveBeenCalledTimes(1)
        jest.advanceTimersByTime(autoCloseDelayLength)
        expect(initialProps.closeMiniBag).toHaveBeenCalledTimes(1)
      })

      it('should not close automatically if it was clicked', () => {
        const { wrapper } = renderComponent({
          ...generateProps(initialFormProps),
          miniBagOpen: false,
          autoClose: true,
        })
        expect(initialProps.closeMiniBag).toHaveBeenCalledTimes(0)
        expect(setInterval).not.toHaveBeenCalled()
        wrapper.setProps({ miniBagOpen: true })
        expect(setInterval).toHaveBeenCalledWith(
          expect.any(Function),
          autoCloseDelayLength
        )
        wrapper.prop('onClick')()
        jest.advanceTimersByTime(autoCloseDelayLength)
        expect(initialProps.closeMiniBag).not.toHaveBeenCalled()
      })

      it('should not close automatically if it was touched', () => {
        const { wrapper } = renderComponent({
          ...generateProps(initialFormProps),
          miniBagOpen: false,
          autoClose: true,
        })
        expect(initialProps.closeMiniBag).toHaveBeenCalledTimes(0)
        expect(setInterval).not.toHaveBeenCalled()
        wrapper.setProps({ miniBagOpen: true })
        expect(setInterval).toHaveBeenCalledWith(
          expect.any(Function),
          autoCloseDelayLength
        )
        wrapper.prop('onTouchStart')()
        jest.advanceTimersByTime(autoCloseDelayLength)
        expect(initialProps.closeMiniBag).not.toHaveBeenCalled()
      })
    })
  })

  describe('@methods', () => {
    describe('scrollMinibag(pos)', () => {
      it('should modify scrollTop property', () => {
        const { instance } = renderComponent(generateProps())
        instance.miniContent = {
          offsetHeight: 1200,
          scrollTop: 0,
        }

        instance.scrollMinibag(100)
        expect(instance.miniContent.scrollTop).toBe(-1100)
      })
    })
  })
})
