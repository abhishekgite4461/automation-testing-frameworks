import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import ShoppingBag from '../../common/ShoppingBag/ShoppingBag'
import PromotionCode from '../PromotionCode/PromotionCode'
import Price from '../../common/Price/Price'
import Button from '../../common/Button/Button'
import ShoppingBagDeliveryOptions from '../../common/ShoppingBagDeliveryOptions/ShoppingBagDeliveryOptions'
import DeliveryEstimate from '../../common/DeliveryEstimate/DeliveryEstimate'
import Loader from '../../common/Loader/Loader'
import { browserHistory } from 'react-router'
import QubitReact from 'qubit-react/wrapper'
import {
  sendAnalyticsClickEvent,
  GTM_CATEGORY,
  GTM_ACTION,
  GTM_LABEL,
} from '../../../analytics'
import { selectedDeliveryLocation } from '../../../lib/checkout-utilities/reshaper'
import Peerius from '../../common/Peerius/Peerius'
import { pluck, path } from 'ramda'
import { deliveryDays } from '../../../lib/get-basket-availability'
import constants from '../../../../shared/constants/espotsDesktop'
import { getShoppingBagTotalItems } from '../../../selectors/shoppingBagSelectors'
import {
  isFeatureCFSIEnabled,
  isFeaturePUDOEnabled,
} from '../../../selectors/featureSelectors'
import Espot from '../Espot/Espot'
import {
  closeMiniBag,
  changeDeliveryType,
} from '../../../actions/common/shoppingBagActions'

@connect(
  (state) => ({
    // @NOTE we are connecting to multiple props all from shoppingBag
    // @TODO prop cleaning
    shoppingBag: state.shoppingBag,
    miniBagOpen: state.shoppingBag.miniBagOpen,
    autoClose: state.shoppingBag.autoClose,
    bagProducts: state.shoppingBag.bag.products,
    bagTotal: state.shoppingBag.bag.total,
    bagCount: getShoppingBagTotalItems(state),
    bagSubtotal: state.shoppingBag.bag.subTotal,
    bagDiscounts: state.shoppingBag.bag.discounts,
    bagDeliveryOptions: state.shoppingBag.bag.deliveryOptions,
    loadingShoppingBag: state.shoppingBag.loadingShoppingBag,
    pathname: state.routing.location.pathname,
    inCheckout: state.routing.location.pathname.startsWith('/checkout'),
    orderSummary: state.checkout.orderSummary,
    yourDetails: state.forms.checkout.yourDetails,
    yourAddress: state.forms.checkout.yourAddress,
    isCFSIEnabled: isFeatureCFSIEnabled(state),
    isPUDOEnabled: isFeaturePUDOEnabled(state),
    selectedStore: state.selectedBrandFulfilmentStore,
  }),
  {
    closeMiniBag,
    sendAnalyticsClickEvent,
    changeDeliveryType,
  }
)
export default class MiniBag extends Component {
  static propTypes = {
    bagProducts: PropTypes.array.isRequired,
    shoppingBag: PropTypes.object,
    orderSummary: PropTypes.object,
    yourAddress: PropTypes.object,
    yourDetails: PropTypes.object,
    selectedStore: PropTypes.object,
    bagDiscounts: PropTypes.array,
    bagDeliveryOptions: PropTypes.array,
    inCheckout: PropTypes.bool,
    isCFSIEnabled: PropTypes.bool,
    isPUDOEnabled: PropTypes.bool,
    loadingShoppingBag: PropTypes.bool,
    bagTotal: PropTypes.string,
    closeMiniBag: PropTypes.func,
    sendAnalyticsClickEvent: PropTypes.func.isRequired,
    changeDeliveryType: PropTypes.func,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  state = { isHovered: false }

  autoCloseInterval = null

  AUTO_CLOSE_DELAY_LENGTH = 3000

  componentWillReceiveProps(nextProps) {
    if (
      !this.props.miniBagOpen &&
      nextProps.autoClose &&
      nextProps.miniBagOpen
    ) {
      this.autoCloseInterval = setInterval(
        this.closeMiniBagIfNeeded,
        this.AUTO_CLOSE_DELAY_LENGTH
      )
    } else if (this.props.miniBagOpen && !nextProps.miniBagOpen) {
      this.stopAutoClose()
    }
  }

  closeMiniBagIfNeeded = () => {
    if (!this.state.isHovered) {
      this.stopAutoClose()
      this.props.closeMiniBag()
    }
  }

  stopAutoClose = () => {
    if (this.autoCloseInterval) {
      clearInterval(this.autoCloseInterval)
      this.autoCloseInterval = null
    }
  }

  handleMouseEnter = () => {
    this.setState({ isHovered: true })
  }

  handleMouseLeave = () => {
    this.setState({ isHovered: false })
  }

  scrollMinibag = (pos) => {
    const miniBagContent = this.miniContent
    if (miniBagContent)
      miniBagContent.scrollTop = pos - miniBagContent.offsetHeight
  }

  clickHandler = () => {
    const { sendAnalyticsClickEvent, closeMiniBag } = this.props
    browserHistory.push('/checkout')
    sendAnalyticsClickEvent({
      category: GTM_CATEGORY.SHOPPING_BAG,
      action: GTM_ACTION.CLICKED,
      label: 'checkout-button',
    })
    closeMiniBag()
  }

  renderEmptyProducts() {
    const { l } = this.context
    const { closeMiniBag } = this.props
    return (
      <div className="MiniBag-emptyBag">
        <p className="MiniBag-emptyLabel">{l`Your shopping bag is currently empty.`}</p>
        <Button className="Button--primary" clickHandler={closeMiniBag}>
          {l`Continue shopping`}
        </Button>
      </div>
    )
  }

  renderDelivery = () => {
    const { l } = this.context
    const { bagDeliveryOptions } = this.props

    const selectedDeliveryOption =
      bagDeliveryOptions && bagDeliveryOptions.find(({ selected }) => selected)
    // scrAPI returns no selected option in EU
    const deliveryOptionLabel =
      selectedDeliveryOption && selectedDeliveryOption.label

    return (
      deliveryOptionLabel && (
        <div className="MiniBag-summaryRow">
          <span className="MiniBag-leftCol">{l`Delivery`}:</span>
          <span className="MiniBag-rightCol MiniBag-deliveryOption">
            {deliveryOptionLabel}
          </span>
        </div>
      )
    )
  }

  renderDiscount = () => {
    const { bagDiscounts } = this.props
    const discount = Array.isArray(bagDiscounts)
      ? bagDiscounts.reduce((pre, cur) => {
          return pre + parseFloat(cur.value)
        }, 0)
      : false

    const { l } = this.context

    return discount ? (
      <div className="MiniBag-summaryRow">
        <span className="MiniBag-leftCol">{l`Your discount`}:</span>
        <span className="MiniBag-rightCol MiniBag-discount">
          -<Price price={discount} />
        </span>
      </div>
    ) : null
  }

  renderCheckoutNowButton = () => {
    const { l } = this.context
    return (
      <div className="MiniBag-summaryButtonSection">
        <Button
          className="MiniBag-continueButton"
          clickHandler={this.clickHandler}
        >
          {l`Checkout now`}
        </Button>
      </div>
    )
  }

  renderBackToCheckout = () => {
    const { l } = this.context
    const { closeMiniBag } = this.props
    return (
      <Button
        className="MiniBag-backToCheckout"
        clickHandler={closeMiniBag}
      >{l`Back to checkout`}</Button>
    )
  }
  getDetailsInfo() {
    const { yourDetails } = this.props
    const details = pluck('value', yourDetails.fields)
    return details
  }

  getAddressInfo() {
    const { yourAddress } = this.props
    const address = pluck('value', yourAddress.fields)
    return { ...this.getDetailsInfo(), ...address }
  }

  renderDeliveryEstimate = () => {
    const { orderSummary } = this.props
    const shippingInfo = {
      ...selectedDeliveryLocation(orderSummary.deliveryLocations),
      estimatedDelivery: orderSummary.estimatedDelivery,
    }
    const isHomeDelivery = /HOME/.test(shippingInfo.deliveryType)
    const address = isHomeDelivery
      ? this.getAddressInfo()
      : { ...this.getDetailsInfo(), ...orderSummary.storeDetails }
    const deliveryType =
      orderSummary.deliveryLocations &&
      orderSummary.deliveryLocations.find(({ selected }) => selected)
        .deliveryLocationType

    return (
      <DeliveryEstimate
        className="miniBag"
        shippingInfo={shippingInfo}
        address={address}
        deliveryType={deliveryType}
      />
    )
  }

  getDeliveryDays = () => {
    const { shoppingBag, selectedStore } = this.props
    return deliveryDays(path(['bag'], shoppingBag), selectedStore)
  }

  renderBagContent = () => {
    const { l } = this.context
    const {
      bagProducts,
      shoppingBag,
      changeDeliveryType,
      bagTotal,
      inCheckout,
      pathname,
      isCFSIEnabled,
      isPUDOEnabled,
    } = this.props
    const deliveryDayAvailability = isCFSIEnabled ? this.getDeliveryDays() : {}
    const deliveryEstimateVisible = /\/checkout\/payment/.test(pathname)
    const promotionCodeVisible = !inCheckout

    return (
      <div
        className="MiniBag-content"
        ref={(div) => {
          this.miniContent = div
        }}
      >
        {inCheckout && this.renderBackToCheckout()}
        {!inCheckout && this.renderCheckoutNowButton()}
        <ShoppingBag
          bagProducts={bagProducts}
          scrollMinibag={this.scrollMinibag}
        />
        {deliveryEstimateVisible && this.renderDeliveryEstimate()}
        <Espot
          identifier={constants.miniBag.middle}
          className="MiniBag-espot"
        />
        {promotionCodeVisible && (
          <PromotionCode
            key="PromotionCode"
            isCompactHeader
            gtmCategory={GTM_CATEGORY.SHOPPING_BAG}
            isOpenIfPopulated
            scrollOnPromoCodeError={false}
          />
        )}
        {!inCheckout && (
          // @TODO create a selector to pass the filtered options to ShoppingBagDeliveryOptions
          // then we can only pass `options` & `changeDeliveryType` props
          <ShoppingBagDeliveryOptions
            shoppingBag={shoppingBag}
            changeDeliveryType={changeDeliveryType}
            isCFSIEnabled={isCFSIEnabled}
            isPUDOEnabled={isPUDOEnabled}
            deliveryDayAvailability={deliveryDayAvailability}
          />
        )}
        <div className="MiniBag-summary MiniBag-footer">
          <QubitReact id="qubit-mini-bag-summary">
            <div className="MiniBag-summarySection">
              {this.renderDelivery()}
              {this.renderDiscount()}
              <div tabIndex="0" className="MiniBag-summaryRow MiniBag-bagTotal">
                <span className="MiniBag-leftCol MiniBag-totalCost">
                  {l`Total cost incl. delivery`}:
                </span>
                <span className="MiniBag-rightCol MiniBag-totalCost">
                  <Price price={bagTotal} />
                </span>
              </div>
            </div>
          </QubitReact>
          {!inCheckout && this.renderCheckoutNowButton()}
        </div>
      </div>
    )
  }

  handleClose = () => {
    const { closeMiniBag, sendAnalyticsClickEvent } = this.props
    closeMiniBag()
    sendAnalyticsClickEvent({
      category: GTM_CATEGORY.SHOPPING_BAG,
      action: GTM_ACTION.CLICKED,
      label: GTM_LABEL.CLOSE_BUTTON,
    })
  }

  render() {
    const { l } = this.context
    const {
      bagProducts,
      bagCount,
      loadingShoppingBag,
      currencyCode,
      miniBagOpen,
    } = this.props

    return (
      <div
        role="toolbar"
        className="MiniBag"
        onClick={this.stopAutoClose}
        onTouchStart={this.stopAutoClose}
        onMouseEnter={this.handleMouseEnter}
        onMouseLeave={this.handleMouseLeave}
      >
        <Espot identifier={constants.miniBag.top} className="MiniBag-espot" />
        {miniBagOpen && (
          <Peerius
            type="basket"
            currency={currencyCode}
            bagProducts={bagProducts}
          />
        )}
        <QubitReact id="qubit-MiniBag-header">
          <div>
            <h5 className="MiniBag-header">
              {l`My Bag`}
              {bagCount ? ` (${bagCount})` : null}
            </h5>
            <button
              className="MiniBag-closeButton"
              onClick={this.handleClose}
              role="button"
            >
              <img
                className="MiniBag-closeButtonImg"
                src="/assets/common/images/close-icon.svg"
                alt="close"
              />
            </button>
          </div>
        </QubitReact>
        {loadingShoppingBag ? (
          <Loader />
        ) : !bagProducts.length ? (
          this.renderEmptyProducts()
        ) : (
          this.renderBagContent()
        )}
        <Espot
          identifier={constants.miniBag.bottom}
          className="MiniBag-espot"
        />
      </div>
    )
  }
}
