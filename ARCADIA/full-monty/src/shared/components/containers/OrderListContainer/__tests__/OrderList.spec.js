import React from 'react'
import testComponentHelper from 'test/unit/helpers/test-component'
import OrderList from '../OrderList'

describe('<OrderList />', () => {
  const renderComponent = testComponentHelper(OrderList)
  const initialProps = {
    orders: [
      {
        date: '03 May 2017',
        orderId: 1446704,
        returnPossible: false,
        returnRequested: false,
        status: 'Your order is currently being packed by our warehouse.',
        statusCode: 'C',
        total: '£53.00',
      },
      {
        date: '27 April 2017',
        orderId: 1438912,
        returnPossible: false,
        returnRequested: false,
        status: 'Your order is currently being packed by our warehouse.',
        statusCode: 'C',
        total: '£42.00',
      },
    ],
  }

  describe('@render', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })

    it('with header', () => {
      expect(
        renderComponent({
          ...initialProps,
          header: <div>mockedHeader</div>,
        }).getTree()
      ).toMatchSnapshot()
    })
  })
})
