import React from 'react'
import testComponentHelper from 'test/unit/helpers/test-component'
import OrderListContainer from '../OrderListContainer'

import { TYPE } from '../types'

describe('<OrderListContainer />', () => {
  const renderComponent = testComponentHelper(OrderListContainer)
  const initialProps = {
    className: 'Mocked-className',
    notFoundMessage: 'mockedNotFoundMessage',
    orders: [],
    title: 'Mocked Title',
  }

  const orderProps = {
    ...initialProps,
    type: TYPE.ORDER,
  }

  const returnProps = {
    ...initialProps,
    type: TYPE.RETURN,
  }

  const mockedOrders = [{}]
  const mockedHeader = <div>mockedHeader</div>

  describe('@render', () => {
    describe('order history', () => {
      it('in default state', () => {
        expect(renderComponent(orderProps).getTree()).toMatchSnapshot()
      })

      it('with orders', () => {
        expect(
          renderComponent({
            ...orderProps,
            orders: mockedOrders,
          }).getTree()
        ).toMatchSnapshot()
      })

      it('with orders and header', () => {
        expect(
          renderComponent({
            ...orderProps,
            orders: mockedOrders,
            header: mockedHeader,
          }).getTree()
        ).toMatchSnapshot()
      })
    })

    describe('return history', () => {
      it('in default state', () => {
        expect(renderComponent(returnProps).getTree()).toMatchSnapshot()
      })

      it('with orders', () => {
        expect(
          renderComponent({
            ...returnProps,
            orders: mockedOrders,
          }).getTree()
        ).toMatchSnapshot()
      })

      it('with orders and header', () => {
        expect(
          renderComponent({
            ...returnProps,
            orders: mockedOrders,
            header: mockedHeader,
          }).getTree()
        ).toMatchSnapshot()
      })
    })

    it('throws in case of getting wrong type', () => {
      expect(() =>
        renderComponent({
          ...initialProps,
          type: 'mockedWrongType',
        })
      ).toThrowErrorMatchingSnapshot()
    })
  })
})
