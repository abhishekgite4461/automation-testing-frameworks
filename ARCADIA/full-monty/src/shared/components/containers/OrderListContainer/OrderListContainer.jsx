import PropTypes from 'prop-types'
import React, { Component } from 'react'

import BackToAccountLink from '../MyAccount/BackToAccountLink'
import NotFound from './NotFound'
import OrderList from './OrderList'

import AccountHeader from '../../common/AccountHeader/AccountHeader'

import { TYPE } from './types'

export default class OrderListContainer extends Component {
  static propTypes = {
    className: PropTypes.string.isRequired,
    notFoundMessage: PropTypes.string.isRequired,
    orders: PropTypes.array.isRequired,
    title: PropTypes.string.isRequired,
    type: PropTypes.oneOf(Object.values(TYPE)).isRequired,
    header: PropTypes.element,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  checkTypeProp() {
    const { type } = this.props
    const acceptedTypes = Object.values(TYPE)

    if (!acceptedTypes.includes(type)) {
      throw new Error(
        `Wrong type passed to OrderListContainer (${type}). Accepted types: ${Object.values(
          TYPE
        ).toString()}`
      )
    }
  }

  get hasOrders() {
    const { orders } = this.props
    return Array.isArray(orders) && orders.length > 0
  }

  render() {
    this.checkTypeProp()

    const { l } = this.context
    const {
      className,
      notFoundMessage,
      orders,
      header,
      title,
      type,
    } = this.props

    return (
      <section className={className}>
        <AccountHeader
          link="/my-account"
          label={l`Back to My Account`}
          title={title}
        />
        {this.hasOrders ? (
          <div className="OrderList-inner">
            <OrderList header={header} orders={orders} type={type} />
            <BackToAccountLink text={l`Back to My Account`} />
          </div>
        ) : (
          <NotFound message={notFoundMessage} />
        )}
      </section>
    )
  }
}
