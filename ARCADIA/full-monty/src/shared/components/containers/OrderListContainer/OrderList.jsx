import PropTypes from 'prop-types'
import React, { Component } from 'react'
import OrderElement from './OrderElement'

export default class OrderList extends Component {
  static propTypes = {
    header: PropTypes.element,
    orders: PropTypes.array.isRequired,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  render() {
    const { header, orders, type } = this.props

    return (
      <div className="OrderList">
        {header}
        <ol className="OrderList-list">
          {orders.map((order) => {
            return (
              <OrderElement
                rmaId={order.rmaId}
                key={order.rmaId}
                orderId={order.orderId}
                orderDate={order.date}
                orderTotal={order.total}
                orderStatus={order.status}
                type={type}
              />
            )
          })}
        </ol>
      </div>
    )
  }
}
