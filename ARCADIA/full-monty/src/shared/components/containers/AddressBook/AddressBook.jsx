import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import AddressBookList from './AddressBookList/AddressBookList'
import AddressBookFormWrapper from './AddressBookForm/AddressBookFormWrapper'
import {
  getSavedAddresses,
  getIsNewAddressFormVisible,
} from '../../../selectors/addressBookSelectors'
import * as AddressBookActions from '../../../actions/common/addressBookActions'
import { validateDDPForCountry } from '../../../actions/common/ddpActions'

export class AddressBook extends Component {
  static contextTypes = {
    l: PropTypes.func,
  }
  static propTypes = {
    savedAddresses: PropTypes.arrayOf(PropTypes.object).isRequired,
    isNewAddressFormVisible: PropTypes.bool.isRequired,
    createAddress: PropTypes.func.isRequired,
    deleteAddress: PropTypes.func.isRequired,
    selectAddress: PropTypes.func.isRequired,
    showNewAddressForm: PropTypes.func.isRequired,
    hideNewAddressForm: PropTypes.func.isRequired,
    validateDDPForCountry: PropTypes.func.isRequired,
  }

  componentDidMount() {
    const { savedAddresses, validateDDPForCountry } = this.props
    const selectedAddress = savedAddresses.find((address) => address.selected)
    if (selectedAddress) validateDDPForCountry(selectedAddress.country)
  }

  onSelectAddress = (address) => {
    const { selectAddress, validateDDPForCountry } = this.props
    selectAddress(address).then(() => validateDDPForCountry(address.country))
  }

  render() {
    const { l } = this.context
    const {
      savedAddresses,
      isNewAddressFormVisible,
      createAddress,
      deleteAddress,
      showNewAddressForm,
      hideNewAddressForm,
    } = this.props
    return (
      <section className="AddressBook">
        <h2 className="AddressBook-title">{l`Delivery address`}</h2>
        {savedAddresses.length > 0 && (
          <AddressBookList
            savedAddresses={savedAddresses}
            onDeleteAddress={deleteAddress}
            onSelectAddress={this.onSelectAddress}
          />
        )}
        {isNewAddressFormVisible && (
          <AddressBookFormWrapper
            formType="newAddress"
            onSaveAddress={createAddress}
            onClose={hideNewAddressForm}
          />
        )}
        {!isNewAddressFormVisible && (
          <a
            href=""
            className="AddressBook-addNew"
            onClick={(event) => {
              event.preventDefault()
              showNewAddressForm()
            }}
          >{l`Add New`}</a>
        )}
      </section>
    )
  }
}

export default connect(
  (state) => ({
    savedAddresses: getSavedAddresses(state),
    isNewAddressFormVisible: getIsNewAddressFormVisible(state),
  }),
  {
    createAddress: AddressBookActions.createAddress,
    updateAddress: AddressBookActions.updateAddress,
    deleteAddress: AddressBookActions.deleteAddress,
    selectAddress: AddressBookActions.selectAddress,
    showNewAddressForm: AddressBookActions.showNewAddressForm,
    hideNewAddressForm: AddressBookActions.hideNewAddressForm,
    validateDDPForCountry,
  }
)(AddressBook)
