import { connect } from 'react-redux'
import AddressBookForm from './AddressBookForm'
import {
  getFormNames,
  getAddressBookForm,
  getCountryFor,
  getHasSelectedAddress,
  getHasFoundAddress,
  getIsFindAddressVisible,
} from '../../../../selectors/addressBookSelectors'
import {
  getYourDetailsSchema,
  getYourAddressSchema,
  getFindAddressSchema,
} from '../../../../schemas/validation/addressFormValidationSchema'
import {
  getPostCodeRules,
  getCountriesByAddressType,
} from '../../../../selectors/common/configSelectors'
import { touchedFormField } from '../../../../actions/common/formActions'

export const mapStateToProps = (
  state,
  { formType, onSaveAddress, onClose }
) => {
  const addressType = 'delivery'
  const formNames = getFormNames(formType)
  const addressForm = getAddressBookForm(addressType, formNames.address, state)
  const detailsForm = getAddressBookForm(addressType, formNames.details, state)
  const findAddressForm = getAddressBookForm(
    addressType,
    formNames.findAddress,
    state
  )
  const country = getCountryFor(addressType, formNames.address, state)
  const countries = getCountriesByAddressType(state, addressType)
  const postCodeRules = getPostCodeRules(state, country)
  const detailsValidationSchema = getYourDetailsSchema(country)
  const addressValidationSchema = getIsFindAddressVisible(
    addressType,
    formNames.address,
    country,
    state
  )
    ? {}
    : getYourAddressSchema(postCodeRules, countries)
  const findAddressValidationSchema = getIsFindAddressVisible(
    addressType,
    formNames.address,
    country,
    state
  )
    ? getFindAddressSchema(postCodeRules, {
        hasFoundAddress: getHasFoundAddress(findAddressForm),
        hasSelectedAddress: getHasSelectedAddress(addressForm),
      })
    : {}
  return {
    formType,
    onSaveAddress,
    onClose,
    addressForm,
    detailsForm,
    findAddressForm,
    addressValidationSchema,
    detailsValidationSchema,
    findAddressValidationSchema,
    touchedFormField,
  }
}

export default connect(mapStateToProps)(AddressBookForm)
