import { connect } from 'react-redux'
import AddressFormDefaults from '../../../../common/AddressForm/AddressForm'
import {
  getFormNames,
  getAddressBookForm,
  getCountryFor,
  getIsFindAddressVisible,
} from '../../../../../selectors/addressBookSelectors'

export const mapStateToProps = (state, { formType }) => {
  const addressType = 'delivery'
  const formNames = getFormNames(formType)
  const country = getCountryFor(addressType, formNames.address, state)
  const isFindAddressVisible = getIsFindAddressVisible(
    addressType,
    formNames.address,
    country,
    state
  )
  return {
    addressType,
    formNames,
    country,
    getAddressFormFromState: getAddressBookForm,
    isFindAddressVisible,
  }
}

export default connect(mapStateToProps)(AddressFormDefaults)
