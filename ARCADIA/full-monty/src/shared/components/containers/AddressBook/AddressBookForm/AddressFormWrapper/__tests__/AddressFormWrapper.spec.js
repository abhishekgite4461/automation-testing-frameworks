import AddressFormWrapper, { mapStateToProps } from '../AddressFormWrapper'
import { getAddressBookForm } from '../../../../../../selectors/addressBookSelectors'

describe('AddressFormWrapper', () => {
  const initialProps = {
    formType: 'newAddress',
  }
  it('should wrap AddressForm', () => {
    expect(AddressFormWrapper.WrappedComponent.name).toBe('AddressForm')
  })
  describe('@mapStateToProps', () => {
    it('should set addressType prop', () => {
      const state = {}
      const { addressType } = mapStateToProps(state, initialProps)
      expect(addressType).toEqual('delivery')
    })
    it('should set formNames prop', () => {
      const state = {}
      const { formNames } = mapStateToProps(state, initialProps)
      expect(formNames).toEqual({
        address: 'newAddress',
        findAddress: 'newFindAddress',
        details: 'newDetails',
      })
    })
    it('should set country prop from addressBook form if set', () => {
      const state = {
        account: {
          user: {
            deliveryDetails: {
              address: {
                country: 'United States',
              },
            },
          },
        },
        forms: {
          addressBook: {
            newAddress: {
              fields: {
                country: {
                  value: 'United Kingdom',
                },
              },
            },
          },
        },
      }
      const { country } = mapStateToProps(state, initialProps)
      expect(country).toEqual('United Kingdom')
    })
    it('should set country prop from account when addressBook form country is not set', () => {
      const state = {
        account: {
          user: {
            deliveryDetails: {
              address: {
                country: 'United States',
              },
            },
          },
        },
      }
      const { country } = mapStateToProps(state, initialProps)
      expect(country).toEqual('United States')
    })
    it('should set getAddressFormFromState to getAddressBookForm', () => {
      const state = {}
      const { getAddressFormFromState } = mapStateToProps(state, initialProps)
      expect(getAddressFormFromState).toEqual(getAddressBookForm)
    })
    it('should set isFindAddressVisible prop', () => {
      const state = {
        forms: {
          addressBook: {
            newAddress: {
              fields: {
                isManual: {
                  value: false,
                },
                country: {
                  value: 'United Kingdom',
                },
              },
            },
          },
        },
        config: {
          qasCountries: {
            'United Kingdom': 'UK',
          },
        },
      }
      const { isFindAddressVisible } = mapStateToProps(state, initialProps)
      expect(isFindAddressVisible).toBe(true)
    })
  })
})
