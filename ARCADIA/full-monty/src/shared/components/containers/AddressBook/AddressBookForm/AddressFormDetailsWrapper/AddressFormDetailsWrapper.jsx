import { connect } from 'react-redux'
import AddressFormDetailsDefaults from '../../../../common/AddressFormDetails/AddressFormDetails'
import {
  getFormNames,
  getAddressBookForm,
  getCountryFor,
} from '../../../../../selectors/addressBookSelectors'

export const mapStateToProps = (state, { formType }) => {
  const addressType = 'delivery'
  const formNames = getFormNames(formType)
  const country = getCountryFor(addressType, formNames.address, state)
  return {
    addressType,
    formNames,
    country,
    getAddressFormFromState: getAddressBookForm,
  }
}

export default connect(mapStateToProps)(AddressFormDetailsDefaults)
