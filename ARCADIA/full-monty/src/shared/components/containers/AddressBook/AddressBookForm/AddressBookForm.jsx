import React, { Component } from 'react'
import PropTypes from 'prop-types'
import AddressFormWrapper from './AddressFormWrapper/AddressFormWrapper'
import AddressFormDetailsWrapper from './AddressFormDetailsWrapper/AddressFormDetailsWrapper'
import Button from '../../../common/Button/Button'
import { getFormNames } from '../../../../selectors/addressBookSelectors'
import { pluck, isEmpty } from 'ramda'
import { validate } from '../../../../lib/validator/index'
import { scrollElementIntoView } from '../../../../lib/scroll-helper'

export default class AddressBookForm extends Component {
  static contextTypes = {
    l: PropTypes.func,
  }
  static propTypes = {
    formType: PropTypes.string.isRequired,
    onSaveAddress: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
    addressForm: PropTypes.object.isRequired,
    detailsForm: PropTypes.object.isRequired,
    findAddressForm: PropTypes.object.isRequired,
    addressValidationSchema: PropTypes.object.isRequired,
    detailsValidationSchema: PropTypes.object.isRequired,
    findAddressValidationSchema: PropTypes.object.isRequired,
    touchedFormField: PropTypes.func.isRequired,
  }
  scrollToFirstError = (name) => {
    if (process.browser) {
      const el = document.querySelector(
        `.AddressBookForm .FormComponent-${name}`
      )
      scrollElementIntoView(el, 400, 20)
    }
  }
  validateForm = (validationSchema, addressForm, formName) => {
    const { l } = this.context
    const { touchedFormField, dispatch } = this.props
    const errors = validate(validationSchema, addressForm, l)
    if (!isEmpty(errors)) {
      this.scrollToFirstError(Object.keys(errors)[0])
      Object.keys(validationSchema).forEach((name) =>
        dispatch(touchedFormField(formName, name))
      )
      return false
    }
    return true
  }
  onSubmitForm = (event) => {
    event.preventDefault()
    const {
      onSaveAddress,
      formType,
      addressForm,
      detailsForm,
      findAddressForm,
      addressValidationSchema,
      detailsValidationSchema,
      findAddressValidationSchema,
    } = this.props
    const formNames = getFormNames(formType)
    const address = pluck('value', addressForm.fields)
    const details = pluck('value', detailsForm.fields)
    const findAddress = pluck('value', findAddressForm.fields)
    const isFormValid = [
      this.validateForm(detailsValidationSchema, details, formNames.details),
      this.validateForm(
        findAddressValidationSchema,
        findAddress,
        formNames.findAddress
      ),
      this.validateForm(addressValidationSchema, address, formNames.address),
    ].every((value) => value)
    if (isFormValid) {
      onSaveAddress({
        address,
        nameAndPhone: details,
      })
    }
  }
  render() {
    const { l } = this.context
    const { formType, onClose } = this.props
    return (
      <form className="AddressBookForm">
        <a
          href=""
          className="AddressBookForm-close"
          onClick={(event) => {
            event.preventDefault()
            onClose()
          }}
        >{l`Cancel`}</a>
        <AddressFormWrapper formType={formType} />
        <AddressFormDetailsWrapper formType={formType} />
        <Button
          className="Button Button--primary Button--twoFifthWidth"
          type="submit"
          clickHandler={this.onSubmitForm}
        >
          {l`Save`}
        </Button>
      </form>
    )
  }
}
