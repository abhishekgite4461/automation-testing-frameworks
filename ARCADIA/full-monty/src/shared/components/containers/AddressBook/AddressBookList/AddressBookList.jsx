import React, { Component } from 'react'
import PropTypes from 'prop-types'
import RadioButton from '../../../common/FormComponents/RadioButton/RadioButton'

export default class AddressBookList extends Component {
  static contextTypes = {
    l: PropTypes.func,
  }
  static propTypes = {
    savedAddresses: PropTypes.arrayOf(PropTypes.object).isRequired,
    onDeleteAddress: PropTypes.func.isRequired,
    onSelectAddress: PropTypes.func.isRequired,
  }
  renderItemDetailsAddress(key, value) {
    return value && value.length > 0 ? (
      <p key={key} className="AddressBookList-itemDetailsAddress">
        {value}
      </p>
    ) : null
  }
  render() {
    const { l } = this.context
    const { savedAddresses, onDeleteAddress, onSelectAddress } = this.props
    return (
      <ol className="AddressBookList">
        {savedAddresses.map((savedAddress) => (
          <li
            key={savedAddress.id ? savedAddress.id : savedAddress.addressId}
            className="AddressBookList-item"
          >
            <div className="AddressBookList-itemDetails">
              <RadioButton
                checked={savedAddress.selected}
                name="selectAddress"
                fullWidth
                onChange={() => onSelectAddress(savedAddress)}
              >
                <h3 className="AddressBookList-itemDetailsName">
                  {savedAddress.title} {savedAddress.firstName}{' '}
                  {savedAddress.lastName}
                </h3>
                {[
                  'address1',
                  'address2',
                  'city',
                  'state',
                  'postCode',
                  'country',
                ].map((key) =>
                  this.renderItemDetailsAddress(key, savedAddress[key])
                )}
              </RadioButton>
            </div>
            <div className="AddressBookList-itemActions">
              {savedAddresses.length > 1 &&
                !savedAddress.selected && (
                  <a
                    href=""
                    className="AddressBookList-itemAction AddressBookList-itemDelete"
                    onClick={(event) => {
                      event.preventDefault()
                      onDeleteAddress(savedAddress)
                    }}
                  >{l`Delete`}</a>
                )}
            </div>
          </li>
        ))}
      </ol>
    )
  }
}
