import testComponentHelper from '../../../../../../../test/unit/helpers/test-component'
import RadioButton from '../../../../common/FormComponents/RadioButton/RadioButton'
import AddressBookList from '../AddressBookList'

describe('AddressBookList', () => {
  const renderComponent = testComponentHelper(AddressBookList)
  describe('@render', () => {
    it('should not show the address book list when there are no saved addresses', () => {
      const props = {
        savedAddresses: [],
        onDeleteAddress: jest.fn(),
        onSelectAddress: jest.fn(),
      }

      const { getTree } = renderComponent(props)
      expect(getTree()).toMatchSnapshot()
    })
    it('should show the address book list when there are saved addresses using address.id', () => {
      const props = {
        savedAddresses: [
          {
            id: 843489,
            addressName: 'London, E3 2DS, United Kingdom',
            selected: false,
            address1: '2 Foo Road',
            address2: '',
            city: 'London',
            state: '',
            postcode: 'E3 2DS',
            country: 'United Kingdom',
            title: 'Ms',
            firstName: 'Jane',
            lastName: 'Doe',
            telephone: '01234 567890',
          },
          {
            id: 843490,
            addressName: 'London, EC3N 1JJ, United Kingdom',
            selected: true,
            address1: '101 Acme Road',
            address2: '',
            city: 'London',
            state: '',
            postcode: 'EC3N 1JJ',
            country: 'United Kingdom',
            title: 'Mr',
            firstName: 'Banana',
            lastName: 'Man',
            telephone: '01234 567890',
          },
        ],
        onDeleteAddress: jest.fn(),
        onSelectAddress: jest.fn(),
      }
      const { getTree } = renderComponent(props)
      expect(getTree()).toMatchSnapshot()
    })
    it('should show the address book list when there are saved addresses using address.addressId', () => {
      const props = {
        savedAddresses: [
          {
            addressId: 843489,
            addressName: 'London, E3 2DS, United Kingdom',
            selected: false,
            address1: '2 Foo Road',
            address2: '',
            city: 'London',
            state: '',
            postcode: 'E3 2DS',
            country: 'United Kingdom',
            title: 'Ms',
            firstName: 'Jane',
            lastName: 'Doe',
            telephone: '01234 567890',
          },
          {
            addressId: 843490,
            addressName: 'London, EC3N 1JJ, United Kingdom',
            selected: true,
            address1: '101 Acme Road',
            address2: '',
            city: 'London',
            state: '',
            postcode: 'EC3N 1JJ',
            country: 'United Kingdom',
            title: 'Mr',
            firstName: 'Banana',
            lastName: 'Man',
            telephone: '01234 567890',
          },
        ],
        onDeleteAddress: jest.fn(),
        onSelectAddress: jest.fn(),
      }
      const { getTree } = renderComponent(props)
      expect(getTree()).toMatchSnapshot()
    })
  })
  describe('@events', () => {
    it('should call the onDeleteAddress function when the itemDelete link is clicked using address.id', () => {
      const props = {
        savedAddresses: [
          {
            id: 843490,
            addressName: 'London, EC3N 1JJ, United Kingdom',
            selected: false,
            address1: '101 Acme Road',
            address2: '',
            city: 'London',
            state: '',
            postcode: 'EC3N 1JJ',
            country: 'United Kingdom',
            title: 'Mr',
            firstName: 'Banana',
            lastName: 'Man',
            telephone: '01234 567890',
          },
          {
            id: 843490,
            addressName: 'London, EC3N 1JJ, United Kingdom',
            selected: true,
            address1: '101 Acme Road',
            address2: '',
            city: 'London',
            state: '',
            postcode: 'EC3N 1JJ',
            country: 'United Kingdom',
            title: 'Mr',
            firstName: 'Banana',
            lastName: 'Man',
            telephone: '01234 567890',
          },
        ],
        onDeleteAddress: jest.fn(),
        onSelectAddress: jest.fn(),
      }
      const { wrapper } = renderComponent(props)
      const itemDeleteLink = wrapper
        .find('a.AddressBookList-itemDelete')
        .first()
      itemDeleteLink.simulate('click', { preventDefault: jest.fn() })
      expect(props.onDeleteAddress).toHaveBeenCalledTimes(1)
      expect(props.onDeleteAddress).toHaveBeenCalledWith(
        props.savedAddresses[0]
      )
    })
    it('should call the onDeleteAddress function when the itemDelete link is clicked using address.addressId', () => {
      const props = {
        savedAddresses: [
          {
            addressId: 843490,
            addressName: 'London, EC3N 1JJ, United Kingdom',
            selected: false,
            address1: '101 Acme Road',
            address2: '',
            city: 'London',
            state: '',
            postcode: 'EC3N 1JJ',
            country: 'United Kingdom',
            title: 'Mr',
            firstName: 'Banana',
            lastName: 'Man',
            telephone: '01234 567890',
          },
          {
            addressId: 843490,
            addressName: 'London, EC3N 1JJ, United Kingdom',
            selected: true,
            address1: '101 Acme Road',
            address2: '',
            city: 'London',
            state: '',
            postcode: 'EC3N 1JJ',
            country: 'United Kingdom',
            title: 'Mr',
            firstName: 'Banana',
            lastName: 'Man',
            telephone: '01234 567890',
          },
        ],
        onDeleteAddress: jest.fn(),
        onSelectAddress: jest.fn(),
      }
      const { wrapper } = renderComponent(props)
      const itemDeleteLink = wrapper
        .find('a.AddressBookList-itemDelete')
        .first()
      itemDeleteLink.simulate('click', { preventDefault: jest.fn() })
      expect(props.onDeleteAddress).toHaveBeenCalledTimes(1)
      expect(props.onDeleteAddress).toHaveBeenCalledWith(
        props.savedAddresses[0]
      )
    })
    it('should not render item delete links when there is only one saved address', () => {
      const props = {
        savedAddresses: [
          {
            id: 843490,
            addressName: 'London, EC3N 1JJ, United Kingdom',
            selected: true,
            address1: '101 Acme Road',
            address2: '',
            city: 'London',
            state: '',
            postcode: 'EC3N 1JJ',
            country: 'United Kingdom',
            title: 'Mr',
            firstName: 'Banana',
            lastName: 'Man',
            telephone: '01234 567890',
          },
        ],
        onDeleteAddress: jest.fn(),
        onSelectAddress: jest.fn(),
      }
      const { wrapper } = renderComponent(props)
      const itemDeleteLink = wrapper.find('a.AddressBookList-itemDelete')
      expect(itemDeleteLink.length).toBe(0)
    })
    it('should call the onSelectAddress function when the RadioButton is clicked', () => {
      const props = {
        savedAddresses: [
          {
            id: 843490,
            addressName: 'London, EC3N 1JJ, United Kingdom',
            selected: true,
            address1: '101 Acme Road',
            address2: '',
            city: 'London',
            state: '',
            postcode: 'EC3N 1JJ',
            country: 'United Kingdom',
            title: 'Mr',
            firstName: 'Banana',
            lastName: 'Man',
            telephone: '01234 567890',
          },
        ],
        onDeleteAddress: jest.fn(),
        onSelectAddress: jest.fn(),
      }
      const { wrapper } = renderComponent(props)
      const radioButton = wrapper.find(RadioButton).first()
      radioButton.prop('onChange')()
      expect(props.onSelectAddress).toHaveBeenCalledTimes(1)
      expect(props.onSelectAddress).toHaveBeenCalledWith(
        props.savedAddresses[0]
      )
    })
  })
})
