import testComponentHelper from '../../../../../../test/unit/helpers/test-component'
import { AddressBook } from '../AddressBook'
import AddressBookList from '../AddressBookList/AddressBookList'
import AddressBookFormWrapper from '../AddressBookForm/AddressBookFormWrapper'

describe('AddressBook', () => {
  const renderComponent = testComponentHelper(AddressBook)
  describe('@render', () => {
    it('should not show the address book list when there are no saved addresses', () => {
      const props = {
        savedAddresses: [],
        isNewAddressFormVisible: false,
        createAddress: jest.fn(),
        updateAddress: jest.fn(),
        deleteAddress: jest.fn(),
        selectAddress: jest.fn(),
        showNewAddressForm: jest.fn(),
        hideNewAddressForm: jest.fn(),
        getAccount: jest.fn(),
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find(AddressBookList)).toHaveLength(0)
    })
    it('should show the address book list when there are saved addresses', () => {
      const props = {
        savedAddresses: [{ id: 843489 }],
        isNewAddressFormVisible: false,
        createAddress: jest.fn(),
        updateAddress: jest.fn(),
        deleteAddress: jest.fn(),
        selectAddress: jest.fn(),
        showNewAddressForm: jest.fn(),
        hideNewAddressForm: jest.fn(),
        getAccount: jest.fn(),
      }
      const { instance, wrapper } = renderComponent(props)
      expect(wrapper.find(AddressBookList)).toHaveLength(1)
      expect(
        wrapper
          .find(AddressBookList)
          .first()
          .prop('onSelectAddress')
      ).toBe(instance.onSelectAddress)
    })
    it('should show the new address book form when isNewAddressFormVisible is true', () => {
      const props = {
        savedAddresses: [],
        isNewAddressFormVisible: true,
        createAddress: jest.fn(),
        updateAddress: jest.fn(),
        deleteAddress: jest.fn(),
        selectAddress: jest.fn(),
        showNewAddressForm: jest.fn(),
        hideNewAddressForm: jest.fn(),
        getAccount: jest.fn(),
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find(AddressBookFormWrapper)).toHaveLength(1)
    })
    it('should not show the new address book form when isNewAddressFormVisible is false', () => {
      const props = {
        savedAddresses: [],
        isNewAddressFormVisible: false,
        createAddress: jest.fn(),
        updateAddress: jest.fn(),
        deleteAddress: jest.fn(),
        selectAddress: jest.fn(),
        showNewAddressForm: jest.fn(),
        hideNewAddressForm: jest.fn(),
        getAccount: jest.fn(),
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find(AddressBookFormWrapper)).toHaveLength(0)
    })
  })
  describe('@events', () => {
    it('should call the showNewAddressForm function when the addNew link is clicked', () => {
      const props = {
        savedAddresses: [],
        isNewAddressFormVisible: false,
        createAddress: jest.fn(),
        updateAddress: jest.fn(),
        deleteAddress: jest.fn(),
        selectAddress: jest.fn(),
        showNewAddressForm: jest.fn(),
        hideNewAddressForm: jest.fn(),
        getAccount: jest.fn(),
      }
      const { wrapper } = renderComponent(props)
      const addNewLink = wrapper.find('a.AddressBook-addNew').first()
      const mockEvent = { preventDefault: jest.fn() }
      addNewLink.simulate('click', mockEvent)
      expect(mockEvent.preventDefault).toHaveBeenCalled()
      expect(props.showNewAddressForm).toHaveBeenCalledTimes(1)
    })

    it('should call `validateDDPForCountry` if selected address is changed', async () => {
      const selectAddressMock = jest.fn(() => Promise.resolve())
      const validateDDPForCountryMock = jest.fn()
      const props = {
        savedAddresses: [{ country: 'France' }, { country: 'United Kingdom' }],
        selectAddress: selectAddressMock,
        validateDDPForCountry: validateDDPForCountryMock,
      }
      const address = props.savedAddresses[0]
      const { instance } = renderComponent(props)

      await instance.onSelectAddress(address)
      expect(selectAddressMock).toHaveBeenCalledWith(address)
      expect(validateDDPForCountryMock).toHaveBeenCalledWith('France')
    })
  })

  describe('@lifecycle', () => {
    describe('componentDidMount', () => {
      it('should call `validateDDPForCountry` with country of selected address', () => {
        const validateDDPForCountryMock = jest.fn()
        const props = {
          savedAddresses: [
            { country: 'France', selected: true },
            { country: 'United Kingdom', selected: false },
          ],
          validateDDPForCountry: validateDDPForCountryMock,
        }
        const { instance } = renderComponent(props)

        instance.componentDidMount()
        expect(validateDDPForCountryMock).toHaveBeenCalledWith('France')
      })

      it('should not call `validateDDPForCountry` if no selected address', () => {
        const validateDDPForCountryMock = jest.fn()
        const props = {
          savedAddresses: [
            { country: 'France', selected: false },
            { country: 'United Kingdom', selected: false },
          ],
          validateDDPForCountry: validateDDPForCountryMock,
        }
        const { instance } = renderComponent(props)

        instance.componentDidMount()
        expect(validateDDPForCountryMock).not.toHaveBeenCalled()
      })
    })
  })
})
