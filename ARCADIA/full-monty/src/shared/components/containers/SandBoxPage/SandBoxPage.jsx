import React, { Component } from 'react'
import analyticsDecorator from '../../../../client/lib/analytics/analytics-decorator'
import SandBox from '../SandBox/SandBox'
import { GTM_CATEGORY } from '../../../analytics'

@analyticsDecorator(GTM_CATEGORY.MR_CMS, {
  isAsync: true,
})
export default class SandBoxPage extends Component {
  static needs = SandBox.needs

  render() {
    return <SandBox {...this.props} />
  }
}
