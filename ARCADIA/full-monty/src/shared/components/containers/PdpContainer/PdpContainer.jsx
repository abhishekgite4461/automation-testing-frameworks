import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { connect } from 'react-redux'
import { path } from 'ramda'

import ProductDetail from '../ProductDetail/ProductDetail'
import Bundles from '../Bundles/Bundles'
import SeoSchema from '../../common/SeoSchema/SeoSchema'
import NoSearchResults from '../../common/NoSearchResults/NoSearchResults'
import Loader from '../../common/Loader/Loader'
import Espot from '../Espot/Espot'
import AccessibleText from '../../common/AccessibleText/AccessibleText'
import BackToTop from '../../common/BackToTop/BackToTop'
import ProductsBreadcrumbs from '../../common/ProductsBreadCrumbs/ProductsBreadCrumbs'

import * as productsActions from '../../../actions/common/productsActions'
import { initCarousel } from '../../../actions/common/carousel-actions'
import * as sandBoxActions from '../../../actions/common/sandBoxActions'
import espots from '../../../constants/espotsMobile'
import cmsConsts from '../../../constants/cmsConsts'
import { updateUniversalVariable } from '../../../lib/analytics/qubit-analytics'
import { geoIPActions } from '../../../reducers/common/geoIPReducer'

import { getGlobalEspotName } from '../../../../shared/selectors/espotSelectors'
import { getPdpBreadcrumbs } from '../../../../shared/selectors/productSelectors'

@connect(
  (state) => ({
    product: state.currentProduct,
    brandCode: state.config.brandCode,
    region: state.config.region,
    scrollToTopFeature: state.features.status.FEATURE_PDP_SCROLL_TO_TOP,
    globalEspotName: getGlobalEspotName(state),
    breadcrumbs: getPdpBreadcrumbs(state),
  }),
  {
    ...productsActions,
    initCarousel,
    ...sandBoxActions,
    updateUniversalVariable,
  }
)
export default class PdpContainer extends Component {
  static propTypes = {
    product: PropTypes.object,
    params: PropTypes.object,
    location: PropTypes.object,
    getProduct: PropTypes.func,
    clearProduct: PropTypes.func,
    showTacticalMessage: PropTypes.func,
    hideTacticalMessage: PropTypes.func,
    updateUniversalVariable: PropTypes.func,
    scrollToTopFeature: PropTypes.bool,
    globalEspotName: PropTypes.string.isRequired,
    breadcrumbs: PropTypes.array.isRequired,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentWillMount() {
    const { params, getProduct, updateUniversalVariable } = this.props
    if (process.browser) getProduct({ ...params }).then(updateUniversalVariable)
  }

  componentDidMount() {
    if (window) window.scrollTo(0, 0)
    this.props.showTacticalMessage()
    // Hook for the Qubit tag "iProspect Floodlight PDP"
    document.dispatchEvent(new Event('landedOnPDP'))
  }

  componentWillReceiveProps({ getProduct, params, location }) {
    if (this.props.location.pathname === location.pathname) return null

    const { l } = this.context
    const { updateUniversalVariable } = this.props

    if (location.pathname === '/webapp/wcs/stores/servlet/ProductDisplay') {
      // [MJI-1077] We need to handle the deprecated PDP URLs:
      // /webapp/wcs/stores/servlet/ProductDisplay?langId=-1&catalogId=33057&storeId=12556&productId=32409407.
      const productId = path(['query', 'productId'], location)
      const p = { identifier: productId }

      return getProduct(p).then(updateUniversalVariable)
    }

    if (location.pathname.includes(`/${l`product`}/`)) {
      getProduct({ ...params }).then(updateUniversalVariable)
    }
  }

  componentDidUpdate({ product }) {
    if (this.props.product !== product && window) window.scrollTo(0, 0)
  }

  componentWillUnmount() {
    const { clearProduct, hideTacticalMessage } = this.props
    clearProduct()
    hideTacticalMessage()
  }

  static needs = [
    initCarousel.bind('', 'productDetail', 1),
    initCarousel.bind('', 'bundles', 1),
    (...args) => (dispatch, getState) =>
      dispatch(productsActions.getProduct(...args)).then(() =>
        dispatch(
          geoIPActions.setRedirectURLForPDP(getState().currentProduct.grouping)
        )
      ),
    sandBoxActions.showTacticalMessage,
    () =>
      sandBoxActions.getContent(
        null,
        espots.pdp[0],
        cmsConsts.ESPOT_CONTENT_TYPE
      ),
    () =>
      sandBoxActions.getContent(
        null,
        espots.pdp[1],
        cmsConsts.ESPOT_CONTENT_TYPE
      ),
  ]

  render() {
    const {
      product,
      location,
      scrollToTopFeature,
      globalEspotName,
      breadcrumbs,
    } = this.props
    const { l } = this.context

    const loader = (
      <div>
        <AccessibleText>{l`Product information loading`}</AccessibleText>
        <Loader />
      </div>
    )

    if (!product) return loader

    const {
      success,
      name,
      productId,
      isBundleOrOutfit,
      sourceUrl,
      metaDescription,
    } = this.props.product

    if (success === false) return <NoSearchResults />

    if (!productId) return loader

    return (
      <div className="container PdpContainer">
        <Helmet
          title={name}
          meta={[{ name: 'description', content: metaDescription }]}
          link={[{ rel: 'canonical', href: sourceUrl }]}
        />
        <SeoSchema
          location={location}
          type="Product"
          data={this.props.product}
        />
        {!isBundleOrOutfit && (
          <AccessibleText
          >{l`This is the product page for: ${name}`}</AccessibleText>
        )}
        <Espot key="pdpGlobalEspot" identifier={globalEspotName} />
        <ProductsBreadcrumbs breadcrumbs={breadcrumbs} />
        {isBundleOrOutfit ? (
          <Bundles product={product} />
        ) : (
          <ProductDetail product={product} location={location} />
        )}
        {scrollToTopFeature && <BackToTop />}
      </div>
    )
  }
}
