import testComponentHelper from '../../../../../../test/unit/helpers/test-component'
import ProductMock from '../../../../../../test/mocks/product-detail'

import ProductDetail from '../../ProductDetail/ProductDetail'
import Bundles from '../../Bundles/Bundles'
import Helmet from 'react-helmet'
import SeoSchema from '../../../common/SeoSchema/SeoSchema'
import Loader from '../../../common/Loader/Loader'
import NoSearchResults from '../../../common/NoSearchResults/NoSearchResults'

import PdpContainer from '../PdpContainer'
import BackToTop from '../../../common/BackToTop/BackToTop'
import ProductsBreadCrumbs from '../../../common/ProductsBreadCrumbs/ProductsBreadCrumbs'

import * as productsActions from '../../../../actions/common/productsActions'
import { geoIPActions } from '../../../../reducers/common/geoIPReducer'
import * as sandBoxActions from '../../../../actions/common/sandBoxActions'

jest.mock('../../../../actions/common/productsActions', () => ({
  getProduct: jest.fn(),
}))

jest.mock('../../../../reducers/common/geoIPReducer', () => ({
  geoIPActions: {
    setRedirectURLForPDP: jest.fn(),
  },
}))

jest.mock('../../../../actions/common/sandBoxActions', () => ({
  showTacticalMessage: jest.fn(),
  getContent: jest.fn(),
}))

describe('PdpContainer', () => {
  const props = {
    product: ProductMock,
    getProduct: jest.fn(() => Promise.resolve()),
    clearProduct: jest.fn(),
    scrollToTopFeature: false,
    params: {
      productId: '123123',
    },
    location: {
      pathname: '/',
    },
    showTacticalMessage: jest.fn(),
    hideTacticalMessage: jest.fn(),
    globalEspotName: 'globalEspotName',
    breadcrumbs: [],
    updateUniversalVariable: jest.fn(),
  }
  const renderComponent = testComponentHelper(PdpContainer.WrappedComponent, {
    disableLifecycleMethods: false,
  })

  beforeAll(() => {
    global.window.scrollTo = jest.fn()
  })

  beforeEach(() => {
    props.getProduct.mockReturnValueOnce(Promise.resolve())
    jest.clearAllMocks()
  })

  afterAll(() => {
    jest.resetAllMocks()
  })

  describe('when the component mounts', () => {
    it(
      'gets the products, scrolls to the top of the page, displays the tactical message and hooks Qubit tag ' +
        '"iProspect Floodlight PDP"',
      async () => {
        const browser = process.browser
        process.browser = true
        jest.spyOn(global.document, 'dispatchEvent')

        const { wrapper } = await renderComponent(props)

        expect(wrapper.find('.PdpContainer')).toBeTruthy()

        expect(props.getProduct).toHaveBeenCalledWith(props.params)
        expect(props.updateUniversalVariable).toHaveBeenCalled()

        expect(global.window.scrollTo).toHaveBeenCalledWith(0, 0)
        expect(props.showTacticalMessage).toHaveBeenCalled()
        expect(global.document.dispatchEvent).toHaveBeenCalled()
        process.browser = browser
      }
    )
  })

  describe('when the component unmounts', () => {
    it('hides the tactical message and clears the product ', async () => {
      const { wrapper } = await renderComponent(props)

      wrapper.unmount()

      expect(props.clearProduct).toHaveBeenCalled()
      expect(props.hideTacticalMessage).toHaveBeenCalled()
    })
  })

  describe('when the component updates', () => {
    it('gets the new product details if the url contains "/product/" and is different than the previous one', async () => {
      let p
      const { wrapper } = await renderComponent(props)
      const nextProps = {
        location: {
          pathname: '/product/',
        },
        params: {
          productId: '123145',
        },
        getProduct: jest.fn(() => {
          p = Promise.resolve()
          return p
        }),
        product: {
          productId: '123145',
        },
      }

      wrapper.setProps({ ...nextProps })
      await p

      expect(nextProps.getProduct).toHaveBeenCalled()
      expect(props.updateUniversalVariable).toHaveBeenCalled()

      expect(global.window.scrollTo.mock.calls.length).toEqual(2)
      expect(global.window.scrollTo).toHaveBeenCalledWith(0, 0)
    })

    it('gets the new product details for PDP deprecated URL /webapp/wcs/stores/servlet/ProductDisplay', async () => {
      let p
      const { wrapper } = await renderComponent(props)
      const nextProps = {
        location: {
          pathname: '/webapp/wcs/stores/servlet/ProductDisplay',
          query: {
            productId: '123',
          },
        },
        getProduct: jest.fn(() => {
          p = Promise.resolve()
          return p
        }),
      }

      wrapper.setProps({ ...nextProps })
      await p

      expect(nextProps.getProduct).toHaveBeenCalledTimes(1)
      expect(nextProps.getProduct).toHaveBeenCalledWith({ identifier: '123' })
      expect(props.updateUniversalVariable).toHaveBeenCalled()
    })

    it('does not refetch the new product details if the same product is passed as a prop', async () => {
      const { wrapper } = await renderComponent(props)
      const nextProps = {
        product: ProductMock,
        getProduct: jest.fn(() => Promise.resolve()),
        params: {
          productId: '123123',
        },
        location: {
          pathname: '/',
        },
      }

      wrapper.setProps({ ...nextProps })

      expect(nextProps.getProduct).not.toHaveBeenCalled()

      expect(global.window.scrollTo.mock.calls.length).toEqual(1)
    })
  })

  describe('@render', () => {
    it('displays product detail child', () => {
      const { wrapper } = renderComponent(props)

      expect(wrapper.find(ProductDetail).props()).toEqual({
        product: props.product,
        location: { pathname: '/' },
      })
    })

    it('displays ProductsBreadCrumbs', () => {
      const { wrapper } = renderComponent(props)
      expect(wrapper.find(ProductsBreadCrumbs)).toHaveLength(1)
    })

    it('displays bundles child', () => {
      const product = Object.assign({}, props.product, {
        isBundleOrOutfit: true,
      })
      const { wrapper } = renderComponent({ ...props, product })

      expect(wrapper.find(Bundles).props()).toEqual({ product })
    })

    it('sets helmet data', () => {
      const { wrapper } = renderComponent(props)

      const {
        product: { name, metaDescription },
      } = props
      const expected = {
        title: name,
        meta: [{ name: 'description', content: metaDescription }],
        link: [{ rel: 'canonical', href: ProductMock.sourceUrl }],
      }

      expect(wrapper.find(Helmet).props()).toEqual(expected)
    })

    it('<PdpContainer /> includes SeoSchema for product', () => {
      const { wrapper } = renderComponent(props)
      const { location, product } = props
      const expected = {
        location,
        type: 'Product',
        data: product,
      }

      expect(wrapper.find(SeoSchema).props()).toEqual(expected)
    })

    it('should not enable back-to-top button when feature is disabled', () => {
      const { wrapper } = renderComponent({
        ...props,
        scrollToTopFeature: false,
      })
      expect(wrapper.find(BackToTop).length).toBe(0)
    })

    it('should not enable back-to-top button when feature is enabled', () => {
      const { wrapper } = renderComponent({
        ...props,
        scrollToTopFeature: true,
      })
      expect(wrapper.find(BackToTop).length).toBe(1)
    })

    describe('loader', () => {
      it('should be displayed when not product is provided', () => {
        const { wrapper } = renderComponent({
          ...props,
          product: null,
        })

        expect(wrapper.find(Loader).length).toBe(1)
      })

      it('should be displayed when not productId is provided', () => {
        const { wrapper } = renderComponent({
          ...props,
          product: {
            success: true,
            productId: null,
          },
        })

        expect(wrapper.find(Loader).length).toBe(1)
      })
    })

    describe('no search results', () => {
      it('should be displayed when not product.success is false', () => {
        const { wrapper } = renderComponent({
          ...props,
          product: {
            success: false,
          },
        })

        expect(wrapper.find(NoSearchResults).length).toBe(1)
      })
    })
  })

  describe('block render until', () => {
    it('gets the products and sets the redirect URL for PDP', async () => {
      const grouping = 'gr'
      const dispatchMock = jest.fn(() => Promise.resolve())
      const getStateMock = jest.fn(() => ({
        currentProduct: {
          grouping,
        },
      }))

      await PdpContainer.needs[2]()(dispatchMock, getStateMock)

      expect(productsActions.getProduct).toHaveBeenCalled()
      expect(dispatchMock.mock.calls.length).toEqual(2)
      expect(geoIPActions.setRedirectURLForPDP).toHaveBeenCalledWith(grouping)
    })

    it('it gets the mobilePDPESpotPos1 CMS page', async () => {
      await PdpContainer.needs[4]()

      expect(sandBoxActions.getContent).toHaveBeenCalledWith(
        null,
        'mobilePDPESpotPos1',
        'espot'
      )
    })

    it('it gets the mobilePDPESpotPos2 CMS page', async () => {
      await PdpContainer.needs[5]()

      expect(sandBoxActions.getContent).toHaveBeenCalledWith(
        null,
        'mobilePDPESpotPos2',
        'espot'
      )
    })
  })
})
