import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'
import CmsPage from '../../common/CmsPage/CmsPage'
import * as cmsActions from '../../../actions/common/cmsActions'

@connect(
  (state) => ({
    error404: state.cms.pages.error404,
  }),
  cmsActions
)
export default class NotFound extends Component {
  static propTypes = {
    error404: PropTypes.object,
    getContent: PropTypes.func,
  }

  componentDidMount() {
    this.props.getContent({ cmsPageName: 'error404' })
  }

  static needs = [() => cmsActions.getContent({ cmsPageName: 'error404' })]

  render() {
    return (
      <div
        className="NotFound"
        style={{ maxWidth: '1199px', margin: '0 auto' }}
      >
        <Helmet
          title="404 - Page not found"
          meta={[
            { name: 'description', content: 'Page not found' },
            { name: 'ROBOTS', content: 'NOINDEX, NOFOLLOW' },
          ]}
        />
        <CmsPage page={this.props.error404} />
      </div>
    )
  }
}
