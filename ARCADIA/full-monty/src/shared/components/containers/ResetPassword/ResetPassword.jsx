/* eslint-disable react/no-did-mount-set-state */
/**
 * This is the reset password link component, which is meant to be used to
 * handle the reset password link that gets sent to a user via an email
 * when the /api/account/reset-password-link endpoint is hit.
 *
 * NOTE: the link within the email goes to legacy desktop and comes out
 * in the following format:
 * https://ts.pplive.arcadiagroup.ltd.uk/webapp/wcs/stores/servlet/ResetPasswordLink?storeId=12556&token=iaresean%40gmail.com&hash=PTDX9O3cs1pqfZabugt1Y4xOjec%3D%0A&catalogId=33057&langId=-1&stop_mobi=yes&CMPID=EML_SER_4
 *
 * All the query string paramaters from the link need to be forwarded
 * to the rewritten URL.
 */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose, path, prop, isEmpty, isNil, mapObjIndexed } from 'ramda'
import analyticsDecorator from '../../../../client/lib/analytics/analytics-decorator'
import Input from '../../common/FormComponents/Input/Input'
import Button from '../../common/Button/Button'
import Message from '../../common/FormComponents/Message/Message'
import { validate } from '../../../lib/validator'
import * as WindowUtils from '../../../lib/window'
import * as Validators from '../../../lib/validator/validators'
import * as FormActions from '../../../actions/common/formActions'
import * as ResetPasswordActions from '../../../actions/components/ResetPasswordActions'
import * as AccountActions from '../../../actions/common/accountActions'
import * as FormSelectors from '../../../selectors/formsSelectors'
import * as ResetPasswordSelectors from '../../../selectors/resetPasswordSelectors'

const isNilOrEmpty = (x) => isNil(x) || isEmpty(x)

export class ResetPassword extends Component {
  static propTypes = {
    leavingResetPasswordForm: PropTypes.func,
    location: PropTypes.object,
    passwordResetSuccessfully: PropTypes.bool,
    resetForm: PropTypes.func,
    resetPasswordForm: PropTypes.object,
    resetPasswordRequest: PropTypes.func,
    resetPasswordLinkRequest: PropTypes.func,
    setFormField: PropTypes.func,
    setFormMessage: PropTypes.func,
    touchedFormField: PropTypes.func,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  constructor(props) {
    super(props)
    this.state = {
      invalidEmailData: false,
      emailData: undefined,
    }
  }

  componentWillMount() {
    // Parse the query string to ensure we have all the expected parameters from
    // the respective email link
    const { location } = this.props
    const { query } = location
    const { hash, token } = query
    if (isNilOrEmpty(hash) || isNilOrEmpty(token)) {
      this.setState(() => ({ invalidEmailData: true }))
      return
    }

    // Store the minimum set of fields we are required to forward on the
    // reset password handler in order to complete the reset password action.
    this.setState(() => ({
      emailData: {
        hash,
        token,
      },
    }))

    // Set up the form validator
    const { l } = this.context
    const { setFormField } = this.props
    this.validationSchema = {
      // For validation to work we need to add the email address
      // to the form model so that it can be referred to by the
      // isNotSameAs helpers.
      email: 'email',
      password: [
        'password',
        Validators.isNotSameAs(
          l`Please ensure that your password does not contain your email address.`,
          'email'
        ),
      ],
      passwordConfirm: [
        'password',
        Validators.isSameAs(
          l`Please ensure that both passwords match.`,
          'password'
        ),
        Validators.isNotSameAs(
          l`Please ensure that your password does not contain your email address.`,
          'email'
        ),
      ],
    }

    // Ensure we use the email address from the token against our email
    // field so that validation works as expected
    setFormField('resetPassword', 'email', token)
  }

  componentDidMount() {
    // Focus on the new password field. We have to do a setTimeout trick
    // otherwise the focus does not work.
    setTimeout(() => {
      const input = path(
        [
          'passwordEl',
          // The input is wrapped by redux 😭 so we need this additional tapping
          // into in order to get the actual instance.
          'refs',
          'wrappedInstance',
        ],
        this
      )
      if (input) {
        input.setFocus()
      }
    })
  }

  componentWillReceiveProps(nextProps) {
    const { passwordResetSuccessfully } = nextProps
    if (passwordResetSuccessfully) {
      WindowUtils.changeURL('/my-account')
    }
  }

  componentWillUnmount() {
    const { resetForm, setFormMessage, leavingResetPasswordForm } = this.props
    leavingResetPasswordForm()
    setFormMessage('resetPassword', {})
    resetForm('resetPassword', {
      email: '',
      password: '',
      passwordConfirm: '',
    })
  }

  onSubmit = (e) => {
    e.preventDefault()
    const { resetPasswordRequest, resetPasswordForm } = this.props
    const { fields } = resetPasswordForm
    const { password, passwordConfirm } = fields
    const { emailData } = this.state
    const { hash, CMPID } = emailData
    resetPasswordRequest({
      CMPID,
      email: emailData.token,
      hash: encodeURIComponent(hash),
      password: password.value,
      passwordConfirm: passwordConfirm.value,
    })
  }

  onGetAnotherLink = (e) => {
    e.preventDefault()
    const { resetPasswordLinkRequest } = this.props
    const { emailData } = this.state
    const data = { email: decodeURIComponent(emailData.token) }
    resetPasswordLinkRequest(data)
  }

  passwordRef = (el) => {
    if (el == null) return
    this.passwordEl = el
  }

  render() {
    const { invalidEmailData } = this.state
    const { l } = this.context

    if (invalidEmailData) {
      return (
        <section className="ResetPassword">
          <div className="ResetPassword-form">
            <h3>{l`Reset password`}</h3>
            <Message
              message={l`This reset password link is invalid.`}
              type="error"
            />
          </div>
        </section>
      )
    }

    const {
      forgetPasswordForm,
      resetPasswordForm,
      setFormField,
      touchedFormField,
      success,
    } = this.props
    const setField = (name) => (e) =>
      setFormField('resetPassword', name, e.target.value)
    const touchedField = (name) => () => touchedFormField('resetPassword', name)
    const errors = validate(
      this.validationSchema,
      mapObjIndexed(prop('value'), resetPasswordForm.fields),
      l
    )
    const formMessage = path(['message', 'message'], resetPasswordForm)
    const formMessageType = path(['message', 'type'], resetPasswordForm)

    return (
      <section className="ResetPassword">
        <form onSubmit={this.onSubmit} className="ResetPassword-form">
          <h3>{l`Reset password`}</h3>
          <p
          >{l`Passwords must have at least six characters including one digit.`}</p>
          <Input
            errors={errors}
            field={resetPasswordForm.fields.password}
            isDisabled={success}
            isRequired
            label={l`New password`}
            name="password"
            placeholder={l`New password`}
            ref={this.passwordRef}
            setField={setField}
            touchedField={touchedField}
            type="password"
          />
          <Input
            errors={errors}
            field={resetPasswordForm.fields.passwordConfirm}
            isDisabled={success}
            isRequired
            label={l`Confirm new password`}
            name="passwordConfirm"
            placeholder={l`Confirm new password`}
            setField={setField}
            touchedField={touchedField}
            type="password"
          />
          <Button
            className="ResetPassword-saveChanges"
            isDisabled={!isEmpty(errors) || success}
            type="submit"
          >
            {l`SAVE CHANGES`}
          </Button>
          <Message message={formMessage} type={formMessageType} />
          <hr className="ResetPassword-seperator" />
          {/* eslint-disable jsx-a11y/label-has-for */}
          <label className="ResetPassword-label">{l`Not working?`}</label>
          {/* eslint-enable jsx-a11y/label-has-for */}
          <Button
            className="ResetPassword-getAnother"
            clickHandler={this.onGetAnotherLink}
          >
            {l`Get another link`}
          </Button>
          <Message
            message={path(['message', 'message'], forgetPasswordForm)}
            type={path(['message', 'type'], forgetPasswordForm)}
          />
        </form>
      </section>
    )
  }
}

export default compose(
  analyticsDecorator('reset-password'),
  connect(
    (state) => ({
      forgetPasswordForm: FormSelectors.getForgetPasswordForm(state),
      resetPasswordForm: FormSelectors.getResetPasswordForm(state),
      passwordResetSuccessfully: ResetPasswordSelectors.getSuccess(state),
    }),
    {
      leavingResetPasswordForm: ResetPasswordActions.leavingResetPasswordForm,
      resetForm: FormActions.resetForm,
      resetPasswordRequest: AccountActions.resetPasswordRequest,
      resetPasswordLinkRequest: AccountActions.resetPasswordLinkRequest,
      setFormField: FormActions.setFormField,
      setFormMessage: FormActions.setFormMessage,
      touchedFormField: FormActions.touchedFormField,
    }
  )
)(ResetPassword)
