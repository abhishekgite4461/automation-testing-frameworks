import testComponentHelper, {
  analyticsDecoratorHelper,
} from '../../../../../../test/unit/helpers/test-component'
import DecoratedResetPassword, { ResetPassword } from '../ResetPassword'
import { browserHistory } from 'react-router'
import * as WindowUtils from '../../../../lib/window'

jest.mock('react-router', () => ({
  browserHistory: jest.fn(),
}))

jest.mock('../../../../lib/window', () => ({
  changeURL: jest.fn(),
}))

describe('ResetPassword', () => {
  const emptyForm = {
    fields: {
      email: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      password: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
      passwordConfirm: {
        value: '',
        isDirty: false,
        isTouched: false,
        isFocused: false,
      },
    },
    isLoading: false,
    errors: {},
    message: {},
  }

  const invalidForm = {
    fields: {
      email: {
        value: 'foobar.com',
        isDirty: true,
        isTouched: true,
        isFocused: false,
      },
      password: {
        value: 'Arcadia',
        isDirty: true,
        isTouched: true,
        isFocused: false,
      },
      passwordConfirm: {
        value: 'Arcadia2',
        isDirty: true,
        isTouched: true,
        isFocused: false,
      },
    },
    message: {
      type: 'error',
      message: 'Password needs to be changed',
    },
  }

  const validForm = {
    fields: {
      email: {
        value: 'foo@bar.com',
        isDirty: true,
        isTouched: false,
        isFocused: false,
      },
      password: {
        value: 'Arcadia2',
        isDirty: true,
        isTouched: true,
        isFocused: false,
      },
      passwordConfirm: {
        value: 'Arcadia2',
        isDirty: true,
        isTouched: true,
        isFocused: false,
      },
    },
    message: {
      type: 'confirm',
      message: '🎉',
    },
  }

  const initialProps = {
    leavingResetPasswordForm: jest.fn(),
    passwordResetSuccessfully: false,
    resetForm: jest.fn(),
    resetPasswordForm: emptyForm,
    resetPasswordLinkRequest: jest.fn(),
    resetPasswordRequest: jest.fn(),
    setFormField: jest.fn(),
    setFormMessage: jest.fn(),
    touchedFormField: jest.fn(),
    location: {
      query: {
        // NOTE: WCS returns us back a strangely encoded hash containing
        // a new line character at the end of it. Keep this data below
        // as close to the actual data as possible as we need to test
        // the process where we re-encode the hash and token before sending
        // the request.  The reset password endpoint requires that both
        // these pieces of data be url encoded.
        hash: 'PTDX9O3cs1pqfZabugt1Y4xOjec=↵',
        token: 'foo@bar.com',
      },
    },
  }

  const renderComponent = testComponentHelper(ResetPassword)

  beforeEach(() => {
    browserHistory.push = jest.fn()
    jest.clearAllMocks()
  })

  describe('@decorators', () => {
    analyticsDecoratorHelper(DecoratedResetPassword, 'reset-password', {
      componentName: 'ResetPassword',
      isAsync: false,
      redux: true,
    })
  })

  describe('@render', () => {
    it('default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })

    it('invalid form', () => {
      expect(
        renderComponent({
          ...initialProps,
          resetPasswordForm: invalidForm,
        }).getTree()
      ).toMatchSnapshot()
    })

    it('valid form', () => {
      expect(
        renderComponent({
          ...initialProps,
          resetPasswordForm: validForm,
        }).getTree()
      ).toMatchSnapshot()
    })

    it('invalid email data', () => {
      expect(
        renderComponent({
          ...initialProps,
          location: {
            query: {},
          },
        }).getTree()
      ).toMatchSnapshot()
    })
  })

  describe('@lifecycle', () => {
    let currentInstance
    let currentWrapper

    beforeEach(() => {
      const { wrapper, instance } = renderComponent(initialProps)
      jest.resetAllMocks()
      currentInstance = instance
      currentWrapper = wrapper
    })

    describe('@componentWillMount', () => {
      it('should call the correct prop methods', () => {
        expect(currentInstance.props.setFormField).not.toHaveBeenCalled()
        currentInstance.componentWillMount()
        expect(currentInstance.props.setFormField).toHaveBeenCalledTimes(1)
      })

      it('should call the correct prop methods with the correct arguments', () => {
        currentInstance.componentWillMount()
        expect(currentInstance.props.setFormField).toHaveBeenLastCalledWith(
          'resetPassword',
          'email',
          'foo@bar.com'
        )
      })

      it('should set the emailData state when valid', () => {
        currentInstance.componentWillMount()
        expect(currentWrapper.state('emailData')).toMatchObject({
          hash: 'PTDX9O3cs1pqfZabugt1Y4xOjec=↵',
          token: 'foo@bar.com',
        })
      })

      it('should set the invalidEmailData flag when the data is invalid', () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          location: {
            query: {},
          },
        })
        expect(wrapper.state('emailData')).toBeUndefined()
        expect(wrapper.state('invalidEmailData')).toBe(true)
      })
    })

    describe('@componentWillReceiveProps', () => {
      it('should redirect to "/my-account" when the password was successfully reset', () => {
        expect(browserHistory).toHaveBeenCalledTimes(0)
        currentInstance.componentWillReceiveProps({
          ...initialProps,
          passwordResetSuccessfully: true,
        })
        expect(WindowUtils.changeURL).toHaveBeenCalledWith('/my-account')
      })

      it('should not redirect to "/my-account" when the password was not successfully reset', () => {
        expect(browserHistory).toHaveBeenCalledTimes(0)
        currentInstance.componentWillReceiveProps({
          ...initialProps,
          passwordResetSuccessfully: false,
        })
        expect(WindowUtils.changeURL).toHaveBeenCalledTimes(0)
      })
    })

    describe('@componentWillUnmount', () => {
      it('should call the methods with the correct data', () => {
        currentInstance.componentWillUnmount()
        expect(
          currentInstance.props.leavingResetPasswordForm
        ).toHaveBeenCalledTimes(1)
        expect(currentInstance.props.setFormMessage).toHaveBeenCalledTimes(1)
        expect(currentInstance.props.setFormMessage).toHaveBeenLastCalledWith(
          'resetPassword',
          {}
        )
        expect(currentInstance.props.resetForm).toHaveBeenCalledTimes(1)
        expect(currentInstance.props.resetForm).toHaveBeenLastCalledWith(
          'resetPassword',
          {
            email: '',
            password: '',
            passwordConfirm: '',
          }
        )
      })
    })
  })

  describe('@events', () => {
    describe('Inputs', () => {
      it('should call setFormField when prop setField is called', () => {
        const event = {
          target: {
            value: 'newPassword',
          },
        }
        const { wrapper, instance } = renderComponent(initialProps)
        wrapper.find('[name="password"]').prop('setField')('foo')(event)
        expect(instance.props.setFormField).toHaveBeenLastCalledWith(
          'resetPassword',
          'foo',
          event.target.value
        )
      })

      it('should call touchedFormField when prop touchedField is called', () => {
        const { wrapper, instance } = renderComponent(initialProps)
        wrapper.find('[name="password"]').prop('touchedField')('foo')()
        expect(instance.props.touchedFormField).toHaveBeenLastCalledWith(
          'resetPassword',
          'foo'
        )
      })
    })

    describe('submit', () => {
      it('calls preventDefault', () => {
        const { wrapper } = renderComponent(initialProps)
        const event = { preventDefault: jest.fn() }
        wrapper.find('form').simulate('submit', event)
        expect(event.preventDefault).toHaveBeenCalledTimes(1)
      })
    })

    describe('get another link', () => {
      it('calls the "resetPasswordLinkRequest" action', () => {
        const { wrapper } = renderComponent(initialProps)
        const event = { preventDefault: jest.fn() }
        wrapper
          .find('.ResetPassword-getAnother')
          .first()
          .prop('clickHandler')(event)
        expect(initialProps.resetPasswordLinkRequest).toHaveBeenCalledTimes(1)
        expect(initialProps.resetPasswordLinkRequest).toHaveBeenLastCalledWith({
          email: 'foo@bar.com',
        })
      })
    })
  })

  describe('@instance methods', () => {
    let currentInstance

    beforeEach(() => {
      const { instance } = renderComponent({
        ...initialProps,
        success: true,
        resetPasswordForm: validForm,
      })
      jest.resetAllMocks()
      currentInstance = instance
    })

    describe('@onSubmit', () => {
      let evt

      beforeEach(() => {
        evt = {
          preventDefault: jest.fn(),
        }
      })

      it('should call preventDefault', () => {
        currentInstance.onSubmit(evt)
        expect(evt.preventDefault).toHaveBeenCalledTimes(1)
      })

      it('should call resetPasswordRequest', () => {
        expect(
          currentInstance.props.resetPasswordRequest
        ).not.toHaveBeenCalled()
        currentInstance.onSubmit(evt)
        expect(
          currentInstance.props.resetPasswordRequest
        ).toHaveBeenCalledTimes(1)
      })

      it('should call resetPasswordRequest with the correct arguments', () => {
        const expected = {
          email: 'foo@bar.com',
          password: 'Arcadia2',
          passwordConfirm: 'Arcadia2',
          hash: encodeURIComponent('PTDX9O3cs1pqfZabugt1Y4xOjec=↵'),
        }
        currentInstance.onSubmit(evt)
        expect(
          currentInstance.props.resetPasswordRequest
        ).toHaveBeenLastCalledWith(expected)
      })
    })
  })
})
