import React, { Component } from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import BrandLogo from '../../common/BrandLogo/BrandLogo'
import Image from '../../common/Image/Image'
import * as TopNavActions from '../../../actions/components/TopNavMenuActions'
import * as SearchBarActions from '../../../actions/components/search-bar-actions'
import * as ShoppingBagActions from '../../../actions/common/shoppingBagActions'
import SearchBar from '../../containers/SearchBar/SearchBar'
import BurgerButton from '../../common/BurgerButton/BurgerButton'
import QuickLinks from '../../common/QuickLinks/QuickLinks'
import Button from '../../common/Button/Button'
import { browserHistory } from 'react-router'
import QubitReact from 'qubit-react/wrapper'
import { eventBasedAnalytics } from '../../../lib/analytics/analytics'
import WishlistHeaderLink from './../../common/WishlistHeaderLink/WishlistHeaderLink'
import {
  sendAnalyticsClickEvent,
  sendAnalyticsDisplayEvent,
  GTM_CATEGORY,
  GTM_ACTION,
  GTM_EVENT,
  GTM_TRIGGER,
} from '../../../analytics'
import { getRoutePath } from '../../../selectors/routingSelectors'
import { isHeaderSticky } from '../../../selectors/pageHeaderSelectors'

const sanitiseRoutePath = (path) =>
  path ? path.replace(/\/checkout\//, '') : ''

@connect(
  (state) => ({
    topNavMenuOpen: state.topNavMenu.open,
    productsSearchOpen: state.productsSearch.open,
    shoppingBagTotalItems: state.shoppingBag.totalItems,
    modalOpen: state.modal.open,
    brandName: state.config.brandName,
    logoVersion: state.config.logoVersion,
    region: state.config.region,
    sticky: isHeaderSticky(state),
    routePath: getRoutePath(state),
  }),
  {
    ...TopNavActions,
    ...SearchBarActions,
    ...ShoppingBagActions,
    sendAnalyticsClickEvent,
    sendAnalyticsDisplayEvent,
  }
)
export default class Header extends Component {
  static propTypes = {
    topNavMenuOpen: PropTypes.bool,
    toggleTopNavMenu: PropTypes.func,
    toggleProductsSearchBar: PropTypes.func,
    productsSearchOpen: PropTypes.bool,
    shoppingBagTotalItems: PropTypes.number,
    toggleMiniBag: PropTypes.func,
    hasCartIcon: PropTypes.bool,
    hasPaymentIcon: PropTypes.bool,
    hasSearchBar: PropTypes.bool,
    hasMenuButton: PropTypes.bool,
    brandName: PropTypes.string,
    hasContinueShoppingButton: PropTypes.bool,
    isCheckoutBig: PropTypes.bool,
    logoVersion: PropTypes.string,
    region: PropTypes.string,
    forceDisplay: PropTypes.bool,
    sticky: PropTypes.bool,
    routePath: PropTypes.string,
    sendAnalyticsClickEvent: PropTypes.func.isRequired,
    sendAnalyticsDisplayEvent: PropTypes.func.isRequired,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentDidUpdate(prevProps) {
    const {
      searchInput,
      props: { productsSearchOpen, hasSearchBar },
    } = this

    if (searchInput && hasSearchBar) {
      if (!prevProps.productsSearchOpen && productsSearchOpen)
        searchInput.focus()
      if (prevProps.productsSearchOpen && !productsSearchOpen)
        searchInput.blur()
    }
  }

  toggleSearchBar = (evt) => {
    evt.preventDefault()
    this.props.toggleProductsSearchBar()
  }

  clickOnBurgerIcon = (evt) => {
    if (evt) evt.stopPropagation()
    this.props.toggleTopNavMenu()
  }

  clickOnShoppingCartIcon = (evt) => {
    if (evt) evt.stopPropagation()
    const { toggleMiniBag, sendAnalyticsDisplayEvent } = this.props
    toggleMiniBag()
    sendAnalyticsDisplayEvent(
      {
        bagDrawerTrigger: GTM_TRIGGER.BAG_ICON_CLICKED,
      },
      GTM_EVENT.BAG_DRAWER_DISPLAYED
    )
  }

  goToHomePage = (ev) => {
    const { sendAnalyticsClickEvent, routePath } = this.props
    if (ev) ev.stopPropagation()
    eventBasedAnalytics({
      events: 'event123',
    })
    sendAnalyticsClickEvent({
      category: GTM_CATEGORY.CHECKOUT,
      action: GTM_ACTION.CONTINUE_SHOPPING,
      label: sanitiseRoutePath(routePath),
      value: '',
    })
    browserHistory.push('/')
  }

  renderHeaderRight = () => {
    const {
      hasCartIcon,
      hasContinueShoppingButton,
      shoppingBagTotalItems,
      logoVersion,
      brandName,
    } = this.props
    const { l } = this.context
    if (hasCartIcon) {
      return (
        <div className="Header-right">
          <WishlistHeaderLink />
          <button
            aria-label={l`View shopping cart`}
            title={l`View shopping cart`}
            className="Header-shoppingCartIconbutton"
            onClick={this.clickOnShoppingCartIcon}
          >
            <Image
              alt={l`View shopping cart`}
              className={`Header-shoppingCartIcon${
                shoppingBagTotalItems === 0 ? ' is-empty' : ''
              }`}
              src={`/assets/${brandName}/images/shopping-cart-icon-default.svg?version=${logoVersion}`}
            />
            <span className="Header-shoppingCartBadgeIcon">
              {shoppingBagTotalItems}
            </span>
          </button>
        </div>
      )
    }

    if (hasContinueShoppingButton) {
      return (
        <Button
          className="Button--secondary Header-right Header-continueShopping"
          clickHandler={this.goToHomePage}
        >
          {l`Continue shopping`}
        </Button>
      )
    }
    return null
  }

  render() {
    const {
      topNavMenuOpen,
      productsSearchOpen,
      hasMenuButton,
      hasSearchBar,
      hasPaymentIcon,
      brandName,
      isCheckoutBig,
      logoVersion,
      region,
      forceDisplay,
      sticky,
    } = this.props
    const { l } = this.context
    const isRegionSpecific = brandName === 'burton'
    const headerContainerClasses = classNames('Header-container', 'hideinapp', {
      'Header-container--searchOpen': productsSearchOpen,
      'is-sticky': sticky,
    })
    const headerClasses = classNames('Header', {
      'is-checkoutBig': isCheckoutBig,
      'is-forceDisplay': forceDisplay,
      'is-sticky': sticky,
    })
    return (
      <QubitReact id="qubit-Header-container">
        <div className={headerContainerClasses}>
          <QubitReact id="qubit-Header">
            <div className={headerClasses}>
              <div className="Header-center">
                <button
                  title={l`Home`}
                  onClick={() => browserHistory.push('/')}
                >
                  <QubitReact brandName={brandName} id="qubit-Header-brandLogo">
                    <BrandLogo
                      brandName={brandName}
                      className="Header-brandLogo"
                      logoVersion={logoVersion}
                      region={region}
                      isRegionSpecific={isRegionSpecific}
                    />
                  </QubitReact>
                </button>
              </div>
              <QuickLinks />
              {hasMenuButton && (
                <div // eslint-disable-line jsx-a11y/no-static-element-interactions
                  className="Header-left Header-burgerButtonContainer"
                  onClick={this.clickOnBurgerIcon}
                >
                  <BurgerButton isArrow={topNavMenuOpen} />
                </div>
              )}
              {hasPaymentIcon && (
                <div className="Header-left Header-secureIconContainer">
                  <Image
                    className="Header-secureIcon"
                    src="/assets/{brandName}/images/payment-icon.png"
                    alt={l`Secure checkout`}
                  />
                </div>
              )}
              {hasSearchBar && (
                <button
                  aria-label={l`Open search bar to search for products`}
                  title={l`Open search bar to search for products`}
                  className="Header-left Header-searchButton"
                  onClick={this.toggleSearchBar}
                >
                  {productsSearchOpen ? (
                    <Image
                      className="Header-searchIcon"
                      alt="Close"
                      src={`/assets/{brandName}/images/close-icon.svg?version=${logoVersion}`}
                    />
                  ) : (
                    <Image
                      className="Header-searchIcon"
                      alt="Search"
                      src={`/assets/{brandName}/images/search-icon.svg?version=${logoVersion}`}
                    />
                  )}
                </button>
              )}
              {this.renderHeaderRight()}
            </div>
          </QubitReact>
          {hasSearchBar && (
            <SearchBar
              searchInputRef={(searchInput) => {
                this.searchInput = searchInput
              }}
            />
          )}
        </div>
      </QubitReact>
    )
  }
}
