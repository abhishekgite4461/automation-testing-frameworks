import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import classNames from 'classnames'
import { connect } from 'react-redux'

import TopNavMenu from '../TopNavMenu/TopNavMenu'
import HeaderContainer from '../Header/HeaderContainer'
import FooterContainer from '../Footer/FooterContainer/FooterContainer'
import DebugButton from '../DebugButton/DebugButton'
import MontyVisualIndicator from '../../common/MontyVisualIndicator/MontyVisualIndicator'
import ErrorMessage from '../ErrorMessage/ErrorMessage'
import Modal from '../../common/Modal/Modal'
import CheckoutMiniBag from '../CheckoutMiniBag/CheckoutMiniBag'
import Drawer from '../Drawer/Drawer'
import SizeGuideDrawer from '../../common/SizeGuideDrawer/SizeGuideDrawer'
import MiniBag from '../MiniBag/MiniBag'
import TacticalMessage from '../TacticalMessage/TacticalMessage'
import LoaderOverlay from '../../common/LoaderOverlay/LoaderOverlay'
import ContentOverlay from '../../common/ContentOverlay/ContentOverlay'
import Async from '../../common/Async/Async'
import Espot from '../Espot/Espot'
import * as viewportActions from '../../../actions/common/viewportActions'
import * as siteOptionsActions from '../../../actions/common/siteOptionsActions'
import { setPeeriusCallback } from '../../../actions/common/peeriusActions'
import { setRecommendations } from '../../../actions/components/RecommendationsActions'
import { getContent } from '../../../actions/common/sandBoxActions'
import {
  getBag,
  initShoppingBag,
} from '../../../actions/common/shoppingBagActions'
import { initFeaturesListener } from '../../../actions/common/featuresActions'
import { retrieveCachedData } from '../../../actions/common/pageCacheActions'
import * as navigationActions from '../../../actions/common/navigationActions'
import * as storeLocatorActions from '../../../actions/components/StoreLocatorActions'
import * as footerActions from '../../../actions/common/footerActions'
import { getViewport, touchDetection } from '../../../lib/viewHelper'
import espots from '../../../constants/espotsMobile'
import cmsConsts from '../../../constants/cmsConsts'
import throttle from 'lodash.throttle'
import breakpoints from '../../../constants/responsive'
import { cmsIframePostMessage } from '../../../../shared/lib/analytics/cms'
import { WindowEventProvider } from '../WindowEventProvider/WindowEventProvider'
import { WindowNavigationListener } from '../WindowEventProvider/WindowNavigationListener'
import espotsDesktopConstants from '../../../../shared/constants/espotsDesktop'
import { getRedirectURL } from '../../../reducers/common/geoIPReducer'
import GeoIPModal from '../../common/GeoIP'
import * as modalActions from '../../../actions/common/modalActions'
import { debug } from '../../../../server/lib/logger'
import { getDefaultWishlist } from '../../../actions/common/wishlistActions'
import CookieMessage from '../../common/CookieMessage/CookieMessage'

import {
  getScrollPositionOnOpenModal,
  isModalOpen,
} from '../../../selectors/modalSelectors'
import {
  shouldDisplayMobileHeaderIfSticky,
  isHeaderSticky,
} from '../../../selectors/pageHeaderSelectors'
import { isHomePage } from '../../../selectors/routingSelectors'
import { getWishlistItemIds } from '../../../selectors/wishlistSelectors'
import {
  getMetaDescription,
  getPageTitle,
} from '../../../selectors/pageDataSelectors'
import { getBrandName } from '../../../selectors/common/configSelectors'
import { getBrandDisplayName } from '../../../selectors/configSelectors'
import { getMegaNavSelectedCategory } from '../../../selectors/navigationSelectors'

if (process.browser) {
  require('../../../styles/typography.css')
  require('../../../styles/base.css')
}

@connect(
  (state) => ({
    topNavMenuOpen: state.topNavMenu.open,
    sizeGuideOpen: state.productDetail.sizeGuideOpen,
    productsSearchOpen: state.productsSearch.open,
    refinementsOpen: state.refinements.isShown,
    modalOpen: isModalOpen(state),
    modalMode: state.modal.mode,
    miniBagOpen: state.shoppingBag.miniBagOpen,
    iosAgent: state.viewport.iosAgent,
    brandName: getBrandName(state),
    brandDisplayName: getBrandDisplayName(state),
    googleSiteVerification: state.config.googleSiteVerification,
    errorMessage: state.errorMessage,
    totalItems: state.shoppingBag.totalItems,
    showTacticalMessage: state.sandbox.showTacticalMessage,
    debugShown: state.debug.isShown,
    assets: state.config.assets,
    media: state.viewport.media,
    isMobile: state.viewport.media === 'mobile',
    loadedMedia: state.viewport.loadedMedia,
    featureResponsive: state.features.status.FEATURE_RESPONSIVE,
    featureMegaNav: state.features.status.FEATURE_MEGA_NAV,
    touchEnabled: state.viewport.touch,
    scrollPositionOnOpenModal: getScrollPositionOnOpenModal(state),
    redirectURL: getRedirectURL(state),
    isHomePage: isHomePage(state),
    wishlistedItemIds: getWishlistItemIds(state),
    stickyHeader: isHeaderSticky(state),
    forceMobileHeader: shouldDisplayMobileHeaderIfSticky(state),
    metaDescription: getMetaDescription(state),
    pageTitle: getPageTitle(state),
    megaNavSelectedCategory: getMegaNavSelectedCategory(state),
  }),
  {
    ...viewportActions,
    ...modalActions,
    setMegaNavSelectedCategory: navigationActions.setMegaNavSelectedCategory,
    getBag,
    getDefaultWishlist,
    initFeaturesListener,
    retrieveCachedData,
    setPeeriusCallback,
    setRecommendations,
    initShoppingBag,
  }
)
export default class Main extends Component {
  static propTypes = {
    location: PropTypes.object.isRequired,
    brandName: PropTypes.string.isRequired,
    brandDisplayName: PropTypes.string.isRequired,
    loadedMedia: PropTypes.array.isRequired,
    iosAgent: PropTypes.bool,
    refinementsOpen: PropTypes.bool,
    children: PropTypes.object,
    errorMessage: PropTypes.object,
    topNavMenuOpen: PropTypes.bool,
    miniBagOpen: PropTypes.bool,
    productsSearchOpen: PropTypes.bool,
    modalOpen: PropTypes.bool,
    modalMode: PropTypes.string,
    showTacticalMessage: PropTypes.bool,
    debugShown: PropTypes.bool,
    googleSiteVerification: PropTypes.string,
    media: PropTypes.string,
    isMobile: PropTypes.bool,
    assets: PropTypes.object,
    featureResponsive: PropTypes.bool,
    touchEnabled: PropTypes.bool,
    updateWindow: PropTypes.func,
    setAndUpdateMediaType: PropTypes.func,
    updateAgent: PropTypes.func,
    updateTouch: PropTypes.func,
    setPeeriusCallback: PropTypes.func,
    setRecommendations: PropTypes.func,
    initFeaturesListener: PropTypes.func,
    retrieveCachedData: PropTypes.func,
    initShoppingBag: PropTypes.func,
    sizeGuideOpen: PropTypes.bool.isRequired,
    scrollPositionOnOpenModal: PropTypes.number,
    showModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
    isHomePage: PropTypes.bool.isRequired,
    wishlistedItemIds: PropTypes.array,
    getDefaultWishlist: PropTypes.func,
    stickyHeader: PropTypes.bool.isRequired,
    forceMobileHeader: PropTypes.bool.isRequired,
    metaDescription: PropTypes.string,
    pageTitle: PropTypes.string,
    megaNavSelectedCategory: PropTypes.string.isRequired,
    setMegaNavSelectedCategory: PropTypes.func.isRequired,
  }

  static defaultProps = {
    metaDescription: '',
    pageTitle: '',
    forceMobileHeader: false,
    stickyHeader: false,
  }

  static contextTypes = {
    store: PropTypes.object,
    l: PropTypes.func,
  }

  componentWillMount() {
    const {
      setPeeriusCallback,
      setRecommendations,
      showModal,
      redirectURL,
    } = this.props

    if (redirectURL) {
      debug('GeoIP', { message: 'Rendering GeoIP modal', redirectURL })
      showModal(
        <GeoIPModal
          handleClose={this.handleClose}
          handleRemoveCloseHandler={this.removeCloseHandler}
          store={this.context.store}
          l={this.context.l}
        />
      )
    } else {
      debug('GeoIP', { message: 'Not showing GeoIP modal', redirectURL })
    }

    if (process.browser) {
      setPeeriusCallback(setRecommendations)
    }
  }

  componentDidMount() {
    const {
      location,
      updateAgent,
      initFeaturesListener,
      retrieveCachedData,
      brandName,
      updateTouch,
      initShoppingBag,
      getDefaultWishlist,
      wishlistedItemIds,
    } = this.props
    const isNotOrderComplete = !location.pathname.includes('/order-complete')
    const isNotCheckout = !location.pathname.includes('/checkout')

    // on order-complete prevent get bag as it conflicts with a concurent request to update bag
    if (isNotCheckout && isNotOrderComplete) retrieveCachedData()
    if (isNotOrderComplete) initShoppingBag()

    if (process.browser) {
      updateAgent(
        !!(
          navigator.userAgent.match(/iPhone/i) ||
          navigator.userAgent.match(/iP[ao]d/i)
        )
      )
      this.windowResizeEvent()
      // Development and desktop

      updateTouch(touchDetection())

      window.addEventListener(
        'resize',
        throttle(this.windowResizeEvent.bind(this), 100)
      )
      window.addEventListener(
        'message',
        cmsIframePostMessage.bind(this, brandName)
      )
      /**
       * History workaround on CriOS
       *
       * There could be some extraneous WebKit `popstate` events which is
       * distingueshed by the `history` module by checking if the `event.state`
       * is undefined. The problem isriOS (Chrome on iOS) could raise a
       * `popstate` event with undefined state too, so an extra condition needs
       * to be added to the module for this which has been fixed in a later
       * version so we have to keep this until we will have the updated
       * history to v4.4.1
       *
       * [fix]{@link https://github.com/mjackson/history/pull/383}
       */
      window.addEventListener('popstate', (event) => {
        const isRealEvent =
          event.state === undefined && navigator.userAgent.includes('CriOS')
        if (isRealEvent) {
          const customEvent = new Event('popstate')
          customEvent.state = {
            key: 'custom',
          }
          window.dispatchEvent(customEvent)
        }
      })

      initFeaturesListener()

      if (!wishlistedItemIds) {
        getDefaultWishlist()
      }
    }
  }

  onMouseDown = (event) => {
    this.removeOutline(event.target)
  }

  onMouseUp = () => {
    this.removeOutline(document.activeElement)
  }

  onBlur = (event) => {
    event.target.style.outline = ''
    event.target.removeEventListener('blur', this.onBlur)
  }

  removeOutline = (element) => {
    element.style.outline = 'none'
    element.addEventListener('blur', this.onBlur)
  }

  windowResizeEvent() {
    const { updateWindow, setAndUpdateMediaType } = this.props
    const dimensions = getViewport()
    updateWindow(dimensions)

    Object.keys(breakpoints).forEach((name) => {
      const data = breakpoints[name]
      if (data.file && dimensions.width >= data.min) {
        if (data.max && dimensions.width > data.max) return
        setAndUpdateMediaType(name)
      }
    })
  }

  componentDidUpdate = () => {
    const {
      modalOpen,
      scrollPositionOnOpenModal,
      resetScrollPositionOnOpenModal,
    } = this.props
    if (!modalOpen && scrollPositionOnOpenModal) {
      window.scrollTo(0, scrollPositionOnOpenModal)
      resetScrollPositionOnOpenModal()
    }
  }

  static getMainElementStyle = (modalOpen, scrollPosition) =>
    modalOpen
      ? { top: -scrollPosition, height: `calc(100vh + ${scrollPosition}px)` }
      : {}

  handleClose = (handler) => {
    this.closeModalHandler = handler
  }

  removeCloseHandler = () => {
    this.closeModalHandler = null
  }

  closeModal = () => {
    const closeModal = () => {
      if (this.props.modalMode !== 'warning') this.props.closeModal()
    }

    if (this.closeModalHandler) {
      if (this.closePromise) return

      this.closePromise = this.closeModalHandler().then(() => {
        closeModal()
        this.closePromise = null
      })
    } else {
      closeModal()
    }
  }

  getDrawerComponent = (isCheckout) => {
    const { sizeGuideOpen, isMobile } = this.props

    if (isCheckout) return CheckoutMiniBag
    if (!isMobile && sizeGuideOpen) return SizeGuideDrawer
    return MiniBag
  }

  static needs = [
    navigationActions.getCategories,
    navigationActions.getMegaNav,
    siteOptionsActions.getSiteOptions,
    () => getContent(null, 'navSocial'),
    () =>
      getContent(null, espots.tacticalMessage[0], cmsConsts.ESPOT_CONTENT_TYPE),
    footerActions.getFooterContent,
    storeLocatorActions.getCountries,
    getDefaultWishlist,
  ]

  getDisplayContent() {
    const { errorMessage } = this.props
    return !errorMessage || (errorMessage && errorMessage.isOverlay)
  }

  getShowOverlay() {
    const {
      sizeGuideOpen,
      miniBagOpen,
      topNavMenuOpen,
      productsSearchOpen,
      modalOpen,
    } = this.props
    return (
      topNavMenuOpen ||
      productsSearchOpen ||
      modalOpen ||
      miniBagOpen ||
      sizeGuideOpen
    )
  }

  getClassNames() {
    const {
      isHomePage,
      topNavMenuOpen,
      miniBagOpen,
      isMobile,
      refinementsOpen,
      debugShown,
      iosAgent,
      modalOpen,
      stickyHeader,
    } = this.props
    return classNames('Main-body', {
      'is-homePage': isHomePage,
      'is-right': topNavMenuOpen,
      'is-left': !topNavMenuOpen && miniBagOpen && isMobile,
      'is-notScrollable': refinementsOpen || debugShown,
      ios: iosAgent,
      'is-modalOpen': modalOpen,
      'is-stickyHeader': stickyHeader,
    })
  }

  closeOverlayAndPreventClickingLinks = () => {
    const { setMegaNavSelectedCategory } = this.props
    setTimeout(() => {
      setMegaNavSelectedCategory('')
    }, 100)
  }

  getMegaNavOverlay() {
    const { megaNavSelectedCategory, touchEnabled, stickyHeader } = this.props
    if (!megaNavSelectedCategory) {
      return null
    }
    const overlayClassNames = classNames('MegaNav-overlay', {
      'MegaNav-overlay--noTouch': !touchEnabled,
      'MegaNav-overlay--sticky': stickyHeader,
    })
    return (
      <div
        onTouchEnd={this.closeOverlayAndPreventClickingLinks}
        className={overlayClassNames}
      />
    )
  }

  render() {
    const {
      location,
      miniBagOpen,
      sizeGuideOpen,
      closeModal,
      brandName,
      brandDisplayName,
      children,
      modalOpen,
      showTacticalMessage,
      debugShown,
      googleSiteVerification,
      media,
      assets,
      featureResponsive,
      loadedMedia,
      isMobile,
      touchEnabled,
      scrollPositionOnOpenModal,
      metaDescription,
      pageTitle,
      forceMobileHeader,
      megaNavSelectedCategory,
    } = this.props
    const appClass = this.getClassNames()
    const displayContent = this.getDisplayContent()
    const showOverlay = this.getShowOverlay()
    const isCheckout = /\/checkout(\/.*)/.test(location.pathname)
    const isOrderComplete = /\/order-complete(.*)/.test(location.pathname)

    const additionalCss = loadedMedia.reduce((prev, next) => {
      const stylesheetAvailable =
        featureResponsive &&
        assets.css &&
        assets.css[`${brandName}/styles-${next}.css`]
      return stylesheetAvailable
        ? prev.concat({
            rel: 'stylesheet',
            href: stylesheetAvailable,
            media: `(min-width: ${breakpoints[next].min}px)`,
          })
        : prev
    }, [])
    const mainElementStyle = Main.getMainElementStyle(
      modalOpen,
      scrollPositionOnOpenModal
    )

    const metaData = [
      {
        name: 'google-site-verification',
        content: googleSiteVerification,
      },
    ]

    if (metaDescription) {
      metaData.push({
        name: 'description',
        content: metaDescription,
      })
    }

    const drawerOpen = !!miniBagOpen || (!isMobile && sizeGuideOpen)
    const DrawerComponent = this.getDrawerComponent(isCheckout)
    const mainInnerClassNames = classNames('Main-inner', {
      'Main-inner--overlay': megaNavSelectedCategory,
    })

    return (
      <WindowEventProvider>
        <WindowNavigationListener modalOpen={modalOpen} closeModal={closeModal}>
          <div
            role="presentation"
            className={`Main ${!touchEnabled ? 'no-touch' : ''}`}
            onMouseDown={this.onMouseDown}
            onMouseUp={this.onMouseUp}
          >
            <Helmet
              titleTemplate={`%s | ${brandDisplayName}`}
              title={pageTitle}
              meta={metaData}
              link={additionalCss}
            />
            <ContentOverlay
              onClick={this.closeModal}
              showOverlay={showOverlay}
            />
            <div className={appClass} style={mainElementStyle}>
              {!isCheckout &&
                !isOrderComplete && (
                  <Espot
                    identifier={
                      espotsDesktopConstants.navigation.siteWideHeader
                    }
                  />
                )}
              {showTacticalMessage && (
                <TacticalMessage
                  onSetHeight={this.onSetTacticalMessageHeight}
                />
              )}
              <HeaderContainer location={location} />
              <ErrorMessage />
              <div tabIndex="0" id="Main-content" />
              <div className={mainInnerClassNames}>
                {displayContent && children}
                {this.getMegaNavOverlay()}
              </div>
            </div>
            {!featureResponsive || media === 'mobile' || forceMobileHeader ? (
              <TopNavMenu />
            ) : null}
            {!isMobile && <FooterContainer />}
            {!isMobile && <MontyVisualIndicator />}
            <Drawer isOpen={drawerOpen} isScrollable={sizeGuideOpen}>
              <DrawerComponent />
            </Drawer>
            <Modal onCloseModal={this.closeModal} />
            <LoaderOverlay />
            {debugShown && (
              <Async
                id="debug"
                getFile={
                  // eslint-disable-next-line no-undef
                  () => System.import('common/Debug/Debug.jsx')
                }
              />
            )}
            <DebugButton />
            <CookieMessage />
          </div>
        </WindowNavigationListener>
      </WindowEventProvider>
    )
  }
}
