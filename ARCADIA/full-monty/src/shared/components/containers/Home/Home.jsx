import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Sandbox from '../SandBox/SandBox'
import Espot from '../Espot/Espot'
import Peerius from '../../common/Peerius/Peerius'

import * as sandBoxActions from '../../../actions/common/sandBoxActions'

import espots from '../../../constants/espotsMobile'
import cmsConsts from '../../../constants/cmsConsts'
import { GTM_CATEGORY } from '../../../analytics'
import espotsDesktopConstants from '../../../../shared/constants/espotsDesktop'

import { updateUniversalVariable } from '../../../lib/analytics/qubit-analytics'
import analyticsDecorator from '../../../../client/lib/analytics/analytics-decorator'
import { getBrandDisplayName } from '../../../../shared/selectors/configSelectors'
import { isFeatureHomePageSegmentationEnabled } from '../../../selectors/featureSelectors'
import breakpoints from '../../../constants/responsive'

const DESKTOP_WIDTH = 1200

@analyticsDecorator(GTM_CATEGORY.HOME, { isAsync: true })
@connect(
  (state) => ({
    viewportWidth: state.viewport.width,
    brandName: getBrandDisplayName(state),
    isHomePageSegmentationEnabled: isFeatureHomePageSegmentationEnabled(state),
  }),
  {
    ...sandBoxActions,
    updateUniversalVariable,
  }
)
export default class Home extends Component {
  static propTypes = {
    showTacticalMessage: PropTypes.func,
    hideTacticalMessage: PropTypes.func,
    updateUniversalVariable: PropTypes.func,
    brandName: PropTypes.string.isRequired,
    isHomePageSegmentationEnabled: PropTypes.bool.isRequired,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentDidMount() {
    const { showTacticalMessage, updateUniversalVariable } = this.props
    showTacticalMessage()
    updateUniversalVariable()
  }

  componentWillUnmount() {
    const { hideTacticalMessage } = this.props
    hideTacticalMessage()
  }

  static needs = [
    sandBoxActions.showTacticalMessage,
    () => sandBoxActions.getHomePageContent(),
    () =>
      sandBoxActions.getContent(
        null,
        espots.home[0],
        cmsConsts.ESPOT_CONTENT_TYPE
      ),
    () =>
      sandBoxActions.getContent(
        null,
        espots.home[1],
        cmsConsts.ESPOT_CONTENT_TYPE
      ),
  ]

  render() {
    const {
      brandName,
      viewportWidth,
      isHomePageSegmentationEnabled,
    } = this.props
    const { l } = this.context
    const segmentationData = {
      wcsEndpoint: '/home',
      responseIdentifier: espotsDesktopConstants.home.mainBody,
    }
    return (
      <div
        style={{ maxWidth: `${breakpoints.laptop.max}px`, margin: '0 auto' }}
      >
        <h1 className="screen-reader-text">{`${brandName} ${l`homepage`}`}</h1>
        <Peerius type="home" />
        <Espot identifier={espotsDesktopConstants.home.content} />
        <div
          style={{
            margin: `0 ${
              viewportWidth > DESKTOP_WIDTH
                ? (DESKTOP_WIDTH - viewportWidth) / 2
                : 0
            }px`,
          }}
        >
          <Sandbox
            cmsPageName="home"
            segmentationRequestData={
              isHomePageSegmentationEnabled ? segmentationData : null
            }
            contentType={cmsConsts.PAGE_CONTENT_TYPE}
            isInPageContent
          />
          <Sandbox
            cmsPageName={espots.home[0]}
            contentType={cmsConsts.ESPOT_CONTENT_TYPE}
            isInPageContent
          />
          <Sandbox
            cmsPageName={espots.home[1]}
            contentType={cmsConsts.ESPOT_CONTENT_TYPE}
            isInPageContent
          />
        </div>
      </div>
    )
  }
}
