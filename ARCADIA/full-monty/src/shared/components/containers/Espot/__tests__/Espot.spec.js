import deepFreeze from 'deep-freeze'
import testComponentHelper from '../../../../../../test/unit/helpers/test-component'

import Espot from '../Espot'
import SandBox from '../../SandBox/SandBox'

describe('<Espot/>', () => {
  const renderComponent = testComponentHelper(Espot.WrappedComponent, {
    disableLifecycleMethods: false,
  })

  beforeEach(jest.resetAllMocks)

  const initialProps = deepFreeze({
    responsiveCMSUrl: 'url',
    isMobile: false,
    qubitid: 'qubitid',
    className: 'class',
    identifier: 'id',
  })

  describe('@renders', () => {
    it('should not render with content on mobile', () => {
      const { wrapper } = renderComponent({ ...initialProps, isMobile: true })
      expect(wrapper.html()).toBeNull()
    })

    it('should render SandBox when responsiveCMSUrl is present', () => {
      const { wrapper } = renderComponent(initialProps)
      const SandBoxComp = wrapper.find(SandBox)
      expect(SandBoxComp).toHaveLength(1)
      expect(SandBoxComp.props()).toEqual({
        location: { pathname: initialProps.responsiveCMSUrl },
        qubitid: initialProps.qubitid,
        isFinalResponsiveEspotSolution: true,
        shouldGetContentOnFirstLoad: true,
        sandBoxClassName: '',
        contentType: 'espot',
      })
    })

    it('should not render SandBox when responsiveCMSUrl is not present', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        responsiveCMSUrl: '',
      })
      expect(wrapper.find(SandBox)).toHaveLength(0)
    })

    const validGridSize = [1, 2, 3, 4]
    validGridSize.forEach((grid) => {
      it(`should provide Espot--plpCol${grid} as the sandBoxClassName prop to the Sandbox component when grid prop is ${grid}`, () => {
        const { wrapper } = renderComponent({ ...initialProps, grid })
        const SandBoxComp = wrapper.find(SandBox)
        expect(SandBoxComp.prop('sandBoxClassName')).toBe(
          `Espot--plpCol${grid}`
        )
      })
    })

    const invalidGridSizes = [undefined, null, 100]
    invalidGridSizes.forEach((grid) => {
      it(`should NOT pass down a sandBoxClassName if grid prop is ${grid}`, () => {
        const { wrapper } = renderComponent({ ...initialProps, grid })
        const SandBoxComp = wrapper.find(SandBox)
        expect(SandBoxComp.prop('sandBoxClassName')).toBe('')
      })
    })
  })
})
