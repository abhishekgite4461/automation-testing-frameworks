import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { isMobile } from '../../../selectors/viewportSelectors'
import { getResponsiveCMSUrl } from '../../../selectors/espotSelectors'

import SandBox from '../SandBox/SandBox'
import cmsConsts from '../../../constants/cmsConsts'

const validPlpGridSizes = [1, 2, 3, 4]

@connect(
  (state, { identifier }) => ({
    responsiveCMSUrl: getResponsiveCMSUrl(state, identifier),
    isMobile: isMobile(state),
  }),
  {}
)
export default class Espot extends PureComponent {
  static propTypes = {
    responsiveCMSUrl: PropTypes.string,
    isMobile: PropTypes.bool.isRequired,
    qubitid: PropTypes.string,
    grid: PropTypes.oneOf(validPlpGridSizes),
  }

  static defaultProps = {
    responsiveCMSUrl: '',
    className: '',
    qubitid: null,
    grid: null,
  }

  render() {
    const { isMobile, qubitid, responsiveCMSUrl, grid } = this.props

    // TODO: remove this check at some point when we are ready to use sandbox for both mobile and desktop
    if (isMobile) {
      return null
    }

    if (!responsiveCMSUrl) {
      return null
    }

    const sandBoxClassName =
      grid && validPlpGridSizes.includes(grid) ? `Espot--plpCol${grid}` : ''

    return (
      <SandBox
        location={{ pathname: responsiveCMSUrl }}
        shouldGetContentOnFirstLoad
        isFinalResponsiveEspotSolution
        qubitid={qubitid}
        sandBoxClassName={sandBoxClassName}
        contentType={cmsConsts.ESPOT_CONTENT_TYPE}
      />
    )
  }
}
