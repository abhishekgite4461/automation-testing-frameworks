import classnames from 'classnames'
import PropTypes from 'prop-types'
import { compose, flatten, pluck } from 'ramda'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import constants from '../../../constants/espotsDesktop'
import { GTM_CATEGORY } from '../../../analytics'

import { deleteRecentlyViewedProduct } from '../../../actions/common/recentlyViewedActions'
import { setCarouselIndex } from '../../../actions/common/carousel-actions'

import ProductMedia from '../../common/ProductMedia/ProductMedia'
import FeatureCheck from '../../common/FeatureCheck/FeatureCheck'
import ProductCarouselThumbnails from '../ProductCarouselThumbnails/ProductCarouselThumbnails'
import Price from '../../common/Price/Price'
import BundlesAddAll from '../../common/BundlesAddAll/BundlesAddAll'
import BundleProducts from '../../common/BundleProducts/BundleProducts'
import ProductDescription from '../../common/ProductDescription/ProductDescription'
import ProductDescriptionExtras from '../../common/ProductDescriptionExtras/ProductDescriptionExtras'
import RecentlyViewed from '../../common/RecentlyViewed/RecentlyViewed'
import Recommendations from '../../common/Recommendations/Recommendations'
import Espot from '../../containers/Espot/Espot'
import analyticsDecorator from '../../../../client/lib/analytics/analytics-decorator'

class Bundles extends Component {
  static propTypes = {
    setCarouselIndex: PropTypes.func.isRequired,
    isFeatureCarouselThumbnailEnabled: PropTypes.bool,
    product: PropTypes.object.isRequired,
    carousel: PropTypes.object.isRequired,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  getItems = (bundleSlots) => bundleSlots.filter((slot) => slot.products.length)

  getFixedBundleProductIds(product) {
    const { carousel } = this.props
    const currentProductIdsFromCarousels = Object.keys(carousel)
      .filter(
        (name) =>
          name.includes('miniProductCarousel') &&
          carousel[name].currentItemReference
      )
      .map((name) => carousel[name].currentItemReference)

    if (
      Array.isArray(currentProductIdsFromCarousels) &&
      currentProductIdsFromCarousels.length > 0
    ) {
      return currentProductIdsFromCarousels
    }

    return compose(
      pluck('productId'),
      flatten,
      pluck('products')
    )(product.bundleSlots)
  }

  clickCarouselThumbs = (index) => {
    const { setCarouselIndex } = this.props
    setCarouselIndex('bundles', index)
  }

  render() {
    const { l } = this.context
    const {
      product: {
        amplienceAssets,
        assets,
        bundleSlots,
        bundleType,
        description,
        name,
        unitPrice,
        seeMoreValue,
      },
      isFeatureCarouselThumbnailEnabled,
      product,
    } = this.props
    const bundleProducts = this.getItems(bundleSlots)

    // bundle is discounted if any one of its products are discounted
    const isDiscounted = flatten(
      bundleProducts.map(({ products }) => products)
    ).some(({ wasWasPrice }) => wasWasPrice)
    const priceClassNames = classnames('Bundles-price', {
      'Bundles-price--discounted': isDiscounted,
    })
    const referringProduct = {
      productId: product.productId,
      lineNumber: product.lineNumber,
      name: product.name,
    }

    return (
      <div className={`Bundles is-${bundleType.toLowerCase()}`}>
        <div className="Bundles-topWrapper">
          <div className="Bundles-topWrapperPart Bundles-carousel">
            <div
              className={
                isFeatureCarouselThumbnailEnabled
                  ? ' Bundles-carouselWithThumbnails'
                  : ''
              }
            >
              <FeatureCheck flag="FEATURE_PRODUCT_CAROUSEL_THUMBNAIL">
                <ProductCarouselThumbnails
                  maxVisible={5}
                  setCarouselIndex={this.clickCarouselThumbs}
                  className="ProductCarouselThumbnails"
                  amplienceAssets={amplienceAssets}
                  amplienceImages={
                    amplienceAssets ? amplienceAssets.images : []
                  }
                  thumbs={assets.filter(
                    (asset) => asset.assetType === 'IMAGE_SMALL'
                  )}
                />
              </FeatureCheck>
              <ProductMedia
                name="bundles"
                productId={product.productId}
                amplienceAssets={product.amplienceAssets}
                assets={assets}
                enableImageOverlay
                className="Bundles-productMedia"
              />
            </div>
            <Espot
              identifier={constants.product.col1pos1}
              className="Bundles-espot"
            />
            <Espot
              identifier={constants.product.klarna1}
              className="Bundles-espot"
            />
            <Espot
              identifier={constants.product.klarna2}
              className="Bundles-espot"
            />
          </div>

          <div className="Bundles-topWrapperPart">
            <div className="Bundles-header">
              <h1 className="Bundles-title">{name}</h1>
              <p className={priceClassNames}>
                <span className="Bundles-priceFrom">{l`From`}</span>{' '}
                <Price className="Bundles-priceValue" price={unitPrice} />
              </p>
            </div>
            <BundleProducts items={bundleProducts} />
            {bundleType === 'Fixed' && (
              <BundlesAddAll
                productId={product.productId}
                deliveryMessage={product.deliveryMessage}
                bundleProductIds={this.getFixedBundleProductIds(product)}
              />
            )}
          </div>
        </div>

        <ProductDescription
          description={description}
          seeMoreValue={seeMoreValue}
          className="ProductDescription--bundle"
        >
          <ProductDescriptionExtras
            attributes={[{ label: l`Code`, value: product.lineNumber }]}
          />
        </ProductDescription>

        <Espot
          identifier={constants.product.content1}
          className="Bundles-espot"
        />

        <Recommendations referringProduct={referringProduct} />

        <RecentlyViewed
          referringProduct={referringProduct}
          currentProductId={referringProduct.productId}
        />
      </div>
    )
  }
}

export default compose(
  analyticsDecorator(GTM_CATEGORY.BUNDLE, { isAsync: true }),
  connect(
    (state) => ({
      carousel: state.carousel,
      isFeatureCarouselThumbnailEnabled: Boolean(
        state.features.status.FEATURE_PRODUCT_CAROUSEL_THUMBNAIL
      ),
    }),
    {
      deleteRecentlyViewedProduct,
      setCarouselIndex,
    }
  )
)(Bundles)

export { Bundles as WrappedBundles }
