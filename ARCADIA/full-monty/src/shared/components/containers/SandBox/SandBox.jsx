import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { connect } from 'react-redux'
import { omit, path } from 'ramda'
import QubitReact from 'qubit-react/wrapper'

import Loader from '../../common/Loader/Loader'
import CmsWrapper from '../../containers/CmsWrapper/CmsWrapper'
import CmsNotAvailable from '../../common/CmsNotAvailable/CmsNotAvailable'
import CMSIframe from '../../common/CMSIframe/CMSIframe'
import * as sandBoxActions from '../../../actions/common/sandBoxActions'
import {
  setContent,
  setFormDefaultSchema,
} from '../../../actions/common/cmsActions'
import cmsUtilities from '../../../lib/cms-utilities'
import { parseCmsForm } from './tempUtils'
import cmsConsts from '../../../constants/cmsConsts'

const isCmsForm = (pageData) => pageData && pageData[0] && pageData[0].formCss

@connect(
  (state) => ({
    visited: state.routing.visited,
    pages: state.sandbox.pages,
    isMobile: state.viewport.media === 'mobile',
    featureLegacyPages: state.features.status.FEATURE_LEGACY_PAGES,
    useMontyCmsForForms: state.features.status.FEATURE_USE_MONTY_CMS_FOR_FORMS,
  }),
  {
    ...sandBoxActions,
    setContent,
    setFormDefaultSchema,
  }
)
export default class SandBox extends Component {
  static propTypes = {
    isInPageContent: PropTypes.bool, // this represents when the component is not the main content of the page
    getContent: PropTypes.func.isRequired,
    getSegmentedContent: PropTypes.func.isRequired,
    removeContent: PropTypes.func,
    location: PropTypes.object,
    visited: PropTypes.array,
    setContent: PropTypes.func,
    setFormDefaultSchema: PropTypes.func,
    shouldGetContentOnFirstLoad: PropTypes.bool.isRequired,
    isMobile: PropTypes.bool,
    featureLegacyPages: PropTypes.bool,
    forceMobile: PropTypes.bool,
    isResponsiveCatHeader: PropTypes.bool,
    isFinalResponsiveEspotSolution: PropTypes.bool,
    qubitid: PropTypes.string,
    onContentLoaded: PropTypes.func,
    sandBoxClassName: PropTypes.string,
    cmsPageName: PropTypes.string,
    /**
     * We are forcing the use of the contentType constants to avoid that type of
     * problem in a future => https://arcadiagroup.atlassian.net/browse/DES-4691
     */
    contentType: PropTypes.string.isRequired,
    /**
     * if component receives `segmentationRequestData` it will fetch a responsiveCmsUrl
     * from wcs corresponding to segmented content, which will then be fetched and
     * rendered via MCR - see `getSegmentedContent` action
     */
    segmentationRequestData: PropTypes.shape({
      wcsEndpoint: PropTypes.string,
      responseIdentifier: PropTypes.string,
    }),
  }
  static defaultProps = {
    shouldGetContentOnFirstLoad: false,
    location: {},
    forceMobile: false,
    isResponsiveCatHeader: false,
    isFinalResponsiveEspotSolution: false,
    qubitid: null,
    sandBoxClassName: '',
    cmsPageName: null,
    segmentationRequestData: null,
  }

  componentDidMount() {
    const { visited, shouldGetContentOnFirstLoad } = this.props

    if (visited.length > 1 || shouldGetContentOnFirstLoad) {
      this.getCmsContent({
        ...this.props,
        cmsPageName: this.getCmsPageName(),
      })
    }
    const content = this.getPage(this.props)
    if (content) this.setWindowProps(content.props)
    cmsUtilities.mapMountedSandboxDOMNodeToBundle()
  }

  componentWillReceiveProps(nextProps) {
    const { location, removeContent, isMobile } = this.props
    const { location: nextLocation, isMobile: nextIsMobile } = nextProps

    const nextCmsPageName = this.getCmsPageName(nextProps)

    const changeOfLocation = (location, nextLocation) =>
      location &&
      nextLocation &&
      nextLocation.pathname &&
      decodeURIComponent(nextLocation.pathname) !==
        decodeURIComponent(location.pathname)
    const changeOfPageName = () => this.getCmsPageName() !== nextCmsPageName

    const viewportDeviceTypeHasChanged = isMobile !== nextIsMobile

    if (
      changeOfLocation(location, nextLocation) ||
      changeOfPageName() ||
      viewportDeviceTypeHasChanged
    ) {
      const id = this.getID(this.props)

      if (id) {
        // To avoid memory leak resulting in one React context (visible in React Chrome dev tools) for each navigation to a CMS page.
        cmsUtilities.unmountPreviousSandboxDOMNode(id)
        // The following is necessary in order to avoid getting the pages from the Store (from previous navigation) since this would cause
        // the CMS bundle to not kick in (to not even be downloaded since the Helmet div does not get updated)
        // and the mapping of the bundle with the Sandbox DOM node to be unsuccessful.
        removeContent(id)
      }

      const { forceMobile } = nextProps
      this.getCmsContent({
        ...this.props,
        location: nextLocation,
        cmsPageName: nextCmsPageName,
        forceMobile,
        isMobile: nextIsMobile,
      })
      return
    }

    const content = this.getPage(this.props)
    const nextContent = this.getPage(nextProps)

    if (content !== nextContent && nextContent) {
      this.setWindowProps(nextContent.props)
    }
  }

  getCmsContent = ({
    segmentationRequestData,
    location,
    cmsPageName,
    contentType,
    forceMobile,
    isMobile,
  }) => {
    const { getSegmentedContent, getContent } = this.props
    if (segmentationRequestData && !isMobile) {
      const { wcsEndpoint, responseIdentifier } = segmentationRequestData
      getSegmentedContent(wcsEndpoint, responseIdentifier, cmsPageName)
    } else {
      getContent(
        location,
        cmsPageName,
        contentType,
        undefined,
        undefined,
        undefined,
        forceMobile
      )
    }
  }

  deferredNotifyContentLoaded = () => {
    const { onContentLoaded } = this.props
    if (!this.sandboxRef || !onContentLoaded) return
    const content = this.sandboxRef.firstElementChild
    if (content && content.clientHeight) {
      clearInterval(this.contentLoadedInterval)
      onContentLoaded()
    }
  }

  componentDidUpdate() {
    cmsUtilities.mapMountedSandboxDOMNodeToBundle()
  }

  componentWillUnmount() {
    // Resetting Store.sandbox and React CMS context to manage scenarios like e-receipts > home > e-receipts.
    const { removeContent } = this.props
    const id = this.getID(this.props)
    cmsUtilities.unmountPreviousSandboxDOMNode(id)
    removeContent(id)
    if (this.contentLoadedInterval) clearInterval(this.contentLoadedInterval)
  }

  setWindowProps = (props) => {
    // One scenario for the next one is Internal Server Error in mrCMS.
    if (!props) return

    if (
      !this.props.useMontyCmsForForms &&
      isCmsForm(props.data && props.data.pageData)
    )
      return

    const sandboxDOMNodeId = this.getID(props)

    cmsUtilities.updateNewSandBox(sandboxDOMNodeId, props)
  }

  getID = (propData) => {
    const props = propData || this.props
    const cmsPageName = this.getCmsPageName() || this.getCmsPageName(props)
    if (cmsPageName) return cmsPageName
    if (props.location && props.location.pathname)
      return props.location.pathname
    if (this.props.location && this.props.location.pathname)
      return this.props.location.pathname
    return null
  }

  getPage = (propData) => {
    const props = propData
    const pageId = this.getID(props)
    // TODO: use path/if and make it easy to read
    return props && props.pages
      ? props.pages[pageId]
        ? props.pages[pageId]
        : props.pages[encodeURIComponent(pageId)]
      : null
  }

  getCmsPageName = (propData) => {
    const { route, params, cmsPageName } = propData || this.props

    if (cmsPageName) return cmsPageName
    if (route && route.cmsPageName) return route.cmsPageName
    if (params && params.cmsPageName) return params.cmsPageName
  }

  setSandboxRef = (ref) => {
    this.sandboxRef = ref
    if (ref && this.props.onContentLoaded) {
      this.contentLoadedInterval = setInterval(
        this.deferredNotifyContentLoaded,
        100
      )
    }
  }

  static needs = [
    (location) => sandBoxActions.getContent(location, location.cmsPageName),
  ]

  render() {
    const {
      contentType,
      isInPageContent,
      setFormDefaultSchema,
      setContent,
      location,
      isMobile,
      featureLegacyPages,
      cmsPageName,
      useMontyCmsForForms,
      isResponsiveCatHeader,
      isFinalResponsiveEspotSolution,
      qubitid,
      sandBoxClassName,
    } = this.props

    const isResponsiveEmbeddedContent =
      isResponsiveCatHeader || isFinalResponsiveEspotSolution

    const page = this.getPage(this.props)

    const pathnameLocation = path(['pathname'], location)
    const cmsContentNotResponsive = !page || !page.isResponsive
    const cmsContentIsResponsiveButNotReadyYet =
      page && page.isResponsive && page.initialBody === ''

    const responsiveCatHeaderTemplateAvailable =
      isResponsiveCatHeader && pathnameLocation

    if (
      featureLegacyPages &&
      !isMobile &&
      (pathnameLocation || cmsPageName === 'home') &&
      (cmsContentNotResponsive || cmsContentIsResponsiveButNotReadyYet) &&
      !isResponsiveEmbeddedContent
    ) {
      return <CMSIframe />
    }

    if (
      contentType === cmsConsts.ESPOT_CONTENT_TYPE &&
      !isMobile &&
      !isFinalResponsiveEspotSolution
    ) {
      return null
    }

    const isMainContent =
      !isInPageContent &&
      contentType !== cmsConsts.ESPOT_CONTENT_TYPE &&
      !responsiveCatHeaderTemplateAvailable &&
      !isFinalResponsiveEspotSolution

    if (!page) {
      return isMainContent ? <Loader /> : null
    }

    if (page.cmsTestMode === true) {
      return <div className="CmsMock">CMS content mocked</div>
    }

    // For example when Internal Server Error on mrCMS.
    if (!page.initialBody && isMainContent) {
      return <CmsNotAvailable />
    }

    const pageData =
      page && page.props && page.props.data && page.props.data.pageData

    if (!useMontyCmsForForms && isCmsForm(pageData)) {
      page.props.data.pageData[0] = parseCmsForm(page.props.data.pageData[0])
      setFormDefaultSchema(
        page.props.data.pageData[0] && page.props.data.pageData[0].fieldSchema
      )
      setContent(page.props.location.pathname, page.props.data)
      const propsToCmsWrapper = {
        params: { cmsPageName: page.props.location.pathname },
        dataAlreadyFetchedByMrCMS: true,
      }
      return <CmsWrapper {...propsToCmsWrapper} />
    }

    const title = page.head && page.head.title
    if (
      !isMainContent &&
      title &&
      (title.includes('404') || title.includes('error'))
    ) {
      return null
    }

    const head = !isMainContent ? omit(['title'], page.head) : page.head
    const pageId = this.getID()

    const sandboxInstance = (
      <div
        className={`CmsFrame ${sandBoxClassName}`}
        style={{ display: 'none' }}
      >
        <Helmet {...head} />
        <div
          ref={this.setSandboxRef}
          id={`Sandbox-${pageId}`}
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{ __html: page.initialBody }}
        />
      </div>
    )

    return qubitid && isFinalResponsiveEspotSolution ? (
      <QubitReact id={qubitid}>{sandboxInstance}</QubitReact>
    ) : (
      sandboxInstance
    )
  }
}
