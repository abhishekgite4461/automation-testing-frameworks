import testComponentHelper from 'test/unit/helpers/test-component'
import SandBox from './SandBox'
import * as utils from './tempUtils'
import cmsUtilities from '../../../lib/cms-utilities'
import cmsConsts from '../../../constants/cmsConsts'
import CMSIframe from '../../common/CMSIframe/CMSIframe'
import * as sandboxActions from '../../../actions/common/sandBoxActions'

jest.mock('../../../lib/cms-utilities')
jest.mock('../../../actions/common/sandBoxActions')

utils.parseCmsForm = jest.fn(() => null)

describe('<SandBox/>', () => {
  const initialProps = {
    isFinalResponsiveEspotSolution: false,
    qubitid: 'qubitid',
    visited: [],
    pages: {},
    getContent: jest.fn(),
    getSegmentedContent: jest.fn(),
    removeContent: jest.fn(),
    location: {},
    route: {},
    setContent: jest.fn(),
    setFormDefaultSchema: jest.fn(),
    mrCmsAnalytics: jest.fn(),
  }
  const renderComponent = testComponentHelper(SandBox.WrappedComponent)

  beforeEach(() => {
    jest.resetAllMocks()
  })

  describe('@renders', () => {
    it('renders the <Loader /> in case of no page data available', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })
    it('renders "CMS content mocked" in case of page as object with "cmsTestMode" property set to true', () => {
      expect(
        renderComponent({
          ...initialProps,
          location: { pathname: 'pageUrl' },
          pages: { pageUrl: { cmsTestMode: true } },
        }).getTree()
      ).toMatchSnapshot()
    })
    it('renders <CmsNotAvailable /> in case of page as object with no property initialBody', () => {
      expect(
        renderComponent({
          ...initialProps,
          location: { pathname: 'pageUrl' },
          pages: { pageUrl: {} },
        }).getTree()
      ).toMatchSnapshot()
    })
    it('renders <CMSIframe/> if the CMS content is responsive but not ready yet', () => {
      expect(
        renderComponent({
          ...initialProps,
          featureLegacyPages: true,
          location: { pathname: 'pageUrl' },
          pages: {
            pageUrl: {
              initialBody: '',
              isResponsive: true,
            },
          },
        }).getTree()
      ).toMatchSnapshot()
    })
    it('renders <CMSIframe/> if the isFinalResponsiveEspotSolution or isResponsiveCatHeader is false', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        isFinalResponsiveEspotSolution: true,
        isResponsiveCatHeader: true,
        featureLegacyPages: true,
        location: { pathname: 'pageUrl' },
        pages: {
          pageUrl: {
            initialBody: '',
            isResponsive: true,
          },
        },
      })
      expect(wrapper.find(CMSIframe)).toHaveLength(0)
      wrapper.setProps({
        isFinalResponsiveEspotSolution: false,
        isResponsiveCatHeader: false,
      })
      expect(wrapper.find(CMSIframe)).toHaveLength(1)
    })
    it('should return null if content type is espot, viewport is not mobile, and isFinalResponsiveEspotSolution is false', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        contentType: 'espot',
        isMobile: false,
        isFinalResponsiveEspotSolution: false,
      })
      expect(wrapper.html()).toBeNull()
    })
    it('should return null if not isMainContent', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        contentType: 'espot',
        isMobile: false,
        isFinalResponsiveEspotSolution: true,
        isInPageContent: false,
        isResponsiveCatHeader: false,
        location: {},
      })
      expect(wrapper.html()).toBeNull()
    })
    it('does not render <CMSIframe/> if the CMS content is responsive, not ready yet and associated to an espot', () => {
      expect(
        renderComponent({
          ...initialProps,
          featureLegacyPages: true,
          cmsPageName: 'espot1',
          pages: {
            espot1: {
              initialBody: '',
              isResponsive: true,
            },
          },
        }).getTree()
      ).toMatchSnapshot()
    })
    it('renders <CMSIframe/> if the CMS content is responsive, not ready yet and associated to the home CMS content via page name', () => {
      expect(
        renderComponent({
          ...initialProps,
          featureLegacyPages: true,
          cmsPageName: 'home',
          pages: {
            home: {
              initialBody: '',
              isResponsive: true,
            },
          },
        }).getTree()
      ).toMatchSnapshot()
    })
    it('renders <CmsWrapper /> in case of props associated to CMS form page', () => {
      expect(
        renderComponent({
          ...initialProps,
          location: { pathname: 'pageUrl' },
          pages: {
            pageUrl: {
              initialBody: {},
              props: {
                location: { pathname: 'abc' },
                data: {
                  pageData: [{ formCss: 'abc' }],
                },
              },
            },
          },
        }).getTree()
      ).toMatchSnapshot()
    })
    it('renders <CmsFrame /> and the Sandbox-pageId DOM node', () => {
      expect(
        renderComponent({
          ...initialProps,
          location: { pathname: 'pageUrl' },
          pages: {
            pageUrl: {
              initialBody: {},
              props: {
                location: { pathname: 'abc' },
                data: { pageData: [{}] },
              },
            },
          },
        }).getTree()
      ).toMatchSnapshot()
    })
    it('with <CMSIframe/> if feature is enabled and is not mobile', () => {
      const { wrapper, getTree } = renderComponent({
        ...initialProps,
        location: { pathname: 'url' },
        featureLegacyPages: true,
        isMobile: false,
      })
      expect(getTree()).toMatchSnapshot()
      expect(wrapper.find('Connect(CMSIframe)').length).toBe(1)
    })
    it('without <CMSIframe/> if CMS page is responsive', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        location: { pathname: 'url' },
        featureLegacyPages: true,
        isMobile: false,
        cmsPageName: 'cmsPage',
        pages: {
          cmsPage: {
            isResponsive: true,
          },
        },
      })

      expect(wrapper.find('Connect(CMSIframe)').length).toBe(0)
    })
    it('responsive CMS content', () => {
      const { getTree } = renderComponent({
        ...initialProps,
        location: { pathname: 'url' },
        featureLegacyPages: true,
        isMobile: false,
        cmsPageName: 'cmsPage',
        pages: {
          cmsPage: {
            isResponsive: true,
            initialBody: '<div>initial body</div>',
          },
        },
      })

      expect(getTree()).toMatchSnapshot()
    })
    it('without <CMSIframe/> if feature is enabled and is mobile', () => {
      const { wrapper, getTree } = renderComponent({
        ...initialProps,
        location: { pathname: 'url' },
        featureLegacyPages: true,
        isMobile: true,
      })
      expect(getTree()).toMatchSnapshot()
      expect(wrapper.find('Connect(CMSIframe)').length).toBe(0)
    })
    it('with <CMSIframe /> if feature is enabled and is not mobile and is cmsPageName = `home`', () => {
      const { wrapper, getTree } = renderComponent({
        ...initialProps,
        cmsPageName: 'home',
        featureLegacyPages: true,
        isMobile: false,
      })
      expect(getTree()).toMatchSnapshot()
      expect(wrapper.find('Connect(CMSIframe)').length).toBe(1)
    })
    it('without <CMSIframe /> if feature is enabled and is not mobile and is not cmsPageName = `home` and np location.pathname', () => {
      const { wrapper, getTree } = renderComponent({
        ...initialProps,
        featureLegacyPages: true,
        isMobile: false,
      })
      expect(getTree()).toMatchSnapshot()
      expect(wrapper.find('Connect(CMSIframe)').length).toBe(0)
    })

    it('with mobile espot when on mobile', () => {
      const pageId = 'espotPageId'
      const { wrapper } = renderComponent({
        ...initialProps,
        contentType: cmsConsts.ESPOT_CONTENT_TYPE,
        isMobile: true,
        pages: { [pageId]: {} },
        location: { pathname: pageId },
      })

      expect(wrapper.find('.CmsFrame')).toHaveLength(1)
      expect(wrapper.find(`#Sandbox-${pageId}`)).toHaveLength(1)
    })

    it('renders QubitReact when qubitid is provided and isFinalResponsiveEspotSolution is true', () => {
      const pageId = 'espotPageId'
      expect(
        renderComponent({
          ...initialProps,
          contentType: cmsConsts.ESPOT_CONTENT_TYPE,
          isMobile: true,
          pages: { [pageId]: {} },
          location: { pathname: pageId },
          qubitid: 'id',
          isFinalResponsiveEspotSolution: true,
        }).getTree()
      ).toMatchSnapshot()
    })

    it('renders default sandbox content when qubitid is not provided and isFinalResponsiveEspotSolution is false', () => {
      const pageId = 'espotPageId'
      expect(
        renderComponent({
          ...initialProps,
          contentType: cmsConsts.ESPOT_CONTENT_TYPE,
          isMobile: true,
          pages: { [pageId]: {} },
          location: { pathname: pageId },
          isFinalResponsiveEspotSolution: false,
        }).getTree()
      ).toMatchSnapshot()
    })

    it('with content type not espot when not on mobile', () => {
      const pageId = 'somePageId'
      const { wrapper } = renderComponent({
        ...initialProps,
        contentType: cmsConsts.PAGE_CONTENT_TYPE,
        isMobile: false,
        pages: { [pageId]: { initialBody: 'some content' } },
        location: { pathname: pageId },
      })

      expect(wrapper.find('.CmsFrame')).toHaveLength(1)
      expect(wrapper.find(`#Sandbox-${pageId}`)).toHaveLength(1)
    })

    it('should apply the sandBoxClassName to the sandbox instance', () => {
      const pageId = 'espotPageId'
      const { wrapper } = renderComponent({
        ...initialProps,
        contentType: cmsConsts.PAGE_CONTENT_TYPE,
        isMobile: false,
        pages: { [pageId]: { initialBody: 'some content' } },
        location: { pathname: pageId },
        isResponsiveEspotEnabled: true,
        sandBoxClassName: 'Espot--whatever',
      })
      expect(wrapper.find('.CmsFrame').hasClass('Espot--whatever')).toBe(true)
    })

    it('with hidden mobile espot when on desktop and responsive espots are enabled', () => {
      const pageId = 'espotPageId'
      const { wrapper } = renderComponent({
        ...initialProps,
        contentType: cmsConsts.ESPOT_CONTENT_TYPE,
        isMobile: false,
        pages: { [pageId]: {} },
        location: { pathname: pageId },
      })

      expect(wrapper.find('.CmsFrame')).toHaveLength(0)
      expect(wrapper.find(`#Sandbox-${pageId}`)).toHaveLength(0)
    })

    it('without <CMSIframe/> if rendering responsive cat header', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        location: { pathname: 'url' },
        featureLegacyPages: true,
        isMobile: false,
        cmsPageName: 'cmsPage',
        pages: {
          cmsPage: {},
        },
        isResponsiveCatHeader: true,
      })

      expect(wrapper.find('Connect(CMSIframe)').length).toBe(0)
    })
  })

  describe('static needs', () => {
    it('should call getContent', () => {
      const { needs } = SandBox.WrappedComponent
      const cmsPageName = 'foo'
      jest.spyOn(sandboxActions, 'getContent')
      expect(needs).toHaveLength(1)
      needs[0]({ cmsPageName })
      expect(sandboxActions.getContent).toHaveBeenCalledTimes(1)
      expect(sandboxActions.getContent).toHaveBeenCalledWith(
        { cmsPageName },
        cmsPageName
      )
    })
  })

  describe('@lifecycle', () => {
    describe('on componentDidMount', () => {
      beforeEach(() => initialProps.getContent.mockReset())

      const segmentationRequestData = {
        wcsEndpoint: '/endpoint',
        responseIdentifier: 'id1',
      }

      describe('in case of page client side rendered', () => {
        describe('if segmentationRequestData provided and device is not mobile', () => {
          it('should call getSegmentedContent once', () => {
            const renderedComponent = renderComponent({
              ...initialProps,
              visited: ['/', '/abc'],
              cmsPageName: 'name',
              segmentationRequestData,
              isMobile: false,
            })
            const { instance } = renderedComponent
            expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
            instance.componentDidMount()
            expect(initialProps.getSegmentedContent).toHaveBeenCalledTimes(1)
            expect(initialProps.getSegmentedContent).toHaveBeenCalledWith(
              segmentationRequestData.wcsEndpoint,
              segmentationRequestData.responseIdentifier,
              'name'
            )
            expect(initialProps.getContent).not.toHaveBeenCalled()
          })
        })
        describe('if segmentationRequestData provided and device is mobile', () => {
          it('should call getContent once', () => {
            const renderedComponent = renderComponent({
              ...initialProps,
              visited: ['/', '/abc'],
              cmsPageName: 'name',
              segmentationRequestData,
              isMobile: true,
            })
            const { instance } = renderedComponent
            expect(initialProps.getContent).not.toHaveBeenCalled()
            instance.componentDidMount()
            expect(initialProps.getContent).toHaveBeenCalledTimes(1)
            expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
          })
        })
        describe('if segmentationRequestData NOT provided', () => {
          it('should call getContent once', () => {
            const renderedComponent = renderComponent({
              ...initialProps,
              visited: ['/', '/abc'],
            })
            const { instance } = renderedComponent
            expect(initialProps.getContent).not.toHaveBeenCalled()
            instance.componentDidMount()
            expect(initialProps.getContent).toHaveBeenCalledTimes(1)
            expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
          })
        })
      })
      describe('when shouldGetContentOnFirstLoad is true', () => {
        describe('if segmentationRequestData provided and device is not mobile', () => {
          it('should call getSegmentedContent', () => {
            const renderedComponent = renderComponent({
              ...initialProps,
              visited: ['/'],
              shouldGetContentOnFirstLoad: true,
              cmsPageName: 'name',
              segmentationRequestData,
              isMobile: false,
            })
            const { instance } = renderedComponent
            expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
            instance.componentDidMount()
            expect(initialProps.getSegmentedContent).toHaveBeenCalledTimes(1)
            expect(initialProps.getSegmentedContent).toHaveBeenCalledWith(
              segmentationRequestData.wcsEndpoint,
              segmentationRequestData.responseIdentifier,
              'name'
            )
            expect(initialProps.getContent).not.toHaveBeenCalled()
          })
        })
        describe('if segmentationRequestData provided and device is mobile', () => {
          it('should call getContent', () => {
            const renderedComponent = renderComponent({
              ...initialProps,
              visited: ['/'],
              shouldGetContentOnFirstLoad: true,
              cmsPageName: 'name',
              segmentationRequestData,
              isMobile: true,
            })
            const { instance } = renderedComponent
            expect(initialProps.getContent).not.toHaveBeenCalled()
            instance.componentDidMount()
            expect(initialProps.getContent).toHaveBeenCalledTimes(1)
            expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
          })
        })
        describe('if segmentationRequestData NOT provided', () => {
          it('should call getContent', () => {
            const renderedComponent = renderComponent({
              ...initialProps,
              visited: ['/'],
              shouldGetContentOnFirstLoad: true,
            })
            const { instance } = renderedComponent
            expect(initialProps.getContent).not.toHaveBeenCalled()
            instance.componentDidMount()
            expect(initialProps.getContent).toHaveBeenCalledTimes(1)
            expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
          })
        })
      })
      it('does not call getSegmentedContent or getContent in case of page server side rendered', () => {
        const renderedComponent = renderComponent({
          ...initialProps,
          visited: ['/'],
        })
        const { instance } = renderedComponent
        expect(initialProps.getContent).not.toHaveBeenCalled()
        expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
        instance.componentDidMount()
        expect(initialProps.getContent).not.toHaveBeenCalled()
        expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
      })
      it('does not call setWindowProps in case of no content from getPage function', () => {
        const renderedComponent = renderComponent({
          ...initialProps,
          visited: ['/', '/abc'],
        })
        const { instance } = renderedComponent
        instance.setWindowProps = jest.fn()
        instance.getPage = jest.fn(() => null)
        expect(instance.setWindowProps).not.toHaveBeenCalled()
        instance.componentDidMount()
        expect(instance.setWindowProps).not.toHaveBeenCalled()
      })
      it('calls once setWindowProps in case of content from getPage function', () => {
        const renderedComponent = renderComponent({
          ...initialProps,
          visited: ['/', '/abc'],
        })
        const { instance } = renderedComponent
        instance.setWindowProps = jest.fn()
        instance.getPage = jest.fn(() => {
          return { props: 'abc' }
        })
        expect(instance.setWindowProps).not.toHaveBeenCalled()
        instance.componentDidMount()
        expect(instance.setWindowProps).toHaveBeenCalledTimes(1)
        expect(instance.setWindowProps).toHaveBeenCalledWith('abc')
      })
      it('calls cmsUtilities.mapMountedSandboxDOMNodeToBundle once', () => {
        const renderedComponent = renderComponent({
          ...initialProps,
          visited: ['/', '/abc'],
        })
        const { instance } = renderedComponent
        expect(
          cmsUtilities.mapMountedSandboxDOMNodeToBundle
        ).not.toHaveBeenCalled()
        instance.componentDidMount()
        expect(
          cmsUtilities.mapMountedSandboxDOMNodeToBundle
        ).toHaveBeenCalledTimes(1)
      })
      describe('if getSegmentationData is NOT provided', () => {
        it('calls getContent passing true as 7th argument when forceMobile prop is set to true', () => {
          const renderedComponent = renderComponent({
            ...initialProps,
            visited: ['/', '/abc'],
            forceMobile: true,
          })
          const { instance } = renderedComponent
          instance.componentDidMount()

          expect(initialProps.getContent).toHaveBeenCalledWith(
            {},
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            true
          )
        })
        it('calls getContent passing undefined as 7th argument when forceMobile prop is not set to true', () => {
          const renderedComponent = renderComponent({
            ...initialProps,
            visited: ['/', '/abc'],
          })
          const { instance } = renderedComponent

          instance.componentDidMount()

          expect(initialProps.getContent).toHaveBeenCalledWith(
            {},
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            false
          )
        })
      })
    })

    describe('on componentWillReceiveProps', () => {
      beforeEach(() => initialProps.getContent.mockReset())
      beforeEach(() => initialProps.removeContent.mockReset())

      const segmentationRequestData = {
        wcsEndpoint: '/endpoint',
        responseIdentifier: 'id1',
      }

      describe('when location has changed', () => {
        it('should call unmountPreviousSandboxDOMNode from cmsUtilities and removeContent', () => {
          const renderedComponent = renderComponent({
            ...initialProps,
            cmsPageName: 'name',
          })
          const { wrapper } = renderedComponent
          expect(
            cmsUtilities.unmountPreviousSandboxDOMNode
          ).not.toHaveBeenCalled()
          expect(initialProps.removeContent).not.toHaveBeenCalled()
          expect(initialProps.getContent).not.toHaveBeenCalled()
          wrapper.setProps({ location: { pathname: 'abc' } })
          expect(
            cmsUtilities.unmountPreviousSandboxDOMNode
          ).toHaveBeenCalledTimes(1)
          expect(initialProps.removeContent).toHaveBeenCalledTimes(1)
          expect(initialProps.removeContent).toHaveBeenCalledWith('name')
        })

        describe('if segmentationRequestData is provided and device is not mobile', () => {
          it('should call getSegmentedContent', () => {
            const renderedComponent = renderComponent({
              ...initialProps,
              segmentationRequestData,
              cmsPageName: 'name',
              isMobile: false,
            })
            const { wrapper } = renderedComponent
            expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
            wrapper.setProps({ location: { pathname: 'abc' } })
            expect(initialProps.getSegmentedContent).toHaveBeenCalledTimes(1)
            expect(initialProps.getSegmentedContent).toHaveBeenCalledWith(
              segmentationRequestData.wcsEndpoint,
              segmentationRequestData.responseIdentifier,
              'name'
            )
            expect(initialProps.getContent).not.toHaveBeenCalled()
          })
        })

        describe('if segmentationRequestData is provided and device is mobile', () => {
          const { wrapper } = renderComponent({
            ...initialProps,
            segmentationRequestData,
            cmsPageName: 'name',
            isMobile: true,
          })
          expect(initialProps.getContent).not.toHaveBeenCalled()
          wrapper.setProps({ location: { pathname: 'abc' } })
          expect(initialProps.getContent).toHaveBeenCalledTimes(1)
          expect(initialProps.getContent).toHaveBeenCalledWith(
            { pathname: 'abc' },
            'name',
            undefined,
            undefined,
            undefined,
            undefined,
            false
          )
          expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
        })

        describe('if segmentationRequestData is NOT provided', () => {
          it('should call getContent', () => {
            const renderedComponent = renderComponent({
              ...initialProps,
              cmsPageName: 'name',
            })
            const { wrapper } = renderedComponent
            expect(initialProps.getContent).not.toHaveBeenCalled()
            wrapper.setProps({ location: { pathname: 'abc' } })
            expect(initialProps.getContent).toHaveBeenCalledTimes(1)
            expect(initialProps.getContent).toHaveBeenCalledWith(
              { pathname: 'abc' },
              'name',
              undefined,
              undefined,
              undefined,
              undefined,
              false
            )
            expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
          })
        })
      })
      describe('when page name changes', () => {
        it('should call unmountPreviousSandboxDOMNode from cmsUtilities and removeContent', () => {
          const renderedComponent = renderComponent({
            ...initialProps,
            cmsPageName: 'name',
          })
          const { wrapper } = renderedComponent
          expect(
            cmsUtilities.unmountPreviousSandboxDOMNode
          ).not.toHaveBeenCalled()
          expect(initialProps.removeContent).not.toHaveBeenCalled()
          expect(initialProps.getContent).not.toHaveBeenCalled()
          wrapper.setProps({ location: {}, cmsPageName: 'abc' })
          expect(
            cmsUtilities.unmountPreviousSandboxDOMNode
          ).toHaveBeenCalledTimes(1)
          expect(initialProps.removeContent).toHaveBeenCalledTimes(1)
          expect(initialProps.removeContent).toHaveBeenCalledWith('name')
        })
        describe('if segmentationRequestData is provided', () => {
          it('should call getSegmentedContent with new page name', () => {
            const renderedComponent = renderComponent({
              ...initialProps,
              segmentationRequestData,
              cmsPageName: 'name',
            })
            const { wrapper } = renderedComponent
            expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
            wrapper.setProps({ location: {}, cmsPageName: 'abc' })
            expect(initialProps.getSegmentedContent).toHaveBeenCalledTimes(1)
            expect(initialProps.getSegmentedContent).toHaveBeenCalledWith(
              segmentationRequestData.wcsEndpoint,
              segmentationRequestData.responseIdentifier,
              'abc'
            )
            expect(initialProps.getContent).not.toHaveBeenCalled()
          })
        })
        describe('if segmentationRequestData is NOT provided', () => {
          it('should call getContent with new page name', () => {
            const renderedComponent = renderComponent({
              ...initialProps,
              cmsPageName: 'name',
            })
            const { wrapper } = renderedComponent
            expect(initialProps.getContent).not.toHaveBeenCalled()
            wrapper.setProps({ location: {}, cmsPageName: 'abc' })
            expect(initialProps.getContent).toHaveBeenCalledTimes(1)
            expect(initialProps.getContent).toHaveBeenCalledWith(
              {},
              'abc',
              undefined,
              undefined,
              undefined,
              undefined,
              false
            )
            expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
          })
        })
      })
      it('no id - does not call unmountPreviousSandboxDOMNode, removeContent', () => {
        const renderedComponent = renderComponent(initialProps)
        const { wrapper } = renderedComponent
        expect(
          cmsUtilities.unmountPreviousSandboxDOMNode
        ).not.toHaveBeenCalled()
        expect(initialProps.removeContent).not.toHaveBeenCalled()
        expect(initialProps.getContent).not.toHaveBeenCalled()
        wrapper.setProps({ location: {}, cmsPageName: 'abc' })
        expect(
          cmsUtilities.unmountPreviousSandboxDOMNode
        ).not.toHaveBeenCalled()
        expect(initialProps.removeContent).not.toHaveBeenCalled()
        expect(initialProps.getContent).toHaveBeenCalledTimes(1)
        expect(initialProps.getContent).toHaveBeenCalledWith(
          {},
          'abc',
          undefined,
          undefined,
          undefined,
          undefined,
          false
        )
      })
      describe('when location and page name stay the same', () => {
        describe('if segmentationRequestData provided', () => {
          it('should not call unmountPreviousSandboxDOMNode, removeContent, or getSegmentedContent', () => {
            const renderedComponent = renderComponent({
              ...initialProps,
              segmentationRequestData,
            })
            const { wrapper } = renderedComponent
            expect(
              cmsUtilities.unmountPreviousSandboxDOMNode
            ).not.toHaveBeenCalled()
            expect(initialProps.removeContent).not.toHaveBeenCalled()
            expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
            wrapper.setProps({ pages: { abc: 'def' } })
            expect(
              cmsUtilities.unmountPreviousSandboxDOMNode
            ).not.toHaveBeenCalled()
            expect(initialProps.removeContent).not.toHaveBeenCalled()
            expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
          })
        })
        describe('if segmentationRequestData NOT provided', () => {
          it('should not call unmountPreviousSandboxDOMNode, removeContent, or getContent', () => {
            const renderedComponent = renderComponent({ ...initialProps })
            const { wrapper } = renderedComponent
            expect(
              cmsUtilities.unmountPreviousSandboxDOMNode
            ).not.toHaveBeenCalled()
            expect(initialProps.removeContent).not.toHaveBeenCalled()
            expect(initialProps.getContent).not.toHaveBeenCalled()
            wrapper.setProps({ pages: { abc: 'def' } })
            expect(
              cmsUtilities.unmountPreviousSandboxDOMNode
            ).not.toHaveBeenCalled()
            expect(initialProps.removeContent).not.toHaveBeenCalled()
            expect(initialProps.getContent).not.toHaveBeenCalled()
          })
        })
      })

      it('calls setWindowProps in case of changed page content', () => {
        const renderedComponent = renderComponent({
          ...initialProps,
          location: { pathname: 'abc' },
        })
        const { wrapper, instance } = renderedComponent
        instance.setWindowProps = jest.fn()
        expect(instance.setWindowProps).not.toHaveBeenCalled()
        wrapper.setProps({
          location: { pathname: 'abc' },
          pages: { abc: 'def' },
        })
        expect(instance.setWindowProps).toHaveBeenCalledTimes(1)
      })

      describe('when viewport deviceType did not change', () => {
        describe('if segmentationRequestData provided', () => {
          it('should not call getSegmentedContent', () => {
            const renderedComponent = renderComponent({
              ...initialProps,
              segmentationRequestData,
              isMobile: false,
            })
            const { wrapper } = renderedComponent

            wrapper.setProps({ isMobile: false })

            expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
          })
        })
        describe('segmentationRequestData NOT provided', () => {
          it('should not call getContent', () => {
            const renderedComponent = renderComponent({
              ...initialProps,
              isMobile: false,
            })
            const { wrapper } = renderedComponent

            wrapper.setProps({ isMobile: false })

            expect(initialProps.getContent).not.toHaveBeenCalled()
          })
        })
      })

      describe('when viewport deviceType is changed', () => {
        describe('if segmentationRequestData is provided', () => {
          it('should call getContent if new deviceType is mobile', () => {
            const renderedComponent = renderComponent({
              ...initialProps,
              segmentationRequestData,
              isMobile: false,
            })
            const { wrapper } = renderedComponent

            expect(initialProps.getContent).not.toHaveBeenCalled()

            wrapper.setProps({ isMobile: true })

            expect(initialProps.getContent).toHaveBeenCalledTimes(1)
            expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
          })

          it('should call getSegmentedContent if new deviceType is not mobile', () => {
            const renderedComponent = renderComponent({
              ...initialProps,
              isMobile: true,
            })
            const { wrapper } = renderedComponent

            expect(initialProps.getContent).not.toHaveBeenCalled()

            wrapper.setProps({ isMobile: false })

            expect(initialProps.getContent).toHaveBeenCalledTimes(1)
            expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
          })
        })

        describe('if segmentationRequestData is NOT provided', () => {
          it('should call getContent', () => {
            const renderedComponent = renderComponent({
              ...initialProps,
              isMobile: false,
            })
            const { wrapper } = renderedComponent

            expect(initialProps.getContent).not.toHaveBeenCalled()

            wrapper.setProps({ isMobile: true })

            expect(initialProps.getContent).toHaveBeenCalledTimes(1)
            expect(initialProps.getSegmentedContent).not.toHaveBeenCalled()
          })
        })
      })

      it('calls getContent providing true as 7th argument when next props contains forceMobile=true', () => {
        const { wrapper } = renderComponent(initialProps)

        wrapper.setProps({
          location: {},
          cmsPageName: 'abc',
          forceMobile: true,
        })

        expect(initialProps.getContent).toHaveBeenCalledWith(
          {},
          'abc',
          undefined,
          undefined,
          undefined,
          undefined,
          true
        )
      })

      it('calls getContent providing undefined as 7th argument when next props does not contains forceMobile', () => {
        const { wrapper } = renderComponent(initialProps)

        wrapper.setProps({ location: {}, cmsPageName: 'abc' })

        expect(initialProps.getContent).toHaveBeenCalledWith(
          {},
          'abc',
          undefined,
          undefined,
          undefined,
          undefined,
          false
        )
      })
    })

    describe('on componentDidUpdate', () => {
      it('calls cmsUtilities.mapMountedSandboxDOMNodeToBundle once', () => {
        const renderedComponent = renderComponent({
          ...initialProps,
          visited: ['/', '/abc'],
        })
        const { instance } = renderedComponent
        expect(
          cmsUtilities.mapMountedSandboxDOMNodeToBundle
        ).not.toHaveBeenCalled()
        instance.componentDidUpdate()
        expect(
          cmsUtilities.mapMountedSandboxDOMNodeToBundle
        ).toHaveBeenCalledTimes(1)
      })
    })

    describe('on componentWillUnmount', () => {
      it('calls once unmountPreviousSandboxDOMNode', () => {
        cmsUtilities.unmountPreviousSandboxDOMNode = jest.fn()
        expect(
          cmsUtilities.unmountPreviousSandboxDOMNode
        ).not.toHaveBeenCalled()
        const renderedComponent = renderComponent({ ...initialProps })
        const { instance } = renderedComponent
        instance.componentWillUnmount()
        expect(
          cmsUtilities.unmountPreviousSandboxDOMNode
        ).toHaveBeenCalledTimes(1)
      })
      it('calls once this.props.removeContent', () => {
        const spyOnremoveContent = jest.fn()
        const { instance } = renderComponent({
          ...initialProps,
          removeContent: spyOnremoveContent,
        })
        expect(spyOnremoveContent).not.toHaveBeenCalled()
        instance.componentWillUnmount()
        expect(spyOnremoveContent).toHaveBeenCalledTimes(1)
      })
    })

    describe('getPage', () => {
      it('should return null if empty object is passed to getPage', () => {
        const { instance } = renderComponent(initialProps)
        expect(instance.getPage({})).toBeNull()
      })

      it('should return page id if found by getPage', () => {
        const { instance } = renderComponent(initialProps)
        const pageId = 'page id'
        jest.spyOn(instance, 'getID').mockReturnValue(pageId)
        expect(
          instance.getPage({
            pages: {
              [pageId]: pageId,
            },
          })
        ).toBe(pageId)
      })
    })

    // TODO
    // - test setWindowProps
    // - test getCmsPageName
  })

  describe('@instance methods', () => {
    describe('deferredNotifyContentLoaded', () => {
      const onContentLoaded = jest.fn()

      beforeEach(() => jest.clearAllMocks())

      it('does not fail if callback not provided', () => {
        const { instance } = renderComponent({
          ...initialProps,
        })
        instance.deferredNotifyContentLoaded()
      })
      it('does not fail if sandboxRef not set', () => {
        const { instance } = renderComponent({
          ...initialProps,
          onContentLoaded,
        })
        expect(onContentLoaded).not.toHaveBeenCalled()
        instance.deferredNotifyContentLoaded()
        expect(onContentLoaded).not.toHaveBeenCalled()
      })
      it('does not call callback if sandboxRef does not have a firstChildElement', () => {
        const { instance } = renderComponent({
          ...initialProps,
          onContentLoaded,
        })
        instance.sandboxRef = {}
        expect(onContentLoaded).not.toHaveBeenCalled()
        instance.deferredNotifyContentLoaded()
        expect(onContentLoaded).not.toHaveBeenCalled()
      })
      it("does not call callback if sandboxRef's firstChildElement does not have height", () => {
        const { instance } = renderComponent({
          ...initialProps,
          onContentLoaded,
        })
        instance.sandboxRef = {
          firstElementChild: {
            clientHeight: 0,
          },
        }
        expect(onContentLoaded).not.toHaveBeenCalled()
        instance.deferredNotifyContentLoaded()
        expect(onContentLoaded).not.toHaveBeenCalled()
      })
      it("invokes callback if sandboxRef's firstChildElement does have height", () => {
        const { instance } = renderComponent({
          ...initialProps,
          onContentLoaded,
        })
        instance.sandboxRef = {
          firstElementChild: {
            clientHeight: 1,
          },
        }
        expect(onContentLoaded).not.toHaveBeenCalled()
        instance.deferredNotifyContentLoaded()
        expect(onContentLoaded).toHaveBeenCalled()
      })
    })
  })
})
