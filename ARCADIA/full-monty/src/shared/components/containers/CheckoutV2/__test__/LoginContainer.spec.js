import { compose } from 'ramda'
import * as reactRouter from 'react-router'

import testComponentHelper, {
  analyticsDecoratorHelper,
  buildComponentRender,
  mountRender,
  withStore,
} from 'test/unit/helpers/test-component'
import { sendEvent } from '../../../../actions/common/googleAnalyticsActions'

import Login from '../../Login/Login'

import LoginContainer, { WrappedLoginContainer } from '../LoginContainer'

jest.mock('react-router')

jest.mock('../../../../actions/common/googleAnalyticsActions')

describe('<LoginContainer />', () => {
  const renderComponent = testComponentHelper(WrappedLoginContainer)

  const getOrderSummaryMock = jest.fn()
  const emptyOrderSummaryMock = jest.fn()

  const requiredProps = {
    getOrderSummary: getOrderSummaryMock,
    emptyOrderSummary: emptyOrderSummaryMock,
    sendEvent,
    getNextRoute: jest.fn(),
    isShoppingBagEmpty: false,
  }

  let oldWindow
  beforeAll(() => {
    oldWindow = window
    window.scrollTo = jest.fn()
  })

  afterEach(() => {
    getOrderSummaryMock.mockReset()
    reactRouter.browserHistory.push.mockImplementation(jest.fn)
    window.scrollTo.mockReset()
  })

  afterAll(() => {
    window.scrollTo = oldWindow.scrollTo
  })

  describe('@renders', () => {
    it('in default state', () => {
      const { getTree } = renderComponent(requiredProps)
      expect(getTree()).toMatchSnapshot()
    })
    it('should pass this.onLoginSuccess method as successCallback', () => {
      const { wrapper, instance } = renderComponent(requiredProps)
      expect(
        wrapper.find(Login).prop('successCallback', instance.onLoginSuccess)
      )
    })
  })

  describe('@decorators', () => {
    analyticsDecoratorHelper(LoginContainer, 'checkout-login', {
      componentName: 'LoginContainer',
      isAsync: true,
      redux: true,
    })
  })

  describe('@lifecycle methods', () => {
    describe('componentDidMount', () => {
      it('should scroll to top of page', () => {
        const mountComponent = buildComponentRender(
          compose(
            mountRender,
            withStore({
              forms: {
                register: {
                  fields: {
                    email: {},
                  },
                },
                login: {
                  fields: {
                    password: {},
                  },
                },
                forgetPassword: {
                  fields: {
                    email: {},
                  },
                },
              },
              config: {
                brandName: 'topshop',
                region: 'gb',
              },
              account: {},
            })
          ),
          WrappedLoginContainer
        )

        expect(window.scrollTo).not.toHaveBeenCalled()
        mountComponent(requiredProps)
        expect(window.scrollTo).toHaveBeenCalled()
        expect(window.scrollTo).toHaveBeenCalledWith(0, 0)
      })
    })
  })

  describe('@instance methods', () => {
    it('should call `getOrderSummary` on successful login if bag has > 0 items', () => {
      const { instance } = renderComponent({
        ...requiredProps,
        isShoppingBagEmpty: false,
      })
      instance.onLoginSuccess()
      expect(getOrderSummaryMock).toHaveBeenCalled()
      expect(emptyOrderSummaryMock).not.toHaveBeenCalled()
    })

    it('should call `emptyOrderSummary` and redirect to homepage on successful login if bag has 0 items', () => {
      const { instance } = renderComponent({
        ...requiredProps,
        isShoppingBagEmpty: true,
      })
      instance.onLoginSuccess()
      expect(getOrderSummaryMock).not.toHaveBeenCalled()
      expect(emptyOrderSummaryMock).toHaveBeenCalled()
    })
  })
})
