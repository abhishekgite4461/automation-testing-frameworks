import React, { Component } from 'react'
import PropTypes from 'prop-types'
import QubitReact from 'qubit-react/wrapper'
import Helmet from 'react-helmet'
import { connect } from 'react-redux'
import { sendEvent } from '../../../actions/common/googleAnalyticsActions'
import analyticsDecorator from '../../../../client/lib/analytics/analytics-decorator'
import { isShoppingBagEmpty } from '../../../selectors/shoppingBagSelectors'

// actions
import {
  getOrderSummary,
  emptyOrderSummary,
} from '../../../actions/common/checkoutActions'

// components
import ForgetPassword from '../ForgetPassword/ForgetPassword'
import Login from '../../../components/containers/Login/Login'
import Register from '../../../components/containers/Register/Register'

class LoginContainer extends Component {
  static propTypes = {
    getOrderSummary: PropTypes.func.isRequired,
    sendEvent: PropTypes.func.isRequired,
    isShoppingBagEmpty: PropTypes.bool.isRequired,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentDidMount() {
    if (window) window.scrollTo(0, 0)
    this.props.sendEvent('Checkout', 'Login', 'Sign In')
  }

  onLoginSuccess = () => {
    const {
      getOrderSummary,
      emptyOrderSummary,
      isShoppingBagEmpty,
    } = this.props
    if (isShoppingBagEmpty) emptyOrderSummary()
    else getOrderSummary()
  }

  getLoginNextRoute = (loginResponse) => {
    if (loginResponse.basketItemCount === 0) return '/'
    return '/checkout'
  }

  getRegisterNextRoute = () => {
    return '/checkout?new-user'
  }

  render() {
    const { l } = this.context
    return (
      <QubitReact id="qubit-checkoutContainer">
        <section className="LoginContainer">
          <Helmet title={l`Sign In`} />
          <section className="LoginContainer-loginSection">
            <Login
              getNextRoute={this.getLoginNextRoute}
              successCallback={this.onLoginSuccess}
            />
            <ForgetPassword />
          </section>
          <section className="LoginContainer-newUserSection">
            <Register getNextRoute={this.getRegisterNextRoute} />
          </section>
        </section>
      </QubitReact>
    )
  }
}

const mapDispatchToProps = {
  emptyOrderSummary,
  getOrderSummary,
  sendEvent,
}

export default analyticsDecorator('checkout-login', { isAsync: true })(
  connect(
    (state) => ({
      isShoppingBagEmpty: isShoppingBagEmpty(state),
    }),
    mapDispatchToProps
  )(LoginContainer)
)

export { LoginContainer as WrappedLoginContainer, mapDispatchToProps }
