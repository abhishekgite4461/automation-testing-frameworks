import PropTypes from 'prop-types'
import QubitReact from 'qubit-react/wrapper'
import { all, compose, isNil, path, pathOr, pick, pluck, values } from 'ramda'
import React, { Component } from 'react'
import Helmet from 'react-helmet'
import { connect } from 'react-redux'

// components
import CheckoutContentContainer from '../shared/CheckoutContentContainer'
import PaymentDetailsContainer from '../shared/PaymentDetailsContainer'
import DeliveryOptionsContainer from '../shared/DeliveryOptionsContainer'
import DeliveryMethodsContainer from '../shared/DeliveryMethodsContainer'
import DeliveryInstructionsContainer from '../shared/DeliveryInstructions/DeliveryInstructionsContainer'
import PromotionCode from '../../../../components/containers/PromotionCode/PromotionCode'
import GiftCardsContainer from '../shared/GiftCardsContainer'
import StoreDeliveryContainer from '../shared/StoreDeliveryContainer'
import TermsAndConditionsContainer from '../shared/TermsAndConditionsContainer'
import DeliveryAddressDetailsContainer from '../shared/DeliveryAddressDetailsContainer'
import SimpleTotals from '../../../common/SimpleTotals/SimpleTotals'
import OrderErrorMessageContainer from '../shared/OrderErrorMessageContainer'
import PaymentButtonContainer from '../shared/PaymentButtonContainer'
import TotalCost from '../shared/TotalCost'
import CheckoutTitle from '../shared/CheckoutTitle'
import Espot from '../../Espot/Espot'
import SavePaymentDetailsCheckboxContainer from '../shared/SavePaymentDetailsCheckboxContainer'
// eslint-disable-next-line import/no-named-as-default
import AddressBook from '../../AddressBook/AddressBook'
import DigitalDeliveryPass from '../shared/DigitalDeliveryPass/DigitalDeliveryPass'

// actions
import { submitOrder } from '../../../../actions/common/orderActions'

import { validateForms } from '../../../../actions/common/checkoutActions'
import { sendEvent } from '../../../../actions/common/googleAnalyticsActions'

// selectors
import { getOrderDetails } from '../../../../selectors/common/orderDetails'
import { isMobile } from '../../../../selectors/viewportSelectors'
import {
  getDeliveryPaymentPageFormNames,
  getDeliveryPaymentPageFormErrors,
  getErrors,
  hasSelectedStore,
  selectedDeliveryLocationTypeEquals,
} from '../../../../selectors/checkoutSelectors'
import {
  isDDPPromotionEnabled,
  isDDPOrder,
} from '../../../../selectors/ddpSelectors'
import {
  isFeatureAddressBookEnabled,
  isFeatureDDPEnabled,
} from '../../../../selectors/featureSelectors'

// decorators
import { whenCheckedOut } from '../../../../decorators/checkoutDecorators'
import analyticsDecorator from '../../../../../client/lib/analytics/analytics-decorator'

import { selectedDeliveryLocation } from '../../../../lib/checkout-utilities/reshaper'
import { fixTotal, giftCardCoversTotal } from '../../../../lib/checkout'
import espotsDesktopConstants from '../../../../constants/espotsDesktop'
import { GTM_CATEGORY } from '../../../../analytics/analytics-constants'

const mapStateToProps = (state = {}) => {
  const orderDetails = getOrderDetails({
    config: {},
    ...state,
  })

  return {
    orderDetails: compose(
      all(isNil),
      values
    )(orderDetails)
      ? undefined
      : orderDetails,
    isMobile: isMobile(state),
    orderSummary: pathOr({}, ['checkout', 'orderSummary'], state),
    formNames: getDeliveryPaymentPageFormNames(state),
    formErrors: getDeliveryPaymentPageFormErrors(state),
    isHomeDeliverySelected: selectedDeliveryLocationTypeEquals(state, 'HOME'),
    hasSelectedStore: hasSelectedStore(state),
    deliveryInstructionsErrors:
      getErrors(['deliveryInstructions'], state).deliveryInstructions || {},
    isFeatureAddressBookEnabled: isFeatureAddressBookEnabled(state),
    isFeatureDDPEnabled: isFeatureDDPEnabled(state),
    isDDPOrder: isDDPOrder(state),
    isDDPPromotionEnabled: isDDPPromotionEnabled(state),
  }
}

const mapDispatchToProps = {
  submitOrder,
  validateForms,
  sendEvent,
}

class DeliveryPaymentContainer extends Component {
  static propTypes = {
    isMobile: PropTypes.bool,
    orderSummary: PropTypes.object.isRequired,
    deliveryInstructionsErrors: PropTypes.object,
    formNames: PropTypes.arrayOf(PropTypes.string),
    formErrors: PropTypes.object,
    isHomeDeliverySelected: PropTypes.bool,
    hasSelectedStore: PropTypes.bool,
    // functions
    sendEvent: PropTypes.func.isRequired,
    isFeatureAddressBookEnabled: PropTypes.bool.isRequired,
    isFeatureDDPEnabled: PropTypes.bool.isRequired,
    isDDPOrder: PropTypes.bool,
    isDDPPromotionEnabled: PropTypes.bool,
  }

  static defaultProps = {
    isMobile: false,
    orderSummary: {},
    hasPaymentMethods: false,
    orderDetails: undefined,
    formNames: [],
    formErrors: {},
    isHomeDeliverySelected: false,
    hasSelectedStore: false,
    isFeatureAddressBookEnabled: false,
    isFeatureDDPEnabled: false,
    isDDPOrder: false,
  }

  static contextTypes = {
    l: PropTypes.func,
    p: PropTypes.func,
  }

  componentDidMount() {
    this.props.sendEvent('Checkout', 'DeliveryPayment', 'Delivery and Payment')
  }

  render() {
    const { l, p } = this.context
    const {
      isMobile,
      isHomeDeliverySelected,
      hasSelectedStore,
      orderSummary,
      formNames,
      formErrors,
      deliveryInstructionsErrors,
      isFeatureAddressBookEnabled,
      isFeatureDDPEnabled,
      isDDPOrder,
      isDDPPromotionEnabled,
    } = this.props
    const shippingInfo = {
      ...selectedDeliveryLocation(orderSummary.deliveryLocations),
      estimatedDelivery: orderSummary.estimatedDelivery,
    }
    const priceInfo = pick(['subTotal'], orderSummary.basket)
    const discounts = path(['basket', 'discounts'], orderSummary)
    const calculateTotal = fixTotal(
      priceInfo.subTotal,
      shippingInfo.cost,
      pluck('value', discounts)
    )
    const giftCards = pathOr(null, ['giftCards'], orderSummary)
    const basketTotal = pathOr(null, ['basket', 'total'], orderSummary)
    const deliveryAddress = isFeatureAddressBookEnabled ? (
      <AddressBook />
    ) : (
      <DeliveryAddressDetailsContainer />
    )
    return (
      <div className="DeliveryPaymentContainer">
        <CheckoutContentContainer adobeAnalyticsKey="delivery-payment-details">
          <Helmet title={l`Delivery and Payment`} />
          {isMobile && <TotalCost totalCost={p(calculateTotal)} />}
          {isFeatureDDPEnabled &&
            isDDPPromotionEnabled && <DigitalDeliveryPass />}
          <DeliveryOptionsContainer />
          <Espot
            identifier={espotsDesktopConstants.orderSummary.discountIntro}
          />
          {isDDPOrder && (
            <p className="DeliveryPaymentContainer-ddpAppliedToOrderMsg">
              {l`Your delivery subscription has been applied to your order...`}
            </p>
          )}
          <StoreDeliveryContainer />
          {(isHomeDeliverySelected || hasSelectedStore) && (
            <div>
              {isHomeDeliverySelected && deliveryAddress}
              <DeliveryMethodsContainer />
              <QubitReact
                id="qubit-HomeDelivery-DeliveryInstructions"
                errors={deliveryInstructionsErrors}
              >
                <DeliveryInstructionsContainer />
              </QubitReact>
              <Espot
                identifier={espotsDesktopConstants.orderSummary.toBeDefined}
              />
              <div className="DeliveryPaymentContainer-wrapper">
                <CheckoutTitle>{l('Promotions & Gift Cards')}</CheckoutTitle>
                <PromotionCode
                  isOpenIfPopulated
                  isContentPadded={false}
                  gtmCategory={GTM_CATEGORY.CHECKOUT}
                />
                <GiftCardsContainer showTotal />
              </div>
              {!giftCardCoversTotal(giftCards, basketTotal) && (
                <PaymentDetailsContainer />
              )}
              <TermsAndConditionsContainer />
              <SavePaymentDetailsCheckboxContainer />
              {isMobile && (
                <div className="DeliveryPaymentContainer-wrapper">
                  <SimpleTotals
                    className={'DeliveryPaymentContainer-simpleTotals'}
                    shippingInfo={shippingInfo}
                    priceInfo={priceInfo}
                    discounts={discounts}
                  />
                </div>
              )}
              <PaymentButtonContainer
                className="PaymentContainer-paynow"
                formNames={formNames}
                formErrors={formErrors}
              />
              <OrderErrorMessageContainer />
            </div>
          )}
        </CheckoutContentContainer>
      </div>
    )
  }
}

export default analyticsDecorator('order-submit-page', { isAsync: true })(
  whenCheckedOut(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(DeliveryPaymentContainer)
  )
)

export { DeliveryPaymentContainer, mapStateToProps, mapDispatchToProps }
