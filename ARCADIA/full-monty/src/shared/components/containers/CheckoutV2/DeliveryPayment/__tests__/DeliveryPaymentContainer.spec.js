import { getOrderDetails } from '../../../../../selectors/common/orderDetails'
import renderComponentHelper, {
  analyticsDecoratorHelper,
} from '../../../../../../../test/unit/helpers/test-component'
import { sendEvent } from '../../../../../actions/common/googleAnalyticsActions'
import DeliveryAddressDetailsContainer from '../../shared/DeliveryAddressDetailsContainer'
import PaymentDetailsContainer from '../../shared/PaymentDetailsContainer'
import DigitalDeliveryPass from '../../shared/DigitalDeliveryPass/DigitalDeliveryPass'
import AddressBookComponent from '../../../AddressBook/AddressBook'

import DeliveryPaymentContainerWrapper, {
  DeliveryPaymentContainer,
  mapStateToProps,
} from '../DeliveryPaymentContainer'

jest.mock('../../../../../selectors/common/orderDetails')
jest.mock('../../../../../actions/common/googleAnalyticsActions')

const renderComponent = renderComponentHelper(DeliveryPaymentContainer)

describe('<DeliveryPaymentContainer />', () => {
  const initialProps = {
    orderSummary: {
      deliveryLocations: [
        {
          deliveryLocationType: 'HOME',
          label: 'Home Delivery Standard (UK up to 4 days; worldwide varies)',
          selected: true,
          deliveryMethods: [
            {
              shipModeId: 26504,
              shipCode: 'S',
              deliveryType: 'HOME_STANDARD',
              label: 'UK Standard up to 4 days',
              additionalDescription: 'Up to 4 days',
              selected: true,
              deliveryOptions: [],
            },
          ],
        },
      ],
      basket: {
        subTotal: '918.00',
        total: '918.00',
        totalBeforeDiscount: '918.00',
        discounts: [],
      },
      giftCards: [],
    },
    isHomeDeliverySelected: true,
    isFeatureAddressBookEnabled: false,
    getPaymentMethods: () => {},
    submitOrder: () => Promise.resolve(),
    validateForms: (_, { onValid }) => onValid(),
    sendEventAnalytics: () => {},
    sendEvent,
  }

  afterAll(() => {
    jest.unmock('../../../../../selectors/common/orderDetails')
  })

  describe('decorators', () => {
    analyticsDecoratorHelper(
      DeliveryPaymentContainerWrapper,
      'order-submit-page',
      {
        componentName: 'DeliveryPaymentContainer',
        isAsync: true,
      }
    )

    it('should be decorated with @whenCheckedOut', () => {
      expect(DeliveryPaymentContainerWrapper.WrappedComponent.displayName).toBe(
        'Connect(WhenCheckedOut)'
      )
    })

    it('should be decorated with @connect', () => {
      expect(
        DeliveryPaymentContainerWrapper.WrappedComponent.WrappedComponent
          .displayName
      ).toBe('Connect(DeliveryPaymentContainer)')
    })
  })

  describe('@renders', () => {
    it('should render the widgets specific to the delivery and payments page', () => {
      const { getTree } = renderComponent(initialProps)
      expect(getTree()).toMatchSnapshot()
    })

    it('should render a total at the top and SimpleTotals in mobile', () => {
      const { getTree } = renderComponent({
        ...initialProps,
        isMobile: true,
      })
      expect(getTree()).toMatchSnapshot()
    })

    it('should only render delivery options if not home delivery and no store selected', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        isHomeDeliverySelected: false,
        hasSelectedStore: false,
      })
      expect(wrapper.find(DeliveryAddressDetailsContainer).length).toBe(0)
    })

    it('with errors next button is not active', () => {
      const { getTree } = renderComponent({
        ...initialProps,
        formErrors: {
          yourAddress: {
            address1: 'This field is required',
          },
        },
      })
      expect(getTree()).toMatchSnapshot()
    })

    it('AddressBook when isFeaturedAddressBookEnabled', () => {
      const { getTree } = renderComponent({
        ...initialProps,
        isFeatureAddressBookEnabled: true,
      })
      expect(getTree()).toMatchSnapshot()
    })

    it('DeliveryAddressDetailsContainer when not isFeaturedAddressBookEnabled', () => {
      const { getTree } = renderComponent(initialProps)
      expect(getTree()).toMatchSnapshot()
    })

    it('should not render PaymentDetailsContainer if there is a giftCard that covers the basket total', () => {
      const props = {
        ...initialProps,
        orderSummary: {
          ...initialProps.orderSummary,
          basket: {
            subTotal: '918.00',
            total: '0.00',
            totalBeforeDiscount: '918.00',
            discounts: [{ value: '918.00' }],
          },
          giftCards: [
            {
              giftCardId: '6646689',
              giftCardNumber: 'XXXX XXXX XXXX 0830',
              balance: '918.00',
              amountUsed: '918.00',
              remainingBalance: '1257.15',
            },
          ],
        },
      }

      const { getTree, wrapper } = renderComponent(props)
      expect(getTree()).toMatchSnapshot()
      expect(wrapper.find(PaymentDetailsContainer).length).toBe(0)
    })

    it('should display DDP accordion if DDP purchase is available', () => {
      const props = {
        ...initialProps,
        isFeatureDDPEnabled: true,
        isDDPPromotionEnabled: true,
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find(DigitalDeliveryPass).length).toBe(1)
    })

    it('should not display DDP accordion if DDP purchase is not available', () => {
      const props = {
        ...initialProps,
        isFeatureDDPEnabled: true,
        isDDPPromotionEnabled: false,
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find(DigitalDeliveryPass).exists()).toEqual(false)
    })

    it('should not display AddressBook or DeliveryAddressDetailsContainer when store collection has been selected', () => {
      const props = {
        ...initialProps,
        isHomeDeliverySelected: false,
        hasSelectedStore: true,
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find(AddressBookComponent).length).toBe(0)
      expect(wrapper.find(DeliveryAddressDetailsContainer).length).toBe(0)
    })

    it('should display `DDP applied to order` message if DDP product in bag', () => {
      const props = {
        ...initialProps,
        isDDPOrder: true,
      }
      const { wrapper } = renderComponent(props)
      expect(
        wrapper.find('.DeliveryPaymentContainer-ddpAppliedToOrderMsg')
      ).toHaveLength(1)
    })

    it('should not display `DDP applied to order` message if no DDP product in bag', () => {
      const props = {
        ...initialProps,
        isDDPOrder: false,
      }
      const { wrapper } = renderComponent(props)
      expect(
        wrapper.find('.DeliveryPaymentContainer-ddpAppliedToOrderMsg').exists()
      ).toBe(false)
    })
  })

  describe('mapStateToProps', () => {
    describe('orderDetails', () => {
      it('should return correct order details', () => {
        const orderDetailsMock = {
          delivery: '',
          billing: '',
          amount: '',
        }
        getOrderDetails.mockImplementation(() => orderDetailsMock)
        expect(mapStateToProps().orderDetails).toBe(orderDetailsMock)
      })

      it('should return `undefined` if all properties are ‘nil’', () => {
        const orderDetailsMock = {
          delivery: undefined,
          billing: undefined,
          amount: undefined,
        }
        getOrderDetails.mockImplementation(() => orderDetailsMock)
        expect(mapStateToProps().orderDetails).toBeUndefined()
      })
    })
  })

  describe('deliveryInstructionsErrors', () => {
    it('should return the errors of the delivery instructions form in the state', () => {
      const state = {
        forms: {
          checkout: {
            deliveryInstructions: {
              errors: {
                foo: 'foo...',
              },
            },
          },
        },
      }
      expect(mapStateToProps(state).deliveryInstructionsErrors).toEqual(
        state.forms.checkout.deliveryInstructions.errors
      )
    })

    it('should return an empty object if the delivery instructions errors are not reachable', () => {
      expect(mapStateToProps().deliveryInstructionsErrors).toEqual({})
    })
  })
})
