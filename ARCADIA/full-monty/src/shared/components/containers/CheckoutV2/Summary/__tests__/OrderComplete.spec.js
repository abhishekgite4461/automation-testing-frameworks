import testComponentHelper, {
  analyticsDecoratorHelper,
} from 'test/unit/helpers/test-component'
import OrderComplete from '../OrderComplete'
import Espot from '../../../Espot/Espot'
import { deliveryTypes } from '../../../../../selectors/checkoutSelectors'
import {
  sendEvent,
  sendOrderCompleteEvent,
} from '../../../../../actions/common/googleAnalyticsActions'
import { getItem } from '../../../../../../client/lib/cookie'
import React from 'react'

const DELIVERY_TYPE = {
  STANDARD: 'standard',
  EXPRESS: 'express',
}

jest.mock('react-router', () => {
  return {
    browserHistory: {
      push: jest.fn(),
    },
  }
})

jest.mock('../../../../../actions/common/googleAnalyticsActions')

jest.mock('../../../../../../client/lib/cookie', () => ({
  getItem: jest.fn(),
}))

global.window.scrollTo = jest.fn()

afterEach(() => {
  jest.resetAllMocks()
})

describe('<OrderComplete/>', () => {
  const renderComponent = testComponentHelper(
    OrderComplete.WrappedComponent.WrappedComponent
  )
  const initialProps = {
    brandName: 'topshop',
    stores: [],
    deliveryType: 'HOME',
    orderCompleted: {
      orderId: 1164497,
      subTotal: '',
      returnPossible: false,
      returnRequested: false,
      deliveryMethod: 'UK Standard up to 4 working days',
      deliveryDate: 'Wednesday 15 February 2017',
      deliveryCost: '4.00',
      deliveryCarrier: 'Parcelnet',
      deliveryPrice: '4.00',
      totalOrderPrice: '30.00',
      totalOrdersDiscountLabel: '',
      totalOrdersDiscount: '',
      billingAddress: {
        name: 'Mr john doe',
        address1: 'Unit 1fl, 2 Michael Road',
        address2: 'LONDON',
        address3: 'SW6 2AD',
        country: 'United Kingdom',
      },
      deliveryAddress: {
        name: 'Mr john doe',
        address1: 'Unit 1fl, 2 Michael Road',
        address2: 'LONDON',
        address3: 'SW6 2AD',
        country: 'United Kingdom',
      },
      orderLines: [
        {
          lineNo: '42C39LBLK',
          name: 'CRUMBLE Canvas Flatform Trainers',
          size: 36,
          colour: 'BLACK',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS42C39LBLK_Small_F_1.jpg',
          quantity: 1,
          unitPrice: '26.00',
          discount: '',
          total: '26.00',
          nonRefundable: false,
        },
      ],
      paymentDetails: [
        {
          paymentMethod: 'Visa',
          cardNumberStar: '************1111',
          totalCost: '£30.00',
        },
      ],
      currencyConversion: {
        currencyRate: 'GBP',
      },
    },
    currencyCode: 'GBP',
    hasUser: true,
    minLaptop: false,
    getAccount: jest.fn(),
    findStore: jest.fn(),
    analyticsOrder: jest.fn(),
    orderSubtotal: '26.00',
    deliveryTypes,
    deliveryDate: 'Wednesday 15 February 2017',
    isCollectFromOrder: false,
    sendEvent,
    sendOrderCompleteEvent,
    location: {
      pathname: '/order-complete-v2',
    },
    visited: [],
    checkout: {
      checkoutVersion: 'AAAAA',
    },
    hostname: 'local.m.topshop.com',
  }

  const homeProps = (deliveryType, minLaptop = false) => ({
    ...initialProps,
    deliveryType: 'HOME',
    orderCompleted: {
      ...initialProps.orderCompleted,
      deliveryMethod:
        deliveryType === DELIVERY_TYPE.STANDARD
          ? 'UK Standard up to 4 working days'
          : 'Wednesday Nominated Day Delivery:',
      deliveryCarrier:
        deliveryType === DELIVERY_TYPE.STANDARD
          ? 'Parcelnet'
          : 'Home Delivery Network',
      deliveryDate: 'Tuesday 14 February 2017',
    },
    deliveryDate: 'Tuesday 14 February 2017',
    minLaptop,
  })

  const storeProps = (deliveryType, minLaptop = false) => ({
    ...initialProps,
    deliveryType: 'STORE',
    orderCompleted: {
      ...initialProps.orderCompleted,
      deliveryMethod:
        deliveryType === DELIVERY_TYPE.STANDARD
          ? 'Collect From Store Standard'
          : 'Collect From Store Express',
      deliveryCarrier:
        deliveryType === DELIVERY_TYPE.STANDARD
          ? 'Retail Store Standard'
          : 'Retail Store Express',
    },
    stores: [
      {
        storeId: 'TS0001',
        brandId: 12556,
        name: 'Oxford Circus',
        distance: 0.16,
        latitude: 51.5157,
        longitude: -0.141396,
        address: {
          line1: '214 Oxford Street',
          line2: 'Oxford Circus',
          city: 'West End',
          postcode: 'W1W 8LG',
        },
        openingHours: {
          monday: '09:30-21:00',
          tuesday: '09:30-21:00',
          wednesday: '09:30-21:00',
          thursday: '09:30-21:00',
          friday: '09:30-21:00',
          saturday: '09:00-21:00',
          sunday: '11:30-18:00',
        },
        telephoneNumber: '03448 487487',
        collectFromStore: {
          standard: {
            dates: [
              {
                availableUntil: '2017-02-08 11:30:00',
                collectFrom: '2017-02-11',
              },
              {
                availableUntil: '2017-02-09 11:30:00',
                collectFrom: '2017-02-12',
              },
              {
                availableUntil: '2017-02-10 11:30:00',
                collectFrom: '2017-02-13',
              },
            ],
            price: 0,
          },
          express: {
            dates: [
              {
                availableUntil: '2017-02-08 20:30:00',
                collectFrom: '2017-02-09',
              },
              {
                availableUntil: '2017-02-09 20:30:00',
                collectFrom: '2017-02-10',
              },
              {
                availableUntil: '2017-02-10 20:30:00,',
                collectFrom: '2017-02-11',
              },
            ],
            price: 3,
          },
        },
      },
    ],
    minLaptop,
    isCollectFromOrder: true,
  })

  const parcelProps = (minLaptop = false) => ({
    ...initialProps,
    deliveryType: 'STORE',
    orderCompleted: {
      ...initialProps.orderCompleted,
      deliveryMethod: 'Collect from ParcelShop',
      deliveryCarrier: 'Retail Store Collection',
    },
    stores: [
      {
        storeId: 'TS0001',
        brandId: 12556,
        name: 'Oxford Circus',
        distance: 0.16,
        latitude: 51.5157,
        longitude: -0.141396,
        address: {
          line1: '214 Oxford Street',
          line2: 'Oxford Circus',
          city: 'West End',
          postcode: 'W1W 8LG',
        },
        openingHours: {
          monday: '09:30-21:00',
          tuesday: '09:30-21:00',
          wednesday: '09:30-21:00',
          thursday: '09:30-21:00',
          friday: '09:30-21:00',
          saturday: '09:00-21:00',
          sunday: '11:30-18:00',
        },
        telephoneNumber: '03448 487487',
        collectFromStore: {
          standard: {
            dates: [
              {
                availableUntil: '2017-02-08 11:30:00',
                collectFrom: '2017-02-11',
              },
              {
                availableUntil: '2017-02-09 11:30:00',
                collectFrom: '2017-02-12',
              },
              {
                availableUntil: '2017-02-10 11:30:00',
                collectFrom: '2017-02-13',
              },
            ],
            price: 0,
          },
          express: {
            dates: [
              {
                availableUntil: '2017-02-08 20:30:00',
                collectFrom: '2017-02-09',
              },
              {
                availableUntil: '2017-02-09 20:30:00',
                collectFrom: '2017-02-10',
              },
              {
                availableUntil: '2017-02-10 20:30:00,',
                collectFrom: '2017-02-11',
              },
            ],
            price: 3,
          },
        },
      },
    ],
    minLaptop,
    isCollectFromOrder: true,
  })

  const noDeliveryDateProps = (minLaptop = false) => ({
    ...initialProps,
    deliveryType: 'HOME',
    orderCompleted: {
      ...initialProps.orderCompleted,
      deliveryDate: '',
    },
    deliveryDate: '',
    minLaptop,
  })

  describe('@decorators', () => {
    analyticsDecoratorHelper(OrderComplete, 'order-completed', {
      isAsync: false,
      sendAdobe: true,
      componentName: 'OrderComplete',
      redux: true,
    })
  })

  describe('@renders', () => {
    describe('GA Tracking pixel', () => {
      beforeEach(() => {
        getItem.mockReturnValue('GA1.3.3.7')
      })

      it('renders if the site is evans', () => {
        const props = {
          ...initialProps,
          brandName: 'evans',
          hostname: 'm.evans.co.uk',
        }
        const renderedComponent = renderComponent(props)
        const gaLink = `https://www.google-analytics.com/collect?v=1&t=event&tid=UA-99206402-3&cid=3.7&ec=ecommerce&ea=montypixelpurchase&el=${
          props.orderCompleted.orderId
        }&dh=${props.hostname}&dp=${props.location.pathname}`

        expect(
          renderedComponent.wrapper.containsMatchingElement(
            <img alt="GA Pixel" src={gaLink} />
          )
        ).toBe(true)
      })

      it('should not render if the site is not evans', () => {
        const renderedComponent = renderComponent({
          ...initialProps,
          brandName: 'topshop',
        })
        expect(
          renderedComponent.wrapper.containsMatchingElement(
            <img alt="GA Pixel" />
          )
        ).toBe(false)
      })

      it('should not render if the ga cookie is not set', () => {
        getItem.mockReturnValue(undefined)
        const renderedComponent = renderComponent({
          ...initialProps,
          brandName: 'evans',
        })
        expect(
          renderedComponent.wrapper.containsMatchingElement(
            <img alt="GA Pixel" />
          )
        ).toBe(false)
      })

      it('should not render if the order is in error', () => {
        const renderedComponent = renderComponent({
          ...initialProps,
          brandName: 'evans',
          orderError: 'some error',
        })
        expect(
          renderedComponent.wrapper.containsMatchingElement(
            <img alt="GA Pixel" />
          )
        ).toBe(false)
      })
    })

    describe('mobile', () => {
      it('no delivery or estimated delivery date', () => {
        expect(
          renderComponent(noDeliveryDateProps()).getTree()
        ).toMatchSnapshot()
      })

      describe('home delivery', () => {
        it('standard', () => {
          const { wrapper, getTree } = renderComponent(
            homeProps(DELIVERY_TYPE.STANDARD)
          )
          expect(getTree()).toMatchSnapshot()
          expect(wrapper.find('.OrderComplete-storeContainer').length).toBe(0)
        })
        it('express', () => {
          const { wrapper, getTree } = renderComponent(
            homeProps(DELIVERY_TYPE.EXPRESS)
          )
          expect(getTree()).toMatchSnapshot()
          expect(wrapper.find('.OrderComplete-storeContainer').length).toBe(0)
        })
      })
      describe('store delivery', () => {
        it('standard', () => {
          const { wrapper, getTree } = renderComponent(
            storeProps(DELIVERY_TYPE.STANDARD)
          )
          expect(getTree()).toMatchSnapshot()
          expect(wrapper.find('.OrderComplete-storeContainer').length).toBe(1)
          expect(wrapper.find('.OrderComplete-storeButton').length).toBe(1)
          expect(wrapper.find('.OrderComplete-mapContainer').length).toBe(0)
        })
        it('express', () => {
          const { wrapper, getTree } = renderComponent(
            storeProps(DELIVERY_TYPE.EXPRESS)
          )
          expect(getTree()).toMatchSnapshot()
          expect(wrapper.find('.OrderComplete-storeContainer').length).toBe(1)
          expect(wrapper.find('.OrderComplete-storeButton').length).toBe(1)
          expect(wrapper.find('.OrderComplete-mapContainer').length).toBe(0)
        })

        it('should add the ‘collect within’ text', () => {
          const component = renderComponent(storeProps(DELIVERY_TYPE.STANDARD))
            .wrapper
          expect(component.find('.OrderComplete-collectWithin').length).toBe(1)
        })
      })
      describe('parcel delivery', () => {
        it('standard', () => {
          const { wrapper, getTree } = renderComponent(parcelProps())
          expect(getTree()).toMatchSnapshot()
          expect(wrapper.find('.OrderComplete-storeContainer').length).toBe(1)
          expect(wrapper.find('.OrderComplete-storeButton').length).toBe(1)
          expect(wrapper.find('.OrderComplete-mapContainer').length).toBe(0)
        })
      })

      it('generic error is rendered when any error occurs', () => {
        expect(
          renderComponent({
            ...initialProps,
            orderError: 'an error occurred',
          }).getTree()
        ).toMatchSnapshot()
      })
    })
    describe('desktop', () => {
      it('no delivery or estimated delivery date', () => {
        expect(
          renderComponent(noDeliveryDateProps(true)).getTree()
        ).toMatchSnapshot()
      })
      describe('home delivery', () => {
        it('standard', () => {
          const { wrapper, getTree } = renderComponent(
            homeProps(DELIVERY_TYPE.STANDARD, true)
          )
          expect(getTree()).toMatchSnapshot()
          expect(wrapper.find('.OrderComplete-storeContainer').length).toBe(0)
        })
        it('express', () => {
          const { wrapper, getTree } = renderComponent(
            homeProps(DELIVERY_TYPE.EXPRESS, true)
          )
          expect(getTree()).toMatchSnapshot()
          expect(wrapper.find('.OrderComplete-storeContainer').length).toBe(0)
        })
      })
      describe('store delivery', () => {
        it('standard', () => {
          const { wrapper, getTree } = renderComponent(
            storeProps(DELIVERY_TYPE.STANDARD, true)
          )
          expect(getTree()).toMatchSnapshot()
          expect(wrapper.find('.OrderComplete-storeContainer').length).toBe(1)
          expect(wrapper.find('.OrderComplete-storeButton').length).toBe(0)
          expect(wrapper.find('.OrderComplete-mapContainer').length).toBe(1)
        })
        it('express', () => {
          const { wrapper, getTree } = renderComponent(
            storeProps(DELIVERY_TYPE.EXPRESS, true)
          )
          expect(getTree()).toMatchSnapshot()
          expect(wrapper.find('.OrderComplete-storeContainer').length).toBe(1)
          expect(wrapper.find('.OrderComplete-storeButton').length).toBe(0)
          expect(wrapper.find('.OrderComplete-mapContainer').length).toBe(1)
        })
      })
      describe('parcel delivery', () => {
        it('standard', () => {
          const { wrapper, getTree } = renderComponent(parcelProps(true))
          expect(getTree()).toMatchSnapshot()
          expect(wrapper.find('.OrderComplete-storeContainer').length).toBe(1)
          expect(wrapper.find('.OrderComplete-storeButton').length).toBe(0)
          expect(wrapper.find('.OrderComplete-mapContainer').length).toBe(1)
        })
      })
      it('full monty espots', () => {
        const { wrapper, getTreeFor } = renderComponent({
          ...noDeliveryDateProps(true),
          useFullMontyEspots: true,
        })
        expect(wrapper.find(Espot).map(getTreeFor)).toMatchSnapshot()
      })
      it('generic error is rendered when any error occurs', () => {
        expect(
          renderComponent({
            ...initialProps,
            orderError: 'an error occurred',
            minLaptop: true,
          }).getTree()
        ).toMatchSnapshot()
      })
    })
    describe('with Collect From Option selected', () => {
      it('should not render Collect From Option if stores have not been populated', () => {
        const { wrapper, getTree } = renderComponent({
          ...storeProps(DELIVERY_TYPE.STANDARD),
          stores: [],
        })
        expect(getTree()).toMatchSnapshot()
        expect(wrapper.find('.OrderComplete-storeContainer').length).toBe(0)
      })
    })
  })

  describe('@lifecycle', () => {
    describe('on componentDidMount', () => {
      it('do not calls getDeliveryStoreDetails when in Home delivery state', () => {
        const props = {
          ...initialProps,
        }
        const { instance } = renderComponent(props)
        instance.getLoadTime = { endTime: jest.fn() }
        instance.componentDidMount()
        expect(props.findStore).not.toHaveBeenCalled()
      })

      it('calls getDeliveryStoreDetails when in Store delivery state', () => {
        const props = {
          ...storeProps(DELIVERY_TYPE.STANDARD),
        }
        const { instance } = renderComponent(props)
        instance.getLoadTime = { endTime: jest.fn() }
        expect(props.findStore).not.toHaveBeenCalled()
        instance.componentDidMount()
        expect(props.findStore).toHaveBeenCalledTimes(1)
      })

      it('calls getDeliveryStoreDetails when in Parcel delivery state', () => {
        const props = {
          ...parcelProps(),
        }
        const { instance } = renderComponent(props)
        instance.getLoadTime = { endTime: jest.fn() }
        expect(props.findStore).not.toHaveBeenCalled()
        instance.componentDidMount()
        expect(props.findStore).toHaveBeenCalledTimes(1)
      })

      it('calls order when mounted', () => {
        const props = {
          ...parcelProps(DELIVERY_TYPE.STANDARD),
        }
        const { instance } = renderComponent(props)
        expect(props.analyticsOrder).not.toHaveBeenCalled()
        instance.componentDidMount()
        expect(props.analyticsOrder).toHaveBeenCalledTimes(1)
      })

      it('calls sendEvent and sendOrderCompleteEvent when mounted', () => {
        const props = {
          ...parcelProps(DELIVERY_TYPE.STANDARD),
        }
        const { instance } = renderComponent(props)
        instance.componentDidMount()
        expect(props.sendEvent).toHaveBeenCalledTimes(1)
        expect(props.sendOrderCompleteEvent).toHaveBeenCalledTimes(1)
      })
    })
  })
})
