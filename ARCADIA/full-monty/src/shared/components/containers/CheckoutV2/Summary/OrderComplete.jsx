import { contains, defaultTo, isEmpty, path } from 'ramda'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { browserHistory } from 'react-router'

import { PrivacyGuard } from '../../../../lib'
import { getMapUrl } from '../../../../lib/map'
import espots from '../../../../constants/espotsMobile'
import { order as analyticsOrder } from '../../../../lib/analytics'
import {
  extractDiscountInfo,
  getSubTotal,
  isCollectFromOrder,
  getDeliveryDate,
} from '../../../../selectors/checkoutSelectors'

// actions
import { getAccount } from '../../../../actions/common/accountActions'
import { updateOrder } from '../../../../actions/common/orderActions'
import { findStore } from '../../../../actions/components/StoreLocatorActions'
import {
  sendEvent,
  sendOrderCompleteEvent,
} from '../../../../actions/common/googleAnalyticsActions'

// containers
import Espot from '../../Espot/Espot'

// components
import SimpleTotals from '../../../common/SimpleTotals/SimpleTotals'
import CmsWrapper from '../../../containers/CmsWrapper/CmsWrapper'
import Button from '../../../common/Button/Button'
import CheckoutBagSide from '../../../common/CheckoutBagSide/CheckoutBagSide'
import Image from '../../../common/Image/Image'
import OrderProducts from '../../../common/OrderProducts/OrderProducts'
import Peerius from '../../../common/Peerius/Peerius'
import GoogleMap from '../../../common/StoreLocator/GoogleMap'
import PaymentSummary from '../../../common/PaymentSummary/PaymentSummary'
import { normalizePrice } from '../../../../lib/price'
import {
  deliveryTypes,
  normaliseDeliveryType,
} from '../../../../lib/checkout-utilities/order-summary'

// decorators
import { connect } from 'react-redux'
import analyticsDecorator from '../../../../../client/lib/analytics/analytics-decorator'
import cmsConsts from '../../../../constants/cmsConsts'
import { getItem } from '../../../../../client/lib/cookie'
import { selectHostname } from '../../../../selectors/routingSelectors'

const addressEspotParams = {
  cmsPageName: espots.orderConfirmation[0],
  contentType: cmsConsts.ESPOT_CONTENT_TYPE,
}

const renderOpeningHoursRow = (day, openingHours) => {
  return openingHours ? (
    <div className="OrderComplete-openingHoursRow">
      <span>{`${day}: ${openingHours}`}</span>
    </div>
  ) : null
}

@analyticsDecorator('order-completed')
@connect(
  (state) => ({
    brandName: state.config.brandName,
    currencyCode: state.siteOptions.currencyCode,
    hasUser: isEmpty(state.account.user),
    isFirstVisit: state.routing.visited.length === 1,
    minLaptop: contains(state.viewport.media, ['laptop', 'desktop']),
    isMobile: state.viewport.media === 'mobile',
    orderCompleted: state.checkout.orderCompleted,
    orderError: state.checkout.orderError,
    stores: state.storeLocator.stores,
    visited: state.routing.visited,
    discountInfo: extractDiscountInfo(state),
    orderSubtotal: getSubTotal(state),
    deliveryTypes,
    isCollectFromOrder: isCollectFromOrder(state),
    deliveryDate: getDeliveryDate(state),
    logoVersion: state.config.logoVersion,
    hostname: selectHostname(state),
  }),
  { getAccount, findStore, analyticsOrder, sendEvent, sendOrderCompleteEvent }
)
export default class OrderComplete extends Component {
  static propTypes = {
    brandName: PropTypes.string.isRequired,
    currencyCode: PropTypes.string,
    hasUser: PropTypes.bool,
    isFirstVisit: PropTypes.bool,
    minLaptop: PropTypes.bool,
    isMobile: PropTypes.bool,
    orderCompleted: PropTypes.object.isRequired,
    orderError: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    stores: PropTypes.arrayOf(
      PropTypes.shape({
        storeId: PropTypes.string,
        latitude: PropTypes.number,
        longitude: PropTypes.number,
      })
    ),
    // actions
    getAccount: PropTypes.func.isRequired,
    findStore: PropTypes.func.isRequired,
    analyticsOrder: PropTypes.func.isRequired,
    discountInfo: PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.number,
        label: PropTypes.string,
      })
    ),
    orderSubtotal: PropTypes.string,
    isCollectFromOrder: PropTypes.bool,
    deliveryDate: PropTypes.string,
    sendEvent: PropTypes.func.isRequired,
    location: PropTypes.object, // Todo - on checkout refactor extract this as a proper proptype
    hostname: PropTypes.string,
  }

  static defaultProps = {
    currencyCode: 'GBP',
    hasUser: false,
    isFirstVisit: false,
    minLaptop: false,
    isMobile: true,
    orderError: false,
    orderCompleted: {},
    stores: [],
    discountInfo: [],
    orderSubtotal: '',
    deliveryTypes: {},
    isCollectFromOrder: false,
    deliveryDate: '',
  }

  static contextTypes = {
    l: PropTypes.func,
    p: PropTypes.func,
  }

  static needs = [getAccount, updateOrder]

  constructor(props) {
    super(props)
    this.buttonClickHandler = this.gotoNextPage.bind(this)
    const gaCookie = defaultTo('GA0.0.0.0')(getItem('_ga'))
    const gaCookieParts = gaCookie.split('.')
    this.gaClientID = `${gaCookieParts[2]}.${gaCookieParts[3]}`
  }

  componentDidMount() {
    const {
      hasUser,
      getAccount,
      isFirstVisit,
      orderCompleted,
      orderError,
      currencyCode,
      analyticsOrder,
    } = this.props
    this.getDeliveryStoreDetails()
    if (!hasUser && !isFirstVisit) getAccount()
    if (window) window.scrollTo(0, 0)
    analyticsOrder()
    // FIRE GLOBAL EVENT NEED FOR OPEN-TAGS-ANALITYCS
    if (process.browser) {
      document.dispatchEvent(
        new CustomEvent('purchase', {
          detail: {
            value: orderCompleted.totalOrderPrice,
            currency: currencyCode,
          },
        })
      )
    }

    this.props.sendEvent(
      'Checkout',
      'Order Complete',
      'Thank You For Your Order'
    )
    this.props.sendOrderCompleteEvent(orderCompleted, orderError)
  }

  getDeliveryStoreDetails() {
    const { findStore, orderCompleted, isCollectFromOrder } = this.props
    if (isCollectFromOrder) {
      findStore(path(['deliveryAddress'], orderCompleted))
    }
  }

  getPeerius = () => {
    const {
      orderCompleted: {
        orderId,
        orderLines,
        deliveryCost,
        deliveryPrice,
        totalOrderPrice,
      },
      orderSubtotal,
      currencyCode,
    } = this.props
    const shipping = deliveryCost || deliveryPrice

    return (
      <Peerius
        type="order"
        currency={currencyCode}
        orderLines={orderLines}
        subTotal={orderSubtotal}
        total={totalOrderPrice}
        shipping={shipping}
        orderNo={orderId}
      />
    )
  }

  gotoNextPage() {
    browserHistory.push(this.props.orderError ? '/checkout' : '/')
  }

  renderStoreDetails(store) {
    const { l } = this.context
    const { minLaptop } = this.props
    const { openingHours } = store
    const arrWeekday = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday']
    const weekDays = arrWeekday.every(
      (day) => openingHours[day] === openingHours.friday
    )
      ? renderOpeningHoursRow(l`Monday to Friday`, openingHours.monday)
      : arrWeekday.map((day) =>
          renderOpeningHoursRow(l(day), openingHours[day])
        )

    return openingHours.monday ||
      openingHours.saturday ||
      openingHours.sunday ? (
      <div className="OrderComplete--store">
        <div className="OrderComplete-openingHoursTitle">
          {minLaptop ? l`Store opening times` : l`Opening hours`}
        </div>
        <span>{weekDays}</span>
        <span>{renderOpeningHoursRow(l`Saturday`, openingHours.saturday)}</span>
        <span>{renderOpeningHoursRow(l`Sunday`, openingHours.sunday)}</span>
      </div>
    ) : null
  }

  renderIcon(deliveryCarrier) {
    const { logoVersion } = this.props
    const icon = path(
      ['icon'],
      deliveryTypes[normaliseDeliveryType(deliveryCarrier)]
    )
    return (
      !!icon && (
        <div className="OrderComplete-deliveryIconContainer">
          <Image
            className="OrderComplete-deliveryIcon"
            src={`/assets/${
              this.props.brandName
            }/images/${icon}?version=${logoVersion}`}
          />
        </div>
      )
    )
  }

  renderViewInMapButton(coordinates) {
    const { l } = this.context
    const { latitude, longitude } = coordinates
    const mapUrl = getMapUrl()

    return (
      <div className="OrderComplete-storeButton">
        <a
          href={`http://${mapUrl}/?q=${latitude},${longitude}`}
          className="Button Button--secondary Button--linkButton"
        >
          {l`View in maps`}
        </a>
      </div>
    )
  }

  renderDeliveryEstimation() {
    const { l } = this.context
    const { deliveryDate } = this.props

    return (
      deliveryDate && (
        <div className="OrderComplete-fields">
          <p className="OrderComplete-collectFrom">{l`Your order will be delivered no later than`}</p>
          <PrivacyGuard>
            <p className="OrderComplete-estimatedDelivery">
              {this.props.deliveryDate}
            </p>
          </PrivacyGuard>
          {this.props.isCollectFromOrder && (
            <p className="OrderComplete-collectWithin">
              {l`We'll email you when it's ready to collect.`} ({l`Please collect within 10 days`})
            </p>
          )}
        </div>
      )
    )
  }

  getDeliveryStore() {
    const { stores } = this.props
    const address4 = path(
      ['address4', 'deliveryAddress', 'orderCompleted'],
      this.props
    )
    return stores
      ? stores.find((store) => {
          return path(['postcode', 'address'], store) === address4
        })
      : null
  }

  renderCollectFromOption = () => {
    const { minLaptop } = this.props

    const deliveryStore = this.getDeliveryStore()
    if (!deliveryStore) return null

    return (
      <div className="OrderComplete-storeContainer">
        {minLaptop && (
          <div className="OrderComplete-mapContainer">
            <div className="OrderComplete-map">
              <GoogleMap borderless />
            </div>
          </div>
        )}
        {this.renderStoreDetails(deliveryStore)}
        {!minLaptop && this.renderViewInMapButton(deliveryStore)}
      </div>
    )
  }

  renderComplete() {
    const { l } = this.context
    const {
      isMobile,
      orderCompleted,
      discountInfo,
      orderSubtotal,
      isCollectFromOrder,
    } = this.props
    const { deliveryMethod, deliveryCost, deliveryPrice } = orderCompleted
    const shippingPrice = deliveryCost || deliveryPrice

    if (!orderCompleted || isEmpty(orderCompleted)) {
      return null
    }

    const shippingInfo = {
      cost: shippingPrice,
      label: deliveryMethod,
    }

    const payments = orderCompleted.paymentDetails.map((payment) => ({
      method: payment.paymentMethod,
      cardNumber: payment.cardNumberStar,
      price: normalizePrice(payment.totalCost),
    }))

    const priceInfo = {
      subTotal: orderSubtotal,
    }

    return (
      <div className="OrderComplete-success">
        {this.getPeerius()}
        <div className="OrderComplete-container">
          <div className="OrderComplete-left">
            <div className="OrderComplete-orderDetailsHeader">
              <h1 className="OrderComplete-orderDetailsHeader--item">{l`Thank You For Your Order`}</h1>
            </div>
            <div className="OrderComplete-orderDetailsContainer">
              <div className="OrderComplete-orderDetails">
                <div className="OrderComplete-details">
                  <div className="OrderComplete-fields">
                    <div>
                      <span className="OrderComplete-field">
                        {l`Order Number`}{' '}
                      </span>
                      <PrivacyGuard>
                        <span className="OrderComplete-orderNumber">
                          {orderCompleted.orderId}
                        </span>
                      </PrivacyGuard>
                    </div>
                    <div>
                      <span className="OrderComplete-confirmationMail">
                        {l`You'll get an order confirmation email shortly.`}
                      </span>
                    </div>
                  </div>
                  {this.renderDeliveryEstimation()}
                </div>
                <div className="OrderComplete-addressContainer">
                  <h3 className="OrderComplete-addressHeader">{l`Your order will be delivered to`}</h3>
                  <div className="OrderComplete-address">
                    {this.renderIcon(orderCompleted.deliveryCarrier)}
                    <div>
                      <div>
                        <PrivacyGuard>
                          <span>{orderCompleted.deliveryAddress.name}</span>
                        </PrivacyGuard>
                      </div>
                      <div>
                        <PrivacyGuard>
                          <span>{orderCompleted.deliveryAddress.address1}</span>
                        </PrivacyGuard>
                      </div>
                      <div>
                        <PrivacyGuard>
                          <span>{orderCompleted.deliveryAddress.address2}</span>
                        </PrivacyGuard>
                      </div>
                      <div>
                        <PrivacyGuard>
                          <span>{orderCompleted.deliveryAddress.address3}</span>
                        </PrivacyGuard>
                      </div>
                      <div>
                        <PrivacyGuard>
                          <span>{orderCompleted.deliveryAddress.country}</span>
                        </PrivacyGuard>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {isCollectFromOrder && this.renderCollectFromOption()}
            </div>
            <Espot identifier="thankyouESpot1" />
            <CmsWrapper mode="noPadding" params={addressEspotParams} />
            <div className="OrderComplete-paymentSummary">
              <PaymentSummary
                payments={payments}
                totalOrderPrice={orderCompleted.totalOrderPrice}
              />
            </div>
            <Espot identifier="thankyouESpot2" />
            <Espot identifier="thankyouESpot3" />
          </div>
          {isMobile ? (
            <div>
              <div className="OrderComplete-subheader">
                <h3 className="OrderComplete-subheaderTitle">{l`Your order`}</h3>
              </div>
              <OrderProducts
                canModify={false}
                products={orderCompleted.orderLines}
              />
              <SimpleTotals
                className="OrderComplete-totals"
                shippingInfo={shippingInfo}
                priceInfo={priceInfo}
                discounts={discountInfo}
              />
            </div>
          ) : (
            <div className="OrderComplete-myBag sessioncamhidetext">
              <CheckoutBagSide
                showDiscounts
                canModify={false}
                orderProducts={orderCompleted.orderLines}
              />
            </div>
          )}
        </div>
        <div className="OrderComplete-bottom">
          <div className="OrderComplete-recentlyViewed">
            <Espot identifier="thankyouESpot7" />
          </div>
          {this.renderButton()}
        </div>
      </div>
    )
  }

  renderError() {
    const { l } = this.context
    return (
      <div className="OrderComplete-error">
        <div className="OrderComplete-icon">
          <Image
            className="OrderComplete-errorImage"
            src={`/assets/${this.props.brandName}/images/error.svg`}
          />
        </div>
        <h1 className="OrderComplete-header">{l`Error`}</h1>
        <p className="OrderComplete-details">
          {l`Unfortunately we could not process your payment. An error may have occurred with your provider or you may have chosen to cancel the payment. No money has been taken from your card and the transaction has been cancelled. If you would like to continue your purchase please try again.`}
        </p>
        {this.renderButton()}
      </div>
    )
  }

  renderButton() {
    const { l } = this.context

    return (
      <Button
        className="OrderComplete-button"
        clickHandler={this.buttonClickHandler}
      >
        {this.props.orderError ? l`Go to Checkout` : l`Continue Shopping`}
      </Button>
    )
  }

  renderPixelTracker() {
    const {
      orderError,
      brandName,
      orderCompleted: { orderId },
      hostname,
      location: { pathname },
    } = this.props
    if (orderError || brandName !== 'evans') return
    if (this.gaClientID === '0.0') return

    const gaLink = `https://www.google-analytics.com/collect?v=1&t=event&tid=UA-99206402-3&cid=${
      this.gaClientID
    }&ec=ecommerce&ea=montypixelpurchase&el=${orderId}&dh=${hostname}&dp=${pathname}`

    return <img alt="GA Pixel" src={gaLink} />
  }

  render() {
    const { l } = this.context
    const { orderError } = this.props

    return (
      <section className="OrderComplete">
        <Helmet title={l`Your Order is Complete`} />
        {orderError ? this.renderError() : this.renderComplete()}
        {this.renderPixelTracker()}
      </section>
    )
  }
}
