import PropTypes from 'prop-types'
import { path } from 'ramda'
import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  isMobile,
  isMobileBreakpoint,
} from '../../../../selectors/viewportSelectors'
import * as types from './PaymentDetails/types'

// actions
import {
  resetFormPartial,
  setFormField,
  touchedFormField,
} from '../../../../actions/common/formActions'
import { setManualAddressMode } from '../../../../actions/common/checkoutActions'
import { sendAnalyticsPaymentMethodChangeEvent } from '../../../../analytics'

// components
import PaymentDetails from './PaymentDetails/PaymentDetails'
import {
  selectPaymentMethodForStoredPaymentDetails,
  selectDecoratedCombinedPaymentMethods,
  getCombinedPaymentMethodByPaymentMethodValue,
} from '../../../../selectors/paymentMethodSelectors'
import { selectStoredPaymentDetails } from '../../../../selectors/common/accountSelectors'
import { createSelector } from 'reselect'
import { selectInDeliveryAndPayment } from '../../../../selectors/routingSelectors'
import {
  selectCheckoutForms,
  selectBillingCardPaymentTypeFromCheckoutForm,
} from '../../../../selectors/formsSelectors'

const selectSelectedCombinedPaymentMethod = createSelector(
  selectDecoratedCombinedPaymentMethods,
  selectBillingCardPaymentTypeFromCheckoutForm,
  (combinedPaymentMethods, value) =>
    getCombinedPaymentMethodByPaymentMethodValue(
      combinedPaymentMethods,
      value,
      combinedPaymentMethods[0]
    )
)

const selectStoredCombinedPaymentMethod = createSelector(
  selectDecoratedCombinedPaymentMethods,
  selectStoredPaymentDetails,
  (combinedPaymentMethods, paymentDetails) =>
    getCombinedPaymentMethodByPaymentMethodValue(
      combinedPaymentMethods,
      path(['type'], paymentDetails),
      undefined
    )
)

const mapStateToProps = (state = {}) => ({
  inDeliveryAndPayment: selectInDeliveryAndPayment(state),
  checkoutForms: selectCheckoutForms(state),
  findAddressState: path(['findAddress'], state),
  combinedPaymentMethods: selectDecoratedCombinedPaymentMethods(state),
  storedPaymentDetails: selectStoredPaymentDetails(state),
  storedPaymentMethod: selectPaymentMethodForStoredPaymentDetails(state),
  storedCombinedPaymentMethod: selectStoredCombinedPaymentMethod(state),
  selectedCombinedPaymentMethod: selectSelectedCombinedPaymentMethod(state),
  isMobile: isMobile(state),
  isMobileBreakpoint: isMobileBreakpoint(state),
})

const mapDispatchToProps = {
  resetFormPartial,
  setFormField,
  touchedFormField,
  setManualAddressMode,
  sendAnalyticsPaymentMethodChangeEvent,
}

@connect(
  mapStateToProps,
  mapDispatchToProps
)
class PaymentDetailsContainer extends Component {
  static propTypes = {
    selectedCombinedPaymentMethod: types.combinedPaymentMethod.isRequired,
    setFormField: PropTypes.func.isRequired,
  }

  componentDidMount() {
    const { selectedCombinedPaymentMethod, setFormField } = this.props
    if (!selectedCombinedPaymentMethod) {
      setFormField('billingCardDetails', 'paymentType', 'VISA', null, {
        isDirty: false,
      })
    }
  }

  render() {
    return <PaymentDetails {...this.props} />
  }
}

export default PaymentDetailsContainer

export { mapStateToProps }
