import PropTypes from 'prop-types'
import React from 'react'

function TotalCost({ totalCost }, { l }) {
  return (
    <div className="TotalCost">
      {l`Total cost`}: {totalCost}
    </div>
  )
}

TotalCost.propTypes = {
  totalCost: PropTypes.string,
}

TotalCost.contextTypes = {
  l: PropTypes.func,
}

export default TotalCost
