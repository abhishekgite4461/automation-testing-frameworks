import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'

import { isMobile } from '../../../../selectors/viewportSelectors'
import {
  isStoreOrParcelDelivery,
  isDeliveryStoreChoiceAccepted,
  getSelectedStoreDetails,
  getSelectedDeliveryLocationType,
} from '../../../../selectors/checkoutSelectors'

// actions
import { showModal } from '../../../../actions/common/modalActions'
import { setStoreUpdating } from '../../../../actions/common/checkoutActions'
import { searchStoresCheckout } from '../../../../actions/components/UserLocatorActions'

// components
import Button from '../../../common/Button/Button'
import DeliveryDetailsForm from './DetailsForm/DeliveryDetailsForm'
import UserLocatorInput from '../../../common/UserLocatorInput/UserLocatorInput'
import CollectFromStore from '../../../common/CollectFromStore/CollectFromStore'

const mapStateToProps = (state) => ({
  isMobile: isMobile(state),
  isStoreUpdating: state.checkout.storeUpdating,
  isStoreOrParcelDelivery: isStoreOrParcelDelivery(state),
  isDeliveryStoreChoiceAccepted: isDeliveryStoreChoiceAccepted(state),
  storeDetails: getSelectedStoreDetails(state),
  selectedDeliveryLocationType: getSelectedDeliveryLocationType(state),
})

const mapDispatchToProps = {
  showModal,
  setStoreUpdating,
  searchStoresCheckout,
}

@connect(mapStateToProps, mapDispatchToProps)
export default class StoreDeliveryContainer extends Component {
  static propTypes = {
    isMobile: PropTypes.bool,
    isStoreUpdating: PropTypes.bool,
    isStoreOrParcelDelivery: PropTypes.bool,
    isDeliveryStoreChoiceAccepted: PropTypes.bool,
    storeDetails: PropTypes.object,
    searchStoresCheckout: PropTypes.func.isRequired,
    setStoreUpdating: PropTypes.func.isRequired,
    selectedDeliveryLocationType: PropTypes.string.isRequired,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentDidMount() {
    if (this.showStoreLocatorModalOnMount()) {
      this.openCollectFromStoreModal()
    }
  }

  componentWillReceiveProps(props) {
    if (this.storeOrParcelDeliveryHasJustBeenSelected(props)) {
      this.openCollectFromStoreModal()
    }
  }

  isDeliveryStoreChosen() {
    return !!this.props.storeDetails && this.props.isDeliveryStoreChoiceAccepted
  }

  storeOrParcelDeliveryHasJustBeenSelected(props) {
    const { isMobile, isStoreUpdating, isStoreOrParcelDelivery } = props

    return !isMobile && isStoreOrParcelDelivery && isStoreUpdating
  }

  showStoreLocatorModalOnMount() {
    const { isMobile, isStoreOrParcelDelivery } = this.props

    return !isMobile && isStoreOrParcelDelivery && !this.isDeliveryStoreChosen()
  }

  showStoreLocatorForm() {
    const { isMobile, isStoreUpdating, isStoreOrParcelDelivery } = this.props

    return (
      (isMobile && isStoreOrParcelDelivery && isStoreUpdating) ||
      (isMobile && isStoreOrParcelDelivery && !this.isDeliveryStoreChosen())
    )
  }

  showSelectedStoreDetails() {
    const {
      isStoreUpdating,
      isStoreOrParcelDelivery,
      isDeliveryStoreChoiceAccepted,
    } = this.props

    return (
      !isStoreUpdating &&
      isStoreOrParcelDelivery &&
      this.isDeliveryStoreChosen() &&
      isDeliveryStoreChoiceAccepted
    )
  }

  get getDeliveryToAnother() {
    return `Deliver to another ${this.deliveryStoreTypeLabel}`
  }

  get storeSelectionButtonLabel() {
    const prefix = this.props.isDeliveryStoreChoiceAccepted
      ? 'Change'
      : 'Choose'
    return `${prefix} ${this.deliveryStoreTypeLabel}`
  }

  get deliveryStoreTypeLabel() {
    if (this.props.selectedDeliveryLocationType === 'STORE') {
      return 'store'
    }

    if (this.props.selectedDeliveryLocationType === 'PARCELSHOP') {
      return 'shop'
    }

    return ''
  }

  goToDeliveryStoreLocator = (event) => {
    event.preventDefault()
    this.props.searchStoresCheckout()
  }

  changeStore = () => {
    const { isMobile, setStoreUpdating } = this.props

    setStoreUpdating(true)

    if (!isMobile) {
      this.openCollectFromStoreModal()
    }
  }

  openCollectFromStoreModal() {
    const { showModal } = this.props
    showModal(<CollectFromStore />, { mode: 'storeLocator' })
  }

  render() {
    const { l } = this.context
    const { storeDetails: store, isMobile } = this.props

    if (!this.props.isStoreOrParcelDelivery) {
      return null
    }

    return (
      <article className="StoreDeliveryContainer">
        {this.showStoreLocatorForm() && (
          <form
            className="StoreDeliveryContainer-searchForm"
            onSubmit={this.goToDeliveryStoreLocator}
          >
            <UserLocatorInput selectedCountry="United Kingdom" />
          </form>
        )}

        {this.showSelectedStoreDetails() && (
          <article className="StoreDeliveryContainer-address">
            {isMobile && (
              <div className="StoreDeliveryContainer-mobileTitleContainer">
                <h3 className="StoreDeliveryContainer-title StoreDeliveryContainer-mobileTitle">
                  {l`Your order will be delivered to`}:
                </h3>
              </div>
            )}

            {!isMobile && (
              <div className="StoreDeliveryContainer-desktopTitleContainer">
                <h3 className="StoreDeliveryContainer-title StoreDeliveryContainer-desktopTitle">
                  {l`Collect from`}
                </h3>
              </div>
            )}

            <div className="StoreDeliveryContainer-addressLines">
              {store.address1 && (
                <p className="StoreDeliveryContainer-addressLine">
                  {store.address1}
                </p>
              )}
              {store.address2 && (
                <p className="StoreDeliveryContainer-addressLine">
                  {store.address2}
                </p>
              )}
              {store.city && (
                <p className="StoreDeliveryContainer-addressLine">
                  {store.city}
                </p>
              )}
              {store.postcode && (
                <p className="StoreDeliveryContainer-addressLine">
                  {store.postcode}
                </p>
              )}
            </div>

            {isMobile && (
              <div className="StoreDeliveryContainer-changeStoreLinkContainer">
                <button
                  className="Button--link StoreDeliveryContainer-changeStoreLink"
                  onClick={this.changeStore}
                >
                  {l(this.getDeliveryToAnother)}
                </button>
              </div>
            )}
          </article>
        )}

        {!isMobile && (
          <div className="StoreDeliveryContainer-changeStoreBtnContainer">
            <Button
              className="StoreDeliveryContainer-changeStoreBtn"
              clickHandler={this.changeStore}
            >
              {l(this.storeSelectionButtonLabel)}
            </Button>
          </div>
        )}

        {this.showSelectedStoreDetails() && (
          <div className="StoreDeliveryContainer-yourDetailsContainer">
            <DeliveryDetailsForm />
          </div>
        )}
      </article>
    )
  }
}
