import PropTypes from 'prop-types'
import { path, pathOr } from 'ramda'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import { getOrderSchema } from './validationSchemas'

// actions
import {
  setAndValidateFormField,
  validateFormField,
  clearFormFieldError,
  resetForm,
} from '../../../../actions/common/formActions'

// components
import Checkbox from '../../../common/FormComponents/Checkbox/Checkbox'

const TermsAndConditions = (
  { value, isTouched, error, url, onChange },
  { l }
) => {
  return (
    <Checkbox
      className="Terms-checkbox"
      checked={{ value }}
      onChange={onChange}
      name="isAcceptedTermsAndConditions"
      errors={{ isAcceptedTermsAndConditions: l(error) }}
      field={{ isTouched }}
      isRequired
    >
      <span>
        {l`I accept the`}{' '}
        <Link
          to={url || `/cms/${l`tcs`}`}
          target="_blank"
        >{l`Terms & Conditions`}</Link>
      </span>
    </Checkbox>
  )
}

TermsAndConditions.propTypes = {
  value: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  isTouched: PropTypes.bool,
  url: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func.isRequired,
}

TermsAndConditions.defaultProps = {
  value: false,
  isTouched: false,
  url: '',
  error: '',
}

TermsAndConditions.contextTypes = {
  l: PropTypes.func,
}

const mapStateToProps = (state) => {
  const { value, isTouched } = pathOr(
    {},
    ['forms', 'checkout', 'order', 'fields', 'isAcceptedTermsAndConditions'],
    state
  )
  const error = pathOr(
    '',
    ['forms', 'checkout', 'order', 'errors', 'isAcceptedTermsAndConditions'],
    state
  )

  return {
    value,
    isTouched,
    url: path(['cms', 'pages', 'termsAndConditions', 'seoUrl'], state),
    error,
  }
}

const mapDispatchToProps = {
  onChange: ({ target: { checked } } = {}) => {
    const validators = getOrderSchema().isAcceptedTermsAndConditions
    return setAndValidateFormField(
      'order',
      'isAcceptedTermsAndConditions',
      checked || '',
      validators
    )
  },
  validate: () => {
    const validators = getOrderSchema().isAcceptedTermsAndConditions
    return validateFormField(
      'order',
      'isAcceptedTermsAndConditions',
      validators
    )
  },
  clearError: () => {
    return clearFormFieldError('order', 'isAcceptedTermsAndConditions')
  },
  resetForm,
}

@connect(mapStateToProps, mapDispatchToProps)
class TermsAndConditionsContainer extends Component {
  componentDidMount() {
    this.props.validate()
  }

  componentWillUnmount() {
    this.props.clearError()
    this.props.resetForm('order', { isAcceptedTermsAndConditions: '' })
  }

  render() {
    return <TermsAndConditions {...this.props} />
  }
}

export default TermsAndConditionsContainer

export { TermsAndConditions, mapStateToProps, mapDispatchToProps }
