import React from 'react'

import AddressForm from './AddressForm'

const BillingAddressForm = (props) => {
  return (
    <AddressForm
      addressType="billing"
      titleHidden
      renderTelephone={props.renderTelephone}
    />
  )
}

export default BillingAddressForm
