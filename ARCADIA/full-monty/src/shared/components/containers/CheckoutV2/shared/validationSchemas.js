import paymentValidationRules from '../../../../constants/paymentValidationRules'
import {
  cardExpiry,
  regexValidate,
  hasLength,
  smsMobileNumber,
  requiredCurried,
} from '../../../../lib/validator/validators'

export const regionalPhoneValidation = (country) => {
  switch (country) {
    case 'United Kingdom':
      return ['ukPhoneNumber']
    case 'United States':
      return ['usPhoneNumber']
    default:
      return ['numbersOnly']
  }
}

export const getYourDetailsSchema = (country) => {
  return {
    title: 'required',
    firstName: ['noEmoji', 'required'],
    lastName: ['noEmoji', 'required'],
    telephone: ['required', ...regionalPhoneValidation(country), 'noEmoji'],
  }
}

export const getDeliveryInstructionsSchema = (country) => {
  const rules = {
    deliveryInstructions: 'noEmoji',
  }
  if (country === 'United Kingdom') rules.smsMobileNumber = smsMobileNumber
  return rules
}

const postCodeValidation = (rules) => {
  return rules && !!rules.postcodeRequired && !!rules.pattern
    ? [
        'required',
        regexValidate('Please enter a valid post code', rules.pattern),
      ]
    : []
}

export const getYourAddressSchema = (rules) => ({
  address1: ['required', 'noEmoji'],
  address2: 'noEmoji',
  postcode: [...postCodeValidation(rules), 'noEmoji'],
  city: ['required', 'noEmoji'],
})

export const getFindAddressSchema = (rules, options = {}) => {
  const { hasFoundAddresses, hasSelectedAddress } = options
  return {
    postCode: postCodeValidation(rules),
    country: 'country',
    houseNumber: 'noEmoji',
    findAddress: hasFoundAddresses
      ? []
      : requiredCurried('Please click on FIND ADDRESS'),
    selectAddress: hasSelectedAddress
      ? []
      : requiredCurried('Please select an address'),
  }
}

const validateCvv = (type) =>
  paymentValidationRules[type]
    ? [
        'numbersOnly',
        hasLength(
          paymentValidationRules[type].cvv.messageV2,
          paymentValidationRules[type].cvv.length
        ),
      ]
    : ''

export const getCardSchema = (type, customerDetailsSection = false) => ({
  cardNumber: paymentValidationRules[type]
    ? [
        hasLength(
          paymentValidationRules[type].cardNumber.message,
          paymentValidationRules[type].cardNumber.length
        ),
        'numbersOnly',
      ]
    : '',
  cvv: customerDetailsSection ? '' : validateCvv(type),
  expiryMonth: paymentValidationRules[type]
    ? cardExpiry(`Please select a valid expiry date`, new Date())
    : '',
})

export const getOrderSchema = () => ({
  isAcceptedTermsAndConditions: ['required'],
})
