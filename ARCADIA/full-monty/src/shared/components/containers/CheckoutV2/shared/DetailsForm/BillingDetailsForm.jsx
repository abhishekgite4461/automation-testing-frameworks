import React from 'react'

import DetailsForm from './DetailsForm'

const BillingDetailsForm = (props) => {
  return (
    <DetailsForm
      detailsType="billing"
      label="Billing address"
      titleHidden
      renderTelephone={props.renderTelephone}
    />
  )
}

export default BillingDetailsForm
