import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'

import { getEnrichedDeliveryLocations } from '../../../../../shared/selectors/checkoutSelectors'

// actions
import { selectDeliveryLocation } from '../../../../actions/common/checkoutActions'

// components
import DeliveryOptions from '../Delivery/DeliveryOptions'

const mapStateToProps = (state) => ({
  deliveryLocations: getEnrichedDeliveryLocations(state),
})

const mapDispatchToProps = (dispatch, { onChangeDeliveryLocation } = {}) => ({
  onChangeDeliveryLocation:
    onChangeDeliveryLocation ||
    ((deliveryLocation, index) => dispatch(selectDeliveryLocation(index))),
})

const DeliveryOptionsContainer = ({
  deliveryLocations,
  onChangeDeliveryLocation,
}) => {
  return deliveryLocations.length > 1 ? (
    <DeliveryOptions
      deliveryLocations={deliveryLocations}
      onChangeDeliveryLocation={onChangeDeliveryLocation}
    />
  ) : null
}

DeliveryOptionsContainer.propTypes = {
  deliveryLocations: PropTypes.array,
  onChangeDeliveryLocation: PropTypes.func.isRequired,
}

DeliveryOptionsContainer.defaultProps = {
  deliveryLocations: [],
}

export default connect(mapStateToProps, mapDispatchToProps)(
  DeliveryOptionsContainer
)

export {
  DeliveryOptionsContainer as WrappedDeliveryOptionsContainer,
  mapStateToProps,
  mapDispatchToProps,
}
