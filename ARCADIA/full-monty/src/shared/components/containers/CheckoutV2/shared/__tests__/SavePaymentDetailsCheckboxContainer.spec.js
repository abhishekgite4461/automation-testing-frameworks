import { identity } from 'ramda'
import testComponentHelper from 'test/unit/helpers/test-component'
import SavePaymentDetailsCheckbox from '../SavePaymentDetailsCheckbox'
import SavePaymentDetailsCheckboxContainer from '../SavePaymentDetailsCheckboxContainer'

describe(SavePaymentDetailsCheckboxContainer.name, () => {
  const context = {
    l: jest.fn(identity),
  }
  const renderComponent = testComponentHelper(
    SavePaymentDetailsCheckboxContainer.WrappedComponent,
    { context }
  )
  const baseProps = {
    onSavePaymentDetailsChange: jest.fn(),
    savePaymentDetailsEnabled: false,
  }
  beforeEach(() => {
    jest.resetAllMocks()
  })
  describe('@render', () => {
    it('should render SavePaymentDetailsCheckbox when the feature flag status is true', () => {
      const props = {
        ...baseProps,
        featureSavePaymentDetailsEnabled: true,
      }
      const { wrapper, getTree } = renderComponent(props)
      expect(getTree()).toMatchSnapshot()
      const savePaymentDetailsCheckbox = wrapper.find(
        SavePaymentDetailsCheckbox
      )
      expect(
        savePaymentDetailsCheckbox.prop('onSavePaymentDetailsChange')
      ).toEqual(baseProps.onSavePaymentDetailsChange)
      expect(
        savePaymentDetailsCheckbox.prop('savePaymentDetailsEnabled')
      ).toEqual(baseProps.savePaymentDetailsEnabled)
    })
    it('should not render SavePaymentDetailsCheckbox when the feature flag status is false', () => {
      const props = {
        ...baseProps,
        featureSavePaymentDetailsEnabled: false,
      }
      const { getTree } = renderComponent(props)
      expect(getTree()).toMatchSnapshot()
    })
  })
})
