import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { map } from 'ramda'

import ProgressTracker from '../../../common/ProgressTracker/ProgressTracker'

const CHECKOUT_V2_STEPS = [
  {
    title: 'Delivery',
    url: '/checkout/delivery',
  },
  {
    title: 'Payment',
    url: '/checkout/payment',
  },
  {
    title: 'Thank You',
    url: '/order-complete',
  },
]

const CHECKOUT_V2_DP_STEPS = [
  {
    title: 'Delivery and Payment',
    url: '/checkout/delivery-payment',
  },
  {
    title: 'Thank You',
    url: '/order-complete',
  },
]

const mapStateToProps = (state) => ({
  location: state.routing.location,
})

@connect(mapStateToProps)
export default class CheckoutProgressTracker extends Component {
  static propTypes = {
    location: PropTypes.object,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  enrichSteps = (steps, pathname) => {
    const { l } = this.context

    return map(({ title, url }) => {
      return {
        title: l(title),
        url,
        active: pathname.startsWith(url),
      }
    }, steps)
  }

  render() {
    const { location } = this.props
    const deliveryAndPaymentEnabled = location.pathname.startsWith(
      '/checkout/delivery-payment'
    )
    const steps = this.enrichSteps(
      deliveryAndPaymentEnabled ? CHECKOUT_V2_DP_STEPS : CHECKOUT_V2_STEPS,
      location.pathname
    )

    return <ProgressTracker steps={steps} />
  }
}
