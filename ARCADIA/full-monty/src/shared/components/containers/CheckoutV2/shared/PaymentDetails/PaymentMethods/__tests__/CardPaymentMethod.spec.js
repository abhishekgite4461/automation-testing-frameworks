import testComponentHelper from 'test/unit/helpers/test-component'

import { WrappedCardPaymentMethod, mapStateToProps } from '../CardPaymentMethod'

describe('<CardPaymentMethod/>', () => {
  const renderComponent = testComponentHelper(WrappedCardPaymentMethod)

  const showModalStub = jest.fn()

  const setFormFieldStub = jest.fn()

  const validateFormFieldStub = jest.fn()

  const setAndValidateFormFieldStub = jest.fn()

  const touchedFormFieldStub = jest.fn()

  const baseProps = {
    showModal: showModalStub,
    setFormField: setFormFieldStub,
    setAndValidateFormField: setAndValidateFormFieldStub,
    validateFormField: validateFormFieldStub,
    touchFormField: touchedFormFieldStub,
    validate: () => {},
    clearErrors: jest.fn(),
    sendAnalyticsValidationState: () => {},
  }

  // Fri Aug 11 2017 11:51:51 GMT+0100 (BST)
  const fixedDate = new Date(1502448695606)

  let dateMock

  beforeEach(() => {
    dateMock = jest.spyOn(global, 'Date').mockImplementation(() => fixedDate)
  })

  afterEach(() => {
    jest.resetAllMocks()
  })

  describe('@connect', () => {
    describe('mapStateToProps', () => {
      const formsState = {
        forms: {
          checkout: {
            billingCardDetails: {
              fields: {
                paymentType: {
                  value: 'VISA',
                },
                cardNumber: {
                  value: '4444333322221111',
                },
                expiryMonth: {
                  value: '06',
                },
                expiryYear: {
                  value: '2017',
                },
                cvv: {
                  value: '123',
                },
              },
            },
          },
        },
      }

      const paymentMethodsState = {
        paymentMethods: [
          { value: 'VISA', type: 'CARD', label: 'Visa' },
          { value: 'AMEX', type: 'CARD', label: 'American Express' },
          { value: 'ACCNT', type: 'OTHER_CARD' },
          { value: 'PYPAL', type: 'OTHER' },
        ],
      }

      const viewportState = {
        viewport: {
          media: 'desktop',
        },
      }

      it('should create default props', () => {
        expect(
          mapStateToProps(
            {
              ...viewportState,
              paymentMethods: [],
            },
            {
              formCardPath: ['checkout', 'billingCardDetails', 'fields'],
              formCardErrorPath: [
                'forms',
                'checkout',
                'billingCardDetails',
                'errors',
              ],
            }
          )
        ).toEqual({
          isMobile: false,
          paymentType: null,
          cardNumberField: {},
          expiryMonthField: {},
          expiryYearField: {},
          cvvField: {},
          errors: {},
          cardTypes: [],
          validationSchema: {
            cardNumber: '',
            cvv: '',
            expiryMonth: '',
          },
        })
      })

      it('should return the fields', () => {
        const props = mapStateToProps(
          {
            ...formsState,
            ...viewportState,
            ...paymentMethodsState,
          },
          {
            formCardPath: ['checkout', 'billingCardDetails', 'fields'],
            formCardErrorPath: [
              'forms',
              'checkout',
              'billingCardDetails',
              'errors',
            ],
          }
        )
        expect(props.cardNumberField).toEqual({ value: '4444333322221111' })
        expect(props.expiryMonthField).toEqual({ value: '06' })
        expect(props.expiryYearField).toEqual({ value: '2017' })
        expect(props.cvvField).toEqual({ value: '123' })
      })

      it('should return the payment type', () => {
        expect(
          mapStateToProps(
            {
              ...formsState,
              ...viewportState,
              ...paymentMethodsState,
            },
            {
              formCardPath: ['checkout', 'billingCardDetails', 'fields'],
              formCardErrorPath: [
                'forms',
                'checkout',
                'billingCardDetails',
                'errors',
              ],
            }
          ).paymentType
        ).toBe('VISA')
      })

      it('should return the errors', () => {
        dateMock.mockRestore()
        expect(
          mapStateToProps(
            {
              ...paymentMethodsState,
              ...viewportState,
              forms: {
                checkout: {
                  billingCardDetails: {
                    errors: {
                      cardNumber: 'A 16 digit card number is required',
                      expiryMonth: 'Please select a valid expiry date',
                      cvv: '3 digits required',
                    },
                  },
                },
              },
            },
            {
              formCardPath: ['checkout', 'billingCardDetails', 'fields'],
              formCardErrorPath: [
                'forms',
                'checkout',
                'billingCardDetails',
                'errors',
              ],
            }
          ).errors
        ).toEqual({
          cardNumber: 'A 16 digit card number is required',
          expiryMonth: 'Please select a valid expiry date',
          cvv: '3 digits required',
        })
      })

      it('should return the card types', () => {
        expect(
          mapStateToProps(
            {
              ...formsState,
              ...paymentMethodsState,
              ...viewportState,
            },
            {
              formCardPath: ['checkout', 'billingCardDetails', 'fields'],
              formCardErrorPath: [
                'forms',
                'checkout',
                'billingCardDetails',
                'errors',
              ],
            }
          ).cardTypes
        ).toEqual(['Visa', 'American Express'])
      })
    })
  })

  describe('@renders', () => {
    it('should render default state correctly', () => {
      const component = renderComponent(baseProps)
      expect(component.getTree()).toMatchSnapshot()
    })

    describe('Card Number Input', () => {
      it('should pass `cardNumberField` as `field` prop', () => {
        const component = renderComponent({
          ...baseProps,
          cardNumberField: {
            value: '4444333322221111',
            isTouched: true,
          },
        })
        expect(
          component.wrapper.find('.CardPaymentMethod-cardNumber').prop('field')
        ).toEqual({
          value: '4444333322221111',
          isTouched: true,
        })
      })

      it('should pass card number error as `errors` prop', () => {
        const component = renderComponent({
          ...baseProps,
          errors: {
            cardNumber: 'A 16 digit card number is required',
          },
        })
        expect(
          component.wrapper.find('.CardPaymentMethod-cardNumber').prop('errors')
        ).toEqual({
          cardNumber: 'A 16 digit card number is required',
        })
      })

      it('should set type to `tel` (needs to _not_ be `number` so iOS displays autofill option', () => {
        const { wrapper } = renderComponent(baseProps)
        expect(wrapper.find('.CardPaymentMethod-cardNumber').prop('type')).toBe(
          'tel'
        )
      })
    })

    describe('Expiry Month Select', () => {
      const props = {
        ...baseProps,
        expiryMonthField: {
          value: '06',
          isDirty: true,
        },
        errors: {
          expiryMonth: 'Please select a valid expiry date',
        },
      }

      it('should pass `expiryMonthField.value` as `value` prop', () => {
        const component = renderComponent(props)
        expect(
          component.wrapper.find('.CardPaymentMethod-expiryMonth').prop('value')
        ).toBe('06')
      })

      it('should add an `is-erroring` class if field is dirty and there is an error', () => {
        const component = renderComponent(props)
        expect(
          component.wrapper
            .find('.CardPaymentMethod-expiryDate')
            .hasClass('is-erroring')
        ).toBe(true)
        expect(
          component.wrapper
            .find('.CardPaymentMethod-expiryMonth')
            .hasClass('is-erroring')
        ).toBe(true)
      })
    })

    describe('Expiry Year Select', () => {
      it('should pass `expiryYearField.value` as `value` prop', () => {
        const component = renderComponent({
          ...baseProps,
          expiryYearField: {
            value: '2017',
            isDirty: true,
          },
        })
        expect(
          component.wrapper.find('.CardPaymentMethod-expiryYear').prop('value')
        ).toBe('2017')
      })
    })

    describe('Expiry Month Error Message', () => {
      it('should display if both the expiry month and year inputs are dirty', () => {
        const component = renderComponent({
          ...baseProps,
          expiryMonthField: {
            isDirty: true,
          },
          expiryYearField: {
            isDirty: true,
          },
          errors: {
            expiryMonth: 'Please select a valid expiry date',
          },
        })
        const errorMessageComponent = component.wrapper.find(
          '.CardPaymentMethod-expiryValidationMessage'
        )
        expect(errorMessageComponent.length).toBe(1)
        expect(errorMessageComponent.prop('message')).toBe(
          'Please select a valid expiry date'
        )
      })
    })

    describe('CVV Field', () => {
      it('should pass `cvvField` as `field` prop', () => {
        const component = renderComponent({
          ...baseProps,
          cvvField: {
            value: '123',
            isTouched: true,
          },
        })
        expect(component.wrapper.find('CVVField').prop('field')).toEqual({
          value: '123',
          isTouched: true,
        })
      })

      it('should pass cvv error as `errors` prop', () => {
        const component = renderComponent({
          ...baseProps,
          errors: {
            cvv: '3 digits required',
          },
        })
        expect(component.wrapper.find('CVVField').prop('error')).toEqual(
          '3 digits required'
        )
      })
    })
  })

  describe('@methods', () => {
    describe('removeWhiteSpaces', () => {
      it('should removes whitespaces for a given field', () => {
        const validator = 'card number validator'
        const fieldName = 'cardNumber'
        const validationSchema = { [fieldName]: validator }
        const { instance } = renderComponent({ ...baseProps, validationSchema })
        instance.removeWhiteSpaces(fieldName)({
          target: { value: '555 555555555 4444' },
        })
        expect(instance.props.setAndValidateFormField).toHaveBeenCalledWith(
          'billingCardDetails',
          fieldName,
          '5555555555554444',
          validator
        )
      })
    })
  })

  describe('@events', () => {
    describe('Card Number', () => {
      it('should be able to set the card number', () => {
        const component = renderComponent(baseProps)
        const setFieldHandler = component.wrapper
          .find('.CardPaymentMethod-cardNumber')
          .prop('setField')
        setFieldHandler()({ target: { value: '4444333322221111' } })
        expect(setAndValidateFormFieldStub).toHaveBeenCalledWith(
          'billingCardDetails',
          'cardNumber',
          '4444333322221111',
          []
        )
      })

      it('should set the `paymentType` if is a payment card and `paymentType` changes', () => {
        const component = renderComponent({
          ...baseProps,
          isPaymentCard: true,
        })
        const setFieldHandler = component.wrapper
          .find('.CardPaymentMethod-cardNumber')
          .prop('setField')
        setFieldHandler()({ target: { value: '4444333322221111' } })
        expect(setFormFieldStub).toHaveBeenCalledWith(
          'billingCardDetails',
          'paymentType',
          'VISA'
        )
      })

      it('should set non-Visa `paymentType` if applicable', () => {
        const component = renderComponent({
          ...baseProps,
          isPaymentCard: true,
          cardTypes: ['American Express'],
        })
        const setFieldHandler = component.wrapper
          .find('.CardPaymentMethod-cardNumber')
          .prop('setField')
        setFieldHandler()({ target: { value: '343434343434343' } })
        expect(setFormFieldStub).toHaveBeenCalledWith(
          'billingCardDetails',
          'paymentType',
          'AMEX'
        )
      })

      it('should not set the `paymentType` if not a payment card', () => {
        const component = renderComponent({
          ...baseProps,
        })
        const setFieldHandler = component.wrapper
          .find('.CardPaymentMethod-cardNumber')
          .prop('setField')
        setFieldHandler()({ target: { value: '4444333322221111' } })
        expect(setAndValidateFormFieldStub).not.toHaveBeenCalledWith(
          'billingCardDetails',
          'paymentType',
          'VISA'
        )
      })

      it('should not set the `paymentType` if `paymentType` hasn’t changed', () => {
        const props = {
          ...baseProps,
          isPaymentCard: true,
          paymentType: 'VISA',
        }
        const component = renderComponent(props)
        const setFieldHandler = component.wrapper
          .find('.CardPaymentMethod-cardNumber')
          .prop('setField')
        setFieldHandler()({ target: { value: '4444333322221111' } })
        expect(setAndValidateFormFieldStub).not.toHaveBeenCalledWith(
          'billingCardDetails',
          'paymentType',
          'VISA'
        )
      })

      it('should be able to touch the card number field', () => {
        const component = renderComponent(baseProps)
        const touchedFieldHandler = component.wrapper
          .find('.CardPaymentMethod-cardNumber')
          .prop('touchedField')
        touchedFieldHandler('cardNumber')()
        expect(touchedFormFieldStub).toHaveBeenCalledWith(
          'billingCardDetails',
          'cardNumber'
        )
      })
    })

    describe('Expiry Month', () => {
      it('should be able to set the expiry month', () => {
        const component = renderComponent(baseProps)
        const onChangeHandler = component.wrapper
          .find('.CardPaymentMethod-expiryMonth')
          .prop('onChange')
        onChangeHandler({ target: { value: '04' } })
        expect(setAndValidateFormFieldStub).toHaveBeenCalledWith(
          'billingCardDetails',
          'expiryMonth',
          '04',
          []
        )
      })

      it('should set to touched on blur', () => {
        const component = renderComponent(baseProps)
        const onBlurHandler = component.wrapper
          .find('.CardPaymentMethod-expiryMonth')
          .prop('onBlur')
        onBlurHandler()
        expect(touchedFormFieldStub).toHaveBeenCalledWith(
          'billingCardDetails',
          'expiryMonth'
        )
      })

      it('should call `sendAnalyticsValidationState` on Blur', () => {
        const analyticsSpy = jest.fn()
        const { wrapper } = renderComponent({
          ...baseProps,
          sendAnalyticsValidationState: analyticsSpy,
        })
        const onBlurHandler = wrapper
          .find('.CardPaymentMethod-expiryMonth')
          .prop('onBlur')
        onBlurHandler()
        expect(analyticsSpy).toHaveBeenCalledTimes(1)
      })

      it('should call `sendAnalyticsValidationState` with validation `failure` if expiryMonth error exists onBlur', () => {
        const analyticsSpy = jest.fn()
        const { wrapper } = renderComponent({
          ...baseProps,
          sendAnalyticsValidationState: analyticsSpy,
          errors: {
            expiryMonth: 'some error',
          },
          expiryMonthField: {
            isDirty: true,
          },
        })
        const onBlurHandler = wrapper
          .find('.CardPaymentMethod-expiryMonth')
          .prop('onBlur')
        onBlurHandler()
        expect(analyticsSpy).toHaveBeenCalledTimes(1)
        expect(analyticsSpy).toHaveBeenCalledWith({
          id: 'expiryMonth',
          validationStatus: 'failure',
        })
      })

      it('should call `sendAnalyticsValidationState` with validation `success` if expiryMonth error doesnt exist', () => {
        const analyticsSpy = jest.fn()
        const { wrapper } = renderComponent({
          ...baseProps,
          sendAnalyticsValidationState: analyticsSpy,
          errors: {
            irrelevantField: 'some error',
          },
        })
        const onBlurHandler = wrapper
          .find('.CardPaymentMethod-expiryMonth')
          .prop('onBlur')
        onBlurHandler()
        expect(analyticsSpy).toHaveBeenCalledTimes(1)
        expect(analyticsSpy).toHaveBeenCalledWith({
          id: 'expiryMonth',
          validationStatus: 'success',
        })
      })
    })

    describe('Expiry Year', () => {
      it('should be able to set the expiry year', () => {
        const component = renderComponent(baseProps)
        const onChangeHandler = component.wrapper
          .find('.CardPaymentMethod-expiryYear')
          .prop('onChange')
        onChangeHandler({ target: { value: '2017' } })
        expect(setFormFieldStub).toHaveBeenCalledWith(
          'billingCardDetails',
          'expiryYear',
          '2017'
        )
        expect(validateFormFieldStub).toHaveBeenCalledWith(
          'billingCardDetails',
          'expiryMonth',
          []
        )
      })

      it('should set to touched on blur', () => {
        const component = renderComponent(baseProps)
        const onBlurHandler = component.wrapper
          .find('.CardPaymentMethod-expiryYear')
          .prop('onBlur')
        onBlurHandler()
        expect(touchedFormFieldStub).toHaveBeenCalledWith(
          'billingCardDetails',
          'expiryYear'
        )
      })

      it('should call `sendAnalyticsValidationState` on Blur', () => {
        const analyticsSpy = jest.fn()
        const { wrapper } = renderComponent({
          ...baseProps,
          sendAnalyticsValidationState: analyticsSpy,
        })
        const onBlurHandler = wrapper
          .find('.CardPaymentMethod-expiryYear')
          .prop('onBlur')
        onBlurHandler()
        expect(analyticsSpy).toHaveBeenCalledTimes(1)
      })

      it('should call `sendAnalyticsValidationState` with validation `failure` if expiryMonth error exists onBlur', () => {
        const analyticsSpy = jest.fn()
        const { wrapper } = renderComponent({
          ...baseProps,
          sendAnalyticsValidationState: analyticsSpy,
          errors: {
            expiryMonth: 'some error',
          },
          expiryMonthField: {
            isDirty: true,
          },
        })
        const onBlurHandler = wrapper
          .find('.CardPaymentMethod-expiryYear')
          .prop('onBlur')
        onBlurHandler()
        expect(analyticsSpy).toHaveBeenCalledTimes(1)
        expect(analyticsSpy).toHaveBeenCalledWith({
          id: 'expiryYear',
          validationStatus: 'failure',
        })
      })

      it('should call `sendAnalyticsValidationState` with validation `success` if expiryMonth error doesnt exist', () => {
        const analyticsSpy = jest.fn()
        const { wrapper } = renderComponent({
          ...baseProps,
          sendAnalyticsValidationState: analyticsSpy,
          errors: {
            irrelevantField: 'some error',
          },
        })
        const onBlurHandler = wrapper
          .find('.CardPaymentMethod-expiryYear')
          .prop('onBlur')
        onBlurHandler()
        expect(analyticsSpy).toHaveBeenCalledTimes(1)
        expect(analyticsSpy).toHaveBeenCalledWith({
          id: 'expiryYear',
          validationStatus: 'success',
        })
      })
    })

    describe('CVV', () => {
      it('should be able to set the cvv', () => {
        const component = renderComponent(baseProps)
        const setFieldHandler = component.wrapper
          .find('CVVField')
          .prop('setField')
        setFieldHandler('cvv')({ target: { value: '123' } })
        expect(setAndValidateFormFieldStub).toHaveBeenCalledWith(
          'billingCardDetails',
          'cvv',
          '123',
          []
        )
      })

      it('should be able to touch the cvv field', () => {
        const component = renderComponent(baseProps)
        const touchedFieldHandler = component.wrapper
          .find('CVVField')
          .prop('touchedField')
        touchedFieldHandler('cvv')()
        expect(touchedFormFieldStub).toHaveBeenCalledWith(
          'billingCardDetails',
          'cvv'
        )
      })
    })

    describe('Reset errors', () => {
      it('clears the form errors when the payment type has changed', () => {
        const { wrapper } = renderComponent({
          ...baseProps,
          paymentType: 'VISA',
        })
        wrapper.setProps({ paymentType: 'AMEX' })
        expect(baseProps.clearErrors).toHaveBeenCalled()
      })
    })
  })
})

// TODO: Look into the proper way to do dateMock.mockRestore()
