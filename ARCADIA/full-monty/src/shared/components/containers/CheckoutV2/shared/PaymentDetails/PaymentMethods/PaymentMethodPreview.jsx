import PropTypes from 'prop-types'
import React from 'react'
import { paymentMethod as paymentMethodType } from '../../../../../../constants/propTypes/paymentMethods'
import { KLARNA } from '../../../../../../constants/paymentTypes'

// components
import ConfirmCVV from '../ConfirmCVV'
import KlarnaForm from '../../../Klarna/KlarnaForm'
import Image from '../../../../../common/Image/Image'

const PaymentMethodPreview = (
  { children, type, value, onChange, storedPaymentMethod = {} },
  { l }
) => {
  return (
    <div className="PaymentMethodPreview">
      <Image
        className="PaymentMethodPreview-icon"
        src={
          storedPaymentMethod.icon
            ? `/assets/common/images/${storedPaymentMethod.icon}`
            : '/assets/{brandName}/images/credit-card.svg'
        }
      />
      <div className="PaymentMethodPreview-details">
        {children}
        <button
          className="Button Button--secondary PaymentMethodPreview-button"
          onClick={onChange}
        >{l`Change`}</button>
        {['CARD', 'OTHER_CARD'].includes(type) && (
          <ConfirmCVV className="PaymentMethodPreview-confirmCVV" />
        )}
      </div>
      {value === KLARNA && <KlarnaForm />}
    </div>
  )
}

PaymentMethodPreview.propTypes = {
  children: PropTypes.node.isRequired,
  type: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  storedPaymentMethod: paymentMethodType,
}

PaymentMethodPreview.defaultProps = {
  type: undefined,
  onChange: () => {},
}

PaymentMethodPreview.contextTypes = {
  l: PropTypes.func,
}

export default PaymentMethodPreview
