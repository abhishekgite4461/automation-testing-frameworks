import testComponentHelper from 'test/unit/helpers/test-component'

import PaymentMethodOption from '../PaymentMethodOption'

describe('<PaymentMethodOption/>', () => {
  const renderComponent = testComponentHelper(PaymentMethodOption)

  const baseProps = {
    label: 'Debit and Credit Card',
    value: 'CARD',
  }

  describe('@renders', () => {
    it('should render default state correctly', () => {
      const component = renderComponent(baseProps)
      expect(component.getTree()).toMatchSnapshot()
    })

    it('should be able to set the description', () => {
      const component = renderComponent({
        ...baseProps,
        description: 'Visa, MasterCard, American Express, Switch/Maestro',
      })
      expect(
        component.wrapper.find('.PaymentMethodOption-description').text()
      ).toEqual('Visa, MasterCard, American Express, Switch/Maestro')
    })

    it('should be able to add icons', () => {
      const icons = ['icon-one.svg', 'icon-two.svg']
      const component = renderComponent({
        ...baseProps,
        icons,
      })
      const iconWrappers = component.wrapper.find('.PaymentMethodOption-icon')

      icons.forEach((icon, index) => {
        const iconWrapper = iconWrappers.at(index)
        expect(iconWrapper.prop('src')).toEqual(`/assets/common/images/${icon}`)
        expect(iconWrapper.key()).toEqual(icon)
      })
    })

    it('should be able to set `checked`', () => {
      const component = renderComponent({
        ...baseProps,
        isChecked: true,
      })
      expect(component.wrapper.find('input').prop('checked')).toBe(true)
    })

    it('should be able to pass an `onChange` handler', () => {
      const onChangeHandler = jest.fn()
      const component = renderComponent({
        ...baseProps,
        onChange: onChangeHandler,
      })
      expect(component.wrapper.find('input').prop('onChange')).toBe(
        onChangeHandler
      )
    })
  })
})
