import React, { Component } from 'react'
import PropTypes from 'prop-types'

// components
import Accordion from '../../../../common/Accordion/Accordion'
import Sandbox from '../../../SandBox/SandBox'

class DigitalDeliveryPass extends Component {
  static contextTypes = {
    l: PropTypes.func,
  }

  state = {
    contentLoaded: false,
  }

  onContentLoaded = () => {
    this.setState({ contentLoaded: true })
  }

  render() {
    const { l } = this.context
    const { contentLoaded } = this.state

    return (
      <Accordion
        className="DigitalDeliveryPass"
        header={
          <h3 className={'DigitalDeliveryPass-header'}>
            {l`Digital Delivery Pass`}
          </h3>
        }
        accordionName="DDP"
        noContentPadding
        showLoader={!contentLoaded}
        expanded
      >
        <Sandbox
          shouldGetContentOnFirstLoad
          isResponsiveCatHeader
          onContentLoaded={this.onContentLoaded}
          cmsPageName="ddpPromo"
        />
      </Accordion>
    )
  }
}

export default DigitalDeliveryPass
