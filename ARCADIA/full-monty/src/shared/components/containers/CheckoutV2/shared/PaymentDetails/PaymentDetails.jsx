import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { pluck, path } from 'ramda'

import * as paymentTypes from '../../../../../constants/paymentTypes'
import * as types from './types'
import { getFindAddressSchema } from '../validationSchemas'

// components
import PaymentMethodOption from './PaymentMethodOption'
import KlarnaForm from '../../Klarna/KlarnaForm'
import CardPaymentMethod from './PaymentMethods/CardPaymentMethod'
import PaymentMethodPreview from './PaymentMethods/PaymentMethodPreview'
import AddressPreview from '../AddressPreview'
import BillingDetailsForm from '../DetailsForm/BillingDetailsForm'
import BillingAddressForm from '../AddressForm/BillingAddressForm'
import CheckoutTitle from '../CheckoutTitle'
import { paymentMethod as paymentMethodType } from '../../../../../constants/propTypes/paymentMethods'

// Card Const
const cardPath = ['checkout', 'billingCardDetails', 'fields']
const cardErrorPath = ['forms', 'checkout', 'billingCardDetails', 'errors']
const cardName = 'billingCardDetails'

export default class PaymentDetails extends Component {
  static propTypes = {
    checkoutForms: PropTypes.object.isRequired,
    combinedPaymentMethods: PropTypes.arrayOf(types.combinedPaymentMethod)
      .isRequired,
    findAddressState: PropTypes.object.isRequired,
    inDeliveryAndPayment: PropTypes.bool.isRequired,
    isMobile: PropTypes.bool.isRequired,
    isMobileBreakpoint: PropTypes.bool.isRequired,
    resetFormPartial: PropTypes.func.isRequired,
    storedPaymentDetails: types.paymentDetails.isRequired,
    storedCombinedPaymentMethod: types.combinedPaymentMethod,
    storedPaymentMethod: paymentMethodType,
    selectedCombinedPaymentMethod: types.combinedPaymentMethod.isRequired,
    setFormField: PropTypes.func.isRequired,
    setManualAddressMode: PropTypes.func.isRequired,
    sendAnalyticsPaymentMethodChangeEvent: PropTypes.func.isRequired,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  state = {
    showPaymentPreview: this.props.storedPaymentDetails,
    showAddressPreview: true,
  }

  handlePaymentMethodChange = ({ target: { checked, value } }) => {
    const {
      resetFormPartial,
      setFormField,
      sendAnalyticsPaymentMethodChangeEvent,
    } = this.props
    resetFormPartial('billingCardDetails', {
      cardNumber: '',
      expiryMonth: '',
      expiryYear: '',
      cvv: '',
    })
    if (checked) {
      setFormField(
        'billingCardDetails',
        'paymentType',
        value === 'CARD' ? 'VISA' : value
      )
      sendAnalyticsPaymentMethodChangeEvent(value)
    }
  }

  handlePaymentMethodPreviewChange = () => {
    this.props.setFormField('billingCardDetails', 'paymentType', 'VISA')
    this.setState({
      showPaymentPreview: false,
    })
  }

  loginNotice(selectedAPMLabel) {
    const { l } = this.context
    return l`You will be asked to log onto ${selectedAPMLabel} to confirm your order`
  }

  renderPaymentMethod(paymentType, { label }) {
    if (paymentType === paymentTypes.KLARNA) {
      return [
        <p key="login-notice" className="PaymentDetails-logInNotice">
          {this.loginNotice(label)}
        </p>,
        <KlarnaForm key="klarna-form" />,
      ]
    } else if (paymentType === 'CARD') {
      return (
        <CardPaymentMethod
          isPaymentCard
          formCardPath={cardPath}
          formCardErrorPath={cardErrorPath}
          formCardName={cardName}
        />
      )
    } else if (paymentType === 'ACCNT') {
      return (
        <CardPaymentMethod
          formCardPath={cardPath}
          formCardErrorPath={cardErrorPath}
          formCardName={cardName}
        />
      )
    }

    return (
      <p className="PaymentDetails-logInNotice">{this.loginNotice(label)}</p>
    )
  }

  getFindAddressSchema(rules) {
    const { findAddressState, checkoutForms } = this.props

    if (findAddressState.isManual) return {}

    return getFindAddressSchema(rules, {
      hasFoundAddresses: findAddressState.monikers.length > 0,
      hasSelectedAddress: !!path(
        ['fields', 'address1', 'value'],
        checkoutForms.billingAddress
      ),
    })
  }

  handleAddressPreviewChange() {
    this.props.setManualAddressMode()
    this.setState({ showAddressPreview: false })
  }

  renderBillingAddressPreviewOrForm() {
    const { checkoutForms, isMobileBreakpoint } = this.props
    const { showAddressPreview } = this.state

    return showAddressPreview ? (
      <div className="PaymentDetails-wrapper">
        <AddressPreview
          address={pluck('value', checkoutForms.billingAddress.fields)}
          details={pluck('value', checkoutForms.billingDetails.fields)}
          onClickChangeButton={() => this.handleAddressPreviewChange()}
          heading
        />
      </div>
    ) : (
      <div className="PaymentDetails-wrapper">
        <BillingDetailsForm renderTelephone={isMobileBreakpoint} />
        <BillingAddressForm renderTelephone={!isMobileBreakpoint} />
      </div>
    )
  }

  renderPaymentMethodsOrPreview() {
    const {
      combinedPaymentMethods,
      storedPaymentDetails,
      storedPaymentMethod,
      storedCombinedPaymentMethod,
      selectedCombinedPaymentMethod,
      isMobile,
    } = this.props
    const { l } = this.context

    return this.state.showPaymentPreview && storedCombinedPaymentMethod ? (
      <PaymentMethodPreview
        onChange={this.handlePaymentMethodPreviewChange}
        type={storedCombinedPaymentMethod.type}
        value={storedCombinedPaymentMethod.value}
        storedPaymentMethod={storedPaymentMethod}
      >
        {storedCombinedPaymentMethod.type === 'OTHER' ? (
          <div>
            <p>{l(storedCombinedPaymentMethod.label)}</p>
            <p>{this.loginNotice(storedCombinedPaymentMethod.label)}</p>
          </div>
        ) : (
          <div>
            <p className="PaymentDetails-preview">
              {l`Card Number`}: {storedPaymentDetails.cardNumberStar}
            </p>
            <p className="PaymentDetails-preview">
              {l`Expiry Date`}: {storedPaymentDetails.expiryMonth}/{
                storedPaymentDetails.expiryYear
              }
            </p>
          </div>
        )}
      </PaymentMethodPreview>
    ) : (
      combinedPaymentMethods.map((paymentMethod) => {
        const { label, description, value, icons } = paymentMethod
        const isSelected = paymentMethod === selectedCombinedPaymentMethod

        return (
          <div className="PaymentDetails-method" key={value}>
            <PaymentMethodOption
              label={l(label)}
              value={value}
              description={isMobile && !isSelected ? '' : l(description)}
              icons={isMobile ? [] : icons}
              isChecked={isSelected}
              onChange={this.handlePaymentMethodChange}
            />
            {isSelected && this.renderPaymentMethod(value, { label: l(label) })}
          </div>
        )
      })
    )
  }

  render() {
    const { inDeliveryAndPayment, combinedPaymentMethods } = this.props

    return (
      <div className="PaymentDetails">
        <CheckoutTitle separator={inDeliveryAndPayment}>
          {this.context.l`Payment details`}
        </CheckoutTitle>
        {inDeliveryAndPayment && this.renderBillingAddressPreviewOrForm()}
        {!!combinedPaymentMethods.length &&
          this.renderPaymentMethodsOrPreview()}
      </div>
    )
  }
}
