import PropTypes from 'prop-types'
import React from 'react'
import CheckoutTitle from '../CheckoutTitle'

// components
import Input from '../../../../common/FormComponents/Input/Input'

const fieldType = PropTypes.shape({
  value: PropTypes.string,
  isTouched: PropTypes.bool,
})

const DeliveryInstructions = (props, { l }) => {
  const {
    deliveryInstructionsField,
    smsMobileNumberField,
    deliveryInstructionsError,
    smsMobileNumberError,
    maxDeliveryInstructionsCharacters,
    validationSchema,
    setAndValidateFormField,
    touchFormField,
  } = props
  // NOTE: need these, as the translations expect the variables to have specific names
  const maxCharacters = maxDeliveryInstructionsCharacters
  const remaining =
    maxDeliveryInstructionsCharacters - deliveryInstructionsField.value.length
  // handlers
  const setAndValidateField = (fieldName) => ({ target: { value } }) => {
    setAndValidateFormField(
      'deliveryInstructions',
      fieldName,
      value,
      validationSchema[fieldName]
    )
  }
  const touchField = (fieldName) => () =>
    touchFormField('deliveryInstructions', fieldName)

  return (
    <section className="DeliveryInstructions">
      <form className="DeliveryInstructions-form">
        <div className="DeliveryInstructions-row">
          <div className="DeliveryInstructions-left left-col-margin">
            <CheckoutTitle>{l`Delivery Instructions`}</CheckoutTitle>
            <p className="DeliveryInstructions-subTitle">
              {l`Let us know where we can leave your order if you are not in.`}
            </p>
            <Input
              field={deliveryInstructionsField}
              name="deliveryInstructions"
              label={l`Delivery Instructions`}
              placeholder={l`Max. ${maxCharacters} characters`}
              setField={setAndValidateField}
              touchedField={touchField}
              maxLength={maxDeliveryInstructionsCharacters}
              errors={{ deliveryInstructions: deliveryInstructionsError }}
            />
            <span className="DeliveryInstructions-charsRemaining">
              {l`${remaining} characters remaining`}
            </span>
          </div>
          <div className="DeliveryInstructions-right right-col-margin" />
        </div>
        <div className="DeliveryInstructions-row">
          <div className="DeliveryInstructions-left left-col-margin">
            <h3>{l`Mobile Number`}</h3>
            <p className="DeliveryInstructions-paragraph">
              {l`Enter your mobile number to receive SMS updates on your delivery.`}
            </p>
            <Input
              field={smsMobileNumberField}
              name="smsMobileNumber"
              label={l`Mobile Number`}
              placeholder={l`07123 123123`}
              setField={setAndValidateField}
              touchedField={touchField}
              type="tel"
              errors={{ smsMobileNumber: smsMobileNumberError }}
            />
          </div>
          <div className="DeliveryInstructions-right right-col-margin" />
        </div>
      </form>
    </section>
  )
}

DeliveryInstructions.propTypes = {
  deliveryInstructionsField: fieldType,
  smsMobileNumberField: fieldType,
  deliveryInstructionsError: PropTypes.string,
  smsMobileNumberError: PropTypes.string,
  maxDeliveryInstructionsCharacters: PropTypes.number,
  setAndValidateFormField: PropTypes.func.isRequired,
  touchFormField: PropTypes.func.isRequired,
}

DeliveryInstructions.defaultProps = {
  deliveryInstructionsField: {
    value: '',
    isTouched: false,
  },
  smsMobileNumberField: {
    value: '',
    isTouched: false,
  },
  deliveryInstructionsError: '',
  smsMobileNumberError: '',
  maxDeliveryInstructionsCharacters: 30,
}

DeliveryInstructions.contextTypes = {
  l: PropTypes.func,
}

export default DeliveryInstructions
