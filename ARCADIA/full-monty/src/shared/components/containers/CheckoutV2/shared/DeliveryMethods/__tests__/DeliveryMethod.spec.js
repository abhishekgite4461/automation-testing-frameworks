import testComponentHelper from 'test/unit/helpers/test-component'

import DeliveryMethod from '../DeliveryMethod'

describe('<DeliveryMethod />', () => {
  const renderComponent = testComponentHelper(DeliveryMethod)

  describe('@render', () => {
    it('should render default state', () => {
      const component = renderComponent({
        deliveryType: 'HOME_STANDARD',
        label: 'UK Standard up to 4 days',
      })
      expect(component.getTree()).toMatchSnapshot()
    })

    it('should display a price of ‘free’ if `STORE_STANDARD`', () => {
      const component = renderComponent({
        deliveryType: 'STORE_STANDARD',
        label: 'Collect From Store Standard',
      })
      expect(component.wrapper.find('.DeliveryMethod-price').text()).toBe(
        'Free'
      )
    })

    it('should display the price if not `STORE_STANDARD`', () => {
      const component = renderComponent({
        deliveryType: 'HOME_STANDARD',
        label: 'UK Standard up to 4 days',
        cost: '4.00',
      })
      expect(component.wrapper.find('.DeliveryMethod-price').text()).toBe(
        '£4.00'
      )
    })

    it('should display a price of ‘free’ if not `STORE_STANDARD` and cost is an empty string', () => {
      const component = renderComponent({
        deliveryType: 'HOME_STANDARD',
        label: 'UK Standard up to 4 days',
        cost: '',
      })
      expect(component.wrapper.find('.DeliveryMethod-price').text()).toBe(
        'Free'
      )
    })

    it('should display description if supplied', () => {
      const component = renderComponent({
        deliveryType: 'HOME_STANDARD',
        label: 'UK Standard up to 4 days',
        additionalDescription: 'Up to 4 days',
      })
      expect(component.wrapper.find('.DeliveryMethod-description').text()).toBe(
        'Up to 4 days'
      )
    })

    it('should add the correct css modifier when "disabled" = true', () => {
      const component = renderComponent({
        deliveryType: 'HOME_STANDARD',
        label: 'UK Standard up to 4 days',
        additionalDescription: 'Up to 4 days',
        disabled: true,
      })
      expect(component.wrapper.find('.DeliveryMethod--disabled')).toHaveLength(
        1
      )
    })
  })

  describe('@events', () => {
    it('should call `onChange` on change', () => {
      const onChangeMock = jest.fn()
      const component = renderComponent({
        deliveryType: 'HOME_STANDARD',
        label: 'UK Standard up to 4 days',
        onChange: onChangeMock,
      })
      component.wrapper.find('RadioButton').prop('onChange')()
      expect(onChangeMock).toHaveBeenCalled()
    })
  })
})
