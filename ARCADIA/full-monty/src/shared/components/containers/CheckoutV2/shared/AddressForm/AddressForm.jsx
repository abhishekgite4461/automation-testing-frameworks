import PropTypes from 'prop-types'
import React from 'react'

import FindAddress from './FindAddress'
import YourAddress from './YourAddress'

const AddressForm = ({
  addressType,
  titleHidden,
  onSelectCountry,
  renderTelephone,
}) => {
  return (
    <section>
      <FindAddress
        addressType={addressType}
        onSelectCountry={onSelectCountry}
        titleHidden={titleHidden}
        renderTelephone={renderTelephone}
      />
      <YourAddress addressType={addressType} />
    </section>
  )
}

AddressForm.propTypes = {
  addressType: PropTypes.oneOf(['delivery', 'billing']).isRequired,
  titleHidden: PropTypes.bool,
  onSelectCountry: PropTypes.func,
}

AddressForm.defaultProps = {
  titleHidden: false,
  onSelectCountry: () => {},
}

export default AddressForm
