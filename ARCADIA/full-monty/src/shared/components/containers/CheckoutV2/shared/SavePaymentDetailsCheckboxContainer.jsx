import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import SavePaymentDetailsCheckbox from './SavePaymentDetailsCheckbox'
import { setSavePaymentDetailsEnabled } from '../../../../actions/common/checkoutActions'
import { isFeatureSavePaymentDetailsEnabled } from '../../../../selectors/featureSelectors'
import { isSavePaymentDetailsEnabled } from '../../../../selectors/checkoutSelectors'

const mapStateToProps = (state) => ({
  featureSavePaymentDetailsEnabled: isFeatureSavePaymentDetailsEnabled(state),
  savePaymentDetailsEnabled: isSavePaymentDetailsEnabled(state),
})

const mapDispatchToProps = {
  onSavePaymentDetailsChange: setSavePaymentDetailsEnabled,
}

@connect(mapStateToProps, mapDispatchToProps)
class SavePaymentDetailsCheckboxContainer extends Component {
  static propTypes = {
    onSavePaymentDetailsChange: PropTypes.func.isRequired,
    savePaymentDetailsEnabled: PropTypes.bool.isRequired,
  }
  static contextTypes = {
    l: PropTypes.func,
  }

  render() {
    const {
      featureSavePaymentDetailsEnabled,
      onSavePaymentDetailsChange,
      savePaymentDetailsEnabled,
    } = this.props
    return featureSavePaymentDetailsEnabled ? (
      <SavePaymentDetailsCheckbox
        onSavePaymentDetailsChange={onSavePaymentDetailsChange}
        savePaymentDetailsEnabled={savePaymentDetailsEnabled}
      />
    ) : null
  }
}

export default SavePaymentDetailsCheckboxContainer
