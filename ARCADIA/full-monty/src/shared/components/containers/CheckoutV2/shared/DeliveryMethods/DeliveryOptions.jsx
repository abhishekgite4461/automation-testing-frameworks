import PropTypes from 'prop-types'
import React from 'react'

import { isDeliveryOptionSelected } from '../../../../../testers/checkoutTesters'

// components
import Select from '../../../../common/FormComponents/Select/Select'

const normaliseDeliveryOptions = (deliveryOptions, p) => {
  return deliveryOptions.map(({ shipModeId, dayText, dateText, price }) => {
    return {
      value: shipModeId,
      label: `${dayText}, ${dateText}: ${p(price)}`,
    }
  })
}

const DeliveryOptions = (
  { deliveryType, deliveryCost, deliveryOptions, onChange },
  { p }
) => {
  const selectedDeliveryOption = deliveryOptions.find(isDeliveryOptionSelected)

  return deliveryOptions.length ? (
    <Select
      name={deliveryType.toLowerCase()}
      value={
        selectedDeliveryOption
          ? selectedDeliveryOption.shipModeId.toString()
          : ''
      }
      options={normaliseDeliveryOptions(deliveryOptions, p, deliveryCost)}
      onChange={({ target }) => onChange(target.value)}
    />
  ) : null
}

DeliveryOptions.propTypes = {
  deliveryType: PropTypes.string.isRequired,
  deliveryCost: PropTypes.string,
  deliveryOptions: PropTypes.arrayOf(
    PropTypes.shape({
      shipModeId: PropTypes.number,
      dayText: PropTypes.string,
      dateText: PropTypes.string,
      price: PropTypes.string,
      selected: PropTypes.bool,
    })
  ),
  onChange: PropTypes.func,
}

DeliveryOptions.defaultProps = {
  deliveryOptions: [],
  onChange: () => {},
}

DeliveryOptions.contextTypes = {
  p: PropTypes.func,
}

export default DeliveryOptions
