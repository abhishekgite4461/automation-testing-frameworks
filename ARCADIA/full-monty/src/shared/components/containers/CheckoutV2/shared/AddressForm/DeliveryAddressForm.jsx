import React, { PureComponent } from 'react'
import { connect } from 'react-redux'

import { selectDeliveryCountry } from '../../../../../actions/common/checkoutActions'

import AddressForm from './AddressForm'

class DeliveryAddressForm extends PureComponent {
  render() {
    return (
      <AddressForm
        addressType="delivery"
        onSelectCountry={this.props.selectDeliveryCountry}
        renderTelephone={this.props.renderTelephone}
      />
    )
  }
}

const mapDispatchToProps = {
  selectDeliveryCountry,
}

export default connect(null, mapDispatchToProps)(DeliveryAddressForm)

export { DeliveryAddressForm as WrappedDeliveryAddressForm, mapDispatchToProps }
