import testComponentHelper from 'test/unit/helpers/test-component'
import CheckoutProgressTracker from './CheckoutProgressTracker'

describe('<CheckoutProgressTracker />', () => {
  const renderComponent = testComponentHelper(
    CheckoutProgressTracker.WrappedComponent
  )

  describe('@renders', () => {
    it('in default state', () => {
      const props = {
        location: {
          pathname: '/checkout/payment',
        },
      }
      expect(renderComponent(props).getTree()).toMatchSnapshot()
    })
    it('in default state for the /checkout/delivery-payment', () => {
      const props = {
        location: {
          pathname: '/checkout/delivery-payment',
        },
      }
      expect(renderComponent(props).getTree()).toMatchSnapshot()
    })
  })
})
