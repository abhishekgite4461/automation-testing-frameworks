import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { KLARNA } from '../../../../constants/paymentTypes'
import { eventBasedAnalytics } from '../../../../lib/analytics/analytics'
import { formatForRadix } from '../../../../lib/price'
import { isEmpty } from 'ramda'

// selectors
import {
  getPaymentType,
  getKlarnaAuthToken,
  getTotal,
} from '../../../../selectors/checkoutSelectors'

// actions
import { submitOrder } from '../../../../actions/common/orderActions'
import { validateForms } from '../../../../actions/common/checkoutActions'

// components
import Button from '../../../common/Button/Button'

const buttonText = {
  PYPAL: 'Pay via PayPal',
  KLRNA: 'Pay via Klarna',
  default: 'Order and Pay Now',
}

const mapStateToProps = (state) => ({
  paymentType: getPaymentType(state) || '',
  klarnaAuthToken: getKlarnaAuthToken(state),
  total: getTotal(state),
})

const mapDispatchToProps = {
  submitOrder,
  validateForms,
}

@connect(mapStateToProps, mapDispatchToProps)
export default class PaymentButtonContainer extends Component {
  static propTypes = {
    className: PropTypes.string,
    formNames: PropTypes.arrayOf(PropTypes.string).isRequired,
    formErrors: PropTypes.object,
    klarnaAuthToken: PropTypes.string,
    paymentType: PropTypes.string,
    sendEventAnalytics: PropTypes.func,
    submitOrder: PropTypes.func,
    total: PropTypes.string,
    validateForms: PropTypes.func,
  }

  static defaultProps = {
    className: '',
    sendEventAnalytics: eventBasedAnalytics,
    formErrors: {},
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  handleSubmit = () => {
    const { formNames, submitOrder, validateForms } = this.props

    return validateForms(formNames, {
      onValid: () => {
        return submitOrder().then((result) => {
          if (result instanceof Error) {
            this.props.sendEventAnalytics({
              eVar72: result.message,
            })
          }
        })
      },
    })
  }

  render() {
    const { l } = this.context
    const {
      className,
      paymentType,
      klarnaAuthToken,
      total,
      formErrors,
    } = this.props

    // There is no point showing the payment specific text if there is no
    // outstanding amount left to pay on the order. So we ensure that we only
    // show the bespoke text if the order total is not equal to "0.00"
    const buttonLabel =
      formatForRadix(total) !== 0 && buttonText[paymentType]
        ? buttonText[paymentType]
        : buttonText.default

    if (paymentType === KLARNA && !klarnaAuthToken) {
      return null
    }

    return (
      <Button
        className={className}
        type="submit"
        clickHandler={this.handleSubmit}
        isActive={isEmpty(formErrors)}
      >
        {/*
            NOTE: Translations don't work with template strings when the text
            being translated is within a variable. So instead of doing l`${buttonLabel}`,
            we have to do l(buttonLabel)
          */}
        {l(buttonLabel)}
      </Button>
    )
  }
}
