import testComponentHelper, {
  mockLocalise,
} from 'test/unit/helpers/test-component'
import generateMockFormProps from 'test/unit/helpers/form-props-helper'
import YourAddress from '../YourAddress'
import {
  initialYourAddressProps as initialProps,
  initialYourAddressFormProps as initialFormProps,
  populatedYourAddressFormProps as populatedFormProps,
} from '../../__tests__/sharedMocks'

describe('<YourAddress />', () => {
  const mountOptions = {
    contextTypes: {
      l: jest.fn(mockLocalise),
    },
  }

  const renderComponent = testComponentHelper(
    YourAddress.WrappedComponent,
    mountOptions
  )
  const generateProps = (newFormProps) => ({
    ...initialProps,
    ...generateMockFormProps(initialFormProps, newFormProps),
    country: 'United Kingdom',
    formName: 'yourAddress',
    findAddressFormName: 'findAddress',
    setFormField: jest.fn(() => () => {}),
    setAndValidateFormField: jest.fn(() => () => () => {}),
    validateForm: jest.fn(),
    touchFormField: jest.fn(),
    clearFormErrors: jest.fn(),
    resetFindAddress: () => {},
  })

  describe('@renders', () => {
    describe('in default state', () => {
      it('with "default" country', () => {
        expect(
          renderComponent(
            generateProps({
              country: 'default',
            })
          ).getTree()
        ).toMatchSnapshot()
      })

      describe('with state field eligible selected country', () => {
        it('renders state Input field ', () => {
          expect(
            renderComponent(
              generateProps({
                country: 'Trinidad & Tobago',
              })
            ).getTree()
          ).toMatchSnapshot()
        })
        it('renders state Select field ', () => {
          expect(
            renderComponent(
              generateProps({
                country: 'United States',
              })
            ).getTree()
          ).toMatchSnapshot()
        })
      })
    })
  })

  describe('@lifecycle', () => {
    describe('on ComponentDidMount', () => {
      it('should validate your address form', () => {
        const validationSchema = {}
        const validateFormMock = jest.fn()
        const { instance } = renderComponent({
          ...generateProps(),
          formName: 'yourAddress',
          validationSchema,
          validateForm: validateFormMock,
        })
        instance.componentDidMount()

        expect(validateFormMock).toHaveBeenCalledWith(
          'yourAddress',
          validationSchema
        )
      })
    })

    describe('on componentDidUpdate', () => {
      it('should validate your address form if schema has changed', () => {
        const validationSchema = {}
        const validateFormMock = jest.fn()
        const { instance } = renderComponent({
          ...generateProps(),
          formName: 'yourAddress',
          validationSchema: {},
          schemaHash: '',
          validateForm: validateFormMock,
        })
        instance.componentDidUpdate({ schemaHash: {} })

        expect(validateFormMock).toHaveBeenCalledWith(
          'yourAddress',
          validationSchema
        )
      })
    })

    describe('on componentWillUnmount', () => {
      it('should clear validation errors', () => {
        const clearFormErrorsMock = jest.fn()
        const { instance } = renderComponent({
          ...generateProps(),
          formName: 'yourAddress',
          clearFormErrors: clearFormErrorsMock,
        })
        instance.componentWillUnmount()

        expect(clearFormErrorsMock).toHaveBeenCalledWith('yourAddress')
      })
    })
  })

  describe('@events', () => {
    describe('On "Clear Form" button click', () => {
      it('should reset forms', () => {
        const resetFormMock = jest.fn()
        const { wrapper } = renderComponent({
          ...generateProps(populatedFormProps),
          resetForm: resetFormMock,
        })
        wrapper
          .find('.YourAddress-link .YourAddress-link--left')
          .prop('onClick')()

        expect(resetFormMock).toHaveBeenCalledWith('yourAddress', {
          address1: '',
          address2: '',
          city: '',
          country: 'United Kingdom',
          county: '',
          postcode: '',
          state: '',
        })
        expect(resetFormMock).toHaveBeenCalledWith('findAddress', {
          findAddress: '',
          houseNumber: '',
          postCode: '',
        })
      })

      it('should set manual address mode', () => {
        const setManualAddressModeMock = jest.fn()
        const { wrapper } = renderComponent({
          ...generateProps(populatedFormProps),
          setManualAddressMode: setManualAddressModeMock,
        })
        wrapper
          .find('.YourAddress-link .YourAddress-link--left')
          .prop('onClick')()

        expect(setManualAddressModeMock).toHaveBeenCalled()
      })

      it('should validate address form', () => {
        const validationSchema = {}
        const validateFormMock = jest.fn()
        const { wrapper } = renderComponent({
          ...generateProps(populatedFormProps),
          validateForm: validateFormMock,
          validationSchema,
        })
        wrapper
          .find('.YourAddress-link .YourAddress-link--left')
          .prop('onClick')()

        expect(validateFormMock).toHaveBeenCalledWith(
          'yourAddress',
          validationSchema
        )
      })
    })

    describe('On "Find Address" button click', () => {
      it('should reset form to display Find Address', () => {
        const resetFindAddressMock = jest.fn()
        const { wrapper } = renderComponent({
          ...generateProps(populatedFormProps),
          canFindAddress: true,
          resetFindAddress: resetFindAddressMock,
        })
        wrapper
          .find('.YourAddress-link .YourAddress-link--right')
          .prop('onClick')()

        expect(resetFindAddressMock).toHaveBeenCalled()
      })
    })

    describe('First Line of Address', () => {
      it('should send analytics `event136` event when focus leaves', () => {
        const sendEventAnalyticsMock = jest.fn()
        const { wrapper } = renderComponent({
          ...generateProps(populatedFormProps),
          touchedFormField: () => () => () => {},
          sendEventAnalytics: sendEventAnalyticsMock,
        })
        wrapper.find('.YourAddress-address1').prop('touchedField')('address1')()
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event136',
        })
      })
    })

    describe('Postcode', () => {
      it('should send analytics `event137` event when focus leaves', () => {
        const sendEventAnalyticsMock = jest.fn()
        const { wrapper } = renderComponent({
          ...generateProps(populatedFormProps),
          touchedFormField: () => () => () => {},
          sendEventAnalytics: sendEventAnalyticsMock,
        })
        wrapper.find('.YourAddress-postcode').prop('touchedField')('postcode')()
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event137',
        })
      })

      it('should updated with trimmed postcode on blur', () => {
        const setAndValidateFormFieldMock = jest.fn()
        const { wrapper } = renderComponent({
          ...generateProps(populatedFormProps),
          setAndValidateFormField: setAndValidateFormFieldMock,
          validationSchema: { postcode: 'postcodeValidation' },
        })
        wrapper
          .find('.YourAddress-postcode')
          .simulate('blur', { target: { value: '  abc  ' } })
        expect(setAndValidateFormFieldMock).toHaveBeenCalledWith(
          'yourAddress',
          'postcode',
          'abc',
          'postcodeValidation'
        )
      })
    })
  })
})
