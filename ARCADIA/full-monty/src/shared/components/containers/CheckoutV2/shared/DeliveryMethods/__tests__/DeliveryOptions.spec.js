import testComponentHelper from 'test/unit/helpers/test-component'

import DeliveryOptions from '../DeliveryOptions'
import Select from '../../../../../common/FormComponents/Select/Select'

describe('<DeliveryOptions />', () => {
  const renderComponent = testComponentHelper(DeliveryOptions)

  const createDeliveryOptions = ({ selectedShipModeId } = {}) => [
    {
      shipModeId: 28005,
      dayText: 'Thu',
      dateText: '07 Sep',
      price: '6.00',
      selected: selectedShipModeId === 28005,
    },
    {
      shipModeId: 28006,
      dayText: 'Fri',
      dateText: '08 Sep',
      price: '6.00',
      selected: selectedShipModeId === 28006,
    },
  ]

  describe('@render', () => {
    it('should render default state', () => {
      const component = renderComponent({
        deliveryType: 'HOME_STANDARD',
      })
      expect(component.getTree()).toMatchSnapshot()
    })

    it('should pass options to `Select` component', () => {
      const component = renderComponent({
        deliveryType: 'HOME_STANDARD',
        deliveryOptions: createDeliveryOptions({ selectedShipModeId: 28005 }),
      })
      expect(component.getTree()).toMatchSnapshot()
    })

    it('should pass value to `Select` as an empty string if no option selected', () => {
      const component = renderComponent({
        deliveryType: 'HOME_STANDARD',
        deliveryOptions: createDeliveryOptions(),
      })
      expect(component.wrapper.find(Select).prop('value')).toBe('')
    })

    it('should handle Free Deliver', () => {
      const deliveryOptions = createDeliveryOptions()
      const component = renderComponent({
        deliveryType: 'HOME_STANDARD',
        deliveryCost: '0.00',
        deliveryOptions: deliveryOptions.map((option) => ({
          ...option,
          price: '0.00',
        })),
      })
      component.wrapper
        .find(Select)
        .prop('options')
        .forEach((option, index) => {
          const propOption = deliveryOptions[index]
          expect(option).toEqual({
            value: propOption.shipModeId,
            label: `${propOption.dayText}, ${propOption.dateText}: £0.00`,
          })
        })
    })
  })

  describe('@events', () => {
    it('should call `onChange` with target‘s value on change', () => {
      const onChangeMock = jest.fn()
      const component = renderComponent({
        deliveryType: 'HOME_STANDARD',
        deliveryOptions: createDeliveryOptions(),
        onChange: onChangeMock,
      })
      component.wrapper.find(Select).prop('onChange')({
        target: { value: '28005' },
      })
      expect(onChangeMock).toHaveBeenCalledWith('28005')
    })
  })
})
