import PropTypes from 'prop-types'
import { path, pathOr } from 'ramda'
import React, { Component } from 'react'
import { connect } from 'react-redux'

// selectors
import {
  findAddressIsVisible,
  getAddressForm,
  getCountryFor,
  getDetailsForm,
  getFindAddressForm,
} from '../../../../../selectors/checkoutSelectors'

import {
  getFindAddressSchema,
  getYourDetailsSchema,
} from '../validationSchemas'

import { eventBasedAnalytics } from '../../../../../lib/analytics/analytics'
import { getFormNames } from '../../../../../lib/checkout-utilities/utils'

// actions
import {
  setFormField,
  setAndValidateFormField,
  validateForm,
  clearFormErrors,
  clearFormFieldError,
  touchedFormField,
} from '../../../../../actions/common/formActions'
import {
  findAddress,
  getAddressByMoniker,
  setManualAddressMode,
} from '../../../../../actions/common/checkoutActions'
import { validateDDPForCountry } from '../../../../../actions/common/ddpActions'

// components
import Input from '../../../../common/FormComponents/Input/Input'
import Button from '../../../../common/FormComponents/Button/Button'
import Select from '../../../../common/FormComponents/Select/Select'
import Message from '../../../../common/FormComponents/Message/Message'

const mapStateToProps = (state, { addressType }) => {
  const formNames = getFormNames(addressType)
  const addressForm = getAddressForm(addressType, state)
  const country =
    addressForm.fields.country.value || getCountryFor(addressType, state)
  const rules = pathOr(
    {},
    ['config', 'checkoutAddressFormRules', country],
    state
  )
  const hasFoundAddress = state.findAddress.monikers.length > 0
  const hasSelectedAddress = !!path(
    ['fields', 'address1', 'value'],
    addressForm
  )
  const isFindAddressVisible = findAddressIsVisible(addressType, state)

  return {
    country,
    countryCode: state.config.qasCountries[country],
    rules,
    monikers: state.findAddress.monikers,
    formNames,
    findAddressForm: getFindAddressForm(addressType, state),
    addressForm,
    detailsForm: getDetailsForm(addressType, state),
    isFindAddressVisible,
    findAddressValidationSchema: isFindAddressVisible
      ? getFindAddressSchema(rules, { hasFoundAddress, hasSelectedAddress })
      : {},
    detailsValidationSchema: getYourDetailsSchema(country),
    countries:
      addressType === 'billing'
        ? state.siteOptions.billingCountries
        : state.siteOptions.deliveryCountries,
    // NOTE: rather hacky way of determining whether the schemas have updated (as can't do a `===` on them)
    // think about using something like https://github.com/reactjs/reselect
    schemaHash: [
      country,
      isFindAddressVisible,
      hasFoundAddress,
      hasSelectedAddress,
    ].join(':'),
  }
}

const mapDispatchToProps = {
  findAddress,
  getAddressByMoniker,
  setManualAddressMode,
  setFormField,
  setAndValidateFormField,
  touchFormField: touchedFormField,
  validateForm,
  clearFormErrors,
  clearFormFieldError,
  validateDDPForCountry,
}

const setTrimmedField = (func) => ({ target: { value } }) => {
  func({ target: { value: value.trim() } })
}

class FindAddress extends Component {
  static propTypes = {
    titleHidden: PropTypes.bool,
    events: PropTypes.objectOf(PropTypes.string),
    country: PropTypes.string,
    countryCode: PropTypes.string,
    rules: PropTypes.object,
    monikers: PropTypes.array,
    formNames: PropTypes.shape({
      findAddress: PropTypes.string,
      delivery: PropTypes.string,
      billing: PropTypes.string,
    }).isRequired,
    findAddressForm: PropTypes.object.isRequired,
    addressForm: PropTypes.object.isRequired,
    detailsForm: PropTypes.object.isRequired,
    isFindAddressVisible: PropTypes.bool,
    findAddressValidationSchema: PropTypes.object,
    detailsValidationSchema: PropTypes.object,
    countries: PropTypes.array,
    schemaHash: PropTypes.string,
    // functions
    onSelectCountry: PropTypes.func,
    sendEventAnalytics: PropTypes.func,
    findAddress: PropTypes.func.isRequired,
    getAddressByMoniker: PropTypes.func.isRequired,
    setManualAddressMode: PropTypes.func.isRequired,
    setFormField: PropTypes.func.isRequired,
    setAndValidateFormField: PropTypes.func.isRequired,
    touchFormField: PropTypes.func.isRequired,
    validateForm: PropTypes.func.isRequired,
    clearFormErrors: PropTypes.func.isRequired,
    clearFormFieldError: PropTypes.func.isRequired,
    validateDDPForCountry: PropTypes.func.isRequired,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  static defaultProps = {
    titleHidden: false,
    events: {
      findAddress: 'event121',
      manualAddress: 'event122',
      country: 'event135',
      postCode: 'event137',
    },
    country: '',
    countryCode: '',
    rules: {},
    monikers: [],
    isFindAddressVisible: true,
    findAddressValidationSchema: {},
    detailsValidationSchema: {},
    countries: [],
    schemaHash: '',
    sendEventAnalytics: eventBasedAnalytics,
    onSelectCountry: () => {},
    validateDDPForCountry: () => {},
  }
  constructor(props) {
    super(props)
    this.onBlurPostcode = setTrimmedField(
      this.setAndValidateFindAddressField('postCode')
    )
  }

  componentDidMount() {
    const { formNames, country, setFormField } = this.props
    setFormField(formNames.address, 'country', country)
    this.validateForms()
  }

  componentDidUpdate(prevProps) {
    if (prevProps.schemaHash !== this.props.schemaHash) {
      this.validateForms()
    }
  }

  componentWillUnmount() {
    this.clearErrors()
  }

  setCountry = (ev) => {
    const {
      formNames,
      events,
      onSelectCountry,
      setFormField,
      sendEventAnalytics,
      addressType,
      validateDDPForCountry,
    } = this.props
    const {
      target: { value: country },
    } = ev

    if (addressType === 'delivery') validateDDPForCountry(country)
    onSelectCountry(country)
    setFormField(formNames.address, 'country', country)
    sendEventAnalytics({
      events: events.country,
    })
  }

  setAndValidateFindAddressField = (fieldName) => ({ target: { value } }) => {
    const {
      formNames,
      findAddressValidationSchema,
      setAndValidateFormField,
    } = this.props
    return setAndValidateFormField(
      formNames.findAddress,
      fieldName,
      value,
      findAddressValidationSchema[fieldName]
    )
  }

  setAndValidateDetailsField = (fieldName) => ({ target: { value } }) => {
    const {
      formNames,
      detailsValidationSchema,
      setAndValidateFormField,
    } = this.props
    return setAndValidateFormField(
      formNames.details,
      fieldName,
      value,
      detailsValidationSchema[fieldName]
    )
  }

  validateForms() {
    const {
      formNames,
      findAddressValidationSchema,
      detailsValidationSchema,
      validateForm,
    } = this.props
    validateForm(formNames.findAddress, findAddressValidationSchema)
    validateForm(formNames.details, detailsValidationSchema)
  }

  touchFindAddressField = (fieldName) => () => {
    const { formNames, events, touchFormField, sendEventAnalytics } = this.props
    touchFormField(formNames.findAddress, fieldName)
    if (fieldName in events) {
      sendEventAnalytics({
        events: events[fieldName],
      })
    }
  }

  touchDetailsField = (fieldName) => () => {
    const { formNames, touchFormField } = this.props
    return touchFormField(formNames.details, fieldName)
  }

  clearErrors() {
    const { formNames, clearFormErrors } = this.props
    clearFormErrors(formNames.findAddress)
    clearFormErrors(formNames.details)
  }

  handleFindAddressRequest = () => {
    const {
      formNames,
      events,
      countryCode,
      findAddressForm,
      clearFormFieldError,
      findAddress,
      sendEventAnalytics,
    } = this.props

    sendEventAnalytics({
      events: events.findAddress,
    })
    findAddress(
      {
        country: countryCode,
        postcode: findAddressForm.fields.postCode.value,
        address: findAddressForm.fields.houseNumber.value,
      },
      formNames.address,
      formNames.findAddress
    ).then(() => {
      clearFormFieldError(formNames.findAddress, 'findAddress')
    })
  }

  handleAddressChange = ({ target: { selectedIndex } }) => {
    const { countryCode, formNames, monikers, getAddressByMoniker } = this.props

    getAddressByMoniker(
      {
        country: countryCode,
        moniker: monikers[selectedIndex - 1].moniker,
      },
      formNames.address
    )
  }

  handleSwitchToManualAddress = () => {
    const { events, setManualAddressMode, sendEventAnalytics } = this.props
    setManualAddressMode()
    sendEventAnalytics({
      events: events.manualAddress,
    })
  }

  render() {
    const { l } = this.context
    const {
      addressType,
      country,
      countries,
      rules,
      findAddressForm,
      addressForm,
      detailsForm,
      monikers,
      titleHidden,
      isFindAddressVisible,
      renderTelephone,
    } = this.props

    return (
      <section className="FindAddress" aria-label="Find Address">
        {!titleHidden && <h3 className="FindAddress-heading">{l`Address`}</h3>}
        <div className="FindAddress-row">
          {renderTelephone && (
            <Input
              className="FindAddress-telephone left-col-margin"
              isDisabled={detailsForm.success}
              field={pathOr({}, ['fields', 'telephone'], detailsForm)}
              name="telephone"
              type="tel"
              errors={detailsForm.errors}
              label={l`Primary Phone Number`}
              placeholder={l`07123 123123`}
              setField={this.setAndValidateDetailsField}
              touchedField={this.touchDetailsField}
              isRequired
            />
          )}
          <Select
            className="FindAddress-country right-col-margin"
            label={
              addressType === 'billing'
                ? l`Billing country`
                : l`Delivery country`
            }
            field={addressForm.fields.country}
            options={[
              {
                label: l`Please select your country`,
                value: '',
                disabled: true,
              },
              ...countries.map((country) => ({
                label: country,
                value: country,
              })),
            ]}
            name="country"
            errors={addressForm.errors}
            value={country}
            onChange={this.setCountry}
            isRequired
            noTranslate
          />
        </div>
        <div
          className={`FindAddress-form${
            isFindAddressVisible ? '' : ' is-hidden'
          }`}
        >
          <div className="FindAddress-row">
            <Input
              className="FindAddress-postCode left-col-margin"
              isDisabled={findAddressForm.success}
              field={pathOr({}, ['fields', 'postCode'], findAddressForm)}
              name="postCode"
              onBlur={this.onBlurPostcode}
              label={l(rules.postcodeLabel || 'Postcode')}
              errors={findAddressForm.errors}
              placeholder={l`Eg. W1T 3NL`}
              setField={this.setAndValidateFindAddressField}
              touchedField={this.touchFindAddressField}
              isRequired={rules.postcodeRequired}
            />
            <Input
              className="FindAddress-houseNumber right-col-margin"
              isDisabled={findAddressForm.success}
              field={pathOr({}, ['fields', 'houseNumber'], findAddressForm)}
              name="houseNumber"
              label={l(rules.premisesLabel || 'House number')}
              errors={findAddressForm.errors}
              placeholder={l`Eg. 214`}
              setField={this.setAndValidateFindAddressField}
              touchedField={this.touchFindAddressField}
              isRequired={rules.premisesRequired}
            />
          </div>
          <Button
            onClick={this.handleFindAddressRequest}
            className="FindAddress-button"
            name="findAddress"
            isDisabled={!!path(['errors', 'postCode'], findAddressForm)}
            error={path(['errors', 'findAddress'], findAddressForm)}
            touched={path(
              ['fields', 'findAddress', 'isTouched'],
              findAddressForm
            )}
          >
            {l`Find Address`}
          </Button>
          <Message
            message={path(['message', 'message'], findAddressForm)}
            type="error"
          />
          <Select
            label={l`Click to select your address`}
            onChange={this.handleAddressChange}
            className={`FindAddress-selectAddress${
              monikers.length > 0 ? '' : ' is-hidden'
            }`}
            name="selectAddress"
            firstDisabled={l`Please select your address...`}
            options={monikers.map(({ address, moniker }) => ({
              label: address,
              value: moniker,
            }))}
            field={path(['fields', 'selectAddress'], findAddressForm)}
            errors={findAddressForm.errors}
            isRequired
          />
          <button
            onClick={this.handleSwitchToManualAddress}
            className="FindAddress-link"
          >
            {l`Enter address manually`}
          </button>
        </div>
      </section>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FindAddress)

export {
  FindAddress as WrappedFindAddress,
  mapStateToProps,
  mapDispatchToProps,
}
