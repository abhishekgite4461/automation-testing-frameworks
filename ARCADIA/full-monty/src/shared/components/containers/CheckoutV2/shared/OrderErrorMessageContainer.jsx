import { connect } from 'react-redux'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Message from '../../../common/FormComponents/Message/Message'
import { getOrderFormErrorMessage } from '../../../../selectors/formsSelectors'

const mapStateToProps = (state) => ({
  errorMessage: getOrderFormErrorMessage(state),
})

@connect(mapStateToProps, null)
export default class OrderErrorMessageContainer extends Component {
  static propTypes = {
    errorMessage: PropTypes.string,
  }

  onMessageDidMount = (message) => message.scrollTo()

  render() {
    const { errorMessage } = this.props

    if (!errorMessage) {
      return null
    }

    return (
      <Message
        message={errorMessage}
        type="error"
        onDidMount={this.onMessageDidMount}
      />
    )
  }
}
