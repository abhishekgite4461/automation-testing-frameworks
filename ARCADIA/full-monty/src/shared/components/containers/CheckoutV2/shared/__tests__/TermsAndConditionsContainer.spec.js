import testComponentHelper from 'test/unit/helpers/test-component'
import Checkbox from '../../../../common/FormComponents/Checkbox/Checkbox'

import TermsAndConditionsContainer, {
  TermsAndConditions,
  mapStateToProps,
  mapDispatchToProps,
} from '../TermsAndConditionsContainer'

describe('<TermsAndConditionsContainer />', () => {
  describe('@mapStateToProps', () => {
    it('should pull out field from state', () => {
      const { value, isTouched } = mapStateToProps({
        forms: {
          checkout: {
            order: {
              fields: {
                isAcceptedTermsAndConditions: {
                  value: true,
                  isTouched: true,
                },
              },
            },
          },
        },
      })
      expect(value).toEqual(true)
      expect(isTouched).toEqual(true)
    })

    it('should pull out url from state', () => {
      const { url } = mapStateToProps({
        cms: {
          pages: {
            termsAndConditions: {
              seoUrl: '/path/to/t-and-c',
            },
          },
        },
      })
      expect(url).toBe('/path/to/t-and-c')
    })

    it('should pull out error from state', () => {
      const { error } = mapStateToProps({
        forms: {
          checkout: {
            order: {
              errors: {
                isAcceptedTermsAndConditions: 'This field is required',
              },
            },
          },
        },
      })
      expect(error).toBe('This field is required')
    })

    it('should return empty string for `error` if no errors', () => {
      const { error } = mapStateToProps({
        forms: {
          checkout: {
            order: {
              errors: {},
            },
          },
        },
      })
      expect(error).toBe('')
    })
  })

  describe('@events', () => {
    const renderComponent = testComponentHelper(
      TermsAndConditionsContainer.WrappedComponent
    )

    describe('componentWillUnmount', () => {
      it('should call clearError and resetForm', () => {
        const requiredProps = {
          onChange: () => {},
        }
        const clearError = jest.fn()
        const resetForm = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          clearError,
          resetForm,
        })
        wrapper.unmount()
        expect(resetForm).toHaveBeenCalledTimes(1)
        expect(clearError).toHaveBeenCalledTimes(1)
        expect(resetForm).toHaveBeenCalledWith('order', {
          isAcceptedTermsAndConditions: '',
        })
      })
    })
  })

  describe('@mapDispatchToProps', () => {
    it('should set form field to true and validate if checked', () => {
      const { onChange } = mapDispatchToProps
      const action = onChange({ target: { checked: true } })
      expect(action).toEqual({
        field: 'isAcceptedTermsAndConditions',
        formName: 'order',
        type: 'SET_AND_VALIDATE_FORM_FIELD',
        value: true,
        validators: ['required'],
      })
    })

    it('should set form field to empty string if un-checked', () => {
      const { onChange } = mapDispatchToProps
      const action = onChange({ target: { checked: false } })
      expect(action).toEqual({
        field: 'isAcceptedTermsAndConditions',
        formName: 'order',
        type: 'SET_AND_VALIDATE_FORM_FIELD',
        value: '',
        validators: ['required'],
      })
    })
  })
})

describe('<TermsAndConditions />', () => {
  const renderComponent = testComponentHelper(TermsAndConditions)

  const onChangeMock = jest.fn()

  const requiredProps = {
    onChange: onChangeMock,
  }

  afterEach(() => {
    jest.resetAllMocks()
  })

  describe('@renders', () => {
    it('should render default state', () => {
      const { getTree } = renderComponent(requiredProps)
      expect(getTree()).toMatchSnapshot()
    })

    it('should pass `value` and `isTouched` to `Checkbox` as `field` and `checked` prop', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        value: true,
        isTouched: true,
      })
      const checkbox = wrapper.find(Checkbox)
      expect(checkbox.prop('field')).toEqual({ isTouched: true })
      expect(checkbox.prop('checked')).toEqual({ value: true })
    })

    it('should pass `error` to `Checkbox`', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        error: 'This field is required',
      })
      const checkbox = wrapper.find(Checkbox)
      expect(checkbox.prop('errors')).toEqual({
        isAcceptedTermsAndConditions: 'This field is required',
      })
    })

    it('should use supplied `url`', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        url: '/path/to/t-and-c',
      })
      const link = wrapper.find('Link')
      expect(link.prop('to')).toBe('/path/to/t-and-c')
    })
  })

  describe('@events', () => {
    it('should call `onChange` on checkbox change', () => {
      const { wrapper } = renderComponent(requiredProps)
      wrapper.find(Checkbox).prop('onChange')()
      expect(onChangeMock).toHaveBeenCalled()
    })
  })
})
