import TotalCost from '../TotalCost'
import testComponentHelper from '../../../../../../../test/unit/helpers/test-component'

const props = {
  totalCost: '$100',
}

describe(TotalCost.name, () => {
  const renderComponent = testComponentHelper(TotalCost)

  describe('@renders', () => {
    it('should render the total cost', () => {
      expect(renderComponent(props).getTree()).toMatchSnapshot()
    })

    it('should render the correct value', () => {
      const { wrapper } = renderComponent({ ...props })
      expect(wrapper.find('.TotalCost').text()).toEqual('Total cost: $100')
    })
  })
})
