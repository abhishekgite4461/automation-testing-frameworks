import { clone } from 'ramda'
import PaymentButtonContainer from '../PaymentButtonContainer'
import testComponentHelper, {
  renderConnectedComponentProps,
} from 'test/unit/helpers/test-component'
import { KLARNA, PAYPAL } from '../../../../../constants/paymentTypes'
import { eventBasedAnalytics } from '../../../../../lib/analytics/analytics'
import Button from '../../../../common/Button/Button'

// actions
import { submitOrder } from '../../../../../actions/common/orderActions'
import { validateForms } from '../../../../../actions/common/checkoutActions'

jest.mock('../../../../../actions/common/orderActions', () => ({
  submitOrder: jest.fn(() => ({ type: 'foo' })),
}))

jest.mock('../../../../../actions/common/checkoutActions', () => ({
  validateForms: jest.fn(() => ({ type: 'foo' })),
}))

jest.mock('../../../../../lib/analytics/analytics', () => ({
  eventBasedAnalytics: jest.fn(),
}))

const initialProps = {
  className: 'Foo',
  klarnaAuthToken: 'auth token...',
  paymentType: 'PAY',
  formNames: ['formOne', 'formTwo'],
  submitOrder: jest.fn(() => Promise.resolve()),
  validateForms: jest.fn((formNames, { onValid }) => onValid()),
  sendEventAnalytics: jest.fn(),
}

const $ = {
  button: Button,
}

const renderComponent = testComponentHelper(
  PaymentButtonContainer.WrappedComponent
)

function propsForEligibleKlarna() {
  return {
    ...clone(initialProps),
    paymentType: KLARNA,
    klarnaAuthToken: 'some auth token',
  }
}

function renderConnectedProps(state) {
  return renderConnectedComponentProps(PaymentButtonContainer, state)
}

describe('<PaymentButtonContainer />', () => {
  beforeEach(() => jest.clearAllMocks())

  describe('@renders', () => {
    describe('when using klarna', () => {
      describe('when klarna eligibility check has not yet been performed', () => {
        const props = {
          ...initialProps,
          paymentType: KLARNA,
          klarnaAuthToken: undefined,
        }

        it('renders nothing', () => {
          const component = renderComponent(props)
          expect(component.wrapper.find($.button)).toHaveLength(0)
        })
      })

      describe('when klarna eligibility check passed', () => {
        it('renders the button', () => {
          const component = renderComponent(propsForEligibleKlarna())
          expect(component.wrapper.find($.button)).toHaveLength(1)
        })
      })
    })

    describe('the rendered button', () => {
      it('renders with the right props', () => {
        const component = renderComponent(propsForEligibleKlarna())
        const button = component.wrapper.find($.button)
        expect(component.getTreeFor(button)).toMatchSnapshot()
      })

      it('with errors next button is not active', () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          formErrors: {
            yourAddress: {
              address1: 'This field is required',
            },
          },
        })
        expect(wrapper.find('Button').prop('isActive')).toBe(false)
      })

      describe('labels', () => {
        it('should render ‘Pay via PayPal’ text in button for paypal', () => {
          const { wrapper } = renderComponent({
            ...initialProps,
            paymentType: PAYPAL,
          })
          expect(
            wrapper
              .find($.button)
              .render()
              .text()
          ).toBe('Pay via PayPal')
        })

        it('should render ‘Pay via Klarna’ text in button for klarna', () => {
          const { wrapper } = renderComponent({
            ...initialProps,
            paymentType: KLARNA,
          })
          expect(
            wrapper
              .find('Button')
              .render()
              .text()
          ).toBe('Pay via Klarna')
        })

        it('should render ‘Order and Pay Now’ text in button instead of custom text if total is "0.00"', () => {
          // PayPal
          const { wrapper: wrapperForPayPal } = renderComponent({
            ...initialProps,
            paymentType: PAYPAL,
            total: '0.00',
          })
          expect(
            wrapperForPayPal
              .find($.button)
              .render()
              .text()
          ).toBe('Order and Pay Now')

          // Klarna
          const { wrapper: wrapperForKlarna } = renderComponent({
            ...initialProps,
            paymentType: PAYPAL,
            total: '0,00',
          })
          expect(
            wrapperForKlarna
              .find($.button)
              .render()
              .text()
          ).toBe('Order and Pay Now')
        })

        it('should render bespoke text for totals that include special radix chars', () => {
          const { wrapper } = renderComponent({
            ...initialProps,
            paymentType: PAYPAL,
            total: '0,01',
          })
          expect(
            wrapper
              .find($.button)
              .render()
              .text()
          ).toBe('Pay via PayPal')
        })

        it('should render ‘Order and Pay Now’ for anything other than paypal and klarna', () => {
          const { wrapper } = renderComponent({
            ...initialProps,
            paymentType: 'FOO',
          })
          expect(
            wrapper
              .find('Button')
              .render()
              .text()
          ).toBe('Order and Pay Now')
        })
      })
    })
  })

  describe('@events', () => {
    it('should submit order on submit button click if valid', () => {
      const props = { ...initialProps }
      const { wrapper } = renderComponent(props)
      wrapper
        .find($.button)
        .dive()
        .simulate('click')
      expect(props.submitOrder).toHaveBeenCalled()
    })

    it('should send analytics eVar72, with error message, on submit order failure', () => {
      const props = {
        ...initialProps,
        submitOrder: jest.fn(() =>
          Promise.resolve(new Error('Please insert a valid card number'))
        ),
      }
      const { wrapper } = renderComponent(props)
      const clickHandler = wrapper.find($.button).prop('clickHandler')

      return clickHandler().then(() => {
        expect(props.sendEventAnalytics).toHaveBeenCalledWith({
          eVar72: 'Please insert a valid card number',
        })
      })
    })

    it('should not send analytics on submit order success', () => {
      const props = { ...initialProps }
      const { wrapper } = renderComponent(props)
      const clickHandler = wrapper.find($.button).prop('clickHandler')

      return clickHandler().then(() => {
        expect(props.sendEventAnalytics).not.toHaveBeenCalled()
      })
    })
  })

  describe('@connected', () => {
    describe('mapping state to props', () => {
      describe('klarnaAuthToken', () => {
        it('is correctly mapped from state when the klarna auth token is available is available', () => {
          const authorizationToken = 'foo...'
          const state = {
            klarna: {
              authorizationToken,
            },
          }
          const props = renderConnectedProps(state)
          expect(props.klarnaAuthToken).toBe(authorizationToken)
        })

        it('is mapped as undefined when the klarna auth token is not available', () => {
          const props = renderConnectedProps()
          expect(props.klarnaAuthToken).toBe(undefined)
        })
      })

      describe('paymentType', () => {
        it('`paymentType` is correctly mapped from state when the payment type is available', () => {
          const value = 'FOO'
          const state = {
            forms: {
              checkout: {
                billingCardDetails: {
                  fields: {
                    paymentType: {
                      value,
                    },
                  },
                },
              },
            },
          }
          const props = renderConnectedProps(state)
          expect(props.paymentType).toBe(value)
        })

        it('is mapped as an empty string when the payment type is not available', () => {
          const props = renderConnectedProps()
          expect(props.paymentType).toBe('')
        })
      })
    })

    describe('mapping dispatch to props', () => {
      it('`submitOrder()` is correctly mapped', () => {
        const props = renderConnectedProps()
        props.submitOrder('hello')
        expect(submitOrder).toHaveBeenCalledTimes(1)
        expect(submitOrder).toHaveBeenCalledWith('hello')
      })

      it('`validateForms()` is correctly mapped', () => {
        const props = renderConnectedProps()
        props.validateForms('yes')
        expect(validateForms).toHaveBeenCalledTimes(1)
        expect(validateForms).toHaveBeenCalledWith('yes')
      })

      it('`sendEventAnalytics()` is correctly mapped', () => {
        const props = renderConnectedProps()
        props.sendEventAnalytics('yep')
        expect(eventBasedAnalytics).toHaveBeenCalledTimes(1)
        expect(eventBasedAnalytics).toHaveBeenCalledWith('yep')
      })
    })
  })
})
