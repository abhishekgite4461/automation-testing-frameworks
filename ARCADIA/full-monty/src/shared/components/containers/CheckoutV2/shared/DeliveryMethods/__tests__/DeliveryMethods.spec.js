import testComponentHelper from 'test/unit/helpers/test-component'

import DeliveryMEthods from '../DeliveryMethods'

describe('<DeliveryMethods />', () => {
  const renderComponent = testComponentHelper(DeliveryMEthods)

  const createDeliveryMethods = ({
    selectedDeliveryMethod = 'HOME_STANDARD',
    selectedDeliveryMethodEnabled = true,
  } = {}) => {
    return [
      {
        deliveryType: 'HOME_STANDARD',
        selected: selectedDeliveryMethod === 'HOME_STANDARD',
        enabled: true,
        cost: '4.00',
        label: 'UK Standard up to 4 days',
        additionalDescription: 'Up to 4 days',
        deliveryOptions: [],
      },
      {
        deliveryType: 'HOME_EXPRESS',
        selected: selectedDeliveryMethod === 'HOME_EXPRESS',
        enabled: selectedDeliveryMethodEnabled === true,
        label: 'Express / Nominated day delivery',
        additionalDescription: '',
        deliveryOptions: [
          {
            shipModeId: 28005,
            dayText: 'Thu',
            dateText: '07 Sep',
            price: '6.00',
            selected: true,
          },
          {
            shipModeId: 28006,
            dayText: 'Fri',
            dateText: '08 Sep',
            price: '6.00',
            selected: false,
          },
        ],
      },
    ]
  }

  describe('@render', () => {
    it('should render default state', () => {
      const component = renderComponent()
      expect(component.getTree()).toMatchSnapshot()
    })

    it('should render delivery methods', () => {
      const component = renderComponent({
        deliveryMethods: createDeliveryMethods(),
      })
      expect(component.getTree()).toMatchSnapshot()
    })

    it('should render delivery options when `HOME_EXPRESS` delivery type is selected', () => {
      const component = renderComponent({
        deliveryMethods: createDeliveryMethods({
          selectedDeliveryMethod: 'HOME_EXPRESS',
        }),
      })
      const deliveryOptions = component.wrapper.find('DeliveryOptions')
      expect(deliveryOptions.length).toBe(1)
      expect(deliveryOptions.prop('deliveryType')).toBe('HOME_EXPRESS')
      expect(deliveryOptions.prop('deliveryOptions')).toEqual([
        {
          shipModeId: 28005,
          dayText: 'Thu',
          dateText: '07 Sep',
          price: '6.00',
          selected: true,
        },
        {
          shipModeId: 28006,
          dayText: 'Fri',
          dateText: '08 Sep',
          price: '6.00',
          selected: false,
        },
      ])
    })

    it('should not render delivery options when `HOME_EXPRESS` delivery type is not enabled', () => {
      const component = renderComponent({
        deliveryMethods: createDeliveryMethods({
          selectedDeliveryMethod: 'HOME_EXPRESS',
          selectedDeliveryMethodEnabled: false,
        }),
      })
      expect(component.wrapper.find('DeliveryOptions').exists()).toEqual(false)
    })
  })

  describe('@actions', () => {
    it('should fire `onDeliveryMethodChange`, with index, on delivery method change', () => {
      const onDeliveryMethodChangeMock = jest.fn()
      const props = {
        deliveryMethods: createDeliveryMethods(),
        onDeliveryMethodChange: onDeliveryMethodChangeMock,
      }
      const component = renderComponent(props)
      component.wrapper.find('DeliveryMethod').forEach((deliveryMethod, i) => {
        deliveryMethod.prop('onChange')()
        expect(onDeliveryMethodChangeMock).toHaveBeenLastCalledWith(i)
      })
    })

    it('should fire `onDeliveryOptionsChange`, bound with `HOME_EXPRESS` argument, on delivery option change', () => {
      const onDeliveryOptionsChangeMock = jest.fn()
      const props = {
        deliveryMethods: createDeliveryMethods({
          selectedDeliveryMethod: 'HOME_EXPRESS',
        }),
        onDeliveryOptionsChange: onDeliveryOptionsChangeMock,
      }
      const component = renderComponent(props)
      const event = { type: 'event' }
      component.wrapper.find('DeliveryOptions').prop('onChange')(event)
      expect(onDeliveryOptionsChangeMock).toHaveBeenCalledWith(
        'HOME_EXPRESS',
        event
      )
    })
  })
})
