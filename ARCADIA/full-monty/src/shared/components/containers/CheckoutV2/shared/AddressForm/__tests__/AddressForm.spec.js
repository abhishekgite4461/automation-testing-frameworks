import testComponentHelper from 'test/unit/helpers/test-component'

import AddressForm from '../AddressForm'
import FindAddress from '../FindAddress'

describe('<AddressForm />', () => {
  const requiredProps = {
    addressType: 'billing',
  }
  const renderComponent = testComponentHelper(AddressForm)

  describe('@renders', () => {
    it('should render default state', () => {
      expect(renderComponent(requiredProps).getTree()).toMatchSnapshot()
    })

    it('should pass `titleHidden` prop to the `FindAddress` component', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        titleHidden: true,
      })
      expect(wrapper.find(FindAddress).prop('titleHidden')).toBe(true)
    })

    it('should pass `onSelectCountry` prop to the `FindAddress` component', () => {
      const onSelectCountry = () => {}
      const { wrapper } = renderComponent({
        ...requiredProps,
        onSelectCountry,
      })
      expect(wrapper.find(FindAddress).prop('onSelectCountry')).toBe(
        onSelectCountry
      )
    })
  })
})
