import { mapStateToProps } from '../PaymentDetailsContainer'

describe('<PaymentDetailsContainer />', () => {
  describe('@mapStateToProps', () => {
    const storedCreditCard = {
      type: 'VISA',
      cardNumberHash: 'tjOBl4zzS+ueTZQWartO5l968iOmCOix',
      cardNumberStar: '************1111',
      expiryMonth: '08',
      expiryYear: '2018',
    }

    const requiredState = {
      viewport: { width: 320 },
    }

    const accountCreditCard = {
      account: {
        user: {
          creditCard: storedCreditCard,
        },
      },
    }

    const paymentMethodsState = {
      paymentMethods: [
        {
          value: 'VISA',
          type: 'CARD',
          label: 'Visa',
          description: 'Pay with VISA',
          icon: 'icon_visa.svg',
        },
        {
          value: 'AMEX',
          type: 'CARD',
          label: 'American Express',
          description: 'Pay with American Express',
          icon: 'icon_amex.svg',
        },
        {
          value: 'ACCNT',
          type: 'OTHER_CARD',
          label: 'Account Card',
          description: 'Pay with Account Card',
          icon: 'icon_account-card.svg',
        },
        {
          value: 'PYPAL',
          type: 'OTHER',
          label: 'Paypal',
          description: 'Check out with your PayPal account',
          icon: 'icon_paypal.svg',
        },
      ],
    }

    const createFormsState = (paymentType) => ({
      forms: {
        checkout: {
          billingCardDetails: {
            fields: {
              paymentType: {
                value: paymentType,
              },
            },
          },
        },
      },
    })

    it('should create default props', () => {
      expect(mapStateToProps({ ...requiredState })).toEqual({
        combinedPaymentMethods: [],
        selectedCombinedPaymentMethod: undefined,
        storedPaymentDetails: {},
        storedPaymentMethod: {},
        storedCombinedPaymentMethod: undefined,
        isMobile: false,
        orderDetails: undefined,
        checkoutForms: undefined,
        config: undefined,
        findAddressState: undefined,
        inDeliveryAndPayment: false,
        orderSummary: undefined,
        siteOptions: undefined,
        isMobileBreakpoint: true,
      })
    })

    it('should return the payment methods, with the cards combined', () => {
      expect(
        mapStateToProps({
          ...requiredState,
          ...paymentMethodsState,
        }).combinedPaymentMethods
      ).toEqual([
        {
          value: 'CARD',
          label: 'Debit and Credit Card',
          description: 'Visa, American Express',
          type: 'CARD',
          paymentTypes: [
            paymentMethodsState.paymentMethods[0],
            paymentMethodsState.paymentMethods[1],
          ],
          icons: ['icon_visa.svg', 'icon_amex.svg'],
        },
        {
          value: 'ACCNT',
          label: 'Account Card',
          description: 'Pay with Account Card',
          type: 'OTHER_CARD',
          paymentTypes: [],
          icons: ['icon_account-card.svg'],
        },
        {
          value: 'PYPAL',
          label: 'Paypal',
          description: 'Check out with your PayPal account',
          type: 'OTHER',
          paymentTypes: [],
          icons: ['icon_paypal.svg'],
        },
      ])
    })

    it('should return the selected payment method', () => {
      expect(
        mapStateToProps({
          ...createFormsState('ACCNT'),
          ...paymentMethodsState,
          viewport: { width: 320 },
        }).selectedCombinedPaymentMethod
      ).toEqual({
        description: 'Pay with Account Card',
        icons: ['icon_account-card.svg'],
        label: 'Account Card',
        paymentTypes: [],
        type: 'OTHER_CARD',
        value: 'ACCNT',
      })
    })

    it('should return a selected payment method of `CARD` if `paymentType` is a card', () => {
      expect(
        mapStateToProps({
          ...createFormsState('CARD'),
          ...paymentMethodsState,
          viewport: { width: 320 },
        }).selectedCombinedPaymentMethod
      ).toEqual({
        description: 'Visa, American Express',
        icons: ['icon_visa.svg', 'icon_amex.svg'],
        label: 'Debit and Credit Card',
        paymentTypes: [
          paymentMethodsState.paymentMethods[0],
          paymentMethodsState.paymentMethods[1],
        ],
        type: 'CARD',
        value: 'CARD',
      })
    })

    it('should return first combinedPaymentMethod if no payment method selected in the form', () => {
      expect(
        mapStateToProps({
          ...paymentMethodsState,
          viewport: { width: 320 },
        }).selectedCombinedPaymentMethod
      ).toEqual({
        description: 'Visa, American Express',
        icons: ['icon_visa.svg', 'icon_amex.svg'],
        label: 'Debit and Credit Card',
        paymentTypes: [
          paymentMethodsState.paymentMethods[0],
          paymentMethodsState.paymentMethods[1],
        ],
        type: 'CARD',
        value: 'CARD',
      })
    })

    it('should get the current payment details from the account', () => {
      expect(
        mapStateToProps({
          ...requiredState,
          ...accountCreditCard,
        }).storedPaymentDetails
      ).toEqual(storedCreditCard)
    })

    it('should get the current payment method based on the account’s payment details', () => {
      expect(
        mapStateToProps({
          ...requiredState,
          ...accountCreditCard,
          ...paymentMethodsState,
        }).storedCombinedPaymentMethod
      ).toEqual({
        description: 'Visa, American Express',
        icons: ['icon_visa.svg', 'icon_amex.svg'],
        label: 'Debit and Credit Card',
        paymentTypes: [
          paymentMethodsState.paymentMethods[0],
          paymentMethodsState.paymentMethods[1],
        ],
        type: 'CARD',
        value: 'CARD',
      })
    })

    it('should return `isMobile` based on the viewport', () => {
      expect(
        mapStateToProps({
          viewport: {
            media: 'mobile',
            width: 320,
          },
        }).isMobile
      ).toBe(true)
    })

    it('should return inDeliveryAndPayment true if on delivery-payment page', () => {
      expect(
        mapStateToProps({
          ...requiredState,
          routing: {
            location: {
              pathname: 'checkout/delivery-payment',
            },
          },
        }).inDeliveryAndPayment
      ).toBe(true)
    })

    it('should return inDeliveryAndPayment false if not on delivery-payment page', () => {
      expect(
        mapStateToProps({
          ...requiredState,
          routing: {
            location: {
              pathname: 'checkout/delivery',
            },
          },
        }).inDeliveryAndPayment
      ).toBe(false)
    })
  })
})
