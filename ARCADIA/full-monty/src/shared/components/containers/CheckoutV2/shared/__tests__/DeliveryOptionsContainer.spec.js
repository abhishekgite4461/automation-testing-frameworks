import testComponentHelper from 'test/unit/helpers/test-component'

import * as checkoutActions from '../../../../../actions/common/checkoutActions'

import {
  WrappedDeliveryOptionsContainer,
  mapDispatchToProps,
} from '../DeliveryOptionsContainer'
import DeliveryOptions from '../../Delivery/DeliveryOptions'

describe('<DeliveryOptionsContainer />', () => {
  const deliveryLocations = {
    home: {
      deliveryLocationType: 'HOME',
      title: 'Home Delivery',
      iconUrl: '/assets/topshop/images/lorry-icon.svg',
    },
    store: {
      deliveryLocationType: 'PARCELSHOP',
      title: 'Collect from ParcelShop',
      iconUrl: '/assets/topshop/images/hermes-icon.svg',
    },
  }
  const requiredProps = {
    onChangeDeliveryLocation: () => {},
  }
  const renderComponent = testComponentHelper(WrappedDeliveryOptionsContainer)

  describe('mapDispatchToProps', () => {
    const selectDeliveryLocationSpy = jest
      .spyOn(checkoutActions, 'selectDeliveryLocation')
      .mockImplementation(() => ({
        type: 'SELECT_DELIVERY_LOCATION',
      }))

    afterEach(() => {
      selectDeliveryLocationSpy.mockReset()
    })

    afterAll(() => {
      selectDeliveryLocationSpy.mockRestore()
    })

    it('should return `selectDeliveryLocation` action for `onChangeDeliveryLocation` prop', () => {
      const dispatchMock = jest.fn()
      const { onChangeDeliveryLocation } = mapDispatchToProps(dispatchMock)
      onChangeDeliveryLocation(deliveryLocations.store, 1)
      expect(selectDeliveryLocationSpy).toHaveBeenCalledWith(1)
      expect(dispatchMock).toHaveBeenCalledWith({
        type: 'SELECT_DELIVERY_LOCATION',
      })
    })

    it('should use `onChangeDeliveryLocation` own prop if supplied', () => {
      const onChangeDeliveryLocation = () => {}
      const props = mapDispatchToProps(null, { onChangeDeliveryLocation })
      expect(props.onChangeDeliveryLocation).toBe(onChangeDeliveryLocation)
    })
  })

  describe('@render', () => {
    it('should render in default state', () => {
      const { getTree } = renderComponent(requiredProps)
      expect(getTree()).toMatchSnapshot()
    })

    it('should not render delivery options if only one delivery location', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        deliveryLocations: [deliveryLocations.home],
      })
      expect(wrapper.find(DeliveryOptions).isEmpty()).toBe(true)
    })

    it('should render delivery options if there is more than one delivery location', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        deliveryLocations: [deliveryLocations.home, deliveryLocations.store],
      })
      expect(wrapper.find(DeliveryOptions).isEmpty()).toBe(false)
    })
  })
})
