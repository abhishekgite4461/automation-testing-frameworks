import { connect } from 'react-redux'

import { getEnrichedDeliveryMethods } from '../../../../selectors/checkoutSelectors'

// actions
import {
  putOrderSummary,
  selectDeliveryType,
} from '../../../../actions/common/checkoutActions'

// components
import DeliveryMethods from './DeliveryMethods/DeliveryMethods'

const selectDeliveryOption = (deliveryType, shipModeId, dependencies = {}) => {
  const deps = {
    putOrderSummary,
    ...dependencies,
  }
  return (dispatch, getState) => {
    const { basket, shippingCountry } = getState().checkout.orderSummary
    const payload = {
      orderId: basket.orderId,
      shippingCountry,
      deliveryType,
      shipModeId,
    }
    dispatch(deps.putOrderSummary(payload))
  }
}

const mapStateToProps = (state) => ({
  deliveryMethods: getEnrichedDeliveryMethods(state),
})

const mapDispatchToProps = {
  onDeliveryMethodChange: selectDeliveryType,
  onDeliveryOptionsChange: selectDeliveryOption,
}

export { mapStateToProps, mapDispatchToProps }

export default connect(mapStateToProps, mapDispatchToProps)(DeliveryMethods)
