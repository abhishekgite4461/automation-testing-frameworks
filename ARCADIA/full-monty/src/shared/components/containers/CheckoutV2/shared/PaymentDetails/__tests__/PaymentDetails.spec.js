import toJson from 'enzyme-to-json'
import { propEq } from 'ramda'

import testComponentHelper from 'test/unit/helpers/test-component'

import PaymentDetails from '../PaymentDetails'
import AddressPreview from '../../AddressPreview'
import PaymentMethodOption from '../PaymentMethodOption'

describe('<PaymentDetails/>', () => {
  const renderComponent = testComponentHelper(PaymentDetails)

  const requiredProps = {
    checkoutForms: {},
    combinedPaymentMethods: [],
    findAddressState: {},
    inDeliveryAndPayment: false,
    isMobile: false,
    isMobileBreakpoint: false,
    resetFormPartial: () => {},
    storedPaymentDetails: {},
    selectedCombinedPaymentMethod: {},
    setFormField: () => {},
    setManualAddressMode: () => {},
    sendAnalyticsPaymentMethodChangeEvent: () => {},
  }

  const resetFormPartialStub = jest.fn()

  const setFormFieldStub = jest.fn()

  const getPaymentMethodsStub = jest.fn()

  const combinedPaymentMethods = [
    { value: 'CARD', label: 'Debit and Credit Card', type: 'CARD' },
    { value: 'ACCNT', label: 'Account Card', type: 'OTHER_CARD' },
    { value: 'PYPAL', label: 'Paypal', type: 'OTHER' },
    { value: 'MPASS', label: 'Masterpass', type: 'OTHER' },
    { value: 'KLRNA', label: 'Try before you buy', type: 'OTHER' },
  ]
  const cardPaymentMethod = {
    value: 'CARD',
    label: 'Debit and Credit Card',
    description: 'Visa, MasterCard, American Express, Switch/Maestro',
  }

  const paymentDetails = {
    cardNumberStar: '************1111',
    expiryMonth: '08',
    expiryYear: '2018',
  }

  const baseProps = {
    combinedPaymentMethods,
    resetFormPartial: resetFormPartialStub,
    setFormField: setFormFieldStub,
    getPaymentMethods: getPaymentMethodsStub,
  }

  afterEach(() => {
    resetFormPartialStub.mockReset()
    setFormFieldStub.mockReset()
    getPaymentMethodsStub.mockReset()
  })

  describe('@renders', () => {
    it('should render default state correctly', () => {
      const component = renderComponent({
        ...requiredProps,
        resetFormPartial: resetFormPartialStub,
        setFormField: setFormFieldStub,
        getPaymentMethods: getPaymentMethodsStub,
      })
      expect(component.getTree()).toMatchSnapshot()
    })

    it('should render payment method options correctly', () => {
      const component = renderComponent({
        ...requiredProps,
        ...baseProps,
      })
      expect(component.getTree()).toMatchSnapshot()
    })

    it('should render card payment method if selected', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        ...baseProps,
        selectedCombinedPaymentMethod: combinedPaymentMethods.find(
          propEq('value', 'CARD')
        ),
      })
      const cardPaymentMethod = wrapper.find('Connect(CardPaymentMethod)')
      expect(cardPaymentMethod.length).toBe(1)
      expect(cardPaymentMethod.props()).toEqual({
        formCardErrorPath: [
          'forms',
          'checkout',
          'billingCardDetails',
          'errors',
        ],
        formCardName: 'billingCardDetails',
        formCardPath: ['checkout', 'billingCardDetails', 'fields'],
        isPaymentCard: true,
      })
    })

    it('should pass description to `PaymentMethodOption` if selected and mobile', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        ...baseProps,
        combinedPaymentMethods: [cardPaymentMethod],
        selectedCombinedPaymentMethod: cardPaymentMethod,
        isMobile: true,
      })
      expect(wrapper.find(PaymentMethodOption).prop('description')).toBe(
        'Visa, MasterCard, American Express, Switch/Maestro'
      )
    })

    it('should not pass description to `PaymentMethodOption` if mobile', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        ...baseProps,
        combinedPaymentMethods: [cardPaymentMethod],
        isMobile: true,
      })
      expect(wrapper.find(PaymentMethodOption).prop('description')).toBe('')
    })

    it('should render account card payment method if selected', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        ...baseProps,
        selectedCombinedPaymentMethod: combinedPaymentMethods.find(
          propEq('value', 'ACCNT')
        ),
      })
      const accountCardPaymentMethod = wrapper.find(
        'Connect(CardPaymentMethod)'
      )
      expect(accountCardPaymentMethod.length).toBe(1)
      expect(accountCardPaymentMethod.props()).toEqual({
        formCardErrorPath: [
          'forms',
          'checkout',
          'billingCardDetails',
          'errors',
        ],
        formCardName: 'billingCardDetails',
        formCardPath: ['checkout', 'billingCardDetails', 'fields'],
      })
    })

    it('should render Klarna payment method if selected', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        ...baseProps,
        selectedCombinedPaymentMethod: combinedPaymentMethods.find(
          propEq('value', 'KLRNA')
        ),
      })
      expect(wrapper.find('Connect(KlarnaForm)').length).toBe(1)
      expect(wrapper.find('.PaymentDetails-logInNotice').length).toBe(1)
    })

    it('should render log in notice if any other payment method', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        ...baseProps,
        selectedCombinedPaymentMethod: combinedPaymentMethods.find(
          propEq('value', 'PYPAL')
        ),
      })
      const payPalPaymentMethod = wrapper.find('.PaymentDetails-logInNotice')
      expect(payPalPaymentMethod.length).toBe(1)
      expect(payPalPaymentMethod.text()).toBe(
        'You will be asked to log onto Paypal to confirm your order'
      )
    })

    it('should render preview if the are current payment details and method', () => {
      const component = renderComponent({
        ...requiredProps,
        ...baseProps,
        ...baseProps,
        storedPaymentDetails: paymentDetails,
        storedCombinedPaymentMethod: combinedPaymentMethods.find(
          propEq('value', 'CARD')
        ),
      })
      const preview = component.wrapper.find('PaymentMethodPreview')
      expect(component.getTreeFor(preview)).toMatchSnapshot()
    })

    it('should render ‘card’ payment preview, is current payment method is `CARD`', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        ...baseProps,
        storedPaymentDetails: paymentDetails,
        storedCombinedPaymentMethod: combinedPaymentMethods.find(
          propEq('value', 'CARD')
        ),
      })
      expect(
        toJson(wrapper.find('PaymentMethodPreview').children())
      ).toMatchSnapshot()
    })

    it('should render ‘card’ payment preview, is current payment method is `ACCNT`', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        ...baseProps,
        storedPaymentDetails: paymentDetails,
        storedCombinedPaymentMethod: combinedPaymentMethods.find(
          propEq('value', 'ACCNT')
        ),
      })
      expect(
        toJson(wrapper.find('PaymentMethodPreview').children())
      ).toMatchSnapshot()
    })

    it('should render ‘non-card’ payment preview, is current payment method is `PYPAL`', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        ...baseProps,
        storedPaymentDetails: paymentDetails,
        storedCombinedPaymentMethod: combinedPaymentMethods.find(
          propEq('value', 'PYPAL')
        ),
      })
      expect(
        toJson(wrapper.find('PaymentMethodPreview').children())
      ).toMatchSnapshot()
    })

    it('should render billing AddressPreview if on delivery payment page and state is default', () => {
      expect(
        renderComponent({
          ...requiredProps,
          ...baseProps,
          checkoutForms: {
            billingAddress: {
              fields: {
                country: {
                  value: 'United Kingdom',
                },
                address1: {
                  value: '123 Fake St',
                },
              },
            },
            billingDetails: {
              fields: {},
            },
          },
          config: {
            checkoutAddressFormRules: {
              'United Kingdom': {
                pattern: '^[\\w ]+$',
                stateFieldType: false,
                postcodeRequired: true,
                postcodeLabel: 'Postal Code',
                premisesRequired: true,
                premisesLabel: 'Street Address',
              },
            },
          },
          findAddressState: {
            isManual: true,
            monikers: [],
          },
          inDeliveryAndPayment: true,
          siteOptions: {
            titles: ['Mr', 'Mrs'],
          },
        }).getTree()
      ).toMatchSnapshot()
    })

    it('should render billing address forms if on delivery payment page and showAddressPreview state is false', () => {
      const { wrapper, getTree } = renderComponent({
        ...requiredProps,
        ...baseProps,
        checkoutForms: {
          billingAddress: {
            fields: {
              country: {
                value: 'United Kingdom',
              },
              address1: {
                value: '123 Fake St',
              },
            },
          },
          billingDetails: {
            fields: {},
          },
          billingFindAddress: {
            fields: {},
          },
          order: {
            fields: {},
          },
        },
        config: {
          checkoutAddressFormRules: {
            'United Kingdom': {
              pattern: '^[\\w ]+$',
              stateFieldType: false,
              postcodeRequired: true,
              postcodeLabel: 'Postal Code',
              premisesRequired: true,
              premisesLabel: 'Street Address',
            },
          },
        },
        findAddressState: {
          isManual: true,
          monikers: [],
        },
        inDeliveryAndPayment: true,
        siteOptions: {
          titles: ['Mr', 'Mrs'],
        },
        orderSummary: {},
      })
      wrapper.setState({ showAddressPreview: false })
      expect(getTree()).toMatchSnapshot()
    })

    it('should render billing address forms if on delivery payment page and showAddressPreview state is false', () => {
      const { wrapper, getTree } = renderComponent({
        ...requiredProps,
        ...baseProps,
        checkoutForms: {
          billingAddress: {
            fields: {
              country: {
                value: 'United Kingdom',
              },
              address1: {
                value: '123 Fake St',
              },
            },
          },
          billingDetails: {
            fields: {},
          },
          billingFindAddress: {
            fields: {},
          },
          order: {
            fields: {},
          },
        },
        config: {
          checkoutAddressFormRules: {
            'United Kingdom': {
              pattern: '^[\\w ]+$',
              stateFieldType: false,
              postcodeRequired: true,
              postcodeLabel: 'Postal Code',
              premisesRequired: true,
              premisesLabel: 'Street Address',
            },
          },
        },
        findAddressState: {
          isManual: false,
          monikers: [],
        },
        inDeliveryAndPayment: true,
        siteOptions: {
          titles: ['Mr', 'Mrs'],
        },
        orderSummary: {},
      })
      wrapper.setState({ showAddressPreview: false })
      expect(getTree()).toMatchSnapshot()
    })
  })

  describe('@events', () => {
    beforeEach(() => jest.clearAllMocks())

    describe('paymentMethodChange', () => {
      it('should reset the `cardNumber`, `expiryMonth/Year` and `cvv` form fields', () => {
        const { wrapper } = renderComponent({
          ...requiredProps,
          ...baseProps,
        })
        const changeHandler = wrapper
          .find(PaymentMethodOption)
          .first()
          .prop('onChange')
        changeHandler({ target: {} })
        expect(resetFormPartialStub).toHaveBeenCalledWith(
          'billingCardDetails',
          {
            cardNumber: '',
            expiryMonth: '',
            expiryYear: '',
            cvv: '',
          }
        )
      })

      it('should set `paymentType` if method is checked', () => {
        const { wrapper } = renderComponent({
          ...requiredProps,
          ...baseProps,
        })
        const changeHandler = wrapper
          .find(PaymentMethodOption)
          .find({ value: 'ACCNT' })
          .prop('onChange')
        changeHandler({ target: { checked: true, value: 'ACCNT' } })
        expect(setFormFieldStub).toHaveBeenCalledWith(
          'billingCardDetails',
          'paymentType',
          'ACCNT'
        )
      })

      it('should set `paymentType` to `VISA` if method is `CARD`', () => {
        const { wrapper } = renderComponent({
          ...requiredProps,
          ...baseProps,
        })
        const changeHandler = wrapper
          .find(PaymentMethodOption)
          .find({ value: 'CARD' })
          .prop('onChange')
        changeHandler({ target: { checked: true, value: 'CARD' } })
        expect(setFormFieldStub).toHaveBeenCalledWith(
          'billingCardDetails',
          'paymentType',
          'VISA'
        )
      })

      it('should send an analytics event if method is checked', () => {
        const analyticsSpy = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          ...baseProps,
          sendAnalyticsPaymentMethodChangeEvent: analyticsSpy,
        })
        const changeHandler = wrapper
          .find(PaymentMethodOption)
          .find({ value: 'CARD' })
          .prop('onChange')
        expect(analyticsSpy).not.toHaveBeenCalled()

        changeHandler({ target: { checked: true, value: 'KLRNA' } })
        expect(analyticsSpy).toHaveBeenCalledTimes(1)
        expect(analyticsSpy).toHaveBeenCalledWith('KLRNA')
      })

      it('should not send an analytics event if method is checked', () => {
        const analyticsSpy = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          ...baseProps,
          sendAnalyticsPaymentMethodChangeEvent: analyticsSpy,
        })
        const changeHandler = wrapper
          .find(PaymentMethodOption)
          .find({ value: 'CARD' })
          .prop('onChange')
        changeHandler({ target: { checked: false, value: 'KLRNA' } })
        expect(analyticsSpy).not.toHaveBeenCalled()
      })
    })

    describe('paymentMethodPreviewChange', () => {
      let wrapper
      let props

      beforeEach(() => {
        props = {
          ...requiredProps,
          ...baseProps,
          storedPaymentDetails: paymentDetails,
          storedCombinedPaymentMethod: combinedPaymentMethods.find(
            propEq('value', 'CARD')
          ),
        }
        const component = renderComponent(props)
        wrapper = component.wrapper
        wrapper.setState({ showPaymentPreview: true })
        const changeHandler = wrapper
          .find('PaymentMethodPreview')
          .prop('onChange')
        changeHandler()
      })

      it('should set `showPreview` state to false on change of preview', () => {
        expect(wrapper.state('showPaymentPreview')).toBe(false)
      })

      it('should set `setFormField` to visa credit card', () => {
        expect(props.setFormField).toHaveBeenCalledTimes(1)
        expect(props.setFormField).toHaveBeenCalledWith(
          'billingCardDetails',
          'paymentType',
          'VISA'
        )
      })
    })

    describe('addressPreviewChange', () => {
      it('should set `showAddressPreview` state to false', () => {
        const { wrapper } = renderComponent({
          ...requiredProps,
          ...baseProps,
          checkoutForms: {
            billingAddress: {
              fields: {
                country: {
                  value: 'United Kingdom',
                },
                address1: {
                  value: '123 Fake St',
                },
              },
            },
            billingDetails: {
              fields: {},
            },
            billingFindAddress: {
              fields: {},
            },
            order: {
              fields: {},
            },
          },
          orderSummary: {
            shippingCountry: 'United Kingdom',
          },
          config: {
            checkoutAddressFormRules: {
              'United Kingdom': {
                pattern: '^[\\w ]+$',
                stateFieldType: false,
                postcodeRequired: true,
                postcodeLabel: 'Postal Code',
                premisesRequired: true,
                premisesLabel: 'Street Address',
              },
            },
          },
          findAddressState: {
            isManual: true,
            monikers: [],
          },
          inDeliveryAndPayment: true,
          siteOptions: {
            titles: ['Mr', 'Mrs'],
          },
        })
        const changeHandler = wrapper
          .find(AddressPreview)
          .prop('onClickChangeButton')
        changeHandler()
        expect(wrapper.state('showAddressPreview')).toBe(false)
      })
    })
  })
})
