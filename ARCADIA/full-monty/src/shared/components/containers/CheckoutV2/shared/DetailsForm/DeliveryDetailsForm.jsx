import PropTypes from 'prop-types'
import React from 'react'

import DetailsForm from './DetailsForm'
import CheckoutTitle from '../CheckoutTitle'

const DeliveryDetailsForm = (props, { l }) => {
  return (
    <div>
      <CheckoutTitle separator>{l('Your Delivery Details')}</CheckoutTitle>
      <DetailsForm
        detailsType="delivery"
        titleHidden
        renderTelephone={props.renderTelephone}
      />
    </div>
  )
}

DeliveryDetailsForm.contextTypes = {
  l: PropTypes.func,
}

export default DeliveryDetailsForm
