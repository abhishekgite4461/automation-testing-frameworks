import React from 'react'

import testComponentHelper from 'test/unit/helpers/test-component'

import PaymentMethodPreview from '../PaymentMethodPreview'
import KlarnaForm from '../../../../Klarna/KlarnaForm'

describe('<PaymentMethodPreview />', () => {
  const renderComponent = testComponentHelper(PaymentMethodPreview)

  const requiredProps = {
    children: <div>child element</div>,
  }

  describe('@renders', () => {
    it('should render default state correctly', () => {
      expect(renderComponent({ ...requiredProps }).getTree()).toMatchSnapshot()
    })

    it('should render the correct card icon of the payment methods passed in', () => {
      const storedPaymentMethod = {
        value: 'AMEX',
        icon: 'AMEX_ICON.SVG',
      }

      expect(
        renderComponent({
          ...requiredProps,
          storedPaymentMethod,
        }).getTree()
      ).toMatchSnapshot()
    })

    it('should render `ConfirmCVV` component if `CARD` type', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        type: 'CARD',
      })
      expect(wrapper.find('Connect(ConfirmCVV)').length).toBe(1)
    })

    it('should render `ConfirmCVV` component if `OTHER_CARD` type', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        type: 'OTHER_CARD',
      })
      expect(wrapper.find('Connect(ConfirmCVV)').length).toBe(1)
    })

    it('should render `KlarnaForm` component if `KLRNA` value', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        value: 'KLRNA',
      })
      expect(wrapper.find(KlarnaForm).length).toBe(1)
    })
  })

  describe('@events', () => {
    it('should call `onChange` prop when button is clicked', () => {
      const onChangeMock = jest.fn()
      const event = {}
      const { wrapper } = renderComponent({
        ...requiredProps,
        onChange: onChangeMock,
      })
      wrapper.find('button').prop('onClick')(event)
      expect(onChangeMock).toHaveBeenCalledWith(event)
    })
  })
})
