import classNames from 'classnames'
import PropTypes from 'prop-types'
import { path } from 'ramda'
import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  findAddressIsVisible,
  getAddressForm,
  getCountryFor,
} from '../../../../../selectors/checkoutSelectors'
import { getYourAddressSchema } from '../validationSchemas'
import { eventBasedAnalytics } from '../../../../../lib/analytics/analytics'
import { getFormNames } from '../../../../../lib/checkout-utilities/utils'

// actions
import {
  setFormField,
  setAndValidateFormField,
  validateForm,
  clearFormErrors,
  touchedFormField,
  resetForm,
} from '../../../../../actions/common/formActions'
import {
  resetAddress,
  setManualAddressMode,
} from '../../../../../actions/common/checkoutActions'

// components
import Input from '../../../../common/FormComponents/Input/Input'
import Select from '../../../../common/FormComponents/Select/Select'

const mapStateToProps = (state, { addressType }) => {
  const country = getCountryFor(addressType, state)
  const rules = state.config.checkoutAddressFormRules[country]
  const isFindAddressVisible = findAddressIsVisible(addressType, state)
  const formNames = getFormNames(addressType)

  return {
    country,
    formName: formNames.address,
    findAddressFormName: formNames.findAddress,
    fields: getAddressForm(addressType, state).fields,
    rules,
    errors: getAddressForm(addressType, state).errors,
    usStates: state.siteOptions.USStates,
    canFindAddress: !!path(['config', 'qasCountries', country], state),
    isFindAddressVisible,
    validationSchema: isFindAddressVisible ? {} : getYourAddressSchema(rules),
    // NOTE: rather hacky way of determining whether the schemas have updated (as can't do a `===` on them)
    // think about using something like https://github.com/reactjs/reselect
    schemaHash: [country, isFindAddressVisible].join(':'),
  }
}

const mapDispatchToProps = {
  setFormField,
  setAndValidateFormField,
  touchFormField: touchedFormField,
  validateForm,
  clearFormErrors,
  resetForm,
  resetFindAddress: resetAddress,
  setManualAddressMode,
}

const setTrimmedField = (func) => ({ target: { value } }) => {
  func({ target: { value: value.trim() } })
}

@connect(
  mapStateToProps,
  mapDispatchToProps
)
export default class YourAddress extends Component {
  static propTypes = {
    events: PropTypes.object,
    country: PropTypes.string,
    formName: PropTypes.string.isRequired,
    findAddressFormName: PropTypes.string.isRequired,
    fields: PropTypes.object,
    rules: PropTypes.object,
    errors: PropTypes.object,
    usStates: PropTypes.arrayOf(PropTypes.string),
    canFindAddress: PropTypes.bool,
    isFindAddressVisible: PropTypes.bool,
    validationSchema: PropTypes.object,
    schemaHash: PropTypes.string,
    // functions
    sendEventAnalytics: PropTypes.func,
    setAndValidateFormField: PropTypes.func.isRequired,
    touchFormField: PropTypes.func.isRequired,
    validateForm: PropTypes.func.isRequired,
    clearFormErrors: PropTypes.func.isRequired,
    resetForm: PropTypes.func.isRequired,
    resetFindAddress: PropTypes.func.isRequired,
    setManualAddressMode: PropTypes.func.isRequired,
  }

  static defaultProps = {
    events: {
      address1: 'event136',
      postcode: 'event137',
    },
    country: '',
    fields: {},
    rules: {},
    errors: {},
    usStates: [],
    canFindAddress: false,
    isFindAddressVisible: false,
    validationSchema: {},
    schemaHash: '',
    sendEventAnalytics: eventBasedAnalytics,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  constructor(props) {
    super(props)
    this.onBlurPostcode = setTrimmedField(
      this.setAndValidateAddressField('postcode')
    )
  }

  componentDidMount() {
    this.validateAddress()
  }

  componentDidUpdate(prevProps) {
    if (prevProps.schemaHash !== this.props.schemaHash) {
      this.validateAddress()
    }
  }

  componentWillUnmount() {
    this.clearErrors()
  }

  clearForms = () => {
    const { country, formName, findAddressFormName, resetForm } = this.props
    // reset the address inputs
    resetForm(formName, {
      address1: '',
      address2: '',
      postcode: '',
      city: '',
      county: '',
      state: '',
      country,
    })
    resetForm(findAddressFormName, {
      postCode: '',
      houseNumber: '',
      findAddress: '',
    })
  }

  clearErrors() {
    const { formName, findAddressFormName, clearFormErrors } = this.props
    clearFormErrors(formName)
    clearFormErrors(findAddressFormName)
  }

  setAndValidateAddressField = (fieldName) => ({ target: { value } }) => {
    const { formName, validationSchema, setAndValidateFormField } = this.props
    return setAndValidateFormField(
      formName,
      fieldName,
      value,
      validationSchema[fieldName]
    )
  }

  validateAddress() {
    const { formName, validationSchema, validateForm } = this.props
    validateForm(formName, validationSchema)
  }

  touchAddressField = (fieldName) => () => {
    const { events, formName, sendEventAnalytics, touchFormField } = this.props
    if (fieldName in events) {
      sendEventAnalytics({
        events: events[fieldName],
      })
    }
    touchFormField(formName, fieldName)
  }

  handleSwitchToFindAddress = () => {
    this.props.resetFindAddress()
    this.clearForms()
    this.clearErrors()
  }

  handleClearForm = () => {
    this.clearForms()
    // make sure we stay in manual address mode after clearing all the fields
    this.props.setManualAddressMode()
    this.validateAddress()
  }

  renderStateField(fieldType) {
    const { l } = this.context
    const { fields, usStates } = this.props

    return fieldType === 'input' ? (
      <Input
        label={l`State`}
        field={fields.state}
        setField={this.setAndValidateAddressField}
        touchedField={this.touchAddressField}
        placeholder="State"
        name="state"
      />
    ) : (
      <Select
        label={l`State`}
        name="state"
        value={fields.state.value}
        onChange={this.setAndValidateAddressField('state')}
        options={usStates}
        defaultValue={fields.state.value || usStates[0]}
      />
    )
  }

  render() {
    const { l } = this.context
    const {
      country,
      fields,
      rules,
      errors,
      canFindAddress,
      isFindAddressVisible,
    } = this.props
    const classes = classNames('YourAddress-form', {
      'is-hidden': isFindAddressVisible,
    })

    return country && country !== 'default' ? (
      <section className={classes} aria-label="Delivery Address">
        <div className="YourAddress-row">
          <Input
            className="YourAddress-address1 left-col-margin"
            label={l`Address Line 1`}
            field={fields.address1}
            setField={this.setAndValidateAddressField}
            touchedField={this.touchAddressField}
            placeholder={l`Address Line 1`}
            name="address1"
            errors={errors}
            isRequired
          />
          <Input
            className="YourAddress-address2 right-col-margin"
            label={l`Address Line 2`}
            field={fields.address2}
            setField={this.setAndValidateAddressField}
            touchedField={this.touchAddressField}
            placeholder={l`Address Line 2`}
            name="address2"
            errors={errors}
          />
        </div>
        {rules.stateFieldType && this.renderStateField(rules.stateFieldType)}
        <div className="YourAddress-row">
          <Input
            className="YourAddress-postcode left-col-margin"
            label={l(rules.postcodeLabel || 'Postcode')}
            field={fields.postcode}
            onBlur={this.onBlurPostcode}
            setField={this.setAndValidateAddressField}
            touchedField={this.touchAddressField}
            placeholder={l(rules.postcodeLabel || 'Postcode')}
            name="postcode"
            errors={errors}
            isRequired={rules.postcodeRequired}
          />
          <Input
            className="YourAddress-city right-col-margin"
            label={l`Town/City`}
            field={fields.city}
            setField={this.setAndValidateAddressField}
            touchedField={this.touchAddressField}
            placeholder={l`Town/City`}
            name="city"
            errors={errors}
            isRequired
          />
        </div>
        <div className="YourAddress-linkWrapper">
          <button
            onClick={this.handleClearForm}
            className="YourAddress-link YourAddress-link--left"
          >
            {l`Clear form`}
          </button>
          {canFindAddress && (
            <button
              onClick={this.handleSwitchToFindAddress}
              className="YourAddress-link YourAddress-link--right"
            >
              {l`Find Address`}
            </button>
          )}
        </div>
      </section>
    ) : null
  }
}
