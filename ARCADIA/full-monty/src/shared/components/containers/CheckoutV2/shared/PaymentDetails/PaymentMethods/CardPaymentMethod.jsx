import creditCardType from 'credit-card-type'
import PropTypes from 'prop-types'
import { pathOr, pluck } from 'ramda'
import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  combineCardOptions,
  withValue,
  getPaymentTypeFromValidCardTypes,
} from '../../../../../../lib/checkout-utilities/utils'
import { getCardSchema } from '../../validationSchemas'
import { getSelectedPaymentType } from '../../../../../../../shared/selectors/formsSelectors'
import { isMobile } from '../../../../../../selectors/viewportSelectors'

// actions
import {
  setFormField,
  setAndValidateFormField,
  validateForm,
  validateFormField,
  clearFormErrors,
  touchedFormField,
} from '../../../../../../actions/common/formActions'
import { showModal } from '../../../../../../actions/common/modalActions'
import { sendAnalyticsValidationState } from '../../../../../../analytics'

// components
import CVVField from './CVVField'
import Input from '../../../../../common/FormComponents/Input/Input'
import Message from '../../../../../common/FormComponents/Message/Message'
import Select from '../../../../../common/FormComponents/Select/Select'

const types = {
  field: PropTypes.shape({
    value: PropTypes.string,
    isTouched: PropTypes.boolean,
  }),
  validator: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.func,
    PropTypes.string,
  ]),
}

const getPaymentTypeFromCardNumber = (cardNumber, cardTypes) => {
  const validCardTypes = creditCardType(cardNumber).filter(({ niceType }) => {
    return cardTypes.some((cardType) => cardType.includes(niceType))
  })

  return getPaymentTypeFromValidCardTypes(validCardTypes)
}

const mapStateToProps = (state, props) => {
  const paymentType = getSelectedPaymentType(state)
  const fields = pathOr({}, props.formCardPath, state.forms)
  const combinedPaymentMethods = combineCardOptions(state.paymentMethods)
  const { paymentTypes: cardPaymentTypes = [] } =
    combinedPaymentMethods.find(withValue('CARD')) || {}
  const cardTypes = pluck('label', cardPaymentTypes)

  return {
    isMobile: isMobile(state),
    cardNumberField: fields.cardNumber || {},
    expiryMonthField: fields.expiryMonth || {},
    expiryYearField: fields.expiryYear || {},
    cvvField: fields.cvv || {},
    paymentType,
    errors: pathOr({}, props.formCardErrorPath, state),
    cardTypes,
    validationSchema: getCardSchema(paymentType),
  }
}

const mapDispatchToProps = (dispatch, props) => ({
  showModal,
  setFormField: (formName, field, validators) =>
    dispatch(setFormField(formName, field, validators)),
  setAndValidateFormField: (formName, fieldName, value, validators) =>
    dispatch(setAndValidateFormField(formName, fieldName, value, validators)),
  validateFormField: (formName, month, year) =>
    dispatch(validateFormField(formName, month, year)),
  touchFormField: (formName, fieldName) =>
    dispatch(touchedFormField(formName, fieldName)),
  validate: () => dispatch(validateForm(props.formCardName)),
  clearErrors: () => dispatch(clearFormErrors(props.formCardName)),
  sendAnalyticsValidationState: ({ id, validationStatus }) =>
    dispatch(sendAnalyticsValidationState({ id, validationStatus })),
})

class CardPaymentMethod extends Component {
  static propTypes = {
    noCVV: PropTypes.bool,
    paymentType: PropTypes.string,
    cardNumberField: types.field,
    expiryMonthField: types.field,
    expiryYearField: types.field,
    cvvField: types.field,
    errors: PropTypes.objectOf(PropTypes.string),
    cardTypes: PropTypes.arrayOf(PropTypes.string),
    isPaymentCard: PropTypes.bool,
    isMobile: PropTypes.bool,
    validationSchema: PropTypes.shape({
      cardNumber: types.validator,
      cvv: types.validator,
      expiryMonth: types.validator,
    }),
    formCardName: PropTypes.string.isRequired,
    // actions
    showModal: PropTypes.func.isRequired,
    setFormField: PropTypes.func.isRequired,
    setAndValidateFormField: PropTypes.func.isRequired,
    validateFormField: PropTypes.func.isRequired,
    touchFormField: PropTypes.func.isRequired,
    validate: PropTypes.func.isRequired,
    clearErrors: PropTypes.func.isRequired,
    sendAnalyticsValidationState: PropTypes.func.isRequired,
  }

  static defaultProps = {
    noCVV: false,
    paymentType: '',
    cardNumberField: {},
    expiryMonthField: {},
    expiryYearField: {},
    cvvField: {},
    errors: {},
    cardTypes: [],
    isPaymentCard: false,
    isMobile: false,
    validationSchema: {
      cardNumber: [],
      cvv: [],
      expiryMonth: [],
    },
    formCardName: 'billingCardDetails',
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  expiryMonths = [...new Array(12)].map((_, index) => `0${index + 1}`.slice(-2))

  expiryYears = [...new Array(12)].map((_, index) =>
    (new Date().getFullYear() + index).toString()
  )

  setPaymentType(cardNumber) {
    const {
      isPaymentCard,
      cardTypes,
      paymentType: currentPaymentType,
      setFormField,
      formCardName,
    } = this.props
    // only set the payment type if this is a credit or debit card
    if (isPaymentCard) {
      const paymentType = getPaymentTypeFromCardNumber(cardNumber, cardTypes)
      if (currentPaymentType !== paymentType) {
        setFormField(formCardName, 'paymentType', paymentType)
      }
    }
  }

  setField = (fieldName) => ({ target: { value } }) => {
    const {
      setAndValidateFormField,
      validationSchema: { [fieldName]: validators },
      formCardName,
    } = this.props
    setAndValidateFormField(formCardName, fieldName, value, validators)
  }

  setCardNumber = () => (event) => {
    this.setField('cardNumber')(event)
    const {
      target: { value: cardNumber },
    } = event
    this.setPaymentType(cardNumber)
  }

  removeWhiteSpaces = (name) => (ev) => {
    const value = ev.target.value.replace(/\s/g, '')
    this.setField(name)({ target: { value } })
  }

  setExpiryYear = () => ({ target: { value } }) => {
    const {
      setFormField,
      validateFormField,
      validationSchema: { expiryMonth: expiryMonthValidators },
      formCardName,
    } = this.props
    setFormField(formCardName, 'expiryYear', value)
    validateFormField(formCardName, 'expiryMonth', expiryMonthValidators)
  }

  touchField = (fieldName) => () => {
    this.props.touchFormField(this.props.formCardName, fieldName)
  }

  componentDidMount() {
    const { validate, validationSchema } = this.props
    validate(validationSchema)
  }

  componentDidUpdate(prevProps) {
    const {
      paymentType,
      validateFormField,
      validationSchema: { cvv },
      formCardName,
    } = this.props
    if (prevProps.paymentType !== paymentType) {
      validateFormField(formCardName, 'cvv', cvv)
    }
  }

  componentWillReceiveProps(nextProps) {
    const {
      clearErrors,
      paymentType,
      cvvField,
      validateFormField,
      validationSchema: { cvv },
      formCardName,
    } = this.props
    if (paymentType !== nextProps.paymentType) {
      clearErrors()
    }

    if (cvvField.isActive && !nextProps.cvvField.isActive) {
      validateFormField(formCardName, 'cvv', cvv)
    }
  }

  componentWillUnmount() {
    this.props.clearErrors()
  }

  handleDateBlur = (field) => (...args) => {
    this.touchField(field)(...args)
    const { errors, expiryMonthField } = this.props
    const dateError = errors && expiryMonthField.isDirty && errors.expiryMonth
    this.props.sendAnalyticsValidationState({
      id: field,
      validationStatus: dateError ? 'failure' : 'success',
    })
  }

  render() {
    const { l } = this.context
    const {
      cardNumberField,
      expiryMonthField,
      expiryYearField,
      cvvField,
      paymentType,
      errors,
      isMobile,
      showModal,
      noCVV,
    } = this.props
    const isExpiryDateErroring = expiryMonthField.isDirty && errors.expiryMonth

    return (
      <div className="CardPaymentMethod">
        <Input
          className="CardPaymentMethod-cardNumber"
          field={cardNumberField}
          name="cardNumber"
          label={l`Card Number`}
          type="tel"
          placeholder={l`Card Number`}
          errors={{ cardNumber: l(errors.cardNumber) }}
          setField={this.setCardNumber}
          onBlur={this.removeWhiteSpaces('cardNumber')}
          touchedField={this.touchField}
          isRequired
        />
        <div className="CardPaymentMethod-container">
          <div
            className={`CardPaymentMethod-expiryDate${
              isExpiryDateErroring ? ' is-erroring' : ''
            }`}
          >
            <Select
              name="expiryMonth"
              className={`CardPaymentMethod-expiryMonth${
                isExpiryDateErroring ? ' is-erroring' : ''
              }`}
              label={l`Expiry: month`}
              value={
                expiryMonthField.value ||
                this.expiryMonths[new Date().getMonth()]
              }
              defaultValue={this.expiryMonths[new Date().getMonth()]}
              onChange={this.setField('expiryMonth')}
              onBlur={this.handleDateBlur('expiryMonth')}
              options={this.expiryMonths}
              isRequired
            />
            <Select
              name="expiryYear"
              className="CardPaymentMethod-expiryYear"
              label={l`Expiry: year`}
              value={expiryYearField.value || this.expiryYears[0]}
              defaultValue={this.expiryYears[0]}
              onChange={this.setExpiryYear()}
              onBlur={this.handleDateBlur('expiryYear')}
              options={this.expiryYears}
              isRequired
            />
            {isExpiryDateErroring && (
              <Message
                className="CardPaymentMethod-expiryValidationMessage"
                message={l(errors.expiryMonth)}
                type="error"
                isCompact
              />
            )}
          </div>
        </div>
        {!noCVV && (
          <div className="CardPaymentMethod-cvvFieldContainer">
            <CVVField
              field={cvvField}
              error={l(errors.cvv)}
              isMobile={isMobile}
              setField={this.setField}
              touchedField={this.touchField}
              showModal={showModal}
              paymentType={paymentType}
            />
          </div>
        )}
      </div>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CardPaymentMethod)

export { CardPaymentMethod as WrappedCardPaymentMethod, mapStateToProps }
