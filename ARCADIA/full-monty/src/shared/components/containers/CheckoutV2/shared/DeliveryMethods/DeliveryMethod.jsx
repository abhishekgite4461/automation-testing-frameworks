import PropTypes from 'prop-types'
import React from 'react'
import classNames from 'classnames'
import { isEmpty, isNil } from 'ramda'

// components
import RadioButton from '../../../../common/FormComponents/RadioButton/RadioButton'

const DeliveryMethod = (
  {
    deliveryType,
    selected,
    cost,
    label,
    additionalDescription,
    onChange,
    disabled,
  },
  { l, p }
) => {
  const className = classNames('DeliveryMethod', {
    'DeliveryMethod--disabled': disabled,
  })
  const storeStandardSelected = deliveryType === 'STORE_STANDARD'
  const priceVisible = storeStandardSelected || !isNil(cost)
  const priceLabel =
    storeStandardSelected || isEmpty(cost) ? l('Free') : p(cost)

  return (
    <div className={className}>
      <RadioButton
        id={`delivery-method-${deliveryType.toLowerCase()}`}
        checked={selected}
        label={label}
        name="deliveryMethod DeliveryMethod-radioButton"
        onChange={onChange}
        isDisabled={disabled}
      >
        <div className="DeliveryMethod-content">
          <div className="DeliveryMethod-label">{label}</div>
          {priceVisible && (
            <span className="DeliveryMethod-price">{priceLabel}</span>
          )}
          {additionalDescription && (
            <div className="DeliveryMethod-description">
              {additionalDescription}
            </div>
          )}
        </div>
      </RadioButton>
    </div>
  )
}

DeliveryMethod.propTypes = {
  deliveryType: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  selected: PropTypes.bool,
  cost: PropTypes.string,
  additionalDescription: PropTypes.string,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
}

DeliveryMethod.defaultProps = {
  selected: false,
  additionalDescription: '',
  onChange: () => {},
}

DeliveryMethod.contextTypes = {
  l: PropTypes.func,
  p: PropTypes.func,
}

export default DeliveryMethod
