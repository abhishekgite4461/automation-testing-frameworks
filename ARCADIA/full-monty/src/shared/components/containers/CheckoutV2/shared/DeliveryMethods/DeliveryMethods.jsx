import PropTypes from 'prop-types'
import React from 'react'

import {
  isDeliveryMethodSelected,
  isDeliveryMethodOfType,
} from '../../../../../testers/checkoutTesters'

// components
import DeliveryMethod from './DeliveryMethod'
import DeliveryOptions from './DeliveryOptions'
import CheckoutTitle from './../CheckoutTitle'

const DeliveryMethods = (
  { deliveryMethods, onDeliveryMethodChange, onDeliveryOptionsChange },
  { l }
) => {
  const selectedDeliveryMethod = deliveryMethods.find(isDeliveryMethodSelected)
  const shouldShowDeliveryOptions =
    selectedDeliveryMethod &&
    selectedDeliveryMethod.enabled &&
    isDeliveryMethodOfType('HOME_EXPRESS', selectedDeliveryMethod)

  return deliveryMethods.length ? (
    <div className="DeliveryMethods">
      <CheckoutTitle>{l`Delivery Type`}</CheckoutTitle>
      <div className="DeliveryMethods-options">
        {deliveryMethods.map((deliveryMethod, i) => (
          <DeliveryMethod
            {...deliveryMethod}
            key={deliveryMethod.deliveryType}
            onChange={() => onDeliveryMethodChange(i)}
          />
        ))}
      </div>
      {shouldShowDeliveryOptions && (
        <DeliveryOptions
          deliveryType={selectedDeliveryMethod.deliveryType}
          deliveryCost={selectedDeliveryMethod.cost}
          deliveryOptions={selectedDeliveryMethod.deliveryOptions}
          onChange={onDeliveryOptionsChange.bind(null, 'HOME_EXPRESS')} // eslint-disable-line react/jsx-no-bind
        />
      )}
    </div>
  ) : null
}

DeliveryMethods.propTypes = {
  deliveryMethods: PropTypes.arrayOf(
    PropTypes.shape({
      deliveryType: PropTypes.string,
      selected: PropTypes.bool,
      enabled: PropTypes.bool,
      cost: PropTypes.string,
      label: PropTypes.string,
      additionalDescription: PropTypes.string,
      deliveryOptions: PropTypes.arrayOf(
        PropTypes.shape({
          shipModeId: PropTypes.number,
          dayText: PropTypes.string,
          dateText: PropTypes.string,
          price: PropTypes.string,
          selected: PropTypes.bool,
        })
      ),
    })
  ),
  onDeliveryMethodChange: PropTypes.func,
  onDeliveryOptionsChange: PropTypes.func,
}

DeliveryMethods.defaultProps = {
  deliveryMethods: [],
  onDeliveryMethodChange: () => {},
  onDeliveryOptionsChange: () => {},
}

DeliveryMethods.contextTypes = {
  l: PropTypes.func,
}

export default DeliveryMethods
