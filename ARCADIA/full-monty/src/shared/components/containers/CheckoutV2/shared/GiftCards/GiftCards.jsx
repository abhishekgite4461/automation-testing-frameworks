import PropTypes from 'prop-types'
import { pluck } from 'ramda'
import React, { PureComponent } from 'react'

import { scrollElementIntoView } from '../../../../../lib/scroll-helper'
import { eventBasedAnalytics } from '../../../../../lib/analytics/analytics'

// components
import Accordion from '../../../../common/Accordion/Accordion'
import Button from '../../../../common/Button/Button'
import Input from '../../../../common/FormComponents/Input/Input'
import Message from '../../../../common/FormComponents/Message/Message'
import Price from '../../../../common/Price/Price'
import GiftCard from './GiftCard'
import { GTM_ACTION, GTM_CATEGORY } from '../../../../../analytics'

const fieldType = PropTypes.shape({
  value: PropTypes.string,
  isDirty: PropTypes.bool,
  isTouched: PropTypes.bool,
  isFocused: PropTypes.bool,
})

const giftCardType = PropTypes.shape({
  giftCardId: PropTypes.string,
  giftCardNumber: PropTypes.string,
  amountUsed: PropTypes.string,
})

export default class GiftCards extends PureComponent {
  static propTypes = {
    fields: PropTypes.shape({
      giftCardNumber: fieldType,
      pin: fieldType,
    }).isRequired,
    giftCardNumberError: PropTypes.string,
    pinError: PropTypes.string,
    giftCards: PropTypes.arrayOf(giftCardType),
    total: PropTypes.string,
    errorMessage: PropTypes.string,
    bannerMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    maxNumGiftCards: PropTypes.number,
    validationSchema: PropTypes.shape({
      giftCardNumber: PropTypes.arrayOf(
        PropTypes.oneOfType([PropTypes.func, PropTypes.string])
      ),
      pin: PropTypes.arrayOf(
        PropTypes.oneOfType([PropTypes.func, PropTypes.string])
      ),
    }),
    // functions
    onAddGiftCard: PropTypes.func.isRequired,
    onRemoveGiftCard: PropTypes.func.isRequired,
    hideBanner: PropTypes.func.isRequired,
    setAndValidateField: PropTypes.func.isRequired,
    validate: PropTypes.func.isRequired,
    touchField: PropTypes.func.isRequired,
    setMeta: PropTypes.func.isRequired,
    clearErrors: PropTypes.func.isRequired,
    sendEventAnalytics: PropTypes.func.isRequired,
  }

  static defaultProps = {
    giftCardNumberError: '',
    pinError: '',
    giftCards: [],
    total: '',
    errorMessage: '',
    bannerMessage: '',
    maxNumGiftCards: 5,
    validationSchema: {
      giftCardNumber: [],
      pin: [],
    },
    sendEventAnalytics: eventBasedAnalytics,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  state = {
    isAccordionExpanded: false,
  }

  static scrollToError() {
    scrollElementIntoView(document.querySelector('.GiftCards-message'))
  }

  componentWillMount() {
    this.props.hideBanner()
  }

  componentWillUnmount() {
    this.props.clearErrors()
  }

  componentDidUpdate(prevProps) {
    const { validationSchema, validate } = this.props
    if (prevProps.validationSchema !== validationSchema) {
      validate(validationSchema)
    }
  }

  setAndValidateField = (field) => ({ target: { value } }) => {
    const { validationSchema, setAndValidateField } = this.props
    setAndValidateField(field, value, validationSchema[field])
  }

  touchField = (field) => () => {
    const { touchField, setMeta } = this.props
    touchField(field)
    setMeta('message', {})
  }

  addCardHandler = () => {
    const { onAddGiftCard, fields, sendAnalyticsClickEvent } = this.props
    sendAnalyticsClickEvent({
      category: GTM_CATEGORY.CHECKOUT,
      action: GTM_ACTION.GIFT_CARD,
      label: fields.giftCardNumber.value,
      value: '',
    })
    onAddGiftCard(pluck('value', fields))
  }

  handleAccordionOpen = () => {
    this.props.sendEventAnalytics({
      events: 'event126',
    })
  }

  onAccordionToggle = () => {
    this.setState((state) => ({
      isAccordionExpanded: !state.isAccordionExpanded,
    }))
  }

  render() {
    const { l } = this.context
    const {
      fields,
      giftCardNumberError,
      pinError,
      giftCards,
      total,
      maxNumGiftCards,
      errorMessage,
      bannerMessage,
      onRemoveGiftCard,
    } = this.props
    const message =
      giftCards.length >= maxNumGiftCards
        ? {
            value: l`You have added the maximum number of gift cards for this order.`,
            type: 'message',
          }
        : { value: errorMessage, type: 'error' }

    return (
      <section className="GiftCards">
        <Accordion
          expanded={this.state.isAccordionExpanded}
          header={<h3>{l`Gift card`}</h3>}
          accordionName="giftCard"
          className={'GiftCards-accordion'}
          scrollPaneSelector=".GiftCards"
          noContentPadding
          analyticsOnToggle={this.handleAccordionOpen}
          onAccordionToggle={this.onAccordionToggle}
        >
          {bannerMessage && (
            <Message
              className="GiftCards-banner"
              message={bannerMessage}
              type="confirm"
            />
          )}
          {giftCards.map(({ giftCardId, giftCardNumber, amountUsed }) => (
            <GiftCard
              key={giftCardId}
              cardNumber={giftCardNumber}
              amountUsed={amountUsed}
              onRemove={onRemoveGiftCard.bind(null, giftCardId)} // eslint-disable-line react/jsx-no-bind
            />
          ))}
          {giftCards.length > 0 &&
            total && (
              <div className="GiftCards-row">
                <span className="GiftCards-newTotalLabel">{l`Total left to pay`}</span>
                <span className="GiftCards-newTotalValue">
                  <Price price={total} />
                </span>
              </div>
            )}
          {giftCards.length < maxNumGiftCards && (
            <div>
              <Input
                name="giftCardNumber"
                field={fields.giftCardNumber}
                type="number"
                label={l`Gift card number`}
                placeholder={l`Gift Card Number`}
                errors={{ giftCardNumber: l(giftCardNumberError) }}
                setField={this.setAndValidateField}
                touchedField={this.touchField}
                maxLength={16}
                privacyProtected
              />
              <div className="GiftCards-row">
                <Input
                  name="pin"
                  field={fields.pin}
                  type="number"
                  label={l`PIN`}
                  placeholder={l`Example: XXXX`}
                  errors={{ pin: l(pinError) }}
                  setField={this.setAndValidateField}
                  touchedField={this.touchField}
                  privacyProtected
                />
                <Button
                  clickHandler={this.addCardHandler}
                  isDisabled={!!(giftCardNumberError || pinError)}
                >{l`Apply card`}</Button>
              </div>
            </div>
          )}
          {message.value && (
            <Message
              className="GiftCards-message"
              message={message.value}
              type={message.type}
              onDidMount={GiftCards.scrollToError}
            />
          )}
        </Accordion>
      </section>
    )
  }
}
