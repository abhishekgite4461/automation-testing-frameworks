import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { openMiniBag } from '../../../../actions/common/shoppingBagActions'
import QuickViewOrderSummary from '../../../common/QuickViewOrderSummary/QuickViewOrderSummary'

const mapDispatchToProps = { openMiniBag }

@connect(
  null,
  mapDispatchToProps
)
export default class CheckoutContentContainer extends Component {
  static contextTypes = {
    l: PropTypes.func,
  }

  componentDidMount() {
    if (window) window.scrollTo(0, 0)
  }

  render() {
    const { children, openMiniBag } = this.props
    const { l } = this.context

    return (
      <section className="CheckoutContentContainer">
        <QuickViewOrderSummary openMiniBag={openMiniBag} l={l} />
        <div className="CheckoutContentContainer-content">{children}</div>
      </section>
    )
  }
}
