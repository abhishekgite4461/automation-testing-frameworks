import testComponentHelper from 'test/unit/helpers/test-component'

import { WrappedFindAddress } from '../FindAddress'

beforeEach(() => jest.clearAllMocks())

describe('<FindAddress/>', () => {
  const requiredProps = {
    formNames: {
      address: 'yourAddress',
      details: 'yourDetails',
      findAddress: 'findAddress',
    },
    findAddressForm: {
      fields: {},
      errors: {},
      message: {},
    },
    addressForm: {
      fields: {},
      errors: {},
      message: {},
    },
    detailsForm: {
      fields: {},
      errors: {},
      message: {},
    },
    findAddress: () => {},
    getAddressByMoniker: () => {},
    setManualAddressMode: () => {},
    setFormField: () => {},
    setAndValidateFormField: () => {},
    touchFormField: () => {},
    validateForm: () => {},
    clearFormErrors: () => {},
    clearFormFieldError: () => {},
    setGenericError: () => {},
    renderTelephone: true,
  }
  const renderComponent = testComponentHelper(WrappedFindAddress)

  describe('@render', () => {
    it('render default status', () => {
      const { getTree } = renderComponent(requiredProps)
      expect(getTree()).toMatchSnapshot()
    })

    it('Find Address not visible', () => {
      const { getTree } = renderComponent({
        ...requiredProps,
        findAddressIsVisible: false,
      })
      expect(getTree()).toMatchSnapshot()
    })

    it('test with no country on yourAddressForm', () => {
      const { getTree } = renderComponent({
        ...requiredProps,
        addressForm: {
          fields: {
            country: {
              value: '',
            },
          },
        },
      })
      expect(getTree()).toMatchSnapshot()
    })

    it('test with no country on yourAddressForm', () => {
      const { getTree } = renderComponent({
        ...requiredProps,
        addressForm: {
          fields: {},
          message: {
            message: test,
          },
        },
      })
      expect(getTree()).toMatchSnapshot()
    })

    it('test No postCode Rules', () => {
      const { getTree } = renderComponent({
        ...requiredProps,
        addressForm: {
          fields: {
            country: {
              value: 'United States',
            },
          },
        },
      })
      expect(getTree()).toMatchSnapshot()
    })

    it('hides the title when titleHidden property is true', () => {
      const { wrapper } = renderComponent({
        ...requiredProps,
        titleHidden: true,
      })
      expect(wrapper.find('.FindAddress-heading').length).toBe(0)
    })
  })

  describe('@lifecycle', () => {
    describe('@componentDidMount', () => {
      it('should set country', () => {
        const setFormFieldMock = jest.fn()
        const { instance } = renderComponent({
          ...requiredProps,
          country: 'France',
          setFormField: setFormFieldMock,
        })
        instance.componentDidMount()

        expect(setFormFieldMock).toHaveBeenCalledWith(
          'yourAddress',
          'country',
          'France'
        )
      })

      it('should validate find address and your details forms', () => {
        const validateFormMock = jest.fn()
        const findAddressValidationSchema = {}
        const detailsValidationSchema = {}
        const { instance } = renderComponent({
          ...requiredProps,
          findAddressValidationSchema,
          detailsValidationSchema,
          validateForm: validateFormMock,
        })
        instance.componentDidMount()

        expect(validateFormMock).toHaveBeenCalledWith(
          'findAddress',
          findAddressValidationSchema
        )
        expect(validateFormMock).toHaveBeenCalledWith(
          'yourDetails',
          detailsValidationSchema
        )
      })
    })

    describe('@componentDidUpdate', () => {
      it('should validate if `schemaHash` changes', () => {
        const validateFormMock = jest.fn()
        const findAddressValidationSchema = {}
        const detailsValidationSchema = {}
        const { instance } = renderComponent({
          ...requiredProps,
          schemaHash: '12345',
          findAddressValidationSchema,
          detailsValidationSchema,
          validateForm: validateFormMock,
        })
        instance.componentDidUpdate({ schemaHash: '67890' })

        expect(validateFormMock).toHaveBeenCalledWith(
          'findAddress',
          findAddressValidationSchema
        )
        expect(validateFormMock).toHaveBeenCalledWith(
          'yourDetails',
          detailsValidationSchema
        )
      })
    })

    describe('@componentWillUnmount', () => {
      it('should clear errors', () => {
        const clearFormErrorsMock = jest.fn()
        const { instance } = renderComponent({
          ...requiredProps,
          clearFormErrors: clearFormErrorsMock,
        })
        instance.componentWillUnmount()

        expect(clearFormErrorsMock).toHaveBeenCalledWith('findAddress')
        expect(clearFormErrorsMock).toHaveBeenCalledWith('yourDetails')
      })
    })
  })

  describe('@Event and functions', () => {
    // const props = { ...mockProps, ...mockCallback, findAddressState: { monikers: [{ moniker: 'fe45d' }] } }
    describe('Select Address', () => {
      it('calls getAddressByMoniker', () => {
        const getAddressByMonikerMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          countryCode: 'UK',
          monikers: [
            {
              moniker: '111 Something Close',
            },
          ],
          getAddressByMoniker: getAddressByMonikerMock,
        })
        wrapper.find('.FindAddress-selectAddress').prop('onChange')({
          target: { selectedIndex: 1 },
        })

        expect(getAddressByMonikerMock).toHaveBeenCalledWith(
          {
            country: 'UK',
            moniker: '111 Something Close',
          },
          'yourAddress'
        )
      })
    })

    describe('Select Country', () => {
      it('change Country call setFieldCountry update', () => {
        const sendEventAnalyticsMock = jest.fn()
        const setFormFieldMock = jest.fn()
        const onSelectCountryMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          setFormField: setFormFieldMock,
          onSelectCountry: onSelectCountryMock,
          sendEventAnalytics: sendEventAnalyticsMock,
        })
        wrapper.find('.FindAddress-country').prop('onChange')({
          target: { value: 'France' },
        })

        expect(onSelectCountryMock).toHaveBeenCalledWith('France')
        expect(setFormFieldMock).toHaveBeenCalledWith(
          'yourAddress',
          'country',
          'France'
        )
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event135',
        })
      })

      it('should call `validateDDPForCountry` if selecting country for delivery address', () => {
        const validateDDPForCountryMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          validateDDPForCountry: validateDDPForCountryMock,
          addressType: 'delivery',
        })
        wrapper.find('.FindAddress-country').prop('onChange')({
          target: { value: 'France' },
        })

        expect(validateDDPForCountryMock).toHaveBeenCalledWith('France')
      })

      it('should not call `validateDDPForCountry` if selecting country for address other than delivery', () => {
        const validateDDPForCountryMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          validateDDPForCountry: validateDDPForCountryMock,
          addressType: 'billing',
        })
        wrapper.find('.FindAddress-country').prop('onChange')({
          target: { value: 'France' },
        })

        expect(validateDDPForCountryMock).not.toHaveBeenCalled()
      })
    })

    describe('Enter Address Manually', () => {
      it('should send analytics event `event122` on click', () => {
        const sendEventAnalyticsMock = jest.fn()
        const setManualAddressModeMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          setManualAddressMode: setManualAddressModeMock,
          sendEventAnalytics: sendEventAnalyticsMock,
        })
        wrapper.find('.FindAddress-link').prop('onClick')()

        expect(setManualAddressModeMock).toHaveBeenCalled()
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event122',
        })
      })
    })

    describe('Telephone', () => {
      it('should set and validate field on change', () => {
        const setAndValidateFormFieldMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          detailsValidationSchema: {
            telephone: ['required'],
          },
          setAndValidateFormField: setAndValidateFormFieldMock,
        })
        wrapper.find('.FindAddress-telephone').prop('setField')('telephone')({
          target: {
            value: '0818118181',
          },
        })
        expect(setAndValidateFormFieldMock).toHaveBeenCalledWith(
          'yourDetails',
          'telephone',
          '0818118181',
          ['required']
        )
      })

      it('should be able to touch field', () => {
        const touchFormFieldMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          touchFormField: touchFormFieldMock,
        })
        wrapper.find('.FindAddress-telephone').prop('touchedField')(
          'telephone'
        )()
        expect(touchFormFieldMock).toHaveBeenCalledWith(
          'yourDetails',
          'telephone'
        )
      })
    })

    describe('Delivery Country', () => {
      it('should send analytics event `event135` on change', () => {
        const sendEventAnalyticsMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          sendEventAnalytics: sendEventAnalyticsMock,
        })
        wrapper.find('.FindAddress-country').prop('onChange')({
          target: {},
        })
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event135',
        })
      })
    })

    describe('Postcode', () => {
      it('should send analytics event `event137` when focus leaves', () => {
        const sendEventAnalyticsMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          sendEventAnalytics: sendEventAnalyticsMock,
        })
        wrapper.find('.FindAddress-postCode').prop('touchedField')('postCode')()
        expect(sendEventAnalyticsMock).toHaveBeenCalledWith({
          events: 'event137',
        })
      })

      it('should updated with trimmed postcode on blur', () => {
        const setAndValidateFormFieldMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          setAndValidateFormField: setAndValidateFormFieldMock,
          findAddressValidationSchema: { postCode: 'postcodeValidation' },
        })
        wrapper
          .find('.FindAddress-postCode')
          .simulate('blur', { target: { value: '  abc  ' } })
        expect(setAndValidateFormFieldMock).toHaveBeenCalledWith(
          'findAddress',
          'postCode',
          'abc',
          'postcodeValidation'
        )
      })
    })
  })
})
