import DeliveryMethodsContainer, {
  mapDispatchToProps,
} from '../DeliveryMethodsContainer'

describe('<DeliveryMethodsContainer />', () => {
  it('should wrap `DeliveryMethods` component', () => {
    expect(DeliveryMethodsContainer.WrappedComponent.name).toBe(
      'DeliveryMethods'
    )
  })

  describe('mapDispatchToProps', () => {
    describe('onDeliveryMethodChange', () => {
      it('should use `selectDeliveryType` action for `onDeliveryMethodChange`', () => {
        const { onDeliveryMethodChange } = mapDispatchToProps
        expect(onDeliveryMethodChange.name).toBe('selectDeliveryType')
      })
    })

    describe('onDeliveryOptionsChange', () => {
      it('should dispatch correct `putOrderSummary` action', () => {
        const { onDeliveryOptionsChange } = mapDispatchToProps
        const putOrderSummaryMock = jest.fn(() => 'putOrderSummaryAction')
        const dispatchMock = jest.fn()
        const getStateMock = () => ({
          checkout: {
            orderSummary: {
              shippingCountry: 'United Kingdom',
              basket: {
                orderId: 1740991,
              },
            },
          },
        })
        const deliveryTypeName = 'HOME_EXPRESS'
        const shipModeId = 28005
        onDeliveryOptionsChange(deliveryTypeName, shipModeId, {
          putOrderSummary: putOrderSummaryMock,
        })(dispatchMock, getStateMock)
        expect(putOrderSummaryMock).toHaveBeenCalledWith({
          orderId: 1740991,
          shippingCountry: 'United Kingdom',
          deliveryType: 'HOME_EXPRESS',
          shipModeId: 28005,
        })
        expect(dispatchMock).toHaveBeenCalledWith('putOrderSummaryAction')
      })
    })
  })
})
