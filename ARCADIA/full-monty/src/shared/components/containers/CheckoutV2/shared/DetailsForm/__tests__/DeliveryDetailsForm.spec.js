import testComponentHelper from 'test/unit/helpers/test-component'

import DeliveryDetailsForm from '../DeliveryDetailsForm'

describe('<DeliveryDetailsForm />', () => {
  const renderComponent = testComponentHelper(DeliveryDetailsForm)

  describe('@renders', () => {
    it('should render default state', () => {
      expect(renderComponent({}).getTree()).toMatchSnapshot()
    })
  })
})
