import PropTypes from 'prop-types'
import React from 'react'

const PaymentMethodOption = ({
  label,
  description,
  value,
  icons,
  isChecked,
  onChange,
}) => {
  return (
    <label // eslint-disable-line jsx-a11y/label-has-for
      className="PaymentMethodOption RadioButton"
    >
      <input
        className="RadioButton-input"
        type="radio"
        value={value}
        onChange={onChange}
        checked={isChecked}
      />
      <span className="PaymentMethodOption-content RadioButton-content">
        <span className="PaymentMethodOption-label">{label}</span>
        {description && (
          <span className="PaymentMethodOption-description">{description}</span>
        )}
      </span>
      {icons.length > 0 && (
        <span className="PaymentMethodOption-icons">
          {icons.map((icon) => (
            <img
              className="PaymentMethodOption-icon"
              key={icon}
              src={`/assets/common/images/${icon}`}
              alt=""
            />
          ))}
        </span>
      )}
    </label>
  )
}

PaymentMethodOption.propTypes = {
  label: PropTypes.string.isRequired,
  description: PropTypes.string,
  value: PropTypes.string.isRequired,
  icons: PropTypes.arrayOf(PropTypes.string),
  isChecked: PropTypes.bool,
  onChange: PropTypes.func,
}

PaymentMethodOption.defaultProps = {
  description: '',
  icons: [],
  isChecked: false,
  onChange: () => {},
}

export default PaymentMethodOption
