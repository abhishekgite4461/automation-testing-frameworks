import React from 'react'
import testComponentHelper from 'test/unit/helpers/test-component'
import DigitalDeliveryPass from '../DigitalDeliveryPass'

const initialProps = {
  brandCode: 'br',
}

describe('<DigitalDeliveryPass />', () => {
  const renderComponent = testComponentHelper(DigitalDeliveryPass)

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent().getTree()).toMatchSnapshot()
    })

    it('should be expanded with loader displayed by default', () => {
      const { wrapper } = renderComponent(initialProps)
      expect(wrapper.prop('expanded')).toBe(true)
      expect(wrapper.prop('showLoader')).toBe(true)
    })

    it('should render the default title for accordion', () => {
      const { wrapper } = renderComponent(initialProps)
      expect(wrapper.find('.DigitalDeliveryPass').prop('header')).toEqual(
        <h3 className="DigitalDeliveryPass-header">Digital Delivery Pass</h3>
      )
    })
  })

  describe('@instance methods', () => {
    describe('onContentLoaded', () => {
      it('updates Accordion to not show loader', () => {
        const { wrapper, instance } = renderComponent(initialProps)
        expect(wrapper.prop('showLoader')).toBe(true)
        instance.onContentLoaded()
        wrapper.update()
        expect(wrapper.prop('showLoader')).toBe(false)
      })
    })
  })
})
