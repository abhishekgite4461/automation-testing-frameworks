import PropTypes from 'prop-types'
import React, { Component } from 'react'

// components
import Input from '../../../../../common/FormComponents/Input/Input'
import CVVInfo from './CVVInfo'

export default class CVVField extends Component {
  static propTypes = {
    field: PropTypes.shape({
      value: PropTypes.string,
      isTouched: PropTypes.bool,
    }).isRequired,
    error: PropTypes.string,
    className: PropTypes.string,
    isMobile: PropTypes.bool,
    // functions
    setField: PropTypes.func.isRequired,
    touchedField: PropTypes.func.isRequired,
    showModal: PropTypes.func.isRequired,
    paymentType: PropTypes.string,
  }

  static defaultProps = {
    error: '',
    className: '',
    isMobile: false,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  state = {
    showMobileCvvInfo: false,
  }

  handleCvvHelp = () => {
    const { isMobile, showModal } = this.props
    if (!isMobile) {
      showModal(<CVVInfo />, { mode: 'cvvInfo' })
    } else {
      this.setState({
        showMobileCvvInfo: !this.state.showMobileCvvInfo,
      })
    }
  }

  render() {
    const { l } = this.context
    const {
      field,
      error,
      className,
      paymentType,
      setField,
      touchedField,
    } = this.props
    const cvvInfoMessage =
      paymentType === 'AMEX'
        ? 'CVV number can be found on the front of your card'
        : 'CVV number can be found on the back of your card next to the signature'
    return (
      <div className={`CVVField${className ? ` ${className}` : ''}`}>
        <div className="CVVField-row">
          <Input
            className="CVVField-cvv"
            field={field}
            name="cvv"
            label={l`CVV`}
            type="text"
            placeholder={l`Eg. ***`}
            errors={{ cvv: l(error) }}
            setField={setField}
            touchedField={touchedField}
            isRequired
          />
          <button
            className="CVVField-link"
            onClick={this.handleCvvHelp}
          >{l`What's this?`}</button>
        </div>
        {this.state.showMobileCvvInfo && (
          <p className="CVVField-cvvInfo">{l(cvvInfoMessage)}</p>
        )}
      </div>
    )
  }
}
