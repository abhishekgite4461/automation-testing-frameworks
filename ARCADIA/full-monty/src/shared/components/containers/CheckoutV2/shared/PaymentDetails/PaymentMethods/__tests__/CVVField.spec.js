import testComponentHelper from 'test/unit/helpers/test-component'

import CVVField from '../CVVField'

describe('<CVVField/>', () => {
  const renderComponent = testComponentHelper(CVVField)

  const showModalStub = jest.fn()

  const baseProps = {
    field: {
      value: '123',
      isTouched: true,
    },
    setField: () => {},
    touchedField: () => {},
    showModal: showModalStub,
  }

  describe('@renders', () => {
    it('should render default state correctly', () => {
      const component = renderComponent(baseProps)
      expect(component.getTree()).toMatchSnapshot()
    })

    it('should pass errors to `Input`', () => {
      const component = renderComponent({
        ...baseProps,
        error: '3 digits required',
      })
      expect(component.wrapper.find('Connect(Input)').prop('errors')).toEqual({
        cvv: '3 digits required',
      })
    })

    it('should localise CVV label, placeholder, error message and help link', () => {
      const renderComponentWithContect = testComponentHelper(CVVField, {
        context: {
          l: () => 'bonjour',
        },
      })
      const { wrapper } = renderComponentWithContect({
        ...baseProps,
        error: '3 digits required',
      })
      const input = wrapper.find('Connect(Input)')
      expect(input.prop('label')).toBe('bonjour')
      expect(input.prop('placeholder')).toBe('bonjour')
      expect(input.prop('errors')).toEqual({
        cvv: 'bonjour',
      })
      expect(wrapper.find('.CVVField-link').text()).toBe('bonjour')
    })

    it('should add provided `className` to the element', () => {
      const { wrapper } = renderComponent({
        ...baseProps,
        className: 'test-class-name',
      })
      expect(wrapper.find('.CVVField.test-class-name').length).toBe(1)
    })
  })

  describe('@events', () => {
    describe('When the cvv help text is clicked', () => {
      it('should call `showModal` when the cvv help text is clicked', () => {
        const component = renderComponent(baseProps)
        component.wrapper.find('.CVVField-link').simulate('click')
        expect(showModalStub).toHaveBeenCalled()
      })

      it('should display help text when the cvv help text is clicked and in mobile', () => {
        const { wrapper } = renderComponent({
          ...baseProps,
          isMobile: true,
        })
        wrapper.find('.CVVField-link').simulate('click')
        expect(wrapper.find('.CVVField-cvvInfo').length).toBe(1)
      })
    })
  })
})
