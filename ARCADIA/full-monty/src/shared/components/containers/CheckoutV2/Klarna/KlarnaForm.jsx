import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Button from '../../../common/Button/Button'
import {
  hashOrderSummary,
  assembleAddressPayload,
} from '../../../../lib/checkout-utilities/klarna-utils'
import {
  createSession,
  loadForm,
  authorizeV2,
  updateAndAuthorizeV2,
} from '../../../../actions/common/klarnaActions'

@connect((state) => ({
  config: state.config,
  klarna: state.klarna,
  checkoutForms: state.forms.checkout,
  user: state.account.user,
  shoppingBag: state.shoppingBag,
  orderSummary: state.checkout.orderSummary,
  klarnaForm: state.forms.klarna,
}))
export default class KlarnaForm extends Component {
  static propTypes = {
    klarna: PropTypes.object.isRequired,
    shoppingBag: PropTypes.object.isRequired,
    orderSummary: PropTypes.object.isRequired,
    config: PropTypes.object,
    checkoutForms: PropTypes.object,
    user: PropTypes.object,
    klarnaForm: PropTypes.object,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentDidMount() {
    const {
      dispatch,
      shoppingBag: {
        bag: { orderId },
      },
      klarna: { clientToken, sessionId },
    } = this.props
    if (!sessionId && orderId)
      return dispatch(createSession({ orderId, ...this.getStoreDetails() }))
    if (sessionId && clientToken)
      return dispatch(
        loadForm(clientToken, document.querySelector('.KlarnaForm-container'))
      )
  }

  getStoreDetails = (isCreateSession = true) => {
    const { orderSummary } = this.props
    if (isCreateSession)
      return orderSummary.storeDetails ? { deliveryType: 'store' } : {}
    if (!isCreateSession)
      return orderSummary.storeDetails
        ? { deliveryType: 'store' }
        : { deliveryType: 'home' }
  }

  authorizeHandler = () => {
    const {
      dispatch,
      user,
      config,
      klarnaForm,
      shoppingBag,
      shoppingBag: {
        bag: { orderId },
      },
      orderSummary,
      klarna: { orderSummaryHash },
      checkoutForms,
    } = this.props
    const hash =
      orderSummaryHash ||
      (process.browser && window.localStorage.getItem('orderHash'))
    const newHash = hashOrderSummary({
      ...checkoutForms,
      shoppingBag,
      orderSummary,
    })
    const addressPayload = assembleAddressPayload({
      ...checkoutForms,
      klarnaForm,
      user,
      config,
      orderSummary,
    })
    if (hash && hash !== newHash) {
      return dispatch(
        updateAndAuthorizeV2(
          { orderId, ...this.getStoreDetails(false) },
          addressPayload,
          newHash
        )
      )
    }
    return dispatch(authorizeV2(addressPayload))
  }

  render() {
    const {
      klarna: { authorizationToken, shown },
    } = this.props
    const { l } = this.context

    return (
      <section className="KlarnaForm">
        <div className="KlarnaForm-container" />
        {!authorizationToken &&
          shown && (
            <Button clickHandler={this.authorizeHandler}>
              {l`Check eligibility`}
            </Button>
          )}
      </section>
    )
  }
}
