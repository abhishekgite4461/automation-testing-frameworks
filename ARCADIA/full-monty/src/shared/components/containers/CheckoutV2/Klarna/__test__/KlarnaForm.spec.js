import React from 'react'
import { shallow } from 'enzyme'
import configureStore from 'redux-mock-store'
import testComponentHelper from 'test/unit/helpers/test-component'
import KlarnaForm from '../KlarnaForm'

jest.mock('../../../../../actions/common/klarnaActions', () => ({
  createSession: jest.fn(),
  loadForm: jest.fn(),
  authorizeV2: jest.fn(),
  updateAndAuthorizeV2: jest.fn(),
}))

jest.mock('../../../../../lib/checkout-utilities/klarna-utils', () => ({
  hashOrderSummary: jest.fn(),
  assembleAddressPayload: jest.fn(),
}))

import {
  createSession,
  loadForm,
  authorizeV2,
  updateAndAuthorizeV2,
} from '../../../../../actions/common/klarnaActions'
import {
  hashOrderSummary,
  assembleAddressPayload,
} from '../../../../../lib/checkout-utilities/klarna-utils'

describe('<KlarnaForm />', () => {
  beforeEach(() => {
    jest.resetAllMocks()
  })
  const renderComponent = testComponentHelper(KlarnaForm.WrappedComponent)
  const initialProps = {
    klarna: {},
    shoppingBag: {
      bag: {
        orderId: 1566239,
      },
    },
    orderSummary: {},
  }

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })

    it('shows check eligibility button', () => {
      expect(
        renderComponent({
          ...initialProps,
          klarna: {
            shown: true,
          },
        }).getTree()
      ).toMatchSnapshot()
    })
  })

  describe('Connected component', () => {
    it('should render connected component correctly', () => {
      const initialState = {
        config: {},
        klarna: {},
        forms: { checkout: {}, klarna: {} },
        account: { user: {} },
        shoppingBag: {},
        checkout: { orderSummary: {} },
      }
      const container = shallow(
        <KlarnaForm store={configureStore()(initialState)} />
      )
      expect(container).toBeTruthy()
    })
  })

  describe('@lifecycle', () => {
    describe('componentDidMount', () => {
      it('should call createSession if no sessionId and if there is an orderId', () => {
        const dispatch = jest.fn()
        const { instance } = renderComponent({
          ...initialProps,
          dispatch,
        })
        instance.getStoreDetails = jest.fn()
        expect(dispatch).not.toHaveBeenCalled()
        expect(createSession).not.toHaveBeenCalled()
        instance.componentDidMount()
        expect(instance.getStoreDetails).toHaveBeenCalledTimes(1)
        expect(dispatch).toHaveBeenCalledTimes(1)
        expect(createSession).toHaveBeenCalledTimes(1)
        expect(createSession).toHaveBeenCalledWith({
          orderId: initialProps.shoppingBag.bag.orderId,
        })
      })

      it('should not call createSession if no sessionId and if there is not an orderId', () => {
        const dispatch = jest.fn()
        const { instance } = renderComponent({
          ...initialProps,
          shoppingBag: {
            ...initialProps.shoppingBag,
            bag: {
              ...initialProps.shoppingBag.bag,
              orderId: 0,
            },
          },
          dispatch,
        })
        instance.getStoreDetails = jest.fn()
        expect(dispatch).not.toHaveBeenCalled()
        expect(createSession).not.toHaveBeenCalled()
        instance.componentDidMount()
        expect(instance.getStoreDetails).not.toHaveBeenCalled()
        expect(dispatch).not.toHaveBeenCalled()
        expect(createSession).not.toHaveBeenCalled()
      })

      it('should call loadForm if there is a sessionId and clientToken', () => {
        const dispatch = jest.fn()
        const klarna = {
          clientToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Ij',
          sessionId: '068df369-13a7-4d47-a564-62f8408bb760',
        }
        const { instance } = renderComponent({
          ...initialProps,
          klarna,
          dispatch,
        })
        document.querySelector = jest.fn()
        expect(document.querySelector).not.toHaveBeenCalled()
        expect(dispatch).not.toHaveBeenCalled()
        expect(loadForm).not.toHaveBeenCalled()
        instance.componentDidMount()
        expect(document.querySelector).toHaveBeenCalledTimes(1)
        expect(document.querySelector).toHaveBeenCalledWith(
          '.KlarnaForm-container'
        )
        expect(dispatch).toHaveBeenCalledTimes(1)
        expect(loadForm).toHaveBeenCalledTimes(1)
        expect(loadForm).toHaveBeenCalledWith(klarna.clientToken, undefined)
      })
    })
  })

  describe('@methods', () => {
    describe('getStoreDetails', () => {
      it('should return an empty object if session and no store details', () => {
        const { instance } = renderComponent({
          ...initialProps,
          orderSummary: {},
        })
        const result = instance.getStoreDetails(true)
        expect(result).toEqual({})
      })

      it('should return an empty object if session and no store details', () => {
        const { instance } = renderComponent({
          ...initialProps,
          orderSummary: {},
        })
        const result = instance.getStoreDetails()
        expect(result).toEqual({})
      })

      it('should return deliveryType store if session and store details', () => {
        const { instance } = renderComponent({
          ...initialProps,
          orderSummary: {
            storeDetails: {},
          },
        })
        const result = instance.getStoreDetails(true)
        expect(result).toEqual({ deliveryType: 'store' })
      })

      it('should return deliveryType home if no session and no store details', () => {
        const { instance } = renderComponent({
          ...initialProps,
          orderSummary: {},
        })
        const result = instance.getStoreDetails(false)
        expect(result).toEqual({ deliveryType: 'home' })
      })

      it('should return deliveryType store if no session and store details', () => {
        const { instance } = renderComponent({
          ...initialProps,
          orderSummary: {
            storeDetails: {},
          },
        })
        const result = instance.getStoreDetails(false)
        expect(result).toEqual({ deliveryType: 'store' })
      })
    })
  })

  describe('@events', () => {
    describe('authorizeHandler', () => {
      it('should call hashOrderSummary', () => {
        const { instance } = renderComponent({
          ...initialProps,
          dispatch: jest.fn(),
        })
        expect(hashOrderSummary).not.toHaveBeenCalled()
        instance.authorizeHandler()
        expect(hashOrderSummary).toHaveBeenCalledTimes(1)
        expect(hashOrderSummary).toHaveBeenCalledWith({
          orderSummary: {},
          shoppingBag: {
            bag: {
              orderId: 1566239,
            },
          },
        })
      })

      it('should call assembleAddressPayload', () => {
        const { instance } = renderComponent({
          ...initialProps,
          dispatch: jest.fn(),
        })
        expect(assembleAddressPayload).not.toHaveBeenCalled()
        instance.authorizeHandler()
        expect(assembleAddressPayload).toHaveBeenCalledTimes(1)
        expect(assembleAddressPayload).toHaveBeenCalledWith({
          config: undefined,
          klarnaForm: undefined,
          orderSummary: {},
          user: undefined,
        })
      })

      it('should call authorizeV2 if hash has not changed', () => {
        const { instance } = renderComponent({
          ...initialProps,
          dispatch: jest.fn(),
        })
        expect(authorizeV2).not.toHaveBeenCalled()
        instance.authorizeHandler()
        expect(authorizeV2).toHaveBeenCalledTimes(1)
      })

      it('should call updateAndAuthorizeV2 when hash has changed', () => {
        const { instance } = renderComponent({
          ...initialProps,
          dispatch: jest.fn(),
        })
        global.process.browser = true
        instance.getStoreDetails = jest.fn()
        const hash = 'fd7fa5608912fedf866068a24aa3a320'
        global.window.localStorage.setItem('orderHash', hash)
        expect(updateAndAuthorizeV2).not.toHaveBeenCalled()
        expect(instance.getStoreDetails).not.toHaveBeenCalled()
        instance.authorizeHandler()
        expect(instance.getStoreDetails).toHaveBeenCalledTimes(1)
        expect(instance.getStoreDetails).toHaveBeenCalledWith(false)
        expect(updateAndAuthorizeV2).toHaveBeenCalledTimes(1)
        expect(updateAndAuthorizeV2).toHaveBeenCalledWith(
          { orderId: 1566239 },
          undefined,
          undefined
        )
      })
    })
  })
})
