import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  clearOrderSummaryBasket,
  getOrderSummary,
  getAccountAndOrderSummary,
} from '../../../actions/common/checkoutActions'
import { sessionReset } from '../../../actions/common/sessionActions'
import CheckoutMessage from '../../common/CheckoutMessage/CheckoutMessage'
import CheckoutBagSide from '../../common/CheckoutBagSide/CheckoutBagSide'
import CheckoutProgressTracker from '../CheckoutV2/shared/CheckoutProgressTracker'
import { getAllPaymentMethods } from '../../../actions/common/paymentMethodsActions'

@connect(
  (state) => ({
    checkout: state.checkout,
    brandCode: state.config.storeCode,
    errorSession: state.errorSession,
    location: state.routing.location,
    orderSummary: state.checkout.orderSummary,
    visited: state.routing.visited,
  }),
  {
    clearOrderSummaryBasket,
    getOrderSummary,
    sessionReset,
    getAllPaymentMethods,
  }
)
export default class CheckoutContainer extends Component {
  static propTypes = {
    checkout: PropTypes.object.isRequired,
    children: PropTypes.node,
    errorSession: PropTypes.object,
    location: PropTypes.object.isRequired,
    orderSummary: PropTypes.object.isRequired,
    visited: PropTypes.array,
    clearOrderSummaryBasket: PropTypes.func.isRequired,
    getOrderSummary: PropTypes.func.isRequired,
    sessionReset: PropTypes.func.isRequired,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  // When navigating to checkout out we assume that you have a valid session
  // The sessionExpired error state might not have been cleared, as a guard the sessionError state is reset.
  componentWillMount() {
    const { clearOrderSummaryBasket, sessionReset, visited } = this.props
    sessionReset()
    if (process.browser && visited.length > 1) clearOrderSummaryBasket()
  }

  componentDidMount() {
    const {
      getOrderSummary,
      location,
      visited,
      getAllPaymentMethods,
    } = this.props
    const inDeliveryAndPayment = location.pathname.endsWith('/delivery-payment')

    if (visited.length > 1) {
      getOrderSummary({
        shouldUpdateBag: !this.isLogin(location),
        shouldUpdateForms: inDeliveryAndPayment,
      })
      getAllPaymentMethods()
    }
  }

  componentWillUnmount() {
    window.onpopstate = null
  }

  isLogin = ({ pathname }) => pathname.includes('login')

  isCollectFromStore = (location) =>
    location.pathname.startsWith('/checkout/delivery/collect-from-store')

  sendForm = () => {
    document.getElementById('paymentForm').submit()
  }

  static needs = [getAccountAndOrderSummary, getAllPaymentMethods]

  render() {
    const {
      checkout,
      children,
      location,
      errorSession,
      orderSummary,
    } = this.props
    const inCollectFromStore = this.isCollectFromStore(location)
    const isLogin = this.isLogin(location)
    const isSessionError = errorSession && errorSession.sessionExpired
    const hideBreadCrumbs = isLogin || inCollectFromStore
    return (
      <section
        role="main"
        className={`CheckoutContainer ${
          inCollectFromStore ? 'CheckoutContainer-collectFromStore' : ''
        }`}
      >
        <div
          className={`CheckoutContainer-row ${
            hideBreadCrumbs ? 'is-hidden' : ''
          }`}
        >
          <div className="CheckoutContainer-fullWidth">
            <CheckoutProgressTracker />
          </div>
        </div>
        <div className="CheckoutContainer-row">
          <div className="CheckoutContainer-groupLeft">
            {!isLogin && !isSessionError && <CheckoutMessage />}
            {checkout.verifyPayment && (
              <div
                ref={() => this.sendForm()}
                // eslint-disable-next-line react/no-danger
                dangerouslySetInnerHTML={{ __html: checkout.verifyPayment }}
              />
            )}
            {!isSessionError && children}
          </div>
          {!isLogin &&
            orderSummary.basket && (
              <div className="CheckoutContainer-groupRight">
                <CheckoutBagSide orderSummary={orderSummary} showDiscounts />
              </div>
            )}
        </div>
      </section>
    )
  }
}
