import PropTypes from 'prop-types'
import React from 'react'

// components
import RadioButton from '../../../common/FormComponents/RadioButton/RadioButton'
import CheckoutTitle from '../shared/CheckoutTitle'

const DeliveryOptions = (
  { deliveryLocations, onChangeDeliveryLocation },
  { l }
) => {
  return (
    <article className="DeliveryOptions" aria-label="Delivery Location Options">
      <CheckoutTitle>{l`Delivery Options`}</CheckoutTitle>
      {deliveryLocations.map((location, index) => (
        <div
          className={`DeliveryOption DeliveryOption--${location.deliveryLocationType.toLowerCase()}`}
          key={location.deliveryLocationType}
        >
          <RadioButton
            id={`delivery-option-${location.deliveryLocationType.toLowerCase()}`}
            checked={location.selected}
            label={l(location.label)}
            name="deliveryLocationType"
            fullWidth
            onChange={onChangeDeliveryLocation.bind(null, location, index)} // eslint-disable-line react/jsx-no-bind
            isDisabled={location.disabled}
          >
            <span className="DeliveryOption-content">
              <span className="DeliveryOption-title h3">
                {l(location.title)}
              </span>
              <span className="DeliveryOption-description">
                {l(location.description)}
              </span>
              <span className="DeliveryOption-additionalDescription">
                {l(location.additionalDescription)}
              </span>
            </span>
          </RadioButton>
        </div>
      ))}
    </article>
  )
}

DeliveryOptions.propTypes = {
  deliveryLocations: PropTypes.arrayOf(
    PropTypes.shape({
      deliveryLocationType: PropTypes.string.isRequired,
      selected: PropTypes.boolean,
      label: PropTypes.string,
      iconUrl: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      description: PropTypes.string.description,
      additionalDescription: PropTypes.string.description,
      disabled: PropTypes.boolean,
    })
  ),
  onChangeDeliveryLocation: PropTypes.func,
}

DeliveryOptions.contextTypes = {
  l: PropTypes.func,
}

export default DeliveryOptions
