import PropTypes from 'prop-types'
import { isEmpty } from 'ramda'
import React, { Component } from 'react'
import Helmet from 'react-helmet'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'

// components
import QuickViewOrderSummary from '../../../common/QuickViewOrderSummary/QuickViewOrderSummary'
import CollectFromStore from '../../../common/CollectFromStore/CollectFromStore'
import HomeDelivery from './HomeDelivery'
import StoreDelivery from './StoreDelivery'
import DeliveryOptionsContainer from '../shared/DeliveryOptionsContainer'
import TotalCost from '../shared/TotalCost'
import espotsDesktopConstants from '../../../../constants/espotsDesktop'
import Espot from '../../../containers/Espot/Espot'
import DigitalDeliveryPass from '../shared/DigitalDeliveryPass/DigitalDeliveryPass'

// actions
import {
  selectDeliveryLocation,
  setDeliveryAsBilling,
  setDeliveryAsBillingFlag,
  copyDeliveryValuesToBillingForms,
  validateForms,
} from '../../../../actions/common/checkoutActions'
import { showModal } from '../../../../actions/common/modalActions'
import { openMiniBag } from '../../../../actions/common/shoppingBagActions'
import { touchedMultipleFormFields } from '../../../../actions/common/formActions'
import { isMobile } from '../../../../selectors/viewportSelectors'
import { sendEvent } from '../../../../actions/common/googleAnalyticsActions'

// selectors
import {
  getDeliveryPageFormNames,
  getOrderCost,
  hasSelectedStore,
  selectedDeliveryLocationTypeEquals,
} from '../../../../selectors/checkoutSelectors'
import {
  isDDPOrder,
  isDDPPromotionEnabled,
} from '../../../../selectors/ddpSelectors'
import { isFeatureDDPEnabled } from '../../../../selectors/featureSelectors'

import analyticsDecorator from '../../../../../client/lib/analytics/analytics-decorator'

const mapStateToProps = (state) => ({
  formNames: getDeliveryPageFormNames(state),
  totalCost: getOrderCost(state),
  isMobile: isMobile(state),
  isHomeDeliverySelected: selectedDeliveryLocationTypeEquals(state, 'HOME'),
  hasSelectedStore: hasSelectedStore(state),
  hasOrderSummary: !isEmpty(state.checkout.orderSummary),
  isFeatureDDPEnabled: isFeatureDDPEnabled(state),
  isDDPOrder: isDDPOrder(state),
  isDDPPromotionEnabled: isDDPPromotionEnabled(state),
})

const mapDispatchToProps = {
  selectDeliveryLocation,
  showModal,
  openMiniBag,
  setDeliveryAsBilling,
  setDeliveryAsBillingFlag,
  touchedMultipleFormFields,
  copyDeliveryValuesToBillingForms,
  validateForms,
  sendEvent,
}

class DeliveryContainer extends Component {
  static propTypes = {
    totalCost: PropTypes.number,
    isMobile: PropTypes.bool,
    isHomeDeliverySelected: PropTypes.bool,
    hasOrderSummary: PropTypes.bool,
    // actions
    selectDeliveryLocation: PropTypes.func.isRequired,
    showModal: PropTypes.func.isRequired,
    openMiniBag: PropTypes.func.isRequired,
    setDeliveryAsBilling: PropTypes.func.isRequired,
    setDeliveryAsBillingFlag: PropTypes.func.isRequired,
    sendEvent: PropTypes.func.isRequired,
    isFeatureDDPEnabled: PropTypes.bool.isRequired,
    isDDPOrder: PropTypes.bool.isRequired,
    isDDPPromotionEnabled: PropTypes.bool,
  }

  static defaultProps = {
    formNames: [],
    formErrors: {},
    totalCost: undefined,
    isMobile: false,
    isHomeDeliverySelected: false,
    hasSelectedStore: false,
    hasOrderSummary: false,
    redirectTo: browserHistory ? browserHistory.push : () => {},
    isFeatureDDPEnabled: false,
    isDDPOrder: false,
  }

  static contextTypes = {
    l: PropTypes.func,
    p: PropTypes.func,
  }

  componentDidMount() {
    if (window) window.scrollTo(0, 0)

    this.props.sendEvent('Checkout', 'Delivery', 'Delivery Options')
  }

  onChangeDeliveryLocation = (deliveryLocation, index) => {
    const {
      isMobile,
      selectDeliveryLocation,
      setDeliveryAsBilling,
      setDeliveryAsBillingFlag,
    } = this.props
    const isHomeDeliverySelected =
      deliveryLocation.deliveryLocationType === 'HOME'
    setDeliveryAsBilling(isHomeDeliverySelected)
    // DES:1820 - Always set to true
    setDeliveryAsBillingFlag(true)
    selectDeliveryLocation(index)
    if (!isHomeDeliverySelected && !isMobile) {
      this.openCollectFromStoreModal()
    }
  }

  openCollectFromStoreModal = () => {
    this.props.showModal(<CollectFromStore />, { mode: 'storeLocator' })
  }

  render() {
    const { l, p } = this.context
    const {
      totalCost,
      isHomeDeliverySelected,
      isMobile,
      hasOrderSummary,
      openMiniBag,
      isFeatureDDPEnabled,
      isDDPPromotionEnabled,
      isDDPOrder,
    } = this.props

    if (!hasOrderSummary) {
      return null
    }
    return (
      <section className="DeliveryContainer DeliveryContainer--v2">
        <Helmet title={l`Delivery Options`} />
        <QuickViewOrderSummary openMiniBag={openMiniBag} l={l} />
        {totalCost && isMobile && <TotalCost totalCost={p(totalCost)} />}
        {isFeatureDDPEnabled &&
          isDDPPromotionEnabled && <DigitalDeliveryPass />}
        <div className="DeliveryContainer-contentV2">
          <DeliveryOptionsContainer
            onChangeDeliveryLocation={this.onChangeDeliveryLocation}
          />
          <Espot
            identifier={espotsDesktopConstants.orderSummary.discountIntro}
          />
          {isDDPOrder && (
            <p className="DeliveryContainer-ddpAppliedToOrderMsg">
              {l`Your delivery subscription has been applied to your order...`}
            </p>
          )}
          {isHomeDeliverySelected ? (
            <HomeDelivery />
          ) : (
            <StoreDelivery
              openCollectFromStoreModal={this.openCollectFromStoreModal}
            />
          )}
        </div>
      </section>
    )
  }
}

export default analyticsDecorator('delivery-details', { isAsync: true })(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DeliveryContainer)
)

export {
  DeliveryContainer as WrappedDeliveryContainer,
  mapStateToProps,
  mapDispatchToProps,
}
