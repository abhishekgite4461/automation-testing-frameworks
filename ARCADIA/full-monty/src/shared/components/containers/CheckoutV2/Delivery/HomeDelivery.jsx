import PropTypes from 'prop-types'
import QubitReact from 'qubit-react/wrapper'
import { pluck, chain, values, keys, path, any, isEmpty } from 'ramda'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'

import { validate } from '../../../../lib/validator'
import { scrollElementIntoView } from '../../../../lib/scroll-helper'
import {
  getYourDetailsSchema,
  getYourAddressSchema,
  getFindAddressSchema,
  getDeliveryInstructionsSchema,
} from '../shared/validationSchemas'
import { findAddressIsVisible } from '../../../../selectors/checkoutSelectors'
import * as viewportSelectors from '../../../../selectors/viewportSelectors'

// actions
import {
  setFormField,
  touchedFormField,
  touchedMultipleFormFields,
} from '../../../../actions/common/formActions'
import { setDeliveryAsBilling } from '../../../../actions/common/checkoutActions'
import {
  GTM_ACTION,
  GTM_CATEGORY,
  ANALYTICS_ERROR,
  sendAnalyticsClickEvent,
  sendAnalyticsErrorMessage,
} from '../../../../analytics'

// components
import DeliveryMethodsContainer from '../shared/DeliveryMethodsContainer'
import DeliveryDetailsForm from '../shared/DetailsForm/DeliveryDetailsForm'
import DeliveryAddressForm from '../shared/AddressForm/DeliveryAddressForm'
import HomeDeliveryInstructions from '../shared/DeliveryInstructions/DeliveryInstructionsContainer'
import Button from '../../../common/Button/Button'

@connect(
  (state) => ({
    config: state.config,
    deliveryInstructions: state.forms.checkout.deliveryInstructions,
    findAddressForm: state.forms.checkout.findAddress,
    findAddressState: state.findAddress,
    orderSummary: state.checkout.orderSummary,
    useDeliveryAsBilling: state.checkout.useDeliveryAsBilling,
    user: state.account.user,
    yourAddress: state.forms.checkout.yourAddress,
    yourDetails: state.forms.checkout.yourDetails,
    findAddressIsVisible: findAddressIsVisible('delivery', state),
    shippingDestination: state.shippingDestination.destination,
    isMobileBreakpoint: viewportSelectors.isMobileBreakpoint(state),
  }),
  {
    setDeliveryAsBilling,
    setFormField,
    sendAnalyticsClickEvent,
    sendAnalyticsErrorMessage,
    touchedFormField,
    touchedMultipleFormFields,
  }
)
export default class HomeDelivery extends Component {
  static propTypes = {
    config: PropTypes.object.isRequired,
    deliveryInstructions: PropTypes.object.isRequired,
    findAddressForm: PropTypes.object.isRequired,
    findAddressState: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    yourAddress: PropTypes.object.isRequired,
    yourDetails: PropTypes.object.isRequired,
    useDeliveryAsBilling: PropTypes.bool.isRequired,
    setDeliveryAsBilling: PropTypes.func.isRequired,
    touchedMultipleFormFields: PropTypes.func.isRequired,
    findAddressIsVisible: PropTypes.bool,
    nextButtonHidden: PropTypes.bool,
  }

  static defaultProps = {
    findAddressIsVisible: false,
    nextButtonHidden: false,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentDidMount() {
    window.onpopstate = this.onBackButtonEvent
  }

  validateHomeDelivery() {
    const { l } = this.context
    const {
      config,
      deliveryInstructions,
      findAddressForm,
      yourAddress,
      yourDetails,
      findAddressIsVisible,
    } = this.props
    const country = yourAddress.fields.country.value
    const rules = config.checkoutAddressFormRules[country]

    const errors = {
      yourDetails: validate(
        getYourDetailsSchema(country),
        pluck('value', yourDetails.fields),
        l
      ),
      deliveryInstructions: validate(
        getDeliveryInstructionsSchema(country),
        pluck('value', deliveryInstructions.fields),
        l
      ),
      findAddress: {},
      yourAddress: {},
    }

    if (findAddressIsVisible) {
      errors.findAddress = validate(
        this.getFindAddressSchema(rules),
        pluck('value', findAddressForm.fields),
        l
      )
    } else {
      errors.yourAddress = validate(
        getYourAddressSchema(rules),
        pluck('value', yourAddress.fields),
        l
      )
    }

    return errors
  }

  // Hijacking the Backbutton because users can get forms into an invalid state using it (MON-2044)
  // TODO Remove once MON-2046 is done (making a central checkout data model where we can validate)
  onBackButtonEvent = (e) => {
    e.preventDefault()
    const { user } = this.props
    const errors = this.validateHomeDelivery()

    if (
      any((e) => !isEmpty(e), chain(keys, values(errors))) &&
      !isEmpty(user)
    ) {
      window.history.go(1)
      this.nextHandler(e, errors)
    }
  }

  getFindAddressSchema(rules) {
    const {
      findAddressState: { monikers = [] },
      yourAddress,
    } = this.props

    return getFindAddressSchema(rules, {
      hasFoundAddresses: monikers.length > 0,
      hasSelectedAddress: !!path(['fields', 'address1', 'value'], yourAddress),
    })
  }

  focusEl(el) {
    const field = el.querySelector('input,select,button')
    if (field) field.focus()
  }

  scrollToFirstError(name) {
    const el = document.querySelector(`.HomeDeliveryV2 .FormComponent-${name}`)
    const offset = el && el.type === 'select-one' ? 30 : 20
    setTimeout(() => {
      scrollElementIntoView(el, 400, offset).then(() => {
        this.focusEl(el)
      })
    }, 15)
  }

  nextHandler(e, errors) {
    const {
      setDeliveryAsBilling,
      touchedMultipleFormFields,
      useDeliveryAsBilling,
      sendAnalyticsClickEvent,
      sendAnalyticsErrorMessage,
    } = this.props

    const erroringFields = chain(keys, values(errors))

    sendAnalyticsClickEvent({
      category: GTM_CATEGORY.CHECKOUT,
      action: GTM_ACTION.CLICKED,
      label: window.location.href,
      value: '',
    })

    if (erroringFields.length) {
      this.scrollToFirstError(erroringFields[0])
      touchedMultipleFormFields('yourDetails', keys(errors.yourDetails))
      touchedMultipleFormFields('findAddress', keys(errors.findAddress))
      touchedMultipleFormFields('yourAddress', keys(errors.yourAddress))

      sendAnalyticsErrorMessage(ANALYTICS_ERROR.PROCEED_TO_PAYMENT)
    } else {
      if (useDeliveryAsBilling) {
        setDeliveryAsBilling(true)
      }

      browserHistory.push('/checkout/payment')
    }
  }

  render() {
    const { l } = this.context
    const { nextButtonHidden, isMobileBreakpoint } = this.props

    const errors = this.validateHomeDelivery()
    const noErrors = Object.keys(errors).reduce((value, err) => {
      if (!isEmpty(errors[err])) value = false
      return value
    }, true)

    return (
      <section className="HomeDeliveryV2">
        <div className="HomeDeliveryV2-form">
          <DeliveryDetailsForm renderTelephone={isMobileBreakpoint} />
          <DeliveryAddressForm renderTelephone={!isMobileBreakpoint} />
        </div>
        <DeliveryMethodsContainer />
        <QubitReact
          id="qubit-HomeDelivery-DeliveryInstructions"
          errors={errors.deliveryInstructions}
        >
          <HomeDeliveryInstructions />
        </QubitReact>
        {!nextButtonHidden && (
          <div>
            <Button
              className="DeliveryContainer-nextButton"
              clickHandler={(e) => this.nextHandler(e, errors)}
              isActive={noErrors}
            >
              {l`Proceed to Payment`}
            </Button>
          </div>
        )}
      </section>
    )
  }
}
