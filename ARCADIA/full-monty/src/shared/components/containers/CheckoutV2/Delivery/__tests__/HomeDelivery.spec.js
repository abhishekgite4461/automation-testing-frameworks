import testComponentHelper from 'test/unit/helpers/test-component'
import generateMockFormProps from 'test/unit/helpers/form-props-helper'
import {
  initialHomeDeliveryProps as initialProps,
  initialHomeDeliveryFormProps as initialFormProps,
  populatedHomeDeliveryFormProps as populatedFormProps,
} from './deliveryMocks'
import {
  GTM_ACTION,
  GTM_CATEGORY,
} from '../../../../../analytics/analytics-constants'

jest.mock('react-router', () => ({
  browserHistory: {
    push: jest.fn(),
  },
}))

import { browserHistory } from 'react-router'
import HomeDelivery from '../HomeDelivery'

describe('<HomeDelivery />', () => {
  jest.useFakeTimers()

  const renderComponent = testComponentHelper(HomeDelivery.WrappedComponent)
  const generateProps = (newFormProps) => ({
    ...initialProps,
    ...generateMockFormProps(initialFormProps, newFormProps),
  })

  beforeEach(() => jest.resetAllMocks())

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(generateProps()).getTree()).toMatchSnapshot()
    })

    it('hides the titles of its children components when "titlesHidden" = true ', () => {
      expect(
        renderComponent(generateProps({ titlesHidden: true })).getTree()
      ).toMatchSnapshot()
    })

    it('hides the next button when "nextButtonHidden" = true', () => {
      const { wrapper } = renderComponent(
        generateProps({ nextButtonHidden: true })
      )
      expect(wrapper.find('DeliveryContainer-nextButton').length).toEqual(0)
    })

    it('isActive when there is no errors', () => {
      const { wrapper } = renderComponent(generateProps(populatedFormProps))
      expect(
        wrapper.find('.DeliveryContainer-nextButton').prop('isActive')
      ).toBe(true)
    })

    it('not isActive when there are some errors', () => {
      const { wrapper } = renderComponent(generateProps())
      expect(
        wrapper.find('.DeliveryContainer-nextButton').prop('isActive')
      ).toBe(false)
    })
  })

  describe('@lifecycle', () => {
    describe('on componentDidMount', () => {
      it('should hijack the backbutton to prevent form with invalid state', () => {
        const { instance } = renderComponent(generateProps())
        instance.componentDidMount()
        expect(global.window.onpopstate).toBe(instance.onBackButtonEvent)
      })
    })
  })

  describe('@events', () => {
    describe('on NEXT button click', () => {
      describe('with required fields missing', () => {
        it('shows errors on form fields', () => {
          const { instance, wrapper } = renderComponent(generateProps())
          instance.scrollToFirstError = jest.fn()

          wrapper
            .find('.DeliveryContainer-nextButton')
            .props()
            .clickHandler()
          expect(instance.scrollToFirstError).toHaveBeenCalledTimes(1)
          expect(
            instance.props.touchedMultipleFormFields
          ).toHaveBeenCalledTimes(3)
        })

        it('should not redirect', () => {
          const { wrapper, instance } = renderComponent(generateProps())

          wrapper
            .find('.DeliveryContainer-nextButton')
            .props()
            .clickHandler()
          expect(browserHistory.push).not.toHaveBeenCalled()
          expect(
            instance.props.sendAnalyticsErrorMessage
          ).toHaveBeenCalledTimes(1)
          expect(instance.props.sendAnalyticsErrorMessage).toHaveBeenCalledWith(
            'Error proceeding to payment'
          )
          expect(instance.props.sendAnalyticsClickEvent).toHaveBeenCalledTimes(
            1
          )
          expect(instance.props.sendAnalyticsClickEvent).toHaveBeenCalledWith({
            category: GTM_CATEGORY.CHECKOUT,
            action: GTM_ACTION.CLICKED,
            label: window.location.href,
            value: '',
          })
        })
      })

      describe('with required fields populated', () => {
        const props = generateProps(populatedFormProps)

        it('should redirect to payment route', () => {
          const { wrapper, instance } = renderComponent(props)

          wrapper
            .find('.DeliveryContainer-nextButton')
            .props()
            .clickHandler()
          expect(browserHistory.push).toHaveBeenCalledWith('/checkout/payment')
          expect(browserHistory.push).toHaveBeenCalledTimes(1)
          expect(instance.props.sendAnalyticsClickEvent).toHaveBeenCalledTimes(
            1
          )
          expect(instance.props.sendAnalyticsClickEvent).toHaveBeenCalledWith({
            category: GTM_CATEGORY.CHECKOUT,
            action: GTM_ACTION.CLICKED,
            label: window.location.href,
            value: '',
          })
          expect(
            instance.props.sendAnalyticsErrorMessage
          ).not.toHaveBeenCalled()
        })
      })
    })
  })

  describe('@methods', () => {
    describe('validateHomeDelivery()', () => {
      describe('with valid required fields', () => {
        it('should not return errors', () => {
          const { instance } = renderComponent(
            generateProps(populatedFormProps)
          )
          expect(instance.validateHomeDelivery()).toMatchSnapshot()
        })
      })
      describe('with invalid or missing fields', () => {
        it('should return errors', () => {
          const { instance } = renderComponent(generateProps())
          expect(instance.validateHomeDelivery()).toMatchSnapshot()
        })

        it('should return `findAddress` errors if `findAddressIsVisible` is `true`', () => {
          const { instance } = renderComponent({
            ...generateProps(),
            findAddressIsVisible: true,
          })
          expect(
            Object.keys(instance.validateHomeDelivery().findAddress)
          ).toEqual(['postCode', 'country', 'findAddress', 'selectAddress'])
        })
      })
    })

    describe('focusEl()', () => {
      it('should call focus if a form input is found', () => {
        const { instance } = renderComponent(generateProps(populatedFormProps))
        const el = {
          querySelector: jest.fn(),
        }
        const focusMock = jest.fn()
        el.querySelector.mockReturnValue({
          focus: focusMock,
        })
        instance.focusEl(el)
        expect(focusMock).toHaveBeenCalledTimes(1)
        expect(el.querySelector).toHaveBeenCalledTimes(1)
        expect(el.querySelector).toHaveBeenCalledWith('input,select,button')
      })

      it('should not call focus if no found form input', () => {
        const { instance } = renderComponent(generateProps(populatedFormProps))
        const el = {
          querySelector: jest.fn(),
        }
        el.querySelector.mockReturnValue(null)
        instance.focusEl(el)
        expect(el.querySelector).toHaveBeenCalledTimes(1)
        expect(el.querySelector).toHaveBeenCalledWith('input,select,button')
      })
    })

    describe('onBackButtonEvent(e)', () => {
      it('should prevent route change', () => {
        const eventMock = {
          target: 'backButton',
          preventDefault: jest.fn(),
        }
        const { instance } = renderComponent({
          ...generateProps(),
          user: { exists: true },
        })
        instance.nextHandler = jest.fn()
        instance.onBackButtonEvent(eventMock)
        expect(instance.nextHandler).toHaveBeenCalledTimes(1)
      })
    })
  })

  describe('scrollToFirstError(name)', () => {
    it('should execute setTimeout', () => {
      const { instance } = renderComponent(generateProps())
      expect(setTimeout).not.toHaveBeenCalled()
      instance.scrollToFirstError('firstName')
      expect(setTimeout).toHaveBeenCalled()
      expect(setTimeout.mock.calls[0][1]).toBe(15)
    })
  })
})
