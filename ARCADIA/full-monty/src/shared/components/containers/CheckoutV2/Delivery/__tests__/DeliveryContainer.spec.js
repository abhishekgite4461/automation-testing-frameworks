import { clone } from 'ramda'
import React from 'react'

import testComponentHelper, {
  analyticsDecoratorHelper,
} from 'test/unit/helpers/test-component'
import { orderSummaryHomeOnly } from 'test/mocks/orderSummary-home-delivery-only'
import { expressDeliveryOrderSummary } from '../../Summary/__tests__/OrderSummary-express-delivery-data'

import { getEnrichedDeliveryLocations } from '../../../../../selectors/checkoutSelectors'

import CollectFromStore from '../../../../common/CollectFromStore/CollectFromStore'
import DeliveryOptionsContainer from '../../shared/DeliveryOptionsContainer'
import DeliveryContainer from '../DeliveryContainer'
import DigitalDeliveryPass from '../../shared/DigitalDeliveryPass/DigitalDeliveryPass'

describe('<DeliveryContainer />', () => {
  const noop = () => {}
  const requiredProps = {
    brandName: 'topshop',
    selectDeliveryLocation: noop,
    showModal: noop,
    openMiniBag: noop,
    setDeliveryAsBilling: noop,
    setDeliveryAsBillingFlag: noop,
    touchedMultipleFormFields: noop,
    copyDeliveryValuesToBillingForms: noop,
    validateForms: noop,
  }
  const expressHomeDeliveryLocations = getEnrichedDeliveryLocations({
    checkout: {
      orderSummary: {
        deliveryLocations: expressDeliveryOrderSummary.deliveryLocations,
      },
    },
  })
  const homeDeliveryLocations = getEnrichedDeliveryLocations({
    checkout: {
      orderSummary: {
        deliveryLocations: orderSummaryHomeOnly.deliveryLocations,
      },
    },
  })
  const renderComponent = testComponentHelper(
    DeliveryContainer.WrappedComponent.WrappedComponent,
    { disableLifecycleMethods: true }
  )

  describe('@renders', () => {
    it('Home Delivery', () => {
      const { getTree } = renderComponent({
        ...requiredProps,
        deliveryLocations: expressHomeDeliveryLocations,
        shippingCountry: 'United Kingdom',
        totalCost: 87.0,
        isHomeDeliverySelected: true,
        hasOrderSummary: true,
        isMobile: true,
      })
      expect(getTree()).toMatchSnapshot()
    })

    it('Home Delivery without StoreDelivery options', () => {
      const { getTree } = renderComponent({
        ...requiredProps,
        deliveryLocations: homeDeliveryLocations,
        shippingCountry: 'United Kingdom',
        totalCost: 87.0,
        isHomeDeliverySelected: true,
        hasOrderSummary: true,
        isMobile: true,
      })
      expect(getTree()).toMatchSnapshot()
    })

    it('Store Delivery', () => {
      const storeDeliveryLocations = clone(expressHomeDeliveryLocations)
      storeDeliveryLocations[0].selected = false
      storeDeliveryLocations[1].selected = true
      const { getTree } = renderComponent({
        ...requiredProps,
        deliveryLocations: storeDeliveryLocations,
        shippingCountry: 'United Kingdom',
        totalCost: 81.0,
        isHomeDeliverySelected: false,
        hasOrderSummary: true,
        isMobile: true,
      })
      expect(getTree()).toMatchSnapshot()
    })

    it('Empty orderSummary should return null', () => {
      const { getTree } = renderComponent(requiredProps)
      expect(getTree()).toMatchSnapshot()
    })

    it('should display DDP accordion if DDP purchase is available', () => {
      const props = {
        ...requiredProps,
        hasOrderSummary: true,
        isFeatureDDPEnabled: true,
        isDDPPromotionEnabled: true,
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find(DigitalDeliveryPass).length).toBe(1)
    })

    it('should not display DDP accordion if DDP purchase is not available', () => {
      const props = {
        ...requiredProps,
        hasOrderSummary: true,
        isFeatureDDPEnabled: true,
        isDDPPromotionEnabled: false,
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find(DigitalDeliveryPass).exists()).toEqual(false)
    })

    it('should display `DDP applied to order` message if DDP product in bag', () => {
      const props = {
        ...requiredProps,
        hasOrderSummary: true,
        isDDPOrder: true,
      }
      const { wrapper } = renderComponent(props)
      expect(
        wrapper.find('.DeliveryContainer-ddpAppliedToOrderMsg')
      ).toHaveLength(1)
    })

    it('should not display `DDP applied to order` message if no DDP product in bag', () => {
      const props = {
        ...requiredProps,
        hasOrderSummary: true,
        isDDPOrder: false,
      }
      const { wrapper } = renderComponent(props)
      expect(
        wrapper.find('.DeliveryContainer-ddpAppliedToOrderMsg').exists()
      ).toBe(false)
    })
  })

  describe('@decorators', () => {
    analyticsDecoratorHelper(DeliveryContainer, 'delivery-details', {
      componentName: 'DeliveryContainer',
      isAsync: true,
      redux: true,
    })
  })

  describe('@events', () => {
    describe('On change delivery option', () => {
      it('it should select the delivery location for home', () => {
        const selectDeliveryLocationMock = jest.fn()
        const setDeliveryAsBillingMock = jest.fn()
        const setDeliveryAsBillingFlagMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          deliveryLocations: expressHomeDeliveryLocations,
          hasOrderSummary: true,
          selectDeliveryLocation: selectDeliveryLocationMock,
          setDeliveryAsBilling: setDeliveryAsBillingMock,
          setDeliveryAsBillingFlag: setDeliveryAsBillingFlagMock,
        })
        wrapper.find(DeliveryOptionsContainer).prop('onChangeDeliveryLocation')(
          { deliveryLocationType: 'HOME' },
          2
        )
        expect(selectDeliveryLocationMock).toHaveBeenCalledWith(2)
        expect(setDeliveryAsBillingMock).toHaveBeenCalledWith(true)
        expect(setDeliveryAsBillingFlagMock).toHaveBeenCalledWith(true)
      })

      it('it should select the delivery location for store', () => {
        const selectDeliveryLocationMock = jest.fn()
        const setDeliveryAsBillingMock = jest.fn()
        const setDeliveryAsBillingFlagMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          deliveryLocations: expressHomeDeliveryLocations,
          hasOrderSummary: true,
          selectDeliveryLocation: selectDeliveryLocationMock,
          setDeliveryAsBilling: setDeliveryAsBillingMock,
          setDeliveryAsBillingFlag: setDeliveryAsBillingFlagMock,
        })
        wrapper.find(DeliveryOptionsContainer).prop('onChangeDeliveryLocation')(
          { deliveryLocationType: 'STORE' },
          2
        )
        expect(selectDeliveryLocationMock).toHaveBeenCalledWith(2)
        expect(setDeliveryAsBillingMock).toHaveBeenCalledWith(false)
        expect(setDeliveryAsBillingFlagMock).toHaveBeenCalledWith(true)
      })

      it('should open the collect from store modal if not home delivery and not mobile', () => {
        const showModalMock = jest.fn()
        const { wrapper } = renderComponent({
          ...requiredProps,
          deliveryLocations: expressHomeDeliveryLocations,
          hasOrderSummary: true,
          showModal: showModalMock,
        })
        wrapper.find(DeliveryOptionsContainer).prop('onChangeDeliveryLocation')(
          { deliveryLocationType: 'STORE' },
          2
        )
        expect(showModalMock).toHaveBeenCalledWith(<CollectFromStore />, {
          mode: 'storeLocator',
        })
      })
    })
  })
})
