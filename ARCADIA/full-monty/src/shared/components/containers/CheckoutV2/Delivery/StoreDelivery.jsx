import PropTypes from 'prop-types'
import { pluck, chain, values, keys, isEmpty } from 'ramda'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'

import { validate } from '../../../../lib/validator'
import { scrollElementIntoView } from '../../../../lib/scroll-helper'
import { getYourDetailsSchema } from '../shared/validationSchemas'
import { composeStoreName } from '../../../../lib/store-utilities'

// actions
import { searchStoresCheckout } from '../../../../actions/components/UserLocatorActions'
import * as formActions from '../../../../actions/common/formActions'
import * as checkoutActions from '../../../../actions/common/checkoutActions'
import {
  GTM_ACTION,
  GTM_CATEGORY,
  ANALYTICS_ERROR,
  sendAnalyticsClickEvent,
  sendAnalyticsErrorMessage,
} from '../../../../analytics'

// components
import UserLocatorInput from '../../../common/UserLocatorInput/UserLocatorInput'
import Message from '../../../common/FormComponents/Message/Message'
import DeliveryMethodsContainer from '../shared/DeliveryMethodsContainer'
import Button from '../../../common/Button/Button'
import DeliveryDetailsForm from '../shared/DetailsForm/DeliveryDetailsForm'
import CheckoutTitle from '../shared/CheckoutTitle'

@connect(
  (state) => ({
    yourDetails: state.forms.checkout.yourDetails,
    config: state.config,
    orderSummary: state.checkout.orderSummary,
    isMobile: state.viewport.media === 'mobile',
    storeUpdating: state.checkout.storeUpdating,
    useDeliveryAsBilling: state.checkout.useDeliveryAsBilling,
  }),
  {
    ...formActions,
    ...checkoutActions,
    searchStoresCheckout,
    sendAnalyticsClickEvent,
    sendAnalyticsErrorMessage,
  }
)
export default class StoreDelivery extends Component {
  state = { message: '' }

  static propTypes = {
    config: PropTypes.object.isRequired,
    yourDetails: PropTypes.object.isRequired,
    orderSummary: PropTypes.object.isRequired,
    isMobile: PropTypes.bool,
    storeUpdating: PropTypes.bool,
    searchStoresCheckout: PropTypes.func,
    setStoreUpdating: PropTypes.func,
    touchedMultipleFormFields: PropTypes.func,
    openCollectFromStoreModal: PropTypes.func,
    setDeliveryAsBilling: PropTypes.func,
    setDeliveryAsBillingFlag: PropTypes.func,
    nextButtonHidden: PropTypes.bool,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  static defaultProps = {
    nextButtonHidden: false,
  }

  componentDidMount() {
    window.onpopstate = this.onBackButtonEvent
  }

  componentWillUpdate(nextProps) {
    const { storeUpdating, isMobile, setStoreUpdating } = this.props
    if (storeUpdating && isMobile !== nextProps.isMobile)
      setStoreUpdating(false)
  }

  // Hijacking the Backbutton because users can get forms into an invalid state using it (MON-2044)
  // TODO Remove once MON-2046 is done (making a central checkout data model where we can validate)
  onBackButtonEvent = () => {
    const errors = this.validateStoreDelivery()

    if (chain(keys, values(errors)).length) {
      window.history.go(1)
      this.nextHandler(errors)
    }
  }

  validateStoreDelivery() {
    const { l } = this.context
    const { yourDetails, config, storeUpdating, orderSummary } = this.props

    return {
      yourDetails: validate(
        getYourDetailsSchema(config.country),
        pluck('value', yourDetails.fields),
        l
      ),
      store:
        storeUpdating || !orderSummary.storeDetails
          ? { storeDetails: true }
          : undefined,
    }
  }

  goToDeliveryStoreLocator = (event) => {
    event.preventDefault()
    this.props.searchStoresCheckout()
  }

  scrollToFirstError(name) {
    const el = document.querySelector(`.StoreDeliveryV2 .Input-${name}`)
    setTimeout(() => {
      scrollElementIntoView(el, 400, 20)
    }, 15)
  }

  nextHandler(errors) {
    const {
      touchedMultipleFormFields,
      setDeliveryAsBillingFlag,
      setDeliveryAsBilling,
      useDeliveryAsBilling,
      sendAnalyticsClickEvent,
      sendAnalyticsErrorMessage,
    } = this.props
    const errorsArray = chain(keys, values(errors))

    sendAnalyticsClickEvent({
      category: GTM_CATEGORY.CHECKOUT,
      action: GTM_ACTION.CLICKED,
      label: window.location.href,
      value: '',
    })

    if (errors.store && errors.store.storeDetails)
      this.setState({
        message: 'Please select a delivery location before you proceed',
      })
    if (errorsArray.length) {
      this.scrollToFirstError(errorsArray[0])
      touchedMultipleFormFields('yourDetails', keys(errors.yourDetails))

      sendAnalyticsErrorMessage(ANALYTICS_ERROR.PROCEED_TO_PAYMENT)
    } else {
      if (useDeliveryAsBilling) {
        setDeliveryAsBillingFlag(true)
        setDeliveryAsBilling(true)
        checkoutActions.copyDeliveryValuesToBillingForms()
      }

      browserHistory.push('/checkout/payment')
    }
  }

  changeStore = () => {
    const { openCollectFromStoreModal, isMobile, setStoreUpdating } = this.props
    setStoreUpdating(true)
    this.setState({ message: '' })
    if (!isMobile) openCollectFromStoreModal()
  }

  renderChangeStoreCTA() {
    const { l } = this.context
    const {
      orderSummary: { deliveryStoreCode, storeDetails },
      isMobile,
    } = this.props
    const storeType =
      deliveryStoreCode && deliveryStoreCode.startsWith('S') ? 'shop' : 'store'
    const deliverToStoreType = `Deliver to another ${storeType}`
    const selectMessage = storeDetails
      ? `Change ${storeType}`
      : `Select ${storeType}`
    return isMobile ? (
      <button className="Button--link" onClick={this.changeStore}>
        {l(deliverToStoreType)}
      </button>
    ) : (
      <Button
        className="StoreDeliveryV2-changeStoreCTA"
        clickHandler={this.changeStore}
      >
        {selectMessage}
      </Button>
    )
  }

  renderStoreDeliveryInfo = () => {
    const { l } = this.context
    const {
      isMobile,
      orderSummary: { deliveryStoreCode, storeDetails },
    } = this.props
    const { message } = this.state
    const { address1: address, city, postcode } = storeDetails || {}
    const storeName = composeStoreName(deliveryStoreCode, storeDetails)
    const titleMessage = isMobile
      ? 'Your order will be delivered to'
      : 'Collect from'

    return (
      <div className="StoreDeliveryV2-storeInfo">
        {storeDetails && (
          <CheckoutTitle separator>{l(titleMessage)}</CheckoutTitle>
        )}
        {storeDetails ? (
          <div className="StoreDeliveryV2-storeAddress">
            {storeName && (
              <p className="StoreDeliveryV2-storeAddressLine">{storeName}</p>
            )}
            {address && (
              <p className="StoreDeliveryV2-storeAddressLine">{address}</p>
            )}
            {city && <p className="StoreDeliveryV2-storeAddressLine">{city}</p>}
            {postcode && (
              <p className="StoreDeliveryV2-storeAddressLine">{postcode}</p>
            )}
          </div>
        ) : (
          <Message message={l(message)} type="error" />
        )}
        {this.renderChangeStoreCTA()}
      </div>
    )
  }

  render() {
    const { l } = this.context
    const {
      orderSummary,
      isMobile,
      storeUpdating,
      nextButtonHidden,
    } = this.props
    const errors = this.validateStoreDelivery()
    const noErrors = isEmpty(errors.yourDetails) && errors.store === undefined

    return (
      <div className="StoreDeliveryV2" aria-label="Store Delivery">
        {!isMobile || (!storeUpdating && !!orderSummary.storeDetails) ? (
          <div>
            {this.renderStoreDeliveryInfo()}
            <DeliveryDetailsForm />
            <DeliveryMethodsContainer />
            {!nextButtonHidden && (
              <Button
                className="DeliveryContainer-nextButton"
                clickHandler={() => this.nextHandler(errors)}
                isActive={noErrors}
              >
                {l`Proceed to Payment`}
              </Button>
            )}
          </div>
        ) : (
          <form onSubmit={this.goToDeliveryStoreLocator}>
            <UserLocatorInput selectedCountry="United Kingdom" />
          </form>
        )}
      </div>
    )
  }
}
