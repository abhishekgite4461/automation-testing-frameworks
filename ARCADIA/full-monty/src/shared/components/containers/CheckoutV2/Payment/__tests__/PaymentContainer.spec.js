import testComponentHelper, {
  analyticsDecoratorHelper,
} from 'test/unit/helpers/test-component'
import PaymentContainer from '../PaymentContainer'
import BillingAddressForm from '../../shared/AddressForm/BillingAddressForm'
import BillingDetailsForm from '../../shared/DetailsForm/BillingDetailsForm'
import AddressPreview from '../../shared/AddressPreview'
import { sendEvent } from '../../../../../actions/common/googleAnalyticsActions'
import {
  GTM_ACTION,
  GTM_CATEGORY,
  ANALYTICS_ERROR,
} from '../../../../../analytics/analytics-constants'

jest.mock('../../../../../actions/common/googleAnalyticsActions')

jest.mock('../../../../../lib/checkout', () => ({
  createOrder: jest.fn(),
  fixTotal: jest.fn(),
  giftCardCoversTotal: jest.fn(),
}))

jest.mock('../../../../../lib/checkout-utilities/klarna-utils', () => ({
  assembleFullPayload: jest.fn(),
  hashOrderSummary: jest.fn(),
}))

describe('<PaymentContainer />', () => {
  jest.useFakeTimers()
  beforeEach(() => {
    jest.resetAllMocks()
    global.scrollTo = jest.fn()
  })

  const renderComponent = testComponentHelper(
    PaymentContainer.WrappedComponent.WrappedComponent
  )
  const detailsFields = {
    title: {
      value: 'Dr',
    },
    firstName: {
      value: 'John',
    },
    lastName: {
      value: 'Smith',
    },
    telephone: {
      value: '07123123123',
    },
  }
  const billingCardDetails = {
    fields: {
      paymentType: {
        value: 'VISA',
      },
      cardNumber: {
        value: '4444333322221111',
      },
      cvv: {
        value: '123',
      },
      expiryMonth: {
        value: '0810',
      },
    },
  }
  const initialProps = {
    setFormField: jest.fn(),
    setFormMeta: jest.fn(),
    touchedFormField: jest.fn(),
    setManualAddressMode: jest.fn(),
    resetAddress: jest.fn(),
    resetForm: jest.fn(),
    findAddress: jest.fn(),
    touchedMultipleFormFields: jest.fn(),
    resetFormPartial: jest.fn(),
    getPaymentMethods: jest.fn(),
    addGiftCard: jest.fn(),
    hideGiftCardBanner: jest.fn(),
    removeGiftCard: jest.fn(),
    createOrder: jest.fn(),
    reAuthorize: jest.fn(),
    useDeliveryAsBilling: false,
    setDeliveryAsBillingFlag: jest.fn(),
    copyDeliveryValuesToBillingForms: jest.fn(),
    resetBillingForms: jest.fn(),
    homeDeliverySelected: false,
    sendAnalyticsErrorMessage: jest.fn(),
    sendAnalyticsClickEvent: jest.fn(),
    orderSummary: {
      creditCard: {
        cardNumberStar: '************1111',
      },
      basket: {
        total: '40.00',
        totalBeforeDiscount: '52.00',
        subTotal: '52.00',
        discounts: [],
      },
      giftCards: [],
      estimatedDelivery: ['No later than Friday 22 July 2016'],
      deliveryLocations: [
        {
          deliveryLocationType: 'HOME',
          label:
            'Home Delivery Standard (UK up to 4 working days; worldwide varies)  Express (UK next or nominated day; worldwide varies)',
          selected: true,
          deliveryMethods: [],
        },
        {
          deliveryLocationType: 'STORE',
          label:
            'Collect from Store Standard (3-7 working days) Express (next day)',
          selected: false,
          deliveryMethods: [],
        },
        {
          deliveryLocationType: 'PARCELSHOP',
          label: 'Collect from ParcelShop',
          selected: false,
          deliveryMethods: [],
        },
      ],
    },
    auth: {
      authentication: true,
    },
    billingDetails: {
      fields: detailsFields,
    },
    billingCardDetails,
    deliveryInstructions: {
      fields: {
        deliveryInstructions: {
          value: 'None',
        },
        smsMobileNumber: {
          value: '07979797979',
        },
      },
    },
    billingAddress: {
      fields: {
        country: {
          value: 'United Kingdom',
        },
      },
    },
    order: {
      fields: {
        isAcceptedTermsAndConditions: {
          value: false,
        },
      },
    },
    config: {
      checkoutAddressFormRules: {
        'United Kingdom': {},
      },
    },
    findAddressState: {
      isManual: true,
    },
    billingFindAddress: {
      fields: {
        postCode: {
          value: 'HP10 8HD',
        },
        houseNumber: {
          value: 'Sinclairs',
        },
      },
    },
    isMobile: true,
    yourDetails: {
      fields: detailsFields,
    },
    yourAddress: {
      fields: {
        address1: {
          value: 'Sinclairs, Sandpits Lane',
        },
        address2: {
          value: 'Penn',
        },
        city: {
          value: 'HIGH WYCOMBE',
        },
        country: {
          value: 'United Kingdom',
        },
        postcode: {
          value: 'HP10 8HD',
        },
      },
    },
    siteOptions: {
      titles: ['Mr', 'Mrs', 'Ms', 'Miss', 'Dr'],
    },
    shoppingBag: {
      bag: {
        discounts: [
          {
            label: 'Topshop Card- £5 Welcome offer on a £50+ spend',
            value: '5.00',
          },
        ],
      },
    },
    paymentMethods: [],
    orderDetails: {
      amount: '20',
      billing: 'United Kingdom',
      delivery: 'United Kingdom',
    },
    user: {
      exists: true,
      email: 'winston@churchill.co.uk',
    },
    submitOrder: () => {},
    updateBillingForms: () => {},
    validateForms: () => {},
    sendEvent,
  }

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })
    it('With errors paynow button should not be active', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        formErrors: {
          billingDetails: {
            address1: 'This field is required',
          },
        },
      })
      expect(wrapper.find('.PaymentContainer-paynow').prop('isActive')).toBe(
        false
      )
    })

    it('pushes an errorEvent to the DataLayer', () => {
      const { wrapper, instance } = renderComponent({
        ...initialProps,
        formErrors: {
          order: {
            isAcceptedTermsAndConditions: 'This field is required',
          },
        },
      })

      instance.handleSubmit()
      wrapper.update()
      expect(instance.props.sendAnalyticsErrorMessage).toHaveBeenCalledTimes(1)
      expect(instance.props.sendAnalyticsErrorMessage).toHaveBeenCalledWith(
        ANALYTICS_ERROR.CONFIRM_AND_PAY
      )
      expect(instance.props.sendAnalyticsClickEvent).toHaveBeenCalledTimes(1)
      expect(instance.props.sendAnalyticsClickEvent).toHaveBeenCalledWith({
        category: GTM_CATEGORY.CHECKOUT,
        action: GTM_ACTION.CLICKED,
        label: 'confirm-and-pay',
        value: '',
      })
    })

    it('with storeDetails', () => {
      expect(
        renderComponent({
          ...initialProps,
          orderSummary: {
            ...initialProps.orderSummary,
            storeDetails: {
              address1: 'c/o Top Shop, 60/64 The Strand',
              city: 'Strand',
              country: 'United Kingdom',
              postcode: 'WC2N 5LR',
            },
          },
        }).getTree()
      ).toMatchSnapshot()
    })

    it('with findAddressState.address as false', () => {
      expect(
        renderComponent({
          ...initialProps,
          findAddressState: {
            address: false,
            monikers: [],
          },
        }).getTree()
      ).toMatchSnapshot()
    })

    it('returns null when orderSummary is empty', () => {
      expect(
        renderComponent({
          ...initialProps,
          orderSummary: {},
        }).getTree()
      ).toMatchSnapshot()
    })

    it('should render a total at the top and SimpleTotals in mobile', () => {
      const { getTree } = renderComponent({
        ...initialProps,
        isMobile: true,
      })
      expect(getTree()).toMatchSnapshot()
    })

    describe('CheckoutPaymentDetails component renders', () => {
      it('no orderSummary.creditCard', () => {
        expect(
          renderComponent({
            ...initialProps,
            orderSummary: {
              ...initialProps.orderSummary,
              creditCard: null,
            },
          }).getTree()
        ).toMatchSnapshot()
      })

      it('billingCardDetails.isShown is true', () => {
        expect(
          renderComponent({
            ...initialProps,
            billingCardDetails: {
              ...initialProps.billingCardDetails,
              isShown: true,
            },
          }).getTree()
        ).toMatchSnapshot()
      })
    })

    describe('when klarnaForm is shown', () => {
      it('should not render Submit Order button', () => {
        expect(
          renderComponent({
            ...initialProps,
            billingCardDetails: {
              ...initialProps.billingCardDetails,
              fields: {
                paymentType: {
                  value: 'KLRNA',
                },
              },
            },
            showKlarnaForm: true,
          }).getTree()
        ).toMatchSnapshot()
      })
    })

    it('should render PayPal CTA on Submit Order button', () => {
      expect(
        renderComponent({
          ...initialProps,
          billingCardDetails: {
            ...initialProps.billingCardDetails,
            fields: {
              paymentType: {
                value: 'PYPAL',
              },
            },
          },
        }).getTree()
      ).toMatchSnapshot()
    })

    it('should render Klarna CTA on Submit Order button', () => {
      expect(
        renderComponent({
          ...initialProps,
          billingCardDetails: {
            ...initialProps.billingCardDetails,
            fields: {
              paymentType: {
                value: 'KLRNA',
              },
            },
          },
          showKlarnaForm: false,
          klarnaAuthToken: 'valid_token',
        }).getTree()
      ).toMatchSnapshot()
    })

    it('shows Terms & Conditions checkbox ticked', () => {
      expect(
        renderComponent({
          ...initialProps,
          order: {
            fields: {
              isAcceptedTermsAndConditions: {
                value: true,
              },
            },
          },
        }).getTree()
      ).toMatchSnapshot()
    })

    it('sets termsAndConditionsUrl from seoUrl', () => {
      expect(
        renderComponent({
          ...initialProps,
          termsAndConditions: {
            seoUrl: '/seoUrlTest',
          },
        }).getTree()
      ).toMatchSnapshot()
    })

    it(
      'hides the AddressPreview component and shows YourDetails and AddressForm components' +
        'when useDeliveryAsBilling is false',
      () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          useDeliveryAsBilling: false,
          homeDeliverySelected: true,
        })
        expect(wrapper.find(AddressPreview).length).toBe(0)
        expect(wrapper.find(BillingDetailsForm).length).toBe(1)
        expect(wrapper.find(BillingAddressForm).length).toBe(1)
      }
    )
    it(
      'hides the BillingAddress component and shows YourDetails and AddressForm components' +
        'when homeDeliverySelected is false',
      () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          useDeliveryAsBilling: true,
          homeDeliverySelected: false,
        })
        expect(wrapper.find(AddressPreview).length).toBe(0)
        expect(wrapper.find(BillingDetailsForm).length).toBe(1)
        expect(wrapper.find(BillingAddressForm).length).toBe(1)
      }
    )
    it(
      'shows the AddressPreview component and hides YourDetails and AddressForm components' +
        'when useDeliveryAsBilling is true',
      () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          useDeliveryAsBilling: true,
          homeDeliverySelected: true,
        })
        expect(wrapper.find(AddressPreview).length).toBe(1)
        expect(wrapper.find(BillingDetailsForm).length).toBe(0)
        expect(wrapper.find(BillingAddressForm).length).toBe(0)
      }
    )
    it('always has the "Same as delivery address" checkbox', () => {
      expect(
        renderComponent(initialProps).wrapper.find('.PaymentContainer-checkbox')
          .length
      ).toBe(1)
    })

    it('should display error `Message` component if `errorMessage` prop passed in', () => {
      const scrollToMock = jest.fn()
      const { wrapper } = renderComponent({
        ...initialProps,
        errorMessage: 'The credit card number must be valid',
      })
      const message = wrapper.find('Message')
      expect(message.prop('message')).toBe(
        'The credit card number must be valid'
      )
      expect(message.prop('type')).toBe('error')
      // should scroll to on Message mount
      message.prop('onDidMount')({ scrollTo: scrollToMock })
      expect(scrollToMock).toHaveBeenCalled()
    })
  })

  describe('@decorators', () => {
    analyticsDecoratorHelper(PaymentContainer, 'order-submit-page', {
      componentName: 'PaymentContainer',
      isAsync: false,
      redux: true,
    })
  })

  describe('@lifecycle', () => {
    beforeEach(() => jest.resetAllMocks())

    describe('componentWillReceiveProps', () => {
      it('calls hashOrderSummary and setOrderSummaryHash', () => {
        const { instance } = renderComponent(initialProps)
        const setOrderSummaryHash = jest.fn()
        const nextProps = {
          ...initialProps,
          setOrderSummaryHash,
          billingCardDetails: {
            fields: {
              paymentType: {
                value: 'KLRNA',
              },
            },
          },
        }
        // expect(hashOrderSummary).not.toHaveBeenCalled()
        instance.componentWillReceiveProps(nextProps)
        expect(setOrderSummaryHash).toHaveBeenCalledTimes(1)
        // expect(hashOrderSummary).toHaveBeenCalledTimes(1)
        // expect(hashOrderSummary).toHaveBeenCalledWith(nextProps)
      })
    })

    describe('componentDidMount', () => {
      it('calls resetAddress', () => {
        const resetAddress = jest.fn()
        const { instance } = renderComponent({
          ...initialProps,
          location: {
            hash: '',
          },
          resetAddress,
        })
        expect(resetAddress).not.toHaveBeenCalled()
        instance.componentDidMount()
        expect(resetAddress).toHaveBeenCalledTimes(1)
      })

      it('setOrderSummaryHash called if there is a existing order hash', () => {
        const setOrderSummaryHash = jest.fn()
        const { instance } = renderComponent({
          ...initialProps,
          location: {
            hash: '',
          },
          resetAddress: jest.fn(),
          setOrderSummaryHash,
        })
        global.process.browser = true
        const hash = 'fd7fa5608912fedf866068a14aa3a320'
        global.window.localStorage.setItem('orderHash', hash)
        expect(setOrderSummaryHash).not.toHaveBeenCalled()
        instance.componentDidMount()
        expect(setOrderSummaryHash).toHaveBeenCalledTimes(1)
        expect(setOrderSummaryHash).toHaveBeenCalledWith(hash)
        global.window.localStorage.removeItem('orderHash')
      })

      it('calls global.window.scrollTo without hash', () => {
        const { instance } = renderComponent({
          ...initialProps,
          location: {
            hash: '',
          },
          resetAddress: jest.fn(),
        })
        global.window.scrollTo = jest.fn()
        expect(global.window.scrollTo).not.toHaveBeenCalled()
        instance.componentDidMount()
        expect(global.window.scrollTo).toHaveBeenCalledTimes(1)
        expect(global.window.scrollTo).toHaveBeenCalledWith(0, 0)
      })

      it('calls global.window.scrollTo with hash', () => {
        const { instance } = renderComponent({
          ...initialProps,
          location: {
            hash: '#fakehash',
          },
          resetAddress: jest.fn(),
        })
        global.window.scrollTo = jest.fn()
        document.getElementById = jest.fn()
        document.getElementById.mockReturnValueOnce({
          offsetTop: 10,
        })
        expect(global.window.scrollTo).not.toHaveBeenCalled()
        instance.componentDidMount()
        expect(global.window.scrollTo).toHaveBeenCalledTimes(1)
        expect(global.window.scrollTo).toHaveBeenCalledWith(0, 10)
      })
    })
  })

  describe('@methods', () => {
    describe('onChangeDeliveryAsBilling', () => {
      it('should call copyDeliveryValuesToBillingForms when checked = true', () => {
        const copyDeliveryValuesToBillingForms = jest.fn()
        const setDeliveryAsBillingFlag = jest.fn()
        const { instance } = renderComponent({
          ...initialProps,
          copyDeliveryValuesToBillingForms,
          setDeliveryAsBillingFlag,
        })
        const checked = true
        expect(copyDeliveryValuesToBillingForms).not.toHaveBeenCalled()
        instance.onChangeDeliveryAsBilling(checked)
        expect(copyDeliveryValuesToBillingForms).toHaveBeenCalledTimes(1)
        expect(copyDeliveryValuesToBillingForms).toHaveBeenCalledWith()
        expect(setDeliveryAsBillingFlag).toHaveBeenCalledTimes(1)
        expect(setDeliveryAsBillingFlag).toHaveBeenCalledWith(true)
      })
      it('should call resetBillingForms when checked = false', () => {
        const resetBillingForms = jest.fn()
        const setDeliveryAsBillingFlag = jest.fn()
        const { instance } = renderComponent({
          ...initialProps,
          resetBillingForms,
          setDeliveryAsBillingFlag,
        })
        const checked = false
        expect(resetBillingForms).not.toHaveBeenCalled()
        instance.onChangeDeliveryAsBilling(checked)
        expect(resetBillingForms).toHaveBeenCalledTimes(1)
        expect(resetBillingForms).toHaveBeenCalledWith()
        expect(setDeliveryAsBillingFlag).toHaveBeenCalledTimes(1)
        expect(setDeliveryAsBillingFlag).toHaveBeenCalledWith(false)
      })
    })
  })

  describe('@events', () => {
    describe('On Submit Order button click', () => {
      it('should submit order if valid', () => {
        const submitOrderMock = jest.fn(() => {
          return Promise.resolve()
        })
        const validateFormsMock = jest.fn((_, { onValid }) => onValid())
        const { wrapper, instance } = renderComponent({
          ...initialProps,
          submitOrder: submitOrderMock,
          validateForms: validateFormsMock,
        })
        wrapper.find('.PaymentContainer-paynow').prop('clickHandler')()
        expect(submitOrderMock).toHaveBeenCalled()
        expect(instance.props.sendAnalyticsClickEvent).toHaveBeenCalledTimes(1)
        expect(instance.props.sendAnalyticsClickEvent).toHaveBeenCalledWith({
          category: GTM_CATEGORY.CHECKOUT,
          action: GTM_ACTION.CLICKED,
          label: 'confirm-and-pay',
          value: '',
        })
        expect(instance.props.sendAnalyticsErrorMessage).not.toHaveBeenCalled()
      })
    })

    describe('On "Same as delivery address" checkbox onChange', () => {
      it('should call onChangeDeliveryAsBilling', () => {
        const { wrapper, instance } = renderComponent(initialProps)
        instance.onChangeDeliveryAsBilling = jest.fn()
        expect(instance.onChangeDeliveryAsBilling).not.toHaveBeenCalled()
        wrapper
          .find('.PaymentContainer-checkbox')
          .first()
          .simulate('change')
        expect(instance.onChangeDeliveryAsBilling).toHaveBeenCalledTimes(1)
      })
    })

    describe('onSetBillingDetailsFormField', () => {
      it('does not call setDeliveryAsBillingFlag when copying values from the yourDetails form', () => {
        const props = {
          ...initialProps,
          yourDetails: {
            fields: {
              firstName: { value: 'Johnny' },
            },
          },
        }
        const { instance } = renderComponent(props)
        instance.onSetBillingDetailsFormField(
          'billingDetails',
          'firstName',
          'Johnny',
          'k'
        )
        expect(initialProps.setDeliveryAsBillingFlag).not.toHaveBeenCalled()
        expect(initialProps.setFormField).toHaveBeenCalledWith(
          'billingDetails',
          'firstName',
          'Johnny',
          'k'
        )
      })
      it('calls setDeliveryAsBillingFlag when values are different from the yourDetails form', () => {
        const props = {
          ...initialProps,
          yourDetails: {
            fields: {
              firstName: { value: 'Andrea' },
            },
          },
        }
        const { instance } = renderComponent(props)
        instance.onSetBillingDetailsFormField(
          'billingDetails',
          'firstName',
          'Johnny',
          'k'
        )
        expect(initialProps.setDeliveryAsBillingFlag).toHaveBeenCalledWith(
          false
        )
        expect(initialProps.setFormField).toHaveBeenCalledWith(
          'billingDetails',
          'firstName',
          'Johnny',
          'k'
        )
      })
    })

    describe('onChangeBillingAddress', () => {
      it('should call `setDeliveryAsBillingFlag` with `true` on address preview change', () => {
        const setDeliveryAsBillingFlagMock = jest.fn()
        const props = {
          ...initialProps,
          homeDeliverySelected: true,
          useDeliveryAsBilling: true,
          setDeliveryAsBillingFlag: setDeliveryAsBillingFlagMock,
        }
        const { wrapper } = renderComponent(props)
        wrapper.find(AddressPreview).prop('onClickChangeButton')()
        expect(setDeliveryAsBillingFlagMock).toHaveBeenCalledWith(false)
      })
    })

    describe('handleSubmit', () => {
      it('submits order when there are no errors', () => {
        const submitOrderMock = jest.fn(() => {
          return Promise.resolve()
        })

        const { instance } = renderComponent({
          ...initialProps,
          submitOrder: submitOrderMock,
          formNames: ['AAAA'],
          validateForms: jest.fn((formNames, { onValid = () => {} }) => {
            onValid()
          }),
        })

        expect(submitOrderMock).not.toHaveBeenCalled()
        instance.handleSubmit()
        expect(submitOrderMock).toHaveBeenCalledTimes(1)
        expect(instance.props.sendAnalyticsClickEvent).toHaveBeenCalledTimes(1)
        expect(instance.props.sendAnalyticsClickEvent).toHaveBeenCalledWith({
          category: GTM_CATEGORY.CHECKOUT,
          action: GTM_ACTION.CLICKED,
          label: 'confirm-and-pay',
          value: '',
        })
        expect(instance.props.sendAnalyticsErrorMessage).not.toHaveBeenCalled()
      })
      it('order not submits when there are errors', async () => {
        const submitOrderMock = jest.fn(() => {
          return Promise.resolve(Error('submit error'))
        })
        const eventBasedAnalyticsMock = jest.fn()
        const { instance } = renderComponent({
          ...initialProps,
          submitOrder: submitOrderMock,
          eventBasedAnalytics: eventBasedAnalyticsMock,
          formNames: ['AAAA'],
          validateForms: jest.fn((formNames, { onValid = () => {} }) => {
            onValid()
          }),
        })

        expect(eventBasedAnalyticsMock).not.toHaveBeenCalled()
        await instance.handleSubmitOrder()
        expect(eventBasedAnalyticsMock).toHaveBeenCalledTimes(1)
        expect(eventBasedAnalyticsMock).toHaveBeenCalledWith({
          eVar72: 'submit error',
        })
      })
    })
  })
})
