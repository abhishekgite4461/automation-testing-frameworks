import PropTypes from 'prop-types'
import { path, pluck, pick, isEmpty } from 'ramda'
import React, { Component } from 'react'
import Helmet from 'react-helmet'
import { connect } from 'react-redux'

import { fixTotal, giftCardCoversTotal } from '../../../../lib/checkout'
import { hashOrderSummary } from '../../../../lib/checkout-utilities/klarna-utils'
import { KLARNA, PAYPAL } from '../../../../constants/paymentTypes'
import { selectedDeliveryLocation } from '../../../../lib/checkout-utilities/reshaper'
import {
  getAddressForm,
  getDetailsForm,
  getErrors,
  selectedDeliveryLocationTypeEquals,
} from '../../../../selectors/checkoutSelectors'
import {
  isMobile,
  isMobileBreakpoint,
} from '../../../../selectors/viewportSelectors'
import analyticsDecorator from '../../../../../client/lib/analytics/analytics-decorator'
import { eventBasedAnalytics } from '../../../../lib/analytics/analytics'
import { sendEvent } from '../../../../actions/common/googleAnalyticsActions'

// actions
import { setOrderSummaryHash } from '../../../../actions/common/klarnaActions'
import { submitOrder } from '../../../../actions/common/orderActions'
import { openMiniBag } from '../../../../actions/common/shoppingBagActions'
import {
  resetFormPartial,
  setFormField,
  setFormMeta,
  touchedFormField,
  touchedMultipleFormFields,
} from '../../../../actions/common/formActions'
import {
  resetAddress,
  setDeliveryAsBillingFlag,
  copyDeliveryValuesToBillingForms,
  resetBillingForms,
  selectDeliveryCountry,
  validateForms,
} from '../../../../actions/common/checkoutActions'
import { getAllPaymentMethods } from '../../../../actions/common/paymentMethodsActions'
import SimpleTotals from '../../../common/SimpleTotals/SimpleTotals'
import {
  sendAnalyticsClickEvent,
  sendAnalyticsErrorMessage,
  GTM_CATEGORY,
  GTM_ACTION,
  ANALYTICS_ERROR,
} from '../../../../analytics'

// components
import PromotionCode from '../../PromotionCode/PromotionCode'
import BillingDetailsForm from '../shared/DetailsForm/BillingDetailsForm'
import BillingAddressForm from '../shared/AddressForm/BillingAddressForm'
import PaymentDetailsContainer from '../shared/PaymentDetailsContainer'
import Button from '../../../common/Button/Button'
import Checkbox from '../../../common/FormComponents/Checkbox/Checkbox'
import AddressPreview from '../shared/AddressPreview'
import QuickViewOrderSummary from '../../../common/QuickViewOrderSummary/QuickViewOrderSummary'
import GiftCardsContainer from '../shared/GiftCardsContainer'
import TermsAndConditionsContainer from '../shared/TermsAndConditionsContainer'
import Message from '../../../common/FormComponents/Message/Message'
import CheckoutTitle from '../shared/CheckoutTitle'
import TotalCost from '../shared/TotalCost'
import Espot from '../../Espot/Espot'
import SavePaymentDetailsCheckboxContainer from '../shared/SavePaymentDetailsCheckboxContainer'
import espotsDesktopConstants from '../../../../constants/espotsDesktop'

const formNames = [
  'billingDetails',
  'billingAddress',
  'billingFindAddress',
  'giftCard',
  'billingCardDetails',
  'order',
]

const mapStateToProps = (state) => ({
  billingAddress: getAddressForm('billing', state),
  billingDetails: getDetailsForm('billing', state),
  billingCardDetails: state.forms.checkout.billingCardDetails,
  yourAddress: getAddressForm('delivery', state),
  yourDetails: getDetailsForm('delivery', state),
  isMobile: isMobile(state),
  isMobileBreakpoint: isMobileBreakpoint(state),
  location: state.routing.location,
  shoppingBag: state.shoppingBag,
  orderSummary: state.checkout.orderSummary,
  orderSummaryHash: state.klarna.orderSummaryHash,
  klarnaAuthToken: state.klarna.authorizationToken,
  showKlarnaForm: state.klarna.showForm,
  useDeliveryAsBilling: state.checkout.useDeliveryAsBilling,
  homeDeliverySelected: selectedDeliveryLocationTypeEquals(state, 'HOME'),
  errorMessage: path(
    ['forms', 'checkout', 'order', 'message', 'message'],
    state
  ),
  formErrors: getErrors(formNames, state),
})

const mapDispatchToProps = {
  resetAddress,
  setDeliveryAsBillingFlag,
  copyDeliveryValuesToBillingForms,
  resetBillingForms,
  selectDeliveryCountry,
  resetFormPartial,
  setFormField,
  setFormMeta,
  touchedFormField,
  touchedMultipleFormFields,
  openMiniBag,
  submitOrder,
  setOrderSummaryHash,
  validateForms,
  eventBasedAnalytics,
  sendEvent,
  sendAnalyticsClickEvent,
  sendAnalyticsErrorMessage,
}

class PaymentContainer extends Component {
  static propTypes = {
    orderSummary: PropTypes.object.isRequired,
    location: PropTypes.object,
    yourDetails: PropTypes.object,
    billingAddress: PropTypes.object,
    billingCardDetails: PropTypes.object,
    resetAddress: PropTypes.func,
    openMiniBag: PropTypes.func,
    klarnaAuthToken: PropTypes.string,
    showKlarnaForm: PropTypes.bool,
    setOrderSummaryHash: PropTypes.func,
    orderSummaryHash: PropTypes.string,
    setDeliveryAsBillingFlag: PropTypes.func,
    homeDeliverySelected: PropTypes.bool,
    submitOrder: PropTypes.func.isRequired,
    validateForms: PropTypes.func.isRequired,
    formNames: PropTypes.arrayOf(PropTypes.string),
    formErrors: PropTypes.object,
    sendEvent: PropTypes.func.isRequired,
  }

  static defaultProps = {
    errorMessage: undefined,
    formNames,
    formErrors: {},
  }

  static contextTypes = {
    l: PropTypes.func,
    p: PropTypes.func,
  }

  static needs = [getAllPaymentMethods]

  componentDidMount() {
    const element =
      this.props.location.hash &&
      document.getElementById(this.props.location.hash.substring(1))
    const scrollPos = element ? element.offsetTop : 0

    window.scrollTo(0, scrollPos)

    if (!this.props.useDeliveryAsBilling) {
      // N.B This resets the find address search state, not the users address info.
      this.props.resetAddress()
    }

    const existingOrderHash =
      process.browser && window.localStorage.getItem('orderHash')
    if (existingOrderHash) this.props.setOrderSummaryHash(existingOrderHash)

    this.props.sendEvent('Checkout', 'Payment', 'Billing Options')
  }

  componentWillReceiveProps(nextProps) {
    const nextPaymentType =
      nextProps.billingCardDetails.fields.paymentType.value
    const thisPaymentType = this.props.billingCardDetails.fields.paymentType
      .value
    if (nextPaymentType !== thisPaymentType) {
      const { setOrderSummaryHash, orderSummaryHash } = nextProps
      if (nextPaymentType === KLARNA && !orderSummaryHash) {
        setOrderSummaryHash(hashOrderSummary(nextProps))
      }
    }
  }

  onChangeDeliveryAsBilling = (checked) => {
    const {
      setDeliveryAsBillingFlag,
      copyDeliveryValuesToBillingForms,
      resetBillingForms,
    } = this.props
    if (checked) {
      copyDeliveryValuesToBillingForms()
    } else {
      resetBillingForms()
    }
    setDeliveryAsBillingFlag(checked)
  }

  onChangeBillingAddress = () => {
    this.props.setDeliveryAsBillingFlag(false)
  }

  onSetBillingDetailsFormField = (formName, field, value, key) => {
    const { setFormField, setDeliveryAsBillingFlag, yourDetails } = this.props
    const deliveryValue = path(['fields', field, 'value'], yourDetails)
    if (deliveryValue !== value) {
      setDeliveryAsBillingFlag(false)
    }
    setFormField(formName, field, value, key)
  }

  handleSubmitOrder = () => {
    const { submitOrder, eventBasedAnalytics } = this.props
    return submitOrder().then((result) => {
      if (result instanceof Error) {
        eventBasedAnalytics({
          eVar72: result.message,
        })
      }
    })
  }

  handleSubmit = () => {
    const {
      formNames,
      validateForms,
      formErrors,
      sendAnalyticsClickEvent,
      sendAnalyticsErrorMessage,
    } = this.props

    sendAnalyticsClickEvent({
      category: GTM_CATEGORY.CHECKOUT,
      action: GTM_ACTION.CLICKED,
      label: 'confirm-and-pay',
      value: '',
    })

    if (Object.keys(formErrors).length > 0)
      sendAnalyticsErrorMessage(ANALYTICS_ERROR.CONFIRM_AND_PAY)

    validateForms(formNames, {
      onValid: this.handleSubmitOrder,
    })
  }

  render() {
    const { l, p } = this.context
    const {
      billingAddress,
      billingCardDetails,
      isMobile,
      isMobileBreakpoint,
      orderSummary,
      openMiniBag,
      orderSummary: { giftCards, basket },
      klarnaAuthToken,
      showKlarnaForm,
      useDeliveryAsBilling,
      yourDetails,
      homeDeliverySelected,
      errorMessage,
      formErrors,
    } = this.props

    let calculateTotal
    let shippingInfo
    let priceInfo
    let discounts
    if (orderSummary && orderSummary.deliveryLocations && orderSummary.basket) {
      shippingInfo = {
        ...selectedDeliveryLocation(orderSummary.deliveryLocations),
        estimatedDelivery: orderSummary.estimatedDelivery,
      }
      priceInfo = pick(['subTotal'], orderSummary.basket)
      discounts = path(['basket', 'discounts'], orderSummary)
      calculateTotal = fixTotal(
        priceInfo.subTotal,
        shippingInfo.cost,
        pluck('value', discounts)
      )
    }
    const paymentType = billingCardDetails.fields.paymentType.value
    let orderButtonText = l`Order and Pay Now`
    if (paymentType === PAYPAL) orderButtonText = l`Pay via PayPal`
    if (paymentType === KLARNA) orderButtonText = l`Pay via Klarna`

    const billingFormsHidden = homeDeliverySelected && useDeliveryAsBilling

    if (!Object.keys(orderSummary).length) return null

    return (
      <section className="PaymentContainer PaymentContainer--v2">
        <Helmet title={l`Billing Options`} />
        <QuickViewOrderSummary openMiniBag={openMiniBag} l={l} />
        <div className="PaymentContainer-content">
          {isMobile && <TotalCost totalCost={p(calculateTotal)} />}
          <CheckoutTitle separator>{l`Billing address`}</CheckoutTitle>
          <Checkbox
            className="PaymentContainer-checkbox"
            checked={{ value: useDeliveryAsBilling }}
            onChange={() =>
              this.onChangeDeliveryAsBilling(!useDeliveryAsBilling)
            }
            name="deliveryAsBilling"
          >
            {l`Same as delivery address`}
          </Checkbox>
          <div className="PaymentContainer-sectionWrapper">
            {billingFormsHidden ? (
              <AddressPreview
                address={pluck('value', billingAddress.fields)}
                details={pluck('value', yourDetails.fields)}
                onClickChangeButton={this.onChangeBillingAddress}
              />
            ) : (
              <div>
                <BillingDetailsForm renderTelephone={isMobileBreakpoint} />
                <BillingAddressForm renderTelephone={!isMobileBreakpoint} />
              </div>
            )}
            <Espot
              identifier={espotsDesktopConstants.orderSummary.discountIntro}
            />
          </div>
          <PromotionCode
            isOpenIfPopulated
            isContentPadded={false}
            key="PromotionCode"
            gtmCategory={GTM_CATEGORY.CHECKOUT}
          />
          <div className="PaymentContainer-sectionWrapper">
            <GiftCardsContainer showTotal />
          </div>

          <Espot identifier={espotsDesktopConstants.orderSummary.toBeDefined} />

          {giftCards &&
            basket &&
            !giftCardCoversTotal(giftCards, basket.total) &&
            (!orderSummary.creditCard || billingCardDetails.isShown) && (
              <PaymentDetailsContainer />
            )}
          <TermsAndConditionsContainer />
          <SavePaymentDetailsCheckboxContainer />
          <div className="PaymentContainer-sectionWrapper">
            {isMobile && (
              <SimpleTotals
                className={'DeliveryPaymentContainer-simpleTotals'}
                shippingInfo={shippingInfo}
                priceInfo={priceInfo}
                discounts={discounts}
              />
            )}
          </div>
          {!showKlarnaForm &&
            (paymentType !== KLARNA || klarnaAuthToken) && (
              <Button
                className="PaymentContainer-paynow"
                type="submit"
                clickHandler={this.handleSubmit}
                isActive={isEmpty(formErrors)}
              >
                {orderButtonText}
              </Button>
            )}
          {errorMessage && (
            <Message
              message={errorMessage}
              type="error"
              onDidMount={(message) => message.scrollTo()}
            />
          )}
        </div>
      </section>
    )
  }
}

export default analyticsDecorator('order-submit-page', { isAsync: false })(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PaymentContainer)
)

export {
  PaymentContainer as WrappedPaymentContainer,
  mapStateToProps,
  mapDispatchToProps,
}
