import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'
import CmsNotAvailable from '../../common/CmsNotAvailable/CmsNotAvailable'
import { getDressipiBrand } from '../../../lib/dressipi'
import { getCanonicalHostname } from '../../../../shared/lib/canonicalisation'
import { fromSeoUrlToRedirectionUrl } from '../../../../shared/lib/navigation'
import analyticsDecorator from '../../../../client/lib/analytics/analytics-decorator'

@analyticsDecorator('style-adviser')
@connect(
  (state) => ({
    height: state.viewport.height,
    width: state.viewport.width,
    brandCode: state.config.brandCode,
    region: state.config.region,
    baseUrl: state.routing.location.hostname,
    pathname: state.routing.location.pathname,
    menuLinks: state.navigation.menuLinks,
  }),
  {}
)
export default class Dressipi extends Component {
  static propTypes = {
    height: PropTypes.number,
    width: PropTypes.number,
    brandCode: PropTypes.string,
    region: PropTypes.string,
    baseUrl: PropTypes.string,
    pathname: PropTypes.string,
    menuLinks: PropTypes.array,
  }

  render() {
    const {
      height,
      width,
      brandCode,
      region,
      baseUrl,
      pathname,
      menuLinks,
    } = this.props
    const { location, brand } = getDressipiBrand(brandCode, region)
    const redirectionUrl = fromSeoUrlToRedirectionUrl(pathname, menuLinks)
    const canonicalPath = redirectionUrl
      ? decodeURIComponent(redirectionUrl)
      : ''

    return (
      <div>
        {brand ? (
          <div className="Dressipi">
            <Helmet
              title={
                brand === 'topshop.com'
                  ? 'My Topshop Wardrobe'
                  : 'Style Adviser'
              }
              link={[
                {
                  rel: 'canonical',
                  href: getCanonicalHostname(baseUrl) + canonicalPath,
                },
              ]}
            />
            <iframe
              className="Dressipi-module"
              src={`//dressipi-production${location}.${brand}?mobile=1`}
              width={`${width}px`}
              height={`${height - 48}px`}
            />
          </div>
        ) : (
          <CmsNotAvailable />
        )}
      </div>
    )
  }
}
