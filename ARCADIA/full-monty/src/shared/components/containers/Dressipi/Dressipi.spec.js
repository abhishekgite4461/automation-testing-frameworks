import React from 'react'
import { shallow, mount } from 'enzyme'
import { analyticsDecoratorHelper } from 'test/unit/helpers/test-component'
import Dressipi from './Dressipi'
import CmsNotAvailable from '../../common/CmsNotAvailable/CmsNotAvailable'

describe('<Dressipi />', () => {
  describe('@decorators', () => {
    analyticsDecoratorHelper(Dressipi, 'style-adviser', {
      componentName: 'Dressipi',
      sendAdobe: true,
      isAsync: false,
    })
  })

  const WrappedComponent = Dressipi.WrappedComponent.WrappedComponent

  it('exists', () => {
    const wrapper = shallow(<WrappedComponent brandCode="ts" region="uk" />)
    expect(wrapper.find('.Dressipi').length).toBe(1)
  })

  it('for TSUK returns UK My Topshop Wardrobe', () => {
    const wrapper = shallow(<WrappedComponent brandCode="ts" region="uk" />)
    expect(wrapper.find('.Dressipi-module').prop('src')).toBe(
      '//dressipi-production.topshop.com?mobile=1'
    )
  })

  it('for TSUS returns US My Topshop Wardrobe', () => {
    const wrapper = shallow(<WrappedComponent brandCode="ts" region="us" />)
    expect(wrapper.find('.Dressipi-module').prop('src')).toBe(
      '//dressipi-production-us.topshop.com?mobile=1'
    )
  })

  it('for WLUK returns Wallis Style Adviser', () => {
    const wrapper = shallow(<WrappedComponent brandCode="wl" region="uk" />)
    expect(wrapper.find('.Dressipi-module').prop('src')).toBe(
      '//dressipi-production.wallis.co.uk?mobile=1'
    )
  })

  it('for EVUK returns Wallis Style Adviser', () => {
    const wrapper = shallow(<WrappedComponent brandCode="ev" region="uk" />)
    expect(wrapper.find('.Dressipi-module').prop('src')).toBe(
      '//dressipi-production.evans.co.uk?mobile=1'
    )
  })

  it('for other brand code returns Not Found', () => {
    const wrapper = shallow(<WrappedComponent brandCode="ts" region="de" />)
    expect(wrapper.find(CmsNotAvailable).length).toBe(1)
  })

  describe('mapStateToProps', () => {
    it('builds props from state', () => {
      const store = {
        subscribe: () => {},
        dispatch: () => {},
        getState: () => ({
          config: {
            brandCode: 'ts',
            region: 'uk',
          },
          navigation: {
            menuLinks: [],
          },
          loaderOverlay: {
            visible: false,
          },
          routing: {
            location: {
              hostname: 'm.topshop.com',
              pathname: '/foo',
            },
            visited: ['/'],
          },
          products: [],
          account: {},
          viewport: {
            height: 800,
            width: 320,
          },
        }),
      }
      const wrapper = mount(<Dressipi store={store} />)

      expect(wrapper.find(WrappedComponent).props()).toMatchObject({
        height: 800,
        width: 320,
        brandCode: 'ts',
        region: 'uk',
        baseUrl: 'm.topshop.com',
        pathname: '/foo',
        menuLinks: [],
      })
    })
  })
})
