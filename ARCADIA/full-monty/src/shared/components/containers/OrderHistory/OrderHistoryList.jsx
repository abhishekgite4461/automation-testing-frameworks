import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import OrderListContainer from '../OrderListContainer/OrderListContainer'

import { orderHistoryRequest } from '../../../actions/common/accountActions'
import { numbers } from '../../../constants/customerCareNumbers'

import { TYPE } from '../OrderListContainer/types'
import analyticsDecorator from '../../../../client/lib/analytics/analytics-decorator'

@analyticsDecorator('my-orders', { isAsync: true })
@connect(
  (state) => ({
    brandCode: state.config.brandCode,
    orders: state.orderHistory.orders,
    region: state.config.region,
    visited: state.routing.visited,
    orderHistoryMessageFeature: state.features.status.FEATURE_ORDER_HISTORY_MSG,
  }),
  { orderHistoryRequest }
)
export default class OrderHistoryList extends Component {
  static propTypes = {
    brandCode: PropTypes.string.isRequired,
    orderHistoryMessageFeature: PropTypes.bool.isRequired,
    orderHistoryRequest: PropTypes.func.isRequired,
    region: PropTypes.string.isRequired,
    visited: PropTypes.array.isRequired,
    orders: PropTypes.array,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  static needs = [orderHistoryRequest]

  componentDidMount() {
    const { orderHistoryRequest, visited } = this.props
    // first fetch is performed by server-side render
    if (visited.length > 1) orderHistoryRequest()
  }

  /*
    sorts from highest to lowest
   */
  sortOrders(list) {
    return [...list].sort((a, b) => b.orderId - a.orderId)
  }

  renderOrderLimitMessage() {
    const { l } = this.context
    const { brandCode, region } = this.props
    const brandNumbers = numbers[brandCode]
    const customerCareNumber = brandNumbers[region] || brandNumbers.intl
    const limitMessage = l`Displaying your last 20 orders. For your full order history, please contact customer service at ${customerCareNumber}.`
    return (
      <p className="OrderHistoryList-orderLimitMessage">
        {(brandNumbers[region] || (region === 'eu' && brandNumbers.intl)) &&
          limitMessage}
        {region !== 'eu' &&
          brandNumbers.intl &&
          ` ${l`For international customers, please call ${
            brandNumbers.intl
          }.`}`}
      </p>
    )
  }

  render() {
    const { l } = this.context
    const { orders, orderHistoryMessageFeature } = this.props
    const hasOrders = Array.isArray(orders) && orders.length > 0

    const orderLimitMessage =
      hasOrders && orderHistoryMessageFeature
        ? this.renderOrderLimitMessage()
        : null

    return (
      <OrderListContainer
        className="OrderHistoryList"
        header={orderLimitMessage}
        notFoundMessage={l`There were no orders found`}
        orders={this.sortOrders(orders)}
        title={l`My orders`}
        type={TYPE.ORDER}
      />
    )
  }
}
