import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Image from '../../../common/Image/Image'
import { removeUrlProtocol } from '../../../../lib/html-entities'

export default class FooterNavigation extends Component {
  static propTypes = {
    footerCategories: PropTypes.array,
  }

  getCategoryCol = ({ label, links, images }, catId) => {
    const uid = `FooterNavigation-${catId}`
    return (
      <div key={uid} className="FooterNavigation-category">
        <h3 className="FooterNavigation-categoryTitle">{label}</h3>
        {links &&
          links.map((link, linkId) => this.getTextLink(link, linkId, catId))}
        {images &&
          images.map((image, linkId) =>
            this.getImageLink(image, linkId, catId)
          )}
      </div>
    )
  }

  getTextLink = ({ label, linkUrl, openNewWindow }, linkId, catId) => {
    const target = openNewWindow ? '_blank' : '_self'
    const uid = `FooterNavigation-${catId}-${linkId}`
    return (
      <div key={uid} className="FooterNavigation-categoryItem">
        <a
          className="FooterNavigation-textLink"
          href={removeUrlProtocol(linkUrl)}
          target={target}
        >
          {label}
        </a>
      </div>
    )
  }

  getImageLink = (
    { imageUrl, imageSize = '', linkUrl, alt, openNewWindow },
    linkId,
    catId
  ) => {
    const target = openNewWindow ? '_blank' : '_self'
    const uid = `FooterNavigation-${catId}-${linkId}`
    const size = imageSize
      ? imageSize.charAt(0).toUpperCase() + imageSize.slice(1)
      : 0
    return (
      <div key={uid} className="FooterNavigation-categoryItem">
        <a
          className="FooterNavigation-imageLink"
          href={removeUrlProtocol(linkUrl)}
          target={target}
        >
          <Image
            className={`FooterNavigation-image${size}`}
            src={removeUrlProtocol(imageUrl)}
            alt={alt}
          />
        </a>
      </div>
    )
  }

  render() {
    const { footerCategories } = this.props
    // TODO : move to the reduced or helper action
    const categories =
      footerCategories.length > 5
        ? footerCategories.splice(0, 5)
        : footerCategories

    return (
      <div className="FooterNavigation">
        {categories.map((category, catId) =>
          this.getCategoryCol(category, catId)
        )}
      </div>
    )
  }
}
