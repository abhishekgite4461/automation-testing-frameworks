import testComponentHelper from 'test/unit/helpers/test-component'
import FooterNavigation from '../FooterNavigation'
import footerCol3Img2Lg from '../../../../../../server/handlers/mocks/footer-3-category-cols-2-large-image-cols.json'
import footerCol4Img1Sm from '../../../../../../server/handlers/mocks/footer-4-category-cols-1-small-images-col.json'

const initialProps = {
  footerCategories: [footerCol4Img1Sm.categories[0]],
}
const link = footerCol4Img1Sm.categories[0].links[0]

describe('<FooterNavigation />', () => {
  const renderComponent = testComponentHelper(FooterNavigation)

  describe('@renders', () => {
    it('in default state', () => {
      const { getTree } = renderComponent(initialProps)
      expect(getTree()).toMatchSnapshot()
    })

    it('does not display more than 5 columns', () => {
      const { instance, wrapper } = renderComponent({
        footerCategories: [
          ...footerCol4Img1Sm.categories,
          ...footerCol4Img1Sm.categories,
        ],
      })
      instance.getCategoryCol = jest.fn()
      instance.render()
      expect(instance.getCategoryCol).toHaveBeenCalledTimes(5)
      expect(wrapper.find('.FooterNavigation-category')).toHaveLength(5)
    })

    it('displays category title', () => {
      const { wrapper } = renderComponent(initialProps)
      expect(wrapper.find('.FooterNavigation-categoryTitle')).toHaveLength(1)
    })

    it('displays a column with 8 text links', () => {
      const { instance, wrapper } = renderComponent(initialProps)
      instance.getTextLink = jest.fn()
      instance.render()
      expect(instance.getTextLink).toHaveBeenCalledTimes(8)
      expect(wrapper.find('.FooterNavigation-textLink')).toHaveLength(8)
    })

    it('displays a column with 1 large image link', () => {
      const { instance, wrapper } = renderComponent({
        footerCategories: [footerCol3Img2Lg.categories[4]],
      })
      instance.getImageLink = jest.fn()
      instance.render()
      expect(instance.getImageLink).toHaveBeenCalledTimes(1)
      expect(wrapper.find('.FooterNavigation-imageLarge')).toHaveLength(1)
    })

    it('displays a column with 2 small image links', () => {
      const { instance, wrapper } = renderComponent({
        footerCategories: [footerCol4Img1Sm.categories[4]],
      })
      instance.getImageLink = jest.fn()
      instance.render()
      expect(instance.getImageLink).toHaveBeenCalledTimes(2)
      expect(wrapper.find('.FooterNavigation-imageSmall')).toHaveLength(2)
    })

    it('link should open in a new window if openNewWindow is true', () => {
      link.openNewWindow = true
      const { wrapper } = renderComponent({
        footerCategories: [{ links: [link] }],
      })
      expect(wrapper.find('.FooterNavigation-textLink').prop('target')).toBe(
        '_blank'
      )
    })

    it('link should open in same window if openNewWindow is false', () => {
      link.openNewWindow = false
      const { wrapper } = renderComponent({
        footerCategories: [{ links: [link] }],
      })
      expect(wrapper.find('.FooterNavigation-textLink').prop('target')).toBe(
        '_self'
      )
    })
  })
})
