import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { browserHistory } from 'react-router'

import Button from '../../common/Button/Button'

const WISHLIST_ICON = `<i class='EmptyWishlist-wishlistIcon'></i>`

class EmptyWishlist extends Component {
  static propTypes = {
    isUserAuthenticated: PropTypes.bool,
    onSignInHandler: PropTypes.func,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  render() {
    const { l } = this.context
    const { isUserAuthenticated, onSignInHandler } = this.props
    return (
      <div className="EmptyWishlist">
        <span
          className="EmptyWishlist-message"
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{
            __html: l`Select the ${WISHLIST_ICON} icon next to our products to save them to your Wish List.`,
          }}
        />
        <Button
          className="EmptyWishlist-continueButton"
          clickHandler={() => browserHistory.push('/')}
        >
          {l`Continue Shopping`}
        </Button>
        {!isUserAuthenticated && (
          <span className="EmptyWishlist-message">
            {l`Or `}
            <button
              className="EmptyWishlist-signInButton"
              onClick={onSignInHandler}
            >
              {l`Sign In`}
            </button>
            {l` to retrieve previous Wish lists.`}
          </span>
        )}
      </div>
    )
  }
}

export default EmptyWishlist
