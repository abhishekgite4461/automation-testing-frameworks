import React from 'react'
import PropTypes from 'prop-types'

if (process.browser) require('./WishlistLimitInfo.css')

const WishlistLimitInfo = (props, { l }) => (
  <div className="WishlistLimitInfo-container">
    <p className="WishlistLimitInfo-text">
      {l`You have reached the limit of products allowed in your Wish list. To add more products, please delete items from your list.`}
    </p>
  </div>
)

WishlistLimitInfo.contextTypes = {
  l: PropTypes.func,
}

export default WishlistLimitInfo
