import testComponentHelper from '../../../../../../test/unit/helpers/test-component'
import WishlistLimitInfo from '../WishlistLimitInfo'

const context = { l: jest.fn() }
const renderComponent = testComponentHelper(WishlistLimitInfo, { context })

describe(WishlistLimitInfo.name, () => {
  it('renders correctly', () => {
    expect(renderComponent({}).getTree()).toMatchSnapshot()
  })
})
