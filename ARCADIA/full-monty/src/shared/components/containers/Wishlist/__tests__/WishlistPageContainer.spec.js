import { identity } from 'ramda'
import testComponentHelper, {
  analyticsDecoratorHelper,
} from '../../../../../../test/unit/helpers/test-component'
import DecoratedWishlistPageContainer, {
  WishlistPageContainer,
} from '../WishlistPageContainer'
import WishlistLoginModal from '../../../containers/Wishlist/WishlistLoginModal'
import { CSSTransition } from 'react-transition-group'

const itemsMock = [
  {
    catEntryId: '31621890',
  },
  {
    catEntryId: '31621847',
  },
]

const props = {
  items: null,
  isUserAuthenticated: false,
  location: { action: 'PUSH' },
  hasReachedPageBottom: false,
  isWishlistSync: false,
  getPaginatedWishlist: jest.fn(),
  triggerWishlistLoginModal: jest.fn(),
}

describe(WishlistPageContainer.name, () => {
  const context = {
    l: jest.fn(identity),
  }

  const renderComponent = testComponentHelper(WishlistPageContainer, {
    context,
  })

  describe('@decorators', () => {
    analyticsDecoratorHelper(DecoratedWishlistPageContainer, 'wishlist', {
      componentName: 'WishlistPageContainer',
      isAsync: false,
    })
  })

  describe('@constructor', () => {
    it('sets initial state when user is not authenticated', () => {
      const { wrapper } = renderComponent(props)
      expect(wrapper.state()).toEqual({
        page: 1,
        showEmptyWishlist: true,
      })
    })
    it('sets initial state when user is authenticated and wishlist items are populated', () => {
      const { wrapper } = renderComponent({
        ...props,
        isUserAuthenticated: true,
        items: [{}],
      })
      expect(wrapper.state()).toEqual({
        page: 1,
        showEmptyWishlist: false,
      })
    })
  })

  describe('@renders', () => {
    describe('when user is not authenticated', () => {
      it('renders <EmptyWishlist /> CSSTransition', () => {
        const { wrapper, getTree } = renderComponent(props)
        expect(
          wrapper
            .find(CSSTransition)
            .at(0)
            .props().in
        ).toBeTruthy()
        expect(
          wrapper
            .find(CSSTransition)
            .at(1)
            .props().in
        ).toBeFalsy()
        expect(getTree()).toMatchSnapshot()
      })
    })
    describe('when user is authenticated', () => {
      describe('when there are no wishlisted items', () => {
        it('renders <EmptyWishlist /> CSSTransition', () => {
          const { wrapper, getTree } = renderComponent({
            ...props,
            isUserAuthenticated: true,
            items: [],
          })
          expect(
            wrapper
              .find(CSSTransition)
              .at(0)
              .props().in
          ).toBeTruthy()
          expect(
            wrapper
              .find(CSSTransition)
              .at(1)
              .props().in
          ).toBeFalsy()
          expect(getTree()).toMatchSnapshot()
        })
      })
      describe('when user has items in the wishlist', () => {
        it('renders <WishlistItemContainer /> CSSTransition', () => {
          const { wrapper, getTree } = renderComponent({
            ...props,
            isUserAuthenticated: true,
            items: itemsMock,
          })
          expect(
            wrapper
              .find(CSSTransition)
              .at(0)
              .props().in
          ).toBeFalsy()
          expect(
            wrapper
              .find(CSSTransition)
              .at(1)
              .props().in
          ).toBeTruthy()
          expect(getTree()).toMatchSnapshot()
        })
      })
    })
  })
  describe('@lifecycle', () => {
    describe('on componentDidMount', () => {
      beforeEach(() => {
        window.scrollTo = jest.fn()
      })
      afterEach(() => jest.clearAllMocks())

      it('should dispatch getPaginatedWishlist when wishlist is not synced', () => {
        const { instance } = renderComponent(props)

        instance.componentDidMount()
        expect(instance.props.getPaginatedWishlist).toHaveBeenCalledTimes(1)
      })
      it('should not dispatch getPaginatedWishlist when wishlist is synced', () => {
        const { instance } = renderComponent({
          ...props,
          isWishlistSync: true,
        })

        instance.componentDidMount()
        expect(instance.props.getPaginatedWishlist).not.toHaveBeenCalled()
      })

      // @TODO write tests to cover window.toScroll logic
    })
    describe('on componentWillReceiveProps', () => {
      it('updates the page state when at the bottom of the current page', () => {
        const { wrapper, instance } = renderComponent(props)

        expect(instance.state.page).toEqual(1)
        wrapper.setProps({ hasReachedPageBottom: true })
        expect(instance.state.page).toEqual(2)
      })
      it('returns the correct amount of items to show in the wishlist when the page value updates', () => {
        const itemsProps = { ...props, items: new Array(40) }

        const { wrapper, instance } = renderComponent(itemsProps)

        expect(instance.getItemsToShow().length).toEqual(20)
        wrapper.setState({ page: 2 })
        expect(instance.getItemsToShow().length).toEqual(40)
        wrapper.setState({ page: 3 })
        expect(instance.getItemsToShow().length).toEqual(40)
      })
      it('updates to hide empty wishlist when there are items in the wishlist', () => {
        const { wrapper, instance } = renderComponent(props)

        expect(instance.state.showEmptyWishlist).toEqual(true)
        wrapper.setProps({
          isUserAuthenticated: true,
          items: itemsMock,
        })
        instance.getItemsCount()
        expect(instance.state.showEmptyWishlist).toEqual(false)
      })
      it('updates to show empty wishlist when there are no items in the wishlist', () => {
        const { wrapper, instance } = renderComponent({
          ...props,
          items: itemsMock,
          isUserAuthenticated: true,
        })

        expect(instance.state.showEmptyWishlist).toEqual(false)
        wrapper.setProps({
          items: [],
        })
        instance.getItemsCount()
        expect(instance.state.showEmptyWishlist).toEqual(true)
      })
      it('updates to show empty wishlist when user is signed out', () => {
        const nextProps = {
          isUserAuthenticated: true,
          hasReachedPageBottom: false,
        }
        const { wrapper, instance } = renderComponent({
          ...props,
          items: itemsMock,
          isUserAuthenticated: true,
        })

        expect(instance.state.showEmptyWishlist).toEqual(false)
        wrapper.setProps({
          isUserAuthenticated: false,
        })
        instance.componentWillReceiveProps(nextProps)
        expect(instance.state.showEmptyWishlist).toEqual(true)
      })
    })
  })

  describe('@methods', () => {
    describe('getItemsCount()', () => {
      it('should return null when wishlist items have not been fetched', () => {
        const { instance } = renderComponent(props)
        expect(instance.getItemsCount()).toBeNull()
      })
      it('should return itemsCount', () => {
        const { instance } = renderComponent({
          ...props,
          items: itemsMock,
        })
        expect(instance.getItemsCount()).toBe(2)
      })
    })
    describe('toggleEmptyWishlistMessage()', () => {
      it('should set the showEmptyWishlistState', () => {
        const { wrapper } = renderComponent(props)
        expect(wrapper.state().showEmptyWishlist).toBeTruthy()
        wrapper.instance().toggleEmptyWishlistMessage(false)
        expect(wrapper.state().showEmptyWishlist).toBeFalsy()
      })
    })
    describe('onSignInHandler()', () => {
      it('should dispatch triggerWishlistLoginModal', () => {
        const { instance } = renderComponent(props)
        expect(instance.props.triggerWishlistLoginModal).not.toHaveBeenCalled()
        instance.onSignInHandler()
        expect(instance.props.triggerWishlistLoginModal).toHaveBeenCalledWith(
          null,
          WishlistLoginModal,
          'wishlist'
        )
        expect(instance.props.triggerWishlistLoginModal).toHaveBeenCalledTimes(
          1
        )
      })
    })
  })

  describe('@transitions', () => {
    describe('wishlistItemContainer transition', () => {
      it('should set the showEmptyWishlist state to true when transition is finished', () => {
        const { wrapper } = renderComponent({
          ...props,
          isUserAuthenticated: true,
          items: itemsMock,
        })
        expect(wrapper.state().showEmptyWishlist).toBeFalsy()
        wrapper
          .find(CSSTransition)
          .at(1)
          .props()
          .onExited()
        expect(wrapper.state().showEmptyWishlist).toBeTruthy()
      })
    })
  })
})
