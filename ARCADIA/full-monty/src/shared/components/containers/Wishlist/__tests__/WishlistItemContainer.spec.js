import React from 'react'
import { identity } from 'ramda'
import testComponentHelper from '../../../../../../test/unit/helpers/test-component'
import WishlistItemContainer from '../WishlistItemContainer'
import AddToBagModal from '../../../common/AddToBagModal/AddToBagModal'
import * as bagActions from '../../../../actions/common/shoppingBagActions'
import * as modalActions from '../../../../actions/common/modalActions'
import * as productActions from '../../../../actions/common/productsActions'
import * as wishlistActions from '../../../../actions/common/wishlistActions'

const props = {
  items: [
    {
      imageUrl:
        'https://ts.pplive.arcadiagroup.ltd.uk/wcsstore/TopShop/images/catalog/19H30IGRY_small.jpg',
      sourceUrl:
        'http://ts.pplive.arcadiagroup.ltd.uk/webapp/wcs/stores/servlet/ProductDisplay?langId=-1&storeId=12556&catalogId=33057&productId=21642083&categoryId=209987&parent_category_rn=208549',
      colour: 'GREY',
      productId: 21642083,
      catEntryId: 21642091,
      lineNumber: '19H30IGRY',
      name: 'Lightweight Knit Beanie Hat',
      shipModeId: '',
      addToBagURL:
        'http://ts.pplive.arcadiagroup.ltd.uk/webapp/wcs/stores/servlet/InterestItemDelete?updatePrices=1&calculationUsageId=-1&calculationUsageId=-2&calculationUsageId=-7&langId=-1&storeId=12556&catalogId=33057&productId=21642083&catEntryId=21642091&orderId=700385209&calculateOrder=1&quantity=1.0&pageName=shoppingBag&savedItem=true&URL=OrderItemAddAjax%3fURL%3dOrderCalculate%3fURL%3dAddToBagFromIntListAjaxView',
      removeSavedItemURL:
        'http://ts.pplive.arcadiagroup.ltd.uk/webapp/wcs/stores/servlet/InterestItemDelete?updatePrices=1&calculationUsageId=-1&langId=-1&storeId=12556&catalogId=33057&productId=21642083&catEntryId=21642091&orderId=700385209&calculateOrder=1&quantity=1.0&URL=InterestItemsRemoveItemAjaxView',
      size: 'ONE',
      quantity: 1,
      itemquantity: 'Quantity',
      instock: true,
      updateURL:
        'http://ts.pplive.arcadiagroup.ltd.uk/webapp/wcs/stores/servlet/ChangeDetailsDisplayAjaxView?orderId=700385209&langId=-1&storeId=12556&catalogId=33057&productId=21642083&catEntryId=21642091&offerPrice=9.72&size=ONE&quantity=1.0&pageName=interestItem',
      wasPrice: 12,
      unitPrice: 9.72,
      totalLabel: 'Total',
      total: '9.72',
      listItemId: 1234,
    },
    {
      imageUrl:
        'https://ts.pplive.arcadiagroup.ltd.uk/wcsstore/TopShop/images/catalog/19H03IBRN_small.jpg',
      sourceUrl:
        'http://ts.pplive.arcadiagroup.ltd.uk/webapp/wcs/stores/servlet/ProductDisplay?langId=-1&storeId=12556&catalogId=33057&productId=21903388&categoryId=798058&parent_category_rn=277012',
      colour: 'BROWN',
      productId: 21903388,
      catEntryId: 21903390,
      lineNumber: '19H03IBRN',
      name: 'Faux Fur Bear Trapper Hat',
      shipModeId: '',
      addToBagURL:
        'http://ts.pplive.arcadiagroup.ltd.uk/webapp/wcs/stores/servlet/InterestItemDelete?updatePrices=1&calculationUsageId=-1&calculationUsageId=-2&calculationUsageId=-7&langId=-1&storeId=12556&catalogId=33057&productId=21903388&catEntryId=21903390&orderId=700385209&calculateOrder=1&quantity=1.0&pageName=shoppingBag&savedItem=true&URL=OrderItemAddAjax%3fURL%3dOrderCalculate%3fURL%3dAddToBagFromIntListAjaxView',
      removeSavedItemURL:
        'http://ts.pplive.arcadiagroup.ltd.uk/webapp/wcs/stores/servlet/InterestItemDelete?updatePrices=1&calculationUsageId=-1&langId=-1&storeId=12556&catalogId=33057&productId=21903388&catEntryId=21903390&orderId=700385209&calculateOrder=1&quantity=1.0&URL=InterestItemsRemoveItemAjaxView',
      size: 'ONE',
      quantity: 1,
      itemquantity: 'Quantity',
      instock: true,
      updateURL:
        'http://ts.pplive.arcadiagroup.ltd.uk/webapp/wcs/stores/servlet/ChangeDetailsDisplayAjaxView?orderId=700385209&langId=-1&storeId=12556&catalogId=33057&productId=21903388&catEntryId=21903390&offerPrice=14.58&size=ONE&quantity=1.0&pageName=interestItem',
      wasPrice: 18,
      unitPrice: 14.58,
      totalLabel: 'Total',
      total: '14.58',
      listItemId: 9876,
    },
  ],
  baseUrlPath: '/en/tsuk',
  wishlistId: 12345,
  grid: 4,
  store: {
    getState: () => ({ config: { storeCode: 'tsuk', lang: 'en' } }),
    dispatch: jest.fn(),
  },
  dispatch: jest.fn(() => Promise.resolve()),
}

describe(WishlistItemContainer.name, () => {
  const context = {
    l: jest.fn(identity),
  }
  const renderComponent = testComponentHelper(
    WishlistItemContainer.WrappedComponent,
    { context }
  )
  describe('@render', () => {
    beforeEach(() => {
      jest.clearAllMocks()
    })

    it('should render correctly', () => {
      expect(renderComponent(props).getTree()).toMatchSnapshot()
    })

    it('sets the selectedSize state correctly', () => {
      const { wrapper } = renderComponent(props)
      const mockProductIds = [6789, 2385]
      const mockCatEntryIds = [1234, 888989]

      wrapper
        .instance()
        .handleSizeChange(
          { target: { value: mockCatEntryIds[0] } },
          mockProductIds[0]
        )
      wrapper
        .instance()
        .handleSizeChange(
          { target: { value: mockCatEntryIds[1] } },
          mockProductIds[1]
        )

      expect(wrapper.state().selectedCatEntryIds[mockProductIds[0]]).toEqual(
        mockCatEntryIds[0]
      )
      expect(wrapper.state().selectedCatEntryIds[mockProductIds[1]]).toEqual(
        mockCatEntryIds[1]
      )
    })

    it('handles adding an item to the bag', () => {
      const { instance } = renderComponent(props)
      bagActions.addToBagWithCatEntryId = jest.fn()

      const mockProductId = 1111
      const mockCatEntryId = 2223

      instance.handleSizeChange(
        { target: { value: mockCatEntryId } },
        mockProductId
      )
      instance.handleAddToBag({ preventDefault: () => {} }, mockProductId)

      expect(props.dispatch).toHaveBeenCalled()
      expect(bagActions.addToBagWithCatEntryId).toHaveBeenCalledWith(
        mockCatEntryId,
        <AddToBagModal />
      )
    })

    it('should capture `add to bag from wishlist` event success', async () => {
      const { instance } = renderComponent(props)
      const mockProductId = 1111
      const mockCatEntryId = 2223
      bagActions.addToBagWithCatEntryId = jest.fn(() => Promise.resolve())
      wishlistActions.captureWishlistEvent = jest.fn()

      instance.handleSizeChange(
        { target: { value: mockCatEntryId } },
        mockProductId
      )
      await instance.handleAddToBag({ preventDefault: () => {} }, mockProductId)

      expect(props.dispatch).toHaveBeenCalledTimes(2)
      expect(wishlistActions.captureWishlistEvent).toHaveBeenCalledWith(
        'GA_ADD_TO_BAG_FROM_WISHLIST',
        { catEntryId: mockCatEntryId }
      )
    })

    it('should not capture `add to bag from wishlist` event failure', async () => {
      const initialProps = {
        ...props,
        dispatch: jest.fn(() => Promise.reject()),
      }
      const { instance } = renderComponent(initialProps)
      const mockProductId = 1111
      const mockCatEntryId = 2223
      bagActions.addToBagWithCatEntryId = jest.fn(() => Promise.reject())
      wishlistActions.captureWishlistEvent = jest.fn()

      instance.handleSizeChange(
        { target: { value: mockCatEntryId } },
        mockProductId
      )
      await instance.handleAddToBag({ preventDefault: () => {} }, mockProductId)

      expect(initialProps.dispatch).toHaveBeenCalledTimes(1)
      expect(wishlistActions.captureWishlistEvent).not.toHaveBeenCalled()
    })

    it('does not add to the bag when a size has not been selected', () => {
      const { wrapper } = renderComponent(props)
      bagActions.addToBagWithCatEntryId = jest.fn()
      const mockProductId = 1111

      wrapper
        .instance()
        .handleAddToBag({ preventDefault: () => {} }, mockProductId)

      expect(props.dispatch).not.toHaveBeenCalled()
      expect(bagActions.addToBagWithCatEntryId).not.toHaveBeenCalled()
      expect(wrapper.state().sizeValidationErrorIds).toContain(1111)
    })

    it('handles calling the product quickview', () => {
      const { wrapper } = renderComponent(props)
      modalActions.showModal = jest.fn()
      productActions.setProductQuickview = jest.fn()
      productActions.setProductIdQuickview = jest.fn()
      const mockProductId = 1234

      wrapper.instance().handleQuickView(mockProductId)

      expect(props.dispatch).toHaveBeenCalledTimes(3)
      expect(productActions.setProductQuickview).toHaveBeenCalledWith({})
      expect(productActions.setProductIdQuickview).toHaveBeenCalledWith(
        mockProductId
      )
      expect(modalActions.showModal).toHaveBeenCalled()
    })

    it('handles removing an item from the wishlist', async () => {
      const { instance } = renderComponent(props)
      const mockProductId = '1234'
      wishlistActions.removeProductFromWishlist = jest.fn()

      await instance.handleRemoveFromWishlist(mockProductId)

      expect(props.dispatch).toHaveBeenCalled()
      expect(wishlistActions.removeProductFromWishlist).toHaveBeenCalledWith(
        1234
      )
    })

    it('should capture `remove from wishlist` event success', async () => {
      const { instance } = renderComponent(props)
      const mockProductId = '666666'
      wishlistActions.removeProductFromWishlist = jest.fn()
      wishlistActions.captureWishlistEvent = jest.fn()

      await instance.handleRemoveFromWishlist(mockProductId)

      expect(props.dispatch).toHaveBeenCalledTimes(1)
      expect(wishlistActions.captureWishlistEvent).not.toHaveBeenCalled()
    })

    it('should not capture `remove from wishlist` event failure', async () => {
      const initialProps = {
        ...props,
        dispatch: jest.fn(() => Promise.reject()),
      }
      const { instance } = renderComponent(initialProps)
      const mockProductId = '666666'
      wishlistActions.removeProductFromWishlist = jest.fn()
      wishlistActions.captureWishlistEvent = jest.fn()

      await instance.handleRemoveFromWishlist(mockProductId)

      expect(initialProps.dispatch).toHaveBeenCalledTimes(1)
      expect(wishlistActions.captureWishlistEvent).not.toHaveBeenCalled()
    })
  })
})
