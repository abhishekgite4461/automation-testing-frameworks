import { identity } from 'ramda'
import testComponentHelper from '../../../../../../test/unit/helpers/test-component'
import EmptyWishlist from '../EmptyWishlist'

describe(EmptyWishlist.name, () => {
  const context = {
    l: jest.fn(identity),
  }
  const renderComponent = testComponentHelper(EmptyWishlist, { context })

  describe('@render', () => {
    it('should render component when user is not authenticated', () => {
      const { getTree } = renderComponent({
        onSignInHandler: jest.fn(),
      })
      expect(getTree()).toMatchSnapshot()
    })
    it('should not render SIGN IN message when user is authenticated', () => {
      const { getTree } = renderComponent({
        isUserAuthenticated: true,
      })
      expect(getTree()).toMatchSnapshot()
    })
  })

  describe('@events', () => {
    describe('onSignInHandler()', () => {
      it('should trigger function to open login modal when sign in button is clicked', () => {
        const { wrapper, instance } = renderComponent({
          itemsCount: null,
          onSignInHandler: jest.fn(),
        })
        const EmptyWishlistSignInButton = wrapper.find(
          '.EmptyWishlist-signInButton'
        )
        expect(instance.props.onSignInHandler).not.toHaveBeenCalled()
        EmptyWishlistSignInButton.simulate('click')
        expect(instance.props.onSignInHandler).toHaveBeenCalledTimes(1)
      })
    })
  })
})
