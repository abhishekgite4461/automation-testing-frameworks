import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { CSSTransition } from 'react-transition-group'
import { connect } from 'react-redux'

import EmptyWishlist from './EmptyWishlist'
import WishlistItemContainer from './WishlistItemContainer'
import WishlistLimitInfo from './WishlistLimitInfo'

import WishlistLoginModal from '../../containers/Wishlist/WishlistLoginModal'

import {
  getPaginatedWishlist,
  triggerWishlistLoginModal,
} from '../../../actions/common/wishlistActions'

import { isUserAuthenticated } from '../../../selectors/userSelectors'
import { isMobile } from '../../../selectors/viewportSelectors'
import {
  getWishlistItemDetails,
  isWishlistSync,
} from '../../../selectors/wishlistSelectors'

import { withWindowScroll } from '../WindowEventProvider/withWindowScroll'

import {
  ITEMS_TO_SHOW,
  MAX_WISHLIST_ITEMS,
} from '../../../constants/wishlistConstants'

import { compose } from 'ramda'
import analyticsDecorator from '../../../../client/lib/analytics/analytics-decorator'

export class WishlistPageContainer extends Component {
  constructor(props) {
    super(props)
    const { isUserAuthenticated } = props
    this.state = {
      page: 1,
      showEmptyWishlist: !isUserAuthenticated || this.getItemsCount() === 0,
    }
  }

  static propTypes = {
    isMobile: PropTypes.bool,
    items: PropTypes.arrayOf(PropTypes.object),
    isWishlistSync: PropTypes.bool,
    preservedScroll: PropTypes.number,
    location: PropTypes.object,
    triggerWishlistLoginModal: PropTypes.func,
    hasReachedPageBottom: PropTypes.bool, // eslint-disable-line react/no-unused-prop-types
    getPaginatedWishlist: PropTypes.func,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentDidMount = () => {
    const {
      getPaginatedWishlist,
      isWishlistSync,
      location,
      preservedScroll,
    } = this.props

    if (!isWishlistSync) getPaginatedWishlist()

    // Jump to either where we were, or the top
    if (location.action !== 'PUSH' && process.browser && preservedScroll) {
      window.scrollTo(0, preservedScroll)
    } else {
      window.scrollTo(0, 0)
    }
  }

  componentWillReceiveProps = (nextProps) => {
    const { showEmptyWishlist } = this.state
    const newItemCount = this.getItemsCount(nextProps) || 0
    if (nextProps.hasReachedPageBottom) {
      this.setState({
        page: ++this.state.page,
      })
    }
    if (showEmptyWishlist && newItemCount > 0) {
      this.setState({ showEmptyWishlist: false })
    } else if (!showEmptyWishlist && newItemCount === 0) {
      this.setState({ showEmptyWishlist: true })
    }
  }

  getItemsCount = (props = this.props) => {
    const { items } = props
    return Array.isArray(items) ? items.length : null
  }

  getItemsToShow = () => {
    const { page } = this.state
    const { items } = this.props

    return Array.isArray(items) ? items.slice(0, page * ITEMS_TO_SHOW) : []
  }

  toggleEmptyWishlistMessage = (shouldShowMessage) => {
    this.setState({ showEmptyWishlist: shouldShowMessage })
  }

  onSignInHandler = () => {
    const { triggerWishlistLoginModal } = this.props
    triggerWishlistLoginModal(null, WishlistLoginModal, 'wishlist')
  }

  render() {
    const { l } = this.context
    const { isUserAuthenticated, isMobile } = this.props
    const grid = isMobile ? 2 : 4
    const itemsCount = this.getItemsCount()
    return (
      <div className="WishlistPageContainer">
        <h1 className="WishlistPageContainer-title">{l`My Wish list`}</h1>
        {itemsCount !== null && (
          <span className="WishlistPageContainer-itemCount">
            {`${itemsCount} ${itemsCount === 1 ? l('item') : l('items')}`}
          </span>
        )}
        <CSSTransition
          classNames="fade"
          timeout={{ enter: 400, exit: 0 }}
          in={this.state.showEmptyWishlist}
          unmountOnExit
        >
          <EmptyWishlist
            isUserAuthenticated={isUserAuthenticated}
            onSignInHandler={this.onSignInHandler}
          />
        </CSSTransition>
        <CSSTransition
          classNames="fade"
          timeout={400}
          in={this.getItemsCount() > 0}
          onExited={() => this.toggleEmptyWishlistMessage(true)}
          unmountOnExit
        >
          <WishlistItemContainer items={this.getItemsToShow()} grid={grid} />
        </CSSTransition>
        {itemsCount >= MAX_WISHLIST_ITEMS && <WishlistLimitInfo />}
      </div>
    )
  }
}

export default compose(
  analyticsDecorator('wishlist'),
  connect(
    (state) => ({
      isUserAuthenticated: isUserAuthenticated(state),
      isMobile: isMobile(state),
      isWishlistSync: isWishlistSync(state),
      items: getWishlistItemDetails(state),
      preservedScroll: state.infinityScroll.preservedScroll,
    }),
    {
      getPaginatedWishlist,
      triggerWishlistLoginModal,
    }
  ),
  withWindowScroll({
    notifyWhenReachedBottomOfPage: true,
    pageBottomBuffer: 700,
  })
)(WishlistPageContainer)
