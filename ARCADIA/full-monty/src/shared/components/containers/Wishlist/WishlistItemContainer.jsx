import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { TransitionGroup, CSSTransition } from 'react-transition-group'

import WishlistItem from './WishlistItem'
import ProductQuickview from '../ProductQuickview/ProductQuickview'
import AddToBagModal from '../../common/AddToBagModal/AddToBagModal'
import { showModal } from '../../../actions/common/modalActions'
import {
  setProductQuickview,
  setProductIdQuickview,
} from '../../../actions/common/productsActions'
import { addToBagWithCatEntryId } from '../../../actions/common/shoppingBagActions'
import { getBaseUrlPath } from '../../../selectors/configSelectors'
import {
  removeProductFromWishlist,
  captureWishlistEvent,
} from '../../../actions/common/wishlistActions'

@connect((state) => ({
  baseUrlPath: getBaseUrlPath(state),
}))
export default class WishlistItemContainer extends React.Component {
  constructor() {
    super()
    this.state = {
      selectedCatEntryIds: {},
      sizeValidationErrorIds: [],
    }
  }

  static propTypes = {
    grid: PropTypes.number.isRequired,
    items: PropTypes.array.isRequired,
    baseUrlPath: PropTypes.string,
  }

  handleQuickView = (productId) => {
    const { dispatch } = this.props
    // We explicitly set the product quick view state to an empty object here
    // in order to aid our analytics decorator on ProductQuickView to know
    // that a product is being quick-viewed. This helps with cases where you
    // quick view the same product sequentially.
    dispatch(setProductQuickview({}))
    dispatch(setProductIdQuickview(productId))

    const html = <ProductQuickview />
    dispatch(showModal(html, { mode: 'plpQuickview' }))
  }

  handleAddToBag = (e, productId) => {
    const { selectedCatEntryIds } = this.state
    const { dispatch } = this.props
    const catEntryId = selectedCatEntryIds[productId]

    e.preventDefault()
    if (!selectedCatEntryIds[productId]) {
      this.setState({
        sizeValidationErrorIds: [
          ...this.state.sizeValidationErrorIds,
          productId,
        ],
      })
      return false
    }

    dispatch(addToBagWithCatEntryId(catEntryId, <AddToBagModal />))
      .then(() =>
        dispatch(
          captureWishlistEvent('GA_ADD_TO_BAG_FROM_WISHLIST', { catEntryId })
        )
      )
      .catch(() => {})
  }

  handleSizeChange = (e, productId) => {
    const catEntryId = e.target.value
    const newValidatedIds = this.state.sizeValidationErrorIds.filter(
      (e) => e !== productId
    )
    this.setState({
      selectedCatEntryIds: {
        ...this.state.selectedCatEntryIds,
        [productId]: catEntryId,
      },
      sizeValidationErrorIds: newValidatedIds,
    })
  }

  handleRemoveFromWishlist = (wishlistItemId) => {
    const { dispatch } = this.props
    const productId = parseInt(wishlistItemId, 10)
    dispatch(removeProductFromWishlist(productId))
  }

  renderItems = (grid, items, baseUrlPath) => {
    return items.map((item) => {
      const validateItem = this.state.sizeValidationErrorIds.includes(
        item.parentProductId
      )
      return (
        <CSSTransition key={item.catEntryId} timeout={400} classNames="fade">
          <WishlistItem
            item={item}
            grid={grid}
            handleAddToBag={this.handleAddToBag}
            handleQuickView={this.handleQuickView}
            handleSizeChange={this.handleSizeChange}
            selectedCatEntryId={
              this.state.selectedCatEntryIds[item.parentProductId]
            }
            baseUrlPath={baseUrlPath}
            handleRemoveFromWishlist={this.handleRemoveFromWishlist}
            sizeValidationError={validateItem}
          />
        </CSSTransition>
      )
    })
  }

  render() {
    const { grid, items, baseUrlPath } = this.props
    return (
      <ul className="WishlistItemContainer">
        <TransitionGroup className="WishlistItemContainer WishlistItemContainer-animation">
          {this.renderItems(grid, items, baseUrlPath)}
        </TransitionGroup>
      </ul>
    )
  }
}
