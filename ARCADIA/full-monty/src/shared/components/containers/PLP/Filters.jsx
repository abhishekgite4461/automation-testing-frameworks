import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import GridSelector from '../../common/GridSelector/GridSelector'
import SortSelector from '../../common/SortSelector/SortSelector'
import { getNumberRefinements } from '../../../../shared/selectors/refinementsSelectors'
import Button from '../../common/Button/Button'
import ProductViews from '../../common/ProductViews/ProductViews'
import * as actions from '../../../actions/components/refinementsActions'
import { isMobile } from '../../../selectors/viewportSelectors'
import { getProductsSearchResultsTotal } from '../../../selectors/productSelectors'

@connect(
  (state) => ({
    numRefinements: getNumberRefinements(state),
    totalProducts: getProductsSearchResultsTotal(state),
    isMobile: isMobile(state),
  }),
  actions
)
export default class Filters extends Component {
  static propTypes = {
    toggleRefinements: PropTypes.func.isRequired,
    numRefinements: PropTypes.number,
    totalProducts: PropTypes.number,
    isMobile: PropTypes.bool.isRequired,
  }

  static defaultProps = {
    numRefinements: 0,
    totalProducts: 0,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  render() {
    const {
      toggleRefinements,
      numRefinements,
      totalProducts,
      isMobile,
    } = this.props
    const { l } = this.context
    return (
      <div className="Filters">
        {isMobile ? (
          <div className="Filters-mobile">
            <div className="Filters-row">
              <GridSelector className="Filters-column Filters-gridSelector" />
              <ProductViews className="Filters-column" />
            </div>
            <div className="Filters-row Filters-refinement">
              <SortSelector className="Filters-column" />
              <div className="Filters-column Filters-refineButtonContainer">
                <span className="Filters-totalResults">
                  {totalProducts} {l`results`}
                </span>
                <Button
                  className="Filters-refineButton"
                  clickHandler={() => toggleRefinements(true)}
                >
                  {l`Filter`}
                  <span>
                    {numRefinements > 0 ? ` (${numRefinements})` : ''}
                  </span>
                </Button>
              </div>
            </div>
          </div>
        ) : (
          <div className="Filters-responsive">
            <ProductViews className="Filters-column Filters-productViews" />
            <GridSelector className="Filters-column Filters-gridSelector" />
            <SortSelector className="Filters-column Filters-sortSelector" />
            <div className="Filters-column Filters-refineButtonContainer">
              <span className="Filters-totalResults">
                {totalProducts} {l`results`}
              </span>
            </div>
          </div>
        )}
      </div>
    )
  }
}
