import testComponentHelper from 'test/unit/helpers/test-component'
import RefinementSummary from '../RefinementSummary'

describe('<RefinementSummary/>', () => {
  const renderComponent = testComponentHelper(
    RefinementSummary.WrappedComponent
  )
  const defaultProps = {
    categoryTitle: 'Dress',
    applyRefinements: jest.fn(),
    clearRefinements: jest.fn(),
  }
  const activeRefinements = [
    {
      key: 'TOPSHOP_UK_CE3_PRODUCT_TYPE',
      title: 'Product Type',
      values: [
        {
          key: 'ECMC_PROD_CE3_PRODUCT_TYPE_1_DRESSES',
          label: 'dresses',
          seoUrl: 'XXXXX',
        },
      ],
    },
    {
      key: 'NOWPRICE',
      title: 'Price',
      values: [
        {
          key: 'NOWPRICE5.032.0',
          label: '',
          upperBound: '32.0',
          lowerBound: '5.0',
        },
      ],
    },
    {
      key: 'TOPSHOP_UK_CATEGORY',
      title: 'Category',
      values: [
        {
          key: 3552,
          label: 'Dress',
        },
      ],
    },
  ]

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(defaultProps).getTree()).toMatchSnapshot()
    })
    it('with activeRefinements', () => {
      expect(
        renderComponent({ ...defaultProps, activeRefinements }).getTree()
      ).toMatchSnapshot()
    })
    it('should add `ButtonLink-button` class when brandName different to evans', () => {
      const { wrapper } = renderComponent({
        ...defaultProps,
        brandName: 'topshop',
      })
      expect(wrapper.find('.ButtonLink-button').isEmpty()).toBe(false)
    })
    it('should NOT add `ButtonLink-button` class when brandName is evans', () => {
      const { wrapper } = renderComponent({
        ...defaultProps,
        brandName: 'evans',
      })
      expect(wrapper.find('.ButtonLink-button').isEmpty()).toBe(true)
    })
  })

  describe('@events', () => {
    it('remove price filter', () => {
      const { wrapper, instance } = renderComponent({
        ...defaultProps,
        categoryTitle: 'Dress',
        activeRefinements,
        removeOptionRange: jest.fn(),
        applyRefinements: jest.fn(),
      })
      expect(instance.props.removeOptionRange).not.toHaveBeenCalled()
      expect(instance.props.applyRefinements).not.toHaveBeenCalled()
      wrapper
        .find('.RefinementSummary-removeTextValue')
        .at(1)
        .prop('onClick')()
      expect(instance.props.removeOptionRange).toHaveBeenCalledTimes(1)
      expect(instance.props.removeOptionRange).toHaveBeenCalledWith('price')
      expect(instance.props.applyRefinements).toHaveBeenCalledTimes(1)
    })
    it('remove value filter', () => {
      const { wrapper, instance } = renderComponent({
        ...defaultProps,
        categoryTitle: 'Dress',
        activeRefinements,
        updateOptionValue: jest.fn(),
        applyRefinements: jest.fn(),
      })
      expect(instance.props.updateOptionValue).not.toHaveBeenCalled()
      expect(instance.props.applyRefinements).not.toHaveBeenCalled()
      wrapper
        .find('.RefinementSummary-removeTextValue')
        .at(0)
        .prop('onClick')()
      expect(instance.props.updateOptionValue).toHaveBeenCalledTimes(1)
      expect(instance.props.updateOptionValue).toHaveBeenCalledWith(
        'product type',
        'dresses'
      )
      expect(instance.props.applyRefinements).toHaveBeenCalledTimes(1)
      expect(instance.props.applyRefinements).toHaveBeenCalledWith('XXXXX')
    })
    it('with some activeRefinements then clear refinements button is not disabled', () => {
      const { wrapper } = renderComponent({
        ...defaultProps,
        activeRefinements,
      })
      const clearRefinementsButton = wrapper.find(
        '.RefinementSummary-clearRefinementsButton'
      )
      expect(clearRefinementsButton.prop('disabled')).toBeFalsy()
    })
    it('when no activeRefinements then clear refinements button is disabled', () => {
      const { wrapper } = renderComponent({
        ...defaultProps,
        activeRefinements: [],
      })
      const clearRefinementsButton = wrapper.find(
        '.RefinementSummary-clearRefinementsButton'
      )
      expect(clearRefinementsButton.prop('disabled')).toBeTruthy()
    })
    it('when clear refinements button is clicked', () => {
      const { instance, wrapper } = renderComponent({
        ...defaultProps,
        activeRefinements,
        clearRefinements: jest.fn(),
        applyRefinements: jest.fn(),
        sendAnalyticsFilterUsedEvent: jest.fn(),
      })
      const clearRefinementsButton = wrapper.find(
        '.RefinementSummary-clearRefinementsButton'
      )

      const expected = {
        filterCategory: 'clear all',
        filterOption: 'clear all',
        filterAction: 'remove',
      }

      clearRefinementsButton.simulate('click')
      expect(clearRefinementsButton.prop('disabled')).toBeFalsy()
      expect(instance.props.clearRefinements).toHaveBeenCalledTimes(1)
      expect(instance.props.applyRefinements).toHaveBeenCalledTimes(1)
      expect(instance.props.sendAnalyticsFilterUsedEvent).toHaveBeenCalledWith(
        expected
      )
    })
  })
})
