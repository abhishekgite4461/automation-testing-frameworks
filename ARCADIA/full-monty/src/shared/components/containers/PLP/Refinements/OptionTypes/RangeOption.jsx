import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Slider from '../../../../common/Slider/Slider'
import * as actions from '../../../../../actions/components/refinementsActions'
import { analyticsPlpClickEvent } from '../../../../../analytics/tracking/site-interactions'

@connect(
  null,
  { ...actions }
)
export default class RangeOption extends Component {
  static propTypes = {
    options: PropTypes.array.isRequired,
    refinement: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    selections: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
    updateOptionRange: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = { reset: false }
  }

  componentWillReceiveProps(nextProps) {
    const reset =
      this.props.selections &&
      (!nextProps.selections || nextProps.selections.length === 0)
    if (reset) this.setState({ reset: !this.state.reset })
  }

  onAfterChange = (refinement, value, seoUrl) => {
    this.props.updateOptionRange(refinement, value)
    this.props.onChange(seoUrl)
    analyticsPlpClickEvent(
      `${refinement.toLowerCase()}-${value[0]} to ${value[1]}`
    )
  }

  render() {
    const { refinement, options } = this.props
    const min = parseInt(options[0].minValue, 10)
    const max = parseInt(options[0].maxValue, 10)
    const seoUrl = options[0].seoUrl

    return (
      <div className="RangeOption" key={this.state.reset}>
        <Slider
          minValue={min}
          maxValue={max}
          onChangeFinished={(leftValue, rightValue) =>
            this.onAfterChange(refinement, [leftValue, rightValue], seoUrl)
          }
        />
      </div>
    )
  }
}
