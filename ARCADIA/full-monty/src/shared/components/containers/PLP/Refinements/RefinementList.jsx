import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import RefinementOptions from './RefinementOptions'
import { capitalize } from '../../../../lib/string-utils'
import * as actions from '../../../../actions/components/refinementsActions'
import Accordion from '../../../common/Accordion/Accordion'
import AccordionGroup from '../../../common/AccordionGroup/AccordionGroup'
import Price from '../../../common/Price/Price'
import { isMobile } from '../../../../selectors/viewportSelectors'
import { getRefinements } from '../../../../selectors/refinementsSelectors'
import { uniq, path } from 'ramda'
import { analyticsPlpClickEvent } from '../../../../analytics/tracking/site-interactions'

@connect(
  (state) => ({
    refinements: getRefinements(state),
    selectedOptions: state.refinements.selectedOptions,
    isMobile: isMobile(state),
  }),
  { ...actions }
)
export default class RefinementList extends Component {
  static propTypes = {
    selectedOptions: PropTypes.object,
    refinements: PropTypes.array,
    onChange: PropTypes.func,
    isMobile: PropTypes.bool,
    onAccordionToggle: PropTypes.func,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  static defaultProps = {
    onChange: () => {},
    onAccordionToggle: () => {},
    selectedOptions: {},
    refinements: [],
    isMobile: true,
  }

  setRefinementType(options, type) {
    const rating = type === 'Rating' ? 'RATING' : ''
    const size = type.includes('Size') ? 'SIZE' : ''
    const firstOption = options[0] ? options[0].type : ''
    return rating || size || firstOption
  }

  selectionText(selections, type) {
    if (!selections || !selections.length) return ''

    const { l } = this.context
    const text =
      type === 'VALUE' || type === 'SIZE'
        ? selections.join(', ')
        : selections.join(' - ')

    // sizes specific
    if (type === 'SIZE') return text.toUpperCase()

    // rating specific
    if (type === 'RATING')
      return text === '5'
        ? `${text} ${l`stars`}`
        : `${text} ${l`stars`} ${l`& up`}`

    // Price specific stuff, as it is the only range
    if (type === 'RANGE')
      return (
        <div>
          <Price price={selections[0]} /> - <Price price={selections[1]} />
        </div>
      )

    // default Capitalization
    return text.replace(/\w\S*/g, capitalize)
  }

  returnInitiallyExpanded(isMobile, refinements) {
    // geting index of Price refinement, ignoring items without refinementOptions
    return isMobile
      ? []
      : refinements.reduce((resultReducer, item) => {
          const isPriceRange =
            path(['refinementOptions', 0, 'type'], item) === 'RANGE'
          const hasSelectedRange =
            item.refinementOptions &&
            item.refinementOptions.filter((x) => x.selectedFlag).length > 0
          if (isPriceRange || hasSelectedRange) {
            resultReducer.push(item.label)
          }
          return resultReducer
        }, [])
  }

  getRefinementForLabel(selectedOptions, productRefinements, label, isMobile) {
    if (isMobile) {
      return uniq(this.props.selectedOptions[label.toLowerCase()] || [])
    }
    const selectedOption = selectedOptions[label.toLowerCase()] || []
    const refinementOption =
      productRefinements
        .filter((x) => x.label === label)
        .map((x) => x.refinementOptions)[0] || []
    return uniq(
      selectedOption.concat(
        refinementOption.filter((x) => x.selectedFlag).map((x) => x.value)
      )
    )
  }

  renderRefinementItem = (refineObj) => {
    const { isMobile, onChange } = this.props
    const { refinementOptions, label } = refineObj
    const type = this.setRefinementType(refinementOptions, label)
    const selection = this.getRefinementForLabel(
      this.props.selectedOptions,
      this.props.refinements,
      label,
      isMobile
    )

    if (!type) {
      return null
    }

    const selectionText = refinementOptions.length
      ? this.selectionText(selection, type)
      : ''

    const header = (
      <div
        role="presentation"
        className="RefinementList-accordionHeader"
        onClick={() => {
          analyticsPlpClickEvent(`${label.toLowerCase()}`)
        }}
      >
        <span
          className={`RefinementList-label ${
            selectionText ? 'is-filtered' : ''
          } is-${type.toLowerCase()}`}
        >
          {label}
        </span>
        {isMobile && (
          <span className={`RefinementList-selection is-${type.toLowerCase()}`}>
            {selectionText}
          </span>
        )}
      </div>
    )

    return (
      <Accordion
        key={label}
        header={header}
        accordionName={label}
        scrollPaneSelector=".RefinementList"
        noContentPadding={isMobile}
        noContentBorderTop={!isMobile}
        label={label}
        noHeaderPadding={!isMobile}
        noExpandedHeaderBackground
        arrowStyle="secondary"
        arrowPosition="right"
      >
        <RefinementOptions
          options={refinementOptions}
          type={type}
          label={label}
          selections={selection}
          onChange={onChange}
          isMobile={isMobile}
        />
      </Accordion>
    )
  }

  render() {
    const { onAccordionToggle, isMobile, refinements } = this.props
    const initiallyExpanded = this.returnInitiallyExpanded(
      isMobile,
      refinements
    )

    return (
      <AccordionGroup
        className="RefinementList"
        groupName="refinements"
        singleOpen={isMobile}
        initiallyExpanded={initiallyExpanded}
        onAccordionToggle={onAccordionToggle}
      >
        {refinements.map(this.renderRefinementItem).filter(Boolean)}
      </AccordionGroup>
    )
  }
}
