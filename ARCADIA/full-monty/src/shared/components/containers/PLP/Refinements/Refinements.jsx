import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { sendAnalyticsFilterUsedEvent } from '../../../../analytics/analytics-actions'

import { isMobile } from '../../../../selectors/viewportSelectors'

// actions
import * as actions from '../../../../actions/components/refinementsActions'

// components
import Button from '../../../common/Button/Button'
import AccessibleText from '../../../common/AccessibleText/AccessibleText'
import RefinementList from './RefinementList'
import { getNumberRefinements } from '../../../../../shared/selectors/refinementsSelectors'

@connect(
  (state) => ({
    refinements: state.products.refinements,
    numRefinements: getNumberRefinements(state),
    isShown: state.refinements.isShown,
    location: state.routing.location,
    isMobile: isMobile(state),
  }),
  {
    ...actions,
    sendAnalyticsFilterUsedEvent,
  }
)
export default class Refinements extends Component {
  static propTypes = {
    toggleRefinements: PropTypes.func,
    clearRefinements: PropTypes.func,
    isShown: PropTypes.bool,
    applyRefinements: PropTypes.func,
    sendAnalyticsFilterUsedEvent: PropTypes.func,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  static defaultProps = {
    isMobile: true,
  }

  componentDidUpdate({ isShown: wasShown }) {
    const { isShown } = this.props
    if (isShown && !wasShown && this.description) this.description.focus()
  }

  componentWillUnmount() {
    this.props.toggleRefinements(false)
  }

  applyRefinementsHandler = () => {
    const { toggleRefinements, applyRefinements } = this.props
    applyRefinements()
    toggleRefinements(false)
  }

  clearAndApplyRefinements = () => {
    const {
      clearRefinements,
      applyRefinements,
      sendAnalyticsFilterUsedEvent,
    } = this.props
    clearRefinements()
    applyRefinements()
    sendAnalyticsFilterUsedEvent({
      filterCategory: 'clear all',
      filterOption: 'clear all',
      filterAction: 'remove',
    })
  }

  renderHeader = () => {
    const { l } = this.context
    const { toggleRefinements, numRefinements } = this.props
    return (
      <div className="Refinements-header">
        <h3 className="Refinements-title">{l`Filter`}</h3>
        <AccessibleText
          ref={(accessibleText) => {
            this.description = accessibleText
          }}
        >
          {l`This is the filters modal. Select any required filters and press apply to return to a filtered product list.`}
        </AccessibleText>
        <button
          className="Refinements-closeIcon"
          onClick={() => toggleRefinements(false)}
        >
          <span className="screen-reader-text">{l`Close filters modal`}</span>
        </button>
        <button
          className="Refinements-clearButton"
          onClick={this.clearAndApplyRefinements}
          disabled={!numRefinements}
        >
          {l`Clear all`}
        </button>
      </div>
    )
  }

  render() {
    const { isShown, isMobile } = this.props
    const { l } = this.context

    return isMobile ? (
      <div className={`Refinements ${isShown ? 'is-shown' : 'is-hidden'}`}>
        <div
          className={`Refinements-content ${
            isShown ? 'is-shown' : 'is-hidden'
          }`}
        >
          {this.renderHeader()}
          <RefinementList />
          <div className="Refinements-applyButtonWrapper">
            <Button
              className="Refinements-applyButton"
              clickHandler={this.applyRefinementsHandler}
            >{l`Apply`}</Button>
          </div>
        </div>
      </div>
    ) : null
  }
}
