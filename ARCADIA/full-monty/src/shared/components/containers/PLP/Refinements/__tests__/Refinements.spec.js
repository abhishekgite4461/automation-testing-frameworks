import testComponentHelper from 'test/unit/helpers/test-component'
import PropTypes from 'prop-types'
import React from 'react'
import { mount } from 'enzyme'
import { createStore } from 'redux'
import Refinements from '../Refinements'

describe('<RefinementList/>', () => {
  const renderComponent = testComponentHelper(Refinements.WrappedComponent)
  const refinements = [
    {
      label: 'Colour',
      refinementOptions: [
        {
          type: 'VALUE',
          label: 'black',
          value: 'black',
        },
      ],
    },
    {
      label: 'Size',
      refinementOptions: [
        {
          type: 'VALUE',
          label: 4,
          value: 4,
        },
        {
          type: 'VALUE',
          label: 6,
          value: 6,
        },
      ],
    },
    {
      label: 'Price',
      refinementOptions: [
        {
          type: 'RANGE',
          label: 0,
          value: 0,
        },
      ],
    },
  ]
  const defaultProps = {}

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(defaultProps).getTree()).toMatchSnapshot()
    })
    it('with refinements', () => {
      expect(
        renderComponent({ ...defaultProps, refinements }).getTree()
      ).toMatchSnapshot()
    })
    it('with refinements and selectedOptions', () => {
      const selectedOptions = {
        Size: [4],
      }
      expect(
        renderComponent({
          ...defaultProps,
          numRefinements: 1,
          refinements,
          selectedOptions,
        }).getTree()
      ).toMatchSnapshot()
    })
    it('with isShown', () => {
      expect(
        renderComponent({ ...defaultProps, isShown: true }).getTree()
      ).toMatchSnapshot()
    })
    it('with location', () => {
      expect(
        renderComponent({
          ...defaultProps,
          location: {
            pathname: 'some/pathname',
          },
        }).getTree()
      ).toMatchSnapshot()
    })
    it('with isMobile as false', () => {
      expect(
        renderComponent({ ...defaultProps, isMobile: false }).getTree()
      ).toMatchSnapshot()
    })
  })

  describe('@lifecycle', () => {
    beforeEach(() => {
      jest.resetAllMocks()
    })
    describe('componentDidUpdate', () => {
      it('should focus description', () => {
        const state = {
          config: {
            brandName: 'TS',
            brandCode: 'TS',
          },
          products: {
            refinements: [],
          },
          refinements: {},
        }
        const store = createStore((state) => state, state)
        const { WrappedComponent } = Refinements
        const context = {
          l: (value) => {
            return value
          },
          store,
        }
        const wrapper = mount(<WrappedComponent isShown />, {
          context,
          childContextTypes: {
            l: PropTypes.any,
            store: PropTypes.any,
          },
        })
        const instance = wrapper.instance()
        instance.description.focus = jest.fn()
        instance.componentDidUpdate({ isShown: false })
        expect(instance.description.focus).toHaveBeenCalled()
      })
    })
    describe('componentWillUnmount', () => {
      it('should call toggleRefinements', () => {
        const { instance } = renderComponent({
          ...defaultProps,
          toggleRefinements: jest.fn(),
        })
        instance.componentWillUnmount()
        expect(instance.props.toggleRefinements).toHaveBeenCalledTimes(1)
        expect(instance.props.toggleRefinements).toHaveBeenCalledWith(false)
      })
    })
  })

  describe('@events', () => {
    describe('clearAndApplyRefinements', () => {
      it('should call clearRefinements and applyRefinements when click clear all', () => {
        const { instance, wrapper } = renderComponent({
          ...defaultProps,
          clearRefinements: jest.fn(),
          applyRefinements: jest.fn(),
          sendAnalyticsFilterUsedEvent: jest.fn(),
        })

        const expected = {
          filterCategory: 'clear all',
          filterOption: 'clear all',
          filterAction: 'remove',
        }

        wrapper.find('.Refinements-clearButton').simulate('click')
        expect(instance.props.applyRefinements).toHaveBeenCalledTimes(1)
        expect(instance.props.clearRefinements).toHaveBeenCalledTimes(1)
        expect(
          instance.props.sendAnalyticsFilterUsedEvent
        ).toHaveBeenCalledWith(expected)
      })
    })
  })

  describe('@functions', () => {
    describe('applyRefinementsHandler', () => {
      it('should call toggleRefinements and applyRefinements', () => {
        const { instance } = renderComponent({
          ...defaultProps,
          toggleRefinements: jest.fn(),
          applyRefinements: jest.fn(),
        })
        instance.applyRefinementsHandler()
        expect(instance.props.applyRefinements).toHaveBeenCalledTimes(1)
        expect(instance.props.toggleRefinements).toHaveBeenCalledTimes(1)
        expect(instance.props.toggleRefinements).toHaveBeenCalledWith(false)
      })
    })
  })
})
