import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as actions from '../../../../../actions/components/refinementsActions'
import RatingImage from '../../../../common/RatingImage/RatingImage'
import Checkbox from '../../../../common/FormComponents/Checkbox/Checkbox'
import { isMobile } from '../../../../../selectors/viewportSelectors'
import { analyticsPlpClickEvent } from '../../../../../analytics/tracking/site-interactions'

@connect(
  (state) => ({
    isMobile: isMobile(state),
  }),
  { ...actions }
)
export default class RatingOption extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    isMobile: PropTypes.bool.isRequired,
    refinement: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
      .isRequired,
    selections: PropTypes.oneOfType([PropTypes.string, PropTypes.array])
      .isRequired,
    options: PropTypes.array.isRequired,
    updateOptionSingle: PropTypes.func.isRequired,
  }

  onClick = (refinement, value, seoUrl) => {
    this.props.updateOptionSingle(refinement, value)
    this.props.onChange(seoUrl)
    analyticsPlpClickEvent(`${refinement.toLowerCase()}-${value.toLowerCase()}`)
  }

  renderOption = (value, i, seoUrl) => {
    const { refinement, selections, isMobile } = this.props

    const isSelected = selections.indexOf(value) > -1
    const rating = parseInt(value, 10)
    const andUpText = rating !== 5 && (
      <span className="RatingOption-andUp">&amp; up</span>
    )

    if (isMobile) {
      return (
        <button
          key={value}
          className={`RatingOption-item ${isSelected ? 'is-selected' : ''}`}
          onClick={() => this.onClick(refinement, value, seoUrl)}
        >
          <span className="RatingOption-stars">
            <RatingImage rating={parseInt(value, 10)} />
          </span>
          {andUpText}
        </button>
      )
    }

    return (
      <Checkbox
        key={value}
        className="RatingOption-checkbox"
        labelClassName="RatingOption-checkboxLabel"
        checked={{ value: isSelected }}
        name={`rating_option_checkbox_${value}`}
        onChange={() => this.onClick(refinement, value, seoUrl)}
      >
        <span className="RatingOption-stars">
          <RatingImage rating={parseInt(value, 10)} />
        </span>
        {andUpText}
      </Checkbox>
    )
  }

  render() {
    return (
      <div className="RatingOption">
        {this.props.options.map(({ value, seoUrl }, i) =>
          this.renderOption(value, i, seoUrl)
        )}
      </div>
    )
  }
}
