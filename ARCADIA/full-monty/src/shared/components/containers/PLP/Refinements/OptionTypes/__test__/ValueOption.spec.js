import testComponentHelper from 'test/unit/helpers/test-component'
import ValueOption from '../ValueOption'
import { analyticsPlpClickEvent } from '../../../../../../analytics/tracking/site-interactions'

jest.mock('../../../../../../analytics/tracking/site-interactions', () => ({
  analyticsPlpClickEvent: jest.fn(),
}))

describe('<ValueOption />', () => {
  const initialProps = {
    options: [
      {
        count: 3122,
        label: '34',
        seoUrl: '/test-url',
        type: 'VALUE',
        value: '34',
      },
      {
        count: 3123,
        label: '38',
        seoUrl: '/test-url',
        type: 'VALUE',
        value: '38',
      },
    ],
    selections: [],
    isMobile: false,
    onChange: jest.fn(),
    type: 'valueType',
    updateOptionValue: jest.fn(),
    refinement: 'waist size',
  }

  const renderComponent = testComponentHelper(ValueOption.WrappedComponent)

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })

    it('wraps filters with anchors', () => {
      const seoUrl = 'product seo url'
      const { wrapper } = renderComponent({
        ...initialProps,
        options: [
          {
            count: 3122,
            label: '34',
            seoUrl,
            type: 'VALUE',
            value: '34',
          },
        ],
      })
      const anchor = wrapper.find('.ValueOption-link')
      expect(anchor.length).toBe(1)
      expect(anchor.first().prop('href')).toBe(seoUrl)
    })

    describe('isMobile is truthy', () => {
      const props = {
        ...initialProps,
        selections: ['38'],
        isMobile: true,
      }
      it('renders .ValueOption-item when isMobile is truthy', () => {
        const { wrapper } = renderComponent(props)
        expect(wrapper.find('.ValueOption-item').length).toBe(2)
        expect(wrapper.find('.ValueOption-checkbox').length).toBe(0)
      })
      it('.ValueOption-item has .is-selected class when `is-selected` is true', () => {
        const { wrapper } = renderComponent(props)
        expect(
          wrapper
            .find('.ValueOption-item')
            .at(1)
            .hasClass('is-selected')
        ).toBe(true)
      })
    })

    describe('isMobile is falsy', () => {
      it('renders .SizeOption-checkbox when isMobile is falsy', () => {
        const { wrapper } = renderComponent({ ...initialProps })
        expect(wrapper.find('.ValueOption-item').length).toBe(0)
        expect(wrapper.find('.ValueOption-checkbox').length).toBe(2)
      })
    })

    describe('with option count', () => {
      it('does not render filter if count is undefined', () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          options: [{}],
        })
        expect(wrapper.find('.ValueOption-link').length).toBe(0)
        expect(wrapper.find('.ValueOption-item').length).toBe(0)
        expect(wrapper.find('.ValueOption-checkbox').length).toBe(0)
      })

      it('does not render filter if count is 0', () => {
        const { wrapper } = renderComponent({
          ...initialProps,
          options: [{ count: 0 }],
        })
        expect(wrapper.find('.ValueOption-link').length).toBe(0)
        expect(wrapper.find('.ValueOption-item').length).toBe(0)
        expect(wrapper.find('.ValueOption-checkbox').length).toBe(0)
      })
    })
  })

  describe('@events', () => {
    describe('isMobile is truthy', () => {
      it('`updateOptionValue` instance method should be called with correct arguments', () => {
        const props = {
          ...initialProps,
          isMobile: true,
          refinement: 'test-refinement',
        }

        const { wrapper, instance } = renderComponent(props)

        const updateOptionValueSpy = jest.spyOn(instance, 'updateOptionValue')

        expect(props.updateOptionValue).toHaveBeenCalledTimes(0)
        expect(props.onChange).toHaveBeenCalledTimes(0)

        wrapper
          .find('.ValueOption-item')
          .at(0)
          .simulate('click')

        expect(updateOptionValueSpy).toHaveBeenCalledTimes(1)
        expect(updateOptionValueSpy).toHaveBeenCalledWith(
          props.refinement,
          props.options[0].value,
          props.options[0].seoUrl
        )

        expect(props.updateOptionValue).toHaveBeenCalledTimes(1)
        expect(props.updateOptionValue).toHaveBeenCalledWith(
          props.refinement,
          props.options[0].value
        )

        expect(props.onChange).toHaveBeenCalledTimes(1)
        expect(analyticsPlpClickEvent).toHaveBeenCalledWith(
          `${props.refinement.toLowerCase()}-${props.options[0].value}`
        )
      })

      it('prevents default on seo anchor click on mobile', () => {
        const preventDefault = jest.fn()
        const { wrapper } = renderComponent({ ...initialProps, isMobile: true })
        const anchor = wrapper.find('.ValueOption-link')
        expect(anchor.length).toBe(2)

        const e = { preventDefault }
        anchor.first().simulate('click', e)
        expect(preventDefault).toHaveBeenCalledTimes(1)
      })

      it('does not prevent default on seo anchor click on tablet / desktop', () => {
        const preventDefault = jest.fn()
        const { wrapper } = renderComponent({
          ...initialProps,
          isMobile: false,
        })
        const anchor = wrapper.find('.ValueOption-link')
        expect(anchor.length).toBe(2)

        const e = { preventDefault }
        anchor.first().simulate('click', e)
        expect(preventDefault).toHaveBeenCalledTimes(0)
      })
    })

    describe('isMobile is falsey', () => {
      it('`updateOptionValue` instance method should be called with correct arguments', () => {
        const props = {
          ...initialProps,
          isMobile: false,
          refinement: 'test-refinement',
          updateOptionValue: jest.fn(),
          onChange: jest.fn(),
        }

        const { wrapper, instance } = renderComponent(props)

        const updateOptionValueSpy = jest.spyOn(instance, 'updateOptionValue')

        expect(props.updateOptionValue).toHaveBeenCalledTimes(0)
        expect(props.onChange).toHaveBeenCalledTimes(0)

        wrapper
          .find('.ValueOption-checkbox')
          .at(0)
          .prop('onChange')()

        expect(updateOptionValueSpy).toHaveBeenCalledTimes(1)
        expect(updateOptionValueSpy).toHaveBeenCalledWith(
          props.refinement,
          props.options[0].value,
          '/test-url'
        )

        expect(props.updateOptionValue).toHaveBeenCalledTimes(1)
        expect(props.updateOptionValue).toHaveBeenCalledWith(
          props.refinement,
          props.options[0].value
        )

        expect(props.onChange).toHaveBeenCalledTimes(1)
      })
    })

    describe('@Methods', () => {
      describe('getCheckoutName()', () => {
        const { instance } = renderComponent(initialProps)
        it('should create a unique name if refinement is a string value', () => {
          const refinement = 'waist size'
          const label = '36'
          const expected = 'waistsize-36'
          expect(instance.getCheckoutName(refinement, label)).toEqual(expected)
        })

        it('should return a name if refinement is an object value', () => {
          const refinement = { name: 'waist size' }
          const label = '36'
          const expected = '36'
          expect(instance.getCheckoutName(refinement, label)).toEqual(expected)
        })
      })
    })
  })
})
