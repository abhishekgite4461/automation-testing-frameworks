import testComponentHelper from 'test/unit/helpers/test-component'
import RangeOption from '../RangeOption'
import Slider from '../../../../../common/Slider/Slider'
import { analyticsPlpClickEvent } from '../../../../../../analytics/tracking/site-interactions'

jest.mock('../../../../../../analytics/tracking/site-interactions', () => ({
  analyticsPlpClickEvent: jest.fn(),
}))

describe('<RangeOption />', () => {
  const initialProps = {
    updateOptionRange: jest.fn(),
    onChange: jest.fn(),
    options: [
      {
        maxValue: 100,
        minValue: 0,
      },
    ],
  }

  const renderComponent = testComponentHelper(RangeOption.WrappedComponent)

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })
  })

  describe('@lifecycle', () => {
    describe('on constructor', () => {
      it('state.reset set to false', () => {
        expect(renderComponent(initialProps).wrapper.state().reset).toBe(false)
      })
    })

    describe('on componentWillReceiveProps', () => {
      it('state.reset toggles when selections is removed', () => {
        const { instance, wrapper } = renderComponent({
          ...initialProps,
          selections: [5, 40],
        })

        expect(
          wrapper
            .find('.RangeOption')
            .first()
            .key()
        ).toBe('false')
        instance.componentWillReceiveProps(initialProps)
        wrapper.update()
        expect(
          wrapper
            .find('.RangeOption')
            .first()
            .key()
        ).toBe('true')
        instance.componentWillReceiveProps({
          ...initialProps,
          selections: [10, 60],
        })
        instance.componentWillReceiveProps(initialProps)
        wrapper.update()
        expect(
          wrapper
            .find('.RangeOption')
            .first()
            .key()
        ).toBe('false')
      })
    })

    it('state.reset toggles when selections is empty', () => {
      const { instance, wrapper } = renderComponent({
        ...initialProps,
        selections: [5, 40],
      })

      expect(
        wrapper
          .find('.RangeOption')
          .first()
          .key()
      ).toBe('false')
      instance.componentWillReceiveProps({
        ...initialProps,
        selections: [],
      })
      wrapper.update()
      expect(
        wrapper
          .find('.RangeOption')
          .first()
          .key()
      ).toBe('true')
    })
  })

  describe('@events', () => {
    it('onChangeFinished should call updateOptionRange and onChange', () => {
      const leftValue = 10
      const rightValue = 20
      const refinement = 'price'
      const { instance, wrapper } = renderComponent({
        ...initialProps,
        refinement,
        updateOptionRange: jest.fn(),
        onChange: jest.fn(),
      })
      expect(instance.props.updateOptionRange).not.toBeCalled()
      expect(instance.props.onChange).not.toBeCalled()
      wrapper
        .find(Slider)
        .first()
        .simulate('changeFinished', leftValue, rightValue)
      expect(instance.props.updateOptionRange).toBeCalledWith(refinement, [
        leftValue,
        rightValue,
      ])
      expect(instance.props.updateOptionRange).toHaveBeenCalledTimes(1)
      expect(instance.props.onChange).toBeCalled()
      expect(instance.props.onChange).toHaveBeenCalledTimes(1)
      expect(analyticsPlpClickEvent).toHaveBeenCalledWith(
        `price-${leftValue} to ${rightValue}`
      )
    })
  })
})
