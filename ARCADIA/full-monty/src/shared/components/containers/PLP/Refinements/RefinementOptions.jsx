import PropTypes from 'prop-types'
import React, { Component } from 'react'

import RangeOption from './OptionTypes/RangeOption'
import ValueOption from './OptionTypes/ValueOption'
import RatingOption from './OptionTypes/RatingOption'

const RANGE_OPTION = 'RANGE'
const SIZE_OPTION = 'SIZE'
const RATING_OPTION = 'RATING'
const VALUE_OPTION = 'VALUE'

export default class RefinementOptions extends Component {
  static propTypes = {
    options: PropTypes.array,
    type: PropTypes.string,
    selections: PropTypes.any,
    label: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  }

  static defaultProps = {
    type: null,
    options: [],
  }

  toTitleCase(text) {
    return text.replace(/\w\S*/g, (txt) => {
      if (text.length <= 4) return text.toUpperCase()
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
    })
  }

  renderRefinementOption() {
    const { type, options, selections, label, onChange } = this.props
    const refinement = label.toLowerCase()

    if (type === VALUE_OPTION) {
      return (
        <ValueOption
          options={options}
          refinement={refinement}
          selections={selections}
          type="valueType"
          onChange={onChange}
        />
      )
    }
    if (type === SIZE_OPTION) {
      const updatedOptions = options.map((option) => ({
        ...option,
        label: this.toTitleCase(option.label),
      }))
      return (
        <ValueOption
          options={updatedOptions}
          refinement={refinement}
          selections={selections}
          type="sizeType"
          onChange={onChange}
        />
      )
    }
    if (type === RANGE_OPTION) {
      return (
        <RangeOption
          options={options}
          refinement={refinement}
          selections={selections}
          onChange={onChange}
        />
      )
    }
    if (type === RATING_OPTION) {
      return (
        <RatingOption
          options={options}
          refinement={refinement}
          selections={selections}
          onChange={onChange}
        />
      )
    }
    return null
  }

  render() {
    const refinementOption = this.renderRefinementOption()

    if (!refinementOption) {
      return null
    }

    return <div className="RefinementOptions">{refinementOption}</div>
  }
}
