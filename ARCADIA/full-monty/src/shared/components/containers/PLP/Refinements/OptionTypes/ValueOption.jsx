import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as actions from '../../../../../actions/components/refinementsActions'
import Checkbox from '../../../../common/FormComponents/Checkbox/Checkbox'
import { isMobile } from '../../../../../selectors/viewportSelectors'
import { analyticsPlpClickEvent } from '../../../../../analytics/tracking/site-interactions'

@connect(
  (state) => ({
    isMobile: isMobile(state),
  }),
  { ...actions }
)
export default class ValueOption extends Component {
  static propTypes = {
    options: PropTypes.array,
    // TODO: investigate if refinement passes an object and if not set to PropTypes.string only
    refinement: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    selections: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
    type: PropTypes.oneOf(['valueType', 'sizeType']).isRequired,
    updateOptionValue: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    isMobile: PropTypes.bool.isRequired,
  }

  onClick = (refinement, value, seoUrl) => {
    this.updateOptionValue(refinement, value, seoUrl)
    analyticsPlpClickEvent(`${refinement.toLowerCase()}-${value.toLowerCase()}`)
  }

  updateOptionValue = (refinement, value, seoUrl) => {
    this.props.updateOptionValue(refinement, value)
    this.props.onChange(seoUrl)
  }

  /* NOTE:
   * refinement PropTypes can be a string or an object.
   *  */
  getCheckoutName = (refinement, label) =>
    typeof refinement === 'string'
      ? `${refinement.replace(' ', '')}-${label}`
      : label

  renderOption = ({ label, count, value, seoUrl }) => {
    const {
      props: { refinement, selections, isMobile },
      getCheckoutName,
    } = this

    if (!count > 0) {
      return null
    }

    const isSelected = selections.includes(value)
    const labelComponent = <span className="ValueOption-label">{label}</span>
    const countComponent = <span className="ValueOption-count"> ({count})</span>

    const filter = isMobile ? (
      <button
        className={`ValueOption-item ${isSelected ? 'is-selected' : ''}`}
        onClick={() => this.onClick(refinement, value, seoUrl)}
        role="button"
        aria-pressed={isSelected}
      >
        {labelComponent}
      </button>
    ) : (
      <Checkbox
        className="ValueOption-checkbox"
        labelClassName="ValueOption-checkboxLabel"
        checked={{ value: isSelected }}
        name={getCheckoutName(refinement, label)}
        onChange={() => this.updateOptionValue(refinement, value, seoUrl)}
      >
        {labelComponent}
      </Checkbox>
    )

    return (
      <div key={seoUrl}>
        <a
          href={seoUrl}
          className="ValueOption-link"
          onClick={(e) => isMobile && e.preventDefault()}
        >
          {filter}
        </a>
        {countComponent}
      </div>
    )
  }

  render() {
    const { type, options } = this.props
    return (
      <div className={`ValueOption ValueOption--${type}`}>
        {options.map(this.renderOption)}
      </div>
    )
  }
}
