/* eslint-disable jsx-a11y/interactive-supports-focus */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import classnames from 'classnames'

// actions
import {
  updateOptionValue,
  removeOptionRange,
  applyRefinements,
  clearRefinements,
} from '../../../../actions/components/refinementsActions'
import { sendAnalyticsFilterUsedEvent } from '../../../../analytics/analytics-actions'

// selectors
import { getActiveRefinements } from '../../../../selectors/refinementsSelectors'

// components
import Price from '../../../common/Price/Price'

@connect(
  (state) => ({
    brandName: state.config.brandName,
    categoryTitle: state.products.categoryTitle,
    activeRefinements: getActiveRefinements(state),
  }),
  {
    updateOptionValue,
    removeOptionRange,
    applyRefinements,
    clearRefinements,
    sendAnalyticsFilterUsedEvent,
  }
)
export default class RefinementSummary extends Component {
  static propTypes = {
    categoryTitle: PropTypes.string.isRequired,
    activeRefinements: PropTypes.array,
    brandName: PropTypes.string,
    updateOptionValue: PropTypes.func,
    removeOptionRange: PropTypes.func,
    applyRefinements: PropTypes.func.isRequired,
    clearRefinements: PropTypes.func.isRequired,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  static defaultProps = {
    categoryTitle: '',
    activeRefinements: [],
    updateOptionValue: () => {},
    removeOptionRange: () => {},
  }

  clearRefinements = () => {
    const {
      clearRefinements,
      applyRefinements,
      sendAnalyticsFilterUsedEvent,
    } = this.props
    clearRefinements()
    applyRefinements()
    sendAnalyticsFilterUsedEvent({
      filterCategory: 'clear all',
      filterOption: 'clear all',
      filterAction: 'remove',
    })
  }

  removeRefinement = (title, value, key, seoUrl) => {
    if (key === 'NOWPRICE') {
      this.props.removeOptionRange(title)
    } else {
      this.props.updateOptionValue(title, value)
    }
    this.props.applyRefinements(seoUrl)
  }

  renderRefinement = (refinement) => {
    const { l } = this.context
    if (refinement.title !== l`Category`) {
      const values =
        refinement.values &&
        refinement.values.map((value) => {
          return (
            <span key={value.key} className="RefinementSummary-valueWrapper">
              <span className="RefinementSummary-value RefinementSummary-refinementValue">
                <span className="RefinementSummary-valueInner">
                  {refinement.key === 'NOWPRICE' ? (
                    <div>
                      <Price price={value.lowerBound} /> -{' '}
                      <Price price={value.upperBound} />
                    </div>
                  ) : (
                    value.label
                  )}
                </span>
                <a
                  className="RefinementSummary-removeTextValue"
                  role="button"
                  onClick={() =>
                    this.removeRefinement(
                      refinement.title && refinement.title.toLowerCase(),
                      value.label,
                      refinement.key,
                      value.seoUrl
                    )
                  }
                >
                  X
                </a>
              </span>
            </span>
          )
        })
      return (
        <div key={refinement.key} className="RefinementSummary-item">
          <span className="RefinementSummary-itemTitle">
            {refinement.title}
          </span>
          <div className="RefinementSummary-valuesContainer">{values}</div>
        </div>
      )
    }
  }

  render() {
    const { brandName, categoryTitle, activeRefinements } = this.props
    const { l } = this.context
    const clearAllButtonClassName = classnames(
      'RefinementSummary-clearRefinementsButton',
      {
        'ButtonLink-button': brandName && brandName !== 'evans',
      }
    )
    return (
      <div className="RefinementSummary">
        <header className="RefinementSummary-header">
          <span className="RefinementSummary-headerTitle">{l`Filter`}</span>
          <button
            disabled={activeRefinements.length === 0}
            className={clearAllButtonClassName}
            onClick={this.clearRefinements}
          >{l`Clear all`}</button>
        </header>
        <div className="RefinementSummary-categoryItem">
          <span className="RefinementSummary-itemTitle">{l`Category`}</span>
          <span className="RefinementSummary-value">{l(categoryTitle)}</span>
        </div>
        {activeRefinements.map((refinement) =>
          this.renderRefinement(refinement)
        )}
      </div>
    )
  }
}
