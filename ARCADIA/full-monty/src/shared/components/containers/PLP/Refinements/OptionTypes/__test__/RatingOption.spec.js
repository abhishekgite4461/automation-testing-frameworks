import testComponentHelper from 'test/unit/helpers/test-component'
import RatingOption from '../RatingOption'
import { analyticsPlpClickEvent } from '../../../../../../analytics/tracking/site-interactions'
import Checkbox from '../../../../../common/FormComponents/Checkbox/Checkbox'

jest.mock('../../../../../../analytics/tracking/site-interactions', () => ({
  analyticsPlpClickEvent: jest.fn(),
}))

describe('<RatingOption />', () => {
  const initialProps = {
    onChange: jest.fn(),
    isMobile: false,
    refinement: 'a refinement',
    selections: ['some', 'selection'],
    options: [{ value: 'an option', seoUrl: '/en/tsuk/some-url' }],
    updateOptionSingle: jest.fn(),
  }
  const renderComponent = testComponentHelper(RatingOption.WrappedComponent)

  it('on selection of a checkbox onChange, onClick and the analytics event are called', () => {
    const { wrapper, instance } = renderComponent(initialProps)
    wrapper
      .find(Checkbox)
      .first()
      .simulate('change')
    expect(instance.props.updateOptionSingle).toHaveBeenCalledWith(
      initialProps.refinement,
      initialProps.options[0].value
    )
    expect(instance.props.onChange).toHaveBeenCalledWith(
      initialProps.options[0].seoUrl
    )
    expect(analyticsPlpClickEvent).toHaveBeenCalledWith(
      `${initialProps.refinement}-${initialProps.options[0].value}`
    )
  })
})
