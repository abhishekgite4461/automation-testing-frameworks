import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { insert, equals, dropLast } from 'ramda'

import Product from '../../common/Product/Product'
import Loader from '../../common/Loader/Loader'
import Espot from '../Espot/Espot'
import { IMAGE_SIZES } from '../../../constants/amplience'
import { getProductListEspots } from '../../../../shared/selectors/espotSelectors'

@connect((state) => ({
  grid: state.grid.columns,
  location: state.routing.location,
  currentPage: state.infinityScroll.currentPage,
  espots: getProductListEspots(state),
  totalProducts: state.products.totalProducts,
}))
export default class ProductList extends Component {
  static propTypes = {
    products: PropTypes.array,
    grid: PropTypes.number,
    isLoading: PropTypes.bool,
    espots: PropTypes.arrayOf(PropTypes.object),
    totalProducts: PropTypes.number,
  }

  static defaultProps = {
    products: [],
    espots: [],
  }

  shouldComponentUpdate(nextProps) {
    return !equals(this.props, nextProps)
  }

  insertContentEspot = (items, espot) => {
    const { position, identifier } = espot
    const shouldHideContentSpot = position > this.props.products.length
    return shouldHideContentSpot
      ? items
      : insert(position - 1, { isEspot: true, position, identifier }, items)
  }

  /* If product list does not render evenly across columns layout then
   * remove the last row of products
   */
  prepareRowsForRendering = (totalProducts, products, columnIndex) => {
    const dropIndex = products.length % columnIndex
    return dropIndex && totalProducts > products.length
      ? dropLast(dropIndex, products)
      : products
  }

  renderProductsAndEspots = () => {
    const {
      props: { products, grid, espots, totalProducts },
      prepareRowsForRendering,
      insertContentEspot,
    } = this

    return prepareRowsForRendering(
      totalProducts,
      espots.reduce(insertContentEspot, products),
      grid
    ).map(
      (productOrEspot, index) =>
        productOrEspot.isEspot ? (
          <Espot
            identifier={productOrEspot.identifier}
            className={`Product Product--col${grid} Espot-productList`}
            key={`productListEspot-${productOrEspot.position}`}
            qubitid={`qubit-plp-EspotProductList${index}`}
            grid={grid}
          />
        ) : (
          <Product
            key={`plp-product-${productOrEspot.productId}-${grid}`}
            grid={grid}
            sizes={IMAGE_SIZES.plp[grid]}
            {...productOrEspot}
            lazyLoad={process.browser || index > 5}
          />
        )
    )
  }

  render() {
    const {
      props: { isLoading },
      renderProductsAndEspots,
    } = this

    return (
      <div className="ProductList">
        <div className="ProductList-products">{renderProductsAndEspots()}</div>
        {isLoading && <Loader className="ProductList-loader" />}
      </div>
    )
  }
}
