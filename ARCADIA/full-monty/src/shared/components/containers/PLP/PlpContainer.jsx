import PropTypes from 'prop-types'
import QubitReact from 'qubit-react/wrapper'
import { compose, isEmpty, path } from 'ramda'
import React, { Component } from 'react'
import Helmet from 'react-helmet'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'

import { splitQuery } from '../../../lib/query-helper'
import { getRouteFromUrl } from '../../../lib/get-product-route'
import { setProducts as analyticsSetProducts } from '../../../lib/analytics'
import { getCanonicalHostname } from '../../../lib/canonicalisation'
import espots from '../../../constants/espotsMobile'
import cmsConsts from '../../../constants/cmsConsts'
import { GTM_CATEGORY } from '../../../analytics'

import { updateUniversalVariable } from '../../../lib/analytics/qubit-analytics'
import analyticsDecorator from '../../../../client/lib/analytics/analytics-decorator'
import { withWindowScroll } from '../WindowEventProvider/withWindowScroll'
import extractRouteParams from '../../../decorators/extract-route-params'

// actions
import * as productsActions from '../../../actions/common/productsActions'
import { clearSortOptions } from '../../../actions/components/sortSelectorActions'
import * as refinementsActions from '../../../actions/components/refinementsActions'
import * as plpContainerActions from '../../../actions/components/PlpContainerActions'
import * as infinityScrollActions from '../../../actions/common/infinityScrollActions'
import * as sandBoxActions from '../../../actions/common/sandBoxActions'

// selectors
import { isMobile } from '../../../selectors/viewportSelectors'
import { getGlobalEspotName } from '../../../../shared/selectors/espotSelectors'
import { getShouldIndex } from '../../../../shared/selectors/productSelectors'
import { isModalOpen } from '../../../selectors/modalSelectors'

// components
import ProductList from './ProductList'
import PlpHeader from './PlpHeader'
import Filters from './Filters'
import NoSearchResults from '../../common/NoSearchResults/NoSearchResults'
import BackToTop from '../../common/BackToTop/BackToTop'
import Refinements from './Refinements/Refinements'
import Peerius from '../../common/Peerius/Peerius'
import SandBox from '../../containers/SandBox/SandBox'
import Espot from '../Espot/Espot'
import NotFound from '../NotFound/NotFound'
import RefinementContainer from './RefinementContainer'
import ProductsBreadcrumbs from '../../common/ProductsBreadCrumbs/ProductsBreadCrumbs'
import { productListPageSize } from '../../../../server/api/mapping/constants/plp'

const NO_INDEX_NO_FOLLOW_META = {
  name: 'robots',
  content: 'noindex,nofollow',
}

class PlpContainer extends Component {
  static propTypes = {
    visited: PropTypes.array,
    location: PropTypes.object,
    products: PropTypes.array,
    breadcrumbs: PropTypes.array,
    canonicalUrl: PropTypes.string,
    getProducts: PropTypes.func,
    totalProducts: PropTypes.number,
    title: PropTypes.string,
    categoryTitle: PropTypes.string,
    categoryDescription: PropTypes.string,
    isLoadingMore: PropTypes.bool,
    isLoadingAll: PropTypes.bool,
    preservedScroll: PropTypes.number,
    isInfinityScrollActive: PropTypes.bool,
    hasReachedPageBottom: PropTypes.bool,
    hitWaypoint: PropTypes.func,
    updateProductsLocation: PropTypes.func,
    productsRefinements: PropTypes.array,
    searchTerm: PropTypes.string,
    refinements: PropTypes.object,
    showTacticalMessage: PropTypes.func,
    hideTacticalMessage: PropTypes.func,
    clearRefinements: PropTypes.func,
    clearSortOptions: PropTypes.func,
    analyticsSetProducts: PropTypes.func,
    refreshPlp: PropTypes.bool,
    plpPropsRefresh: PropTypes.func,
    updateUniversalVariable: PropTypes.func,
    applyRefinements: PropTypes.func,
    redirectIfOneProduct: PropTypes.bool,
    isMobile: PropTypes.bool.isRequired,
    categoryBanner: PropTypes.string,
    isCategoryHeaderIframeEnabled: PropTypes.bool,
    isCategoryHeaderEncodedEnabled: PropTypes.bool,
    shouldIndex: PropTypes.bool.isRequired,
    isModalOpen: PropTypes.bool.isRequired,
  }

  static defaultProps = {
    redirectIfOneProduct: false,
    catHeaderResponsiveCmsUrl: '',
    clearSortOptions: () => {},
    clearRefinements: () => {},
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentWillMount() {
    const {
      clearSortOptions,
      clearRefinements,
      location: {
        query: { refinements },
      },
    } = this.props

    if (!refinements) {
      // blank slate when loading component if refinements is not present in url query
      clearSortOptions()
      clearRefinements()
    }
  }

  componentDidMount() {
    const {
      visited,
      location,
      getProducts,
      updateProductsLocation,
      productsRefinements: refinements,
      canonicalUrl,
      preservedScroll,
      showTacticalMessage,
      plpPropsRefresh,
      updateUniversalVariable,
    } = this.props
    getProducts(location)
    showTacticalMessage()
    plpPropsRefresh()
    // On the first load, if we landed on a PLP theres a chance the URL is
    // from useablenet, using category params
    // Get rid of them.

    if (visited.length === 1) {
      if (location.query.category) {
        updateProductsLocation({ refinements, canonicalUrl }, location)
      }
    }

    // Jump to either where we were, or the top
    if (location.action !== 'PUSH' && process.browser && preservedScroll) {
      window.scrollTo(0, preservedScroll)
    } else {
      window.scrollTo(0, 0)
    }

    // Hook for the Qubit tag "iProspect Floodlight PLP"
    document.dispatchEvent(new Event('landedOnPLP'))

    updateUniversalVariable()

    if (
      visited.length > 2 &&
      location.pathname === visited[visited.length - 3]
    ) {
      analyticsSetProducts('PLP Container')
    }
  }

  searchTermsExistAndAreNotEqual(searchTerm, nextSearchTerm) {
    if (
      typeof searchTerm === 'undefined' ||
      typeof nextSearchTerm === 'undefined'
    )
      return true
    return searchTerm !== nextSearchTerm
  }

  componentWillReceiveProps(nextProps) {
    const {
      searchTerm: nextSearchTerm,
      location: nextLocation,
      products: nextProducts,
      isLoadingAll: nextIsLoadingAll,
      refreshPlp: nextRefreshPlp,
      hasReachedPageBottom: nextHasReachedPageBottom,
      isModalOpen,
    } = nextProps

    const {
      searchTerm,
      location,
      getProducts,
      visited,
      clearRefinements,
      analyticsSetProducts,
      refreshPlp,
      hasReachedPageBottom,
      redirectIfOneProduct,
    } = this.props

    if (
      !isModalOpen &&
      this.props.isInfinityScrollActive &&
      nextHasReachedPageBottom &&
      !hasReachedPageBottom
    ) {
      return this.loadingMoreProducts()
    }

    const searchTermsExistAndNotEqual = this.searchTermsExistAndAreNotEqual(
      searchTerm,
      nextSearchTerm
    )
    // TODO Should be in the search action
    /*
       The logic `visited.length > 1` is to check whether the user has gone straight to
       a specific canonical url.

       If the user has then do not redirect to the pdp if there is only one product.
     */
    if (
      redirectIfOneProduct &&
      !nextIsLoadingAll &&
      nextProducts &&
      nextProducts.length === 1 &&
      nextProducts[0] &&
      visited.length > 1
    ) {
      const { sourceUrl, seoUrl } = nextProducts[0]
      browserHistory.replace(getRouteFromUrl(sourceUrl || seoUrl))
      return
    }

    if (nextLocation !== location) {
      // If we navigate around using the browser back, need to clear the refinements
      if (nextLocation.action === 'POP') clearRefinements()
      getProducts(nextLocation)
    } else if (refreshPlp !== nextRefreshPlp && searchTermsExistAndNotEqual) {
      analyticsSetProducts()
    }
  }

  shouldComponentUpdate(nextProps) {
    const props = [
      'products',
      'breadcrumbs',
      'totalProducts',
      'categoryTitle',
      'categoryDescription',
      'isLoadingAll',
      'isLoadingMore',
      'isMobile',
      'stickyHeader',
    ]

    return !!props.find((prop) => {
      return nextProps[prop] !== this.props[prop]
    })
  }

  componentDidUpdate(prevProps) {
    const { isLoadingAll } = this.props
    const { isLoadingAll: prevIsLoadingAll } = prevProps

    if (!prevIsLoadingAll && isLoadingAll) {
      window.scrollTo(0, 0)
    }
  }

  componentWillUnmount() {
    const { hideTacticalMessage } = this.props
    hideTacticalMessage()
  }

  getPeeriusTracking = () => {
    const {
      location: { pathname, search },
      breadcrumbs,
      products,
      isLoadingAll,
    } = this.props

    if (isLoadingAll) return null

    const query = splitQuery(search)
    if (pathname.includes('search')) {
      return (
        products && (
          <Peerius type="searchResults" term={query.q} products={products} />
        )
      )
    }
    return <Peerius type="category" breadcrumbs={breadcrumbs} />
  }

  getProductList = () => {
    const { isLoadingMore, products } = this.props
    const productListType = this.productListType
    const mobileEspot = (
      <SandBox
        key={2}
        {...{
          cmsPageName: espots.plp[0],
          contentType: cmsConsts.ESPOT_CONTENT_TYPE,
          isInPageContent: true,
        }}
      />
    )
    const filter = <Filters key={1} />
    const refinements = <Refinements key={3} />
    const listHeader = [filter, mobileEspot, refinements]
    if (productListType === 'loading') {
      return [...listHeader, <ProductList isLoading key={'lo4'} />]
    }
    if (productListType === 'productList') {
      return [
        ...listHeader,
        <ProductList key="pl4" products={products} isLoading={isLoadingMore} />,
        <BackToTop key="plp-back-to-top" />,
        this.getPagination(),
      ]
    }
    if (productListType === 'noSearchResultsWithRefinements') {
      return [...listHeader, <NoSearchResults filterType key={'ns4'} />]
    }
    if (productListType === 'noSearchResultsIncludesSearch') {
      return <NoSearchResults />
    }

    return <NotFound />
  }

  get productListType() {
    const {
      isLoadingAll,
      totalProducts,
      refinements,
      location: { pathname },
    } = this.props
    if (isLoadingAll) return 'loading'
    if (totalProducts > 0) return 'productList'
    // following refinements search show no search results under filter container
    if (!isEmpty(refinements)) return 'noSearchResultsWithRefinements'
    // If it's a search, show no search results
    if (pathname.includes('search')) return 'noSearchResultsIncludesSearch'
    return 'notFound'
  }

  getSeoUrls = () => {
    const {
      totalProducts,
      location: { protocol, hostname, pathname, query },
      canonicalUrl: seoUrl,
    } = this.props
    // These urls should be provided by the API, but if they are not we can guess at them
    const canonicalUrl =
      seoUrl || `${protocol}//${getCanonicalHostname(hostname)}${pathname}`
    if (!query) return { canonicalUrl }

    const maxPages = Math.ceil(totalProducts / productListPageSize)
    const currPage = query.currentPage ? parseInt(query.currentPage, 10) : 1
    const paginationUrl = `${pathname}?${
      query.q ? `q=${decodeURIComponent(query.q)}&` : ''
    }pagination=1&currentPage=`
    const prevPageUrl =
      currPage > 1 ? paginationUrl + (currPage - 1) : undefined
    const nextPageUrl =
      currPage < maxPages ? paginationUrl + (currPage + 1) : undefined

    return {
      canonicalUrl,
      nextPageUrl,
      prevPageUrl,
    }
  }

  getHelmetProps = () => {
    const {
      isLoadingAll,
      title,
      categoryTitle,
      categoryDescription,
      location: { pathname },
      shouldIndex,
    } = this.props
    const { canonicalUrl, nextPageUrl, prevPageUrl } = this.getSeoUrls()
    const { l } = this.context

    if (pathname.includes('search')) {
      return {
        title: l`Search - ${categoryTitle}`,
        meta: [
          {
            name: 'description',
            content: l`Search - ${categoryTitle}`,
          },
          NO_INDEX_NO_FOLLOW_META,
        ],
      }
    }

    const canonical = canonicalUrl && { rel: 'canonical', href: canonicalUrl }
    const prevPage = prevPageUrl && { rel: 'prev', href: prevPageUrl }
    const nextPage = nextPageUrl && { rel: 'next', href: nextPageUrl }

    const link = [canonical, prevPage, nextPage].filter(Boolean)

    if (isLoadingAll) {
      return {
        link,
        title: l`loading products`,
        meta: [
          {
            name: 'description',
            content: l`loading products`,
          },
          NO_INDEX_NO_FOLLOW_META,
        ],
      }
    }

    const desc = {
      name: 'description',
      content: categoryDescription,
    }

    return {
      link,
      title,
      meta: shouldIndex ? [desc] : [desc, NO_INDEX_NO_FOLLOW_META],
    }
  }

  getHelmet = () => {
    return <Helmet {...this.getHelmetProps()} />
  }

  getPagination = () => {
    const { nextPageUrl, prevPageUrl } = this.getSeoUrls()
    const { l } = this.context
    return (
      <div key={6} className="PlpContainer-pagination">
        <a
          className={`PlpContainer-paginationPrev`}
          href={prevPageUrl}
        >{l`Previous`}</a>
        <a
          className={`PlpContainer-paginationNext`}
          href={nextPageUrl}
        >{l`Next`}</a>
      </div>
    )
  }

  loadingMoreProducts = () => {
    const { products = [], totalProducts, hitWaypoint } = this.props

    return products.length < parseInt(totalProducts, 10) && hitWaypoint()
  }

  static needs = [
    sandBoxActions.showTacticalMessage,
    productsActions.getServerProducts,
    () =>
      sandBoxActions.getContent(
        null,
        espots.plp[0],
        cmsConsts.ESPOT_CONTENT_TYPE
      ),
  ]

  get hasNoSearchResult() {
    return (
      this.productListType === 'noSearchResultsWithRefinements' ||
      this.productListType === 'noSearchResultsIncludesSearch'
    )
  }

  render() {
    const {
      categoryTitle,
      totalProducts,
      categoryBanner,
      isMobile,
      location,
      globalEspotName,
      refinements,
      brandCode,
      isCategoryHeaderIframeEnabled,
      isCategoryHeaderEncodedEnabled,
      breadcrumbs,
      stickyHeader,
      catHeaderResponsiveCmsUrl,
    } = this.props
    const plp = (
      <div className="PlpContainer">
        <ProductsBreadcrumbs breadcrumbs={breadcrumbs} />
        {this.getHelmet()}
        {this.getPeeriusTracking()}

        <Espot identifier={globalEspotName} />

        <PlpHeader
          key={0}
          title={categoryTitle}
          total={totalProducts && totalProducts.toString()}
          bannerHTML={categoryBanner}
          isMobile={isMobile}
          brandCode={brandCode}
          location={location}
          isCategoryHeaderIframeEnabled={isCategoryHeaderIframeEnabled}
          isCategoryHeaderEncodedEnabled={isCategoryHeaderEncodedEnabled}
          catHeaderResponsiveCmsUrl={catHeaderResponsiveCmsUrl}
        />
        <div className="PlpContainer-resultsSection">
          <RefinementContainer
            stickyHeader={stickyHeader}
            applyRefinements={this.props.applyRefinements}
            hasNoSearchResult={this.hasNoSearchResult}
            refinements={refinements}
          />
          <QubitReact id="qubit-plp-productListContainer">
            <div className="PlpContainer-productListContainer">
              {this.getProductList()}
            </div>
          </QubitReact>
        </div>
      </div>
    )

    return this.hasNoSearchResult ? (
      <QubitReact id="qubit-Plp-NoResults">{plp}</QubitReact>
    ) : (
      plp
    )
  }
}

export function composeCategoryBanner(state) {
  return (
    path(['products', 'categoryBanner', 'encodedcmsMobileContent'], state) ||
    path(['products', 'categoryBanner', 'bannerHtml'], state) ||
    ''
  )
}

export default compose(
  analyticsDecorator(GTM_CATEGORY.PLP, { isAsync: true }),
  connect(
    (state) => ({
      visited: state.routing.visited,
      products: state.products.products,
      productsRefinements: state.products.refinements,
      productsLocation: state.products.location,
      searchTerm: state.products.searchTerm,
      totalProducts: state.products.totalProducts,
      canonicalUrl: state.products.canonicalUrl,
      title: state.products.title,
      categoryTitle: state.products.categoryTitle,
      categoryDescription: state.products.categoryDescription,
      breadcrumbs: state.products.breadcrumbs,
      brandCode: state.config.brandCode,
      isLoadingMore: state.products.isLoadingMore,
      isLoadingAll: state.products.isLoadingAll,
      isInfinityScrollActive: state.infinityScroll.isActive,
      preservedScroll: state.infinityScroll.preservedScroll,
      refinements: state.refinements.selectedOptions,
      refreshPlp: state.plpContainer.refreshPlp,
      isMobile: isMobile(state),
      categoryBanner: composeCategoryBanner(state),
      catHeaderResponsiveCmsUrl: path(
        ['products', 'categoryBanner', 'cmsMobileContent', 'responsiveCMSUrl'],
        state
      ),
      globalEspotName: getGlobalEspotName(state),
      isCategoryHeaderIframeEnabled:
        state.features.status.FEATURE_LEGACY_CAT_HEADERS_IFRAME,
      isCategoryHeaderEncodedEnabled:
        state.features.status.FEATURE_LEGACY_CAT_HEADERS,
      shouldIndex: getShouldIndex(state),
      stickyHeader: state.pageHeader.sticky,
      isModalOpen: isModalOpen(state),
    }),
    {
      ...productsActions,
      ...plpContainerActions,
      ...infinityScrollActions,
      ...sandBoxActions,
      ...refinementsActions,
      clearSortOptions,
      analyticsSetProducts,
      updateUniversalVariable,
    }
  ),
  withWindowScroll({
    notifyWhenReachedBottomOfPage: true,
    pageBottomBuffer: 1400,
  }),
  extractRouteParams(['redirectIfOneProduct'])
)(PlpContainer)

export { PlpContainer as WrappedPlpContainer }
