import { shallow } from 'enzyme'
import QubitReact from 'qubit-react/wrapper'
import React from 'react'
import { browserHistory } from 'react-router'

import testComponentHelper, { until } from 'test/unit/helpers/test-component'
import { mockStoreCreator } from '../../../../../../test/unit/helpers/get-redux-mock-store'
import { forAnalyticsDecorator as createMockStoreForAnalytics } from 'test/unit/helpers/mock-store'
import mockProducts from 'test/mocks/products'

import PlpContainer, {
  WrappedPlpContainer,
  composeCategoryBanner,
} from '../PlpContainer'
import NotFound from '../../NotFound/NotFound'
import ProductList from '../ProductList'
import PlpHeader from '../PlpHeader'
import RefinementContainer from '../RefinementContainer'
import Peerius from '../../../common/Peerius/Peerius'
import Filters from '../Filters'
import BackToTop from '../../../common/BackToTop/BackToTop'
import ProductsBreadCrumbs from '../../../common/ProductsBreadCrumbs/ProductsBreadCrumbs'

jest.mock('../../../../lib/get-product-route.js', () => ({
  getRouteFromUrl: jest.fn(),
}))

jest.mock('../PlpHeader.jsx', () => 'PlpHeader')

const NO_INDEX_NO_FOLLOW_META = {
  name: 'robots',
  content: 'noindex,nofollow',
}

import * as productRoutes from '../../../../lib/get-product-route'

beforeAll(() => {
  global.s = {
    Util: {
      cookieWrite: jest.fn(),
    },
  }
  // Only For wallaby
  if (!global.performance) {
    global.performance = {
      timing: {
        navigationStart: 1000,
      },
    }
  }
})

global.scrollTo = jest.fn()

describe('PlpContainer', () => {
  beforeEach(() => jest.resetAllMocks())

  const defaultLocation = {
    pathname: 'fakepathname',
    action: 'POP',
    query: {},
  }

  const renderComponent = testComponentHelper(WrappedPlpContainer)
  const initialProps = {
    visited: [],
    location: defaultLocation,
    products: [],
    breadcrumbs: [],
    brandUrl: 'someString',
    brandCode: 'someString',
    canonicalUrl: 'canonicalUrl',
    getProducts: jest.fn(),
    getServerProducts: jest.fn(),
    clearOptions: jest.fn(),
    clearProducts: jest.fn(),
    setInfinityPage: jest.fn(),
    totalProducts: 'someString',
    categoryTitle: 'categoryTitle',
    title: 'page title',
    categoryDescription: 'categoryDescription',
    setInfinityActive: jest.fn(),
    isLoadingMore: false,
    isLoadingAll: false,
    preservedScroll: 10,
    isInfinityScrollActive: false,
    hitWaypoint: jest.fn(),
    params: {},
    updateProductsLocation: jest.fn(),
    productsRefinements: [],
    productsLocation: {},
    searchTerm: '',
    refinements: {},
    showTacticalMessage: jest.fn(),
    hideTacticalMessage: jest.fn(),
    clearRefinements: jest.fn(),
    clearSortOptions: jest.fn(),
    analyticsSetProducts: jest.fn(),
    refreshPlp: false,
    plpPropsRefresh: jest.fn(),
    updateUniversalVariable: jest.fn(),
    applyRefinements: jest.fn(),
    isMobile: false,
    categoryBanner: '',
    shouldIndex: true,
    stickyHeader: false,
    isModalOpen: false,
  }

  describe('@render', () => {
    it('default render', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })

    it('renders for mobile', () => {
      expect(
        renderComponent({ ...initialProps, isMobile: true }).getTree()
      ).toMatchSnapshot()
    })

    it('renders ProductsBreadCrumbs', () => {
      const { wrapper } = renderComponent(initialProps)
      expect(wrapper.find(ProductsBreadCrumbs)).toHaveLength(1)
    })

    it('renders stickyHeader=true', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        stickyHeader: true,
      })
      const RefinementContainerComponent = wrapper.find(RefinementContainer)
      expect(RefinementContainerComponent).toHaveLength(1)
      expect(RefinementContainerComponent.prop('stickyHeader')).toBe(true)
    })

    it('shows no search results when no items are found', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        isLoadingAll: false,
        totalProducts: '0',
        refinements: {
          color: 'black',
        },
      })
      expect(wrapper.find('Connect(NoSearchResults)')).toHaveLength(1)
    })

    it('should wrap in <QubitReact /> if no results', () => {
      const { wrapper } = renderComponent({
        ...initialProps,
        isLoadingAll: false,
        totalProducts: '0',
        refinements: {
          color: 'black',
        },
      })
      expect(wrapper.prop('id')).toBe('qubit-Plp-NoResults')
      expect(wrapper.is(QubitReact)).toBe(true)
    })

    it('should show not found when no products returned for a category url', () => {
      const props = {
        ...initialProps,
        isLoadingAll: false,
        getProducts: () => {},
      }

      const { wrapper } = renderComponent(props)
      expect(wrapper.find(NotFound)).toHaveLength(1)
    })

    it('should display products when products returned', () => {
      const props = {
        ...initialProps,
        products: mockProducts,
        totalProducts: '372',
      }

      const { wrapper } = renderComponent(props)
      expect(wrapper.find(ProductList)).toHaveLength(1)
    })

    it('should display filters when products returned', () => {
      const props = {
        ...initialProps,
        products: mockProducts,
        totalProducts: '372',
      }

      const { wrapper } = renderComponent(props)
      expect(wrapper.find(Filters)).toHaveLength(1)
    })

    it('should display title when products returned', () => {
      const props = {
        ...initialProps,
        products: mockProducts,
        totalProducts: '372',
      }

      const { wrapper } = renderComponent(props)
      expect(wrapper.find(PlpHeader)).toHaveLength(1)
    })

    it('should display back to top button when products returned', () => {
      const props = {
        ...initialProps,
        products: mockProducts,
        totalProducts: '372',
      }

      const { wrapper } = renderComponent(props)
      expect(wrapper.find(BackToTop)).toHaveLength(1)
    })

    it('should display filters when products change', () => {
      const props = {
        ...initialProps,
        totalProducts: '372',
      }

      const { wrapper } = renderComponent(props)
      expect(wrapper.find(Filters)).toHaveLength(1)
    })

    it('should create pagination links', () => {
      const props = {
        ...initialProps,
        totalProducts: '372',
      }
      const { wrapper } = renderComponent(props)
      expect(wrapper.find('.PlpContainer-pagination')).toHaveLength(1)
      expect(wrapper.find('.PlpContainer-paginationNext')).toHaveLength(1)
      expect(wrapper.find('.PlpContainer-paginationPrev')).toHaveLength(1)
    })

    it('should call Peerius with search results', () => {
      const props = {
        ...initialProps,
        products: mockProducts,
        location: {
          pathname: '/search',
          search: '?q=hello',
          query: {},
        },
      }
      const { wrapper } = renderComponent(props)
      const expected = {
        type: 'searchResults',
        term: 'hello',
        products: mockProducts,
      }

      expect(wrapper.find(Peerius).props()).toEqual(expected)
    })

    it('should not call peerius while loading', () => {
      const props = {
        ...initialProps,
        location: {
          pathname: '/en/tsuk/category/clothing-281559/mens-jeans-281570',
          search: '?category=203984%2C208524&currentPage=3',
          query: {},
        },
        isLoadingAll: true,
      }
      const { wrapper } = renderComponent(props)

      expect(wrapper.find(Peerius)).toHaveLength(0)
    })

    it('should call Peerius with categories', () => {
      const props = {
        ...initialProps,
        brandCode: 'tsuk',
        visited: ['one'],
        products: mockProducts,
        breadcrumbs: [{ label: 'Bread' }, { label: 'crumbs' }],
        totalProducts: '372',
        location: {
          pathname: '/en/tsuk/category/clothing-281559/mens-jeans-281570',
          search: '?category=203984%2C208524&currentPage=3',
          query: {},
        },
      }

      const { wrapper } = renderComponent(props)
      const expected = {
        type: 'category',
        breadcrumbs: props.breadcrumbs,
      }
      expect(wrapper.find(Peerius).props()).toEqual(expected)
    })

    it('should call hitWaypoint when there are more products', () => {
      const hitWaypoint = jest.fn()
      const props = {
        ...initialProps,
        totalProducts: '372',
        hitWaypoint,
      }
      const { instance } = renderComponent(props)

      instance.loadingMoreProducts()

      expect(hitWaypoint).toHaveBeenCalled()
    })

    it('should not call hitWaypoint when all the products are loaded', () => {
      const hitWaypoint = jest.fn()
      const props = {
        ...initialProps,
        products: mockProducts,
        totalProducts: '20',
        hitWaypoint,
      }
      const { instance } = renderComponent(props)

      instance.loadingMoreProducts()

      expect(hitWaypoint).not.toHaveBeenCalled()
    })

    it('renders responsive plp category header', () => {
      const props = {
        ...initialProps,
        catHeaderResponsiveCmsUrl:
          '/cms/pages/json/json-0000140416/json-0000140416.json',
      }
      expect(renderComponent(props).getTree()).toMatchSnapshot()
    })
  })

  describe('@decorators', () => {
    it('should be wrapped container in analytics, and window scroll', () => {
      const mockStore = createMockStoreForAnalytics({ preloadedState: {} })
      const shallowOptions = { context: { store: mockStore } }
      const wrapper = shallow(<PlpContainer />, shallowOptions)
      const analyticsDecorator = until(
        wrapper,
        'AnalyticsDecorator',
        shallowOptions
      )

      expect(PlpContainer.displayName).toMatch(/AnalyticsDecorator/)
      expect(analyticsDecorator.instance().pageType).toBe('plp')
      expect(analyticsDecorator.instance().isAsync).toBe(true)
      expect(analyticsDecorator.instance().sendAdobe).toBe(true)
    })
  })

  describe('@methods', () => {
    describe('@getHelmetProps', () => {
      it('should return noindex meta for search page', () => {
        const props = {
          ...initialProps,
          location: {
            ...defaultLocation,
            pathname: 'prefix/search?querystring',
          },
        }

        const { instance } = renderComponent(props)

        expect(instance.getHelmetProps()).toEqual({
          title: `Search - ${initialProps.categoryTitle}`,
          meta: [
            {
              name: 'description',
              content: `Search - ${initialProps.categoryTitle}`,
            },
            NO_INDEX_NO_FOLLOW_META,
          ],
        })
      })

      it('should return noindex meta when products are loading', () => {
        const props = {
          ...initialProps,
          isLoadingAll: true,
        }
        const link = [{ rel: 'canonical', href: initialProps.canonicalUrl }]
        const { instance } = renderComponent(props)
        expect(instance.getHelmetProps()).toEqual({
          link,
          title: `loading products`,
          meta: [
            {
              name: 'description',
              content: `loading products`,
            },
            NO_INDEX_NO_FOLLOW_META,
          ],
        })
      })

      it('should return canonical link, without noindex meta when shouldIndex is true', () => {
        const props = {
          ...initialProps,
          shouldIndex: true,
        }
        const link = [{ rel: 'canonical', href: initialProps.canonicalUrl }]
        const desc = {
          name: 'description',
          content: initialProps.categoryDescription,
        }
        const { instance } = renderComponent(props)
        expect(instance.getHelmetProps()).toEqual({
          link,
          title: initialProps.title,
          meta: [desc],
        })
      })

      it('should return noindex meta when shouldIndex is false', () => {
        const props = {
          ...initialProps,
          shouldIndex: false,
        }
        const link = [{ rel: 'canonical', href: initialProps.canonicalUrl }]
        const desc = {
          name: 'description',
          content: initialProps.categoryDescription,
        }
        const { instance } = renderComponent(props)
        expect(instance.getHelmetProps()).toEqual({
          link,
          title: initialProps.title,
          meta: [desc, NO_INDEX_NO_FOLLOW_META],
        })
      })
    })
  })

  describe('@lifecycle', () => {
    describe('@componentDidMount', () => {
      it('should have called updateUniversalVariable', async () => {
        const props = {
          ...initialProps,
        }

        expect(props.updateUniversalVariable).not.toHaveBeenCalled()

        const { instance } = renderComponent(props)

        instance.componentDidMount()

        expect(instance.props.updateUniversalVariable).toHaveBeenCalledTimes(1)
      })

      it('should have called updateProductsLocation', async () => {
        const props = {
          ...initialProps,
          updateProductsLocation: jest.fn(),
          visited: ['category'],
          location: {
            ...defaultLocation,
            query: {
              category: 'category',
            },
          },
        }

        expect(props.updateProductsLocation).not.toHaveBeenCalled()

        const { instance } = renderComponent(props)

        instance.componentDidMount()

        expect(instance.props.updateProductsLocation).toHaveBeenCalledTimes(1)
      })

      it('should preserve scroll', async () => {
        const props = {
          ...initialProps,
          updateProductsLocation: jest.fn(),
          visited: ['category'],
          location: {
            ...defaultLocation,
            action: 'POP',
          },
        }
        expect(global.scrollTo).not.toHaveBeenCalled()

        const { instance } = renderComponent(props)

        const browserState = process.browser
        global.process.browser = true

        instance.componentDidMount()

        expect(global.scrollTo).toHaveBeenCalledWith(0, 10)

        global.process.browser = browserState
      })
    })

    describe('@componentWillMount', () => {
      it('should call clearRefinements and clearSortOptions', () => {
        const props = {
          ...initialProps,
          clearRefinements: jest.fn(),
          clearSortOptions: jest.fn(),
        }

        expect(props.clearRefinements).not.toHaveBeenCalled()
        expect(props.clearSortOptions).not.toHaveBeenCalled()

        const { instance } = renderComponent(props)

        expect(instance.props.clearRefinements).toHaveBeenCalledTimes(1)
        expect(instance.props.clearSortOptions).toHaveBeenCalledTimes(1)
      })

      it('should NOT call clearRefinements and clearSortOptions if refinements is in the url query', () => {
        const props = {
          ...initialProps,
          clearRefinements: jest.fn(),
          clearSortOptions: jest.fn(),
          location: {
            ...defaultLocation,
            query: {
              refinements: 'colour:yellow',
            },
          },
        }

        expect(props.clearRefinements).not.toHaveBeenCalled()
        expect(props.clearSortOptions).not.toHaveBeenCalled()

        const { instance } = renderComponent(props)

        expect(instance.props.clearRefinements).not.toHaveBeenCalled()
        expect(instance.props.clearSortOptions).not.toHaveBeenCalled()
      })
    })

    describe('@componentWillReceiveProps', () => {
      let browserHistoryReplaceSpy
      beforeEach(() => {
        browserHistoryReplaceSpy = jest.spyOn(browserHistory, 'replace')
      })

      it('should call searchTermsExistAndAreNotEqual', () => {
        const { instance } = renderComponent(initialProps)
        const searchTermsEqualSpy = jest.spyOn(
          instance,
          'searchTermsExistAndAreNotEqual'
        )

        expect(searchTermsEqualSpy).not.toHaveBeenCalled()
        instance.componentWillReceiveProps({
          location: {
            action: 'blah',
          },
        })
        expect(searchTermsEqualSpy).toHaveBeenCalledTimes(1)
      })

      it('should call loadingMoreProducts', () => {
        const getProducts = jest.fn()
        const analyticsSetProducts = jest.fn()
        const { instance } = renderComponent({
          ...initialProps,
          isInfinityScrollActive: true,
          hasReachedPageBottom: false,
          getProducts,
          analyticsSetProducts,
        })
        const loadingMoreProductsSpy = jest.spyOn(
          instance,
          'loadingMoreProducts'
        )
        const nextProps = {
          isModalOpen: false,
          hasReachedPageBottom: true,
        }
        instance.componentWillReceiveProps(nextProps)
        expect(loadingMoreProductsSpy).toHaveBeenCalled()
        expect(browserHistoryReplaceSpy).not.toHaveBeenCalled()
        expect(getProducts).not.toHaveBeenCalled()
        expect(analyticsSetProducts).not.toHaveBeenCalled()
      })

      it('should not call loadingMoreProducts', () => {
        const getProducts = jest.fn()
        const analyticsSetProducts = jest.fn()
        const { instance } = renderComponent({
          ...initialProps,
          isInfinityScrollActive: true,
          hasReachedPageBottom: false,
          getProducts,
          analyticsSetProducts,
        })
        const loadingMoreProductsSpy = jest.spyOn(
          instance,
          'loadingMoreProducts'
        )
        const nextProps = {
          isModalOpen: true,
          hasReachedPageBottom: true,
          location: {},
        }
        instance.componentWillReceiveProps(nextProps)
        expect(loadingMoreProductsSpy).not.toHaveBeenCalled()
      })

      it('should browserHistory and getRouteFromUrl when not loading, has one product and visited length > 1', () => {
        const getRouteFromUrlSpy = jest.spyOn(productRoutes, 'getRouteFromUrl')

        const { instance } = renderComponent({
          ...initialProps,
          visited: [1, 2],
          redirectIfOneProduct: true,
        })

        expect(browserHistoryReplaceSpy).not.toHaveBeenCalled()
        expect(getRouteFromUrlSpy).not.toHaveBeenCalled()
        instance.componentWillReceiveProps({
          isLoadingAll: false,
          products: [{ sourceUrl: 'someUrl' }],
          location: {
            action: 'blah',
          },
        })
        expect(browserHistoryReplaceSpy).toHaveBeenCalledTimes(1)
        expect(getRouteFromUrlSpy).toHaveBeenCalledTimes(1)
        expect(getRouteFromUrlSpy).toHaveBeenCalledWith('someUrl')
      })

      describe('Next location does not equal current location', () => {
        it('calls clearRefinements and getProducts when nextLocation.action is POP', () => {
          const { instance } = renderComponent({
            ...initialProps,
            clearRefinements: jest.fn(),
            getProducts: jest.fn(),
          })

          expect(instance.props.clearRefinements).toHaveBeenCalledTimes(1)
          expect(instance.props.getProducts).not.toHaveBeenCalled()

          instance.componentWillReceiveProps({
            isLoadingAll: true,
            products: [{ sourceUrl: 'someUrl' }],
            location: {
              action: 'POP',
              pathname: 'someotherpathname',
            },
          })
          expect(instance.props.clearRefinements).toHaveBeenCalledTimes(2)
          expect(instance.props.getProducts).toHaveBeenCalledTimes(1)
        })

        it('calls getProducts but not clearRefinements when action is not POP', () => {
          const { instance } = renderComponent(initialProps)

          expect(instance.props.clearRefinements).toHaveBeenCalledTimes(1)
          expect(instance.props.getProducts).not.toHaveBeenCalled()

          instance.componentWillReceiveProps({
            isLoadingAll: true,
            products: [{ sourceUrl: 'someUrl' }],
            location: {
              action: 'NOTPOP',
              pathname: 'someotherpathname',
            },
          })

          expect(instance.props.clearRefinements).toHaveBeenCalledTimes(1)
          expect(instance.props.getProducts).toHaveBeenCalledTimes(1)
          expect(instance.props.getProducts).toHaveBeenCalledWith({
            action: 'NOTPOP',
            pathname: 'someotherpathname',
          })
        })
      })

      describe('Next location does equal current location', () => {
        it('calls analyticsSetProducts when refreshPlp and nextRefreshPLP are not equal and searchTerms are not equal', () => {
          const { instance } = renderComponent(initialProps)
          jest
            .spyOn(instance, 'searchTermsExistAndAreNotEqual')
            .mockImplementation(() => true)

          expect(instance.props.analyticsSetProducts).not.toHaveBeenCalled()

          instance.componentWillReceiveProps({
            isLoadingAll: true,
            products: [{ sourceUrl: 'someUrl' }],
            location: defaultLocation,
            refreshPlp: true,
          })

          expect(instance.props.analyticsSetProducts).toHaveBeenCalledTimes(1)
        })
      })
    })
    describe('@componentWillUnmount', () => {
      it('should have called hideTacticalMessage', async () => {
        const props = {
          ...initialProps,
        }

        expect(props.hideTacticalMessage).not.toHaveBeenCalled()

        const { instance } = renderComponent(props)

        instance.componentWillUnmount()

        expect(props.hideTacticalMessage).toHaveBeenCalledTimes(1)
      })
    })

    describe('@componentDidUpdate', () => {
      const prevProps = {
        isLoadingAll: false,
      }

      it('should scroll user to top if user isLoadingAll is true and previous isLoadingAll is false', async () => {
        const props = {
          ...initialProps,
          isLoadingAll: true,
        }
        const { instance } = renderComponent(props)

        expect(global.scrollTo).not.toHaveBeenCalled()
        instance.componentDidUpdate(prevProps)
        expect(global.scrollTo).toHaveBeenCalledTimes(1)
        expect(global.scrollTo).toHaveBeenCalledWith(0, 0)
      })

      it('should not scroll user to top if isLoadingAll is false and previous isLoadingAll is false', async () => {
        const props = {
          ...initialProps,
          isLoadingAll: false,
        }
        const { instance } = renderComponent(props)

        expect(global.scrollTo).not.toHaveBeenCalled()
        instance.componentDidUpdate(prevProps)
        expect(global.scrollTo).not.toHaveBeenCalled()
      })

      it('should not scroll user to top if isLoadingAll is true and previous isLoadingAll is true', async () => {
        const props = {
          ...initialProps,
          isLoadingAll: true,
        }
        const { instance } = renderComponent(props)

        expect(global.scrollTo).not.toHaveBeenCalled()
        instance.componentDidUpdate({
          ...prevProps,
          isLoadingAll: true,
        })
        expect(global.scrollTo).not.toHaveBeenCalled()
      })

      it('should not scroll user to top if isLoadingAll is false and previous isLoadingAll is true', async () => {
        const props = {
          ...initialProps,
          isLoadingAll: false,
        }
        const { instance } = renderComponent(props)

        expect(global.scrollTo).not.toHaveBeenCalled()
        instance.componentDidUpdate({
          ...prevProps,
          isLoadingAll: true,
        })
        expect(global.scrollTo).not.toHaveBeenCalled()
      })
    })
  })

  describe('@instance', () => {
    describe('@searchTermsExistAndAreNotEqual', () => {
      const { instance } = renderComponent(initialProps)
      const searchTerm = 'searchyMcSearchFace'

      it('should return true if either or both are undefined', () => {
        expect(
          instance.searchTermsExistAndAreNotEqual(undefined, undefined)
        ).toBe(true)
        expect(
          instance.searchTermsExistAndAreNotEqual(undefined, searchTerm)
        ).toBe(true)
        expect(
          instance.searchTermsExistAndAreNotEqual(searchTerm, undefined)
        ).toBe(true)
      })

      it('should return true if both exist but are different', () => {
        expect(
          instance.searchTermsExistAndAreNotEqual(searchTerm, 'blah')
        ).toBe(true)
        expect(
          instance.searchTermsExistAndAreNotEqual('blah', searchTerm)
        ).toBe(true)
      })

      it('should return false if both exist and are equal', () => {
        expect(
          instance.searchTermsExistAndAreNotEqual(searchTerm, searchTerm)
        ).toBe(false)
      })
    })
  })

  describe('@component helpers', () => {
    describe('composeCategoryBanner', () => {
      it('should return encoded html data from encodedcmsMobileContent for category banner', () => {
        const props = {
          ...initialProps,
          products: {
            categoryBanner: {
              encodedcmsMobileContent: 'mock_encoded_html_data',
            },
          },
        }
        const store = mockStoreCreator(props)
        const state = store.getState()
        const banner = composeCategoryBanner(state)
        expect(banner).toEqual('mock_encoded_html_data')
      })
      it('should return encoded html data from bannerHtml as a fallback for category banner', () => {
        const props = {
          ...initialProps,
          products: {
            categoryBanner: {
              bannerHtml: 'fallback_html_encoded',
            },
          },
        }
        const store = mockStoreCreator(props)
        const state = store.getState()
        const banner = composeCategoryBanner(state)
        expect(banner).toEqual('fallback_html_encoded')
      })
      it('should return empty string if no categoryBanner', () => {
        const props = {
          ...initialProps,
          products: {
            categoryBanner: {},
          },
        }
        const store = mockStoreCreator(props)
        const state = store.getState()
        const banner = composeCategoryBanner(state)
        expect(banner).toEqual('')
      })
    })
  })
})
