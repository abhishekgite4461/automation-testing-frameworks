import {
  buildComponentRender,
  shallowRender,
} from 'test/unit/helpers/test-component'
import PlpHeader from '../PlpHeader'

const bannerHtml1 =
  '%3C%21--+CMS+Page+Version%3A+categoryheader-0000077316+baseline%3A+63+revision%3A+0+published%3A+2017-07-05+11%3A25%3A12.826409%3ABen+Moffitt+--%3E%0A%3C%21--+CMS+Page+Data+Start+--%3E%0A%0A%0A++%0A%3Cscript+type%3D%22text%2Fjavascript%22%3E%0A%2F%2F%3C%21%5BCDATA%5B%0A++++++++var+cmsPage77316+%3D+%7B%0A++++++++++++++++%22pageId%22%3A+77316%2C%0A++++++++++++++++%22pageName%22%3A+%22categoryheader-0000077316%22%2C%0A++++++++++++++++%22breadcrumb%22%3A+%22%22%2C%0A++++++++++++++++%22baseline%22%3A+%2263%22%2C%0A++++++++++++++++%22revision%22%3A+%220%22%2C%0A++++++++++++++++%22lastPublished%22%3A+%222017-07-05+11%3A25%3A12.826409%22%2C%0A++++++++++++++++%22contentPath%22%3A+%22cms%2Fpages%2Fcategoryheader%2Fcategoryheader-0000077316%2Fcategoryheader-0000077316.html%22%2C%0A++++++++++++++++%22seoUrl%22%3A+%22%22%2C%0A++++++++++++++++%22mobileCMSUrl%22%3A+%22%22%0A++++++++%7D%0A++++++++%2F%2F%5D%5D%3E%0A%3C%2Fscript%3E%0A++++%3C%21--+CMS+Page+Data+End+--%3E%0A++++%3C%21--+CMS+Temp+Version%3A+template-0000011311+baseline%3A+23+revision%3A+0+description%3A+CatHeader+Multi-Row+Menu+%28MASTER%29+%28REF+0000011309%29+published%3A+2014-02-13+15%3A22%3A03.468851+--%3E%0A%3Clink+rel%3D%22stylesheet%22+type%3D%22text%2Fcss%22+href%3D%22http%3A%2F%2Fts.stage.arcadiagroup.ltd.uk%2Fwcsstore%2FConsumerDirectStorefrontAssetStore%2Fimages%2Fcolors%2Fcolor7%2Fcms%2Ftemplates%2Fcategoryheader%2Ftemplate-0000011311%2Fcss%2Fdefault.css%22+%2F%3E%3Cstyle+type%3D%22text%2Fcss%22%3E%0A%2F*%3C%21%5BCDATA%5B*%2F%0A%0A%0A++++++++%3B%0A++++++++%3B%0A%0A%0A%2F*%5D%5D%3E*%2F%0A%3C%2Fstyle%3E%0A%0A++++%0A++%0A%0A++%0A++++%3Cdiv+id%3D%22CatHeaderMultiColumnMenu77316%22+class%3D%22CatHeaderMultiColumnMenuContent+default%22%3E%0A++++++%3Cdiv+id%3D%22mainContent%22%3E%0A++++++++%3Ch1%3EDresses%3C%2Fh1%3E%0A%0A++++++++%3Cdiv+id%3D%27description%27%3E%0A++++++++++%3Cp%3EShop+the+essential+dresses+of+the+season+online+at+Topshop.+From+bardot+necklines+to+wrap+styles+and+shirt+dresses+%26%238211%3B+we%26%238217%3Bve+got+trending+styles+to+go+AM+to+PM.+After-hours+dresses+are+given+a+cool+spin+for+%3Ca+style%3D%22text-decoration%3A+none%3B%22+href%3D%22%2Fen%2Ftsuk%2Fcategory%2Fclothing-427%2Fgoing-out-938%2FN-867Zdgl%3FNo%3D0%26amp%3BNrpp%3D20%26amp%3BsiteId%3D%252F12556%22%3Epartywear%3C%2Fa%3E+season+-+think+tulle+styles%2C+%3Ca+style%3D%22text-decoration%3A+none%3B%22+href%3D%22%2Fen%2Ftsuk%2Fcategory%2Fclothing-427%2Fdresses-442%2Fbodycon-dresses%2FN-85cZr2xZdgl%22%3Ebodycon+dresses%3C%2Fa%3E%26%23160%3Band+top-to-toe+sequins%2C+while+%3Ca+style%3D%22text-decoration%3A+none%3B%22+href%3D%22%2Fen%2Ftsuk%2Fcategory%2Fclothing-427%2Fdresses-442%2Fday-dresses%2FN-85cZ1y60Zdgl%22%3Eday+dresses%3C%2Fa%3E%26%23160%3Bcome+with+new+spring-inspired+florals+and+oversized+ruffles.+Love+them+all%3F+Check+out+our+%3Ca+style%3D%22text-decoration%3A+none%3B%22+href%3D%22%2Fen%2Ftsuk%2Fcategory%2Fclothing-427%2Fthe-dress-guide-5052314%2Fhome%22%3EDress+Guide%3C%2Fa%3E%26%23160%3Bto+find+your+perfect+fit.%3C%2Fp%3E%0A++++++++%3C%2Fdiv%3E%0A%0A++++++++%3Cdiv+id%3D%22columns%22%3E%0A++++++++++%3Cul+id%3D%27firstUl%27+class%3D%27colWidth4%27%3E%0A++++++++++++%3Cli+class%3D%27boldmenulinks+colourmenulinks%27+id%3D%27firstli1%27%3E%3Ca+href%3D%22%2Fen%2Ftsuk%2Fcategory%2Fclothing-427%2Fdresses-442%2Fbardot-dresses%2FN-85cZ226wZdgl%3FNrpp%3D20%26amp%3BsiteId%3D%2F12556%26amp%3Bcat1%3D203984%26amp%3Bcat2%3D208523%26amp%3Bintcmpid%3DW_HEADER_VIEWALLDRESSES_UK_BARDOT%22+title%3D%27Bardot+Dresses%27%3EBardot+Dresses%3C%2Fa%3E%3C%2Fli%3E%0A%0A++++++++++++%3Cli+class%3D%27boldmenulinks+colourmenulinks%27+id%3D%27firstli2%27%3E%3Ca+href%3D%22%2Fen%2Ftsuk%2Fcategory%2Fclothing-427%2Fdresses-442%2Fslip-dresses%2FN-85cZr32Zdgl%3FNrpp%3D20%26amp%3BsiteId%3D%2F12556%26amp%3Bcat1%3D203984%26amp%3Bcat2%3D208523%26amp%3Bintcmpid%3DW_HEADER_VIEWALLDRESSES_UK_SLIP_DRESSES%22+title%3D%27Slip+Dresses%27%3ESlip+Dresses%3C%2Fa%3E%3C%2Fli%3E%0A%0A++++++++++++%3Cli+class%3D%27boldmenulinks+colourmenulinks%27+id%3D%27firstli3%27%3E%3Ca+href%3D%22%2Fen%2Ftsuk%2Fcategory%2Fclothing-427%2Fdresses-442%2Fmaxi%2FN-85cZr7jZdgl%3FNrpp%3D20%26amp%3BsiteId%3D%2F12556%26amp%3Bcat1%3D203984%26amp%3Bcat2%3D208523%26amp%3Bintcmpid%3DW_HEADER_VIEWALLDRESSES_UK_MAXI%22+title%3D%27Maxi+Dresses%27%3EMaxi+Dresses%3C%2Fa%3E%3C%2Fli%3E%0A%0A++++++++++++%3Cli+class%3D%27boldmenulinks+colourmenulinks%27+id%3D%27firstli4%27%3E%3Ca+href%3D%22%2Fen%2Ftsuk%2Fcategory%2Fclothing-427%2Fdresses-442%2Fshirt-dresses%2FN-85cZr30Zdgl%3FNo%3D0%26amp%3BNrpp%3D20%26amp%3BsiteId%3D%252F12556%26amp%3Bintcmpid%3DW_HEADER_VIEWALLDRESSES_UK_SHIRT_DRESSES%22+title%3D%27Shirt+Dresses%27%3EShirt+Dresses%3C%2Fa%3E%3C%2Fli%3E%0A++++++++++%3C%2Ful%3E%0A%0A++++++++++%3Cul+id%3D%27secondUl%27+class%3D%27colWidth4%27%3E%0A++++++++++++%3Cli+class%3D%27boldmenulinks+colourmenulinks%27+id%3D%27secondli1%27%3E%3Ca+href%3D%22%2Fen%2Ftsuk%2Fcategory%2Fclothing-427%2Fdresses-442%2Fwrap-dresses%2FN-85cZ20tkZdgl%3FNo%3D0%26amp%3BNrpp%3D20%26amp%3BsiteId%3D%252F12556%26amp%3Bintcmpid%3DW_HEADER_VIEWALLDRESSES_UK_WRAP_DRESSES%22+title%3D%27Wrap+Dresses%27%3EWrap+Dresses%3C%2Fa%3E%3C%2Fli%3E%0A%0A++++++++++++%3Cli+class%3D%27boldmenulinks+colourmenulinks%27+id%3D%27secondli2%27%3E%3Ca+href%3D%22%2Fen%2Ftsuk%2Fcategory%2Fclothing-427%2Fdresses-442%2Fgoing-out-party-dresses%2FN-85cZ1y63Zdgl%3FNo%3D0%26amp%3BNrpp%3D20%26amp%3BsiteId%3D%252F12556%26amp%3Bintcmpid%3DW_HEADER_VIEWALLDRESSES_UK_GOING_OUT_DRESSES%22+title%3D%27Going+Out+Dresses%27%3EGoing+Out+Dresses%3C%2Fa%3E%3C%2Fli%3E%0A%0A++++++++++++%3Cli+class%3D%27boldmenulinks+colourmenulinks%27+id%3D%27secondli3%27%3E%3Ca+href%3D%22%2Fen%2Ftsuk%2Fcategory%2Fclothing-427%2Fdresses-442%2Fpinafore-dresses%2FN-85cZr2yZdgl%3FNo%3D0%26amp%3BNrpp%3D20%26amp%3BsiteId%3D%252F12556%26amp%3Bintcmpid%3DW_HEADER_VIEWALLDRESSES_UK_PINAFORE_DRESSES%22+title%3D%27Pinafore+Dresses%27%3EPinafore+Dresses%3C%2Fa%3E%3C%2Fli%3E%0A%0A++++++++++++%3Cli+class%3D%27boldmenulinks+colourmenulinks%27+id%3D%27secondli4%27%3E%3Ca+href%3D%22%2Fen%2Ftsuk%2Fcategory%2Fclothing-427%2Fdresses-442%2Fskater-dresses%2FN-85cZr31Zdgl%3FNo%3D0%26amp%3BNrpp%3D20%26amp%3BsiteId%3D%252F12556%26amp%3Bintcmpid%3DW_HEADER_VIEWALLDRESSES_UK_SKATER_DRESSES%22+title%3D%27Skater+Dresses%27%3ESkater+Dresses%3C%2Fa%3E%3C%2Fli%3E%0A++++++++++%3C%2Ful%3E%0A++++++++%3C%2Fdiv%3E%0A++++++%3C%2Fdiv%3E%0A%3C%2Fdiv%3E%3Cscript+type%3D%22text%2Fjavascript%22%3E%0A%2F%2F%3C%21%5BCDATA%5B%0A++++++++%0A++++++++var+_ArcQ+%3D+_ArcQ+%7C%7C+%5B%5D%3B%0A++++var+catHeaderToggle+%3D+function+%28%29+%7B%0A++++++++%0A++++%7D%3B%0A++++++++%0A++++_ArcQ.push%28%5BcatHeaderToggle%5D%29%3B%0A%2F%2F%5D%5D%3E%0A%3C%2Fscript%3E%0A++++%3C%21--Using+Master+Template%3A+%2Fcategoryheader%2Ftemplate-0000011309+--%3E%0A++%0A%0A'

describe('<PlpHeader />', () => {
  const renderComponent = buildComponentRender(shallowRender, PlpHeader)
  const initialProps = {
    title: 'Jeans',
    total: '492',
    brandCode: '123',
    bannerHTML: bannerHtml1,
    location: {
      pathname: '',
    },
  }

  describe('@renders', () => {
    const renderedComponent = renderComponent(initialProps)

    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })

    it('should displays the category title and search results', () => {
      const { wrapper } = renderedComponent
      expect(
        wrapper
          .find('.PlpHeader-title')
          .first()
          .text()
      ).toBe('Jeans')
      expect(
        wrapper
          .find('.PlpHeader-total')
          .first()
          .text()
      ).toBe('492')
      expect(wrapper.find('h1.screen-reader-text')).toHaveLength(0)
    })

    it('should handle category Header (Iframe)', () => {
      const { getTree, wrapper } = renderComponent({
        ...initialProps,
        isCategoryHeaderIframeEnabled: true,
      })
      expect(getTree()).toMatchSnapshot()
      expect(wrapper.find('h1.screen-reader-text')).toHaveLength(1)
    })

    it('should handle category Header (encodedHtml)', () => {
      const { getTree, wrapper } = renderComponent({
        ...initialProps,
        isCategoryHeaderEncodedEnabled: true,
      })
      expect(getTree()).toMatchSnapshot()
      expect(wrapper.find('h1.screen-reader-text')).toHaveLength(1)
    })

    it('should handle category default Banner', () => {
      const { getTree, wrapper } = renderComponent(initialProps)
      expect(getTree()).toMatchSnapshot()
      expect(wrapper.find('h1.screen-reader-text')).toHaveLength(0)
    })

    it('should displays the banner and not title when `showBanner` is `true', () => {
      const renderedComponent = renderComponent({
        ...initialProps,
        isMobile: false,
        isCategoryHeaderEncodedEnabled: true,
        bannerHTML: 'text',
      })
      const { wrapper } = renderedComponent
      expect(wrapper.find('.PlpHeader-banner').isEmpty()).toBe(false)
    })

    it('renders responsive category header', () => {
      const responsiveCatHeader = renderComponent({
        ...initialProps,
        catHeaderResponsiveCmsUrl: '/url/to/catheader/cms/template',
      })

      expect(responsiveCatHeader.getTree()).toMatchSnapshot()
    })
  })
})
