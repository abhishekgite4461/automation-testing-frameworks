import testComponentHelper from 'test/unit/helpers/test-component'
import { range } from 'ramda'

import products from '../../../../../../test/mocks/productMocks'
import { IMAGE_SIZES } from '../../../../constants/amplience'
import ProductList from '../ProductList'
import Product from '../../../common/Product/Product'
import Espot from '../../Espot/Espot'

describe('<ProductList />', () => {
  const renderComponent = testComponentHelper(ProductList.WrappedComponent)
  describe('@renders', () => {
    it('should display the correct number of products', () => {
      const grid = 1

      const { wrapper } = renderComponent({
        products,
        grid,
      })

      expect(wrapper.find(Product)).toHaveLength(20)
      expect(
        wrapper
          .find(Product)
          .first()
          .prop('sizes')
      ).toBe(IMAGE_SIZES.plp[grid])
    })

    it('adds content spots', () => {
      const espotsMock = [
        {
          id: 'productListItem1',
          position: 1,
        },
        {
          id: 'productListItem2',
          position: 3,
        },
        {
          id: 'productListItem3',
          position: 6,
        },
        {
          id: 'productListItem4',
          position: 25,
        },
      ]

      const { wrapper } = renderComponent({
        products,
        grid: 1,
        espots: espotsMock,
      })

      expect(wrapper.find(Espot)).toHaveLength(3)

      const getProductItem = (position) =>
        wrapper
          .find('.ProductList-products')
          .children()
          .at(position)

      expect(getProductItem(0).hasClass('Espot-productList')).toBe(true)

      expect(getProductItem(1).hasClass('Espot-productList')).toBe(false)
      expect(getProductItem(1).props().productId).toEqual(products[0].productId)

      expect(getProductItem(2).hasClass('Espot-productList')).toBe(true)

      expect(getProductItem(3).hasClass('Espot-productList')).toBe(false)
      expect(getProductItem(3).props().productId).toEqual(products[1].productId)

      expect(getProductItem(4).hasClass('Espot-productList')).toBe(false)
      expect(getProductItem(4).props().productId).toEqual(products[2].productId)

      expect(getProductItem(5).hasClass('Espot-productList')).toBe(true)

      expect(getProductItem(6).hasClass('Espot-productList')).toBe(false)
      expect(getProductItem(6).props().productId).toEqual(products[3].productId)
    })

    it('should display the loader when API is fetching products', () => {
      const { wrapper } = renderComponent({ products, isLoading: true })
      expect(wrapper.find('.ProductList-loader')).toHaveLength(1)
    })
  })

  describe('@lifecycle', () => {
    describe('shouldComponentUpdate', () => {
      const props = {
        products,
        grid: 1,
        children: undefined,
        espots: [],
      }
      const { instance } = renderComponent(props)

      it('should return false if props are not changed', () => {
        expect(instance.shouldComponentUpdate(props)).toBe(false)
      })

      it('should return true if props are changed', () => {
        expect(
          instance.shouldComponentUpdate({
            ...props,
            grid: 2,
          })
        ).toBe(true)
      })
    })
  })

  describe('@methods', () => {
    const initState = {
      totalProducts: 80,
      products: range(0, 27),
      grid: 3,
    }
    const { instance } = renderComponent()

    describe('prepareRowsForRendering()', () => {
      it('should return an array without the last product', () => {
        const state = Object.values({ ...initState, grid: 2 })
        const productList = instance.prepareRowsForRendering(...[...state])
        expect(productList.length).toEqual(26)
      })

      it('should return an array without the last two products', () => {
        const state = Object.values({ ...initState, products: range(0, 26) })
        const productList = instance.prepareRowsForRendering(...[...state])
        expect(productList.length).toEqual(24)
      })

      it('should return an array without the last three products', () => {
        const state = Object.values({ ...initState, grid: 4 })
        const productList = instance.prepareRowsForRendering(...[...state])
        expect(productList.length).toEqual(24)
      })

      it('should return an array with all products', () => {
        const state = Object.values({ ...initState, products: range(0, 80) })
        const productList = instance.prepareRowsForRendering(...[...state])
        expect(productList.length).toEqual(80)
      })
    })
  })
})
