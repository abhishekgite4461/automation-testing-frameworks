import React from 'react'
import PropTypes from 'prop-types'
import CMSCatframe from '../../common/CMSCatframe/CMSCatframe'
import SandBox from '../SandBox/SandBox'
import { fixCmsUrl } from '../../../lib/cms-utilities'
import cmsConsts from '../../../constants/cmsConsts'

const PlpHeader = (props, context) => {
  const {
    bannerHTML,
    total,
    title,
    isMobile,
    brandCode,
    location,
    isCategoryHeaderIframeEnabled,
    isCategoryHeaderEncodedEnabled,
    catHeaderResponsiveCmsUrl,
  } = props
  const { l } = context
  const replacementsHTML = bannerHTML.replace(
    /%3D%22http%3A%2F%2F/gi,
    '%3D%22https%3A%2F%2F'
  )
  const finalHtml = decodeURIComponent(replacementsHTML).replace(/\+/g, ' ')

  const defaultHeader = (
    <div className="PlpHeader">
      <h1>
        <span className="PlpHeader-title">{title}</span>
      </h1>
      <span className="PlpHeader-total PlpHeader-totalValue">{total}</span>
      <span className="PlpHeader-total PlpHeader-totalLabel">{l`results`}</span>
    </div>
  )

  const seoHiddenTitle = <h1 className="screen-reader-text">{title}</h1>
  let outputHeader

  if (!isMobile) {
    if (catHeaderResponsiveCmsUrl) {
      // Responsive CMS Solution

      const location = {
        pathname: fixCmsUrl(catHeaderResponsiveCmsUrl),
      }
      outputHeader = (
        <SandBox
          location={location}
          contentType={cmsConsts.ESPOT_CONTENT_TYPE}
          shouldGetContentOnFirstLoad
          isResponsiveCatHeader
          isInPageContent
        />
      )
    } else if (finalHtml && finalHtml !== '') {
      // Fallback Solution

      if (isCategoryHeaderIframeEnabled) {
        outputHeader = location.pathname !== '/search/' && (
          <div>
            {seoHiddenTitle}
            <CMSCatframe defaultHeight={250} />
          </div>
        )
      } else if (isCategoryHeaderEncodedEnabled) {
        outputHeader = (
          <div className="PlpHeader">
            {seoHiddenTitle}
            <div
              id="wrapper_content"
              className={`PlpHeader-banner PlpHeader-${brandCode} PlpHeader-banner--clearfix category-header`}
              // eslint-disable-next-line react/no-danger
              dangerouslySetInnerHTML={{ __html: finalHtml }}
            />
          </div>
        )
      }
    }
  }

  outputHeader = outputHeader || defaultHeader

  return <div>{outputHeader}</div>
}

PlpHeader.propTypes = {
  total: PropTypes.string,
  title: PropTypes.string,
  bannerHTML: PropTypes.string,
  isMobile: PropTypes.bool,
  brandCode: PropTypes.string.isRequired,
  location: PropTypes.object,
  isCategoryHeaderIframeEnabled: PropTypes.bool,
  isCategoryHeaderEncodedEnabled: PropTypes.bool,
  catHeaderResponsiveCmsUrl: PropTypes.string,
}

PlpHeader.contextTypes = {
  l: PropTypes.func,
}

PlpHeader.defaultProps = {
  bannerHTML: '',
  isMobile: false,
  location: {},
  catHeaderResponsiveCmsUrl: '',
}

export default PlpHeader
