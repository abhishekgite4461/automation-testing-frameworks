import {
  buildComponentRender,
  shallowRender,
} from '../../../../../../test/unit/helpers/test-component'

import Category from '../Category'
import { replaceUrl } from '../../../../lib/html-entities'
import { touchDetection } from '../../../../lib/viewHelper'

jest.mock('../../../../lib/viewHelper', () => ({
  touchDetection: jest.fn().mockReturnValue(false),
}))

describe('<Category />', () => {
  beforeEach(() => jest.clearAllMocks())

  const requiredProps = {
    category: {
      categoryId: '12345',
      columns: [
        {
          span: '0',
          subcategories: {
            0: {
              newIn: [
                { image: { span: 'footer' } },
                { label: 'something to buy' },
              ],
            },
          },
        },
        {
          span: '1',
          subcategories: {
            0: {
              jeans: [{ image: { span: 'footer' } }, { label: 'blue jeans' }],
              0: [{ image: { span: 1 } }, { label: 'red jeans' }],
            },
          },
        },
        {
          span: '2',
          subcategories: {
            0: {
              socks: [{ label: 'winter socks' }],
              jeans: [{ image: { url: '' } }],
            },
          },
        },
      ],
      url: 'http://www.topman.com',
    },
    apiEnvironment: 'prod',
  }

  const renderComponent = buildComponentRender(shallowRender, Category)

  describe('@renders', () => {
    beforeEach(() => jest.clearAllMocks())

    it('should render default state', () => {
      expect(renderComponent(requiredProps).getTree()).toMatchSnapshot()
    })

    it('should not render links for touch devices', () => {
      touchDetection.mockReturnValueOnce(true)
      expect(renderComponent(requiredProps).getTree()).toMatchSnapshot()
    })

    it('should render links for touch devices if sub category columns is empty', () => {
      touchDetection.mockReturnValueOnce(true)
      expect(
        renderComponent({
          ...requiredProps,
          category: { columns: [], label: 'category' },
        }).getTree()
      ).toMatchSnapshot()
    })

    it('should not render sub category if columns is empty', () => {
      expect(
        renderComponent({
          ...requiredProps,
          category: { columns: [], label: 'category' },
        }).getTree()
      ).toMatchSnapshot()
    })

    it('should render correct state and css classes when touchDetection is true', () => {
      touchDetection.mockReturnValueOnce(true)
      const { wrapper } = renderComponent(requiredProps)
      expect(wrapper.state().touchEnabled).toBe(true)
      expect(
        wrapper
          .find('li')
          .at(0)
          .hasClass('MegaNav-category--isTouch')
      ).toBe(true)
    })

    it('should render correct state and css classes when touchDetection is false', () => {
      touchDetection.mockReturnValueOnce(false)
      const { wrapper } = renderComponent(requiredProps)
      expect(wrapper.state().touchEnabled).toBe(false)
      expect(
        wrapper
          .find('li')
          .at(0)
          .hasClass('MegaNav-category--isNotTouch')
      ).toBe(true)
      expect(
        wrapper
          .find('.MegaNav-subNavWrapper')
          .at(0)
          .hasClass('MegaNav-subNavWrapper--isNotTouch')
      ).toBe(true)
    })

    it('should render without a className categoryId', () => {
      expect(
        renderComponent({ ...requiredProps, categoryId: '' }).getTree()
      ).toMatchSnapshot()
    })

    it('should render for touch device with no sub categories', () => {
      touchDetection.mockReturnValueOnce(true)

      const { getTree } = renderComponent({
        ...requiredProps,
        category: {
          categoryId: '12345',
          columns: [],
        },
      })
      expect(getTree()).toMatchSnapshot()
    })
  })

  describe('@Lifecycle', () => {
    describe('constructor', () => {
      beforeEach(() => jest.clearAllMocks())

      it('should set touchEnabled state to false', () => {
        const category = {
          columns: [],
        }
        const props = {
          ...requiredProps,
          category,
        }
        const { instance, wrapper } = renderComponent(props)
        instance.constructor(props)
        expect(wrapper.state('touchEnabled')).toBe(false)
      })
    })

    describe('shouldComponentUpdate', () => {
      it('should return return false when isOpen does not changed', () => {
        const { instance } = renderComponent({
          ...requiredProps,
          isOpen: false,
        })
        expect(instance.shouldComponentUpdate({ isOpen: false })).toBe(false)
      })

      it('should return return true when isOpen does change', () => {
        const { instance } = renderComponent({
          ...requiredProps,
          isOpen: true,
        })
        expect(instance.shouldComponentUpdate({ isOpen: false })).toBe(true)
      })
    })

    describe('componentWillReceiveProps', () => {
      describe(`receiving 'isOpen' prop`, () => {
        it(`updates 'show' state if different`, () => {
          const { instance } = renderComponent(requiredProps)
          expect(instance.state.show).toBeFalsy()
          instance.componentWillReceiveProps({
            ...instance.props,
            isOpen: true,
          })
          expect(instance.state.show).toBeTruthy()
        })

        it(`does not update 'show' state if value unchanged`, () => {
          const { instance } = renderComponent(requiredProps)
          expect(instance.state.show).toBeFalsy()
          instance.componentWillReceiveProps({
            ...instance.props,
            isOpen: false,
          })
          expect(instance.state.show).toBeFalsy()
        })
      })
    })
  })

  describe('@Methods', () => {
    beforeEach(() => jest.clearAllMocks())

    const { instance } = renderComponent(requiredProps)

    describe('getUrl()', () => {
      beforeEach(() => jest.clearAllMocks())

      it('should not edit seo url in Production', () => {
        const props = instance.props
        const expected = 'http://www.topman.com'
        expect(instance.getUrl(props)).toEqual(expected)
      })

      it('should edit seo url not in Production', () => {
        const { instance } = renderComponent({
          ...requiredProps,
          apiEnvironment: '',
        })
        const props = instance.props
        const expected = replaceUrl('http://www.topman.com')
        expect(instance.getUrl(props)).toEqual(expected)
      })
    })
  })

  describe('@Events', () => {
    describe('mouseenter', () => {
      beforeEach(() => {
        jest.clearAllMocks()
      })

      it('calls onSetOpen callback if has sub categories', () => {
        const { wrapper, instance } = renderComponent({
          ...requiredProps,
          onSetOpen: jest.fn(),
        })
        expect(instance.props.onSetOpen).toHaveBeenCalledTimes(0)
        wrapper.find('li').simulate('mouseenter')
        expect(instance.props.onSetOpen).toHaveBeenCalledTimes(1)
        expect(instance.props.onSetOpen).toHaveBeenCalledWith(true)
      })

      it('not calls onSetOpen callback if has not sub categories', () => {
        const { wrapper, instance } = renderComponent({
          ...requiredProps,
          category: {
            columns: [],
          },
          onSetOpen: jest.fn(),
        })
        wrapper.find('li').simulate('mouseenter')
        expect(instance.props.onSetOpen).toHaveBeenCalledTimes(0)
      })
    })

    describe('touchstart', () => {
      beforeEach(() => {
        jest.clearAllMocks()
      })

      it('calls onSetOpen callback if has sub categories', () => {
        const { wrapper, instance } = renderComponent({
          ...requiredProps,
          onSetOpen: jest.fn(),
        })
        expect(instance.props.onSetOpen).toHaveBeenCalledTimes(0)
        wrapper.find('li').simulate('touchstart')
        expect(instance.props.onSetOpen).toHaveBeenCalledTimes(1)
        expect(instance.props.onSetOpen).toHaveBeenCalledWith(true)
      })

      it('not calls onSetOpen callback if has not sub categories', () => {
        const { wrapper, instance } = renderComponent({
          ...requiredProps,
          category: {
            columns: [],
          },
          onSetOpen: jest.fn(),
        })
        wrapper.find('li').simulate('touchstart')
        expect(instance.props.onSetOpen).toHaveBeenCalledTimes(0)
      })
    })

    describe('mouseleave', () => {
      beforeEach(() => {
        jest.clearAllMocks()
        touchDetection.mockReturnValueOnce(true)
      })

      it('calls onSetOpen(false) callback if has sub categories', () => {
        const { wrapper, instance } = renderComponent({
          ...requiredProps,
          onSetOpen: jest.fn(),
        })
        expect(instance.props.onSetOpen).toHaveBeenCalledTimes(0)
        wrapper.find('li').simulate('mouseleave')
        expect(instance.props.onSetOpen).toHaveBeenCalledWith(false)
      })

      it('not calls onSetOpen callback if has not sub categories', () => {
        const { wrapper, instance } = renderComponent({
          ...requiredProps,
          category: {
            columns: [],
          },
          onSetOpen: jest.fn(),
        })
        wrapper.find('li').simulate('mouseleave')
        expect(instance.props.onSetOpen).toHaveBeenCalledTimes(0)
      })
    })
  })
})
