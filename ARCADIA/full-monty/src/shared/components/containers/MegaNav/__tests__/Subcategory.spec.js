import {
  buildComponentRender,
  shallowRender,
} from '../../../../../../test/unit/helpers/test-component'

import Subcategory from '../Subcategory'

describe('<Subcategory />', () => {
  const requiredProps = {
    hideMegaNav: jest.fn(),
    subcategory: {
      redirectionUrl: '',
      url: 'http://www.topman.com/category/jeans/blue-jeans.html',
      label: 'This is a label',
      paddingTop: 20,
      image: {
        span: 1,
        url: 'http://topman.com',
      },
    },
    apiEnvironment: 'prod',
  }

  const renderComponent = buildComponentRender(
    shallowRender,
    Subcategory.WrappedComponent
  )

  describe('@renders', () => {
    it('should render default state', () => {
      expect(renderComponent(requiredProps).getTree()).toMatchSnapshot()
    })

    it('should render image content with <Link> tags', () => {
      expect(renderComponent(requiredProps).getTree()).toMatchSnapshot()
    })

    it('should render text content with <Link> tags', () => {
      expect(
        renderComponent({
          ...requiredProps,
          subcategory: {
            redirectionUrl: '',
            url: 'http://www.topman.com/category/jeans/blue-jeans.html',
            label: 'This is a label',
            paddingTop: 20,
          },
        }).getTree()
      ).toMatchSnapshot()
    })

    it('should render text content with <a> tags', () => {
      expect(
        renderComponent({
          ...requiredProps,
          subcategory: {
            redirectionUrl: '/blog/',
            url: 'http://www.topman.com/category/jeans/blue-jeans.html',
            label: 'This is a label',
            paddingTop: 20,
          },
        }).getTree()
      ).toMatchSnapshot()
    })
  })

  describe('@Events', () => {
    const { wrapper } = renderComponent(requiredProps)
    describe('click', () => {
      wrapper.find('Link').simulate('click', { button: 0 })
      it('closes the megaNav when clicking on a <Link>', () => {
        expect(requiredProps.hideMegaNav).toHaveBeenCalled()
      })
    })

    describe('touchEnd', () => {
      wrapper.find('Link').simulate('touchEnd', { button: 0 })
      it(
        'closes the meganav with a delay of 200 milliseconds',
        async (done) => {
          jest.setTimeout(200)
          expect(requiredProps.hideMegaNav).toHaveBeenCalled()
          done()
        },
        200
      )
    })
  })

  describe('@Methods', () => {
    describe('hasFooterImage()', () => {
      it('should return false if an image is not available', () => {
        const { instance } = renderComponent({
          ...requiredProps,
          subcategory: {},
        })
        const subcategory = instance.props.subcategory
        expect(instance.hasFooterImage(subcategory)).toBeFalsy()
      })

      it('should return false if an span is not available', () => {
        const { instance } = renderComponent({
          ...requiredProps,
          subcategory: { image: {} },
        })
        const subcategory = instance.props.subcategory
        expect(instance.hasFooterImage(subcategory)).toBeFalsy()
      })

      it('should return false if image span is not equal to "footer"', () => {
        const { instance } = renderComponent(requiredProps)
        const subcategory = instance.props.subcategory
        expect(instance.hasFooterImage(subcategory)).toBeFalsy()
      })

      it('should return true if image span is equal to "footer"', () => {
        const { instance } = renderComponent({
          ...requiredProps,
          subcategory: { image: { span: 'footer' } },
        })
        const subcategory = instance.props.subcategory
        expect(instance.hasFooterImage(subcategory)).toBeTruthy()
      })
    })

    describe('getContentType()', () => {
      it('should return a string "image"', () => {
        const { instance } = renderComponent(requiredProps)
        const expected = 'image'
        expect(instance.getContentType(instance.props.subcategory)).toEqual(
          expected
        )
      })

      it('should return a string "text"', () => {
        const { instance } = renderComponent({
          ...requiredProps,
          subcategory: {},
        })
        const expected = 'text'
        expect(instance.getContentType(instance.props.subcategory)).toEqual(
          expected
        )
      })
    })

    describe('getContent()', () => {
      it('should return null if content type is empty string', () => {
        const { instance } = renderComponent(requiredProps)
        expect(instance.getContent('', instance.props.subcategory)).toBeNull()
      })
    })

    describe('getLinkProps()', () => {
      it('should return props for url links', () => {
        const { instance } = renderComponent(requiredProps)
        const props = [
          false,
          instance.props.subcategory,
          instance.props.hideMegaNav,
        ]
        expect(instance.getLinkProps(...props)).toHaveProperty(
          ['to'],
          'http://www.topman.com/category/jeans/blue-jeans.html'
        )
      })

      it('should return props for redirect links', () => {
        const newProps = {
          ...requiredProps,
          subcategory: {
            ...requiredProps.subcategory,
            redirectionUrl: '/blog/',
          },
        }
        const { instance } = renderComponent(newProps)
        const props = [
          true,
          instance.props.subcategory,
          instance.props.hideMegaNav,
        ]
        expect(instance.getLinkProps(...props)).toHaveProperty(
          ['href'],
          '/blog/'
        )
      })
    })
  })
})
