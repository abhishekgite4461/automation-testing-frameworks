import {
  buildComponentRender,
  shallowRender,
} from '../../../../../../test/unit/helpers/test-component'

import { clone } from 'ramda'
import Footer from '../Footer'

describe('<Footer />', () => {
  const requiredProps = {
    category: {
      columns: [
        {
          subcategories: {
            0: {
              newIn: [
                { image: { span: 'footer' } },
                { label: 'something to buy' },
              ],
            },
          },
        },
        {
          subcategories: {
            0: {
              jeans: [{ image: { span: 'footer' } }, { label: 'blue jeans' }],
              0: [{ image: { span: 1 } }, { label: 'red jeans' }],
            },
          },
        },
        {
          subcategories: {
            0: {
              socks: [{ label: 'winter socks' }],
              jeans: [{ image: { url: '' } }],
            },
          },
        },
      ],
      url: 'http://www.topman.com',
    },
    isDesktop: true,
    apiEnvironment: 'prod',
  }

  const renderComponent = buildComponentRender(shallowRender, Footer)

  describe('@renders', () => {
    it('should render with two footer columns', () => {
      expect(renderComponent(requiredProps).getTree()).toMatchSnapshot()
    })

    it('should render with no footer columns', () => {
      const props = clone(requiredProps)
      props.category.columns[0].subcategories['0'].newIn[0].image.span = 1
      props.category.columns[1].subcategories['0'].jeans[0].image.span = 1
      expect(renderComponent(props).getTree()).toMatchSnapshot()
    })
  })

  describe('@Methods', () => {
    const { instance } = renderComponent(requiredProps)
    const filteredColoums = [
      {
        subcategories: {
          0: {
            newIn: [
              { image: { span: 'footer' } },
              { label: 'something to buy' },
            ],
          },
        },
      },
      {
        subcategories: {
          0: {
            jeans: [{ image: { span: 'footer' } }, { label: 'blue jeans' }],
            0: [{ image: { span: 1 } }, { label: 'red jeans' }],
          },
        },
      },
    ]

    const filteredColoumsRemovedSubCategories = [
      {
        subcategories: {
          0: {
            newIn: [
              { image: { span: 'footer' } },
              { label: 'something to buy' },
            ],
          },
        },
      },
      {
        subcategories: {
          0: {
            jeans: [{ image: { span: 'footer' } }, { label: 'blue jeans' }],
          },
        },
      },
    ]

    describe('filterColumns()', () => {
      it('should remove columns without a span equal footer', () => {
        const category = instance.props.category
        expect(instance.filterColumns(category)).toEqual(filteredColoums)
      })
    })

    describe('removeSubCategoryWithoutFooter()', () => {
      it('should remove sub categories with footer span equal footer', () => {
        expect(
          instance.removeSubCategoryWithoutFooter(filteredColoums)
        ).toEqual(filteredColoumsRemovedSubCategories)
      })
    })

    describe('filterSubCategoriesWithoutFooter()', () => {
      it('should filter subcategories which does not have a footer image', () => {
        const { instance } = renderComponent(requiredProps)
        const expected = [
          {
            subcategories: {
              0: {
                // eslint-disable-next-line no-useless-computed-key
                ['']: [{ image: { span: 2 } }],
              },
            },
          },
          {
            subcategories: {
              0: {
                // eslint-disable-next-line no-useless-computed-key
                ['']: [{ image: { span: 2 } }],
              },
            },
          },
        ]

        expect(
          instance.filterSubCategoriesWithoutFooter(
            filteredColoumsRemovedSubCategories
          )
        ).toEqual(expected)
      })
    })

    describe('hasFooterImage()', () => {
      it('should return true if object has a footer image', () => {
        const props =
          instance.props.category.columns[0].subcategories[0].newIn[0]
        expect(instance.hasFooterImage(props)).toBeTruthy()
      })

      it('should return false if object does not have a footer image', () => {
        const props =
          instance.props.category.columns[0].subcategories[0].newIn[1]
        expect(instance.hasFooterImage(props)).toBeFalsy()
      })

      it('should return false if image.span does not equal footer', () => {
        const props =
          instance.props.category.columns[1].subcategories[0].jeans[1]
        expect(instance.hasFooterImage(props)).toBeFalsy()
      })

      it('should return false if image.span does not exists', () => {
        const props =
          instance.props.category.columns[2].subcategories[0].jeans[0]
        expect(instance.hasFooterImage(props)).toBeFalsy()
      })
    })

    describe('getFooterColumns()', () => {
      it('should return an empty array if columns is empty', () => {
        const props = instance.props.category
        const expected = []
        expect(instance.getFooterColumns({ ...props, columns: [] })).toEqual(
          expected
        )
      })
    })
  })
})
