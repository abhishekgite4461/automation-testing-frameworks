import {
  buildComponentRender,
  shallowRender,
} from '../../../../../../test/unit/helpers/test-component'

import Column from '../Column'

describe('<Column />', () => {
  const requiredProps = {
    category: {
      columns: [{ span: 1 }, { span: 2 }, { span: 3 }, { span: 4 }],
    },
    column: {
      span: 4,
      subcategories: [
        {
          '': [
            {
              image: { span: 2 },
            },
          ],
        },
      ],
    },
  }

  const renderComponent = buildComponentRender(shallowRender, Column)

  describe('@renders', () => {
    it('should render default state', () => {
      expect(renderComponent(requiredProps).getTree()).toMatchSnapshot()
    })

    it('should not render default state', () => {
      expect(
        renderComponent({ ...requiredProps, category: {} }).getTree()
      ).toMatchSnapshot()
    })
  })

  describe('@Methods', () => {
    describe('noColumns()', () => {
      it('should return false if category provides a list of columns', () => {
        const { instance } = renderComponent(requiredProps)
        const category = instance.props.category
        expect(instance.noColumns(category)).toBeFalsy()
      })

      it('should return true if category does not provide a list of columns', () => {
        const { instance } = renderComponent(requiredProps)
        const category = {}
        expect(instance.noColumns(category)).toBeTruthy()
      })
    })

    describe('getColumnSpan()', () => {
      it('should get and return a column width from column object', () => {
        const { instance } = renderComponent(requiredProps)
        const column = instance.props.column
        const lensImagePath = instance.state.lensImagePath
        expect(instance.getColumnSpan(column, lensImagePath)).toEqual(2)
      })

      it('should return 1 column if column does not supply an image', () => {
        const { instance } = renderComponent({ ...requiredProps, column: {} })
        const column = instance.props.column
        const lensImagePath = instance.state.lensImagePath
        expect(instance.getColumnSpan(column, lensImagePath)).toEqual(1)
      })
    })

    describe('getColumnWidth()', () => {
      it('should return 25% for a 1 column width', () => {
        const { instance } = renderComponent(requiredProps)
        const args = 1
        const expected = '25%'
        expect(instance.getColumnWidth(args)).toEqual(expected)
      })

      it('should return 50% for a 2 column width', () => {
        const { instance } = renderComponent(requiredProps)
        const args = 2
        const expected = '50%'
        expect(instance.getColumnWidth(args)).toEqual(expected)
      })

      it('should return 75% for a 3 column width', () => {
        const { instance } = renderComponent(requiredProps)
        const args = 3
        const expected = '75%'
        expect(instance.getColumnWidth(args)).toEqual(expected)
      })

      it('should return 100% for a 4 column width', () => {
        const { instance } = renderComponent(requiredProps)
        const args = 4
        const expected = '100%'
        expect(instance.getColumnWidth(args)).toEqual(expected)
      })
    })

    describe('isLastColumn()', () => {
      it('should return false if it is not the last column', () => {
        const { instance } = renderComponent(requiredProps)
        const column = { span: 1 }
        const category = instance.props.category
        expect(instance.isLastColumn(category, column)).toBeFalsy()
      })

      it('should return true if it is the last column', () => {
        const { instance } = renderComponent(requiredProps)
        const column = instance.props.column
        const category = instance.props.category
        expect(instance.isLastColumn(category, column)).toBeTruthy()
      })
    })
  })
})
