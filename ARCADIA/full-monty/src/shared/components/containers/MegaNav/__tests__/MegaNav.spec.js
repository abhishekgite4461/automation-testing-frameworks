import {
  buildComponentRender,
  shallowRender,
  withStore,
} from '../../../../../../test/unit/helpers/test-component'
import { mockStoreCreator } from '../../../../../../test/unit/helpers/get-redux-mock-store'
import { compose } from 'ramda'
import MegaNav from '../MegaNav'
import Category from '../Category'

describe('<MegaNav />', () => {
  const requiredProps = {
    megaNavCategories: [
      {
        categoryId: '208491',
        label: 'New In',
        seoUrl:
          'http://www.topshop.com/en/tsuk/category/new-in-this-week-2169932',
        isHidden: false,
        columns: [
          {
            subcategories: [
              {
                categoryId: '277012',
                paddingTop: 0,
                label: 'New In Fashion',
                seoUrl:
                  'http://www.topshop.com/en/tsuk/category/new-in-this-week-2169932/new-in-fashion-6367514',
              },
            ],
          },
        ],
      },
    ],
    getMegaNav: jest.fn(),
    megaNavSelectedCategory: false,
    setMegaNavSelectedCategory: jest.fn(),
  }
  const renderComponent = buildComponentRender(
    shallowRender,
    MegaNav.WrappedComponent
  )

  describe('@renders', () => {
    it('should render categories with correct props', () => {
      const categories = renderComponent(requiredProps).wrapper.find(Category)
      expect(categories).toHaveLength(requiredProps.megaNavCategories.length)
      expect(categories.at(0).prop('category')).toEqual(
        requiredProps.megaNavCategories[0]
      )
    })

    it('should not render categorys if not available default state', () => {
      expect(
        renderComponent({
          ...requiredProps,
          megaNavCategories: [],
        }).wrapper.find(Category)
      ).toHaveLength(0)
    })

    describe('`MegaNav`', () => {
      it('should render with className', () => {
        expect(
          renderComponent({
            ...requiredProps,
            className: 'someClassName',
          }).wrapper.hasClass('someClassName')
        ).toBe(true)
      })
    })
  })

  describe('@methods', () => {
    describe('isOpen', () => {
      it('returns true if category id matches', () => {
        const { instance } = renderComponent({
          ...requiredProps,
          megaNavSelectedCategory: 1,
        })
        const categoryId = 1
        const category = {
          categoryId,
        }

        expect(instance.isOpen(category)).toEqual(true)
      })
      it('returns false if category does not match', () => {
        const { instance } = renderComponent({
          ...requiredProps,
          megaNavSelectedCategory: 2,
        })

        const category = {
          categoryId: 1,
        }
        expect(instance.isOpen(category)).toEqual(false)
      })
    })
    describe('onSetOpen', () => {
      it('calls setMegaNavSelectedCategory with category id', () => {
        const { instance } = renderComponent({
          ...requiredProps,
          megaNavSelectedCategory: 2,
        })
        const categoryId = 1
        const category = {
          categoryId,
        }

        instance.onSetOpen(category)(true)
        expect(requiredProps.setMegaNavSelectedCategory).toHaveBeenCalledWith(
          categoryId
        )
      })
      it('calls setMegaNavSelectedCategory with empty string', () => {
        const { instance } = renderComponent({
          ...requiredProps,
          megaNavSelectedCategory: 1,
        })
        const categoryId = 1
        const category = {
          categoryId,
        }

        instance.onSetOpen(category)(false)
        expect(requiredProps.setMegaNavSelectedCategory).toHaveBeenCalledWith(
          ''
        )
      })
    })
  })

  describe('@lifecycle', () => {
    describe('onComponenDidMount', () => {
      it('should not dispatch getMegaNav action if megaNav has already been fetched', () => {
        const { instance } = renderComponent(requiredProps)

        instance.componentDidMount()
        expect(instance.props.getMegaNav).not.toHaveBeenCalled()
      })
      it('should dispatch getMegaNav action if is empty', () => {
        const { instance } = renderComponent({
          ...requiredProps,
          megaNavCategories: [],
        })

        instance.componentDidMount()
        expect(instance.props.getMegaNav).toHaveBeenCalledTimes(1)
      })
    })
  })

  describe('@connected component', () => {
    const mockMegaNav = {
      categories: requiredProps.megaNavCategories,
    }
    const initialState = {
      navigation: {
        megaNav: mockMegaNav,
      },
      config: {
        brandName: 'topshop',
      },
      viewport: {
        media: 'desktop',
      },
      debug: {
        environment: 'stage',
      },
    }
    const store = mockStoreCreator(initialState)
    const state = store.getState()
    const render = compose(
      shallowRender,
      withStore(state)
    )
    const renderComponent = buildComponentRender(render, MegaNav)
    const { instance } = renderComponent()

    it('should wrap `MegaNav` component', () => {
      expect(MegaNav.WrappedComponent.name).toBe('MegaNav')
    })

    describe('`mapStateToProps`', () => {
      it('should `megaNav` from state', () => {
        const prop = instance.stateProps.megaNavCategories
        expect(prop).toEqual(mockMegaNav.categories)
      })
    })
  })
})
