import PropTypes from 'prop-types'
import React from 'react'
import { isNil, lensPath, view, equals, type } from 'ramda'
import classnames from 'classnames'

// components
import Content from './Content'

class Column extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      lensImagePath: lensPath(['subcategories', 0, '', 0, 'image', 'span']),
    }
  }
  static propTypes = {
    category: PropTypes.object,
    column: PropTypes.object,
    hideMegaNav: PropTypes.func.isRequired,
  }

  static defaultProps = {
    category: {},
    column: {
      subcategories: [],
    },
  }

  noColumns = (category) =>
    isNil(category.columns) || category.columns.length <= 0

  getColumnSpan = (column, lensImagePath) =>
    isNil(view(lensImagePath, column)) ? 1 : view(lensImagePath, column)

  getColumnWidth = (columnCount) => {
    const NUMBER_OF_COLUMNS = 4
    return `${(columnCount / NUMBER_OF_COLUMNS) * 100}%`
  }

  isLastColumn = (category, column) => {
    if (category.columns) {
      return equals(Number(column.span), category.columns.length)
    }
  }

  getColumnClassNames = (lastColumn) =>
    classnames('MegaNav-column', { 'MegaNav-lastColumn': lastColumn })

  render() {
    const {
      props: { category, column, hideMegaNav },
      state: { lensImagePath },
      noColumns,
      isLastColumn,
      getColumnSpan,
      getColumnWidth,
      getColumnClassNames,
    } = this

    const columnSpan = getColumnSpan(column, lensImagePath)
    const lastColumn = isLastColumn(category, column)

    // if category does not supply columns then do not render
    if (noColumns(category)) {
      return null
    }

    return (
      <div
        className={getColumnClassNames(lastColumn)}
        style={{
          width: getColumnWidth(columnSpan),
        }}
      >
        {
          // NOTE:
          // column.subcategories is passed as two prop types Array and String
          //  - String is rendered as an empty column
          //  - Array should render subcategory links
        }
        {type(column.subcategories) === 'Array' &&
          column.subcategories.map((block) => {
            return Object.keys(block).map((blockName, idx) => {
              const arraySubcategories = block[blockName]
              return (
                <Content
                  // eslint-disable-next-line react/no-array-index-key
                  key={`content_${idx}`}
                  arraySubcategories={arraySubcategories}
                  blockName={blockName}
                  hideMegaNav={hideMegaNav}
                />
              )
            })
          })}
      </div>
    )
  }
}

export default Column
