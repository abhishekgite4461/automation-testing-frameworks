/* Subcategory
 *
 *  Renders 'text' or 'image' links
 *
 * */

import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import classnames from 'classnames'
import { has, isEmpty } from 'ramda'
import { heDecode } from '../../../lib/html-entities'

import ImageContainer from './ImageContainer'

const CONTENT_TYPE = {
  TEXT: 'text',
  IMAGE: 'image',
}

@connect(
  (state) => ({
    apiEnvironment: state.debug.environment,
  }),
  null
)
export default class Subcategory extends React.Component {
  static propTypes = {
    subcategory: PropTypes.object,
    apiEnvironment: PropTypes.string,
    hideMegaNav: PropTypes.func,
  }

  hasFooterImage = (subcategory) => {
    const hasImage = has('image')
    const hasSpan = has('span')

    return (
      hasImage(subcategory) &&
      hasSpan(subcategory.image) &&
      subcategory.image.span === 'footer'
    )
  }

  getContentType = (subcategory) => {
    const hasImage = has('image')
    return hasImage(subcategory) ? CONTENT_TYPE.IMAGE : CONTENT_TYPE.TEXT
  }

  getItemClassNames = (subcategory) =>
    classnames('MegaNav-subcategoryItem', {
      [`category_${subcategory.categoryId}`]: subcategory.categoryId,
    })

  getItemLinkClassNames = (subcategory) =>
    classnames('MegaNav-subcategoryItemLink', {
      'MegaNav-subcategoryItemLink--bold': subcategory.bold,
    })

  getStyles = (subcategory) => {
    // paddingTop must be a number and not a string
    const paddingTop = Number(subcategory.paddingTop) || 0
    return { paddingTop }
  }

  getContent = (contentType, subcategory) => {
    switch (contentType) {
      case CONTENT_TYPE.TEXT: {
        return (
          <div className="MegaNav-ItemContainer">
            <span className="MegaNav-subcategoryItemLabel">
              {heDecode(subcategory.label)}
            </span>
          </div>
        )
      }

      case CONTENT_TYPE.IMAGE: {
        return (
          <ImageContainer
            subcategory={subcategory}
            className="MegaNav-imageContainer"
          />
        )
      }

      default:
        return null
    }
  }

  getLinkProps = (REDIRECT_LINK, subcategory, hideMegaNav) => {
    const defaultParams = {
      className: this.getItemLinkClassNames(subcategory),
      title: heDecode(subcategory.label),
      style: this.getStyles(subcategory),
    }

    return REDIRECT_LINK
      ? {
          ...defaultParams,
          href: subcategory.redirectionUrl,
        }
      : {
          ...defaultParams,
          to: subcategory.url,
          onClick: hideMegaNav,
          onTouchEnd: () => setTimeout(hideMegaNav, 200),
        }
  }

  render() {
    const {
      props: { subcategory, hideMegaNav },
      hasFooterImage,
      getContentType,
      getItemClassNames,
      getLinkProps,
      getContent,
    } = this

    if (hasFooterImage(subcategory)) {
      return null
    }

    const REDIRECT_LINK = !isEmpty(subcategory.redirectionUrl)
    const contentType = getContentType(subcategory)

    return (
      <li
        key={subcategory.categoryId}
        className={getItemClassNames(subcategory)}
      >
        {REDIRECT_LINK ? (
          <a {...getLinkProps(REDIRECT_LINK, subcategory, hideMegaNav)}>
            {getContent(contentType, subcategory)}
          </a>
        ) : (
          <Link {...getLinkProps(REDIRECT_LINK, subcategory, hideMegaNav)}>
            {getContent(contentType, subcategory)}
          </Link>
        )}
      </li>
    )
  }
}
