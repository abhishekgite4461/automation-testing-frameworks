import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'

import Category from './Category'
import { isDesktop } from '../../../selectors/viewportSelectors'
import {
  getMegaNavSelectedCategory,
  getMegaNavCategoriesFilteredColumns,
} from '../../../selectors/navigationSelectors'
import {
  getMegaNav,
  setMegaNavSelectedCategory,
} from '../../../actions/common/navigationActions'

const mapStateToProps = (state) => ({
  apiEnvironment: state.debug.environment,
  megaNav: state.navigation.megaNav,
  isDesktop: isDesktop(state),
  megaNavSelectedCategory: getMegaNavSelectedCategory(state),
  megaNavCategories: getMegaNavCategoriesFilteredColumns(state),
})

const mapDispatchToProps = {
  getMegaNav,
  setMegaNavSelectedCategory,
}

class MegaNav extends Component {
  static propTypes = {
    megaNavCategories: PropTypes.array.isRequired,
    className: PropTypes.string,
    apiEnvironment: PropTypes.string,
    getMegaNav: PropTypes.func,
    setMegaNavSelectedCategory: PropTypes.func.isRequired,
    megaNavSelectedCategory: PropTypes.string.isRequired,
  }
  static defaultProps = {
    className: '',
    apiEnvironment: 'prod',
  }

  componentDidMount() {
    const { megaNavCategories, getMegaNav } = this.props
    if (megaNavCategories.length === 0) getMegaNav()
  }

  isOpen = (category) =>
    category.categoryId === this.props.megaNavSelectedCategory

  onSetOpen = (category) => (open) => {
    const { megaNavSelectedCategory, setMegaNavSelectedCategory } = this.props
    if (open && megaNavSelectedCategory !== category.categoryId) {
      setMegaNavSelectedCategory(category.categoryId)
    } else if (!open && megaNavSelectedCategory === category.categoryId) {
      setMegaNavSelectedCategory('')
    }
  }

  render() {
    const { megaNavCategories, className, apiEnvironment } = this.props

    return (
      <div className={`MegaNav ${className || ''}`}>
        <ul className="MegaNav-categories">
          {megaNavCategories.map((category) => (
            <Category
              key={category.categoryId}
              category={category}
              apiEnvironment={apiEnvironment}
              onSetOpen={this.onSetOpen(category)}
              isOpen={this.isOpen(category)}
            />
          ))}
        </ul>
      </div>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MegaNav)
