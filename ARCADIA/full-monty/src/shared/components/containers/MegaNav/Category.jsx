import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import classnames from 'classnames'
import { isEmpty } from 'ramda'
import { heDecode, replaceUrl } from '../../../lib/html-entities'
import { touchDetection } from '../../../lib/viewHelper'

import Column from './Column'
import Footer from './Footer'

export default class Category extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      show: props.isOpen,
      touchEnabled: touchDetection(),
    }
  }

  static propTypes = {
    category: PropTypes.object,
    isOpen: PropTypes.bool,
    onSetOpen: PropTypes.func,
  }

  static defaultProps = {
    category: {
      column: [],
    },
    isOpen: false,
    onSetOpen: () => {},
  }

  shouldComponentUpdate(nextProps) {
    return this.props.isOpen !== nextProps.isOpen
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.isOpen !== nextProps.isOpen &&
      nextProps.isOpen !== this.state.show
    ) {
      this.setState({ show: nextProps.isOpen })
    }
  }

  getUrl = ({ apiEnvironment, category }) =>
    apiEnvironment === 'prod' ? category.url : replaceUrl(category.url)

  showMegaNavSubNavWrapper = () => {
    const { category, onSetOpen } = this.props
    if (!isEmpty(category.columns)) {
      onSetOpen(true)
    }
  }

  hideMegaNavSubNavWrapper = () => {
    const { category, onSetOpen } = this.props
    if (!isEmpty(category.columns)) {
      onSetOpen(false)
    }
  }

  render() {
    const {
      props,
      props: { category },
      getUrl,
      hideMegaNavSubNavWrapper,
      showMegaNavSubNavWrapper,
    } = this

    const { touchEnabled, show } = this.state
    const classNames = classnames('MegaNav-category', {
      [`category_${category.categoryId}`]: category.categoryId,
      'MegaNav-category--isNotTouch': !touchEnabled,
      'MegaNav-category--isTouch': touchEnabled,
      'MegaNav-category--isShown': show,
    })

    const subNavWrapperClassNames = classnames('MegaNav-subNavWrapper', {
      'MegaNav-subNavWrapper--isNotTouch': !touchEnabled,
      'MegaNav-subNavWrapper--isShown': show,
    })

    const label = heDecode(category.label)

    return (
      <li
        key={category.categoryId}
        className={classNames}
        onTouchStart={showMegaNavSubNavWrapper}
        onMouseEnter={showMegaNavSubNavWrapper}
        onMouseLeave={hideMegaNavSubNavWrapper}
      >
        {touchEnabled && !isEmpty(category.columns) ? (
          <span
            className="MegaNav-categoryLink"
            title={label}
            style={{ cursor: 'pointer' }}
          >
            {label}
          </span>
        ) : (
          <Link
            className="MegaNav-categoryLink"
            to={getUrl(props)}
            title={label}
            onClick={hideMegaNavSubNavWrapper}
          >
            {label}
          </Link>
        )}
        {!isEmpty(category.columns) && (
          <div className={subNavWrapperClassNames}>
            <div className="MegaNav-subNav">
              {category.columns.map((column) => (
                <Column
                  key={`column_${column.span}`}
                  category={category}
                  column={column}
                  hideMegaNav={hideMegaNavSubNavWrapper}
                />
              ))}
            </div>
            <Footer
              category={category}
              hideMegaNav={hideMegaNavSubNavWrapper}
            />
          </div>
        )}
      </li>
    )
  }
}
