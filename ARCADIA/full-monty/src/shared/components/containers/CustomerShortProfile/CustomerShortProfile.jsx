import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { mapObjIndexed, prop, isEmpty, any, values, path, pick } from 'ramda'
import Button from '../../common/Button/Button'
import Select from '../../common/FormComponents/Select/Select'
import AccountHeader from '../../common/AccountHeader/AccountHeader'
import BackToAccountLink from '../MyAccount/BackToAccountLink'
import * as AccountActions from '../../../actions/common/accountActions'
import * as FormActions from '../../../actions/common/formActions'
import Input from '../../common/FormComponents/Input/Input'
import Message from '../../common/FormComponents/Message/Message'
import { validate } from '../../../lib/validator'
import analyticsDecorator from '../../../../client/lib/analytics/analytics-decorator'

const getCustomerShortProfile = pick([
  'title',
  'firstName',
  'lastName',
  'email',
])

const validationSchema = {
  email: 'email',
  lastName: 'required',
  firstName: 'required',
  title: 'required',
}

@analyticsDecorator('my-details')
@connect(
  (state) => ({
    customerShortProfile: state.forms.customerShortProfile,
    user: state.account.user,
    titles: state.siteOptions.titles,
  }),
  { ...AccountActions, ...FormActions }
)
export default class CustomerShortProfile extends React.Component {
  static propTypes = {
    customerShortProfile: PropTypes.object,
    user: PropTypes.object,
    titles: PropTypes.array,
    changeShortProfileRequest: PropTypes.func,
    resetForm: PropTypes.func,
    setFormField: PropTypes.func,
    setFormMessage: PropTypes.func,
    setFormSuccess: PropTypes.func,
    touchedFormField: PropTypes.func,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentDidMount() {
    const { resetForm, user, setFormSuccess } = this.props
    setFormSuccess('customerShortProfile', false)
    resetForm('customerShortProfile', getCustomerShortProfile(user))
  }

  componentWillUnmount() {
    this.props.setFormMessage('customerShortProfile', {})
  }

  onSubmit = (ev) => {
    ev.preventDefault()
    const {
      changeShortProfileRequest,
      customerShortProfile,
      titles,
    } = this.props
    changeShortProfileRequest({
      email: path(['fields', 'email', 'value'], customerShortProfile),
      lastName: path(['fields', 'lastName', 'value'], customerShortProfile),
      firstName: path(['fields', 'firstName', 'value'], customerShortProfile),
      title:
        path(['fields', 'title', 'value'], customerShortProfile) || titles[0],
    })
  }

  changeShortProfileAgain = () => {
    this.props.setFormMessage('customerShortProfile', {})
    this.props.setFormSuccess('customerShortProfile', false)
  }

  backToAccount = (e) => {
    e.preventDefault()
    browserHistory.push('/my-account')
    this.props.setFormSuccess('customerShortProfile', false)
    this.props.setFormMessage('customerShortProfile', {})
  }

  render() {
    const { l } = this.context
    const {
      customerShortProfile,
      setFormField,
      touchedFormField,
      titles,
    } = this.props
    const setField = (name) => (e) =>
      setFormField('customerShortProfile', name, e.target.value)
    const touchedField = (name) => () =>
      touchedFormField('customerShortProfile', name)

    const errors = validate(
      validationSchema,
      mapObjIndexed(prop('value'), customerShortProfile.fields),
      l
    )
    return (
      <section className="CustomerShortProfile">
        <AccountHeader
          link="/my-account"
          label={l`Back to My Account`}
          title={l`My details`}
        />
        <form
          onSubmit={this.onSubmit}
          className="MyAccount-form MyAccount-wrapper"
        >
          <Select
            label={l`Title`}
            disabled={customerShortProfile.success}
            name="title"
            value={path(['fields', 'title', 'value'], customerShortProfile)}
            defaultValue={path(
              ['fields', 'title', 'value'],
              customerShortProfile
            )}
            firstDisabled={l`Please select`}
            onChange={setField('title')}
            options={titles}
            isRequired
            privacyProtected
            errors={errors}
            field={path(['fields', 'title'], customerShortProfile)}
          />

          <Input
            isDisabled={customerShortProfile.success}
            field={path(['fields', 'firstName'], customerShortProfile)}
            name="firstName"
            type="text"
            placeholder={l`Example: John`}
            errors={errors}
            label={l`First Name`}
            setField={setField}
            touchedField={touchedField}
            isRequired
            privacyProtected
          />

          <Input
            isDisabled={customerShortProfile.success}
            field={path(['fields', 'lastName'], customerShortProfile)}
            name="lastName"
            type="text"
            placeholder={l`Example: Doe`}
            errors={errors}
            label={l`Last Name`}
            setField={setField}
            touchedField={touchedField}
            isRequired
            privacyProtected
          />

          <Input
            isDisabled={customerShortProfile.success}
            field={path(['fields', 'email'], customerShortProfile)}
            name="email"
            type="email"
            placeholder={l`example@domain.com`}
            errors={errors}
            label={l`Email address`}
            setField={setField}
            touchedField={touchedField}
            isRequired
            privacyProtected
          />

          <Button
            type="submit"
            isDisabled={
              !isEmpty(errors) ||
              customerShortProfile.success ||
              !any(prop('isDirty'), values(customerShortProfile.fields))
            }
            className="CustomerShortProfile-saveChanges"
          >
            {l`SAVE CHANGES`}
          </Button>

          <Message
            message={path(['message', 'message'], customerShortProfile)}
            type={path(['message', 'type'], customerShortProfile)}
          />
          <div className={customerShortProfile.success ? '' : ' hidden'}>
            <p
              role="presentation"
              className="MyAccount-linkChange"
              onClick={this.changeShortProfileAgain}
            >
              {' '}
              {l`Change my details`}
            </p>
            <BackToAccountLink
              clickHandler={this.backToAccount}
              text={l`Back to My Account`}
            />
          </div>
        </form>
      </section>
    )
  }
}
