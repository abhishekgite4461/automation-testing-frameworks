import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { isEmpty, mapObjIndexed, path, prop } from 'ramda'
import { connect } from 'react-redux'
import { validate } from '../../../lib/validator'
import { isSameAs, isNotSameAs } from '../../../lib/validator/validators'
import {
  setFormField,
  touchedFormField,
  resetForm,
} from '../../../actions/common/formActions'
import { registerRequest } from '../../../actions/common/authActions'
import Input from '../../common/FormComponents/Input/Input'
import Checkbox from '../../common/FormComponents/Checkbox/Checkbox'
import Button from '../../common/Button/Button'
import Message from '../../common/FormComponents/Message/Message'
import PrivacyNotice from '../../common/PrivacyNotice/PrivacyNotice'
import {
  sendAnalyticsClickEvent,
  sendAnalyticsErrorMessage,
  ANALYTICS_ERROR,
  GTM_CATEGORY,
  GTM_ACTION,
} from '../../../analytics'

const defaultValues = {
  email: '',
  password: '',
  passwordConfirm: '',
  subscribe: false,
}

@connect(
  (state) => ({
    registrationForm: state.forms.register,
  }),
  {
    touchedFormField,
    setFormField,
    sendAnalyticsClickEvent,
    sendAnalyticsErrorMessage,
    resetForm,
    registerRequest,
  }
)
export default class Register extends Component {
  static propTypes = {
    registrationForm: PropTypes.object.isRequired,
    setFormField: PropTypes.func.isRequired,
    touchedFormField: PropTypes.func.isRequired,
    resetForm: PropTypes.func.isRequired,
    registerRequest: PropTypes.func.isRequired,
    getNextRoute: PropTypes.func,
    className: PropTypes.string,
    successCallback: PropTypes.func,
    errorCallback: PropTypes.func,
    formName: PropTypes.string,
  }

  static defaultProps = {
    className: '',
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentDidMount() {
    const { resetForm } = this.props
    resetForm('register', defaultValues)
  }

  getErrors = () => {
    const { l } = this.context
    const validationSchema = {
      email: 'email',
      password: [
        'password',
        isNotSameAs(l`Password cannot be your email`, 'email'),
      ],
      passwordConfirm: [
        'password',
        isSameAs(l`Please ensure that both passwords match`, 'password'),
      ],
    }
    return validate(
      validationSchema,
      mapObjIndexed(prop('value'), this.props.registrationForm.fields),
      l
    )
  }

  submitHandler = (e) => {
    e.preventDefault()
    const {
      getNextRoute,
      registrationForm: { fields },
      registerRequest,
      formName,
      successCallback,
      sendAnalyticsClickEvent,
      sendAnalyticsErrorMessage,
      errorCallback,
    } = this.props
    if (Object.keys(this.getErrors()).length) {
      sendAnalyticsErrorMessage(ANALYTICS_ERROR.REGISTRATION_FAILED)
      return
    }
    sendAnalyticsClickEvent({
      category: GTM_CATEGORY.REGISTER,
      action: GTM_ACTION.SUBMIT,
      label: 'register-in-button',
    })
    const formData = mapObjIndexed(prop('value'), fields)
    registerRequest({
      formData,
      getNextRoute,
      formName,
      successCallback,
      errorCallback,
    })
  }

  render() {
    const { l } = this.context
    const {
      setFormField,
      touchedFormField,
      registrationForm,
      className,
    } = this.props
    const setField = (name) => (e) =>
      setFormField('register', name, e.target.value)
    const setChecked = (name) => (e) =>
      setFormField('register', name, e.target.checked)
    const touchedField = (name) => () => touchedFormField('register', name)
    const fields = registrationForm.fields
    const errors = this.getErrors()

    return (
      <section className={`Register ${className}`}>
        <h3 className="Register-header">{l`New Customer?`}</h3>
        <form onSubmit={this.submitHandler} className="Register-form">
          <Input
            id="Register-email"
            field={fields.email}
            name="email"
            type="email"
            errors={errors}
            label={l`Email address`}
            placeholder={l`example@domain.com`}
            setField={setField}
            touchedField={touchedField}
            isRequired
          />
          <Input
            id="Register-password"
            field={fields.password}
            name="password"
            type="password"
            errors={errors}
            label={l`Password`}
            setField={setField}
            touchedField={touchedField}
            isRequired
          />
          <Input
            field={fields.passwordConfirm}
            type="password"
            name="passwordConfirm"
            errors={errors}
            label={l`Confirm Password`}
            setField={setField}
            touchedField={touchedField}
            isRequired
          />
          <PrivacyNotice />
          <Checkbox
            className="Checkbox-subscription"
            checked={fields.subscribe}
            onChange={setChecked('subscribe')}
            name="subscribe"
          >{l`Please sign me up for emails`}</Checkbox>
          <Button
            type="submit"
            isDisabled={!isEmpty(errors)}
            isActive={isEmpty(errors)}
            className="Register-saveChanges"
          >
            {l`Register`}
          </Button>

          <Message
            message={path(['message', 'message'], registrationForm)}
            type={path(['message', 'type'], registrationForm)}
          />
        </form>
      </section>
    )
  }
}
