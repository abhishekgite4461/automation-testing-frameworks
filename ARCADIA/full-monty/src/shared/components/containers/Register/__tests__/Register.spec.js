import { mapObjIndexed, prop, clone } from 'ramda'
import deepFreeze from 'deep-freeze'
import testComponentHelper from 'test/unit/helpers/test-component'
import Register from '../Register'
import {
  GTM_CATEGORY,
  GTM_ACTION,
  ANALYTICS_ERROR,
} from '../../../../analytics/analytics-constants'

const defaultValues = {
  email: '',
  password: '',
  passwordConfirm: '',
  subscribe: false,
}

describe('<Register />', () => {
  beforeEach(() => {
    jest.resetAllMocks()
  })

  const initialProps = deepFreeze({
    registrationForm: {
      fields: {
        email: {
          value: 'elroy@arcadia.com',
        },
        password: {
          value: 'password1',
        },
        passwordConfirm: {
          value: 'password1',
        },
        subscribe: {
          value: true,
        },
      },
    },
    setFormField: jest.fn(),
    sendAnalyticsClickEvent: jest.fn(),
    sendAnalyticsErrorMessage: jest.fn(),
    touchedFormField: jest.fn(),
    resetForm: jest.fn(),
    registerRequest: jest.fn(),
    brandName: 'missselfridge',
    region: 'uk',
    getNextRoute: jest.fn(),
  })
  const registerFormWithError = deepFreeze({
    ...initialProps,
    registrationForm: {
      ...initialProps.registrationForm,
      message: {
        message: 'this is a fake Message',
        type: 'fake_type',
      },
    },
  })
  const renderComponent = testComponentHelper(Register.WrappedComponent)

  describe('@renders', () => {
    it('in default state', () => {
      expect(renderComponent(initialProps).getTree()).toMatchSnapshot()
    })

    it('in error state', () => {
      expect(
        renderComponent(
          deepFreeze({
            ...initialProps,
            registrationForm: {
              fields: {
                email: {},
                password: {},
                passwordConfirm: {},
                subscribe: {},
              },
            },
          })
        ).getTree()
      ).toMatchSnapshot()
    })

    it('with registrationForm having message', () => {
      expect(
        renderComponent(deepFreeze(registerFormWithError)).getTree()
      ).toMatchSnapshot()
    })
  })

  describe('@lifecycle', () => {
    describe('componentDidMount', () => {
      it('should call resetForm', () => {
        const { instance } = renderComponent(initialProps)
        expect(instance.props.resetForm).not.toHaveBeenCalled()
        instance.componentDidMount()
        expect(instance.props.resetForm).toHaveBeenCalledTimes(1)
        expect(instance.props.resetForm).toHaveBeenLastCalledWith(
          'register',
          defaultValues
        )
      })
    })
  })

  describe('@events', () => {
    let renderedComponent
    const event = deepFreeze({
      target: {
        value: 123,
        checked: true,
      },
      preventDefault: jest.fn(),
    })

    beforeEach(() => {
      renderedComponent = renderComponent(initialProps)
      jest.resetAllMocks()
    })

    describe('email Input', () => {
      const field = 'email'

      it('should call setFormField when prop setField is called', () => {
        const { wrapper, instance } = renderedComponent
        expect(instance.props.setFormField).not.toHaveBeenCalled()
        wrapper.find(`Connect(Input) [name="${field}"]`).prop('setField')(
          field
        )(event)
        expect(instance.props.setFormField).toHaveBeenCalledTimes(1)
        expect(instance.props.setFormField).toHaveBeenLastCalledWith(
          'register',
          field,
          event.target.value
        )
      })

      it('should call touchedFormField when prop touchedField is called', () => {
        const { wrapper, instance } = renderedComponent
        expect(instance.props.touchedFormField).not.toHaveBeenCalled()
        wrapper.find(`Connect(Input) [name="${field}"]`).prop('touchedField')(
          field
        )(event)
        expect(instance.props.touchedFormField).toHaveBeenCalledTimes(1)
        expect(instance.props.touchedFormField).toHaveBeenLastCalledWith(
          'register',
          field
        )
      })
    })

    describe('password Input', () => {
      const field = 'password'

      it('should call setFormField when prop setField is called', () => {
        const { wrapper, instance } = renderedComponent
        expect(instance.props.setFormField).not.toHaveBeenCalled()
        wrapper.find(`Connect(Input) [name="${field}"]`).prop('setField')(
          field
        )(event)
        expect(instance.props.setFormField).toHaveBeenCalledTimes(1)
        expect(instance.props.setFormField).toHaveBeenLastCalledWith(
          'register',
          field,
          event.target.value
        )
      })

      it('should call touchedFormField when prop touchedField is called', () => {
        const { wrapper, instance } = renderedComponent
        expect(instance.props.touchedFormField).not.toHaveBeenCalled()
        wrapper.find(`Connect(Input) [name="${field}"]`).prop('touchedField')(
          field
        )(event)
        expect(instance.props.touchedFormField).toHaveBeenCalledTimes(1)
        expect(instance.props.touchedFormField).toHaveBeenLastCalledWith(
          'register',
          field
        )
      })
    })

    describe('passwordConfirm Input', () => {
      const field = 'passwordConfirm'

      it('should call setFormField when prop setField is called', () => {
        const { wrapper, instance } = renderedComponent
        expect(instance.props.setFormField).not.toHaveBeenCalled()
        wrapper.find(`Connect(Input) [name="${field}"]`).prop('setField')(
          field
        )(event)
        expect(instance.props.setFormField).toHaveBeenCalledTimes(1)
        expect(instance.props.setFormField).toHaveBeenLastCalledWith(
          'register',
          field,
          event.target.value
        )
      })

      it('should call touchedFormField when prop touchedField is called', () => {
        const { wrapper, instance } = renderedComponent
        expect(instance.props.touchedFormField).not.toHaveBeenCalled()
        wrapper.find(`Connect(Input) [name="${field}"]`).prop('touchedField')(
          field
        )(event)
        expect(instance.props.touchedFormField).toHaveBeenCalledTimes(1)
        expect(instance.props.touchedFormField).toHaveBeenLastCalledWith(
          'register',
          field
        )
      })
    })

    describe('subscribe Checkbox', () => {
      const field = 'subscribe'

      it('should call setFormField when prop setField is called', () => {
        const { wrapper, instance } = renderedComponent
        expect(instance.props.setFormField).not.toHaveBeenCalled()
        wrapper
          .find(`Connect(Checkbox) [name="${field}"]`)
          .simulate('change', event)
        expect(instance.props.setFormField).toHaveBeenCalledTimes(1)
        expect(instance.props.setFormField).toHaveBeenLastCalledWith(
          'register',
          field,
          event.target.checked
        )
      })
    })

    describe('submit form', () => {
      it('should call e.preventDefault() ', () => {
        const { wrapper } = renderedComponent
        expect(event.preventDefault).not.toHaveBeenCalled()
        wrapper.find('form').simulate('submit', event)
        expect(event.preventDefault).toHaveBeenCalledTimes(1)
      })

      it('should not call analytics methods when there are no input values and no error message', () => {
        const props = clone(initialProps)
        props.registrationForm.fields.email = ''
        props.registrationForm.fields.password = ''
        const { wrapper, instance } = renderComponent(props)
        expect(instance.props.sendAnalyticsClickEvent).not.toHaveBeenCalled()
        wrapper.find('form').simulate('submit', event)
        expect(instance.props.sendAnalyticsClickEvent).not.toHaveBeenCalled()
        expect(instance.props.sendAnalyticsErrorMessage).toHaveBeenCalled()
        expect(instance.props.sendAnalyticsErrorMessage).toHaveBeenCalledWith(
          ANALYTICS_ERROR.REGISTRATION_FAILED
        )
      })

      it('should call sendAnalyticsErrorMessage() when user submits form with incorrect validation', () => {
        const props = clone(initialProps)
        props.registrationForm.fields.email = ''
        props.registrationForm.fields.password = 'testpassword12345'
        const { wrapper, instance } = renderComponent(props)
        expect(instance.props.sendAnalyticsClickEvent).not.toHaveBeenCalled()
        wrapper.find('form').simulate('submit', event)
        expect(instance.props.sendAnalyticsClickEvent).not.toHaveBeenCalled()
        expect(instance.props.sendAnalyticsErrorMessage).toHaveBeenCalled()
        expect(instance.props.sendAnalyticsErrorMessage).toHaveBeenCalledWith(
          ANALYTICS_ERROR.REGISTRATION_FAILED
        )
      })

      it('should call analytics methods when there are input values', () => {
        const { instance, wrapper } = renderComponent(registerFormWithError)
        wrapper.find('form').simulate('submit', event)
        expect(instance.props.sendAnalyticsClickEvent).toHaveBeenCalledWith({
          category: GTM_CATEGORY.REGISTER,
          action: GTM_ACTION.SUBMIT,
          label: 'register-in-button',
        })
      })

      it('should call registerRequest if no errors', () => {
        const { instance, wrapper } = renderedComponent
        const {
          getNextRoute,
          successCallback,
          errorCallback,
          registerRequest,
          registrationForm,
        } = instance.props
        const formData = mapObjIndexed(prop('value'), registrationForm.fields)
        instance.getErrors = jest.fn(() => ({}))
        expect(registerRequest).not.toHaveBeenCalled()
        wrapper.find('form').simulate('submit', event)
        expect(registerRequest).toHaveBeenCalledTimes(1)
        expect(registerRequest).toHaveBeenLastCalledWith({
          formData,
          getNextRoute,
          successCallback,
          errorCallback,
        })
      })

      it('should not call registerRequest if errors', () => {
        const { instance, wrapper } = renderedComponent
        instance.getErrors = jest.fn(() => ({
          email: 'has errors',
        }))
        wrapper.find('form').simulate('submit', event)
        expect(instance.props.registerRequest).not.toHaveBeenCalled()
      })
    })
  })
})
