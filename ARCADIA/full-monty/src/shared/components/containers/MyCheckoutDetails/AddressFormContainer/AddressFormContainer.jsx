import { connect } from 'react-redux'

// components
import AddressFormDefaults from '../../../common/AddressForm/AddressForm'

// selectors
import {
  getFormNames,
  getMCDAddressForm,
  getCountryFor,
  getFindAddressIsVisible,
} from '../../../../selectors/common/accountSelectors'

export const mapStateToProps = (state, { addressType }) => {
  const formNames = getFormNames(addressType)
  const country = getCountryFor(addressType, formNames.address, state)
  const isFindAddressVisible = getFindAddressIsVisible(
    addressType,
    formNames.address,
    country,
    state
  )
  return {
    addressType,
    country,
    formNames,
    getAddressFormFromState: getMCDAddressForm,
    isFindAddressVisible,
  }
}

export default connect(mapStateToProps)(AddressFormDefaults)
