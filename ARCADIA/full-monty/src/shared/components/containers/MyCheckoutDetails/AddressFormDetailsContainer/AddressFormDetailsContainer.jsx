import { connect } from 'react-redux'

// components
import AddressFormDetails from '../../../common/AddressFormDetails/AddressFormDetails'

// selectors
import {
  getFormNames,
  getMCDAddressForm,
  getCountryFor,
} from '../../../../selectors/common/accountSelectors'

export const mapStateToProps = (state, { addressType }) => {
  const formNames = getFormNames(addressType)
  const country = getCountryFor(addressType, formNames.address, state)
  return {
    addressType,
    country,
    formNames,
    getAddressFormFromState: getMCDAddressForm,
  }
}

export default connect(mapStateToProps)(AddressFormDetails)
