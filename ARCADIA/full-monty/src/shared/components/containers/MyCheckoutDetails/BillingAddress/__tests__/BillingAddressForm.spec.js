import testComponentHelper from 'test/unit/helpers/test-component'
import BillingAddressForm from '../BillingAddressForm'
import AddressFormContainer from '../../AddressFormContainer/AddressFormContainer'

describe('<BillingAddressForm />', () => {
  const renderComponent = testComponentHelper(BillingAddressForm)

  describe('@renders', () => {
    const renderedComponent = renderComponent()
    const { wrapper } = renderedComponent

    it('in default state', () => {
      expect(renderedComponent.getTree()).toMatchSnapshot()
    })

    it('should set `addressType` prop to `billing` in the `AddressForm` component', () => {
      expect(wrapper.find(AddressFormContainer).prop('addressType')).toBe(
        'billing'
      )
    })
  })
})
