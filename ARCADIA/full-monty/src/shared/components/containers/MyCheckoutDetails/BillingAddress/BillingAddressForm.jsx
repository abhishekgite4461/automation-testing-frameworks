import React from 'react'

import AddressFormContainer from '../AddressFormContainer/AddressFormContainer'

const BillingAddressForm = () => {
  return <AddressFormContainer addressType="billing" />
}

export default BillingAddressForm
