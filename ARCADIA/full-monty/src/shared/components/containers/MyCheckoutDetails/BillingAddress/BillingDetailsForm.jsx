import React from 'react'

import AddressFormDetailsContainer from '../AddressFormDetailsContainer/AddressFormDetailsContainer'

const BillingDetailsForm = () => {
  return (
    <AddressFormDetailsContainer
      addressType="billing"
      label="Billing Details"
    />
  )
}

export default BillingDetailsForm
