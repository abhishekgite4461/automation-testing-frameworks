import testComponentHelper from 'test/unit/helpers/test-component'
import DeliveryAddressForm from '../DeliveryAddressForm'
import AddressFormContainer from '../../AddressFormContainer/AddressFormContainer'

describe('<DeliveryAddressForm />', () => {
  const renderComponent = testComponentHelper(DeliveryAddressForm)

  describe('@renders', () => {
    const renderedComponent = renderComponent()
    const { wrapper } = renderedComponent

    it('in default state', () => {
      expect(renderedComponent.getTree()).toMatchSnapshot()
    })

    it('should set `addressType` prop to `delivery` in the `AddressForm` component', () => {
      expect(wrapper.find(AddressFormContainer).prop('addressType')).toBe(
        'delivery'
      )
    })
  })
})
