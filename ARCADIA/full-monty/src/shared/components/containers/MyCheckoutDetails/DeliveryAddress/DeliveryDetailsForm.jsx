import React from 'react'

import AddressFormDetailsContainer from '../AddressFormDetailsContainer/AddressFormDetailsContainer'

const DeliveryDetailsForm = () => {
  return (
    <AddressFormDetailsContainer
      addressType="delivery"
      label="Delivery Details"
    />
  )
}

export default DeliveryDetailsForm
