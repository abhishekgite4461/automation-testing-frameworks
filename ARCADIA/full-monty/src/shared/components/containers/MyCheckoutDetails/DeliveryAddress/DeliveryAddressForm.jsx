import React from 'react'

import AddressFormContainer from '../AddressFormContainer/AddressFormContainer'

const DeliveryAddressForm = () => {
  return <AddressFormContainer addressType="delivery" />
}

export default DeliveryAddressForm
