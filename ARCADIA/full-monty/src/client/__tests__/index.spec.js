global.process.browser = false
import { getItem } from '../lib/cookie'
import { setupLoaders } from '../../shared/lib/image-loader/image-loader'
import { getSelectedStore, loadRecentlyViewedState } from '../lib/storage'
import {
  retrievePartialOrderSummary,
  partialOrderSubscribeStore,
} from '../lib/partial-orders'

jest.mock('../../shared/lib/image-loader/image-loader')

jest.mock('../lib/cookie', () => ({
  getItem: jest.fn(() => 'TS0001'),
  getJSON: jest.fn(),
}))

jest.mock('../lib/partial-orders', () => ({
  retrievePartialOrderSummary: jest.fn(),
  partialOrderSubscribeStore: jest.fn(),
}))

jest.mock('../lib/storage', () => ({
  getSelectedStore: jest.fn(() => {}),
  loadRecentlyViewedState: jest.fn(() => ({})),
}))

jest.mock('react-dom', () => ({
  render: jest.fn(),
}))
jest.mock('../../shared/lib/price', () => ({
  format: jest.fn(() => {}),
  formatPrice: jest.fn(() => {}),
}))
jest.mock('../Root.jsx', () => '')
jest.mock('../../shared/lib/localisation', () => ({
  setDictionary: jest.fn(() => 'TS0001'),
  localise: jest.fn(() => {}),
}))
jest.mock('../../shared/lib/context-provider', () => '')

describe('Index', () => {
  global.__INITIAL_STATE__ = {
    localisation: {
      dictionary: [],
    },
    config: {
      language: 'EN',
      brandName: 'Topshop',
    },
    storeLocator: {},
    checkout: {},
  }

  it('calls the top-level setup functions', () => {
    // index has global code so it needs to be required after mocks, instead of being imported
    require('../index')

    expect(loadRecentlyViewedState).toHaveBeenCalledTimes(2) // once in index.js and once in recentlyViewedReducer
    expect(getItem).toHaveBeenCalledWith('WC_pickUpStore')
    expect(setupLoaders).toHaveBeenCalledTimes(1)
    expect(setupLoaders).toHaveBeenCalledWith(window)
    expect(getSelectedStore).toHaveBeenCalledTimes(1)
    expect(retrievePartialOrderSummary).toHaveBeenCalledTimes(1)
    expect(partialOrderSubscribeStore).toHaveBeenCalledTimes(1)
  })
})
