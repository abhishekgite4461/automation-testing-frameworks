import React from 'react'
import { render } from 'react-dom'
import configureStore from '../shared/lib/configure-store'
import Root from './Root'
import ContextProvider from '../shared/lib/context-provider'
import analyticsMiddleware from '../shared/analytics/middleware'
import { localise, setDictionary } from '../shared/lib/localisation'
import { format as formatPrice } from '../shared/lib/price'
import { makeReferenceToStore } from '../shared/lib/get-value-from-store'
import { getSelectedStore, loadRecentlyViewedState } from './lib/storage'
import * as sendReporter from './lib/reporter'
import hijack from './lib/hijack-links'
import { getItem } from './lib/cookie'
import {
  retrievePartialOrderSummary,
  partialOrderSubscribeStore,
} from './lib/partial-orders'
import { setupLoaders } from '../shared/lib/image-loader/image-loader'
import initKeepAlivePolling from './lib/keep-alive'
import { getFeaturesKeepAlive } from '../shared/selectors/featureSelectors'

setupLoaders(window)
analyticsMiddleware()
sendReporter.start()

if (window.isRedAnt)
  hijack((pathname) => {
    window.location = pathname
  })

const state = Object.assign({}, window.__INITIAL_STATE__, {
  recentlyViewed: loadRecentlyViewedState(),
})

// TODO clean up checkout related stuff
if (state.checkout) {
  state.checkout.partialOrderSummary = retrievePartialOrderSummary(state)
}

if (state.storeLocator && getItem('WC_pickUpStore')) {
  state.storeLocator.selectedStore = getSelectedStore()
}

const store = configureStore(state)
window.__qubitStore = store
window.__React = React

const keepAliveInterval = getFeaturesKeepAlive(store.getState())
if (keepAliveInterval) initKeepAlivePolling(store, keepAliveInterval)
setDictionary(state.localisation.dictionary)
partialOrderSubscribeStore(store)

function renderApp(RootComponent) {
  const { language, brandName } = state.config
  render(
    // eslint-disable-next-line react/jsx-no-bind
    <ContextProvider
      localise={localise.bind(null, language, brandName)}
      formatPrice={formatPrice.bind(null, state.config.currencyCode)}
    >
      <RootComponent store={store} />
    </ContextProvider>,
    document.getElementById('root')
  )
}

makeReferenceToStore(store)
renderApp(Root)

// Webpack Hot Module Replacement API
if (module.hot) {
  module.hot.accept()
}
