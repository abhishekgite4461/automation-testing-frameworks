import {
  retrievePartialOrderSummary,
  partialOrderSubscribeStore,
} from '../partial-orders'
import {
  setPartialOrderSummary,
  getPartialOrderSummary,
  isStorageSupported,
} from '../storage'
import { mockStoreCreator } from '../../../../test/unit/helpers/get-redux-mock-store'
import * as actions from '../../../shared/actions/common/checkoutActions'

jest.mock('../storage', () => ({
  setPartialOrderSummary: jest.fn(),
  getPartialOrderSummary: jest.fn(),
  isStorageSupported: jest.fn(),
  loadRecentlyViewedState: jest.fn(() => ({})),
}))

afterEach(() => {
  jest.resetAllMocks()
})

describe('Partial order retrieval from storage', () => {
  it('returns partial orders from storage when available', () => {
    isStorageSupported.mockImplementation(() => true)
    const mockedPartialOrdersFromStorage = [{ foo: 'bar' }]
    getPartialOrderSummary.mockImplementation(
      () => mockedPartialOrdersFromStorage
    )
    const partialOrderSummary = retrievePartialOrderSummary()

    expect(isStorageSupported).toHaveBeenCalledTimes(1)
    expect(getPartialOrderSummary).toHaveBeenCalledTimes(1)
    expect(partialOrderSummary).toEqual(mockedPartialOrdersFromStorage)
  })

  it('does not attempt to read from storage when not available', () => {
    isStorageSupported.mockImplementation(() => false)
    retrievePartialOrderSummary()

    expect(isStorageSupported).toHaveBeenCalledTimes(1)
    expect(getPartialOrderSummary).toHaveBeenCalledTimes(0)
  })
})

describe('Partial orders retrieval from state', () => {
  isStorageSupported.mockImplementation(() => false)

  it('returns undefined when partial order in state is not an array', () => {
    const state = {
      checkout: {
        orderCompleted: {
          orderLines: {},
          deliveryStoreCode: 'baz',
        },
      },
    }
    const partialOrderSummary = retrievePartialOrderSummary(state)
    expect(partialOrderSummary).toBeUndefined()
  })

  it('returns undefined when partial order in state is an empty array', () => {
    const state = {
      checkout: {
        orderCompleted: {
          orderLines: [],
          deliveryStoreCode: 'baz',
        },
      },
    }
    const partialOrderSummary = retrievePartialOrderSummary(state)
    expect(partialOrderSummary).toBeUndefined()
  })

  it('returns partial order from state', () => {
    const state = {
      checkout: {
        orderCompleted: {
          orderLines: [
            {
              lineNo: 'foo1',
              productId: 'bar1',
              wasPrice: '0.99',
              attributes: ['attributes1_1', 'attribute1_2'],
            },
            {
              lineNo: 'foo2',
              productId: 'bar2',
              wasPrice: '0.88',
              attributes: ['attributes2_1', 'attribute2_2'],
            },
          ],
          deliveryStoreCode: 'baz',
        },
      },
    }
    const expectedPartialOrder = {
      basket: [
        {
          lineNumber: 'foo1',
          productId: 'bar1',
          wasPrice: '0.99',
          attributes: ['attributes1_1', 'attribute1_2'],
        },
        {
          lineNumber: 'foo2',
          productId: 'bar2',
          wasPrice: '0.88',
          attributes: ['attributes2_1', 'attribute2_2'],
        },
      ],
      deliveryStoreCode: 'baz',
    }
    const partialOrderSummary = retrievePartialOrderSummary(state)
    expect(partialOrderSummary).toEqual(expectedPartialOrder)
  })
})

describe('Partial order store subscription', () => {
  it('does not attempt to set partial order summary when no basket in order summary', () => {
    const store = mockStoreCreator({ some: 'state' })
    partialOrderSubscribeStore(store)
    store.dispatch(actions.clearCheckoutForms())
    expect(setPartialOrderSummary).toHaveBeenCalledTimes(0)
  })

  it('calls set partial order summary with correct data', () => {
    const store = mockStoreCreator({
      checkout: {
        orderSummary: {
          basket: {
            products: [
              {
                lineNumber: '1',
                wasPrice: '0.99',
                productId: 'id1',
                attributes: {
                  attr1_1: '1_1',
                  attr1_2: '1_2',
                },
                foo: 'omit me',
                baz: 'omit me',
              },
              {
                lineNumber: '2',
                wasPrice: '0.88',
                productId: 'id2',
                attributes: {
                  attr2_1: '2_1',
                  attr2_2: '2_2',
                },
                baz: 'omit me',
                qux: 'omit me',
              },
            ],
            deliveryOptions: 'omit me',
            ageVerificationRequired: 'omit me',
            restrictedDeliveryItem: 'omit me',
          },
          ageVerificationDeliveryConfirmationRequired: 'omit me',
          billingDetails: 'omit me',
          cardNumberHash: 'omit me',
          creditCard: 'omit me',
          deliveryDetails: 'omit me',
          deliveryInstructions: 'omit me',
          deliveryLocations: 'omit me',
          estimatedDelivery: 'omit me',
          giftCards: 'omit me',
          savedAddresses: 'omit me',
          smsMobileNumber: 'omit me',
          version: 'omit me',
        },
      },
    })
    partialOrderSubscribeStore(store)
    store.dispatch(actions.clearCheckoutForms())
    expect(setPartialOrderSummary).toHaveBeenCalledTimes(1)
    expect(setPartialOrderSummary).toHaveBeenCalledWith({
      basket: {
        products: [
          {
            lineNumber: '1',
            wasPrice: '0.99',
            productId: 'id1',
            attributes: {
              attr1_1: '1_1',
              attr1_2: '1_2',
            },
          },
          {
            lineNumber: '2',
            wasPrice: '0.88',
            productId: 'id2',
            attributes: {
              attr2_1: '2_1',
              attr2_2: '2_2',
            },
          },
        ],
      },
    })
  })
})
