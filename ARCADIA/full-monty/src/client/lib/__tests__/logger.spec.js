import { error, info, debug } from '../logger'
import { errorReport, infoReport, debugReport } from '../reporter'

jest.mock('../reporter')

describe('client logger', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  describe('error', () => {
    it('passes error through to `errorReport`', () => {
      error('ERROR', { loggerMessage: 'An error occurred' })
      expect(errorReport).toHaveBeenCalledWith('ERROR', {
        loggerMessage: 'An error occurred',
      })
    })

    it('second arg `message` can be an object', () => {
      error('ERROR', { message: 'err', lineno: 100, filename: 'foo.js' })
      expect(errorReport).toHaveBeenCalledWith('ERROR', {
        message: 'err',
        lineno: 100,
        filename: 'foo.js',
      })
    })

    it('second arg `message` can be an instance of Error', () => {
      const errorEvent = new Error('Oh noes!')
      error('ERROR', errorEvent)
      expect(errorReport).toHaveBeenCalledWith('ERROR', errorEvent)
    })
  })

  describe('info', () => {
    it('passes everything to `infoReport` as JSON string', () => {
      info('error', { foo: 'bar' })
      expect(infoReport).toHaveBeenCalledWith('error', { foo: 'bar' })
    })
  })

  describe('debug', () => {
    it('passes everything to `debugReport` as JSON string', () => {
      debug('error', { foo: 'bar' })
      expect(debugReport).toHaveBeenCalledWith('error', { foo: 'bar' })
    })
  })
})
