const reporter = require('../reporter')

reporter.isDebugLogEnable = jest.fn(() => true)
import {
  infoReport,
  debugReport,
  errorReport,
  errorReportEvent,
  errorReportThrottled,
  start,
} from '../reporter'

jest.mock('lodash.throttle', () => () => jest.fn())

jest.mock('../reporter-utils', () => ({
  postToServer: jest.fn(),
}))

import { postToServer } from '../reporter-utils'

describe('reporter', () => {
  const logginLevel = global.process.env.LOGGING_LEVEL
  const addEvent = global.window.addEventListener

  beforeEach(() => {
    jest.clearAllMocks()
    global.process.env.LOGGING_LEVEL = logginLevel
    global.window.addEventListener = addEvent
  })

  describe('infoReport', () => {
    it('infoReport null', () => {
      infoReport()
      expect(postToServer).toHaveBeenCalledTimes(1)
      expect(postToServer).toHaveBeenCalledWith(
        'client-info',
        'client-info',
        {}
      )
    })

    it('infoReport title', () => {
      infoReport('title', { aaa: 'bbb' })
      expect(postToServer).toHaveBeenCalledTimes(1)
      expect(postToServer).toHaveBeenCalledWith('client-info', 'title', {
        aaa: 'bbb',
      })
    })
  })

  describe('debugReport', () => {
    it('debugReport null', () => {
      debugReport()
      expect(postToServer).toHaveBeenCalledTimes(1)
      expect(postToServer).toHaveBeenCalledWith(
        'client-debug',
        'client-debug',
        {}
      )
    })

    it('debugReport title', () => {
      debugReport('title', { aaa: 'bbb' })
      expect(postToServer).toHaveBeenCalledTimes(1)
      expect(postToServer).toHaveBeenCalledWith('client-debug', 'title', {
        aaa: 'bbb',
      })
    })
  })

  describe('errorReport', () => {
    it('default values', () => {
      errorReport()
      expect(postToServer).toHaveBeenCalledTimes(1)
      expect(postToServer).toHaveBeenCalledWith(
        'client-error',
        'client-error',
        {}
      )
    })

    it('set values', () => {
      errorReport('tsuk', { aaa: 'bbb' })
      expect(postToServer).toHaveBeenCalledTimes(1)
      expect(postToServer).toHaveBeenCalledWith('client-error', 'tsuk', {
        aaa: 'bbb',
      })
    })
  })

  describe('errorReportEvent', () => {
    it('set values from error', () => {
      errorReportEvent({
        message: 'aaa',
        filename: 'bbb',
        lineno: 1,
        error: {
          stack: 'cccc',
        },
        nativeError: {
          stack: 'ddddd',
        },
      })
      expect(postToServer).toHaveBeenCalledTimes(1)
      expect(postToServer).toHaveBeenCalledWith('client-error', 'errorEvent', {
        filename: 'bbb',
        lineno: 1,
        message: 'aaa',
        stack: 'cccc',
        nativeError: { stack: 'ddddd' },
        error: { stack: 'cccc' },
      })
    })

    it('set values from nativeError', () => {
      errorReportEvent({
        message: 'aaa',
        filename: 'bbb',
        lineno: 1,
        nativeError: {
          stack: 'ddddd',
        },
      })
      expect(postToServer).toHaveBeenCalledTimes(1)
      expect(postToServer).toHaveBeenCalledWith('client-error', 'errorEvent', {
        filename: 'bbb',
        lineno: 1,
        message: 'aaa',
        stack: 'ddddd',
        nativeError: { stack: 'ddddd' },
      })
    })

    it('call errorReportThrottled for "Script error"', () => {
      const error = {
        message: 'Script error.',
      }

      errorReportEvent(error)

      expect(errorReportThrottled).toHaveBeenCalledTimes(1)
      expect(errorReportThrottled.mock.calls[0][1]).toEqual(
        expect.objectContaining(error)
      )
    })

    it('must not call errorReportThrottled for non "Script error"', () => {
      const error = {
        message: 'some error',
      }

      errorReportEvent(error)

      expect(errorReportThrottled).toHaveBeenCalledTimes(0)
    })
  })

  describe('start', () => {
    it('hooks with error', () => {
      const addEventListenerMock = jest.fn()
      global.window.addEventListener = addEventListenerMock
      start()
      expect(addEventListenerMock).toHaveBeenCalledTimes(1)
      expect(addEventListenerMock).toHaveBeenCalledWith(
        'error',
        errorReportEvent
      )
    })
  })
})
