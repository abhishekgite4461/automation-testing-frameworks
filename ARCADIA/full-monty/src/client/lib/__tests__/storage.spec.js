global.process.browser = true

const storage = require('../storage')

const JSONData = '{"test": "example"}'
const testObj = { test: 'test' }

jest.mock('../../../shared/constants/localStorage', () => ({
  localStoragePaths: { recentlyViewed: '{"test": "example"}' },
}))
import { errorReport } from '../reporter'

jest.mock('../reporter', () => ({
  errorReport: jest.fn(),
}))
const originalJSON = global.JSON.parse
describe('Storage', () => {
  beforeEach(() => {
    global.JSON.parse = originalJSON
    global.sessionStorage = {
      setItem: jest.fn(),
      removeItem: jest.fn(),
      getItem: jest.fn(() => JSONData),
    }
    global.localStorage = {
      getItem: jest.fn(() => JSONData),
      setItem: jest.fn(),
      removeItem: jest.fn(),
    }
  })
  describe('Is storage supported', () => {
    it('should return storage support status', () => {
      const storageSupported = storage.isStorageSupported()
      expect(storageSupported).toBe(true)
    })
  })

  describe('Get partial orderSummary', () => {
    it('should return as JSON', () => {
      const partialOrderSummaryString = storage.getPartialOrderSummary()
      expect(partialOrderSummaryString).toEqual(JSON.parse(JSONData))
    })
  })

  describe('Get selected store', () => {
    it('should return as JSON', () => {
      const selectedStore = storage.getSelectedStore()
      expect(selectedStore).toEqual(JSON.parse(JSONData))
    })
    it('should return error if not JSON', () => {
      global.JSON.parse = () => {
        throw new Error()
      }
      storage.getSelectedStore()
      expect(errorReport).toHaveBeenCalledTimes(1)
    })
    it('should return empty object if no selected store', () => {
      global.sessionStorage = {
        getItem: jest.fn(() => false),
      }
      const selectedStore = storage.getSelectedStore()
      expect(selectedStore).toEqual({})
    })
  })

  describe('Set partial order summary', () => {
    it('should set object', () => {
      storage.setPartialOrderSummary(testObj)
      expect(sessionStorage.setItem).toBeCalledWith(
        'partialOrderSummary',
        JSON.stringify(testObj)
      )
    })
  })

  describe('Set selected store', () => {
    it('should set object', () => {
      storage.setSelectedStore(testObj)
      expect(sessionStorage.setItem).toBeCalledWith(
        'selectedStore',
        JSON.stringify(testObj)
      )
    })
  })

  describe('Get cache data', () => {
    it('should call getItem', () => {
      storage.getCacheData()
      expect(localStorage.getItem).toBeCalled()
    })
  })

  describe('Set cache data', () => {
    it('should call setItem', () => {
      storage.setCacheData('test', testObj)
      expect(localStorage.setItem).toBeCalledWith(
        'cached_test',
        JSON.stringify(testObj)
      )
    })
  })

  describe('Clear cache items', () => {
    it('removes items from cache', () => {
      storage.clearCacheData()
      expect(localStorage.removeItem).toBeCalled()
    })
  })

  describe('Load recently viewed state', () => {
    it('should return json', () => {
      const recentlyViewedState = storage.loadRecentlyViewedState()
      expect(recentlyViewedState).toEqual(JSON.parse(JSONData))
    })
  })

  describe('Save recently viewed state', () => {
    it('should call setItem', () => {
      storage.saveRecentlyViewedState(testObj)
      expect(localStorage.setItem).toBeCalledWith(
        JSONData,
        JSON.stringify(testObj)
      )
    })
  })

  describe('Shipping destination', () => {
    it('should save to local session storage', () => {
      const param = {
        shippingDestination: {
          destination: 'Trinidad',
        },
      }
      storage.saveShippingDestinationDetails(param)
      expect(sessionStorage.setItem).toBeCalledWith(
        'shippingDestination',
        JSON.stringify(param)
      )
    })
  })

  it('should get it from local session storage', () => {
    storage.getShippingDestinationDetails()
    global.sessionStorage = {
      getItem: jest.fn(() => '{"destination": "Trinidad"}'),
    }
    expect(storage.getShippingDestinationDetails()).toEqual('Trinidad')
  })
})
