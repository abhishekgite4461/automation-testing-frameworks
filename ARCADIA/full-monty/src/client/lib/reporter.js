import { pathOr } from 'ramda'
import { postToServer } from './reporter-utils'
import { getTraceIdFromCookie } from '../../../src/shared/lib/cookie'
import throttle from 'lodash.throttle'

export const isDebugLogEnable = () =>
  process.browser &&
  pathOr('', ['document', 'location', 'search'], window).indexOf(
    'debuglog=true'
  ) > -1

export const infoReport = (namespace = 'client-info', infoObject = {}) => {
  postToServer('client-info', namespace, infoObject)
}

export const debugReport = (namespace = 'client-debug', debugObject = {}) => {
  if (exports.isDebugLogEnable()) {
    postToServer('client-debug', namespace, debugObject)
  }
}

export const errorReport = (namespace = 'client-error', errorObject = {}) => {
  if (process.browser && process.env.ENABLE_CLIENT_ERROR_LOGGING) {
    window.console.error('client-error', namespace, errorObject)
  }

  postToServer('client-error', namespace, errorObject)
}

export const errorReportThrottled = throttle(
  errorReport,
  process.env.ERROR_REPORT_THROTTLE_TIMEOUT || 2000,
  { leading: true, trailing: false }
)

const getReporter = (message) =>
  message === 'Script error.' ? errorReportThrottled : errorReport

export const errorReportEvent = (errorEvent) => {
  const { message, filename, lineno, error, nativeError } = errorEvent
  const stack = (error && error.stack) || (nativeError && nativeError.stack)

  getReporter(message)('errorEvent', {
    message,
    filename,
    lineno,
    stack,
    error,
    nativeError,
  })
}

export function start() {
  window.addEventListener('error', errorReportEvent)
}

if (exports.isDebugLogEnable()) {
  const traceId = getTraceIdFromCookie(window.document.cookie)
  if (traceId) {
    prompt('Your Trace Id is...', traceId) // eslint-disable-line no-alert
  } else {
    alert('Please refresh your browser') // eslint-disable-line no-alert
  }
}
