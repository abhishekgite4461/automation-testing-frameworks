// TODO clean up this file, refactor out functions into selectors
import { omit, assocPath, isEmpty } from 'ramda'
import {
  isStorageSupported,
  setPartialOrderSummary,
  getPartialOrderSummary,
} from './storage'
import {
  getCheckoutOrderLines,
  getCheckoutOrderSummary,
  getCheckoutOrderSummaryBasket,
  getCheckoutOrderCompletedDeliveryStoreCode,
} from '../../shared/selectors/checkoutSelectors'

const mapOrderLines = (line) => {
  return {
    lineNumber: line.lineNo,
    productId: line.productId,
    wasPrice: line.wasPrice,
    attributes: line.attributes,
  }
}

const getPartialOrderSummaryFromState = (state) => {
  const orderLines = getCheckoutOrderLines(state)
  if (!Array.isArray(orderLines) || !orderLines.length) {
    return undefined
  }

  return {
    basket: orderLines.map(mapOrderLines),
    deliveryStoreCode: getCheckoutOrderCompletedDeliveryStoreCode(state),
  }
}

export const retrievePartialOrderSummary = (state = {}) => {
  if (isStorageSupported()) {
    return getPartialOrderSummary()
  }

  return getPartialOrderSummaryFromState(state)
}

export const partialOrderSubscribeStore = (store) => {
  store.subscribe(() => {
    const state = store.getState()
    const stateBasket = getCheckoutOrderSummaryBasket(state)

    if (isEmpty(stateBasket)) {
      return
    }

    const basket = omit(
      ['deliveryOptions', 'ageVerificationRequired', 'restrictedDeliveryItem'],
      stateBasket
    )
    basket.products = Array.isArray(stateBasket.products)
      ? stateBasket.products.map(
          ({ lineNumber, wasPrice, productId, attributes }) => ({
            lineNumber,
            wasPrice,
            productId,
            attributes,
          })
        )
      : []
    const checkoutOrderSummary = getCheckoutOrderSummary(state)
    const partialOrderSummary = omit(
      [
        'ageVerificationDeliveryConfirmationRequired',
        'billingDetails',
        'cardNumberHash',
        'creditCard',
        'deliveryDetails',
        'deliveryInstructions',
        'deliveryLocations',
        'estimatedDelivery',
        'giftCards',
        'savedAddresses',
        'smsMobileNumber',
        'version',
      ],
      assocPath(['basket'], basket)(checkoutOrderSummary)
    )
    try {
      setPartialOrderSummary(partialOrderSummary)
    } catch (e) {
      // no need to log since we've captured storage capability above
    }
  })
}
