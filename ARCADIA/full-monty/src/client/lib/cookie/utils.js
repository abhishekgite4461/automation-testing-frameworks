export const removeItem = (key, path = '/') => {
  if (!process.browser) return null
  window.document.cookie = `${key}=; expires=Thu, 01 Jan 1970 00:00:01 GMT; Path=${path}`
}

export const getItem = (key) => {
  if (!process.browser) return null
  const cookie = ` ${window.document.cookie}`
  const regex = new RegExp(` ${key}=([^;]*[^;])`)
  const matches = cookie.match(regex)
  return (matches && matches.length && matches[1]) || undefined
}

export const setItem = (key, value, day) => {
  if (!process.browser) return null

  let expires = ''

  if (day) {
    const now = new Date()
    now.setTime(now.getTime() + day * 24 * 60 * 60 * 1000)
    expires = `;expires=${now.toGMTString()}`
  }

  window.document.cookie = `${key}=${value}${expires};path=/`
}

export const hasItem = (key) => (process.browser ? !!getItem(key) : null)
