import { errorReport, infoReport, debugReport } from './reporter'

export const error = (message, error) => errorReport(message, error)

export const info = (message, data) => infoReport(message, data)

export const debug = (message, data) => debugReport(message, data)

export const generateTransactionId = () =>
  `STRANS${Date.now()}R${Math.floor(Math.random() * 1000000, 0)}`
