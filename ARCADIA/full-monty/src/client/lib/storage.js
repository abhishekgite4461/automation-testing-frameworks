import { errorReport } from './reporter'
import { localStoragePaths } from '../../shared/constants/localStorage'
import { pathOr } from 'ramda'

const cachedItems = ['auth', 'account']
let storageSupported = null

export const isStorageSupported = () => {
  if (storageSupported === null) {
    try {
      sessionStorage.setItem('isStorageSupported', true)
      sessionStorage.removeItem('isStorageSupported')
      storageSupported = true
    } catch (e) {
      errorReport('storage', {
        loggerMessage: 'Browser storage not supported.',
      })
      storageSupported = false
    }
  }

  return storageSupported
}

export const getPartialOrderSummary = () => {
  const partialOrderSummaryString = sessionStorage.getItem(
    'partialOrderSummary'
  )
  if (partialOrderSummaryString) {
    try {
      return JSON.parse(partialOrderSummaryString)
    } catch (e) {
      errorReport('storage', e)
    }
  }
}

export const getSelectedStore = () => {
  const selectedStore = sessionStorage.getItem('selectedStore')
  if (selectedStore) {
    try {
      return JSON.parse(selectedStore)
    } catch (e) {
      errorReport('storage', e)
    }
  }
  return {}
}

export const setPartialOrderSummary = (partialOrderSummary) => {
  sessionStorage.setItem(
    'partialOrderSummary',
    JSON.stringify(partialOrderSummary)
  )
}

export const setSelectedStore = (selectedStore) => {
  sessionStorage.setItem('selectedStore', JSON.stringify(selectedStore))
}

export const getCacheData = (name) => {
  const data = cachedItems.reduce((prev, item) => {
    const storage = JSON.parse(localStorage.getItem(`cached_${item}`))
    return storage ? { ...prev, [item]: storage } : prev
  }, {})
  return name ? data[name] : data
}

export const setCacheData = (name, data) => {
  try {
    localStorage.setItem(`cached_${name}`, JSON.stringify(data))
  } catch (e) {
    errorReport('storage', {
      loggerMessage: 'Browser storage (localStorage) not supported',
      ...e,
    })
  }
}

export const clearCacheData = () => {
  cachedItems.forEach((item) => localStorage.removeItem(`cached_${item}`))
}

export const loadRecentlyViewedState = () => {
  try {
    const serializedState = localStorage.getItem(
      localStoragePaths.recentlyViewed
    )
    return serializedState === null ? [] : JSON.parse(serializedState)
  } catch (err) {
    return []
  }
}

export const saveRecentlyViewedState = (state) => {
  try {
    const serializedState = JSON.stringify(state)
    localStorage.setItem(localStoragePaths.recentlyViewed, serializedState)
  } catch (err) {
    console.error('There was an error while saving recently viewed products.') // eslint-disable-line
  }
}

export const saveShippingDestinationDetails = (details) => {
  sessionStorage.setItem('shippingDestination', JSON.stringify(details))
}

export const getShippingDestinationDetails = () => {
  const store = JSON.parse(sessionStorage.getItem('shippingDestination'))
  return pathOr('', ['destination'], store)
}
