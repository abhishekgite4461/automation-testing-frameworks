import { Server } from 'hapi'
import Inert from 'inert'
import vision from 'vision'
import handlebars from 'handlebars'
import basicAuth from 'hapi-auth-basic'
import { omit } from 'ramda'
import { basicAuthVerification } from './lib/auth'
import dictionaries from '../shared/constants/dictionaries'
import { setDictionaries } from '../shared/lib/localisation'
import routes from './routes'
import {
  onRequest,
  onPreResponse,
  cacheHeaders as cacheHeadersMiddleware,
  session as sessionMiddleware,
  decodeJwt,
  debug as debugMiddleware,
} from './lib/middleware'
import { validate } from './lib/request-validation'
import applyLogging from './lib/lifecycle-logging'
import { updateFeatures } from './lib/features-service'

const { JWT_SECRET } = process.env

if (!JWT_SECRET) {
  throw new Error('JWT_SECRET key has not been set')
}

export const prepareRoutes = (routes) =>
  routes
    // in production, remove routes flagged as devOnly
    .filter(
      (route) =>
        process.env.NODE_ENV !== 'production' ||
        process.env.WCS_ENVIRONMENT !== 'prod' ||
        (process.env.NODE_ENV === 'production' && !route.devOnly)
    )
    // omit additional properties as hapi doesn't like them
    .map(omit(['meta', 'devOnly']))

const server = new Server({
  connections: {
    routes: { security: { hsts: false } },
  },
})

setDictionaries(dictionaries)

server.register(vision, () => {
  server.connection({
    port: process.env.PORT || 3000,
    state: { strictHeader: false },
  })

  server.views({
    engines: { html: handlebars },
    relativeTo: __dirname,
    path: 'templates',
  })
  server.register(require('h2o2'))

  server.register([basicAuth, Inert], () => {
    server.auth.strategy('simple', 'basic', {
      validateFunc: basicAuthVerification,
    })
    server.route(prepareRoutes(routes))
  })

  server.ext('onRequest', onRequest)
  server.ext('onPreAuth', decodeJwt)
  server.ext('onPreHandler', validate)
  server.ext('onPreHandler', debugMiddleware)
  server.ext('onPreResponse', onPreResponse)
  server.ext('onPreResponse', cacheHeadersMiddleware)
  server.ext('onPreResponse', sessionMiddleware)

  applyLogging(server)

  updateFeatures()
})

export default server
