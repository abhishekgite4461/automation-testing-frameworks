import path, { resolve } from 'path'
import serverSideRenderer from './handlers/server-side-renderer'
import { assetHandler } from './handlers/asset-handler'
import {
  productsHandler,
  productsSearchHandler,
  productDetailsHandler,
  productEmailMeStock,
} from './handlers/product-handler'
import {
  registerAccountHandler,
  loginAccountHandler,
  customerDetails,
  changePasswordHandler,
  forgetPasswordHandler,
  shortProfileHandler,
  orderHistoryHandler,
  returnHistoryHandler,
  logoutAccountHandler,
  getAccount,
} from './handlers/account-handler'
import { categoriesHandler } from './handlers/navigation-handler'
import {
  addItemToBagHandler,
  getItemsFromBagHandler,
  deleteItemFromBagHandler,
  addPromotionCodeHandler,
  delPromotionCodeHandler,
  updateItemInBagHandler,
  fetchItemSizesAndQuantitiesHandler,
  emptyBagHandler,
  changeDeliveryType,
} from './handlers/shopping-bag-handler'
import { siteOptionsHandler } from './handlers/site-options-handler'
import {
  getOrderSummaryHandler,
  putOrderSummaryHandler,
  putBillingAddressHandler,
  postDeliveryAddressHandler,
  giftCardHandler,
} from './handlers/order-summary'
import {
  createSessionHandler,
  directKlarnaUpdateSessionHandler,
} from './handlers/klarna-handler'
import {
  paymentMethodsHandler,
  removePaymentHandler,
} from './handlers/payment-methods-handler'
import { postOrderHandler, putOrderHandler } from './handlers/order'
import {
  getStoresHandler,
  getCountriesHandler,
} from './handlers/store-locator-handler'
import * as address from './handlers/address'
import {
  getCmsFormHandler,
  cmsFormSubmitHandler,
  montyCmsFormSubmitHandler,
} from './handlers/cms-handler'
import { processReport } from './handlers/client-report-handler'
import { removeDiacritics, validateQuery } from './lib/middleware'
import featuresHandler, {
  consumerFeatureHandler,
  consumerFeatureHandlerSpec,
} from './handlers/features-handler'
import { getBrandConfig } from './config'
import { getEtagMethod } from '../shared/lib/cacheable-urls'
import { getStaticAssets } from './lib/get-static-assets'
import { getHealthCheckHandler } from './handlers/healthcheck'
import { getPlatformHealthCheckHandler } from './handlers/platform-healthcheck'
import {
  mrCmsContentHandler,
  mrCmsAssetsHandler,
} from './handlers/mr-cms-handler'
import { legacyCmsFeatureContentHandler } from './handlers/legacy-proxy-handler'
import routeHandler from '../server/api/handler'
import * as specs from '../server/api/specs'
import invalidateSessionKey from './handlers/invalidate-session-key'
import forceTimeout from './handlers/force-timeout'
import geoIPPixelHandler from './handlers/geo-ip-pixel'

const auth = process.env.BASIC_AUTH_ENABLED === 'true' ? 'simple' : false

const isAssetRequest = (param) => {
  return typeof param === 'string' && !param.includes('/')
}

const getBrandName = (hostname) => {
  return getBrandConfig(hostname).brandName
}

const getAssetPath = (assets, brandName, fileName) => {
  const brandAssets = assets[brandName]
  if (brandAssets && brandAssets.files.includes(fileName)) {
    return resolve(brandAssets.path, brandAssets.subFolder, fileName)
  }
}

/**
 * Returns a route that dispatches to WCS JSON/JSP endpoints if the "wcsPath=true" query param is set
 * else fallsback to scrAPI
 *
 * @param handler {Function} scrAPI handler
 * @return {Function} Dispatching hapi handler function
 */
function routeByWCSPathQueryParam(handler) {
  return (req, ...args) => {
    return req.query.wcsPath === 'true'
      ? routeHandler(req, ...args)
      : handler(req, ...args)
  }
}

function getHandler(scrApiHandler) {
  return process.env.USE_NEW_HANDLER === 'true'
    ? routeHandler
    : routeByWCSPathQueryParam(scrApiHandler)
}

export default [
  {
    method: 'GET',
    path: '/api/keep-alive',
    handler: routeHandler,
  },
  {
    devOnly: true,
    method: 'GET',
    path: '/api-docs/{param*}',
    handler: {
      directory: {
        path: path.join(__dirname, '../../swagger-ui'),
      },
    },
  },
  {
    method: 'GET',
    path: '/api/invalidate-session-key',
    devOnly: true,
    handler: invalidateSessionKey,
    meta: {
      excludeFromDocs: true,
    },
  },
  {
    method: 'GET',
    path: '/api/force-timeout',
    devOnly: true,
    handler: forceTimeout,
    meta: {
      excludeFromDocs: true,
    },
  },
  {
    method: 'POST',
    path: '/api/products/email-back-in-stock',
    handler: routeHandler,
    meta: {
      swagger: specs.emailMeInStockSpec,
    },
  },
  {
    // This is desktop specific (no scrAPI handler available)
    method: 'GET',
    path: '/api/desktop/navigation',
    handler: (req, reply) =>
      process.env.USE_NEW_HANDLER === 'true'
        ? routeHandler(req, reply)
        : reply({}),
  },
  {
    method: 'GET',
    path: '/cmscontent',
    handler: mrCmsContentHandler,
  },
  {
    method: 'GET',
    path: '/cmsfeaturecontent',
    handler: legacyCmsFeatureContentHandler,
  },
  {
    method: 'GET',
    path: '/assets/content',
    handler: mrCmsAssetsHandler,
  },
  {
    method: 'GET',
    path: '/assets/{pathName*}',
    handler: assetHandler,
  },
  {
    method: 'GET',
    path: '/api/{targetCountry}/products/{partNumber}',
    handler: routeHandler,
    meta: {
      swagger: specs.foreignProductFromPartNumberSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/products/{identifier}',
    handler: getHandler(productDetailsHandler),
    meta: {
      swagger: specs.productDetailsSpec,
    },
  },
  {
    method: 'GET',
    path: '/health',
    handler: getHealthCheckHandler,
  },
  {
    method: 'GET',
    path: '/platform-health',
    handler: getPlatformHealthCheckHandler,
  },
  {
    method: 'GET',
    path: '/api/products',
    handler: getHandler(productsHandler),
    meta: {
      swagger: specs.getProductsSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/products/seo/{param*}',
    handler: getHandler(productsSearchHandler),
    meta: {
      swagger: specs.productsFromSeoSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/products/promo',
    handler: routeHandler,
    meta: {
      swagger: specs.productsFromPromoSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/products/seemore',
    handler: routeHandler,
    meta: {
      swagger: specs.getSeeMoreSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/products/quickview',
    handler: routeHandler,
    meta: {
      swagger: specs.productQuickViewSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/account',
    handler: getHandler(getAccount),
    meta: {
      swagger: specs.accountSpec,
    },
  },
  {
    method: 'POST',
    path: '/api/account/register',
    handler: getHandler(registerAccountHandler),
    meta: {
      swagger: specs.registerSpec,
    },
  },
  {
    method: 'POST',
    path: '/api/account/login',
    handler: getHandler(loginAccountHandler),
    meta: {
      swagger: specs.logonSpec,
    },
  },
  {
    method: 'DELETE',
    path: '/api/account/logout',
    handler: getHandler(logoutAccountHandler),
    meta: {
      swagger: specs.logoutSpec,
    },
  },
  {
    method: 'PUT',
    path: '/api/account/changepassword',
    handler: getHandler(changePasswordHandler),
    meta: {
      swagger: specs.changePasswordSpec,
    },
  },
  {
    method: 'POST',
    path: '/api/account/forgetpassword',
    handler: getHandler(forgetPasswordHandler),
    meta: {
      swagger: specs.forgotPasswordSpec,
    },
  },
  {
    method: 'PUT',
    path: '/api/account/reset_password',
    handler: routeHandler,
    meta: {
      swagger: specs.resetPasswordSpec,
    },
  },
  {
    method: 'POST',
    path: '/api/account/reset_password_link',
    handler: routeHandler,
    meta: {
      swagger: specs.resetPasswordLinkSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/account/customerdetails',
    handler: customerDetails,
  },
  {
    method: 'PUT',
    path: '/api/account/customerdetails',
    config: {
      validate: process.env.USE_NEW_HANDLER === 'true' ? {} : removeDiacritics,
    },
    handler: getHandler(customerDetails),
    meta: {
      swagger: specs.updatePaymentDetailsSpec,
    },
  },
  {
    method: 'PUT',
    path: '/api/account/shortdetails',
    config: {
      validate: process.env.USE_NEW_HANDLER === 'true' ? {} : removeDiacritics,
    },
    handler: getHandler(shortProfileHandler),
    meta: {
      swagger: specs.updateProfileSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/account/order-history',
    handler: getHandler(orderHistoryHandler),
    meta: {
      swagger: specs.orderHistorySpec,
    },
  },
  {
    method: 'GET',
    path: '/api/account/order-history/{orderId}',
    handler: getHandler(orderHistoryHandler),
    meta: {
      swagger: specs.orderDetailsSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/account/return-history',
    handler: getHandler(returnHistoryHandler),
    meta: {
      swagger: specs.returnHistorySpec,
    },
  },
  {
    method: 'GET',
    path: '/api/account/return-history/{orderId}/{rmaId}',
    handler: getHandler(returnHistoryHandler),
    meta: {
      swagger: specs.returnDetailsSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/navigation/categories',
    handler: getHandler(categoriesHandler),
    meta: {
      swagger: specs.navigationSpec,
    },
  },
  {
    method: 'POST',
    path: '/api/shopping_bag/add_item',
    handler: getHandler(addItemToBagHandler),
    meta: {
      swagger: specs.addToBasketSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/shopping_bag/get_items',
    handler: getHandler(getItemsFromBagHandler),
    meta: {
      swagger: specs.getBasketSpec,
    },
  },
  {
    method: 'DELETE',
    path: '/api/shopping_bag/delete_item',
    handler: getHandler(deleteItemFromBagHandler),
    meta: {
      swagger: specs.removeFromBasketSpec,
    },
  },
  {
    method: 'DELETE',
    path: '/api/shopping_bag/empty_bag',
    handler: emptyBagHandler,
  },
  {
    method: 'POST',
    path: '/api/shopping_bag/transfer',
    handler: routeHandler,
    meta: {
      swagger: specs.transferBasketSpec,
    },
  },
  {
    method: 'POST',
    path: '/api/shopping_bag/addPromotionCode',
    handler: getHandler(addPromotionCodeHandler),
    meta: {
      swagger: specs.addPromoSpec,
    },
  },
  {
    method: 'DELETE',
    path: '/api/shopping_bag/delPromotionCode',
    handler: getHandler(delPromotionCodeHandler),
    meta: {
      swagger: specs.deletePromoSpec,
    },
  },
  {
    method: 'PUT',
    path: '/api/shopping_bag/update_item',
    handler: getHandler(updateItemInBagHandler),
    meta: {
      swagger: specs.updateItemSpec,
    },
  },
  {
    method: 'PUT',
    path: '/api/shopping_bag/delivery',
    handler: getHandler(changeDeliveryType),
    meta: {
      swagger: specs.updateDeliveryTypeSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/shopping_bag/fetch_item_sizes_and_quantities',
    handler: getHandler(fetchItemSizesAndQuantitiesHandler),
    meta: {
      swagger: specs.sizesAndQuantitiesSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/shopping_bag/mini_bag',
    handler: routeHandler,
    meta: {
      swagger: specs.miniBagSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/site-options',
    handler: getHandler(siteOptionsHandler),
    meta: {
      swagger: specs.siteOptionsSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/home',
    handler: routeHandler,
    meta: {
      swagger: specs.homeSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/payments',
    handler: paymentMethodsHandler,
    meta: {
      swagger: specs.getPaymentMethodsSpec,
    },
  },
  {
    method: 'DELETE',
    path: '/api/payments',
    handler: removePaymentHandler,
  },
  {
    method: 'POST',
    path: '/api/order',
    config: {
      validate: process.env.USE_NEW_HANDLER === 'true' ? {} : removeDiacritics,
    },
    handler: getHandler(postOrderHandler),
    meta: {
      swagger: specs.createOrderSpec,
    },
  },
  {
    method: 'PUT',
    path: '/api/order',
    handler: getHandler(putOrderHandler),
    meta: {
      swagger: specs.confirmOrderSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/checkout/order_summary',
    handler: getHandler(getOrderSummaryHandler),
    meta: {
      swagger: specs.orderSummarySpec,
    },
  },
  {
    method: 'PUT',
    path: '/api/checkout/order_summary',
    handler: getHandler(putOrderSummaryHandler),
    meta: {
      swagger: specs.deliverySelectorSpec,
    },
  },
  {
    method: 'PUT',
    path: '/api/checkout/order_summary/billing_address',
    handler: putBillingAddressHandler,
  },
  {
    method: 'POST',
    path: '/api/checkout/order_summary/delivery_address',
    handler: getHandler(postDeliveryAddressHandler),
    meta: {
      swagger: specs.addDeliveryAddressSpec,
    },
  },
  {
    method: 'PUT',
    path: '/api/checkout/order_summary/delivery_address',
    handler: routeHandler,
    meta: {
      swagger: specs.chooseSavedAddressSpec,
    },
  },
  {
    method: 'POST',
    path: '/api/checkout/gift-card',
    handler: getHandler(giftCardHandler),
    meta: {
      swagger: specs.addGiftCardSpec,
    },
  },
  {
    method: 'DELETE',
    path: '/api/checkout/gift-card',
    handler: getHandler(giftCardHandler),
    meta: {
      swagger: specs.deleteGiftCardSpec,
    },
  },
  {
    method: 'DELETE',
    path: '/api/checkout/order_summary/delivery_address',
    handler: routeHandler,
    meta: {
      swagger: specs.deleteDeliveryAddressSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/store-locator',
    handler: getStoresHandler,
  },
  {
    method: 'GET',
    path: '/api/stores-countries',
    handler: getCountriesHandler,
  },
  {
    method: 'GET',
    path: '/api/email-me-in-stock',
    handler: getHandler(productEmailMeStock),
    meta: {
      swagger: specs.notifyMeSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/address',
    config: { timeout: { server: 2000 } },
    handler: address.list,
  },
  {
    method: 'GET',
    path: '/api/address/{moniker}',
    config: { timeout: { server: 2000 } },
    handler: address.getByMoniker,
  },
  {
    method: 'GET',
    path: '/api/cms/seo',
    handler: routeHandler,
    meta: {
      swagger: specs.cmsSeoUrlSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/cms/page/{pageName}',
    handler: routeHandler,
    meta: {
      swagger: specs.cmsPageNameSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/cms/forms/{formName}',
    handler: getCmsFormHandler,
  },
  {
    method: 'POST',
    path: '/api/montycms/form/submit',
    handler: montyCmsFormSubmitHandler,
  },
  {
    method: 'POST',
    path: '/api/cms/form/submit',
    handler: cmsFormSubmitHandler,
  },
  {
    method: 'POST',
    path: '/api/client-{ltype}',
    handler: processReport,
  },
  {
    method: 'GET',
    path: '/api/features/{consumer}',
    handler: consumerFeatureHandler,
    meta: {
      swagger: consumerFeatureHandlerSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/features',
    handler: featuresHandler,
  },
  {
    method: 'POST',
    path: '/api/klarna-session',
    handler: getHandler(createSessionHandler),
    meta: {
      swagger: specs.klarnaSessionSpec,
    },
  },
  {
    method: 'PUT',
    path: '/api/klarna-session',
    handler: directKlarnaUpdateSessionHandler,
  },
  {
    method: 'POST',
    path: '/api/saved_basket',
    handler: routeHandler,
    meta: {
      swagger: specs.saveBasketSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/saved_basket',
    handler: routeHandler,
    meta: {
      swagger: specs.getSavedBasketSpec,
    },
  },
  {
    method: 'POST',
    path: '/api/saved_basket/item',
    handler: routeHandler,
    meta: {
      swagger: specs.saveFromBasketSpec,
    },
  },
  {
    method: 'PUT',
    path: '/api/saved_basket/item/restore',
    handler: routeHandler,
    meta: {
      swagger: specs.restoreSavedBasketItemSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/saved_basket/item/fetch_item_sizes_and_quantities',
    handler: routeHandler,
    meta: {
      swagger: specs.sizesAndQuantitiesSpec,
    },
  },
  {
    method: 'DELETE',
    path: '/api/saved_basket/item',
    handler: routeHandler,
    meta: {
      swagger: specs.deleteSavedItemSpec,
    },
  },
  {
    method: 'DELETE',
    path: '/api/saved_basket',
    handler: routeHandler,
    meta: {
      swagger: specs.deleteSavedBasketSpec,
    },
  },
  {
    method: 'PUT',
    path: '/api/saved_basket/item/update_item',
    handler: routeHandler,
    meta: {
      swagger: specs.updateSavedItemSpec,
    },
  },
  {
    method: 'POST',
    path: '/api/wishlist/create',
    handler: routeHandler,
    meta: {
      swagger: specs.createWishlistSpec,
    },
  },
  {
    method: 'POST',
    path: '/api/wishlist/add_item',
    handler: routeHandler,
    meta: {
      swagger: specs.addToWishlistSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/wishlist/item_ids',
    handler: routeHandler,
    meta: {
      swagger: specs.getWishlistItemIdsSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/wishlist',
    handler: routeHandler,
    meta: {
      swagger: specs.getWishlistSpec,
    },
  },
  {
    method: 'DELETE',
    path: '/api/wishlist/remove_item',
    handler: routeHandler,
    meta: {
      swagger: specs.removeFromWishlistSpec,
    },
  },
  {
    method: 'GET',
    path: '/api/wishlists',
    handler: routeHandler,
    meta: {
      swagger: specs.getAllWishlistsSpec,
    },
  },
  {
    method: 'POST',
    path: '/api/wishlist/add_to_bag',
    handler: routeHandler,
    meta: {
      swagger: specs.addToBasketFromWishlistSpec,
    },
  },
  {
    method: 'GET',
    path: '/robots.txt',
    handler: (req, reply) =>
      reply
        .file(path.join(__dirname, '../../public/robots.txt'))
        .type('text/plain'),
  },
  {
    method: 'GET',
    path: '/api/footers',
    handler: (req, reply) =>
      process.env.USE_NEW_HANDLER === 'true'
        ? routeHandler(req, reply)
        : reply({}),
  },
  {
    method: 'GET',
    path: '/api/geo-ip-pixel/{ISO}',
    handler: geoIPPixelHandler,
    meta: {
      excludeFromDocs: true,
    },
  },
  {
    method: 'GET',
    path: '/wcsstore/{param*}',
    handler: (req, reply) => reply('Invalid WCS URL').code(400),
  },
  {
    method: 'GET',
    path: '/{param*}',
    config: {
      auth,
      handler: function appRouteHandler(req, reply) {
        const {
          info: { hostname },
          params: { param },
        } = req

        if (isAssetRequest(param)) {
          const brandName = getBrandName(hostname)
          const assetPath = getAssetPath(this.staticAssets, brandName, param)

          // force a Monty style 404 page if no file with name `param` exists
          if (assetPath) {
            return reply.file(assetPath, {
              etagMethod: getEtagMethod('hapi', `/${param}`),
            })
          }
        }

        return serverSideRenderer(req, reply)
      },
      bind: { staticAssets: getStaticAssets() },
      validate: { query: validateQuery },
    },
  },
  {
    method: 'POST',
    path: '/order-complete',
    config: { auth, validate: { query: validateQuery } },
    handler: serverSideRenderer,
  },
]
