import paymentValidationSchema from './paymentValidationSchema'

const VISA = {
  value: 'VISA',
  type: 'CARD',
  label: 'Visa',
  description: 'Pay with VISA',
  icon: 'icon_visa_new.svg',
  validation: paymentValidationSchema.STANDARD,
}
const MASTERCARD = {
  value: 'MCARD',
  type: 'CARD',
  label: 'MasterCard',
  description: 'Pay with MasterCard',
  icon: 'icon_mastercard.svg',
  validation: paymentValidationSchema.STANDARD,
}
const AMEX = {
  value: 'AMEX',
  type: 'CARD',
  label: 'American Express',
  description: 'Pay with American Express',
  icon: 'icon_amex.svg',
  validation: paymentValidationSchema.AMEX,
}
const SWITCH = {
  value: 'SWTCH',
  type: 'CARD',
  label: 'Switch/Maestro',
  description: 'Pay with Switch / Maestro',
  icon: 'icon_switch_new.svg',
  validation: paymentValidationSchema.STANDARD,
}
const ACCOUNT = {
  value: 'ACCNT',
  type: 'OTHER_CARD',
  label: 'Account Card',
  description: 'Pay with Account Card',
  icon: 'icon_account-card.svg',
  validation: paymentValidationSchema.STANDARD,
}
const PAYPAL = {
  value: 'PYPAL',
  type: 'OTHER',
  label: 'PayPal',
  description: 'Check out with your PayPal account',
  icon: 'icon_paypal_new.svg',
}
const ALIPAY = {
  value: 'ALIPY',
  type: 'OTHER',
  label: 'AliPay',
  description: 'Check out with your AliPay account',
  icon: 'icon_alipay.svg',
  billingCountry: ['China'],
}
const CUPAY = {
  value: 'CUPAY',
  type: 'OTHER',
  label: 'China Union Pay',
  description: 'Check out with your China Union Pay account',
  icon: 'icon_cupay.svg',
  billingCountry: ['China'],
}
const IDEAL = {
  value: 'IDEAL',
  type: 'OTHER',
  label: 'Ideal',
  description: 'Check out with your IDEAL account',
  icon: 'icon_ideal.svg',
  billingCountry: ['Netherlands'],
}
const SOFORT = {
  value: 'SOFRT',
  type: 'OTHER',
  label: 'Sofort',
  description: 'Check out with your SOFORT account',
  icon: 'icon_sofort.svg',
  billingCountry: ['Austria', 'Belgium', 'France', 'Germany'],
}
const MASTERPASS = {
  value: 'MPASS',
  type: 'OTHER',
  label: 'Masterpass',
  description: 'Check out with your Masterpass account',
  icon: 'icon_masterpass.svg',
}
const KLARNA_UK = {
  value: 'KLRNA',
  type: 'OTHER',
  threshold: 0,
  label: 'Try before you buy',
  description: 'Get your goods today and pay 3 months later',
  icon: 'icon_klarna.svg',
  region: 'uk',
  deliveryCountry: ['United Kingdom'],
  billingCountry: ['United Kingdom'],
}
const KLARNA_DE = {
  value: 'KLRNA',
  type: 'OTHER',
  threshold: 0,
  label: 'Try before you buy',
  description: 'Get your goods today and pay 3 months later',
  icon: 'icon_klarna.svg',
  region: 'de',
  deliveryCountry: ['Germany'],
  billingCountry: ['Germany'],
}

/**
 * The supported payment methods for each brand in each region
 */
export const paymentsConfig = {
  ts: {
    uk: [
      VISA,
      MASTERCARD,
      AMEX,
      SWITCH,
      ACCOUNT,
      PAYPAL,
      MASTERPASS,
      ALIPAY,
      CUPAY,
      KLARNA_UK,
    ],
    us: [VISA, MASTERCARD, AMEX, PAYPAL],
    eu: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL, IDEAL, SOFORT],
    fr: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL, IDEAL, SOFORT],
    de: [VISA, MASTERCARD, AMEX, PAYPAL, KLARNA_DE, IDEAL, SOFORT],
    my: [VISA, MASTERCARD, SWITCH],
    sg: [VISA, MASTERCARD, SWITCH],
    th: [VISA, MASTERCARD, SWITCH],
  },
  tm: {
    uk: [
      VISA,
      MASTERCARD,
      AMEX,
      SWITCH,
      ACCOUNT,
      PAYPAL,
      MASTERPASS,
      ALIPAY,
      CUPAY,
      KLARNA_UK,
    ],
    us: [VISA, MASTERCARD, AMEX, PAYPAL],
    eu: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL, IDEAL, SOFORT],
    fr: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL, IDEAL, SOFORT],
    de: [VISA, MASTERCARD, AMEX, PAYPAL, KLARNA_DE, IDEAL, SOFORT],
    my: [VISA, MASTERCARD, SWITCH],
    sg: [VISA, MASTERCARD, SWITCH],
    th: [VISA, MASTERCARD, SWITCH],
  },
  dp: {
    uk: [
      VISA,
      MASTERCARD,
      AMEX,
      SWITCH,
      ACCOUNT,
      PAYPAL,
      MASTERPASS,
      ALIPAY,
      CUPAY,
      KLARNA_UK,
    ],
    us: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL],
    eu: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL, IDEAL, SOFORT],
    // @NOTE Currently there are no mobile sites for dpfr or dpde
    // fr: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL, IDEAL],
    // de: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL, KLARNA_DE, IDEAL],
    my: [VISA, MASTERCARD, SWITCH],
    sg: [VISA, MASTERCARD, SWITCH],
    th: [VISA, MASTERCARD, SWITCH],
  },
  ms: {
    uk: [
      VISA,
      MASTERCARD,
      AMEX,
      SWITCH,
      ACCOUNT,
      PAYPAL,
      MASTERPASS,
      ALIPAY,
      CUPAY,
      KLARNA_UK,
    ],
    us: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL],
    eu: [VISA, MASTERCARD, SWITCH, PAYPAL, IDEAL, SOFORT],
    fr: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL, IDEAL, SOFORT],
    de: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL, KLARNA_DE, IDEAL, SOFORT],
  },
  wl: {
    uk: [
      VISA,
      MASTERCARD,
      AMEX,
      SWITCH,
      ACCOUNT,
      PAYPAL,
      MASTERPASS,
      ALIPAY,
      CUPAY,
      KLARNA_UK,
    ],
    us: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL],
    eu: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL, IDEAL, SOFORT],
    de: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL, KLARNA_DE, IDEAL, SOFORT],
  },
  ev: {
    uk: [
      VISA,
      MASTERCARD,
      AMEX,
      SWITCH,
      ACCOUNT,
      PAYPAL,
      MASTERPASS,
      ALIPAY,
      CUPAY,
      KLARNA_UK,
    ],
    us: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL],
    eu: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL, IDEAL, SOFORT],
    de: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL, KLARNA_DE, IDEAL, SOFORT],
  },
  br: {
    uk: [
      VISA,
      MASTERCARD,
      AMEX,
      SWITCH,
      ACCOUNT,
      PAYPAL,
      MASTERPASS,
      ALIPAY,
      CUPAY,
      KLARNA_UK,
    ],
    eu: [VISA, MASTERCARD, AMEX, SWITCH, PAYPAL, IDEAL, SOFORT],
  },
}

export const allPaymentsList = [
  VISA,
  MASTERCARD,
  AMEX,
  SWITCH,
  ACCOUNT,
  PAYPAL,
  ALIPAY,
  CUPAY,
  KLARNA_UK,
  KLARNA_DE,
  MASTERPASS,
  IDEAL,
  SOFORT,
]
