// @flow
import topshop from './brands/topshop.json'
import topman from './brands/topman.json'
import wallis from './brands/wallis.json'
import burton from './brands/burton.json'
import dorothyPerkins from './brands/dorothy-perkins.json'
import evans from './brands/evans.json'
import missSelfidge from './brands/miss-selfridge.json'

import paymentSchema from '../../shared/constants/paymentSchema'
import customerDetailsSchema from '../../shared/constants/customerDetailsSchema'
import checkoutAddressFormRules from '../../shared/constants/checkoutAddressFormRules'
import qasCountries from '../../shared/constants/qasCountries'
import R, { chain } from 'ramda'

export const configs = [
  ...topshop,
  ...topman,
  ...wallis,
  ...burton,
  ...dorothyPerkins,
  ...evans,
  ...missSelfidge,
]

const DEFAULT = {
  paymentSchema,
  customerDetailsSchema,
  checkoutAddressFormRules,
  qasCountries,
  logoVersion: '19102018',
}

const replaceDotsWithDashes = R.replace(/\./g, '-')
const sanitizeConfig = (siteConfig) => {
  // TODO defaulting to topshop is dangerous, Monty does not exists without a brand
  // Consider removing the following fallback
  if (siteConfig === undefined) {
    siteConfig = { ...topshop[0] }
  }
  return { ...DEFAULT, ...siteConfig }
}

export function getConfigByStoreCode(code) {
  return sanitizeConfig(configs.find(({ storeCode }) => storeCode === code))
}

const matchConfigByDomain = (config, match) => {
  return Object.values(config.domains).some((envDomains) =>
    envDomains.some((domain) => match(domain))
  )
}

export function getBrandConfig(hostname) {
  let siteConfig
  let host
  const isAkamaiDevEnv = hostname.match(/^[^.]+\.m\./i)
  if (isAkamaiDevEnv) {
    host = hostname.split('.')
    host.shift()
    host = replaceDotsWithDashes(host.join('.'))
    siteConfig = configs.find((config) =>
      matchConfigByDomain(
        config,
        (domain) => replaceDotsWithDashes(domain) === host
      )
    )
  } else if (hostname.includes('.arcadiagroup.co.uk')) {
    siteConfig = configs.find((config) =>
      matchConfigByDomain(config, (domain) =>
        replaceDotsWithDashes(hostname).includes(replaceDotsWithDashes(domain))
      )
    )
  } else {
    siteConfig = configs.find((config) =>
      matchConfigByDomain(
        config,
        (domain) =>
          replaceDotsWithDashes(hostname) === replaceDotsWithDashes(domain)
      )
    )
  }
  return sanitizeConfig(siteConfig)
}

export const isMDotDomain = (domain) => /^m\.|\.m\.|^m-|-m-/.test(domain)

/**
 * Given an array of configs, returns pairs of environments to related domains
 * @param  {Array<Config>} configs
 * @return {Array<Array>}
 * [
 *   ['prod', ['m.topshop.com', 'www.topshop.com']],
 *   ['EBT', ['www.topshop.com.arcadiagroup.co.uk']],
 *   ['prod', ['m.us.topshop.com', 'us.topshop.com']],
 *   ['EBT', ['us.topshop.com.arcadiagroup.co.uk']],
 * ]
 */
const getAllSitesEnvDomainsPairs = (configs) =>
  chain((config) => R.toPairs(config.domains), configs)

/**
 * Given the `envDomainsPairs` array and `domain`, finds the environment that the domain belongs to
 * @param  {Array<Array>} envDomainsPairs
 * @param  {String} domain
 * @return {String|undefined}
 */
const getEnvFromDomain = (envDomainsPairs, domain) =>
  (envDomainsPairs.find(([, envDomains]) => envDomains.includes(domain)) ||
    [])[0]

export function getBrandHostnames(brandName, domain) {
  const isMDot = isMDotDomain(domain)
  const brandConfigs = configs.filter(
    (config) => config.brandName === brandName
  )

  const envDomainsPairs = getAllSitesEnvDomainsPairs(brandConfigs)
  const givenEnv = getEnvFromDomain(envDomainsPairs, domain)

  if (!givenEnv) {
    // domain is likely a dev environment domain that isn't in the brand configs  ¯\_(ツ)_/¯
    return getBrandHostnames(
      brandName,
      brandConfigs[0].domains.prod.find((hostname) => {
        return isMDot ? isMDotDomain(hostname) : !isMDotDomain(hostname)
      })
    )
  }

  return R.zipWith(
    ([env], config) => ({
      hostname: config.domains[env].find(
        (hostname) => isMDotDomain(hostname) === isMDot
      ),
      defaultLanguage: config.defaultLanguage,
      country: config.country,
    }),
    envDomainsPairs.filter(([env]) => env === givenEnv),
    brandConfigs
  ).reduce((acc, { hostname, defaultLanguage, country }) => {
    acc[country] = { hostname, defaultLanguage }
    return acc
  }, {})
}

export function getBrandConfigByPreferredISO(ISO, brandCode) {
  const brandConfigs = configs.filter(
    (config) => config.brandCode === brandCode
  )
  if (brandConfigs.length === 0) return null

  return (
    brandConfigs.find(({ preferredISOs }) => preferredISOs.includes(ISO)) ||
    brandConfigs.find(({ preferredISOs }) => preferredISOs.includes('GB'))
  )
}

export function getWCSDomainFromConfig(config) {
  if (R.type(config) !== 'Object')
    throw new Error('Expected a brand config object')

  // At the moment this will provide a production WCS domain
  // in an ideal world we'd provide a WCS domain of the same environment as being requested
  // however that requires changing the CoreAPI base Mapper class and handler.
  // I've raised PTM-255 to address this.
  return config.domains.prod[1]
}

/**
 * Returns an array of all the sites' hostnames for a given brand
 *
 * @param  {String} brandName The brand name of the brand to get hostnames for
 * @return {Array<String>} Array of domains
 */
export function getAllBrandHostnames(brandName, domain) {
  if (!brandName) throw new Error('Missing `brandName` parameter')

  const brandConfigs = configs.filter((conf) => conf.brandName === brandName)
  if (!brandConfigs.length)
    throw new Error(`Invalid \`brandName\` given of ${brandName}`)

  const envDomainsPairs = getAllSitesEnvDomainsPairs(brandConfigs)
  const env = getEnvFromDomain(envDomainsPairs, domain)
  if (!env)
    return getAllBrandHostnames(brandName, brandConfigs[0].domains.prod[0])

  return R.uniq(
    brandConfigs.reduce((acc, conf) => {
      return acc.concat(conf.domains[env])
    }, [])
  )
}

/**
 * Returns the list of domains that match the environment of the given domain
 * @param  {Config} config
 * @param  {String} domain
 * @return {Array<String>} Array of domains
 */
export const getEnvDomainsFromConfig = (config, domain) => {
  if (Object.prototype.toString.call(config) !== '[object Object]')
    throw new Error(`Invalid config: ${config}`)
  if (Object.prototype.toString.call(domain) !== '[object String]')
    throw new Error(`Invalid domain: ${domain}`)

  return (
    Object.values(config.domains).find((domains) => domains.includes(domain)) ||
    config.domains.prod.slice()
  )
}

export const getFirstPreferredISO = (config) => config.preferredISOs[0]
