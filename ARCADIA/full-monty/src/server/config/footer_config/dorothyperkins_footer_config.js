// FOOTER CONFIG FOR DOROTHY PERKINS
// See comments in footer_config/topshop_footer_config.js for an explanation of each section
// ============================================================================================

export const defaultConfig = {
  // Newsletter
  // =================================================================
  newsletter: {
    isVisible: true,
    location: 'TOP_CENTER',
  },

  // Social Links
  // =================================================================
  socialLinks: {
    isVisible: true,
    location: 'TOP_CENTER',
    links: [
      {
        fileName: 'icon-facebook.svg',
        linkUrl: 'https://www.facebook.com/dorothyperkins',
      },
      {
        fileName: 'icon-twitter.svg',
        linkUrl: 'https://twitter.com/dorothy_perkins',
      },
      {
        fileName: 'icon-pinterest.svg',
        linkUrl: 'https://uk.pinterest.com/dorothyperkins',
      },
      {
        fileName: 'icon-instagram.svg',
        linkUrl: 'https://www.instagram.com/dorothyperkins',
      },
      {
        fileName: 'icon-youtube.svg',
        linkUrl: 'http://www.youtube.com/user/OfficialDP',
      },
      {
        fileName: 'icon-googleplus.svg',
        linkUrl: 'https://plus.google.com/+dorothyperkins/posts',
      },
      {
        fileName: 'icon-theblog.svg',
        linkUrl: 'http://www.dorothyperkins.com/blog/',
      },
    ],
  },

  // Bottom Content
  // =================================================================
  bottomContent: {
    // Bottom Content - LEFT SIDE
    // This array can contain image(s) or text
    // =================================================================
    left: [
      {
        fileName: 'visa.svg',
        openNewWindow: false,
        alt: 'Visa',
        linkUrl: '',
      },
      {
        fileName: 'mastercard.svg',
        openNewWindow: false,
        alt: 'Mastercard',
        linkUrl: '',
      },
      {
        fileName: 'visaelectron.svg',
        openNewWindow: false,
        alt: 'Visa Electron',
        linkUrl: '',
      },
      {
        fileName: 'amex.svg',
        openNewWindow: false,
        alt: 'Amex',
        linkUrl: '',
      },
      {
        fileName: 'maestro.svg',
        openNewWindow: false,
        alt: 'Maestro',
        linkUrl: '',
      },
      {
        fileName: 'dorothyperkinsmastercard.svg',
        openNewWindow: false,
        alt: 'Mastercard',
        linkUrl: '',
      },
      {
        fileName: 'paypal.svg',
        openNewWindow: false,
        alt: 'PayPal',
        linkUrl: '',
      },
      {
        fileName: 'masterpass.svg',
        openNewWindow: false,
        alt: 'MasterPass',
        linkUrl: '',
      },
      {
        fileName: 'klarna.svg',
        openNewWindow: false,
        alt: 'Klarna',
        linkUrl: '',
      },
    ],

    // Bottom Content - RIGHT SIDE
    // This array can contain image(s) or text
    // =================================================================
    right: [
      {
        fileName: 'verisigntrusted.svg',
        openNewWindow: false,
        alt: 'Verisign Trusted',
        linkUrl: '',
      },
      {
        fileName: 'verifiedbyvisa.svg',
        openNewWindow: false,
        alt: 'Verified by Visa',
        linkUrl: '',
      },
      {
        fileName: 'mastercardsecurecode.svg',
        openNewWindow: false,
        alt: 'Mastercard Securecode',
        linkUrl: '',
      },
      {
        fileName: 'comodosecure.svg',
        openNewWindow: false,
        alt: 'Comodo Secure',
        linkUrl: '',
      },
    ],
  },
}

/* =================== region: uk =================== */
export const uk = {
  ...defaultConfig,
  /*
    --- you can override config here ---
  */
}
