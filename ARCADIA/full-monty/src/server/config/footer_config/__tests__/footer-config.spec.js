import { getFooterConfig } from '../'

const defaultObject = {
  newsletter: {},
  socialLinks: {},
  bottomContent: {},
}

describe('footer-config', () => {
  it('results in correct footer-config for topshop de', () => {
    expect(getFooterConfig('topshop', 'de')).toMatchSnapshot()
  })
  it('results in correct footer-config for topman fr', () => {
    expect(getFooterConfig('topman', 'fr')).toMatchSnapshot()
  })
  it('results in correct footer-config for burton us', () => {
    expect(getFooterConfig('burton', 'us')).toMatchSnapshot()
  })
  it('results in correct footer-config for wallis uk', () => {
    expect(getFooterConfig('wallis', 'uk')).toMatchSnapshot()
  })
  it('results in correct footer-config for evans de', () => {
    expect(getFooterConfig('evans', 'de')).toMatchSnapshot()
  })
  it('results in correct footer-config for missselfridge uk', () => {
    expect(getFooterConfig('missselfridge', 'uk')).toMatchSnapshot()
  })
  it('results in correct footer-config for dorothyperkins uk', () => {
    expect(getFooterConfig('dorothyperkins', 'uk')).toMatchSnapshot()
  })
  it('returns with `defaultObject` if brand or region are invalid', () => {
    expect(getFooterConfig('dorothyperkins', 'invalid-region')).toMatchObject(
      defaultObject
    )
    expect(getFooterConfig('invalid-brand', 'uk')).toMatchObject(defaultObject)
  })
})
