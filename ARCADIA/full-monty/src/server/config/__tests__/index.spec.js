import { uniq } from 'ramda'
import {
  getBrandConfigByPreferredISO,
  getWCSDomainFromConfig,
  getBrandConfig,
  getAllBrandHostnames,
  getBrandHostnames,
  configs,
  isMDotDomain,
  getEnvDomainsFromConfig,
} from '../'
import topshop from '../brands/topshop.json'
import burton from '../brands/burton.json'
import dp from '../brands/dorothy-perkins.json'
import ev from '../brands/evans.json'

describe('getBrandConfig', () => {
  it('check that correct evans brand is matched for m.euro.evansfashion.com url.', () => {
    const brand = getBrandConfig('m.euro.evansfashion.com')
    expect('Evans EU').toEqual(brand.name)
  })
  it('check that correct evans brand is matched for www.evansfashion.com url.', () => {
    const brand = getBrandConfig('www.evansfashion.com')
    expect('Evans UK').toEqual(brand.name)
  })
  it('check that correct evans brand is matched for evansfashion.com url.', () => {
    const brand = getBrandConfig('evansfashion.com')
    expect('Evans UK').toEqual(brand.name)
  })
  it('check that default brand given is topshop if no match.', () => {
    const brand = getBrandConfig('www.example.com')
    expect('Topshop UK').toEqual(brand.name)
  })
  it('check if i receive the correct brand name for DP fr', () => {
    const brand = getBrandConfig('m.dorothyperkins.fr')
    expect('Dorothy Perkins FR').toEqual(brand.name)
  })
  it('check if i receive the correct brand name for DP de', () => {
    const brand = getBrandConfig('de.dorothyperkins.com')
    expect('Dorothy Perkins DE').toEqual(brand.name)
  })
  it('check that develop url still works with perf-m-topman-com.digital.arcadiagroup.co.uk.', () => {
    const brand = getBrandConfig('perf-m-topman-com.digital.arcadiagroup.co.uk')
    expect('Topman UK').toEqual(brand.name)
  })
  it('check that production url still works with perf-m-topman-com.digital-prod.arcadiagroup.co.uk.', () => {
    const brand = getBrandConfig(
      'perf-m-topman-com.digital-prod.arcadiagroup.co.uk'
    )
    expect('Topman UK').toEqual(brand.name)
  })
  it('check that searching for develop environment that does not exist still returns topshop.', () => {
    const brand = getBrandConfig(
      'perf-m-example-com.digital.arcadiagroup.co.uk'
    )
    expect('Topshop UK').toEqual(brand.name)
  })
  it('check that Akamai environments for perf topshop still returns topman.', () => {
    const brand = getBrandConfig('perf.m.topman.com')
    expect('Topman UK').toEqual(brand.name)
  })
  it('check that integration.m.euro.evansfashion.com returns the correct brand.', () => {
    const brand = getBrandConfig('integration.m.euro.evansfashion.com')
    expect('Evans EU').toEqual(brand.name)
  })
  it('check New website-config (DE-FR-TH-MY) use dummy openTags', () => {
    const brand = getBrandConfig('m.th.dorothyperkins.com')
    expect('montydummy').toEqual(brand.opentagRef)
  })
})

describe('getBrandConfigByPreferredISO', () => {
  ;[topshop, burton, dp, ev].forEach((brand) => {
    it(`gets the relevant brand config from the preferred ISO code provided (${
      brand[0].brandName
    })`, () => {
      const expected = brand[0]
      const actual = getBrandConfigByPreferredISO('GB', brand[0].brandCode)
      expect(actual).toEqual(expected)
    })
  })
  ;[topshop, dp, ev].forEach((brand) => {
    it(`gets the US brand config from the preferred ISO code provided (${
      brand[0].brandName
    })`, () => {
      const preferredISO = 'US'
      const expected = brand.find(({ preferredISOs }) =>
        preferredISOs.includes(preferredISO)
      )
      const actual = getBrandConfigByPreferredISO(
        preferredISO,
        expected.brandCode
      )
      expect(actual).toEqual(expected)
    })
  })

  it('gets the default config for missing preferred ISO', () => {
    expect(getBrandConfigByPreferredISO('tv', 'ts')).toEqual(topshop[0])
    expect(getBrandConfigByPreferredISO('RUBBISH_ISO', 'ts')).toEqual(
      topshop[0]
    )
  })

  it('invalid brand code returns null', () => {
    expect(getBrandConfigByPreferredISO('GB', 'FOO')).toEqual(null)
  })
})

describe('getWCSDomainFromConfig', () => {
  it('returns the WCS domain given a brand config object', () => {
    expect(getWCSDomainFromConfig(topshop[0])).toBe('www.topshop.com')
    expect(getWCSDomainFromConfig(burton[0])).toBe('www.burton.co.uk')
  })

  it('throws an error if not passed a config object', () => {
    ;[undefined, 1, [], new Map(), ''].forEach((arg) => {
      expect(() => getWCSDomainFromConfig(arg)).toThrow(
        'Expected a brand config object'
      )
    })
  })
})

describe('getAllBrandHostnames', () => {
  it('returns all hostnames for a given brand', () => {
    expect(getAllBrandHostnames('topshop', 'm.topshop.com')).toEqual([
      'm.topshop.com',
      'www.topshop.com',
      'm.us.topshop.com',
      'us.topshop.com',
      'm.de.topshop.com',
      'de.topshop.com',
      'm.fr.topshop.com',
      'fr.topshop.com',
      'm.eu.topshop.com',
      'eu.topshop.com',
      'm.sg.topshop.com',
      'sg.topshop.com',
      'm.my.topshop.com',
      'my.topshop.com',
      'm.th.topshop.com',
      'th.topshop.com',
    ])
  })

  it('invalid brandName', () => {
    expect(() => getAllBrandHostnames()).toThrow(
      'Missing `brandName` parameter'
    )
    expect(() => getAllBrandHostnames('asos')).toThrow(
      'Invalid `brandName` given of asos'
    )
  })

  it('domain missing in configuration returns prod domains', () => {
    expect(getAllBrandHostnames('topshop', 'foobar.m.topshop.com')).toEqual([
      'm.topshop.com',
      'www.topshop.com',
      'm.us.topshop.com',
      'us.topshop.com',
      'm.de.topshop.com',
      'de.topshop.com',
      'm.fr.topshop.com',
      'fr.topshop.com',
      'm.eu.topshop.com',
      'eu.topshop.com',
      'm.sg.topshop.com',
      'sg.topshop.com',
      'm.my.topshop.com',
      'my.topshop.com',
      'm.th.topshop.com',
      'th.topshop.com',
    ])
  })
  it('returns prod DP domains', () => {
    expect(
      getAllBrandHostnames('dorothyperkins', 'foobar.m.dorothyperkins.com')
    ).toEqual([
      'm.dorothyperkins.com',
      'www.dorothyperkins.com',
      'm.euro.dorothyperkins.com',
      'euro.dorothyperkins.com',
      'm.us.dorothyperkins.com',
      'us.dorothyperkins.com',
      'm.dorothyperkins.fr',
      'www.dorothyperkins.fr',
      'm.de.dorothyperkins.com',
      'de.dorothyperkins.com',
      'm.sg.dorothyperkins.com',
      'sg.dorothyperkins.com',
      'm.my.dorothyperkins.com',
      'my.dorothyperkins.com',
      'm.th.dorothyperkins.com',
      'th.dorothyperkins.com',
    ])
  })
})

describe('getBrandHostnames', () => {
  const genBrandName = gen.oneOf(uniq(configs.map((conf) => conf.brandName)))
  const genBrandNameAndDomain = genBrandName.then((brandName) =>
    gen.object({
      brandName,
      domain: gen
        .oneOf(configs.filter((conf) => conf.brandName === brandName))
        .then((conf) => gen.oneOf(Object.values(conf.domains)))
        .then((envDomains) => gen.oneOf(envDomains)),
    })
  )

  check.it(
    'each have a hostname and defaultLanguage',
    genBrandNameAndDomain,
    ({ brandName, domain }) => {
      Object.values(getBrandHostnames(brandName, domain)).forEach(
        (langHostname) => {
          expect(langHostname.hostname).toMatch(/.+/)
          expect(langHostname.defaultLanguage).toMatch(/.+/)
        }
      )
    }
  )

  check.it(
    'supported languages only',
    genBrandNameAndDomain,
    ({ brandName, domain }) => {
      Object.values(getBrandHostnames(brandName, domain)).forEach(
        (langHostname) => {
          expect(langHostname.defaultLanguage).toBeInArray([
            'English',
            'German',
            'French',
          ])
        }
      )
    }
  )

  check.it('must have UK', genBrandNameAndDomain, ({ brandName, domain }) => {
    const langHostnames = getBrandHostnames(brandName, domain)
    expect(langHostnames['United Kingdom']).toBeInstanceOf(Object)
  })

  check.it(
    'unique country keys',
    genBrandNameAndDomain,
    ({ brandName, domain }) => {
      const countries = Object.keys(getBrandHostnames(brandName, domain))
      expect(uniq(countries)).toEqual(countries)
    }
  )

  check.it(
    'has default property where English is the defaultLanguage',
    genBrandNameAndDomain,
    ({ brandName, domain }) => {
      const langHostnames = getBrandHostnames(brandName, domain)
      expect(langHostnames.default).toBeInstanceOf(Object)
      expect(langHostnames.default.defaultLanguage).toBe('English')
    }
  )

  check.it(
    'had nonEU property where English is the defaultLanguage',
    genBrandNameAndDomain,
    ({ brandName, domain }) => {
      const langHostnames = getBrandHostnames(brandName, domain)
      expect(langHostnames.nonEU).toBeInstanceOf(Object)
      expect(langHostnames.nonEU.defaultLanguage).toBe('English')
    }
  )

  const genBrandNameAndMobileDomain = genBrandName.then((brandName) =>
    gen.object({
      brandName,
      domain: gen
        .oneOf(configs.filter((conf) => conf.brandName === brandName))
        .then((conf) =>
          conf.domains.prod.find((domain) => isMDotDomain(domain))
        ),
    })
  )
  check.it(
    'matches m.* domains',
    genBrandNameAndMobileDomain,
    ({ brandName, domain }) => {
      Object.values(getBrandHostnames(brandName, domain)).forEach(
        (langHostname) => {
          expect(isMDotDomain(langHostname.hostname)).toBe(true)
        }
      )
    }
  )

  const genBrandNameAndDesktopDomain = genBrandName.then((brandName) =>
    gen.object({
      brandName,
      domain: gen
        .oneOf(configs.filter((conf) => conf.brandName === brandName))
        .then((conf) =>
          conf.domains.prod.find((domain) => !isMDotDomain(domain))
        ),
    })
  )
  check.it(
    'matches non m.* domains',
    genBrandNameAndDesktopDomain,
    ({ brandName, domain }) => {
      Object.values(getBrandHostnames(brandName, domain)).forEach(
        (langHostname) => {
          expect(isMDotDomain(langHostname.hostname)).toBe(false)
        }
      )
    }
  )

  const genBrandNameAndMobileAkamaiDevDomain = genBrandName.then((brandName) =>
    gen.object({
      brandName,
      domain: gen
        .oneOf(configs.filter((conf) => conf.brandName === brandName))
        .then(
          (conf) =>
            `stage.${conf.domains.prod.find((domain) => isMDotDomain(domain))}`
        ),
    })
  )
  check.it(
    'produces prod mobile hostnames for local and mobile akamai dev domains',
    genBrandNameAndMobileAkamaiDevDomain,
    ({ brandName, domain }) => {
      Object.values(getBrandHostnames(brandName, domain)).forEach(
        (langHostname) => {
          expect(langHostname.hostname).toMatch(/^m\./)
        }
      )
    }
  )

  const genBrandNameAndMobileAWSDomain = genBrandName.then((brandName) =>
    gen.object({
      brandName,
      domain: gen
        .oneOf(configs.filter((conf) => conf.brandName === brandName))
        .then(
          (conf) =>
            `${conf.domains.prod
              .find((domain) => isMDotDomain(domain))
              .replace(/\./g, '-')}.arcadiagroup.co.uk`
        ),
    })
  )
  check.it(
    'produces prod mobile hostnames for mobile aws dev domains',
    genBrandNameAndMobileAWSDomain,
    ({ brandName, domain }) => {
      Object.values(getBrandHostnames(brandName, domain)).forEach(
        (langHostname) => {
          expect(langHostname.hostname).toMatch(/^m\./)
        }
      )
    }
  )
})

describe('getEnvDomainsFromConfig', () => {
  const config = topshop[0]
  const domain = 'm.topshop.com'
  it("gets the domains relating to a particular domain's environment", () => {
    expect(getEnvDomainsFromConfig(config, domain)).toEqual([
      'm.topshop.com',
      'www.topshop.com',
    ])
  })

  it('throws for missing args', () => {
    const config = topshop[0]
    expect(() => getEnvDomainsFromConfig()).toThrow('Invalid config: undefined')
    expect(() => getEnvDomainsFromConfig(config)).toThrow(
      'Invalid domain: undefined'
    )
  })

  it('returns prod domains for unconfigured domain', () => {
    expect(getEnvDomainsFromConfig(config, 'foobar.m.topshop.com')).toEqual([
      'm.topshop.com',
      'www.topshop.com',
    ])
  })
})
