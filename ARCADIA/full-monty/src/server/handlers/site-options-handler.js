import arcadiaApi from './lib/arcadia-api'

/*
 * Get All Site Options from API
 */
export function siteOptionsHandler(req, reply) {
  const url = 'site-options'

  return arcadiaApi(url, req)
    .then((result) => {
      return reply(result.body).code(200)
    })
    .catch(reply)
}
