import * as logger from '../lib/logger'
import { omit } from 'ramda'

export const processReport = (req, reply) => {
  const ltype = req.params.ltype
  if (logger[ltype]) {
    logger[ltype](
      `client-report:${req.payload.namespace}`,
      omit(['namespace'], req.payload)
    )
  }
  reply()
}
