// This code allows us to simulate a WCS timeout in dev
// you can curl /api/force-timeout to cause the timeout

let _isTimeoutTriggered = false

/**
 * Simulates a WCS timeout and disarms the timeout trigger
 *
 * @returns a rejected promise with the value of a timeout error
 */
export function simulateTimeout() {
  _isTimeoutTriggered = false
  return Promise.resolve({
    body: {
      timeout: true,
    },
    status: 200,
  })
}

/**
 * Returns true if the simulated timeout is triggered to run, otherwise returns false.
 *
 * @returns {Boolean} is the timeout triggered
 */
export function isTimeoutTriggered() {
  return _isTimeoutTriggered
}

// sets a global flag to simulate a timeout
export default (req, reply) => {
  _isTimeoutTriggered = true
  reply({ success: true })
}
