import arcadiaApi from './lib/arcadia-api'

export function getOrderSummaryHandler(req, reply) {
  const url = 'order-summary'
  arcadiaApi(url, req)
    .then((result) => {
      return reply(result.body).code(200)
    })
    .catch(reply)
}

export function putOrderSummaryHandler(req, reply) {
  const url = 'order-summary'
  arcadiaApi(url, req)
    .then((result) => {
      return reply(result.body).code(200)
    })
    .catch(reply)
}

export function putBillingAddressHandler(req, reply) {
  const url = 'order-summary/billing-address'
  arcadiaApi(url, req)
    .then((res) => {
      reply(res.body).code(200)
    })
    .catch(reply)
}

export function postDeliveryAddressHandler(req, reply) {
  const url = 'order-summary/delivery-address'
  arcadiaApi(url, req)
    .then((result) => {
      const summary = JSON.parse(result.text)
      reply(summary).code(200)
    })
    .catch(reply)
}

export function giftCardHandler(req, reply) {
  const url = 'order-summary/gift-card'
  arcadiaApi(url, req)
    .then((res) => {
      reply(res.body).code(200)
    })
    .catch(reply)
}
