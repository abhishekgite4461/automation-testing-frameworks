import {
  logLegacyCmsRequest,
  sanitizeResponseAndReply,
} from '../lib/legacy-cms-utils'

export function legacyCmsFeatureContentHandler(req, reply) {
  if (process.env.LEGACY_CMS_TEST_MODE === 'true') {
    // no calls made to legacy scrapper while testing
    return reply(null, { cmsTestMode: true })
  }
  logLegacyCmsRequest(req)
  return reply.proxy({
    host: process.env.LEGACY_CMS_URL,
    port: process.env.LEGACY_CMS_PORT,
    protocol: process.env.LEGACY_CMS_PROTOCOL,
    timeout: 20000,
    onResponse: sanitizeResponseAndReply(),
  })
}
