import arcadiaApi from './lib/arcadia-api'
import { cookieOptionsBag, cookieOptionsUnset } from '../lib/auth'
import Boom from 'boom'
import { isEmpty } from 'ramda'
import { localise } from '../../shared/lib/localisation'
import { getBrandConfig } from '../../server/config'

function getBagCount({ products = [] }) {
  return products.reduce((count, { quantity }) => count + quantity, 0)
}

export function addItemToBagHandler(req, reply) {
  const url = 'basket/item'
  arcadiaApi(url, req)
    .then(({ body }) => {
      reply(body)
        .state('bagCount', getBagCount(body).toString(), cookieOptionsBag)
        .code(200)
    })
    .catch((err) => {
      try {
        const {
          output: {
            payload: { statusCode, originalMessage },
          },
        } = err
        if (statusCode === 404 && originalMessage === 'Remote 404') {
          const hostname =
            req.info && req.info.hostname ? req.info.hostname : ''
          const { language, brandName } = getBrandConfig(hostname)
          const l = localise.bind(null, language, brandName)
          err.message = l`Sorry, this item is out of stock`
          err.reformat()
          reply(err)
        } else {
          reply(err)
        }
      } catch (e) {
        reply(e)
      }
    })
}

export function changeDeliveryType(req, reply) {
  const url = 'basket/delivery'
  arcadiaApi(url, req)
    .then(({ body }) => {
      reply(body).code(200)
    })
    .catch(reply)
}

export function getItemsFromBagHandler(req, reply) {
  const url = 'basket'
  arcadiaApi(url, req)
    .then(({ body }) => {
      reply(body)
        .state('bagCount', getBagCount(body).toString(), cookieOptionsBag)
        .code(200)
    })
    .catch(reply)
}

export function deleteItemFromBagHandler(req, reply) {
  const url = 'basket/item'
  arcadiaApi(url, req)
    .then(({ body }) => {
      reply(body)
        .state('bagCount', getBagCount(body).toString(), cookieOptionsBag)
        .code(200)
    })
    .catch(reply)
}

export function emptyBagHandler(req, reply) {
  const url = 'basket'
  arcadiaApi(url, req)
    .then(({ body }) => {
      reply(body)
        .state('bagCount', '0', cookieOptionsBag)
        .code(200)
    })
    .catch(reply)
}

export function addPromotionCodeHandler(req, reply) {
  const url = 'basket/promotion'
  arcadiaApi(url, req)
    .then(({ body }) => {
      reply(body)
        .state('arcpromoCode', '', cookieOptionsUnset)
        .code(body.statusCode || 200)
    })
    .catch(reply)
}

export function delPromotionCodeHandler(req, reply) {
  if (!req.payload || isEmpty(req.payload)) {
    return reply(
      Boom.badRequest('You must include a promotionCode in the body')
    )
  }

  const url = `basket/promotion?promotionCode=${encodeURIComponent(
    req.payload.promotionCode
  )}`
  arcadiaApi(url, req)
    .then(({ body }) => {
      reply(body)
        .state('arcpromoCode', '', cookieOptionsUnset)
        .code(body.statusCode || 200)
    })
    .catch(reply)
}

export function fetchItemSizesAndQuantitiesHandler(req, reply) {
  const url = 'basket/item/sizes-and-quantities'
  arcadiaApi(url, req)
    .then(({ body }) => {
      reply(body).code(200)
    })
    .catch(reply)
}

export function updateItemInBagHandler(req, reply) {
  const url = 'basket/item'
  arcadiaApi(url, req)
    .then(({ body }) => {
      reply(body).code(200)
    })
    .catch(reply)
}
