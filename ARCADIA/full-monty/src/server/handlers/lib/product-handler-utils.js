const matchAssetType = (type) => ({ assetType }) => assetType === type
const createMobileAsset = (assetType, baseUrl) => ({
  assetType,
  index: 1,
  url: baseUrl.replace(/_(Small|Medium|Large)_/, '_Mobile_'),
})

const addMobileAssets = (products) =>
  products.map((product) => {
    const additionalAssets = product.additionalAssets.slice(0)
    const largeBanner = additionalAssets.find(
      matchAssetType('IMAGE_BANNER_LARGE')
    )
    const largeBadge = additionalAssets.find(
      matchAssetType('IMAGE_BADGE_LARGE')
    )

    if (largeBanner) {
      additionalAssets.push(
        createMobileAsset('IMAGE_BANNER_MOBILE', largeBanner.url)
      )
    }
    if (largeBadge) {
      additionalAssets.push(
        createMobileAsset('IMAGE_BADGE_MOBILE', largeBadge.url)
      )
    }

    return {
      ...product,
      additionalAssets,
    }
  })

export { addMobileAssets }
