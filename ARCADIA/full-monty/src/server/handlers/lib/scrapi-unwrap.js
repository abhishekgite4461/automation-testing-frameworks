import Boom from 'boom'
import { isEmpty, path } from 'ramda'

const isErrorBody = ({ body: { statusCode, success, wcsErrorCode } }) => {
  return (!success && statusCode >= 400 && statusCode < 600) || wcsErrorCode
}

const makeBoom = (statusCode = 520, message, originalMessage) => {
  const boomting = Boom.create(statusCode, message)
  if (originalMessage) boomting.output.payload.originalMessage = originalMessage
  return boomting
}

const validationError = ({
  message,
  originalMessage,
  statusCode,
  validationErrors,
}) => {
  const boom = makeBoom(statusCode, message, originalMessage)
  boom.output.payload.validationErrors = validationErrors
  return boom
}

const unwrap = (resp) => {
  if (resp.isBoom) return resp

  // for scrAPI response `statusCode=500` then expect `response=resp.response`
  const body = resp.body || resp.response.body

  // superagent sets `response.error` to an `Error` object when an HTTP error occurs
  if (isEmpty(body) && path(['response', 'error'], resp) instanceof Error) {
    return Boom.wrap(resp.response.error, resp.status, resp.message)
  }

  if (body.wcsErrorCode) return makeBoom(body.wcsErrorCode, 'Not found')
  if (body.originalMessage === 'Validation error') return validationError(body)
  if (
    body.originalMessage &&
    body.originalMessage.includes('CMS page not supported on mobile')
  ) {
    return makeBoom(404, body.message, body.originalMessage)
  }
  return makeBoom(body.statusCode, body.message, body.originalMessage)
}

const setSessionKey = (req, resp) => {
  if (!resp.cached && resp.headers) {
    req.arcadiaSessionKey = resp.headers['arcadia-session-key']
  }
}

export const respInspect = (req) => {
  /**
   * This function takes a scrAPI response object as an argument, and returns a
   * promise. This function exams the passed scrAPI response object for a nested
   * response, and determine if that nested response contains an error state.
   *
   * If a nested response is found, and that response contains an error state,
   * then this function will unwrap the nested error and reject its returned
   * promise with that error.
   *
   * However, if no nested error state is found then this function will resolve
   * its returned promise with the original, unaltered scrAPI response object.
   *
   * @param resp
   * @returns {Promise}
   */
  return (resp) => {
    setSessionKey(req, resp)

    return new Promise((resolve, reject) => {
      // check whether the successful resp returns an error in the body
      return isErrorBody(resp) ? reject(unwrap(resp)) : resolve(resp)
    })
  }
}

export const catchInspect = (req) => {
  /**
   * This function takes an Error object as an argument, and returns a promise.
   * This function exams the passed Error object, and unwraps any nested response,
   * or creates a Boom error response object if required.
   *
   * The return promise is rejected to the value of an error response object.
   *
   * @param error
   * @returns {Promise}
   */
  return (error) => {
    /**
     * superagent 4xx or 5xx will carry `error.status` and `error.response`
     * If not isRespErr scrAPI has responded with 2xx and either we've already determined error in response body
     * during respInspect or the reponse has failed gateway validation
     */
    const isRespErr = !!(error.status && error.response)

    if (isRespErr) {
      setSessionKey(req, error.response)
    }

    return Promise.reject(isRespErr ? unwrap(error) : Boom.wrap(error))
  }
}
