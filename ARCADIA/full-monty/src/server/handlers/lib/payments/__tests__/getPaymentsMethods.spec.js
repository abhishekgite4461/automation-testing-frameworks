import GetPaymentsMethods from '../getPaymentsMethods'

const getPaymentsMethods = (req = {}) =>
  new GetPaymentsMethods(req).selectRequest()

describe('getPaymentMethods', () => {
  describe('throw error', () => {
    it('for missing parameters', () => {
      expect(getPaymentsMethods).toThrowError('Missing Params')
    })
    it('for invalid parameters', () => {
      const invalidRequest = () =>
        getPaymentsMethods({ query: { value: 'VISA;INVLD;MCARD' } })
      expect(invalidRequest).toThrowError('Invalid Params')
    })
  })
  describe('get single payment method by value', () => {
    it('VISA for default region (uk)', () => {
      expect(
        getPaymentsMethods({
          query: { value: 'VISA' },
        })
      ).toMatchSnapshot()
    })
    it('Klarna for region: fr (falls back to uk)', () => {
      expect(
        getPaymentsMethods({
          headers: {
            'brand-code': 'tsfr',
          },
          query: {
            value: 'KLRNA',
          },
        })
      ).toMatchSnapshot()
    })
    it('Klarna for region: de', () => {
      expect(
        getPaymentsMethods({
          headers: {
            'brand-code': 'tsde',
          },
          query: {
            value: 'KLRNA',
          },
        })
      ).toMatchSnapshot()
    })
  })
  describe('get single payment method by label', () => {
    it('VISA for default region (uk)', () => {
      expect(
        getPaymentsMethods({
          query: { label: 'Visa' },
        })
      ).toMatchSnapshot()
    })
    it('Klarna for region: fr (falls back to uk)', () => {
      expect(
        getPaymentsMethods({
          headers: {
            'brand-code': 'tsfr',
          },
          query: {
            label: 'Try before you buy',
          },
        })
      ).toMatchSnapshot()
    })
    it('Klarna for region: de', () => {
      expect(
        getPaymentsMethods({
          headers: {
            'brand-code': 'tsde',
          },
          query: {
            label: 'Try before you buy',
          },
        })
      ).toMatchSnapshot()
    })
  })
  describe('get multiple payment methods by value', () => {
    it('VISA, Klarna, Mastercard (tsuk)', () => {
      expect(
        getPaymentsMethods({
          query: { value: 'VISA;KLRNA;MCARD' },
        })
      ).toMatchSnapshot()
    })
    it('PayPal, VISA, Klarna (tsde)', () => {
      expect(
        getPaymentsMethods({
          headers: {
            'brand-code': 'tsde',
          },
          query: {
            value: 'PYPAL;VISA;KLRNA',
          },
        })
      ).toMatchSnapshot()
    })
  })
  describe('get multiple payment methods by label', () => {
    it('VISA, Klarna, Mastercard (tsuk)', () => {
      expect(
        getPaymentsMethods({
          query: {
            label: 'Visa;Try before you buy;MasterCard',
          },
        })
      ).toMatchSnapshot()
    })
    it('PayPal, VISA, Klarna (tsde)', () => {
      expect(
        getPaymentsMethods({
          headers: {
            'brand-code': 'tsde',
          },
          query: {
            label: 'PayPal;Visa;Try before you buy',
          },
        })
      ).toMatchSnapshot()
    })
  })
  describe('get all payment methods available for a region', () => {
    it('uk', () => {
      expect(
        getPaymentsMethods({
          query: {
            delivery: 'United Kingdom',
            billing: 'United Kingdom',
          },
        })
      ).toMatchSnapshot()
    })
    it('uk with Klarna (extra treshold)', () => {
      expect(
        getPaymentsMethods({
          query: {
            delivery: 'United Kingdom',
            billing: 'United Kingdom',
            amount: 100,
          },
        })
      ).toMatchSnapshot()
    })
    it('de', () => {
      expect(
        getPaymentsMethods({
          headers: {
            'brand-code': 'tsde',
          },
          query: {
            delivery: 'Germany',
          },
        })
      ).toMatchSnapshot()
    })
  })
})
