import PaymentManager from './paymentManager'

const DEFAULT_REGION = 'uk'
const ALL = 'all'
const SEPARATOR = ';'

/*
 * ADD HERE ONLY THE TYPE OF REQUEST (Ex. list of payments available, 1 payment details ... etc)
 */
export default class PaymentsMethods extends PaymentManager {
  selectRequest = () => {
    const { action, label, payments, region, value } = this.config
    if (!action && value) {
      if (value === ALL) {
        return this.getPaymentsAvailableByBrandAndCountry()
      }
      const values = value.split(SEPARATOR)
      if (values.length > 1) {
        return this.getMultiplePaymentMethods(region, values)
      }
      return this.getSinglePaymentMethod(region, value)
    }
    if (!action && label) {
      const labels = label.split(SEPARATOR)
      if (labels.length > 1) {
        return this.getMultiplePaymentMethods(region, labels, 'label')
      }
      return this.getSinglePaymentMethod(region, label, 'label')
    }
    if (action === 'delete') {
      return this.deletePaymentFromList(
        this.getPaymentsAvailableByBrandAndCountry(payments)
      )
    }
    return this.getAvailablePaymentMethods()
  }

  getAvailablePaymentMethods = () => {
    return this.getPaymentsAvailableByBrandAndCountry().filter(
      (payment) =>
        this.getPaymentAvailableFromAddressDelivery(payment) &&
        this.getPaymentAvailableFromAddressBilling(payment) &&
        this.getPaymentAvailableIfThreshold(payment)
    )
  }

  getSinglePaymentMethod = (region, value, property = 'value') => {
    const payments = this.getAllPaymentsAvailable().filter(
      (payment) => payment[property] === value
    )
    const regionalPayment = payments.find(
      (payment) => region === payment.region && value === payment[property]
    )
    return (
      regionalPayment ||
      payments.find(
        (payment) =>
          (!payment.region || payment.region === DEFAULT_REGION) &&
          value === payment[property]
      )
    )
  }

  getMultiplePaymentMethods = (region, values, property) => {
    const paymentMethods = values.map((value) =>
      this.getSinglePaymentMethod(region, value, property)
    )
    if (
      !paymentMethods.every(
        (paymentMethod) => typeof paymentMethod !== 'undefined'
      )
    ) {
      throw Error('Invalid Params')
    }
    return paymentMethods
  }
}
