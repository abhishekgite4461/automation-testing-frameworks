/*
 * Configure And Validate the paymentMethod Request
 */
import { getConfigByStoreCode } from '../../../config/index'
import { paymentsConfig, allPaymentsList } from '../../../config/paymentConfig'

/*
 * ADD HERE ONLY THE LOGIC ABOUT AVAILABLE/NOT-AVAILABLE Payment method
 */
export default class PaymentManager {
  constructor({ headers = {}, query = {} }) {
    // Config payments method available only for brand-Area
    const code = headers['brand-code'] || 'tsuk'
    const { brandCode, region } = getConfigByStoreCode(code)
    this.config = { brandCode, region, ...this.validateParamas(query) }
  }

  validateParamas(query) {
    const { action, billing, delivery, label, value } = query
    if (!delivery && !label && !value && action !== 'delete') {
      throw new Error('Missing Params')
    }

    return {
      ...query,
      billing: billing || delivery, // if not billing country, we use the same of delivery
    }
  }

  deletePaymentFromList = (paymentList) => {
    return paymentList.filter((payment) => {
      return payment.value !== this.config.value
    })
  }

  getPaymentsAvailableByBrandAndCountry = (paymentsAvailable) => {
    return (
      paymentsAvailable ||
      paymentsConfig[this.config.brandCode][this.config.region]
    )
  }

  getPaymentAvailableFromAddressDelivery = (payment) => {
    return (
      !payment.deliveryCountry || // if not defined countries we assume is valid everywhere
      !!payment.deliveryCountry.find(
        (country) => country === this.config.delivery
      )
    )
  }

  getPaymentAvailableFromAddressBilling = (payment) => {
    return (
      !payment.billingCountry || // if not defined countries we assume is valid everywhere
      !!payment.billingCountry.find(
        (country) => country === this.config.billing
      )
    )
  }

  getPaymentAvailableIfThreshold = (payment) => {
    const amount = this.config.amount || 0
    return payment.threshold ? payment.threshold <= amount : true
  }

  getAllPaymentsAvailable = () => {
    return allPaymentsList.slice(0)
  }
}
