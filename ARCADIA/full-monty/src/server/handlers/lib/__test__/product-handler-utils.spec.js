import { addMobileAssets } from '../product-handler-utils'

describe(addMobileAssets.name, () => {
  const products = [
    {
      name: 'Dark Navy Shirt',
      productId: 31185333,
      additionalAssets: [
        {
          assetType: 'IMAGE_BANNER_LARGE',
          index: 1,
          url:
            'http://media.burton.co.uk/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color2/cms/pages/static/static-0000109712/images/Banner_Large_att.png',
        },
      ],
    },
    {
      name: 'Pink Shirt',
      productId: 31185334,
      additionalAssets: [
        {
          assetType: 'IMAGE_BADGE_LARGE',
          index: 1,
          url:
            'http://media.burton.co.uk/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color2/cms/pages/static/static-0000109712/images/Badge_Large_att.png',
        },
      ],
    },
  ]

  it("adds Mobile Banner to product's additionalAssets", () => {
    const expectedProducts = addMobileAssets(products)
    expect(expectedProducts[0].additionalAssets.length).toBe(2)
    expect(expectedProducts[0].additionalAssets).toEqual(
      expect.arrayContaining([
        {
          assetType: 'IMAGE_BANNER_MOBILE',
          index: 1,
          url:
            'http://media.burton.co.uk/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color2/cms/pages/static/static-0000109712/images/Banner_Mobile_att.png',
        },
      ])
    )
  })

  it("adds Mobile Badge to product's additionalAssets", () => {
    const expectedProducts = addMobileAssets(products)
    expect(expectedProducts[1].additionalAssets.length).toBe(2)
    expect(expectedProducts[1].additionalAssets).toEqual(
      expect.arrayContaining([
        {
          assetType: 'IMAGE_BADGE_MOBILE',
          index: 1,
          url:
            'http://media.burton.co.uk/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color2/cms/pages/static/static-0000109712/images/Badge_Mobile_att.png',
        },
      ])
    )
  })
})
