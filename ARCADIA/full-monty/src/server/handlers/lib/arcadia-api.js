import superagent from '../../../shared/lib/superagent'
import { getCacheExpiration } from '../../../shared/lib/cacheable-urls'
import {
  catchInspect,
  respInspect,
} from '../../../server/handlers/lib/scrapi-unwrap'
import * as logger from '../../lib/logger'
import gateway from './gateway'

const disableCache = global.process.env.DISABLE_API_CACHE || false

export default function requestToArcadiaApi(path, req) {
  let { jwtPayload } = req

  const arcadiaSessionKeyToLog =
    (jwtPayload && jwtPayload['arcadia-session-key']) || '-'

  // -- ONLY FOR TESTS --
  if (req.auth.credentials) jwtPayload = req.auth.credentials

  const scrApiPath = `/${req.headers['brand-code']}/${path}`
  const scrApiUrl = `${process.env.API_URL}${scrApiPath}`

  const method = req.method === 'delete' ? 'del' : req.method

  const expiration =
    method === 'get' &&
    getCacheExpiration('scrAPI', scrApiPath + req.url.search)
  const transactionId = logger.generateTransactionId()

  const requestObject = {
    transactionId,
    loggerMessage: 'request',
    method: method.toUpperCase(),
    scrApiUrl,
    query: req.url.search,
    session: arcadiaSessionKeyToLog,
    expires: expiration ? `${expiration}s` : '-',
  }
  logger.debug('arcadia-api', requestObject)

  const request = superagent[method](scrApiUrl)
    .query(req.url.query)
    .send(req.payload)
    .set('Arcadia-Api-Key', process.env.API_KEY)
    .set('Content-Type', 'application/json')

  if (!disableCache) {
    request.cache(expiration)
  }

  if (jwtPayload) {
    request.set('Arcadia-Session-Key', jwtPayload['arcadia-session-key'])
  }

  return request
    .then(respInspect(req))
    .then(gateway(method, scrApiPath, req.url.search))
    .then((resp) => {
      const statusCode = (resp.body && resp.body.statusCode) || resp.statusCode
      logger.debug('arcadia-api', {
        loggerMessage: 'response',
        statusCode,
        method: method.toUpperCase(),
        scrApiUrl,
        query: req.query,
        session: resp.headers ? resp.headers['arcadia-session-key'] : '-',
        cached: !!resp.cached,
        expires: expiration ? `${expiration}s` : '-',
      })

      return resp
    })
    .catch(catchInspect(req))
    .catch((err) => {
      if (err.isBoom) {
        const {
          output: { payload, statusCode },
        } = err

        const errorObject = {
          statusCode,
          method: method.toUpperCase(),
          scrApiUrl,
          query: req.query,
          ...payload,
          session: arcadiaSessionKeyToLog,
          expires: expiration ? `${expiration}s` : '-',
        }

        logger.error('arcadia-api', {
          transactionId,
          loggerMessage: 'error',
          ...errorObject,
        })
      } else {
        logger.error('arcadia-api', {
          transactionId,
          loggerMessage: 'error',
          ...err,
        })
      }
      throw err
    })
}
