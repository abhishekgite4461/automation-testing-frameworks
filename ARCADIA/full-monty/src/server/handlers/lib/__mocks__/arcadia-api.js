let mockResponse = Promise.resolve({ body: 'Default mocked body' })

export function __setMockResponse(url, req, responsePromise) {
  mockResponse = {}

  // mockResponse = {
  //   'product/seo': {
  //     'req': new Promise(...)
  //   }
  // }
  mockResponse[url] = {}
  mockResponse[url][req] = responsePromise
}

// eslint-disable-next-line prefer-arrow-callback
export default jest.fn(function requestToArcadiaApi(path, req) {
  return mockResponse[path] && mockResponse[path][req]
    ? mockResponse[path][req]
    : mockResponse
})
