import aracadiaApi from './lib/arcadia-api'
import { translatePriceKeyInRefinements } from '../lib/requestRefinementHack'
import { omit, isEmpty, compose, equals, type, both, not } from 'ramda'
import * as plpToCmsMock from '../../../test/mocks/plp-to-cms.json'
import { addMobileAssets } from './lib/product-handler-utils'

const ALL_NUMBERS_REGEX = /^\d*$/

// TODO - refactor out the renaming of props - it's a waste of time and masked that the API had added new props
// Plus was duplicated in two handlers - any misallignment would cause chaos.
function getResponseData(body) {
  const { categoryHeader, paging, products } = body

  if (!categoryHeader && !paging) return { totalProducts: 0 }

  return {
    ...omit(['categoryHeader', 'paging', 'products'], body),
    products: addMobileAssets(products.slice(0, 20)),
    categoryTitle: categoryHeader.title,
    categoryDescription: categoryHeader.description,
    totalProducts: paging.total,
    sortOptions: paging.pagingSorts,
  }
}

/*
 * Products list
 */
export function productsHandler(req, reply) {
  const url = 'product'
  req.url = translatePriceKeyInRefinements(req) // HACK! see comment in requestRefinementHack.js
  aracadiaApi(url, req)
    .then((result) => {
      if (Array.isArray(result.body.products)) {
        if (
          result.body.cmsPage &&
          result.body.cmsPage.seoUrl &&
          result.body.products.length === 0
        ) {
          return reply({
            cmsPage: result.body.cmsPage,
          }).code(200)
        } else if (result.body.products.length <= 1) {
          reply({
            products: result.body.products,
            total: result.body.products.length,
            searchTerm: result.body.searchTerm,
          }).code(200)
        } else {
          return reply(getResponseData(result.body)).code(200)
        }
      } else {
        return reply(result.body.message).code(result.body.statusCode)
      }
    })
    .catch(reply)
}

/*
 * Products search
 */
export function productsSearchHandler(req, reply) {
  if (typeof reply !== 'function') {
    throw new Error('Invalid arguments provided to productsSearchHandler')
  }

  const url = 'product/seo'

  if (process.env.CMS_TEST_MODE === 'true') {
    // DES-769
    if (
      req &&
      req.query &&
      req.query.seoUrl === '/en/tsuk/category/plpToCmsMockedSeoUrl'
    ) {
      return reply(getResponseData(plpToCmsMock)).code(200)
    }
  }

  return aracadiaApi(url, req)
    .then(({ body }) => {
      reply(getResponseData(body)).code(200)
    })
    .catch(reply)
}

export function fetchBySeo(identifier, req) {
  const url = `product/seo?seoUrl=${encodeURIComponent(identifier)}`
  return aracadiaApi(url, req).then(({ body }) => {
    if (body.success === false) {
      throw new Error({ ...body, success: false })
    }
    return (body.products && body.products[0]) || { success: false }
  })
}

export function fetchById(identifier, req) {
  const url = `product/${identifier}`
  return aracadiaApi(url, req).then(({ body }) => {
    if (body.success === false) {
      throw new Error({ ...body, success: false })
    }
    return body
  })
}

/*
 * Product details
 */
export function productDetailsHandler(req, reply) {
  const { identifier } = req.params
  const isObject = compose(equals('Object'), type)
  const isNotEmpty = compose(not, isEmpty)
  const isNonEmptyObject = both(isObject, isNotEmpty)
  let prom

  if (identifier.match(ALL_NUMBERS_REGEX)) {
    prom = exports.fetchById(identifier, req)
  } else {
    prom = exports.fetchBySeo(identifier, req)
  }

  prom
    .then((product) => {
      if (isNonEmptyObject(product.productDataQuantity)) {
        const items = product.productDataQuantity.quantities.map(
          (quantity, index) => {
            const sku = product.productDataQuantity.SKUs[index]
            return {
              quantity,
              size: sku.value,
              sku: sku.partnumber,
              selected: false,
            }
          }
        )
        return reply({
          ...product,
          items,
        })
      }
      return reply(product)
    })
    .catch((err) => reply({ ...err, success: false }))
}

/*
 * Email me back in stock
 */
export function productEmailMeStock(req, reply) {
  const url = 'product/notify-me'
  aracadiaApi(url, req)
    .then((result) => {
      reply(result.body)
    })
    .catch(reply)
}
