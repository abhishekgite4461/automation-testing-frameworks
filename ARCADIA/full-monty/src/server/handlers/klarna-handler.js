import arcadiaApi from './lib/arcadia-api'
import { klarnaCookieOptions } from '../lib/auth'

const request = require('superagent')

import { getBrandConfig } from '../config/index'

// When updating a session we need to do it both on WCS side and with Klarna
// This handler sends requests to WCS through ScrAPI
export function updateKlarnaSessionHandler(req, reply) {
  const url = 'order/klarna-session'
  arcadiaApi(url, req)
    .then(({ body }) => {
      return reply({
        ...body,
        sessionId: req.state.klarnaSessionId,
        clientToken: req.state.klarnaClientToken,
      }).code(200)
    })
    .catch((err) => {
      return reply(null, err.data).code(err.output.statusCode)
    })
}

// When updating a session we need to do it both on WCS side and with Klarna
// This handler sends requests directly to Klarna
export function directKlarnaUpdateSessionHandler(req, reply) {
  const sessionId = req.state.klarnaSessionId
  const url = `${process.env.KLARNA_URL}/sessions/${sessionId}`

  const {
    info: { hostname },
  } = req
  const { brandName } = getBrandConfig(hostname)
  const username = process.env[`KLARNA_USERNAME_${brandName.toUpperCase()}`]
  const password = process.env[`KLARNA_PASSWORD_${brandName.toUpperCase()}`]

  request
    .post(url)
    .auth(username, password)
    .send(req.payload)
    .set('Accept', 'application/json')
    .end((err, res) => {
      if (err) {
        return reply(null, err)
      }

      return reply(res.body)
    })
}

export function createSessionHandler(req, reply) {
  const url = 'order/klarna-session'
  const sessionId = req.state && req.state.klarnaSessionId
  const clientToken = req.state && req.state.klarnaClientToken
  if (sessionId && clientToken) {
    req.method = 'put'
    req.payload.session_id = sessionId
    updateKlarnaSessionHandler(req, reply)
  } else {
    arcadiaApi(url, req)
      .then(({ body, statusCode }) => {
        return reply(body)
          .state('klarnaSessionId', body.sessionId, klarnaCookieOptions)
          .state('klarnaClientToken', body.clientToken, klarnaCookieOptions)
          .code(statusCode)
      })
      .catch((err) => {
        return reply(null, err.data).code(err.output.statusCode)
      })
  }
}
