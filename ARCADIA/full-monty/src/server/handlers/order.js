import arcadiaApi from './lib/arcadia-api'
import { getBrandConfig } from '../config'
import { cookieOptionsUnset } from '../lib/auth'
import { path } from 'ramda'
import { KLARNA } from '../../shared/constants/paymentTypes'

// for the sake of `stage.api` image hostname must be replaced
const replaceImageUrls = (hostname, orderLines) => {
  const { mediaHostname } = getBrandConfig(hostname)
  if (!Array.isArray(orderLines) || !orderLines.length) return []
  return orderLines.map((orderLine) => ({
    ...orderLine,
    imageUrl: orderLine.imageUrl.replace(
      /(http:)\/\/[a-z]+\..+\.arcadiagroup\.ltd\.uk(.*)/i,
      `//${mediaHostname}$2`
    ),
  }))
}

const extractClientIP = (req) => {
  const header = req.headers['x-forwarded-for'] // `header` is; '', '217.22.82.162', or '217.22.82.162, 2.20.133.114, ...'
  if (typeof header === 'string' && header.length > 0) {
    return header.split(',')[0]
  }
  if (req.info && req.info.remoteAddress) {
    return req.info.remoteAddress
  }
  return '127.0.0.1'
}

export function postOrderHandler(req, reply) {
  const url = 'order'
  req.payload.remoteIpAddress = extractClientIP(req)
  arcadiaApi(url, req)
    .then(({ body, statusCode }) => {
      const paymentType = path(['payload', 'creditCard', 'type'], req)
      const paidWithKlarna = paymentType === KLARNA
      if (body.paypalUrl || body.vbvForm || body.paymentUrl) {
        reply(body).code(statusCode)
      } else if (paidWithKlarna) {
        reply(body)
          .state('klarnaClientToken', null, cookieOptionsUnset)
          .state('klarnaSessionId', null, cookieOptionsUnset)
          .code(statusCode)
      } else {
        const hostname = req.info && req.info.hostname
        const {
          completedOrder: { orderLines },
        } = body
        body.completedOrder.orderLines = replaceImageUrls(hostname, orderLines)
        reply(body).code(statusCode)
      }
    })
    .catch(reply)
}

export function putOrderHandler(req, reply) {
  const url = 'order'
  arcadiaApi(url, req)
    .then(({ body, statusCode }) => {
      const hostname = req.info && req.info.hostname
      body.orderLines = replaceImageUrls(hostname, body.orderLines)
      reply(body).code(statusCode)
    })
    .catch(reply)
}
