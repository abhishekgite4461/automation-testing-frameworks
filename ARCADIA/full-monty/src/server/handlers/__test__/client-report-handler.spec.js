import { processReport } from '../client-report-handler'

const mockReply = jest.fn()
const req = {
  params: {
    ltype: 'debug',
  },
  payload: {
    namespace: 'AAAAAAA',
    bob: 'BBBBB',
  },
}

jest.mock('../../lib/logger', () => ({
  debug: jest.fn(),
}))
import { debug } from '../../lib/logger'

describe('client-report-handler', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('should call processReport', () => {
    processReport(req, mockReply)
    expect(mockReply).toHaveBeenCalledTimes(1)
    expect(debug).toHaveBeenCalledTimes(1)
    expect(debug).toHaveBeenCalledWith('client-report:AAAAAAA', {
      bob: 'BBBBB',
    })
  })

  it('should handle foo call to intself', () => {
    req.params.ltype = 'FOO'
    processReport(req, mockReply)
    expect(mockReply).toHaveBeenCalledTimes(1)
  })
})
