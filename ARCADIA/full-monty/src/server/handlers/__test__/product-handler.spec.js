import * as productHandlerModule from '../product-handler'
import plpToCmsMock from '../../../../test/mocks/plp-to-cms-processed'
import arcadiaApi from '../lib/arcadia-api'

jest.mock('../lib/arcadia-api')

// Rewire private function in ES6 module
// Test async code
// Handle resetMocks in async scenario
// Handle testing promises

async function verifyCmsTestModeScenario(
  mockedCodeFunction,
  mockedReplyFunction,
  mockedReqObject
) {
  expect(mockedReplyFunction).toHaveBeenCalledTimes(0)
  expect(mockedCodeFunction).toHaveBeenCalledTimes(0)

  await productHandlerModule.productsSearchHandler(
    mockedReqObject,
    mockedReplyFunction
  )
  expect(mockedCodeFunction).toHaveBeenCalledTimes(1)
  expect(mockedReplyFunction).toHaveBeenCalledTimes(1)
  expect(mockedReplyFunction).toHaveBeenCalledWith({ totalProducts: 0 })
  expect(mockedCodeFunction).toHaveBeenCalledWith(200)
}

describe('products-handler', () => {
  beforeAll(() => {
    jest.resetAllMocks()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  afterAll(() => {
    jest.resetAllMocks()
  })

  describe('#productsSearchHandler', () => {
    beforeEach(() => {
      global.process.env.CMS_TEST_MODE = false
    })

    //
    // Invalid arguments scenario
    //
    it('throws error if "reply" argument is not a function', () => {
      expect(productHandlerModule.productsSearchHandler).toThrowError(
        'Invalid arguments provided to productsSearchHandler'
      )
    })

    //
    // CMS_TEST_MODE scenarios
    //
    it('replies with mock in case of CMS_TEST_MODE set to true', () => {
      global.process.env.CMS_TEST_MODE = 'true'
      const mockedCodeFunction = jest.fn()
      const mockedReplyFunction = jest.fn(() => ({ code: mockedCodeFunction }))
      const mockedReqObject = {
        query: { seoUrl: '/en/tsuk/category/plpToCmsMockedSeoUrl' },
      }

      expect(mockedCodeFunction).toHaveBeenCalledTimes(0)

      productHandlerModule.productsSearchHandler(
        mockedReqObject,
        mockedReplyFunction
      )

      expect(mockedReplyFunction).toHaveBeenCalledTimes(1)
      expect(mockedCodeFunction).toHaveBeenCalledTimes(1)
      expect(mockedReplyFunction).toHaveBeenCalledWith(plpToCmsMock)
      expect(mockedCodeFunction).toHaveBeenCalledWith(200)
    })

    it('does not reply with mock data if CMS_TEST_MODE set to false', async () => {
      const mockedCodeFunction = jest.fn()
      const mockedReplyFunction = jest.fn(() => ({ code: mockedCodeFunction }))
      const mockedReqObject = {
        query: { seoUrl: '/en/tsuk/category/plpToCmsMockedSeoUrl' },
      }

      arcadiaApi.mockImplementation(
        jest.fn(() => Promise.resolve({ body: 'body response' }))
      )

      await verifyCmsTestModeScenario(
        mockedCodeFunction,
        mockedReplyFunction,
        mockedReqObject
      )
    })

    it('does not reply with mock data if CMS_TEST_MODE set to true and req.query.seoUrl is not the expected one', async () => {
      global.process.env.CMS_TEST_MODE = true
      const mockedCodeFunction = jest.fn()
      const mockedReplyFunction = jest.fn(() => ({ code: mockedCodeFunction }))
      const mockedReqObject = {
        query: { seoUrl: 'url not associated with CMS mock data' },
      }

      arcadiaApi.mockImplementation(
        jest.fn(() => Promise.resolve({ body: 'body response' }))
      )

      await verifyCmsTestModeScenario(
        mockedCodeFunction,
        mockedReplyFunction,
        mockedReqObject
      )
    })

    //
    // Call to arcadiaApi scenarios
    //
    it('calls arcadiaApi and then replies as expected', async () => {
      const mockedCodeFunction = jest.fn()
      const mockedReplyFunction = jest.fn(() => ({ code: mockedCodeFunction }))
      const mockedReqArgument = 'req'

      arcadiaApi.mockImplementation(
        jest.fn(() => Promise.resolve({ body: 'body response' }))
      )

      expect(mockedReplyFunction).toHaveBeenCalledTimes(0)
      expect(mockedCodeFunction).toHaveBeenCalledTimes(0)

      await productHandlerModule.productsSearchHandler(
        mockedReqArgument,
        mockedReplyFunction
      )

      expect(mockedReplyFunction).toHaveBeenCalledTimes(1)
      expect(mockedReplyFunction).toHaveBeenCalledWith({ totalProducts: 0 })
      expect(mockedCodeFunction).toHaveBeenCalledTimes(1)
      expect(mockedCodeFunction).toHaveBeenCalledWith(200)
    })

    it('handles correctly arcadiaApi promise rejection', async () => {
      const mockedCodeFunction = jest.fn()
      const mockedReplyFunction = jest.fn(() => ({ code: mockedCodeFunction }))

      arcadiaApi.mockImplementation(jest.fn(() => Promise.reject('rejected')))

      expect(mockedReplyFunction).toHaveBeenCalledTimes(0)

      await productHandlerModule.productsSearchHandler(
        'req',
        mockedReplyFunction
      )

      expect(mockedCodeFunction).not.toHaveBeenCalledWith(200)
      expect(mockedReplyFunction).toHaveBeenCalledTimes(1)
      expect(mockedReplyFunction).toHaveBeenCalledWith('rejected')
    })
  })

  describe('#productDetailsHandler', () => {
    it('handles productDataQuantity response from arcadiaApi', async () => {
      let response
      const mockedReplyFunction = jest.fn((product) => {
        response = product
      })
      const mockedReqArgument = {
        params: {
          identifier: '1',
        },
      }
      const data = {
        productDataQuantity: {
          quantities: [1, 3, 5],
          SKUs: [
            { value: 'sku122', partnumber: 'p1' },
            { value: 'sku544', partnumber: 'p2' },
            { value: 'sku655', partnumber: 'p3' },
          ],
        },
      }

      jest.spyOn(productHandlerModule, 'fetchById').mockImplementation(() => {
        return {
          then: jest.fn((reply) => {
            reply(data)
            return new Promise((resolve) => {
              resolve()
            })
          }),
        }
      })

      productHandlerModule.productDetailsHandler(
        mockedReqArgument,
        mockedReplyFunction
      )

      expect(mockedReplyFunction).toHaveBeenCalledTimes(1)
      expect(productHandlerModule.fetchById).toHaveBeenCalledTimes(1)
      expect(response).toEqual({
        ...data,
        items: data.productDataQuantity.SKUs.map((p, i) => {
          return {
            size: p.value,
            quantity: data.productDataQuantity.quantities[i],
            sku: p.partnumber,
            selected: false,
          }
        }),
      })
    })

    it('handle no productDataQuantity response from arcadiaApi', () => {
      let response
      const mockedReplyFunction = jest.fn((product) => {
        response = product
      })
      const mockedReqArgument = {
        params: {
          identifier: '1',
        },
      }

      jest.spyOn(productHandlerModule, 'fetchById').mockImplementation(() => {
        return {
          then: jest.fn((reply) => {
            reply({})
            return new Promise((resolve) => {
              resolve()
            })
          }),
        }
      })

      productHandlerModule.productDetailsHandler(
        mockedReqArgument,
        mockedReplyFunction
      )

      expect(mockedReplyFunction).toHaveBeenCalledTimes(1)
      expect(productHandlerModule.fetchById).toHaveBeenCalledTimes(1)
      expect(response).toEqual({})
    })
  })
})
