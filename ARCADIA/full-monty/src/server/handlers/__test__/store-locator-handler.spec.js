import { getStoresHandler, getCountriesHandler } from '../store-locator-handler'
import { getStores, getCountries } from '../lib/store-locator-service'

jest.mock('../lib/store-locator-service')

describe('store-locator-handler', () => {
  beforeAll(() => {
    jest.resetAllMocks()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  afterAll(() => {
    jest.resetAllMocks()
  })

  describe('@getStoresHandler', () => {
    it('reply with stores in UK (200)', async () => {
      const stores = [
        {
          name: 'mockedStore1',
          distance: 1,
        },
        {
          name: 'mockedStore2',
          distance: 1.6,
        },
      ]
      const convertedStores = [
        {
          name: 'mockedStore1',
          distance: '0.62',
        },
        {
          name: 'mockedStore2',
          distance: '0.99',
        },
      ]
      const request = {
        query: {
          region: 'uk',
        },
      }
      const code = jest.fn()
      const reply = jest.fn(() => ({ code }))

      getStores.mockImplementation(jest.fn(() => Promise.resolve(stores)))

      expect(reply).toHaveBeenCalledTimes(0)
      expect(code).toHaveBeenCalledTimes(0)
      await getStoresHandler(request, reply)
      expect(reply).toHaveBeenCalledTimes(1)
      expect(reply).toHaveBeenCalledWith(convertedStores)
      expect(code).toHaveBeenCalledTimes(1)
      expect(code).toHaveBeenCalledWith(200)
    })

    it('reply with stores (200)', async () => {
      const stores = 'mockedStores'
      const request = {
        query: {
          region: 'international',
        },
      }
      const code = jest.fn()
      const reply = jest.fn(() => ({ code }))

      getStores.mockImplementation(jest.fn(() => Promise.resolve(stores)))

      expect(reply).toHaveBeenCalledTimes(0)
      expect(code).toHaveBeenCalledTimes(0)
      await getStoresHandler(request, reply)
      expect(reply).toHaveBeenCalledTimes(1)
      expect(reply).toHaveBeenCalledWith(stores)
      expect(code).toHaveBeenCalledTimes(1)
      expect(code).toHaveBeenCalledWith(200)
    })

    it('reply with error (400)', async () => {
      const error = 'mockedError'
      const code = jest.fn()
      const request = {
        query: {},
      }
      const reply = jest.fn(() => ({ code }))

      getStores.mockImplementation(jest.fn(() => Promise.reject(error)))

      expect(reply).toHaveBeenCalledTimes(0)
      expect(code).toHaveBeenCalledTimes(0)
      await getStoresHandler(request, reply)
      expect(reply).toHaveBeenCalledTimes(1)
      expect(reply).toHaveBeenCalledWith({ error })
      expect(code).toHaveBeenCalledTimes(1)
      expect(code).toHaveBeenCalledWith(400)
    })
  })

  describe('@getCountriesHandler', () => {
    it('reply with countries', async () => {
      const countries = 'mockedCountries'
      const request = { query: {} }
      const code = jest.fn()
      const reply = jest.fn(() => ({ code }))

      getCountries.mockImplementation(jest.fn(() => Promise.resolve(countries)))

      expect(reply).toHaveBeenCalledTimes(0)
      expect(code).toHaveBeenCalledTimes(0)
      await getCountriesHandler(request, reply)
      expect(reply).toHaveBeenCalledTimes(1)
      expect(reply).toHaveBeenCalledWith(countries)
      expect(code).toHaveBeenCalledTimes(1)
      expect(code).toHaveBeenCalledWith(200)
    })

    it('reply with error (400)', async () => {
      const error = 'mockedError'
      const code = jest.fn()
      const request = {
        query: {},
      }
      const reply = jest.fn(() => ({ code }))

      getCountries.mockImplementation(jest.fn(() => Promise.reject(error)))

      expect(reply).toHaveBeenCalledTimes(0)
      expect(code).toHaveBeenCalledTimes(0)
      await getCountriesHandler(request, reply)
      expect(reply).toHaveBeenCalledTimes(1)
      expect(reply).toHaveBeenCalledWith({ error })
      expect(code).toHaveBeenCalledTimes(1)
      expect(code).toHaveBeenCalledWith(400)
    })
  })
})
