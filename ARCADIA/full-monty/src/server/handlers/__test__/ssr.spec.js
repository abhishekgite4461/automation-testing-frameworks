import nock from 'nock'
import serverSideRender from '../server-side-renderer'
import { getAllPaymentMethods } from '../../../shared/actions/common/paymentMethodsActions'

jest.mock('../../../shared/actions/common/paymentMethodsActions')

process.env.USE_NEW_HANDLER = true

const BASE_REQUEST = {
  info: {
    hostname: 'local.m.topshop.com',
  },
  url: {
    pathname: '/',
    query: {},
  },
  headers: {
    cookie: '',
  },
  state: {},
}

const response = {}
const reply = jest.fn(() => response)
reply.redirect = jest.fn()

describe('/delivery-payment', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('fetches payment methods only once', () => {
    nock('local.m.topshop.com')
      .get(/.*/)
      .reply(200, {})

    serverSideRender(
      {
        ...BASE_REQUEST,
        url: {
          ...BASE_REQUEST.url,
          pathname: '/checkout/delivery-payment',
        },
        state: {
          authenticated: 'yes',
        },
      },
      reply
    )

    expect(getAllPaymentMethods).toHaveBeenCalledTimes(1)
  })
})
