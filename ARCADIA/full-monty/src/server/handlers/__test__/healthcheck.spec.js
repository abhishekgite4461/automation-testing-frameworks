import { getHealthCheckHandler } from '../healthcheck'

const mockreply = jest.fn(() => ({
  code: jest.fn(),
}))

jest.mock('../../../server/lib/logger', () => ({
  generateTransactionId: () => 'AAAAA',
  info: jest.fn(),
}))

import { info } from '../../../server/lib/logger'

describe('#Healthcheck Handler', () => {
  it('calls the appropriate sub handlers', () => {
    getHealthCheckHandler({}, mockreply)
    expect(mockreply).toHaveBeenCalledTimes(1)
    expect(mockreply).toHaveBeenCalledWith('Ok')
    expect(info).toHaveBeenCalledTimes(2)
    expect(info).toHaveBeenCalledWith('health:check', {
      loggerMessage: 'redis',
      success: false,
      transactionId: 'AAAAA',
    })
  })
})
