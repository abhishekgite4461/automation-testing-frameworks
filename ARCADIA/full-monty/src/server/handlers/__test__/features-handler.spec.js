import server from '../../server'
import { type } from 'ramda'

describe('consumer feature handler', () => {
  it('returns monty feature flags if consumer is monty', async () => {
    const res = await server.inject({
      url: 'example.com:8080/api/features/monty',
      headers: { 'brand-code': 'tsuk' },
    })

    expect(Array.isArray(res.result.features)).toBeTruthy()
    expect(res.result.features.length > 0).toBeTruthy()
    expect(res.headers['cache-control']).toMatch(/max-age=600/)
  })

  it('returns app feature flags if consumer is app', async () => {
    const res = await server.inject({
      url: 'example.com:8080/api/features/app',
      headers: { 'brand-code': 'tsuk' },
    })

    const body = JSON.parse(res.result)
    expect(body.success).toBe(true)
    expect(type(body.flags)).toBe('Object')
    expect(res.headers['cache-control']).toMatch(/max-age=600/)
  })

  it('404 Not Found for invalid consumer', async () => {
    const res = await server.inject({
      url: 'example.com:8080/api/features/foo',
      headers: { 'brand-code': 'tsuk' },
    })

    const body = JSON.parse(res.result)
    expect(body).toEqual({
      success: false,
      message: "Invalid consumer provided. Should be one of: 'app', 'monty'",
    })
    expect(res.statusCode).toBe(404)
  })
})
