import { postOrderHandler } from '../order'
import arcadiaApi from '../lib/arcadia-api'

const mockOrderVbv = require('../mocks/order-vbv.json')

jest.mock('../lib/arcadia-api')

describe('postOrderHandler', () => {
  const req = {
    info: {
      hostname: 'www',
    },
    method: 'GET',
    url: {
      path: 'Hello',
      search: 'XXXX',
    },
    headers: {
      cookie: 'AAAAAAA',
    },
    payload: {
      remoteIpAddress: 'AAAA',
    },
  }

  beforeAll(() => {
    jest.resetAllMocks()

    arcadiaApi.mockImplementation(
      jest.fn(() => {
        const reply = () => {}

        reply.then = (fn) => {
          fn({ body: mockOrderVbv, statusCode: 200 })
          return reply
        }

        reply.catch = () => {
          return reply
        }

        return reply
      })
    )
  })

  afterAll(() => {
    jest.resetAllMocks()
  })

  it('Handle submitting to cookie (POST) VBV', (done) => {
    const reply = () => ({
      code: () => {
        done()
      },
    })

    postOrderHandler(req, reply)

    expect(arcadiaApi).toHaveBeenCalledTimes(1)
  })
})
