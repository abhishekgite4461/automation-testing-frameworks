import Hapi from 'hapi'
import routes from '../../routes'
import { prepareRoutes } from '../../server'
import { simulateTimeout, isTimeoutTriggered } from '../force-timeout'

const setupServer = () => {
  const server = new Hapi.Server().connection({ port: 80 })
  server.route(
    prepareRoutes(routes).filter((r) => /force-timeout/.test(r.path))
  )
  return server
}

const createReq = () => ({
  method: 'GET',
  url: 'http://local.m.topshop.com/api/force-timeout',
})

describe('simulateTimeout', () => {
  it('resets the timeout trigger to false', async () => {
    await setupServer().inject(createReq())

    try {
      await simulateTimeout()
    } catch (error) {
      // this is expected
    }

    expect(isTimeoutTriggered()).toBe(false)
  })

  it('returns a resolved promise', () => {
    const result = simulateTimeout()

    expect(result instanceof Promise).toBe(true)

    result
      .then(() => expect(true).toBe(true))
      // eslint-disable-next-line
      .catch(() => fail('Promise was expected to be rejected'))
  })

  it('the promise has the correct data to signify a timeout', async () => {
    try {
      const response = await simulateTimeout()
      expect(response).toEqual({
        body: {
          timeout: true,
        },
        status: 200,
      })
    } catch (error) {
      // eslint-disable-next-line
      fail('Promise was expected to resolve')
    }
  })
})

describe('forceTimeout', () => {
  it('returns feedback to acknowledge request', async () => {
    const res = await setupServer().inject(createReq())

    expect(res.statusCode).toBe(200)
    expect(res.result.success).toBe(true)
  })

  it('sets global flag to force timeout', async () => {
    await setupServer().inject(createReq())

    expect(isTimeoutTriggered()).toBe(true)
  })
})
