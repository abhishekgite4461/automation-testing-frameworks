import serverSideRender from '../server-side-renderer'
import cmsMocks from '../mocks/cms.json'
import mrCmsMocks from '../mocks/mr-cms.json'
import { signJwt } from '../../lib/auth'
import nock from 'nock'
import { clone } from 'ramda'
import { SUPPORTED_FEATURES } from '../../../shared/constants/features'
import { jsessionidCookieOptions } from '../../api/constants/session'
import { getScripts, getStyles } from '../../lib/get-assets'
import { isFeatureQubitHiddenEnabled } from '../../../shared/selectors/featureSelectors'
import { getFeatures } from '../../lib/features-service'
import paymentsHelper from '../../lib/payments-helper'
import { getRedirect } from '../../../shared/selectors/routingSelectors'
import * as modalActions from '../../../shared/actions/common/modalActions'
import * as storeLocatorActions from '../../../shared/actions/components/StoreLocatorActions'
import * as orderActions from '../../../shared/actions/common/orderActions'
import * as shoppingBagActions from '../../../shared/actions/common/shoppingBagActions'
import * as productViewActions from '../../../shared/actions/common/productViewsActions'
import * as accountActions from '../../../shared/actions/common/accountActions'
import * as debugActions from '../../../shared/actions/components/debugActions'
import * as klarnaActions from '../../../shared/actions/common/klarnaActions'
import * as mobileHostnameActions from '../../../shared/actions/common/mobileHostnameActions'

jest.mock('react-helmet', () => {
  const { Component } = require('react')
  return class MockedHelmet extends Component {
    static rewind = () => ({ title: '', meta: '', link: '' })
    rewind() {}
    render() {
      return null
    }
  }
})

jest.mock('../../lib/get-assets', () => ({
  getScripts: jest.fn(),
  getStyles: jest.fn(),
}))

jest.mock('../../../shared/lib/fetch-component-data', () => () =>
  Promise.resolve()
)

jest.mock(
  '../../../../public/generated-assets.json',
  () => ({
    chunks: {
      testManifest: 'test',
    },
    js: {
      'common/polyfill-ie11.js': 'common/polyfill-ie11.js',
    },
  }),
  {
    virtual: true,
  }
)

jest.mock('../../../shared/selectors/featureSelectors', () => ({
  isFeatureEnabled: () => false,
  isFeatureStickyHeaderEnabled: () => false,
  isFeatureCFSIEnabled: () => false,
  isFeatureShowCFSIEspotEnabled: () => false,
  isFeaturePUDOEnabled: () => false,
  isFeatureSavePaymentDetailsEnabled: () => false,
  isFeatureTransferBasketEnabled: () => false,
  isFeatureWishlistEnabled: () => false,
  isFeatureMyCheckoutDetailsEnabled: () => false,
  isFeatureAddressBookEnabled: () => false,
  isFeaturePLPRedirectIfOneProductEnabled: () => false,
  isAmplienceFeatureEnabled: () => false,
  isFeatureDDPPromoEnabled: () => false,
  isFeatureQubitHiddenEnabled: jest.fn(),
  isFeatureUnifiedLoginRegisterEnabled: jest.fn(),
  isFeatureHomePageSegmentationEnabled: jest.fn(),
}))

jest.mock('../../../shared/selectors/routingSelectors', () => ({
  getRedirect: jest.fn(),
  getRoutePath: jest.fn(),
  isHomePage: () => false,
  isInCheckout: () => false,
}))

jest.mock('../../../shared/lib/localisation', () => ({
  getLocaleDictionary: () => {},
  localise: () => 'en',
}))

jest.mock('../../../shared/selectors/ddpSelectors', () => ({
  isDDPUserInPreExpiryWindow: () => {},
}))

jest.mock('../../lib/logger')

jest.mock('../../lib/features-service', () => ({
  getFeatures: jest.fn(),
}))

jest.mock('../../lib/payments-helper', () => jest.fn())

jest.mock('../../api/api', () => ({
  generateNewSessionKey: () => '__new_jsessionid__',
}))

jest.mock('../../../shared/actions/common/paymentMethodsActions')

const { FEATURE_GEOIP } = SUPPORTED_FEATURES
// Basic request DO NOT MUTATE.
// Will render homepage with no state.
const BASE_REQUEST = {
  info: {
    hostname: 'topshop',
  },
  url: {
    pathname: '/',
    query: {},
  },
  headers: {
    cookie: '',
  },
  state: {},
}

// Helper method for nocking many endpoints on a single API url
const nockAll = (api, endpoints) => {
  Object.keys(endpoints).forEach((url) => {
    nock(api)
      .get(url)
      .reply(200, endpoints[url])
  })
}
const API_URL = 'http://localhost:3000'
// Nock all the cms endpoints
const nockCms = () => {
  Object.keys(cmsMocks).forEach((key) => {
    nock(API_URL)
      .get(`/api/cms/pages/${key}`)
      .reply(200, cmsMocks[key])
  })

  // mrCMS
  Object.keys(mrCmsMocks).forEach((pageName) => {
    nock(API_URL)
      .get(
        `/cmscontent?location=&storeCode=tsuk&brandName=topshop&cmsPageName=${pageName}`
      )
      .reply(200, mrCmsMocks[pageName])
  })
}
// Creates a fake reply method to pass to ssr
// provides promise like methods to use in tests
// Also parses initialState for convenience
const getFakeReply = () => {
  let exposedResolve
  let exposedReject

  const replyPromise = new Promise((resolve, reject) => {
    exposedResolve = resolve
    exposedReject = reject
  })

  const reply = (error) => {
    exposedReject(error)
  }

  reply.view = (page, renderPayload) => {
    exposedResolve({
      ...renderPayload,
      initialState: JSON.parse(renderPayload.initialState),
    })
    reply.initialState = JSON.parse(renderPayload.initialState)
    return reply
  }

  reply.then = (fn) => {
    replyPromise.then(fn)
    return reply
  }

  reply.catch = (fn) => {
    replyPromise.catch(fn)
    return reply
  }

  reply.state = jest.fn(() => reply)

  reply.code = jest.fn()

  reply.redirect = jest.fn(() => {
    exposedResolve()
    return reply
  })

  reply.permanent = jest.fn(() => {
    exposedResolve()
    return reply
  })

  return reply
}
// Mock out some standard dependencies for a ssr call
const mockStandardDependencies = () => {
  nockAll(API_URL, {
    '/api/navigation/categories': {},
    '/api/site-options': { creditCardOptions: [] },
    '/api/cms/pages/mobileTacticalMessageESpotPos1': {},
  })
  nockCms()
}

const getFakeAuthenticatedToken = () =>
  new Promise((resolve) =>
    signJwt(
      {
        'arcadia-session-key': 'testSessionKey',
        email: 'test@test.com',
        userTrackingId: 'testTrackingId',
        exists: true,
        hasCardNumberHash: true,
        hasPayPal: true,
        hasDeliveryDetails: true,
        hasBillingDetails: true,
      },
      (err, token) => resolve(token)
    )
  )

let reply

describe('server-side-render', () => {
  beforeEach(() => {
    nock.cleanAll()
    mockStandardDependencies()
    jest.resetAllMocks()
    reply = getFakeReply()
    getFeatures.mockImplementation(() => [])

    global.process.env.API_URL = 'https://some.fake.url/api'
  })

  describe('@render', () => {
    afterEach(() => {
      jest.restoreAllMocks()
    })

    it('If the user lands on the homepage, outputs the initialState with no personal data', async () => {
      const token = await getFakeAuthenticatedToken()

      serverSideRender(
        {
          ...BASE_REQUEST,
          jwtPayload: {
            exists: true,
            isPwdReset: false,
          },
          state: {
            token,
          },
        },
        reply
      )

      const { html, initialState } = await reply

      // User State is removed
      expect(initialState.account.user).toEqual({})
      expect(initialState.auth.authentication).toBeFalsy()

      // No instances of it anywhere else
      const stringState = JSON.stringify(initialState)
      expect(stringState.includes('testSessionKey')).toBeFalsy()
      expect(stringState.includes('test@test.com')).toBeFalsy()
      expect(stringState.includes('testTrackingId')).toBeFalsy()

      // Or in the markup
      expect(html.includes('testSessionKey')).toBeFalsy()
      expect(html.includes('test@test.com')).toBeFalsy()
      expect(html.includes('testTrackingId')).toBeFalsy()
    })

    it('outputs an html string for rendering', async () => {
      serverSideRender(BASE_REQUEST, reply)

      const { html } = await reply
      expect(typeof html).toBe('string')
    })

    it('outputs the newrelic script response as nreum', async () => {
      serverSideRender(BASE_REQUEST, reply)

      const { initialState } = await reply
      expect(typeof initialState).toBe('object')
    })

    it('outputs the assets for the app to inject', async () => {
      getScripts.mockImplementation(() => 'string')
      getStyles.mockImplementation(() => 'string')
      serverSideRender(BASE_REQUEST, reply)

      const { scripts, styles } = await reply
      expect(typeof scripts).toBe('string')
      expect(typeof styles).toBe('string')
    })

    it('outputs meta data for the page', async () => {
      serverSideRender(BASE_REQUEST, reply)

      const {
        brandName,
        lang,
        title,
        meta,
        link,
        version,
        webpackManifest,
        isRedAnt,
      } = await reply
      expect(typeof brandName).toBe('string')
      expect(typeof lang).toBe('string')
      expect(typeof title).toBe('string')
      expect(typeof meta).toBe('string')
      expect(typeof link).toBe('string')
      expect(typeof version).toBe('string')
      expect(typeof webpackManifest).toBe('string')
      expect(typeof isRedAnt).toBe('boolean')
    })

    describe('GeoIP', () => {
      modalActions.toggleModal = jest.fn()

      beforeEach(() => {
        jest.clearAllMocks()
        getFeatures.mockImplementation(() => [])
      })

      it('sets the Geo IP state when a request comes in and calls toggle modal to display the content overlay when a redirect url should be provided', async () => {
        modalActions.toggleModal.mockReturnValueOnce({})
        const info = {
          hostname: 'm.topshop.com',
        }
        const headers = {
          cookie: 'GEOIP=US',
          'x-user-geo': 'GB',
        }
        const state = {
          featuresOverride: JSON.stringify({
            [FEATURE_GEOIP]: true,
          }),
        }
        const request = { ...BASE_REQUEST, headers, state, info }

        serverSideRender(request, reply)
        const { initialState } = await reply

        expect(initialState.geoIP).toMatchObject({
          redirectURL: '',
          hostname: 'm.topshop.com',
          geoISO: 'GB',
          storedGeoPreference: 'US',
        })
        expect(modalActions.toggleModal).toHaveBeenCalled()
      })

      it('sets the Geo IP state when a request comes in and does not call the toggle modal to display the content overlay when a redirect url should not be provided', async () => {
        modalActions.toggleModal.mockReturnValueOnce({})
        const info = {
          hostname: 'm.topshop.com',
        }
        const headers = {
          cookie: '',
          'x-user-geo': 'GB',
        }
        const state = {
          featuresOverride: JSON.stringify({
            [FEATURE_GEOIP]: true,
          }),
        }
        const request = { ...BASE_REQUEST, headers, state, info }

        serverSideRender(request, reply)
        const { initialState } = await reply

        expect(initialState.geoIP).toMatchObject({
          redirectURL: '',
          hostname: 'm.topshop.com',
          geoISO: 'GB',
          storedGeoPreference: '',
        })
        expect(modalActions.toggleModal).not.toHaveBeenCalled()
      })
    })

    describe('pathname', () => {
      it('should set the store locator country', async () => {
        jest.spyOn(storeLocatorActions, 'selectCountry')

        const request = clone(BASE_REQUEST)
        request.url.pathname = '/store-locator'
        request.url.query.country = 'GB'

        serverSideRender(request, reply)

        await reply

        expect(storeLocatorActions.selectCountry).toHaveBeenCalledWith('GB')
      })

      it('should set the store locator country to "United Kingdom"', async () => {
        jest.spyOn(storeLocatorActions, 'selectCountry')

        const request = clone(BASE_REQUEST)
        request.url.pathname = '/store-locator'
        request.url.query = {
          param1: 'p1',
        }

        serverSideRender(request, reply)

        await reply

        expect(storeLocatorActions.selectCountry).toHaveBeenCalledWith(
          'United Kingdom'
        )
      })

      it('should set the order to completed', async () => {
        jest.spyOn(orderActions, 'setOrderPendingConfirm')
        paymentsHelper.mockImplementation((p1, p2) => p2('data'))

        const request = clone(BASE_REQUEST)
        request.url.pathname = '/order-complete'

        serverSideRender(request, reply)

        await orderActions.setOrderPendingConfirm

        expect(orderActions.setOrderPendingConfirm).toHaveBeenCalledWith('data')
      })

      it('should set the order error', async () => {
        jest.spyOn(orderActions, 'setOrderError')
        paymentsHelper.mockImplementation((p1, p2) => p2())

        const request = clone(BASE_REQUEST)
        request.url.pathname = '/order-complete'

        serverSideRender(request, reply)

        await orderActions.setOrderError

        expect(orderActions.setOrderError).toHaveBeenCalledWith(
          'Unable to complete payment, please retry again later'
        )
      })
    })

    describe('bagCount', () => {
      it('should update the shopping bag count', async () => {
        jest.spyOn(shoppingBagActions, 'updateShoppingBagBadgeCount')

        const request = clone(BASE_REQUEST)
        request.state.bagCount = 2

        serverSideRender(request, reply)

        await reply

        expect(
          shoppingBagActions.updateShoppingBagBadgeCount
        ).toHaveBeenCalledWith(2)
      })
    })

    describe('product view', () => {
      it('should display the product view', async () => {
        jest.spyOn(productViewActions, 'selectView')

        const request = clone(BASE_REQUEST)
        request.state.productIsActive = 'true'

        serverSideRender(request, reply)

        await reply

        expect(productViewActions.selectView).toHaveBeenCalledWith('Product')
      })

      it('should display the outfit view', async () => {
        jest.spyOn(productViewActions, 'selectView')

        const request = clone(BASE_REQUEST)
        request.state.productIsActive = 'false'

        serverSideRender(request, reply)

        await reply

        expect(productViewActions.selectView).toHaveBeenCalledWith('Outfit')
      })
    })

    describe('password reset', () => {
      it('should dispatch the USER_ACCOUNT action', async () => {
        jest.spyOn(accountActions, 'userAccount')

        const request = clone(BASE_REQUEST)
        request.jwtPayload = {
          isPwdReset: true,
        }

        serverSideRender(request, reply)

        await reply

        expect(accountActions.userAccount).toHaveBeenCalledWith(
          request.jwtPayload
        )
      })
    })

    describe('monty debug', () => {
      it('should allow debugging', async () => {
        jest.spyOn(debugActions, 'allowDebug')

        const request = clone(BASE_REQUEST)
        request.state = {
          montydebug: true,
        }

        serverSideRender(request, reply)

        await reply

        expect(debugActions.allowDebug).toHaveBeenCalled()
      })
    })

    describe('klarna', () => {
      it('should set the klarna session id', async () => {
        jest.spyOn(klarnaActions, 'setKlarnaSessionId')

        const request = clone(BASE_REQUEST)
        request.state = {
          klarnaSessionId: 'id',
        }

        serverSideRender(request, reply)

        await reply

        expect(klarnaActions.setKlarnaSessionId).toHaveBeenCalledWith('id')
      })

      it('should set the klarna client token', async () => {
        jest.spyOn(klarnaActions, 'setKlarnaClientToken')

        const request = clone(BASE_REQUEST)
        request.state = {
          klarnaClientToken: 'token',
        }

        serverSideRender(request, reply)

        await reply

        expect(klarnaActions.setKlarnaClientToken).toHaveBeenCalledWith('token')
      })
    })

    describe('arc promo code', () => {
      it('should be set', async () => {
        jest.spyOn(shoppingBagActions, 'setPromotionCode')

        const request = clone(BASE_REQUEST)
        request.state = {
          arcpromoCode: 'code',
        }

        serverSideRender(request, reply)

        await reply

        expect(shoppingBagActions.setPromotionCode).toHaveBeenCalledWith('code')
      })
    })
  })

  describe('QBIT', () => {
    const qEnv = global.process.env.QUBIT_DISABLED
    let originalGoogleAPIKey
    beforeEach(() => {
      originalGoogleAPIKey = process.env.GOOGLE_API_KEY
      process.env.GOOGLE_API_KEY = undefined
    })

    afterEach(() => {
      global.process.env.QUBIT_DISABLED = qEnv
      process.env.GOOGLE_API_KEY = originalGoogleAPIKey
    })

    it('should call getScripts with no open ref tag because feature flag is on', async () => {
      global.process.env.GOOGLE_API_KEY = 'googleApiKey'
      isFeatureQubitHiddenEnabled.mockImplementation(() => true)
      const request = clone(BASE_REQUEST)
      serverSideRender(request, reply)
      await reply
      expect(getScripts).toHaveBeenCalledTimes(1)
      expect(getScripts).toHaveBeenCalledWith({
        googleMapsKey: global.process.env.GOOGLE_API_KEY,
        locale: 'en',
        opentagRef: undefined,
        region: 'uk',
      })
    })

    it('should call getScripts with no open ref tag because environment flag is set to "true"', async () => {
      global.process.env.QUBIT_DISABLED = 'true'
      global.process.env.GOOGLE_API_KEY = 'googleApiKey'
      const request = clone(BASE_REQUEST)
      serverSideRender(request, reply)
      await reply
      expect(getScripts).toHaveBeenCalledTimes(1)
      expect(getScripts).toHaveBeenCalledWith({
        googleMapsKey: global.process.env.GOOGLE_API_KEY,
        locale: 'en',
        opentagRef: undefined,
        region: 'uk',
      })
    })

    it('should call getScripts with open ref tag because environment flag is anything but "true"', async () => {
      global.process.env.GOOGLE_API_KEY = 'googleApiKey'
      global.process.env.QUBIT_DISABLED = 'false'
      const request = clone(BASE_REQUEST)
      serverSideRender(request, reply)
      await reply
      expect(getScripts).toHaveBeenCalledTimes(1)
      expect(getScripts).toHaveBeenCalledWith({
        googleMapsKey: global.process.env.GOOGLE_API_KEY,
        locale: 'en',
        opentagRef: 'topshopuk',
        region: 'uk',
      })
    })

    it('should call getScripts with open ref tag because feature flag is not on', async () => {
      global.process.env.GOOGLE_API_KEY = 'googleApiKey'
      isFeatureQubitHiddenEnabled.mockImplementation(() => false)
      const request = clone(BASE_REQUEST)
      serverSideRender(request, reply)
      await reply
      expect(getScripts).toHaveBeenCalledTimes(1)
      expect(getScripts).toHaveBeenCalledWith({
        googleMapsKey: global.process.env.GOOGLE_API_KEY,
        locale: 'en',
        opentagRef: 'topshopuk',
        region: 'uk',
      })
    })
  })

  describe('deviceType', () => {
    it('is set as cookie with value "desktop" if no request property "monty-client-device-type"', async () => {
      const request = clone(BASE_REQUEST)
      serverSideRender(request, reply)
      await reply
      expect(reply.state.mock.calls[1]).toEqual([
        'deviceType',
        'desktop',
        { path: '/' },
      ])
    })
    it('is set as cookie with value "desktop" in case of request property "monty-client-device-type" set to desktop', async () => {
      const request = clone(BASE_REQUEST)
      request['monty-client-device-type'] = 'desktop'
      serverSideRender(request, reply)
      await reply
      expect(reply.state.mock.calls[1]).toEqual([
        'deviceType',
        'desktop',
        { path: '/' },
      ])
    })
    it('is set as cookie with value "mobile" in case of request property "monty-client-device-type" set to mobile', async () => {
      const request = clone(BASE_REQUEST)
      request.headers['monty-client-device-type'] = 'mobile'
      serverSideRender(request, reply)
      await reply
      expect(reply.state.mock.calls[1]).toEqual([
        'deviceType',
        'mobile',
        { path: '/' },
      ])
    })
  })

  describe('State property hostname -> mobile', () => {
    it('is set to true for mobile hostname', async () => {
      const request = { ...BASE_REQUEST, info: { hostname: 'm.topshop.com' } }
      const spy = jest.spyOn(mobileHostnameActions, 'setIsMobileHostname')

      serverSideRender(request, reply)
      await reply

      expect(true).toBe(true)
      expect(mobileHostnameActions.setIsMobileHostname).toHaveBeenCalledWith(
        true
      )

      spy.mockRestore()
    })
    it('is set to false for desktop hostname', async () => {
      const request = { ...BASE_REQUEST, info: { hostname: 'www.topshop.com' } }
      const spy = jest.spyOn(mobileHostnameActions, 'setIsMobileHostname')

      serverSideRender(request, reply)
      await reply

      expect(true).toBe(true)
      expect(mobileHostnameActions.setIsMobileHostname).toHaveBeenCalledWith(
        false
      )

      spy.mockRestore()
    })
  })

  describe('Jsession', () => {
    it('sets state if in cookie', async () => {
      const request = clone(BASE_REQUEST)
      request.state.jsessionid = 'AAAAA'
      serverSideRender(request, reply)
      await reply
      expect(reply.state).toHaveBeenCalledWith(
        'jsessionid',
        'AAAAA',
        jsessionidCookieOptions
      )
    })

    it('sets a new jsessionid if not in cookie', async () => {
      const request = clone(BASE_REQUEST)
      request.state.jsessionid = undefined
      serverSideRender(request, reply)
      await reply
      expect(reply.state).toHaveBeenCalledWith(
        'jsessionid',
        '__new_jsessionid__',
        jsessionidCookieOptions
      )
    })

    it('clears state', async () => {
      const request = clone(BASE_REQUEST)
      request.state.jsessionid = 'AAAAA'
      serverSideRender(request, reply)
      await reply
      expect(reply.state.jsessionid).toEqual(undefined)
    })
  })

  describe('IE11 Polyfill', () => {
    it('should be included in the page', async () => {
      const request = clone(BASE_REQUEST)
      serverSideRender(request, reply)
      const { iePolyFill } = await reply
      expect(iePolyFill).toBe('common/polyfill-ie11.js')
    })
  })

  describe('debug environment', () => {
    const apiUrl = global.process.env.API_URL
    const coreEnv = global.process.env.WCS_ENVIRONMENT
    const wcsHandlder = global.process.env.USE_NEW_HANDLER

    afterEach(() => {
      global.process.env.API_URL = apiUrl
      global.process.env.WCS_ENVIRONMENT = coreEnv
      global.process.env.USE_NEW_HANDLER = wcsHandlder
    })

    it('should handle core API', async () => {
      global.process.env.USE_NEW_HANDLER = true
      global.process.env.WCS_ENVIRONMENT = 'prod'
      const request = clone(BASE_REQUEST)
      serverSideRender(request, reply)
      const { initialState } = await reply
      expect(initialState.debug.environment).toBe('prod')
    })

    it('should handle Scrapi', async () => {
      global.process.env.USE_NEW_HANDLER = false
      global.process.env.API_URL = 'https://prod.api.arcadiagroup.co.uk/api'
      const request = clone(BASE_REQUEST)
      serverSideRender(request, reply)
      const { initialState } = await reply
      expect(initialState.debug.environment).toBe('prod')
    })
  })

  describe('Redirect', () => {
    it('does not redirect if routing redirect is not present', async () => {
      serverSideRender(clone(BASE_REQUEST), reply)
      await reply

      expect(reply.redirect).not.toHaveBeenCalled()
      expect(reply.permanent).not.toHaveBeenCalled()
    })

    it('does redirect if routing redirect is present', async () => {
      const redirect = 'redirect.com/redirect-pathname'
      getRedirect.mockImplementation(() => ({
        url: redirect,
      }))

      serverSideRender(clone(BASE_REQUEST), reply)
      await reply

      expect(reply.redirect).toHaveBeenCalledTimes(1)
      expect(reply.redirect).toHaveBeenCalledWith(redirect)

      expect(reply.permanent).not.toHaveBeenCalled()
    })

    it('does permanently redirect if routing redirect is present and permanent flag is true', async () => {
      const redirect = 'redirect.com/redirect-pathname'
      getRedirect.mockImplementation(() => ({
        url: redirect,
        permanent: true,
      }))

      serverSideRender(clone(BASE_REQUEST), reply)
      await reply

      expect(reply.redirect).toHaveBeenCalledTimes(1)
      expect(reply.redirect).toHaveBeenCalledWith(redirect)

      expect(reply.permanent).toHaveBeenCalledTimes(1)
      expect(reply.permanent).toHaveBeenCalledWith(true)
    })
  })
})
