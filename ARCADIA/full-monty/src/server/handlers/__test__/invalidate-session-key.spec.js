import Hapi from 'hapi'
import routes from '../../routes'
import { prepareRoutes } from '../../server'
import { signJwt } from '../../lib/auth'

const setupServer = () => {
  const server = new Hapi.Server().connection({ port: 80 })
  server.route(
    prepareRoutes(routes).filter((r) => /invalidate-session-key/.test(r.path))
  )
  return server
}

const createReq = ({ cookie }) => ({
  method: 'GET',
  url: 'http://local.m.topshop.com/api/invalidate-session-key',
  headers: { cookie },
})

describe('invalidateSessionKey', () => {
  it('400 failure if token cookie is not present', async () => {
    const res = await setupServer().inject(createReq({ cookie: '' }))

    expect(res.statusCode).toBe(400)
    expect(res.result.error).toBe('Bad Request')
  })

  it('400 failure if token cannot be decoded', async () => {
    const res = await setupServer().inject(createReq({ cookie: 'token=123; ' }))

    expect(res.statusCode).toBe(400)
    expect(res.result.message).toMatch(/jwt malformed/)
  })

  const signJwtPromise = (payload) =>
    new Promise((res, rej) => {
      signJwt(payload, (err, token) => {
        if (err) return rej(err)
        res(token)
      })
    })

  it('200 successful', async () => {
    const server = setupServer()
    const validKey = '123-123-123-123'

    const token = await signJwtPromise({ 'arcadia-session-key': validKey })

    const res = await server.inject(createReq({ cookie: `token=${token}; ` }))

    expect(res.statusCode).toBe(200)
    expect(res.result.success).toBe(true)
    expect(res.result.oldToken['arcadia-session-key']).toBe(validKey)
    expect(res.result.newToken['arcadia-session-key']).toBe('foobarbaz')
  })
})
