import * as mrCmsHandlerModule from '../mr-cms-handler'
import * as mrCmsUtilsModule from '../../lib/mrcms-utils'
import superagent from 'superagent'

jest.mock('../../lib/mrcms-utils', () => ({
  sanitizeResponseAndReply: jest.fn(),
  logMrCmsRequest: jest.fn(),
  logMrCmsResponse: jest.fn(),
}))

jest.mock('superagent', () => ({
  get: jest.fn(),
}))

describe('mr-cms-handler', () => {
  const mockReply = jest.fn()
  mockReply.proxy = jest.fn()
  const request = 'request'

  beforeAll(() => {
    jest.resetAllMocks()
  })

  beforeEach(() => {
    global.process.env.CMS_TEST_MODE = false
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  afterAll(() => {
    jest.resetAllMocks()
  })

  describe('#mrCmsContentHandler', () => {
    it('executes reply.proxy in case of global.process.env.CMS_TEST_MODE set to "false"', () => {
      mrCmsHandlerModule.mrCmsContentHandler(request, mockReply)

      expect(mrCmsUtilsModule.logMrCmsRequest).toHaveBeenCalledWith(request)
      expect(mockReply.proxy).toHaveBeenCalledWith({
          host: global.process.env.MR_CMS_URL,
          port: global.process.env.MR_CMS_PORT,
          protocol: global.process.env.MR_CMS_PROTOCOL,
        timeout: 20000,
        onResponse: mrCmsUtilsModule.sanitizeResponseAndReply,
      })
    })

    it('does not execute reply.proxy in case of global.process.env.CMS_TEST_MODE set to "true"', () => {
      global.process.env.CMS_TEST_MODE = 'true'

      mrCmsHandlerModule.mrCmsContentHandler(request, mockReply)

      expect(mrCmsUtilsModule.logMrCmsRequest).toHaveBeenCalledTimes(0)
      expect(mockReply.proxy).toHaveBeenCalledTimes(0)
      expect(mockReply).toHaveBeenCalledWith(null, { cmsTestMode: true })
    })
  })

  describe('#mrCmsAssetsHandler', () => {
    it('executes reply.proxy in case of global.process.env.CMS_TEST_MODE set to "false"', () => {
      mrCmsHandlerModule.mrCmsAssetsHandler(request, mockReply)
      expect(mrCmsUtilsModule.logMrCmsRequest).toHaveBeenCalledWith(request)
      expect(mockReply.proxy).toHaveBeenCalledWith({
        host: global.process.env.MR_CMS_URL,
        port: global.process.env.MR_CMS_PORT,
        protocol: global.process.env.MR_CMS_PROTOCOL,
        timeout: 20000,
        onResponse: expect.any(Function),
      })
    })

    it('does not execute reply.proxy in case of global.process.env.CMS_TEST_MODE set to "true"', () => {
      global.process.env.CMS_TEST_MODE = 'true'

      mrCmsHandlerModule.mrCmsAssetsHandler(request, mockReply)

      expect(mrCmsUtilsModule.logMrCmsRequest).toHaveBeenCalledTimes(0)
      expect(mockReply.proxy).toHaveBeenCalledTimes(0)
      expect(mockReply).toHaveBeenCalledWith(null, { cmsTestMode: true })
    })

    it('logs the response and processes it when reply.proxy executes the onResponse parameter', () => {
      const onResponseParams = ['error', 'response', request, jest.fn()]

      const mockReply2 = jest.fn()
      mockReply2.proxy = jest.fn((p) => p.onResponse(...onResponseParams))

      mrCmsHandlerModule.mrCmsAssetsHandler(request, mockReply2)

      expect(mockReply2.proxy).toHaveBeenCalledTimes(1)
      expect(mrCmsUtilsModule.logMrCmsResponse).toHaveBeenCalledWith(
        onResponseParams[2],
        onResponseParams[1],
        onResponseParams[0]
      )
      expect(onResponseParams[3]).toHaveBeenCalledWith(
        onResponseParams[0],
        onResponseParams[1]
      )
    })
  })

  describe('#mrCmsHealthHandler', () => {
    const reply = (body) => {
      if (body.status && body.status !== 200) {
        return body.status
      }

      return {
        code: (replyCode) => replyCode,
      }
    }

    beforeAll(() => {
      global.process.env.MR_CMS_PROTOCOL = 'http'
      global.process.env.MR_CMS_URL = 'cms'
      global.process.env.MR_CMS_PORT = '123'
    })

    afterAll(() => {
      global.process.env.MR_CMS_PROTOCOL = null
      global.process.env.MR_CMS_URL = null
      global.process.env.MR_CMS_PORT = null
    })

    it('return 200 if the requests completes successfully', async () => {
      superagent.get.mockImplementationOnce(() =>
        Promise.resolve({
          text: 'ok',
        })
      )

      const result = await mrCmsHandlerModule.mrCmsHealthHandler({}, reply)

      expect(superagent.get).toHaveBeenCalledWith('http://cms:123/health')
      expect(result).toEqual(200)
    })

    it('return the error code if the requests fails', async () => {
      superagent.get.mockImplementationOnce(() =>
        Promise.reject({ error: 'failed', status: 404 })
      )

      const result = await mrCmsHandlerModule.mrCmsHealthHandler({}, reply)

      expect(result).toEqual(404)
    })
  })
})
