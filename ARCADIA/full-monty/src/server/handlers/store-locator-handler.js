import { getStores, getCountries } from './lib/store-locator-service'

export function getStoresHandler({ query }, reply) {
  return getStores(query)
    .then((result) => {
      const { region } = query
      if (region === 'uk') {
        const stores = result.map((store) => ({
          ...store,
          distance: (store.distance * 0.621).toFixed(2),
        }))
        return reply(stores).code(200)
      }
      return reply(result).code(200)
    })
    .catch((error) => reply({ error: error.toString() }).code(400))
}

export function getCountriesHandler({ query }, reply) {
  return getCountries(query)
    .then((result) => {
      reply(result).code(200)
    })
    .catch((error) => reply({ error: error.toString() }).code(400))
}
