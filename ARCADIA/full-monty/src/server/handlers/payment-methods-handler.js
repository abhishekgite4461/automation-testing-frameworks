import GetPaymentsMethods from './lib/payments/getPaymentsMethods'

/* **** We expose this handler (like interface) **** */

/*
 **** GET ****
 *
 * - ACCEPT : (delivery OR type mandatory)
 * delivery (string) - ex: '?delivery=United%20Kingdom'
 * billing (string) - ex: '?billing=United%20Kingdom&delivery=United%20Kingdom'
 * amount (number) - ex: ?delivery=United%20Kingdom&amount=52
 * value (string) [all or payment type(s)] - ex: '?value=KLRNA' - '?value=KLRNA;VISA' - '?value=all'
 * label (string) [payment type(s)] - ex: '?label='Try before you buy' - '?value=Try before you buy;Visa'
 *
 * - RETURN :
 * list of objects (payments methods)
 * single object (payment method details)
 */
export const paymentMethodsHandler = (req, reply) => {
  try {
    const payments = new GetPaymentsMethods(req)
    reply(payments.selectRequest()).code(200)
  } catch (err) {
    reply({ error: err.message }).code(417)
  }
}

export const getPaymentMethodsSpec = {
  summary: 'Returns the available payment methods for the given brand code',
  parameters: [
    {
      in: 'query',
      name: 'delivery',
      description: 'Delivery country name e.g. "United Kingdom"',
      required: true,
      type: 'string',
    },
    {
      in: 'query',
      name: 'billing',
      description: 'Billing country name e.g. "United Kingdom"',
      required: true,
      type: 'string',
    },
    {
      in: 'query',
      name: 'amount',
      description: 'Total order amount',
      pattern: '^\\d+\\.\\d\\d$',
      required: false,
      type: 'string',
    },
  ],
  responses: {
    200: {
      description: 'The available payment methods',
      schema: {
        type: 'array',
        items: {
          required: [
            'value',
            'type',
            'label',
            'description',
            'icon',
            'validation',
          ],
          properties: {
            value: {
              type: 'string',
            },
            type: {
              type: 'string',
            },
            label: {
              type: 'string',
            },
            description: {
              type: 'string',
            },
            icon: {
              type: 'string',
            },
            validation: {
              required: ['cardNumber', 'cvv', 'expiryDate', 'startDate'],
              properties: {
                cardNumber: {
                  required: ['length', 'message'],
                  properties: {
                    length: {
                      type: 'number',
                    },
                    message: {
                      type: 'string',
                    },
                  },
                  type: 'object',
                },
                cvv: {
                  required: ['length', 'message'],
                  properties: {
                    length: {
                      type: 'number',
                    },
                    message: {
                      type: 'string',
                    },
                  },
                  type: 'object',
                },
                expiryDate: {
                  type: 'string',
                },
              },
              type: 'object',
            },
          },
        },
      },
    },
  },
}

/*
 **** DELETE ****
 *
 *  - ACCEPT : (payments is mandatory)
 * payments (array) - list of current payment method available
 * type (string) - type of payment to remove from list
 *
 * - RETURN :
 * list of objects (payments methods)
 */
export const removePaymentHandler = (req, reply) => {
  if (req.payload && req.payload.payments) {
    // add a query for interface logic
    req.query = {
      value: req.payload.value,
      payments: req.payload.payments,
      action: 'delete',
    }
    try {
      const payments = new GetPaymentsMethods(req)
      reply(payments.selectRequest()).code(200)
    } catch (err) {
      reply({ error: err.message }).code(417)
    }
  } else {
    reply({ error: 'Missing Params (payment list)' }).code(417)
  }
}
