import 'babel-polyfill'
import React from 'react'
import { omit, path } from 'ramda'
import { renderToString } from 'react-dom/server'
import Helmet from 'react-helmet'
import { RouterContext, match } from 'react-router'
import { Provider as ReduxProvider } from 'react-redux'
import { getBrandConfig, getBrandHostnames } from '../config/index'
import { isMobileHostname } from '../../shared/lib/hostname'
import { getFooterConfig } from '../config/footer_config'
import getVersion from './version-handler'
import fetchComponentData from '../../shared/lib/fetch-component-data'
import configureStore from '../../shared/lib/configure-store'
import getRoutes from '../../shared/getRoutes'
import * as actions from '../../shared/actions/common/authActions'
import { userAccount } from '../../shared/actions/common/accountActions'
import { setAdobeAnalyticsEnv } from '../../shared/actions/common/analyticsActions'
import * as errorMessageActions from '../../shared/actions/common/errorMessageActions'
import * as configActions from '../../shared/actions/common/configActions'
import { setFooterConfig } from '../../shared/actions/common/footerActions'
import { selectCountry } from '../../shared/actions/components/StoreLocatorActions'
import {
  setOrderPendingConfirm,
  setOrderError,
} from '../../shared/actions/common/orderActions'
import paymentsHelper from '../lib/payments-helper'
import { isDayLightSaving } from '../lib/date-time-utils'
import {
  updateShoppingBagBadgeCount,
  setPromotionCode,
} from '../../shared/actions/common/shoppingBagActions'
import { getLocaleDictionary, localise } from '../../shared/lib/localisation'
import * as routingActions from '../../shared/actions/common/routingActions'
import * as localisationActions from '../../shared/actions/common/localisationActions'
import { setPeeriusDomain } from '../../shared/actions/common/peeriusActions'
import {
  allowDebug,
  setDebugInfo,
} from '../../shared/actions/components/debugActions'
import {
  setKlarnaSessionId,
  setKlarnaClientToken,
} from '../../shared/actions/common/klarnaActions'
import { getScripts, getStyles } from '../lib/get-assets'
import { zeroAjaxCounter } from '../../shared/actions/components/LoaderOverlayActions'
import * as logger from '../lib/logger'
import * as newrelic from '../lib/newrelic'
import { initFeatures } from '../../shared/actions/common/featuresActions'
import { preCacheReset } from '../../shared/actions/common/pageCacheActions'
import { selectView } from '../../shared/actions/common/productViewsActions'
import { updateMediaType } from '../../shared/actions/common/viewportActions'
import ContextProvider from '../../shared/lib/context-provider'
import { format as formatPrice } from '../../shared/lib/price'
import { getFeatures } from '../lib/features-service'
import urlp from 'url'
import { setJsessionid } from '../../shared/actions/common/jsessionidActions'
import { setIsMobileHostname } from '../../shared/actions/common/mobileHostnameActions'
import { jsessionidCookieOptions } from '../api/constants/session'
import { isFeatureQubitHiddenEnabled } from '../../shared/selectors/featureSelectors'
import { getRedirect } from '../../shared/selectors/routingSelectors'
import { PRODUCT, OUTFIT } from '../../shared/constants/productImageTypes'
import { getTraceIdFromCookie, getCookieValue } from '../../shared/lib/cookie'
import {
  geoIPActions,
  getRedirectURL,
} from '../../shared/reducers/common/geoIPReducer'
import { toggleModal } from '../../shared/actions/common/modalActions'
import { generateNewSessionKey } from '../api/api'

let generatedAssets

const locales = ['en', 'fr', 'de']
const localeRegEx = (localeCode) =>
  new RegExp(`m(.*)(\\.|\\-)(${localeCode})(\\.|\\-|\\b)`)
const getLocale = (hostname) =>
  locales.find((localeCode) => hostname.match(localeRegEx(localeCode))) || 'en'
const version = JSON.stringify(getVersion())
const buildInfo = JSON.parse(version)

function getGeneratedAssets() {
  if (!generatedAssets)
    generatedAssets = require('../../../public/generated-assets.json') // eslint-disable-line import/no-unresolved
  return generatedAssets
}

function getPromoCode(req) {
  return path(['url', 'query', 'ARCPROMO_CODE'], req) || req.state.arcpromoCode
}

function isQubitAllowed(state, opentagRef) {
  return isFeatureQubitHiddenEnabled(state)
    ? undefined
    : process.env.QUBIT_DISABLED === 'true'
      ? undefined
      : opentagRef
}

function serverSideRender(req, reply) {
  const useNewHandler =
    typeof process.env.USE_NEW_HANDLER === 'string'
      ? process.env.USE_NEW_HANDLER === 'true'
      : Boolean(process.env.USE_NEW_HANDLER)

  const authenticated = process.env.USE_NEW_HANDLER
    ? req.state.authenticated === 'yes'
    : !!(req.jwtPayload && req.jwtPayload.exists)
  logger.info('server-side-render', {
    hasToken: !!req.jwtPayload,
    authenticated,
    sessionKey: path(['jwtPayload', 'arcadia-session-key'], req),
  })

  const {
    info: { hostname },
    url,
    url: { pathname, query },
    state: { bagCount, bvToken, productIsActive, token, featuresOverride },
    headers,
  } = req

  const store = configureStore(
    {
      auth: {
        authentication: false,
        // token set here to use it when fetching component needs, not needed on the client
        token,
      },
    },
    {
      traceId: getTraceIdFromCookie(headers.cookie) || headers['x-trace-id'],
      xUserGeo: headers['x-user-geo'],
    }
  )

  const brandConfig = getBrandConfig(hostname)
  const {
    region,
    brandName,
    language,
    lang,
    currencyCode,
    opentagRef,
    googleTagManagerId,
    logoVersion,
  } = brandConfig
  store.dispatch(configActions.setConfig(brandConfig))

  store.dispatch(setIsMobileHostname(isMobileHostname(hostname)))

  const footerConfig = getFooterConfig(brandName, region)
  store.dispatch(setFooterConfig(footerConfig))

  const localeDictionary = getLocaleDictionary(language, brandName)
  store.dispatch(localisationActions.setLocaleDictionary(localeDictionary))

  const brandConfigFeatures = getFeatures(brandConfig)

  store.dispatch(
    configActions.setBrandHostnames(getBrandHostnames(brandName, hostname))
  )
  store.dispatch(configActions.isDayLightSavingTime(isDayLightSaving()))
  store.dispatch(setPeeriusDomain())
  store.dispatch(setAdobeAnalyticsEnv(process.env.ADOBE_ANALYTICS_ENV))

  if (req.state.montydebug || typeof query.montydebug !== 'undefined') {
    store.dispatch(allowDebug())
  }

  const environment = useNewHandler
    ? process.env.WCS_ENVIRONMENT
    : urlp.parse(process.env.API_URL).host.split('.')[0]
  store.dispatch(setDebugInfo({ environment, buildInfo }))
  if (req.state.klarnaSessionId)
    store.dispatch(setKlarnaSessionId(req.state.klarnaSessionId))
  if (req.state.klarnaClientToken)
    store.dispatch(setKlarnaClientToken(req.state.klarnaClientToken))

  let jsessionid
  if (req.state.jsessionid) {
    logger.info('server-side-render', {
      loggerMessage: 'Setting jsessionId from Cookie',
      jsessionid: req.state.jsessionid,
    })
    jsessionid = req.state.jsessionid
  } else {
    jsessionid = generateNewSessionKey()
  }
  store.dispatch(setJsessionid(jsessionid))

  const features = [...brandConfigFeatures]
  store.dispatch(initFeatures(features, featuresOverride))

  store.dispatch(
    geoIPActions.setGeoIPRequestData({
      hostname: req.info.hostname,
      geoISO: req.headers['x-user-geo'],
      storedGeoPreference: getCookieValue(req.headers.cookie, 'GEOIP'),
    })
  )

  // Without explicitly checking and calling this here we get a fairly substantial
  // delay in the rendering of the GeoIP Modal and the background Content Overlay
  if (getRedirectURL(store.getState())) store.dispatch(toggleModal())

  if (pathname === '/store-locator') {
    if (query.country) {
      store.dispatch(selectCountry(query.country))
    } else if (Object.keys(query).length) {
      store.dispatch(selectCountry('United Kingdom'))
    }
    // payment redirect
  } else if (pathname.includes('/order-complete')) {
    paymentsHelper(req, (data) => {
      const error = 'Unable to complete payment, please retry again later'
      return data
        ? store.dispatch(setOrderPendingConfirm(data))
        : store.dispatch(setOrderError(error))
    })
  }

  if (bagCount)
    store.dispatch(updateShoppingBagBadgeCount(parseInt(bagCount, 10)))

  if (typeof productIsActive !== 'undefined') {
    store.dispatch(selectView(productIsActive === 'true' ? PRODUCT : OUTFIT))
  }

  if (authenticated) {
    store.dispatch(actions.authLogin(bvToken))
    store.dispatch(
      userAccount(omit(['arcadia-session-key', 'iat'], req.jwtPayload))
    )
    store.dispatch(actions.setAuthentication(true))
  }
  // forgotten password flow, getting email info from cookie
  if (req.jwtPayload && req.jwtPayload.isPwdReset) {
    store.dispatch(
      userAccount(omit(['arcadia-session-key', 'iat'], req.jwtPayload))
    )
  }

  store.dispatch(configActions.setAssets(getGeneratedAssets()))
  store.dispatch(configActions.setEnvCookieMessage(process.env.COOKIE_MESSAGE))
  store.dispatch(routingActions.updateLocationServer(url, hostname))

  const { location } = store.getState().routing
  const l = localise.bind(null, language, brandName)
  const p = formatPrice.bind(null, currencyCode)

  const deviceType = req.headers['monty-client-device-type'] || 'desktop'
  store.dispatch(updateMediaType(deviceType))

  match(
    { routes: getRoutes(store, { l }), location },
    (error, redirectLocation, renderProps) => {
      function renderView() {
        const App = renderToString(
          <ContextProvider localise={l} formatPrice={p}>
            <ReduxProvider store={store}>
              <RouterContext {...renderProps} />
            </ReduxProvider>
          </ContextProvider>
        )
        const { title, meta, link } = Helmet.rewind()

        const initialState = JSON.stringify(store.getState()).replace(
          /<(\/?)script>/g,
          '&lt;$1script>'
        ) // #MON-1938

        const scripts = getScripts({
          googleMapsKey:
            process.env[`GOOGLE_API_KEY_${brandName.toUpperCase()}`] ||
            process.env.GOOGLE_API_KEY,
          region,
          locale: getLocale(hostname),
          opentagRef: isQubitAllowed(store.getState(), opentagRef),
        })
        const styles = getStyles(brandName)

        return {
          html: App,
          nreum: newrelic.getBrowserScript(),
          initialState,
          scripts,
          styles,
          brandName,
          lang,
          googleTagManagerId,
          logoVersion,
          title: title.toString(),
          meta: meta.toString(),
          link: link.toString(),
          version,
          webpackManifest: JSON.stringify(getGeneratedAssets().chunks),
          isRedAnt: !!req.headers['ar-app-bundleid'],
          iePolyFill: generatedAssets.js['common/polyfill-ie11.js'],
        }
      }

      function displayServerError(err) {
        // TODO: read error from language file
        store.dispatch(errorMessageActions.setGenericError(err))
        try {
          return reply.view('index', renderView()).code(500)
        } catch (renderError) {
          logger.error('server-side-render', {
            loggerMessage: 'Server Side Error Displaying Error',
            ...renderError,
          })
          return reply(renderError)
        }
      }

      if (redirectLocation) {
        reply.redirect(redirectLocation.pathname)
      } else if (renderProps) {
        const innerRoute = renderProps.routes[1]
        const cmsPageName =
          renderProps.params.cmsPageName || innerRoute.cmsPageName
        const isCmsPage = innerRoute.contentType === 'page'

        if (innerRoute.cacheable) store.dispatch(preCacheReset())

        // if promoCode is present in the cookie or query, set it in the store
        const ARCPROMO_CODE = getPromoCode(req)
        if (ARCPROMO_CODE) store.dispatch(setPromotionCode(ARCPROMO_CODE))

        let jsessionidValue

        fetchComponentData(store.dispatch, renderProps.components, {
          ...renderProps.params,
          ...location,
          // In case of request to the server for the route='/' then the following object
          // will contain the property "cmsPageName" that needs to be passed to the action
          // responsible for the cms content fetching.
          cmsPageName,
          isCmsPage,
        })
          .then(() => store.sagaPromise)
          .then(() => {
            const redirect = getRedirect(store.getState())
            if (redirect) {
              const redirectError = Object.assign(new Error('Redirect'), {
                montyRedirect: redirect,
              })
              throw redirectError
            }
          })
          .then(() => store.dispatch(zeroAjaxCounter()))
          .then(() => {
            jsessionidValue = store.getState().jsessionid.value
            // Removing the jsessionid value from the state so that we avoid passing it to the Client in __INITIAL_STATE__
            // inside the source of the page which could be cached by AKAMAI and served to other
            // Users.
            store.dispatch(setJsessionid())
          })
          .then(renderView)
          .then((viewData) => {
            const { pageStatusCode } = store.getState().routing

            reply
              .view('index', viewData)
              .state('jsessionid', jsessionidValue, jsessionidCookieOptions)
              .state('deviceType', deviceType, { path: '/' })
              .code(pageStatusCode || 200)
          })
          .catch((err) => {
            if (err.montyRedirect) {
              const { url, permanent } = err.montyRedirect
              const response = reply.redirect(url)
              if (permanent) {
                return response.permanent(true)
              }
              return
            }

            logger.error('server-side-render', {
              loggerMessage: 'error',
              ...err,
              path: location.pathname,
            })

            displayServerError(err)
          })
      } else {
        return displayServerError(error)
      }
    }
  )
}

// Required for Rewire to work - issue with the package.
export default serverSideRender
