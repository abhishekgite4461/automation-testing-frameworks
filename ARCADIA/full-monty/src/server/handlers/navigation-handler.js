import arcadiaApi from './lib/arcadia-api'
import Boom from 'boom'
import * as navigationMenu from '../../../test/mocks/navigationMenu.json'

export function categoriesHandler(req, reply) {
  const url = 'navigation'

  if (process.env.CMS_TEST_MODE === 'true') {
    reply(navigationMenu.navigationEntries).code(200)
    return
  }

  arcadiaApi(url, req)
    .then(({ body: { navigationEntries } }) => {
      if (!navigationEntries) {
        reply(Boom.badImplementation('Unexpected response format from the API'))
      } else {
        reply(navigationEntries).code(200)
      }
    })
    .catch(reply)
}
