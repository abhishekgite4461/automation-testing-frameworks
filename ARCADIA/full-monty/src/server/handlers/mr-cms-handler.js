import {
  sanitizeResponseAndReply,
  logMrCmsRequest,
  logMrCmsResponse,
} from '../lib/mrcms-utils'

import request from 'superagent'

function mrCmsHandler(req, reply, passThrough, onResponse) {
  if (process.env.CMS_TEST_MODE === 'true') {
    // No calls to montyCMS to facilitate testing and local development
    return reply(null, { cmsTestMode: true })
  }

  logMrCmsRequest(req)

  return reply.proxy({
    host: process.env.MR_CMS_URL,
    port: process.env.MR_CMS_PORT,
    protocol: process.env.MR_CMS_PROTOCOL,
    timeout: 20000, // 20s before to abort
    onResponse,
  })
}

export function mrCmsContentHandler(req, reply) {
  return mrCmsHandler(req, reply, false, sanitizeResponseAndReply)
}

function onAssetsResponse(err, res, request, reply) {
  logMrCmsResponse(request, res, err)
  reply(err, res)
}

export function mrCmsAssetsHandler(req, reply) {
  return mrCmsHandler(req, reply, true, onAssetsResponse)
}

export function mrCmsHealthHandler(req, reply) {
  const url = `${process.env.MR_CMS_PROTOCOL}://${process.env.MR_CMS_URL}:${
    process.env.MR_CMS_PORT
  }/health`

  return request
    .get(url)
    .then((res) => {
      return reply(res.text).code(200)
    })
    .catch((err) => {
      return reply(err)
    })
}
