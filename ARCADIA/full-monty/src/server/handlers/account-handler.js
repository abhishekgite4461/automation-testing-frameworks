import arcadiaApi from './lib/arcadia-api'
import {
  signJwt,
  cookieOptions,
  cookieOptionsBag,
  cookieOptionsAuth,
  cookieOptionsUnset,
} from '../lib/auth'
import Boom from 'boom'
import { encodeUserId } from '../lib/bazaarvoice-utils'
import { pick, pickBy, identity, isEmpty, omit } from 'ramda'

const jwtMessage = 'jwt signing failed'

const hasAddress = (details = {}) =>
  !isEmpty(pickBy(identity, omit(['country'], details.address)))

const pruneProfile = (body) => {
  const { creditCard } = body
  const profile = {
    ...pick(['email', 'userTrackingId', 'exists'], body),
    hasCardNumberHash: creditCard && !!creditCard.cardNumberHash,
    hasPayPal: creditCard && creditCard.type === 'PYPAL',
    hasDeliveryDetails: hasAddress(body.deliveryDetails),
    hasBillingDetails: hasAddress(body.billingDetails),
  }
  return profile
}

/*
 * Account Registration
 */
export function registerAccountHandler(req, reply) {
  const url = 'account'
  arcadiaApi(url, req)
    .then(({ body, header }) => {
      signJwt(
        {
          'arcadia-session-key': header['arcadia-session-key'],
          ...pruneProfile(body),
        },
        (err, token) => {
          const bvToken = encodeUserId(body.userTrackingId)
          if (err) {
            reply(Boom.badData(jwtMessage, err))
          } else {
            req.disablePreResponse = true
            reply({ ...body, ...pruneProfile(body) })
              .state('token', token, cookieOptions)
              .state('authenticated', 'yes', cookieOptionsAuth)
              .state('bvToken', bvToken, cookieOptions)
              .code(200)
          }
        }
      )
    })
    .catch(reply)
}

/*
 * Account Login
 */

export function loginAccountHandler(req, reply) {
  const url = 'login'
  arcadiaApi(url, req)
    .then(({ body, header }) => {
      signJwt(
        {
          'arcadia-session-key': header['arcadia-session-key'],
          ...pruneProfile(body),
        },
        (err, token) => {
          const bvToken = encodeUserId(body.userTrackingId)
          if (err) {
            reply(Boom.badData(jwtMessage, err))
          } else {
            req.disablePreResponse = true
            reply({ ...body, ...pruneProfile(body) })
              .header('bvtoken', bvToken)
              .state('token', token, cookieOptions)
              .state('bvToken', bvToken, cookieOptions)
              .state(
                'bagCount',
                body.basketItemCount.toString(),
                cookieOptionsBag
              )
              .state('authenticated', 'yes', cookieOptionsAuth)
              .state('klarnaClientToken', null, cookieOptionsUnset)
              .state('klarnaSessionId', null, cookieOptionsUnset)
              .code(200)
          }
        }
      )
    })
    .catch((error) => {
      // If response is: "Password needs to be changed", this is a forgotten password login
      // We will need to redirect to the changePassword page to finish flow
      // To enable server rendering, we will set username in cookie in this case instead of missing profile info (api doesn't send it)
      const { message } = error.output.payload
      const isPwdReset = (msg) => msg === 'Password needs to be changed'
      if (isPwdReset(message)) {
        const payload = {
          email: req.payload.username,
          isPwdReset: true,
          exists: true,
        }
        reply(payload).code(200)
      } else {
        reply(Boom.badData(message, error))
      }
    })
    .catch(reply)
}

export function logoutAccountHandler(req, reply) {
  const url = 'login'
  function success() {
    reply()
      .state('tempsession', 'true', cookieOptions)
      .state('token', null, cookieOptionsUnset)
      .state('bvToken', null, cookieOptionsUnset)
      .state('bagCount', null, cookieOptionsUnset)
      .state('authenticated', null, cookieOptionsAuth)
      .state('klarnaClientToken', null, cookieOptionsUnset)
      .state('klarnaSessionId', null, cookieOptionsUnset)
      .code(200)
  }
  if (req.state && req.state.token) {
    arcadiaApi(url, req)
      .then(success)
      .catch(reply)
  } else {
    success()
  }
}

/*
 * User/Customer Full details
 */
export function customerDetails(req, reply) {
  const url = 'account'
  arcadiaApi(url, req)
    .then(({ body, text }) => {
      signJwt(
        {
          'arcadia-session-key':
            req.jwtPayload && req.jwtPayload['arcadia-session-key'],
          ...pruneProfile(body),
        },
        (err, token) => {
          if (err) {
            reply(Boom.badData(jwtMessage, err))
          } else {
            const parsedText = JSON.parse(text)
            reply(parsedText)
              .state('token', token, cookieOptions)
              .state('authenticated', 'yes', cookieOptionsAuth)
              .code(200)
          }
        }
      )
    })
    .catch(reply)
}

/*
 * User/Customer Short details
 */
export function shortProfileHandler(req, reply) {
  const url = 'account/short-profile'
  arcadiaApi(url, req)
    .then(({ body, text }) => {
      signJwt(
        {
          'arcadia-session-key':
            req.jwtPayload && req.jwtPayload['arcadia-session-key'],
          ...pruneProfile(body),
        },
        (err, token) => {
          if (err) {
            reply(Boom.badData(jwtMessage, err))
          } else {
            const parsedText = JSON.parse(text)
            reply(parsedText)
              .state('token', token, cookieOptions)
              .state('authenticated', 'yes', cookieOptionsAuth)
              .code(200)
          }
        }
      )
    })
    .catch((err) => {
      return reply(err)
    })
}

/*
 * Account Change Password
 */
export function changePasswordHandler(req, reply) {
  const url = 'account/password'
  arcadiaApi(url, req)
    .then((result) => {
      reply(result.body).code(200)
    })
    .catch(reply)
}

/*
 * Account Forget Password
 */
export function forgetPasswordHandler(req, reply) {
  const url = 'account/password'
  arcadiaApi(url, req)
    .then((result) => {
      reply(result.body).code(200)
    })
    .catch(reply)
}

export function getAccount(req, reply) {
  const url = 'account'
  arcadiaApi(url, req)
    .then(({ body }) => {
      // TODO: Need to add in the bvToken via AWS as per normal login method.
      const data = req.url.query.email ? {} : pruneProfile(body)
      signJwt(
        {
          'arcadia-session-key':
            req.jwtPayload && req.jwtPayload['arcadia-session-key'],
          ...data,
        },
        (err, token) => {
          if (err) {
            reply(Boom.badData(jwtMessage, err))
          } else if (body.basketItemCount !== undefined) {
            reply(body)
              .state('token', token, cookieOptions)
              .state(
                'bagCount',
                body.basketItemCount.toString(),
                cookieOptionsBag
              )
              .state('authenticated', 'yes', cookieOptionsAuth)
              .code(200)
          } else {
            reply(body)
              .state('token', token, cookieOptions)
              .state('authenticated', 'yes', cookieOptionsAuth)
              .code(200)
          }
        }
      )
    })
    .catch(reply)
}

// Handler for both orderHistory list and order hsitory details requests
export function orderHistoryHandler(req, reply) {
  const match = /[a-z\-/]*\/(\d+)/.exec(req.url.pathname)
  const routeParam = match ? match[1] : ''
  const url = `account/order/${routeParam}`
  arcadiaApi(url, req)
    .then((result) => {
      reply(result.body).code(200)
    })
    .catch(reply)
}

export function returnHistoryHandler(req, reply) {
  const { orderId, rmaId } = req.params
  const url =
    orderId && rmaId
      ? `/account/return-detail?orderId=${orderId}&rmaId=${rmaId}`
      : '/account/return'
  arcadiaApi(url, req)
    .then((result) => {
      reply(result.body).code(200)
    })
    .catch(reply)
}
