import Boom from 'boom'
import { verifyJwt, signJwt, cookieOptions } from '../lib/auth'

export default (req, reply) => {
  if (!req.state || !req.state.token) {
    return reply(Boom.badRequest('You need a token please. Try logging in.'))
  }

  // Here we clobber the old JWT. Not sure if we are meant to do this but it's how it's implemented in `setWebToken`
  const newToken = { 'arcadia-session-key': 'foobarbaz' }
  verifyJwt(req.state.token, (error, oldToken) => {
    if (error) return reply(Boom.badRequest(error))
    signJwt(newToken, (err, token) => {
      if (err) return reply(Boom.badImplementation(err))

      reply.state('token', token, cookieOptions)
      reply({ success: true, oldToken, newToken })
    })
  })
}
