import paymentsHelper from './payments-helper'

describe('paymentHelper', () => {
  beforeEach(() => jest.resetAllMocks())
  const mockQuery = {
    catalogId: '1234',
  }
  const mockUrl = {
    search: 'foo',
  }
  const mockCb = jest.fn()

  it('has a query without orderId, goes to error case without exception', () => {
    paymentsHelper(
      {
        url: mockUrl,
        payload: undefined,
        query: { ...mockQuery, orderId: undefined },
      },
      mockCb
    )
    expect(mockCb).toHaveBeenCalledTimes(1)
    expect(mockCb).lastCalledWith(false)
  })

  it('has a query without paymentMethod, goes to error case without exception', () => {
    paymentsHelper(
      {
        url: mockUrl,
        payload: undefined,
        query: { ...mockQuery, paymentMethod: undefined },
      },
      mockCb
    )
    expect(mockCb).toHaveBeenCalledTimes(1)
    expect(mockCb).lastCalledWith(false)
  })
})
