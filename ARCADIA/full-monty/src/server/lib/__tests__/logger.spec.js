import {
  isTraceLoggingEnabled,
  isDebugLoggingEnabled,
  isInfoLoggingEnabled,
  trace,
  debug,
  info,
  error,
} from '../logger'

jest.mock('../logger-utils', () => ({
  printLog: jest.fn(),
  maskObject: jest.fn(),
}))
import { printLog, maskObject } from '../logger-utils'

describe('logger', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it('log trace when trace', () => {
    global.process.env.LOGGING_LEVEL = 'TRACE'
    maskObject.mockReturnValue({ aaa: 'bbb' })
    trace('message', { aaa: 'bbb' })
    expect(printLog).toHaveBeenCalledTimes(1)
    expect(printLog).toHaveBeenCalledWith('trace', 'message', { aaa: 'bbb' })
  })

  it('not log trace when debug', () => {
    global.process.env.LOGGING_LEVEL = 'DEBUG'
    trace('message', { aaa: 'bbb' })
    expect(printLog).toHaveBeenCalledTimes(0)
  })

  it('logs debug when debug', () => {
    global.process.env.LOGGING_LEVEL = 'DEBUG'
    debug('message', { aaa: 'bbb' })
    expect(printLog).toHaveBeenCalledTimes(1)
    expect(printLog).toHaveBeenCalledWith('debug', 'message', { aaa: 'bbb' })
  })

  it('logs debug when trace', () => {
    global.process.env.LOGGING_LEVEL = 'TRACE'
    debug('message', { aaa: 'bbb' })
    expect(printLog).toHaveBeenCalledTimes(1)
    expect(printLog).toHaveBeenCalledWith('debug', 'message', { aaa: 'bbb' })
  })

  it('not log debug when info', () => {
    global.process.env.LOGGING_LEVEL = 'INFO'
    debug('message', { aaa: 'bbb' })
    expect(printLog).toHaveBeenCalledTimes(0)
  })

  it('logs info', () => {
    global.process.env.LOGGING_LEVEL = 'INFO'
    info('message', { aaa: 'bbb' })
    expect(printLog).toHaveBeenCalledTimes(1)
    expect(printLog).toHaveBeenCalledWith('info', 'message', { aaa: 'bbb' })
  })

  it('loggs error', () => {
    error('message', { aaa: 'bbb' })
    expect(printLog).toHaveBeenCalledTimes(1)
    expect(printLog).toHaveBeenCalledWith('error', 'message', { aaa: 'bbb' })
  })

  describe('logLevels', () => {
    const loggingLevel = global.process.env.LOGGING_LEVEL
    beforeEach(() => {
      global.process.env.LOGGING_LEVEL = loggingLevel
    })

    it('isTraceLoggingEnabled', () => {
      global.process.env.LOGGING_LEVEL = 'TRACE'
      expect(isTraceLoggingEnabled()).toEqual(true)
      global.process.env.LOGGING_LEVEL = 'DEBUG'
      expect(isTraceLoggingEnabled()).toEqual(false)
      global.process.env.LOGGING_LEVEL = 'INFO'
      expect(isTraceLoggingEnabled()).toEqual(false)
    })

    it('isDebugLoggingEnabled', () => {
      global.process.env.LOGGING_LEVEL = 'TRACE'
      expect(isDebugLoggingEnabled()).toEqual(true)
      global.process.env.LOGGING_LEVEL = 'DEBUG'
      expect(isDebugLoggingEnabled()).toEqual(true)
      global.process.env.LOGGING_LEVEL = 'INFO'
      expect(isDebugLoggingEnabled()).toEqual(false)
    })

    it('isInfoLoggingEnabled', () => {
      global.process.env.LOGGING_LEVEL = 'TRACE'
      expect(isInfoLoggingEnabled()).toEqual(true)
      global.process.env.LOGGING_LEVEL = 'DEBUG'
      expect(isInfoLoggingEnabled()).toEqual(true)
      global.process.env.LOGGING_LEVEL = 'INFO'
      expect(isInfoLoggingEnabled()).toEqual(true)
    })
  })

  describe('maskObject', () => {
    it('masks info', () => {
      global.process.env.LOGGING_LEVEL = 'DEBUG'
      debug('message', { aaa: 'bbb' })
      expect(maskObject).toHaveBeenCalledTimes(1)
      expect(maskObject).toHaveBeenCalledWith({ aaa: 'bbb' })
    })

    it('masks error', () => {
      error('message', { aaa: 'bbb' })
      expect(maskObject).toHaveBeenCalledTimes(2)
      expect(maskObject).toHaveBeenCalledWith({ aaa: 'bbb' })
    })

    it('masks debug', () => {
      info('message', { aaa: 'bbb' })
      expect(maskObject).toHaveBeenCalledTimes(2)
      expect(maskObject).toHaveBeenCalledWith({ aaa: 'bbb' })
    })
  })
})
