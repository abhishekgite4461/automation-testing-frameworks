import { getStyles, getScripts } from '../get-assets'
import fs from 'fs'

// Fake Assets
jest.mock('../get-assets-util', () => ({
  generateAssets: () => {
    return {
      css: {
        'tsuk/styles.css': 'AAAAAA',
      },
      js: {
        'common/vendor.js': 'CCCCC',
        'common/bundle.js': 'CCCCC',
        'common/service-desk.js': 'DDDD',
      },
    }
  },
  disableClientAppAccess: jest.fn(),
}))

describe('get-assets', () => {
  describe('getStyles', () => {
    it('should generate styles', () => {
      expect(getStyles('tsuk')).toEqual(
        '<link rel="stylesheet" href="AAAAAA" />'
      )
    })
  })

  describe('getScripts', () => {
    it('should generate scripts', () => {
      const standardMock = fs.readFileSync(
        require.resolve('./mocks/standardscripts-mock.json'),
        'UTF-8'
      )
      expect(getScripts({ opentagRef: 'hello' })).toEqual(standardMock)
    })

    it('should exclude qubit', () => {
      const standardMockQubitNo = fs.readFileSync(
        require.resolve('./mocks/standardscripts-mock-qbitx.json'),
        'UTF-8'
      )
      expect(getScripts({})).toEqual(standardMockQubitNo)
    })

    it('should include qubit', () => {
      const standardMockQubitYes = fs.readFileSync(
        require.resolve('./mocks/standardscripts-mock-qbit.json'),
        'UTF-8'
      )
      expect(getScripts({ opentagRef: 'hello' })).toEqual(standardMockQubitYes)
    })
    it('should load production tealeaf', () => {
      global.process.env.NODE_ENV = 'production'
      global.process.env.WCS_ENVIRONMENT = 'prod'
      expect(getScripts({ opentagRef: 'hello' })).toMatchSnapshot()
    })
    it('should load develop tealeaf', () => {
      global.process.env.NODE_ENV = 'production'
      global.process.env.WCS_ENVIRONMENT = 'tst1'
      expect(getScripts({ opentagRef: 'hello' })).toMatchSnapshot()
    })
  })
})
