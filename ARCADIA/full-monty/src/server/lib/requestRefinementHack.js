import { path, join, evolve, map, toPairs, compose } from 'ramda'
import { localise } from '../../shared/lib/localisation'
import { getConfigByStoreCode } from '../../server/config'

const encode = compose(encodeURIComponent, JSON.stringify)
const strigiyfyQuery = compose(join('&'), map(join('=')), toPairs, map(encode))

const getlocalise = (req) => {
  const { language, brandName } = getConfigByStoreCode(
    path(['headers', 'brand-code'], req)
  )
  return localise.bind(null, language, brandName)
}

// HACK!
// returns a new url object with updated refinements param in query
// when fetching products on a non-english site, the list of refinements is defined in native language
// this works fine for all refinements, except Price.
// this function translate any non-english keys for price to english and updates the request with the new uri

export const translatePriceKeyInRefinements = (req) => {
  const l = getlocalise(req)
  const priceKey = l`Price`.toLowerCase()

  const translateNonEnglishPriceKey = (r) =>
    r.key === priceKey ? { ...r, key: 'price' } : r

  // map over the decoded refinements and replace only the keys that translates in to "price"
  const translateInRefinementsString = compose(
    JSON.stringify,
    map(translateNonEnglishPriceKey),
    JSON.parse
  )

  const mapRefinementsInQuery = evolve({
    refinements: translateInRefinementsString,
  })

  const query = req.url.query ? mapRefinementsInQuery(req.url.query) : null
  const search = query ? `?${strigiyfyQuery(query)}` : ''
  const path = req.url.pathname + search

  // return the request url with the translated values
  return { ...req.url, query, search, path, href: path }
}
