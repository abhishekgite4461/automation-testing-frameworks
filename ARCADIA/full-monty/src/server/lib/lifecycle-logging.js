import * as logger from './logger'

export default function apply(server) {
  // See https://github.com/hapijs/hapi/blob/master/API.md#server-logs
  server.on('log', (event, tags) => {
    if (tags.load) {
      return logger.error('Request was rejected due to high load', event)
    }
    if (tags.error) {
      let info
      if (tags.internal) info = 'Internal implementation error'
      if (tags.client) info = 'Client error received on listener'
      logger.error('Log event emitted with error tag', {
        lifecycle: 'log',
        info,
        ...event,
      })
    }
  })

  server.on('request', (request, event, tags) => {
    if (tags.error) {
      logger.error('Request event emitted with error tag', {
        lifecycle: 'request',
        host: request.info && request.info.host,
        method: request.method,
        path: request.path,
        query: request.url && request.url.search,
        ...event,
      })
    }
  })

  // Triggered whenever 500 response is sent automatically as a result of Hapi catching an exception from our handlers
  server.on('request-error', (request, error) => {
    logger.error('Server responded with 500', {
      lifecycle: 'request-error',
      host: request.info && request.info.host,
      method: request.method,
      path: request.path,
      query: request.url && request.url.search,
      error: error.message,
      stack: error.stack,
    })
  })
}
