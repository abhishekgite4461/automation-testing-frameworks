import * as logger from './logger'
import AWS from 'aws-sdk'

AWS.config.update({
  accessKeyId: process.env.ACCESS_KEY_ID_DYNAMODB,
  secretAccessKey: process.env.SECRET_ACCESS_KEY_DYNAMODB,
  region: 'eu-west-1',
})
AWS.config.dynamodb = { endpoint: 'https://dynamodb.eu-west-1.amazonaws.com' }
const dynamodb = new AWS.DynamoDB.DocumentClient()

export default function bootstrapEnvVarsFromDynamoDB(cb) {
  dynamodb.scan(
    {
      TableName: process.env.APP_CONFIG_TABLENAME,
    },
    (err, { Items }) => {
      if (err) {
        logger.error(
          'Error occured attempting to fetch env vars from DynamoDB, trying again...'
        )
        bootstrapEnvVarsFromDynamoDB()
      } else {
        Items.map(({ key, value }) => {
          process.env[key] = value
          return value
        })
        if (cb) cb()
      }
    }
  )
}
