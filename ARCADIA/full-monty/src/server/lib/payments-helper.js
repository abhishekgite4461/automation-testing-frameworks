import { path } from 'ramda'
// @note: This function help on server side render to exstract data from POST/GET redirect payment request
// And create the correct payload for each type of payment need a PUT/ORDER request

export default ({ url, payload, query }, cb) => {
  // CASE 1) POST REQUEST : VERYFY BY VISA
  if ((query && query.MD) || (payload && payload.MD)) {
    const orderPayload = {
      md: query.MD || payload.MD,
      paRes: query.PaRes || payload.PaRes,
      authProvider: 'VBV',
    }
    return cb(orderPayload)
  }
  // CASE 2) KLARNA
  if (query.klarnaOrderId) {
    // CASE 2) GET REQUEST : KLARNA
    const order = query.klarnaOrderId
    const orderPayload = {
      order,
      authProvider: 'KLRNA',
    }
    return cb(orderPayload)
  }
  // CASE 3) GET REQUEST : PAYPAL-ALIPAY-CHINAUNION-IDEAL-SOFORT
  if (Object.keys(query).length) {
    const matchOrder = /orderId=(.*?)&/.exec(url.search)
    const orderId = path([1], matchOrder)
    const matchPaymentMethod = /paymentMethod=(.*?)\//.exec(url.search)
    const paymentMethod = path([1], matchPaymentMethod)
    if (orderId) {
      // CASE 3A) PAYPAL (need special payload)
      if (paymentMethod === 'PYPAL') {
        const orderPayload = {
          policyId: query.policyId,
          payerId: query.PayerID,
          userApproved: query.userApproved,
          orderId,
          token: query.token,
          tranId: query.tran_id,
          authProvider: 'PYPAL',
        }
        return cb(orderPayload)
        // CASE 3B) ALIPAY - CHINAUNION - IDEAL - SOFORT
      }
      return cb({
        ...query,
        orderId,
        transId: query.trans_id,
        authProvider: paymentMethod,
      })
    }
  }

  // OTHERS CASE : ERROR ??
  return cb(false)
}
