import { readFileSync } from 'fs'
import { resolve } from 'path'
import { generateAssets, disableClientAppAccess } from './get-assets-util'

const loaderPath = resolve(__dirname, './client-script-loader.js')
const loaderScript = readFileSync(loaderPath, 'utf8')

export function getScripts({ googleMapsKey, region, locale, opentagRef }) {
  const tealeafScript =
    process.env.NODE_ENV !== 'production' ||
    process.env.WCS_ENVIRONMENT !== 'prod'
      ? 'Arcadia_TL_DEV_v5_4_1_291018.js'
      : 'Arcadia_TL_PROD_v5_4_1_291018.js'
  const generatedAssets = generateAssets()
  const scriptsToLoad = [
    {
      src: generatedAssets.js['common/vendor.js'],
    },
    {
      src: generatedAssets.js['common/bundle.js'],
    },
    {
      src: generatedAssets.js['common/service-desk.js'],
    },
    {
      id: 'googleMapsScript',
      defer: true,
      isAsync: true,
      src: `https://maps.googleapis.com/maps/api/js?key=${googleMapsKey}&libraries=places&language=${locale}&region=${region}`,
    },
    {
      id: 'tealeafScript',
      src: `/assets/common/vendors/${tealeafScript}`,
      defer: true,
    },
    {
      id: 'analyticsScript',
      src: '/assets/common/vendors/app-measurement.js',
      defer: true,
    },
  ]
  if (opentagRef) {
    // Don't add qbit if the feature flag (hide qbit) is enabled
    scriptsToLoad.push({
      src: `//d3c3cq33003psk.cloudfront.net/opentag-31935-monty${opentagRef}mobile.js`,
      isAsync: true,
      defer: true,
    })
  }

  const scripts = [
    disableClientAppAccess(),
    loaderScript,
    `window.loadScripts(${JSON.stringify(scriptsToLoad)})`,
  ]

  return `<script type="text/javascript">${scripts.join(';')}</script>`
}

export function getStyles(brandName) {
  const generatedAssets = generateAssets()
  return `<link rel="stylesheet" href="${
    generatedAssets.css[`${brandName}/styles.css`]
  }" />`
}
