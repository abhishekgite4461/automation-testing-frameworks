import bunyan from 'bunyan'
import * as newrelic from './newrelic'
import { printLog, maskObject } from './logger-utils'

const bLog = bunyan.createLogger({
  name: 'monty',
  level: process.env.LOGGING_LEVEL
    ? process.env.LOGGING_LEVEL.toLowerCase()
    : 'info',
})

const checkLevels = (levels) => {
  return (
    process.env.LOGGING_LEVEL &&
    levels.indexOf(process.env.LOGGING_LEVEL.toUpperCase()) > -1
  )
}

export const isTraceLoggingEnabled = () => checkLevels(['TRACE'])

export const isDebugLoggingEnabled = () => checkLevels(['TRACE', 'DEBUG'])

export const isInfoLoggingEnabled = () =>
  checkLevels(['TRACE', 'DEBUG', 'INFO'])

export function trace(message, jsonObject) {
  if (isTraceLoggingEnabled()) {
    bLog.trace({ ...maskObject(jsonObject), namespace: message })
    printLog('trace', message, maskObject(jsonObject))
  }
}

export function debug(message, jsonObject) {
  if (isDebugLoggingEnabled()) {
    bLog.debug({ ...maskObject(jsonObject), namespace: message })
    printLog('debug', message, jsonObject)
  }
}

export const info = (message, jsonObject) => {
  if (isInfoLoggingEnabled()) {
    newrelic.recordCustomEvent('info', maskObject({ message, ...jsonObject }))
    bLog.info({ ...maskObject(jsonObject), namespace: message })
    printLog('info', message, jsonObject)
  }
}

export function error(errorMessage, jsonObject) {
  newrelic.noticeError(errorMessage, jsonObject)
  newrelic.recordCustomEvent(
    'error',
    maskObject({ errorMessage, ...jsonObject })
  )
  bLog.error({ ...maskObject(jsonObject), namespace: errorMessage })
  printLog('error', errorMessage, jsonObject)
}

export function generateTransactionId() {
  return `STRANS${Date.now()}R${Math.floor(Math.random() * 1000000, 0)}`
}
