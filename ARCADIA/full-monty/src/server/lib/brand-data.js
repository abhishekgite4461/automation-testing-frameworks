import { join } from 'path'

const normalised = join(__dirname, '../config/brands')
const brandData = require('fs')
  .readdirSync(normalised)
  .reduce((brandsObj, file) => {
    // eslint-disable-next-line import/no-dynamic-require
    return [...require(`../config/brands/${file}`), ...brandsObj]
  }, [])

export default brandData
