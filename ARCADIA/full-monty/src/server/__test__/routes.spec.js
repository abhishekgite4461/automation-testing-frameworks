jest.mock('../handlers/lib/arcadia-api')

import routes from '../routes'
import requestToArcadiaApi from '../handlers/lib/arcadia-api'

jest.mock('../api/utils', () => ({
  extractCookieValue: jest.fn(() => 'AAA'),
}))
jest.mock('../lib/logger', () => ({
  error: jest.fn(),
  info: jest.fn(),
}))
jest.mock('../api/handler')
import routeHandler from '../api/handler'
import r from 'ramda'

const plpRoute = routes.find((r) => r.path === '/api/products')
const navigationRoutes = routes.find(
  (r) => r.path === '/api/desktop/navigation'
)
const wcsRoute = routes.find((r) => r.path.indexOf('/wcsstore') !== -1)
const mockReply = { code: jest.fn() }
const replyMock = jest.fn(() => mockReply)

function sendFakeRequest(
  route,
  req = {},
  reply = jest.fn() // eslint-disable-line prefer-arrow-callback
) {
  req = {
    method: 'get',
    url: {
      query: '',
      ...(req.url || {}),
    },
    query: {
      ...(req.query || {}),
    },
    ...req,
  }

  route.handler(req, reply)
  return { req, reply }
}

function scrAPIRequestMethod() {
  return requestToArcadiaApi
}

describe('routes', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  describe('api', () => {
    it('calls scrAPI by default', () => {
      sendFakeRequest(plpRoute)
      expect(scrAPIRequestMethod()).toHaveBeenCalled()
      expect(routeHandler).not.toHaveBeenCalled()
    })

    it('calls WCS directly with query param', () => {
      sendFakeRequest(plpRoute, {
        query: {
          wcsPath: 'true',
        },
      })
      expect(scrAPIRequestMethod()).not.toHaveBeenCalled()
      expect(routeHandler).toHaveBeenCalled()
    })
  })

  it('no duplicate routes', () => {
    const uniqueRoutes = r.compose(
      r.uniq,
      r.map((route) => `${route.method}#${route.path}`)
    )(routes)

    expect(routes.length).toBe(uniqueRoutes.length)
  })

  describe('wcsstore calls', () => {
    it('blocks anything that starts with wcsstore/', () => {
      sendFakeRequest(wcsRoute, { url: '/wcsstore/yoursummer.gif' }, replyMock)
      expect(replyMock).toHaveBeenCalled()
      expect(replyMock).toHaveBeenCalledWith('Invalid WCS URL')
    })
  })

  describe('navigationRoutes', () => {
    const globalEnv = global.process.env.USE_NEW_HANDLER

    afterEach(() => {
      global.process.env.USE_NEW_HANDLER = globalEnv
    })

    it('should not call WCS is USE_NEW_HANDLER not set', () => {
      global.process.env.USE_NEW_HANDLER = 'false'
      sendFakeRequest(
        navigationRoutes,
        { url: '/api/desktop/navigation' },
        replyMock
      )
      expect(routeHandler).not.toHaveBeenCalled()
    })

    it('should call WCS is USE_NEW_HANDLER set', () => {
      global.process.env.USE_NEW_HANDLER = 'true'
      sendFakeRequest(
        navigationRoutes,
        { url: '/api/desktop/navigation' },
        replyMock
      )
      expect(routeHandler).toHaveBeenCalledTimes(1)
    })
  })
})
