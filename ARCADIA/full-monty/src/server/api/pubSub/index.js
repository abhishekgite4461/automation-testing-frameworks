// Topic Name:
// projects/arcadia-monty-api-logging/topics/api-logs
//
// Subscription Name:
// projects/arcadia-monty-api-logging/subscriptions/get-logs

import PubSub from '@google-cloud/pubsub'
import * as logger from '../../lib/logger'
import { maskSensitiveData } from './utils'

const pubsub = process.env.GOOGLE_APPLICATION_CREDENTIALS ? new PubSub() : false

const topicName = 'api-logs'

//
// transaction = {
//   timestamp: 1539346419898
//   deviceType: 'desktop'
//   isMobileHostname: 'true'
//   montyHeader: 'mdesktop'
//   clientSessionKey: 'eyJhbGciOiJIUzI1NiJ9.YzFlMDk5NGUtOWVhYy00NDlhLTliYzYtZTFhZTYyMjRlNTBh.INOcKAit8Lhb5pj1Zvl92byqxfH3A6gUNgOqwqCjhv0',
//   reqDestination: 'https://ts-prd1stage.prd.arcadiagroup.co.uk/webapp/wcs/stores/servlet/FooterDisplay',
//   reqMethod: 'get',
//   reqQuery: { langId: '-1', storeId: 12556, catalogId: '33057' },
//   reqPayload: {},
//   reqCookies: [ 'Apache=10.104.183.8.1539338875119576; path=/',
//                 'JSESSIONID=0000grXxh-UvcpHi65XozgcJteI:prd1stage_fe1.wcs01; Path=/',
//                 'WC_SESSION_ESTABLISHED=true; Path=/' ...],
//   resHeaders: { 'content-type': 'text/html; charset=UTF-8',
//                 'x-start': 't=1539338879571130',
//                 'content-encoding': 'gzip', ...
//                 'set-cookie': [ 'WC_USERACTIVITY_-1002=-1002%2C12556%...],
//                 ...
//               },
//   resText: '\n{\n"categories":[\n{"categoryId":"208491",\n\n"url":"/en/tsuk/category/new-in-this-week-2169932...',
//   resError: SyntaxError: Unexpected token s in JSON at position
// }
//
export default function sendToPubSub(transaction) {
  if (!pubsub) {
    // In this case it means that this loggin solution is disabled because of the missing
    // GOOGLE_APPLICATION_CREDENTIALS environment variable.
    return
  }

  transaction = maskSensitiveData(transaction)

  if (!transaction) {
    logger.error('pubSub:error', 'Error while trying to mask transaction')
    return
  }

  let transactionAsString
  try {
    transactionAsString = JSON.stringify(transaction)
  } catch (e) {
    logger.error('pubSub:error', e)
    return
  }

  const data = transactionAsString

  // Publishes the message as a string, e.g. "Hello, world!" or JSON.stringify(someObject)
  const dataBuffer = Buffer.from(data)

  pubsub
    .topic(topicName)
    .publisher()
    .publish(dataBuffer)
    // Leaving this comment just for debugging purposes
    // .then(messageId => {
    //   console.log(`Message ${messageId} published.`)
    // })
    .catch((err) => {
      logger.error('pubSub:error', err)
    })
}
