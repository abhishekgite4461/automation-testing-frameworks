import { maskSensitiveData } from './utils'

describe('#maskSensitiveData', () => {
  it('returns false if the argument "transaction" is not an Object', () => {
    expect(maskSensitiveData(null)).toBe(false)
    expect(maskSensitiveData('unexpected argument type')).toBe(false)
  })
  it('returns false if the argument is an object with no "reqDestination" property or "reqDestination" is not a string', () => {
    const transactionWithInvalidReqDestination = {
      reqDestination: {},
    }
    expect(maskSensitiveData(transactionWithInvalidReqDestination)).toBe(false)
  })
  it('returns an unmodified clone of the "transaction" argument if the transaction is not one of those containing sensitive data', () => {
    const transactionWithoutSensitiveData = {
      reqDestination:
        'https://ts-prd1stage.prd.arcadiagroup.co.uk/webapp/wcs/stores/servlet/FooterDisplay',
      reqQuery: { langId: '-1', storeId: 12556, catalogId: '33057' },
      reqPayload: {},
    }

    expect(maskSensitiveData(transactionWithoutSensitiveData)).toEqual(
      transactionWithoutSensitiveData
    )
  })
  it('returns a masked clone of the "transaction" argument if the transaction is one of those with sensitive data', () => {
    const transactionWithSensitiveData = {
      reqDestination:
        'https://ts-prd1stage.prd.arcadiagroup.co.uk/webapp/wcs/stores/servlet/OrderCalculate',
      reqQuery: {
        cardSecurityNumber: '123',
        cardExpiryYear: '123',
        cardExpiryMonth: '123',
        cardNumberStar: '123',
        cardNumber: '123',
        paymentToken: '123',
      },
      reqPayload: {
        cardSecurityNumber: '123',
        cardExpiryYear: '123',
        cardExpiryMonth: '123',
        cardNumberStar: '123',
        cardNumber: '123',
        paymentToken: '123',
      },
    }

    expect(maskSensitiveData(transactionWithSensitiveData)).toEqual({
      ...transactionWithSensitiveData,
      reqQuery: {
        cardSecurityNumber: '***',
        cardExpiryYear: '***',
        cardExpiryMonth: '***',
        cardNumberStar: '***',
        cardNumber: '***',
        paymentToken: '***',
      },
      reqPayload: {
        cardSecurityNumber: '***',
        cardExpiryYear: '***',
        cardExpiryMonth: '***',
        cardNumberStar: '***',
        cardNumber: '***',
        paymentToken: '***',
      },
    })
  })
  it('returns a masked clone of the "transaction" Do3DEnrollmentCheck', () => {
    const transactionWithSensitiveData = {
      reqDestination:
        'https://ts-prd1stage.prd.arcadiagroup.co.uk/webapp/wcs/stores/servlet/Do3DEnrollmentCheck',
      reqQuery: {},
      reqPayload: {
        ipAddress: '127.0.0.1',
        orderId: 2790588,
        userAgent:
          'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
        deliveryInstructions: '',
        shipCode: 'S',
        langId: '-1',
        storeId: 12556,
        catalogId: '33057',
        errorViewName: 'DoPaymentErrorView',
        contentType: '*/*',
        acceptHeader: '*/*',
        notifyShopper: '0',
        notifyOrderSubmitted: '0',
        smsAlerts: '',
        nominatedDate: '',
        TERMSANDCONDITIONS_OPTIN: 'true',
        save_details: 'on',
        auth_token: undefined,
        billing_address_id: '3454947',
        addressId: '3454946',
        paymentToken: undefined,
        cardSecurityNumber: '123',
        cardExpiryYear: '2021',
        cardExpiryMonth: '10',
        cardNumberStar: '4444333322221111',
        cardBrand: 'VISA',
        cardNumber: undefined,
        mobile:
          'http://local.m.topshop.com:8080/order-complete?paymentMethod=VISA',
      },
    }

    expect(maskSensitiveData(transactionWithSensitiveData)).toEqual({
      ...transactionWithSensitiveData,
      reqQuery: {},
      reqPayload: {
        ipAddress: '127.0.0.1',
        orderId: 2790588,
        userAgent:
          'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
        deliveryInstructions: '',
        shipCode: 'S',
        langId: '-1',
        storeId: 12556,
        catalogId: '33057',
        errorViewName: 'DoPaymentErrorView',
        contentType: '*/*',
        acceptHeader: '*/*',
        notifyShopper: '0',
        notifyOrderSubmitted: '0',
        smsAlerts: '',
        nominatedDate: '',
        TERMSANDCONDITIONS_OPTIN: 'true',
        save_details: 'on',
        auth_token: undefined,
        billing_address_id: '3454947',
        addressId: '3454946',
        paymentToken: '***',
        cardSecurityNumber: '***',
        cardExpiryYear: '***',
        cardExpiryMonth: '***',
        cardNumberStar: '***',
        cardBrand: '***',
        cardNumber: '***',
        mobile:
          'http://local.m.topshop.com:8080/order-complete?paymentMethod=VISA',
      },
    })
  })
  it('returns a masked clone of the "transaction" LogonForm for password update', () => {
    const transactionWithSensitiveData = {
      reqDestination:
        'https://ts-prd1stage.prd.arcadiagroup.co.uk/webapp/wcs/stores/servlet/LogonForm?catalogId=33057&...',
      reqQuery: {},
      reqPayload: {
        pageFrom: '',
        URL: 'LogonForm?page=account',
        reLogonURL: 'ChangePassword',
        Relogon: 'Update',
        nextPage: 'MyAccount',
        'login.x': '63',
        'login.y': '9',
        login: 'Change Password',
        storeId: 12556,
        catalogId: '33057',
        langId: '-1',
        logonId: '12556t@h25.it',
        logonPasswordOld: 'oldpassword123',
        logonPassword: 'newpassword123',
        logonPasswordVerify: 'newpassword123',
        appId: undefined,
        userId: undefined,
        userToken: undefined,
      },
    }

    expect(maskSensitiveData(transactionWithSensitiveData)).toEqual({
      ...transactionWithSensitiveData,
      reqQuery: {},
      reqPayload: {
        pageFrom: '',
        URL: 'LogonForm?page=account',
        reLogonURL: 'ChangePassword',
        Relogon: 'Update',
        nextPage: 'MyAccount',
        'login.x': '63',
        'login.y': '9',
        login: 'Change Password',
        storeId: 12556,
        catalogId: '33057',
        langId: '-1',
        logonId: '***',
        logonPasswordOld: '***',
        logonPassword: '***',
        logonPasswordVerify: '***',
        appId: '***',
        userId: '***',
        userToken: '***',
      },
    })
  })
  it('handles transactions where reqQuery is null', () => {
    const transaction = {
      reqDestination:
        'https://ts-prd1stage.prd.arcadiagroup.co.uk/webapp/wcs/stores/servlet/LogonForm?catalogId=33057&...',
      reqQuery: null,
      reqPayload: {
        logonId: '12556t@h25.it',
        logonPasswordOld: 'oldpassword123',
        logonPassword: 'newpassword123',
        logonPasswordVerify: 'newpassword123',
        appId: undefined,
        userId: undefined,
        userToken: undefined,
      },
    }

    expect(maskSensitiveData(transaction)).toEqual({
      reqDestination:
        'https://ts-prd1stage.prd.arcadiagroup.co.uk/webapp/wcs/stores/servlet/LogonForm?catalogId=33057&...',
      reqQuery: null,
      reqPayload: {
        logonId: '***',
        logonPasswordOld: '***',
        logonPassword: '***',
        logonPasswordVerify: '***',
        appId: '***',
        userId: '***',
        userToken: '***',
      },
    })
  })
  it('handles transactions where reqPayload is null', () => {
    const transaction = {
      reqDestination:
        'https://ts-prd1stage.prd.arcadiagroup.co.uk/webapp/wcs/stores/servlet/LogonForm?catalogId=33057&...',
      reqQuery: {
        logonId: '12556t@h25.it',
        logonPasswordOld: 'oldpassword123',
        logonPassword: 'newpassword123',
        logonPasswordVerify: 'newpassword123',
        appId: undefined,
        userId: undefined,
        userToken: undefined,
      },
      reqPayload: null,
    }

    expect(maskSensitiveData(transaction)).toEqual({
      reqDestination:
        'https://ts-prd1stage.prd.arcadiagroup.co.uk/webapp/wcs/stores/servlet/LogonForm?catalogId=33057&...',
      reqQuery: {
        logonId: '***',
        logonPasswordOld: '***',
        logonPassword: '***',
        logonPasswordVerify: '***',
        appId: '***',
        userId: '***',
        userToken: '***',
      },
      reqPayload: null,
    })
  })
  it('masks transactions to add promo codes', () => {
    const transaction = {
      reqDestination:
        'https://ts-prd1stage.prd.arcadiagroup.co.uk/webapp/wcs/stores/servlet/PromotionCodeManage',
      reqQuery: {},
      reqPayload: {
        actionType: 'verify_promo',
        calculationUsageId: [-1, -2, -7],
        calculateOrder: 1,
        sourcePage: 'OrderItemDisplay',
        URL: 'OrderCalculate?URL=OrderPrepare?URL=PromotionCodeAjaxView',
        errorViewName: 'PromotionCodeAjaxView',
        taskType: 'A',
        catalogId: '33057',
        langId: '-1',
        storeId: 12556,
        promoCode: 'ABC123',
        orderId: '2794582',
      },
    }

    expect(maskSensitiveData(transaction)).toEqual({
      reqDestination:
        'https://ts-prd1stage.prd.arcadiagroup.co.uk/webapp/wcs/stores/servlet/PromotionCodeManage',
      reqQuery: {},
      reqPayload: {
        actionType: 'verify_promo',
        calculationUsageId: [-1, -2, -7],
        calculateOrder: 1,
        sourcePage: 'OrderItemDisplay',
        URL: 'OrderCalculate?URL=OrderPrepare?URL=PromotionCodeAjaxView',
        errorViewName: 'PromotionCodeAjaxView',
        taskType: 'A',
        catalogId: '33057',
        langId: '-1',
        storeId: 12556,
        promoCode: '***',
        orderId: '2794582',
      },
    })
  })
  it('masks transactions to add gift cards', () => {
    const transaction = {
      reqDestination:
        'https://ts-prd1stage.prd.arcadiagroup.co.uk/webapp/wcs/stores/servlet/PPTGiftCardsManager',
      reqQuery: {},
      reqPayload: {
        URL: 'GiftCardsAjaxView',
        errorViewName: 'GiftCardsErrorAjaxView',
        orderId: '2794582',
        catalogId: '33057',
        langId: '-1',
        storeId: 12556,
        giftCardNo: '1234564234234234',
        giftCardPinNo: '1234',
        action: 'add',
        sourcePage: 'OrderSubmitForm',
      },
    }

    expect(maskSensitiveData(transaction)).toEqual({
      reqDestination:
        'https://ts-prd1stage.prd.arcadiagroup.co.uk/webapp/wcs/stores/servlet/PPTGiftCardsManager',
      reqQuery: {},
      reqPayload: {
        URL: 'GiftCardsAjaxView',
        errorViewName: 'GiftCardsErrorAjaxView',
        orderId: '2794582',
        catalogId: '33057',
        langId: '-1',
        storeId: 12556,
        giftCardNo: '***',
        giftCardPinNo: '***',
        action: 'add',
        sourcePage: 'OrderSubmitForm',
      },
    })
  })
})
