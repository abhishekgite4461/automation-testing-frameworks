const parameterWithSensitiveData = [
  'cardSecurityNumber',
  'cardExpiryYear',
  'cardExpiryMonth',
  'cardNumberStar',
  'cardNumber',
  'paymentToken',
  'pay_cardNumberStar',
  'pay_cardNumber',
  'pay_cardExpiryMonth',
  'pay_cardExpiryYear',
  'giftCardNo',
  'giftCardPinNo',
  'cardBrand',
  'logonId',
  'logonPasswordOld',
  'logonPassword',
  'logonPasswordVerify',
  'appId',
  'userId',
  'userToken',
  'create_logonId',
  'reset_logonId',
  'promoCode',
  'shipping_email1',
  'shipping_phone1',
  'billing_phone1',
  'billing_email1',
  'resetPassword',
  'oldPassword',
  'tempEmail1',
  'origLogonId',
  'email',
  'passwordConfirm',
  'password',
]

/**
 * Masks Credit Card, Gift Card, Promo Codes data in the "transaction" object payload/query
 * when the transaction is associated with a WCS endpoint containing such data.
 *
 * @param {Object} transaction {
 *    timestamp: ...
 *    deviceType: '...'
 *    isMobileHostname: '...'
 *    montyHeader: '...'
 *    clientSessionKey: '...',
 *    reqDestination: 'https://ts-prd1stage.prd.arcadiagroup.co.uk/webapp/wcs/stores/servlet/FooterDisplay',
 *    reqMethod: '...',
 *    reqQuery: { langId: '-1', catalogId: '33057', storeId: 12556 },
 *    reqPayload: {...},
 *    reqCookies: [...],
 *    resHeaders: {...},
 *    resText: '...',
 *    resError: '...'
 * }
 * @returns {Object} a clone of the transaction where the sensitive data has been masked
 */
export function maskSensitiveData(transaction) {
  if (!transaction || typeof transaction !== 'object') return false
  if (typeof transaction.reqDestination !== 'string') return false

  const transactionReqQuery = transaction.reqQuery
  const transactionReqPayload = transaction.reqPayload

  function maskProperty(transactionProperty) {
    if (!transactionProperty || typeof transactionProperty !== 'object')
      return transactionProperty

    return Object.keys(transactionProperty).reduce((pre, cur) => {
      return {
        ...pre,
        [cur]: parameterWithSensitiveData.includes(cur)
          ? '***'
          : transactionProperty[cur],
      }
    }, transactionProperty)
  }

  const maskedReqQuery = maskProperty(transactionReqQuery)
  const maskedReqPayload = maskProperty(transactionReqPayload)

  return {
    ...transaction,
    reqQuery: maskedReqQuery,
    reqPayload: maskedReqPayload,
  }
}
