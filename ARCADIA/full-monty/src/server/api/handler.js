/*
  This module has the knowledge about "hapi" in the architecture that permits the communication with the destination API (WCS).
  Knows the "reqeust" object structure and hence is capable to extract "query" and "payload" and to pass these
  to the "mapper" whic doesn't know anything about monty server technology.
  Once the mapper is executed it returns a promise with the "body" of the API response and the handler can
  reply using the hapi "reply" object.

  Let's say that we want to change our server library from "hapi" to "Express": with this architecture only
  the current module will change. It will change the process of extracting from the request object the "query"
  and the "payload" and it will change the way it replies.
  The "mapper" will remain untouched by the library change.
*/

import map from './mapping/map'
import Mapper from './mapping/Mapper'
import { jsessionidCookieOptions } from './constants/session'
import { sessionTimeoutError } from './constants/errors'
import * as logger from '../lib/logger'
import { cookieOptionsUnset } from '../../server/lib/auth'

/**
 * [getMapper provides a specific Mapper which may or may not override the Mapper functions]
 * @param {*} args [pathname, query, payload, method, headers, params]
 *                    {String} pathname [hapi endpoint hit by the current request]
 *                    {String} query          [query parameters of the current hapi request]
 *                    {Object} payload        [payload object of the current request]
 *                    {String} method         [current request's method]
 *                    {Object} headers        [headers passed in the current request]
 *                    {Object} params         [contains the request parameters passed throgh the path (e.g.: /api/products/{identifier} => params = { identifier: '123' })]
 * @return {Object}                [An instance of a specific mapper associated with the hapi endpoint (inherits Mapper and overrides) or Mapper]
 */
export function getMapper(...args) {
  const [originEndpoint, , , method] = args

  const handlerData = map.find((el) => {
    return (
      el.re &&
      typeof el.re.test === 'function' &&
      el.re.test(originEndpoint) &&
      el.method === method
    )
  })

  const SpecificMapper =
    handlerData && handlerData.handler ? handlerData.handler : Mapper

  // e.g.: DeliverySelectoFactory
  const specificMapperIsFactory =
    typeof SpecificMapper.createMapper === 'function'

  return specificMapperIsFactory
    ? SpecificMapper.createMapper(...args)
    : new SpecificMapper(...args)
}

export default function routeHandler(req, reply) {
  const {
    url: { pathname },
    query,
    payload,
    method,
    headers = {},
    params,
  } = req
  const mapper = exports.getMapper(
    pathname,
    query,
    payload,
    method,
    headers,
    params
  )

  const transactionId = logger.generateTransactionId()

  return mapper
    .execute()
    .then((res) => {
      const response = reply(res.body)

      // We return the jsessionId (for both SSR and CSR). This allows the client/SSR to get the latest jsessionId.

      const responseJsessionId = res.jsessionid

      if (responseJsessionId) {
        response.state(
          'jsessionid',
          responseJsessionId,
          jsessionidCookieOptions
        )
      }
      if (res.setCookies) {
        res.setCookies.forEach(({ name, value, options }) =>
          response.state(name, value, options)
        )
      }
      if (res.setHeaders) {
        res.setHeaders.forEach(({ name, value }) =>
          response.header(name, value)
        )
      }

      return response.code(res.status || 200)
    })
    .catch((err) => {
      if (err.message === 'wcsSessionTimeout') {
        const response = reply(sessionTimeoutError)
        logger.error('Handler', {
          transactionId,
          loggerMessage: 'Error',
          ...err,
        })
        response.header('session-expired', true)

        if (process.env.CLEAR_SESSION_KEY_ON_TIMEOUT === 'true') {
          response.state('jsessionid', null, cookieOptionsUnset)
        }

        return response.code(422)
      }

      return reply(err)
    })
}
