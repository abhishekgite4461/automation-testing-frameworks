const sessionTimeoutError = {
  errorStatusCode: 422,
  error: 'Unprocessable Entity',
  message: 'Must be logged in to perform this action',
  originalMessage: 'Must be logged in to perform this action',
}

export { sessionTimeoutError }
