import request from 'superagent'
import extendWithProxy from 'superagent-proxy' // eslint-disable-line import/no-unresolved
import {
  removeCookie,
  mergeCookiesResponse,
  getNewDestination,
  canRequestHaveBody,
} from './utils'
import Boom from 'boom'
import qs from 'qs'
import sendToPubSub from '../pubSub'
import {
  simulateTimeout,
  isTimeoutTriggered,
} from '../../handlers/force-timeout'

const {
  WCS_REQUESTS_THROUGH_PROXY: useProxy,
  WCS_PROXY_PROTOCOL: proxyProtocol,
  WCS_PROXY_USERNAME: proxyUsername,
  WCS_PROXY_PASSWORD: proxyPassword,
  WCS_PROXY_URL: proxyUrl,
  WCS_PROXY_PORT: proxyPort,
} = process.env

const recursivelyFollowRedirectsAndCollectCookies = (
  destination,
  method,
  query,
  payload,
  cookies = [],
  totalRedirections = 0,
  devicetype,
  isMobileHostname,
  clientSessionKey,
  timeout = 0
) => {
  // The logic that follows is necessary on WCS in order to be able to differentiate the orders from
  // mobile hostname from mobile device (monty header = 'mtrue')
  // mobile hostname from dekstop device (monty header = 'mdesktop')
  // destkop hostname from mobile device (monty header = 'true')
  // desktop hostname from dekstop device (monty header = 'desktop')
  // during the Dual Run phase.
  // Potentially this logic can be removed once the Dual Run is over but we need to make sure we don't need
  // this capability in the future for data analysis purposes.
  let montyHeader =
    devicetype && devicetype !== 'mobile'
      ? devicetype === 'apps'
        ? 'apps'
        : 'desktop'
      : 'true'

  if (isMobileHostname === 'true' && devicetype !== 'apps') {
    montyHeader = `m${montyHeader}`
  }

  request.serialize['application/x-www-form-urlencoded'] = (obj) =>
    qs.stringify(obj, { indices: false })

  if (useProxy === 'true') extendWithProxy(request)

  const requestToSend = canRequestHaveBody(method)
    ? request[method](destination).send(payload)
    : request[method](destination)

  if (useProxy === 'true')
    requestToSend.proxy(
      `${proxyProtocol}://${proxyUsername}:${proxyPassword}@${proxyUrl}:${proxyPort}`
    )

  const requestTransaction = {
    timestamp: new Date().getTime(),
    devicetype,
    isMobileHostname,
    montyHeader,
    clientSessionKey,
    reqDestination: destination,
    reqMethod: method,
    reqQuery: query,
    reqPayload: payload,
    reqCookies: cookies,
  }

  const makeRequest =
    process.env.WCS_ENVIRONMENT !== 'prod' && isTimeoutTriggered()
      ? simulateTimeout()
      : requestToSend
          .query(query)
          .set('Cookie', cookies)
          .set('monty', montyHeader)
          .set('Content-Type', 'application/x-www-form-urlencoded')
          .timeout(timeout)
          .redirects(0)

  return makeRequest
    .then((res) => {
      sendToPubSub({
        ...requestTransaction,
        resHeaders: res && res.headers,
        resText: res.text,
      })

      const returnCookies = removeCookie(
        'userCountry',
        mergeCookiesResponse(res, cookies)
      )

      const body =
        res.body && typeof res.body === 'object' && Object.keys(res.body).length
          ? // sometimes the response is unparsed inside the "text" response property and sometimes it is also in "body" as object.
            // We give precedence to the "body".
            res.body
          : // the response content is in "text" and we expect it to be a stringyfied object
            JSON.parse(res.text)

      return { cookies: returnCookies, body }
    })
    .catch((err) => {
      if (err.status !== 302 && err.status !== 307 && err.status !== 301) {
        sendToPubSub({
          ...requestTransaction,
          resError: `status: ${err.status} - message: ${err.message}`,
        })

        if (err.name === 'SyntaxError') {
          throw Boom.badGateway(['Error parsing upstream data'], [err])
        }
        throw err
      } else {
        // The threshold was set to 4 (5 redirects) but after checking on WCS side (Karthi) it is clear that in some
        // legitimate scenarios there could be 10 redirects. Setting the threshold to 15 just to be comfortable.
        if (totalRedirections > 15) {
          sendToPubSub({
            ...requestTransaction,
            resError: 'Maximum number of WCS response redirections exceeded',
          })

          throw Boom.badGateway(
            'Maximum number of WCS response redirections exceeded'
          )
        }

        // Logging request and redirection response.
        sendToPubSub({
          ...requestTransaction,
          resHeaders: err.response && err.response.headers,
          resText: err.response && err.response.text,
          resStatus: err.response && err.response.status,
        })

        const newCookies = mergeCookiesResponse(err.response, cookies)
        const redirectDestination = getNewDestination(err.response)

        return recursivelyFollowRedirectsAndCollectCookies(
          redirectDestination,
          method,
          {},
          payload,
          newCookies,
          totalRedirections + 1,
          devicetype,
          isMobileHostname,
          clientSessionKey,
          timeout
        )
      }
    })
}

const wcs = (
  { destination, method, query, payload },
  cookies,
  devicetype,
  isMobileHostname,
  clientSessionKey,
  timeout
) => {
  return recursivelyFollowRedirectsAndCollectCookies(
    destination,
    method,
    query,
    payload,
    cookies,
    0,
    devicetype,
    isMobileHostname,
    clientSessionKey,
    timeout
  )
}

export default wcs
