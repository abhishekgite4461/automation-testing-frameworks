const requestsWithBodies = [
  'post',
  'put',
  'patch',
  'delete',
  'options',
  'link',
  'unlink',
  'lock',
  'propfind',
  'view',
]

export { requestsWithBodies }
