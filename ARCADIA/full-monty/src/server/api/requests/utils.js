import { requestsWithBodies } from './constants'

const getCookieName = (cookie) => cookie.slice(0, cookie.indexOf('='))

const mergeCookies = (oldCookies, newCookies) => {
  const newCookieNames = newCookies.map(getCookieName)
  const cookies = oldCookies.filter(
    (cookie) => !newCookieNames.includes(getCookieName(cookie))
  )
  return cookies.concat(newCookies)
}

const mergeCookiesResponse = (response, oldCookies) => {
  const newCookies =
    response && response.headers && response.headers['set-cookie']
  return newCookies ? mergeCookies(oldCookies, newCookies) : oldCookies
}

const addCookie = ({ name, value, path = '/' }, cookies = []) => {
  if (!name) return cookies
  return cookies.concat(`${name}=${value}; Path=${path}`)
}

const removeCookie = (name, cookies = []) => {
  if (!name) return cookies
  return cookies.filter((cookie) => name !== getCookieName(cookie))
}

const getNewDestination = (response) =>
  response && response.headers && response.headers.location
    ? response.headers.location
    : ''

const getJSessionId = (cookies) => {
  const cookie = cookies.find((item) => item.startsWith('JSESSIONID=')) || ''
  const startIndex = cookie.indexOf('JSESSIONID=') + 'JSESSIONID='.length
  const endIndex = cookie.indexOf(':', startIndex)
  return cookie.slice(startIndex, endIndex)
}

const canRequestHaveBody = (method) => requestsWithBodies.includes(method)

export {
  getJSessionId,
  mergeCookiesResponse,
  getNewDestination,
  canRequestHaveBody,
  mergeCookies,
  addCookie,
  removeCookie,
}
