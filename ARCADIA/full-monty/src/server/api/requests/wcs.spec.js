import request from 'superagent'
import Boom from 'boom'
import extendWithProxy from 'superagent-proxy'
import * as utils from './utils'

const WCS_REQUESTS_THROUGH_PROXY_old =
  global.process.env.WCS_REQUESTS_THROUGH_PROXY

global.process.env.WCS_REQUESTS_THROUGH_PROXY = 'true'

const wcs = require('./wcs').default

jest.mock('superagent', () => ({
  get: jest.fn(),
  post: jest.fn(),
  serialize: {
    'application/x-www-form-urlencoded': jest.fn(),
  },
}))
jest.mock('boom', () => ({
  badGateway: jest.fn(),
}))
jest.mock('superagent-proxy', () => jest.fn())

const cookies = ['JSESSIONID=0000lB6WWCq8X98Y2gOwk4vQcCA:live_7c2.01; Path=/']
const query = { queryOption: 'x' }
const payload = { payloadOption: 'y' }
const destination = 'http://www.topshop.com/api/call'
const HTTP_REDIRECT_302 = 302

describe('WCS Request', () => {
  const mock = jest.fn()
  const requestRespondsOnceWith = (res) => {
    mock.redirects.mockReturnValueOnce(res)
  }
  const requestRespondsWith = (res) => {
    mock.redirects.mockReturnValue(res)
  }

  beforeEach(() => {
    jest.resetAllMocks()

    mock.query = jest.fn().mockReturnValue(mock)
    mock.set = jest.fn().mockReturnValue(mock)
    mock.redirects = jest.fn().mockReturnValue(mock)
    mock.send = jest.fn().mockReturnValue(mock)
    mock.proxy = jest.fn()
    mock.timeout = jest.fn().mockReturnValue(mock)

    request.get.mockReturnValue(mock)
    request.post.mockReturnValue(mock)
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  afterAll(() => {
    global.process.env.WCS_REQUESTS_THROUGH_PROXY = WCS_REQUESTS_THROUGH_PROXY_old
  })

  describe('Apps', () => {
    it('returns the text response parsed to JSON', () => {
      requestRespondsWith(
        Promise.resolve({
          headers: {
            'set-cookie': cookies,
          },
          text: '{ "success": true }',
        })
      )
      request.post.mockReturnValue(mock)

      return wcs(
        {
          destination,
          query,
          method: 'post',
          payload,
        },
        [],
        'apps'
      ).then((data) => {
        expect(data.cookies).toEqual(cookies)
        expect(data.body.success).toBe(true)

        expect(request.post).toHaveBeenCalledTimes(1)
        expect(request.post).toHaveBeenCalledWith(destination)
        expect(mock.query).toHaveBeenCalledTimes(1)
        expect(mock.query).toHaveBeenCalledWith(query)
        expect(mock.set).toHaveBeenCalledTimes(3)
        expect(mock.set).toHaveBeenCalledWith('Cookie', [])
        expect(mock.set).toHaveBeenCalledWith(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
        expect(mock.set).toHaveBeenCalledWith('monty', 'apps')
        expect(mock.redirects).toHaveBeenCalledTimes(1)
        expect(mock.redirects).toHaveBeenCalledWith(0)
        expect(mock.send).toHaveBeenCalledTimes(1)
        expect(mock.send).toHaveBeenCalledWith(payload)
      })
    })

    it('returns the response body received from WCS', () => {
      requestRespondsWith(
        Promise.resolve({
          headers: {
            'set-cookie': cookies,
          },
          body: {
            content: 'content',
          },
        })
      )
      request.post.mockReturnValue(mock)

      return wcs(
        {
          destination,
          query,
          method: 'post',
          payload,
        },
        [],
        'apps'
      ).then((data) => {
        expect(data.body.content).toEqual('content')
      })
    })
  })

  describe('Mobile device', () => {
    it('returns a response', () => {
      requestRespondsWith(
        Promise.resolve({
          headers: {
            'set-cookie': cookies,
          },
          text: '{ "success": true }',
        })
      )
      request.post.mockReturnValue(mock)

      return wcs(
        {
          destination,
          query,
          method: 'post',
          payload,
        },
        [],
        'mobile'
      ).then((data) => {
        expect(data.cookies).toEqual(cookies)
        expect(data.body.success).toBe(true)

        expect(request.post).toHaveBeenCalledTimes(1)
        expect(request.post).toHaveBeenCalledWith(destination)
        expect(mock.query).toHaveBeenCalledTimes(1)
        expect(mock.query).toHaveBeenCalledWith(query)
        expect(mock.set).toHaveBeenCalledTimes(3)
        expect(mock.set).toHaveBeenCalledWith('Cookie', [])
        expect(mock.set).toHaveBeenCalledWith(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
        expect(mock.set).toHaveBeenCalledWith('monty', 'true')
        expect(mock.redirects).toHaveBeenCalledTimes(1)
        expect(mock.redirects).toHaveBeenCalledWith(0)
        expect(mock.send).toHaveBeenCalledTimes(1)
        expect(mock.send).toHaveBeenCalledWith(payload)
      })
    })

    it('returns a response following a redirect', () => {
      const redirect = 'http://www.topshop.com/api/different?option=5'
      requestRespondsOnceWith(
        Promise.reject({
          status: HTTP_REDIRECT_302,
          response: {
            headers: {
              location: redirect,
              'set-cookie': cookies,
            },
          },
        })
      )
      requestRespondsWith(
        Promise.resolve({
          headers: {
            'set-cookie': [],
          },
          text: '{ "success": true }',
        })
      )

      return wcs(
        {
          destination,
          query: { queryOption: 'x' },
          method: 'post',
          payload: { payloadOption: 'y' },
        },
        [],
        'mobile'
      ).then((data) => {
        expect(data.cookies).toEqual(cookies)
        expect(data.body.success).toBe(true)

        expect(request.post).toHaveBeenCalledTimes(2)
        expect(request.post).toHaveBeenCalledWith(destination)
        expect(request.post).toHaveBeenCalledWith(redirect)

        expect(mock.query).toHaveBeenCalledTimes(2)
        expect(mock.query).toHaveBeenCalledWith(query)
        expect(mock.query).toHaveBeenCalledWith({})

        expect(mock.set).toHaveBeenCalledTimes(6)
        expect(mock.set).toHaveBeenCalledWith('Cookie', [])
        expect(mock.set).toHaveBeenCalledWith(
          'Content-Type',
          'application/x-www-form-urlencoded'
        )
        expect(mock.set).toHaveBeenCalledWith('monty', 'true')

        expect(mock.redirects).toHaveBeenCalledTimes(2)
        expect(mock.redirects).toHaveBeenCalledWith(0)

        expect(mock.send).toHaveBeenCalledTimes(2)
        expect(mock.send).toHaveBeenCalledWith(payload)
      })
    })

    it('throws an error in case of more than 4 redirections', () => {
      requestRespondsWith(
        Promise.reject({
          status: HTTP_REDIRECT_302,
          response: {
            headers: {
              location: 'http://www.topshop.com/api/different',
            },
          },
        })
      )

      return wcs(
        {
          destination,
          query: {},
          method: 'get',
          payload: {},
        },
        [],
        'mobile'
      ).catch(() => {
        expect(Boom.badGateway).toHaveBeenCalledTimes(1)
        expect(Boom.badGateway).toHaveBeenCalledWith(
          'Maximum number of WCS response redirections exceeded'
        )
        expect(request.get).toHaveBeenCalledTimes(17)
      })
    })

    it('Calls WCS passing the header monty="mtrue"', () => {
      requestRespondsWith(
        Promise.resolve({
          headers: {
            'set-cookie': cookies,
          },
          text: '{ "success": true }',
        })
      )

      return wcs(
        {
          destination,
          query,
          method: 'post',
          payload,
        },
        [],
        'mobile',
        'true'
      ).then(() => {
        expect(mock.set).toHaveBeenCalledWith('monty', 'mtrue')
      })
    })
  })

  describe('Non mobile device(tablet/desktop)', () => {
    it('Calls WCS passing the header monty="desktop"', () => {
      requestRespondsWith(
        Promise.resolve({
          headers: {
            'set-cookie': cookies,
          },
          text: '{ "success": true }',
        })
      )

      return wcs(
        {
          destination,
          query,
          method: 'post',
          payload,
        },
        [],
        'desktop'
      ).then(() => {
        expect(mock.set).toHaveBeenCalledWith('monty', 'desktop')
      })
    })

    it('Calls WCS passing the header monty="mdesktop"', () => {
      requestRespondsWith(
        Promise.resolve({
          headers: {
            'set-cookie': cookies,
          },
          text: '{ "success": true }',
        })
      )

      return wcs(
        {
          destination,
          query,
          method: 'post',
          payload,
        },
        [],
        'desktop',
        'true'
      ).then(() => {
        expect(mock.set).toHaveBeenCalledWith('monty', 'mdesktop')
      })
    })
  })

  describe('Proxy is enabled', () => {
    beforeAll(() => {
      jest.spyOn(utils, 'canRequestHaveBody').mockReturnValue(false)
    })

    afterAll(() => {
      utils.canRequestHaveBody.restore()
    })

    it('should proxy the request', () => {
      requestRespondsWith(
        Promise.resolve({
          headers: {
            'set-cookie': cookies,
          },
          text: '{ "success": true }',
        })
      )
      request.post.mockReturnValue(mock)

      return wcs(
        {
          destination,
          query,
          method: 'post',
          payload,
        },
        [],
        'mobile'
      ).then(() => {
        expect(extendWithProxy).toHaveBeenCalled()
        expect(mock.proxy).toHaveBeenCalled()
      })
    })
  })

  it('should not encode a redirected url', () => {
    const location =
      'http://www.topshop.com/api/different?option=5%2BM%3D&ddkey=https%3AOrderCalculate'
    requestRespondsOnceWith(
      Promise.reject({
        status: HTTP_REDIRECT_302,
        response: {
          headers: {
            location,
            'set-cookie': cookies,
          },
        },
      })
    )
    requestRespondsWith(
      Promise.resolve({
        headers: {
          'set-cookie': [],
        },
        text: '{ "success": true }',
      })
    )

    return wcs({
      destination,
      query: { queryOption: 'x' },
      method: 'post',
      payload: { payloadOption: 'y' },
    }).then(() => {
      expect(request.post).toHaveBeenCalledWith(location)
    })
  })

  it('throws a Bad Gateway error if it can not parse the json', async () => {
    requestRespondsWith(
      Promise.resolve({
        headers: {
          'set-cookie': [],
        },
        text: ' "success": true }',
      })
    )

    Boom.badGateway.mockReturnValue('WCS SyntaxError')

    await wcs({
      destination,
      query: { queryOption: 'x' },
      method: 'post',
      payload: { payloadOption: 'y' },
    }).catch((err) => {
      expect(err).toBe('WCS SyntaxError')
      expect(Boom.badGateway).toHaveBeenCalledTimes(1)
    })
  })

  it('throws rethrows the caught error if it is not a SyntaxError', async () => {
    requestRespondsWith(
      Promise.reject({
        status: 500,
      })
    )

    await wcs({
      destination,
      query: { queryOption: 'x' },
      method: 'post',
      payload: { payloadOption: 'y' },
    }).catch((err) => {
      expect(err).toEqual({
        status: 500,
      })
    })
  })
})
