import {
  getJSessionId,
  mergeCookiesResponse,
  getNewDestination,
  canRequestHaveBody,
  mergeCookies,
  addCookie,
  removeCookie,
} from './utils'

const oldCookies = [
  'usergeo=GB; path=/',
  'JSESSIONID=0000fyAMAadGxpebiLzvAL52jiH:live_792.04; Path=/',
]
const newCookies = [
  'Apache=80.239.234.172.1493724001402700; path=/',
  'JSESSIONID=0000lB6WWCq8X98Y2gOwk4vQcCA:live_7c2.01; Path=/',
  'cartId=333163198; Expires=Thu, 01-Jun-17 11:20:01 GMT; Path=/',
]
const mergedCookies = [
  'usergeo=GB; path=/',
  'Apache=80.239.234.172.1493724001402700; path=/',
  'JSESSIONID=0000lB6WWCq8X98Y2gOwk4vQcCA:live_7c2.01; Path=/',
  'cartId=333163198; Expires=Thu, 01-Jun-17 11:20:01 GMT; Path=/',
]

describe('utils', () => {
  describe('getJSessionId', () => {
    it('gets the JSESSIONID', () => {
      const cookies = [
        'Apache=80.239.234.172.1493724001402700; path=/',
        'JSESSIONID=0000lB6WWCq8X98Y2gOwk4vQcCA:live_7c2.01; Path=/',
        'WC_PERSISTENT=vJy27BjJsmeqwFRLum173dXaSho%3D%0A%3B2017-05-02+12%3A20%3A01.406_1493724001406-1352318_0; Expires=Thu, 01-Jun-17 11:20:01 GMT; Path=/',
        'cartId=333163198; Expires=Thu, 01-Jun-17 11:20:01 GMT; Path=/',
        'cartValue=1|15.00; Expires=Thu, 01-Jun-17 11:20:01 GMT; Path=/',
        'WC_SESSION_ESTABLISHED=true; Path=/',
        'WC_PERSISTENT=KqN5We4xPyckSYJKtlzKQ7i581o%3D%0A%3B2017-05-02+12%3A20%3A01.572_1493724001406-1352318_12556_642832601%2C-1%2CGBP%2C1UKxocI%2F9JVdPZJJPxFLtgOG6mkdVjd8rWjSlZNrZzLhZGeI0pe7Wo8xGL3Dlk1QEx1vKSBe6Ni3B%2F5LjoYzDQ%3D%3D_12556; Expires=Thu, 01-Jun-17 11:20:01 GMT; Path=/',
        'WC_ACTIVEPOINTER=-1%2C12556; Path=/',
        'WC_USERACTIVITY_642832601=642832601%2C12556%2Cnull%2Cnull%2C1493724001572%2Cnull%2Cnull%2Cnull%2Cnull%2Cnull%2CFkzuWBHtljOLqSigUb20Nyady4qhNVISLebyLm2PtMgNIzkrsLXitk5SOPZFTW9nyrFVa0GmMaaR7WSI8CykXBMbt8R0xZEBsLaFiy1lYEnB%2BQEBpm9YvTasEM5p1Fdsrznridp29cReCT%2BnZ%2B0CuFp48iVGqeVR7wEKCpUbT74pjhWnkXjeMB7qq2g7Yh1527bdEOQq1o1AFujHS4yGjaAWGm4MTy%2BUyq3A9OWI9eM%3D; Path=/',
        'akavpau_VP_TS=1493724301~id=9864767c500ff5313fea11f3cf0763f0; Path=/',
      ]
      const key = getJSessionId(cookies)
      expect(key).toBe('0000lB6WWCq8X98Y2gOwk4vQcCA')
    })
    it('returns an empty string if no JSESSIONID is found', () => {
      const cookies = []
      const key = getJSessionId(cookies)
      expect(key).toBe('')
    })
  })
  describe('mergeCookiesResponse', () => {
    const cookies = [
      'Apache=80.239.234.172.1493724001402700; path=/',
      'JSESSIONID=0000lB6WWCq8X98Y2gOwk4vQcCA:live_7c2.01; Path=/',
    ]
    it('merges cookies from a response with other cookies', () => {
      const mergedCookies = [
        'Apache=80.239.234.172.1493724001402700; path=/',
        'JSESSIONID=0000lB6WWCq8X98Y2gOwk4vQcCA:live_7c2.01; Path=/',
        'cartId=333163198; Expires=Thu, 01-Jun-17 11:20:01 GMT; Path=/',
      ]
      const response = {
        headers: {
          'set-cookie': [
            'JSESSIONID=0000lB6WWCq8X98Y2gOwk4vQcCA:live_7c2.01; Path=/',
            'cartId=333163198; Expires=Thu, 01-Jun-17 11:20:01 GMT; Path=/',
          ],
        },
      }
      const newCookies = mergeCookiesResponse(response, cookies)
      expect(newCookies).toEqual(mergedCookies)
    })
    it('overwrites old cookies with new cookies', () => {
      const mergedCookies = [
        'Apache=80.239.234.172.1493724001402700; path=/',
        'JSESSIONID=newjsessionid:live_7c2.01; Path=/',
        'cartId=333163198; Expires=Thu, 01-Jun-17 11:20:01 GMT; Path=/',
      ]
      const response = {
        headers: {
          'set-cookie': [
            'JSESSIONID=newjsessionid:live_7c2.01; Path=/',
            'cartId=333163198; Expires=Thu, 01-Jun-17 11:20:01 GMT; Path=/',
          ],
        },
      }
      const newCookies = mergeCookiesResponse(response, cookies)
      expect(newCookies).toEqual(mergedCookies)
    })
    it('return the old cookies if no new cookies are set in the reponse', () => {
      const response = {
        headers: {},
      }
      const newCookies = mergeCookiesResponse(response, cookies)
      expect(newCookies).toEqual(cookies)
    })
  })
  describe('getNewDestination', () => {
    it('returns the new destination from the response', () => {
      const location = 'http://www.topshop.com/api/different?option=5'
      const response = {
        headers: {
          location,
        },
      }
      const destination = getNewDestination(response)
      expect(destination).toBe(location)
    })
    it('return an empty string if no location is found', () => {
      const response = {
        headers: {},
      }
      const destination = getNewDestination(response)
      expect(destination).toBe('')
    })
  })
  describe('canRequestHaveBody', () => {
    it('return true for request methods that can have a body', () => {
      expect(canRequestHaveBody('post')).toBeTruthy()
      expect(canRequestHaveBody('put')).toBeTruthy()
      expect(canRequestHaveBody('patch')).toBeTruthy()
      expect(canRequestHaveBody('delete')).toBeTruthy()
      expect(canRequestHaveBody('options')).toBeTruthy()
      expect(canRequestHaveBody('link')).toBeTruthy()
      expect(canRequestHaveBody('unlink')).toBeTruthy()
      expect(canRequestHaveBody('lock')).toBeTruthy()
      expect(canRequestHaveBody('propfind')).toBeTruthy()
      expect(canRequestHaveBody('view')).toBeTruthy()
    })
    it('return false for request methods that can not have a body', () => {
      expect(canRequestHaveBody('get')).toBeFalsy()
      expect(canRequestHaveBody('copy')).toBeFalsy()
      expect(canRequestHaveBody('head')).toBeFalsy()
      expect(canRequestHaveBody('purge')).toBeFalsy()
      expect(canRequestHaveBody('unlock')).toBeFalsy()
    })
  })
  describe('mergeCookies', () => {
    it('merges two cookies arrays', () => {
      const cookies = mergeCookies(oldCookies, newCookies)
      expect(cookies).toEqual(mergedCookies)
    })
    it('only contains each cookie once', () => {
      const cookies = mergeCookies(oldCookies, newCookies)
      const cookieNames = cookies.map((cookie) =>
        cookie.slice(0, cookie.indexOf('='))
      )
      const unique = cookieNames.filter(
        (cookie) =>
          cookieNames.indexOf(cookie) === cookieNames.lastIndexOf(cookie)
      )
      expect(unique.length).toBe(cookies.length)
    })
    it('uses the newCookies value if there is a duplicate', () => {
      const cookies = mergeCookies(oldCookies, newCookies)
      const jSessionId = cookies.find((item) => item.startsWith('JSESSIONID='))
      expect(jSessionId).toBe(newCookies[1])
    })
  })
  describe('addCookie', () => {
    it('should return the original list, if no name is supplied', () => {
      const cookies = addCookie({}, oldCookies)
      expect(cookies).toEqual(oldCookies)
    })
    it('should insert the cookie at the end of the list', () => {
      const newCookie = 'newCookie=true; Path=/'
      const cookies = addCookie(
        {
          name: 'newCookie',
          value: true,
          path: '/',
        },
        oldCookies
      )
      expect(cookies[cookies.length - 1]).toEqual(newCookie)
    })
  })
  describe('removeCookie', () => {
    it('should return the original list, if no name is supplied', () => {
      const cookies = removeCookie({}, oldCookies)
      expect(cookies).toEqual(oldCookies)
    })
    it('should remove usergeo', () => {
      const userGeoCookie = 'usergeo=GB; path=/'
      const cookies = removeCookie('usergeo', oldCookies)
      expect(cookies).not.toContain(userGeoCookie)
    })
  })
})
