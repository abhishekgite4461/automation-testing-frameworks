import { cookieOptionsUnset } from '../../../server/lib/auth'

const mockExecute = jest.fn(() => Promise.resolve())
const mockCreateMapper = jest.fn(() => ({
  execute: mockExecute,
}))

jest.mock('../mapping/map', () => {
  return [
    {
      re: /^\/api\/site-options$/,
      method: 'get',
      handler: jest.fn(() => {
        return {
          execute: mockExecute,
        }
      }),
    },
    {
      re: /^\/api\/checkout\/order_summary$/,
      method: 'put',
      handler: {
        createMapper: () => mockCreateMapper(),
      },
    },
  ]
})

jest.mock('../api')

import routeHandler from '../handler'
import Boom from 'boom'

describe('CoreAPI handler', () => {
  const mockState = jest.fn()
  const mockHeader = jest.fn()
  const mockCode = jest.fn()
  const reply = jest.fn(() => ({
    state: mockState,
    header: mockHeader,
    code: mockCode,
  }))

  beforeEach(() => {
    jest.clearAllMocks()
  })

  // TODO this doesn't seem useful. Requesting a missing endpoint should respond with 404
  it('no mapped endpoint', async () => {
    await routeHandler(
      {
        url: {
          pathname: '/api/foo',
        },
        method: 'get',
      },
      reply
    )

    expect(reply).toHaveBeenCalledWith(undefined)
    expect(mockCode).toHaveBeenCalledWith(200)
  })

  it('no mapped method for endpoint', async () => {
    await routeHandler(
      {
        url: {
          pathname: '/api/site-options',
        },
        method: 'post',
      },
      reply
    )

    expect(reply).toHaveBeenCalledWith(undefined)
    expect(mockCode).toHaveBeenCalledWith(200)
  })

  it('matches a mapped route', async () => {
    const res = {
      status: 302,
      body: { success: true },
      jsessionid: '123',
      setCookies: [
        {
          name: 'foo',
          value: 'bar',
          options: {
            path: '/',
          },
        },
      ],
      setHeaders: [
        {
          name: 'baz',
          value: 'quux',
        },
      ],
    }
    mockExecute.mockReturnValueOnce(Promise.resolve(res))

    await routeHandler(
      {
        url: {
          pathname: '/api/site-options',
        },
        method: 'get',
        headers: { 'brand-code': 'tsuk' },
      },
      reply
    )

    expect(mockExecute).toHaveBeenCalledTimes(1)
    expect(reply).toHaveBeenCalledWith(res.body)
    expect(mockState).toHaveBeenCalledWith('jsessionid', '123', {
      ttl: 1000 * 60 * 60 * 24 * 5,
      path: '/',
      encoding: 'none',
      isSecure: false,
      isHttpOnly: true,
      clearInvalid: false,
      strictHeader: false,
    })
    expect(mockState).toHaveBeenCalledWith('foo', 'bar', { path: '/' })
    expect(mockHeader).toHaveBeenCalledWith('baz', 'quux')
    expect(mockCode).toHaveBeenCalledWith(302)
  })

  it('matches a route with alternate mappers', async () => {
    await routeHandler(
      {
        url: {
          pathname: '/api/checkout/order_summary',
        },
        method: 'put',
        headers: { 'brand-code': 'tsuk' },
      },
      reply
    )

    expect(mockExecute).toHaveBeenCalledTimes(1)
  })

  it('does not set the jsessionid cookie if one is not provided in the response', async () => {
    const res = {
      body: { success: true },
    }
    mockExecute.mockReturnValueOnce(Promise.resolve(res))

    await routeHandler(
      {
        url: {
          pathname: '/api/site-options',
        },
        method: 'get',
        headers: { 'brand-code': 'tsuk' },
      },
      reply
    )

    expect(reply).toHaveBeenCalledWith(res.body)
    expect(mockState).not.toHaveBeenCalledWith(
      'jsessionid',
      expect.anything(),
      expect.anything()
    )
  })

  it('handles error responses', async () => {
    const err = Boom.notFound()
    mockExecute.mockReturnValueOnce(Promise.reject(err))

    await routeHandler(
      {
        url: {
          pathname: '/api/site-options',
        },
        method: 'get',
        headers: { 'brand-code': 'tsuk' },
      },
      reply
    )

    expect(reply).toHaveBeenCalledWith(err)
    expect(mockState).not.toHaveBeenCalled()
    expect(mockHeader).not.toHaveBeenCalled()
  })

  it('removes jsessionid on session timeout and process.env.CLEAR_SESSION_KEY_ON_TIMEOUT set to true', async () => {
    const clearSessionKeyOnTimeout =
      global.process.env.CLEAR_SESSION_KEY_ON_TIMEOUT

    global.process.env.CLEAR_SESSION_KEY_ON_TIMEOUT = 'true'

    const err = {
      message: 'wcsSessionTimeout',
    }
    mockExecute.mockReturnValueOnce(Promise.reject(err))

    await routeHandler(
      {
        url: {
          pathname: '/api/site-options',
        },
        method: 'get',
        headers: { 'brand-code': 'tsuk' },
      },
      reply
    )

    expect(mockState).toHaveBeenCalledWith(
      'jsessionid',
      null,
      cookieOptionsUnset
    )

    global.process.env.CLEAR_SESSION_KEY_ON_TIMEOUT = clearSessionKeyOnTimeout
  })

  it('does not remove jsessionid on session timeout and process.env.CLEAR_SESSION_KEY_ON_TIMEOUT not set to true', async () => {
    const clearSessionKeyOnTimeout =
      global.process.env.CLEAR_SESSION_KEY_ON_TIMEOUT

    global.process.env.CLEAR_SESSION_KEY_ON_TIMEOUT = 'false'

    const err = {
      message: 'wcsSessionTimeout',
    }
    mockExecute.mockReturnValueOnce(Promise.reject(err))

    await routeHandler(
      {
        url: {
          pathname: '/api/site-options',
        },
        method: 'get',
        headers: { 'brand-code': 'tsuk' },
      },
      reply
    )

    expect(mockState).not.toHaveBeenCalledWith(
      'jsessionid',
      null,
      expect.anything()
    )

    global.process.env.CLEAR_SESSION_KEY_ON_TIMEOUT = clearSessionKeyOnTimeout
  })

  it('does not remove jsessionid if no session timeout', async () => {
    const clearSessionKeyOnTimeout =
      global.process.env.CLEAR_SESSION_KEY_ON_TIMEOUT

    global.process.env.CLEAR_SESSION_KEY_ON_TIMEOUT = 'true'

    const res = {
      body: { success: true },
    }
    mockExecute.mockReturnValueOnce(Promise.resolve(res))

    await routeHandler(
      {
        url: {
          pathname: '/api/site-options',
        },
        method: 'get',
        headers: { 'brand-code': 'tsuk' },
      },
      reply
    )

    expect(mockState).not.toHaveBeenCalledWith(
      'jsessionid',
      null,
      cookieOptionsUnset
    )

    global.process.env.CLEAR_SESSION_KEY_ON_TIMEOUT = clearSessionKeyOnTimeout
  })
})
