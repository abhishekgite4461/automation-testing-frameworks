jest.mock('../../lib/logger')
import * as logger from '../../lib/logger'

jest.mock('@ag-digital/pantry', () => jest.fn())
import pantry from '@ag-digital/pantry'

jest.mock('../../../../test/apiResponses/wcsMockedApi')
import wcsMockedApi from '../../../../test/apiResponses/wcsMockedApi'

jest.mock('../requests/wcs')
import recursivelyFollowRedirectsAndCollectCookies from '../requests/wcs'

jest.mock('uuid/v4')
import uuidv4 from 'uuid/v4'

jest.mock('../../../server/lib/auth')
import { verifyJwtSync, signJwtSync } from '../../../server/lib/auth'

import { sendRequestToApi } from '../api'

const {
  USE_MOCK_API,
  REDIS_HOST_FOR_SESSION_STORE,
  REDIS_PORT_FOR_SESSION_STORE,
  NODE_ENV,
} = global.process.env

const mockPantry = (
  { res = {}, cookieJar = [], key } = {
    res: {},
    cookieJar: [],
    key: undefined,
  }
) => {
  const prepRes = (r) => {
    if (r instanceof Promise) return r
    if (typeof r !== 'object' || !('body' in r)) r = { body: r }
    return Promise.resolve(r)
  }
  const saveSessionMock = jest.fn()
  if (Array.isArray(res)) {
    res.forEach((r) => {
      saveSessionMock.mockReturnValueOnce(prepRes(r))
    })
  } else {
    saveSessionMock.mockReturnValue(prepRes(res))
  }

  const requestMock = jest.fn((fn) => {
    fn(cookieJar)
    return { saveSession: saveSessionMock }
  })
  const retrieveSessionMock = jest.fn(() => ({ request: requestMock }))
  pantry.mockReturnValue({ retrieveSession: retrieveSessionMock })

  global.process.env.USE_MOCK_API = false
  global.process.env.REDIS_HOST_FOR_SESSION_STORE = 'host'
  global.process.env.REDIS_PORT_FOR_SESSION_STORE = 'port'

  return {
    verify: () => {
      expect(retrieveSessionMock).toHaveBeenCalledTimes(1)
      if (key) expect(retrieveSessionMock).toHaveBeenCalledWith(key)
    },
    retrieveSessionMock,
    requestMock,
    saveSessionMock,
  }
}

const mockJWT = (
  { invalid = false, generatedKey } = {
    invalid: false,
    generatedKey: undefined,
  }
) => {
  if (invalid) {
    verifyJwtSync.mockImplementation(() => {
      throw new Error('verification failure')
    })
  }
  uuidv4.mockReturnValue('uuidv4')
  const newJsessionid = 'signedJWTWithUUID'
  signJwtSync.mockReturnValue(generatedKey || newJsessionid)

  return {
    verifyJWT: () => {
      expect(uuidv4).toHaveBeenCalledTimes(1)
      expect(signJwtSync).toHaveBeenCalledTimes(1)
      expect(signJwtSync).toHaveBeenCalledWith('uuidv4')
    },
  }
}

describe('sendRequestToApi', () => {
  beforeEach(() => {
    jest.resetAllMocks()
    recursivelyFollowRedirectsAndCollectCookies.mockImplementation(() =>
      Promise.resolve('wcs response')
    )
  })

  afterEach(() => {
    global.process.env.USE_MOCK_API = USE_MOCK_API
    global.process.env.REDIS_HOST_FOR_SESSION_STORE = REDIS_HOST_FOR_SESSION_STORE
    global.process.env.REDIS_PORT_FOR_SESSION_STORE = REDIS_PORT_FOR_SESSION_STORE
    global.process.env.NODE_ENV = NODE_ENV
  })

  it('returns a rejected promise in case of missing parameter "hostname"', async () => {
    return expect(
      sendRequestToApi('', 'path', 'query', 'payload')
    ).rejects.toThrow('Missing mandatory parameter "hostname"')
  })

  it('returns a rejected promise in case of missing parameter "path"', async () => {
    return expect(
      sendRequestToApi('hostname', '', 'query', 'payload', 'method', {})
    ).rejects.toThrow('Missing mandatory parameter "path"')
  })

  it('returns a rejected promise in case of "query" is non-object', () => {
    return expect(
      sendRequestToApi('hostname', 'path', 'query', 'payload', 'method', {
        'brand-code': 'tsuk',
      })
    ).rejects.toThrow('"query" and "payload" should be objects')
  })

  it('returns a rejected promise in case of missing parameter "payload"', async () => {
    return expect(
      sendRequestToApi('hostname', 'path', {}, 'payload', 'method', {
        'brand-code': 'tsuk',
      })
    ).rejects.toThrow('"query" and "payload" should be objects')
  })

  it('handles correctly the scenario where global.process.env.USE_MOCK_API is set to true', async () => {
    global.process.env.USE_MOCK_API = 'true'
    expect(wcsMockedApi).not.toHaveBeenCalled()

    await sendRequestToApi(
      'hostname',
      'path',
      null,
      null,
      'get',
      {
        'brand-code': 'tsuk',
      },
      null
    )

    expect(wcsMockedApi).toHaveBeenCalledTimes(1)
    expect(wcsMockedApi).toHaveBeenCalledWith('path', 'get')
  })

  it('resolves the promise as expected', async () => {
    const response = 'res'
    const jsessionid = '123'
    const { verify } = mockPantry({ res: response, key: jsessionid })

    const res = await sendRequestToApi(
      'hostname',
      'path',
      { query: 'query' },
      { payload: 'payload' },
      'get',
      {
        'brand-code': 'tsuk',
        cookie: `jsessionid=${jsessionid}`,
      }
    )

    expect(res).toEqual(expect.objectContaining({ body: response }))
    verify()
  })

  it('generates a new session key if client session key does not satisfy jwttoken verification', async () => {
    const newJsessionid = 'signedJWTWithUUID'
    const { verifyJWT } = mockJWT({
      invalid: true,
      generatedKey: newJsessionid,
    })
    const { verify } = mockPantry({ key: newJsessionid })

    await sendRequestToApi(
      'hostname',
      'path',
      { query: 'query' },
      { payload: 'payload' },
      'get',
      {
        'brand-code': 'tsuk',
        cookie: 'jsessionid=badId',
      }
    )

    verifyJWT()
    verify()
  })

  it('generates a new session key if the Client does not provide one', async () => {
    const newJsessionid = 'signedJWTWithUUID'
    const { verifyJWT } = mockJWT({ generatedKey: newJsessionid })
    const { verify } = mockPantry({ key: newJsessionid })

    await sendRequestToApi('hostname', 'path', {}, {}, 'get', {
      'brand-code': 'tsuk',
      cookie: '',
    })

    verifyJWT()
    verify()
  })

  it('throws if generated key is already used as key in Redis', () => {
    const newJsessionid = 'signedJWTWithUUID'
    const { verifyJWT } = mockJWT({ generatedKey: newJsessionid })
    mockPantry({ cookieJar: ['JSESSIONID=123'], key: newJsessionid })

    expect(() =>
      sendRequestToApi(
        'hostname',
        'path',
        { query: 'query' },
        { payload: 'payload' },
        'get',
        {
          'brand-code': 'tsuk',
        }
      )
    ).toThrow('Error while retrieving User Session')
    verifyJWT()
  })

  it('logs error when sessions clash', async () => {
    mockPantry({
      res: {
        body: 'body',
        cookies: ['WC_USERACTIVITY_456=123', 'WC_AUTHENTICATION_456=456'],
      },
      cookieJar: ['WC_USERACTIVITY_123=123', 'WC_AUTHENTICATION_123=123'],
    })

    await sendRequestToApi('hostname', 'path', {}, {}, 'get', {
      'brand-code': 'tsuk',
      cookie: 'jsessionid=123',
    })

    expect(logger.error).toHaveBeenCalledWith(
      'wcs:err',
      expect.objectContaining({
        loggerMessage: 'Invalid transition of WC_USERACTIVITY_ID',
      })
    )
  })

  it('should use URLs with encoded special characters on first request', async () => {
    mockPantry()

    await sendRequestToApi(
      'hostname',
      '/api/fr/tsfr/catégorie/vêtements-415222/jupes-415244',
      {},
      {},
      'get',
      { 'brand-code': 'tsuk' }
    )

    expect(recursivelyFollowRedirectsAndCollectCookies).toHaveBeenCalledWith(
      expect.objectContaining({
        destination:
          'hostname/api/fr/tsfr/cat%C3%A9gorie/v%C3%AAtements-415222/jupes-415244',
      }),
      expect.anything(),
      undefined,
      undefined,
      undefined,
      undefined
    )
  })

  it('reject if cannot form valid destiation url', async () => {
    mockPantry()

    return expect(
      sendRequestToApi('hostname', '/%E0/foo', {}, {}, 'get', {
        'brand-code': 'tsuk',
      })
    ).rejects.toThrow(/URI malformed/)
  })

  it('adds request and response payloads to logs when trace logging is enabled', async () => {
    const requestPayload = { myRequestPayload: true }
    const responsePayload = { myResponsePayload: true }
    mockPantry({
      res: responsePayload,
    })
    logger.isTraceLoggingEnabled.mockReturnValue(true)

    await sendRequestToApi(
      'hostname',
      '/api/fr/tsfr/catégorie/vêtements-415222/jupes-415244',
      {},
      requestPayload,
      'get',
      { 'brand-code': 'tsuk' }
    )

    expect(logger.info).toHaveBeenCalledWith(
      'wcs:api',
      expect.objectContaining({
        payload: requestPayload,
      })
    )
    expect(logger.info).toHaveBeenCalledWith(
      'wcs:api',
      expect.objectContaining({
        body: responsePayload,
      })
    )
  })

  it('defaults response body in logs to empty object if body missing', async () => {
    mockPantry({
      res: false,
    })
    logger.isTraceLoggingEnabled.mockReturnValue(true)

    await sendRequestToApi(
      'hostname',
      '/api/fr/tsfr/catégorie/vêtements-415222/jupes-415244',
      {},
      {},
      'get',
      { 'brand-code': 'tsuk' }
    )

    expect(logger.info).toHaveBeenCalledWith(
      'wcs:api',
      expect.objectContaining({
        body: {},
      })
    )
  })

  it('reject if cannot form valid destiation url', async () => {
    mockPantry()

    return expect(
      sendRequestToApi('hostname', '/%E0/foo', {}, {}, 'get', {
        'brand-code': 'tsuk',
      })
    ).rejects.toThrow(/URI malformed/)
  })

  it('adds request and response payloads to logs when trace logging is enabled', async () => {
    const requestPayload = { myRequestPayload: true }
    const responsePayload = { myResponsePayload: true }
    mockPantry({
      res: responsePayload,
    })
    logger.isTraceLoggingEnabled.mockReturnValue(true)

    await sendRequestToApi(
      'hostname',
      '/api/fr/tsfr/catégorie/vêtements-415222/jupes-415244',
      {},
      requestPayload,
      'get',
      { 'brand-code': 'tsuk' }
    )

    expect(logger.info).toHaveBeenCalledWith(
      'wcs:api',
      expect.objectContaining({
        payload: requestPayload,
      })
    )
    expect(logger.info).toHaveBeenCalledWith(
      'wcs:api',
      expect.objectContaining({
        body: responsePayload,
      })
    )
  })

  it('defaults response body in logs to empty object if body missing', async () => {
    mockPantry({
      res: false,
    })
    logger.isTraceLoggingEnabled.mockReturnValue(true)

    await sendRequestToApi('hostname', '/api/path', {}, {}, 'get', {
      'brand-code': 'tsuk',
    })

    expect(logger.info).toHaveBeenCalledWith(
      'wcs:api',
      expect.objectContaining({
        body: {},
      })
    )
  })

  it('calls recursivelyFollowRedirectsAndCollectCookies with the device type received in the headers argument', async () => {
    mockPantry()

    await sendRequestToApi('hostname', '/whatever', {}, {}, 'get', {
      'monty-client-device-type': 'desktop',
      'brand-code': 'tsuk',
    })

    expect(recursivelyFollowRedirectsAndCollectCookies).toHaveBeenCalledWith(
      expect.objectContaining({
        destination: 'hostname/whatever',
      }),
      expect.anything(),
      'desktop',
      undefined,
      undefined,
      undefined
    )
  })

  it('calls recursivelyFollowRedirectsAndCollectCookies with the fifth argument valued as the header monty-mobile-hostname', async () => {
    mockPantry()

    await sendRequestToApi('hostname', '/whatever', {}, {}, 'get', {
      'brand-code': 'tsuk',
      'monty-client-device-type': 'desktop',
      'monty-mobile-hostname': 'true',
    })

    expect(recursivelyFollowRedirectsAndCollectCookies).toHaveBeenCalledWith(
      expect.objectContaining({
        destination: 'hostname/whatever',
      }),
      expect.anything(),
      'desktop',
      'true',
      undefined,
      undefined
    )
  })
})
