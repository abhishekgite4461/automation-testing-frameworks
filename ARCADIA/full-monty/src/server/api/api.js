/*
  This module responabiity is to send the request to the API.
  This module does not have any knowledge about our hapi Server and about the mapping activities needed for the request
  and the response.
  All the details of the destination API are here (destination url, redirection following logic and cookies collection, call
  the session manager to store the cookies).
  This is the only place where with knowledge anout JSESSIONID since it is WCS specific.
*/

import wcsMockedApi from '../../../test/apiResponses/wcsMockedApi'
import pantry from '@ag-digital/pantry'
import recursivelyFollowRedirectsAndCollectCookies from './requests/wcs'
import { extractCookieValue, sessionsClash } from './utils'
import { addCookie } from './requests/utils'
import * as logger from '../lib/logger'
import uuidv4 from 'uuid/v4'
import { verifyJwtSync, signJwtSync } from '../../server/lib/auth'
import Boom from 'boom'
import countryList from 'country-list'
import { getConfigByStoreCode, getFirstPreferredISO } from '../config'

const countries = countryList()

const bodyAndJsessionIdAndCookies = (res) => {
  return {
    body: res.body,
    jsessionid: res.key,
    cookies: res.cookies,
    status: res.status,
  }
}

const rejectionBecauseOf = (error) => {
  logger.error('wcs:api', error)
  return Promise.reject(error)
}

export const generateNewSessionKey = () => {
  return signJwtSync(uuidv4())
}

const encodeUserCountry = (countryISO) =>
  encodeURI(countries.getName(countryISO, 'en'))

/**
 * Returns the ISO 2 country code of the requester
 * @param  {Object} headers
 * @return {String}
 */
const getCountryISO = (headers) =>
  getFirstPreferredISO(getConfigByStoreCode(headers['brand-code']))

export function sendRequestToApi(
  hostname,
  path,
  query,
  payload,
  method = 'get',
  headers = {},
  sessionKey,
  noPath = false,
  retryAfterSessionTimeout = false,
  timeout
) {
  if (!hostname)
    return rejectionBecauseOf('Missing mandatory parameter "hostname"')
  if (!path && !noPath)
    return rejectionBecauseOf('Missing mandatory parameter "path"')
  if (!headers['brand-code'])
    return rejectionBecauseOf('Missing mandatory header "brand-code"')
  if (typeof query !== 'object' || typeof payload !== 'object')
    return rejectionBecauseOf('"query" and "payload" should be objects')

  if (process.env.USE_MOCK_API === 'true') {
    return Promise.resolve(wcsMockedApi(path, method))
  }

  //
  // The decoding/encoding is necessary in order to handle both requests, coming from the Client and from the Server (server side rendering).
  // e.g.:
  // PDP request from the Client:
  //      path = /fr/msfr/produit/robes-3457985/afficher-tout-854522/robe-corolle-vert-chartreuse-à-liseré-et-encolure-années-90-6653256
  // Refreshing the PDP above (server side rendering of PDP)
  //      path = /fr/msfr/produit/robes-3457985/afficher-tout-854522/robe-corolle-vert-chartreuse-%C3%A0-liser%C3%A9-et-encolure-ann%C3%A9es-90-6653256
  //
  // In both cases we need to produce the destination as:
  //      https://msfr.stage.arcadiagroup.ltd.uk/fr/msfr/produit/robes-3457985/afficher-tout-854522/robe-corolle-vert-chartreuse-%C3%A0-liser%C3%A9-et-encolure-ann%C3%A9es-90-6653256
  //
  let destination

  try {
    destination = `${hostname}${encodeURI(decodeURI(path))}`
  } catch (err) {
    return rejectionBecauseOf(err)
  }

  const clientSessionKey =
    sessionKey || extractCookieValue('jsessionid', headers.cookie)

  // This is the session key that we will pass back to the Client as a cookie and its value will be used as Redis key
  // to collect the collection of WCS cookies represeting the User Session on WCS.
  let key
  let isNewUser
  let isUserWithInvalidSessionKey

  if (clientSessionKey) {
    // Using the client session key to retrieve WCS cookies from Redis
    key = clientSessionKey

    // Verifying that the session key provided by the client has been signed by us.
    try {
      verifyJwtSync(clientSessionKey)
    } catch (err) {
      // e.g.: User that used coreApi before when we were passing back to the Client as session key the WCS jsessionid
      isUserWithInvalidSessionKey = true
      key = generateNewSessionKey()
    }
  } else {
    // If the User doesn't have a session key then we generate a new one.

    isNewUser = true
    key = generateNewSessionKey()
  }

  const deviceType = headers['monty-client-device-type']
  const isMobileHostname = headers['monty-mobile-hostname']

  const cookieJarSettings = {
    host: process.env.REDIS_HOST_FOR_SESSION_STORE,
    port: process.env.REDIS_PORT_FOR_SESSION_STORE,
  }
  const transactionId = logger.generateTransactionId()
  let redisCookies

  return pantry(cookieJarSettings)
    .retrieveSession(key)
    .request((cookies) => {
      const countryISO = headers['x-user-geo'] || getCountryISO(headers)
      const combinedCookies = addCookie(
        {
          name: 'userCountry',
          value: encodeUserCountry(countryISO),
          path: '/',
        },
        cookies
      )

      const requestObject = {
        loggerMessage: 'request',
        transactionId,
        method: method.toUpperCase(),
        url: destination,
        sessionKey: key,
        combinedCookies,
        query,
        deviceType,
        isMobileHostname,
      }
      if (logger.isTraceLoggingEnabled()) {
        requestObject.payload = payload
      }
      logger.info('wcs:api', requestObject)

      redisCookies = cookies
      if ((isNewUser || isUserWithInvalidSessionKey) && cookies.length) {
        // We generate a brand new session key if the User is a new one or one with invalid jsessionid (e.g.: User of the coreApi which was using the WCS jsessionid as session key).
        // In both cases there shouldn't be any Redis entry and hence we throw here.

        logger.error('wcs:err', 'generated key already used in Redis')

        throw Boom.badImplementation('Error while retrieving User Session')
      }

      return recursivelyFollowRedirectsAndCollectCookies(
        { destination, method, query, payload },
        combinedCookies,
        deviceType,
        isMobileHostname,
        key, // [MJI-1123] adding this just for logging to Google Cloud Big Query purposes (Dual Run analysis)
        timeout
      ).then((wcsResponse) => {
        return { ...wcsResponse, key }
      })
    })
    .saveSession()
    .then(bodyAndJsessionIdAndCookies)
    .then((res) => {
      const responseObject = {
        loggerMessage: 'response',
        transactionId,
        method: method.toUpperCase(),
        url: destination,
        sessionKey: key,
        cookies: res.cookies,
        deviceType,
        isMobileHostname,
      }

      if (logger.isTraceLoggingEnabled()) {
        responseObject.body = res.body || {}
      }

      logger.info('wcs:api', responseObject)
      const wcsCookies = res.cookies || []

      if (res && res.body && res.body.timeout === true) {
        logger.info('wcs:sessiontimeout', {
          method,
          destination,
          sessionKey: key,
          body: res.body,
          wcsCookies,
          redisCookies,
          retryAfterSessionTimeout,
          hostname,
          path,
          query,
          status: res.status,
          requestHeaders: headers,
        })

        throw new Error('wcsSessionTimeout')
      }

      if (sessionsClash(redisCookies, wcsCookies, path)) {
        logger.error('wcs:err', {
          loggerMessage: 'Invalid transition of WC_USERACTIVITY_ID',
          wcsCookies,
          redisCookies,
        })
      }

      return res
    })
    .catch((err) => {
      if (retryAfterSessionTimeout) {
        // We made a first call to WCS which replied with a session timeout response, we made a second request
        // to WCS which then replied with an error. We do not throw but we return an empty response so that the associated
        // mapper will return to the Client an default response (e.g.: empty shopping bag if we were trying to update size and quantity with session timed out)
        logger.info('wcs:sessiontimeout', {
          success: false,
          retryAfterSessionTimeout,
          method,
          hostname,
          path,
          query,
          endpoint: path + query,
          destination,
          status: err.status,
          redisCookies,
          wcsCookies: err.response.cookies,
          requestHeaders: headers,
        })

        logger.error('wcs:api', {
          loggerMessage: 'wcsSessionTimeout',
          retryAfterSessionTimeout: true,
          ...err,
          method,
          destination,
          sessionKey: key,
          hostname,
        })

        return {
          body: { wcsSessionTimeout: true },
          sessionTimeout: true,
        }
      }

      logger.error('wcs:api', {
        err,
        method,
        destination,
        sessionKey: key,
      })

      throw err
    })
}
