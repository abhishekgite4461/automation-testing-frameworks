import { clone } from 'ramda'
import orderTransform, {
  billingAddressFragment,
  deliveryAddressFragment,
  orderLineFragment,
  orderLinesFragment,
  paymentDetailsFragment,
} from '../order'
import wcs from '../../../../../../test/apiResponses/create-order/wcs.json'
import monty from '../../../../../../test/apiResponses/create-order/hapi.json'

describe('order transformer', () => {
  describe('billingAddressFragment', () => {
    it('returns an address object setting properties to empty strings by default', () => {
      expect(billingAddressFragment()).toEqual({
        name: '',
        address1: '',
        address2: '',
        address3: '',
        country: '',
      })
    })
    describe('WCS provides address2 response parameter', () => {
      it('returns an address object from the billingDetails object', () => {
        expect(
          billingAddressFragment(wcs.OrderConfirmation.billingDetails)
        ).toEqual(monty.completedOrder.billingAddress)
      })
    })
    describe('WCS does not provide address2 response parameter', () => {
      it('returns the expected billing address', () => {
        const wcs = {
          title: 'Mr',
          lastName: 'AUTHORISED',
          firstName: '3D.',
          city: 'LONDON',
          country: 'United Kingdom',
          address1: 'Flat 6, 22 Crescent Road',
          address2: '',
          zipCode: 'N8 8AX',
        }

        const hapi = {
          name: 'Mr 3D. AUTHORISED',
          address1: 'Flat 6, 22 Crescent Road',
          address2: 'LONDON',
          address3: 'N8 8AX',
          country: 'United Kingdom',
        }

        expect(billingAddressFragment(wcs)).toEqual(hapi)
      })
    })
  })
  describe('deliveryAddressFragment', () => {
    it('returns an address object setting properties to empty strings by default', () => {
      expect(deliveryAddressFragment()).toEqual({
        name: '',
        address1: '',
        address2: '',
        address3: '',
        country: '',
      })
    })
    it('returns an address object from the deliveryDetails object', () => {
      expect(
        deliveryAddressFragment(wcs.OrderConfirmation.deliveryDetails)
      ).toEqual(monty.completedOrder.deliveryAddress)
    })
  })
  describe('orderLineFragment', () => {
    const defaultResponse = {
      lineNo: '',
      name: '',
      size: '',
      colour: '',
      imageUrl: '',
      quantity: 0,
      unitPrice: '',
      discount: '',
      discountPrice: '0.00',
      total: '0.00',
      nonRefundable: false,
    }
    it('returns an empty object setting properties to defaults', () => {
      expect(orderLineFragment()).toEqual(defaultResponse)
    })
    it('returns an orderLine object from the Product object', () => {
      expect(orderLineFragment(wcs.ShowMyOrderForm.Products[0])).toEqual(
        monty.completedOrder.orderLines[0]
      )
    })
    it('should forward the baseImageUrl property if provided by wcs', () => {
      const baseImageUrl =
        'https://images.topshop.com/i/TopShop/TS32G13PMUL_F_1'
      const wcsWithBaseImageUrl = clone(wcs)
      wcsWithBaseImageUrl.ShowMyOrderForm.Products[0].baseImageUrl = baseImageUrl
      expect(
        orderLineFragment(wcsWithBaseImageUrl.ShowMyOrderForm.Products[0])
      ).toEqual({
        ...monty.completedOrder.orderLines[0],
        baseImageUrl,
      })
    })
    it('should not add a baseImageUrl property if not provided by wcs', () => {
      const wcsWithoutBaseImageUrl = clone(wcs)
      wcsWithoutBaseImageUrl.ShowMyOrderForm.Products[0].baseImageUrl = undefined
      expect(
        orderLineFragment(wcsWithoutBaseImageUrl.ShowMyOrderForm.Products[0])
      ).not.toHaveProperty('baseImageUrl')
    })
    it('should map `isDDPProduct` flag', () => {
      expect(orderLineFragment({ isDDPProduct: false }, true)).toEqual({
        ...defaultResponse,
        isDDPProduct: false,
      })
    })
    describe('product promotions', () => {
      const productWcs = {
        catCode: '',
        name: '',
        size: '',
        productColor: '',
        productImgURL: '',
        quantity: '0',
        totalPrice: 0,
        unitPrice: '',
      }

      const productMonty = {
        lineNo: '',
        name: '',
        size: '',
        colour: '',
        imageUrl: '',
        quantity: 0,
        unitPrice: '',
        total: '0.00',
        nonRefundable: false,
      }
      it('maps correctly empty promotions', () => {
        expect(
          orderLineFragment({
            ...productWcs,
            promotions: [],
          })
        ).toEqual({
          ...productMonty,
          discount: '',
          discountPrice: '0.00',
        })
      })
      it('maps correctly multiple promotions', () => {
        expect(
          orderLineFragment({
            ...productWcs,
            promotions: [
              { discount: 'discount name', totalDiscountedAmt: '-0.60000' },
              { discount: 'whatever', totalDiscountedAmt: '-1.40000' },
            ],
          })
        ).toEqual({
          ...productMonty,
          discount: 'discount name',
          discountPrice: '2.00',
        })
      })
      it('maps correctly invalid "promotions"', () => {
        expect(
          orderLineFragment({
            ...productWcs,
            promotions: 'abc',
          })
        ).toEqual({
          ...productMonty,
          discount: '',
          discountPrice: '0.00',
        })
      })
      it('maps correctly invalid promotion in "promotions"', () => {
        expect(
          orderLineFragment({
            ...productWcs,
            promotions: 'abc',
          })
        ).toEqual({
          ...productMonty,
          discount: '',
          discountPrice: '0.00',
        })

        expect(
          orderLineFragment({
            ...productWcs,
            promotions: [
              { discount: 'discount name', totalDiscountedAmt: 'b' },
              { discount: 'whatever', totalDiscountedAmt: 'a' },
            ],
          })
        ).toEqual({
          ...productMonty,
          discount: 'discount name',
          discountPrice: '0.00',
        })
      })
    })
  })
  describe('orderLinesFragment', () => {
    it('returns an empty array by default', () => {
      expect(orderLinesFragment()).toEqual([])
    })
    it('returns an orderLines array from the Products array', () => {
      expect(orderLinesFragment(wcs.ShowMyOrderForm.Products)).toEqual(
        monty.completedOrder.orderLines
      )
    })
  })
  describe('paymentDetailsFragment', () => {
    it('returns an array with a default object in by default', () => {
      expect(paymentDetailsFragment()).toEqual([
        {
          paymentMethod: '',
          cardNumberStar: '',
          totalCost: '0.00',
        },
      ])
    })
    it('returns an paymentDetails array from the creditCard object', () => {
      expect(
        paymentDetailsFragment(wcs.OrderConfirmation.creditCard, '£')
      ).toEqual(monty.completedOrder.paymentDetails)
    })
  })
  describe('orderTransform', () => {
    it('returns an address object setting properties to empty strings by default', () => {
      expect(orderTransform()).toEqual({
        completedOrder: {
          orderId: '',
          subTotal: '0.00',
          returnPossible: false,
          returnRequested: false,
          deliveryMethod: '',
          deliveryDate: '',
          deliveryCost: '0.00',
          deliveryCarrier: '',
          deliveryPrice: '0.00',
          totalOrderPrice: '',
          totalOrdersDiscountLabel: '',
          totalOrdersDiscount: '',
          billingAddress: {
            name: '',
            address1: '',
            address2: '',
            address3: '',
            country: '',
          },
          deliveryAddress: {
            name: '',
            address1: '',
            address2: '',
            address3: '',
            country: '',
          },
          orderLines: [],
          paymentDetails: [
            {
              paymentMethod: '',
              cardNumberStar: '',
              totalCost: '0.00',
            },
          ],
          currencyConversion: { currencyRate: '' },
          returning_buyer: false,
        },
      })
    })
    it('returns an order object from the order response', () => {
      expect(orderTransform(wcs, '£', 'GBP')).toEqual(monty)
    })
  })
})
