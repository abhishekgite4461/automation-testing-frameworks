import { path, isEmpty, assocPath } from 'ramda'

const orderEspotMap = {
  espot1: 'eMarketingEspot1URL',
  espot2EmailSignup: 'eMarketingEspot2URL',
  espot3SocialNetworking: 'eMarketingEspot3URL',
  espot4Peerius: 'eMarketingEspot4URL',
  espot5: 'eMarketingEspot5URL',
  espot6: 'eMarketingEspot6URL',
  espot7: 'eMarketingEspot7URL',
  espot8PaymentMethod: 'eMarketingKlarnaSpotURL',
}

const billingAddressFragment = ({
  title,
  firstName = '',
  lastName = '',
  address1 = '',
  address2 = '',
  city = '',
  zipCode = '',
  country = '',
  // Store Standard/Express
  PhyStoreBrandName = '',
  PhyStlocAddress1 = '',
  postcode = '',
} = {}) => {
  const commonRes = {
    address1,
    name: [title, firstName, lastName].filter((value) => value).join(' '),
    country,
  }
  return PhyStoreBrandName
    ? {
        ...commonRes,
        address1: `${PhyStoreBrandName} - ${PhyStlocAddress1}`,
        address2: address1,
        address3: city,
        address4: postcode,
      }
    : address2
      ? {
          ...commonRes,
          address2,
          address3: city,
          address4: zipCode,
        }
      : {
          ...commonRes,
          address2: city,
          address3: zipCode,
        }
}

const deliveryAddressFragment = ({
  nameAndPhone = {},
  AddressDetails = {}, // Home Standard/Express
  address = {}, // Store Standard/Express
} = {}) => {
  const addressInfo = Object.keys(AddressDetails).length
    ? AddressDetails
    : address

  return billingAddressFragment({ ...nameAndPhone, ...addressInfo })
}

const sumUpAllPromotions = (promotions) => {
  if (!Array.isArray(promotions)) return 0

  return promotions.reduce((pre, cur) => {
    return cur.totalDiscountedAmt && !isNaN(cur.totalDiscountedAmt)
      ? pre + parseFloat(cur.totalDiscountedAmt)
      : pre
  }, 0)
}

const orderLineFragment = ({
  catCode = '',
  name = '',
  size = '',
  productColor = '',
  productImgURL = '',
  quantity = '0',
  totalPrice = 0,
  promotions = [], // e.g.: "promotions": [{"totalDiscountedAmt": "-0.60000","discount": ""},{"totalDiscountedAmt": "-1.40000","discount": ""}]
  unitPrice = '',
  baseImageUrl,
  isDDPProduct,
} = {}) => {
  const quantityNumber = parseInt(quantity, 10)
  const parseUnitPrice = parseFloat(unitPrice)

  return {
    lineNo: catCode,
    name,
    size,
    colour: productColor,
    imageUrl: productImgURL.replace('_small', '_thumb'),
    quantity: quantityNumber,
    unitPrice: parseUnitPrice ? parseUnitPrice.toFixed(2) : '',
    discount: path([0, 'discount'], promotions) || '', // [Cogz] ShowMyOrderForm.Products[].promotions[].discount (scrAPI picks first title and provides back in response)
    discountPrice: (sumUpAllPromotions(promotions) * -1).toFixed(2), // [Cogz] ShowMyOrderForm.Products[].promotions[].totalDiscountedAmt  (sum up the totalDiscountedAmt given in promotions array)
    total: totalPrice.toFixed(2),
    nonRefundable: false, // [Cogz] ScrAPI uses different endpoint to get this one. Do we need this?
    ...(baseImageUrl && { baseImageUrl }),
    isDDPProduct,
  }
}

const orderLinesFragment = (products = []) => products.map(orderLineFragment)

const paymentDetailsFragment = (
  { CARD_BRAND_TEXT = '', grandTotalAmount = 0, cardNumberStar = '' } = {},
  currencySymbol = '',
  giftCards = []
) => {
  let paymentDetails = [
    {
      paymentMethod: CARD_BRAND_TEXT,
      cardNumberStar: cardNumberStar.replace(/\s+/g, '').replace(/X/g, '*'),
      totalCost: `${currencySymbol}${grandTotalAmount.toFixed(2)}`,
    },
  ]

  if (Array.isArray(giftCards)) {
    paymentDetails = paymentDetails.concat(
      giftCards.map(({ giftCardLabel, giftCardNumber, balance }) => {
        return {
          paymentMethod: giftCardLabel,
          cardNumberStar: giftCardNumber,
          totalCost: balance,
        }
      })
    )
  }

  return paymentDetails
}

const orderTransform = (
  {
    OrderConfirmation = {},
    ShowMyOrderForm = {},
    totalBeforeDiscount = 0,
    deliveryOptionPrice = 0,
    nominatedDeliveryDate = '',
    qubitConfirmationURL = '',
    discounts,
    currencyCode,
    deliveryDetails, // present for Store delivery,
    espots = {},
  } = {},
  currencySymbol = '',
  currencyCodeFromStoreConfig = ''
) => {
  const { orderItems = [{}] } = OrderConfirmation || {}

  const grandTotalAmount = path(
    ['creditCard', 'grandTotalAmount'],
    OrderConfirmation
  )

  let completedOrder = {
    orderId: OrderConfirmation.orderId || '',
    subTotal: totalBeforeDiscount.toFixed(2),
    returnPossible: false, // scrApi does multiple calls for this one, is this needed by the Client?
    returnRequested: false, // scrApi does multiple calls for this one, is this needed by the Client?
    deliveryMethod: path([0, 'selectedDeliveryMethod'], orderItems) || '',
    deliveryDate: nominatedDeliveryDate,
    deliveryCost: deliveryOptionPrice.toFixed(2),
    deliveryCarrier: orderItems[0].deliveryCarrier || '',
    deliveryPrice: deliveryOptionPrice.toFixed(2),
    totalOrderPrice:
      typeof grandTotalAmount !== 'string' &&
      !isNaN(grandTotalAmount) &&
      grandTotalAmount !== null
        ? grandTotalAmount.toFixed(2)
        : '',
    totalOrdersDiscountLabel: path(['Discount', 0, 'label'], discounts) || '',
    totalOrdersDiscount: path(['Discount', 0, 'value'], discounts) || '',
    billingAddress: billingAddressFragment(OrderConfirmation.billingDetails),
    deliveryAddress: deliveryAddressFragment(
      OrderConfirmation.deliveryDetails || deliveryDetails
    ),
    orderLines: orderLinesFragment(ShowMyOrderForm.Products),
    paymentDetails: paymentDetailsFragment(
      OrderConfirmation.creditCard,
      currencySymbol,
      OrderConfirmation.giftCards
    ),
    currencyConversion: {
      currencyRate: currencyCode || currencyCodeFromStoreConfig,
    },
    // eslint-disable-next-line no-bitwise
    returning_buyer: !!~qubitConfirmationURL.indexOf(
      'returning_buyer%22%3A+true'
    ),
  }

  Object.keys(espots).forEach((espot) => {
    if (!isEmpty(espots[espot])) {
      completedOrder = assocPath(
        ['espots', orderEspotMap[espot] || espot],
        espots[espot],
        completedOrder
      )
    }
  })

  return { completedOrder }
}

export {
  billingAddressFragment,
  deliveryAddressFragment,
  orderLineFragment,
  orderLinesFragment,
  paymentDetailsFragment,
}

export default orderTransform
