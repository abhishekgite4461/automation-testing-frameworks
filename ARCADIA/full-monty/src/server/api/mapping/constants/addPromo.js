export const addPromoConstants = {
  actionType: 'verify_promo',
  calculationUsageId: [-1, -2, -7],
  calculateOrder: 1,
  sourcePage: 'OrderItemDisplay',
  URL: 'OrderCalculate?URL=OrderPrepare?URL=PromotionCodeAjaxView',
  errorViewName: 'PromotionCodeAjaxView',
  taskType: 'A',
}
