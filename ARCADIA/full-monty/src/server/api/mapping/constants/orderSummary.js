export const orderSummaryConstants = {
  page: '',
  URL:
    'OrderCalculate?updatePrices=1%26orderItemId*=%26quantity*=%26URL=BillingAddressView',
  returnPage: 'ShoppingBag',
  outOrderItemName: '',
  Proceed: '',
  CheckoutURL:
    'OrderCopy?URL=OrderPrepare%3fURL%3dOrderDisplay%26errorViewName=InvalidInputErrorView',
  PromoURL:
    'PromotionCodeManage?URL=OrderCalculate%3fURL%3dOrderPrepare%3fURL%3dOrderItemDisplay%26taskType=A%26errorViewName=OrderItemDisplayViewShiptoAssoc',
  shoppingBasketURL:
    'OrderCalculate?langId=-1%26storeId=12556%26catalogId=33057%26updatePrices=1%26calculationUsageId=-1%26URL=OrderItemDisplay',
  showCheckout: false,
}

export const guestOrderSummaryConstants = {
  new: 'Y',
  returnPage: 'ShoppingBag',
}

export const deliveryMethodsMonty = {
  homeStandard: 'HOME_STANDARD',
  homeExpress: 'HOME_EXPRESS',
  storeExpress: 'STORE_EXPRESS',
  storeStandard: 'STORE_STANDARD',
  storeImmediate: 'STORE_IMMEDIATE',
  parcelshopCollection: 'PARCELSHOP_COLLECTION',
}

export const deliveryMethods = {
  Standard: deliveryMethodsMonty.homeStandard,
  S: deliveryMethodsMonty.homeStandard,
  E: deliveryMethodsMonty.homeExpress,
  'Retail Store Express': deliveryMethodsMonty.storeExpress,
  'Retail Store Standard': deliveryMethodsMonty.storeStandard,
  'Retail Store Immediate': deliveryMethodsMonty.storeImmediate,
  'Retail Store Collection': deliveryMethodsMonty.parcelshopCollection,
}

export const deliveryLocationDescriptions = {
  HOME: ['homeDeliveryText', 'homeDeliveryContent'],
  STORE: ['storeDeliveryText', 'collectFromStoreContent'],
  PARCELSHOP: ['hermesDeliveryText', 'hermesContent'],
}

export const addDeliveryAddressConstants = {
  proceed: '',
  registerType: 'R',
  returnPage: 'ShoppingBag',
  isoCode: '',
  page: 'account',
  editRegistration: 'Y',
  editSection: '',
  outOrderItemName: '',
  actionType: 'updateCountryAndOrderItems',
  sourcePage: '',
  deliveryOptionType: 'H',
  URL: 'ProcessDeliveryDetails',
  errorViewName: 'AddressUpdateAjaxView',
  preventAddressOverride: 'Y',
  shipping_errorViewName: 'UserRegistrationForm',
  lookupHouseNumber: '',
  lookupPostcode: '',
  addressResults: '',
  shipping_state_hidden: '',
  shipping_state_select_canada: '',
  shipping_state_select: '',
  preferredLanguage: '',
  preferredCurrency: '',
}
