import pantry from '@ag-digital/pantry'
import { extractCookieValue, mapCookies } from '../../utils'
import { extractCookie } from '../../../../shared/lib/cookie'

const getCookies = (session = {}) => {
  return session.cookies && Array.isArray(session.cookies)
    ? Promise.resolve(session.cookies)
    : Promise.reject('Could not get cookies from session')
}

const getSession = (cookies = {}) => {
  const clientJsessionid = extractCookieValue('jsessionid', cookies)
  if (!clientJsessionid) return Promise.reject('No jsessionid')
  const cookieJarSettings = {
    host: process.env.REDIS_HOST_FOR_SESSION_STORE,
    port: process.env.REDIS_PORT_FOR_SESSION_STORE,
  }
  return pantry(cookieJarSettings).retrieveSession(clientJsessionid)
}

const getCookieFromStore = (cookieName, cookies = {}) => {
  return getSession(cookies)
    .then((res) => getCookies(res))
    .then((res) => {
      return extractCookie(cookieName, res)
    })
    .catch(() => null)
}

function isAuthenticated(clientCookies) {
  const cookiesAr = mapCookies(clientCookies)
  if (Array.isArray(cookiesAr)) {
    const authenticated = cookiesAr.find((cookie) =>
      cookie.startsWith('authenticated=')
    )
    return (authenticated && authenticated.indexOf('yes') >= 0) || false
  }
  return false
}

export { getCookies, getSession, isAuthenticated, getCookieFromStore }
