import { getExpiryYears } from '../genericUtils'

const currentYear = new Date().getFullYear()
const currentYearString = currentYear.toString()

describe('# genericUtils', () => {
  describe('# getExpiryYears', () => {
    it('given a falsy argument returns an array containing the current year as a string', () => {
      expect(getExpiryYears(false)).toEqual([currentYearString])
      expect(getExpiryYears(null)).toEqual([currentYearString])
      expect(getExpiryYears(undefined)).toEqual([currentYearString])
    })
    it('given a string as argument returns an array containing the current year as a string', () => {
      expect(getExpiryYears('a')).toEqual([currentYearString])
    })
    it('given a negative integer as argument returns an array containing the current year as a string', () => {
      expect(getExpiryYears(-1)).toEqual([currentYearString])
    })
    it('given 0 as argument returns an array containing the current year as a string', () => {
      expect(getExpiryYears(0)).toEqual([currentYearString])
    })
    it('given 1 as argument returns an array containing the current year as a string', () => {
      expect(getExpiryYears(1)).toEqual([currentYearString])
    })
    it('given 2 as argument returns an array containing 8 consecutive years (from the current year) as strings', () => {
      expect(getExpiryYears(2)).toEqual([
        currentYearString,
        (currentYear + 1).toString(),
      ])
    })
  })
})
