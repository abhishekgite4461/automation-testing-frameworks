import * as sessionUtils from '../sessionUtils'
import * as cartUtils from '../cartUtils'

const cookies = [
  'cookie1=foobar;',
  'cartId=123456;',
  'anotherCookie=batbaz;',
  'nominatedDeliveryDate=2017-09-26;',
]

describe('extractCartId', () => {
  afterEach(() => {
    jest.resetAllMocks()
  })
  it('should return a rejected promise if the cookies array does not contain a cartId cookie', () => {
    expect(cartUtils.extractCartId(['cookie1=foobar'])).rejects.toBe(
      'No cartId found in session cookies'
    )
  })

  it('should correctly parse the cartId cookie from an array of cookie strings', () => {
    expect(cartUtils.extractCartId(cookies)).resolves.toBe('123456')
  })
})

describe('getOrderId', () => {
  afterEach(() => {
    jest.resetAllMocks()
  })

  it('should return a rejected promise if the jsessionid cannot be determined', () => {
    sessionUtils.extractJsessionid = jest.fn(() =>
      Promise.reject('No jsessionid')
    )
    expect(cartUtils.getOrderId()).rejects.toBe('No jsessionid')
  })

  it('should return a rejected promise if the session cookies cannot be obtained', () => {
    sessionUtils.extractJsessionid = jest.fn(() => Promise.resolve())
    sessionUtils.getSession = jest.fn(() => Promise.resolve())
    sessionUtils.getCookies = jest.fn(() => Promise.reject('Cookies not found'))
    expect(cartUtils.getOrderId()).rejects.toBe('Cookies not found')
  })

  it('should call extractCartId with a session if it obtains one successfully', () => {
    sessionUtils.extractJsessionid = jest.fn()
    sessionUtils.getCookies = jest.fn(() => Promise.resolve(['cartId=67890;']))
    cartUtils.extractCartId = jest.fn()
    expect(cartUtils.getOrderId({ cookie: 'jessionId=12345' })).resolves.toBe(
      '67890'
    )
  })
})

describe('extractNominatedDeliveryDate', () => {
  it("returns false if doesn't exist", () => {
    expect(cartUtils.extractNominatedDeliveryDate([])).toBe(false)
  })

  it('throws if passed invalid cookies', () => {
    expect(() => cartUtils.extractNominatedDeliveryDate()).toThrow()
  })

  it('returns the date', () => {
    expect(cartUtils.extractNominatedDeliveryDate(cookies)).toBe('2017-09-26')
  })
})
