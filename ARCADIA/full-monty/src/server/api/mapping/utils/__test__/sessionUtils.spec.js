import {
  getCookies,
  getSession,
  isAuthenticated,
  getCookieFromStore,
} from '../sessionUtils'

jest.mock('@ag-digital/pantry', () => jest.fn())
import pantry from '@ag-digital/pantry'

describe('sessionUtils', () => {
  describe('#getCookies', () => {
    it('should return a rejected promise if the object passed does not contain cookies', () => {
      expect(getCookies({})).rejects.toBe('Could not get cookies from session')
    })

    it('should return a rejected promise if the cookies in the object passed are not an array', () => {
      expect(getCookies({ cookies: {} })).rejects.toBe(
        'Could not get cookies from session'
      )
    })

    it('should return a resolved promise with a cookies array when passed a session object that contains it', () => {
      expect(getCookies({ cookies: ['foo=bar'] })).resolves.toEqual(['foo=bar'])
    })
  })

  describe('#getSession', () => {
    afterEach(() => {
      jest.resetAllMocks()
    })

    it('should return a rejected promise if it is unable to extract the jessionid', () => {
      expect(getSession()).rejects.toBe('No jsessionid')
    })

    it('should use the pantry library to return a resolved promise containing a session', () => {
      global.process.env.REDIS_HOST_FOR_SESSION_STORE = 'host'
      global.process.env.REDIS_PORT_FOR_SESSION_STORE = 'port'

      const retrieveSessionMock = jest.fn(() => Promise.resolve('session'))
      pantry.mockReturnValue({ retrieveSession: retrieveSessionMock })

      expect(
        getSession(
          'cookieIWant=hereItIs; iDoNotWantThis=one; jsessionid=jsessionid'
        )
      ).resolves.toBe('session')
      expect(pantry).toHaveBeenCalledTimes(1)
      expect(pantry).toHaveBeenCalledWith({ host: 'host', port: 'port' })
      expect(retrieveSessionMock).toHaveBeenCalledTimes(1)
      expect(retrieveSessionMock).toHaveBeenCalledWith('jsessionid')
    })
  })

  describe('#getCookieFromStore', () => {
    afterEach(() => {
      jest.resetAllMocks()
      jest.restoreAllMocks()
    })

    it('returns false if no arguments provided', () => {
      return expect(getCookieFromStore()).resolves.toBe(null)
    })
    it('returns false if the cookie cannot be found', () => {
      global.process.env.REDIS_HOST_FOR_SESSION_STORE = 'host'
      global.process.env.REDIS_PORT_FOR_SESSION_STORE = 'port'
      const retrieveSessionMock = jest.fn(() =>
        Promise.resolve({
          cookies: ['cookieA=a', 'cookieSomething=cookieValue'],
        })
      )
      pantry.mockReturnValue({ retrieveSession: retrieveSessionMock })
      return expect(
        getCookieFromStore('cookieName', 'jsessionid=a;')
      ).resolves.toBe(null)
    })
    it('returns the value of the cookie searched', () => {
      global.process.env.REDIS_HOST_FOR_SESSION_STORE = 'host'
      global.process.env.REDIS_PORT_FOR_SESSION_STORE = 'port'
      const retrieveSessionMock = jest.fn(() =>
        Promise.resolve({ cookies: ['cookieA=a', 'cookieName=cookieValue'] })
      )
      pantry.mockReturnValue({ retrieveSession: retrieveSessionMock })
      return expect(
        getCookieFromStore(
          'cookieName',
          'cookieIWant=hereItIs; iDoNotWantThis=one; jsessionid=a'
        )
      ).resolves.toBe('cookieValue')
    })
  })

  describe('isAuthenticated', () => {
    it('should return false if the argument passed is not a string containing cookies', () => {
      expect(isAuthenticated()).toBe(false)
      expect(isAuthenticated([])).toBe(false)
    })

    it('should return false if there is no "authenticated" cookie', () => {
      expect(isAuthenticated('foo=bar;')).toBe(false)
    })

    it('should return true if there is an "authenticated" cookie with value "yes"', () => {
      expect(isAuthenticated('authenticated=yes;')).toBe(true)
    })
  })
})
