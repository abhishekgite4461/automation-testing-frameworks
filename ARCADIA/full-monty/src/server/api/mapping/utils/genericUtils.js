import diacritics from 'diacritics'

/*
    Given an integer "numberOfYears" returns an array containing "numberOfYears" years starting from the current one.
    e.g.:
          - numberOfYears = 6 => [2017, 2018, 2019, 2020, 2021, 2022]
 */
export function getExpiryYears(numberOfYears) {
  const expiryDates = [new Date().getFullYear().toString()]
  while (expiryDates.length < numberOfYears) {
    expiryDates.push(
      (parseInt(expiryDates[expiryDates.length - 1], 10) + 1).toString()
    )
  }
  return expiryDates
}

/**
 * Temporary function to remove diacritic characters from payload.
 * @todo Remove this function when WCS changes are applied to allow diacritic characters.
 * @param {string} field
 */
export const removeAddressDiacritics = (field) =>
  typeof field === 'string' ? diacritics.remove(field) : field
