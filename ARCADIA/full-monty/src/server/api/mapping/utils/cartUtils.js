import r from 'ramda'
import { getSession, getCookies } from './sessionUtils'

const extractCartId = (cookies = []) => {
  const cookie = cookies.find((item) => item.indexOf('cartId') >= 0)
  return cookie
    ? Promise.resolve(
        cookie.substring(cookie.indexOf('cartId=') + 7, cookie.indexOf(';'))
      )
    : Promise.reject('No cartId found in session cookies')
}

const getOrderId = async (clientCookies = {}) => {
  const session = await getSession(clientCookies)
  const sessionCookies = await getCookies(session)
  return extractCartId(sessionCookies)
}

/**
 * Returns the delivery date or false
 *
 * @throws if passed invalid cookies
 * @param cookies {Array<String>} The cookies from the session between monty server and WCS
 * @return {String|false}
 */
export function extractNominatedDeliveryDate(cookies) {
  if (!cookies || !Array.isArray(cookies))
    throw new Error('Expected cookies array')
  return cookies.reduce((acc, c) => {
    return r.match(/^nominatedDeliveryDate=([^;]+);$/, c)[1] || acc
  }, false)
}

export { extractCartId, getOrderId }
