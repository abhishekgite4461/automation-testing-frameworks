/*
  The responsability of this is to provide the default Mapper.
  The default mapper does not modify the request query/payload, calls the API just once (no 1:N mapping), does not
  modify the response body and finally returns the promise.

  The default Mapper will be extended by the specific mappers which will override the default methods.
 */

import { sendRequestToApi } from '../api'
import { getConfigByStoreCode } from '../../config'
import { getDestinationHostFromStoreCode, translate } from '../utils'
import errorDictionary from '../dictionaries/errorMessages.json'

export default class Mapper {
  /**
   * [constructor sets instance attributes associated with the current request]
   * @param  {String} originEndpoint [hapi endpoint hit by the current request]
   * @param  {Object} query          [query parameters of the current request]
   * @param  {Object} payload        [payload object of the current request]
   * @param  {String} method         [current request's method]
   * @param  {Object} headers        [headers passed in the current request]
   * @param  {Object} params         [contains the request parameters passed through the path (e.g.: /api/products/{identifier} => params = { identifier: '123' })]
   */
  constructor(originEndpoint, query, payload, method, headers, params) {
    const storeCode = (headers && headers['brand-code']) || 'tsuk'
    this.storeConfig = getConfigByStoreCode(storeCode)
    this.destinationHostname = getDestinationHostFromStoreCode(
      process.env.WCS_ENVIRONMENT,
      storeCode
    )
    this.destinationEndpoint = originEndpoint
    this.query = query
    this.payload = payload
    this.method = method
    this.headers = headers
    this.params = params
    this.timeout = 0
  }

  mapEndpoint() {}

  mapRequestParameters() {}

  mapResponseBody(body) {
    return body
  }

  mapResponseError(error) {
    if (error.message) {
      if (error.message === 'wcsSessionTimeout') {
        throw error
      }

      error.message = translate(
        errorDictionary,
        this.storeConfig.lang,
        error.message
      )
    }
    throw error
  }

  mapResponse(res) {
    return {
      jsessionid: res.jsessionid,
      body: this.mapResponseBody(res.body),
    }
  }

  execute() {
    this.mapRequestParameters()
    this.mapEndpoint()
    return sendRequestToApi(
      this.destinationHostname,
      this.destinationEndpoint,
      this.query,
      this.payload,
      this.method.toLowerCase(),
      this.headers,
      null,
      false,
      false,
      this.timeout
    )
      .then((res) => this.mapResponse(res))
      .catch((apiResponseError) => {
        return this.mapResponseError(apiResponseError)
      })
  }
}
