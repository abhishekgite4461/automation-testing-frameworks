import SiteOptions from '../SiteOptions'
import * as transform from '../../transforms/siteOptions'
import wcs from '../../../../../../test/apiResponses/site-options/wcs.json'
import hapiMonty from '../../../../../../test/apiResponses/site-options/hapiMonty.json'

jest.mock('../../Mapper')

describe('# SiteOptions', () => {
  describe('# mapEndpoint', () => {
    it('expects to set "destinationEndpoint" property to the expected value', () => {
      const siteOptions = new SiteOptions()
      expect(siteOptions.destinationEndpoint).toBeUndefined()
      siteOptions.mapEndpoint()
      expect(siteOptions.destinationEndpoint).toEqual(
        '/webapp/wcs/stores/servlet/SiteOptions'
      )
    })
  })
  describe('# mapRequestParameters', () => {
    it('expects to set "query" property to the expeted value if no storeConfig available', () => {
      const siteOptions = new SiteOptions()

      expect(siteOptions.query).toBeUndefined()
      siteOptions.mapRequestParameters()
      expect(siteOptions.query).toEqual({
        langId: undefined,
        catalogId: undefined,
        storeId: undefined,
      })
    })
    it('expects to set "query" property to the expeted value if "brand-code" in "headers" property', () => {
      const siteOptions = new SiteOptions()
      siteOptions.storeConfig = {
        siteId: 12556,
        catalogId: '33057',
        langId: '-1',
      }
      siteOptions.headers = { 'brand-code': 'tsuk' }
      expect(siteOptions.query).toBeUndefined()
      siteOptions.mapRequestParameters()
      expect(siteOptions.query).toEqual({
        storeId: 12556,
        catalogId: '33057',
        langId: '-1',
      })
    })
  })
  describe('# mapReponseBody', () => {
    it('# returns the expected object', () => {
      const spy = jest.spyOn(transform, 'default')
      const siteOptions = new SiteOptions()

      expect(spy).toHaveBeenCalledTimes(0)
      const r = siteOptions.mapResponseBody(wcs)
      expect(spy).toHaveBeenCalledTimes(1)
      expect(spy).toHaveBeenCalledWith(wcs)
      expect(r).toEqual(hapiMonty)

      spy.mockRestore()
    })
  })
})
