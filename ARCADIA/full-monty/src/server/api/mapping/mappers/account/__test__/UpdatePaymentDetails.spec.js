import UpdatePaymentDetails from '../UpdatePaymentDetails'

import wcsProfileFormView from '../../../../../../../test/apiResponses/my-account/wcs-ProfileFormView.json'

jest.mock('../../../transforms/logon')
jest.mock('../../../../api', () => ({
  sendRequestToApi: jest.fn(),
}))
jest.mock('../cookies', () => ({
  authenticatedCookies: jest.fn(),
}))
import { authenticatedCookies } from '../cookies'

import { sendRequestToApi } from '../../../../api'

import logonTransform from '../../../transforms/logon'

const storeConfig = {
  catalogId: 'test',
  langId: -1,
  siteId: 'test',
}

const payloadFromMonty = {
  billingDetails: {
    nameAndPhone: {
      title: 'Mr',
      firstName: 'Karthi',
      lastName: 'D',
      telephone: '1231231231',
    },
    address: {
      country: 'United Kingdom',
      postcode: 'N7 7AJ',
      address1: 'Emirates Stadium',
      address2: 'Queensland Road',
      city: 'LONDON',
      state: '',
    },
  },
  deliveryDetails: {
    nameAndPhone: {
      title: 'Ms',
      firstName: 'Testy',
      lastName: 'McTestTest',
      telephone: '73577357',
    },
    address: {
      country: 'United Kingdom',
      postcode: '1A1 1A1',
      address1: 'Test House',
      address2: 'Test Street',
      city: 'LONDON',
      state: '',
    },
  },
  creditCard: {
    expiryYear: '2018',
    expiryMonth: '01',
    type: 'VISA',
    cardNumber: '0000000000000000',
  },
}

const payloadToWCS = {
  callingForm: 'QuickCheckout',
  storeId: 12556,
  langId: '-1',
  catalogId: '33057',
  page: 'quickcheckout',
  returnPage: '',
  errorViewName: 'ProfileFormView',
  billing_errorViewName: 'ProfileFormView',
  shipping_errorViewName: 'ProfileFormView',
  billing_nickName: 'Default_Billing_12556',
  shipping_nickName: 'Default_Shipping_12556',
  billing_email1: 'monty@desktop.com',
  URL:
    'LogonForm?shipping*=&billing*=&nickName*=&lastName*=&firstName*=&address*=&zipCode*=&city*=&state*=&country*=&phone1*=&phone2*=&pay_cardNumber*=',
  addressType: 'R',
  primary: '0',
  dropDownAction: '0',
  isoCode: '',
  billingIsoCode: 'GBR',
  shippingIsoCode: 'GBR',
  billing_personTitle: 'Mr',
  billing_firstName: 'Karthi',
  billing_lastName: 'D',
  billing_phone1: '1231231231',
  billing_country: 'United Kingdom',
  ShippingCountryInList: 'true',
  billing_house_number: '',
  billing_postcode: 'N7 7AJ',
  billing_state_hidden: '',
  billing_address1: 'Emirates Stadium',
  billing_address2: 'Queensland Road',
  billing_city: 'LONDON',
  billing_state_select: '',
  billing_state_input: '',
  billing_zipCode: 'N7 7AJ',
  sameaddress: 'on',
  shipping_personTitle: 'Ms',
  shipping_firstName: 'Testy',
  shipping_lastName: 'McTestTest',
  shipping_phone1: '73577357',
  shipping_country: 'United Kingdom',
  delivery_house_number: '',
  delivery_postcode: '',
  delivery_address_results: '',
  shipping_state_hidden: '',
  shipping_address1: 'Test House',
  shipping_address2: 'Test Street',
  shipping_city: 'LONDON',
  shipping_state_select: '',
  shipping_state_input: '',
  shipping_postcode: '1A1 1A1',
  shipping_zipCode: '1A1 1A1',
  pay_payMethodId: '10008',
  pay_cardBrand: 'VISA',
  lastCard: '',
  pay_cardNumberStar: '0000000000000000',
  encryptedCCFlag: 'encryptedCCFlag',
  pay_cardNumber: '0000000000000000',
  pay_cardExpiryMonth: '01',
  pay_cardExpiryYear: '2018',
  'submitButton.x': '41',
  'submitButton.y': '7',
}

const accountInfoBody = { body: 'accountInfo' }
const responseBody = { body: 'WCS Response' }
const transformedBody = { body: 'Monty Response' }

describe('UpdatePaymentDetails mapper', () => {
  describe('mapEndpoint', () => {
    it('sets the endpoint to /webapp/wcs/stores/servlet/QASAddress', () => {
      const updatePaymentDetails = new UpdatePaymentDetails()
      expect(updatePaymentDetails.destinationEndpoint).toBeUndefined()
      updatePaymentDetails.mapEndpoint()
      expect(updatePaymentDetails.destinationEndpoint).toBe(
        '/webapp/wcs/stores/servlet/QASAddress'
      )
    })
  })

  describe('getAccountInfo', () => {
    it('should call sendRequestToApi', () => {
      const updatePaymentDetails = new UpdatePaymentDetails()
      expect(sendRequestToApi).not.toHaveBeenCalled()
      updatePaymentDetails.getAccountInfo()
      expect(sendRequestToApi).toHaveBeenCalledTimes(1)
    })

    it('should call sendRequestToApi with the correct parameters', () => {
      const updatePaymentDetails = new UpdatePaymentDetails()
      updatePaymentDetails.headers = { hea: 'ders' }
      updatePaymentDetails.storeConfig = storeConfig
      updatePaymentDetails.getAccountInfo()
      expect(sendRequestToApi).toHaveBeenCalledWith(
        false,
        '/webapp/wcs/stores/servlet/ProfileFormView',
        {
          catalogId: 'test',
          langId: -1,
          storeId: 'test',
        },
        {},
        'get',
        { hea: 'ders' }
      )
    })

    it('should set the response from the WCS as accountInfo', async () => {
      sendRequestToApi.mockReturnValue(Promise.resolve(accountInfoBody))
      const updatePaymentDetails = new UpdatePaymentDetails()
      await updatePaymentDetails.getAccountInfo()
      expect(updatePaymentDetails.accountInfo).toBe('accountInfo')
    })
  })

  describe('mapRequestParameters', () => {
    it('sets the payload to the expected values', () => {
      const updatePaymentDetails = new UpdatePaymentDetails()
      updatePaymentDetails.payload = payloadFromMonty
      updatePaymentDetails.accountInfo = wcsProfileFormView
      updatePaymentDetails.mapRequestParameters()
      expect(updatePaymentDetails.payload).toEqual(payloadToWCS)
    })
  })

  describe('mapResponseBody', () => {
    it('maps the response body using the updatePaymentDetails transform function', () => {
      const updatePaymentDetails = new UpdatePaymentDetails()
      logonTransform.mockReturnValue(transformedBody)
      expect(updatePaymentDetails.mapResponseBody(responseBody)).toBe(
        transformedBody
      )
      expect(logonTransform).toHaveBeenCalledTimes(1)
      expect(logonTransform).toHaveBeenCalledWith(responseBody, false)
    })
  })

  describe('mapResponse', () => {
    it('should return the response in an expected format', () => {
      const updatePaymentDetails = new UpdatePaymentDetails()
      updatePaymentDetails.mapResponseBody = jest.fn(() => 'body')
      authenticatedCookies.mockReturnValue('cookie')
      expect(updatePaymentDetails.mapResponse({ jsessionid: 'foo' })).toEqual({
        jsessionid: 'foo',
        body: 'body',
        setCookies: 'cookie',
      })
    })
  })
})
