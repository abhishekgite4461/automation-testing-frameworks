import Register from '../Register'

jest.mock('../../../Mapper')
jest.mock('../../../transforms/logon')
jest.mock('../../../../api', () => ({
  sendRequestToApi: jest.fn(),
}))
jest.mock('../../../../../lib/bazaarvoice-utils', () => ({
  encodeUserId: jest.fn(),
}))

import transform from '../../../transforms/logon'
import { sendRequestToApi } from '../../../../api'
import { encodeUserId } from '../../../../../lib/bazaarvoice-utils'

jest.mock('../cookies', () => ({
  registerCookies: jest.fn(),
}))
import { registerCookies } from '../cookies'

jest.mock('../../../utils/sessionUtils')
import * as sessionUtils from '../../../utils/sessionUtils'

const wcsPayload = {
  storeId: 12556,
  catalogId: '33057',
  create_logonId: 'bob@mvrht.net',
  logonPassword: 'test123',
  logonPasswordVerify: 'test123',
  subscribe: 'NO',
  source: 'MYACCOUNT',
  preferredLanguage: '-1',
  preferredCurrency: 'GBP',
  profileType: 'C',
  registerType: 'G',
  challengeAnswer: '-',
  challengeQuestion: '-',
  page: 'account',
  redirectURL:
    'UserRegistrationForm?langId=-1&storeId=12556&catalogId=33057&new=Y&returnPage=',
  URL: 'UserRegistrationAjaxView',
  personalizedCatalog: 'false',
  new: 'Y',
  checkUserAccountUrl:
    'UserIdExists?storeId=12556&catalogId=33057&URL=UserRegistrationAjaxView&ErrorViewName=UserRegistrationAjaxView&action=check',
  errorViewName: 'UserRegistrationAjaxView',
  defaultServiceId: 8,
}

const montyPayload = {
  email: 'bob@mvrht.net',
  password: 'test123',
  passwordConfirm: 'test123',
  subscribe: false,
}

const storeConfig = {
  catalogId: '33057',
  langId: '-1',
  siteId: 12556,
  currencyCode: 'GBP',
}

const responseBody = { success: true, userTrackingId: 1848001 }

const transformedBody = { body: 'monty', userTrackingId: 1848001 }
const resultBody = { body: 'monty', userTrackingId: 1848001 }

const logonFormQuery = {
  storeId: 12556,
  catalogId: '33057',
  langId: '-1',
  new: 'Y',
  returnPage: '',
  personalizedCatalog: false,
  reLogonURL: 'LogonForm',
}

const jsessionid = '123'

const firstApiResponse = {
  body: {
    success: true,
  },
  jsessionid,
}

const secondApiResponse = {
  body: {
    success: true,
    userTrackingId: '12345678',
  },
}
const headers = { cookies: 'Some cookies' }

const expectedSetCookies = ['cookie1', 'cookie2']

const response = {
  jsessionid,
  body: responseBody,
}

const resultResponse = {
  jsessionid,
  body: transformedBody,
  setCookies: expectedSetCookies,
}

describe('Register', () => {
  beforeEach(() => {
    jest.resetAllMocks()
  })

  describe('mapEndpoint', () => {
    it('sets destinationEndpoint to UserRegistrationAdd', async () => {
      const register = new Register()
      register.headers = { cookie: 'cookie' }
      sessionUtils.getCookieFromStore.mockReturnValueOnce(Promise.resolve('N'))

      expect(register.destinationEndpoint).toBeUndefined()

      await register.mapEndpoint()

      expect(register.destinationEndpoint).toBe(
        `/webapp/wcs/stores/servlet/UserRegistrationAdd`
      )
    })
    it('sets the destination endpoint to UserRegistrationUpdate', async () => {
      const register = new Register()
      register.headers = { cookie: 'cookie' }
      sessionUtils.getCookieFromStore.mockReturnValueOnce(Promise.resolve('Y'))

      expect(register.destinationEndpoint).toBeUndefined()

      await register.mapEndpoint()

      expect(register.destinationEndpoint).toBe(
        '/webapp/wcs/stores/servlet/UserRegistrationUpdate'
      )
    })
    it('sets logonFormEndpoint to LogonForm', async () => {
      const register = new Register()
      register.headers = { cookie: 'cookie' }
      sessionUtils.getCookieFromStore.mockReturnValueOnce(Promise.resolve('Y'))

      expect(register.logonFormEndpoint).toBeUndefined()

      await register.mapEndpoint()

      expect(register.logonFormEndpoint).toEqual(
        `/webapp/wcs/stores/servlet/LogonForm`
      )
    })
  })

  describe('mapRequestParameters', () => {
    it('sets the payload', () => {
      const register = new Register()

      register.storeConfig = storeConfig
      expect(register.payload).toBeUndefined()
      register.payload = montyPayload

      register.mapRequestParameters()

      expect(register.payload).toEqual(wcsPayload)
    })

    it('sets logonFormQuery', () => {
      const register = new Register()

      register.storeConfig = storeConfig

      expect(register.logonFormQuery).toBeUndefined()

      register.payload = montyPayload
      register.mapRequestParameters()

      expect(register.logonFormQuery).toEqual(logonFormQuery)
    })

    it('sets the appId in the payload', () => {
      const appId = '12345'
      const register = new Register()

      register.storeConfig = storeConfig

      register.payload = {
        ...montyPayload,
        appId,
      }

      register.mapRequestParameters()

      expect(register.payload).toEqual({
        ...wcsPayload,
        appId,
      })
    })
  })

  describe('mapResponseBody', () => {
    it('calls transform with the body passed in', () => {
      transform.mockReturnValue(transformedBody)

      const register = new Register()

      expect(register.mapResponseBody(responseBody)).toEqual(resultBody)
      expect(transform).toHaveBeenCalledTimes(1)
      expect(transform).toHaveBeenCalledWith(responseBody, true)
    })
  })

  describe('mapResponse', () => {
    it('returns a mapped response containing a setCookies property', () => {
      const register = new Register()
      register.mapResponseBody = jest.fn(() => transformedBody)
      registerCookies.mockReturnValue(expectedSetCookies)
      encodeUserId.mockReturnValue('bv')
      expect(register.mapResponse(response)).toEqual(resultResponse)
    })
    it('calls mapResponseBody', () => {
      const register = new Register()
      register.mapResponseBody = jest.fn(() => transformedBody)
      registerCookies.mockReturnValue(expectedSetCookies)
      register.mapResponse(response)
      expect(register.mapResponseBody).toHaveBeenCalledTimes(1)
      expect(register.mapResponseBody).toHaveBeenCalledWith(responseBody)
    })
    it('calls getCookiesToSet', () => {
      const register = new Register()
      register.mapResponseBody = jest.fn(() => transformedBody)
      registerCookies.mockReturnValue(expectedSetCookies)
      encodeUserId.mockReturnValue('bv')
      register.mapResponse(response)
      expect(registerCookies).toHaveBeenCalledTimes(1)
      expect(registerCookies).toHaveBeenCalledWith('bv')
    })
  })

  describe('execute', () => {
    it('calls mapRequestParameters', () => {
      sendRequestToApi.mockReturnValue({})
      const register = new Register()
      register.mapRequestParameters = jest.fn()
      expect(register.mapRequestParameters).not.toHaveBeenCalled()
      register.execute()
      expect(register.mapRequestParameters).toHaveBeenCalledTimes(1)
      expect(register.mapRequestParameters).toHaveBeenCalledWith()
    })
    it('calls mapEndpoint', () => {
      sendRequestToApi.mockReturnValue(Promise.resolve({}))
      const register = new Register()
      register.mapRequestParameters = jest.fn()
      register.mapEndpoint = jest.fn()
      expect(register.mapEndpoint).not.toHaveBeenCalled()
      register.execute()
      expect(register.mapEndpoint).toHaveBeenCalledTimes(1)
      expect(register.mapEndpoint).toHaveBeenCalledWith()
    })
    it('calls sendRequestToApi two times', async () => {
      sendRequestToApi
        .mockReturnValueOnce(Promise.resolve(firstApiResponse))
        .mockReturnValueOnce(Promise.resolve(secondApiResponse))
      transform.mockReturnValue(transformedBody)
      const register = new Register()
      register.mapRequestParameters = jest.fn()
      register.mapEndpoint = jest.fn()
      expect(sendRequestToApi).not.toHaveBeenCalled()
      await register.execute()
      expect(sendRequestToApi).toHaveBeenCalledTimes(2)
    })
    it('calls sendRequestToApi for the UserRegistrationAdd endpoint', async () => {
      sendRequestToApi
        .mockReturnValueOnce(Promise.resolve(firstApiResponse))
        .mockReturnValueOnce(Promise.resolve(secondApiResponse))
      transform.mockReturnValue(transformedBody)
      const register = new Register()
      register.mapRequestParameters = jest.fn()
      register.mapEndpoint = jest.fn()
      register.destinationEndpoint = 'destinationEndpoint'
      register.payload = wcsPayload
      register.headers = headers
      register.method = 'post'
      register.destinationHostname = 'hostname'

      expect(sendRequestToApi).not.toHaveBeenCalled()

      await register.execute()

      expect(sendRequestToApi).toHaveBeenCalledTimes(2)

      expect(sendRequestToApi.mock.calls[0]).toEqual([
        'hostname',
        'destinationEndpoint',
        {},
        wcsPayload,
        'post',
        headers,
      ])
    })
    it('calls sendRequestToApi for the logonForm endpoint', async () => {
      sendRequestToApi
        .mockReturnValueOnce(Promise.resolve(firstApiResponse))
        .mockReturnValueOnce(Promise.resolve(secondApiResponse))
      transform.mockReturnValue(transformedBody)
      const register = new Register()
      register.mapRequestParameters = jest.fn()
      register.mapEndpoint = jest.fn()
      register.logonFormEndpoint = 'logonFormEndpoint'
      register.logonFormQuery = logonFormQuery
      register.headers = headers
      register.destinationHostname = 'hostname'

      expect(sendRequestToApi).not.toHaveBeenCalled()

      await register.execute()

      expect(sendRequestToApi).toHaveBeenCalledTimes(2)
      expect(sendRequestToApi.mock.calls[1]).toEqual([
        'hostname',
        'logonFormEndpoint',
        logonFormQuery,
        {},
        'get',
        headers,
        jsessionid,
      ])
    })
  })
})
