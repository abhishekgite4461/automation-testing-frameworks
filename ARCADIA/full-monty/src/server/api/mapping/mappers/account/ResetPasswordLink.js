import Boom from 'boom'
import Mapper from '../../Mapper'

export default class ResetPasswordLink extends Mapper {
  mapEndpoint() {
    this.destinationEndpoint = '/webapp/wcs/stores/servlet/ResetPassword'
  }

  mapRequestParameters() {
    const { catalogId, langId, siteId: storeId } = this.storeConfig

    const { email = '' } = this.payload

    this.payload = {
      catalogId,
      storeId,
      langId,
      URL: 'ResetPasswordAjaxView',
      challengeAnswer: '-',
      reset_logonId: email,
      deviceType: 'desktop',
    }

    // If cookies are sent containing a jsessionid then WCS sometimes treats the
    // request in a manner different to what is required, producing an failure
    // where the error message says something similar to "Please enter your password.".
    this.headers.cookie = undefined
  }

  mapResponseError(body) {
    if (body.errorMessage) {
      // An unexpected error has occurred within WCS. We have logged this issue
      // with service desk. It appears the request for the password reset link
      // randomly fails for some user accounts. The body response contains an
      // "errorMessage" prop for this case so we will throw an internal error
      // when it occurs to avoid confusing users with inappropriate error
      // messages from WCS.
      throw Boom.internal(
        'Unfortunately an error occurred. Please try again later.',
        body
      )
    } else {
      throw body.message ? Boom.badData(body.message) : body
    }
  }

  mapResponseBody(body = {}) {
    if (!body.success || body.success === 'false')
      return this.mapResponseError(body)
    return body
  }
}

export const resetPasswordLinkSpec = {
  summary:
    'Request a password reset email containing a link to reset the password',
  parameters: [
    {
      name: 'payload',
      in: 'body',
      required: true,
      description: 'Email for account whose password is to be changed',
      schema: {
        type: 'object',
        properties: {
          email: {
            type: 'string',
            example: 'foo@bar.com',
          },
        },
      },
    },
  ],
  responses: {
    200: {
      description: 'New account summary',
      schema: {
        type: 'object',
        properties: {
          success: {
            type: 'boolean',
            example: true,
          },
          message: {
            type: 'string',
            example:
              "Thanks! We've sent you an email. It should arrive in a couple of minutes - be sure to check your junk folder just in case.",
          },
        },
      },
    },
    422: {
      description: "Account doesn't exist",
      schema: {
        type: 'object',
        properties: {
          statusCode: {
            type: 'number',
            example: 422,
          },
          error: {
            type: 'string',
            example: 'Unprocessable Entity',
          },
          message: {
            type: 'string',
            example:
              'There is no account with that email address in our records. If you would like to use that email address, you can create an account with the New customers section of this page.',
          },
        },
      },
    },
  },
}
