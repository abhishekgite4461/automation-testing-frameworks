import {
  cookieOptions,
  cookieOptionsBag,
  cookieOptionsAuth,
  cookieOptionsUnset,
} from '../../../../../lib/auth'

const logonHeaders = (bvToken) => [
  {
    name: 'bvtoken',
    value: bvToken,
  },
]

const createCookies = (instructions) =>
  instructions.map(([name, value, options]) => ({
    name,
    value,
    options,
  }))

const logonCookies = ({ basketItemCount = 0 }, bvToken) =>
  createCookies([
    ['bvToken', bvToken, cookieOptions],
    ['bagCount', basketItemCount.toString(), cookieOptionsBag],
    ['authenticated', 'yes', cookieOptionsAuth],
    ['klarnaClientToken', null, cookieOptionsUnset],
    ['klarnaSessionId', null, cookieOptionsUnset],
  ])

const logoutCookies = () =>
  createCookies([
    ['bvToken', null, cookieOptionsUnset],
    ['bagCount', null, cookieOptionsUnset],
    ['authenticated', null, cookieOptionsAuth],
    ['klarnaClientToken', null, cookieOptionsUnset],
    ['klarnaSessionId', null, cookieOptionsUnset],
  ])

const authenticatedCookies = () =>
  createCookies([['authenticated', 'yes', cookieOptionsAuth]])

const registerCookies = (bvToken) =>
  createCookies([
    ['authenticated', 'yes', cookieOptionsAuth],
    ['bvToken', bvToken, cookieOptions],
  ])

export {
  logonHeaders,
  logonCookies,
  logoutCookies,
  registerCookies,
  authenticatedCookies,
}
