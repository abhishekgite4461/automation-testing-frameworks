import * as utils from '../../../__test__/utils'
import ResetPasswordLink from '../ResetPasswordLink'

describe('ResetPasswordLink mapper', () => {
  const wcsResponse = {
    body: {
      success: true,
      action: 'reset',
      message:
        "Thanks! We've sent you an email. It should arrive in a couple of minutes - be sure to check your junk folder just in case.",
    },
  }

  beforeEach(() => {
    jest.clearAllMocks()
    jest.resetModules()
    utils.setUserSession([
      utils.createCookies()(utils.createJSessionIdCookie(12345)),
    ])
    utils.setWCSResponse(wcsResponse)
  })

  const email = 'foo@bar.com'
  const config = utils.getConfigByStoreCode('tsuk')

  const payloadFromMonty = {
    email,
  }

  const payloadToWCS = {
    URL: 'ResetPasswordAjaxView',
    storeId: config.siteId,
    langId: config.langId,
    catalogId: config.catalogId,
    challengeAnswer: '-',
    reset_logonId: email,
    deviceType: 'desktop',
  }

  const defaults = {
    payload: payloadFromMonty,
    method: 'post',
    query: {},
    headers: {
      cookie: 'jsession=ABCITSEASYAS123',
    },
    params: {},
  }

  const execute = utils.buildExecutor(ResetPasswordLink, defaults)

  describe('Requests to WCS', () => {
    it('should set the cookies as being blank to avoid jsessionid being sent to WCS', () => {
      execute()
      expect(utils.getRequestArgs(0).headers.cookie).toEqual(undefined)
    })

    it('should use the correct method', () => {
      execute()
      expect(utils.getRequestArgs(0).method).toBe('post')
    })

    it('should use the correct endpoint', () => {
      execute()
      expect(utils.getRequestArgs(0).endpoint).toBe(
        '/webapp/wcs/stores/servlet/ResetPassword'
      )
    })

    it('should set the payload correctly', () => {
      execute()
      expect(utils.getRequestArgs(0).payload).toEqual(payloadToWCS)
    })
  })

  describe('Successful responses to WCS', () => {
    it('should be mapped correctly', () => {
      expect(execute()).resolves.toEqual({
        body: {
          success: true,
          action: 'reset',
          message:
            "Thanks! We've sent you an email. It should arrive in a couple of minutes - be sure to check your junk folder just in case.",
        },
      })
    })
  })

  describe('Unsuccessful responses from WCS', () => {
    it('should throw an error for an invalid email', async () => {
      utils.setWCSResponse({
        success: 'false',
        message: 'There was no account associated with that email address.',
      })
      expect.assertions(1)
      await expect(execute()).rejects.toMatchObject(
        new Error('There was no account associated with that email address.')
      )
    })

    it('should throw for unexpected errors', async () => {
      utils.setWCSResponse({
        errorMessage: 'An unexpected error occurred.',
      })
      await expect(execute()).rejects.toMatchObject(
        new Error('Unfortunately an error occurred. Please try again later.')
      )
    })
  })

  describe('Unsuccessful requests to WCS', () => {
    it('should throw a 422 error', () => {
      utils.setWCSResponse(Promise.reject('WCS is down'))
      expect(execute()).rejects.toBe('WCS is down')
    })
  })
})
