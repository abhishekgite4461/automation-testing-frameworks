import UpdateProfile from '../UpdateProfile'

jest.mock('../../../../api', () => ({ sendRequestToApi: jest.fn() }))
import { sendRequestToApi } from '../../../../api'

jest.mock('../../../transforms/logon')
import logonTransform from '../../../transforms/logon'

jest.mock('boom', () => ({
  badData: jest.fn(),
}))
import Boom from 'boom'

import {
  updateProfileConstants,
  logonFormConstants,
} from '../../../constants/updateProfile'

import montyLogon from '../../../../../../../test/apiResponses/my-account/hapiMonty.json'

jest.mock('../cookies', () => ({
  authenticatedCookies: jest.fn(),
}))
import { authenticatedCookies } from '../cookies'

const storeConfig = {
  catalogId: 33057,
  langId: '-1',
  siteId: 12556,
}

const payloadFromMonty = {
  email: 'monty@desktop.com',
  firstName: 'Karthi',
  lastName: 'D',
  title: 'Mr',
}

const payloadToWCS = {
  ...updateProfileConstants,
  catalogId: 33057,
  langId: '-1',
  storeId: 12556,
  errorViewName: 'UserRegistrationForm',
  personTitle: 'Mr',
  firstName: 'Karthi',
  lastName: 'D',
  logonId: 'monty@desktop.com',
  origLogonId: 'monty@desktop.com',
  tempEmail1: '12556monty@desktop.com',
  nickName: '12556monty@desktop.com',
  default_service_id: 8,
  subscribe: 'NO',
}

const logonFormParameters = {
  catalogId: '33057',
  langId: '-1',
  storeId: 12556,
  ...logonFormConstants,
}

const responseBody = { body: 'WCS Logon response' }

const transformedBody = { body: 'Monty response' }

const wcs = '/webapp/wcs/stores/servlet/'

describe('UpdateProfile', () => {
  beforeEach(() => {
    jest.resetAllMocks()
  })

  describe('mapEndpoint', () => {
    it('should set the destinationEndpoint to /webapp/wcs/stores/servlet/UserRegistrationUpdate', () => {
      const updateProfile = new UpdateProfile()
      expect(updateProfile.destinationEndpoint).toBeUndefined()
      updateProfile.mapEndpoint()
      expect(updateProfile.destinationEndpoint).toBe(
        `${wcs}UserRegistrationUpdate`
      )
    })

    it('sets the logonFormEndpoint to /webapp/wcs/stores/servlet/LogonForm', () => {
      const updateProfile = new UpdateProfile()
      expect(updateProfile.logonFormEndpoint).toBeUndefined()
      updateProfile.mapEndpoint()
      expect(updateProfile.logonFormEndpoint).toBe(`${wcs}LogonForm`)
    })
  })

  describe('mapRequestParameters', () => {
    it('should set the payload to a format expected by WCS', () => {
      const updateProfile = new UpdateProfile()
      updateProfile.storeConfig = storeConfig
      updateProfile.payload = payloadFromMonty
      updateProfile.mapRequestParameters()
      expect(updateProfile.payload).toEqual(payloadToWCS)
    })

    describe('store is burton uk', () => {
      it('should set the nickName with parent store ID', () => {
        const updateProfile = new UpdateProfile()
        updateProfile.storeConfig = { storeCode: 'bruk' }
        updateProfile.payload = payloadFromMonty
        updateProfile.mapRequestParameters()
        expect(updateProfile.payload.nickName).toBe('12551monty@desktop.com')
        expect(updateProfile.payload.tempEmail1).toBe('12551monty@desktop.com')
      })

      it('should set the serviceID to 5', () => {
        const updateProfile = new UpdateProfile()
        updateProfile.storeConfig = { storeCode: 'bruk' }
        updateProfile.payload = payloadFromMonty
        updateProfile.mapRequestParameters()
        expect(updateProfile.payload.serviceID).toBe(5)
      })

      it('should set the default_service_id to 5', () => {
        const updateProfile = new UpdateProfile()
        updateProfile.storeConfig = { storeCode: 'bruk' }
        updateProfile.payload = payloadFromMonty
        updateProfile.mapRequestParameters()
        expect(updateProfile.payload.default_service_id).toBe(5)
      })
    })

    describe('marketing emails subscription', () => {
      it("should set the 'subscribe' flag to 'YES'", () => {
        const updateProfile = new UpdateProfile()
        updateProfile.payload = {
          ...payloadFromMonty,
          marketingSubscription: true,
        }

        updateProfile.mapRequestParameters()

        expect(updateProfile.payload.subscribe).toEqual('YES')
      })

      it("should set the 'subscribe' flag to 'NO'", () => {
        const updateProfile = new UpdateProfile()
        updateProfile.payload = {
          ...payloadFromMonty,
          marketingSubscription: false,
        }

        updateProfile.mapRequestParameters()

        expect(updateProfile.payload.subscribe).toEqual('NO')
      })
    })
  })

  describe('mapLogonFormParameters', () => {
    it('should return the correct parameters for the LogonForm endpoint', () => {
      const updateProfile = new UpdateProfile()
      expect(updateProfile.mapLogonFormParameters()).toEqual(
        logonFormParameters
      )
    })
  })

  describe('getWCSLogonData', () => {
    it('should obtain and set logon data from WCS', async () => {
      const updateProfile = new UpdateProfile()
      sendRequestToApi.mockReturnValue({ body: 'logonData' })
      expect(updateProfile.logonData).toBeUndefined()
      await updateProfile.getWCSLogonData()
      expect(updateProfile.logonData).toBe('logonData')
    })

    it('should use the sendRequestApi function with correct parameters', async () => {
      const updateProfile = new UpdateProfile()
      updateProfile.destinationHostname = 'hostname'
      updateProfile.logonFormEndpoint = 'foo'
      updateProfile.mapLogonFormParameters = jest.fn(() => 'bar')
      updateProfile.headers = 'baz'

      expect(sendRequestToApi).not.toHaveBeenCalled()
      expect(updateProfile.mapLogonFormParameters).not.toHaveBeenCalled()

      await updateProfile.getWCSLogonData()

      expect(sendRequestToApi).toHaveBeenCalledTimes(1)
      expect(updateProfile.mapLogonFormParameters).toHaveBeenCalledTimes(1)
      expect(sendRequestToApi).toHaveBeenCalledWith(
        'hostname',
        'foo',
        'bar',
        {},
        'get',
        'baz',
        false
      )
    })
  })

  describe('mapResponseBody', () => {
    it('should throw an exception on session timeout', () => {
      const responseBody = { pageTitle: 'LogonForm' }
      const updateProfile = new UpdateProfile()

      expect(() => updateProfile.mapResponseBody(responseBody)).toThrowError(
        'wcsSessionTimeout'
      )
    })

    it('should map the response from WCS using the logonTransform function', () => {
      const updateProfile = new UpdateProfile()
      logonTransform.mockReturnValue(transformedBody)
      expect(updateProfile.mapResponseBody(responseBody)).toEqual(
        transformedBody
      )
      expect(logonTransform).toHaveBeenCalledTimes(1)
      expect(logonTransform).toHaveBeenCalledWith(responseBody, false)
    })
  })

  describe('mapResponseError', () => {
    afterEach(() => {
      jest.resetAllMocks()
    })

    it('should throw Boom.badData() if there is a serverErrorMessage value in the response', () => {
      const updateProfile = new UpdateProfile()
      Boom.badData.mockReturnValue('Bam!')
      expect(() =>
        updateProfile.mapResponseError({ serverErrorMessage: 'Error' })
      ).toThrow('Bam!')
      expect(Boom.badData).toHaveBeenCalledTimes(1)
    })

    it('should simply throw the body, and not call Boom.badData(), if there is no serverErrorMessage value in the response', () => {
      const updateProfile = new UpdateProfile()
      expect(() => updateProfile.mapResponseError('Error')).toThrow('Error')
      expect(Boom.badData).not.toHaveBeenCalled()
    })
  })

  describe('mapResponse', () => {
    afterEach(() => {
      jest.resetAllMocks()
    })

    it('should call mapResponseError with the response if the response contains a serverErrorMessage', () => {
      const updateProfile = new UpdateProfile()
      updateProfile.mapResponseError = jest.fn()
      updateProfile.mapResponse({ body: { serverErrorMessage: 'Error!' } })
      expect(updateProfile.mapResponseError).toHaveBeenCalledWith({
        serverErrorMessage: 'Error!',
      })
    })

    it('should call mapResponseBody with logonData if the body has a success value of true', async () => {
      const updateProfile = new UpdateProfile()
      updateProfile.mapResponseBody = jest.fn()
      updateProfile.mapResponse({ body: { success: true } })

      await updateProfile.getWCSLogonData()

      expect(updateProfile.mapResponseBody).toHaveBeenCalledTimes(1)
      expect(updateProfile.mapResponseBody).toHaveBeenCalledWith(
        updateProfile.logonData
      )
    })

    it('should return the response in an expected format', async () => {
      const updateProfile = new UpdateProfile()
      updateProfile.mapResponseBody = jest.fn(() => montyLogon)
      authenticatedCookies.mockReturnValue('cookie')
      expect(await updateProfile.mapResponse({ jsessionid: 'foo' })).toEqual({
        jsessionid: 'foo',
        body: montyLogon,
        setCookies: 'cookie',
        status: false,
      })
    })
  })
})
