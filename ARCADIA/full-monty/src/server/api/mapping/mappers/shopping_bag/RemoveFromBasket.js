import Mapper from '../../Mapper'
import { basketCookies } from './cookies'
import transform from '../../transforms/basket'
import Boom from 'boom'
import { path } from 'ramda'

export default class RemoveFromBasket extends Mapper {
  mapEndpoint() {
    const wcs = '/webapp/wcs/stores/servlet/'
    this.destinationEndpoint = `${wcs}OrderItemDelete`
    this.method = 'post'
  }
  mapRequestParameters() {
    const { catalogId, langId, siteId: storeId } = this.storeConfig
    const { orderItemId, orderId } = this.query
    this.payload = {
      orderItemId,
      orderId,
      updatePrices: '1',
      calculationUsageId: ['-1', '-2', '-7'],
      langId,
      storeId,
      catalogId,
      URL: 'OrderCalculate?URL=ShoppingBagRemoveItemAjaxView',
    }
    this.query = {}
  }

  mapResponseBody(body) {
    // In case of sessionTimeout property set to true we need to return an empty bag as scrApi is currently doing.
    // The property sessionTimeout is set in api.js.
    if (!body.products && !body.wcsSessionTimeout)
      throw Boom.badData(['Item was not removed'], [body])

    return transform(body, this.storeConfig.currencySymbol)
  }

  mapResponse(res) {
    return {
      jsessionid: res.jsessionid,
      body: this.mapResponseBody(res.body),
      setCookies: basketCookies(path(['products', 'Product'], res.body)),
    }
  }
}

export const removeFromBasketSpec = {
  summary: 'Remove an item from the basket',
  parameters: [
    {
      name: 'orderId',
      in: 'query',
      type: 'string',
      required: true,
    },
    {
      name: 'orderItemId',
      in: 'query',
      type: 'string',
      required: true,
    },
  ],
  responses: {
    200: {
      description: 'Shopping Basket Object',
      schema: {
        $ref: '#/definitions/basket',
      },
    },
  },
}
