import { cookieOptionsBag, cookieOptionsUnset } from '../../../../../lib/auth'

const addPromoCookies = () => [
  {
    name: 'arcpromocode',
    value: '',
    options: cookieOptionsUnset,
  },
]

const basketCookies = (products) => [
  {
    name: 'bagCount',
    value:
      products && Array.isArray(products)
        ? products.reduce((acc, { quantity }) => acc + quantity, 0).toString()
        : '0',
    options: cookieOptionsBag,
  },
]

export { addPromoCookies, basketCookies }
