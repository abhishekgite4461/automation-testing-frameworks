import Boom from 'boom'
import Mapper from '../../Mapper'
import basketTransform from '../../transforms/basket'
import { getSession } from '../../utils/sessionUtils'
import { extractCartId } from '../../utils/cartUtils'

function getOrderItemUpdateQuery(catEntryIdToAdd) {
  return {
    catEntryId: catEntryIdToAdd,
    URL: 'OrderCalculate?URL=ShoppingBagUpdateItemAjaxView',
  }
}

function getOrderItemDeleteQuery(catEntryIdToDelete, catEntryIdToAdd) {
  return {
    catEntryId: catEntryIdToDelete,
    pageName: 'shoppingBag',
    URL: `OrderItemAddAjax?catEntryId=${catEntryIdToAdd}&URL=OrderCalculate?URL=ShoppingBagUpdateItemAjaxView`,
  }
}

export default class UpdateBasketItem extends Mapper {
  constructor(...args) {
    super(...args)

    // if the user changes the item in the bag (by size etc) we first need to remove
    // the old catEntryId (catEntryIdToDelete) then add the new one (catEntryIdToAdd)
    this.shouldDeleteItemFirst =
      this.payload.catEntryIdToAdd !== this.payload.catEntryIdToDelete
  }

  mapEndpoint() {
    this.destinationEndpoint = this.shouldDeleteItemFirst
      ? '/webapp/wcs/stores/servlet/OrderItemDelete'
      : '/webapp/wcs/stores/servlet/OrderItemUpdateAjax'
    this.method = 'post'
  }

  mapRequestParameters() {
    const queryDefaults = {
      orderId: this.orderId,
      updatePrices: 1,
      calculationUsageId: -1,
      langId: this.storeConfig.langId,
      storeId: this.storeConfig.siteId,
      catalogId: this.storeConfig.catalogId,
      calculateOrder: 1,
      quantity: this.payload.quantity,
      sourcePage: 'PaymentDetails',
      isSizeUpdate: true,
    }

    if (this.shouldDeleteItemFirst) {
      this.query = {
        ...queryDefaults,
        ...getOrderItemDeleteQuery(
          this.payload.catEntryIdToDelete,
          this.payload.catEntryIdToAdd,
          this.payload.quantity
        ),
      }
    } else {
      this.query = {
        ...queryDefaults,
        ...getOrderItemUpdateQuery(this.payload.catEntryIdToAdd),
        getOrderItemFromCookie: true,
      }
    }

    this.payload = {}
  }

  mapResponseBody(body) {
    return basketTransform(body, this.storeConfig.currencySymbol)
  }

  mapResponseError(error) {
    if (error.message === 'wcsSessionTimeout') throw error

    throw Boom.internal(error)
  }

  async execute() {
    const session = await getSession(this.headers.cookie)
    this.orderId = await extractCartId(session.cookies)

    return super.execute()
  }
}

export const updateItemSpec = {
  summary: "Update an item's quantity, size or colour",
  parameters: [
    {
      name: 'payload',
      in: 'body',
      required: true,
      schema: {
        type: 'object',
        properties: {
          catEntryIdToAdd: {
            type: 'integer',
            format: 'int32',
          },
          catEntryIdToDelete: {
            type: 'integer',
            format: 'int32',
          },
          quantity: {
            type: 'string',
          },
        },
      },
    },
  ],
  responses: {
    200: {
      description: 'Shopping Basket Object',
      schema: {
        $ref: '#/definitions/basket',
      },
    },
  },
}
