import * as utils from '../../../__test__/utils'
import DeletePromotionCode from '../DeletePromotionCode'
import wcsResp from '../../../../../../../test/apiResponses/shopping-bag/delete-promo-code/wcs.json'
import hapiMontyResp from '../../../../../../../test/apiResponses/shopping-bag/delete-promo-code/hapiMonty.json'

describe('DeletePromotionCode', () => {
  const orderId = '123'
  beforeEach(() => {
    jest.resetModules()
    jest.clearAllMocks()

    utils.setUserSession({
      cookies: [`cartId=${orderId};`],
    })
    utils.setWCSResponse({ body: wcsResp })
  })
  const input = {
    endpoint: '/foo',
    query: '',
    payload: {
      promotionCode: 'TSCARD1',
    },
    method: 'DELETE',
    headers: {
      cookie: 'jsessionid=1;',
    },
    params: {},
  }

  const execute = utils.buildExecutor(DeletePromotionCode, input)

  it('should map the request', async () => {
    await execute()

    utils.expectRequestMadeWith({
      endpoint: '/webapp/wcs/stores/servlet/PromotionCodeManage',
      query: '',
      payload: {
        orderId,
        actionType: 'verify_promo',
        updatePrices: 1,
        calculationUsageId: -1,
        langId: '-1',
        storeId: 12556,
        catalogId: '33057',
        promoCode: input.payload.promotionCode,
        calculateOrder: 1,
        sourcePage: 'OrderItemDisplay',
        URL: 'OrderCalculate?URL=OrderPrepare?URL=PromotionCodeAjaxView',
        errorViewName: 'PromotionCodeAjaxView',
        taskType: 'R',
      },
      method: 'post',
      headers: input.headers,
      jsessionid: null,
      hostname: false,
    })
  })

  it('should map the response', async () => {
    const resp = await execute()
    expect(resp.body).toEqual(hapiMontyResp)
  })
})
