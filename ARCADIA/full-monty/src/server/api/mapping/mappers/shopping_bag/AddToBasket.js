import Mapper from '../../Mapper'
import { sendRequestToApi } from '../../../api'
import transform from '../../transforms/basket'
import { basketCookies } from './cookies'
import Boom from 'boom'
import {
  basketConstants,
  addToBasketConstants,
} from '../../constants/basketConstants'
import {
  extractParametersFromProduct,
  extractParametersFromBundle,
} from './utils'
import { path } from 'ramda'

export default class AddToBasket extends Mapper {
  mapEndpoint() {
    const wcs = '/webapp/wcs/stores/servlet/'
    this.destinationEndpoint = `${wcs}NewProductDetailsActionControl`
    this.basketEndpoint = `${wcs}OrderCalculate`
    this.productEndpoint = `${wcs}ProductDisplay`
  }
  mapRequestParameters(product) {
    const { catalogId, langId, siteId: storeId } = this.storeConfig
    const { productId, sku, quantity, bundleItems } = this.payload
    const { isDDPProduct } = product
    this.payload = {
      catalogId,
      langId,
      storeId,
      ...addToBasketConstants,
    }
    if (!bundleItems) {
      this.payload = {
        ...this.payload,
        quantity,
        catEntryId: productId,
        ...(isDDPProduct ? {} : extractParametersFromProduct(product, sku)),
      }
    } else {
      this.payload = {
        ...this.payload,
        productId,
        quantity: 1,
        isBundle: 'true',
        ...extractParametersFromBundle(product, bundleItems),
      }
    }
  }
  mapBasketParameters() {
    const { catalogId, langId, siteId: storeId } = this.storeConfig
    return {
      catalogId,
      langId,
      storeId,
      ...basketConstants,
    }
  }
  mapProductParameters(productId) {
    const { catalogId, langId, siteId: storeId } = this.storeConfig
    return {
      catalogId,
      langId,
      storeId,
      productId,
    }
  }
  getProduct() {
    const { productId } = this.payload
    if (productId) {
      return sendRequestToApi(
        this.destinationHostname,
        this.productEndpoint,
        this.mapProductParameters(productId),
        {},
        'get',
        this.headers
      )
    }
    return {}
  }
  mapResponseBody(body = {}) {
    return transform(body.Basket, this.storeConfig.currencySymbol)
  }
  mapResponse(res) {
    if (res.body && res.body.success === false) throw res
    return {
      jsessionid: res.jsessionid,
      body: this.mapResponseBody(res.body),
      setCookies: basketCookies(
        path(['Basket', 'products', 'Product'], res.body)
      ),
    }
  }
  mapResponseError(res = {}) {
    throw res.body && res.body.success === false
      ? Boom.badData(
          [res.body.message || res.body.errorMessage || res.body.errorMessage2],
          [res]
        )
      : res
  }
  async addToBasket(jsesssionid = false) {
    const result = await sendRequestToApi(
      this.destinationHostname,
      this.destinationEndpoint,
      {},
      this.payload,
      'post',
      this.headers,
      jsesssionid
    )

    return result
  }

  async execute() {
    this.mapEndpoint()
    const product = await this.getProduct()

    // This could be the same or different than the one provided by the Client in the scenario where the User Session is
    // expired and WCS provides a new one.
    const jsessionidFromGetProduct = product.jsessionid || false

    if (!product.body || product.body.success === false) {
      return this.mapResponseError(product)
    }

    this.mapRequestParameters(product.body)

    const result = await this.addToBasket(jsessionidFromGetProduct)

    if (result.body && result.body.success !== true) {
      return this.mapResponseError(result)
    }

    // In order to handle session expiry scenario where WCS provides a new jsessionid in the response, for the next request in the chain of requests to WCS
    // we will use the jsessionid provided in the response and not the one coming from the Client request.
    const jsessionidFromAddToBasket = result.jsessionid || false

    return sendRequestToApi(
      this.destinationHostname,
      this.basketEndpoint,
      this.mapBasketParameters(),
      {},
      'get',
      this.headers,
      jsessionidFromAddToBasket
    )
      .then((res) => {
        return this.mapResponse(res)
      })
      .catch((apiResponseError) => {
        return this.mapResponseError(apiResponseError)
      })
  }
}

export const addToBasketSpec = {
  summary: 'Add a product (SKU) to the basket',
  parameters: [
    {
      name: 'payload',
      in: 'body',
      required: true,
      schema: {
        type: 'object',
        properties: {
          productId: {
            type: 'number',
          },
          quantity: {
            type: 'integer',
            format: 'int32',
            minimum: 1,
          },
          sku: {
            type: 'string',
          },
        },
      },
    },
  ],
  responses: {
    200: {
      description: 'Shopping Basket Object',
      schema: {
        $ref: '#/definitions/basket',
      },
    },
  },
}
