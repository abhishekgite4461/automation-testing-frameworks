import Mapper from '../../Mapper'
import { path } from 'ramda'
import basketTransform from '../../transforms/basket'
import { getOrderId } from '../../utils/cartUtils'

export default class DeletePromotionCode extends Mapper {
  mapEndpoint() {
    this.destinationEndpoint = '/webapp/wcs/stores/servlet/PromotionCodeManage'
  }

  mapRequestParameters() {
    this.method = 'post'
    this.payload = {
      orderId: this.orderId,
      actionType: 'verify_promo',
      updatePrices: 1,
      calculationUsageId: -1,
      langId: this.storeConfig.langId,
      storeId: this.storeConfig.siteId,
      catalogId: this.storeConfig.catalogId,
      promoCode: this.payload.promotionCode,
      calculateOrder: 1,
      sourcePage: 'OrderItemDisplay',
      URL: 'OrderCalculate?URL=OrderPrepare?URL=PromotionCodeAjaxView',
      errorViewName: 'PromotionCodeAjaxView',
      taskType: 'R',
    }
  }

  mapResponseBody(body) {
    return basketTransform(body, path(['currencySymbol'], this.storeConfig))
  }

  async execute() {
    // For the time being we need to get the order id from redis
    // because atm we cannot update monty client to send it
    this.orderId = await getOrderId(this.headers.cookie)
    return super.execute()
  }
}

export const deletePromoSpec = {
  summary: 'Delete a promotion code',
  parameters: [
    {
      name: 'payload',
      in: 'body',
      required: true,
      schema: {
        type: 'object',
        required: ['promotionCode'],
        properties: {
          promotionCode: {
            type: 'string',
          },
        },
      },
    },
  ],
  responses: {
    200: {
      description: 'Shopping Basket Object',
      schema: {
        $ref: '#/definitions/basket',
      },
    },
  },
}
