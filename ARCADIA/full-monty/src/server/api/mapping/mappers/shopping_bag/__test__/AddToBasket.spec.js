import AddToBasket from '../AddToBasket'
import wcsPdpResponse from '../../../../../../../test/apiResponses/pdp/wcs.json'
import wcsPdpResponseFixedBundle from '../../../../../../../test/apiResponses/pdp/bundles/fixed/wcs_fixed_bundle.json'
import { cookieOptionsBag } from '../../../../../lib/auth'
import { omit } from 'ramda'

import * as utils from '../utils'

jest.mock('../../../Mapper')

jest.mock('../../../../api', () => ({ sendRequestToApi: jest.fn() }))
import { sendRequestToApi } from '../../../../api'

jest.mock('../../../transforms/basket')
import transform from '../../../transforms/basket'

jest.mock('boom', () => ({
  badData: jest.fn(),
}))
import Boom from 'boom'

const wcs = '/webapp/wcs/stores/servlet/'

const storeConfig = {
  catalogId: 33057,
  langId: '-1',
  siteId: 12556,
}

const payloadFromMonty = {
  productId: 29099635,
  sku: '602016000997332',
  quantity: 1,
}

const payloadFromMontyBundle = {
  productId: 28402147,
  bundleItems: [
    { productId: 28361263, sku: '602017001099935' },
    { productId: 28359754, sku: '602017001099797' },
  ],
}

const payloadToWcs = {
  add2cartErrorViewName: 'Add2ShopCartResponseView',
  Add2ShopCart: 'true',
  quantity: 1,
  updatePrices: '1',
  calculationUsageId: '-1',
  catEntryId: 29099635,
  errorViewName: 'ProductDisplayErrorView',
  langId: '-1',
  storeId: 12556,
  catalogId: 33057,
  result: 'ADDED_TO_BAG',
  orderId: '.',
  attrName: ['134315349', '134315353'],
  attrValue: ['KHAKI', '8'],
  shouldCachePage: 'false',
  URL: 'ProductDisplay',
  lowStockThreshold: 1,
}

const payloadToWcsBundle = {
  add2cartErrorViewName: 'Add2ShopCartResponseView',
  Add2ShopCart: 'true',
  quantity: 1,
  updatePrices: '1',
  calculationUsageId: '-1',
  errorViewName: 'ProductDisplayErrorView',
  langId: '-1',
  storeId: 12556,
  catalogId: 33057,
  result: 'ADDED_TO_BAG',
  orderId: '.',
  isBundle: 'true',
  productId: 28402147,
  slot_1: '1',
  catEntryId_1: 28361263,
  attrName_1: ['156889348', '156889350'],
  attrValue_1: ['PEACH', '10'],
  quantity_1: 1,
  slot_2: '2',
  catEntryId_2: 28359754,
  attrName_2: ['156872440', '156872441'],
  attrValue_2: ['PEACH', '4'],
  quantity_2: 1,
  shouldCachePage: 'false',
  URL: 'ProductDisplay',
  lowStockThreshold: 1,
}

const basketParameters = {
  langId: '-1',
  storeId: 12556,
  catalogId: 33057,
  updatePrices: 1,
  calculationUsageId: [-1, -2, -7],
  calculateOrder: 1,
  orderId: '.',
  URL: 'OrderItemDisplay',
}

const productParameters = {
  langId: '-1',
  storeId: 12556,
  catalogId: 33057,
  productId: 29099635,
}

const productResponse = {
  productData: {
    items: [
      {
        size: '4',
        sku: '602016000997330',
        quantity: 10,
        skuid: 25770118,
        selected: false,
      },
      {
        size: '6',
        sku: '602017001130582',
        quantity: 9,
        skuid: 25770153,
        selected: false,
      },
      {
        size: '8',
        sku: '602016000997332',
        quantity: 10,
        skuid: 25770145,
        selected: false,
      },
    ],
  },
}

const addToBagResponse = {
  success: true,
  message: 'Item(s) successfully added to your shopping bag.',
  items: 7,
  total: 83.0,
}

const addToBagFailResponse = {
  success: false,
  message:
    'Please select a size and/or quantity you require before adding it to your bag.',
  items: 7,
  total: 83.0,
}

const product = {
  id: 'product',
}

const wcsBody = {
  Basket: {
    products: {
      Product: [{ quantity: 1 }, { quantity: 3 }],
    },
  },
}

const transformedBody = { body: 'I am a transformed basket body for Monty' }

describe('AddToBasket', () => {
  beforeEach(() => {
    jest.resetAllMocks()
  })
  describe('mapEndpoint', () => {
    it('should set the destinationEndpoint to /webapp/wcs/stores/servlet/NewProductDetailsActionControl', () => {
      const addToBasket = new AddToBasket()
      expect(addToBasket.destinationEndpoint).toBeUndefined()
      addToBasket.mapEndpoint()
      expect(addToBasket.destinationEndpoint).toBe(
        `${wcs}NewProductDetailsActionControl`
      )
    })
    it('sets the productEndpoint to /webapp/wcs/stores/servlet/ProductDisplay', () => {
      const addToBasket = new AddToBasket()
      expect(addToBasket.productEndpoint).toBeUndefined()
      addToBasket.mapEndpoint()
      expect(addToBasket.productEndpoint).toBe(`${wcs}ProductDisplay`)
    })
    it('sets the basketEndpoint to /webapp/wcs/stores/servlet/OrderCalculate', () => {
      const addToBasket = new AddToBasket()
      expect(addToBasket.basketEndpoint).toBeUndefined()
      addToBasket.mapEndpoint()
      expect(addToBasket.basketEndpoint).toBe(`${wcs}OrderCalculate`)
    })
  })
  describe('mapRequestParameters', () => {
    it('should set the payload to a format expected by WCS for a product', () => {
      const addToBasket = new AddToBasket()
      addToBasket.storeConfig = storeConfig
      addToBasket.payload = payloadFromMonty
      addToBasket.mapRequestParameters(wcsPdpResponse)
      expect(addToBasket.payload).toEqual(payloadToWcs)
    })
    it('should set the payload to a format expected by WCS for a bundle', () => {
      const addToBasket = new AddToBasket()
      addToBasket.storeConfig = storeConfig
      addToBasket.payload = payloadFromMontyBundle
      addToBasket.mapRequestParameters(wcsPdpResponseFixedBundle)
      expect(addToBasket.payload).toEqual(payloadToWcsBundle)
    })
    it('should not call extractParametersFromProduct for DDP Product', () => {
      const spy = jest.spyOn(utils, 'extractParametersFromProduct')
      const addToBasket = new AddToBasket()
      const wcsDDPResponse = {
        ...wcsPdpResponse,
        isDDPProduct: true,
      }

      addToBasket.storeConfig = storeConfig
      addToBasket.payload = payloadFromMonty
      addToBasket.mapRequestParameters(wcsDDPResponse)

      expect(spy).not.toHaveBeenCalled()
      expect(addToBasket.payload).toEqual(
        omit(['attrName', 'attrValue'], payloadToWcs)
      )

      spy.mockRestore()
    })
  })
  describe('mapBasketParameters', () => {
    it('should return the correct parameters for the Basket endpoint', () => {
      const addToBasket = new AddToBasket()
      addToBasket.storeConfig = storeConfig
      expect(addToBasket.mapBasketParameters()).toEqual(basketParameters)
    })
  })
  describe('mapProductParameters', () => {
    it('should return the correct parameters for the Basket endpoint', () => {
      const addToBasket = new AddToBasket()
      addToBasket.storeConfig = storeConfig
      expect(addToBasket.mapProductParameters(29099635)).toEqual(
        productParameters
      )
    })
  })
  describe('getProduct', () => {
    it('should obtain and return the catEntryId for the product', async () => {
      const addToBasket = new AddToBasket()
      sendRequestToApi.mockReturnValue(productResponse)
      addToBasket.mapProductParameters = jest.fn()
      addToBasket.payload = { productId: 29099635 }
      expect(await addToBasket.getProduct(29099635)).toBe(productResponse)
    })
    it('should call mapProductParameters with the productId', async () => {
      const addToBasket = new AddToBasket()
      sendRequestToApi.mockReturnValue({})
      addToBasket.mapProductParameters = jest.fn()
      addToBasket.payload = { productId: 29099635 }
      expect(addToBasket.mapProductParameters).not.toHaveBeenCalled()
      await addToBasket.getProduct()
      expect(addToBasket.mapProductParameters).toHaveBeenCalledTimes(1)
      expect(addToBasket.mapProductParameters).toHaveBeenCalledWith(29099635)
    })
    it('should use the sendRequestApi function with correct parameters', async () => {
      const addToBasket = new AddToBasket()
      addToBasket.destinationHostname = 'hostname'
      addToBasket.productEndpoint = 'foo'
      addToBasket.mapProductParameters = jest.fn(() => 'bar')
      addToBasket.headers = 'baz'
      addToBasket.payload = { productId: 29099635 }
      expect(sendRequestToApi).not.toHaveBeenCalled()
      expect(addToBasket.mapProductParameters).not.toHaveBeenCalled()
      await addToBasket.getProduct()
      expect(sendRequestToApi).toHaveBeenCalledTimes(1)
      expect(addToBasket.mapProductParameters).toHaveBeenCalledTimes(1)
      expect(sendRequestToApi).toHaveBeenCalledWith(
        'hostname',
        'foo',
        'bar',
        {},
        'get',
        'baz'
      )
    })
  })
  describe('addToBasket', () => {
    it('should add the item to the basket and return the response', async () => {
      const addToBasket = new AddToBasket()
      sendRequestToApi.mockReturnValue(addToBagResponse)
      expect(await addToBasket.addToBasket()).toBe(addToBagResponse)
    })
    it('should use the sendRequestApi function with correct parameters', async () => {
      const addToBasket = new AddToBasket()
      addToBasket.destinationHostname = 'hostname'
      addToBasket.destinationEndpoint = 'foo'
      addToBasket.headers = 'baz'
      addToBasket.payload = payloadToWcs

      expect(sendRequestToApi).not.toHaveBeenCalled()

      await addToBasket.addToBasket()

      expect(sendRequestToApi).toHaveBeenCalledTimes(1)
      expect(sendRequestToApi).toHaveBeenCalledWith(
        'hostname',
        'foo',
        {},
        payloadToWcs,
        'post',
        'baz',
        false
      )
    })
  })
  describe('execute', () => {
    const addMocks = (addToBasket) => {
      addToBasket.mapEndpoint = jest.fn()
      addToBasket.getProduct = jest.fn()
      addToBasket.addToBasket = jest.fn()
      addToBasket.mapBasketParameters = jest.fn()
      addToBasket.mapRequestParameters = jest.fn()
      addToBasket.mapResponse = jest.fn()
      addToBasket.mapResponseError = jest.fn()
      addToBasket.getProduct.mockReturnValue({ body: 'body' })
      addToBasket.addToBasket.mockReturnValue({ body: addToBagResponse })
    }
    it('calls mapRequestParameters', async () => {
      sendRequestToApi.mockReturnValue(Promise.resolve({}))
      const addToBasket = new AddToBasket()
      addMocks(addToBasket)
      addToBasket.getProduct.mockReturnValue(Promise.resolve({ body: product }))
      expect(addToBasket.mapRequestParameters).not.toHaveBeenCalled()
      await addToBasket.execute()
      expect(addToBasket.mapRequestParameters).toHaveBeenCalledTimes(1)
      expect(addToBasket.mapRequestParameters).toHaveBeenCalledWith(product)
    })
    it('calls mapEndpoint', async () => {
      sendRequestToApi.mockReturnValue(Promise.resolve({}))
      const addToBasket = new AddToBasket()
      addMocks(addToBasket)

      expect(addToBasket.mapEndpoint).not.toHaveBeenCalled()
      await addToBasket.execute()
      expect(addToBasket.mapEndpoint).toHaveBeenCalledTimes(1)
      expect(addToBasket.mapEndpoint).toHaveBeenCalledWith()
    })
    it('calls getProduct', async () => {
      sendRequestToApi.mockReturnValue(Promise.resolve({}))
      const addToBasket = new AddToBasket()
      addMocks(addToBasket)

      expect(addToBasket.getProduct).not.toHaveBeenCalled()
      await addToBasket.execute()
      expect(addToBasket.getProduct).toHaveBeenCalledTimes(1)
      expect(addToBasket.getProduct).toHaveBeenCalledWith()
    })
    it('calls addToBasket to add product to the basket', async () => {
      sendRequestToApi.mockReturnValue(Promise.resolve({}))

      const addToBasket = new AddToBasket()

      addMocks(addToBasket)

      expect(addToBasket.addToBasket).not.toHaveBeenCalled()

      await addToBasket.execute()

      expect(addToBasket.addToBasket).toHaveBeenCalledTimes(1)
      expect(addToBasket.addToBasket).toHaveBeenCalledWith(false)
    })
    it('calls sendRequestToApi to get the basket', async () => {
      sendRequestToApi.mockReturnValue(Promise.resolve({}))
      const addToBasket = new AddToBasket()
      addMocks(addToBasket)

      addToBasket.destinationHostname = 'hostname'
      addToBasket.basketEndpoint = 'basket'
      addToBasket.mapBasketParameters.mockReturnValue({ query: 'yes' })
      addToBasket.headers = { headers: 'value' }

      expect(sendRequestToApi).not.toHaveBeenCalled()

      await addToBasket.execute()

      expect(sendRequestToApi).toHaveBeenCalledTimes(1)
      expect(sendRequestToApi).toHaveBeenCalledWith(
        'hostname',
        'basket',
        { query: 'yes' },
        {},
        'get',
        { headers: 'value' },
        false
      )
    })
    it('calls mapResponse if an item is added to the basket correctly', async () => {
      sendRequestToApi.mockReturnValue(Promise.resolve({ value: '1243' }))
      const addToBasket = new AddToBasket()
      addMocks(addToBasket)

      expect(addToBasket.mapResponse).not.toHaveBeenCalled()
      await addToBasket.execute()
      expect(addToBasket.mapResponse).toHaveBeenCalledTimes(1)
      expect(addToBasket.mapResponse).toHaveBeenCalledWith({ value: '1243' })
    })
    it('calls mapResponseError if an item is not added to the basket correctly', async () => {
      sendRequestToApi.mockReturnValue(Promise.resolve({}))
      const addToBasket = new AddToBasket()
      addMocks(addToBasket)
      addToBasket.addToBasket.mockReturnValue({ body: addToBagFailResponse })
      expect(addToBasket.mapResponseError).not.toHaveBeenCalled()
      await addToBasket.execute()
      expect(addToBasket.mapResponseError).toHaveBeenCalledTimes(1)
      expect(addToBasket.mapResponseError).toHaveBeenCalledWith({
        body: addToBagFailResponse,
      })
    })
    it('returns the basket if successful', () => {
      sendRequestToApi.mockReturnValue(Promise.resolve({}))
      const addToBasket = new AddToBasket()
      addMocks(addToBasket)
      addToBasket.mapResponse.mockReturnValue('sucess')
      return expect(addToBasket.execute()).resolves.toBe('sucess')
    })
  })
  describe('mapResponseBody', () => {
    it('should use the basket transform function', () => {
      transform.mockReturnValue(transformedBody)
      const addToBasket = new AddToBasket()
      addToBasket.storeConfig = {
        currencySymbol: '£',
      }
      expect(addToBasket.mapResponseBody(wcsBody)).toEqual(transformedBody)
      expect(transform).toHaveBeenCalledTimes(1)
    })
  })
  describe('mapResponse', () => {
    it('should call mapResponseBody with the response body', () => {
      const addToBasket = new AddToBasket()
      addToBasket.mapResponseBody = jest.fn()
      addToBasket.mapResponseBody.mockReturnValue(transformedBody)
      addToBasket.mapResponse({ jsessionid: 123, body: wcsBody })
      expect(addToBasket.mapResponseBody).toHaveBeenCalledTimes(1)
      expect(addToBasket.mapResponseBody).toHaveBeenCalledWith(wcsBody)
    })
    it('return a transformed resoponse', () => {
      const addToBasket = new AddToBasket()
      addToBasket.mapResponseBody = jest.fn()
      addToBasket.mapResponseBody.mockReturnValue(transformedBody)
      expect(
        addToBasket.mapResponse({ jsessionid: 123, body: wcsBody })
      ).toEqual({
        jsessionid: 123,
        body: transformedBody,
        setCookies: [
          {
            name: 'bagCount',
            value: '4',
            options: cookieOptionsBag,
          },
        ],
      })
    })
    it('throw the response as an error if it is a false response', () => {
      const addToBasket = new AddToBasket()
      const errorBody = { body: { success: false } }
      expect(() => {
        addToBasket.mapResponse(errorBody)
      }).toThrow(new Error(errorBody))
    })
  })
  describe('mapResponseError', () => {
    it('should throw any error passed to it that is not recognised', () => {
      const addToBasket = new AddToBasket()
      expect(() => {
        addToBasket.mapResponseError()
      }).toThrow(undefined)
      expect(() => {
        addToBasket.mapResponseError({})
      }).toThrow(new Error({}))
    })
    it('should throw a Boom bad data error if the wcs response has an error', () => {
      Boom.badData.mockReturnValue('BAM!')
      const addToBasket = new AddToBasket()
      expect(() => {
        addToBasket.mapResponseError({ body: addToBagFailResponse })
      }).toThrow('BAM!')
    })
  })
})
