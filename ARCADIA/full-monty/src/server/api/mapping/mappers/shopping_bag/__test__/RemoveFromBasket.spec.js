import * as utils from '../../../__test__/utils'
import RemoveFromBasket from '../RemoveFromBasket'

jest.mock('boom', () => ({
  badData: jest.fn(),
}))
import Boom from 'boom'

jest.mock('../../../transforms/basket')
import transform from '../../../transforms/basket'

import { cookieOptionsBag } from '../../../../../lib/auth'

const orderId = 1822719
const orderItemId = 8235446
const deleteItemResponseWcs = {
  products: {
    Product: [{ quantity: 1 }, { quantity: 3 }],
  },
}

const basketResponseMonty = { basketMonty: {} }
const jsessionid = '12345'
const cookiesToSet = [
  {
    name: 'bagCount',
    value: '4',
    options: cookieOptionsBag,
  },
]

const montyRequest = {
  originEndpoint: '/api/shopping_bag/delete_item',
  query: {
    orderId,
    orderItemId,
  },
  payload: {},
  method: 'delete',
  headers: {},
  params: {},
}

const wcsPayload = {
  orderItemId,
  orderId,
  updatePrices: '1',
  calculationUsageId: ['-1', '-2', '-7'],
  langId: '-1',
  storeId: 12556,
  catalogId: '33057',
  URL: 'OrderCalculate?URL=ShoppingBagRemoveItemAjaxView',
}

const execute = utils.buildExecutor(RemoveFromBasket, montyRequest)

const happyApi = () => {
  utils.setWCSResponse({ body: deleteItemResponseWcs, jsessionid })
}

describe('RemoveFromBasket', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })
  describe('calling the wcs endpoint for deleting an item from the basket', () => {
    it('should call the endpoint /webapp/wcs/stores/servlet/OrderItemDelete', async () => {
      happyApi()
      await execute()
      expect(utils.getRequestArgs(0).endpoint).toBe(
        '/webapp/wcs/stores/servlet/OrderItemDelete'
      )
    })
    it('should call with the post method', async () => {
      happyApi()
      await execute()
      expect(utils.getRequestArgs(0).method).toBe('post')
    })
    it('should call with the correct payload', async () => {
      happyApi()
      await execute()
      expect(utils.getRequestArgs(0).payload).toEqual(wcsPayload)
    })
    describe('if the response is not expected', () => {
      it('should throw an error if the product property is not present', async () => {
        utils.setWCSResponse({ body: {} })
        Boom.badData.mockReturnValue('BAM!')
        return expect(execute()).rejects.toBe('BAM!')
      })
    })
  })
  describe('response', () => {
    beforeEach(() => {
      happyApi()
    })
    it('should contain the basket response in the body', async () => {
      transform.mockReturnValue(basketResponseMonty)
      const result = await execute()
      expect(result.body).toEqual(basketResponseMonty)
    })
    it('should contain the basket count cookie to set', async () => {
      transform.mockReturnValue(basketResponseMonty)
      const result = await execute()
      expect(result.setCookies).toEqual(cookiesToSet)
    })
    it('should contain the jsessionid', async () => {
      transform.mockReturnValue(basketResponseMonty)
      const result = await execute()
      expect(result.jsessionid).toBe(jsessionid)
    })
  })
})
