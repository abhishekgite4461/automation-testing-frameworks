import AddPromo from '../AddPromo'
import { cookieOptionsUnset } from '../../../../../lib/auth'

import * as cartUtils from '../../../utils/cartUtils'

jest.mock('boom', () => ({
  badData: jest.fn(),
}))
import Boom from 'boom'

import { addPromoConstants } from '../../../constants/addPromo'

jest.mock('../../../../api', () => ({
  sendRequestToApi: jest.fn(),
}))
import { sendRequestToApi } from '../../../../api'

const payloadToWCS = {
  ...addPromoConstants,
  catalogId: '33057',
  langId: '-1',
  storeId: 12556,
  promoCode: 'TSUK10',
  orderId: 'order123',
}

const cookies = ['cookie1=foobar;', 'cartId=123456;', 'anotherCookie=batbaz;']

const cookiesToSet = [
  {
    name: 'arcpromocode',
    value: '',
    options: cookieOptionsUnset,
  },
]

describe('AddPromo Mapper', () => {
  describe('mapEndpoint', () => {
    it('should set the destinationEndpoint to /webapp/wcs/stores/servlet/PromotionCodeManage', () => {
      const addPromo = new AddPromo()
      expect(addPromo.destinationEndpoint).toBeUndefined()
      addPromo.mapEndpoint()
      expect(addPromo.destinationEndpoint).toBe(
        '/webapp/wcs/stores/servlet/PromotionCodeManage'
      )
    })
  })

  describe('mapRequestParameters', () => {
    it('should set the payload to a format expected by WCS', async () => {
      const addPromo = new AddPromo(
        {},
        {},
        { promotionId: 'TSUK10' },
        'post',
        {},
        {}
      )
      cartUtils.getOrderId = jest.fn(() => 'order123')
      await addPromo.mapRequestParameters()
      expect(addPromo.payload).toEqual(payloadToWCS)
    })
  })

  describe('mapResponseError', () => {
    afterEach(() => {
      jest.resetAllMocks()
    })
    it('should throw Boom.badData if the error response has a "message" value', () => {
      const addPromo = new AddPromo()
      Boom.badData.mockReturnValue('Error')
      expect(() =>
        addPromo.mapResponseError({
          success: false,
          message: 'Bad promo code',
        })
      ).toThrow('Error')
      expect(Boom.badData).toHaveBeenCalledTimes(1)
    })

    it('should not call Boom.badData if the response does not contain a "message" value', () => {
      const addPromo = new AddPromo('', {}, {}, 'post', {})
      expect(() =>
        addPromo
          .mapResponseError({
            success: false,
            errorMessage3: 'Error',
          })
          .toThrow()
      )
      expect(Boom.badData).not.toHaveBeenCalled()
    })
  })

  describe('mapResponse', () => {
    it('should call mapResponseBody if the success value of the response is true', () => {
      const addPromo = new AddPromo()
      addPromo.mapResponseBody = jest.fn(() => 'Transformed Body')
      expect(
        addPromo.mapResponse({ body: { success: true }, jsessionid: '1234' })
      ).toEqual({
        jsessionid: '1234',
        body: 'Transformed Body',
        setCookies: cookiesToSet,
      })
      expect(addPromo.mapResponseBody).toHaveBeenCalledTimes(1)
      expect(addPromo.mapResponseBody).toHaveBeenCalledWith({ success: true })
    })
  })

  describe('execute', () => {
    afterEach(() => {
      jest.resetAllMocks()
    })
    it('should call mapResponseError if the promo code endpoint returns a value that is false', async () => {
      const addPromo = new AddPromo(
        {},
        {},
        {},
        'post',
        { jsessionid: '12345', cookie: cookies },
        {}
      )
      addPromo.mapResponseError = jest.fn()
      sendRequestToApi.mockReturnValue({})
      await addPromo.execute()
      await expect(addPromo.mapResponseError).toHaveBeenCalledTimes(1)
    })

    it('should make two requests to WCS, and should get the basket from WCS on the second request', async () => {
      const addPromo = new AddPromo({}, {}, {}, 'post', { cookie: cookies }, {})
      addPromo.destinationHostname = 'hostname'

      addPromo.mapRequestParameters = jest.fn(() =>
        Promise.resolve(payloadToWCS)
      )
      sendRequestToApi.mockReturnValue(
        Promise.resolve({ body: { success: true } })
      )
      await addPromo.execute()
      expect(sendRequestToApi).toHaveBeenCalledTimes(2)
      expect(sendRequestToApi.mock.calls[0]).toEqual([
        addPromo.destinationHostname,
        addPromo.destinationEndpoint,
        addPromo.query,
        addPromo.payload,
        addPromo.method,
        addPromo.headers,
      ])
      expect(sendRequestToApi.mock.calls[1]).toEqual([
        addPromo.destinationHostname,
        addPromo.basketEndpoint,
        addPromo.basketQuery,
        {},
        'get',
        addPromo.headers,
      ])
    })

    it('should call mapResponse with the response from the basket call', async () => {
      const addPromo = new AddPromo({}, {}, {}, 'post', { cookie: cookies }, {})
      addPromo.mapRequestParameters = jest.fn(() =>
        Promise.resolve(payloadToWCS)
      )
      addPromo.mapResponse = jest.fn()
      sendRequestToApi.mockReturnValue(
        Promise.resolve({ body: { success: true } })
      )
      await addPromo.execute()
      expect(addPromo.mapResponse).toHaveBeenCalledTimes(1)
      expect(addPromo.mapResponse).toHaveBeenCalledWith({
        body: { success: true },
      })
    })

    it('should call mapResponseError if there is an error with the request to WCS', async () => {
      const addPromo = new AddPromo({}, {}, {}, 'post', {}, {})
      addPromo.mapRequestParameters = jest.fn(() =>
        Promise.resolve(payloadToWCS)
      )
      addPromo.mapResponseError = jest.fn(() => Promise.reject('error'))
      sendRequestToApi.mockReturnValue(Promise.reject('error'))
      await expect(addPromo.execute()).rejects.toBe('error')
      expect(addPromo.mapResponseError).toHaveBeenCalledTimes(1)
      expect(addPromo.mapResponseError).toHaveBeenCalledWith('error')
    })

    it('should return the expected result', async () => {
      const addPromo = new AddPromo(
        {},
        {},
        { promotionId: 'TSUK10' },
        'post',
        { cookie: cookies },
        {}
      )
      addPromo.mapRequestParameters = jest.fn(() =>
        Promise.resolve(payloadToWCS)
      )
      sendRequestToApi
        .mockReturnValueOnce(Promise.resolve({ body: { success: true } }))
        .mockReturnValueOnce(Promise.resolve({ body: 'Basket' }))
      addPromo.mapResponse = jest.fn(() => Promise.resolve('Basket response'))
      await expect(addPromo.execute()).resolves.toEqual('Basket response')
      expect(addPromo.mapResponse).toHaveBeenCalledTimes(1)
      expect(addPromo.mapResponse).toHaveBeenCalledWith({ body: 'Basket' })
    })
  })
})
