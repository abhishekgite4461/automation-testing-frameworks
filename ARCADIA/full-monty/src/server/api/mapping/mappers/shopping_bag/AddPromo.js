import Boom from 'boom'
import Mapper from '../../Mapper'
import { sendRequestToApi } from '../../../api'
import { getOrderId } from '../../utils/cartUtils'
import { addPromoConstants } from '../../constants/addPromo'
import { basketConstants } from '../../constants/basketConstants'
import transform from '../../transforms/basket'
import { addPromoCookies } from './cookies'

export default class AddPromo extends Mapper {
  mapEndpoint() {
    this.destinationEndpoint = '/webapp/wcs/stores/servlet/PromotionCodeManage'
    this.basketEndpoint = '/webapp/wcs/stores/servlet/OrderCalculate'
  }

  async mapRequestParameters() {
    const { catalogId, langId, siteId: storeId } = this.storeConfig
    this.basketQuery = {
      ...basketConstants,
      catalogId,
      langId,
      storeId,
    }
    const orderId = await getOrderId(this.headers.cookie)
    this.payload = {
      ...addPromoConstants,
      catalogId,
      langId,
      storeId,
      promoCode: this.payload.promotionId || '',
      orderId,
    }
  }

  mapResponseError(body = {}) {
    throw body.message ? Boom.badData(body.message) : body
  }

  mapResponseBody(body = {}) {
    return transform(body.Basket, this.storeConfig.currencySymbol)
  }

  mapResponse(res) {
    return {
      setCookies: addPromoCookies(),
      jsessionid: res.jsessionid,
      body: this.mapResponseBody(res.body),
    }
  }

  async execute() {
    this.mapEndpoint()
    try {
      await this.mapRequestParameters()
      const res = await sendRequestToApi(
        this.destinationHostname,
        this.destinationEndpoint,
        this.query,
        this.payload,
        this.method,
        this.headers
      )
      if (!res.body.success || res.body.success === 'false')
        return this.mapResponseError(res.body)
      const basketRes = await sendRequestToApi(
        this.destinationHostname,
        this.basketEndpoint,
        this.basketQuery,
        {},
        'get',
        this.headers
      )
      return this.mapResponse(basketRes)
    } catch (err) {
      return this.mapResponseError(err)
    }
  }
}

export const addPromoSpec = {
  summary: 'Add a promotional code',
  parameters: [
    {
      name: 'payload',
      in: 'body',
      required: true,
      schema: {
        type: 'object',
        required: ['promotionId'],
        properties: {
          promotionId: {
            type: 'string',
            example: '12345',
          },
        },
      },
    },
  ],
  responses: {
    200: {
      description: 'Shopping Basket Object',
      schema: {
        $ref: '#/definitions/basket',
      },
    },
    422: {
      description: 'Invalid promo code',
      schema: {
        type: 'object',
        properties: {
          statusCode: {
            type: 'number',
            example: 422,
          },
          error: {
            type: 'string',
            example: 'Unprocessable Entity',
          },
          message: {
            type: 'string',
            example:
              'The promotion code you have entered has not been recognised. Please confirm the code and try again.',
          },
          originalMessage: {
            type: 'string',
            example:
              'The promotion code you have entered has not been recognised. Please confirm the code and try again.',
          },
        },
      },
    },
  },
}
