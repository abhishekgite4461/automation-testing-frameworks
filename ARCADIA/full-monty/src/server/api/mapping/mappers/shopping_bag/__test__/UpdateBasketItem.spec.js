import * as utils from '../../../__test__/utils'
import UpdateBasketItem from '../UpdateBasketItem'
import wcsResp from '../../../../../../../test/apiResponses/shopping-bag/update-basket-item/wcs.json'
import hapiMontyResp from '../../../../../../../test/apiResponses/shopping-bag/update-basket-item/hapiMonty.json'

describe('UpdateBasketItem', () => {
  const orderItemId = '456'
  const orderId = '__orderId__'
  const catEntryId = '123'
  beforeEach(() => {
    jest.resetModules()
    jest.clearAllMocks()

    utils.setUserSession({
      cookies: [
        `cartId=${orderId};`,
        `cartItemId=${catEntryId}|${orderItemId};`,
      ],
    })
  })

  const updateInputDefaults = {
    endpoint: '/foo',
    query: '',
    payload: {
      quantity: '2',
      catEntryIdToDelete: 21189329,
      catEntryIdToAdd: 21189364,
    },
    method: 'put',
    headers: {
      cookie: 'jsessionid=1; ',
    },
    params: {},
  }

  const execute = utils.buildExecutor(UpdateBasketItem, updateInputDefaults)

  it('should map an update quantity request correctly', async () => {
    const quantity = 2

    utils.setWCSResponse({ body: wcsResp })

    await execute({
      payload: {
        catEntryIdToAdd: catEntryId,
        catEntryIdToDelete: catEntryId,
        quantity,
      },
    })

    utils.expectRequestMadeWith({
      endpoint: '/webapp/wcs/stores/servlet/OrderItemUpdateAjax',
      query: {
        orderId,
        updatePrices: 1,
        calculationUsageId: -1,
        langId: '-1',
        storeId: 12556,
        catalogId: '33057',
        catEntryId,
        calculateOrder: 1,
        quantity,
        sourcePage: 'PaymentDetails',
        URL: 'OrderCalculate?URL=ShoppingBagUpdateItemAjaxView',
        getOrderItemFromCookie: true,
        isSizeUpdate: true,
      },
      payload: {},
      method: 'post',
      headers: updateInputDefaults.headers,
      jsessionid: null,
      hostname: false,
    })
  })

  it('should map an update size and quantity request correctly', async () => {
    const oldCatEntryId = catEntryId
    const newCatEntryId = 21189330
    const quantity = 2

    utils.setWCSResponse({ body: wcsResp })

    await execute({
      payload: {
        catEntryIdToAdd: newCatEntryId,
        catEntryIdToDelete: catEntryId,
        quantity,
      },
    })

    utils.expectRequestMadeWith({
      endpoint: '/webapp/wcs/stores/servlet/OrderItemDelete',
      query: {
        orderId,
        updatePrices: 1,
        calculationUsageId: -1,
        langId: '-1',
        storeId: 12556,
        catalogId: '33057',
        catEntryId: oldCatEntryId,
        calculateOrder: 1,
        quantity,
        sourcePage: 'PaymentDetails',
        pageName: 'shoppingBag',
        URL: `OrderItemAddAjax?catEntryId=${newCatEntryId}&URL=OrderCalculate?URL=ShoppingBagUpdateItemAjaxView`,
        isSizeUpdate: true,
      },
      payload: {},
      method: 'post',
      headers: updateInputDefaults.headers,
      jsessionid: null,
      hostname: false,
    })
  })

  it('should map the response correctly', async () => {
    const catEntryId = 21189329
    const quantity = 2

    utils.setWCSResponse({ body: wcsResp })

    const resp = await execute({
      payload: {
        catEntryIdToAdd: catEntryId,
        catEntryIdToDelete: catEntryId,
        quantity,
      },
    })

    expect(resp.body).toEqual(hapiMontyResp)
  })

  it('should throw for session timeout', () => {
    const updateBasketItem = new UpdateBasketItem(
      '',
      null,
      { catEntryIdToAdd: 123 },
      'put',
      null,
      null
    )
    expect(() =>
      updateBasketItem.mapResponseError({
        message: 'wcsSessionTimeout',
      })
    ).toThrow({
      message: 'wcsSessionTimeout',
    })
  })
})
