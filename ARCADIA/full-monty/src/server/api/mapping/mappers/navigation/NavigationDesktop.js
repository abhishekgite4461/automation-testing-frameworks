import Mapper from '../../Mapper'

export default class NavigationDesktop extends Mapper {
  mapEndpoint() {
    this.destinationEndpoint = '/webapp/wcs/stores/servlet/CategoryDetails'
  }
  mapRequestParameters() {
    const { catalogId, langId, siteId: storeId } = this.storeConfig || {}
    this.query = { langId, catalogId, storeId }
  }
}
