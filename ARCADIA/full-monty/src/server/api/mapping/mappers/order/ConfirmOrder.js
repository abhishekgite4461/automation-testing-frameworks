import Boom from 'boom'
import Mapper from '../../Mapper'
import { sendRequestToApi } from '../../../api'
import { getSession } from '../../utils/sessionUtils'
import { extractCookie } from '../../../../../shared/lib/cookie'
import orderConfirmationTransform from '../../transforms/order'

/*
 * In this mapper we are going to call the endpoint which has been passed from WCS as a cookie in the POST /order response
 * ("https://ts.stage.arcadiagroup.ltd.uk/webapp/wcs/stores/servlet/PunchoutPaymentCallBack?orderId=2093607&catalogId=33057&policyId=40006&tran_id=550267&storeId=12556&langId=-1&notifyShopper=0&notifyOrderSubmitted=0").
 * To the existing query parameters we need to concatenate also the parameters PaRes and MD whose value can be retrieved from
 * the Client's payload.
 */
export default class ConfirmOrder extends Mapper {
  mapResponseBody(body, currencySymbol = '£') {
    const mappedResponse = orderConfirmationTransform(
      body,
      currencySymbol,
      this.storeConfig.currencyCode
    )

    const response = { body: mappedResponse && mappedResponse.completedOrder }

    return response
  }

  mapResponseError(res) {
    throw Boom.badData(res.status || res)
  }

  execute() {
    const { authProvider } = this.payload
    const { catalogId, langId, siteId: storeId } = this.storeConfig

    if (authProvider === 'PYPAL' || authProvider === 'MPASS') {
      const {
        policyId,
        payerId: PayerId,
        userApproved,
        orderId,
        token,
        tranId: tran_id,
      } = this.payload

      const payload =
        authProvider === 'PYPAL'
          ? {
              langId,
              storeId,
              catalogId,
              policyId,
              PayerId,
              userApproved,
              orderId,
              token,
              tran_id,
              authProvider,
              notifyShopper: 0,
              notifyOrderSubmitted: 0,
            }
          : this.payload // for MASTERPASS we pass to WCS the payload as we receive it in the request

      return sendRequestToApi(
        this.destinationHostname,
        '/webapp/wcs/stores/servlet/PunchoutPaymentCallBack',
        {},
        payload,
        'post',
        this.headers,
        undefined
      )
        .then((res) => {
          if (res.body && res.body.success === false) {
            throw Boom.badGateway(
              res.body.errorMessage || 'Error while trying to complete order'
            )
          }

          return this.mapResponseBody(res.body)
        })
        .catch((err) => {
          return this.mapResponseError(err)
        })
    } else if (
      /.*\b(ALIPY|SOFRT|CUPAY|IDEAL).*$/i.test(this.payload.paymentMethod)
    ) {
      //
      // Upon returning from the ALIPAY/SOFORT/CUPAY/IDEAL external payment site
      //

      const { paymentMethod, paymentStatus } = this.payload
      if (paymentStatus !== 'AUTHORISED')
        return this.mapResponseError('Payment was not authorised')

      const url =
        typeof paymentMethod === 'string' &&
        paymentMethod.slice(paymentMethod.indexOf('/') + 1)

      return sendRequestToApi(
        url,
        '',
        {},
        this.payload,
        'post',
        this.headers,
        undefined,
        true
      )
        .then((res) => {
          return this.mapResponseBody(res.body)
        })
        .catch((err) => {
          return this.mapResponseError(err)
        })
    }

    //
    // Upon returning from other payments
    //
    return getSession(this.headers.cookie)
      .then((session) => {
        // retrieve paymentCallBackUrl cookie

        if (!session || !session.cookies || !Array.isArray(session.cookies))
          throw Boom.badData('order confirmation: cannot retrieve user session')

        const paymentCallbackUrl = extractCookie(
          'paymentCallBackUrl',
          session.cookies
        )

        return paymentCallbackUrl
      })
      .then((paymentCallBackUrl) => {
        // call WCS

        const { paRes: PaRes, md: MD } = this.payload
        const url = `${paymentCallBackUrl}`

        // hostname, path, query, payload, method = 'get', headers = {}, jsessionid
        return sendRequestToApi(
          url,
          '',
          {},
          { PaRes, MD },
          'post',
          this.headers,
          undefined,
          true
        )
      })
      .then((res) => {
        // {
        //   "title":"Punchout Payment Result",
        //   "redirectURL":"http://ts.pplive.arcadiagroup.ltd.uk/webapp/wcs/stores/servlet/OrderProcess?catalogId=33057&orderId=700395666&storeId=12556"}
        // }
        if (!res || !res.body || !res.body.redirectURL)
          throw Boom.badGateway(
            'order confirmation: unexpected response from WCS'
          )

        return sendRequestToApi(
          res.body.redirectURL,
          '',
          {},
          {},
          'get',
          this.headers,
          undefined,
          true
        )
      })
      .then((res) => {
        if (
          !res ||
          !res.body ||
          !res.body.OrderConfirmation ||
          !res.body.orderId
        )
          throw Boom.badGateway(
            'order confirmation: malformed response from WCS'
          )

        return this.mapResponseBody(res.body)
      })
      .catch((err) => {
        throw Boom.badGateway(err)
      })
  }
}

export const confirmOrderSpec = {
  summary: 'Updates WCS with a credentials from a third party payment site.',
  parameters: [
    {
      name: 'payload',
      in: 'body',
      required: true,
      description:
        'Depending on the payment type, this will contain a key, token, or other value that is passed to WCS to confirm the payment was authorised on the third party site. See https://arcadiagroup.atlassian.net/wiki/spaces/SE/pages/417366020/PUT+order+payloads for examples for each payment type.',
      schema: {
        type: 'object',
      },
    },
  ],
  responses: {
    200: {
      description: 'Payment is approved and order is confirmed',
      schema: {
        type: 'object',
        properties: {
          completedOrder: {
            type: 'object',
            properties: {
              billingAddress: {
                type: 'object',
                properties: {
                  address1: {
                    type: 'string',
                    example: '123 Sesame Street',
                  },
                  address2: {
                    type: 'string',
                    example: '',
                  },
                  address3: {
                    type: 'string',
                    example: 'London',
                  },
                  country: {
                    type: 'string',
                    example: 'United Kingdeom',
                  },
                  name: {
                    type: 'string',
                    example: 'Bob Barker',
                  },
                },
              },
              currencyConversion: {
                type: 'object',
                properties: {
                  currencyRate: {
                    type: 'string',
                    example: 'GBP',
                  },
                },
              },
              deliveryAddress: {
                type: 'object',
                properties: {
                  address1: {
                    type: 'string',
                    example: '1 Oxford Circus',
                  },
                  address2: {
                    type: 'string',
                    example: 'West End',
                  },
                  address3: {
                    type: 'string',
                    example: 'London',
                  },
                  country: {
                    type: 'string',
                    example: 'United Kingdom',
                  },
                  name: {
                    type: 'string',
                    example: 'Bob Barker',
                  },
                },
              },
              deliveryCarrier: {
                type: 'string',
                example: '',
              },
              deliveryCost: {
                type: 'string',
                example: '4.00',
              },
              deliveryDate: {
                type: 'string',
                example: '12 January 2018',
              },
              deliveryMethod: {
                type: 'string',
                example: 'Standard Delivery',
              },
              deliveryPrice: {
                type: 'string',
                example: '4.00',
              },
              orderId: {
                type: 'number',
                example: 700381254,
              },
              orderLines: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    colour: {
                      type: 'string',
                      example: 'BLUE',
                    },
                    discount: {
                      type: 'string',
                      example: '10.00',
                    },
                    discountPrice: {
                      type: 'string',
                      example: '30.00',
                    },
                    imageUrl: {
                      type: 'string',
                      example: 'https://stage.topshop.com/image',
                    },
                    lineNo: {
                      type: 'string',
                      example: '1A1A1A',
                    },
                    name: {
                      type: 'string',
                      example: 'Blue dress',
                    },
                    nonRefundable: {
                      type: 'boolean',
                      example: false,
                    },
                    quantity: {
                      type: 'quantity',
                      example: 1,
                    },
                    size: {
                      type: 'string',
                      example: '12',
                    },
                    total: {
                      type: 'string',
                      example: '30.00',
                    },
                    unitPrice: {
                      type: 'string',
                      example: '40.00',
                    },
                  },
                },
              },
              paymentDetails: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    cardNumberStar: {
                      type: 'string',
                      example: '************1111',
                    },
                    paymentMethod: {
                      type: 'string',
                      example: 'VISA',
                    },
                    totalCost: {
                      type: 'string',
                      example: '34.00',
                    },
                  },
                },
              },
              returnPossible: {
                type: 'boolean',
                example: false,
              },
              returnRequested: {
                type: 'boolean',
                example: false,
              },
              returning_buyer: {
                type: 'boolean',
                example: true,
              },
              subTotal: {
                type: 'string',
                example: '30.00',
              },
              totalOrderPrice: {
                type: 'string',
                example: '34.00',
              },
              totalOrdersDiscount: {
                type: 'string',
                example: '10.00',
              },
              totalOrdersDiscountLabel: {
                type: 'string',
                example: '',
              },
            },
          },
        },
      },
    },
  },
}
