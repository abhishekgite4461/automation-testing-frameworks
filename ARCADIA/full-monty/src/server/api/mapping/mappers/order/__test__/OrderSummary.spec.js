import OrderSummary from '../OrderSummary'
import { orderSummaryConstants } from '../../../constants/orderSummary'
import Boom from 'boom'

jest.mock('../../../transforms/basket')

import basketTransform from '../../../transforms/basket'

jest.mock('../../../transforms/orderSummary', () => ({
  __esModule: true,
  default: jest.fn(),
  throwForFailedSummary: jest.fn(),
  throwForOutOfStock: jest.fn(),
}))
import transform, {
  throwForFailedSummary,
} from '../../../transforms/orderSummary'

jest.mock('../../../utils/cartUtils')
import * as cartUtils from '../../../utils/cartUtils'

jest.mock('../../../utils/sessionUtils')
import * as sessionUtils from '../../../utils/sessionUtils'

jest.mock('../../../../api')
import * as api from '../../../../api'

const payloadToWCS = {
  ...orderSummaryConstants,
  catalogId: '33057',
  langId: '-1',
  storeId: 12556,
  orderId: 7890,
  isTempUserNotRqrd: true,
}

const mockSession = {
  key: '0000Vs9YEL1ZgdHVWxfR1OUEbfl',
  cookies: [
    'JSESSIONID=0000Vs9YEL1ZgdHVWxfR1OUEbfl:ppl_515.01; Path=/',
    'WC_SESSION_ESTABLISHED=true; Path=/',
    'cartSize=""; Expires=Thu, 01-Dec-94 16:00:00 GMT; Path=/',
    'WC_ACTIVEPOINTER=-1%2C12556; Path=/',
    'WC_PERSISTENT=roopdRyePfKCU6jUn6wlkRV9HHA%3D%0A%3B2017-09-22+10%3A38%3A18.59_1504083751497-710_12556; Expires=Sun, 22-Oct-17 09:38:17 GMT; Path=/',
    'WC_AUTHENTICATION_1221110=1221110%2C2tUMZR5IjNo73m9eFtFtWEfWpXM%3D; Path=/; Secure',
    'WC_USERACTIVITY_1221110=1221110%2C12556%2C0%2Cnull%2C1506073098591%2C1506074982424%2Cnull%2Cnull%2Cnull%2Cnull%2C0wQDBFqqEuT69aScYehcbq9NNirrfQilTsfaGxFTeTbONA0MbEn00Ze%2F%2BjkNU60%2Bx2%2Fu92C147yaBCmo9hCL088UI25n7Ukbu0JFo7oUjXqyEiC7gfa1KP1YiGX6ou57J1ZlfpGNvNbyIWzQCfuwLFWMfP026BpTeXpjlG8Wsvq9PvNaMTohrTE9EfqVu2wfoudWHWpFSf6TBk%2B%2BIbKtBwUMlUxPMo7rQVXdpecCj6k%3D; Path=/',
    'cartId=700284012; Expires=Mon, 02-Oct-17 09:39:52 GMT; Path=/',
    'cartValue=4|126.00000; Expires=Mon, 02-Oct-17 09:39:52 GMT; Path=/',
    'cartItemId=21953047|7460310,20602389|7460309; Expires=Mon, 02-Oct-17 09:39:54 GMT; Path=/',
  ],
}

const transformedBody = {
  body: 'I am a transformed body for monty',
}

const catEntryIds = ['20602389|7460309', '21953047|7460310']

const sessionMock = () => {
  cartUtils.getOrderId = jest.fn(() => 7890)
  cartUtils.mapCatEntryIds = jest.fn(() => catEntryIds)
  sessionUtils.getSession = jest.fn(() => mockSession)
}

describe('OrderSummary Mapper', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  describe('mapEndpoint', () => {
    it('should set the destinationEndpoint to /webapp/wcs/stores/servlet/PreCheckout', () => {
      const orderSummary = new OrderSummary()
      orderSummary.mapEndpoint()
      expect(orderSummary.destinationEndpoint).toBe(
        '/webapp/wcs/stores/servlet/PreCheckout'
      )
    })
  })

  describe('mapRequestParameters', () => {
    afterEach(() => {
      jest.resetAllMocks()
    })

    it('should set the payload to a format expected by WCS', async () => {
      const orderSummary = new OrderSummary(
        {},
        {},
        {},
        'get',
        { cookie: '' },
        {}
      )
      sessionMock()
      await orderSummary.mapRequestParameters()
      expect(orderSummary.payload).toEqual(payloadToWCS)
    })

    it('should use the getOrderId utility to obtain the orderId from the session', async () => {
      const orderSummary = new OrderSummary(
        {},
        {},
        {},
        'get',
        { cookie: 'cookies' },
        {}
      )
      sessionMock()
      await orderSummary.mapRequestParameters()
      expect(cartUtils.getOrderId).toHaveBeenCalledTimes(1)
      expect(cartUtils.getOrderId).toHaveBeenCalledWith('cookies')
    })

    it('should correctly map OrderItemIds from session cookies', async () => {
      const orderSummary = new OrderSummary(
        {},
        {},
        {},
        'get',
        { cookie: 'cookies' },
        {}
      )
      sessionMock()
      await orderSummary.mapRequestParameters()
      expect(orderSummary.payload).toEqual({
        CheckoutURL:
          'OrderCopy?URL=OrderPrepare%3fURL%3dOrderDisplay%26errorViewName=InvalidInputErrorView',
        Proceed: '',
        PromoURL:
          'PromotionCodeManage?URL=OrderCalculate%3fURL%3dOrderPrepare%3fURL%3dOrderItemDisplay%26taskType=A%26errorViewName=OrderItemDisplayViewShiptoAssoc',
        URL:
          'OrderCalculate?updatePrices=1%26orderItemId*=%26quantity*=%26URL=BillingAddressView',
        catalogId: '33057',
        langId: '-1',
        orderId: 7890,
        outOrderItemName: '',
        page: '',
        isTempUserNotRqrd: true,
        returnPage: 'ShoppingBag',
        shoppingBasketURL:
          'OrderCalculate?langId=-1%26storeId=12556%26catalogId=33057%26updatePrices=1%26calculationUsageId=-1%26URL=OrderItemDisplay',
        showCheckout: false,
        storeId: 12556,
      })
    })
  })

  describe('mapResponseBody', () => {
    it('should use the orderDetails transform function', () => {
      const orderSummary = new OrderSummary()

      transform.mockReturnValue(transformedBody)

      expect(
        orderSummary.mapResponseBody({ orderSummary: {}, success: true })
      ).toEqual(transformedBody)
      expect(transform).toHaveBeenCalledTimes(1)
      expect(transform).toBeCalledWith({}, false, '£')
    })

    it('should throw a wcsSessionTimeout error when the body of the response is empty', () => {
      const orderSummary = new OrderSummary()

      expect(() => orderSummary.mapResponseBody()).toThrow('wcsSessionTimeout')
    })

    it('should NOT throw a wcsSessionTimeout error when success is false BUT we still receiving a orderSummary Object', () => {
      const orderSummary = new OrderSummary()

      const fakeOrderSummaryFalse = {
        success: false,
        error: 'some error',
        orderSummary: {
          not: 'empty',
        },
      }
      orderSummary.mapResponseBody(fakeOrderSummaryFalse)
      expect(transform).toHaveBeenCalledTimes(1)
    })
  })

  describe('execute', () => {
    afterEach(() => {
      jest.resetAllMocks()
    })

    it('does not call WCS temporary signin endpoint in case of checkout V2', async () => {
      const orderSummary = new OrderSummary(
        '',
        {},
        {},
        'get',
        { cookie: 'abc=def' },
        ''
      )

      orderSummary.mapRequestParameters = jest.fn()
      orderSummary.mapEndpoint = jest.fn()
      api.sendRequestToApi.mockReturnValueOnce(
        Promise.resolve({
          body: {
            orderSummary: { tempSignInURL: 'tempSignInURL', success: true },
          },
        })
      )

      await orderSummary.execute()

      expect(api.sendRequestToApi.mock.calls.length).toEqual(1)
    })

    // it('should make a second call to WCS if the user is a guest and the checkout v2 is not enabled', async () => {
    //   const orderSummary = new OrderSummary(
    //     '',
    //     {},
    //     {},
    //     'get',
    //     { cookie: 'abc=def' },
    //     ''
    //   )
    //   orderSummary.mapRequestParameters = jest.fn()
    //   orderSummary.mapEndpoint = jest.fn()

    //   api.sendRequestToApi
    //     .mockReturnValueOnce(
    //       Promise.resolve({
    //         body: {
    //           tempSignInURL: 'tempSignInURL',
    //         },
    //       })
    //     )
    //     .mockReturnValueOnce(Promise.resolve('OK'))

    //   await orderSummary.execute()

    //   expect(api.sendRequestToApi.mock.calls.length).toBe(2)

    //   return expect(api.sendRequestToApi).toHaveBeenCalledWith(
    //     false,
    //     '/webapp/wcs/stores/servlet/tempSignInURL',
    //     {},
    //     {},
    //     'get',
    //     { cookie: 'abc=def' }
    //   )
    // })

    describe('error handling', () => {
      describe('request parameter mapping failure', () => {
        let orderSummary

        beforeEach(() => {
          orderSummary = new OrderSummary(
            '',
            {},
            {},
            'get',
            { cookie: 'abc=def' },
            ''
          )
        })

        it('should throw a wcsSessionTimeout error when the user is not a guest', () => {
          orderSummary.mapRequestParameters = jest
            .fn()
            .mockReturnValue(Promise.reject())

          return expect(orderSummary.execute()).rejects.toThrow(
            'wcsSessionTimeout'
          )
        })

        it('should rethrow the caught error when the user is a guest', () => {
          const err = 'error'

          orderSummary.isGuest = true
          orderSummary.mapRequestParameters = jest
            .fn()
            .mockReturnValue(Promise.reject(err))

          return expect(orderSummary.execute()).rejects.toThrow(err)
        })
      })

      describe('WCS call failure', () => {
        let orderSummary

        beforeEach(() => {
          orderSummary = new OrderSummary(
            '',
            {},
            {},
            'get',
            { cookie: 'abc=def' },
            ''
          )

          orderSummary.mapRequestParameters = jest.fn()
          orderSummary.mapEndpoint = jest.fn()
          orderSummary.mapResponseError = jest.fn()
        })

        it('should throw a wcsSessionTimeout error when the user is not a guest', () => {
          api.sendRequestToApi.mockReturnValueOnce(Promise.reject({}))

          return expect(orderSummary.execute()).rejects.toThrow(
            'wcsSessionTimeout'
          )
        })

        it('should map the error message when the user is a guest', async () => {
          const error = { isBoom: true }
          orderSummary.isGuest = true

          api.sendRequestToApi.mockReturnValueOnce(Promise.reject(error))

          await orderSummary.execute()

          return expect(orderSummary.mapResponseError).toHaveBeenCalledWith(
            error
          )
        })

        it('should throw in case of session timeout', async () => {
          const error = { message: 'wcsSessionTimeout' }
          orderSummary.isGuest = true

          api.sendRequestToApi.mockReturnValueOnce(Promise.reject(error))

          let err
          try {
            await orderSummary.execute()
          } catch (e) {
            err = e
          }

          expect(err).toEqual(error)
        })

        it('should map an error if there is no order summary from the basket message for buyer', async () => {
          orderSummary.isGuest = false

          const messageForBuyer = 'This is a test error message'

          throwForFailedSummary.mockImplementation(() => {
            throw Boom.badData(messageForBuyer)
          })
          orderSummary.mapResponseError.mockImplementation((error) => {
            throw error
          })
          basketTransform.mockImplementation((value) => value)

          const mockBasket = {
            orderId: 1234,
            subTotal: 1,
            total: 1.2,
            totalBeforeDiscount: 1.0,
            deliveryOptions: [],
            promotions: [],
            discounts: [],
            products: [],
            savedProducts: [],
            ageVeficicationRequired: false,
            restrictedDeliveryItem: false,
            inventoryPosition: {},
            isDDPOrder: false,
            messageForBuyer,
          }

          api.sendRequestToApi.mockReturnValueOnce(
            Promise.resolve({
              body: {
                Basket: mockBasket,
                orderSummary: undefined,
              },
            })
          )

          let error

          try {
            await orderSummary.execute()
          } catch (caughtError) {
            error = caughtError
          }

          expect(error.isBoom).toBe(true)
          expect(error.message).toBe(messageForBuyer)
          expect(error.output.payload.data.basket).toEqual(mockBasket)
        })
      })
    })
  })
})
