import * as utils from '../../../__test__/utils'
import KlarnaSession from '../KlarnaSession'
import wcsCreateKlarnaSession from 'test/apiResponses/orders/wcs-createKlarnaSession.json'
import wcsUpdateKlarnaSession from 'test/apiResponses/orders/wcs-updateKlarnaSession.json'
import montyUpdateKlarnaSession from 'test/apiResponses/orders/hapiMonty-updateKlarnaSession.json'
import { klarnaCookies } from '../cookies/index'

describe('KlarnaSession Mapper', () => {
  const orderId = '345345345'

  beforeEach(() => {
    jest.clearAllMocks()
    jest.resetModules()
  })

  const payloadFromMonty = {
    orderId,
  }

  const updateSessionCookies = 'klarnaSessionId=foo; klarnaClientToken=bar;'

  const defaults = {
    method: 'post',
    query: {},
    payload: payloadFromMonty,
    endpoint: '',
    headers: {},
  }

  const createQueryToWCS = {
    orderId,
    requestType: 'create_session',
  }

  const updateQueryToWCS = {
    orderId,
    requestType: 'update_session',
  }

  const execute = utils.buildExecutor(KlarnaSession, defaults)

  describe('All requests', () => {
    it('should use the correct endpoint', () => {
      execute()
      expect(utils.getRequestArgs(0).endpoint).toBe(
        '/webapp/wcs/stores/servlet/KlarnaAjaxView'
      )
    })

    it('should use the correct method', () => {
      execute()
      expect(utils.getRequestArgs(0).method).toBe('get')
    })

    it('should not have a payload', () => {
      execute()
      expect(utils.getRequestArgs(0).payload).toEqual({})
    })
  })

  describe('Requests to create a session (i.e. no cookies)', () => {
    it('should have the correct query', () => {
      execute()
      expect(utils.getRequestArgs(0).query).toEqual(createQueryToWCS)
    })
  })

  describe('Requests to update a session (i.e. with cookies)', () => {
    it('should have the correct query', () => {
      execute({
        headers: {
          cookie: updateSessionCookies,
        },
      })
      expect(utils.getRequestArgs(0).query).toEqual(updateQueryToWCS)
    })
  })

  describe('Successful responses from creating a session', () => {
    it('should be returned correctly, and set correct cookies', () => {
      utils.setWCSResponse({ body: wcsCreateKlarnaSession })
      return expect(execute()).resolves.toEqual({
        body: wcsCreateKlarnaSession,
        setCookies: klarnaCookies(wcsCreateKlarnaSession),
      })
    })
  })

  describe('Successful responses from updating a session', () => {
    it('should be returned correctly, without setting cookies ', () => {
      utils.setWCSResponse({ body: wcsUpdateKlarnaSession })
      return expect(
        execute({
          headers: {
            cookie: updateSessionCookies,
          },
        })
      ).resolves.toEqual({
        body: montyUpdateKlarnaSession,
      })
    })
  })

  describe('Unsuccessful responses', () => {
    it('should throw a 422 error', () => {
      utils.setWCSResponse({
        error: 'Invalid orderId',
      })
      expect(execute()).rejects.toMatchObject({
        output: {
          payload: {
            statusCode: 422,
            message: 'Invalid orderId',
          },
        },
      })
    })
  })
})
