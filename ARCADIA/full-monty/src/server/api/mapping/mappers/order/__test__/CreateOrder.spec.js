import * as utils from '../../../__test__/utils'
import CreateOrder from '../CreateOrder'
import wcs from '../../../../../../../test/apiResponses/create-order/wcs.json'
import wcsFail from '../../../../../../../test/apiResponses/create-order/wcs-failure.json'
import monty from '../../../../../../../test/apiResponses/create-order/hapi.json'

import wcsKlarnaReturningCustomer from 'test/apiResponses/create-order/wcs-klarnaReturningCustomer.json'
import montyKlarnaReturningCustomer from 'test/apiResponses/create-order/hapi-klarnaReturningCustomer.json'
import { getCookieFromStore } from '../../../utils/sessionUtils'

jest.mock('../../../utils/sessionUtils', () => ({
  getCookieFromStore: jest.fn(),
}))
const jsessionid = '12345'

const montyRequest = (payload = {}) => ({
  originEndpoint: '/api/shopping_bag/mini_bag',
  query: {},
  payload,
  method: 'post',
  headers: {
    host: 'host',
    'user-agent':
      'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36',
    cookie: 'jessionid=a',
  },
  params: {},
})

const montyReturningAccountCard = {
  smsMobileNumber: '',
  remoteIpAddress: '127.0.0.1',
  cardCvv: '123',
  orderDeliveryOption: {
    orderId: 700366413,
    shippingCountry: 'United Kingdom',
    shipCode: 'S',
    deliveryType: 'HOME_STANDARD',
    shipModeId: 26504,
  },
  deliveryInstructions: '',
  returnUrl:
    'https://pplive-m-topshop-com.digital.arcadiagroup.co.uk/order-complete?paymentMethod=ACCNT',
  cardNumberHash: 'poJy1ZvvwAb6SPEjhgVdZIbGAZLeyMmQ',
}

const creditCard = {
  expiryYear: '2019',
  expiryMonth: '03',
  cardNumber: '6000082000000088',
  type: 'ACCNT',
}

const montyReturningNewAccountCard = {
  ...montyReturningAccountCard,
  creditCard,
}

const montyPayPalRequest = {
  smsMobileNumber: '',
  remoteIpAddress: '127.0.0.1',
  cardCvv: '0',
  orderDeliveryOption: {
    orderId: 700402896,
    shippingCountry: 'United Kingdom',
    shipCode: 'S',
    deliveryType: 'HOME_STANDARD',
    shipModeId: 26504,
  },
  deliveryInstructions: '',
  returnUrl:
    'http://local.m.topshop.com:8080/order-complete?paymentMethod=PYPAL',
  cardNumberHash: '3J8Vw1B5YPIr0fgHS8y/p8aDoXgbbq/u',
}

const montyApplePayRequest = {
  smsMobileNumber: '',
  remoteIpAddress: '127.0.0.1',
  orderDeliveryOption: {
    orderId: 700366413,
    shippingCountry: 'United Kingdom',
    shipCode: 'S',
    deliveryType: 'HOME_STANDARD',
    shipModeId: 26504,
  },
  deliveryInstructions: '',
  returnUrl:
    'http://local.m.topshop.com:8080/order-complete?paymentMethod=APPLE',
  paymentToken: 'testString',
}

const wcsPayPalPayload = {
  ipAddress: '127.0.0.1',
  orderId: 700402896,
  errorViewName: 'DoPaymentErrorView',
  userAgent:
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36',
  contentType: '*/*',
  acceptHeader: '*/*',
  auth_token: undefined,
  cardBrand: 'PYPAL',
  cardSecurityNumber: undefined,
  addressId: undefined,
  billing_address_id: undefined,
  notifyShopper: '0',
  notifyOrderSubmitted: '0',
  deliveryInstructions: '',
  smsAlerts: '',
  shipCode: 'S',
  nominatedDate: '',
  URL: 'OrderPrepare?URL=ConfirmAndPay',
  TERMSANDCONDITIONS_OPTIN: 'true',
  langId: '-1',
  mobile: 'http://local.m.topshop.com:8080/order-complete?paymentMethod=PYPAL',
  storeId: 12556,
  catalogId: '33057',
  save_details: 'on',
}

const wcsPayload = {
  ipAddress: '127.0.0.1',
  orderId: 700366413,
  errorViewName: 'DoPaymentErrorView',
  userAgent:
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36',
  contentType: '*/*',
  acceptHeader: '*/*',
  auth_token: undefined,
  cardBrand: undefined,
  cardExpiryMonth: undefined,
  cardExpiryYear: undefined,
  cardNumber: 'poJy1ZvvwAb6SPEjhgVdZIbGAZLeyMmQ',
  cardNumberStar: undefined,
  notifyShopper: '0',
  notifyOrderSubmitted: '0',
  deliveryInstructions: '',
  smsAlerts: '',
  shipCode: 'S',
  nominatedDate: '',
  URL: 'OrderPrepare?URL=ConfirmAndPay',
  cardSecurityNumber: '123',
  TERMSANDCONDITIONS_OPTIN: 'true',
  langId: '-1',
  storeId: 12556,
  catalogId: '33057',
  save_details: 'on',
}

const wcsPayloadWithCreditCard = {
  ...wcsPayload,
  cardBrand: creditCard.type,
  cardNumberStar: creditCard.cardNumber,
  cardExpiryMonth: creditCard.expiryMonth,
  cardExpiryYear: creditCard.expiryYear,
}

const wcsPayloadWithApplePay = {
  ...wcsPayload,
  cardBrand: 'APPLE',
  cardNumber: undefined,
  cardSecurityNumber: undefined,
  addressId: undefined,
  billing_address_id: undefined,
  paymentToken: 'testString',
}

const addressDetails = {
  address1: '1 Baker St',
  address2: '',
  city: 'London',
  state: '',
  country: 'England',
  postcode: 'WC123A',
}

const execute = utils.buildExecutor(
  CreateOrder,
  montyRequest(montyReturningAccountCard)
)

const happyApi = () => {
  utils.setWCSResponse({ body: wcs, jsessionid })
  getCookieFromStore.mockReturnValue(Promise.resolve('Y'))
}

const happyApiNoFullCheckoutProfile = () => {
  utils.setWCSResponse(
    {
      body: {
        success: true,
        orderSummary: {},
        ConfirmAndPayCardDetailsForm: { addressId: '123' },
      },
      jsessionid,
    },
    { n: 0 }
  )
  utils.setWCSResponse(
    {
      body: {
        success: true,
        orderSummary: {},
        errorMessage: '',
        ConfirmAndPayCardDetailsForm: {
          billing_address_id: '123',
        },
      },
      jsessionid,
    },
    { n: 1 }
  )
  utils.setWCSResponse({ body: wcs, jsessionid }, { n: 2 })
  getCookieFromStore.mockReturnValue(Promise.resolve('N'))
}

const unhappyApi = () => {
  utils.setWCSResponse({ body: wcsFail, jsessionid })
  getCookieFromStore.mockReturnValue(Promise.resolve('Y'))
}

describe('CreateOrder', () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  describe('"ipAddress" payload parameter to WCS', () => {
    it('is set by default to "127.0.0.1"', () => {
      happyApi()
      expect.assertions(1)
      return execute().then(() =>
        expect(utils.getRequestArgs().payload.ipAddress).toEqual('127.0.0.1')
      )
    })
    it('is set with the value of the first IP address received in the "x-forwarded-for" header', () => {
      happyApi()
      const execute = utils.buildExecutor(CreateOrder, {
        originEndpoint: '/api/shopping_bag/mini_bag',
        query: {},
        payload: {},
        method: 'post',
        headers: {
          'x-forwarded-for': '1.1.1.1, 2.2.2.2, 3.3.3.3',
        },
        params: {},
      })
      expect.assertions(1)
      return execute().then(() =>
        expect(utils.getRequestArgs().payload.ipAddress).toEqual('1.1.1.1')
      )
    })
  })

  describe('"nominatedDate" payload parameter to WCS', () => {
    // This test has been introduced to intercept regression for the scenario described in MJI-861

    describe('User with checkout profile', () => {
      it('is set as expected', () => {
        happyApi()
        const execute = utils.buildExecutor(CreateOrder, {
          originEndpoint: '',
          query: {},
          payload: {
            orderDeliveryOption: {
              nominatedDate: 'nominatedDate',
            },
          },
          method: 'post',
          headers: {},
          params: {},
        })
        expect.assertions(1)
        return execute().then(() =>
          expect(utils.getRequestArgs().payload.nominatedDate).toEqual(
            'nominatedDate'
          )
        )
      })
    })
    describe('User with NO checkout profile', () => {
      it('is set as expected', async () => {
        happyApiNoFullCheckoutProfile()
        const execute = utils.buildExecutor(CreateOrder, {
          originEndpoint: '',
          query: {},
          payload: {
            deliveryAddress: addressDetails,
            billingDetails: addressDetails,
            orderDeliveryOption: {
              nominatedDate: 'nominatedDate',
            },
          },
          method: 'post',
          headers: {},
          params: {},
        })
        await execute()
        expect(utils.getRequestArgs().payload.nominatedDate).toEqual(
          'nominatedDate'
        )
      })
    })
  })

  describe('An order that fails', () => {
    describe('response', () => {
      beforeEach(() => {
        unhappyApi()
      })
      it('should reject with a 422 and the wcs error message', () => {
        return expect(execute()).rejects.toHaveProperty('output.payload', {
          error: 'Unprocessable Entity',
          statusCode: 422,
          message: wcsFail.errorMessage,
        })
      })
    })
  })

  describe('All requests to create an order', () => {
    it('should call the endpoint /webapp/wcs/stores/servlet/OrderCalculate', async () => {
      happyApi()
      await execute()
      expect(utils.getRequestArgs(0).endpoint).toBe(
        '/webapp/wcs/stores/servlet/OrderCalculate'
      )
    })
    it('should call with the post method', async () => {
      happyApi()
      await execute()
      expect(utils.getRequestArgs(0).method).toBe('post')
    })
  })

  describe('User with checkout profile', () => {
    it('will not send a request to UserRegistrationUpdate endpoint', async () => {
      happyApi()
      await execute()
      expect(utils.getRequests().length).toBe(1)
      expect(utils.getRequestArgs(0).endpoint).not.toBe(
        '/webapp/wcs/stores/servlet/UserRegistrationUpdate'
      )
      expect(utils.getRequestArgs(0).endpoint).toBe(
        '/webapp/wcs/stores/servlet/OrderCalculate'
      )
    })
  })

  describe('A user with no checkout profile', () => {
    it('should update delivery and billing details on users profile and submit order', async () => {
      happyApiNoFullCheckoutProfile()
      const execute = utils.buildExecutor(CreateOrder, {
        originEndpoint: '',
        query: {},
        payload: {
          deliveryAddress: addressDetails,
          billingDetails: addressDetails,
        },
        method: 'post',
        headers: {},
        params: {},
      })

      await execute()
      expect(utils.getRequests().length).toBe(3)
      expect(utils.getRequestArgs(0).endpoint).toBe(
        '/webapp/wcs/stores/servlet/UserRegistrationUpdate'
      )
      expect(utils.getRequestArgs(0).payload.montyUserAction).toBe('shipping')
      expect(utils.getRequestArgs(1).endpoint).toBe(
        '/webapp/wcs/stores/servlet/UserRegistrationUpdate'
      )
      expect(utils.getRequestArgs(1).payload.montyUserAction).toBe('billing')
      expect(utils.getRequestArgs(2).endpoint).toBe(
        '/webapp/wcs/stores/servlet/ConfirmAndPay'
      )
    })
  })

  describe('A returning customer', () => {
    describe('Paying with an Account card already used', () => {
      describe('calling the wcs endpoint for creating an Order', () => {
        it('should call with the correct payload', async () => {
          happyApi()
          await execute()
          expect(utils.getRequestArgs(0).payload).toEqual(wcsPayload)
        })
      })
      describe('response', () => {
        beforeEach(() => {
          happyApi()
        })
        it('should transform the wcs response into a monty response', async () => {
          const result = await execute()
          expect(result.body).toEqual(monty)
        })
        it('should contain the jsessionid', async () => {
          const result = await execute()
          expect(result.jsessionid).toBe(jsessionid)
        })
      })
    })

    describe('Paying with a new Account card, with an old billing address', () => {
      describe('calling the wcs endpoint for creating an Order', () => {
        it('should call with the correct payload', async () => {
          happyApi()
          await execute({
            payload: montyReturningNewAccountCard,
          })
          expect(utils.getRequestArgs(0).payload).toEqual(
            wcsPayloadWithCreditCard
          )
        })
      })
      describe('response', () => {
        beforeEach(() => {
          happyApi()
        })
        it('should transform the wcs response into a monty response', async () => {
          const result = await execute()
          expect(result.body).toEqual(monty)
        })
        it('should contain the jsessionid', async () => {
          const result = await execute()
          expect(result.jsessionid).toBe(jsessionid)
        })
      })
    })

    describe('3D payments: VISA, MASTERCARD, AMEX', () => {
      describe('requests to WCS', () => {
        it('should make the expected requests to WCS', () => {
          utils.setWCSResponse({
            body: {
              formEncoded: true,
              TermUrl: 'termUrl',
              piId: 'piId',
              cardBrand: 'cardBrand',
              action: 'action',
              MD: 'md',
              PaReq: 'paReq',
              punchOutRepayURL: 'punchOutRepayURL',
            },
          })
          utils.setWCSResponse(
            { body: { OrderConfirmation: 'orderConfirmation', orderId: 123 } },
            { n: 1 }
          )

          expect.assertions(4)
          return execute().then(() => {
            expect(utils.getRequestArgs(1).endpoint).toEqual(
              '/webapp/wcs/stores/servlet/punchOutRepayURL'
            )
            expect(utils.getRequestArgs(1).query).toEqual({})
            expect(utils.getRequestArgs(1).payload).toEqual({})
            expect(utils.getRequestArgs(1).method).toEqual('post')
          })
        })
      })
      describe('response', () => {
        it('should be mapped correctly', () => {
          happyApi()
          utils.setWCSResponse({
            body: {
              formEncoded: true,
              TermUrl: 'termUrl',
              piId: 'piId',
              cardBrand: 'cardBrand',
              action: 'action',
              MD: 'md',
              PaReq: 'paReq',
            },
          })
          utils.setWCSResponse({ body: { orderId: 123 } }, { n: 1 })

          expect.assertions(1)
          return execute()
            .then((res) => {
              return res.body
            })
            .then((res) => {
              const vbvForm = {
                termUrl: `https://pplive-m-topshop-com.digital.arcadiagroup.co.uk/order-complete?paymentMethod=ACCNT`,
                originalTermUrl: 'termUrl',
                vbvForm: `<form method='post' action='action'><input type='hidden' name='TermUrl' value='https://pplive-m-topshop-com.digital.arcadiagroup.co.uk/order-complete?paymentMethod=ACCNT'/><input type='hidden' name='MD' value='md'/><textarea style='display:none;' name='PaReq'>paReq</textarea></form><script language='text/javascript' type='text/javascript'>document.getElementsByTagName('form')[0].submit();</script>`,
                piId: 'piId',
              }
              expect(res).toEqual({ vbvForm })
            })
        })
      })
    })

    describe('no 3D payments: MASTERO, VISA, MASTERCARD, AMEX', () => {
      describe('requests to WCS', () => {
        it('are done as expected', () => {
          utils.setWCSResponse({
            body: {
              paymentRedirectURL: 'paymentRedirectURL',
              punchOutRepayURL: 'punchOutRepayURL',
            },
          })
          utils.setWCSResponse({ body: { orderId: 123 } }, { n: 1 })
          utils.setWCSResponse(
            { body: { redirectURL: 'redirectURL' } },
            { n: 2 }
          )
          utils.setWCSResponse({ body: { OrderConfirmation: {} } }, { n: 3 })

          expect.assertions(14)
          return execute().then(() => {
            expect(utils.getRequestArgs(1).endpoint).toEqual(
              '/webapp/wcs/stores/servlet/punchOutRepayURL'
            )
            expect(utils.getRequestArgs(1).query).toEqual({})
            expect(utils.getRequestArgs(1).payload).toEqual({})
            expect(utils.getRequestArgs(1).method).toEqual('post')

            expect(utils.getRequestArgs(2).hostname).toEqual(
              'paymentRedirectURL'
            )
            expect(utils.getRequestArgs(2).endpoint).toEqual('')
            expect(utils.getRequestArgs(2).query).toEqual({})
            expect(utils.getRequestArgs(2).payload).toEqual({})
            expect(utils.getRequestArgs(2).method).toEqual('get')

            expect(utils.getRequestArgs(3).hostname).toEqual('redirectURL')
            expect(utils.getRequestArgs(3).endpoint).toEqual('')
            expect(utils.getRequestArgs(3).query).toEqual({})
            expect(utils.getRequestArgs(3).payload).toEqual({})
            expect(utils.getRequestArgs(3).method).toEqual('get')
          })
        })
      })
      describe('response', () => {
        it('is mapped correctly', () => {
          utils.setWCSResponse({
            body: {
              paymentRedirectURL: 'paymentRedirectURL',
              punchOutRepayURL: 'punchOutRepayURL',
            },
          })
          utils.setWCSResponse({ body: { orderId: 123 } }, { n: 1 })
          utils.setWCSResponse(
            { body: { redirectURL: 'redirectURL' } },
            { n: 2 }
          )
          utils.setWCSResponse({ body: wcsKlarnaReturningCustomer }, { n: 3 })

          expect.assertions(1)
          return execute()
            .then((res) => {
              return res.body
            })
            .then((res) => {
              expect(res).toEqual(montyKlarnaReturningCustomer)
            })
        })
      })
    })

    describe('ApplePay', () => {
      describe('requests to WCS', () => {
        it('are done as expected', async () => {
          utils.setWCSResponse({ body: wcs, jsessionid })
          getCookieFromStore.mockReturnValue(Promise.resolve('Y'))
          await execute({ payload: montyApplePayRequest })
          expect(utils.getRequestArgs(0).payload).toEqual(
            wcsPayloadWithApplePay
          )
        })

        it('validates paymentToken type', () => {
          utils.setWCSResponse({ body: wcs, jsessionid })
          getCookieFromStore.mockReturnValue(Promise.resolve('Y'))
          return expect(
            execute({
              payload: { ...montyApplePayRequest, paymentToken: {} },
            })
          ).rejects.toThrow()
        })
      })
      describe('response', () => {
        beforeEach(() => {
          happyApi()
        })
        it('should transform the wcs response into a monty response', async () => {
          const result = await execute()
          expect(result.body).toEqual(monty)
        })
        it('should contain the jsessionid', async () => {
          const result = await execute()
          expect(result.jsessionid).toBe(jsessionid)
        })
      })
    })

    describe('PayPal', () => {
      describe('requests to WCS', () => {
        it('are done as expected', () => {
          utils.setWCSResponse({
            body: {
              ...wcs,
              confirmationTitle: false,
              cardBrand: 'PYPAL',
              punchOutRepayURL: 'punchOutRepayURL',
              paymentRedirectURL: 'paymentRedirectURL',
            },
            jsessionid,
          })
          utils.setWCSResponse({}, { n: 1 })

          expect.assertions(3)
          return execute({ payload: montyPayPalRequest }).then(() => {
            expect(utils.getRequestArgs(0).payload).toEqual(wcsPayPalPayload)

            expect(utils.getRequestArgs(1).endpoint).toEqual(
              '/webapp/wcs/stores/servlet/punchOutRepayURL'
            )
            expect(utils.getRequestArgs(1).method).toEqual('post')
          })
        })
      })
      describe('response', () => {
        it('is mapped correctly', () => {
          utils.setWCSResponse({
            body: {
              ...wcs,
              confirmationTitle: false,
              cardBrand: 'PYPAL',
              punchOutRepayURL: 'punchOutRepayURL',
              paymentRedirectURL: 'paymentRedirectURL',
            },
            jsessionid,
          })
          utils.setWCSResponse({ orderId: 'orderId' }, { n: 1 })

          expect.assertions(1)
          return execute({ payload: montyPayPalRequest })
            .then((res) => {
              return res.body
            })
            .then((res) => {
              expect(res).toEqual({
                paymentUrl: 'paymentRedirectURL',
                paypalUrl: 'paymentRedirectURL',
              })
            })
        })
      })
    })
  })
})
