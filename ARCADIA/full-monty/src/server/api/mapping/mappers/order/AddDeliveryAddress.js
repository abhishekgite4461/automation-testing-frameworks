import { path } from 'ramda'
import Boom from 'boom'
import Mapper from '../../Mapper'
import { isAuthenticated } from '../../utils/sessionUtils'
import { sendRequestToApi } from '../../../api'
import { getOrderId } from '../../utils/cartUtils'
import { addDeliveryAddressConstants } from '../../constants/orderSummary'
import transform from '../../transforms/addDeliveryAddress'

export default class AddDeliveryAddress extends Mapper {
  async mapRequestParameters() {
    const { langId, catalogId, siteId: storeId } = this.storeConfig

    const orderId = await getOrderId(this.headers.cookie)

    this.payload = {
      langId,
      storeId,
      catalogId,
      shipping_address1: path(['address', 'address1'], this.payload),
      shipping_address2: path(['address', 'address2'], this.payload),
      shipping_city: path(['address', 'city'], this.payload),
      orderId,
      shipping_zipCode: path(['address', 'postcode'], this.payload),
      shipping_nickName: `Default_Shipping_${storeId}`,
      shipping_personTitle: path(['nameAndPhone', 'title'], this.payload),
      shipping_firstName: path(['nameAndPhone', 'firstName'], this.payload),
      shipping_lastName: path(['nameAndPhone', 'lastName'], this.payload),
      shipping_phone1: path(['nameAndPhone', 'telephone'], this.payload),
      shipping_country: path(['address', 'country'], this.payload),
      shipping_state_input: path(['address', 'state'], this.payload),
      shippingIsoCode: '',
      shipping_email1: '',
      ...addDeliveryAddressConstants,
    }
  }

  mapEndpoint() {
    const isGuest = !isAuthenticated(this.headers.cookie)
    const wcs = '/webapp/wcs/stores/servlet/'
    this.destinationEndpoint = isGuest
      ? `${wcs}ProcessDeliveryDetails`
      : `${wcs}UserRegistrationUpdate`
  }

  mapResponseBody(body = {}) {
    if (!body.orderSummary || body.errorMessage)
      return this.mapResponseError(body)
    return transform(body.orderSummary)
  }

  mapResponseError(body) {
    throw body.errorMessage ? Boom.badData(body.errorMessage) : body
  }
  async execute() {
    await this.mapRequestParameters()
    this.mapEndpoint()
    try {
      const res = await sendRequestToApi(
        this.destinationHostname,
        this.destinationEndpoint,
        this.query,
        this.payload,
        this.method,
        this.headers
      )
      return this.mapResponse(res)
    } catch (err) {
      return this.mapResponseError(err)
    }
  }
}

export const addDeliveryAddressSpec = {
  summary: 'Add a delivery address to a user account',
  parameters: [
    {
      name: 'payload',
      in: 'body',
      required: true,
      description: 'Delivery details',
      schema: {
        type: 'object',
        properties: {
          addressDetailsId: {
            type: 'integer',
            example: 12345,
          },
          address: {
            type: 'object',
            properties: {
              state: {
                type: 'string',
                example: 'Greater London',
              },
              address1: {
                type: 'string',
                example: '1 Oxford Street',
              },
              address2: {
                type: 'string',
                example: 'Westminster',
              },
              postcode: {
                type: 'string',
                example: 'W1 1A1',
              },
              city: {
                type: 'string',
                example: 'London',
              },
              country: {
                type: 'string',
                example: 'United Kingdom',
              },
            },
          },
          nameAndPhone: {
            type: 'object',
            properties: {
              lastName: {
                type: 'string',
                example: 'Barker',
              },
              firstName: {
                type: 'string',
                example: 'Bob',
              },
              title: {
                type: 'string',
                example: 'Mr',
              },
              telephone: {
                type: 'string',
                example: '020111222',
              },
            },
          },
        },
      },
    },
  ],
  responses: {
    200: {
      description: 'All Saved Addresses',
      schema: {
        type: 'object',
        properties: {
          savedAddresses: {
            $ref: '#/definitions/savedAddresses',
          },
          deliveryDetails: {
            $ref: '#/definitions/userDetails',
          },
          billingDetails: {
            $ref: '#/definitions/userDetails',
          },
          creditCard: {
            $ref: '#/definitions/creditCard',
          },
        },
      },
    },
  },
}
