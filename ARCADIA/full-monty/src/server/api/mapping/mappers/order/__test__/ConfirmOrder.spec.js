import * as utils from '../../../__test__/utils'

import ConfirmOrder from '../ConfirmOrder'

// Home Standard
import wcsHomeStandard from 'test/apiResponses/orders/putOrder/homeStandard/wcs.json'
import hapiHomeStandard from 'test/apiResponses/orders/putOrder/homeStandard/hapi.json'

// Home Express
import wcsHomeExpress from 'test/apiResponses/orders/putOrder/homeExpress/wcs.json'
import hapiHomeExpress from 'test/apiResponses/orders/putOrder/homeExpress/hapi.json'

// Store Standard
import wcsStoreStandard from 'test/apiResponses/orders/putOrder/storeStandard/wcs.json'
import hapiStoreStandard from 'test/apiResponses/orders/putOrder/storeStandard/hapi.json'

// Store Express
import wcsStoreExpress from 'test/apiResponses/orders/putOrder/storeExpress/wcs.json'
import hapiStoreExpress from 'test/apiResponses/orders/putOrder/storeExpress/hapi.json'

// Parcelshop
import wcsParcelshop from 'test/apiResponses/orders/putOrder/parcelshop/wcs.json'
import hapiParcelShop from 'test/apiResponses/orders/putOrder/parcelshop/hapi.json'

// International

// ALIPAY/SOFRT/CUPAY/IDEAL
import wcsAlipay from 'test/apiResponses/orders/putOrder/putOrder.alipay/wcs.json'
import hapiAlipay from 'test/apiResponses/orders/putOrder/putOrder.alipay/hapiMonty.json'

describe('ConfirmOrder Mapper', () => {
  beforeEach(() => {
    jest.resetModules()
    jest.clearAllMocks()
  })

  const headers = {
    cookie: 'jsessionid=12345',
  }

  const defaults = {
    endpoint: 'putOrder',
    query: {},
    payload: { paRes: 'PaRes', md: 'MD' },
    method: 'put',
    headers,
    params: {},
  }

  const execute = utils.buildExecutor(ConfirmOrder, defaults)

  describe('Requests to WCS', () => {
    describe('PayPal', () => {
      it('Sends the expected request to WCS', () => {
        const executePayPalPutOrder = utils.buildExecutor(ConfirmOrder, {
          payload: {
            authProvider: 'PYPAL',
            policyId: 'policyId',
            payerId: 'PayerId',
            userApproved: 'userApproved',
            orderId: 'orderId',
            token: 'token',
            tranId: 'tran_id',
          },
        })

        expect.assertions(3)
        return executePayPalPutOrder().then(() => {
          expect(utils.getRequestArgs(0).endpoint).toEqual(
            '/webapp/wcs/stores/servlet/PunchoutPaymentCallBack'
          )
          expect(utils.getRequestArgs(0).method).toEqual('post')
          expect(utils.getRequestArgs(0).payload).toEqual({
            authProvider: 'PYPAL',
            policyId: 'policyId',
            PayerId: 'PayerId',
            userApproved: 'userApproved',
            orderId: 'orderId',
            token: 'token',
            tran_id: 'tran_id',
            notifyShopper: 0,
            notifyOrderSubmitted: 0,
            langId: '-1',
            catalogId: '33057',
            storeId: 12556,
          })
        })
      })
    })

    describe('ALIPY/SOFRT/CUPAY/IDEAL', () => {
      const montyPayload = {
        paymentMethod: 'ALIPY/https://foo.bar',
        catalogId: 33057,
        storeId: 12556,
        langId: -1,
        tran_id: 784789,
        userApproved: 1,
        dummy: '',
        orderKey: 'ORDER_KEY',
        paymentStatus: 'AUTHORISED',
        paymentAmount: '23760',
        paymentCurrency: 'GBP',
        mac: 'MAC',
        orderId: 'orderId',
        authProvider: 'ALIPY',
      }
      const execute = utils.buildExecutor(ConfirmOrder, {
        payload: montyPayload,
      })
      describe('Requests', () => {
        it('should pass through the payload from Monty', async () => {
          await execute()
          expect(utils.getRequestArgs(0).payload).toEqual(montyPayload)
        })

        it('should fail if the payment is not authorised', () => {
          // try-catch block used as execute() is not a Promise
          try {
            execute({ payload: { ...montyPayload, paymentStatus: 'DECLINED' } })
          } catch (err) {
            expect(err).toHaveProperty('output.payload', {
              error: 'Unprocessable Entity',
              statusCode: 422,
              message: 'Payment was not authorised',
            })
          }
        })

        it('should fail if the WCS request fails', () => {
          utils.setWCSResponse(Promise.reject('WCS error'))
          expect(execute()).rejects.toHaveProperty('output.payload', {
            error: 'Unprocessable Entity',
            message: 'WCS error',
            statusCode: 422,
          })
        })
      })

      describe('Responses', () => {
        it('should be mapped correctly', () => {
          utils.setWCSResponse({ body: wcsAlipay })
          return expect(execute()).resolves.toEqual({ body: hapiAlipay })
        })
      })
    })

    it('are done passing the expected argumentss', async () => {
      utils.setUserSession({ cookies: [`paymentCallBackUrl=abc;`] })
      utils.setWCSResponse({ body: { redirectURL: 'redirectUrl' } })
      utils.setWCSResponse(
        { body: { OrderConfirmation: 'orderConfirmation', orderId: 123 } },
        { n: 1 }
      )
      await execute()
        .then(() => {
          const scenarios = [
            { n: 0, argumentName: 'hostname', argumentValue: 'abc' },
            { n: 0, argumentName: 'endpoint', argumentValue: '' },
            { n: 0, argumentName: 'query', argumentValue: {} },
            {
              n: 0,
              argumentName: 'payload',
              argumentValue: { PaRes: 'PaRes', MD: 'MD' },
            },
            { n: 0, argumentName: 'method', argumentValue: 'post' },
            { n: 0, argumentName: 'headers', argumentValue: headers },
            { n: 0, argumentName: 'jsessionid', argumentValue: undefined },
            { n: 0, argumentName: 'nopath', argumentValue: true },

            { n: 1, argumentName: 'hostname', argumentValue: 'redirectUrl' },
            { n: 1, argumentName: 'endpoint', argumentValue: '' },
            { n: 1, argumentName: 'query', argumentValue: {} },
            { n: 1, argumentName: 'payload', argumentValue: {} },
            { n: 1, argumentName: 'method', argumentValue: 'get' },
            { n: 1, argumentName: 'headers', argumentValue: headers },
            { n: 1, argumentName: 'jsessionid', argumentValue: undefined },
            { n: 1, argumentName: 'nopath', argumentValue: true },
          ]

          scenarios.map((scenario) =>
            expect(
              utils.getRequestArgs(scenario.n)[scenario.argumentName]
            ).toEqual(scenario.argumentValue)
          )
        })
        .catch((err) => {
          throw err
        })
    })
  })

  describe('Mapping the response', () => {
    describe('Home Standard delivery', () => {
      it('maps WCS responses for all the different scenarios associated to delievery', () => {
        utils.setUserSession({ cookies: [`paymentCallBackUrl=abc;`] })
        utils.setWCSResponse({ body: { redirectURL: 'redirectUrl' } })
        utils.setWCSResponse({ body: wcsHomeStandard }, { n: 1 })

        expect.assertions(1)
        return execute().then((res) =>
          expect(res).toEqual({ body: hapiHomeStandard })
        )
      })
    })

    describe('Home Express delivery', () => {
      it('maps WCS responses for all the different scenarios associated to delievery', () => {
        utils.setUserSession({ cookies: [`paymentCallBackUrl=abc;`] })
        utils.setWCSResponse({ body: { redirectURL: 'redirectUrl' } })
        utils.setWCSResponse({ body: wcsHomeExpress }, { n: 1 })

        expect.assertions(1)
        return execute().then((res) =>
          expect(res).toEqual({ body: hapiHomeExpress })
        )
      })
    })

    describe('Store Standard delivery', () => {
      it('maps WCS responses for all the different scenarios associated to delievery', () => {
        utils.setUserSession({ cookies: [`paymentCallBackUrl=abc;`] })
        utils.setWCSResponse({ body: { redirectURL: 'redirectUrl' } })
        utils.setWCSResponse({ body: wcsStoreStandard }, { n: 1 })

        expect.assertions(1)
        return execute().then((res) =>
          expect(res).toEqual({ body: hapiStoreStandard })
        )
      })
    })

    describe('Store Express delivery', () => {
      it('maps WCS responses for all the different scenarios associated to delievery', () => {
        utils.setUserSession({ cookies: [`paymentCallBackUrl=abc;`] })
        utils.setWCSResponse({ body: { redirectURL: 'redirectUrl' } })
        utils.setWCSResponse({ body: wcsStoreExpress }, { n: 1 })

        expect.assertions(1)
        return execute().then((res) =>
          expect(res).toEqual({ body: hapiStoreExpress })
        )
      })
    })

    describe('Parcelshop delivery', () => {
      it('maps WCS responses for all the different scenarios associated to delievery', () => {
        utils.setUserSession({ cookies: [`paymentCallBackUrl=abc;`] })
        utils.setWCSResponse({ body: { redirectURL: 'redirectUrl' } })
        utils.setWCSResponse({ body: wcsParcelshop }, { n: 1 })

        expect.assertions(1)
        return execute().then((res) =>
          expect(res).toEqual({ body: hapiParcelShop })
        )
      })
    })
  })
})
