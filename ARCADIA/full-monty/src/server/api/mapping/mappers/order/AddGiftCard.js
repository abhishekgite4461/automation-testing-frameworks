import Boom from 'boom'
import Mapper from '../../Mapper'
import transform from '../../transforms/giftCard'
import { isAuthenticated } from '../../utils/sessionUtils'
import { giftCardConstants } from '../../constants/giftCard'
import { getOrderId } from '../../utils/cartUtils'
import { sendRequestToApi } from '../../../api'

export default class AddGiftCard extends Mapper {
  mapEndpoint() {
    this.destinationEndpoint = '/webapp/wcs/stores/servlet/PPTGiftCardsManager'
  }

  async mapRequestParameters() {
    const orderId = await getOrderId(this.headers.cookie)
    const { catalogId, langId, siteId: storeId } = this.storeConfig
    const { giftCardNumber, pin } = this.payload
    this.payload = {
      ...giftCardConstants,
      orderId,
      catalogId,
      langId,
      storeId,
      giftCardNo: giftCardNumber,
      giftCardPinNo: pin,
      action: 'add',
      sourcePage: isAuthenticated(this.headers.cookie)
        ? 'OrderSubmitForm'
        : 'PaymentDetails',
    }
  }

  mapResponseBody(body = {}) {
    if (!body.success || body.success === 'false')
      return this.mapResponseError(body)
    const isGuest = !isAuthenticated(this.headers.cookie)
    return transform(body, isGuest, this.storeConfig.currencySymbol)
  }

  mapResponseError(body = {}) {
    throw body.message ? Boom.badData(body.message) : body
  }

  async execute() {
    await this.mapRequestParameters()
    this.mapEndpoint()
    try {
      const res = await sendRequestToApi(
        this.destinationHostname,
        this.destinationEndpoint,
        this.query,
        this.payload,
        this.method,
        this.headers
      )
      return this.mapResponse(res)
    } catch (err) {
      return this.mapResponseError(err)
    }
  }
}

export const addGiftCardSpec = {
  summary: 'Add a gift card to an order',
  parameters: [
    {
      name: 'payload',
      in: 'body',
      required: true,
      description: '16 digit card number and 4 digit pin',
      schema: {
        type: 'object',
        properties: {
          giftCardnumber: {
            type: 'string',
            example: '1234123412341234',
          },
          pin: {
            type: 'string',
            example: '5678',
          },
        },
      },
    },
  ],
  responses: {
    200: {
      description: 'Order Summary including added gift card',
      schema: {
        type: 'object',
        properties: {
          basket: {
            $ref: '#/definitions/basket',
          },
          deliveryLocations: {
            $ref: '#/definitions/deliveryLocations',
          },
          discounts: {
            $ref: '#/definitions/discounts',
          },
          giftCards: {
            $ref: '#/definitions/giftCards',
          },
          deliveryInstructions: {
            type: 'string',
            example: 'Leave on my front door please',
          },
          smsMobileNumber: {
            type: 'string',
            example: '1234123412',
          },
          shippingCountry: {
            type: 'string',
            example: 'United Kingdom',
          },
          savedAddresses: {
            $ref: '#/definitions/savedAddresses',
          },
          ageVerificationDeliveryConfirmationRequired: {
            type: 'boolean',
            example: false,
          },
          estimatedDelivery: {
            type: 'array',
            items: {
              type: 'string',
              example: 'Thursday Oct 4th 2019',
            },
          },
        },
      },
    },
    422: {
      description:
        'Error with gift card number/pin, or there if there was insufficient credit on the gift card.',
      schema: {
        type: 'object',
        properties: {
          statusCode: {
            description: 'Error status code',
            type: 'number',
          },
          error: {
            description: 'Error type',
            type: 'string',
          },
          message: {
            description: 'Error message that is displayed to the user',
            type: 'string',
          },
        },
      },
    },
  },
}
