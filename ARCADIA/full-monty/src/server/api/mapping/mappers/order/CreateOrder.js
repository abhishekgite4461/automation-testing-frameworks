import Boom from 'boom'
import Mapper from '../../Mapper'
import transform from '../../transforms/order'
import { sendRequestToApi } from '../../../api'
import { getCookieFromStore } from '../../utils/sessionUtils'
import { clearKlarnaCookies } from './cookies/index'
import { removeAddressDiacritics } from '../../utils/genericUtils'

// Partly Copied from the old (scrApi) monty handler src/server/handlers/order.js.
// This function extracts from the headers the first one in 'x-forwarded-for' which is set by AKAMAI and is necessary in order to be able to pass to the Payment
// Gateways downstream the IP of the Client so that we avoid Fraud detection.
const extractClientIP = (headers) => {
  const header = headers && headers['x-forwarded-for'] // `header` is; '', '217.22.82.162', or '217.22.82.162, 2.20.133.114, ...'
  if (typeof header === 'string' && header.length > 0) {
    return header.split(',')[0]
  }
  return '127.0.0.1'
}

function createVbvForm(wcsResponse, orderCompleteReturnUrl) {
  const { action, MD: md, PaReq: paReq } = wcsResponse

  return (
    `<form method='post' action='${action}'>` +
    `<input type='hidden' name='TermUrl' value='${orderCompleteReturnUrl}'/>` +
    `<input type='hidden' name='MD' value='${md}'/>` +
    `<textarea style='display:none;' name='PaReq'>${paReq}</textarea>` +
    `</form>` +
    `<script language='text/javascript' type='text/javascript'>document.getElementsByTagName('form')[0].submit();</script>`
  )
}

const createBillingPayload = (montyPayload = {}, storeConfig = {}) => {
  const { catalogId, langId, siteId: storeId } = storeConfig
  const {
    orderDeliveryOption: {
      orderId = '',
      deliveryStoreCode = '',
      shippingCountry = '',
    } = {},
    billingDetails: {
      address: {
        address1: billingAddress1 = '',
        address2: billingAddress2 = '',
        city: billingCity = '',
        state: billingState,
        country: billingCountry = '',
        postcode: billingPostcode = '',
      } = {},
      nameAndPhone: {
        firstName: billingFirstName = '',
        lastName: billingLastName = '',
        telephone: billingTelephone = '',
        title: billingTitle = '',
      } = {},
    } = {},
  } = montyPayload

  return {
    storeId,
    catalogId,
    orderId,
    langId,
    shipping_nickName: `Default_${
      deliveryStoreCode ? 'Store' : 'Shipping'
    }_${storeId}`,

    billing_nickName: `Default_Billing_${storeId}`,
    billing_personTitle: billingTitle,
    billing_firstName: billingFirstName,
    billing_lastName: billingLastName,
    billing_phone1: billingTelephone,
    billing_country: removeAddressDiacritics(billingCountry),
    billing_address1: removeAddressDiacritics(billingAddress1),
    billing_address2: removeAddressDiacritics(billingAddress2),
    billing_city: removeAddressDiacritics(billingCity),
    billing_zipCode: billingPostcode,

    billing_state_input:
      billingState !== null && shippingCountry !== 'Canada'
        ? removeAddressDiacritics(billingState)
        : '',
    billing_state_select:
      billingState !== null && shippingCountry !== 'Canada'
        ? removeAddressDiacritics(billingState)
        : '',
    billing_state_select_canada:
      shippingCountry === 'Canada' ? removeAddressDiacritics(billingState) : '',
    billing_state_hidden:
      billingState !== null ? removeAddressDiacritics(billingState) : '',

    // dependant on shipping
    DTS_Shipping: deliveryStoreCode ? 'true' : 'false', // true for store delivery
    preferredLanguage: '',
    preferredCurrency: '',

    // constants
    ShippingCountryInList: 'true', // always true
    proceed: '',
    registerType: 'R',
    returnPage: 'ShoppingBag',
    fromBillingPage: 'y',
    source: 'CHECKOUT',
    isoCode: '',
    page: 'account',
    editRegistration: 'Y',
    editSection: 'CB',
    outOrderItemName: '',
    URL: 'OrderCalculate?URL=OrderPrepare?URL=AddressUpdateAjaxView',
    errorViewName: 'AddressUpdateAjaxView',
    billing_errorViewName: 'DeliveryPaymentPageUpdateAjaxView',
    lookupHouseNumber: '',
    lookupPostcode: '',
    montyUserAction: 'billing',
    subscribe: '',
    addressResults: '1',
  }
}

const createDeliveryPayload = (montyPayload = {}, storeConfig = {}) => {
  const { catalogId, langId, siteId: storeId } = storeConfig
  const {
    orderDeliveryOption: {
      orderId = '',
      // shipModeId = '',
      deliveryStoreCode,
      shippingCountry = '',
    } = {},
    deliveryAddress: {
      address1 = '',
      address2 = '',
      city = '',
      state = '',
      country = '',
      postcode = '',
    } = {},
    deliveryNameAndPhone: {
      firstName = '',
      lastName = '',
      telephone = '',
      title = '',
    } = {},
  } = montyPayload

  return {
    orderId,
    storeId,
    catalogId,
    langId,
    shipping_nickName: `Default_${
      deliveryStoreCode ? 'Store' : 'Shipping'
    }_${storeId}`,
    shipping_personTitle: title,
    shipping_firstName: firstName,
    shipping_lastName: lastName,
    shipping_phone1: telephone,
    shipping_country: removeAddressDiacritics(country),
    shipping_address1: removeAddressDiacritics(address1),
    shipping_address2: removeAddressDiacritics(address2),
    shipping_city: removeAddressDiacritics(city),

    shipping_state_input:
      state !== null && shippingCountry !== 'Canada'
        ? removeAddressDiacritics(state)
        : '',
    shipping_state_select:
      state !== null && shippingCountry !== 'Canada'
        ? removeAddressDiacritics(state)
        : '',
    shipping_state_select_canada:
      shippingCountry === 'Canada' ? removeAddressDiacritics(state) : '',
    shipping_state_hidden: state !== null ? removeAddressDiacritics(state) : '',

    shipping_zipCode: postcode,
    // shipModeId, ? mayby need to pass

    // dependant on shipping
    deliveryOptionType: deliveryStoreCode ? 'S' : 'H', // delivery dependant H home, S for other
    field1: deliveryStoreCode, // store delivery, store id eg TS0001

    // constants
    URL: 'ProcessDeliveryDetails',
    proceed: '',
    registerType: 'R',
    returnPage: 'ShoppingBag',
    isoCode: '',
    page: 'account',
    editRegistration: 'Y',
    editSection: '',
    outOrderItemName: '',
    actionType: 'updateCountryAndOrderItems',
    shipping_errorViewName: 'UserRegistrationForm',
    lookupHouseNumber: '',
    lookupPostcode: '',
    status: 'P',
    errorViewName: 'AddressUpdateAjaxView',
    preferredLanguage: '',
    preferredCurrency: '',
    sourcePage: '',
    montyUserAction: 'shipping',
  }
}

const createGuestUserPayloads = (montyPayload = {}, storeConfig = {}) => {
  const { catalogId, langId, siteId: storeId, currencyCode } = storeConfig
  const {
    orderDeliveryOption: {
      orderId = '',
      shipCode = '',
      shipModeId = '',
      deliveryStoreCode = '',
      shippingCountry = '',
      nominatedDate = '',
    } = {},
    deliveryAddress: {
      address1 = '',
      address2 = '',
      city = '',
      state = '',
      country = '',
      postcode = '',
    } = {},
    deliveryNameAndPhone: {
      firstName = '',
      lastName = '',
      telephone = '',
      title = '',
    } = {},
    deliveryInstructions = '',
    smsMobileNumber = '',
    billingDetails: {
      address: {
        address1: billingAddress1 = '',
        address2: billingAddress2 = '',
        city: billingCity = '',
        state: billingState,
        country: billingCountry = '',
        postcode: billingPostcode = '',
      } = {},
      nameAndPhone: {
        firstName: billingFirstName = '',
        lastName: billingLastName = '',
        telephone: billingTelephone = '',
        title: billingTitle = '',
      } = {},
    } = {},
    accountCreate: {
      email = '',
      password = '',
      passwordConfirm = '',
      subscribe = '',
    } = {},
  } = montyPayload

  const updateDeliveryAddressPayload = {
    smsAlerts: smsMobileNumber,
    orderId,
    storeId,
    catalogId,
    langId,
    shipping_nickName: `Default_${
      deliveryStoreCode ? 'Store' : 'Shipping'
    }_${storeId}`,
    shipping_personTitle: title,
    shipping_firstName: firstName,
    shipping_lastName: lastName,
    shipping_phone1: telephone,
    shipping_country: removeAddressDiacritics(country),
    shipping_address1: removeAddressDiacritics(address1),
    shipping_address2: removeAddressDiacritics(address2),
    shipping_city: removeAddressDiacritics(city),

    shipping_state_input:
      state !== null && shippingCountry !== 'Canada'
        ? removeAddressDiacritics(state)
        : '',
    shipping_state_select:
      state !== null && shippingCountry !== 'Canada'
        ? removeAddressDiacritics(state)
        : '',
    shipping_state_select_canada:
      shippingCountry === 'Canada' ? removeAddressDiacritics(state) : '',
    shipping_state_hidden: state !== null ? removeAddressDiacritics(state) : '',

    shipping_zipCode: postcode,
    shipModeId,
    deliveryInstructions,
    available_date: deliveryStoreCode ? undefined : shipModeId,
    carrier_instructions: deliveryInstructions,
    carrier_mobile: smsMobileNumber,
    orderItemShipMode: shipModeId,
    orderItemShipCode: shipCode,

    // dependant on shipping
    deliveryOptionType: deliveryStoreCode ? 'S' : 'H', // delivery dependant H home, S for other
    field1: deliveryStoreCode, // store delivery, store id eg TS0001

    preferredLanguage: langId,
    preferredCurrency: currencyCode,

    // constants
    URL: 'ProcessDeliveryDetails?actionType=updateDeliveryDetails',
    proceed: 'Y', // Pass Y
    registerType: 'R',
    returnPage: 'ShoppingBag',
    isoCode: '',
    page: 'account',
    editRegistration: 'Y',
    editSection: '',
    outOrderItemName: '',
    actionType: 'updateCountryAndOrderItems',
    sourcePage: 'DeliveryPage',
    shipping_errorViewName: 'UserRegistrationForm',
    lookupHouseNumber: '',
    lookupPostcode: '',
    addressResults: '',
    status: 'P',
    errorViewName: 'UserRegistrationForm',
    nominatedDate,
    redirectPageChange: 'Y',
    montyUserAction: 'shipping',
  }

  const registerPayload = email
    ? {
        create_logonId: email,
        logonPassword: password,
        logonPasswordVerify: passwordConfirm,
        subscribe: subscribe ? 'YES' : 'NO',
      }
    : {}
  const billingAddressPayload = {
    ...registerPayload,
    storeId,
    catalogId,
    orderId,
    langId,
    shipping_nickName: `Default_${
      deliveryStoreCode ? 'Store' : 'Shipping'
    }_${storeId}`,
    shipping_personTitle: title,
    shipping_firstName: firstName,
    shipping_lastName: lastName,
    shipping_phone1: telephone,
    shipping_phone2: smsMobileNumber,
    shipping_country: removeAddressDiacritics(country),
    shipping_address1: removeAddressDiacritics(address1),
    shipping_address2: removeAddressDiacritics(address2),
    shipping_state: state !== null ? removeAddressDiacritics(state) : '',
    shipping_city: removeAddressDiacritics(city),
    shipping_zipCode: postcode,

    saved_title: title,
    saved_firstName: firstName,
    saved_lastName: lastName,
    saved_addressLine1: removeAddressDiacritics(address1),
    saved_addressLine2: removeAddressDiacritics(address2),
    saved_townCity: removeAddressDiacritics(city),
    saved_state: state !== null ? removeAddressDiacritics(state) : '',
    saved_postcode: postcode,
    saved_country: removeAddressDiacritics(country),
    saved_telephone: telephone,

    billing_nickName: `Default_Billing_${storeId}`,
    billing_personTitle: billingTitle,
    billing_firstName: billingFirstName,
    billing_lastName: billingLastName,
    billing_phone1: billingTelephone,
    billing_country: removeAddressDiacritics(billingCountry),
    billing_address1: removeAddressDiacritics(billingAddress1),
    billing_address2: removeAddressDiacritics(billingAddress2),
    billing_city: removeAddressDiacritics(billingCity),
    billing_zipCode: billingPostcode,

    billing_state_input:
      billingState !== null && shippingCountry !== 'Canada'
        ? removeAddressDiacritics(billingState)
        : '',
    billing_state_select:
      billingState !== null && shippingCountry !== 'Canada'
        ? removeAddressDiacritics(billingState)
        : '',
    billing_state_select_canada:
      shippingCountry === 'Canada' ? removeAddressDiacritics(billingState) : '',
    billing_state_hidden:
      billingState !== null ? removeAddressDiacritics(billingState) : '',

    // dependant on shipping
    DTS_Shipping: deliveryStoreCode ? 'true' : 'false', // true for store delivery
    preferredLanguage: langId,
    preferredCurrency: currencyCode,

    // constants
    ShippingCountryInList: 'true', // always true
    proceed: 'Y',
    registerType: 'R',
    returnPage: 'ShoppingBag',
    fromBillingPage: 'y',
    source: 'CHECKOUT',
    isoCode: '',
    page: 'account',
    editRegistration: 'Y',
    editSection: 'NA',
    outOrderItemName: '',
    shipping_addressType: 'SB', // SB for ever
    shipping_primary: '0',
    URL: 'OrderCalculate?URL=OrderPrepare?URL=PaymentPageUpdateAjaxView',
    errorViewName: 'PaymentPageUpdateAjaxView',
    billing_errorViewName: 'UserRegistrationForm',
    lookupHouseNumber: '',
    lookupPostcode: '',
    montyUserAction: 'billing',
  }

  return {
    updateDeliveryAddressPayload,
    billingAddressPayload,
  }
}

export default class CreateOrder extends Mapper {
  mapEndpoint() {
    this.destinationEndpoint = !this.hasCheckoutProfile
      ? '/webapp/wcs/stores/servlet/ConfirmAndPay'
      : '/webapp/wcs/stores/servlet/OrderCalculate'
    this.method = 'post'
  }

  mapRequestParameters() {
    const userAgent = (this.headers && this.headers['user-agent']) || ''

    const { catalogId, langId, siteId: storeId } = this.storeConfig

    // {"smsMobileNumber":"","remoteIpAddress":"127.0.0.1","cardCvv":"123","orderDeliveryOption":{"orderId":2195209,"shippingCountry":"United Kingdom","shipCode":"S","deliveryType":"HOME_STANDARD","shipModeId":26504},"deliveryInstructions":"","returnUrl":"http://local.m.topshop.com:8080/order-complete-v1?paymentMethod=VISA","cardNumberHash":"tjOBl4zzS+ueTZQWartO5l968iOmCOix"}
    const {
      cardNumberHash,
      cardCvv,
      orderDeliveryOption: { orderId, shipCode, nominatedDate = '' } = {},
      deliveryInstructions,
      authToken,
      returnUrl,
      smsMobileNumber = '',
      paymentToken,
    } = this.payload

    if (paymentToken && typeof paymentToken !== 'string')
      throw Boom.badRequest(
        'paymentToken must be a JSON string with header (keys, transaction id and hash), signature, version and data'
      )

    // saving it before the overwriting of this.payload. We need to use it in the response when we will create the vbv form.
    this.orderCompleteReturnUrl = returnUrl

    let cardBrand = (this.payload.creditCard || {}).type
    const {
      expiryYear: cardExpiryYear,
      expiryMonth: cardExpiryMonth,
      cardNumber: cardNumberStar,
    } =
      this.payload.creditCard || {}
    this.payload = {
      ipAddress: extractClientIP(this.headers),
      orderId,
      userAgent,
      deliveryInstructions,
      shipCode,
      langId,
      storeId,
      catalogId,
      errorViewName: 'DoPaymentErrorView',
      contentType: '*/*',
      acceptHeader: '*/*',
      notifyShopper: '0',
      notifyOrderSubmitted: '0',
      smsAlerts: smsMobileNumber, // need logic to set this, what is needed 'y', 'yes', 'true'?
      nominatedDate,
      TERMSANDCONDITIONS_OPTIN: 'true',
      // save_details is a new property in the incoming request payload,
      // by default set the outgoing request save_details = 'on' to maintain
      // current behaviour
      save_details: this.payload.save_details === false ? undefined : 'on',
      auth_token: authToken,
      billing_address_id: this.billingAddressId, // need if new address
      addressId: this.addressId, // need if new address
      paymentToken,
    }

    if (cardCvv && cardCvv !== '0') {
      // cardSecurityNumber was part of the main payload and unconditionally sent to WCS in the request.
      // Moved here to follow Cogz suggestion in order to make it work the create Order with Gift Card.

      this.payload = {
        ...this.payload,
        cardSecurityNumber: cardCvv,
      }
    }

    if (this.hasCheckoutProfile) {
      // URL was part of the main payload and unconditionally sent to WCS in the request.
      // Moved here to follow Cogz suggestion in order to make it work the create Order with Gift Card.

      this.payload = {
        ...this.payload,
        URL: 'OrderPrepare?URL=ConfirmAndPay',
      }
    }

    const isPayPal = /paymentMethod=PYPAL/.test(returnUrl)
    const isMasterpass = /paymentMethod=MPASS/.test(returnUrl)

    if (paymentToken) {
      cardBrand = 'APPLE'
    }

    const card =
      isPayPal || isMasterpass
        ? {
            cardBrand: isPayPal ? 'PYPAL' : 'MPASS',
            // Once the Dual Run starts the mobile parameter will be a bit confusing given that it will contain the return URL
            // for both monty-mobile and monty-desktop.
            // This parameter should be renamed to "returnUrl" but before to do that
            // a modification on WCS side needs to be applied.
            mobile: returnUrl,
          }
        : {
            cardExpiryYear,
            cardExpiryMonth,
            cardNumberStar,
            cardBrand,
            cardNumber: cardNumberHash,
          }

    this.payload = {
      ...this.payload,
      ...card,
    }

    // Exclude KLRNA and ACCNT payments from getting a mobile url as they don't redirect
    if (!/.*\b(KLRNA|ACCNT|APPLE).*$/i.test(returnUrl)) {
      this.payload.mobile = returnUrl
    }

    this.query = {}
  }

  mapResponseBody(body) {
    const { currencySymbol, currencyCode } = this.storeConfig
    if (body.errorMessage) this.mapResponseError(body)
    if (body.confirmationTitle) {
      //
      // KLARNA, ACCOUNT CARD
      //

      return transform(body, currencySymbol, currencyCode)
    } else if (body.cardBrand === 'PYPAL' || body.cardBrand === 'MPASS') {
      //
      // PAYPAL, MASTERPASS
      //

      const { punchOutRepayURL, paymentRedirectURL } = body

      return sendRequestToApi(
        this.destinationHostname,
        `/webapp/wcs/stores/servlet/${punchOutRepayURL}`,
        {},
        {},
        'post',
        this.headers
      ).then((res) => {
        if (!res || !res.body || !res.body.orderId)
          throw Boom.badGateway(
            'create order (PayPal): unexpected response for punchOutRepayURL request'
          )

        return {
          paypalUrl: paymentRedirectURL,
          paymentUrl: paymentRedirectURL,
        }
      })
    } else if (body.formEncoded) {
      //
      // 3D VISA, MASTERCARD, AMEX
      //

      const { TermUrl: originalTermUrl, piId, punchOutRepayURL } = body

      return sendRequestToApi(
        this.destinationHostname,
        `/webapp/wcs/stores/servlet/${punchOutRepayURL}`,
        {},
        {},
        'post',
        this.headers
      )
        .then((res) => {
          if (!res || !res.body || !res.body.orderId)
            throw Boom.badGateway(
              'create order (3D): unexpected response for punchOutRepayURL request'
            )

          return {
            vbvForm: {
              // "termUrl":"http://local.m.topshop.com:8080/order-complete?paymentMethod=MCARD"
              // "termUrl":"http://local.m.topshop.com:8080/order-complete?paymentMethod=AMEX"
              termUrl: this.orderCompleteReturnUrl,
              originalTermUrl,
              vbvForm: createVbvForm(body, this.orderCompleteReturnUrl),
              piId,
            },
          }
        })
        .catch((err) => {
          throw Boom.badGateway(err)
        })
    } else if (
      body.paymentRedirectURL &&
      /^.*\b(ALIPY|SOFRT|CUPAY|IDEAL).*$/i.test(body.cardBrand)
    ) {
      //
      // SOFRT, ALIPY, CUPAY, IDEAL
      //
      return sendRequestToApi(
        this.destinationHostname,
        `/webapp/wcs/stores/servlet/PunchoutPaymentRepay?orderId=${
          body.orderId
        }&piId=${body.piId}&requesttype=ajax`,
        {},
        {},
        'post',
        this.headers
      )
        .then((res) => {
          if (res.body.errorMessage) {
            throw res.body
          }
          if (!res || !res.body || !res.body.orderId) {
            throw Boom.badGateway(
              `create order ${
                body.cardBrand
              } : unexpected response for punchOutRepayURL request`
            )
          }
          return { paymentUrl: body.paymentRedirectURL } // This triggers Monty client to redirect to that site.
        })
        .catch((err) => {
          return this.mapResponseError(err)
        })
    } else if (body.paymentRedirectURL) {
      //
      // no 3D VISA, MASTERCARD, AMEX, MASTERO
      //
      const { punchOutRepayURL, paymentRedirectURL } = body

      return sendRequestToApi(
        this.destinationHostname,
        `/webapp/wcs/stores/servlet/${punchOutRepayURL}`,
        {},
        {},
        'post',
        this.headers
      )
        .then((res) => {
          if (!res || !res.body || !res.body.orderId)
            throw Boom.badGateway(
              'create order (no 3D): unexpected response for punchOutRepayURL request'
            )

          return sendRequestToApi(
            paymentRedirectURL,
            '',
            {},
            {},
            'get',
            this.headers,
            undefined,
            true
          )
        })
        .then((res) => {
          if (!res || !res.body || !res.body.redirectURL)
            throw Boom.badGateway(
              'create order (no 3D): unexpected response for paymentRedirectURL request'
            )

          return sendRequestToApi(
            res.body.redirectURL,
            '',
            {},
            {},
            'get',
            this.headers,
            undefined,
            true
          )
        })
        .then((res) => {
          if (!res || !res.body || !res.body.OrderConfirmation)
            throw Boom.badGateway(
              'create order (no 3D): unexpected OrderConfirmation response'
            )

          return transform(res.body, currencySymbol, currencyCode)
        })
        .catch((err) => {
          throw Boom.badGateway(err)
        })
    }
    // If none of the response conditions are met, return a 422.
    return this.mapResponseError(body)
  }

  mapResponseError(body = {}) {
    throw body.errorMessage ? Boom.badData(body.errorMessage) : body
  }

  mapResponse(res = {}) {
    return {
      jsessionid: res.jsessionid,
      body: this.mapResponseBody(res.body),
      // Klarna cookies are created even if a user chooses Klarna and then chooses another method. They should always be cleared.
      setCookies: clearKlarnaCookies(),
    }
  }

  async registerGuest() {
    const {
      updateDeliveryAddressPayload,
      billingAddressPayload,
    } = createGuestUserPayloads(this.payload, this.storeConfig)

    const {
      body: {
        success: updateDeliverySuccess,
        orderSummary: updateDeliveryOrderSummary,
        errorMessage: updateDeliveryErrorMessage = '',
        ConfirmAndPayCardDetailsForm: { addressId = '' } = {},
      },
    } = await sendRequestToApi(
      this.destinationHostname,
      `/webapp/wcs/stores/servlet/UserRegistrationUpdate`,
      {},
      updateDeliveryAddressPayload,
      'post',
      this.headers
    )
    // In some error cases WCS returns an orderSummary object with an error message.
    if (updateDeliveryOrderSummary && updateDeliveryOrderSummary.errorMessage) {
      this.mapResponseError(updateDeliveryOrderSummary)
    } else if (updateDeliverySuccess === false || !addressId) {
      this.mapResponseError(
        updateDeliveryErrorMessage || {
          errorMessage: 'Could not add delivery address',
        }
      )
    }
    this.addressId = addressId

    const {
      body: {
        success: registerSuccess = '',
        orderSummary: updateBillingOrderSummary,
        errorMessage: registerErrorMessage = '',
        ConfirmAndPayCardDetailsForm: {
          billing_address_id: billingAddressId = '',
        } = {},
      },
    } = await sendRequestToApi(
      this.destinationHostname,
      `/webapp/wcs/stores/servlet/UserRegistrationUpdate`,
      {},
      billingAddressPayload,
      'post',
      this.headers
    )
    // In some error cases WCS returns an orderSummary object with an error message.
    if (updateBillingOrderSummary && updateBillingOrderSummary.errorMessage) {
      this.mapResponseError(updateBillingOrderSummary)
    } else if (registerSuccess === false || !billingAddressId) {
      this.mapResponseError(registerErrorMessage || 'Could not register user')
    }
    this.billingAddressId = billingAddressId
  }

  async changeDeliveryAddress() {
    const {
      body: {
        success = '',
        errorMessage = '',
        orderSummary: {
          OrderCalculateForm: { addressId = '' },
          errorMessage: orderSummaryErrorMessage = '',
        },
        orderSummary,
      },
    } = await sendRequestToApi(
      this.destinationHostname,
      `/webapp/wcs/stores/servlet/UserRegistrationUpdate`,
      {},
      createDeliveryPayload(this.payload, this.storeConfig),
      'post',
      this.headers
    )

    if (orderSummaryErrorMessage) {
      this.mapResponseError(orderSummary)
    } else if (success === false || !addressId) {
      this.mapResponseError(
        { errorMessage } || { errorMessage: 'Could not add delivery address' }
      )
    }
    this.addressId = addressId
  }

  async changeBillingAddress() {
    const {
      body: {
        success = '',
        errorMessage = '',
        orderSummary: {
          OrderCalculateForm: {
            billing_address_id: billingAddressId = '',
          } = {},
          errorMessage: orderSummaryErrorMessage = '',
        } = {},
        orderSummary,
      },
    } = await sendRequestToApi(
      this.destinationHostname,
      `/webapp/wcs/stores/servlet/UserRegistrationUpdate`,
      {},
      createBillingPayload(this.payload, this.storeConfig),
      'post',
      this.headers
    )

    if (orderSummaryErrorMessage) {
      this.mapResponseError(orderSummary)
    } else if (success === false || !billingAddressId) {
      this.mapResponseError({ errorMessage } || 'Could not add billing address')
    }
    this.billingAddressId = billingAddressId
  }

  async execute() {
    const checkoutProfileValue = await getCookieFromStore(
      'profileExists',
      this.headers.cookie
    )

    this.hasCheckoutProfile =
      !this.payload.accountCreate && checkoutProfileValue === 'Y'
    if (
      !this.hasCheckoutProfile &&
      this.payload.deliveryAddress &&
      this.payload.billingDetails
    ) {
      await this.registerGuest()
    } else {
      if (this.payload.deliveryAddress) {
        await this.changeDeliveryAddress()
      }
      if (this.payload.billingDetails) {
        await this.changeBillingAddress()
      }
    }
    return super.execute()
  }
}

export const createOrderSpec = {
  summary: 'Create an order',
  parameters: [
    {
      name: 'payload',
      in: 'body',
      required: true,
      description:
        'The details of the order to be processed. The following properties are optional and are sent depending on the user flow: cardNumberHash, cardCvv, deliveryAddress, deliveryNameAndPhone, billingDetails, accountCreate, creditCard, authToken',
      schema: {
        type: 'object',
        properties: {
          paymentToken: {
            type: 'string',
          },
          cardNumberHash: {
            type: 'string',
          },
          remoteIpAddress: {
            type: 'string',
          },
          cardCvv: {
            type: 'string',
          },
          returnUrl: {
            type: 'string',
          },
          orderDeliveryOption: {
            type: 'object',
            properties: {
              orderId: {
                type: 'string',
              },
              shipCode: {
                type: 'string',
              },
              shipModeId: {
                type: 'string',
              },
              deliveryStoreCode: {
                type: 'string',
              },
              shippingCountry: {
                type: 'string',
              },
            },
          },
          deliveryAddress: {
            type: 'object',
            properties: {
              address1: {
                type: 'string',
              },
              address2: {
                type: 'string',
              },
              city: {
                type: 'string',
              },
              state: {
                type: 'string',
              },
              country: {
                type: 'string',
              },
              postcode: {
                type: 'string',
              },
            },
          },
          deliveryNameAndPhone: {
            type: 'object',
            properties: {
              firstName: {
                type: 'string',
              },
              lastName: {
                type: 'string',
              },
              telephone: {
                type: 'string',
              },
              title: {
                type: 'string',
              },
            },
          },
          deliveryInstructions: {
            type: 'string',
          },
          smsMobileNumber: {
            type: 'string',
          },
          billingDetails: {
            type: 'object',
            properties: {
              address: {
                type: 'object',
                properties: {
                  address1: {
                    type: 'string',
                  },
                  address2: {
                    type: 'string',
                  },
                  city: {
                    type: 'string',
                  },
                  state: {
                    type: 'string',
                  },
                  country: {
                    type: 'string',
                  },
                  postcode: {
                    type: 'string',
                  },
                },
              },
              nameAndPhone: {
                type: 'object',
                properties: {
                  firstName: {
                    type: 'string',
                  },
                  lastName: {
                    type: 'string',
                  },
                  telephone: {
                    type: 'string',
                  },
                  title: {
                    type: 'string',
                  },
                },
              },
              accountCreate: {
                type: 'object',
                properties: {
                  email: {
                    type: 'string',
                  },
                  password: {
                    type: 'string',
                  },
                  passwordConfirm: {
                    type: 'string',
                  },
                  subscribe: {
                    type: 'string',
                  },
                },
              },
              creditCard: {
                type: 'object',
                properties: {
                  expiryYear: {
                    type: 'string',
                  },
                  expiryMonth: {
                    type: 'string',
                  },
                  cardNumber: {
                    type: 'string',
                  },
                  type: {
                    type: 'string',
                  },
                },
                authToken: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  ],
  responses: {
    200: {
      description:
        'In cases where no third party verification is required, the successful response will be a completed order object. If third party authentication is required, the response will contain a paymentRedirectUrl which the client will automatically use to redirect the user.',
      schema: {
        type: 'object',
        properties: {
          completedOrder: {
            type: 'object',
            properties: {
              billingAddress: {
                type: 'object',
                properties: {
                  address1: {
                    type: 'string',
                    example: '123 Sesame Street',
                  },
                  address2: {
                    type: 'string',
                    example: '',
                  },
                  address3: {
                    type: 'string',
                    example: 'London',
                  },
                  country: {
                    type: 'string',
                    example: 'United Kingdeom',
                  },
                  name: {
                    type: 'string',
                    example: 'Bob Barker',
                  },
                },
              },
              currencyConversion: {
                type: 'object',
                properties: {
                  currencyRate: {
                    type: 'string',
                    example: 'GBP',
                  },
                },
              },
              deliveryAddress: {
                type: 'object',
                properties: {
                  address1: {
                    type: 'string',
                    example: '1 Oxford Circus',
                  },
                  address2: {
                    type: 'string',
                    example: 'West End',
                  },
                  address3: {
                    type: 'string',
                    example: 'London',
                  },
                  country: {
                    type: 'string',
                    example: 'United Kingdom',
                  },
                  name: {
                    type: 'string',
                    example: 'Bob Barker',
                  },
                },
              },
              deliveryCarrier: {
                type: 'string',
                example: '',
              },
              deliveryCost: {
                type: 'string',
                example: '4.00',
              },
              deliveryDate: {
                type: 'string',
                example: '12 January 2018',
              },
              deliveryMethod: {
                type: 'string',
                example: 'Standard Delivery',
              },
              deliveryPrice: {
                type: 'string',
                example: '4.00',
              },
              orderId: {
                type: 'number',
                example: 700381254,
              },
              orderLines: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    colour: {
                      type: 'string',
                      example: 'BLUE',
                    },
                    discount: {
                      type: 'string',
                      example: '10.00',
                    },
                    discountPrice: {
                      type: 'string',
                      example: '30.00',
                    },
                    imageUrl: {
                      type: 'string',
                      example: 'https://stage.topshop.com/image',
                    },
                    lineNo: {
                      type: 'string',
                      example: '1A1A1A',
                    },
                    name: {
                      type: 'string',
                      example: 'Blue dress',
                    },
                    nonRefundable: {
                      type: 'boolean',
                      example: false,
                    },
                    quantity: {
                      type: 'quantity',
                      example: 1,
                    },
                    size: {
                      type: 'string',
                      example: '12',
                    },
                    total: {
                      type: 'string',
                      example: '30.00',
                    },
                    unitPrice: {
                      type: 'string',
                      example: '40.00',
                    },
                  },
                },
              },
              paymentDetails: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    cardNumberStar: {
                      type: 'string',
                      example: '************1111',
                    },
                    paymentMethod: {
                      type: 'string',
                      example: 'VISA',
                    },
                    totalCost: {
                      type: 'string',
                      example: '34.00',
                    },
                  },
                },
              },
              returnPossible: {
                type: 'boolean',
                example: false,
              },
              returnRequested: {
                type: 'boolean',
                example: false,
              },
              returning_buyer: {
                type: 'boolean',
                example: true,
              },
              subTotal: {
                type: 'string',
                example: '30.00',
              },
              totalOrderPrice: {
                type: 'string',
                example: '34.00',
              },
              totalOrdersDiscount: {
                type: 'string',
                example: '10.00',
              },
              totalOrdersDiscountLabel: {
                type: 'string',
                example: '',
              },
            },
          },
        },
        paymentRedirectUrl:
          'http://foo.bar.com/authorisepayment&token=whatever',
      },
    },
  },
}
