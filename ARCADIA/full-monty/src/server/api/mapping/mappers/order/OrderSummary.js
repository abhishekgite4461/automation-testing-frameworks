import { path } from 'ramda'

import Mapper from '../../Mapper'
import transform, {
  throwForOutOfStock,
  throwForFailedSummary,
} from '../../transforms/orderSummary'
import basketTransform from '../../transforms/basket'
import { sendRequestToApi } from '../../../api'

import { orderSummaryConstants } from '../../constants/orderSummary'
import { getOrderId } from '../../utils/cartUtils'

export default class OrderSummary extends Mapper {
  constructor(originEndpoint, query, payload, method, headers, params) {
    super(originEndpoint, query, payload, method, headers, params)
    this.isGuest = false
  }

  mapEndpoint() {
    this.destinationEndpoint = '/webapp/wcs/stores/servlet/PreCheckout'
  }

  async mapRequestParameters() {
    const { catalogId, langId, siteId: storeId } = this.storeConfig
    const orderId = await getOrderId(this.headers.cookie)

    this.method = 'post'
    this.payload = {
      catalogId,
      langId,
      storeId,
      orderId,
      ...orderSummaryConstants,
    }

    this.payload = { ...this.payload, isTempUserNotRqrd: true }
  }

  mapResponseBody(body = {}) {
    // It is possible that WCS returns a basket shaped response to answer an order summary request:
    // an example is given by the scenario where the User proceeds to checkout (GET order summar) and one of
    // the products went out of stock.
    // (!) We don't include this logic in the order summary transformer because in this case the response is not shaped as an order summary.
    try {
      throwForOutOfStock(path(['Basket', 'products', 'Product'], body))
      throwForFailedSummary(body)
    } catch (error) {
      error.output.payload.data = {
        basket: basketTransform(body.Basket || {}),
      }

      throw error
    }

    if (!this.isGuest) {
      if (
        !body ||
        !body.orderSummary ||
        typeof body.orderSummary !== 'object' ||
        (body.success === false && !body.orderSummary)
      ) {
        throw new Error('wcsSessionTimeout')
      }
    }

    // If we are here it means that the response from WCS has the shape of a proper order summary
    return transform(
      body.orderSummary,
      this.isGuest,
      this.storeConfig.currencySymbol
    )
  }

  async execute() {
    try {
      await this.mapRequestParameters()
    } catch (err) {
      if (!this.isGuest) {
        // Covering in this way the scenario where the User cleared the cookies and hence we don't have the jsessionid
        // to retrieve the cookies which are needed to create the payload for WCS.
        // Forcing the User to login again and start from scratch.
        throw new Error('wcsSessionTimeout')
      }
      throw err
    }

    this.mapEndpoint()

    return sendRequestToApi(
      this.destinationHostname,
      this.destinationEndpoint,
      this.query,
      this.payload,
      this.method,
      this.headers
    )
      .then((res) => {
        // Commented because this is checkout V1 logic. We do not remove just in case of need to reintroduce it
        // for full guest checkout.
        //
        // const temporarySigninEndpoint =
        //   res && res.body && res.body.tempSignInURL

        // if (temporarySigninEndpoint) {
        //   // In this case the User is a Guest and we need to make another call to WCS so
        //   // that it creates a temporary account for the User in order to permit them to
        //   // proceed in the checkout journey even if not logged in yet.

        //   this.isGuest = true

        //   return sendRequestToApi(
        //     this.destinationHostname,
        //     `/webapp/wcs/stores/servlet/${temporarySigninEndpoint}`,
        //     {},
        //     {},
        //     'get',
        //     this.headers
        //   )
        // }

        return res
      })
      .then((res) => {
        return this.mapResponse(res)
      })
      .catch((err) => {
        if (err.message === 'wcsSessionTimeout') throw err

        if (!this.isGuest && !err.isBoom) {
          // Forcing the User to start from scratch.
          // This is a temporary solution due to the fact that sometimes we observed the User to reach a strange state on WCS.
          // Boom errors that are caught need to be excluded from this block so that they are mapped normally.
          throw new Error('wcsSessionTimeout')
        }
        return this.mapResponseError(err)
      })
  }
}

export const orderSummarySpec = {
  summary: 'Get the summary of an order to be processed.',
  parameters: [],
  responses: {
    200: {
      description: 'Order Summary object',
      schema: {
        $ref: '#/definitions/orderSummary',
      },
    },
    422: {
      description: 'Items out of stock or are restricted from delivery',
      schema: {
        type: 'object',
        properties: {
          statusCode: {
            type: 'number',
            example: 422,
          },
          error: {
            type: 'text',
            example: 'Unprocessable Entity',
          },
          message: {
            type: 'text',
            example:
              'There are items in your basket that are now out of stock, remove these to continue your order',
          },
          data: {
            type: 'object',
            description:
              'Used for failed requests to return data which may still be useable, such as the basket state',
            properties: {
              basket: {
                $ref: '#/definitions/basket',
              },
            },
          },
        },
      },
    },
  },
}
