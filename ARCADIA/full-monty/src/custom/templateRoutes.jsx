/* eslint-disable no-undef */
// TODO remove System and eslint-disable

import React from 'react'
import Async from '../shared/components/common/Async/Async'
import Quiz from '../shared/components/containers/Quiz/Quiz'
import Carousel from '../shared/components/common/Carousel/Carousel'

export const getTemplateComponent = (template, props = {}) => {
  if (template === 'quiz') return <Quiz {...props} />

  if (template === 'lfw16')
    return (
      <Async
        id={template}
        getFile={() => System.import('../custom/Lfw16/Lfw16.jsx')}
        {...props}
      />
    )

  if (template === 'lfwcarouselblog')
    return (
      <Carousel
        {...props}
        name="lfw"
        mode="social"
        template={template}
        type="blog"
      />
    )

  return false
}

export default {
  getTemplateComponent,
}
