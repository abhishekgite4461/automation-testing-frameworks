import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import CmsComponent, {
  cmsTypes,
} from '../../shared/components/common/CmsComponent/CmsComponent'
import TabTitle from './tabtitle/TabTitle'
import Showspace from './showspace/Showspace'
import CountDown from './countdown/CountDown'
import NewsLetter from './newsletter/NewsLetter'
import BlogFeed from './blogfeed/BlogFeed'
import Carousel from '../../shared/components/common/Carousel/Carousel'
import * as carouselActions from '../../shared/actions/common/carousel-actions'
import * as actions from '../../shared/actions/components/campaignActions'

if (process.browser) {
  // eslint-disable-next-line import/no-webpack-loader-syntax, import/no-unresolved, import/no-extraneous-dependencies
  require('file-loader?name=topshop/custom/lfw16/lfw16.css!./Lfw16.css')
}

@connect(
  ({ campaigns: { lfw } }) => ({
    socialFeed: lfw.socialFeed,
  }),
  { ...carouselActions, ...actions }
)
export default class Lfw16 extends Component {
  static propTypes = {
    pageContent: PropTypes.object,
    socialFeed: PropTypes.array,
    getCampaignSocialFeed: PropTypes.func,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentDidMount() {
    this.props.getCampaignSocialFeed('lfw')
    window.addEventListener('message', this.resizeIfame)
  }

  componentWillUnmount() {
    window.removeEventListener('message', this.resizeIfame)
  }

  getCustomComponent = ({ name, key, data, data: { template } }) => {
    let dataMarkup
    try {
      dataMarkup = JSON.parse(data.markup)
    } catch (e) {
      dataMarkup = false
    }

    const renderFn = {
      social: () => this.renderSocial(name, data),
      showspace: () => this.renderShowspace(name, dataMarkup),
      countdown: () => this.renderCountdown(name, data),
      newsletter: () => this.renderNewsletter(name, data),
      blogfeed: () => this.renderBlogFeed(name, data),
      tabtitle: () => this.renderTabTitle(dataMarkup),
    }

    return renderFn[template] ? (
      <div key={key}>{renderFn[template]()}</div>
    ) : null
  }

  getComponent = (setProps) => {
    if (setProps.type === 'custom') {
      return this.getCustomComponent(setProps)
    } else if (cmsTypes.indexOf(setProps.type) > -1) {
      return <CmsComponent {...setProps} />
    }
    return null
  }

  resizeIfame(message) {
    // Resizing the Product Lightboxes iframe height in case of "Load More/Less" products.
    // The Flux iframe posts a message with the new height of the iframe every time the User
    // clicks "Load More/Less".
    const iframe = document.querySelector('#lfw16 .CmsComponent--iframe')

    if (
      iframe &&
      message.data &&
      message.data.height &&
      !isNaN(message.data.height)
    ) {
      if (message.data.height < iframe.height) {
        window.scrollTo(0, iframe.offsetTop)
      }
      iframe.height = message.data.height
    }
  }

  renderSocial = (name) => {
    return (
      <section className={`SocialCarousel ${name}`}>
        <Carousel mode="social" name="lfw" assets={this.props.socialFeed} />
      </section>
    )
  }

  renderTabTitle(data) {
    const { title, icon, name } = data
    return data ? <TabTitle title={title} icon={icon} name={name} /> : null
  }

  renderBlogFeed(name) {
    return (
      <section className={name}>
        <BlogFeed />
      </section>
    )
  }

  renderShowspace(name, data) {
    return (
      <section className={name}>
        <Showspace
          uniqueShopLink={
            data && data.uniqueShopLink ? data.uniqueShopLink : ''
          }
        />
      </section>
    )
  }

  renderCountdown(name, { markup }) {
    return (
      <section className={name}>
        <CountDown markup={markup} />
      </section>
    )
  }

  renderNewsletter(name) {
    return (
      <section className={name}>
        <NewsLetter />
      </section>
    )
  }

  render() {
    const {
      pageContent: { pageData },
    } = this.props

    if (!Array.isArray(pageData)) return null

    const items = pageData.reduce((result, { type, data }, key) => {
      const componentProps = { name: `lfw16${key}`, key, type, data }
      const comp = this.getComponent(componentProps)
      return comp ? result.concat(comp) : result
    }, [])

    return <div className="Lfw16">{items}</div>
  }
}
