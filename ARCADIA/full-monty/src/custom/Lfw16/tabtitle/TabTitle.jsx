import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Image from '../../../shared/components/common/Image/Image'

if (process.browser) {
  // eslint-disable-next-line import/no-webpack-loader-syntax, import/no-unresolved, import/no-extraneous-dependencies
  require('file-loader?name=topshop/custom/lfw16/tabtitle.css!./TabTitle.css')
}

export default class TabTitle extends Component {
  static propTypes = {
    title: PropTypes.string,
    icon: PropTypes.string,
    name: PropTypes.string,
  }

  render() {
    const { title, icon, name } = this.props

    return (
      <div className="TabTitle">
        <h3 className={`TabTitle-text TabTitle--${name}`}>
          <Image
            alt={`Topshop LFW16 ${name} icon`}
            className="TabTitle-icon"
            src={icon}
          />{' '}
          {title}
        </h3>
      </div>
    )
  }
}
