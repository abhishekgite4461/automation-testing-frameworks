import React, { Component } from 'react'
import Image from '../../../common/Image/Image' // eslint-disable-line import/no-unresolved, import/extensions

export default class Showspace extends Component {
  constructor() {
    super()
    this.state = {
      activeItem: 0,
    }
  }

  selectPin(index) {
    this.setState({ activeItem: index })
  }

  incrementSpaceIndex() {
    const currentIndex = this.state.activeItem
    this.setState({ activeItem: (currentIndex + 1) % 4 })
  }

  //
  // With this approach we could save a lot of lines of code but the transaction between highlighted
  // areas does not feel smooth at all. For every change of the state React has to remove a part of
  // the DOM and substitute it with the one associated with the highlighted area. With the verbose
  // approach instead (the current one) React has just to modify the classnames of the images without
  // any modification to the structure of the DOM.
  //
  // renderCurrentHighlightedArea() {
  //   const current = this.state.activeItem
  //
  //   return (
  //     <div className="Showspace-image">
  //       <Image
  //         className={`Showspace-mapimage${current}`}
  //         src={`/assets/custom/lfw16/images/mapimage${current}.png`}
  //       />
  //     </div>
  //   )
  // }

  render() {
    return (
      <div className="Interactivemap">
        <div className="Showspace">
          <div className="Showspace-image">
            <Image
              className="Showspace-mapbackimage"
              src="/assets/custom/lfw16/images/phase1-map-back.png"
            />
          </div>
          {/* --- Highlighted Areas --- */}
          <div className="Showspace-image">
            <Image
              className={`Showspace-mapimage0 ${
                this.state.activeItem !== 0 ? 'is-hidden' : ''
              }`}
              src={`/assets/custom/lfw16/images/mapimage0.png`}
            />
          </div>
          <div className="Showspace-image">
            <Image
              className={`Showspace-mapimage1 ${
                this.state.activeItem !== 1 ? 'is-hidden' : ''
              }`}
              src={`/assets/custom/lfw16/images/mapimage1.png`}
            />
          </div>
          <div className="Showspace-image">
            <Image
              onClick={() => {
                this.state.activeItem = 2
              }}
              className={`Showspace-mapimage2 ${
                this.state.activeItem !== 2 ? 'is-hidden' : ''
              }`}
              src={`/assets/custom/lfw16/images/mapimage2.png`}
            />
          </div>
          <div className="Showspace-image">
            <Image
              className={`Showspace-mapimage3 ${
                this.state.activeItem !== 3 ? 'is-hidden' : ''
              }`}
              src={`/assets/custom/lfw16/images/mapimage3.png`}
            />
          </div>
          {/* --- Pins --- */}
          <div // eslint-disable-line jsx-a11y/no-static-element-interactions
            className="Showspace-pin"
            onClick={() => this.selectPin(0)}
          >
            <Image
              className={`Showspace-pinimage0 ${
                this.state.activeItem !== 0 ? 'small' : ''
              }`}
              src={`/assets/custom/lfw16/images/pin${
                this.state.activeItem !== 0 ? 'Inactive' : 'Active'
              }.svg`}
            />
          </div>
          <div // eslint-disable-line jsx-a11y/no-static-element-interactions
            className="Showspace-pin"
            onClick={() => this.selectPin(1)}
          >
            <Image
              onClick={() => {
                this.state.activeItem = 1
              }}
              className={`Showspace-pinimage1 ${
                this.state.activeItem !== 1 ? 'small' : ''
              }`}
              src={`/assets/custom/lfw16/images/pin${
                this.state.activeItem !== 1 ? 'Inactive' : 'Active'
              }.svg`}
            />
          </div>
          <div // eslint-disable-line jsx-a11y/no-static-element-interactions
            className="Showspace-pin"
            onClick={() => this.selectPin(2)}
          >
            <Image
              onClick={() => {
                this.state.activeItem = 3
              }}
              className={`Showspace-pinimage2 ${
                this.state.activeItem !== 2 ? 'small' : ''
              }`}
              src={`/assets/custom/lfw16/images/pin${
                this.state.activeItem !== 2 ? 'Inactive' : 'Active'
              }.svg`}
            />
          </div>
          <div // eslint-disable-line jsx-a11y/no-static-element-interactions
            className="Showspace-pin"
            onClick={() => this.selectPin(3)}
          >
            <Image
              className={`Showspace-pinimage3 ${
                this.state.activeItem !== 3 ? 'small' : ''
              }`}
              src={`/assets/custom/lfw16/images/pin${
                this.state.activeItem !== 3 ? 'Inactive' : 'Active'
              }.svg`}
            />
          </div>
        </div>
        <div className="Slidershowspace">
          <div className="Slidershowspace-wrapper">
            {/* --- Sliders --- */}
            <div
              className={`Slidershowspace-slider ${
                this.state.activeItem !== 0 ? 'is-hidden' : ''
              }`}
            >
              <h2 className="Slidershowspace-title">The runway</h2>
              <p className="Slidershowspace-paragraph">
                Lorem ipsum dolor sit amet, consecutor adipisicing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis
              </p>
            </div>
            <div
              className={`Slidershowspace-slider ${
                this.state.activeItem !== 1 ? 'is-hidden' : ''
              }`}
            >
              <h2 className="Slidershowspace-title">Second slide</h2>
              <p className="Slidershowspace-paragraph">
                Lorem ipsum dolor sit amet, consecutor adipisicing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis
              </p>
            </div>
            <div
              className={`Slidershowspace-slider ${
                this.state.activeItem !== 2 ? 'is-hidden' : ''
              }`}
            >
              <h2 className="Slidershowspace-title">Third slide</h2>
              <p className="Slidershowspace-paragraph">
                Lorem ipsum dolor sit amet, consecutor adipisicing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis
              </p>
            </div>
            <div
              className={`Slidershowspace-slider ${
                this.state.activeItem !== 3 ? 'is-hidden' : ''
              }`}
            >
              <h2 className="Slidershowspace-title">Fourth slide</h2>
              <p className="Slidershowspace-paragraph">
                Lorem ipsum dolor sit amet, consecutor adipisicing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis
              </p>
            </div>
            <div className="Slider-arrow" />
            <div // eslint-disable-line jsx-a11y/no-static-element-interactions
              className="Slidershowspace-arrow"
              onClick={() => this.incrementSpaceIndex()}
            >
              <Image
                className="Slidershowspace-arrowimage"
                src="/assets/custom/lfw16/images/right-arrow.svg"
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}
