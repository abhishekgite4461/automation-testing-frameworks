import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Image from '../../../shared/components/common/Image/Image'

if (process.browser) {
  // eslint-disable-next-line import/no-webpack-loader-syntax, import/no-unresolved, import/no-extraneous-dependencies
  require('file-loader?name=topshop/custom/lfw16/showspace.css!./Showspace.css')
}

export default class Showspace extends Component {
  static propTypes = {
    uniqueShopLink: PropTypes.string,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  constructor() {
    super()
    this.state = {
      activeItem: 0,
    }
  }

  selectPin(index) {
    this.setState({ activeItem: index })
  }

  incrementSpaceIndex() {
    const currentIndex = this.state.activeItem
    this.setState({ activeItem: (currentIndex + 1) % 4 })
  }

  //
  // With this approach we could save a lot of lines of code but the transaction between highlighted
  // areas does not feel smooth at all. For every change of the state React has to remove a part of
  // the DOM and substitute it with the one associated with the highlighted area. With the verbose
  // approach instead (the current one) React has just to modify the classnames of the images without
  // any modification to the structure of the DOM.
  //
  // renderCurrentHighlightedArea() {
  //   const current = this.state.activeItem
  //
  //   return (
  //     <div className="Showspace-image">
  //       <Image
  //         className={`Showspace-mapimage${current}`}
  //         src={`/assets/custom/lfw16/images/mapimage${current}.png`}
  //       />
  //     </div>
  //   )
  // }

  render() {
    const { l } = this.context
    const { uniqueShopLink } = this.props

    return (
      <div className="Interactivemap">
        <div className="Showspace">
          <div className="Showspace-image">
            <Image
              className="Showspace-mapbackimage"
              src="/assets/custom/lfw16/images/phase1-map-back.png"
            />
          </div>
          {/* --- Highlighted Areas --- */}
          <div className="Showspace-image">
            <Image
              className={`Showspace-mapimage0 ${
                this.state.activeItem !== 0 ? 'is-hidden' : ''
              }`}
              src={`/assets/custom/lfw16/images/mapimage0.png`}
            />
          </div>
          <div className="Showspace-image">
            <Image
              className={`Showspace-mapimage1 ${
                this.state.activeItem !== 1 ? 'is-hidden' : ''
              }`}
              src={`/assets/custom/lfw16/images/mapimage1.png`}
            />
          </div>
          <div className="Showspace-image">
            <Image
              onClick={() => {
                this.state.activeItem = 2
              }}
              className={`Showspace-mapimage2 ${
                this.state.activeItem !== 2 ? 'is-hidden' : ''
              }`}
              src={`/assets/custom/lfw16/images/mapimage2.png`}
            />
          </div>
          <div className="Showspace-image">
            <Image
              className={`Showspace-mapimage3 ${
                this.state.activeItem !== 3 ? 'is-hidden' : ''
              }`}
              src={`/assets/custom/lfw16/images/mapimage3.png`}
            />
          </div>
          {/* --- Pins --- */}
          <div // eslint-disable-line jsx-a11y/no-static-element-interactions
            className="Showspace-pin"
            onClick={() => this.selectPin(0)}
          >
            <Image
              className={`Showspace-pinimage0 ${
                this.state.activeItem !== 0 ? 'small' : ''
              }`}
              src={`/assets/custom/lfw16/images/pin${
                this.state.activeItem !== 0 ? 'Inactive' : 'Active'
              }.svg`}
            />
          </div>
          <div // eslint-disable-line jsx-a11y/no-static-element-interactions
            className="Showspace-pin"
            onClick={() => this.selectPin(1)}
          >
            <Image
              onClick={() => {
                this.state.activeItem = 1
              }}
              className={`Showspace-pinimage1 ${
                this.state.activeItem !== 1 ? 'small' : ''
              }`}
              src={`/assets/custom/lfw16/images/pin${
                this.state.activeItem !== 1 ? 'Inactive' : 'Active'
              }.svg`}
            />
          </div>
          <div // eslint-disable-line jsx-a11y/no-static-element-interactions
            className="Showspace-pin"
            onClick={() => this.selectPin(2)}
          >
            <Image
              onClick={() => {
                this.state.activeItem = 3
              }}
              className={`Showspace-pinimage2 ${
                this.state.activeItem !== 2 ? 'small' : ''
              }`}
              src={`/assets/custom/lfw16/images/pin${
                this.state.activeItem !== 2 ? 'Inactive' : 'Active'
              }.svg`}
            />
          </div>
          <div // eslint-disable-line jsx-a11y/no-static-element-interactions
            className="Showspace-pin"
            onClick={() => this.selectPin(3)}
          >
            <Image
              className={`Showspace-pinimage3 ${
                this.state.activeItem !== 3 ? 'small' : ''
              }`}
              src={`/assets/custom/lfw16/images/pin${
                this.state.activeItem !== 3 ? 'Inactive' : 'Active'
              }.svg`}
            />
          </div>
        </div>
        <div className="Slidershowspace">
          <div className="Slidershowspace-wrapper">
            {/* --- Sliders --- */}
            <div
              className={`Slidershowspace-slider ${
                this.state.activeItem !== 0 ? 'is-hidden' : ''
              }`}
            >
              <h2 className="Slidershowspace-title">{l`THE RUNWAY`}</h2>
              <p className="Slidershowspace-paragraph">{l`The catwalk runs directly through Spitafield’s market – with room to house over 700 fashion editors and celebrities on the front row. Want to watch too? You can tune in to the livestream at 2pm BST.`}</p>
              <Image
                className="Slidershowspace-sliderImage"
                src="/assets/custom/lfw16/images/sliderImage0.jpg"
              />
            </div>
            <div
              className={`Slidershowspace-slider ${
                this.state.activeItem !== 1 ? 'is-hidden' : ''
              }`}
            >
              <h2 className="Slidershowspace-title">{l`BACKSTAGE`}</h2>
              <p className="Slidershowspace-paragraph">{l`Hair, make-up, final touches to the styling… there’s a lot that goes into making the Topshop Unique show look this good. Add us @topshop_snaps to get an insider look at the backstage action.`}</p>
              <Image
                className="Slidershowspace-sliderImage"
                src="/assets/custom/lfw16/images/sliderImage1.jpg"
              />
            </div>
            <div
              className={`Slidershowspace-slider ${
                this.state.activeItem !== 2 ? 'is-hidden' : ''
              }`}
            >
              <h2 className="Slidershowspace-title">{l`THE MARKET`}</h2>
              <p className="Slidershowspace-paragraph">
                {l`We’ve curated exclusive market stalls open to all. Get your jeans customised, shop catwalk beauty looks and more…`}
                <a
                  className="Slidershowspace-button"
                  href={l`http://insideout.topshop.com/2016/08/topshop-uniques-new-home-old-spitalfields-market`}
                >{l`Find out more`}</a>
              </p>
              <Image
                className="Slidershowspace-sliderImage"
                src="/assets/custom/lfw16/images/sliderImage2.jpg"
              />
            </div>
            <div
              className={`Slidershowspace-slider ${
                this.state.activeItem !== 3 ? 'is-hidden' : ''
              }`}
            >
              <h2 className="Slidershowspace-title">{l`TOPSHOP UNIQUE SHOP`}</h2>
              <p className="Slidershowspace-paragraph">
                {l`Open to the public from 3pm, this market stall will sell pieces from the collection straight after the show.`}
                <a
                  className="Slidershowspace-button"
                  href={uniqueShopLink}
                >{l`Find out more`}</a>
              </p>
              <Image
                className="Slidershowspace-sliderImage"
                src="/assets/custom/lfw16/images/sliderImage3.jpg"
              />
            </div>
            <div className="Slider-arrow" />
            <div // eslint-disable-line jsx-a11y/no-static-element-interactions
              className="Slidershowspace-arrow"
              onClick={() => this.incrementSpaceIndex()}
            >
              <Image
                className="Slidershowspace-arrowimage"
                src="/assets/custom/lfw16/images/right-arrow.svg"
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}
