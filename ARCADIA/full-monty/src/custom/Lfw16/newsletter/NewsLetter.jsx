import PropTypes from 'prop-types'
import React, { Component } from 'react'
import superagent from '../../../shared/lib/superagent'

if (process.browser) {
  // eslint-disable-next-line import/no-webpack-loader-syntax, import/no-unresolved, import/no-extraneous-dependencies
  require('file-loader?name=topshop/custom/lfw16/newsletter.css!./NewsLetter.css')
}

export default class NewsLetter extends Component {
  static contextTypes = {
    l: PropTypes.func,
  }

  constructor(props) {
    super(props)
    this.state = { mailError: false, submitted: false }
  }

  sendEmail = (mail) => {
    const formMethod = 'post'
    const action = '/webapp/wcs/stores/servlet/AddSubscriberDetails'
    const payload = {
      feedback_form: 'maildb',
      subject: 'Topshop UK: Unique AW16 Sign Up',
      emailserviceid: '8',
      sender: 'Topshop',
      from: 'noreply@topshop.com',
      thirdpartymailing: 'N',
      source: 'WEB',
      failureURL: 'http://www.topshop.com',
      successUrl: 'http://www.topshop.com',
      fieldValue: '2',
      intcmpid: 'desktop-footer-emailsignup',
      int_preference: 'UNQSEPT16',
      subscriptiondate: '20150718',
      emailid: mail,
    }

    superagent[formMethod](action)
      .set('Content-Type', 'application/json')
      .send(payload)
      .end((err) => {
        if (err) {
          this.setState({ submitted: true })
        } else {
          this.setState({ submitted: true })
        }
      })
  }

  checkEmail = () => {
    const value = this.email ? this.email.value : ''
    this.setState({
      mailError: !/^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value),
    })
    if (/^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
      this.sendEmail(value)
    }
  }

  renderSubmitForm = () => {
    const { l } = this.context
    return (
      <div className="NewsLetter-form">
        <label className="NewsLetter-label" htmlFor="email">{l`Email`}</label>
        <input
          ref={(input) => {
            this.email = input
          }}
          className="NewsLetter-input"
          type="text"
          id="email"
        />
        <button
          className="NewsLetter-submit"
          onClick={() => {
            this.checkEmail()
          }}
        >{l`Go`}</button>
      </div>
    )
  }

  renderTnx = () => {
    return (
      <div className="NewsLetter-form">
        <div className="NewsLetter-tnx">Thanks for signing up!</div>
      </div>
    )
  }

  render() {
    const { mailError } = this.state
    const { l } = this.context
    const alert = l`Please enter a valid email address.`
    return (
      <div className="NewsLetter">
        <p className="NewsLetter-subtitle">
          {l`Sign Up for news straight to your inbox`}
        </p>
        {this.state.submitted ? this.renderTnx() : this.renderSubmitForm()}
        <p className="NewsLetter-error">{mailError && alert}</p>
      </div>
    )
  }
}
