import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { decode } from 'he'
import { connect } from 'react-redux'
import superagent from '../../../shared/lib/superagent'

if (process.browser) {
  // eslint-disable-next-line import/no-webpack-loader-syntax, import/no-unresolved, import/no-extraneous-dependencies
  require('file-loader?name=topshop/custom/lfw16/blogfeed.css!./BlogFeed.css')
}

@connect(null, null)
export default class BlogFeed extends Component {
  static contextTypes = {
    l: PropTypes.func,
  }

  constructor(props) {
    super(props)
    this.state = {
      blogPostCount: 3,
      feed: [],
    }
  }

  componentWillMount() {
    if (process.browser) {
      superagent
        .get(
          'https://api-gateway.digital.arcadiagroup.co.uk/social/wordpressgo?feedURL=http://insideout.topshop.com/fashion-week-1/feed?category=fashion-week-1'
        )
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          this.setState({ feed: res.body })
        })
    }
  }

  readMorePosts = () => {
    this.setState({ blogPostCount: this.state.blogPostCount + 3 })
  }

  loadSpinner = () => (
    <div className="BlogFeed-loader">
      <img src="/assets/custom/lfw16/images/spinner.gif" alt="" />
    </div>
  )

  render() {
    const { blogPostCount, feed } = this.state
    const readMoreButton =
      blogPostCount <= feed.length ? (
        <div // eslint-disable-line jsx-a11y/no-static-element-interactions
          className="BlogFeed-readMorePosts"
          onClick={() => this.readMorePosts()}
        >
          <img src="/assets/custom/lfw16/images/down-arrow.svg" alt="" />
        </div>
      ) : null

    const latestUpdates = Array.isArray(feed)
      ? feed
          .slice(0, blogPostCount)
          .map(({ description, title, link, image }) => (
            <div key={title} className="BlogFeed-blogPost">
              <h5 className="BlogFeed-origin">
                <img
                  alt="Blog icon"
                  className="BlogFeed-blogIcon"
                  src={'/assets/custom/lfw16/images/blog.svg'}
                />{' '}
                Blog
              </h5>
              <img alt="Blog post" className="BlogFeed-blogImage" src={image} />
              <h4 className="BlogFeed-blogTitle">{title}</h4>
              <p className="BlogFeed-blogText">
                {description && decode(description)}
              </p>
              <a className="BlogFeed-blogLink" target="blank" href={link}>
                Read More{' '}
                <img
                  alt="Read more icon"
                  className="BlogFeed-readMoreIcon"
                  src={'/assets/custom/lfw16/images/ctaarrow.svg'}
                />
              </a>
            </div>
          ))
      : false

    return (
      <div className="BlogFeed">
        {latestUpdates === false
          ? null
          : latestUpdates.length
            ? latestUpdates
            : this.loadSpinner()}
        {readMoreButton}
      </div>
    )
  }
}
