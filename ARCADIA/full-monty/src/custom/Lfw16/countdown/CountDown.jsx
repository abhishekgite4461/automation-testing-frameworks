import React, { Component } from 'react'
import PropTypes from 'prop-types'
import countdown from 'countdown'
// we dont import css for use on homePage (check in-line style)
// if (process.browser) require('file?name=topshop/custom/lfw16/countdown.css!./CountDown.css')

export default class CountDown extends Component {
  static propTypes = {
    markup: PropTypes.string,
  }

  static contextTypes = {
    l: PropTypes.func,
  }

  componentDidMount() {
    const { l } = this.context
    const countDonwLabelSingular = l`| sec| min| hour| day| week| month| year|||`
    const countDonwLabelPlural = l`| secs| mins| hours| days| weeks| months| years|||`
    // countdown.setLabels(singular, plural, last, delim, empty, formatter)
    if (process.browser) {
      countdown.setLabels(
        countDonwLabelSingular,
        countDonwLabelPlural,
        ' : ',
        ' : ',
        'Now.'
      )
      this.countdownInterval = countdown(
        new Date('2016-09-18T13:30:00'),
        (ts) => {
          document.getElementById('pageTimer').innerHTML = ts.toHTML('strong')
        },
        countdown.DAYS | countdown.HOURS | countdown.MINUTES | countdown.SECONDS // eslint-disable-line no-bitwise
      )
    }
    // timer.start
  }

  componentWillUnmount() {
    if (process.browser) window.clearInterval(this.countdownInterval)
  }

  validateJson = (data) => {
    // Default url (defensive code)
    const defaultObject = {
      url:
        '/en/tsuk/category/london-fashion-week-unique-show-4706872/home?TS=1441825748288&intcmpid=SITEWIDE_BANNER_LFW_HUB',
      text: 'Find out more',
    }

    try {
      return JSON.parse(data)
    } catch (err) {
      return defaultObject
    }
  }

  render() {
    const { markup } = this.props
    const data = this.validateJson(markup)
    const { l } = this.context
    return (
      <div
        className="CountDown"
        style={{
          backgroundColor: '#232323',
          color: '#e6e6dd',
          padding: '5px',
          textAlign: 'center',
        }}
      >
        <div
          className="CountDown-title"
          style={{
            fontFamily: 'Platform, sans-serif',
            letterSpacing: '0.085em',
            textTransform: 'uppercase',
          }}
        >
          {l`TOPSHOP UNIQUE SHOW STARTS IN`}
        </div>
        <div
          id="pageTimer"
          className="CountDown-timer"
          style={{
            display: 'inline-block',
            textTransform: 'uppercase',
            fontFamily: 'Platform, sans-serif',
            letterSpacing: '0.085em',
          }}
        >
          ... {l`Loading`}
        </div>
        <div className="CountDown-findOut" style={{ marginTop: '3px' }}>
          <a
            href={data.url}
            className="CountDown-link"
            style={{
              fontFamily: 'Platform, sans-serif',
              letterSpacing: '0.085em',
              textTransform: 'uppercase',
              fontSize: '12px',
              color: '#e6e6dd',
              textDecoration: 'inherit',
            }}
          >
            {data.text}{' '}
            <img
              style={{ height: '14px', verticalAlign: 'middle' }}
              alt="Read more arrow"
              className="CountDown-arrow"
              src={'/assets/custom/lfw16/images/ctaarrow-sand.svg'}
            />
          </a>
        </div>
      </div>
    )
  }
}
