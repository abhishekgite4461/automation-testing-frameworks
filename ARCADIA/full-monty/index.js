require('babel-register')
if (process.env.NEWRELIC_KEY) require('newrelic')

const AWS = require('aws-sdk')

AWS.config.update({
  region: 'eu-west-1',
})

const path = require('path')
const logger = require('./src/server/lib/logger')
const bootstrapEnvVarsFromDynamoDB = require('./src/server/lib/bootstrap-env-vars-from-dynamodb')
  .default

const envPath = path.join(__dirname, '/.env')

// will be reassigned on server restart
let server

function initialiseSNS() {
  const snsServer = require('./src/server/sns-server').default

  snsServer.start(() => {
    logger.info('sns-server', {
      loggerMessage: 'Sns Server is running',
      port: snsServer.info.port,
    })
  })
}

function startMonty(cb) {
  server = require('./src/server/server').default

  server.start(() => {
    if (cb) cb()
    logger.info('server', {
      loggerMessage: 'Server is running',
      port: server.info.port,
      environment: process.env,
    })
  })
}

// Please set CI env var to true if running tests locally while Monty is in production mode!
if (!process.env.CI && process.env.NODE_ENV === 'production') {
  bootstrapEnvVarsFromDynamoDB(() => startMonty(initialiseSNS))
} else {
  startMonty()
}

const serverDelete = () => {
  require('child_process').execSync('./scripts/write_version_info_file.sh', {
    encoding: 'utf8',
  })
  Object.keys(require.cache).map((id) => {
    if (id.includes('/server/') || id.includes('/shared/')) {
      delete require.cache[id]
    }
    return id
  })

  server.stop((err) => {
    if (err) logger.error('server', { loggerMessage: 'Server stop', ...err })

    process.env = {}

    require('dotenv').config({ path: envPath })

    server = require('./src/server/server').default

    server.start((e) => {
      logger.info('server', { loggerMessage: 'Server restarting ...' })
      if (e)
        return logger.error('server', {
          loggerMessage: 'Server restart error',
          ...e,
        })
    })
  })
}

if (process.env.NODE_ENV !== 'production') {
  const chokidar = require('chokidar')
  const watcher = chokidar.watch([path.join(__dirname, '/src/'), envPath])

  watcher.on('ready', () => {
    watcher.on('change', serverDelete)
  })
}
