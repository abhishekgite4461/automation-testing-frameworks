// journey: buy item for less than £50 and apply Free standard shipping promotion code: TSDEL0103
export const orderCompletedApplyDeliveryDiscount = {
  checkout: {
    orderCompleted: {
      orderId: 1868590,
      subTotal: '39.00',
      returnPossible: false,
      returnRequested: false,
      deliveryMethod: 'UK Standard up to 4 days',
      deliveryDate: 'Friday 6 October 2017',
      deliveryCost: '',
      deliveryCarrier: 'Parcelnet',
      deliveryPrice: '0.00',
      totalOrderPrice: '39.00',
      totalOrdersDiscountLabel: '',
      totalOrdersDiscount: '',
      orderLines: [
        {
          lineNo: '35J17MIVR',
          name: 'Cinched Waist Mini Dress',
          size: '12',
          colour: 'IVORY',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS35J17MIVR_Small_F_1.jpg',
          quantity: 1,
          unitPrice: '39.00',
          discount: '',
          total: '39.00',
          nonRefundable: false,
        },
      ],
    },
    partialOrderSummary: {
      basket: {
        orderId: 1868590,
        subTotal: '39.00',
        total: '39.00',
        totalBeforeDiscount: '39.00',
        promotions: [
          {
            promotionCode: 'TSDEL0103',
            label: 'Free standard shipping Free collect from store',
          },
        ],
      },
    },
  },
}

// journey: buy Apply off £5 pound discount - Order Discount
export const orderCompletedApplyOrderDiscount = {
  checkout: {
    orderCompleted: {
      orderId: 1859725,
      subTotal: '122.00',
      returnPossible: false,
      returnRequested: false,
      deliveryMethod: 'UK Standard up to 4 days',
      deliveryDate: 'Thursday 5 October 2017',
      deliveryCost: '',
      deliveryCarrier: 'Parcelnet',
      deliveryPrice: '0.00',
      totalOrderPrice: '117.00',
      totalOrdersDiscountLabel: '£5 off',
      totalOrdersDiscount: '-£5.00',
      currencyConversion: {
        currencyRate: 'GBP',
      },
      billingAddress: {
        name: 'Ms Jose Quinto',
        address1: '35 Britten Close',
        address2: 'LONDON',
        address3: 'NW11 7HQ',
        country: 'United Kingdom',
      },
      deliveryAddress: {
        name: 'Ms Jose Quinto',
        address1: '35 Britten Close',
        address2: 'LONDON',
        address3: 'NW11 7HQ',
        country: 'United Kingdom',
      },
      orderLines: [
        {
          lineNo: '10S76MRED',
          name: 'Rocket Man Tea Dress',
          size: '12',
          colour: 'RED',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS10S76MRED_Small_F_1.jpg',
          quantity: 2,
          unitPrice: '46.00',
          discount: '',
          total: '92.00',
          nonRefundable: false,
        },
        {
          lineNo: '27V01LBLE',
          name: 'Bonded Lace Wrap Midi',
          size: '10',
          colour: 'BLUE',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS27V01LBLE_Small_F_1.jpg',
          quantity: 3,
          unitPrice: '10.00',
          discount: '',
          total: '30.00',
          nonRefundable: false,
        },
      ],
      paymentDetails: [
        {
          paymentMethod: 'Visa',
          cardNumberStar: '************4444',
          totalCost: '£9.00',
        },
        {
          paymentMethod: 'Giftcard',
        },
      ],
    },
    partialOrderSummary: {
      basket: {
        orderId: 1859725,
        subTotal: '122.00',
        total: '117.00',
        totalBeforeDiscount: '92.00',
        deliveryOptions: [],
        promotions: [
          {
            promotionCode: 'TESTUK5OFF',
            label: '£5 off',
          },
        ],
        discounts: [
          {
            label: '£5 off',
            value: '5.00',
          },
        ],
        products: [
          {
            productId: 29676237,
            catEntryId: 29676278,
            orderItemId: 8296565,
            shipModeId: 26504,
            lineNumber: '10S76MRED',
            size: '12',
            name: 'Rocket Man Tea Dress',
            quantity: 2,
            lowStock: false,
            inStock: true,
            unitPrice: '46.00',
            totalPrice: '92.00',
            assets: [],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
          {
            productId: 27585616,
            catEntryId: 27585976,
            orderItemId: 8296578,
            shipModeId: 26504,
            lineNumber: '27V01LBLE',
            size: '10',
            name: 'Bonded Lace Wrap Midi',
            quantity: 3,
            lowStock: false,
            inStock: true,
            wasWasPrice: '60.00',
            unitPrice: '10.00',
            totalPrice: '30.00',
            assets: [],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
      },
    },
  },
}

// journey: but item and select Collect From Store and Collect From Store Express
export const orderCompletedCollectFromStoreExpress = {
  checkout: {
    orderCompleted: {
      orderId: 1868927,
      subTotal: '',
      returnPossible: false,
      returnRequested: false,
      deliveryMethod: 'Collect From Store Express',
      deliveryDate: '',
      deliveryCost: '3.00',
      deliveryCarrier: 'Retail Store Express',
      deliveryPrice: '3.00',
      totalOrderPrice: '42.00',
      totalOrdersDiscountLabel: '',
      totalOrdersDiscount: '',
      orderLines: [
        {
          lineNo: '35J17MIVR',
          name: 'Cinched Waist Mini Dress',
          size: '12',
          colour: 'IVORY',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS35J17MIVR_Small_F_1.jpg',
          quantity: 1,
          unitPrice: '39.00',
          discount: '',
          total: '39.00',
          nonRefundable: false,
        },
      ],
    },
    partialOrderSummary: {
      basket: {
        orderId: 1868927,
        subTotal: '39.00',
        total: '42.00',
        totalBeforeDiscount: '39.00',
      },
      deliveryStoreCode: 'TS0873',
    },
  },
}

// journey: buy item and select Collect From Store and Collect From Store Standard
export const orderCompletedCollectFromStoreStandard = {
  checkout: {
    orderCompleted: {
      orderId: 1868625,
      subTotal: '',
      returnPossible: false,
      returnRequested: false,
      deliveryMethod: 'Collect From Store Standard',
      deliveryDate: '',
      deliveryCost: '',
      deliveryCarrier: 'Retail Store Standard',
      deliveryPrice: '0.00',
      totalOrderPrice: '39.00',
      totalOrdersDiscountLabel: '',
      totalOrdersDiscount: '',
      orderLines: [
        {
          lineNo: '35J17MIVR',
          name: 'Cinched Waist Mini Dress',
          size: '12',
          colour: 'IVORY',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS35J17MIVR_Small_F_1.jpg',
          quantity: 1,
          unitPrice: '39.00',
          discount: '',
          total: '39.00',
          nonRefundable: false,
        },
      ],
    },
    partialOrderSummary: {
      basket: {
        orderId: 1868590,
        subTotal: '39.00',
        total: '39.00',
        totalBeforeDiscount: '39.00',
        promotions: [
          {
            promotionCode: 'TSDEL0103',
            label: 'Free standard shipping Free collect from store',
          },
        ],
      },
      deliveryStoreCode: 'TS0873',
    },
    deliveryStore: {
      deliveryStoreCode: 'TS0873',
      storeAddress1: 'Unit Msu1',
      storeAddress2: '1 New Change',
      storeCity: 'City Of London',
      storePostcode: 'EC2V 6AH',
    },
  },
}

// journey: buy 3 for £8 pound socks (Crop Ribbed Glitter Socks)
export const orderCompletedDiscountedItem = {
  checkout: {
    orderCompleted: {
      orderId: 1869054,
      subTotal: '8.00',
      returnPossible: false,
      returnRequested: false,
      deliveryMethod: 'UK Standard up to 4 days',
      deliveryDate: 'Friday 6 October 2017',
      deliveryCost: '4.00',
      deliveryCarrier: 'Parcelnet',
      deliveryPrice: '4.00',
      totalOrderPrice: '12.00',
      totalOrdersDiscountLabel: '',
      totalOrdersDiscount: '',
      orderLines: [
        {
          lineNo: '08L03MBLE',
          name: 'Crop Ribbed Glitter Socks',
          size: 'ONE',
          colour: 'BLUE',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS08L03MBLE_Small_F_1.jpg',
          quantity: 3,
          unitPrice: '3.50',
          discount: '3 for £8 Ankle Socks Promotion',
          total: '8.00',
          nonRefundable: false,
        },
      ],
    },
    partialOrderSummary: {
      basket: {
        orderId: 1869054,
        subTotal: '8.00',
        total: '12.00',
        totalBeforeDiscount: '9.00',
        products: [
          {
            productId: 29617017,
            catEntryId: 29617031,
            orderItemId: 8310874,
            shipModeId: 26504,
            lineNumber: '08L03MBLE',
            size: 'ONE',
            name: 'Crop Ribbed Glitter Socks',
            quantity: 3,
            lowStock: false,
            inStock: true,
            unitPrice: '3.50',
            totalPrice: '8.00',
            discountText: '3 for £8 Ankle Socks Promotion',
            assets: [],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
      },
    },
  },
}

// journey: buy a Marked Down Item (Bonded Lace Wrap Midi)
export const orderCompletedMarkedDownItems = {
  checkout: {
    orderCompleted: {
      orderId: 1869506,
      subTotal: '',
      returnPossible: false,
      returnRequested: false,
      deliveryMethod: 'UK Standard up to 4 days',
      deliveryDate: 'Monday 9 October 2017',
      deliveryCost: '4.00',
      deliveryCarrier: 'Parcelnet',
      deliveryPrice: '4.00',
      totalOrderPrice: '34.00',
      totalOrdersDiscountLabel: '',
      totalOrdersDiscount: '',
      orderLines: [
        {
          lineNo: '27V01LBLE',
          name: 'Bonded Lace Wrap Midi',
          size: '12',
          colour: 'BLUE',
          imageUrl:
            '//media.topshop.com/wcsstore/TopShop/images/catalog/TS27V01LBLE_Small_F_1.jpg',
          quantity: 3,
          unitPrice: '10.00',
          discount: '',
          total: '30.00',
          nonRefundable: false,
        },
      ],
    },
    partialOrderSummary: {
      basket: {
        orderId: 1869506,
        subTotal: '30.00',
        total: '34.00',
        totalBeforeDiscount: '.00',
        deliveryOptions: [{}, {}, {}],
        promotions: [],
        discounts: [],
        products: [
          {
            productId: 27585616,
            catEntryId: 27585985,
            orderItemId: 8311385,
            shipModeId: 26504,
            lineNumber: '27V01LBLE',
            size: '12',
            name: 'Bonded Lace Wrap Midi',
            quantity: 3,
            lowStock: false,
            inStock: true,
            wasWasPrice: '60.00',
            unitPrice: '10.00',
            totalPrice: '30.00',
            assets: [],
            items: [],
            bundleProducts: [],
            attributes: {},
            colourSwatches: [],
            tpmLinks: [],
            bundleSlots: [],
            ageVerificationRequired: false,
            isBundleOrOutfit: false,
          },
        ],
      },
    },
  },
}

export const orderCompletedTwoProducts = {
  checkout: {
    partialOrderSummary: {
      basket: {},
    },
    orderCompleted: {
      orderId: 1638681,
      subTotal: '68.00',
      returnPossible: false,
      returnRequested: false,
      deliveryMethod: 'UK Standard up to 4 days',
      deliveryDate: 'Thursday 5 October 2017',
      deliveryCost: '4.00',
      deliveryCarrier: 'Parcelnet',
      deliveryPrice: '4.00',
      totalOrdersDiscountLabel: '',
      totalOrdersDiscount: '',
      totalOrderPrice: '68.00',
      currencyConversion: {
        currencyRate: 'GBP',
      },
      deliveryAddress: {
        country: 'United Kingdom',
      },
      orderLines: [
        {
          lineNo: '32J07MPNK',
          name: 'JETSET V-Cut Slingback Shoes',
          size: '35',
          colour: 'PINK',
          quantity: 1,
          unitPrice: '42.00',
          discount: '',
          total: '42.00',
        },
        {
          lineNo: '42V02MBLK',
          name: 'GEORGIA Mules',
          size: '35',
          colour: 'BLACK',
          quantity: 1,
          unitPrice: '26.00',
          discount: '',
          total: '26.00',
        },
      ],
      paymentDetails: [
        { paymentMethod: 'Visa' },
        { paymentMethod: 'Giftcard' },
      ],
    },
  },
}
