export default {
  breadcrumbs: [
    {
      label: 'Home',
    },
    {
      category: '2091646',
      label: 'Magazine',
    },
  ],
  canonicalUrl:
    'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/category/magazine-3934038/home',
  categoryDescription: '',
  categoryRefinement: {
    refinementOptions: [],
  },
  categoryTitle: 'Magazine',
  cmsPage: {
    baseline: '245',
    contentPath: 'cms/pages/static/static-0000090081/static-0000090081.html',
    lastPublished: '2017-03-31 10:28:43.564176',
    mobileCMSUrl: 'cms/pages/json/json-0000119921/json-0000119921.json',
    pageId: 90081,
    pageName: 'static-0000090081',
    revision: '0',
    seoUrl: '/en/tsuk/category/magazine-3934038/home',
  },
  default: {
    breadcrumbs: [
      {
        label: 'Home',
      },
      {
        category: '2091646',
        label: 'Magazine',
      },
    ],
    canonicalUrl:
      'http://ts.stage.arcadiagroup.ltd.uk/en/tsuk/category/magazine-3934038/home',
    categoryHeader: {
      description: '',
      title: 'Magazine',
    },
    categoryRefinement: {
      refinementOptions: [],
    },
    cmsPage: {
      baseline: '245',
      contentPath: 'cms/pages/static/static-0000090081/static-0000090081.html',
      lastPublished: '2017-03-31 10:28:43.564176',
      mobileCMSUrl: 'cms/pages/json/json-0000119921/json-0000119921.json',
      pageId: 90081,
      pageName: 'static-0000090081',
      revision: '0',
      seoUrl: '/en/tsuk/category/magazine-3934038/home',
    },
    paging: {},
    products: [],
    refinements: [],
    version: '1.6',
  },
  products: [],
  refinements: [],
  sortOptions: undefined,
  totalProducts: undefined,
  version: '1.6',
}
