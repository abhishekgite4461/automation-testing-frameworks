/*
  This module is used just for very simple mocking to speed up development and is not ment to represent a full mock API (session management, ...).
 */

import siteOptionsMockResponse from './site-options/wcs.json'
import pdp from './pdp/wcs.json'
import mobileNavigation from './navigation/wcs-navigation-12556.json'

const urlToMockVersion = {
  '/webapp/wcs/stores/servlet/SiteOptions': {
    get: siteOptionsMockResponse,
  },
  '/navigation-12556.json': {
    get: mobileNavigation,
  },
  '/webapp/wcs/stores/servlet/ProductDisplay': {
    get: pdp,
  },
}

export default function(endpoint, method) {
  const response =
    urlToMockVersion[endpoint] && urlToMockVersion[endpoint][method]
      ? urlToMockVersion[endpoint][method]
      : { body: 'no associated mocked response' }
  return response
}
