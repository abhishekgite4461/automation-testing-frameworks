require('babel-polyfill')

jest.unmock('superagent')
import superagent from 'superagent'
import chai from 'chai'

chai.use(require('chai-json-schema'))

const assert = chai.assert
import eps from '../routes_tests'
import { headers, stringType, booleanType, arrayType } from '../utilis'

describe('It should return the Site Options Json Schema', () => {
  let response
  beforeAll(async () => {
    response = await superagent.get(eps.siteOptions.path).set(headers)
  })

  it('Site Options Schema', () => {
    const body = response.body
    const siteOptionsSchema = {
      title: 'Site Options Schema',
      type: 'object',
      required: [
        'billingCountries',
        'creditCardOptions',
        'currencyCode',
        'deliveryCountries',
        'expiryMonths',
        'expiryYears',
        'titles',
        'USStates',
      ],
      properties: {
        billingCountries: arrayType(122, false, 'string'),
        creditCardOptions: arrayType(7),
        currencyCode: stringType,
        deliveryCountries: arrayType(84, undefined, 'string'),
        expiryMonths: arrayType(12, undefined, 'string'),
        expiryYears: arrayType(12, undefined, 'string'),
        titles: arrayType(5, undefined, 'string'),
        USStates: arrayType(51, undefined, 'string'),
      },
    }
    assert.jsonSchema(body, siteOptionsSchema)
  })

  it('Site Options Credit Card Schema', () => {
    const body = response.body.creditCardOptions
    body.forEach((obj) => {
      const cardsOptionsSchema = {
        title: 'Navigation Mobile Sub-Categories Schema',
        type: 'object',
        required: ['defaultPayment', 'label', 'value'],
        properties: {
          defaultPayment: booleanType(true),
          label: stringType,
          value: stringType,
        },
      }
      assert.jsonSchema(obj, cardsOptionsSchema)
    })
  })
})
