// PAYLOADS
export const createSpecificUser = (
  email = '',
  password = '',
  passwordConfirm = '',
  subscribe
) => {
  return {
    email,
    password,
    passwordConfirm,
    subscribe,
  }
}
// Error messages in responses
export const registerSameEmailError =
  'The email address you have entered already exists. Please use a different email address and try again.'
export const registerEnterEmailError = 'Please enter your email address.'
export const registerEnterPasswordError = 'Please enter a password.'
export const registerConfirmPasswordError = 'Please confirm your password.'
export const registerNoMatchingPasswordsError =
  'The password you have entered in the Verify Password box does not match your password. Please try again.'
export const registerPasswordError =
  'Passwords must be at least {0} characters in length, and include {1} digit(s) and {2} letter(s). Please try again.'
export const registerNumericPasswordError =
  'Passwords must be at least {1} characters in length, and include {2} digit(s) and {0} letter(s). Please try again.'
export const registerLetterPasswordError =
  'Passwords must be at least {1} characters in length, and include {0} digit(s) and {2} letter(s). Please try again.'
export const registerSameCharacterPasswordError =
  'A character in your password occurs more consecutively than the allowed limit of {0}. Please try again.'
export const registerErrorEmail = 'testautomation12@a.com'
