require('babel-polyfill')

jest.unmock('superagent')
import superagent from 'superagent'
import chai from 'chai'

chai.use(require('chai-json-schema'))

const assert = chai.assert
import eps from '../routes_tests'
import {
  booleanType,
  headers,
  numberType,
  numberTypePattern,
  objectType,
  stringType,
  stringTypeEmpty,
  stringTypePattern,
} from '../utilis'
import {
  createSpecificUser,
  registerConfirmPasswordError,
  registerEnterEmailError,
  registerEnterPasswordError,
  registerErrorEmail,
  registerLetterPasswordError,
  registerNoMatchingPasswordsError,
  registerNumericPasswordError,
  registerPasswordError,
  registerSameCharacterPasswordError,
  registerSameEmailError,
} from './register-account-data'
import { createAccount } from './../utilis/userAccount'

const validateBasicRegisterSchema = (response) => {
  const registerAccountSchema = {
    title: 'Logon User Partial Profile Schema',
    type: 'object',
    required: [
      'basketItemCount',
      'billingDetails',
      'creditCard',
      'deliveryDetails',
      'email',
      'exists',
      'firstName',
      'hasBillingDetails',
      'hasCardNumberHash',
      'hasDeliveryDetails',
      'hasPayPal',
      'lastName',
      'subscriptionId',
      'title',
      'userTrackingId',
      'version',
    ],
    properties: {
      basketItemCount: numberTypePattern(0, 0),
      billingDetails: objectType,
      creditCard: objectType,
      deliveryDetails: objectType,
      email: stringType,
      exists: booleanType(true),
      firstName: stringTypeEmpty,
      hasBillingDetails: booleanType(false),
      hasCardNumberHash: booleanType(false),
      hasDeliveryDetails: booleanType(false),
      hasPayPal: booleanType(false),
      lastName: stringTypeEmpty,
      subscriptionId: { type: ['number', 'string'] },
      title: stringTypeEmpty,
      userTrackingId: numberType,
      version: stringType,
    },
  }
  assert.jsonSchema(response, registerAccountSchema)
}

describe('It should return the Account Registration Json Schema', () => {
  describe('Register account with subscription', () => {
    let response
    beforeAll(async () => {
      response = await createAccount()
      response = response.accountProfile
    })
    it('Register Account', () => validateBasicRegisterSchema(response))

    it('Register Account Credit Card Schema', () => {
      const body = response.creditCard
      const creditCardSchema = {
        title: 'Register Account Schema Credit Card Object Schema',
        type: 'object',
        required: [
          'cardNumberHash',
          'cardNumberStar',
          'expiryMonth',
          'expiryYear',
          'type',
        ],
        properties: {
          cardNumberHash: stringTypeEmpty,
          cardNumberStar: stringTypeEmpty,
          expiryMonth: stringTypeEmpty,
          expiryYear: stringTypeEmpty,
          type: stringTypeEmpty,
        },
      }
      assert.jsonSchema(body, creditCardSchema)
    })

    it('Register Account Billing Details Schema', () => {
      const body = response.billingDetails
      const billingDetailsSchema = {
        title: 'Register Account Schema Billing Details Object Schema',
        type: 'object',
        required: ['addressDetailsId', 'nameAndPhone', 'address'],
        properties: {
          address: objectType,
          addressDetailsId: numberTypePattern(-1, -1),
          nameAndPhone: objectType,
        },
      }
      assert.jsonSchema(body, billingDetailsSchema)
    })

    it('Register Account Billing Details nameAndPhone Schema', () => {
      const body = response.billingDetails.nameAndPhone
      const billingDetailsSchema = {
        title: 'Register Account Schema Billing Details NameAndPhone Schema',
        type: 'object',
        required: ['lastName', 'telephone', 'title', 'firstName'],
        properties: {
          firstName: stringTypeEmpty,
          lastName: stringTypeEmpty,
          telephone: stringTypeEmpty,
          title: stringTypeEmpty,
        },
      }
      assert.jsonSchema(body, billingDetailsSchema)
    })

    it('Register Account Billing Details address Schema', () => {
      const body = response.billingDetails.address
      const billingDetailsSchema = {
        title: 'Register Account Schema Billing Details Address Schema',
        type: 'object',
        required: [
          'address1',
          'address2',
          'city',
          'country',
          'postcode',
          'state',
        ],
        properties: {
          address1: stringTypeEmpty,
          address2: stringTypeEmpty,
          city: stringTypeEmpty,
          country: stringTypeEmpty,
          postcode: stringTypeEmpty,
          state: stringTypeEmpty,
        },
      }
      assert.jsonSchema(body, billingDetailsSchema)
    })

    it('Register Account Delivery Details Schema', () => {
      const body = response.deliveryDetails
      const deliveryDetailsSchema = {
        title: 'Register Account Schema Delivery Details Object Schema',
        type: 'object',
        required: ['addressDetailsId', 'nameAndPhone', 'address'],
        properties: {
          address: objectType,
          addressDetailsId: numberTypePattern(-1, -1),
          nameAndPhone: objectType,
        },
      }
      assert.jsonSchema(body, deliveryDetailsSchema)
    })

    it('Register Account Delivery Details nameAndPhone Schema', () => {
      const body = response.deliveryDetails.nameAndPhone
      const deliveryDetailsSchema = {
        title: 'Register Account Schema Delivery Details NameAndPhone Schema',
        type: 'object',
        required: ['lastName', 'telephone', 'title', 'firstName'],
        properties: {
          firstName: stringTypeEmpty,
          lastName: stringTypeEmpty,
          telephone: stringTypeEmpty,
          title: stringTypeEmpty,
        },
      }
      assert.jsonSchema(body, deliveryDetailsSchema)
    })

    it('Register Account Delivery Details address Schema', () => {
      const body = response.deliveryDetails.address
      const deliveryDetailsSchema = {
        title: 'Register Account Schema Delivery Details Address Schema',
        type: 'object',
        required: [
          'address1',
          'address2',
          'city',
          'country',
          'postcode',
          'state',
        ],
        properties: {
          address1: stringTypeEmpty,
          address2: stringTypeEmpty,
          city: stringTypeEmpty,
          country: stringTypeEmpty,
          postcode: stringTypeEmpty,
          state: stringTypeEmpty,
        },
      }
      assert.jsonSchema(body, deliveryDetailsSchema)
    })
  })
  describe('Register Account with no Subscription', () => {
    let response
    beforeAll(async () => {
      response = await createAccount({ subscribe: false })
      response = response.accountProfile
    })
    it('Register Account Schema No Subscription', () =>
      validateBasicRegisterSchema(response))
  })
  describe('Register Account with errors', () => {
    let response
    it('It should return an error response with existing email', async () => {
      response = await superagent
        .post(eps.myAccount.register.path)
        .set(headers)
        .send(
          createSpecificUser('automation1@a.com', 'test12', 'test12', false)
        )
        .then(() => {})
        .catch((error) => {
          return error
        })
      const body = response.response.body
      const registerErrorSchema = {
        title: 'Register account Schema with existing email address',
        type: 'object',
        required: ['statusCode', 'error', 'message'],
        properties: {
          error: stringTypePattern('Unprocessable Entity'),
          message: stringTypePattern(registerSameEmailError),
          statusCode: numberTypePattern(422),
        },
      }
      assert.jsonSchema(body, registerErrorSchema)
    })
    it('It should return an error response with no data sent', async () => {
      response = await superagent
        .post(eps.myAccount.register.path)
        .set(headers)
        .send(createSpecificUser(false))
        .then(() => {})
        .catch((error) => {
          return error
        })
      const body = response.response.body
      const registerErrorSchema = {
        title: 'Register account Schema with no data sent',
        type: 'object',
        required: ['statusCode', 'error', 'message'],
        properties: {
          error: stringTypePattern('Unprocessable Entity'),
          message: stringTypePattern(registerEnterEmailError),
          statusCode: numberTypePattern(422),
        },
      }
      assert.jsonSchema(body, registerErrorSchema)
    })
    it('It should return an error response with no email sent', async () => {
      response = await superagent
        .post(eps.myAccount.register.path)
        .set(headers)
        .send(createSpecificUser(false))
        .then(() => {})
        .catch((error) => {
          return error
        })
      const body = response.response.body
      const registerErrorSchema = {
        title: 'Register account Schema with no data sent',
        type: 'object',
        required: ['statusCode', 'error', 'message'],
        properties: {
          error: stringTypePattern('Unprocessable Entity'),
          message: stringTypePattern(registerEnterEmailError),
          statusCode: numberTypePattern(422),
        },
      }
      assert.jsonSchema(body, registerErrorSchema)
    })
    it('It should return an error response with no password sent', async () => {
      response = await superagent
        .post(eps.myAccount.register.path)
        .set(headers)
        .send(createSpecificUser(registerErrorEmail, '', '', false))
        .then(() => {})
        .catch((error) => {
          return error
        })
      const body = response.response.body
      const registerErrorSchema = {
        title: 'Register account Schema with no password sent',
        type: 'object',
        required: ['statusCode', 'error', 'message'],
        properties: {
          error: stringTypePattern('Unprocessable Entity'),
          message: stringTypePattern(registerEnterPasswordError),
          statusCode: numberTypePattern(422),
        },
      }
      assert.jsonSchema(body, registerErrorSchema)
    })
    it('It should return an error response with no confirm password sent', async () => {
      response = await superagent
        .post(eps.myAccount.register.path)
        .set(headers)
        .send(createSpecificUser(registerErrorEmail, 'test12', '', false))
        .then(() => {})
        .catch((error) => {
          return error
        })
      const body = response.response.body
      const registerErrorSchema = {
        title: 'Register account Schema with no confirm password sent',
        type: 'object',
        required: ['statusCode', 'error', 'message'],
        properties: {
          error: stringTypePattern('Unprocessable Entity'),
          message: stringTypePattern(registerConfirmPasswordError),
          statusCode: numberTypePattern(422),
        },
      }
      assert.jsonSchema(body, registerErrorSchema)
    })
    it('It should return an error response with different password and confirm password', async () => {
      response = await superagent
        .post(eps.myAccount.register.path)
        .set(headers)
        .send(
          createSpecificUser(registerErrorEmail, 'test12', 'test123', false)
        )
        .then(() => {})
        .catch((error) => {
          return error
        })
      const body = response.response.body
      const registerErrorSchema = {
        title: 'Register account Schema with no password sent',
        type: 'object',
        required: ['statusCode', 'error', 'message'],
        properties: {
          error: stringTypePattern('Unprocessable Entity'),
          message: stringTypePattern(registerNoMatchingPasswordsError),
          statusCode: numberTypePattern(422),
        },
      }
      assert.jsonSchema(body, registerErrorSchema)
    })
    it('It should return an error response with password too short sent', async () => {
      response = await superagent
        .post(eps.myAccount.register.path)
        .set(headers)
        .send(createSpecificUser(registerErrorEmail, 'test1', 'test1', false))
        .then(() => {})
        .catch((error) => {
          return error
        })
      const body = response.response.body
      const registerErrorSchema = {
        title: 'Register account Schema with password too short sent',
        type: 'object',
        required: ['statusCode', 'error', 'message'],
        properties: {
          error: stringTypePattern('Unprocessable Entity'),
          message: stringType,
          statusCode: numberTypePattern(422),
        },
      }
      assert.jsonSchema(body, registerErrorSchema)
      assert.equal(registerPasswordError, body.message)
    })
    it('It should return an error response with password with same character consecutively sent', async () => {
      response = await superagent
        .post(eps.myAccount.register.path)
        .set(headers)
        .send(
          createSpecificUser(
            registerErrorEmail,
            'aaaaaaaa12',
            'aaaaaaaa12',
            false
          )
        )
        .then(() => {})
        .catch((error) => {
          return error
        })
      const body = response.response.body
      const registerErrorSchema = {
        title: 'Register account Schema with same character sent',
        type: 'object',
        required: ['statusCode', 'error', 'message'],
        properties: {
          error: stringTypePattern('Unprocessable Entity'),
          message: stringType,
          statusCode: numberTypePattern(422),
        },
      }
      assert.jsonSchema(body, registerErrorSchema)
      assert.equal(registerSameCharacterPasswordError, body.message)
    })
    it('It should return an error response with password numeric sent', async () => {
      response = await superagent
        .post(eps.myAccount.register.path)
        .set(headers)
        .send(createSpecificUser(registerErrorEmail, '123456', '123456', false))
        .then(() => {})
        .catch((error) => {
          return error
        })
      const body = response.response.body
      const registerErrorSchema = {
        title: 'Register account Schema with numeric password',
        type: 'object',
        required: ['statusCode', 'error', 'message'],
        properties: {
          error: stringTypePattern('Unprocessable Entity'),
          message: stringType,
          statusCode: numberTypePattern(422),
        },
      }
      assert.jsonSchema(body, registerErrorSchema)
      assert.equal(registerNumericPasswordError, body.message)
    })
    it('It should return an error response with password letter sent', async () => {
      response = await superagent
        .post(eps.myAccount.register.path)
        .set(headers)
        .send(createSpecificUser(registerErrorEmail, 'qwerty', 'qwerty', false))
        .then(() => {})
        .catch((error) => {
          return error
        })
      const body = response.response.body
      const registerErrorSchema = {
        title: 'Register account Schema with letter password',
        type: 'object',
        required: ['statusCode', 'error', 'message'],
        properties: {
          error: stringTypePattern('Unprocessable Entity'),
          message: stringType,
          statusCode: numberTypePattern(422),
        },
      }
      assert.jsonSchema(body, registerErrorSchema)
      assert.equal(registerLetterPasswordError, body.message)
    })
  })
  describe('Register acount for native Apps', () => {
    describe('When I register an account by additonally providing the appId', () => {
      let response
      beforeAll(async () => {
        response = await createAccount({
          subscribe: false,
          appId: '1234-1234-1234-1234',
          deviceType: 'apps',
        })
        response = response.accountProfile
      })
      it('Then I receive back the userId', () => {
        // WCS is now sending us back userId.
        // So removed '.not' statement
        expect(response.userId).toBeDefined()
        expect(typeof response.userId).toBe('string')
      })
      it('Then I receive back the userToken', () => {
        // WCS is now sending us back userToken.
        // So removed '.not' statement
        expect(typeof response.userToken).toBe('string')
      })
      it('Then the rest of the response matches the regular registering schema', () =>
        validateBasicRegisterSchema(response))
    })
  })
})
