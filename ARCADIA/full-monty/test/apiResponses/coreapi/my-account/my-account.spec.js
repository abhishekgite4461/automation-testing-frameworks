import chai from 'chai'

chai.use(require('chai-json-schema'))

const assert = chai.assert
import {
  stringType,
  stringTypeEmpty,
  stringTypePattern,
  objectType,
  booleanType,
  numberType,
  numberTypePattern,
  arrayType,
} from '../utilis'
import {
  createAccount,
  getUserAccount,
  updateAccountShortProfile,
  updateCheckoutDetails,
  changePassword,
  logIn,
  logOut,
  forgottenPassword,
} from '../utilis/userAccount'
import { addItemToShoppingBag } from '../utilis/shoppingBag'
import { getProducts } from '../utilis/selectProducts'
import { payOrder } from '../utilis/payOrder'

describe('It should return the My Account Json Schema', () => {
  let products
  beforeAll(async () => {
    products = await getProducts()
  }, 60000)

  describe('My Account Full Profile', () => {
    let newAccount
    let shoppingBag
    let account
    beforeAll(async () => {
      newAccount = await createAccount()
      shoppingBag = await addItemToShoppingBag(
        newAccount.jsessionid,
        products.productsSimpleId
      )
      await payOrder(newAccount.jsessionid, shoppingBag.orderId)
      account = await getUserAccount(newAccount.jsessionid)
    }, 60000)

    it(
      'My Account - User Full Checkout Profile Schema',
      () => {
        const fullAccountSchema = {
          title: 'My Account - User Full Profile Schema',
          type: 'object',
          required: [
            'exists',
            'email',
            'title',
            'firstName',
            'lastName',
            'userTrackingId',
            'subscriptionId',
            'basketItemCount',
            'creditCard',
            'deliveryDetails',
            'billingDetails',
          ],
          properties: {
            exists: booleanType(true),
            email: stringType,
            title: stringType,
            firstName: stringType,
            lastName: stringType,
            userTrackingId: numberType,
            subscriptionId: numberType,
            basketItemCount: numberType,
            creditCard: objectType,
            deliveryDetails: objectType,
            billingDetails: objectType,
          },
        }
        assert.jsonSchema(account, fullAccountSchema)
      },
      30000
    )

    it(
      'My Account - User Full Checkout Profile Credit Card Schema',
      () => {
        const creditCardSchema = {
          title:
            'My Account - User Full Profile Schema Credit Card Object Schema',
          type: 'object',
          required: [
            'type',
            'cardNumberHash',
            'cardNumberStar',
            'expiryMonth',
            'expiryYear',
          ],
          properties: {
            type: stringType,
            cardNumberHash: stringType,
            cardNumberStar: stringType,
            expiryMonth: stringType,
            expiryYear: stringType,
          },
        }
        assert.jsonSchema(account.creditCard, creditCardSchema)
      },
      30000
    )

    it(
      'My Account - User Full Checkout Profile Billing Details Schema',
      () => {
        const billingDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Billing Details Object Schema',
          type: 'object',
          required: ['addressDetailsId', 'nameAndPhone', 'address'],
          properties: {
            addressDetailsId: numberType,
            nameAndPhone: objectType,
            address: objectType,
          },
        }
        assert.jsonSchema(account.billingDetails, billingDetailsSchema)
      },
      30000
    )

    it(
      'My Account - User Full Checkout Profile Billing Details nameAndPhone Schema',
      () => {
        const billingDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Billing Details NameAndPhone Schema',
          type: 'object',
          required: ['lastName', 'telephone', 'title', 'firstName'],
          properties: {
            lastName: stringType,
            telephone: stringType,
            title: stringType,
            firstName: stringType,
          },
        }
        assert.jsonSchema(
          account.billingDetails.nameAndPhone,
          billingDetailsSchema
        )
      },
      30000
    )

    it(
      'My Account - User Full Checkout Profile Billing Details address Schema',
      () => {
        const billingDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Billing Details Address Schema',
          type: 'object',
          required: [
            'address1',
            'address2',
            'city',
            'state',
            'country',
            'postcode',
          ],
          properties: {
            address1: stringType,
            address2: stringType,
            city: stringType,
            state: stringTypeEmpty,
            country: stringType,
            postcode: stringType,
          },
        }
        assert.jsonSchema(account.billingDetails.address, billingDetailsSchema)
      },
      30000
    )

    it(
      'My Account - User Full Checkout Profile Delivery Details Schema',
      () => {
        const deliveryDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Delivery Details Object Schema',
          type: 'object',
          required: ['addressDetailsId', 'nameAndPhone', 'address'],
          properties: {
            addressDetailsId: numberType,
            nameAndPhone: objectType,
            address: objectType,
          },
        }
        assert.jsonSchema(account.deliveryDetails, deliveryDetailsSchema)
      },
      30000
    )

    it(
      'My Account - User Full Checkout Profile Delivery Details nameAndPhone Schema',
      () => {
        const deliveryDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Delivery Details NameAndPhone Schema',
          type: 'object',
          required: ['lastName', 'telephone', 'title', 'firstName'],
          properties: {
            lastName: stringType,
            telephone: stringType,
            title: stringType,
            firstName: stringType,
          },
        }
        assert.jsonSchema(
          account.deliveryDetails.nameAndPhone,
          deliveryDetailsSchema
        )
      },
      30000
    )

    it(
      'My Account - User Full Checkout Profile Delivery Details address Schema',
      () => {
        const deliveryDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Delivery Details Address Schema',
          type: 'object',
          required: [
            'address1',
            'address2',
            'city',
            'state',
            'country',
            'postcode',
          ],
          properties: {
            address1: stringType,
            address2: stringType,
            city: stringType,
            state: stringTypeEmpty,
            country: stringType,
            postcode: stringType,
          },
        }
        assert.jsonSchema(
          account.deliveryDetails.address,
          deliveryDetailsSchema
        )
      },
      30000
    )
  })

  describe('My Account Partial Profile', () => {
    let newAccount
    beforeAll(async () => {
      newAccount = await createAccount()
    }, 30000)

    it(
      'My Account - User Partial Checkout Profile Schema',
      () => {
        const partialAccountSchema = {
          title: 'My Account - User Partial Profile Schema',
          type: 'object',
          required: [
            'exists',
            'email',
            'title',
            'firstName',
            'lastName',
            'basketItemCount',
            'creditCard',
            'deliveryDetails',
            'billingDetails',
          ],
          properties: {
            exists: booleanType(true),
            email: stringType,
            title: stringTypeEmpty,
            firstName: stringTypeEmpty,
            lastName: stringTypeEmpty,
            userTrackingId: numberType,
            subscriptionId: numberType,
            basketItemCount: numberType,
            creditCard: objectType,
            deliveryDetails: objectType,
            billingDetails: objectType,
          },
        }
        assert.jsonSchema(newAccount.accountProfile, partialAccountSchema)
      },
      30000
    )

    it(
      'My Account - User Partial Checkout Profile Credit Card Schema',
      () => {
        const creditCardSchema = {
          title:
            'My Account - User Partial Profile Schema Credit Card Object Schema',
          type: 'object',
          required: [
            'type',
            'cardNumberHash',
            'cardNumberStar',
            'expiryMonth',
            'expiryYear',
          ],
          properties: {
            type: stringTypeEmpty,
            cardNumberHash: stringTypeEmpty,
            cardNumberStar: stringTypeEmpty,
            expiryMonth: stringTypeEmpty,
            expiryYear: stringTypeEmpty,
          },
        }
        assert.jsonSchema(
          newAccount.accountProfile.creditCard,
          creditCardSchema
        )
      },
      30000
    )

    it(
      'My Account - User Partial Checkout Profile Billing Details Schema',
      () => {
        const billingDetailsSchema = {
          title:
            'My Account - User Partial Profile Schema Billing Details Object Schema',
          type: 'object',
          required: ['addressDetailsId', 'nameAndPhone', 'address'],
          properties: {
            addressDetailsId: numberTypePattern(-1, -1),
            nameAndPhone: objectType,
            address: objectType,
          },
        }
        assert.jsonSchema(
          newAccount.accountProfile.billingDetails,
          billingDetailsSchema
        )
      },
      30000
    )

    it(
      'My Account - User Partial Checkout Profile Billing Details nameAndPhone Schema',
      () => {
        const billingDetailsSchema = {
          title:
            'My Account - User Partial Profile Schema Billing Details NameAndPhone Schema',
          type: 'object',
          required: ['lastName', 'telephone', 'title', 'firstName'],
          properties: {
            lastName: stringTypeEmpty,
            telephone: stringTypeEmpty,
            title: stringTypeEmpty,
            firstName: stringTypeEmpty,
          },
        }
        assert.jsonSchema(
          newAccount.accountProfile.billingDetails.nameAndPhone,
          billingDetailsSchema
        )
      },
      30000
    )

    it(
      'My Account - User Partial Checkout Profile Billing Details address Schema',
      () => {
        const billingDetailsSchema = {
          title:
            'My Account - User Partial Profile Schema Billing Details Address Schema',
          type: 'object',
          required: [
            'address1',
            'address2',
            'city',
            'state',
            'country',
            'postcode',
          ],
          properties: {
            address1: stringTypeEmpty,
            address2: stringTypeEmpty,
            city: stringTypeEmpty,
            state: stringTypeEmpty,
            country: stringTypeEmpty,
            postcode: stringTypeEmpty,
          },
        }
        assert.jsonSchema(
          newAccount.accountProfile.billingDetails.address,
          billingDetailsSchema
        )
      },
      30000
    )

    it(
      'My Account - User Partial Checkout Profile Delivery Details Schema',
      () => {
        const deliveryDetailsSchema = {
          title:
            'My Account - User Partial Profile Schema Delivery Details Schema',
          type: 'object',
          required: ['addressDetailsId', 'nameAndPhone', 'address'],
          properties: {
            addressDetailsId: numberTypePattern(-1, -1),
            nameAndPhone: objectType,
            address: objectType,
          },
        }
        assert.jsonSchema(
          newAccount.accountProfile.deliveryDetails,
          deliveryDetailsSchema
        )
      },
      30000
    )

    it(
      'My Account - User Partial Checkout Profile Delivery Details nameAndPhone Schema',
      () => {
        const deliveryDetailsSchema = {
          title:
            'My Account - User Partial Profile Schema Delivery Details NameAndPhone Schema',
          type: 'object',
          required: ['lastName', 'telephone', 'title', 'firstName'],
          properties: {
            lastName: stringTypeEmpty,
            telephone: stringTypeEmpty,
            title: stringTypeEmpty,
            firstName: stringTypeEmpty,
          },
        }
        assert.jsonSchema(
          newAccount.accountProfile.deliveryDetails.nameAndPhone,
          deliveryDetailsSchema
        )
      },
      30000
    )

    it(
      'My Account - User Partial Checkout Profile Delivery Details address Schema',
      () => {
        const deliveryDetailsSchema = {
          title:
            'My Account - User Partial Profile Schema Delivery Details Address Schema',
          type: 'object',
          required: [
            'address1',
            'address2',
            'city',
            'state',
            'country',
            'postcode',
          ],
          properties: {
            address1: stringTypeEmpty,
            address2: stringTypeEmpty,
            city: stringTypeEmpty,
            state: stringTypeEmpty,
            country: stringTypeEmpty,
            postcode: stringTypeEmpty,
          },
        }
        assert.jsonSchema(
          newAccount.accountProfile.deliveryDetails.address,
          deliveryDetailsSchema
        )
      },
      30000
    )
  })

  describe('My Account Change Password', () => {
    let newAccount
    let updatedPassword
    let userLogin
    beforeAll(async () => {
      newAccount = await createAccount()
      updatedPassword = await changePassword(
        newAccount.jsessionid,
        newAccount.email
      )
      await logOut()
      userLogin = await logIn({
        username: newAccount.accountProfile.email,
        password: updatedPassword.newPassword,
      })
    }, 30000)

    it(
      'Change Password Json Schema',
      () => {
        const partialAccountSchema = {
          title: 'Change Password - User Partial Profile Schema',
          type: 'object',
          required: [
            'exists',
            'email',
            'title',
            'firstName',
            'lastName',
            'basketItemCount',
            'creditCard',
            'deliveryDetails',
            'billingDetails',
          ],
          properties: {
            exists: booleanType(true),
            email: stringTypePattern(newAccount.accountProfile.email),
            title: stringTypeEmpty,
            firstName: stringTypeEmpty,
            lastName: stringTypeEmpty,
            basketItemCount: numberType,
            creditCard: objectType,
            deliveryDetails: objectType,
            billingDetails: objectType,
          },
        }
        assert.jsonSchema(userLogin, partialAccountSchema)
      },
      30000
    )

    it(
      'Change Password - User Partial Checkout Profile Credit Card Schema',
      () => {
        const creditCardSchema = {
          title:
            'Change Password - User Partial Profile Schema Credit Card Object Schema',
          type: 'object',
          required: [
            'type',
            'cardNumberHash',
            'cardNumberStar',
            'expiryMonth',
            'expiryYear',
          ],
          properties: {
            type: stringTypeEmpty,
            cardNumberHash: stringTypeEmpty,
            cardNumberStar: stringTypeEmpty,
            expiryMonth: stringTypeEmpty,
            expiryYear: stringTypeEmpty,
          },
        }
        assert.jsonSchema(userLogin.creditCard, creditCardSchema)
      },
      30000
    )

    it(
      'Change Password - User Partial Checkout Profile Billing Details Schema',
      () => {
        const billingDetailsSchema = {
          title:
            'Change Password - User Partial Profile Schema Billing Details Object Schema',
          type: 'object',
          required: ['addressDetailsId', 'nameAndPhone', 'address'],
          properties: {
            addressDetailsId: {
              type: 'number',
            },
            nameAndPhone: {
              type: 'object',
            },
            address: {
              type: 'object',
            },
          },
        }
        assert.jsonSchema(userLogin.billingDetails, billingDetailsSchema)
      },
      30000
    )

    it(
      'Change Password - User Partial Checkout Profile Billing Details nameAndPhone Schema',
      () => {
        const billingDetailsSchema = {
          title:
            'Change Password - User Partial Profile Schema Billing Details NameAndPhone Schema',
          type: 'object',
          required: ['lastName', 'telephone', 'title', 'firstName'],
          properties: {
            lastName: stringTypeEmpty,
            telephone: stringTypeEmpty,
            title: stringTypeEmpty,
            firstName: stringTypeEmpty,
          },
        }
        assert.jsonSchema(
          userLogin.billingDetails.nameAndPhone,
          billingDetailsSchema
        )
      },
      30000
    )

    it(
      'Change Password - User Partial Checkout Profile Billing Details address Schema',
      () => {
        const billingDetailsSchema = {
          title:
            'Change Password - User Partial Profile Schema Billing Details Address Schema',
          type: 'object',
          required: [
            'address1',
            'address2',
            'city',
            'state',
            'country',
            'postcode',
          ],
          properties: {
            address1: stringTypeEmpty,
            address2: stringTypeEmpty,
            city: stringTypeEmpty,
            state: stringTypeEmpty,
            country: stringTypeEmpty,
            postcode: stringTypeEmpty,
          },
        }
        assert.jsonSchema(
          userLogin.billingDetails.address,
          billingDetailsSchema
        )
      },
      30000
    )

    it(
      'Change Password - User Partial Checkout Profile Delivery Details Schema',
      () => {
        const deliveryDetailsSchema = {
          title:
            'Change Password - User Partial Profile Schema Delivery Details Schema',
          type: 'object',
          required: ['addressDetailsId', 'nameAndPhone', 'address'],
          properties: {
            addressDetailsId: numberType,
            nameAndPhone: objectType,
            address: objectType,
          },
        }
        assert.jsonSchema(userLogin.deliveryDetails, deliveryDetailsSchema)
      },
      30000
    )

    it(
      'Change Password - User Partial Checkout Profile Delivery Details nameAndPhone Schema',
      () => {
        const deliveryDetailsSchema = {
          title:
            'Change Password - User Partial Profile Schema Delivery Details NameAndPhone Schema',
          type: 'object',
          required: ['lastName', 'telephone', 'title', 'firstName'],
          properties: {
            lastName: stringTypeEmpty,
            telephone: stringTypeEmpty,
            title: stringTypeEmpty,
            firstName: stringTypeEmpty,
          },
        }
        assert.jsonSchema(
          userLogin.deliveryDetails.nameAndPhone,
          deliveryDetailsSchema
        )
      },
      30000
    )

    it(
      'Change Password - User Partial Checkout Profile Delivery Details address Schema',
      () => {
        const deliveryDetailsSchema = {
          title:
            'Change Password - User Partial Profile Schema Delivery Details Address Schema',
          type: 'object',
          required: [
            'address1',
            'address2',
            'city',
            'state',
            'country',
            'postcode',
          ],
          properties: {
            address1: stringTypeEmpty,
            address2: stringTypeEmpty,
            city: stringTypeEmpty,
            state: stringTypeEmpty,
            country: stringTypeEmpty,
            postcode: stringTypeEmpty,
          },
        }
        assert.jsonSchema(
          userLogin.deliveryDetails.address,
          deliveryDetailsSchema
        )
      },
      30000
    )

    it('Change Password - Native App', async () => {
      newAccount = await createAccount()
      try {
        updatedPassword = await changePassword(
          newAccount.jsessionid,
          newAccount.email,
          'app'
        )
      } catch (e) {
        updatedPassword = e.response.statusCode
      }
      expect(updatedPassword).toBe(500)
    })
  })

  describe('My Account Forgotten Password Json Schema', () => {
    let newAccount
    let passwordForgotten
    beforeAll(async () => {
      newAccount = await createAccount()
      await logOut()
      passwordForgotten = await forgottenPassword(
        newAccount.accountProfile.email
      )
    }, 30000)

    it(
      'Forgotten Password',
      () => {
        const forgotPasswordSchema = {
          title: 'Forgot Password',
          type: 'object',
          required: [
            'success',
            'message',
            'originalMessage',
            'additionalData',
            'validationErrors',
          ],
          properties: {
            success: booleanType(true),
            message: stringTypePattern(
              `Your password has been reset successfully. A new password has been e-mailed to you and should arrive shortly.`
            ),
            originalMessage: stringTypePattern(
              `Your password has been reset successfully. A new password has been e-mailed to you and should arrive shortly.`
            ),
            additionalData: arrayType(),
            validationErrors: arrayType(),
          },
        }
        assert.jsonSchema(passwordForgotten, forgotPasswordSchema)
      },
      30000
    )
  })

  describe('My Account Short Profile Update Json Schema', () => {
    let newAccount
    let shortProfile
    let userLogin
    beforeAll(async () => {
      newAccount = await createAccount()
      shortProfile = await updateAccountShortProfile(newAccount.jsessionid)
    }, 30000)

    it(
      'Account Short Profile Update',
      () => {
        const registerAccountSchema = {
          title: 'Account Short Profile Update Schema',
          type: 'object',
          required: [
            'exists',
            'email',
            'title',
            'firstName',
            'lastName',
            'userTrackingId',
            'subscriptionId',
            'basketItemCount',
            'creditCard',
            'deliveryDetails',
            'billingDetails',
          ],
          properties: {
            exists: booleanType(true),
            email: stringTypePattern(shortProfile.email),
            title: stringTypePattern(shortProfile.title),
            firstName: stringTypePattern(shortProfile.firstName),
            lastName: stringTypePattern(shortProfile.lastName),
            userTrackingId: numberType,
            subscriptionId: stringTypeEmpty,
            basketItemCount: numberTypePattern(0),
            creditCard: objectType,
            deliveryDetails: objectType,
            billingDetails: objectType,
          },
        }
        assert.jsonSchema(shortProfile, registerAccountSchema)
      },
      30000
    )

    it(
      'Should logon with the new user email',
      async () => {
        userLogin = await logIn({
          username: shortProfile.email,
          password: 'monty1',
        })
        const partialAccountSchema = {
          title: 'My Account - User Partial Profile Schema',
          type: 'object',
          required: [
            'exists',
            'email',
            'title',
            'firstName',
            'lastName',
            'userTrackingId',
            'subscriptionId',
            'basketItemCount',
            'creditCard',
            'deliveryDetails',
            'billingDetails',
          ],
          properties: {
            exists: booleanType(true),
            email: stringTypePattern(shortProfile.email),
            title: stringTypePattern(shortProfile.title),
            firstName: stringTypePattern(shortProfile.firstName),
            lastName: stringTypePattern(shortProfile.lastName),
            userTrackingId: numberType,
            subscriptionId: stringTypeEmpty,
            basketItemCount: numberType,
            creditCard: objectType,
            deliveryDetails: objectType,
            billingDetails: objectType,
          },
        }
        assert.jsonSchema(userLogin, partialAccountSchema)
      },
      30000
    )
  })

  describe('My Account Update My Checkout Details', () => {
    let newAccount
    let account
    let shoppingBag
    let checkoutProfile
    beforeAll(async () => {
      newAccount = await createAccount()
      shoppingBag = await addItemToShoppingBag(
        newAccount.jsessionid,
        products.productsSimpleId
      )
      await payOrder(newAccount.jsessionid, shoppingBag.orderId)
      checkoutProfile = await updateCheckoutDetails(
        newAccount.jsessionid,
        'VISA',
        '1111222233334444'
      )
      account = await getUserAccount(newAccount.jsessionid)
    }, 60000)

    it(
      'My Account - Update Full Checkout Profile Schema',
      () => {
        const fullAccountSchema = {
          title: 'My Account - User Full Profile Schema',
          type: 'object',
          required: [
            'exists',
            'email',
            'title',
            'firstName',
            'lastName',
            'userTrackingId',
            'subscriptionId',
            'basketItemCount',
            'creditCard',
            'deliveryDetails',
            'billingDetails',
          ],
          properties: {
            exists: booleanType(true),
            email: stringTypePattern(newAccount.accountProfile.username),
            title: stringTypePattern(
              checkoutProfile.billingDetails.nameAndPhone.title
            ),
            firstName: stringTypePattern(
              checkoutProfile.billingDetails.nameAndPhone.firstName
            ),
            lastName: stringTypePattern(
              checkoutProfile.billingDetails.nameAndPhone.lastName
            ),
            userTrackingId: numberType,
            subscriptionId: numberType,
            basketItemCount: numberType,
            creditCard: objectType,
            deliveryDetails: objectType,
            billingDetails: objectType,
          },
        }
        assert.jsonSchema(checkoutProfile, fullAccountSchema)
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Billing Details Schema',
      () => {
        const billingDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Billing Details Object Schema',
          type: 'object',
          required: ['addressDetailsId', 'nameAndPhone', 'address'],
          properties: {
            addressDetailsId: numberType,
            nameAndPhone: objectType,
            address: objectType,
          },
        }
        assert.jsonSchema(checkoutProfile.billingDetails, billingDetailsSchema)
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Billing Details nameAndPhone Schema',
      () => {
        const billingDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Billing Details NameAndPhone Schema',
          type: 'object',
          required: ['lastName', 'telephone', 'title', 'firstName'],
          properties: {
            lastName: stringTypePattern(
              checkoutProfile.billingDetails.nameAndPhone.lastName
            ),
            telephone: stringTypePattern(
              checkoutProfile.billingDetails.nameAndPhone.telephone
            ),
            title: stringTypePattern(
              checkoutProfile.billingDetails.nameAndPhone.title
            ),
            firstName: stringTypePattern(
              checkoutProfile.billingDetails.nameAndPhone.firstName
            ),
          },
        }
        assert.jsonSchema(
          checkoutProfile.billingDetails.nameAndPhone,
          billingDetailsSchema
        )
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Billing Details address Schema',
      () => {
        const billingDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Billing Details Address Schema',
          type: 'object',
          required: [
            'address1',
            'address2',
            'city',
            'state',
            'country',
            'postcode',
          ],
          properties: {
            address1: stringTypePattern(
              checkoutProfile.billingDetails.address.address1
            ),
            address2: stringTypePattern(
              checkoutProfile.billingDetails.address.address2
            ),
            city: stringTypePattern(
              checkoutProfile.billingDetails.address.city
            ),
            state: stringTypeEmpty,
            country: stringTypePattern(
              checkoutProfile.billingDetails.address.country
            ),
            postcode: stringTypePattern(
              checkoutProfile.billingDetails.address.postcode
            ),
          },
        }
        assert.jsonSchema(
          checkoutProfile.billingDetails.address,
          billingDetailsSchema
        )
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Delivery Details Schema',
      () => {
        const deliveryDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Delivery Details Object Schema',
          type: 'object',
          required: ['addressDetailsId', 'nameAndPhone', 'address'],
          properties: {
            addressDetailsId: numberType,
            nameAndPhone: objectType,
            address: objectType,
          },
        }
        assert.jsonSchema(
          checkoutProfile.deliveryDetails,
          deliveryDetailsSchema
        )
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Delivery Details nameAndPhone Schema',
      () => {
        const deliveryDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Delivery Details NameAndPhone Schema',
          type: 'object',
          required: ['lastName', 'telephone', 'title', 'firstName'],
          properties: {
            lastName: stringTypePattern(
              checkoutProfile.billingDetails.nameAndPhone.lastName
            ),
            telephone: stringTypePattern(
              checkoutProfile.billingDetails.nameAndPhone.telephone
            ),
            title: stringTypePattern(
              checkoutProfile.billingDetails.nameAndPhone.title
            ),
            firstName: stringTypePattern(
              checkoutProfile.billingDetails.nameAndPhone.firstName
            ),
          },
        }
        assert.jsonSchema(
          checkoutProfile.deliveryDetails.nameAndPhone,
          deliveryDetailsSchema
        )
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Delivery Details address Schema',
      () => {
        const deliveryDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Delivery Details Address Schema',
          type: 'object',
          required: [
            'address1',
            'address2',
            'city',
            'state',
            'country',
            'postcode',
          ],
          properties: {
            address1: stringTypePattern(
              checkoutProfile.billingDetails.address.address1
            ),
            address2: stringTypePattern(
              checkoutProfile.billingDetails.address.address2
            ),
            city: stringTypePattern(
              checkoutProfile.billingDetails.address.city
            ),
            state: stringTypePattern(),
            country: stringTypePattern(
              checkoutProfile.billingDetails.address.country
            ),
            postcode: stringTypePattern(
              checkoutProfile.billingDetails.address.postcode
            ),
          },
        }
        assert.jsonSchema(
          checkoutProfile.deliveryDetails.address,
          deliveryDetailsSchema
        )
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Credit Card Schema VISA',
      () => {
        const creditCardSchema = {
          title:
            'My Account - User Full Profile Schema Credit Card Object Schema',
          type: 'object',
          required: [
            'type',
            'cardNumberHash',
            'cardNumberStar',
            'expiryMonth',
            'expiryYear',
          ],
          properties: {
            type: stringTypePattern(checkoutProfile.creditCard.type),
            cardNumberHash: stringType,
            cardNumberStar: stringType,
            expiryMonth: stringTypePattern(
              checkoutProfile.creditCard.expiryMonth
            ),
            expiryYear: stringTypePattern(
              checkoutProfile.creditCard.expiryYear
            ),
          },
        }
        assert.jsonSchema(checkoutProfile.creditCard, creditCardSchema)
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Credit Card Schema MASTERCARD',
      async () => {
        checkoutProfile = await updateCheckoutDetails(
          newAccount.jsessionid,
          'MCARD',
          '5500000000000004'
        )
        const creditCardSchema = {
          title:
            'My Account - User Full Profile Schema Credit Card Object Schema',
          type: 'object',
          required: [
            'type',
            'cardNumberHash',
            'cardNumberStar',
            'expiryMonth',
            'expiryYear',
          ],
          properties: {
            type: stringTypePattern(checkoutProfile.creditCard.type),
            cardNumberHash: stringType,
            cardNumberStar: stringType,
            expiryMonth: stringTypePattern(
              checkoutProfile.creditCard.expiryMonth
            ),
            expiryYear: stringTypePattern(
              checkoutProfile.creditCard.expiryYear
            ),
          },
        }
        assert.jsonSchema(checkoutProfile.creditCard, creditCardSchema)
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Credit Card Schema AMERICAN EXPRESS',
      async () => {
        checkoutProfile = await updateCheckoutDetails(
          newAccount.jsessionid,
          'AMEX',
          '340000000000009'
        )
        const creditCardSchema = {
          title:
            'My Account - User Full Profile Schema Credit Card Object Schema',
          type: 'object',
          required: [
            'type',
            'cardNumberHash',
            'cardNumberStar',
            'expiryMonth',
            'expiryYear',
          ],
          properties: {
            type: stringTypePattern(checkoutProfile.creditCard.type),
            cardNumberHash: stringType,
            cardNumberStar: stringType,
            expiryMonth: stringTypePattern(
              checkoutProfile.creditCard.expiryMonth
            ),
            expiryYear: stringTypePattern(
              checkoutProfile.creditCard.expiryYear
            ),
          },
        }
        assert.jsonSchema(checkoutProfile.creditCard, creditCardSchema)
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Credit Card Schema MAESTRO',
      async () => {
        checkoutProfile = await updateCheckoutDetails(
          newAccount.jsessionid,
          'SWTCH',
          '6759649826438453'
        )
        const creditCardSchema = {
          title:
            'My Account - User Full Profile Schema Credit Card Object Schema',
          type: 'object',
          required: [
            'type',
            'cardNumberHash',
            'cardNumberStar',
            'expiryMonth',
            'expiryYear',
          ],
          properties: {
            type: stringTypePattern(checkoutProfile.creditCard.type),
            cardNumberHash: stringType,
            cardNumberStar: stringType,
            expiryMonth: stringTypePattern(
              checkoutProfile.creditCard.expiryMonth
            ),
            expiryYear: stringTypePattern(
              checkoutProfile.creditCard.expiryYear
            ),
          },
        }
        assert.jsonSchema(checkoutProfile.creditCard, creditCardSchema)
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Credit Card Schema ACCOUNT NUMBER',
      async () => {
        checkoutProfile = await updateCheckoutDetails(
          newAccount.jsessionid,
          'ACCNT',
          '2003000092441196'
        )
        const creditCardSchema = {
          title:
            'My Account - User Full Profile Schema Credit Card Object Schema',
          type: 'object',
          required: [
            'type',
            'cardNumberHash',
            'cardNumberStar',
            'expiryMonth',
            'expiryYear',
          ],
          properties: {
            type: stringTypePattern(checkoutProfile.creditCard.type),
            cardNumberHash: stringType,
            cardNumberStar: stringType,
            expiryMonth: stringTypePattern(
              checkoutProfile.creditCard.expiryMonth
            ),
            expiryYear: stringTypePattern(
              checkoutProfile.creditCard.expiryYear
            ),
          },
        }
        assert.jsonSchema(checkoutProfile.creditCard, creditCardSchema)
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Credit Card Schema PAYPAL',
      async () => {
        checkoutProfile = await updateCheckoutDetails(
          newAccount.jsessionid,
          'PYPAL',
          '0'
        )
        const creditCardSchema = {
          title:
            'My Account - User Full Profile Schema Credit Card Object Schema',
          type: 'object',
          required: [
            'type',
            'cardNumberHash',
            'cardNumberStar',
            'expiryMonth',
            'expiryYear',
          ],
          properties: {
            type: stringTypePattern(checkoutProfile.creditCard.type),
            cardNumberHash: stringType,
            cardNumberStar: stringType,
            expiryMonth: stringTypePattern(
              checkoutProfile.creditCard.expiryMonth
            ),
            expiryYear: stringTypePattern(
              checkoutProfile.creditCard.expiryYear
            ),
          },
        }
        assert.jsonSchema(checkoutProfile.creditCard, creditCardSchema)
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Credit Card Schema MASTERPASS',
      async () => {
        checkoutProfile = await updateCheckoutDetails(
          newAccount.jsessionid,
          'MPASS',
          '0'
        )
        const creditCardSchema = {
          title:
            'My Account - User Full Profile Schema Credit Card Object Schema',
          type: 'object',
          required: [
            'type',
            'cardNumberHash',
            'cardNumberStar',
            'expiryMonth',
            'expiryYear',
          ],
          properties: {
            type: stringTypePattern(checkoutProfile.creditCard.type),
            cardNumberHash: stringType,
            cardNumberStar: stringType,
            expiryMonth: stringTypePattern(
              checkoutProfile.creditCard.expiryMonth
            ),
            expiryYear: stringTypePattern(
              checkoutProfile.creditCard.expiryYear
            ),
          },
        }
        assert.jsonSchema(checkoutProfile.creditCard, creditCardSchema)
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Credit Card Schema KLARNA',
      async () => {
        checkoutProfile = await updateCheckoutDetails(
          newAccount.jsessionid,
          'KLRNA',
          '0'
        )
        const creditCardSchema = {
          title:
            'My Account - User Full Profile Schema Credit Card Object Schema',
          type: 'object',
          required: [
            'type',
            'cardNumberHash',
            'cardNumberStar',
            'expiryMonth',
            'expiryYear',
          ],
          properties: {
            type: stringTypePattern(checkoutProfile.creditCard.type),
            cardNumberHash: stringType,
            cardNumberStar: stringType,
            expiryMonth: stringTypePattern(
              checkoutProfile.creditCard.expiryMonth
            ),
            expiryYear: stringTypePattern(
              checkoutProfile.creditCard.expiryYear
            ),
          },
        }
        assert.jsonSchema(checkoutProfile.creditCard, creditCardSchema)
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Check GET Account Schema',
      async () => {
        const fullAccountSchema = {
          title: 'My Account - User Full Profile Schema',
          type: 'object',
          required: [
            'exists',
            'email',
            'title',
            'firstName',
            'lastName',
            'userTrackingId',
            'subscriptionId',
            'basketItemCount',
            'creditCard',
            'deliveryDetails',
            'billingDetails',
          ],
          properties: {
            exists: booleanType(true),
            email: stringTypePattern(account.username),
            title: stringTypePattern(account.billingDetails.nameAndPhone.title),
            firstName: stringTypePattern(
              account.billingDetails.nameAndPhone.firstName
            ),
            lastName: stringTypePattern(
              account.billingDetails.nameAndPhone.lastName
            ),
            userTrackingId: numberType,
            subscriptionId: numberType,
            basketItemCount: numberType,
            creditCard: objectType,
            deliveryDetails: objectType,
            billingDetails: objectType,
          },
        }
        assert.jsonSchema(account, fullAccountSchema)
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Check GET Account Billing Details Schema',
      () => {
        const billingDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Billing Details Object Schema',
          type: 'object',
          required: ['addressDetailsId', 'nameAndPhone', 'address'],
          properties: {
            addressDetailsId: numberType,
            nameAndPhone: objectType,
            address: objectType,
          },
        }
        assert.jsonSchema(account.billingDetails, billingDetailsSchema)
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Check GET Account Billing Details nameAndPhone Schema',
      () => {
        const billingDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Billing Details NameAndPhone Schema',
          type: 'object',
          required: ['lastName', 'telephone', 'title', 'firstName'],
          properties: {
            lastName: stringTypePattern(
              account.billingDetails.nameAndPhone.lastName
            ),
            telephone: stringTypePattern(
              account.billingDetails.nameAndPhone.telephone
            ),
            title: stringTypePattern(account.billingDetails.nameAndPhone.title),
            firstName: stringTypePattern(
              account.billingDetails.nameAndPhone.firstName
            ),
          },
        }
        assert.jsonSchema(
          account.billingDetails.nameAndPhone,
          billingDetailsSchema
        )
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Check GET Account Billing Details address Schema',
      () => {
        const billingDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Billing Details Address Schema',
          type: 'object',
          required: [
            'address1',
            'address2',
            'city',
            'state',
            'country',
            'postcode',
          ],
          properties: {
            address1: stringTypePattern(
              account.billingDetails.address.address1
            ),
            address2: stringTypePattern(
              account.billingDetails.address.address2
            ),
            city: stringTypePattern(account.billingDetails.address.city),
            state: stringTypeEmpty,
            country: stringTypePattern(account.billingDetails.address.country),
            postcode: stringTypePattern(
              account.billingDetails.address.postcode
            ),
          },
        }
        assert.jsonSchema(account.billingDetails.address, billingDetailsSchema)
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Check GET Account Delivery Details Schema',
      () => {
        const deliveryDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Delivery Details Object Schema',
          type: 'object',
          required: ['addressDetailsId', 'nameAndPhone', 'address'],
          properties: {
            addressDetailsId: numberType,
            nameAndPhone: objectType,
            address: objectType,
          },
        }
        assert.jsonSchema(account.deliveryDetails, deliveryDetailsSchema)
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Check GET Account Delivery Details nameAndPhone Schema',
      () => {
        const deliveryDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Delivery Details NameAndPhone Schema',
          type: 'object',
          required: ['lastName', 'telephone', 'title', 'firstName'],
          properties: {
            lastName: stringTypePattern(
              account.billingDetails.nameAndPhone.lastName
            ),
            telephone: stringTypePattern(
              account.billingDetails.nameAndPhone.telephone
            ),
            title: stringTypePattern(account.billingDetails.nameAndPhone.title),
            firstName: stringTypePattern(
              account.billingDetails.nameAndPhone.firstName
            ),
          },
        }
        assert.jsonSchema(
          account.deliveryDetails.nameAndPhone,
          deliveryDetailsSchema
        )
      },
      30000
    )

    it(
      'My Account - Update Full Checkout Profile Check GET Account Delivery Details address Schema',
      () => {
        const deliveryDetailsSchema = {
          title:
            'My Account - User Full Profile Schema Delivery Details Address Schema',
          type: 'object',
          required: [
            'address1',
            'address2',
            'city',
            'state',
            'country',
            'postcode',
          ],
          properties: {
            address1: stringTypePattern(
              account.billingDetails.address.address1
            ),
            address2: stringTypePattern(
              account.billingDetails.address.address2
            ),
            city: stringTypePattern(account.billingDetails.address.city),
            state: stringTypePattern(),
            country: stringTypePattern(account.billingDetails.address.country),
            postcode: stringTypePattern(
              account.billingDetails.address.postcode
            ),
          },
        }
        assert.jsonSchema(
          account.deliveryDetails.address,
          deliveryDetailsSchema
        )
      },
      30000
    )
  })

  describe('My Account Partial Profile on Native App', () => {
    let newAccount
    beforeAll(async () => {
      try {
        newAccount = await createAccount({ deviceType: 'apps' })
      } catch (e) {
        newAccount = e.response.statusCode
      }
    }, 30000)

    it(
      'My Account - User Partial Profile Schema Native App',
      async () => {
        const partialAccountSchema = {
          title: 'My Account - User Partial Profile Schema',
          type: 'object',
          required: [
            'exists',
            'email',
            'title',
            'firstName',
            'lastName',
            'basketItemCount',
            'creditCard',
            'deliveryDetails',
            'billingDetails',
          ],
          properties: {
            exists: booleanType(true),
            email: stringType,
            title: stringTypeEmpty,
            firstName: stringTypeEmpty,
            lastName: stringTypeEmpty,
            userTrackingId: numberType,
            subscriptionId: numberType,
            basketItemCount: numberType,
            creditCard: objectType,
            deliveryDetails: objectType,
            billingDetails: objectType,
          },
        }
        assert.jsonSchema(newAccount.accountProfile, partialAccountSchema)
      },
      30000
    )
  })
})
