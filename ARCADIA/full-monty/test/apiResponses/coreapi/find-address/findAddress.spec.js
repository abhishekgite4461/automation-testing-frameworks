require('babel-polyfill')

import { createAccount } from '../utilis/userAccount'
import chai from 'chai'
import { getProducts } from '../utilis/selectProducts'
import { addItemToShoppingBag } from '../utilis/shoppingBag'
import address from '../utilis/address'
import { stringType, nullType } from '../utilis'

chai.use(require('chai-json-schema'))

const assert = chai.assert

describe('find address in checkout', () => {
  let products
  let findAddress
  describe('find address United Kingdom', () => {
    beforeAll(async () => {
      products = await getProducts()
      const newAccount = await createAccount()
      await addItemToShoppingBag(
        newAccount.jsessionid,
        products.productsSimpleId
      )
      findAddress = await address.findAddress()
    }, 60000)

    it('should contain an array', async () => {
      expect(Array.isArray(findAddress.body)).toBe(true)
      expect(findAddress.body.length).toBe(1)
    })

    it('should return the expected properties', async () => {
      findAddress.body.forEach((address) => {
        const findAddressSchema = {
          title: 'Find Address Properties',
          type: 'object',
          required: ['address', 'moniker'],
          properties: {
            address: stringType,
            moniker: stringType,
          },
        }
        assert.jsonSchema(address, findAddressSchema)
      })
    })

    it('should return a list of addresses', async () => {
      const listOfAddresses = await address.findAddress(undefined, 'HA47HH')
      expect(Array.isArray(listOfAddresses.body)).toBe(true)
      expect(listOfAddresses.body.length).toBeGreaterThan(0)
    })

    it('should return an empty array if address is not found', async () => {
      const emptyArray = await address.findAddress(undefined, 'ZA15ZZ')
      expect(Array.isArray(emptyArray.body)).toBe(true)
      expect(emptyArray.body.length).toBe(0)
    })
  })

  describe('moniker address', () => {
    let addressMoniker

    beforeAll(async () => {
      const newAccount = await createAccount()
      await addItemToShoppingBag(
        newAccount.jsessionid,
        products.productsSimpleId
      )
      findAddress = await address.findAddress()
      addressMoniker = await address.findAddressMoniker(
        findAddress.body[0].moniker
      )
    }, 60000)

    it('should return address with moniker', async () => {
      const findAddressMonikerSchema = {
        title: 'Find Address with Moniker Properties',
        type: 'object',
        required: [
          'address1',
          'address2',
          'city',
          'country',
          'postcode',
          'state',
        ],
        properties: {
          address1: stringType,
          address2: stringType,
          city: stringType,
          country: stringType,
          postcode: stringType,
          state: nullType,
        },
      }
      assert.jsonSchema(addressMoniker.body, findAddressMonikerSchema)
    })
  })
})
