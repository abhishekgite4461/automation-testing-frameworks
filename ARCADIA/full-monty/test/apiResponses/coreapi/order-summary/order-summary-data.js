export const orderSummaryProduct = {
  productId: 21088572,
  sku: '602015000875263',
  quantity: 1,
}
export const orderSummaryAddProductSimpleOneSize = {
  productId: 21919934,
  sku: '602015000890858',
  quantity: 1,
}
export const orderSummaryUser = {
  username: 'orderSummaryUser@noqcprofile.com',
  password: 'test123',
}
export const storeStandardPayload = (orderId) => ({
  orderId,
  deliveryType: 'STORE_STANDARD',
  shippingCountry: 'United Kingdom',
  deliveryStoreCode: 'TS0032',
  storeAddress1: '60/64 The Strand',
  storeAddress2: '',
  storeCity: 'Strand',
  storePostcode: 'WC2N 5LR',
})
export const homeExpressPayload = (orderId) => ({
  orderId,
  deliveryType: 'HOME_EXPRESS',
  shippingCountry: 'United Kingdom',
  shipModeId: 28005,
})
export const storeExpressPayload = (orderId) => ({
  orderId,
  deliveryType: 'STORE_EXPRESS',
  shippingCountry: 'United Kingdom',
  shipModeId: 56018,
  deliveryStoreCode: 'TS0032',
  storeAddress1: '60/64 The Strand',
  storeAddress2: '',
  storeCity: 'Strand',
  storePostcode: 'WC2N 5LR',
})
export const parcelshopPayload = (orderId) => ({
  orderId,
  deliveryType: 'PARCELSHOP_COLLECTION',
  shipModeId: 56017,
  shippingCountry: 'United Kingdom',
  deliveryStoreCode: 'S12419',
  storeAddress1: '456-459 Strand',
  storeAddress2: '',
  storeCity: 'Greater London',
  storePostcode: 'WC2R 0RG',
})
export const homeStandardPayload = (orderId) => ({
  orderId,
  deliveryType: 'HOME_STANDARD',
  shippingCountry: 'United Kingdom',
  shipModeId: 26504,
})
export const sriLankaOrderSummaryUser = {
  username: 'srilankauser1@qcprofile.com',
  password: 'test123',
}
export const trackedInternationalPayload = (orderId, shippingCountry) => ({
  orderId,
  deliveryType: 'HOME_STANDARD',
  shippingCountry,
  shipModeId: 33096,
})
export const standardInternationalPayload = (orderId, shippingCountry) => ({
  orderId,
  deliveryType: 'HOME_STANDARD',
  shippingCountry,
  shipModeId: 34054,
})
