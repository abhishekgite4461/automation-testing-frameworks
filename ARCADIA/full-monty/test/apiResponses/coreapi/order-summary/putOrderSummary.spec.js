/* eslint-disable no-restricted-syntax,no-prototype-builtins */
import { getProducts } from '../utilis/selectProducts'

require('babel-polyfill')

jest.unmock('superagent')
import superagent from 'superagent'
import chai from 'chai'

chai.use(require('chai-json-schema'))

const assert = chai.assert
import eps from '../routes_tests'
import {
  headers,
  objectType,
  arrayType,
  stringType,
  stringTypeCanBeEmpty,
  stringTypeEmpty,
  numberType,
  stringTypePattern,
  booleanType,
  anyBooleanType,
} from '../utilis'
import {
  homeExpressPayload,
  storeStandardPayload,
  storeExpressPayload,
  parcelshopPayload,
  homeStandardPayload,
} from './order-summary-data'
import { createAccount } from '../utilis/userAccount'
import { addItemToShoppingBag } from '../utilis/shoppingBag'

describe('Modifying order delivery methods existing user', () => {
  let products
  let newAccount
  let jsessionId
  let orderId
  let orderSummaryResp

  beforeAll(async () => {
    products = await getProducts()
    newAccount = await createAccount()
    jsessionId = newAccount.jsessionid
    await addItemToShoppingBag(jsessionId, products.productsSimpleId)
    orderSummaryResp = await superagent
      .get(eps.checkout.orderSummary.path)
      .set(headers)
      .set({ Cookie: jsessionId })
    orderId = orderSummaryResp.body.basket.orderId
  }, 60000)

  describe('It should return an Existing User Order Summary Json Schema - Home Standard', () => {
    it(
      'Get Order Summary',
      () => {
        const body = orderSummaryResp.body
        const orderSummarySchema = {
          title: 'Existing User Order Summary',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringType,
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(0, true, 'string'),
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'Get Order Summary => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        const orderSummarySchemaBasket = {
          title: 'Existing User Order Summary => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          optional: ['isDDPOrder', 'isDDPProduct'],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
            isDDPOrder: anyBooleanType,
            isDDPProduct: anyBooleanType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'Get Order Summary => Basket => Delivery Options (Standard Delivery 4.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Existing User Order Summary => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            optional: ['label'],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId: stringType,
              label: stringType,
              enabled: booleanType,
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'Get Order Summary => Basket => Products',
      () => {
        const body = orderSummaryResp.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Existing User Order Summary => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
              'freeItem',
            ],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
              freeItem: anyBooleanType,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'Get Order Summary => Basket => Products => assets',
      () => {
        const body = orderSummaryResp.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Existing User Shopping Bag GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'Get Order Summary => Basket => inventoryPositions Json Schema',
      () => {
        const obj = orderSummaryResp.body.basket.inventoryPositions
        let inv
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              inv = obj[props].invavls
            } else {
              inv = obj[props].inventorys
            }
            const shoppingBagInventoryPositionsProps = {
              partNumber: obj[props].partNumber,
              catentryId: obj[props].catentryId,
              inv,
            }
            let shoppingBagInventoryPositionsSchema
            if (inv !== null) {
              shoppingBagInventoryPositionsSchema = {
                title:
                  'Existing User Shopping Bag Simple Product inventoryPositions Json Schema',
                type: 'object',
                required: ['partNumber', 'catentryId', 'inv'],
                properties: {
                  partNumber: { type: 'string' },
                  catEntryId: { type: 'number' },
                  inv: {
                    type: 'array',
                    minItems: 1,
                    uniqueItems: true,
                    items: { type: 'object' },
                  },
                },
              }
            } else {
              shoppingBagInventoryPositionsSchema = {
                title:
                  'Existing User Shopping Bag Simple Product inventoryPositions Json Schema',
                type: 'object',
                required: ['partNumber', 'catentryId', 'inv'],
                properties: {
                  partNumber: { type: 'string' },
                  catEntryId: { type: 'number' },
                },
              }
            }
            assert.jsonSchema(
              shoppingBagInventoryPositionsProps,
              shoppingBagInventoryPositionsSchema
            )
          }
        }
      },
      30000
    )

    it(
      'Get Order Summary => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = orderSummaryResp.body.basket.inventoryPositions
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              obj[props].invavls.forEach((prop) => {
                const shoppingBagInvAvlsPositionsProps = {
                  cutofftime: prop.cutofftime,
                  quantity: prop.quantity,
                  stlocIdentifier: prop.stlocIdentifier,
                  expressdates: prop.expressdates,
                }
                const shoppingBagInvAvlsPositionsSchema = {
                  title:
                    'Existing User Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
                  type: 'object',
                  required: [
                    'cutofftime',
                    'quantity',
                    'stlocIdentifier',
                    'expressdates',
                  ],
                  properties: {
                    cutofftime: { type: 'string' },
                    quantity: { type: 'number' },
                    stlocIdentifier: { type: 'string' },
                    expressdates: {
                      type: 'array',
                      minItems: 2,
                      uniqueItems: true,
                      items: { type: 'string' },
                    },
                  },
                }
                assert.jsonSchema(
                  shoppingBagInvAvlsPositionsProps,
                  shoppingBagInvAvlsPositionsSchema
                )
              })
            } else if (obj[props].inventorys) {
              obj[props].inventorys.forEach((prop) => {
                const shoppingBagInventorysPositionsProps = {
                  cutofftime: prop.cutofftime,
                  quantity: prop.quantity,
                  ffmcenterId: prop.ffmcenterId,
                  expressdates: prop.expressdates,
                }
                const shoppingBagInventorysPositionsSchema = {
                  title:
                    'Existing User Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
                  type: 'object',
                  required: [
                    'cutofftime',
                    'quantity',
                    'ffmcenterId',
                    'expressdates',
                  ],
                  properties: {
                    cutofftime: { type: 'string' },
                    quantity: { type: 'number' },
                    ffmcenterId: { type: 'number' },
                    expressdates: {
                      type: 'array',
                      minItems: 2,
                      uniqueItems: true,
                      items: { type: 'string' },
                    },
                  },
                }
                assert.jsonSchema(
                  shoppingBagInventorysPositionsProps,
                  shoppingBagInventorysPositionsSchema
                )
              })
            }
          }
        }
      },
      30000
    )

    it(
      'Get Order Summary => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = orderSummaryResp.body.basket.inventoryPositions
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              obj[props].invavls.forEach((prop) => {
                const expressdateSchema = {
                  title: 'Existing User Express date in inventory item',
                  type: 'array',
                  required: ['0', '1'],
                  properties: {
                    0: stringType,
                    1: stringType,
                  },
                }
                assert.jsonSchema(prop.expressdates, expressdateSchema)
              })
            } else if (obj[props].inventorys) {
              obj[props].inventorys.forEach((prop) => {
                const expressdateSchema = {
                  title: 'Existing User Express date in inventory item',
                  type: 'array',
                  required: ['0', '1'],
                  properties: {
                    0: stringType,
                    1: stringType,
                  },
                }
                assert.jsonSchema(prop.expressdates, expressdateSchema)
              })
            }
          }
        }
      },
      30000
    )

    it(
      'Get Order Summary => deliveryLocations Json Schema',
      () => {
        orderSummaryResp.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'Get Order Summary => deliveryLocations => deliveryMethods Json Schema',
      () => {
        orderSummaryResp.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
                'shipCode',
              ],
              optional: ['shipModeId'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'HOME_STANDARD'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'Get Order Summary => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        orderSummaryResp.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Existing User Order Summary => Modifying Delivery Option To Store Standard', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(storeStandardPayload(orderId))
    }, 60000)

    it(
      'PUT Order Summary Existing User Store Standard => Basket',
      () => {
        const body = response.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Standard => Basket => Delivery Options (Free Collect From Store Standard £0.00)',
      () => {
        const body = response.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'enabled',
            ],
            optional: ['label'],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId: stringType,
              label: stringType,
              enabled: booleanType,
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Standard => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: booleanType,
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Standard => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
              ],
              optional: ['shipCode', 'shipModeId'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription: stringTypeCanBeEmpty,
                selected: booleanType,
                deliveryOptions: arrayType(),
                enabled: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'STORE_IMMEDIATE'
                      ? [false]
                      : [true],
                },
                cost: stringType,
                shipCode: stringTypeCanBeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Standard => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Existing user Order Summary => Modifying Delivery Option To Home Express', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: newAccount.jsessionid })
        .send(homeExpressPayload(orderId))
    }, 60000)

    it(
      'PUT Order Summary Existing User Home Express => Basket',
      () => {
        const body = response.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Express => Basket => Delivery Options (Express / Nominated Day Delivery £6.00)',
      () => {
        const body = response.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'enabled',
            ],
            optional: ['label'],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId: stringType,
              label: stringType,
              enabled: booleanType,
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Express => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Express => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
                'shipCode',
              ],
              optional: ['shipModeId'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription: stringTypeCanBeEmpty,
                selected: { type: 'boolean' },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode: stringTypeCanBeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Express => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Existing User Order Summary => Modifying Delivery Option To Parcelshop', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(parcelshopPayload(orderId))
    }, 60000)

    it(
      'PUT Order Summary Existing User Parcelshop => Basket',
      () => {
        const body = response.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Parcelshop => Basket => Delivery Options (Collect from ParcelShop £4.00)',
      () => {
        const body = response.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              optional: ['label'],
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId: stringType,
              label: stringType,
              enabled: booleanType,
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Parcelshop => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              optional: ['label'],
              selected: booleanType,
              label: stringType,
              enabled: booleanType,
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Parcelshop => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
              ],
              properties: {
                deliveryType: stringType,
                label: stringType,
                additionalDescription: stringTypeCanBeEmpty,
                selected: booleanType,
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode: stringTypeCanBeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Parcelshop => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Existing User Order Summary => Modifying Delivery Option To Store Express', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: newAccount.jsessionid })
        .send(storeExpressPayload(orderId))
    }, 60000)

    it(
      'PUT Order Summary Existing User Store Express => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Express => Basket => Delivery Options (Collect From Store Express £3.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'enabled',
            ],
            optional: ['label'],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId: stringType,
              label: stringType,
              enabled: booleanType,
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Express => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: booleanType,
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Express => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'shipModeId',
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'cost',
              ],
              optional: ['shipCode', 'label'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription: stringType,
                selected: booleanType,
                deliveryOptions: arrayType(),
                enabled: booleanType,
                cost: stringType,
                shipCode: stringType,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Express => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Existing User Order Summary => Modifying Delivery Option To Home Standard', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(homeStandardPayload(orderId))
    }, 30000)

    it(
      'PUT Order Summary Existing User Home Standard => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => Basket => Delivery Options (Standard Delivery 4.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              optional: ['label'],
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId: stringType,
              label: stringType,
              enabled: booleanType,
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: booleanType,
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
                'shipCode',
              ],
              optional: ['shipModeId'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'HOME_STANDARD'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Existing User Order Summary => Modifying Delivery Option To Parcelshop', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(parcelshopPayload(orderId))
    }, 30000)

    it(
      'PUT Order Summary Existing User Parcelshop => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Parcelshop => Basket => Delivery Options (Collect from ParcelShop £4.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId: stringType,
              label: stringType,
              enabled:
                delOpts.label === 'Collect From Store Today £3.00'
                  ? booleanType(false)
                  : booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Parcelshop => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'PARCELSHOP'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Parcelshop => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'shipModeId',
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
              ],
              optional: ['shipCode'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringTypePattern('PARCELSHOP_COLLECTION'),
                label: stringTypePattern('Collect from ParcelShop'),
                additionalDescription: stringType,
                selected: booleanType(true),
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode: stringType,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Parcelshop => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Existing user Order Summary => Modifying Delivery Option To Home Express', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(homeExpressPayload(orderId))
    }, 30000)

    it(
      'PUT Order Summary Existing User Home Express => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Express => Basket => Delivery Options (Express / Nominated Day Delivery £6.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'enabled',
            ],
            optional: ['label'],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId: stringType,
              label: stringType,
              enabled: booleanType,
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Express => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Express => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
                'shipCode',
              ],
              optional: ['shipModeId'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'HOME_EXPRESS'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode: stringType,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Express => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Existing User Order Summary => Modifying Delivery Option To Store Express', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(storeExpressPayload(orderId))
    }, 30000)

    it(
      'PUT Order Summary Existing User Store Express => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Express => Basket => Delivery Options (Collect From Store Express £3.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'enabled',
            ],
            optional: ['label'],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId: stringType,
              label: stringType,
              enabled: booleanType,
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Express => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'STORE'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Express => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'shipModeId',
                'deliveryType',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'cost',
              ],
              optional: ['shipCode', 'label'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription: stringType,
                selected: booleanType,
                deliveryOptions: arrayType(),
                enabled: booleanType,
                cost: stringType,
                shipCode: stringType,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Express => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Existing User Order Summary => Modifying Delivery Option To Store Standard', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(storeStandardPayload(orderId))
    }, 30000)

    it(
      'PUT Order Summary Existing User Store Standard => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Standard => Basket => Delivery Options (Free Collect From Store Standard £0.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Free Collect From Store Standard £0.00'
                  ? stringTypePattern('retail_store_standard')
                  : stringType,
              label: stringType,
              enabled:
                delOpts.label === 'Collect From Store Today £3.00'
                  ? booleanType(false)
                  : booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Standard => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'STORE'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Standard => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'shipModeId',
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
              ],
              optional: ['shipCode'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription: stringType,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'STORE_STANDARD'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'STORE_IMMEDIATE'
                      ? [false]
                      : [true],
                },
                cost: stringType,
                shipCode: stringType,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Standard => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Existing User Order Summary => Modifying Delivery Option To Parcelshop', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(parcelshopPayload(orderId))
    }, 30000)

    it(
      'PUT Order Summary Existing User Parcelshop => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Parcelshop => Basket => Delivery Options (Collect from ParcelShop £4.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Collect from ParcelShop £4.00'
                  ? stringTypePattern('retail_store_collection')
                  : stringType,
              label: stringType,
              enabled:
                delOpts.label === 'Collect From Store Today £3.00'
                  ? booleanType(false)
                  : booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Parcelshop => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'PARCELSHOP'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Parcelshop => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'shipModeId',
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
              ],
              optional: ['shipCode'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringTypePattern('PARCELSHOP_COLLECTION'),
                label: stringTypePattern('Collect from ParcelShop'),
                additionalDescription: stringType,
                selected: booleanType(true),
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode: stringType,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Parcelshop => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Existing User Order Summary => Modifying Delivery Option To Store Standard', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(storeStandardPayload(orderId))
    }, 30000)

    it(
      'PUT Order Summary Existing User Store Standard => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Standard => Basket => Delivery Options (Free Collect From Store Standard £0.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Free Collect From Store Standard £0.00'
                  ? stringTypePattern('retail_store_standard')
                  : stringType,
              label: stringType,
              enabled:
                delOpts.label === 'Collect From Store Today £3.00'
                  ? booleanType(false)
                  : booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Standard => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'STORE'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Standard => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'shipModeId',
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
              ],
              optional: ['shipCode'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription: stringType,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'STORE_STANDARD'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'STORE_IMMEDIATE'
                      ? [false]
                      : [true],
                },
                cost: stringType,
                shipCode: stringType,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Standard => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Existing User Order Summary => Modifying Delivery Option To Home Standard', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(homeStandardPayload(orderId))
    }, 30000)

    it(
      'PUT Order Summary Existing User Home Standard => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => Basket => Delivery Options (Standard Delivery 4.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              optional: ['label'],
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId: stringType,
              label: stringType,
              enabled: booleanType,
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
                'shipCode',
              ],
              optional: ['shipModeId'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'HOME_STANDARD'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Existing User Order Summary => Modifying Delivery Option To Store Express', () => {
    it(
      'PUT Order Summary Existing User Store Express => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Express => Basket => Delivery Options (Collect From Store Express £3.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId: stringType,
              label: stringType,
              enabled:
                delOpts.label === 'Collect From Store Today £3.00'
                  ? booleanType(false)
                  : booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Express => deliveryLocations Json Schema',
      () => {
        orderSummaryResp.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: booleanType,
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Express => deliveryLocations => deliveryMethods Json Schema',
      () => {
        orderSummaryResp.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
                'shipCode',
              ],
              properties: {
                deliveryType: stringType,
                label: stringType,
                additionalDescription: stringTypeCanBeEmpty,
                selected: booleanType,
                deliveryOptions: arrayType(),
                enabled: booleanType,
                cost: stringType,
                shipCode: stringTypeCanBeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Store Express => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        orderSummaryResp.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Existing user Order Summary => Modifying Delivery Option To Home Express', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(homeExpressPayload(orderId))
    }, 30000)

    it(
      'PUT Order Summary Existing User Home Express => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Express => Basket => Delivery Options (Express / Nominated Day Delivery £6.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'enabled',
            ],
            optional: ['label'],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId: stringType,
              label: stringType,
              enabled: booleanType,
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Express => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Express => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
                'shipCode',
              ],
              optional: ['shipModeId'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'HOME_EXPRESS'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode: stringType,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Express => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Existing User Order Summary => Modifying Delivery Option To Home Standard', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(homeStandardPayload(orderId))
    }, 30000)

    it(
      'PUT Order Summary Existing User Home Standard => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => Basket => Delivery Options (Standard Delivery 4.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              optional: ['label'],
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId: stringType,
              label: stringType,
              enabled: booleanType,
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
                'shipCode',
              ],
              optional: ['shipModeId'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'HOME_STANDARD'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })
})
