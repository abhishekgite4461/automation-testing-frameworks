require('babel-polyfill')

jest.unmock('superagent')
import superagent from 'superagent'
import chai from 'chai'

chai.use(require('chai-json-schema'))

const assert = chai.assert
import eps from '../routes_tests'
import {
  headers,
  objectType,
  arrayType,
  stringType,
  stringTypeEmpty,
  numberType,
  stringTypePattern,
  booleanType,
  anyBooleanType,
} from '../utilis'
import {
  homeStandardPayload,
  sriLankaOrderSummaryUser,
  standardInternationalPayload,
  trackedInternationalPayload,
} from './order-summary-data'

describe('International order summary Schema', () => {
  let jsessionId
  let orderId
  let orderSummaryResp

  beforeAll(async () => {
    const resHeader = await superagent
      .post(eps.myAccount.login.path)
      .set(headers)
      .send(sriLankaOrderSummaryUser)
    const sessionId = resHeader.headers['set-cookie'].toString().split(',')
    jsessionId = sessionId[0]
    orderSummaryResp = await superagent
      .get(eps.checkout.orderSummary.path)
      .set(headers)
      .set({ Cookie: jsessionId })
    orderId = orderSummaryResp.body.basket.orderId
  }, 60000)

  describe('It should return an Order Summary Json Schema - Home Standard International 8', () => {
    it(
      'Get Order Summary',
      () => {
        const body = orderSummaryResp.body
        const orderSummarySchema = {
          title: 'Existing User Order Summary',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringTypePattern('Sri Lanka'),
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(0, true, 'string'),
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'Get Order Summary => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        orderId = body.orderId
        const orderSummarySchemaBasket = {
          title: 'Existing User Order Summary => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          optional: ['isDDPOrder', 'isDDPProduct'],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(2),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
            isDDPOrder: anyBooleanType,
            isDDPProduct: anyBooleanType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'Get Order Summary => Basket => Delivery Options (Free Standard International 8 £0.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Existing User Order Summary => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Free Standard International 8 £0.00'
                  ? stringTypePattern('s')
                  : stringType,
              label: delOpts.selected
                ? stringTypePattern('Free Standard International 8 £0.00')
                : stringType,
              enabled: booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'Get Order Summary => Basket => Products',
      () => {
        const body = orderSummaryResp.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Existing User Order Summary => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
            ],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'Get Order Summary => Basket => Products => assets',
      () => {
        const body = orderSummaryResp.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Existing User Shopping Bag GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'Get Order Summary => Basket => inventoryPositions Json Schema',
      () => {
        const obj = orderSummaryResp.body.basket.inventoryPositions
        let inv
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            inv = obj[props].invavls
          } else {
            inv = obj[props].inventorys
          }
          const shoppingBagInventoryPositionsProps = {
            partNumber: obj[props].partNumber,
            catentryId: obj[props].catentryId,
            inv,
          }
          let shoppingBagInventoryPositionsSchema
          if (inv !== null) {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Existing User Shopping Bag Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
                inv: {
                  type: 'array',
                  minItems: 1,
                  uniqueItems: true,
                  items: { type: 'object' },
                },
              },
            }
          } else {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Existing User Shopping Bag Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
              },
            }
          }
          assert.jsonSchema(
            shoppingBagInventoryPositionsProps,
            shoppingBagInventoryPositionsSchema
          )
        })
      },
      30000
    )

    it(
      'Get Order Summary => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = orderSummaryResp.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const shoppingBagInvAvlsPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                stlocIdentifier: prop.stlocIdentifier,
                expressdates: prop.expressdates,
              }
              const shoppingBagInvAvlsPositionsSchema = {
                title:
                  'Existing User Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'stlocIdentifier',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  stlocIdentifier: { type: 'string' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInvAvlsPositionsProps,
                shoppingBagInvAvlsPositionsSchema
              )
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const shoppingBagInventorysPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                ffmcenterId: prop.ffmcenterId,
                expressdates: prop.expressdates,
              }
              const shoppingBagInventorysPositionsSchema = {
                title:
                  'Existing User Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'ffmcenterId',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  ffmcenterId: { type: 'number' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInventorysPositionsProps,
                shoppingBagInventorysPositionsSchema
              )
            })
          }
        })
      },
      30000
    )

    it(
      'Get Order Summary => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = orderSummaryResp.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          }
        })
      },
      30000
    )

    it(
      'Get Order Summary => deliveryLocations Json Schema',
      () => {
        orderSummaryResp.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'Get Order Summary => deliveryLocations => deliveryMethods Json Schema',
      () => {
        orderSummaryResp.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'shipModeId',
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
              ],
              properties: {
                shipModeId: numberType,
                deliveryType:
                  deliveryMethod.label === 'Standard International 8'
                    ? stringTypePattern('HOME_STANDARD')
                    : stringTypePattern('HOME_EXPRESS'),
                label:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringTypePattern('Standard International 8')
                    : stringTypePattern('Tracked and Faster 4'),
                additionalDescription: stringType,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'HOME_STANDARD'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: { type: ['number', 'string'] },
                shipCode:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )
  })

  describe('Existing international Order Summary => Modifying Delivery Option To Tracked and Faster 4', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(trackedInternationalPayload(orderId, 'Sri Lanka'))
    }, 30000)

    it(
      'PUT Order Summary Existing User Tracked and Faster 4',
      () => {
        const body = response.body
        const orderSummarySchema = {
          title: 'Order Summary Existing User',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringTypePattern('Sri Lanka'),
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(1, true, 'string'),
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Tracked and Faster 4 => Basket',
      () => {
        const body = response.body.basket
        orderId = body.orderId
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(2),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Tracked and Faster 4 => Basket => Delivery Options (Tracked and Faster 4 £18.00)',
      () => {
        const body = response.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected:
                delOpts.label === 'Tracked and Faster 4 £18.00'
                  ? booleanType(true)
                  : booleanType(false),
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Tracked and Faster 4 £18.00'
                  ? stringTypePattern('e')
                  : stringTypePattern('s'),
              label: delOpts.selected
                ? stringTypePattern('Tracked and Faster 4 £18.00')
                : stringTypePattern('Standard International 8 £10.50'),
              enabled: booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Tracked and Faster 4 => Basket => Products',
      () => {
        const body = response.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
            ],
            optional: ['wasWasPrice', 'wasPrice'],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
              wasWasPrice: stringType,
              wasPrice: stringType,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Tracked and Faster 4 => Basket => Products => assets',
      () => {
        const body = response.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Shopping Bag Existing User GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Tracked and Faster 4 => Basket => inventoryPositions Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        let inv
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            inv = obj[props].invavls
          } else {
            inv = obj[props].inventorys
          }
          const shoppingBagInventoryPositionsProps = {
            partNumber: obj[props].partNumber,
            catentryId: obj[props].catentryId,
            inv,
          }
          let shoppingBagInventoryPositionsSchema
          if (inv !== null) {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Shopping Bag Existing User Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
                inv: {
                  type: 'array',
                  minItems: 1,
                  uniqueItems: true,
                  items: { type: 'object' },
                },
              },
            }
          } else {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Shopping Bag Existing User Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
              },
            }
          }
          assert.jsonSchema(
            shoppingBagInventoryPositionsProps,
            shoppingBagInventoryPositionsSchema
          )
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Tracked and Faster 4 => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const shoppingBagInvAvlsPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                stlocIdentifier: prop.stlocIdentifier,
                expressdates: prop.expressdates,
              }
              const shoppingBagInvAvlsPositionsSchema = {
                title:
                  'Shopping Bag Existing User Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'stlocIdentifier',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  stlocIdentifier: { type: 'string' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInvAvlsPositionsProps,
                shoppingBagInvAvlsPositionsSchema
              )
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const shoppingBagInventorysPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                ffmcenterId: prop.ffmcenterId,
                expressdates: prop.expressdates,
              }
              const shoppingBagInventorysPositionsSchema = {
                title:
                  'Shopping Bag Existing User Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'ffmcenterId',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  ffmcenterId: { type: 'number' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInventorysPositionsProps,
                shoppingBagInventorysPositionsSchema
              )
            })
          }
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Tracked and Faster 4 => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          }
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Tracked and Faster 4 => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Tracked and Faster 4 => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'shipModeId',
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
              ],
              optional: ['shipCode'],
              properties: {
                shipModeId: numberType,
                deliveryType:
                  deliveryMethod.label === 'Standard International 8'
                    ? stringTypePattern('HOME_STANDARD')
                    : stringTypePattern('HOME_EXPRESS'),
                label:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringTypePattern('Standard International 8')
                    : stringTypePattern('Tracked and Faster 4'),
                additionalDescription: stringType,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.label === 'Tracked and Faster 4'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode: stringTypePattern('S'),
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )
  })

  describe('It should return an Order Summary Json Schema - Tracked and Faster 4', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .get(eps.checkout.orderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
    }, 30000)

    it(
      'GET Order Summary Existing User Tracked and Faster 4',
      () => {
        const body = response.body
        const orderSummarySchema = {
          title: 'Order Summary Existing User',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringTypePattern('Sri Lanka'),
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(1, true, 'string'),
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'GET Order Summary Existing User Tracked and Faster 4 => Basket',
      () => {
        const body = response.body.basket
        orderId = body.orderId
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(2),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'GET Order Summary Existing User Tracked and Faster 4 => Basket => Delivery Options (Tracked and Faster 4 £18.00)',
      () => {
        const body = response.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected:
                delOpts.label === 'Tracked and Faster 4 £18.00'
                  ? booleanType(true)
                  : booleanType(false),
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Tracked and Faster 4 £18.00'
                  ? stringTypePattern('e')
                  : stringTypePattern('s'),
              label: delOpts.selected
                ? stringTypePattern('Tracked and Faster 4 £18.00')
                : stringTypePattern('Standard International 8 £10.50'),
              enabled: booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User Tracked and Faster 4 => Basket => Products',
      () => {
        const body = response.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
            ],
            optional: ['wasWasPrice', 'wasPrice'],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
              wasWasPrice: stringType,
              wasPrice: stringType,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User Tracked and Faster 4 => Basket => Products => assets',
      () => {
        const body = response.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Shopping Bag Existing User GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User Tracked and Faster 4 => Basket => inventoryPositions Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        let inv
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            inv = obj[props].invavls
          } else {
            inv = obj[props].inventorys
          }
          const shoppingBagInventoryPositionsProps = {
            partNumber: obj[props].partNumber,
            catentryId: obj[props].catentryId,
            inv,
          }
          let shoppingBagInventoryPositionsSchema
          if (inv !== null) {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Shopping Bag Existing User Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
                inv: {
                  type: 'array',
                  minItems: 1,
                  uniqueItems: true,
                  items: { type: 'object' },
                },
              },
            }
          } else {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Shopping Bag Existing User Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
              },
            }
          }
          assert.jsonSchema(
            shoppingBagInventoryPositionsProps,
            shoppingBagInventoryPositionsSchema
          )
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User Tracked and Faster 4 => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls)
            if (obj[props].invavls) {
              obj[props].invavls.forEach((prop) => {
                const shoppingBagInvAvlsPositionsProps = {
                  cutofftime: prop.cutofftime,
                  quantity: prop.quantity,
                  stlocIdentifier: prop.stlocIdentifier,
                  expressdates: prop.expressdates,
                }
                const shoppingBagInvAvlsPositionsSchema = {
                  title:
                    'Shopping Bag Existing User Simple Product inventoryPositions => inventories && invavls Json Schema',
                  type: 'object',
                  required: [
                    'cutofftime',
                    'quantity',
                    'stlocIdentifier',
                    'expressdates',
                  ],
                  properties: {
                    cutofftime: { type: 'string' },
                    quantity: { type: 'number' },
                    stlocIdentifier: { type: 'string' },
                    expressdates: {
                      type: 'array',
                      minItems: 2,
                      uniqueItems: true,
                      items: { type: 'string' },
                    },
                  },
                }
                assert.jsonSchema(
                  shoppingBagInvAvlsPositionsProps,
                  shoppingBagInvAvlsPositionsSchema
                )
              })
            } else if (obj[props].inventorys) {
              obj[props].inventorys.forEach((prop) => {
                const shoppingBagInventorysPositionsProps = {
                  cutofftime: prop.cutofftime,
                  quantity: prop.quantity,
                  ffmcenterId: prop.ffmcenterId,
                  expressdates: prop.expressdates,
                }
                const shoppingBagInventorysPositionsSchema = {
                  title:
                    'Shopping Bag Existing User Simple Product inventoryPositions => inventories && invavls Json Schema',
                  type: 'object',
                  required: [
                    'cutofftime',
                    'quantity',
                    'ffmcenterId',
                    'expressdates',
                  ],
                  properties: {
                    cutofftime: { type: 'string' },
                    quantity: { type: 'number' },
                    ffmcenterId: { type: 'number' },
                    expressdates: {
                      type: 'array',
                      minItems: 2,
                      uniqueItems: true,
                      items: { type: 'string' },
                    },
                  },
                }
                assert.jsonSchema(
                  shoppingBagInventorysPositionsProps,
                  shoppingBagInventorysPositionsSchema
                )
              })
            }
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User Tracked and Faster 4 => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          }
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User Tracked and Faster 4 => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User Tracked and Faster 4 => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'shipModeId',
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
              ],
              optional: ['shipCode'],
              properties: {
                shipModeId: numberType,
                deliveryType:
                  deliveryMethod.label === 'Standard International 8'
                    ? stringTypePattern('HOME_STANDARD')
                    : stringTypePattern('HOME_EXPRESS'),
                label:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringTypePattern('Standard International 8')
                    : stringTypePattern('Tracked and Faster 4'),
                additionalDescription: stringType,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.label === 'Tracked and Faster 4'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode: stringTypePattern('S'),
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )
  })

  describe('Existing international Order Summary => Modifying to International 8 to Canada', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(standardInternationalPayload(orderId, 'Canada'))
    }, 30000)

    it(
      'GET Order Summary Existing User International 8',
      () => {
        const body = response.body
        const orderSummarySchema = {
          title: 'Order Summary Existing User',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringTypePattern('Canada'),
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(1, true, 'string'),
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => Basket',
      () => {
        const body = response.body.basket
        orderId = body.orderId
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(2),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => Basket => Delivery Options (International 8)',
      () => {
        const body = response.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected:
                delOpts.label === 'Free Standard International 8 £0.00'
                  ? booleanType(true)
                  : booleanType(false),
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Free Standard International 8 £0.00'
                  ? stringTypePattern('s')
                  : stringTypePattern('e'),
              label: delOpts.selected
                ? stringTypePattern('Free Standard International 8 £0.00')
                : stringTypePattern('Tracked and Faster 3 £20.00'),
              enabled: booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => Basket => Products',
      () => {
        const body = response.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
            ],
            optional: ['wasWasPrice', 'wasPrice'],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
              wasWasPrice: stringType,
              wasPrice: stringType,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => Basket => Products => assets',
      () => {
        const body = response.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Shopping Bag Existing User GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => Basket => inventoryPositions Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        let inv
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            inv = obj[props].invavls
          } else {
            inv = obj[props].inventorys
          }
          const shoppingBagInventoryPositionsProps = {
            partNumber: obj[props].partNumber,
            catentryId: obj[props].catentryId,
            inv,
          }
          let shoppingBagInventoryPositionsSchema
          if (inv !== null) {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Shopping Bag Existing User Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
                inv: {
                  type: 'array',
                  minItems: 1,
                  uniqueItems: true,
                  items: { type: 'object' },
                },
              },
            }
          } else {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Shopping Bag Existing User Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
              },
            }
          }
          assert.jsonSchema(
            shoppingBagInventoryPositionsProps,
            shoppingBagInventoryPositionsSchema
          )
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const shoppingBagInvAvlsPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                stlocIdentifier: prop.stlocIdentifier,
                expressdates: prop.expressdates,
              }
              const shoppingBagInvAvlsPositionsSchema = {
                title:
                  'Shopping Bag Existing User Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'stlocIdentifier',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  stlocIdentifier: { type: 'string' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInvAvlsPositionsProps,
                shoppingBagInvAvlsPositionsSchema
              )
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const shoppingBagInventorysPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                ffmcenterId: prop.ffmcenterId,
                expressdates: prop.expressdates,
              }
              const shoppingBagInventorysPositionsSchema = {
                title:
                  'Shopping Bag Existing User Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'ffmcenterId',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  ffmcenterId: { type: 'number' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInventorysPositionsProps,
                shoppingBagInventorysPositionsSchema
              )
            })
          }
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          }
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'shipModeId',
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
              ],
              optional: ['shipCode'],
              properties: {
                shipModeId: numberType,
                deliveryType:
                  deliveryMethod.label === 'Standard International 8'
                    ? stringTypePattern('HOME_STANDARD')
                    : stringTypePattern('HOME_EXPRESS'),
                label:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringTypePattern('Standard International 8')
                    : stringTypePattern('Tracked and Faster 3'),
                additionalDescription: stringType,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.label === 'Standard International 8'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode: stringTypePattern('S'),
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )
  })

  describe('It should return an Order Summary Json Schema - International 8 Canada', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .get(eps.checkout.orderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
    }, 30000)

    it(
      'GET Order Summary Existing User International 8',
      () => {
        const body = response.body
        const orderSummarySchema = {
          title: 'Order Summary Existing User',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringTypePattern('Canada'),
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(1, true, 'string'),
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => Basket',
      () => {
        const body = response.body.basket
        orderId = body.orderId
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(2),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => Basket => Delivery Options (International 8)',
      () => {
        const body = response.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected:
                delOpts.label === 'Free Standard International 8 £0.00'
                  ? booleanType(true)
                  : booleanType(false),
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Free Standard International 8 £0.00'
                  ? stringTypePattern('s')
                  : stringTypePattern('e'),
              label: delOpts.selected
                ? stringTypePattern('Free Standard International 8 £0.00')
                : stringTypePattern('Tracked and Faster 3 £20.00'),
              enabled: booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => Basket => Products',
      () => {
        const body = response.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
            ],
            optional: ['wasWasPrice', 'wasPrice'],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
              wasWasPrice: stringType,
              wasPrice: stringType,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => Basket => Products => assets',
      () => {
        const body = response.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Shopping Bag Existing User GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => Basket => inventoryPositions Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        let inv
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            inv = obj[props].invavls
          } else {
            inv = obj[props].inventorys
          }
          const shoppingBagInventoryPositionsProps = {
            partNumber: obj[props].partNumber,
            catentryId: obj[props].catentryId,
            inv,
          }
          let shoppingBagInventoryPositionsSchema
          if (inv !== null) {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Shopping Bag Existing User Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
                inv: {
                  type: 'array',
                  minItems: 1,
                  uniqueItems: true,
                  items: { type: 'object' },
                },
              },
            }
          } else {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Shopping Bag Existing User Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
              },
            }
          }
          assert.jsonSchema(
            shoppingBagInventoryPositionsProps,
            shoppingBagInventoryPositionsSchema
          )
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const shoppingBagInvAvlsPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                stlocIdentifier: prop.stlocIdentifier,
                expressdates: prop.expressdates,
              }
              const shoppingBagInvAvlsPositionsSchema = {
                title:
                  'Shopping Bag Existing User Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'stlocIdentifier',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  stlocIdentifier: { type: 'string' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInvAvlsPositionsProps,
                shoppingBagInvAvlsPositionsSchema
              )
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const shoppingBagInventorysPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                ffmcenterId: prop.ffmcenterId,
                expressdates: prop.expressdates,
              }
              const shoppingBagInventorysPositionsSchema = {
                title:
                  'Shopping Bag Existing User Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'ffmcenterId',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  ffmcenterId: { type: 'number' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInventorysPositionsProps,
                shoppingBagInventorysPositionsSchema
              )
            })
          }
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          }
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User International 8 => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'shipModeId',
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
              ],
              optional: ['shipCode'],
              properties: {
                shipModeId: numberType,
                deliveryType:
                  deliveryMethod.label === 'Standard International 8'
                    ? stringTypePattern('HOME_STANDARD')
                    : stringTypePattern('HOME_EXPRESS'),
                label:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringTypePattern('Standard International 8')
                    : stringTypePattern('Tracked and Faster 3'),
                additionalDescription: stringType,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.label === 'Standard International 8'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode: stringTypePattern('S'),
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )
  })

  describe('Existing international Order Summary => Modifying Delivery Option To Tracked and Faster 4 to Sri Lanka', () => {
    // In this scenario, we expect the standard delivery to be selected, even if we send the express one in the payload.
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(trackedInternationalPayload(orderId, 'Sri Lanka'))
    }, 30000)

    it(
      'PUT Order Summary Existing User Tracked and Faster 4',
      () => {
        const body = response.body
        const orderSummarySchema = {
          title: 'Order Summary Existing User',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringTypePattern('Sri Lanka'),
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(1, true, 'string'),
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Tracked and Faster 4 => Basket',
      () => {
        const body = response.body.basket
        orderId = body.orderId
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(2),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary => Basket => Delivery Options (Free Standard International 8 £0.00)',
      () => {
        const body = response.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Existing User Order Summary => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected:
                delOpts.label === 'Free Standard International 8 £0.00'
                  ? booleanType(true)
                  : booleanType(false),
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Free Standard International 8 £0.00'
                  ? stringTypePattern('s')
                  : stringType,
              label: delOpts.selected
                ? stringTypePattern('Free Standard International 8 £0.00')
                : stringType,
              enabled: booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Tracked and Faster 4 => Basket => Products',
      () => {
        const body = response.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
            ],
            optional: ['wasWasPrice', 'wasPrice'],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
              wasWasPrice: stringType,
              wasPrice: stringType,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Tracked and Faster 4 => Basket => Products => assets',
      () => {
        const body = response.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Shopping Bag Existing User GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Tracked and Faster 4 => Basket => inventoryPositions Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        let inv
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            inv = obj[props].invavls
          } else {
            inv = obj[props].inventorys
          }
          const shoppingBagInventoryPositionsProps = {
            partNumber: obj[props].partNumber,
            catentryId: obj[props].catentryId,
            inv,
          }
          let shoppingBagInventoryPositionsSchema
          if (inv !== null) {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Shopping Bag Existing User Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
                inv: {
                  type: 'array',
                  minItems: 1,
                  uniqueItems: true,
                  items: { type: 'object' },
                },
              },
            }
          } else {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Shopping Bag Existing User Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
              },
            }
          }
          assert.jsonSchema(
            shoppingBagInventoryPositionsProps,
            shoppingBagInventoryPositionsSchema
          )
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Tracked and Faster 4 => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const shoppingBagInvAvlsPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                stlocIdentifier: prop.stlocIdentifier,
                expressdates: prop.expressdates,
              }
              const shoppingBagInvAvlsPositionsSchema = {
                title:
                  'Shopping Bag Existing User Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'stlocIdentifier',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  stlocIdentifier: { type: 'string' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInvAvlsPositionsProps,
                shoppingBagInvAvlsPositionsSchema
              )
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const shoppingBagInventorysPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                ffmcenterId: prop.ffmcenterId,
                expressdates: prop.expressdates,
              }
              const shoppingBagInventorysPositionsSchema = {
                title:
                  'Shopping Bag Existing User Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'ffmcenterId',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  ffmcenterId: { type: 'number' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInventorysPositionsProps,
                shoppingBagInventorysPositionsSchema
              )
            })
          }
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Tracked and Faster 4 => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          }
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Tracked and Faster 4 => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'shipModeId',
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
              ],
              properties: {
                shipModeId: numberType,
                deliveryType:
                  deliveryMethod.label === 'Standard International 8'
                    ? stringTypePattern('HOME_STANDARD')
                    : stringTypePattern('HOME_EXPRESS'),
                label:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringTypePattern('Standard International 8')
                    : stringTypePattern('Tracked and Faster 4'),
                additionalDescription: stringType,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'HOME_STANDARD'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )
  })

  describe('It should return an Order Summary Json Schema - Tracked and Faster 4 to Sri Lanka', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .get(eps.checkout.orderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
    }, 30000)

    it(
      'GET Order Summary Existing User Tracked and Faster 4',
      () => {
        const body = response.body
        const orderSummarySchema = {
          title: 'Order Summary Existing User',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringTypePattern('Sri Lanka'),
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(1, true, 'string'),
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'GET Order Summary Existing User Tracked and Faster 4 => Basket',
      () => {
        const body = response.body.basket
        orderId = body.orderId
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(2),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'GET Order Summary => Basket => Delivery Options (Free Standard International 8 £0.00)',
      () => {
        const body = response.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Existing User Order Summary => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected:
                delOpts.label === 'Free Standard International 8 £0.00'
                  ? booleanType(true)
                  : booleanType(false),
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Free Standard International 8 £0.00'
                  ? stringTypePattern('s')
                  : stringType,
              label: delOpts.selected
                ? stringTypePattern('Free Standard International 8 £0.00')
                : stringType,
              enabled: booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User Tracked and Faster 4 => Basket => Products',
      () => {
        const body = response.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
            ],
            optional: ['wasWasPrice', 'wasPrice'],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
              wasWasPrice: stringType,
              wasPrice: stringType,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User Tracked and Faster 4 => Basket => Products => assets',
      () => {
        const body = response.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Shopping Bag Existing User GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User Tracked and Faster 4 => Basket => inventoryPositions Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        let inv
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            inv = obj[props].invavls
          } else {
            inv = obj[props].inventorys
          }
          const shoppingBagInventoryPositionsProps = {
            partNumber: obj[props].partNumber,
            catentryId: obj[props].catentryId,
            inv,
          }
          let shoppingBagInventoryPositionsSchema
          if (inv !== null) {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Shopping Bag Existing User Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
                inv: {
                  type: 'array',
                  minItems: 1,
                  uniqueItems: true,
                  items: { type: 'object' },
                },
              },
            }
          } else {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Shopping Bag Existing User Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
              },
            }
          }
          assert.jsonSchema(
            shoppingBagInventoryPositionsProps,
            shoppingBagInventoryPositionsSchema
          )
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User Tracked and Faster 4 => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const shoppingBagInvAvlsPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                stlocIdentifier: prop.stlocIdentifier,
                expressdates: prop.expressdates,
              }
              const shoppingBagInvAvlsPositionsSchema = {
                title:
                  'Shopping Bag Existing User Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'stlocIdentifier',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  stlocIdentifier: { type: 'string' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInvAvlsPositionsProps,
                shoppingBagInvAvlsPositionsSchema
              )
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const shoppingBagInventorysPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                ffmcenterId: prop.ffmcenterId,
                expressdates: prop.expressdates,
              }
              const shoppingBagInventorysPositionsSchema = {
                title:
                  'Shopping Bag Existing User Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'ffmcenterId',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  ffmcenterId: { type: 'number' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInventorysPositionsProps,
                shoppingBagInventorysPositionsSchema
              )
            })
          }
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User Tracked and Faster 4 => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          }
        })
      },
      30000
    )

    it(
      'GET Order Summary Existing User Tracked and Faster 4 => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'GET Order Summary => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'shipModeId',
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
              ],
              properties: {
                shipModeId: numberType,
                deliveryType:
                  deliveryMethod.label === 'Standard International 8'
                    ? stringTypePattern('HOME_STANDARD')
                    : stringTypePattern('HOME_EXPRESS'),
                label:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringTypePattern('Standard International 8')
                    : stringTypePattern('Tracked and Faster 4'),
                additionalDescription: stringType,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'HOME_STANDARD'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )
  })

  describe('Existing International Order Summary => Modifying Delivery country to UK Home Standard', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(homeStandardPayload(orderId))
    }, 30000)

    it(
      'PUT Order Summary Existing User Home Standard',
      () => {
        const body = response.body
        const orderSummarySchema = {
          title: 'Order Summary Existing User',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringType,
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(1, true, 'string'),
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => Basket',
      () => {
        const body = response.body.basket
        orderId = body.orderId
        const orderSummarySchemaBasket = {
          title: 'Order Summary Existing User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => Basket => Delivery Options (Standard Delivery 4.00)',
      () => {
        const body = response.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Standard Delivery £4.00'
                  ? stringTypePattern('s')
                  : stringType,
              label: stringType,
              enabled:
                delOpts.label === 'Collect From Store Today £3.00'
                  ? booleanType(false)
                  : booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => Basket => Products',
      () => {
        const body = response.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Existing User => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
            ],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => Basket => Products => assets',
      () => {
        const body = response.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Shopping Bag Existing User GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => Basket => inventoryPositions Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        let inv
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            inv = obj[props].invavls
          } else {
            inv = obj[props].inventorys
          }
          const shoppingBagInventoryPositionsProps = {
            partNumber: obj[props].partNumber,
            catentryId: obj[props].catentryId,
            inv,
          }
          let shoppingBagInventoryPositionsSchema
          if (inv !== null) {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Shopping Bag Existing User Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
                inv: {
                  type: 'array',
                  minItems: 1,
                  uniqueItems: true,
                  items: { type: 'object' },
                },
              },
            }
          } else {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Shopping Bag Existing User Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
              },
            }
          }
          assert.jsonSchema(
            shoppingBagInventoryPositionsProps,
            shoppingBagInventoryPositionsSchema
          )
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const shoppingBagInvAvlsPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                stlocIdentifier: prop.stlocIdentifier,
                expressdates: prop.expressdates,
              }
              const shoppingBagInvAvlsPositionsSchema = {
                title:
                  'Shopping Bag Existing User Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'stlocIdentifier',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  stlocIdentifier: { type: 'string' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInvAvlsPositionsProps,
                shoppingBagInvAvlsPositionsSchema
              )
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const shoppingBagInventorysPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                ffmcenterId: prop.ffmcenterId,
                expressdates: prop.expressdates,
              }
              const shoppingBagInventorysPositionsSchema = {
                title:
                  'Shopping Bag Existing User Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'ffmcenterId',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  ffmcenterId: { type: 'number' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInventorysPositionsProps,
                shoppingBagInventorysPositionsSchema
              )
            })
          }
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          }
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
                'shipCode',
              ],
              optional: ['shipModeId'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'HOME_STANDARD'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: { type: ['number', 'string'] },
                shipCode:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Existing User Home Standard => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('It should return an Order Summary Json Schema - UK Home Standard', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .get(eps.checkout.orderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
    }, 30000)

    it(
      'Get Order Summary',
      () => {
        const body = response.body
        const orderSummarySchema = {
          title: 'Existing User Order Summary',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringType,
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(0, true, 'string'),
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'Get Order Summary => Basket',
      () => {
        const body = response.body.basket
        orderId = body.orderId
        const orderSummarySchemaBasket = {
          title: 'Existing User Order Summary => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'Get Order Summary => Basket => Delivery Options (Standard Delivery 4.00)',
      () => {
        const body = response.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Existing User Order Summary => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Standard Delivery £4.00'
                  ? stringTypePattern('s')
                  : stringType,
              label: stringType,
              enabled:
                delOpts.label === 'Collect From Store Today £3.00'
                  ? booleanType(false)
                  : booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'Get Order Summary => Basket => Products',
      () => {
        const body = response.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Existing User Order Summary => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
            ],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'Get Order Summary => Basket => Products => assets',
      () => {
        const body = response.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Existing User Shopping Bag GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'Get Order Summary => Basket => inventoryPositions Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        let inv
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            inv = obj[props].invavls
          } else {
            inv = obj[props].inventorys
          }
          const shoppingBagInventoryPositionsProps = {
            partNumber: obj[props].partNumber,
            catentryId: obj[props].catentryId,
            inv,
          }
          let shoppingBagInventoryPositionsSchema
          if (inv !== null) {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Existing User Shopping Bag Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
                inv: {
                  type: 'array',
                  minItems: 1,
                  uniqueItems: true,
                  items: { type: 'object' },
                },
              },
            }
          } else {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Existing User Shopping Bag Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
              },
            }
          }
          assert.jsonSchema(
            shoppingBagInventoryPositionsProps,
            shoppingBagInventoryPositionsSchema
          )
        })
      },
      30000
    )

    it(
      'Get Order Summary => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const shoppingBagInvAvlsPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                stlocIdentifier: prop.stlocIdentifier,
                expressdates: prop.expressdates,
              }
              const shoppingBagInvAvlsPositionsSchema = {
                title:
                  'Existing User Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'stlocIdentifier',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  stlocIdentifier: { type: 'string' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInvAvlsPositionsProps,
                shoppingBagInvAvlsPositionsSchema
              )
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const shoppingBagInventorysPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                ffmcenterId: prop.ffmcenterId,
                expressdates: prop.expressdates,
              }
              const shoppingBagInventorysPositionsSchema = {
                title:
                  'Existing User Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'ffmcenterId',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  ffmcenterId: { type: 'number' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInventorysPositionsProps,
                shoppingBagInventorysPositionsSchema
              )
            })
          }
        })
      },
      30000
    )

    it(
      'Get Order Summary => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          }
        })
      },
      30000
    )

    it(
      'Get Order Summary => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'Get Order Summary => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
                'shipCode',
              ],
              optional: ['shipModeId'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'HOME_STANDARD'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: { type: ['number', 'string'] },
                shipCode:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'Get Order Summary => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Existing User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Existing international Order Summary => Modifying from UK to Sri Lanka International 8', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(standardInternationalPayload(orderId, 'Sri Lanka'))
    }, 30000)

    it(
      'PUT Order Summary',
      () => {
        const body = response.body
        const orderSummarySchema = {
          title: 'Existing User Order Summary',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringTypePattern('Sri Lanka'),
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(0, true, 'string'),
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'PUT Order Summary => Basket',
      () => {
        const body = response.body.basket
        orderId = body.orderId
        const orderSummarySchemaBasket = {
          title: 'Existing User Order Summary => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(2),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary => Basket => Delivery Options (Free Standard International 8 £0.00)',
      () => {
        const body = response.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Existing User Order Summary => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected:
                delOpts.label === 'Free Standard International 8 £0.00'
                  ? booleanType(true)
                  : booleanType(false),
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Free Standard International 8 £0.00'
                  ? stringTypePattern('s')
                  : stringType,
              label: delOpts.selected
                ? stringTypePattern('Free Standard International 8 £0.00')
                : stringType,
              enabled: booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary => Basket => Products',
      () => {
        const body = response.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Existing User Order Summary => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
            ],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary => Basket => Products => assets',
      () => {
        const body = response.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Existing User Shopping Bag GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary => Basket => inventoryPositions Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        let inv
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            inv = obj[props].invavls
          } else {
            inv = obj[props].inventorys
          }
          const shoppingBagInventoryPositionsProps = {
            partNumber: obj[props].partNumber,
            catentryId: obj[props].catentryId,
            inv,
          }
          let shoppingBagInventoryPositionsSchema
          if (inv !== null) {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Existing User Shopping Bag Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
                inv: {
                  type: 'array',
                  minItems: 1,
                  uniqueItems: true,
                  items: { type: 'object' },
                },
              },
            }
          } else {
            shoppingBagInventoryPositionsSchema = {
              title:
                'Existing User Shopping Bag Simple Product inventoryPositions Json Schema',
              type: 'object',
              required: ['partNumber', 'catentryId', 'inv'],
              properties: {
                partNumber: { type: 'string' },
                catEntryId: { type: 'number' },
              },
            }
          }
          assert.jsonSchema(
            shoppingBagInventoryPositionsProps,
            shoppingBagInventoryPositionsSchema
          )
        })
      },
      30000
    )

    it(
      'PUT Order Summary => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const shoppingBagInvAvlsPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                stlocIdentifier: prop.stlocIdentifier,
                expressdates: prop.expressdates,
              }
              const shoppingBagInvAvlsPositionsSchema = {
                title:
                  'Existing User Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'stlocIdentifier',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  stlocIdentifier: { type: 'string' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInvAvlsPositionsProps,
                shoppingBagInvAvlsPositionsSchema
              )
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const shoppingBagInventorysPositionsProps = {
                cutofftime: prop.cutofftime,
                quantity: prop.quantity,
                ffmcenterId: prop.ffmcenterId,
                expressdates: prop.expressdates,
              }
              const shoppingBagInventorysPositionsSchema = {
                title:
                  'Existing User Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
                type: 'object',
                required: [
                  'cutofftime',
                  'quantity',
                  'ffmcenterId',
                  'expressdates',
                ],
                properties: {
                  cutofftime: { type: 'string' },
                  quantity: { type: 'number' },
                  ffmcenterId: { type: 'number' },
                  expressdates: {
                    type: 'array',
                    minItems: 2,
                    uniqueItems: true,
                    items: { type: 'string' },
                  },
                },
              }
              assert.jsonSchema(
                shoppingBagInventorysPositionsProps,
                shoppingBagInventorysPositionsSchema
              )
            })
          }
        })
      },
      30000
    )

    it(
      'PUT Order Summary => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = response.body.basket.inventoryPositions
        Object.keys(obj).forEach((props) => {
          if (obj[props].invavls) {
            obj[props].invavls.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          } else if (obj[props].inventorys) {
            obj[props].inventorys.forEach((prop) => {
              const expressdateSchema = {
                title: 'Existing User Express date in inventory item',
                type: 'array',
                required: ['0', '1'],
                properties: {
                  0: stringType,
                  1: stringType,
                },
              }
              assert.jsonSchema(prop.expressdates, expressdateSchema)
            })
          }
        })
      },
      30000
    )

    it(
      'PUT Order Summary => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Existing User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Existing User Delivery Method Schema',
              type: 'object',
              required: [
                'shipModeId',
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
              ],
              properties: {
                shipModeId: numberType,
                deliveryType:
                  deliveryMethod.label === 'Standard International 8'
                    ? stringTypePattern('HOME_STANDARD')
                    : stringTypePattern('HOME_EXPRESS'),
                label:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringTypePattern('Standard International 8')
                    : stringTypePattern('Tracked and Faster 4'),
                additionalDescription: stringType,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'HOME_STANDARD'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )
  })
})
