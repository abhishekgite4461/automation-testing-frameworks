/* eslint-disable no-restricted-syntax,no-prototype-builtins */
import { createAccount } from '../utilis/userAccount'

require('babel-polyfill')

jest.unmock('superagent')
import superagent from 'superagent'
import chai from 'chai'

chai.use(require('chai-json-schema'))

const assert = chai.assert
import eps from '../routes_tests'
import {
  arrayType,
  booleanType,
  headers,
  numberType,
  objectType,
  stringType,
  stringTypeCanBeEmpty,
  stringTypeEmpty,
  stringTypePattern,
  anyBooleanType,
} from '../utilis'
import {
  storeStandardPayload,
  homeExpressPayload,
  storeExpressPayload,
  parcelshopPayload,
  homeStandardPayload,
} from './order-summary-data'
import { addItemToShoppingBag } from '../utilis/shoppingBag'
import { getProducts } from '../utilis/selectProducts'

describe('Order Summary -> Guest User Scenarios', () => {
  let products
  let newAccount
  let jsessionId
  let orderId
  let orderSummaryResp

  beforeAll(async () => {
    products = await getProducts()
    newAccount = await createAccount()
    jsessionId = newAccount.jsessionid
    await addItemToShoppingBag(jsessionId, products.productsSimpleId)
    orderSummaryResp = await superagent
      .get(eps.checkout.orderSummary.path)
      .set(headers)
      .set({ Cookie: jsessionId })
      .send(storeExpressPayload(orderId))
    orderId = orderSummaryResp.body.basket.orderId
  }, 60000)

  describe('It should return a Guest User Order Summary Json', () => {
    it(
      'Get Order Summary Guest User',
      () => {
        const body = orderSummaryResp.body
        const orderSummarySchema = {
          title: 'Order Summary Guest User',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringType,
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(1, true, 'string'),
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'Get Order Summary Guest User => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Guest User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          optional: ['isDDPOrder', 'isDDPProduct'],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
            isDDPOrder: anyBooleanType,
            isDDPProduct: anyBooleanType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'Get Order Summary Guest User => Basket => Delivery Options (Standard Delivery 4.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Guest User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId: stringType,
              label: stringType,
              enabled:
                delOpts.label === 'Collect From Store Today £3.00'
                  ? booleanType(false)
                  : booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'Get Order Summary Guest User => Basket => Products',
      () => {
        const body = orderSummaryResp.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Guest User => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
            ],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'Get Order Summary Guest User => Basket => Products => assets',
      () => {
        const body = orderSummaryResp.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Shopping Bag Guest User GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'Get Order Summary Guest User => Basket => inventoryPositions Json Schema',
      () => {
        const obj = orderSummaryResp.body.basket.inventoryPositions
        let inv
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              inv = obj[props].invavls
            } else {
              inv = obj[props].inventorys
            }
            const shoppingBagInventoryPositionsProps = {
              partNumber: obj[props].partNumber,
              catentryId: obj[props].catentryId,
              inv,
            }
            let shoppingBagInventoryPositionsSchema
            if (inv !== null) {
              shoppingBagInventoryPositionsSchema = {
                title:
                  'Shopping Bag Guest User Simple Product inventoryPositions Json Schema',
                type: 'object',
                required: ['partNumber', 'catentryId', 'inv'],
                properties: {
                  partNumber: { type: 'string' },
                  catEntryId: { type: 'number' },
                  inv: {
                    type: 'array',
                    minItems: 1,
                    uniqueItems: true,
                    items: { type: 'object' },
                  },
                },
              }
            } else {
              shoppingBagInventoryPositionsSchema = {
                title:
                  'Shopping Bag Guest User Simple Product inventoryPositions Json Schema',
                type: 'object',
                required: ['partNumber', 'catentryId', 'inv'],
                properties: {
                  partNumber: { type: 'string' },
                  catEntryId: { type: 'number' },
                },
              }
            }
            assert.jsonSchema(
              shoppingBagInventoryPositionsProps,
              shoppingBagInventoryPositionsSchema
            )
          }
        }
      },
      30000
    )

    it(
      'Get Order Summary Guest User => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = orderSummaryResp.body.basket.inventoryPositions
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              obj[props].invavls.forEach((prop) => {
                const shoppingBagInvAvlsPositionsProps = {
                  cutofftime: prop.cutofftime,
                  quantity: prop.quantity,
                  stlocIdentifier: prop.stlocIdentifier,
                  expressdates: prop.expressdates,
                }
                const shoppingBagInvAvlsPositionsSchema = {
                  title:
                    'Shopping Bag Guest User Simple Product inventoryPositions => inventories && invavls Json Schema',
                  type: 'object',
                  required: [
                    'cutofftime',
                    'quantity',
                    'stlocIdentifier',
                    'expressdates',
                  ],
                  properties: {
                    cutofftime: { type: 'string' },
                    quantity: { type: 'number' },
                    stlocIdentifier: { type: 'string' },
                    expressdates: {
                      type: 'array',
                      minItems: 2,
                      uniqueItems: true,
                      items: { type: 'string' },
                    },
                  },
                }
                assert.jsonSchema(
                  shoppingBagInvAvlsPositionsProps,
                  shoppingBagInvAvlsPositionsSchema
                )
              })
            } else if (obj[props].inventorys) {
              obj[props].inventorys.forEach((prop) => {
                const shoppingBagInventorysPositionsProps = {
                  cutofftime: prop.cutofftime,
                  quantity: prop.quantity,
                  ffmcenterId: prop.ffmcenterId,
                  expressdates: prop.expressdates,
                }
                const shoppingBagInventorysPositionsSchema = {
                  title:
                    'Shopping Bag Guest User Simple Product inventoryPositions => inventories && invavls Json Schema',
                  type: 'object',
                  required: [
                    'cutofftime',
                    'quantity',
                    'ffmcenterId',
                    'expressdates',
                  ],
                  properties: {
                    cutofftime: { type: 'string' },
                    quantity: { type: 'number' },
                    ffmcenterId: { type: 'number' },
                    expressdates: {
                      type: 'array',
                      minItems: 2,
                      uniqueItems: true,
                      items: { type: 'string' },
                    },
                  },
                }
                assert.jsonSchema(
                  shoppingBagInventorysPositionsProps,
                  shoppingBagInventorysPositionsSchema
                )
              })
            }
          }
        }
      },
      30000
    )

    it(
      'Get Order Summary Guest User => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = orderSummaryResp.body.basket.inventoryPositions
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              obj[props].invavls.forEach((prop) => {
                const expressdateSchema = {
                  title: 'Guest User Express date in inventory item',
                  type: 'array',
                  required: ['0', '1'],
                  properties: {
                    0: stringType,
                    1: stringType,
                  },
                }
                assert.jsonSchema(prop.expressdates, expressdateSchema)
              })
            } else if (obj[props].inventorys) {
              obj[props].inventorys.forEach((prop) => {
                const expressdateSchema = {
                  title: 'Guest User Express date in inventory item',
                  type: 'array',
                  required: ['0', '1'],
                  properties: {
                    0: stringType,
                    1: stringType,
                  },
                }
                assert.jsonSchema(prop.expressdates, expressdateSchema)
              })
            }
          }
        }
      },
      30000
    )

    it(
      'Get Order Summary Guest User => deliveryLocations Json Schema',
      () => {
        orderSummaryResp.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Guest User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'Get Order Summary Guest User => deliveryLocations => deliveryMethods Json Schema',
      () => {
        orderSummaryResp.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Guest User Delivery Method Schema',
              type: 'object',
              required: [
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
                'shipCode',
              ],
              optional: ['shipModeId'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'HOME_STANDARD'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'Get Order Summary Guest User => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        orderSummaryResp.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Guest User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Guest User Order Summary => Modifying Delivery Option To Home Express', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: newAccount.jsessionid })
        .send(homeExpressPayload(orderId))
    }, 60000)
    it(
      'PUT Order Summary Guest User Home Express',
      () => {
        const body = response.body
        const orderSummarySchema = {
          title: 'Order Summary Guest User',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringType,
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(1, true, 'string'),
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Express => Basket',
      () => {
        const body = response.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Guest User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Express => Basket => Delivery Options (Express / Nominated Day Delivery £6.00)',
      () => {
        const body = response.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Guest User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Express / Nominated Day Delivery £6.00'
                  ? stringTypePattern('n4')
                  : stringType,
              label: stringType,
              enabled:
                delOpts.label === 'Collect From Store Today £3.00'
                  ? booleanType(false)
                  : booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Express => Basket => Products',
      () => {
        const body = response.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Guest User => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
            ],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Express => Basket => Products => assets',
      () => {
        const body = response.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Shopping Bag Guest User GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Express => Basket => inventoryPositions Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        let inv
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              inv = obj[props].invavls
            } else {
              inv = obj[props].inventorys
            }
            const shoppingBagInventoryPositionsProps = {
              partNumber: obj[props].partNumber,
              catentryId: obj[props].catentryId,
              inv,
            }
            let shoppingBagInventoryPositionsSchema
            if (inv !== null) {
              shoppingBagInventoryPositionsSchema = {
                title:
                  'Shopping Bag Guest User Simple Product inventoryPositions Json Schema',
                type: 'object',
                required: ['partNumber', 'catentryId', 'inv'],
                properties: {
                  partNumber: { type: 'string' },
                  catEntryId: { type: 'number' },
                  inv: {
                    type: 'array',
                    minItems: 1,
                    uniqueItems: true,
                    items: { type: 'object' },
                  },
                },
              }
            } else {
              shoppingBagInventoryPositionsSchema = {
                title:
                  'Shopping Bag Guest User Simple Product inventoryPositions Json Schema',
                type: 'object',
                required: ['partNumber', 'catentryId', 'inv'],
                properties: {
                  partNumber: { type: 'string' },
                  catEntryId: { type: 'number' },
                },
              }
            }
            assert.jsonSchema(
              shoppingBagInventoryPositionsProps,
              shoppingBagInventoryPositionsSchema
            )
          }
        }
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Express => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              obj[props].invavls.forEach((prop) => {
                const shoppingBagInvAvlsPositionsProps = {
                  cutofftime: prop.cutofftime,
                  quantity: prop.quantity,
                  stlocIdentifier: prop.stlocIdentifier,
                  expressdates: prop.expressdates,
                }
                const shoppingBagInvAvlsPositionsSchema = {
                  title:
                    'Shopping Bag Guest User Simple Product inventoryPositions => inventories && invavls Json Schema',
                  type: 'object',
                  required: [
                    'cutofftime',
                    'quantity',
                    'stlocIdentifier',
                    'expressdates',
                  ],
                  properties: {
                    cutofftime: { type: 'string' },
                    quantity: { type: 'number' },
                    stlocIdentifier: { type: 'string' },
                    expressdates: {
                      type: 'array',
                      minItems: 2,
                      uniqueItems: true,
                      items: { type: 'string' },
                    },
                  },
                }
                assert.jsonSchema(
                  shoppingBagInvAvlsPositionsProps,
                  shoppingBagInvAvlsPositionsSchema
                )
              })
            } else if (obj[props].inventorys) {
              obj[props].inventorys.forEach((prop) => {
                const shoppingBagInventorysPositionsProps = {
                  cutofftime: prop.cutofftime,
                  quantity: prop.quantity,
                  ffmcenterId: prop.ffmcenterId,
                  expressdates: prop.expressdates,
                }
                const shoppingBagInventorysPositionsSchema = {
                  title:
                    'Shopping Bag Guest User Simple Product inventoryPositions => inventories && invavls Json Schema',
                  type: 'object',
                  required: [
                    'cutofftime',
                    'quantity',
                    'ffmcenterId',
                    'expressdates',
                  ],
                  properties: {
                    cutofftime: { type: 'string' },
                    quantity: { type: 'number' },
                    ffmcenterId: { type: 'number' },
                    expressdates: {
                      type: 'array',
                      minItems: 2,
                      uniqueItems: true,
                      items: { type: 'string' },
                    },
                  },
                }
                assert.jsonSchema(
                  shoppingBagInventorysPositionsProps,
                  shoppingBagInventorysPositionsSchema
                )
              })
            }
          }
        }
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Express => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = response.body.basket.inventoryPositions
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              obj[props].invavls.forEach((prop) => {
                const expressdateSchema = {
                  title: 'Guest User Express date in inventory item',
                  type: 'array',
                  required: ['0', '1'],
                  properties: {
                    0: stringType,
                    1: stringType,
                  },
                }
                assert.jsonSchema(prop.expressdates, expressdateSchema)
              })
            } else if (obj[props].inventorys) {
              obj[props].inventorys.forEach((prop) => {
                const expressdateSchema = {
                  title: 'Guest User Express date in inventory item',
                  type: 'array',
                  required: ['0', '1'],
                  properties: {
                    0: stringType,
                    1: stringType,
                  },
                }
                assert.jsonSchema(prop.expressdates, expressdateSchema)
              })
            }
          }
        }
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Express => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Guest User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Express => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Guest User Delivery Method Schema',
              type: 'object',
              required: [
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
                'shipCode',
              ],
              optional: ['shipModeId'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
                selected: booleanType,
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode: stringTypeCanBeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Express => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Guest User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Guest User Order Summary => Modifying Delivery Option To Store Standard', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(storeStandardPayload(orderId))
    }, 30000)

    it(
      'PUT Order Summary Guest User Store Standard',
      () => {
        const body = orderSummaryResp.body
        const orderSummarySchema = {
          title: 'Order Summary Guest User',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringType,
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(1, true, 'string'),
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Standard => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Guest User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Standard => Basket => Delivery Options (Free Collect From Store Standard £0.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Guest User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Free Collect From Store Standard £0.00'
                  ? stringTypePattern('retail_store_standard')
                  : stringType,
              label: stringType,
              enabled:
                delOpts.label === 'Collect From Store Today £3.00'
                  ? booleanType(false)
                  : booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Standard => Basket => Products',
      () => {
        const body = orderSummaryResp.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Guest User => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
            ],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Standard => Basket => Products => assets',
      () => {
        const body = orderSummaryResp.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Shopping Bag Guest User GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Standard => Basket => inventoryPositions Json Schema',
      () => {
        const obj = orderSummaryResp.body.basket.inventoryPositions
        let inv
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              inv = obj[props].invavls
            } else {
              inv = obj[props].inventorys
            }
            const shoppingBagInventoryPositionsProps = {
              partNumber: obj[props].partNumber,
              catentryId: obj[props].catentryId,
              inv,
            }
            let shoppingBagInventoryPositionsSchema
            if (inv !== null) {
              shoppingBagInventoryPositionsSchema = {
                title:
                  'Shopping Bag Guest User Simple Product inventoryPositions Json Schema',
                type: 'object',
                required: ['partNumber', 'catentryId', 'inv'],
                properties: {
                  partNumber: { type: 'string' },
                  catEntryId: { type: 'number' },
                  inv: {
                    type: 'array',
                    minItems: 1,
                    uniqueItems: true,
                    items: { type: 'object' },
                  },
                },
              }
            } else {
              shoppingBagInventoryPositionsSchema = {
                title:
                  'Shopping Bag Guest User Simple Product inventoryPositions Json Schema',
                type: 'object',
                required: ['partNumber', 'catentryId', 'inv'],
                properties: {
                  partNumber: { type: 'string' },
                  catEntryId: { type: 'number' },
                },
              }
            }
            assert.jsonSchema(
              shoppingBagInventoryPositionsProps,
              shoppingBagInventoryPositionsSchema
            )
          }
        }
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Standard => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = orderSummaryResp.body.basket.inventoryPositions
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              obj[props].invavls.forEach((prop) => {
                const shoppingBagInvAvlsPositionsProps = {
                  cutofftime: prop.cutofftime,
                  quantity: prop.quantity,
                  stlocIdentifier: prop.stlocIdentifier,
                  expressdates: prop.expressdates,
                }
                const shoppingBagInvAvlsPositionsSchema = {
                  title:
                    'Shopping Bag Guest User Simple Product inventoryPositions => inventories && invavls Json Schema',
                  type: 'object',
                  required: [
                    'cutofftime',
                    'quantity',
                    'stlocIdentifier',
                    'expressdates',
                  ],
                  properties: {
                    cutofftime: { type: 'string' },
                    quantity: { type: 'number' },
                    stlocIdentifier: { type: 'string' },
                    expressdates: {
                      type: 'array',
                      minItems: 2,
                      uniqueItems: true,
                      items: { type: 'string' },
                    },
                  },
                }
                assert.jsonSchema(
                  shoppingBagInvAvlsPositionsProps,
                  shoppingBagInvAvlsPositionsSchema
                )
              })
            } else if (obj[props].inventorys) {
              obj[props].inventorys.forEach((prop) => {
                const shoppingBagInventorysPositionsProps = {
                  cutofftime: prop.cutofftime,
                  quantity: prop.quantity,
                  ffmcenterId: prop.ffmcenterId,
                  expressdates: prop.expressdates,
                }
                const shoppingBagInventorysPositionsSchema = {
                  title:
                    'Shopping Bag Guest User Simple Product inventoryPositions => inventories && invavls Json Schema',
                  type: 'object',
                  required: [
                    'cutofftime',
                    'quantity',
                    'ffmcenterId',
                    'expressdates',
                  ],
                  properties: {
                    cutofftime: { type: 'string' },
                    quantity: { type: 'number' },
                    ffmcenterId: { type: 'number' },
                    expressdates: {
                      type: 'array',
                      minItems: 2,
                      uniqueItems: true,
                      items: { type: 'string' },
                    },
                  },
                }
                assert.jsonSchema(
                  shoppingBagInventorysPositionsProps,
                  shoppingBagInventorysPositionsSchema
                )
              })
            }
          }
        }
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Standard => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = orderSummaryResp.body.basket.inventoryPositions
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              obj[props].invavls.forEach((prop) => {
                const expressdateSchema = {
                  title: 'Guest User Express date in inventory item',
                  type: 'array',
                  required: ['0', '1'],
                  properties: {
                    0: stringType,
                    1: stringType,
                  },
                }
                assert.jsonSchema(prop.expressdates, expressdateSchema)
              })
            } else if (obj[props].inventorys) {
              obj[props].inventorys.forEach((prop) => {
                const expressdateSchema = {
                  title: 'Guest User Express date in inventory item',
                  type: 'array',
                  required: ['0', '1'],
                  properties: {
                    0: stringType,
                    1: stringType,
                  },
                }
                assert.jsonSchema(prop.expressdates, expressdateSchema)
              })
            }
          }
        }
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Standard => deliveryLocations Json Schema',
      () => {
        orderSummaryResp.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Guest User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: booleanType,
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Standard => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Guest User Delivery Method Schema',
              type: 'object',
              required: [
                'shipModeId',
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
              ],
              optional: ['shipCode'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription: stringType,
                selected: booleanType,
                deliveryOptions: arrayType(),
                enabled: booleanType,
                cost: stringType,
                shipCode: stringType,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Standard => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        orderSummaryResp.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Guest User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Guest User Order Summary => Modifying Delivery Option To Store Express', () => {
    it(
      'PUT Order Summary Guest User Store Express',
      () => {
        const body = orderSummaryResp.body
        const orderSummarySchema = {
          title: 'Order Summary Guest User',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringType,
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(1, true, 'string'),
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Express => Basket',
      () => {
        const body = orderSummaryResp.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Guest User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Express => Basket => Delivery Options (Collect From Store Express £3.00)',
      () => {
        const body = orderSummaryResp.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Guest User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId: stringType,
              label: stringType,
              enabled: booleanType,
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Express => Basket => Products',
      () => {
        const body = orderSummaryResp.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Guest User => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
            ],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Express => Basket => Products => assets',
      () => {
        const body = orderSummaryResp.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Shopping Bag Guest User GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Express => Basket => inventoryPositions Json Schema',
      () => {
        const obj = orderSummaryResp.body.basket.inventoryPositions
        let inv
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              inv = obj[props].invavls
            } else {
              inv = obj[props].inventorys
            }
            const shoppingBagInventoryPositionsProps = {
              partNumber: obj[props].partNumber,
              catentryId: obj[props].catentryId,
              inv,
            }
            let shoppingBagInventoryPositionsSchema
            if (inv !== null) {
              shoppingBagInventoryPositionsSchema = {
                title:
                  'Shopping Bag Guest User Simple Product inventoryPositions Json Schema',
                type: 'object',
                required: ['partNumber', 'catentryId', 'inv'],
                properties: {
                  partNumber: { type: 'string' },
                  catEntryId: { type: 'number' },
                  inv: {
                    type: 'array',
                    minItems: 1,
                    uniqueItems: true,
                    items: { type: 'object' },
                  },
                },
              }
            } else {
              shoppingBagInventoryPositionsSchema = {
                title:
                  'Shopping Bag Guest User Simple Product inventoryPositions Json Schema',
                type: 'object',
                required: ['partNumber', 'catentryId', 'inv'],
                properties: {
                  partNumber: { type: 'string' },
                  catEntryId: { type: 'number' },
                },
              }
            }
            assert.jsonSchema(
              shoppingBagInventoryPositionsProps,
              shoppingBagInventoryPositionsSchema
            )
          }
        }
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Express => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = orderSummaryResp.body.basket.inventoryPositions
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              obj[props].invavls.forEach((prop) => {
                const shoppingBagInvAvlsPositionsProps = {
                  cutofftime: prop.cutofftime,
                  quantity: prop.quantity,
                  stlocIdentifier: prop.stlocIdentifier,
                  expressdates: prop.expressdates,
                }
                const shoppingBagInvAvlsPositionsSchema = {
                  title:
                    'Shopping Bag Guest User Simple Product inventoryPositions => inventories && invavls Json Schema',
                  type: 'object',
                  required: [
                    'cutofftime',
                    'quantity',
                    'stlocIdentifier',
                    'expressdates',
                  ],
                  properties: {
                    cutofftime: { type: 'string' },
                    quantity: { type: 'number' },
                    stlocIdentifier: { type: 'string' },
                    expressdates: {
                      type: 'array',
                      minItems: 2,
                      uniqueItems: true,
                      items: { type: 'string' },
                    },
                  },
                }
                assert.jsonSchema(
                  shoppingBagInvAvlsPositionsProps,
                  shoppingBagInvAvlsPositionsSchema
                )
              })
            } else if (obj[props].inventorys) {
              obj[props].inventorys.forEach((prop) => {
                const shoppingBagInventorysPositionsProps = {
                  cutofftime: prop.cutofftime,
                  quantity: prop.quantity,
                  ffmcenterId: prop.ffmcenterId,
                  expressdates: prop.expressdates,
                }
                const shoppingBagInventorysPositionsSchema = {
                  title:
                    'Shopping Bag Guest User Simple Product inventoryPositions => inventories && invavls Json Schema',
                  type: 'object',
                  required: [
                    'cutofftime',
                    'quantity',
                    'ffmcenterId',
                    'expressdates',
                  ],
                  properties: {
                    cutofftime: { type: 'string' },
                    quantity: { type: 'number' },
                    ffmcenterId: { type: 'number' },
                    expressdates: {
                      type: 'array',
                      minItems: 2,
                      uniqueItems: true,
                      items: { type: 'string' },
                    },
                  },
                }
                assert.jsonSchema(
                  shoppingBagInventorysPositionsProps,
                  shoppingBagInventorysPositionsSchema
                )
              })
            }
          }
        }
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Express => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = orderSummaryResp.body.basket.inventoryPositions
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              obj[props].invavls.forEach((prop) => {
                const expressdateSchema = {
                  title: 'Guest User Express date in inventory item',
                  type: 'array',
                  required: ['0', '1'],
                  properties: {
                    0: stringType,
                    1: stringType,
                  },
                }
                assert.jsonSchema(prop.expressdates, expressdateSchema)
              })
            } else if (obj[props].inventorys) {
              obj[props].inventorys.forEach((prop) => {
                const expressdateSchema = {
                  title: 'Guest User Express date in inventory item',
                  type: 'array',
                  required: ['0', '1'],
                  properties: {
                    0: stringType,
                    1: stringType,
                  },
                }
                assert.jsonSchema(prop.expressdates, expressdateSchema)
              })
            }
          }
        }
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Express => deliveryLocations Json Schema',
      () => {
        orderSummaryResp.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Guest User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: booleanType,
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Express => deliveryLocations => deliveryMethods Json Schema',
      () => {
        orderSummaryResp.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Guest User Delivery Method Schema',
              type: 'object',
              required: [
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
                'shipCode',
              ],
              optional: ['shipCode'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription: stringTypeCanBeEmpty,
                selected: booleanType,
                deliveryOptions: arrayType(),
                enabled: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'STORE_IMMEDIATE'
                      ? [false]
                      : [true],
                },
                cost:
                  deliveryMethod.deliveryType === 'STORE_STANDARD'
                    ? numberType
                    : stringType,
                shipCode: stringTypeCanBeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Store Express => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        orderSummaryResp.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Guest User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })

  describe('Guest User Order Summary => Modifying Delivery Option To Parcelshop', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(parcelshopPayload(orderId))
    }, 30000)

    it(
      'PUT Order Summary Guest User Parcelshop',
      () => {
        const body = response.body
        const orderSummarySchema = {
          title: 'Order Summary Guest User',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
            'storeDetails',
            'deliveryStoreCode',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringType,
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(1, true, 'string'),
            storeDetails: objectType,
            deliveryStoreCode: stringType,
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Parcelshop => Basket',
      () => {
        const body = response.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Guest User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Parcelshop => Basket => Delivery Options (Collect from ParcelShop £4.00)',
      () => {
        const body = response.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Guest User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected:
                delOpts.label === 'Collect from ParcelShop £4.00'
                  ? booleanType(true)
                  : booleanType(false),
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Collect from ParcelShop £4.00'
                  ? stringTypePattern('retail_store_collection')
                  : stringType,
              label: delOpts.selected
                ? stringTypePattern('Collect from ParcelShop £4.00')
                : stringType,
              enabled:
                delOpts.label === 'Collect From Store Today £3.00'
                  ? booleanType(false)
                  : booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Parcelshop => Basket => Products',
      () => {
        const body = response.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Guest User => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
            ],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Parcelshop => Basket => Products => assets',
      () => {
        const body = response.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Shopping Bag Guest User GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Parcelshop => Basket => inventoryPositions Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        let inv
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              inv = obj[props].invavls
            } else {
              inv = obj[props].inventorys
            }
            const shoppingBagInventoryPositionsProps = {
              partNumber: obj[props].partNumber,
              catentryId: obj[props].catentryId,
              inv,
            }
            let shoppingBagInventoryPositionsSchema
            if (inv !== null) {
              shoppingBagInventoryPositionsSchema = {
                title:
                  'Shopping Bag Guest User Simple Product inventoryPositions Json Schema',
                type: 'object',
                required: ['partNumber', 'catentryId', 'inv'],
                properties: {
                  partNumber: { type: 'string' },
                  catEntryId: { type: 'number' },
                  inv: {
                    type: 'array',
                    minItems: 1,
                    uniqueItems: true,
                    items: { type: 'object' },
                  },
                },
              }
            } else {
              shoppingBagInventoryPositionsSchema = {
                title:
                  'Shopping Bag Guest User Simple Product inventoryPositions Json Schema',
                type: 'object',
                required: ['partNumber', 'catentryId', 'inv'],
                properties: {
                  partNumber: { type: 'string' },
                  catEntryId: { type: 'number' },
                },
              }
            }
            assert.jsonSchema(
              shoppingBagInventoryPositionsProps,
              shoppingBagInventoryPositionsSchema
            )
          }
        }
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Parcelshop => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              obj[props].invavls.forEach((prop) => {
                const shoppingBagInvAvlsPositionsProps = {
                  cutofftime: prop.cutofftime,
                  quantity: prop.quantity,
                  stlocIdentifier: prop.stlocIdentifier,
                  expressdates: prop.expressdates,
                }
                const shoppingBagInvAvlsPositionsSchema = {
                  title:
                    'Shopping Bag Guest User Simple Product inventoryPositions => inventories && invavls Json Schema',
                  type: 'object',
                  required: [
                    'cutofftime',
                    'quantity',
                    'stlocIdentifier',
                    'expressdates',
                  ],
                  properties: {
                    cutofftime: { type: 'string' },
                    quantity: { type: 'number' },
                    stlocIdentifier: { type: 'string' },
                    expressdates: {
                      type: 'array',
                      minItems: 2,
                      uniqueItems: true,
                      items: { type: 'string' },
                    },
                  },
                }
                assert.jsonSchema(
                  shoppingBagInvAvlsPositionsProps,
                  shoppingBagInvAvlsPositionsSchema
                )
              })
            } else if (obj[props].inventorys) {
              obj[props].inventorys.forEach((prop) => {
                const shoppingBagInventorysPositionsProps = {
                  cutofftime: prop.cutofftime,
                  quantity: prop.quantity,
                  ffmcenterId: prop.ffmcenterId,
                  expressdates: prop.expressdates,
                }
                const shoppingBagInventorysPositionsSchema = {
                  title:
                    'Shopping Bag Guest User Simple Product inventoryPositions => inventories && invavls Json Schema',
                  type: 'object',
                  required: [
                    'cutofftime',
                    'quantity',
                    'ffmcenterId',
                    'expressdates',
                  ],
                  properties: {
                    cutofftime: { type: 'string' },
                    quantity: { type: 'number' },
                    ffmcenterId: { type: 'number' },
                    expressdates: {
                      type: 'array',
                      minItems: 2,
                      uniqueItems: true,
                      items: { type: 'string' },
                    },
                  },
                }
                assert.jsonSchema(
                  shoppingBagInventorysPositionsProps,
                  shoppingBagInventorysPositionsSchema
                )
              })
            }
          }
        }
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Parcelshop => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = response.body.basket.inventoryPositions
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              obj[props].invavls.forEach((prop) => {
                const expressdateSchema = {
                  title: 'Guest User Express date in inventory item',
                  type: 'array',
                  required: ['0', '1'],
                  properties: {
                    0: stringType,
                    1: stringType,
                  },
                }
                assert.jsonSchema(prop.expressdates, expressdateSchema)
              })
            } else if (obj[props].inventorys) {
              obj[props].inventorys.forEach((prop) => {
                const expressdateSchema = {
                  title: 'Guest User Express date in inventory item',
                  type: 'array',
                  required: ['0', '1'],
                  properties: {
                    0: stringType,
                    1: stringType,
                  },
                }
                assert.jsonSchema(prop.expressdates, expressdateSchema)
              })
            }
          }
        }
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Parcelshop => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Guest User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'PARCELSHOP'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Parcelshop => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Guest User Delivery Method Schema',
              type: 'object',
              required: [
                'shipModeId',
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
              ],
              optional: ['shipCode'],
              properties: {
                shipModeId: numberType,
                deliveryType: 'PARCELSHOP_COLLECTION',
                label: 'Collect from ParcelShop',
                additionalDescription: stringType,
                selected: booleanType(true),
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode: stringType,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Parcelshop => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Guest User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary => storeDetails',
      () => {
        const storeDetails = response.body.storeDetails
        const storeDetailsSchema = {
          title: 'Existing User Store Details Schema',
          type: 'object',
          required: [
            'address1',
            'address2',
            'city',
            'state',
            'country',
            'postcode',
          ],
          properties: {
            address1: stringType,
            address2: stringType,
            city: stringType,
            state: stringType,
            country: stringType,
            postcode: stringType,
          },
        }
        assert.jsonSchema(storeDetails, storeDetailsSchema)
      },
      30000
    )
  })

  describe('Guest User Order Summary => Modifying Delivery Option To Home Standard', () => {
    let response
    beforeAll(async () => {
      response = await superagent
        .put(eps.checkout.updateOrderSummary.path)
        .set(headers)
        .set({ Cookie: jsessionId })
        .send(homeStandardPayload(orderId))
    }, 30000)

    it(
      'PUT Order Summary Guest User Home Standard',
      () => {
        const body = response.body
        const orderSummarySchema = {
          title: 'Order Summary Guest User',
          type: 'object',
          required: [
            'basket',
            'deliveryLocations',
            'giftCards',
            'deliveryInstructions',
            'smsMobileNumber',
            'shippingCountry',
            'savedAddresses',
            'ageVerificationDeliveryConfirmationRequired',
            'estimatedDelivery',
          ],
          properties: {
            basket: objectType,
            deliveryLocations: arrayType(3),
            giftCards: arrayType(),
            deliveryInstructions: stringTypeEmpty,
            smsMobileNumber: stringTypeEmpty,
            shippingCountry: stringType,
            savedAddresses: arrayType(),
            ageVerificationDeliveryConfirmationRequired: booleanType(false),
            estimatedDelivery: arrayType(1, true, 'string'),
          },
        }
        assert.jsonSchema(body, orderSummarySchema)
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Standard => Basket',
      () => {
        const body = response.body.basket
        const orderSummarySchemaBasket = {
          title: 'Order Summary Guest User => Basket',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(5),
            promotions: arrayType(),
            discounts: arrayType(),
            products: arrayType(1),
            savedProducts: arrayType(),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
          },
        }
        assert.jsonSchema(body, orderSummarySchemaBasket)
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Standard => Basket => Delivery Options (Standard Delivery 4.00)',
      () => {
        const body = response.body.basket.deliveryOptions
        body.forEach((delOpts) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Guest User => Basket => Delivery Options',
            type: 'object',
            required: [
              'selected',
              'deliveryOptionId',
              'deliveryOptionExternalId',
              'label',
              'enabled',
            ],
            properties: {
              selected: booleanType,
              deliveryOptionsId: numberType,
              deliveryOptionExternalId:
                delOpts.label === 'Standard Delivery £4.00'
                  ? stringTypePattern('s')
                  : stringType,
              label: stringType,
              enabled:
                delOpts.label === 'Collect From Store Today £3.00'
                  ? booleanType(false)
                  : booleanType(true),
            },
          }
          assert.jsonSchema(delOpts, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Standard => Basket => Products',
      () => {
        const body = response.body.basket.products
        body.forEach((product) => {
          const orderSummarySchemaBasketDelOpts = {
            title: 'Order Summary Guest User => Basket => Products',
            type: 'object',
            required: [
              'productId',
              'catEntryId',
              'orderItemId',
              'shipModeId',
              'lineNumber',
              'size',
              'name',
              'quantity',
              'inStock',
              'unitPrice',
              'totalPrice',
              'assets',
              'lowStock',
              'items',
              'bundleProducts',
              'attributes',
              'colourSwatches',
              'tpmLinks',
              'bundleSlots',
              'ageVerificationRequired',
              'isBundleOrOutfit',
              'discountText',
            ],
            properties: {
              productId: numberType,
              catEntryId: numberType,
              orderItemId: numberType,
              shipModeId: numberType,
              lineNumber: stringType,
              size: stringType,
              name: stringType,
              quantity: numberType,
              inStock: booleanType(true),
              unitPrice: stringType,
              totalPrice: numberType,
              assets: arrayType(4),
              lowStock: { type: 'boolean' },
              items: arrayType(),
              bundleProducts: arrayType(),
              attributes: objectType,
              colourSwatches: arrayType(),
              tpmLinks: arrayType(),
              bundleSlots: arrayType(),
              ageVerificationRequired: booleanType(false),
              isBundleOrOutfit: booleanType(false),
              discountText: stringTypeEmpty,
            },
          }
          assert.jsonSchema(product, orderSummarySchemaBasketDelOpts)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Standard => Basket => Products => assets',
      () => {
        const body = response.body.basket.products
        body.forEach((prod) => {
          prod.assets.forEach((asset) => {
            const shoppingBagProductsSchema = {
              title:
                'Shopping Bag Guest User GET Product products assets Json Schema',
              type: 'object',
              required: ['assetType', 'index', 'url'],
              properties: {
                assetType: stringType,
                index: numberType,
                url: stringType,
              },
            }
            assert.jsonSchema(asset, shoppingBagProductsSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Standard => Basket => inventoryPositions Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        let inv
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              inv = obj[props].invavls
            } else {
              inv = obj[props].inventorys
            }
            const shoppingBagInventoryPositionsProps = {
              partNumber: obj[props].partNumber,
              catentryId: obj[props].catentryId,
              inv,
            }
            let shoppingBagInventoryPositionsSchema
            if (inv !== null) {
              shoppingBagInventoryPositionsSchema = {
                title:
                  'Shopping Bag Guest User Simple Product inventoryPositions Json Schema',
                type: 'object',
                required: ['partNumber', 'catentryId', 'inv'],
                properties: {
                  partNumber: { type: 'string' },
                  catEntryId: { type: 'number' },
                  inv: {
                    type: 'array',
                    minItems: 1,
                    uniqueItems: true,
                    items: { type: 'object' },
                  },
                },
              }
            } else {
              shoppingBagInventoryPositionsSchema = {
                title:
                  'Shopping Bag Guest User Simple Product inventoryPositions Json Schema',
                type: 'object',
                required: ['partNumber', 'catentryId', 'inv'],
                properties: {
                  partNumber: { type: 'string' },
                  catEntryId: { type: 'number' },
                },
              }
            }
            assert.jsonSchema(
              shoppingBagInventoryPositionsProps,
              shoppingBagInventoryPositionsSchema
            )
          }
        }
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Standard => Basket => inventoryPositions => inventories && invavls Json Schema',
      () => {
        const obj = response.body.basket.inventoryPositions
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              obj[props].invavls.forEach((prop) => {
                const shoppingBagInvAvlsPositionsProps = {
                  cutofftime: prop.cutofftime,
                  quantity: prop.quantity,
                  stlocIdentifier: prop.stlocIdentifier,
                  expressdates: prop.expressdates,
                }
                const shoppingBagInvAvlsPositionsSchema = {
                  title:
                    'Shopping Bag Guest User Simple Product inventoryPositions => inventories && invavls Json Schema',
                  type: 'object',
                  required: [
                    'cutofftime',
                    'quantity',
                    'stlocIdentifier',
                    'expressdates',
                  ],
                  properties: {
                    cutofftime: { type: 'string' },
                    quantity: { type: 'number' },
                    stlocIdentifier: { type: 'string' },
                    expressdates: {
                      type: 'array',
                      minItems: 2,
                      uniqueItems: true,
                      items: { type: 'string' },
                    },
                  },
                }
                assert.jsonSchema(
                  shoppingBagInvAvlsPositionsProps,
                  shoppingBagInvAvlsPositionsSchema
                )
              })
            } else if (obj[props].inventorys) {
              obj[props].inventorys.forEach((prop) => {
                const shoppingBagInventorysPositionsProps = {
                  cutofftime: prop.cutofftime,
                  quantity: prop.quantity,
                  ffmcenterId: prop.ffmcenterId,
                  expressdates: prop.expressdates,
                }
                const shoppingBagInventorysPositionsSchema = {
                  title:
                    'Shopping Bag Guest User Simple Product inventoryPositions => inventories && invavls Json Schema',
                  type: 'object',
                  required: [
                    'cutofftime',
                    'quantity',
                    'ffmcenterId',
                    'expressdates',
                  ],
                  properties: {
                    cutofftime: { type: 'string' },
                    quantity: { type: 'number' },
                    ffmcenterId: { type: 'number' },
                    expressdates: {
                      type: 'array',
                      minItems: 2,
                      uniqueItems: true,
                      items: { type: 'string' },
                    },
                  },
                }
                assert.jsonSchema(
                  shoppingBagInventorysPositionsProps,
                  shoppingBagInventorysPositionsSchema
                )
              })
            }
          }
        }
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Standard => Basket => inventoryPositions => inventories && invavls => ExpressDates Properties Values',
      () => {
        const obj = response.body.basket.inventoryPositions
        for (const props in obj) {
          if (obj.hasOwnProperty(props)) {
            if (obj[props].invavls) {
              obj[props].invavls.forEach((prop) => {
                const expressdateSchema = {
                  title: 'Guest User Express date in inventory item',
                  type: 'array',
                  required: ['0', '1'],
                  properties: {
                    0: stringType,
                    1: stringType,
                  },
                }
                assert.jsonSchema(prop.expressdates, expressdateSchema)
              })
            } else if (obj[props].inventorys) {
              obj[props].inventorys.forEach((prop) => {
                const expressdateSchema = {
                  title: 'Guest User Express date in inventory item',
                  type: 'array',
                  required: ['0', '1'],
                  properties: {
                    0: stringType,
                    1: stringType,
                  },
                }
                assert.jsonSchema(prop.expressdates, expressdateSchema)
              })
            }
          }
        }
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Standard => deliveryLocations Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          const deliveryLocSchema = {
            title: 'Guest User Delivery Location Schema',
            type: 'object',
            required: [
              'deliveryLocationType',
              'selected',
              'enabled',
              'label',
              'deliveryMethods',
            ],
            properties: {
              deliveryLocationType: stringType,
              selected: {
                type: 'boolean',
                enum:
                  deliveryLoc.deliveryLocationType === 'HOME'
                    ? [true]
                    : [false],
              },
              enabled: booleanType(true),
              label: stringType,
              deliveryMethods: arrayType(),
            },
          }
          assert.jsonSchema(deliveryLoc, deliveryLocSchema)
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Standard => deliveryLocations => deliveryMethods Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            const deliveryLocSchema = {
              title: 'Guest User Delivery Method Schema',
              type: 'object',
              required: [
                'deliveryType',
                'label',
                'additionalDescription',
                'selected',
                'deliveryOptions',
                'enabled',
                'cost',
                'shipCode',
              ],
              optional: ['shipModeId'],
              properties: {
                shipModeId: numberType,
                deliveryType: stringType,
                label: stringType,
                additionalDescription:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
                selected: {
                  type: 'boolean',
                  enum:
                    deliveryMethod.deliveryType === 'HOME_STANDARD'
                      ? [true]
                      : [false],
                },
                deliveryOptions: arrayType(),
                enabled: booleanType(true),
                cost: stringType,
                shipCode:
                  deliveryMethod.deliveryType === 'HOME_STANDARD'
                    ? stringType
                    : stringTypeEmpty,
              },
            }
            assert.jsonSchema(deliveryMethod, deliveryLocSchema)
          })
        })
      },
      30000
    )

    it(
      'PUT Order Summary Guest User Home Standard => deliveryLocations => deliveryMethods => deliveryOptions Json Schema',
      () => {
        response.body.deliveryLocations.forEach((deliveryLoc) => {
          deliveryLoc.deliveryMethods.forEach((deliveryMethod) => {
            deliveryMethod.deliveryOptions.forEach((deliveryOption) => {
              const deliveryOptionSchema = {
                title: 'Guest User Delivery Option Schema',
                type: 'object',
                required: [
                  'dayText',
                  'dateText',
                  'nominatedDate',
                  'selected',
                  'shipModeId',
                  'price',
                  'enabled',
                ],
                properties: {
                  dayText: stringType,
                  dateText: stringType,
                  nominatedDate: stringType,
                  selected: { type: 'boolean' },
                  shipModeId: numberType,
                  price: stringType,
                  enabled: { type: 'boolean' },
                },
              }
              assert.jsonSchema(deliveryOption, deliveryOptionSchema)
            })
          })
        })
      },
      30000
    )
  })
})
