/* eslint-disable func-names */
require('babel-polyfill')

jest.unmock('superagent')
import superagent from 'superagent'
import chai from 'chai'

chai.use(require('chai-json-schema'))

const assert = chai.assert
import eps from '../routes_tests'
import {
  headers,
  stringType,
  stringTypeEmpty,
  stringTypeCanBeEmpty,
  numberType,
  booleanType,
  objectType,
  arrayType,
  userCredentialsOrdersAndReturnsProfile,
} from '../utilis'
import { getOrderDetailsForAllOfOrderHistory } from '../utilis/orderDetails'

describe('Order History Tests', () => {
  describe('Order History Schema', () => {
    let response
    let jsessionid
    beforeAll(async () => {
      const resHeader = await superagent
        .post(eps.myAccount.login.path)
        .set(headers)
        .send(userCredentialsOrdersAndReturnsProfile)
      jsessionid = resHeader.headers['set-cookie'].toString().split(',')[0]
      response = await superagent
        .get(eps.myAccount.orderHistory.path)
        .set(headers)
        .set({ Cookie: jsessionid })
    })

    it('Order History - Main', () => {
      const body = response.body
      const orderHistorySchema = {
        title: 'Order History Schema',
        type: 'object',
        required: ['orders'],
        properties: {
          orders: arrayType(1),
        },
      }
      assert.jsonSchema(body, orderHistorySchema)
    })

    it('Order History - Orders', () => {
      const body = response.body.orders
      body.forEach((obj) => {
        const orderHistoryObjSchema = {
          title: 'Order Schema',
          type: 'object',
          required: [
            'orderId',
            'date',
            'status',
            'statusCode',
            'total',
            'returnPossible',
            'returnRequested',
          ],
          properties: {
            orderId: numberType,
            date: stringType,
            status: stringType,
            statusCode: stringTypeCanBeEmpty,
            total: stringType,
            returnPossible: booleanType(false),
            returnRequested: booleanType(false),
          },
        }
        assert.jsonSchema(obj, orderHistoryObjSchema)
      })
    })
  })

  describe('Order History Details Schema', function() {
    beforeAll(async () => {
      this.testData = await getOrderDetailsForAllOfOrderHistory()
    }, 60000)

    it('Order Details - Main', () => {
      this.testData.forEach((testSet) => {
        const orderDetailsSchema = {
          title: 'Order Details Schema',
          type: 'object',
          required: [
            'orderId',
            'subTotal',
            'statusCode',
            'status',
            'returnPossible',
            'returnRequested',
            'deliveryMethod',
            'deliveryDate',
            'deliveryCost',
            'deliveryCarrier',
            'deliveryPrice',
            'totalOrderPrice',
            'totalOrdersDiscount',
            'totalOrdersDiscountLabel',
            'billingAddress',
            'deliveryAddress',
            'orderLines',
            'paymentDetails',
            'smsNumber',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            statusCode: stringType,
            status: stringType,
            returnPossible: booleanType(false),
            returnRequested: booleanType(false),
            deliveryMethod: stringType,
            deliveryDate: stringType,
            deliveryCost: stringTypeCanBeEmpty,
            deliveryCarrier: stringType,
            deliveryPrice: stringType,
            totalOrderPrice: stringType,
            totalOrdersDiscountLabel: stringType,
            totalOrdersDiscount: stringTypeEmpty,
            billingAddress: objectType,
            deliveryAddress: objectType,
            orderLines: arrayType(1),
            paymentDetails: arrayType(1),
            smsNumber: stringTypeEmpty,
          },
        }
        assert.jsonSchema(testSet.orderDetails, orderDetailsSchema)
      })
    })

    it('Order Details - Billing Address Schema', () => {
      this.testData.forEach((testSet) => {
        const body = testSet.orderDetails.billingAddress
        const orderDetailsBillingSchema = {
          title: 'Order Details Billing Address Schema',
          type: 'object',
          required: [
            'name',
            'address1',
            'address2',
            'address3',
            'address4',
            'country',
          ],
          properties: {
            name: stringType,
            address1: stringType,
            address2: stringTypeEmpty,
            address3: stringType,
            address4: stringType,
            country: stringType,
          },
        }
        assert.jsonSchema(body, orderDetailsBillingSchema)
      })
    })

    it('Order Details - Delivery Address Schema', () => {
      this.testData.forEach((testSet) => {
        const body = testSet.orderDetails.deliveryAddress
        const orderDetailsDeliverySchema = {
          title: 'Order Details Delivery Address Schema',
          type: 'object',
          required: [
            'name',
            'address1',
            'address2',
            'address3',
            'address4',
            'country',
          ],
          properties: {
            name: stringType,
            address1: stringType,
            address2: stringTypeEmpty,
            address3: stringType,
            address4: stringType,
            country: stringType,
          },
        }
        assert.jsonSchema(body, orderDetailsDeliverySchema)
      })
    })

    it('Order Details - Order Lines Schema', () => {
      this.testData.forEach((testSet) => {
        const body = testSet.orderDetails.orderLines
        body.forEach((obj) => {
          const orderDetailsOrderLinesSchema = {
            title: 'Order Details Order Lines Schema',
            type: 'object',
            required: [
              'lineNo',
              'name',
              'size',
              'colour',
              'imageUrl',
              'quantity',
              'unitPrice',
              'discount',
              'total',
              'nonRefundable',
            ],
            optional: ['wasPrice', 'trackingNumber'],
            properties: {
              lineNo: stringType,
              name: stringType,
              size: stringType,
              colour: stringType,
              imageUrl: stringType,
              quantity: numberType,
              unitPrice: stringTypeEmpty, // temp fix until MJI-1063 is merged to Monty, this should never be empty but is for multi-line order of same product with different size.
              discount: stringTypeEmpty,
              total: stringType,
              nonRefundable: booleanType(false),
              wasPrice: stringTypeCanBeEmpty,
              trackingNumber: stringType,
            },
          }
          assert.jsonSchema(obj, orderDetailsOrderLinesSchema)
        })
      })
    })

    it('Order Details - Payment Details Schema', () => {
      this.testData.forEach((testSet) => {
        const body = testSet.orderDetails.paymentDetails
        body.forEach((obj) => {
          const orderDetailsPaymentDetailsSchema = {
            title: 'Order Details Payment Details Schema',
            type: 'object',
            required: ['paymentMethod', 'cardNumberStar', 'totalCost'],
            properties: {
              paymentMethod: stringType,
              cardNumberStar: stringTypeCanBeEmpty,
              totalCost: stringType,
            },
          }
          assert.jsonSchema(obj, orderDetailsPaymentDetailsSchema)
        })
      })
    })
  })
})
