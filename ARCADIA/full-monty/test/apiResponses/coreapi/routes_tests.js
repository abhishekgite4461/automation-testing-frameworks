const baseUrl = `http://local.m.topshop.com:3000/api`

export default {
  navigation: {
    mobile: {
      method: 'get',
      path: `${baseUrl}/navigation/categories`,
      status: 'done',
    },
    desktop: {
      method: 'get',
      path: `${baseUrl}/desktop/navigation`,
      status: 'done',
    },
  },
  myAccount: {
    account: {
      method: 'get',
      path: `${baseUrl}/account`,
      status: 'done',
    },
    register: {
      method: 'post',
      path: `${baseUrl}/account/register`,
      status: 'done',
    },
    login: {
      method: 'post',
      path: `${baseUrl}/account/login`,
      status: 'done',
    },
    logout: {
      method: 'delete',
      path: `${baseUrl}/account/logout`,
      status: 'done',
    },
    changePassword: {
      method: 'put',
      path: `${baseUrl}/account/changepassword`,
      status: 'done',
    },
    forgotPassword: {
      method: 'post',
      path: `${baseUrl}/account/forgetpassword`,
      status: 'done',
    },
    resetPassword: {
      method: 'put',
      path: `${baseUrl}/account/reset_password`,
      status: 'TODO',
    },
    resetPasswordLink: {
      method: 'post',
      path: `${baseUrl}/account/reset_password_link`,
      status: 'TODO',
    },
    customerDetails: {
      method: 'get',
      path: `${baseUrl}/account/customerdetails`,
      status: 'TODO',
    },
    changeCustomerDetails: {
      method: 'put',
      path: `${baseUrl}/account/customerdetails`,
      status: 'done',
    },
    accountShortProfile: {
      method: 'put',
      path: `${baseUrl}/account/shortdetails`,
      status: 'done',
    },
    orderHistory: {
      method: 'get',
      path: `${baseUrl}/account/order-history`,
      status: 'done',
    },
    orderHistoryDetails: {
      method: 'get',
      path: (orderId) => `${baseUrl}/account/order-history/${orderId}`,
      status: 'done',
    },
    returnHistory: {
      method: 'get',
      path: `${baseUrl}/account/return-history`,
      status: 'done',
    },
    returnHistoryDetails: {
      method: 'get',
      path: (orderId, rmaId) =>
        `${baseUrl}/account/return-history/${orderId}/${rmaId}`,
      status: 'done',
    },
  },
  shoppingBag: {
    addItem: {
      method: 'post',
      path: `${baseUrl}/shopping_bag/add_item`,
      status: 'done',
    },
    items: {
      method: 'get',
      path: `${baseUrl}/shopping_bag/get_items`,
      status: 'done',
    },
    deleteItem: {
      method: 'delete',
      path: `${baseUrl}/shopping_bag/delete_item`,
      status: 'done',
    },
    deleteAllItems: {
      method: 'delete',
      path: `${baseUrl}/shopping_bag/empty_bag`,
      status: 'TODO',
    },
    transferShoppingBag: {
      method: 'post',
      path: `${baseUrl}/shopping_bag/transfer`,
      status: 'TODO',
    },
    addPromotionCode: {
      method: 'post',
      path: `${baseUrl}/shopping_bag/addPromotionCode`,
      status: 'done',
    },
    deletePromotionCode: {
      method: 'delete',
      path: `${baseUrl}/shopping_bag/delPromotionCode`,
      status: 'done',
    },
    updateItem: {
      method: 'put',
      path: `${baseUrl}/shopping_bag/update_item`,
      status: 'done',
    },
    updateDelivery: {
      method: 'put',
      path: `${baseUrl}/shopping_bag/delivery`,
      status: 'done',
    },
    fetchSizeQty: {
      method: 'get',
      path: `${baseUrl}/shopping_bag/fetch_item_sizes_and_quantities`,
      status: 'done',
    },
  },
  products: {
    emailBackInStock: {
      method: 'post',
      path: `${baseUrl}/products/email-back-in-stock`,
      status: 'blocked',
    },
    productDetailsPage: {
      method: 'get',
      path: (productId) => `${baseUrl}/products/${productId}`,
      status: 'done',
    },
    productsListingPage: {
      method: 'get',
      path: `${baseUrl}/products`,
      status: 'done',
    },
    productsListingSEO: {
      method: 'get',
      path: `${baseUrl}/products/seo`,
      status: 'done',
    },
    productsPromo: {
      method: 'get',
      path: `${baseUrl}/products/promo`,
      status: 'blocked',
    },
    productsSeeMore: {
      method: 'get',
      path: `${baseUrl}/products/seemore`,
      status: 'blocked',
    },
    productsQuickView: {
      method: 'get',
      path: `${baseUrl}/products/quickview`,
      status: 'done',
    },
  },
  assets: {
    assetsContent: {
      method: 'get',
      path: `${baseUrl}/assets/content`,
      status: 'TODO',
    },
    assetsPath: {
      method: 'get',
      path: `${baseUrl}/assets/{pathName*}`,
      status: 'TODO',
    },
  },
  orders: {
    payOrder: {
      method: 'post',
      path: `${baseUrl}/order`,
      status: 'TODO',
    },
    updateOrder: {
      method: 'put',
      path: `${baseUrl}/order`,
      status: 'TODO',
    },
    orderComplete: {
      method: 'post',
      path: `${baseUrl}/order-complete`,
      status: 'TODO',
    },
    orderCompleteReturnUrl: {
      path: (payMethod) =>
        `${baseUrl}/order-complete?paymentMethod=${payMethod}`,
    },
  },
  checkout: {
    orderSummary: {
      method: 'get',
      path: `${baseUrl}/checkout/order_summary`,
      status: 'done',
    },
    updateOrderSummary: {
      method: 'put',
      path: `${baseUrl}/checkout/order_summary`,
      status: 'done',
    },
    updateOrderSummaryBillingAddress: {
      method: 'put',
      path: `${baseUrl}/checkout/order_summary/billing_address`,
      status: 'TODO',
    },
    updateOrderSummaryDeliveryAddress: {
      method: 'post',
      path: `${baseUrl}/checkout/order_summary/delivery_address`,
      status: 'TODO',
    },
    orderSummaryDeleteDeliveryAddress: {
      method: 'delete',
      path: `${baseUrl}/checkout/order_summary/delivery_address`,
      status: 'TODO',
    },
    addGiftCard: {
      method: 'post',
      path: `${baseUrl}/checkout/gift-card`,
      status: 'TODO',
    },
    deleteGiftCard: {
      method: 'delete',
      path: `${baseUrl}/checkout/gift-card`,
      status: 'TODO',
    },
  },
  payments: {
    payment: {
      method: 'get',
      path: `${baseUrl}/payments`,
      status: 'done',
    },
    deletePayment: {
      method: 'delete',
      path: `${baseUrl}/payments`,
      status: 'TODO',
    },
  },
  storeLocator: {
    store: {
      method: 'get',
      path: `${baseUrl}/store-locator`,
      status: 'TODO',
    },
    storeCountries: {
      method: 'get',
      path: `${baseUrl}/stores-countries`,
      status: 'TODO',
    },
  },
  emailMeInStock: {
    method: 'get',
    path: `${baseUrl}/email-me-in-stock`,
    status: 'done',
  },
  address: {
    method: 'get',
    path: `${baseUrl}/address`,
    status: 'DONE',
  },
  features: {
    method: 'get',
    path: `${baseUrl}/features`,
    status: 'TODO',
  },
  klarna: {
    klarnaSession: {
      method: 'post',
      path: `${baseUrl}/klarna-session`,
      status: 'TODO',
    },
    updateKlarnaSession: {
      method: 'put',
      path: `${baseUrl}/klarna-session`,
      status: 'TODO',
    },
  },
  basket: {
    saveBasket: {
      method: 'post',
      path: `${baseUrl}/saved_basket`,
      status: 'TODO',
    },
    savedBasket: {
      method: 'get',
      path: `${baseUrl}/saved_basket`,
      status: 'TODO',
    },
    saveItemToSavedBasket: {
      method: 'post',
      path: `${baseUrl}/saved_basket/item`,
      status: 'TODO',
    },
    restoreSavedBasket: {
      method: 'put',
      path: `${baseUrl}/saved_basket/item/restore`,
      status: 'TODO',
    },
    fetchSizeQtySavedBasket: {
      method: 'get',
      path: `${baseUrl}/saved_basket/item/fetch_item_sizes_and_quantities`,
      status: 'TODO',
    },
    deleteItemFromSavedBasket: {
      method: 'delete',
      path: `${baseUrl}/saved_basket/item`,
      status: 'TODO',
    },
    deleteSavedBasket: {
      method: 'delete',
      path: `${baseUrl}/saved_basket`,
      status: 'TODO',
    },
    updatedItemInSavedBasket: {
      method: 'put',
      path: `${baseUrl}/saved_basket/item/update_item`,
      status: 'TODO',
    },
  },
  siteOptions: {
    method: 'get',
    path: `${baseUrl}/site-options`,
    status: 'done',
  },
  wishlist: {
    createWishlist: {
      method: 'post',
      path: `${baseUrl}/wishlist/create`,
      status: 'done',
    },
    addItemWishlist: {
      method: 'post',
      path: `${baseUrl}/wishlist/add_item`,
      status: 'done',
    },
    getWishlist: {
      method: 'get',
      path: `${baseUrl}/wishlist`,
      status: 'done',
    },
    deleteItemWishlist: {
      method: 'delete',
      path: `${baseUrl}/wishlist/remove_item`,
      status: 'done',
    },
    getAllWishlists: {
      method: 'get',
      path: `${baseUrl}/wishlists`,
      status: 'TODO',
    },
    addToBagWishlist: {
      method: 'post',
      path: `${baseUrl}/wishlist/add_to_bag`,
      status: 'done',
    },
  },
  cmsPageName: {
    method: 'get',
    path: (pageName) => `${baseUrl}/cms/page/${pageName}`,
  },
  cmsSeo: {
    method: 'get',
    path: (seoUrl) => `${baseUrl}/cms/seo?pathname=${seoUrl}`,
  },
  health: {
    platformHealth: {
      method: 'get',
      path: `${baseUrl.replace('/api', '')}/platform-health`,
      status: 'done',
    },
  },
}
