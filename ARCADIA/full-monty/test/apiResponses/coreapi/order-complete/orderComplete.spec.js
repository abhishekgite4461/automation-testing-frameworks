import chai from 'chai'

chai.use(require('chai-json-schema'))

const assert = chai.assert

import { createAccount } from '../utilis/userAccount'
import { getProducts } from '../utilis/selectProducts'
import { payOrder } from '../utilis/payOrder'
import { addItemToShoppingBag } from '../utilis/shoppingBag'
import completedOrderSchema from './SharedSchemas'

// Currently the calls return 422 when completing an order
describe('It should return the Order Complete Json schema for a New User', () => {
  let products

  beforeAll(async () => {
    products = await getProducts()
  }, 60000)

  describe('ApplePay', () => {
    let newAccount
    let shoppingBag
    let orderCompleted

    beforeAll(async () => {
      try {
        newAccount = await createAccount()
        shoppingBag = await addItemToShoppingBag(
          newAccount.jsessionid,
          products.productsSimpleId
        )

        orderCompleted = await payOrder(
          newAccount.jsessionid,
          shoppingBag.orderId,
          'APPLE'
        )
      } catch (e) {
        orderCompleted = e.response.statusCode
      }
    }, 60000)

    it('error: Unprocessable Entity, Unable to retrieve the payment system.', async () => {
      expect(orderCompleted).toEqual(422)
    })
  })

  describe('can pay with VISA card', () => {
    let newAccount
    let shoppingBag
    let orderCompleted

    beforeAll(async () => {
      newAccount = await createAccount()
      shoppingBag = await addItemToShoppingBag(
        newAccount.jsessionid,
        products.productsSimpleId
      )

      orderCompleted = await payOrder(
        newAccount.jsessionid,
        shoppingBag.orderId,
        'VISA'
      )
    }, 60000)

    it('should return the completePayment Json schema for the VISA card', () => {
      const visaPaymentSchema = {
        title: 'VISA payment',
        type: 'object',
        required: completedOrderSchema.requiredPropertiesHighLevel,
        properties: completedOrderSchema.completedOrderHighLevelSchema,
      }
      assert.jsonSchema(orderCompleted.body.completedOrder, visaPaymentSchema)
    })

    it('should return billing address object schema', () => {
      const visaPaymentBillingAddressObjectSchema = {
        title: 'VISA payment object billing address',
        type: 'object',
        required: completedOrderSchema.requiredPropertiesAddresses,
        properties: completedOrderSchema.completedOrderBillingAddressSchema,
      }
      assert.jsonSchema(
        orderCompleted.body.completedOrder.billingAddress,
        visaPaymentBillingAddressObjectSchema
      )
    })

    it('should return delivery address object schema', () => {
      const visaPaymentDeliveryAddressObjectSchema = {
        title: 'VISA payment object delivery address',
        type: 'object',
        required: completedOrderSchema.requiredPropertiesAddresses,
        properties: completedOrderSchema.completedOrderDeliveryAddressSchema,
      }
      assert.jsonSchema(
        orderCompleted.body.completedOrder.billingAddress,
        visaPaymentDeliveryAddressObjectSchema
      )
    })

    it('should return order lines schema', () => {
      orderCompleted.body.completedOrder.orderLines.forEach((obj) => {
        const visaPaymentOrderLinesSchema = {
          title: 'VISA payment object order lines',
          type: 'object',
          required: completedOrderSchema.requiredPropertiesOrderLines,
          properties: completedOrderSchema.completedOrderOrderLinesSchema,
        }
        assert.jsonSchema(obj, visaPaymentOrderLinesSchema)
      })
    })

    it('should return payment details schema', () => {
      orderCompleted.body.completedOrder.paymentDetails.forEach((obj) => {
        const visaPaymentPaymentDetailsSchema = {
          title: 'VISA payment object payment details',
          type: 'object',
          required: ['paymentMethod', 'cardNumberStar', 'totalCost'],
          properties: completedOrderSchema.completedOrderPaymentDetailsSchema(
            'Visa'
          ),
        }
        assert.jsonSchema(obj, visaPaymentPaymentDetailsSchema)
      })
    })

    it('should return currency conversion schema', () => {
      const visaPaymentCurrencyConversionSchema = {
        title: 'VISA payment object currency conversion',
        type: 'object',
        required: ['currencyRate'],
        properties: completedOrderSchema.completedOrderCurrencyConversionSchema(
          'GBP'
        ),
      }
      assert.jsonSchema(
        orderCompleted.body.completedOrder.currencyConversion,
        visaPaymentCurrencyConversionSchema
      )
    })
  })

  describe('can pay with MASTERCARD card', () => {
    let newAccount
    let shoppingBag
    let orderCompleted

    beforeAll(async () => {
      newAccount = await createAccount()
      shoppingBag = await addItemToShoppingBag(
        newAccount.jsessionid,
        products.productsSimpleId
      )

      orderCompleted = await payOrder(
        newAccount.jsessionid,
        shoppingBag.orderId,
        'MCARD'
      )
    }, 60000)

    it('should return the completePayment Json schema for the MASTERCARD card', () => {
      const visaPaymentSchema = {
        title: 'MASTERCARD payment',
        type: 'object',
        required: completedOrderSchema.requiredPropertiesHighLevel,
        properties: completedOrderSchema.completedOrderHighLevelSchema,
      }
      assert.jsonSchema(orderCompleted.body.completedOrder, visaPaymentSchema)
    })

    it('should return billing address object schema', () => {
      const visaPaymentBillingAddressObjectSchema = {
        title: 'MASTERCARD payment object billing address',
        type: 'object',
        required: completedOrderSchema.requiredPropertiesAddresses,
        properties: completedOrderSchema.completedOrderBillingAddressSchema,
      }
      assert.jsonSchema(
        orderCompleted.body.completedOrder.billingAddress,
        visaPaymentBillingAddressObjectSchema
      )
    })

    it('should return delivery address object schema', () => {
      const visaPaymentDeliveryAddressObjectSchema = {
        title: 'MASTERCARD payment object delivery address',
        type: 'object',
        required: completedOrderSchema.requiredPropertiesAddresses,
        properties: completedOrderSchema.completedOrderDeliveryAddressSchema,
      }
      assert.jsonSchema(
        orderCompleted.body.completedOrder.billingAddress,
        visaPaymentDeliveryAddressObjectSchema
      )
    })

    it('should return order lines schema', () => {
      orderCompleted.body.completedOrder.orderLines.forEach((obj) => {
        const visaPaymentOrderLinesSchema = {
          title: 'MASTERCARD payment object order lines',
          type: 'object',
          required: completedOrderSchema.requiredPropertiesOrderLines,
          properties: completedOrderSchema.completedOrderOrderLinesSchema,
        }
        assert.jsonSchema(obj, visaPaymentOrderLinesSchema)
      })
    })

    it('should return payment details schema', () => {
      orderCompleted.body.completedOrder.paymentDetails.forEach((obj) => {
        const visaPaymentPaymentDetailsSchema = {
          title: 'MASTERCARD payment object payment details',
          type: 'object',
          required: ['paymentMethod', 'cardNumberStar', 'totalCost'],
          properties: completedOrderSchema.completedOrderPaymentDetailsSchema(
            'Mastercard'
          ),
        }
        assert.jsonSchema(obj, visaPaymentPaymentDetailsSchema)
      })
    })

    it('should return currency conversion schema', () => {
      const visaPaymentCurrencyConversionSchema = {
        title: 'MASTERCARD payment object currency conversion',
        type: 'object',
        required: ['currencyRate'],
        properties: completedOrderSchema.completedOrderCurrencyConversionSchema(
          'GBP'
        ),
      }
      assert.jsonSchema(
        orderCompleted.body.completedOrder.currencyConversion,
        visaPaymentCurrencyConversionSchema
      )
    })
  })

  describe('can pay with AMERICA EXPRESS card', () => {
    let newAccount
    let shoppingBag
    let orderCompleted

    beforeAll(async () => {
      newAccount = await createAccount()
      shoppingBag = await addItemToShoppingBag(
        newAccount.jsessionid,
        products.productsSimpleId
      )

      orderCompleted = await payOrder(
        newAccount.jsessionid,
        shoppingBag.orderId,
        'AMEX'
      )
    }, 60000)

    it('should return the completePayment Json schema for the AMERICA EXPRESS card', () => {
      const visaPaymentSchema = {
        title: 'AMERICA EXPRESS payment',
        type: 'object',
        required: completedOrderSchema.requiredPropertiesHighLevel,
        properties: completedOrderSchema.completedOrderHighLevelSchema,
      }
      assert.jsonSchema(orderCompleted.body.completedOrder, visaPaymentSchema)
    })

    it('should return billing address object schema', () => {
      const visaPaymentBillingAddressObjectSchema = {
        title: 'AMERICA EXPRESS payment object billing address',
        type: 'object',
        required: completedOrderSchema.requiredPropertiesAddresses,
        properties: completedOrderSchema.completedOrderBillingAddressSchema,
      }
      assert.jsonSchema(
        orderCompleted.body.completedOrder.billingAddress,
        visaPaymentBillingAddressObjectSchema
      )
    })

    it('should return delivery address object schema', () => {
      const visaPaymentDeliveryAddressObjectSchema = {
        title: 'AMERICA EXPRESS payment object delivery address',
        type: 'object',
        required: completedOrderSchema.requiredPropertiesAddresses,
        properties: completedOrderSchema.completedOrderDeliveryAddressSchema,
      }
      assert.jsonSchema(
        orderCompleted.body.completedOrder.billingAddress,
        visaPaymentDeliveryAddressObjectSchema
      )
    })

    it('should return order lines schema', () => {
      orderCompleted.body.completedOrder.orderLines.forEach((obj) => {
        const visaPaymentOrderLinesSchema = {
          title: 'AMERICA EXPRESS payment object order lines',
          type: 'object',
          required: completedOrderSchema.requiredPropertiesOrderLines,
          properties: completedOrderSchema.completedOrderOrderLinesSchema,
        }
        assert.jsonSchema(obj, visaPaymentOrderLinesSchema)
      })
    })

    it('should return payment details schema', () => {
      orderCompleted.body.completedOrder.paymentDetails.forEach((obj) => {
        const visaPaymentPaymentDetailsSchema = {
          title: 'AMERICA EXPRESS payment object payment details',
          type: 'object',
          required: ['paymentMethod', 'cardNumberStar', 'totalCost'],
          properties: completedOrderSchema.completedOrderPaymentDetailsSchema(
            'American Express'
          ),
        }
        assert.jsonSchema(obj, visaPaymentPaymentDetailsSchema)
      })
    })

    it('should return currency conversion schema', () => {
      const visaPaymentCurrencyConversionSchema = {
        title: 'AMERICA EXPRESS payment object currency conversion',
        type: 'object',
        required: ['currencyRate'],
        properties: completedOrderSchema.completedOrderCurrencyConversionSchema(
          'GBP'
        ),
      }
      assert.jsonSchema(
        orderCompleted.body.completedOrder.currencyConversion,
        visaPaymentCurrencyConversionSchema
      )
    })
  })

  describe('can pay with ACCOUNT CARD', () => {
    let newAccount
    let shoppingBag
    let orderCompleted

    beforeAll(async () => {
      newAccount = await createAccount()
      shoppingBag = await addItemToShoppingBag(
        newAccount.jsessionid,
        products.productsSimpleId
      )

      orderCompleted = await payOrder(
        newAccount.jsessionid,
        shoppingBag.orderId,
        'ACCNT'
      )
    }, 60000)

    it('should return the completePayment Json schema for the ACCOUNT CARD', () => {
      const visaPaymentSchema = {
        title: 'ACCOUNT CARD payment',
        type: 'object',
        required: completedOrderSchema.requiredPropertiesHighLevel,
        properties: completedOrderSchema.completedOrderHighLevelSchema,
      }
      assert.jsonSchema(orderCompleted.body.completedOrder, visaPaymentSchema)
    })

    it('should return billing address object schema', () => {
      const visaPaymentBillingAddressObjectSchema = {
        title: 'ACCOUNT CARD payment object billing address',
        type: 'object',
        required: completedOrderSchema.requiredPropertiesAddresses,
        properties: completedOrderSchema.completedOrderBillingAddressSchema,
      }
      assert.jsonSchema(
        orderCompleted.body.completedOrder.billingAddress,
        visaPaymentBillingAddressObjectSchema
      )
    })

    it('should return delivery address object schema', () => {
      const visaPaymentDeliveryAddressObjectSchema = {
        title: 'ACCOUNT CARD payment object delivery address',
        type: 'object',
        required: completedOrderSchema.requiredPropertiesAddresses,
        properties: completedOrderSchema.completedOrderDeliveryAddressSchema,
      }
      assert.jsonSchema(
        orderCompleted.body.completedOrder.billingAddress,
        visaPaymentDeliveryAddressObjectSchema
      )
    })

    it('should return order lines schema', () => {
      orderCompleted.body.completedOrder.orderLines.forEach((obj) => {
        const visaPaymentOrderLinesSchema = {
          title: 'ACCOUNT CARD payment object order lines',
          type: 'object',
          required: completedOrderSchema.requiredPropertiesOrderLines,
          properties: completedOrderSchema.completedOrderOrderLinesSchema,
        }
        assert.jsonSchema(obj, visaPaymentOrderLinesSchema)
      })
    })

    it('should return payment details schema', () => {
      orderCompleted.body.completedOrder.paymentDetails.forEach((obj) => {
        const visaPaymentPaymentDetailsSchema = {
          title: 'ACCOUNT CARD payment object payment details',
          type: 'object',
          required: ['paymentMethod', 'cardNumberStar', 'totalCost'],
          properties: completedOrderSchema.completedOrderPaymentDetailsSchema(
            'Account Card'
          ),
        }
        assert.jsonSchema(obj, visaPaymentPaymentDetailsSchema)
      })
    })

    it('should return currency conversion schema', () => {
      const visaPaymentCurrencyConversionSchema = {
        title: 'ACCOUNT CARD payment object currency conversion',
        type: 'object',
        required: ['currencyRate'],
        properties: completedOrderSchema.completedOrderCurrencyConversionSchema(
          'GBP'
        ),
      }
      assert.jsonSchema(
        orderCompleted.body.completedOrder.currencyConversion,
        visaPaymentCurrencyConversionSchema
      )
    })
  })

  describe('can pay with PAYPAL', () => {
    let newAccount
    let shoppingBag
    let orderCompleted

    beforeAll(async () => {
      newAccount = await createAccount()
      shoppingBag = await addItemToShoppingBag(
        newAccount.jsessionid,
        products.productsSimpleId
      )
      orderCompleted = await payOrder(
        newAccount.jsessionid,
        shoppingBag.orderId,
        'PYPAL'
      )
    }, 60000)

    it('should return a successful server response 200 for PAYPAL', () => {
      expect(orderCompleted.statusCode).toBe(200)
    })
  })

  describe('can pay with MPASS', () => {
    let newAccount
    let shoppingBag
    let orderCompleted

    beforeAll(async () => {
      newAccount = await createAccount()
      shoppingBag = await addItemToShoppingBag(
        newAccount.jsessionid,
        products.productsSimpleId
      )

      orderCompleted = await payOrder(
        newAccount.jsessionid,
        shoppingBag.orderId,
        'MPASS'
      )
    }, 60000)

    it('should return a successful server response 200 for MPASS', () => {
      expect(orderCompleted.statusCode).toBe(200)
    })
  })
})
