import {
  booleanTypeAny,
  arrayType,
  numberType,
  objectType,
  stringType,
  stringTypeCanBeEmpty,
  stringTypePattern,
} from '../utilis'

const requiredPropertiesHighLevel = [
  'orderId',
  'subTotal',
  'returnPossible',
  'returnRequested',
  'deliveryMethod',
  'deliveryDate',
  'deliveryCost',
  'deliveryCarrier',
  'deliveryPrice',
  'totalOrderPrice',
  'totalOrdersDiscountLabel',
  'totalOrdersDiscount',
  'billingAddress',
  'deliveryAddress',
  'orderLines',
  'paymentDetails',
  'currencyConversion',
  'returning_buyer',
]

const completedOrderHighLevelSchema = {
  orderId: numberType,
  subTotal: stringType,
  returnPossible: booleanTypeAny,
  returnRequested: booleanTypeAny,
  deliveryMethod: stringType,
  deliveryDate: stringType,
  deliveryCost: stringType,
  deliveryCarrier: stringType,
  deliveryPrice: stringType,
  totalOrderPrice: stringType,
  totalOrdersDiscountLabel: stringTypeCanBeEmpty,
  totalOrdersDiscount: stringTypeCanBeEmpty,
  billingAddress: objectType,
  deliveryAddress: objectType,
  orderLines: arrayType(1),
  paymentDetails: arrayType(1),
  currencyConversion: objectType,
  returning_buyer: booleanTypeAny,
}

const requiredPropertiesAddresses = [
  'address1',
  'name',
  'country',
  'address2',
  'address3',
  'address4',
]

const completedOrderBillingAddressSchema = {
  address1: stringType,
  name: stringType,
  country: stringType,
  address2: stringType,
  address3: stringType,
  address4: stringType,
}

const completedOrderDeliveryAddressSchema = {
  address1: stringType,
  name: stringType,
  country: stringType,
  address2: stringType,
  address3: stringType,
  address4: stringType,
}

const requiredPropertiesOrderLines = [
  'lineNo',
  'name',
  'size',
  'colour',
  'imageUrl',
  'quantity',
  'unitPrice',
  'discount',
  'discountPrice',
  'total',
  'nonRefundable',
]
const completedOrderOrderLinesSchema = {
  lineNo: stringType,
  name: stringType,
  size: stringType,
  colour: stringType,
  imageUrl: stringType,
  quantity: numberType,
  unitPrice: stringType,
  discount: stringTypeCanBeEmpty,
  discountPrice: stringType,
  total: stringType,
  nonRefundable: booleanTypeAny,
}

const completedOrderPaymentDetailsSchema = (cardType) => {
  return {
    paymentMethod: stringTypePattern(cardType),
    cardNumberStar: stringType,
    totalCost: stringType,
  }
}

const completedOrderCurrencyConversionSchema = (currency) => {
  return {
    currencyRate: stringTypePattern(currency),
  }
}

export default {
  requiredPropertiesHighLevel,
  completedOrderHighLevelSchema,
  requiredPropertiesAddresses,
  completedOrderBillingAddressSchema,
  completedOrderDeliveryAddressSchema,
  requiredPropertiesOrderLines,
  completedOrderOrderLinesSchema,
  completedOrderPaymentDetailsSchema,
  completedOrderCurrencyConversionSchema,
}
