require('babel-polyfill')

import chai from 'chai'

chai.use(require('chai-json-schema'))

const assert = chai.assert

import {
  booleanType,
  numberType,
  stringTypePattern,
  arrayType,
  stringType,
  objectType,
  nullType,
  numberTypePattern,
  booleanTypeAny,
} from '../utilis'
import {
  itemWishlist,
  userCredentialsWishlist,
  wishlistName,
} from './wishlist-data'
import { createAccount, logInWithJsessionId } from '../utilis/userAccount'
import { removeItemFromShoppingBag } from '../utilis/shoppingBag'
import {
  retrieveWishlist,
  createWishlist,
  addItemToWishlist,
  addItemToBagFromWishlist,
  removeItemFromWishlist,
} from '../utilis/wishlist'

let wishlistItemId
let newUserAccount
let login

describe('It should return the wishlist Json Schema', () => {
  describe('Wishlist Creation Schema', () => {
    let response
    beforeAll(async () => {
      newUserAccount = await createAccount()
      response = await createWishlist(newUserAccount.jsessionid)
    }, 30000)

    it(
      'Wishlist creation Schema',
      () => {
        const wishlistCreationSchema = {
          title: 'Wishlist creation Schema',
          type: 'object',
          required: [
            'success',
            'externalId',
            'giftListName',
            'giftListId',
            'storeId',
          ],
          properties: {
            success: booleanType(true),
            externalId: numberType,
            giftListName: stringTypePattern(wishlistName),
            giftListId: numberType,
            storeId: numberType,
          },
        }
        assert.jsonSchema(response, wishlistCreationSchema)
      },
      30000
    )
  })

  describe('Add Item to Wishlist Schema', () => {
    let response
    beforeAll(async () => {
      login = await logInWithJsessionId(
        userCredentialsWishlist.username,
        userCredentialsWishlist.password
      )
      response = await addItemToWishlist(login.jsessionid)
    }, 30000)

    it(
      'Add item to wishlist Schema',
      () => {
        const addItemToWishlistSchema = {
          title: 'Wishlist add item Schema',
          type: 'object',
          required: [
            'productList',
            'pageNo',
            'name',
            'noOfItemsInList',
            'type',
            'pageSize',
            'giftListId',
            'success',
            'message',
          ],
          optional: ['isDDPOrder'],
          properties: {
            productList: arrayType(1),
            pageNo: numberType,
            name: stringType,
            noOfItemsInList: numberType,
            type: stringType,
            pageSize: numberType,
            giftListId: numberType,
            success: booleanType(true),
            message: stringType,
            isDDPOrder: booleanTypeAny,
          },
        }
        assert.jsonSchema(response, addItemToWishlistSchema)
      },
      30000
    )

    it(
      'Add Item => Product List schema',
      () => {
        response.productList.forEach((productList) => {
          const productListSchema = {
            title: 'Product List Schema',
            type: 'object',
            required: ['productId', 'giftListItemId'],
            properties: {
              productId: numberType,
              giftListItemId: numberType,
            },
          }
          assert.jsonSchema(productList, productListSchema)
        })
      },
      30000
    )
  })

  describe('Get Wishlist Schema', () => {
    let response
    beforeAll(async () => {
      login = await logInWithJsessionId(
        userCredentialsWishlist.username,
        userCredentialsWishlist.password
      )
      response = await retrieveWishlist(login.jsessionid)
    }, 30000)

    it(
      'Get Wishlist Schema',
      () => {
        const wishlistCreationSchema = {
          title: 'Get wishlist Schema',
          type: 'object',
          required: [
            'pageSize',
            'pageNo',
            'name',
            'giftListId',
            'type',
            'itemDetails',
            'noOfItemsInList',
          ],
          properties: {
            pageSize: numberType,
            pageNo: numberType,
            name: stringType,
            giftListId: numberType,
            type: stringType,
            itemDetails: arrayType(1),
            noOfItemsInList: numberType,
          },
        }
        assert.jsonSchema(response, wishlistCreationSchema)
      },
      30000
    )

    it(
      'Get wishlist => Item Details schema',
      () => {
        wishlistItemId = response.itemDetails[0].listItemId
        response.itemDetails.forEach((itemDetails) => {
          const productDetailsSchema = {
            title: 'Item Details Schema',
            type: 'object',
            required: [
              'parentProductId',
              'size',
              'sizeAndQuantity',
              'avlQuantity',
              'productImageUrl',
              'outfitBaseImageUrl',
              'price',
              'quantity',
              'listItemId',
              'productImage',
              'colour',
              'colourKey',
              'productBaseImageUrl',
              'catEntryId',
              'outfitImageUrl',
              'outfitImage',
              'title',
              'rating',
              'sizeKey',
              'assets',
            ],
            properties: {
              parentProductId: stringType,
              size: stringType,
              sizeAndQuantity: arrayType(1),
              avlQuantity: numberType,
              productImageUrl: stringType,
              outfitBaseImageUrl: stringType,
              price: objectType,
              quantity: stringType,
              listItemId: stringType,
              colour: stringType,
              colourKey: stringType,
              productBaseImageUrl: stringType,
              catEntryId: stringTypePattern(itemWishlist.catEntryId),
              outfitImageUrl: stringType,
              outfitImage: stringType,
              title: stringType,
              rating: nullType,
              sizeKey: stringType,
              assets: arrayType(1),
            },
          }
          assert.jsonSchema(itemDetails, productDetailsSchema)
        })
      },
      30000
    )

    it(
      'Get wishlist => Item Details => sizeAndQuantity schema',
      () => {
        response.itemDetails.forEach((itemDetails) => {
          itemDetails.sizeAndQuantity.forEach((sizeAndQuantity) => {
            const sizeAndQuantitySchema = {
              title: 'sizeAndQuantity schema',
              type: 'object',
              required: ['size', 'catentryId', 'quantity'],
              properties: {
                size: stringType,
                catentryId: numberType,
                quantity: numberType,
              },
            }
            assert.jsonSchema(sizeAndQuantity, sizeAndQuantitySchema)
          })
        })
      },
      30000
    )

    it(
      'Get wishlist => Item Details => Price schema',
      () => {
        response.itemDetails.forEach((itemDetails) => {
          const price = itemDetails.price
          const priceSchema = {
            title: 'Price Schema',
            type: 'object',
            required: ['was2Price', 'was1Price', 'nowPrice'],
            properties: {
              was2Price: nullType,
              was1Price: nullType,
              nowPrice: stringType,
            },
          }
          assert.jsonSchema(price, priceSchema)
        })
      },
      30000
    )
  })

  describe('Add Item to Bag Schema', () => {
    let response
    beforeAll(async () => {
      login = await logInWithJsessionId(
        userCredentialsWishlist.username,
        userCredentialsWishlist.password
      )
      response = await addItemToBagFromWishlist(login.jsessionid)
    }, 30000)

    // Removing the item from the shopping bag to avoid making it full for the given size.
    afterEach(async () => {
      await removeItemFromShoppingBag(
        login.jsessionid,
        response.orderId,
        response.products[0].orderItemId
      )
    }, 30000)

    it(
      'Add item to Bag Schema',
      () => {
        const addItemToWishlistSchema = {
          title: 'Wishlist add item to shopping bag Schema',
          type: 'object',

          required: [
            'orderId',
            'subTotal',
            'total',
            'totalBeforeDiscount',
            'deliveryOptions',
            'promotions',
            'discounts',
            'products',
            'savedProducts',
            'ageVerificationRequired',
            'restrictedDeliveryItem',
            'inventoryPositions',
            'shoppingBagTotalEspot',
          ],
          properties: {
            orderId: numberType,
            subTotal: stringType,
            total: stringType,
            totalBeforeDiscount: stringType,
            deliveryOptions: arrayType(1),
            promotions: arrayType(0),
            discounts: arrayType(0),
            products: arrayType(1),
            savedProducts: arrayType(0),
            ageVerificationRequired: booleanType(false),
            restrictedDeliveryItem: booleanType(false),
            inventoryPositions: objectType,
            shoppingBagTotalEspot: objectType,
          },
        }
        assert.jsonSchema(response, addItemToWishlistSchema)
      },
      30000
    )
    // The schema for inner properties of the Shopping Bag has been already tested in shopping-bag section.
  })

  describe('Remove Item from Wishlist Schema', () => {
    let response
    beforeAll(async () => {
      login = await logInWithJsessionId(
        userCredentialsWishlist.username,
        userCredentialsWishlist.password
      )
      response = await removeItemFromWishlist(login.jsessionid, wishlistItemId)
    }, 30000)

    it(
      'Remove item from wishlist Schema',
      () => {
        const addItemToWishlistSchema = {
          title: 'Wishlist remove item Schema',
          type: 'object',
          required: [
            'success',
            'externalId',
            'giftListId',
            'giftListItemId',
            'storeId',
          ],
          properties: {
            success: booleanType(true),
            externalId: arrayType(1, true, 'string'),
            giftListId: arrayType(1, true, 'string'),
            giftListItemId: arrayType(1, true, 'string'),
            storeId: arrayType(1, true, 'string'),
          },
        }
        assert.jsonSchema(response, addItemToWishlistSchema)
      },
      30000
    )
  })

  describe('Get Empty Wishlist Schema', () => {
    let response
    beforeAll(async () => {
      login = await logInWithJsessionId(
        userCredentialsWishlist.username,
        userCredentialsWishlist.password
      )

      response = await retrieveWishlist(login.jsessionid)
    }, 30000)

    it(
      'Get Empty Wishlist Schema',
      () => {
        const emptyWishlistSchema = {
          title: 'Get wishlist Schema',
          type: 'object',
          required: [
            'pageSize',
            'pageNo',
            'name',
            'giftListId',
            'type',
            'itemDetails',
            'noOfItemsInList',
          ],
          properties: {
            pageSize: numberType,
            pageNo: numberType,
            name: stringType,
            giftListId: numberType,
            type: stringType,
            itemDetails: arrayType(0),
            noOfItemsInList: numberTypePattern(0, 0),
          },
        }
        assert.jsonSchema(response, emptyWishlistSchema)
      },
      30000
    )
  })
})
