/* eslint-disable no-await-in-loop */
require('babel-polyfill')

jest.unmock('superagent')
import superagent from 'superagent'
import eps from '../routes_tests'
import { headers } from '../utilis'

export const getCmsPageName = async (pageName) => {
  const cmsPageBody = await superagent
    .get(eps.cmsPageName.path(pageName))
    .set(headers)
  return cmsPageBody
}

export const getCmsSeo = async (pageName) => {
  const cmsSeoBody = await superagent
    .get(eps.cmsSeo.path(pageName))
    .set(headers)
  return cmsSeoBody
}
