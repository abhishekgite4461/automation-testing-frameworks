require('babel-polyfill')

jest.unmock('superagent')
import superagent from 'superagent'
import chai from 'chai'
import eps from '../routes_tests'
import { headers, stringTypePattern, booleanType } from '../utilis'
import { emailMeInStockRequest } from './email-me-stock-data'

chai.use(require('chai-json-schema'))

const assert = chai.assert

describe('It should return the Email Me In Stock Json Schema', () => {
  let response
  beforeAll(async () => {
    response = await superagent
      .get(eps.emailMeInStock.path)
      .set(headers)
      .query(emailMeInStockRequest)
  })

  it('Email Me In Stock Schema', () => {
    const body = response.body
    const emailMeInStockSchema = {
      title: 'Email Me In Stock Schema',
      type: 'object',
      required: ['success', 'action', 'message'],
      properties: {
        success: booleanType(true),
        action: stringTypePattern(`NotifyMe`),
        message: stringTypePattern(
          `Your request has been received, and you will receive an email when the item is in stock.`
        ),
      },
    }
    assert.jsonSchema(body, emailMeInStockSchema)
  })
})
