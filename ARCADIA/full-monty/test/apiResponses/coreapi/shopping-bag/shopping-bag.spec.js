require('babel-polyfill')

import chai from 'chai'

chai.use(require('chai-json-schema'))

const assert = chai.assert

import {
  stringType,
  stringTypeEmpty,
  stringTypePattern,
  numberType,
  numberTypePattern,
  booleanType,
  booleanTypeAny,
  objectType,
  arrayType,
  booleanOrNull,
} from '../utilis'
import {
  deliveryOptions,
  addItemToShoppingBag,
  removeItemFromShoppingBag,
  fetchSizeQty,
  updateShoppingBagItem,
  updateShoppingBagDelivery,
  promotionCode,
  deletePromotionCode,
  PROMOTION_CODE,
} from '../utilis/shoppingBag'
import { createAccount } from '../utilis/userAccount'
import { getProducts } from '../utilis/selectProducts'

describe('It should return the Shopping Bag Json Schema', () => {
  let products
  let newAccount
  let shoppingBag
  let productSizeQty
  let addPromotionCode
  beforeAll(async () => {
    products = await getProducts()
    newAccount = await createAccount()
    await addItemToShoppingBag(newAccount.jsessionid, products.productsSimpleId)
    shoppingBag = await addItemToShoppingBag(
      newAccount.jsessionid,
      products.productsWasPriceId
    )
    productSizeQty = await fetchSizeQty(
      newAccount.jsessionid,
      products.productsSimpleId.catEntry
    )
    addPromotionCode = await promotionCode(newAccount.jsessionid)
  }, 60000)

  describe('Shopping Bag GET Products', () => {
    it('Shopping Bag GET Product Json Schema', () => {
      const shoppingBagSchema = {
        title: 'Shopping Bag GET Product Json Schema',
        type: 'object',
        required: [
          'ageVerificationRequired',
          'deliveryOptions',
          'discounts',
          'inventoryPositions',
          'orderId',
          'products',
          'promotions',
          'restrictedDeliveryItem',
          'savedProducts',
          'subTotal',
          'total',
          'totalBeforeDiscount',
        ],
        optional: ['isDDPOrder', 'isDDPUser', 'isDDPProduct'],
        properties: {
          ageVerificationRequired: booleanType(false),
          deliveryOptions: arrayType(),
          discounts: arrayType(),
          inventoryPositions: objectType,
          isDDPOrder: booleanTypeAny,
          orderId: numberType,
          products: arrayType(1),
          promotions: arrayType(),
          restrictedDeliveryItem: booleanType(false),
          savedProducts: arrayType(),
          subTotal: stringType,
          total: stringType,
          totalBeforeDiscount: stringType,
          isDDPUser: booleanOrNull,
          isDDPProduct: booleanTypeAny,
        },
      }
      assert.jsonSchema(shoppingBag, shoppingBagSchema)
    })

    it('Shopping Bag GET Product deliveryOptions Json Schema', () => {
      shoppingBag.deliveryOptions.forEach((deliveryOption) => {
        const shoppingBagDeliveryOptionsSchema = {
          title: 'Shopping Bag GET Product deliveryOptions Json Schema',
          type: 'object',
          required: [
            'deliveryOptionExternalId',
            'deliveryOptionId',
            'enabled',
            'label',
            'selected',
          ],
          properties: {
            deliveryOptionId: numberType,
            label: stringType,
            selected: {
              type: 'boolean',
              enum: deliveryOption.label.includes('UK Standard')
                ? [true]
                : [false],
            },
            deliveryOptionExternalId: stringType,
            enabled: {
              type: 'boolean',
              enum: deliveryOption.label.includes('Collect From Store Today')
                ? [false]
                : [true],
            },
          },
        }
        assert.jsonSchema(deliveryOption, shoppingBagDeliveryOptionsSchema)
      })
    })

    it('Shopping Bag GET Product products Json Schema', () => {
      shoppingBag.products.forEach((product) => {
        if (product.productId === products.productsSimpleId.productId) {
          const shoppingBagSimpleProductsSchema = {
            title: 'Shopping Bag GET Product products Json Schema',
            type: 'object',
            required: [
              'ageVerificationRequired',
              'assets',
              'attributes',
              'bundleProducts',
              'bundleSlots',
              'catEntryId',
              'colourSwatches',
              'discountText',
              'freeItem',
              'inStock',
              'isBundleOrOutfit',
              'items',
              'lineNumber',
              'lowStock',
              'name',
              'orderItemId',
              'productId',
              'quantity',
              'shipModeId',
              'size',
              'totalPrice',
              'tpmLinks',
              'unitPrice',
            ],
            properties: {
              ageVerificationRequired: booleanType(false),
              assets: arrayType(4),
              attributes: objectType,
              bundleProducts: arrayType(),
              bundleSlots: arrayType(),
              catEntryId: numberType,
              colourSwatches: arrayType(),
              discountText: stringTypeEmpty,
              freeItem: booleanTypeAny,
              inStock: booleanType(true),
              isBundleOrOutfit: booleanType(false),
              items: arrayType(),
              lineNumber: stringType,
              lowStock: booleanType(false),
              name: stringType,
              orderItemId: numberType,
              productId: numberType,
              quantity: numberType,
              shipModeId: numberType,
              size: stringType,
              totalPrice: numberType,
              tpmLinks: arrayType(),
              unitPrice: stringType,
            },
          }
          assert.jsonSchema(product, shoppingBagSimpleProductsSchema)
        } else if (
          product.productId === products.productsWasPriceId.productId
        ) {
          const shoppingBagProductsWasPriceSchema = {
            title: 'Shopping Bag Product products Json Schema',
            type: 'object',
            required: [
              'ageVerificationRequired',
              'assets',
              'attributes',
              'bundleProducts',
              'bundleSlots',
              'catEntryId',
              'colourSwatches',
              'freeItem',
              'inStock',
              'isBundleOrOutfit',
              'items',
              'lineNumber',
              'lowStock',
              'name',
              'orderItemId',
              'productId',
              'quantity',
              'shipModeId',
              'size',
              'totalPrice',
              'tpmLinks',
              'unitPrice',
              'wasPrice',
            ],
            properties: {
              ageVerificationRequired: booleanType(false),
              assets: arrayType(4),
              attributes: objectType,
              bundleProducts: arrayType(),
              bundleSlots: arrayType(),
              catEntryId: numberType,
              colourSwatches: arrayType(),
              freeItem: booleanTypeAny,
              inStock: booleanType(true),
              isBundleOrOutfit: booleanType(false),
              items: arrayType(),
              lineNumber: stringType,
              lowStock: booleanType(false),
              name: stringType,
              orderItemId: numberType,
              productId: numberType,
              quantity: numberType,
              shipModeId: numberType,
              size: stringType,
              totalPrice: numberType,
              tpmLinks: arrayType(),
              unitPrice: stringType,
              wasPrice: stringType,
            },
          }
          assert.jsonSchema(product, shoppingBagProductsWasPriceSchema)
        }
      })
    })

    it('Shopping Bag GET Product products assets Json Schema', () => {
      shoppingBag.products.forEach((prod) => {
        prod.assets.forEach((asset) => {
          const shoppingBagProductsSchema = {
            title: 'Shopping Bag GET Product products assets Json Schema',
            type: 'object',
            required: ['assetType', 'index', 'url'],
            properties: {
              assetType: stringType,
              index: numberType,
              url: stringType,
            },
          }
          assert.jsonSchema(asset, shoppingBagProductsSchema)
        })
      })
    })

    it('Shopping Bag Simple Product inventoryPositions Json Schema', () => {
      const obj = shoppingBag.inventoryPositions
      let inv
      Object.keys(obj).forEach((props) => {
        if (obj[props].invavls) {
          inv = obj[props].invavls
        } else {
          inv = obj[props].inventorys
        }
        const shoppingBagInventoryPositionsProps = {
          partNumber: obj[props].partNumber,
          catentryId: obj[props].catentryId,
          inv,
        }
        let shoppingBagInventoryPositionsSchema
        if (inv !== null) {
          shoppingBagInventoryPositionsSchema = {
            title: 'Shopping Bag Simple Product inventoryPositions Json Schema',
            type: 'object',
            required: ['partNumber', 'catentryId', 'inv'],
            properties: {
              partNumber: stringType,
              catEntryId: numberType,
              inv: {
                type: 'array',
                minItems: 1,
                uniqueItems: true,
                items: { type: 'object' },
              },
            },
          }
        } else {
          shoppingBagInventoryPositionsSchema = {
            title: 'Shopping Bag Simple Product inventoryPositions Json Schema',
            type: 'object',
            required: ['partNumber', 'catentryId', 'inv'],
            properties: {
              partNumber: stringType,
              catEntryId: numberType,
            },
          }
        }
        assert.jsonSchema(
          shoppingBagInventoryPositionsProps,
          shoppingBagInventoryPositionsSchema
        )
      })
    })

    it('Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema', () => {
      const obj = shoppingBag.inventoryPositions
      Object.keys(obj).forEach((props) => {
        if (obj[props].invavls) {
          obj[props].invavls.forEach((prop) => {
            const shoppingBagInvAvlsPositionsProps = {
              cutofftime: prop.cutofftime,
              quantity: prop.quantity,
              stlocIdentifier: prop.stlocIdentifier,
              expressdates: prop.expressdates,
            }
            const shoppingBagInvAvlsPositionsSchema = {
              title:
                'Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
              type: 'object',
              required: [
                'cutofftime',
                'quantity',
                'stlocIdentifier',
                'expressdates',
              ],
              properties: {
                cutofftime: stringType,
                quantity: numberType,
                stlocIdentifier: stringType,
                expressdates: {
                  type: 'array',
                  minItems: 2,
                  uniqueItems: true,
                  items: stringType,
                },
              },
            }
            assert.jsonSchema(
              shoppingBagInvAvlsPositionsProps,
              shoppingBagInvAvlsPositionsSchema
            )
          })
        } else if (obj[props].inventorys) {
          obj[props].inventorys.forEach((prop) => {
            const shoppingBagInventorysPositionsProps = {
              cutofftime: prop.cutofftime,
              quantity: prop.quantity,
              ffmcenterId: prop.ffmcenterId,
              expressdates: prop.expressdates,
            }
            const shoppingBagInventorysPositionsSchema = {
              title:
                'Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
              type: 'object',
              required: [
                'cutofftime',
                'quantity',
                'ffmcenterId',
                'expressdates',
              ],
              properties: {
                cutofftime: stringType,
                quantity: numberType,
                ffmcenterId: numberType,
                expressdates: {
                  type: 'array',
                  minItems: 2,
                  uniqueItems: true,
                  items: stringType,
                },
              },
            }
            assert.jsonSchema(
              shoppingBagInventorysPositionsProps,
              shoppingBagInventorysPositionsSchema
            )
          })
        }
      })
    })

    it('Shopping Bag Simple Product inventoryPositions => inventories && invavls => ExpressDates Properties Values', () => {
      const obj = shoppingBag.inventoryPositions
      Object.keys(obj).forEach((props) => {
        if (obj[props].invavls) {
          obj[props].invavls.forEach((prop) => {
            const expressdateSchema = {
              title: 'Express date in inventory item',
              type: 'array',
              required: ['0', '1'],
              properties: {
                0: stringType,
                1: stringType,
              },
            }
            assert.jsonSchema(prop.expressdates, expressdateSchema)
          })
        } else if (obj[props].inventorys) {
          obj[props].inventorys.forEach((prop) => {
            const expressdateSchema = {
              title: 'Express date in inventory item',
              type: 'array',
              required: ['0', '1'],
              properties: {
                0: stringType,
                1: stringType,
              },
            }
            assert.jsonSchema(prop.expressdates, expressdateSchema)
          })
        }
      })
    })
  })
  describe('Shopping Bag POST Product with wasPrice', () => {
    it('Shopping Bag POST Product Json Schema', () => {
      const shoppingBagSchema = {
        title: 'Shopping Bag Product Json Schema',
        type: 'object',
        required: [
          'ageVerificationRequired',
          'deliveryOptions',
          'discounts',
          'inventoryPositions',
          'orderId',
          'products',
          'promotions',
          'restrictedDeliveryItem',
          'savedProducts',
          'subTotal',
          'total',
          'totalBeforeDiscount',
        ],
        optional: ['isDDPOrder', 'isDDPProduct'],
        properties: {
          ageVerificationRequired: booleanType(false),
          deliveryOptions: arrayType(5),
          discounts: arrayType(),
          inventoryPositions: objectType,
          isDDPOrder: booleanTypeAny,
          orderId: numberType,
          products: arrayType(1),
          promotions: arrayType(),
          restrictedDeliveryItem: booleanType(false),
          savedProducts: arrayType(),
          subTotal: stringType,
          total: stringType,
          totalBeforeDiscount: stringType,
          isDDPProduct: booleanTypeAny,
        },
      }
      assert.jsonSchema(shoppingBag, shoppingBagSchema)
    })

    it('Shopping Bag POST Product deliveryOptions Json Schema', () => {
      shoppingBag.deliveryOptions.forEach((deliveryOptions) => {
        const shoppingBagDeliveryOptionsSchema = {
          title: 'Shopping Bag POST Product deliveryOptions Json Schema',
          type: 'object',
          required: [
            'deliveryOptionId',
            'label',
            'deliveryOptionExternalId',
            'selected',
            'enabled',
          ],
          properties: {
            deliveryOptionId: numberType,
            label: stringType,
            deliveryOptionExternalId: stringType,
            selected: {
              type: 'boolean',
              enum: deliveryOptions.label.includes('UK Standard ')
                ? [true]
                : [false],
            },
            enabled: {
              type: 'boolean',
              enum: deliveryOptions.label.includes('Collect From Store Today')
                ? [false]
                : [true],
            },
          },
        }
        assert.jsonSchema(deliveryOptions, shoppingBagDeliveryOptionsSchema)
      })
    })

    it('Shopping Bag POST Product products Json Schema', () => {
      const shoppingBagProductsSchema = {
        title: 'Shopping Bag POST Product products Json Schema',
        type: 'object',
        required: [
          'ageVerificationRequired',
          'assets',
          'attributes',
          'bundleProducts',
          'bundleSlots',
          'catEntryId',
          'colourSwatches',
          'discountText',
          'inStock',
          'isBundleOrOutfit',
          'items',
          'lineNumber',
          'lowStock',
          'name',
          'orderItemId',
          'productId',
          'quantity',
          'shipModeId',
          'size',
          'totalPrice',
          'tpmLinks',
          'unitPrice',
          'wasPrice',
        ],
        optional: ['isDDPOrder', 'isDDPProduct'],
        properties: {
          ageVerificationRequired: booleanType(false),
          assets: arrayType(4),
          attributes: objectType,
          bundleProducts: arrayType(),
          bundleSlots: arrayType(),
          catEntryId: numberType,
          colourSwatches: arrayType(),
          discountText: stringTypeEmpty,
          inStock: booleanType(true),
          isBundleOrOutfit: booleanType(false),
          isDDPProduct: booleanTypeAny,
          isDDPOrder: booleanTypeAny,
          items: arrayType(),
          lineNumber: stringType,
          lowStock: booleanType(false),
          name: stringType,
          orderItemId: numberType,
          productId: numberType,
          quantity: numberType,
          shipModeId: numberType,
          size: stringType,
          totalPrice: numberType,
          tpmLinks: arrayType(),
          unitPrice: stringType,
          wasPrice: stringType,
        },
      }
      assert.jsonSchema(shoppingBag.products[1], shoppingBagProductsSchema)
    })

    it('Shopping Bag POST Product products assets Json Schema', () => {
      shoppingBag.products.forEach((prod) => {
        prod.assets.forEach((asset) => {
          const shoppingBagProductsSchema = {
            title: 'Shopping Bag POST Product products assets Json Schema',
            type: 'object',
            required: ['assetType', 'index', 'url'],
            properties: {
              assetType: stringType,
              index: numberType,
              url: stringType,
            },
          }
          assert.jsonSchema(asset, shoppingBagProductsSchema)
        })
      })
    })

    it('Shopping Bag Simple Product inventoryPositions Json Schema', () => {
      const obj = shoppingBag.inventoryPositions
      let inv
      Object.keys(obj).forEach((props) => {
        if (obj[props].invavls) {
          inv = obj[props].invavls
        } else {
          inv = obj[props].inventorys
        }
        const shoppingBagInventoryPositionsProps = {
          partNumber: obj[props].partNumber,
          catentryId: obj[props].catentryId,
          inv,
        }
        let shoppingBagInventoryPositionsSchema
        if (inv !== null) {
          shoppingBagInventoryPositionsSchema = {
            title: 'Shopping Bag Simple Product inventoryPositions Json Schema',
            type: 'object',
            required: ['partNumber', 'catentryId', 'inv'],
            properties: {
              partNumber: stringType,
              catEntryId: numberType,
              inv: {
                type: 'array',
                minItems: 1,
                uniqueItems: true,
                items: { type: 'object' },
              },
            },
          }
        } else {
          shoppingBagInventoryPositionsSchema = {
            title: 'Shopping Bag Simple Product inventoryPositions Json Schema',
            type: 'object',
            required: ['partNumber', 'catentryId', 'inv'],
            properties: {
              partNumber: numberType,
              catEntryId: numberType,
            },
          }
        }
        assert.jsonSchema(
          shoppingBagInventoryPositionsProps,
          shoppingBagInventoryPositionsSchema
        )
      })
    })

    it('Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema', () => {
      const obj = shoppingBag.inventoryPositions
      Object.keys(obj).forEach((props) => {
        if (obj[props].invavls) {
          obj[props].invavls.forEach((prop) => {
            const shoppingBagInvAvlsPositionsProps = {
              cutofftime: prop.cutofftime,
              quantity: prop.quantity,
              stlocIdentifier: prop.stlocIdentifier,
              expressdates: prop.expressdates,
            }
            const shoppingBagInvAvlsPositionsSchema = {
              title:
                'Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
              type: 'object',
              required: [
                'cutofftime',
                'quantity',
                'stlocIdentifier',
                'expressdates',
              ],
              properties: {
                cutofftime: stringType,
                quantity: numberType,
                stlocIdentifier: stringType,
                expressdates: {
                  type: 'array',
                  minItems: 2,
                  uniqueItems: true,
                  items: stringType,
                },
              },
            }
            assert.jsonSchema(
              shoppingBagInvAvlsPositionsProps,
              shoppingBagInvAvlsPositionsSchema
            )
          })
        } else if (obj[props].inventorys) {
          obj[props].inventorys.forEach((prop) => {
            const shoppingBagInventorysPositionsProps = {
              cutofftime: prop.cutofftime,
              quantity: prop.quantity,
              ffmcenterId: prop.ffmcenterId,
              expressdates: prop.expressdates,
            }
            const shoppingBagInventorysPositionsSchema = {
              title:
                'Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
              type: 'object',
              required: [
                'cutofftime',
                'quantity',
                'ffmcenterId',
                'expressdates',
              ],
              properties: {
                cutofftime: stringType,
                quantity: numberType,
                ffmcenterId: numberType,
                expressdates: {
                  type: 'array',
                  minItems: 2,
                  uniqueItems: true,
                  items: stringType,
                },
              },
            }
            assert.jsonSchema(
              shoppingBagInventorysPositionsProps,
              shoppingBagInventorysPositionsSchema
            )
          })
        }
      })
    })

    it('Shopping Bag Simple Product inventoryPositions => inventories && invavls => ExpressDates Properties Values', () => {
      const obj = shoppingBag.inventoryPositions
      Object.keys(obj).forEach((props) => {
        if (obj[props].invavls) {
          obj[props].invavls.forEach((prop) => {
            const expressdateSchema = {
              title: 'Express date in inventory item',
              type: 'array',
              required: ['0', '1'],
              properties: {
                0: stringType,
                1: stringType,
              },
            }
            assert.jsonSchema(prop.expressdates, expressdateSchema)
          })
        } else if (obj[props].inventorys) {
          obj[props].inventorys.forEach((prop) => {
            const expressdateSchema = {
              title: 'Express date in inventory item',
              type: 'array',
              required: ['0', '1'],
              properties: {
                0: stringType,
                1: stringType,
              },
            }
            assert.jsonSchema(prop.expressdates, expressdateSchema)
          })
        }
      })
    })
  })

  describe('Shopping Bag POST Product Simple', () => {
    it('Shopping Bag POST Product Json Schema', () => {
      const shoppingBagSchema = {
        title: 'Shopping Bag Product Json Schema',
        type: 'object',
        required: [
          'ageVerificationRequired',
          'deliveryOptions',
          'discounts',
          'inventoryPositions',
          'orderId',
          'products',
          'promotions',
          'restrictedDeliveryItem',
          'savedProducts',
          'subTotal',
          'total',
          'totalBeforeDiscount',
        ],
        optional: ['isDDPOrder', 'isDDPProduct'],
        properties: {
          ageVerificationRequired: booleanType(false),
          deliveryOptions: arrayType(5),
          discounts: arrayType(),
          inventoryPositions: objectType,
          isDDPProduct: booleanTypeAny,
          isDDPOrder: booleanTypeAny,
          orderId: numberType,
          products: arrayType(1),
          promotions: arrayType(),
          restrictedDeliveryItem: booleanType(false),
          savedProducts: arrayType(),
          subTotal: stringType,
          total: stringType,
          totalBeforeDiscount: stringType,
        },
      }
      assert.jsonSchema(shoppingBag, shoppingBagSchema)
    })

    it('Shopping Bag POST Product deliveryOptions Json Schema', () => {
      shoppingBag.deliveryOptions.forEach((deliveryOptions) => {
        const shoppingBagDeliveryOptionsSchema = {
          title: 'Shopping Bag POST Product deliveryOptions Json Schema',
          type: 'object',
          required: [
            'deliveryOptionExternalId',
            'deliveryOptionId',
            'enabled',
            'label',
            'selected',
          ],
          properties: {
            deliveryOptionId: numberType,
            label: stringType,
            deliveryOptionExternalId: stringType,
            selected: {
              type: 'boolean',
              enum: deliveryOptions.label.includes('UK Standard')
                ? [true]
                : [false],
            },
            enabled: {
              type: 'boolean',
              enum: deliveryOptions.label.includes('Collect From Store Today')
                ? [false]
                : [true],
            },
          },
        }
        assert.jsonSchema(deliveryOptions, shoppingBagDeliveryOptionsSchema)
      })
    })

    it('Shopping Bag POST Product products Json Schema', () => {
      shoppingBag.products.forEach((product) => {
        const shoppingBagProductsSchema = {
          title: 'Shopping Bag POST Product products Json Schema',
          type: 'object',
          required: [
            'ageVerificationRequired',
            'assets',
            'attributes',
            'bundleProducts',
            'bundleSlots',
            'catEntryId',
            'colourSwatches',
            'discountText',
            'inStock',
            'isBundleOrOutfit',
            'items',
            'lineNumber',
            'lowStock',
            'name',
            'orderItemId',
            'productId',
            'quantity',
            'shipModeId',
            'size',
            'totalPrice',
            'tpmLinks',
            'unitPrice',
          ],
          optional: ['isDDPOrder', 'isDDPProduct'],
          properties: {
            ageVerificationRequired: booleanType(false),
            assets: arrayType(4),
            attributes: objectType,
            bundleProducts: arrayType(),
            bundleSlots: arrayType(),
            catEntryId: numberType,
            colourSwatches: arrayType(),
            discountText: stringTypeEmpty,
            inStock: booleanType(true),
            isBundleOrOutfit: booleanType(false),
            isDDPProduct: booleanTypeAny,
            isDDPOrder: booleanTypeAny,
            items: arrayType(),
            lineNumber: stringType,
            lowStock: booleanType(false),
            name: stringType,
            orderItemId: numberType,
            productId: numberType,
            quantity: numberType,
            shipModeId: numberType,
            size: stringType,
            totalPrice: numberType,
            tpmLinks: arrayType(),
            unitPrice: stringType,
          },
        }
        assert.jsonSchema(product, shoppingBagProductsSchema)
      })
    })

    it('Shopping Bag POST Product products assets Json Schema', () => {
      shoppingBag.products.forEach((prod) => {
        prod.assets.forEach((asset) => {
          const shoppingBagProductsSchema = {
            title: 'Shopping Bag POST Product products assets Json Schema',
            type: 'object',
            required: ['assetType', 'index', 'url'],
            properties: {
              assetType: stringType,
              index: numberType,
              url: stringType,
            },
          }
          assert.jsonSchema(asset, shoppingBagProductsSchema)
        })
      })
    })

    it('Shopping Bag Simple Product inventoryPositions Json Schema', () => {
      const obj = shoppingBag.inventoryPositions
      let inv
      Object.keys(obj).forEach((props) => {
        if (obj[props].invavls) {
          inv = obj[props].invavls
        } else {
          inv = obj[props].inventorys
        }
        const shoppingBagInventoryPositionsProps = {
          partNumber: obj[props].partNumber,
          catentryId: obj[props].catentryId,
          inv,
        }
        let shoppingBagInventoryPositionsSchema
        if (inv !== null) {
          shoppingBagInventoryPositionsSchema = {
            title: 'Shopping Bag Simple Product inventoryPositions Json Schema',
            type: 'object',
            required: ['partNumber', 'catentryId', 'inv'],
            properties: {
              partNumber: stringType,
              catEntryId: numberType,
              inv: {
                type: 'array',
                minItems: 1,
                uniqueItems: true,
                items: { type: 'object' },
              },
            },
          }
        } else {
          shoppingBagInventoryPositionsSchema = {
            title: 'Shopping Bag Simple Product inventoryPositions Json Schema',
            type: 'object',
            required: ['partNumber', 'catentryId', 'inv'],
            properties: {
              partNumber: stringType,
              catEntryId: numberType,
            },
          }
        }
        assert.jsonSchema(
          shoppingBagInventoryPositionsProps,
          shoppingBagInventoryPositionsSchema
        )
      })
    })

    it('Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema', () => {
      const obj = shoppingBag.inventoryPositions
      Object.keys(obj).forEach((props) => {
        if (obj[props].invavls) {
          obj[props].invavls.forEach((prop) => {
            const shoppingBagInvAvlsPositionsProps = {
              cutofftime: prop.cutofftime,
              quantity: prop.quantity,
              stlocIdentifier: prop.stlocIdentifier,
              expressdates: prop.expressdates,
            }
            const shoppingBagInvAvlsPositionsSchema = {
              title:
                'Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
              type: 'object',
              required: [
                'cutofftime',
                'quantity',
                'stlocIdentifier',
                'expressdates',
              ],
              properties: {
                cutofftime: stringType,
                quantity: numberType,
                stlocIdentifier: stringType,
                expressdates: {
                  type: 'array',
                  minItems: 2,
                  uniqueItems: true,
                  items: stringType,
                },
              },
            }
            assert.jsonSchema(
              shoppingBagInvAvlsPositionsProps,
              shoppingBagInvAvlsPositionsSchema
            )
          })
        } else if (obj[props].inventorys) {
          obj[props].inventorys.forEach((prop) => {
            const shoppingBagInventorysPositionsProps = {
              cutofftime: prop.cutofftime,
              quantity: prop.quantity,
              ffmcenterId: prop.ffmcenterId,
              expressdates: prop.expressdates,
            }
            const shoppingBagInventorysPositionsSchema = {
              title:
                'Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
              type: 'object',
              required: [
                'cutofftime',
                'quantity',
                'ffmcenterId',
                'expressdates',
              ],
              properties: {
                cutofftime: stringType,
                quantity: numberType,
                ffmcenterId: numberType,
                expressdates: {
                  type: 'array',
                  minItems: 2,
                  uniqueItems: true,
                  items: stringType,
                },
              },
            }
            assert.jsonSchema(
              shoppingBagInventorysPositionsProps,
              shoppingBagInventorysPositionsSchema
            )
          })
        }
      })
    })

    it('Shopping Bag Simple Product inventoryPositions => inventories && invavls => ExpressDates Properties Values', () => {
      const obj = shoppingBag.inventoryPositions
      Object.keys(obj).forEach((props) => {
        if (obj[props].invavls) {
          obj[props].invavls.forEach((prop) => {
            const expressdateSchema = {
              title: 'Express date in inventory item',
              type: 'array',
              required: ['0', '1'],
              properties: {
                0: stringType,
                1: stringType,
              },
            }
            assert.jsonSchema(prop.expressdates, expressdateSchema)
          })
        } else if (obj[props].inventorys) {
          obj[props].inventorys.forEach((prop) => {
            const expressdateSchema = {
              title: 'Express date in inventory item',
              type: 'array',
              required: ['0', '1'],
              properties: {
                0: stringType,
                1: stringType,
              },
            }
            assert.jsonSchema(prop.expressdates, expressdateSchema)
          })
        }
      })
    })
  })

  describe('Shopping Bag DELETE Product', () => {
    let shoppingBagRemoveItems
    beforeAll(async () => {
      shoppingBagRemoveItems = await removeItemFromShoppingBag(
        newAccount.jsessionid,
        shoppingBag.orderId,
        shoppingBag.products[0].orderItemId
      )
    }, 60000)

    it('Shopping Bag DELETE Product Json Schema', () => {
      const shoppingBagSchema = {
        title: 'Shopping Bag Product Json Schema',
        type: 'object',
        required: [
          'ageVerificationRequired',
          'deliveryOptions',
          'discounts',
          'inventoryPositions',
          'orderId',
          'products',
          'promotions',
          'restrictedDeliveryItem',
          'savedProducts',
          'espots',
          'subTotal',
          'total',
          'totalBeforeDiscount',
        ],
        optional: ['isDDPOrder', 'isDDPProduct'],
        properties: {
          ageVerificationRequired: booleanType(false),
          deliveryOptions: arrayType(),
          discounts: arrayType(),
          inventoryPositions: objectType,
          isDDPProduct: booleanTypeAny,
          isDDPOrder: booleanTypeAny,
          orderId: numberType,
          products: arrayType(1),
          promotions: arrayType(),
          restrictedDeliveryItem: booleanType(false),
          savedProducts: arrayType(),
          espots: objectType,
          subTotal: stringType,
          total: stringType,
          totalBeforeDiscount: stringType,
        },
      }
      assert.jsonSchema(shoppingBagRemoveItems, shoppingBagSchema)
    })
  })

  describe('Shopping GET Fetch Size and Quantity Json Schema', () => {
    it('Shopping Bag GET Size & Qty Json Schema', () => {
      productSizeQty.items.forEach((item) => {
        const shoppingBagSchema = {
          title: 'Shopping Bag PUT Fetch Size and Quantity Product Json Schema',
          type: 'object',
          required: [
            'catEntryId',
            'quantity',
            'selected',
            'size',
            'unitPrice',
            'wasPrice',
            'wasWasPrice',
          ],
          properties: {
            catEntryId: numberType,
            quantity: numberType,
            selected: { type: 'boolean' },
            size: stringType,
            unitPrice: stringType,
            wasPrice: stringTypePattern(),
            wasWasPrice: stringTypeEmpty,
          },
        }
        assert.jsonSchema(item, shoppingBagSchema)
      })
    })
  })

  describe('Shopping PUT Update Qty Product Json Schema', () => {
    let productUpdate
    beforeAll(async () => {
      productUpdate = await updateShoppingBagItem(
        newAccount.jsessionid,
        productSizeQty.items[0].catEntryId,
        productSizeQty.items[0].catEntryId,
        2
      )
    }, 60000)

    it('Shopping Bag PUT Product Json Schema', () => {
      const shoppingBagSchema = {
        title: 'Shopping Bag PUT Product Json Schema',
        type: 'object',
        required: [
          'ageVerificationRequired',
          'deliveryOptions',
          'discounts',
          'inventoryPositions',
          'orderId',
          'products',
          'promotions',
          'restrictedDeliveryItem',
          'savedProducts',
          'subTotal',
          'total',
          'totalBeforeDiscount',
        ],
        optional: ['isDDPOrder', 'isDDPProduct'],
        properties: {
          ageVerificationRequired: booleanType(false),
          deliveryOptions: arrayType(5),
          discounts: arrayType(),
          inventoryPositions: objectType,
          isDDPProduct: booleanTypeAny,
          isDDPOrder: booleanTypeAny,
          orderId: numberType,
          products: arrayType(1),
          promotions: arrayType(),
          restrictedDeliveryItem: booleanType(false),
          savedProducts: arrayType(),
          subTotal: stringType,
          total: stringType,
          totalBeforeDiscount: stringType,
        },
      }
      assert.jsonSchema(productUpdate, shoppingBagSchema)
    })

    it('Shopping Bag PUT Product deliveryOptions Json Schema', () => {
      const body = productUpdate.deliveryOptions
      body.forEach((deliveryOptions) => {
        const shoppingBagDeliveryOptionsSchema = {
          title: 'Shopping Bag PUT Product deliveryOptions Json Schema',
          type: 'object',
          required: [
            'deliveryOptionId',
            'label',
            'deliveryOptionExternalId',
            'selected',
            'enabled',
          ],
          properties: {
            deliveryOptionId: numberType,
            label: stringType,
            deliveryOptionExternalId: stringType,
            selected: { type: 'boolean' },
            enabled: { type: 'boolean' },
          },
        }
        assert.jsonSchema(deliveryOptions, shoppingBagDeliveryOptionsSchema)
      })
    })

    it('Shopping Bag PUT Product products Json Schema', () => {
      const body = productUpdate.products
      body.forEach((product) => {
        const shoppingBagProductsSchema = {
          title: 'Shopping Bag GET Product products Json Schema',
          type: 'object',
          required: [
            'ageVerificationRequired',
            'assets',
            'attributes',
            'bundleProducts',
            'bundleSlots',
            'catEntryId',
            'colourSwatches',
            'discountText',
            'inStock',
            'isBundleOrOutfit',
            'items',
            'lineNumber',
            'lowStock',
            'name',
            'orderItemId',
            'productId',
            'quantity',
            'shipModeId',
            'size',
            'totalPrice',
            'tpmLinks',
            'unitPrice',
          ],
          optional: ['isDDPOrder', 'isDDPProduct'],
          properties: {
            ageVerificationRequired: booleanType(false),
            assets: arrayType(4),
            attributes: objectType,
            bundleProducts: arrayType(),
            bundleSlots: arrayType(),
            catEntryId: numberType,
            colourSwatches: arrayType(),
            discountText: stringTypeEmpty,
            inStock: booleanType(true),
            isBundleOrOutfit: booleanType(false),
            isDDPProduct: booleanTypeAny,
            isDDPOrder: booleanTypeAny,
            items: arrayType(),
            lineNumber: stringType,
            lowStock: booleanType(false),
            name: stringType,
            orderItemId: numberType,
            productId: numberType,
            quantity: numberTypePattern(1, 2),
            shipModeId: numberType,
            size: stringType,
            totalPrice: numberType,
            tpmLinks: arrayType(),
            unitPrice: stringType,
          },
        }
        assert.jsonSchema(product, shoppingBagProductsSchema)
      })
    })

    it('Shopping Bag PUT Product products assets Json Schema', () => {
      const body = productUpdate.products
      body.forEach((prod) => {
        prod.assets.forEach((asset) => {
          const shoppingBagProductsSchema = {
            title: 'Shopping Bag PUT Product products assets Json Schema',
            type: 'object',
            required: ['assetType', 'index', 'url'],
            properties: {
              assetType: stringType,
              index: numberType,
              url: stringType,
            },
          }
          assert.jsonSchema(asset, shoppingBagProductsSchema)
        })
      })
    })

    it('Shopping Bag Simple Product inventoryPositions Json Schema', () => {
      const obj = productUpdate.inventoryPositions
      let inv
      Object.keys(obj).forEach((props) => {
        if (obj[props].invavls) {
          inv = obj[props].invavls
        } else {
          inv = obj[props].inventorys
        }
        const shoppingBagInventoryPositionsProps = {
          partNumber: obj[props].partNumber,
          catentryId: obj[props].catentryId,
          inv,
        }
        let shoppingBagInventoryPositionsSchema
        if (inv !== null) {
          shoppingBagInventoryPositionsSchema = {
            title: 'Shopping Bag Simple Product inventoryPositions Json Schema',
            type: 'object',
            required: ['partNumber', 'catentryId', 'inv'],
            properties: {
              partNumber: stringType,
              catEntryId: numberType,
              inv: {
                type: 'array',
                minItems: 1,
                uniqueItems: true,
                items: { type: 'object' },
              },
            },
          }
        } else {
          shoppingBagInventoryPositionsSchema = {
            title: 'Shopping Bag Simple Product inventoryPositions Json Schema',
            type: 'object',
            required: ['partNumber', 'catentryId', 'inv'],
            properties: {
              partNumber: stringType,
              catEntryId: numberType,
            },
          }
        }
        assert.jsonSchema(
          shoppingBagInventoryPositionsProps,
          shoppingBagInventoryPositionsSchema
        )
      })
    })

    it('Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema', () => {
      const obj = productUpdate.inventoryPositions
      Object.keys(obj).forEach((props) => {
        if (obj[props].invavls) {
          obj[props].invavls.forEach((prop) => {
            const shoppingBagInvAvlsPositionsProps = {
              cutofftime: prop.cutofftime,
              quantity: prop.quantity,
              stlocIdentifier: prop.stlocIdentifier,
              expressdates: prop.expressdates,
            }
            const shoppingBagInvAvlsPositionsSchema = {
              title:
                'Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
              type: 'object',
              required: [
                'cutofftime',
                'quantity',
                'stlocIdentifier',
                'expressdates',
              ],
              properties: {
                cutofftime: stringType,
                quantity: numberType,
                stlocIdentifier: stringType,
                expressdates: {
                  type: 'array',
                  minItems: 2,
                  uniqueItems: true,
                  items: stringType,
                },
              },
            }
            assert.jsonSchema(
              shoppingBagInvAvlsPositionsProps,
              shoppingBagInvAvlsPositionsSchema
            )
          })
        } else if (obj[props].inventorys) {
          obj[props].inventorys.forEach((prop) => {
            const shoppingBagInventorysPositionsProps = {
              cutofftime: prop.cutofftime,
              quantity: prop.quantity,
              ffmcenterId: prop.ffmcenterId,
              expressdates: prop.expressdates,
            }
            const shoppingBagInventorysPositionsSchema = {
              title:
                'Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
              type: 'object',
              required: [
                'cutofftime',
                'quantity',
                'ffmcenterId',
                'expressdates',
              ],
              properties: {
                cutofftime: stringType,
                quantity: numberType,
                ffmcenterId: numberType,
                expressdates: {
                  type: 'array',
                  minItems: 2,
                  uniqueItems: true,
                  items: stringType,
                },
              },
            }
            assert.jsonSchema(
              shoppingBagInventorysPositionsProps,
              shoppingBagInventorysPositionsSchema
            )
          })
        }
      })
    })

    it('Shopping Bag Simple Product inventoryPositions => inventories && invavls => ExpressDates Properties Values', () => {
      const obj = productUpdate.inventoryPositions
      Object.keys(obj).forEach((props) => {
        if (obj[props].invavls) {
          obj[props].invavls.forEach((prop) => {
            const expressdateSchema = {
              title: 'Express date in inventory item',
              type: 'array',
              required: ['0', '1'],
              properties: {
                0: stringType,
                1: stringType,
              },
            }
            assert.jsonSchema(prop.expressdates, expressdateSchema)
          })
        } else if (obj[props].inventorys) {
          obj[props].inventorys.forEach((prop) => {
            const expressdateSchema = {
              title: 'Express date in inventory item',
              type: 'array',
              required: ['0', '1'],
              properties: {
                0: stringType,
                1: stringType,
              },
            }
            assert.jsonSchema(prop.expressdates, expressdateSchema)
          })
        }
      })
    })
  })

  describe('Shopping PUT Update Size Product Json Schema', () => {
    let productUpdate
    beforeAll(async () => {
      productUpdate = await updateShoppingBagItem(
        newAccount.jsessionid,
        productSizeQty.items[1].catEntryId,
        productSizeQty.items[0].catEntryId,
        1
      )
    }, 60000)

    it('Shopping Bag PUT Product Json Schema', () => {
      const shoppingBagSchema = {
        title: 'Shopping Bag PUT Product Json Schema',
        type: 'object',
        required: [
          'ageVerificationRequired',
          'deliveryOptions',
          'discounts',
          'inventoryPositions',
          'orderId',
          'products',
          'promotions',
          'restrictedDeliveryItem',
          'savedProducts',
          'subTotal',
          'total',
          'totalBeforeDiscount',
        ],
        optional: ['isDDPOrder', 'isDDPProduct'],
        properties: {
          ageVerificationRequired: booleanType(false),
          deliveryOptions: arrayType(5),
          discounts: arrayType(),
          inventoryPositions: objectType,
          isDDPProduct: booleanTypeAny,
          isDDPOrder: booleanTypeAny,
          orderId: numberType,
          products: arrayType(1),
          promotions: arrayType(),
          restrictedDeliveryItem: booleanType(false),
          savedProducts: arrayType(),
          subTotal: stringType,
          total: stringType,
          totalBeforeDiscount: stringType,
        },
      }
      assert.jsonSchema(productUpdate, shoppingBagSchema)
    })

    it('Shopping Bag PUT Product deliveryOptions Json Schema', () => {
      const body = productUpdate.deliveryOptions
      body.forEach((deliveryOptions) => {
        const shoppingBagDeliveryOptionsSchema = {
          title: 'Shopping Bag PUT Product deliveryOptions Json Schema',
          type: 'object',
          required: [
            'deliveryOptionId',
            'label',
            'deliveryOptionExternalId',
            'selected',
            'enabled',
          ],
          properties: {
            deliveryOptionId: numberType,
            label: stringType,
            deliveryOptionExternalId: stringType,
            selected: { type: 'boolean' },
            enabled: { type: 'boolean' },
          },
        }
        assert.jsonSchema(deliveryOptions, shoppingBagDeliveryOptionsSchema)
      })
    })

    it('Shopping Bag PUT Product products Json Schema', () => {
      const body = productUpdate.products
      body.forEach((product) => {
        const shoppingBagProductsSchema = {
          title: 'Shopping Bag GET Product products Json Schema',
          type: 'object',
          required: [
            'ageVerificationRequired',
            'assets',
            'attributes',
            'bundleProducts',
            'bundleSlots',
            'catEntryId',
            'colourSwatches',
            'discountText',
            'inStock',
            'isBundleOrOutfit',
            'items',
            'lineNumber',
            'lowStock',
            'name',
            'orderItemId',
            'productId',
            'quantity',
            'shipModeId',
            'size',
            'totalPrice',
            'tpmLinks',
            'unitPrice',
          ],
          optional: ['isDDPOrder', 'isDDPProduct'],
          properties: {
            ageVerificationRequired: booleanType(false),
            assets: arrayType(4),
            attributes: objectType,
            bundleProducts: arrayType(),
            bundleSlots: arrayType(),
            catEntryId: numberType,
            colourSwatches: arrayType(),
            discountText: stringTypeEmpty,
            inStock: booleanType(true),
            isBundleOrOutfit: booleanType(false),
            isDDPProduct: booleanTypeAny,
            isDDPOrder: booleanTypeAny,
            items: arrayType(),
            lineNumber: stringType,
            lowStock: booleanType(false),
            name: stringType,
            orderItemId: numberType,
            productId: numberType,
            quantity: numberTypePattern(1),
            shipModeId: numberType,
            size: stringType,
            totalPrice: numberType,
            tpmLinks: arrayType(),
            unitPrice: stringType,
          },
        }
        assert.jsonSchema(product, shoppingBagProductsSchema)
      })
    })

    it('Shopping Bag PUT Product products assets Json Schema', () => {
      const body = productUpdate.products
      body.forEach((prod) => {
        prod.assets.forEach((asset) => {
          const shoppingBagProductsSchema = {
            title: 'Shopping Bag PUT Product products assets Json Schema',
            type: 'object',
            required: ['assetType', 'index', 'url'],
            properties: {
              assetType: stringType,
              index: numberType,
              url: stringType,
            },
          }
          assert.jsonSchema(asset, shoppingBagProductsSchema)
        })
      })
    })

    it('Shopping Bag Simple Product inventoryPositions Json Schema', () => {
      const obj = productUpdate.inventoryPositions
      let inv
      Object.keys(obj).forEach((props) => {
        if (obj[props].invavls) {
          inv = obj[props].invavls
        } else {
          inv = obj[props].inventorys
        }
        const shoppingBagInventoryPositionsProps = {
          partNumber: obj[props].partNumber,
          catentryId: obj[props].catentryId,
          inv,
        }
        let shoppingBagInventoryPositionsSchema
        if (inv !== null) {
          shoppingBagInventoryPositionsSchema = {
            title: 'Shopping Bag Simple Product inventoryPositions Json Schema',
            type: 'object',
            required: ['partNumber', 'catentryId', 'inv'],
            properties: {
              partNumber: stringType,
              catEntryId: numberType,
              inv: {
                type: 'array',
                minItems: 1,
                uniqueItems: true,
                items: { type: 'object' },
              },
            },
          }
        } else {
          shoppingBagInventoryPositionsSchema = {
            title: 'Shopping Bag Simple Product inventoryPositions Json Schema',
            type: 'object',
            required: ['partNumber', 'catentryId', 'inv'],
            properties: {
              partNumber: stringType,
              catEntryId: numberType,
            },
          }
        }
        assert.jsonSchema(
          shoppingBagInventoryPositionsProps,
          shoppingBagInventoryPositionsSchema
        )
      })
    })

    it('Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema', () => {
      const obj = productUpdate.inventoryPositions
      Object.keys(obj).forEach((props) => {
        if (obj[props].invavls) {
          obj[props].invavls.forEach((prop) => {
            const shoppingBagInvAvlsPositionsProps = {
              cutofftime: prop.cutofftime,
              quantity: prop.quantity,
              stlocIdentifier: prop.stlocIdentifier,
              expressdates: prop.expressdates,
            }
            const shoppingBagInvAvlsPositionsSchema = {
              title:
                'Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
              type: 'object',
              required: [
                'cutofftime',
                'quantity',
                'stlocIdentifier',
                'expressdates',
              ],
              properties: {
                cutofftime: stringType,
                quantity: numberType,
                stlocIdentifier: stringType,
                expressdates: {
                  type: 'array',
                  minItems: 2,
                  uniqueItems: true,
                  items: stringType,
                },
              },
            }
            assert.jsonSchema(
              shoppingBagInvAvlsPositionsProps,
              shoppingBagInvAvlsPositionsSchema
            )
          })
        } else if (obj[props].inventorys) {
          obj[props].inventorys.forEach((prop) => {
            const shoppingBagInventorysPositionsProps = {
              cutofftime: prop.cutofftime,
              quantity: prop.quantity,
              ffmcenterId: prop.ffmcenterId,
              expressdates: prop.expressdates,
            }
            const shoppingBagInventorysPositionsSchema = {
              title:
                'Shopping Bag Simple Product inventoryPositions => inventories && invavls Json Schema',
              type: 'object',
              required: [
                'cutofftime',
                'quantity',
                'ffmcenterId',
                'expressdates',
              ],
              properties: {
                cutofftime: stringType,
                quantity: numberType,
                ffmcenterId: numberType,
                expressdates: {
                  type: 'array',
                  minItems: 2,
                  uniqueItems: true,
                  items: stringType,
                },
              },
            }
            assert.jsonSchema(
              shoppingBagInventorysPositionsProps,
              shoppingBagInventorysPositionsSchema
            )
          })
        }
      })
    })

    it('Shopping Bag Simple Product inventoryPositions => inventories && invavls => ExpressDates Properties Values', () => {
      const obj = productUpdate.inventoryPositions
      Object.keys(obj).forEach((props) => {
        if (obj[props].invavls) {
          obj[props].invavls.forEach((prop) => {
            const expressdateSchema = {
              title: 'Express date in inventory item',
              type: 'array',
              required: ['0', '1'],
              properties: {
                0: stringType,
                1: stringType,
              },
            }
            assert.jsonSchema(prop.expressdates, expressdateSchema)
          })
        } else if (obj[props].inventorys) {
          obj[props].inventorys.forEach((prop) => {
            const expressdateSchema = {
              title: 'Express date in inventory item',
              type: 'array',
              required: ['0', '1'],
              properties: {
                0: stringType,
                1: stringType,
              },
            }
            assert.jsonSchema(prop.expressdates, expressdateSchema)
          })
        }
      })
    })
  })

  describe('Shopping Bag POST Update Delivery', () => {
    let selectDeliveryOptions
    beforeAll(async () => {
      selectDeliveryOptions = await updateShoppingBagDelivery(
        newAccount.jsessionid,
        deliveryOptions.collectFromParcelShop
      )
    }, 60000)

    it('Shopping Bag Update Delivery to Collect From ParcelShop', async () => {
      selectDeliveryOptions.deliveryOptions.forEach((deliveryOptions) => {
        const shoppingBagChangeDeliveryOptionsSchema = {
          title:
            'Shopping Bag Update deliveryOptions to Collect From ParcelShop',
          type: 'object',
          required: [
            'deliveryOptionId',
            'label',
            'selected',
            'deliveryOptionExternalId',
            'enabled',
          ],
          properties: {
            deliveryOptionId: numberType,
            label: stringType,
            selected: { type: 'boolean' },
            deliveryOptionExternalId: stringType,
            enabled: { type: 'boolean' },
          },
        }
        assert.jsonSchema(
          deliveryOptions,
          shoppingBagChangeDeliveryOptionsSchema
        )
      })
    })

    it('Shopping Bag Update Delivery to Free Collect From Store Standard', async () => {
      selectDeliveryOptions.deliveryOptions.forEach((deliveryOptions) => {
        const shoppingBagChangeDeliveryOptionsSchema = {
          title:
            'Shopping Bag Update deliveryOptions to Free Collect From Store Standard £0.00',
          type: 'object',
          required: [
            'deliveryOptionId',
            'label',
            'selected',
            'deliveryOptionExternalId',
            'enabled',
          ],
          properties: {
            deliveryOptionId: numberType,
            label: stringType,
            selected: { type: 'boolean' },
            deliveryOptionExternalId: stringType,
            enabled: { type: 'boolean' },
          },
        }
        assert.jsonSchema(
          deliveryOptions,
          shoppingBagChangeDeliveryOptionsSchema
        )
      })
    })

    it('Shopping Bag Update Delivery to Express / Nominated Day Delivery', async () => {
      selectDeliveryOptions = await updateShoppingBagDelivery(
        newAccount.jsessionid,
        deliveryOptions.expressNominatedDayDelivery
      )
      selectDeliveryOptions.deliveryOptions.forEach((deliveryOptions) => {
        const shoppingBagChangeDeliveryOptionsSchema = {
          title:
            'Shopping Bag Update deliveryOptions to Express / Nominated Day Delivery £6.00',
          type: 'object',
          required: [
            'deliveryOptionId',
            'label',
            'selected',
            'deliveryOptionExternalId',
            'enabled',
          ],
          properties: {
            deliveryOptionId: numberType,
            label: stringType,
            selected: { type: 'boolean' },
            deliveryOptionExternalId: stringType,
            enabled: { type: 'boolean' },
          },
        }
        assert.jsonSchema(
          deliveryOptions,
          shoppingBagChangeDeliveryOptionsSchema
        )
      })
    })

    it('Shopping Bag Update Delivery to Standard Delivery', async () => {
      selectDeliveryOptions = await updateShoppingBagDelivery(
        newAccount.jsessionid,
        deliveryOptions.standardDelivery
      )
      selectDeliveryOptions.deliveryOptions.forEach((deliveryOptions) => {
        const shoppingBagChangeDeliveryOptionsSchema = {
          title:
            'Shopping Bag Update deliveryOptions to Standard Delivery £4.00',
          type: 'object',
          required: [
            'deliveryOptionId',
            'label',
            'selected',
            'deliveryOptionExternalId',
            'enabled',
          ],
          properties: {
            deliveryOptionId: numberType,
            label: stringType,
            selected: { type: 'boolean' },
            deliveryOptionExternalId: stringType,
            enabled: { type: 'boolean' },
          },
        }
        assert.jsonSchema(
          deliveryOptions,
          shoppingBagChangeDeliveryOptionsSchema
        )
      })
    })

    it('Shopping Bag Update Delivery to Collect From Store Express', async () => {
      selectDeliveryOptions = await updateShoppingBagDelivery(
        newAccount.jsessionid,
        deliveryOptions.collectFromStoreExpress
      )
      selectDeliveryOptions.deliveryOptions.forEach((deliveryOptions) => {
        const shoppingBagChangeDeliveryOptionsSchema = {
          title:
            'Shopping Bag Update deliveryOptions to Collect From Store Express £3.00',
          type: 'object',
          required: [
            'deliveryOptionExternalId',
            'deliveryOptionId',
            'enabled',
            'label',
            'selected',
          ],
          properties: {
            deliveryOptionExternalId: stringType,
            deliveryOptionId: numberType,
            enabled: { type: 'boolean' },
            label: stringType,
            selected: { type: 'boolean' },
          },
        }
        assert.jsonSchema(
          deliveryOptions,
          shoppingBagChangeDeliveryOptionsSchema
        )
      })
    })
  })

  describe('Shopping Bag POST Add Promotion Code', () => {
    it('Shopping bag schema', () => {
      const shoppingBagSchema = {
        title: 'Shopping Bag with Promotion Schema',
        type: 'object',
        required: [
          'orderId',
          'subTotal',
          'total',
          'totalBeforeDiscount',
          'deliveryOptions',
          'promotions',
          'discounts',
          'products',
          'savedProducts',
          'ageVerificationRequired',
          'restrictedDeliveryItem',
          'inventoryPositions',
        ],
        optional: ['isDDPOrder', 'isDDPProduct'],
        properties: {
          orderId: numberType,
          subTotal: stringType,
          total: stringType,
          totalBeforeDiscount: stringType,
          deliveryOptions: arrayType(5),
          promotions: {
            type: 'array',
            minItems: 1,
            maxItems: 1,
            items: { type: 'object' },
          },
          discounts: arrayType(),
          products: arrayType(2),
          savedProducts: arrayType(),
          ageVerificationRequired: booleanType(false),
          restrictedDeliveryItem: booleanType(false),
          inventoryPositions: objectType,
          isDDPProduct: booleanTypeAny,
          isDDPOrder: booleanTypeAny,
        },
      }
      assert.jsonSchema(addPromotionCode, shoppingBagSchema)
    })

    it('Shopping bag => Promotions schema', () => {
      const body = addPromotionCode.promotions[0]
      const shoppingBagSchema = {
        title: 'Shopping Bag => Promotions Schema',
        type: 'object',
        required: ['promotionCode', 'label'],
        properties: {
          promotionCode: stringTypePattern(PROMOTION_CODE),
          label: stringType,
        },
      }
      assert.jsonSchema(body, shoppingBagSchema)
    })
  })

  describe('Shopping Bag DELETE Promotion Code', () => {
    let removePromotionCode
    beforeAll(async () => {
      removePromotionCode = await deletePromotionCode(newAccount.jsessionid)
    }, 60000)

    it('Shopping bag schema', () => {
      const shoppingBagSchema = {
        title: 'Shopping Bag with Promotion Schema',
        type: 'object',
        required: [
          'orderId',
          'subTotal',
          'total',
          'totalBeforeDiscount',
          'deliveryOptions',
          'promotions',
          'discounts',
          'products',
          'savedProducts',
          'ageVerificationRequired',
          'restrictedDeliveryItem',
          'inventoryPositions',
        ],
        optional: ['isDDPOrder', 'isDDPProduct'],
        properties: {
          orderId: numberType,
          subTotal: stringType,
          total: stringType,
          totalBeforeDiscount: stringType,
          deliveryOptions: arrayType(5),
          promotions: {
            type: 'array',
            minItems: 0,
            maxItems: 0,
            items: { type: 'object' },
          },
          discounts: arrayType(),
          products: arrayType(2),
          savedProducts: arrayType(),
          ageVerificationRequired: booleanType(false),
          restrictedDeliveryItem: booleanType(false),
          inventoryPositions: objectType,
          isDDPProduct: booleanTypeAny,
          isDDPOrder: booleanTypeAny,
        },
      }
      assert.jsonSchema(removePromotionCode, shoppingBagSchema)
    })
  })
})
