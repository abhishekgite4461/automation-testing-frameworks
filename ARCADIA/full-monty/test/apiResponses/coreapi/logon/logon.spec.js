require('babel-polyfill')

import chai from 'chai'

chai.use(require('chai-json-schema'))

const assert = chai.assert

import {
  stringType,
  stringTypeEmpty,
  objectType,
  booleanType,
  numberType,
  numberTypePattern,
  stringTypePattern,
  booleanOrNull,
} from '../utilis'
import {
  newUserMobileAppPayload,
  newUserPayload,
  reauthenticationMobileAppPayload,
} from './logon-data'
import {
  loginAsNewUserAndRegisterIfNotExisting,
  logIn,
  logOut,
  createAccount,
} from './../utilis/userAccount'
import { addItemToShoppingBag } from '../utilis/shoppingBag'
import { getProducts } from '../utilis/selectProducts'
import { payOrder } from '../utilis/payOrder'

let newAccount

const testNewUserProfileSchema = (response) => {
  const logonNewUserSchema = {
    title: 'Logon User Full Profile Schema',
    type: 'object',
    required: [
      'exists',
      'email',
      'title',
      'firstName',
      'lastName',
      'userTrackingId',
      'subscriptionId',
      'basketItemCount',
      'creditCard',
      'deliveryDetails',
      'billingDetails',
      'version',
      'hasCardNumberHash',
      'hasPayPal',
      'hasDeliveryDetails',
      'hasBillingDetails',
    ],
    optional: ['isDDPUser'],
    properties: {
      exists: booleanType(true),
      email: stringType,
      title: stringTypeEmpty,
      firstName: stringTypeEmpty,
      lastName: stringTypeEmpty,
      userTrackingId: numberType,
      subscriptionId: stringTypeEmpty,
      basketItemCount: numberType,
      creditCard: objectType,
      deliveryDetails: objectType,
      billingDetails: objectType,
      version: stringType,
      hasCardNumberHash: booleanType(false),
      hasPayPal: booleanType(false),
      hasDeliveryDetails: booleanType(false),
      hasBillingDetails: booleanType(false),
      isDDPUser: booleanOrNull,
    },
  }
  assert.jsonSchema(response, logonNewUserSchema)
}

const testPartialProfileUserSchema = (response) => {
  const logOnSchema = {
    title: 'Logon User Partial Profile Schema',
    type: 'object',
    required: [
      'basketItemCount',
      'billingDetails',
      'creditCard',
      'deliveryDetails',
      'email',
      'exists',
      'firstName',
      'hasBillingDetails',
      'hasCardNumberHash',
      'hasDeliveryDetails',
      'hasPayPal',
      'lastName',
      'subscriptionId',
      'title',
      'userTrackingId',
      'version',
    ],
    optional: ['isDDPUser'],
    properties: {
      basketItemCount: numberType,
      billingDetails: objectType,
      creditCard: objectType,
      deliveryDetails: objectType,
      email: stringType,
      exists: booleanType(true),
      firstName: stringTypeEmpty,
      hasBillingDetails: booleanType(false),
      hasCardNumberHash: booleanType(false),
      hasDeliveryDetails: booleanType(false),
      hasPayPal: booleanType(false),
      lastName: stringTypeEmpty,
      subscriptionId: numberType,
      title: stringTypeEmpty,
      userTrackingId: numberType,
      version: stringType,
      isDDPUser: booleanOrNull,
    },
  }
  assert.jsonSchema(response, logOnSchema)
}

describe('Logon Schema New User Mobile Apps', () => {
  describe('Given I am a registered new user', () => {
    let responseBody
    beforeAll(async () => {
      responseBody = await loginAsNewUserAndRegisterIfNotExisting(
        newUserMobileAppPayload
      )
    }, 60000)

    describe('When I login for the first time with my email, password and appId', () => {
      it('Then I receive a userToken', () => {
        // TODO: WCS doesn't yet send us back a userToken. Delete "not" once it
        // starts failing. That means WCS is actually sending the userToken.
        expect(responseBody.userToken).not.toBeDefined()
        expect(typeof responseBody.userToken).not.toBe('string')
      })
      it('And I receive a userId', () => {
        // TODO: WCS doesn't yet send us back a userId. Delete "not" once it
        // starts failing. That means WCS is actually sending the userId.
        expect(responseBody.userId).not.toBeDefined()
        expect(typeof responseBody.userId).not.toBe('number')
      })
      it('And the rest of the response matches the logon schema for new users', () =>
        testNewUserProfileSchema(responseBody))
    })

    describe('Given that I have logged in before', () => {
      describe('When I login again with my userToken, userId and appId, not providing my email address and password', () => {
        let responseBody
        beforeAll(async () => {
          try {
            responseBody = await loginAsNewUserAndRegisterIfNotExisting(
              reauthenticationMobileAppPayload
            )
          } catch (error) {
            responseBody = error
          }
          // We are currently expecting an error code because WCS doesn't
          // yet include the functionality to reauthenticate
          expect(responseBody.response.body.statusCode).toBe(422)
        }, 60000)
        it('Then I receive back a response matching the usual login success', () => {
          // Use "testNewUserProfileSchema(responseBody)" once response body is
          // defined
          expect(responseBody.exists).not.toBeDefined()
        })

        it('Then there is the same userId returned', () => {
          // Remove "not" once it starts failing: that means WCS feature works
          expect(responseBody.userId).not.toBeDefined()
          expect(typeof responseBody.userId).not.toBe('number')
          expect(responseBody.userId).not.toBe(
            reauthenticationMobileAppPayload.userId
          )
        })
        it('Then there is the same appId returned', () => {
          // Remove "not" once it starts failing: that means WCS feature works
          expect(responseBody.appId).not.toBeDefined()
          expect(typeof responseBody.appId).not.toBe('string')
          expect(responseBody.appId).not.toBe(
            reauthenticationMobileAppPayload.appId
          )
        })
        it('Then there is a userToken returned (which can have changed compared to the one submitted)', () => {
          // Remove "not" once it starts failing: that means WCS feature works
          expect(responseBody.userToken).not.toBeDefined()
          expect(typeof responseBody.userToken).not.toBe('string')
          expect(responseBody.userToken).not.toBe(
            reauthenticationMobileAppPayload.userToken
          )
        })
      })
    })
  })
})

describe('Logon Schema Partial Profile Mobile Apps', () => {
  describe('Given I can login as a returning user with deviceType "apps"', () => {
    let login

    beforeAll(async () => {
      newAccount = await createAccount()
      await logOut()
      try {
        login = await logIn({
          username: newAccount.accountProfile.email,
          password: 'monty1',
          deviceType: 'apps',
        })
      } catch (e) {
        login = e.response.statusCode
      }
    })

    it('Then the response matches the regular partial profile response', () => {
      testPartialProfileUserSchema(login)
    })

    it('Then the response contains a userToken', () => {
      expect(login.userToken).toBe(null)
    })
  })
})

describe('Logon Schema New User', () => {
  let responseBody
  beforeAll(async () => {
    responseBody = await loginAsNewUserAndRegisterIfNotExisting(newUserPayload)
  }, 60000)

  it('New User Profile Schema', () => testNewUserProfileSchema(responseBody))

  it('Full Checkout Profile Credit Card Schema', () => {
    const body = responseBody.creditCard
    const creditCardSchemaNewUser = {
      title: 'Logon User Full Profile Schema Credit Card Object Schema',
      type: 'object',
      required: [
        'cardNumberHash',
        'cardNumberStar',
        'expiryMonth',
        'expiryYear',
        'type',
      ],
      properties: {
        cardNumberHash: stringTypeEmpty,
        cardNumberStar: stringTypeEmpty,
        expiryMonth: stringTypeEmpty,
        expiryYear: stringTypeEmpty,
        type: stringTypeEmpty,
      },
    }
    assert.jsonSchema(body, creditCardSchemaNewUser)
  })

  it('Full Checkout Profile Billing Details Schema', () => {
    const body = responseBody.billingDetails
    const billingDetailsSchemaNewUser = {
      title: 'Logon User Full Profile Schema Billing Details Object Schema',
      type: 'object',
      required: ['addressDetailsId', 'nameAndPhone', 'address'],
      properties: {
        address: objectType,
        addressDetailsId: numberType,
        nameAndPhone: objectType,
      },
    }
    assert.jsonSchema(body, billingDetailsSchemaNewUser)
  })

  it('Full Checkout Profile Billing Details nameAndPhone Schema', () => {
    const body = responseBody.billingDetails.nameAndPhone
    const billingDetailsSchemaNewUser = {
      title:
        'Logon User Full Profile Schema Billing Details NameAndPhone Schema',
      type: 'object',
      required: ['lastName', 'telephone', 'title', 'firstName'],
      properties: {
        firstName: stringTypeEmpty,
        lastName: stringTypeEmpty,
        telephone: stringTypeEmpty,
        title: stringTypeEmpty,
      },
    }
    assert.jsonSchema(body, billingDetailsSchemaNewUser)
  })

  it('Full Checkout Profile Billing Details address Schema', () => {
    const body = responseBody.billingDetails.address
    const billingDetailsSchemaNewUser = {
      title: 'Logon User Full Profile Schema Billing Details Address Schema',
      type: 'object',
      required: [
        'address1',
        'address2',
        'city',
        'country',
        'postcode',
        'state',
      ],
      properties: {
        address1: stringTypeEmpty,
        address2: stringTypeEmpty,
        city: stringTypeEmpty,
        country: stringTypeEmpty,
        postcode: stringTypeEmpty,
        state: stringTypeEmpty,
      },
    }
    assert.jsonSchema(body, billingDetailsSchemaNewUser)
  })

  it('Full Checkout Profile Delivery Details Schema', () => {
    const body = responseBody.deliveryDetails
    const deliveryDetailsSchemaNewUser = {
      title: 'Logon User Full Profile Schema Delivery Details Object Schema',
      type: 'object',
      required: ['addressDetailsId', 'nameAndPhone', 'address'],
      properties: {
        address: objectType,
        addressDetailsId: numberType,
        nameAndPhone: objectType,
      },
    }
    assert.jsonSchema(body, deliveryDetailsSchemaNewUser)
  })

  it('Full Checkout Profile Delivery Details nameAndPhone Schema', () => {
    const body = responseBody.deliveryDetails.nameAndPhone
    const deliveryDetailsSchemaNewUser = {
      title:
        'Logon User Full Profile Schema Delivery Details NameAndPhone Schema',
      type: 'object',
      required: ['lastName', 'telephone', 'title', 'firstName'],
      properties: {
        firstName: stringTypeEmpty,
        lastName: stringTypeEmpty,
        telephone: stringTypeEmpty,
        title: stringTypeEmpty,
      },
    }
    assert.jsonSchema(body, deliveryDetailsSchemaNewUser)
  })

  it('Full Checkout Profile Delivery Details address Schema', () => {
    const body = responseBody.deliveryDetails.address
    const deliveryDetailsSchemaNewUser = {
      title: 'Logon User Full Profile Schema Delivery Details Address Schema',
      type: 'object',
      required: [
        'address1',
        'address2',
        'city',
        'country',
        'postcode',
        'state',
      ],
      properties: {
        address1: stringTypeEmpty,
        address2: stringTypeEmpty,
        city: stringTypeEmpty,
        country: stringTypeEmpty,
        postcode: stringTypeEmpty,
        state: stringTypeEmpty,
      },
    }
    assert.jsonSchema(body, deliveryDetailsSchemaNewUser)
  })
})

describe('Logon Schema Full Profile', () => {
  let products
  let shoppingBag
  let login

  beforeAll(async () => {
    products = await getProducts()
    newAccount = await createAccount()
    shoppingBag = await addItemToShoppingBag(
      newAccount.jsessionid,
      products.productsSimpleId
    )
    await payOrder(newAccount.jsessionid, shoppingBag.orderId)
    await logOut()
    login = await logIn({
      username: newAccount.accountProfile.email,
      password: 'monty1',
    })
  }, 60000)

  it('Full Checkout Profile Schema', () => {
    const logOnSchema = {
      title: 'Logon User Full Profile Schema',
      type: 'object',
      required: [
        'basketItemCount',
        'billingDetails',
        'creditCard',
        'deliveryDetails',
        'email',
        'exists',
        'firstName',
        'hasBillingDetails',
        'hasCardNumberHash',
        'hasDeliveryDetails',
        'hasPayPal',
        'lastName',
        'subscriptionId',
        'title',
        'userTrackingId',
        'version',
      ],
      optional: ['isDDPUser'],
      properties: {
        basketItemCount: numberType,
        billingDetails: objectType,
        creditCard: objectType,
        deliveryDetails: objectType,
        email: stringType,
        exists: booleanType(true),
        firstName: stringType,
        hasBillingDetails: booleanType(true),
        hasCardNumberHash: booleanType(true),
        hasDeliveryDetails: booleanType(true),
        hasPayPal: booleanType(false),
        lastName: stringType,
        subscriptionId: numberType,
        title: stringType,
        userTrackingId: numberType,
        version: stringType,
        isDDPUser: booleanOrNull,
      },
    }
    assert.jsonSchema(login, logOnSchema)
  })

  it('Full Checkout Profile Credit Card Schema', () => {
    const creditCardSchema = {
      title: 'Logon User Full Profile Schema Credit Card Object Schema',
      type: 'object',
      required: [
        'cardNumberHash',
        'cardNumberStar',
        'expiryMonth',
        'expiryYear',
        'type',
      ],
      properties: {
        cardNumberHash: stringType,
        cardNumberStar: stringType,
        expiryMonth: stringType,
        expiryYear: stringType,
        type: stringType,
      },
    }
    assert.jsonSchema(login.creditCard, creditCardSchema)
  })

  it('Full Checkout Profile Billing Details Schema', () => {
    const billingDetailsSchema = {
      title: 'Logon User Full Profile Schema Billing Details Object Schema',
      type: 'object',
      required: ['addressDetailsId', 'nameAndPhone', 'address'],
      properties: {
        address: objectType,
        addressDetailsId: numberType,
        nameAndPhone: objectType,
      },
    }
    assert.jsonSchema(login.billingDetails, billingDetailsSchema)
  })

  it('Full Checkout Profile Billing Details nameAndPhone Schema', () => {
    const billingDetailsSchema = {
      title:
        'Logon User Full Profile Schema Billing Details NameAndPhone Schema',
      type: 'object',
      required: ['lastName', 'telephone', 'title', 'firstName'],
      properties: {
        firstName: stringType,
        lastName: stringType,
        telephone: stringType,
        title: stringType,
      },
    }
    assert.jsonSchema(login.billingDetails.nameAndPhone, billingDetailsSchema)
  })

  it('Full Checkout Profile Billing Details address Schema', () => {
    const billingDetailsSchema = {
      title: 'Logon User Full Profile Schema Billing Details Address Schema',
      type: 'object',
      required: [
        'address1',
        'address2',
        'city',
        'country',
        'postcode',
        'state',
      ],
      properties: {
        address1: stringType,
        address2: stringType,
        city: stringType,
        country: stringType,
        postcode: stringType,
        state: stringTypeEmpty,
      },
    }
    assert.jsonSchema(login.billingDetails.address, billingDetailsSchema)
  })

  it('Full Checkout Profile Delivery Details Schema', () => {
    const deliveryDetailsSchema = {
      title: 'Logon User Full Profile Schema Delivery Details Object Schema',
      type: 'object',
      required: ['addressDetailsId', 'nameAndPhone', 'address'],
      properties: {
        address: objectType,
        addressDetailsId: numberType,
        nameAndPhone: objectType,
      },
    }
    assert.jsonSchema(login.deliveryDetails, deliveryDetailsSchema)
  })

  it('Full Checkout Profile Delivery Details nameAndPhone Schema', () => {
    const deliveryDetailsSchema = {
      title:
        'Logon User Full Profile Schema Delivery Details NameAndPhone Schema',
      type: 'object',
      required: ['lastName', 'telephone', 'title', 'firstName'],
      properties: {
        firstName: stringType,
        lastName: stringType,
        telephone: stringType,
        title: stringType,
      },
    }
    assert.jsonSchema(login.deliveryDetails.nameAndPhone, deliveryDetailsSchema)
  })

  it('Full Checkout Profile Delivery Details address Schema', () => {
    const deliveryDetailsSchema = {
      title: 'Logon User Full Profile Schema Delivery Details Address Schema',
      type: 'object',
      required: [
        'address1',
        'address2',
        'city',
        'country',
        'postcode',
        'state',
      ],
      properties: {
        address1: stringType,
        address2: stringType,
        city: stringType,
        country: stringType,
        postcode: stringType,
        state: stringTypeEmpty,
      },
    }
    assert.jsonSchema(login.deliveryDetails.address, deliveryDetailsSchema)
  })
})

describe('Logon Schema Partial Profile', () => {
  let login

  beforeAll(async () => {
    newAccount = await createAccount()
    await logOut()
    login = await logIn({
      username: newAccount.accountProfile.email,
      password: 'monty1',
    })
  }, 60000)
  it('Partial Checkout Profile Schema', () => {
    testPartialProfileUserSchema(login)
    const logOnSchema = {
      title: 'Logon User Partial Profile Schema',
      type: 'object',
      required: [
        'basketItemCount',
        'billingDetails',
        'creditCard',
        'deliveryDetails',
        'email',
        'exists',
        'firstName',
        'hasBillingDetails',
        'hasCardNumberHash',
        'hasDeliveryDetails',
        'hasPayPal',
        'lastName',
        'subscriptionId',
        'title',
        'userTrackingId',
        'version',
      ],
      optional: ['isDDPUser'],
      properties: {
        basketItemCount: numberType,
        billingDetails: objectType,
        creditCard: objectType,
        deliveryDetails: objectType,
        email: stringType,
        exists: booleanType(true),
        firstName: stringTypeEmpty,
        hasBillingDetails: booleanType(false),
        hasCardNumberHash: booleanType(false),
        hasDeliveryDetails: booleanType(false),
        hasPayPal: booleanType(false),
        lastName: stringTypeEmpty,
        subscriptionId: numberType,
        title: stringTypeEmpty,
        userTrackingId: numberType,
        version: stringType,
        isDDPUser: booleanOrNull,
      },
    }
    assert.jsonSchema(login, logOnSchema)
  })

  it('Partial Checkout Profile Credit Card Schema', () => {
    const creditCardSchema = {
      title: 'Logon User Partial Profile Schema Credit Card Object Schema',
      type: 'object',
      required: [
        'cardNumberHash',
        'cardNumberStar',
        'expiryMonth',
        'expiryYear',
        'type',
      ],
      properties: {
        cardNumberHash: stringTypeEmpty,
        cardNumberStar: stringTypeEmpty,
        expiryMonth: stringTypeEmpty,
        expiryYear: stringTypeEmpty,
        type: stringTypeEmpty,
      },
    }
    assert.jsonSchema(login.creditCard, creditCardSchema)
  })

  it('Partial Checkout Profile Billing Details Schema', () => {
    const billingDetailsSchema = {
      title: 'Logon User Partial Profile Schema Billing Details Object Schema',
      type: 'object',
      required: ['addressDetailsId', 'nameAndPhone', 'address'],
      properties: {
        address: objectType,
        addressDetailsId: numberTypePattern(-1, -1),
        nameAndPhone: objectType,
      },
    }
    assert.jsonSchema(login.billingDetails, billingDetailsSchema)
  })

  it('Partial Checkout Profile Billing Details nameAndPhone Schema', () => {
    const billingDetailsSchema = {
      title:
        'Logon User Partial Profile Schema Billing Details NameAndPhone Schema',
      type: 'object',
      required: ['lastName', 'telephone', 'title', 'firstName'],
      properties: {
        firstName: stringTypeEmpty,
        lastName: stringTypeEmpty,
        telephone: stringTypeEmpty,
        title: stringTypeEmpty,
      },
    }
    assert.jsonSchema(login.billingDetails.nameAndPhone, billingDetailsSchema)
  })

  it('Partial Checkout Profile Billing Details address Schema', () => {
    const billingDetailsSchema = {
      title: 'Logon User Partial Profile Schema Billing Details Address Schema',
      type: 'object',
      required: [
        'address1',
        'address2',
        'city',
        'country',
        'postcode',
        'state',
      ],
      properties: {
        address1: stringTypeEmpty,
        address2: stringTypeEmpty,
        city: stringTypeEmpty,
        country: stringTypeEmpty,
        postcode: stringTypeEmpty,
        state: stringTypeEmpty,
      },
    }
    assert.jsonSchema(login.billingDetails.address, billingDetailsSchema)
  })

  it('Partial Checkout Profile Delivery Details Schema', () => {
    const deliveryDetailsSchema = {
      title: 'Logon User Partial Profile Schema Delivery Details Object Schema',
      type: 'object',
      required: ['addressDetailsId', 'nameAndPhone', 'address'],
      properties: {
        address: objectType,
        addressDetailsId: numberTypePattern(-1),
        nameAndPhone: objectType,
      },
    }
    assert.jsonSchema(login.deliveryDetails, deliveryDetailsSchema)
  })

  it('Partial Checkout Profile Delivery Details nameAndPhone Schema', () => {
    const deliveryDetailsSchema = {
      title:
        'Logon User Partial Profile Schema Delivery Details NameAndPhone Schema',
      type: 'object',
      required: ['lastName', 'telephone', 'title', 'firstName'],
      properties: {
        firstName: stringTypeEmpty,
        lastName: stringTypeEmpty,
        telephone: stringTypeEmpty,
        title: stringTypeEmpty,
      },
    }
    assert.jsonSchema(login.deliveryDetails.nameAndPhone, deliveryDetailsSchema)
  })

  it('Partial Checkout Profile Delivery Details address Schema', () => {
    const deliveryDetailsSchema = {
      title:
        'Logon User Partial Profile Schema Delivery Details Address Schema',
      type: 'object',
      required: [
        'address1',
        'address2',
        'city',
        'country',
        'postcode',
        'state',
      ],
      properties: {
        address1: stringTypeEmpty,
        address2: stringTypeEmpty,
        city: stringTypeEmpty,
        country: stringTypeEmpty,
        postcode: stringTypeEmpty,
        state: stringTypeEmpty,
      },
    }
    assert.jsonSchema(login.deliveryDetails.address, deliveryDetailsSchema)
  })
})

describe('Logon Errors', () => {
  let login

  beforeAll(async () => {
    newAccount = await createAccount()
    await logOut()
  }, 60000)

  it('Logon Invalid Email and Password', async () => {
    try {
      login = await logIn({
        username: 'faillogon@sampleemail.org',
        password: 'monty1',
      })
    } catch (e) {
      login = e.response.body
    }

    const logOnInvalidAccountSchema = {
      title: 'Logon Invalid Account Schema',
      type: 'object',
      required: ['statusCode', 'error', 'message'],
      properties: {
        statusCode: numberTypePattern(422),
        error: stringTypePattern('Unprocessable Entity'),
        message: stringTypePattern(
          'The email address or password you entered has not been found. Please enter them again.'
        ),
      },
    }
    assert.jsonSchema(login, logOnInvalidAccountSchema)
  })

  it('Logon Invalid Password', async () => {
    try {
      login = await logIn({
        username: 'faillogon@sampleemail.org',
        password: 'monty1',
      })
    } catch (e) {
      login = e.response.body
    }

    const logOnInvalidPasswordSchema = {
      title: 'Logon Invalid Password',
      type: 'object',
      required: ['statusCode', 'error', 'message'],
      properties: {
        statusCode: numberTypePattern(422),
        error: stringTypePattern('Unprocessable Entity'),
        message: stringTypePattern(
          'The email address or password you entered has not been found. Please enter them again.'
        ),
      },
    }
    assert.jsonSchema(login, logOnInvalidPasswordSchema)
  })

  it('Logon Invalid Email', async () => {
    try {
      login = await logIn({
        username: 'faillogon@sampleemail.org',
        password: 'monty1',
      })
    } catch (e) {
      login = e.response.body
    }

    const logOnInvalidEmailSchema = {
      title: 'Logon Invalid Email',
      type: 'object',
      required: ['statusCode', 'error', 'message'],
      properties: {
        statusCode: numberTypePattern(422),
        error: stringTypePattern('Unprocessable Entity'),
        message: stringTypePattern(
          'The email address or password you entered has not been found. Please enter them again.'
        ),
      },
    }
    assert.jsonSchema(login, logOnInvalidEmailSchema)
  })
})
