require('babel-polyfill')

jest.unmock('superagent')
import superagent from 'superagent'

import eps from '../routes_tests'

import {
  addToBagPayload,
  itemWishlist,
  wishlistId,
  wishlistName,
} from '../wishlist/wishlist-data'

export const headers = {
  'Content-Type': 'application/json',
  'BRAND-CODE': 'tsuk',
}

export const createWishlist = async (jsessionid) => {
  const wishlistResponse = await superagent
    .post(eps.wishlist.createWishlist.path)
    .set(headers)
    .set({ Cookie: jsessionid })
    .send(wishlistName)
  return wishlistResponse.body
}

export const addItemToWishlist = async (jsessionid) => {
  const addToItemWishlistResponse = await superagent
    .post(eps.wishlist.addItemWishlist.path)
    .set(headers)
    .set({ Cookie: jsessionid })
    .send(itemWishlist)
  return addToItemWishlistResponse.body
}

export const retrieveWishlist = async (jsessionid) => {
  const getWishlistResponse = await superagent
    .get(eps.wishlist.getWishlist.path)
    .set(headers)
    .set({ Cookie: jsessionid })
    .query(wishlistId)
  return getWishlistResponse.body
}

export const addItemToBagFromWishlist = async (jsessionid) => {
  const addItemToBagFromWishlistResponse = await superagent
    .post(eps.wishlist.addToBagWishlist.path)
    .set(headers)
    .set({ Cookie: jsessionid })
    .send(addToBagPayload)
  return addItemToBagFromWishlistResponse.body
}

export const removeItemFromWishlist = async (jsessionid, wishlistItemId) => {
  const removeItemFromWishlistResponse = await superagent
    .delete(eps.wishlist.deleteItemWishlist.path)
    .set(headers)
    .set({ Cookie: jsessionid })
    .send({ wishlistId: '27003', wishlistItemId })
  return removeItemFromWishlistResponse.body
}
