require('babel-polyfill')

jest.unmock('superagent')
import superagent from 'superagent'

// DELIVERY OPTIONS
import eps from '../routes_tests'

export const PROMOTION_CODE = 'F84MB92NV9'

export const headers = {
  'Content-Type': 'application/json',
  'BRAND-CODE': 'tsuk',
}

export const deliveryOptions = {
  collectFromParcelShop: { deliveryOptionId: '47524' },
  collectFromStoreExpress: { deliveryOptionId: '45020' },
  standardDelivery: { deliveryOptionId: '26504' },
  freeCollectFromStoreStandard: { deliveryOptionId: '45019' },
  expressNominatedDayDelivery: { deliveryOptionId: '28004' },
  collectFromStoreToday: { deliveryOptionId: '51017' },
}

// GET
export const shoppingBagAddProduct = 21248188
export const shoppingBagProductWasPrice = 17699927
// POST
export const shoppingBagAddProductSimple = {
  productId: 21919934,
  sku: '602015000890858',
  quantity: 1,
}
export const shoppingBagAddProductWasPrice = {
  productId: 17699927,
  sku: '602014000789145',
  quantity: 1,
}
export const shoppingBagAddProductWasWasPrice = {
  productId: 21986324,
  sku: '602015000892527',
  quantity: 1,
}
// PUT
export const shoppingBagAddProductWasWasPriceChangeQty = {
  quantity: '2',
  catEntryIdToDelete: 21986331,
  catEntryIdToAdd: 21986331,
}
export const expectedTotalPriceOnChangeQty = '110.00'
export const expectedPriceOnChangeQty = '106.00'
export const shoppingBagAddProductWasWasPriceChangeSize = {
  quantity: '1',
  catEntryIdToDelete: 21986331,
  catEntryIdToAdd: 21986341,
}

export const addItemToShoppingBag = async (jsessionid = '', item = {}) => {
  const product = {
    productId: item.productId,
    sku: item.productSku,
    quantity: item.productQuantity,
  }
  const shoppingBagResponse = await superagent
    .post(eps.shoppingBag.addItem.path)
    .set(headers)
    .set({ Cookie: jsessionid })
    .send(product)
  return shoppingBagResponse.body
}

export const removeItemFromShoppingBag = async (
  jsessionid,
  idOrder,
  idItemOrder
) => {
  const itemTobeRemoved = await superagent
    .delete(eps.shoppingBag.deleteItem.path)
    .query(`orderId=${idOrder}&orderItemId=${idItemOrder}`)
    .set(headers)
    .set({ Cookie: jsessionid })
  return itemTobeRemoved.body
}

export const fetchSizeQty = async (jsessionid, catEntryId) => {
  const fetchBody = await superagent
    .get(eps.shoppingBag.fetchSizeQty.path)
    .set(headers)
    .set({ Cookie: jsessionid })
    .query({
      catEntryId,
    })
  return fetchBody.body
}

export const updateShoppingBagItem = async (
  jsessionid,
  catEntryIdToAdd,
  catEntryIdToDelete,
  quantity
) => {
  const payload = {
    catEntryIdToAdd,
    catEntryIdToDelete,
    quantity,
  }
  const updateShoppingBagBody = await superagent
    .put(eps.shoppingBag.updateItem.path)
    .set(headers)
    .set({ Cookie: jsessionid })
    .send(payload)
  return updateShoppingBagBody.body
}

export const updateShoppingBagDelivery = async (
  jsessionid,
  deliveryOptionsId
) => {
  const shoppingBagDeliveryBody = await superagent
    .put(eps.shoppingBag.updateDelivery.path)
    .set(headers)
    .set({ Cookie: jsessionid })
    .send(deliveryOptionsId)
  return shoppingBagDeliveryBody.body
}

export const promotionCode = async (jsessionid) => {
  const addPromoBody = await superagent
    .post(eps.shoppingBag.addPromotionCode.path)
    .set(headers)
    .set({ Cookie: jsessionid })
    .send({ promotionId: PROMOTION_CODE })
  return addPromoBody.body
}

export const deletePromotionCode = async (jsessionid) => {
  const removePromoBody = await superagent
    .delete(eps.shoppingBag.deletePromotionCode.path)
    .set(headers)
    .set({ Cookie: jsessionid })
    .send({ promotionCode: PROMOTION_CODE })
  return removePromoBody.body
}
