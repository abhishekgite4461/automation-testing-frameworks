import { headers } from '../utilis'
import superagent from 'superagent'

import eps from '../routes_tests'
import generateAddress from '../utilis/address'
import nameAndPhone from '../utilis/nameAndPhone'
import pay from '../utilis/creditCard'

require('babel-polyfill')

jest.unmock('superagent')

const CARD_CVV = '123'
const REMOTE_IP = '127.0.0.1'
const SHIPPING_COUNTRY = 'United Kingdom'
const SHIPPING_CODE = 'S'
const DELIVERY_TYPE = 'HOME_STANDARD'
const SHIPPING_MODE_ID = 26504

const payOrderPayload = (orderId, paymentType) => {
  return {
    smsMobileNumber: '',
    remoteIpAddress: REMOTE_IP,
    orderDeliveryOption: {
      orderId,
      shippingCountry: SHIPPING_COUNTRY,
      shipCode: SHIPPING_CODE,
      deliveryType: DELIVERY_TYPE,
      shipModeId: SHIPPING_MODE_ID,
    },
    deliveryInstructions: '',
    deliveryAddress: generateAddress.address(),
    deliveryNameAndPhone: nameAndPhone(),
    billingDetails: {
      address: generateAddress.address(),
      nameAndPhone: nameAndPhone(),
    },
    returnUrl: eps.orders.orderCompleteReturnUrl.path(paymentType),
    save_details: true,
  }
}

// Pass the paymentType using the abbreviation all uppercase
const paymentMethodPayload = (orderId, paymentType) => {
  const { paymentMethod, applePayToken } = pay

  const mainBody = payOrderPayload(orderId, paymentType)

  if (paymentType === 'APPLE') {
    return {
      ...mainBody,
      applePayToken,
    }
  }

  return {
    ...mainBody,
    creditCard: paymentMethod(paymentType),
    ...(paymentType === 'PYPAL' || paymentType === 'MPASS'
      ? null
      : { cardCvv: CARD_CVV }),
  }
}

export const payOrder = async (jsessionid, orderId, payment = 'VISA') => {
  return superagent
    .post(eps.orders.payOrder.path)
    .set(headers)
    .set({ Cookie: jsessionid })
    .send(paymentMethodPayload(orderId, payment))
}
