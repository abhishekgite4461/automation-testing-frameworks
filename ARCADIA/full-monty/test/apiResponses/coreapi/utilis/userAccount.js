jest.unmock('superagent')
import superagent from 'superagent'

const randomstring = require('randomstring')

import eps from '../routes_tests'
import { headers } from '../utilis'
import { reauthenticationMobileAppPayload } from '../logon/logon-data'

const PASSWORD = 'monty1'

// PAYLOAD FUNCTIONS
const userShortProfile = () => {
  const email = randomstring.generate({ length: 6, charset: 'alphabetic' })
  const firstName = randomstring.generate({ length: 6, charset: 'alphabetic' })
  const lastName = randomstring.generate({ length: 6, charset: 'alphabetic' })
  const titleList = ['Mr', 'Mrs', 'Ms', 'Miss', 'Dr']
  const title = titleList[Math.floor(titleList.length * Math.random())]
  const marketingSubscription = false
  return {
    email: `${email.toLowerCase()}@sample.org`,
    firstName: `FirstName_${firstName}`,
    lastName: `LastName_${lastName}`,
    title,
    marketingSubscription,
  }
}

// Address ITEM
const address = () => {
  const postCodeList = [
    'NW1 5QD',
    'IP3 0BL',
    'W1T 3NL',
    'EC3N 1HP',
    'W6 8BT',
    'SW6 7ST',
  ]
  const postCode = postCodeList[Math.floor(postCodeList.length * Math.random())]
  const cityList = [
    'London',
    'Manchester',
    'Norwich',
    'Ipswich',
    'York',
    'Brighton',
  ]
  const city = cityList[Math.floor(postCodeList.length * Math.random())]
  return {
    country: 'United Kingdom',
    postcode: postCode,
    address1: randomstring
      .generate({
        length: 6,
        charset: `alphabetic`,
      })
      .toLowerCase(),
    address2: randomstring
      .generate({
        length: 6,
        charset: `alphabetic`,
      })
      .toLowerCase(),
    city,
    state: '',
  }
}
// NAME AND PHONE ITEM
const nameAndPhone = () => ({
  title: 'Mr',
  firstName: randomstring
    .generate({
      length: 6,
      charset: `alphabetic`,
    })
    .toLowerCase(),
  lastName: randomstring
    .generate({
      length: 6,
      charset: `alphabetic`,
    })
    .toLowerCase(),
  telephone: `07${randomstring
    .generate({
      length: 8,
      charset: `numeric`,
    })
    .toLowerCase()}`,
})

// CHANGE CHECKOUT DETAILS PAYLOAD
const changeFullDetails = (type, cardNumber) => {
  return {
    billingDetails: {
      nameAndPhone: nameAndPhone(),
      address: address(),
    },
    deliveryDetails: {
      nameAndPhone: nameAndPhone(),
      address: address(),
    },
    creditCard: {
      expiryYear: '2020',
      expiryMonth: '07',
      type,
      cardNumber,
    },
  }
}

const createUserCredentials = ({ subscribe = true, appId } = {}) => {
  const email = randomstring.generate({ length: 6, charset: `alphabetic` })
  return {
    email: `${email.toLowerCase()}@sample.org`,
    password: PASSWORD,
    passwordConfirm: PASSWORD,
    subscribe,
    appId,
  }
}

/* API CALLS */
export const createAccount = async ({
  deviceType = '',
  payload = undefined,
  appId,
  subscribe = true,
} = {}) => {
  const accountBody = await superagent
    .post(eps.myAccount.register.path)
    .set(headers)
    .set('Cookie', `deviceType=${deviceType}`)
    .send(payload || createUserCredentials({ appId, subscribe }))
  const sessionId = accountBody.headers['set-cookie'].toString().split(';')
  const jsessionid = sessionId[0]
  return {
    accountProfile: accountBody.body,
    jsessionid,
  }
}

export const getUserAccount = async (jsessionid) => {
  const accountBody = await superagent
    .get(eps.myAccount.account.path)
    .set(headers)
    .set({ Cookie: jsessionid })
  return accountBody.body
}

export const logIn = async ({
  username,
  password,
  deviceType = '',
  payload,
}) => {
  const postBody = payload || { username, password }
  const loginBody = await superagent
    .post(eps.myAccount.login.path)
    .set(headers)
    .set('Cookie', `deviceType=${deviceType}`)
    .send(postBody)
  return loginBody.body
}
// Adding logIn for Wishlist with jsessioId
export const logInWithJsessionId = async (
  username,
  password,
  deviceType = ''
) => {
  const payload = { username, password }
  const loginBody = await superagent
    .post(eps.myAccount.login.path)
    .set(headers)
    .set('Cookie', `deviceType=${deviceType}`)
    .send(payload)
  const sessionId = loginBody.headers['set-cookie'].toString().split(';')
  const jsessionid = sessionId[0]
  return {
    body: loginBody.body,
    jsessionid,
  }
}

export const logOut = async (platform = 'web') => {
  const payloadNativeApp = {
    userToken: 'UmljayBTY2h1YmVydCB3YXMgaGVyZQ0K',
    appId: '1234-1234-1234-1234',
    userId: 654321,
  }
  const logOutBody = await superagent
    .delete(eps.myAccount.logout.path)
    .set(headers)
    .send(platform !== 'web' && payloadNativeApp)
  return logOutBody
}

export const changePassword = async (
  jsessionid,
  emailAddress,
  platform = 'web'
) => {
  const payload = {
    emailAddress,
    newPassword: 'monty123',
    newPasswordConfirm: 'monty123',
    oldPassword: 'monty1',
  }
  const payloadNativeApp = {
    ...payload,
    ...reauthenticationMobileAppPayload,
  }
  const changePasswordBody = await superagent
    .put(eps.myAccount.changePassword.path)
    .set(headers)
    .set({ Cookie: jsessionid })
    .send(platform === 'web' ? payload : payloadNativeApp)
  return {
    newPasswordBody: changePasswordBody.body,
    newPassword: payload.newPassword,
  }
}

export const forgottenPassword = async (email) => {
  const forgottenPasswordBody = await superagent
    .post(eps.myAccount.forgotPassword.path)
    .set(headers)
    .send({ email })
  return forgottenPasswordBody.body
}

export const updateCheckoutDetails = async (jsessionid, type, cardNumber) => {
  const payload = changeFullDetails(type, cardNumber)
  const checkoutDetailsBody = await superagent
    .put(eps.myAccount.customerDetails.path)
    .set(headers)
    .set({ Cookie: jsessionid })
    .send(payload)
  return checkoutDetailsBody.body
}

export const updateAccountShortProfile = async (jsessionid) => {
  const shortProfileUpdate = userShortProfile()
  const shortProfileBody = await superagent
    .put(eps.myAccount.accountShortProfile.path)
    .set(headers)
    .set({ Cookie: jsessionid })
    .send(shortProfileUpdate)
  return shortProfileBody.body
}

const registerNewUserIfNotYetExisting = async (payload) => {
  const newUserData = await createAccount({ payload })
  if (newUserData.accountProfile.exists !== true)
    throw new Error(
      'Retried registering a fresh user and it was still unsuccessful.'
    )
}

export const loginAsNewUserAndRegisterIfNotExisting = async (payload) => {
  let response
  try {
    response = await logIn({ payload })
  } catch (error) {
    const errorResponse = JSON.parse(error.response.text)
    if (errorResponse.error === 'Unprocessable Entity')
      await registerNewUserIfNotYetExisting({
        email: payload.username,
        password: payload.password,
        passwordConfirm: payload.password,
        subscribe: false,
      })
    response = await logIn({ payload })
  }
  return response
}
