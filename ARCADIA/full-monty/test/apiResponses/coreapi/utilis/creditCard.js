const applePayToken = { paymentToken: 'testString' }

const cards = {
  VISA: '1111222233334444',
  MCARD: '5555555555554444',
  AMEX: '34343434343434',
  ACCNT: '6000082000000005',
  PYPAL: '0',
  MPASS: '0',
  KLARNA_UK: '0',
}

const paymentMethod = (method) => {
  return {
    expiryYear: '2020',
    expiryMonth: '12',
    cardNumber: cards[method],
    type: method,
  }
}

export default {
  paymentMethod,
  applePayToken,
}
