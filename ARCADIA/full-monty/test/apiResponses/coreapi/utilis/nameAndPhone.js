const randomstring = require('randomstring')

const nameAndPhone = () => ({
  title: 'Mr',
  firstName: randomstring
    .generate({
      length: 6,
      charset: `alphabetic`,
    })
    .toLowerCase(),
  lastName: randomstring
    .generate({
      length: 6,
      charset: `alphabetic`,
    })
    .toLowerCase(),
  telephone: `07${randomstring
    .generate({
      length: 8,
      charset: `numeric`,
    })
    .toLowerCase()}`,
})

export default nameAndPhone
