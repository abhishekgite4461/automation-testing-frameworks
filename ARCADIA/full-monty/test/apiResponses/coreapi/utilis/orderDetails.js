jest.unmock('superagent')

import superagent from 'superagent'
import { headers, userCredentialsOrdersAndReturnsProfile } from '../utilis'
import eps from '../routes_tests'

const getOrderHistory = async () => {
  // TODO change the userAccount util to return response header and import here to remove repetition.
  const resHeader = await superagent
    .post(eps.myAccount.login.path)
    .set(headers)
    .send(userCredentialsOrdersAndReturnsProfile)
  const jsessionid = resHeader.headers['set-cookie'].toString().split(';')[0]
  const response = await superagent
    .get(eps.myAccount.orderHistory.path)
    .set(headers)
    .set({ Cookie: jsessionid })
  return {
    orderHistoryBody: response.body,
    jsessionid,
  }
}

export const savedOrderIds = async () => {
  const ids = []
  const orderNumbers = await getOrderHistory()
  const jsessionid = orderNumbers.jsessionid
  const body = orderNumbers.orderHistoryBody.orders
  body.forEach((obj) => {
    ids.push(obj.orderId)
  })
  return {
    ids,
    jsessionid,
  }
}

export const getOrderDetails = async (orderId, jsession) => {
  const responseBody = await superagent
    .get(eps.myAccount.orderHistoryDetails.path(orderId))
    .set(headers)
    .set({ Cookie: jsession })
  return responseBody.body
}

export const getOrderDetailsForAllOfOrderHistory = async () => {
  const testData = []
  const { ids, jsessionid } = await savedOrderIds()
  const populateTestDataWithOrderDetails = async (orderItem) => {
    const orderDetails = await getOrderDetails(orderItem, jsessionid)
    testData.push({
      orderItem,
      orderDetails,
    })
  }
  const promises = ids.map(populateTestDataWithOrderDetails)
  await Promise.all(promises)
  return testData
}
