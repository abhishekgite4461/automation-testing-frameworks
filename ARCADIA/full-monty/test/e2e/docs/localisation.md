# Localisation 

* There are currently two brands with formal German: Wallis DE and Evans DE
* Languages used in individual brand code is mapped within e2e/config/brands.js as well, with formal German set with property 'formal: true'