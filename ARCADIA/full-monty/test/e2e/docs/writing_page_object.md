Writing Page Objects
====================

```
import * as <import_name> from '<import_path>'
//-------------------------------
// Main container identifier
const mobileContainerClass = '<main_container>'
const desktopContainerClass = '<main_container>'

// page element identifiers
const element1_class = '<ele1>'
 
//-------------------------------
const containerClass = client.hasDesktopLayout() ? desktopContainerClass : mobileContainerClass

export const getMainContainer = () => {
overlayPO.waitForOverlay()
  try {
    browser.waitForVisible(containerClass)
    return browser.$(containerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage('[ERROR CAPTURE] container did not appear', err, 'FAILED-TO-GET-CONTAINER')
  }
}

export const getElementOne = () => {
    const container = getMainContainer()
    return container.$(element1_class)
}

export const setElementOne = () => {
  const container = getMainContainer()
  container.setValue(element1_class, 'value')
}


export const performAction = () => {
  getElementOne()
  setElementOne()
}
```
