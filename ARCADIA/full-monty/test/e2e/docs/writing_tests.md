Writing tests
=============
 
* No new code added to files in pageObject folder as it is deprecated.   
* Run eslint against files changed, please make sure it passes linting
* Unless a test is written to test desktop or mobile-specific functionality, please ensure it is written in a format that 
will work for both layout. Please make sure to append '-des' in feature file if it is a desktop-specific implementation. E.g.: carousel-des.feature
* Please use the following naming convention _&lt;page&gt;-&lt;component/section&gt;-&lt;action&gt;.feature_ or _&lt;journey&gt;-&lt;component/section&gt;-&lt;action&gt;.feature_
  
  E.g.:
  
  deliveryDetails-deliveryCountry-loggedIn.feature 
  
  quickSearch-noResult.feature
 
  fullProfile-expressDelivery.feature
  
  guest-collectFromStore.feature
* localise all strings in test. This can be done by adding in

 ```
    const l = require('../lib/localisation').init()
    
    and then wrapping the string with the l function
    
    l`string`
```
