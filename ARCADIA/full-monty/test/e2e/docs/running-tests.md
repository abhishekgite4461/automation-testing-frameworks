# Running E2E tests

## Requirements
* API_KEY and API_URL for the environment that you want to run on. E.g.: stage.api and monty.api. If you do not have this key, please see Ricardo who can help provide you one.
* Add entries from hosts.md to your local host

## Running tests

### Step 1: Configure test environment

Update API_KEY and API_URL in .env file to the right environment (e.g. ebt.api, stage.api or montyprod.api)
Start Monty application using command below. This will run monty application with CMS mocks enabled. 

Note: ```npm run e2e``` will run Monty application in production mode, this uses port 3000. E.g.: http://local.m.topshop.com:3000

    $ CMS_TEST_MODE=true npm run e2e

### Step 2: Run test
Open a new terminal in the project root and run E2E test using

    $ npm run e2e:test

This will run against TSUK brand, you can override the settings by passing in arguments. Examples below.

#### Run test against Burton UK on staging

    $ WDIO_ENVIRONMENT=stage WDIO_BRAND=bruk npm run e2e:test

#### or any brand on any environment
Environment: local | stage | integration | production
Brand: Please use existing brand code or refer to models/lib/brands.js

    $ WDIO_ENVIRONMENT=integration WDIO_BRAND=tmfr npm run e2e:test

    $ WDIO_ENVIRONMENT=production WDIO_BRAND=tsde npm run e2e:test

#### Run checkout tests

    $ FEATURES_PATH='test/e2e/features/checkoutFull/*.feature' WDIO_ENVIRONMENT=stage npm run e2e:test

## Debugging with Repl 

Run the following command, it should open an empty Chrome browser

    $ ./node_modules/.bin/wdio repl chrome
    
Once you have that, you can then run any WDIO command
 
    $ # open m.topshop.com
    $ browser.url('https://m.topshop.com')
    $ # retrieve homepage title
    $ browser.getTitle()
