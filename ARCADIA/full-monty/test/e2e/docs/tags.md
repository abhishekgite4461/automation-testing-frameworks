# Cucumber tags
This read me lists all the cucumbers tags in the test suite and a description of there purpose.

# Tags

@wip = This tag means "Work In Progress". Test has not been written and will fail or return unimplemented

@broken = This tag indicates the test is broken and requires investigation before it can be run again. Either there is a break in the system or with the test.

@tsuk_only = This tag indicates that the test is configured to only run on topshop UK. These tests need to be looked at and fixed on a individual basis to work across brands and territories.

@mobile = These tests are ```mobile``` only and will fail when ran in desktop mode

@desktop = these tests are ```desktop``` only and will fail if ran in mobile

@regression = Regression tests marked, this selection is a replica of the old Java tests suites regression test suite

@smoke = the production smoke tests, these are mostly checkout tests and will only run on ```monty-prod``` or ```prod``` due to the response expected from the psp (ie declined)

@realCheckout = runs all the realCheckout tests, these can only be ran on a ```stage``` like environment due to the response expected from the psp (ie successful)

@paypal = paypal tests

@3dsecure = verified by visa

@klarna = klarna payment method

@visa = standard visa flow

@not_stage_env = don't run these on stage environment (i.e. integration, showcase, any temporary environment)
