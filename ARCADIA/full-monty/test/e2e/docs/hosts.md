# Insert the following in your local host file

    # Burton
    127.0.0.1     local.m.eu.burton-menswear.com
    127.0.0.1     local.m.burton.co.uk

    # Evans
    127.0.0.1     local.m.evansmode.de
    127.0.0.1     local.m.euro.evansfashion.com
    127.0.0.1     local.m.evans.co.uk
    127.0.0.1     local.m.evansusa.com

    # Miss Selfridge
    127.0.0.1     local.m.euro.missselfridge.com
    127.0.0.1     local.m.missselfridge.fr
    127.0.0.1     local.m.missselfridge.de
    127.0.0.1     local.m.us.missselfridge.com
    127.0.0.1     local.m.missselfridge.com

    # Dorothy Perkins
    127.0.0.1     local.m.euro.dorothyperkins.com
    127.0.0.1     local.m.us.dorothyperkins.com
    127.0.0.1     local.m.dorothyperkins.com

    # Topman
    127.0.0.1     local.m.eu.topman.com
    127.0.0.1     local.m.fr.topman.com
    127.0.0.1     local.m.topman.com
    127.0.0.1     local.m.de.topman.com
    127.0.0.1     local.m.us.topman.com

    # Topshop
    127.0.0.1     local.m.eu.topshop.com
    127.0.0.1     local.m.fr.topshop.com
    127.0.0.1     local.m.de.topshop.com
    127.0.0.1     local.m.us.topshop.com
    127.0.0.1     local.m.topshop.com

    # Wallis
    127.0.0.1     local.m.wallismode.de
    127.0.0.1     local.m.wallis.co.uk
    127.0.0.1     local.m.euro.wallisfashion.com
    127.0.0.1     local.m.wallisfashion.com
