export default {
  ts: {
    common:
      'black dresses,tops,watches,socks,jumpers,shorts,long sleeve shirts,low heels,black,red,yellow,white,tall,small',
    multiSize: 'moto jeans,shoes,ankle boots,heels,black dresses',
    singleSize: 'mirror,faux leather bag,watch',
    colour: 'red,black,white,yellow,green,orange,blue,brown,grey,purple',
  },
  br: {
    common: 'suit,shirt,jacket,shoes',
    multiSize: 'shoes,boots,shirt',
  },
}
