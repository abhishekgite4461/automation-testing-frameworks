export default {
  GB: {
    // UK DEFAULTS
    country: 'United Kingdom',
    postcode: 'E12 6NG',
    telephone: '07891234567',
    title: 'Mr',
    firstName: 'tester',
    lastName: 'testerton',
  },
  US: {
    // US CUSTOMER DETAILS
    country: 'United States',
    postcode: '34333',
    telephone: '212-243-8560',
  },
  FR: {
    // FRENCH CUSTOMER DEFAULTS
    country: 'France',
    postcode: '70058',
    telephone: '0123456789',
    title: 'Dr',
    firstName: 'le_tester',
    lastName: 'le_testerton',
  },
  DE: {
    // GERMAN CUSTOMER DEFAULTS
    country: 'Germany',
    postcode: '10243',
    telephone: '0123456789',
    title: 'Dr',
    firstName: 'Hans',
    lastName: 'Gruber',
  },
  NL: {
    // DUTCH CUSTOMER DEFAULTS
    country: 'Netherlands',
    postcode: '5683 AI',
    telephone: '06 3849 8657',
    title: 'Mr',
    firstName: 'Thomas',
    lastName: 'Ven',
  },
  CN: {
    // CHINA CUSTOMER DEFAULTS
    country: 'China',
    postcode: '266033 SHANDONG',
    telephone: '06 3849 8657',
    title: 'Mr',
    firstName: 'ZHIMIN',
    lastName: 'LI',
  },
}
