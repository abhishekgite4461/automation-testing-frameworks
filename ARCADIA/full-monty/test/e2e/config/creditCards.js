export default {
  VISA: 'Visa',
  MCARD: 'MasterCard',
  AMEX: 'American Express',
  SWTCH: 'Maestro',
  ALIPY: 'Alipay',
  PYPAL: 'PayPal',
  CUPAY: 'China UnionPay',
  SOFRT: 'Sofort',
  IDEAL: 'iDEAL',
  MPASS: 'MasterPass',
  KLRNA: 'Klarna',
}
