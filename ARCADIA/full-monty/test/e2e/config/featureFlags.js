export const setFeatureFlags = () => {
  // Disable features which e2ev1 cannot test against
  browser.setCookie({
    name: 'featuresOverride',
    value: JSON.stringify({
      PASSWORD_SHOW_TOGGLE: false,
      FEATURE_ORDER_HISTORY_MSG: false,
      FEATURE_RESPONSIVE: false,
      FEATURE_GEOIP: false,
      FEATURE_PRODUCT_CAROUSEL_THUMBNAIL: false,
      FEATURE_SHOP_THE_LOOK: false,
      FEATURE_SAVE_PAYMENT_DETAILS: false,
      FEATURE_SHOW_FIT_ATTRIBUTE_LINKS: false,
      FEATURE_TRANSFER_BASKET: false,
      FEATURE_MY_CHECKOUT_DETAILS: false,
      FEATURE_PRODUCT_DESCRIPTION_SEE_MORE: false,
      FEATURE_ADDRESS_BOOK: false,
      FEATURE_MEGA_NAV: false,
      FEATURE_DESKTOP_RESET_PASSWORD: false,
      FEATURE_CVV_HELP: false,
      FEATURE_LEGACY_CAT_HEADERS_IFRAME: false,
      FEATURE_LEGACY_PAGES: false,
    }),
  })
}
