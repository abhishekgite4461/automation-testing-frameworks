export default {
  'My Account': '/my-account',
  'Change Password': '/my-account/my-password',
  'Store Locator': '/store-locator',
  Login: '/login',
  'My Details': '/my-account/my-details',
  'My Orders': '/my-account/order-history',
}
