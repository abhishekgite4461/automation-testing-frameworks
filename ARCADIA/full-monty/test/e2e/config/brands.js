export default {
  breu: {
    url: 'm.eu.burton-menswear.com',
    brandName: 'burton',
    language: 'en-gb',
  },
  bruk: {
    url: 'm.burton.co.uk',
    brandName: 'burton',
    language: 'en-gb',
  },
  evde: {
    url: 'm.evansmode.de',
    brandName: 'evans',
    language: 'de-DE',
    formal: true,
  },
  eveu: {
    url: 'm.euro.evansfashion.com',
    brandName: 'evans',
    language: 'en-gb',
  },
  evuk: {
    url: 'm.evans.co.uk',
    brandName: 'evans',
    language: 'en-gb',
  },
  evus: {
    url: 'm.evansusa.com',
    brandName: 'evans',
    language: 'en-gb',
  },
  mseu: {
    url: 'm.euro.missselfridge.com',
    brandName: 'missselfridge',
    language: 'en-gb',
  },
  msfr: {
    url: 'm.missselfridge.fr',
    brandName: 'missselfridge',
    language: 'fr-fr',
  },
  msde: {
    url: 'm.missselfridge.de',
    brandName: 'missselfridge',
    language: 'de-DE',
  },
  msus: {
    url: 'm.us.missselfridge.com',
    brandName: 'missselfridge',
    language: 'en-gb',
  },
  msuk: {
    url: 'm.missselfridge.com',
    brandName: 'missselfridge',
    language: 'en-gb',
  },
  dpeu: {
    url: 'm.euro.dorothyperkins.com',
    brandName: 'dorothyperkins',
    language: 'en-gb',
  },
  dpus: {
    url: 'm.us.dorothyperkins.com',
    brandName: 'dorothyperkins',
    language: 'en-gb',
  },
  dpuk: {
    url: 'm.dorothyperkins.com',
    brandName: 'dorothyperkins',
    language: 'en-gb',
  },
  tmeu: {
    url: 'm.eu.topman.com',
    brandName: 'topman',
    language: 'en-gb',
  },
  tmfr: {
    url: 'm.fr.topman.com',
    brandName: 'topman',
    language: 'fr-fr',
  },
  tmuk: {
    url: 'm.topman.com',
    brandName: 'topman',
    language: 'en-gb',
  },
  tmde: {
    url: 'm.de.topman.com',
    brandName: 'topman',
    language: 'de-DE',
  },
  tmus: {
    url: 'm.us.topman.com',
    brandName: 'topman',
    language: 'en-gb',
  },
  tseu: {
    url: 'm.eu.topshop.com',
    brandName: 'topshop',
    language: 'en-gb',
  },
  tsfr: {
    url: 'm.fr.topshop.com',
    brandName: 'topshop',
    language: 'fr-fr',
  },
  tsde: {
    url: 'm.de.topshop.com',
    brandName: 'topshop',
    language: 'de-DE',
  },
  tsus: {
    url: 'm.us.topshop.com',
    brandName: 'topshop',
    language: 'en-gb',
  },
  tsuk: {
    url: 'm.topshop.com',
    brandName: 'topshop',
    language: 'en-gb',
  },
  wlde: {
    url: 'm.wallismode.de',
    brandName: 'wallis',
    language: 'de-DE',
    formal: true,
  },
  wluk: {
    url: 'm.wallis.co.uk',
    brandName: 'wallis',
    language: 'en-gb',
  },
  wleu: {
    url: 'm.euro.wallisfashion.com',
    brandName: 'wallis',
    language: 'en-gb',
  },
  wlus: {
    url: 'm.wallisfashion.com',
    brandName: 'wallis',
    language: 'en-gb',
  },
}
