Feature: Shopping bag > Remove Product

  As a customer
  I would like an option to edit my shopping bag
  So that i can make changes to my orders before making the purchase

  Background:
    Given the set up is done

  @tsuk_only
  Scenario: Customer removes an item from shopping bag
    Given The shopping bag contains a product
    And The shopping bag contains a high value product over £100
    And I navigate to the shopping bag
    And I check the total of the shopping bag
    When I remove a product from the shopping bag
    And I confirm deletion
    Then The shopping bag should not be empty
    And I re-check the total of the shopping bag has changed
