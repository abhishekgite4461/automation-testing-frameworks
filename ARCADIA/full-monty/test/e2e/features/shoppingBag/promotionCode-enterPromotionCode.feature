Feature: Shopping Bag > Promotion Code

  As a customer
  I want to enter a promotion code into my shopping bag
  So that I can get money off the total cost of my bag

  MON-320: Add Promotion

  Background:
    Given the set up is done
    And The shopping bag contains a product valued above £100
    And I open the shopping bag
    And I open the promotion code accordion

  @tsuk_only
  Scenario: Customer enters promotion code
    When I enter and submit the promotion code "TSW15%"
    Then the shopping bag total should be reduced by 15%
