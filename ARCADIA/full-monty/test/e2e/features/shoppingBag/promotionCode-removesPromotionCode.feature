Feature: Shopping Bag > Promotion Code - remove code

  As a customer
  I want to enter a promotion code into my shopping bag
  So that I can get money off the total cost of my bag

  MON-320: Add Promotion

  Background:
    Given the set up is done
    And The shopping bag contains a product valued above £100
    And I open the shopping bag
    And I open the promotion code accordion

  @tsuk_only
  Scenario: Customer enters and removes promotion code
    When I enter and submit the promotion code "TSW15%"
    And the promotion code has been applied
    When I remove the promotion code
    Then the promotion should be removed from the price
    And I should see an empty list of promotion codes
