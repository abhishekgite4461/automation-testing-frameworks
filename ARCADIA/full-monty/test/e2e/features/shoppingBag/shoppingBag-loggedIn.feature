Feature: Shopping bag > View Shopping Bag - logged in

  As a customer
  I would like to view my shopping bag page during my purchase journey
  So that i am aware of the items that have been added to cart

  Background:
    Given the set up is done

  @regression
  Scenario: 2. Authenticated customer can view shopping bag
    Given I have registered as a new user
    And The shopping bag contains a product
    When I navigate to the shopping bag
    Then The shopping bag should not be empty
    And I can edit the product
    And I can remove the product
    And I empty the shopping bag
