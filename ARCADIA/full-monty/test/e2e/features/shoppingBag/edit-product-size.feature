Feature: Shopping bag -> Edit Product

  As a customer
  I would like an option to edit my shopping bag
  So that i can make changes to my orders before making the purchase

  Background:
    Given the set up is done


  Scenario: Customer can edit product size
    And A product with different sizes has been added to the shopping bag
    And I navigate to the shopping bag
    And I edit a product
    When I change the product size and save the product
    Then The product size should be updated
