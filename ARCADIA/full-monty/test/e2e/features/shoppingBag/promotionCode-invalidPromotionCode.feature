Feature: Shopping Bag > Promotion Code - Invalid code

  As a customer
  I want to enter a promotion code into my shopping bag
  So that I can get money off the total cost of my bag

  Background:
    Given the set up is done
    And The shopping bag contains a high value product over £100
    And I open the shopping bag
    And I open the promotion code accordion

  Scenario: Customer enters invalid promotion code
    When I enter the promotion code "IMNOTVALID"
    And I submit the promotion code
    Then The promotion form should show a server error: "The code you have entered has not been recognised. Please confirm the code and try again."
