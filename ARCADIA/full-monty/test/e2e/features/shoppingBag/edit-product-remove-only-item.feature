Feature: Shopping bag > Edit Product

  As a customer
  I would like an option to edit my shopping bag
  So that i can make changes to my orders before making the purchase

  Background:
    Given the set up is done

  @regression @tsuk_only
  Scenario: Customer removes the only product from shopping bag
    Given The shopping bag contains a product
    And I navigate to the shopping bag
    When I remove a product from the shopping bag
    And I confirm deletion
    Then The shopping bag should be empty
