Feature: Shopping Bag > Remove product

  As a customer
  I would like to remove products from my shopping bag
  So that I can purchase the correct products

  Background:
    Given the set up is done


  Scenario: Customer closes the delete confirm overlay without confirming the deletion of product by clicking outside the modal
    Given The shopping bag contains a product
    And I navigate to the shopping bag
    When I remove a product from the shopping bag
    And I click outside delete confirm overlay
    Then The shopping bag should not be empty
