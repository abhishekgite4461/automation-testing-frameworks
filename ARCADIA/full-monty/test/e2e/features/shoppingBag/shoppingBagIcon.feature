Feature: Shopping Bag > Shopping bag icon

  Background:
    Given the set up is done

  @regression
  Scenario: shopping bag icon still shows items in the bag after refresh
    Given The shopping bag contains a product
    When I reload the page
    Then the shopping bag header icon should indicate it has an item in it