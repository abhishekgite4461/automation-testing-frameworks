Feature: Shopping Bag > Remove product - not confirm

  As a customer
  I would like to remove products from my shopping bag
  So that I can purchase the correct products

  Background:
    Given the set up is done

  #put 2 files together

  @tsuk_only
  Scenario: Customer closes the delete confirm overlay without confirming the deletion of product by closing it
    Given The shopping bag contains a product
    And I navigate to the shopping bag
    When I remove a product from the shopping bag
    And I do not confirm deletion
    Then The shopping bag should not be empty


