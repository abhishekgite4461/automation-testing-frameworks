Feature: Shopping Bag > Remove product - empty bag
  As a customer
  I would like to view my shopping bag page during my purchase journey
  So that I am aware of the items that have been added to cart

  Background:
    Given the set up is done

  @regression
  Scenario: 1. Empty shopping bag displays message
    When I navigate to the shopping bag
    Then The shopping bag should be empty
