Feature: Shopping bag > Edit Product
  As a customer
  I would like an option to edit my shopping bag
  So that i can make changes to my orders before making the purchase

  Background:
    Given the set up is done

  @regression
  Scenario: Customer can edit quantity
    And the shopping bag contains a product with multiple items in stock
    And I edit a product
    When I change the product quantity and save the product
    Then The product quantity should be updated








