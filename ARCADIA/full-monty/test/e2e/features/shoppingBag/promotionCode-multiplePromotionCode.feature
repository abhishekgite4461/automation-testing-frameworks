Feature: Shopping Bag > Promotion Code - Multiple code

  As a customer
  I want to enter a promotion code into my shopping bag
  So that I can get money off the total cost of my bag

  MON-320: Add Promotion

  Background:
    Given the set up is done
    And The shopping bag contains a product valued above £100
    And I open the shopping bag
    And I open the promotion code accordion

  @tsuk_only
  Scenario: Customer enters multiple promotion codes
    And I enter and submit the promotion code "TSDEL0103"
    And I should see "TSDEL0103" in the list of promotions
    When the shopping bag total should not be effected
    And I click add another promotion code
    And I enter and submit the promotion code "TSDEL0103"
    Then The promotion form should show a server error: "This promotion TSDEL0103 has already been applied to this order"
    And I enter and submit the promotion code "TSCARD1"
    And the shopping bag total should be reduced by "5" pounds
