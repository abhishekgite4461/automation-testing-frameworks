@myAccountDetails
Feature: My Account -> My Details

  As a customer
  I want to be able to update my details
  So that I can ensure my information is updated

  Background:
    Given the set up is done

  @backToMyAccount
  Scenario: User can navigate back to My Account
    And I log in with mydeets7@arcadiatest.com and password password1
    When I am on My Details page
    Then I should see back to My Account

  @exUserDetailsPreFilled @tsuk_only
  Scenario: Existing user should see My details pre-filled
    And I log in with myaccount-test2@arcadiatest.com and password password1
    And the account has the default my account details
    When I am on My Details page
    Then I should see my account details pre-filled
      | field        | value                           |
      | title        | Mr                              |
      | firstName    | Faisal                          |
      | lastName     | Khaliquee                       |
      | emailAddress | myaccount-test2@arcadiatest.com |

  @updateUserDetails
  Scenario: User can update account details
    And I have registered as a new user
    And I select My Details
    When I edit my account details
    Then I should see message "Your profile details have been successfully updated."

  @updateBlankEmail
  Scenario: User tries to update my details with blank email address
    And I log in with mydeets5@arcadiatest.com and password password1
    And I am on My Details page
    When I enter my details form without email address
    Then I should see validation message indicating I must enter my email address

  @updateInvalidEmail
  Scenario: User tries to update my details with invalid email address
    And I log in with mydeets6@arcadiatest.com and password password1
    And I am on My Details page
    When I enter my details form with an invalid email address
    Then I should see validation message indicating that I must enter a valid email address

  @updateBlankFirstName @tsuk_only
  Scenario: User tries to update my details with blank first name
    And I log in with mydeets8@arcadiatest.com and password password1
    And I am on My Details page
    When I enter my details form without first name
    Then I should see validation message indicating I must enter my first name

  @updateBlankLastName
  Scenario: User tries to update my details with blank last name
    And I log in with mydeets4@arcadiatest.com and password password1
    And I am on My Details page
    When I enter my details form without last name
    Then I should see validation message indicating I must enter my last name
