@register
Feature: My Account -> Register Account

	As a user
	I can register an account
	So I can purchase and have all the frills of a registered account

    Background:
      Given the set up is done
      And I am on the registration page

  @registerBlankEmail
  Scenario: User tries to register with a blank email
    When I enter the registration details requested but leave the email blank
    Then I should receive a validation message indicating that I must enter an email

  @registerBlankPassword
  Scenario: User tries to register with blank password
    When I enter the registration details requested without a password
    Then I should get a validation message indicating I must enter a password


  @registerInvalidEmail
  Scenario: User tries to register with invalid email format
    When I enter the registration details requested with a incorrectly formatted email address
    Then I should receive a validation message indicating the email address format is incorrect

  @registerPasswordLength
  Scenario: User tries to register with password under 6 characters
    When I enter the registration details requested with a password of less than 6 characters
    Then I should get a validation message indicating my password is less than 6 characters

  # TODO: Jest test candidate
  @registerNewsletter
  Scenario: Registration option for newsletter is checked by default
    Then I should see the newsletter option
    And I should see the newsletter option unchecked by default

#  TODO Scenario: Register with the same username and password


