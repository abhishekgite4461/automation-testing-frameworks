@myOrderHistory
Feature: My Account -> Order History

  As a customer
  I want to be able to see my order history
  So that I can review the orders I have purchased

  Background:
    Given the set up is done


  @orderHistory
  Scenario: User with previous orders can see their orders history
    And I log in with bob0505.fullprofile@arcadiatest.com and password password1
    When I select My Orders
    Then An order number is visible in order history
    And An order date is visible in order history
    And An order total is visible in order history
