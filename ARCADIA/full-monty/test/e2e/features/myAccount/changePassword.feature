@changePassword
Feature: My Account -> Change password

  As a customer,
  I would like the option to change my user password
  So that I can ensure it's secure, up to date, and I can remember it.

  Background:
    Given the set up is done

  @changePassword @regression
  Scenario: Change password successfully
    Given I have registered as a new user
    And I go to the Change Password page
    When I change my current password
    Then I see a success message on the change password page

  @samePassword
  Scenario: New password is the same as the current password
    Given I log in with cxp9@acradiatest.com and password password1
    And I go to the Change Password page
    When I enter my current password into the new password field
    Then I see an error showing that my new password cannot be the same as my old password


  @mismatchPassword
  Scenario: Mismatch new password
    Given I log in with cxp10@acradiatest.com and password password1
    And I go to the Change Password page
    When I enter unequal values for my new password
    Then I see an error message outlining the unequal values

  @sameUserNamePassword
  Scenario: Same username and password
    Given I log in with cpp11@atest.com and password password1
    And I go to the Change Password page
    When I enter my email cpp11@atest.com into the new password
    Then I see an error showing that my new password cannot be the same as my username

  @passwordLength
  Scenario: Password length
    Given I log in with cxp5@acradiatest.com and password password1
    And I go to the Change Password page
    When I enter a new password that is too short
    Then I see an error showing that my new password must be at least 6 characters
