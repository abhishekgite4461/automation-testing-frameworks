@forgotPassword
Feature: My Account -> Forgot password

  As a Registered Customer if I forget my login password,
  I would like a route to set up a new password
  So I can manage my account & wishlists, view my order history, or checkout.

  Background:
    Given the set up is done
    And I go to the Login page


  @guestUserResetPassword @mobile
  Scenario: A guest user requests password reset email without registering to the site
    When I submit the forgot password form with an email address that is not registered
    Then I see an error message showing that my email address does not exist
