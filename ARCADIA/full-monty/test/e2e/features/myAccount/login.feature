@login
Feature: My Account -> Login

  As a user
  I would like to sign in with my registered details
  So that I could view my account section

  Background:
    Given the set up is done

  @navigateToLogin @mobile
  Scenario: User navigate to my account in menu
    And I open the menu
    When I select My Account
    Then I am directed to the login page

  @navigateToLogin @desktop
  Scenario: User navigate to my account in menu
    When I select the sign in link
    Then I am directed to the login page

  @invalidPassword
  Scenario Outline: A user enters an invalid password
    And I go to the Login page
    And I enter an email address <email>
    When I enter an incorrect password <password>
    Then I see a password validation message "<message>" displayed in login form
    Examples:
      | email                   | password | message                                                |
      | yettoregister@gmail.com | EMPTY    | A password is required.                                |
      | yettoregister@gmail.com | short    | Please enter a password that is at least 6 characters long and includes a number |
      | yettoregister@gmail.com | longer   | Please enter a password that is at least 6 characters long and includes a number |

  @invalidEmail
  Scenario Outline: A user enters an invalid email address
    And I go to the Login page
    When I enter an incorrect email address <email>
    Then I see an email validation message "<message>" displayed in login form
    Examples:
      | email               | message                             |
      | invalidEmailAddress | Please enter a valid email address. |
      | EMPTY               | An email address is required.       |

  @unregisteredUserLogIn
  Scenario: A user logs in using unregistered account credentials
    And I go to the Login page
    When I log in with an unregistered account
    Then I see email address or password error message
