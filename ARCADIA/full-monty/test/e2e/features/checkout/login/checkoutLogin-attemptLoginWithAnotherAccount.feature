@GuestCheckout
Feature: Checkout > Login - email already exists message can be cleared.
  As a user,
  when I land on the email address entry in the quick checout flow
  after completing all requested information the click Email entry button is initially titled 'Verify'

  MON-1162: Existing user login from guest checkout flow

  Background:
    Given the set up is done
    And I have added a product to the basket and checkout as guest
    And I have checked out with the default uk delivery and billing info

  @tsuk_only
  Scenario: A customer tries to reset the login form from checkout flow
    When I enter the guest email as "myaccount@example.com"
    And I verify my email
    And I submit the "Try another email address"
    Then the login form should be cleared
