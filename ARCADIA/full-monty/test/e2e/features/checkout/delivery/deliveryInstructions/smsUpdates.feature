Feature: Checkout > Delivery > Delivery Instructions - customer can add mobile number to receive delivery instructions

  Background:
    Given the set up is done
    And I have registered as a new user
    And I have added a product to the basket and checkout as the logged in user
    And I have filled out the delivery details without continuing

  Scenario: as a customer i can add my mobile number to the delivery details to receive updates on my delivery
    Then I should see the mobile field for delivery updates
    And I should see the delivery instructions text field

