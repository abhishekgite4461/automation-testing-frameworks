Feature: Checkout > Delivery > Delivery Instructions - customer can add delivery instructions without mobile number to receive delivery instructions

  Background:
    Given the set up is done
    And I have registered as a new user
    And I have added a product to the basket and checkout as the logged in user
    And I have filled out the delivery details without continuing

  Scenario: as a customer I can enter delivery instructions without a mobile number for updates
    When I enter a standard delivery instructions note
    Then The standard delivery instructions note should be shown in the standard delivery instructions note field
