Feature: Checkout > Delivery > Delivery Instructions - can add mobile number to receive delivery instructions

  Background:
    Given the set up is done
    And I have registered as a new user
    And I have added a product to the basket and checkout as the logged in user
    And I have filled out the delivery details without continuing

  Scenario: as a customer I can enter delivery instructions and a mobile number for delivery updates
    When I enter a delivery updates mobile number
    And I enter a standard delivery instructions note
    And I click next
    And I have entered the billing details
    Then I should be on the summary page
