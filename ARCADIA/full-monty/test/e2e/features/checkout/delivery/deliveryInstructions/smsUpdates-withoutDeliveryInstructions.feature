Feature: Checkout > Delivery > Delivery Instructions - customer can add mobile number to receive delivery instructions

  Background:
    Given the set up is done
    And I have registered as a new user
    And I have added a product to the basket and checkout as the logged in user
    And I have filled out the delivery details without continuing

  Scenario: As a customer I can enter a mobile number for updates without delivery instructions
    When I enter a delivery updates mobile number
    Then The number should be shown in the delivery updates mobile number
