Feature: Checkout > Delivery > Delivery Instructions - can add mobile number to receive delivery instructions

  Background:
    Given the set up is done
    And I have registered as a new user
    And I have added a product to the basket and checkout as the logged in user
    And I have filled out the delivery details without continuing

  Scenario: delivery instructions can not be over 30 characters
    When I enter delivery instructions longer than 30 characters
    Then I should see i have no characters left to use

