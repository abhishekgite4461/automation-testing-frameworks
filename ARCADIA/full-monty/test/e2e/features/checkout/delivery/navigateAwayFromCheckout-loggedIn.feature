@GuestCheckout
Feature:  Checkout > Delivery - Logged in, Delivery details are not saved
  I navigate away from checkout and back in to it
  My entered details should not be kept

  Background:
    Given the set up is done

  Scenario: logged in user with no checkout profiles navigates away from checkout
    And I have registered as a new user
    And I have added a product to the basket and checkout as the logged in user
    And I have entered a delivery address
    When The shopping bag contains a high value product over £100
    And I checkout as the logged in user
    Then the delivery page should not have saved any of my details

