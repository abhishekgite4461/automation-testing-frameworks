@GuestCheckout
Feature:  Checkout > Delivery - Guest user, Delivery details are not saved
  I navigate away from checkout and back in to it
  My entered details should not be kept

  Background:
    Given the set up is done

  Scenario: user navigates away from delivery address page
    And I have added a product to the basket and checkout as guest
    And I have entered a delivery address
    When The shopping bag contains a high value product over £100
    And I have checked out as a guest to the delivery address page
    Then the delivery page should not have saved any of my details
