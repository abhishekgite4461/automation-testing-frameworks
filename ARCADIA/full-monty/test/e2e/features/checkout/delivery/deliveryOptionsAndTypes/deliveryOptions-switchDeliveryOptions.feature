Feature: Checkout > Delivery > Delivery Options and Types - Express / Nominated day delivery
As a customer
I want to be able to switch between delivery options
So that I can decide the best delivery options for me

  Background:
    Given the set up is done
    And I have registered as a new user
    And I have added a product to the basket and checkout as the logged in user

  Scenario: A customer is able to select a different delivery option after selecting "Express / Nominated day delivery"
    And I select "Express / Nominated day" delivery type
    When I update delivery option to "Collect from Store"
    Then I should see "Collect from Store" delivery option selected