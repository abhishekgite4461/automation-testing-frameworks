@GuestCheckout
Feature: Checkout > Delivery > Delivery Options and Types - Standard Delivery
  As a customer,
  I want to be able to select a home delivery type,
  so I can pick the best option for delivery that suits my needs and budget.

  Background:
    Given the set up is done

  @regression @browserStackMobile
  Scenario: 2. A customer with a UK based delivery address, selects standard delivery option
    Given I have added a product to the basket and checkout as guest
    And Delivery Location Home Delivery should be selected
    And Delivery Type Standard Delivery should be selected
    Then I select Free Standard delivery
    And Delivery Type Standard Delivery should be selected
