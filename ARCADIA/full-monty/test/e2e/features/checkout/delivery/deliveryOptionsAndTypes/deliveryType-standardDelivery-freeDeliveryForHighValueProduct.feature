@GuestCheckout
Feature: Checkout > Delivery > Delivery Options and Types - free shipping triggered for high spend
  As a customer,
  I want to be able to select a home delivery type,
  so I can pick the best option for delivery that suits my needs and budget.

  Background:
    Given the set up is done

  @regression @mobile @browserStackMobile
  Scenario: 3. A customer with a UK based delivery address, see no charge when item qualifies for free shipping
    Given The shopping bag contains a high value product over £100
    And I have checked out as a guest to the delivery address page
    Then The Delivery charge is hidden
