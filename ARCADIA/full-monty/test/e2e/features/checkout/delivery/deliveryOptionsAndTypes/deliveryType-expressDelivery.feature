@GuestCheckout
Feature: Checkout > Delivery > Delivery Options and Types - Nominated day delivery

  Background:
    Given the set up is done

  @regression @browserStackMobile
  Scenario: A customer with a UK based delivery address, selects express delivery option
    Given I have added a product to the basket and checkout as guest
    And I select Express / Nominated delivery
    And Delivery Type Express Delivery should be selected
    And Express delivery options are visible
    When I select an express delivery option from the drop down
    Then The shown delivery costs should be the same as the option chosen
    And delivery date selected should match option chosen
