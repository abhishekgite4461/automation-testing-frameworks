@GuestCheckout
Feature: Checkout > Delivery > Delivery Details > Find Address - Able to switch to manual address form
  As a user,
  I would like to see only the most relevant address form fields
  so that I don't fill out unnecessary fields

  Background:
    Given the set up is done
    And I have added a product to the basket and checkout as guest

  Scenario: User enters delivery address manually
    When I click the "enter address manually"
    Then I should see a first name text field
    And I should see a last name text field
    And I should see a mobile text field
    And I should see an Address Line 1 text field
    And I should see a Address Line 2 text field
    And I should see a towncity text field
    And I should see a Post code text field
    And I should see a Title drop down
    And I should see a Country drop down
