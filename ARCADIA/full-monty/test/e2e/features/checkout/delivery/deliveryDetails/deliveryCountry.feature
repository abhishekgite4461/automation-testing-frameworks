Feature: Checkout > Delivery > Delivery Details - manual address form should display when find address is not available
  As a customer,
  I would like to enter my delivery address I would like my order delivered
  So that I can control where I will receive it.

  Background:
    Given the set up is done

  Scenario Outline: as a customer i can choose a new delivery country and perform checkout
    Given I have registered as a new user
    And I have added a product to the basket and checkout as the logged in user
    When I select the delivery country "<country>"
    Then I should see a clear form link
    And I should have be presented with the manual address form
    And I log out
    Examples:
      | country |
      | Poland  |
#      | Turkey  |
#      | Romania |


