Feature: Checkout > Delivery > Delivery Details - Valid postcode lookup can be cleared and retried.
  As a customer,
  I would like to enter my delivery address I would like my order delivered
  So that I can control where I will receive it.

  Background:
    Given the set up is done

  @regression @GuestCheckout @browserStackMobile
  Scenario: A customer in UK enters a delivery details in delivery page
    And I am on the "tsuk" home page
    And I have added a product to the basket and checkout as guest
    And I enter the delivery Details "Miss", "Jane", "Daws", "07123 123 123"
    And I select the delivery country "United Kingdom"
    And I enter the find address postcode "W1W 8LG"
    And I enter the find address house number "216"
    Then I submit the Find Address
    And I should see a clear form link
    And I should see a find address link
    #And I should see following select your address option
    #  | Nike town, 36-38 Great Castle Street, LONDON W1W 8LG |

