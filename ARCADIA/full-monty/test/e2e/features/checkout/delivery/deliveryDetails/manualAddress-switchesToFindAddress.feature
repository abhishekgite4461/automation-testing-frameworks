@GuestCheckout
Feature: Checkout > Delivery > Delivery Details > Find Address - Able to switch to manual address form
  As a user,
  I would like to see only the most relevant address form fields
  so that I don't fill out unnecessary fields

  Background:
    Given the set up is done
    And I have added a product to the basket and checkout as guest

  Scenario: A customer switches back to find address form fields
    When I click the "enter address manually"
    Then I go back to find address via post code
    Then I should see a first name text field
    And I should see a last name text field
    And I should see a mobile text field
    And I should see a Title drop down
    And I should see a Country drop down
    And I should see a house number text field
    And I should see the auto find postcode text field
    And I should not see a address line 1 text field
    And I should not see an address line 2 text field
    And I should not see a towncity text field
    And I should not see the manual enter post code text field
    And I can view the "find address" button
    And I can view the "enter address manually" link

