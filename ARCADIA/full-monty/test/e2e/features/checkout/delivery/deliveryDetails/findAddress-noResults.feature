Feature: Checkout > Delivery > Delivery Details > Find Address - Error message when there are no result
  As a customer,
  I would like to enter my delivery address I would like my order delivered
  So that I can control where I will receive it.

  Background:
    Given the set up is done

  @regression @GuestCheckout @browserStackMobile
  Scenario: A Customer sees an error message when no address returned for the searched postcode
    Given I have added a product to the basket and checkout as guest
    And I select the delivery country "United Kingdom"
    And I enter the find address postcode "SE156PR"
    And I enter the find address house number " "
    Then I submit the Find Address
    And I received the no-address error message
