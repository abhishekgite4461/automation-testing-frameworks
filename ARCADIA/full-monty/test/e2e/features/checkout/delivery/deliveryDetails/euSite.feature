Feature: Checkout > Delivery > Delivery Details - EU sites should not have default delivery country.
  As a customer,
  I would like to enter my delivery address I would like my order delivered
  So that I can control where I will receive it.

  Background:
    Given the set up is done

  @GuestCheckout
  Scenario Outline: A guest customer views delivery page in EU site
    And I am on the "<brand>" home page
    When I have added a product to the basket and checkout as guest
    Then delivery country should be unselected

    Examples:
      | brand | brandTitle |
#      | breu  | Burton         |
#      | tmeu  | Topman         |
      | tseu  | Topshop    |
#      | eveu  | Evans          |
#      | mseu  | Missselfridge  |
#      | dpeu  | Dorothyperkins |
#      | wleu  | Wallis         |
