Feature: Customer > Billing > Find Address - when address does not exist
  As a customer,
  I would like to enter my billing address I would like my order delivered
  So that I can control where I will be billed to.
  # These tests have been repointed to create a new user to do this feature, this allows us to test the desktop delivery and billing page interfaces, without guest checkout
  # when and if the guest checkout is enabled, then we will be able to turn this back to use a guest flow, as that is easier from a use of time taken to run the tests

  Background:
    Given the set up is done
    And I have registered as a new user
    And I have added a product to the basket and checkout as the logged in user
    And I have entered a delivery address
    And the auto address finder form is present

  @regression @browserStackMobile
  Scenario: A Customer sees an error message when no address returned for the searched postcode
    And I select the delivery country "United Kingdom"
    And I enter a value as "SE156PR" for my "postCode"
    And I enter a value as "" for my "houseNumber"
    When I submit the Find Address
    Then I received the no-address error message
