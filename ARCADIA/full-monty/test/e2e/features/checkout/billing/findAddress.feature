Feature: Customer > Billing > Find Address - Link to manual address form should be available even after finding an address
  As a customer,
  I would like to enter my billing address I would like my order delivered
  So that I can control where I will be billed to.

  Background:
    Given the set up is done
    And I have registered as a new user
    And I have added a product to the basket and checkout as the logged in user
    And I have entered a delivery address
    And the auto address finder form is present

  @regression @browserStackMobile
  Scenario: 1. A customer in UK enters billing details in payment page
    And I select the delivery country "United Kingdom"
    And I enter a value as "se13 6sx" for my "postCode"
    And I enter a value as "72" for my "houseNumber"
    When I submit the Find Address
    Then The "Enter address manually" link should be displayed
    And I should see following select your address option: Flat, 72 Springbank Road, LONDON SE13 6SX
