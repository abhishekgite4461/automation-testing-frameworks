@realCheckout @klarna @mobile
Feature: Checkout > Success > Guest checkout - Klarna, with existing account
  As a user
  and I complete a purchase flow
  I should still be logged in afterwards

  Background:
    Given the set up is done

  @GuestCheckout
  Scenario: A customer (approved Klarna without checkout profile) logs in during checkout before placing an order
    And I have registered as an approved Klarna user
    And I log out
    # Enable Klarna feature flag again as we clear all cookies when logging out
    And The shopping bag contains a product valued above £40
    And I have checked out as a guest to the delivery address page
    And I have checked out with the default uk delivery address
    When I complete the purchase with buy now pay later as an existing user
    And Confirm the order
    Then I should be redirected to the complete order page
