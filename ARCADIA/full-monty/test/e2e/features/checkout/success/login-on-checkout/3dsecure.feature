Feature: Checkout > Success > Login on checkout - 3D secure

  Background:
    Given the set up is done

  @3dsecure
  Scenario: Add to bag > checkout > login > delivery > billing > order confirmation > place order > VBV
    And I have registered as a new user
    And I log out
    When I have added a product to the basket and checkout logging in as the user
    And I have checked out with the default uk delivery and verified by visa billing info
    When Confirm the order with verified by visa
    And I should be redirected from verified by visa to the complete order page
