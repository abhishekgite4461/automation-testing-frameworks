Feature: Checkout > Success > Login on checkout - Account card

  Background:
    Given the set up is done

  @accountcard
  Scenario: StoreCard Holder > add to bag > checkout > login > delivery > billing > order confirmation > place order
    And I have registered as a new user
    And I log out
    When I have added a product to the basket and checkout logging in as the user
    And I have checked out with the default uk delivery address
    And I pay with the card type ACCOUNT the card number 6000082000000005 and the cvv 123
    And Confirm the order
    Then I should be redirected to the complete order page
