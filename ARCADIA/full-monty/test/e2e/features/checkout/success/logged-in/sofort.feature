@broken
  # All broken for CoreAPI. FS
Feature: Checkout > Success > Logged in - Sofort

  Background:
    Given the set up is done


  @sofort @mobile
  Scenario Outline: Logged in customer with China billing details can make payment using Sofort
    And I am on the "<brand>" home page
    And I have registered as a new user
    And I have added a product to the basket and checkout as the logged in user
    And I enter "Germany" delivery details
    And I enter "<billingCountry>" billing details
    And I pay with "Sofort"
    When I confirm order with "Sofort"
    Then I should be redirected to the complete order page
    And I continue shopping
    And I confirm I have the previous order in the my order history

    Examples:
      | brand | brandTitle                  | billingCountry |
      # | DPDE  | DorothyPerkins (Germany)    | Austria        |
      # | EVDE  | Evans (Germany)             | Austria        |
      # | MSDE  | MissSelfridge (Germany)     | Austria        |
      # | TMDE  | TopMan (Germany)            | Austria        |
      # | TSDE  | TopShop (Germany)           | Austria        |
      # | WLDE  | Wallis (Germany)            | Austria        |
      # | BREU  | Burton (EU)                 | Austria        |
      # | DPEU  | DorothyPerkins (EU)         | Austria        |
      # | EVEU  | Evans (EU)                  | Austria        |
      # | MSEU  | MissSelfridge (EU)          | Austria        |
      # | TMEU  | TopMan (EU)                 | Austria        |
      | TSEU  | TopShop (EU)                | Austria        |
      # | WLEU  | Wallis (EU)                 | Austria        |
      # | DPFR  | DorothyPerkins (France)     | Austria        |
      # | MSFR  | MissSelfridge (France)      | Austria        |
      # | TMFR  | TopMan (France)             | Austria        |
      # | TSFR  | TopShop (France)            | Austria        |
      # | DPDE  | DorothyPerkins (Germany)    | Belgium        |
      # | EVDE  | Evans (Germany)             | Belgium        |
      # | MSDE  | MissSelfridge (Germany)     | Belgium        |
      # | TMDE  | TopMan (Germany)            | Belgium        |
      # | TSDE  | TopShop (Germany)           | Belgium        |
      # | WLDE  | Wallis (Germany)            | Belgium        |
      # | BREU  | Burton (EU)                 | Belgium        |
      # | DPEU  | DorothyPerkins (EU)         | Belgium        |
      # | EVEU  | Evans (EU)                  | Belgium        |
      # | MSEU  | MissSelfridge (EU)          | Belgium        |
      # | TMEU  | TopMan (EU)                 | Belgium        |
      # | TSEU  | TopShop (EU)                | Belgium        |
      # | WLEU  | Wallis (EU)                 | Belgium        |
      # | DPFR  | DorothyPerkins (France)     | Belgium        |
      # | MSFR  | MissSelfridge (France)      | Belgium        |
      # | TMFR  | TopMan (France)             | Belgium        |
      # | TSFR  | TopShop (France)            | Belgium        |
      # | DPDE  | DorothyPerkins (Germany)    | Germany        |
      # | EVDE  | Evans (Germany)             | Germany        |
      # | MSDE  | MissSelfridge (Germany)     | Germany        |
      # | TMDE  | TopMan (Germany)            | Germany        |
      # | TSDE  | TopShop (Germany)           | Germany        |
      # | WLDE  | Wallis (Germany)            | Germany        |
      # | BREU  | Burton (EU)                 | Germany        |
      # | DPEU  | DorothyPerkins (EU)         | Germany        |
      # | EVEU  | Evans (EU)                  | Germany        |
      # | MSEU  | MissSelfridge (EU)          | Germany        |
      # | TMEU  | TopMan (EU)                 | Germany        |
      # | TSEU  | TopShop (EU)                | Germany        |
      # | WLEU  | Wallis (EU)                 | Germany        |
      # | DPFR  | DorothyPerkins (France)     | Germany        |
      # | MSFR  | MissSelfridge (France)      | Germany        |
      # | TMFR  | TopMan (France)             | Germany        |
      # | TSFR  | TopShop (France)            | Germany        |
      # | DPDE  | DorothyPerkins (Germany)    | France         |
      # | EVDE  | Evans (Germany)             | France         |
      # | MSDE  | MissSelfridge (Germany)     | France         |
      # | TMDE  | TopMan (Germany)            | France         |
      # | TSDE  | TopShop (Germany)           | France         |
      # | WLDE  | Wallis (Germany)            | France         |
      # | BREU  | Burton (EU)                 | France         |
      # | DPEU  | DorothyPerkins (EU)         | France         |
      # | EVEU  | Evans (EU)                  | France         |
      # | MSEU  | MissSelfridge (EU)          | France         |
      # | TMEU  | TopMan (EU)                 | France         |
      # | TSEU  | TopShop (EU)                | France         |
      # | WLEU  | Wallis (EU)                 | France         |
      # | DPFR  | DorothyPerkins (France)     | France         |
      # | MSFR  | MissSelfridge| (France)     | France         |
      # | TMFR  | TopMan (France)             | France         |
      # | TSFR  | TopShop (France)            | France         |
