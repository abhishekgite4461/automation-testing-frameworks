@realCheckout
Feature: Checkout > Success > Logged in, reload billing page before continuing

  Background:
    Given the set up is done

  Scenario Outline: logged in customer can refresh the page during checkout on billing page then i should be redirected to delivery page
    And I have registered as a new user
    And I have added a product to the basket and checkout as the logged in user
    And I have checked out with the default uk delivery address
    And I pay with the card type <cardType> the card number <cardNumber> and the cvv <cvv>
    When I reload the page
    Then I should be taken to the delivery page

    Examples:
      | cardType | cardNumber       | cvv |
      | VISA     | 4462030000000000 | 123 |
