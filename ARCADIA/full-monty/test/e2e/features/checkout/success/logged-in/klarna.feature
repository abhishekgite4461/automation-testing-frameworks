@realCheckout @klarna @mobile
Feature: Checkout > Success > Logged in - Klarna
  As a user
  and I complete a purchase flow
  I should still be logged in afterwards

  Background:
    Given the set up is done

  Scenario: Logged in (approved Klarna without checkout profile) places an order using Klarna payment method
    And I have registered as an approved Klarna user
    And The shopping bag contains a product valued above £40
    And I checkout as the logged in user
    And I have checked out with the default uk delivery address
    When I complete the purchase with buy now pay later
    And Confirm the order
    Then I should be redirected to the complete order page
