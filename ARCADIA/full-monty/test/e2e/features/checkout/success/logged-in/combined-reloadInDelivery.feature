@realCheckout
Feature: Checkout > Success > Logged in, reload delivery page before continuing

  Background:
    Given the set up is done
    And I have registered as a new user

  Scenario Outline: Logged in (no checkout profile) > add to bag > checkout > reload page > delivery > billing > order confirmation > place order
    And The shopping bag contains a product
    And I checkout as the logged in user
    And I reload the page
    And I have checked out with the default uk delivery address
    And I pay with the card type <cardType> the card number <cardNumber> and the cvv <cvv>
    When Confirm the order
    Then I should be redirected to the complete order page

    Examples:
      | cardType | cardNumber       | cvv | comment    |
#      | VISA      | 4444333322221111 | 123  | VISA                |
      | VISA     | 4462030000000000 | 123 | VISA Debit |
#      | VISA      | 4917300800000000 | 123  | VISA Electron (UK)  |
#      | VISA      | 4484070000000000 | 123  | VISA Purchasing     |
#      | AMEX      | 343434343434343  | 1234 | American Express    |
#      | MCARD     | 5555555555554444 | 123  | MasterCard          |
#      | SWTCH     | 6759649826438453 | 123  | Maestro             |
