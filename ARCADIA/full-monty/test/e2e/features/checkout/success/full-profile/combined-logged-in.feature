@realCheckout
Feature: Checkout > Success > Full profile + logged in

  Background:
    Given the set up is done

  Scenario Outline: A full profile customer pays with a credit card
    And I have registered as a new full profile user with card type <cardType>, card number <cardNumber> and the cvv <cvv>
    And I have added a product to the basket and checkout as the logged in user
    When I enter a value as "<cvv>" for my "cvv"
    And Confirm the order
    Then I should be redirected to the complete order page

    Examples:
      | cardType | cardNumber      | cvv  | comment          |
       | VISA      | 4444333322221111 | 123  | VISA                |
#        | VISA      | 4462030000000000 | 123  | VISA Debit          |
#        | VISA      | 4917300800000000 | 123  | VISA Electron (UK)  |
#        | VISA      | 4484070000000000 | 123  | VISA Purchasing     |
      | AMEX     | 343434343434343 | 1234 | American Express |
#        | MCARD     | 5555555555554444 | 123  | MasterCard          |
#      | SWTCH     | 6759649826438453 | 123  | Maestro             |
