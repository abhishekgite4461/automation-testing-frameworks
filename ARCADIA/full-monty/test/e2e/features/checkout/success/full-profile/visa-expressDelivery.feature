@realCheckout
Feature: Checkout > Success > Full profile + Logged in - VISA, with express delivery

  Background:
    Given the set up is done

  Scenario: A returning customer is able to do direct checkout and not view express delivery options if they have selected express delivery in the minibag and have visited the '/checkout/delivery' page.
    Given I have registered as a new full profile user with card type VISA, card number 4444333322221111 and the cvv 123
    And The shopping bag contains a product
    And I update the delivery option in the shopping bag to "Express / Nominated Day Delivery £6.00" and checkout
    And I can view my delivery & billing details are pre-populated
    When I update my delivery option to standard home and navigate back to checkout confirmation
    Then I can view my delivery & billing details are pre-populated
    And I do not see nominated day express delivery options
