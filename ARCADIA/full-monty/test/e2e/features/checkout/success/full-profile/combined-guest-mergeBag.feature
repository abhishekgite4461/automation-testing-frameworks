@realCheckout
Feature: Checkout > Success > Full profile + guest checkout, merge bag

  Background:
    Given the set up is done

  Scenario Outline: A customer logs in (with checkout profile) and merges bag during checkout before placing an order
    And I have registered as a new full profile user with card type <cardType>, card number <cardNumber> and the cvv <cvv>
    And The shopping bag contains a product
    And I log out
    And I have added a product to the basket and checkout as guest
    And I have checked out with the default uk delivery address
    And I pay with the card type VISA the card number 4444333322221111 and the cvv 123
    When I confirm the order as an existing user and merging products in bag
    Then I should be redirected to the complete order page

    Examples:
      | cardType | cardNumber       | cvv | comment |
      | VISA     | 4444333322221111 | 123 | VISA    |
#      | VISA      | 4462030000000000 | 123  | VISA Debit          |
#      | VISA      | 4917300800000000 | 123  | VISA Electron (UK)  |
#      | VISA      | 4484070000000000 | 123  | VISA Purchasing     |
#      | AMEX      | 343434343434343  | 1234 | American Express    |
#      | MCARD     | 5555555555554444 | 123  | MasterCard          |
#      | SWTCH     | 6759649826438453 | 123  | Maestro             |
