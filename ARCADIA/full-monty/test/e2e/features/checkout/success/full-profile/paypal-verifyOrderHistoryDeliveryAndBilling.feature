@realCheckout
Feature: Checkout > Success > Full profile + Logged in - PayPal, verify order history, delivery and billing address

  Background:
    Given the set up is done

  # Registers a customer with a VISA card before switching over to PayPal for payment
  @paypal
  Scenario: a full profile customer pays with PayPal
    And I have registered as a new full profile user with card type VISA, card number 4444333322221111 and the cvv 123
    And I have added a product to the basket and checkout as the logged in user
    And I have checked out with the saved address and change the payment type to paypal and checkout
    And Confirm the order with paypal
    And I should be redirected from paypal to the complete order page
    And I continue shopping
    Then I confirm I have the previous order in the my order history
    And my details should have the same delivery and billing address entered for the purchase
