@smoke
Feature: Checkout > Success - Smoke + new delivery country and payment
  As a user
  and I complete a purchase flow
  I should still be logged in afterwards

  Background:
    Given the set up is done

  Scenario: as a customer i can choose a new delivery country and perform checkout
    Given I have registered as a new user
    And I have added a product to the basket and checkout as the logged in user
    When I select the delivery country "Poland"
    And I enter the polish address
    And I provide billing details
    And Confirm the order
    Then I should receive the payment can not be processed message
