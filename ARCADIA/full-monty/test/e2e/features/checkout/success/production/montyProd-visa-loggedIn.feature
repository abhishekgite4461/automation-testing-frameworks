@smoke
Feature: Checkout > Success - Smoke + VISA payment
  As a user
  and I complete a purchase flow
  I should still be logged in afterwards

  Background:
    Given the set up is done

  Scenario: Anonymous > add to bag > checkout > login > delivery > billing > order confirmation > place order
    Given I have registered as a new user
    And I log out
    When I have added a product to the basket and checkout logging in as the user
    And I have checked out with the default uk delivery address
    And I pay with the card type VISA the card number 4917300800000000 and the cvv 123
    And Confirm the order
    Then I should receive the payment can not be processed message
