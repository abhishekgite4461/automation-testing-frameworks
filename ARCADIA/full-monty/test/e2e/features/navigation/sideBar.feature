@mobile
Feature: Navigation > Menu
  As a Customer
  I would like to view a sidebar
  So that I can quickly access website features

  Background:
    Given the set up is done

  @regression
  Scenario: Unauthenticated user can see correct options in menu
    And I am on the landing page
    When I open the menu
    Then I can see "My Account" link in the menu options
    And I can see "Sign in or Register" link in the menu options
