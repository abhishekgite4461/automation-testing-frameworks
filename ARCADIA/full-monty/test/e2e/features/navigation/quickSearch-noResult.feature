Feature: Navigation > Quick Search - no results
  As a customer
  I would like to search from any page
  so that I can find the products I'm looking for

  Background:
    Given the set up is done

  @regression
  Scenario: A user performs search with a text returning no results
    Given I search for "!@£$%^&*"
    Then The message "Sorry - nothing matches" is displayed
