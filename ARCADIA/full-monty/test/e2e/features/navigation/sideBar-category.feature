Feature: Navigation > Menu - Category navigation

  As a customer
  I would like to browse and select categories
  So that I can sort and refine the search results based on my preferences

  Background:
    Given the set up is done

  @regression @tsuk_only @mobile
  Scenario: 1. Customer views related products after navigating to second level category from menu options
    When I open the menu
    And I open the main menu item "Clothing" and the submenu "Dresses"
    Then I am directed to the Product Listing page matching "Dresses"
