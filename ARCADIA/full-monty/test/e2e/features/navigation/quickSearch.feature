Feature: Navigation > Quick Search
  As a customer
  I would like to search from any page
  so that I can find the products I'm looking for

  Background:
    Given the set up is done

  @regression
  Scenario: A user performs search with a text to return multiple products
    Given I search for "COURT"
    Then I am directed to the Product Listing page matching "COURT"
