@mobile
Feature: Navigation > Menu - visible menus when logged in
  As a Customer
  I would like to view a sidebar
  So that I can quickly access website features

  Background:
    Given the set up is done

  @regression
  Scenario: Authenticated user can see correct options in menu
    Given I log in with sidebar@arcadiatest.com and password password1
    And I am on the landing page
    When I open the menu
    Then I can see "My Account" link in the menu options
    And I can see "Sign out" link in the menu options
