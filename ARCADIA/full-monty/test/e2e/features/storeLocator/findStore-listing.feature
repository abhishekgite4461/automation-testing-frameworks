Feature: Find a store > Store listing
  As a customer
  I would like to select my country
  So that I can see a list of stores that are relevant to me

  Background:
    Given the set up is done

  @mobile
  Scenario: A non-UK customer can view store listing
    And I open the menu
    When I select a new country
    And I select go
    Then I should see a list of stores for the brand

  @regression @mobile
  Scenario: A UK customer can search and view UK-specific results
    Given I open the menu
    And I select my country as "United Kingdom"
    When I search for stores in "Die"
    Then I should see locations in "London" displayed in search helper
    And I select the first Store prediction
    And I verify the map is displayed
