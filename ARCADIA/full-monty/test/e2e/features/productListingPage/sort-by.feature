Feature: Product listing > Sort by
  As a customer
  I would like the option to manage the results on the PLP
  So I can sort or change the image of the products I am viewing

  Background:
    Given the set up is done


  Scenario: Sort by Outfit
    Given I am viewing a list of products
    When I switch to Outfit view
    Then I see the Modelled Images

  Scenario: Sort by Product
    Given I am viewing a list of products
    When I switch to Product view
    Then I see the Product Images

  Scenario: Customer sorts by newest items
    Given I am viewing a list of products
    And I sort products by 'Newest'
    Then I can see a sorted list of products

  Scenario: Customer sorts by Price - Low to Highs
    Given I am viewing a list of products
    And I sort products by 'Price - High To Low'
    Then I can see a sorted list of products

  Scenario: Customer sorts by Price - High to Low
    Given I am viewing a list of products
    And I sort products by 'Price - Low To High'
    Then I can see a sorted list of products
