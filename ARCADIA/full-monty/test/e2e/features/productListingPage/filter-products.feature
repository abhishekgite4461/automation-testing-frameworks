@mobile
Feature: Product listing > Filter

  Background:
    Given the set up is done

  Scenario: Customer filters by min and max price
    Given I am viewing a list of products
    And I filter the product list
    And I filter by price
    And I increase price min filter
    And I increase price max filter
    When I apply these filters
    Then Filter button has 1 filter
    And Filter returns a product list
