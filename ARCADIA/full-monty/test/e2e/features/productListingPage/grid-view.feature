@mobile
Feature: Product listing > Grid view

  Background:
    Given the set up is done

  @regression
  Scenario: 1. One column product grid view
    Given I am viewing a list of products
    And I switch product grid view to 1
    Then I can see 1 product at a time

  @regression
  Scenario: 2. Two column product grid view
    Given I am viewing a list of products
    And I switch product grid view to 2
    Then I can see 2 product at a time

  @regression
  Scenario: 3. Three column product grid view
    Given I am viewing a list of products
    And I switch product grid view to 3
    Then I can see 3 product at a time
