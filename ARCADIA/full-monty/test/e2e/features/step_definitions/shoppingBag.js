import * as shoppingBag from '../../models/pageObjects/shoppingBag'
import * as client from '../../models/lib/client'
import * as shared from '../../models/lib/shared'
import * as overlay from '../../models/newPageObjects/components/overlay'
import * as findAddress from '../../models/newPageObjects/checkout/FindAddress_po'
import * as manualAddress from '../../models/newPageObjects/checkout/ManualAddress_po'
import * as shoppingBagPO from '../../models/newPageObjects/shoppingBag/ShoppingBagBase'
import * as addToBagPO from '../../models/newPageObjects/productDetail/addToBag'
import * as deliveryOptions from '../../models/newPageObjects/checkout/DeliveryOptions_po'
import * as topBarPO from '../../models/newPageObjects/components/topbar_po'
import * as checkoutLogin from '../../models/newPageObjects/checkout/checkoutLogin'
import * as deliveryPO from '../../models/newPageObjects/checkout/DeliveryBase_po'

const assert = require('assert')

module.exports = function() {
  this.Given(
    /^I have added a product to the basket and checkout as guest$/,
    () => {
      addToBagPO.addRandomProduct()
      checkoutLogin.checkoutAsGuest()
    }
  )
  this.Given(
    /^I have added a product to the basket and checkout logging in as the user$/,
    () => {
      shared.enableResponsiveLayout()
      addToBagPO.addRandomProduct()
      checkoutLogin.checkoutAsUser(process.env.USERNAME, process.env.PASSWORD)
    }
  )
  this.Given(
    /^I have added a product to the basket and checkout as the logged in user$/,
    () => {
      addToBagPO.addRandomProduct()
      checkoutLogin.checkout()
    }
  )
  this.Given(
    /^The shopping bag contains a product$/,
    addToBagPO.addRandomProduct
  )
  this.Given(
    /^the shopping bag contains a product with multiple items in stock$/,
    addToBagPO.addRandomProductWithHighStockValue
  )
  this.Given(
    /^The shopping bag contains a high value product over £100$/,
    () => {
      addToBagPO.searchAndAddProductToBag(
        process.env.COLOUR_SERACH_PRODUCTS,
        null,
        false,
        '500',
        '102'
      )
    }
  )
  this.Given(
    /^The shopping bag contains a product valued below £(.*)$/,
    (minPrice) => {
      addToBagPO.searchAndAddProductToBag(
        process.env.COLOUR_SERACH_PRODUCTS,
        1,
        false,
        minPrice,
        '1'
      )
    }
  )
  this.Given(
    /^The shopping bag contains a product valued above £(.*)$/,
    (maxPrice) => {
      addToBagPO.searchAndAddProductToBag(
        process.env.COLOUR_SERACH_PRODUCTS,
        1,
        false,
        '500',
        maxPrice
      )
    }
  )
  this.Given(/^I reload the page$/, client.reloadPage)
  this.Given(
    /^A product with different sizes has been added to the shopping bag$/,
    () => {
      addToBagPO.searchAndAddProductToBag(process.env.MULTY_SIZE_PRODUCTS, 5)
    }
  )
  this.Given(/^I have added a one size only product to the basket$/, () => {
    addToBagPO.searchAndAddProductToBag(process.env.SINGLE_SIZE_PRODUCTS)
  })

  this.Given(
    /^I am on a product details page$/,
    shoppingBag.goToRandomProductsPage
  )

  this.Given(
    /^I add a product with 8 or less sizes to the basket$/,
    addToBagPO.add8OrLessSizeRandomProduct
  )
  this.Given(
    /^I have checked out with the default uk delivery and billing info$/,
    shoppingBag.realCheckout
  )

  this.Given(
    /^I have checked out with the default uk delivery and amex billing info$/,
    shoppingBag.realCheckoutAmex
  )
  this.Given(
    /^I have checked out with the default uk delivery address$/,
    shoppingBag.enterDefaultDeliveryInfo
  )
  this.Given(
    /^I pay with the card type (.*) the card number (.*) and the cvv (.*)$/,
    shoppingBag.checkoutWithCustomCard
  )
  this.Given(
    /^I checkout to the billing page$/,
    shoppingBag.enterDefaultDeliveryInfo
  )
  this.Given(
    /^I checkout to the delivery info page$/,
    shoppingBag.checkoutToDeliveryPage
  )
  this.Given(
    /^I click the "enter address manually"$/,
    manualAddress.clickManualAddressLink
  )
  this.Given(/^I navigate to the shopping bag$/, topBarPO.openShoppingBag)
  this.Given(
    /^I check the total of the shopping bag$/,
    topBarPO.checkTotalOfTheShoppingBag
  )
  this.Given(
    /^I re-check the total of the shopping bag has changed$/,
    topBarPO.recheckTotalOfTheShoppingBag
  )
  this.Given(
    /^I have entered the billing details$/,
    shoppingBag.enterDefaultPaymentInfo
  )
  this.When(
    /^I open the promotion code accordion$/,
    shoppingBagPO.openPromoCodeAccordion
  )
  this.When(/^I enter the promotion code "(.*)"$/, shoppingBagPO.enterPromoCode)
  this.When(/^I enter and submit the promotion code "(.*)"$/, (promoCode) => {
    process.env.TOTAL_PRICE_BEFORE_DISCOUNT = shoppingBagPO.getTotalPrice()
    shoppingBagPO.enterPromoCode(promoCode)
    shoppingBagPO.submitPromoCode()
  })
  this.When(
    /^the promotion code has been applied$/,
    shoppingBag.waitForPormotionToBeApplied
  )
  this.When(/^I submit the promotion code$/, shoppingBagPO.submitPromoCode)
  this.When(
    /^I click add another promotion code$/,
    shoppingBag.addAnotherPromotionCode
  )
  this.When(/^I remove the promotion code$/, shoppingBag.removePromotionCode)
  this.When(/^I open the shopping bag$/, topBarPO.openShoppingBag)
  this.When(
    /^I remove a product from the shopping bag$/,
    shoppingBagPO.removeProduct
  )
  this.When(/^I confirm deletion$/, () => {
    overlay.waitForOverlay()
    shoppingBagPO.clickProductRemovalConfirmation()
  })
  this.When(/^I do not confirm deletion$/, shoppingBagPO.clickCloseModal)
  this.When(/^I click outside delete confirm overlay$/, shoppingBag.closeModal)
  this.When(
    /^I click Continue button in cart page$/,
    shoppingBagPO.clickCheckout
  )
  this.When(
    /^I remove all products from the shopping bag$/,
    shoppingBag.openAndEmptyBag
  )
  this.When(/^I edit a product$/, shoppingBagPO.clickEditProduct)
  this.When(
    /^I change the product quantity and save the product$/,
    shoppingBag.changeQuantityAndSave
  )
  this.When(
    /^I change the product size and save the product$/,
    shoppingBag.changeSizeAndSave
  )
  this.When(/^I save the product$/, shoppingBagPO.clickSaveProduct)
  this.When(/^The product size should be updated$/, () => {
    assert.ok(
      process.env.BEFORE_EDIT_SIZE !== process.env.AFTER_EDIT_SIZE,
      `Product size is not updated. Expected: ${
        process.env.BEFORE_EDIT_SIZE
      }, Actual: ${process.env.AFTER_EDIT_SIZE}`
    )
  })
  this.When(/^I empty the shopping bag$/, shoppingBagPO.emptyBag)
  this.When(/^I open and empty the shopping bag$/, shoppingBag.openAndEmptyBag)
  this.When(/^I confirm this$/, shoppingBag.clickConfirmEmpty)
  this.When(
    /^I have updated the express delivery day on the delivery options page$/,
    shoppingBag.setDeliveryDayOnDeliveryPage
  )
  this.Then(
    /^the shopping bag header icon should indicate it has an item in it$/,
    shoppingBag.shoppingBagIconIndicatesProduct
  )
  this.Then(/^I should be taken to the delivery page$/, () => {
    const result = deliveryPO.deliveryCheckoutPagePresent()
    assert.ok(
      result,
      `got ${result} meaning delivery page country selector was not visible`
    )
  })
  this.Then(
    /^I should see "(.*)" in the list of promotions$/,
    shoppingBag.hasPromoCode
  )
  this.Then(/^I should see an empty list of promotion codes$/, () =>
    shoppingBag.hasNPromoCodes(0)
  )
  this.Then(
    /^The promotion form should show a server error: "(.*)"$/,
    shoppingBag.hasServerError
  )
  this.Then(
    /^The shopping bag total should be "(.*)"$/,
    shoppingBag.shoppingBagTotalEquals
  )
  this.Then(
    /^the shopping bag total should be reduced by "(.*)" pounds$/,
    shoppingBag.totalReducedBy
  )
  this.Then(
    /^the shopping bag total should be reduced by (.*)%$/,
    shoppingBag.shoppingBagTotalPercentageDiscount
  )
  this.Then(
    /^the promotion should be removed from the price$/,
    shoppingBag.shoppingBagPromotionDiscountHasBeenRemoved
  )
  this.Then(
    /^the shopping bag total should not be effected$/,
    shoppingBag.shoppingBagPromotionDiscountHasBeenRemoved
  )
  this.Then(/^The shopping bag should not be empty$/, shoppingBagPO.isNotEmpty)
  this.Then(/^The shopping bag should be empty$/, shoppingBag.isEmpty)
  this.Then(/^I can edit the product$/, shoppingBagPO.hasEdit)
  this.Then(/^I can remove the product$/, shoppingBagPO.hasRemove)
  this.Then(/^I should see the following text fields$/, () =>
    shoppingBag.textFieldsExist()
  )
  this.Then(/^I should see a first name text field$/, () =>
    shoppingBag.textFieldsExist('.Input-field.Input-field-firstName')
  )
  this.Then(/^I should see a last name text field$/, () =>
    shoppingBag.textFieldsExist('.Input-field.Input-field-lastName')
  )
  this.Then(/^I should see a mobile text field$/, () =>
    shoppingBag.textFieldsExist('.Input-field.Input-field-telephone')
  )
  this.Then(/^I should see an Address Line 1 text field$/, () =>
    shoppingBag.textFieldsExist('.Input-field.Input-field-address1')
  )
  this.Then(/^I should see a Address Line 2 text field$/, () =>
    shoppingBag.textFieldsExist('.Input-field.Input-field-address2')
  )
  this.Then(/^I should see a towncity text field$/, () =>
    shoppingBag.textFieldsExist('.Input-field.Input-field-city')
  )
  this.Then(/^I should see a Post code text field$/, () =>
    shoppingBag.textFieldsExist('.Input-field.Input-field-postcode')
  )
  this.Then(/^I should see a Title drop down$/, () =>
    shoppingBag.dropDownExist('.YourDetails .Select-select')
  )
  this.Then(/^I should see a Country drop down$/, () =>
    shoppingBag.dropDownExist('.FindAddress .Select-select')
  )
  this.Then(/^I should see a house number text field$/, () =>
    shoppingBag.textFieldsExist('.Input-field.Input-field-houseNumber')
  )
  this.Then(/^I should not see a address line 1 text field$/, () =>
    shoppingBag.textFieldDoesNotExist('.Input-field.Input-field-address1')
  )
  this.Then(/^I should not see an address line 2 text field$/, () =>
    shoppingBag.textFieldDoesNotExist('.Input-field.Input-field-address2')
  )
  this.Then(/^I should not see a towncity text field$/, () =>
    shoppingBag.dropDownExist('.FindAddress .Select-select')
  )
  this.Then(/^I should see the auto find postcode text field$/, () =>
    shoppingBag.textFieldsExist('.Input-field.Input-field-postCode')
  )
  this.Then(/^I should not see the manual enter post code text field$/, () =>
    shoppingBag.textFieldDoesNotExist('.Input-field.Input-field-postcode')
  )
  this.Then(
    /^I can view the "find address" button$/,
    shoppingBag.findAddressLinExists
  )
  this.Then(
    /^I can view the "enter address manually" link$/,
    shoppingBag.enterAddressManuallyLinkExists
  )
  this.Then(
    /^I go back to find address via post code$/,
    findAddress.clickFindAddressButton
  )
  this.Then(/^The product quantity should be updated$/, () => {
    assert.ok(
      process.env.BEFORE_EDIT_QUANTITY !== process.env.AFTER_EDIT_QUANTITY,
      `Product quantity is not updated. Expected: ${
        process.env.BEFORE_EDIT_QUANTITY
      }, Actual: ${process.env.AFTER_EDIT_QUANTITY}`
    )
  })
  this.Then(
    /^I can view my delivery & billing details are pre-populated$/,
    shoppingBag.confirmPagePreFilledDefaultAddresses
  )
  this.Then(
    /^I update my delivery option to standard home and navigate back to checkout confirmation$/,
    shoppingBag.updateCheckoutDeliveryOptionAndCheckoutToConfirm
  )
  this.Then(
    /^I can view the express delivery options$/,
    shoppingBag.updateConfirmationDeliveryOptionAndcheckout
  )
  this.Then(
    /^I select an express delivery option$/,
    shoppingBag.confirmPagePreFilledDefaultAddresses
  )
  this.Then(/^I do not see nominated day express delivery options$/, () => {
    assert.ok(
      deliveryOptions.expressDeliveryOptionsVisible === false,
      `Express delivery option is visible. Expected: No express delivery option displayed`
    )
  })
}
