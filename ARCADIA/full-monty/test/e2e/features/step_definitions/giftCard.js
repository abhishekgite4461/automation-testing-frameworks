import * as payment from '../../models/pageObjects/payment'

module.exports = function() {
  this.When(/^I expand the gift card menu$/, payment.openGiftCardMenu)
  this.When(
    /^I enter the gift card (.*) and the pin (.*)$/,
    payment.enterGiftCardAndPin
  )
  this.When(/^I submit the gift card for verification$/, payment.submitGiftCard)
  this.Then(
    /^I see success message with '(.*)' displayed$/,
    payment.discountSuccessMessageContains
  )
  this.Then(
    /^I should see the amount used on the gift card as (.*)$/,
    payment.giftCardAmountEquals
  )
}
