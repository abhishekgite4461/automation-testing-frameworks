import * as shoppingBag from '../../models/pageObjects/shoppingBag'
import * as delivery from '../../models/pageObjects/delivery'

module.exports = function() {
  this.When(
    /^I checkout to the confirmation page$/,
    shoppingBag.enterDefaultPaymentInfo
  )
  this.Then(
    /^I should see the store chosen on the delivery page$/,
    delivery.collectFromStoreChoiceExists
  )

  this.When(
    /^I select a parcel shop in "(.*)" and checkout$/,
    delivery.setParcelShopToCollectFrom
  )
  this.Then(
    /^I should see the store chosen on the confirmation page$/,
    delivery.choosenStorePresentOnConfirmation
  )
}
