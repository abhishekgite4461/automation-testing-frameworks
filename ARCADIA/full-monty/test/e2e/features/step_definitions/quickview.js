import * as quickviewPage from '../../models/newPageObjects/quickviewDesktop/quickviewPO'

const assert = require('assert')

module.exports = function() {
  this.When(/^I click on the add to bag button$/, () => {
    const productTitle = quickviewPage.clickAddToBag()
    // verify the title is consistant throughout the flow
    assert.ok(
      process.env.prodName.toLowerCase() === productTitle.toLowerCase(),
      'Product title in QuickView not same as PDP Item selected'
    )
  })
  this.When(
    /^I click on the link to see full details button$/,
    quickviewPage.clickProductPageLink
  )

  this.When(/^I opt to CHECKOUT$/, quickviewPage.selectCheckoutOpt)
  this.When(/^I get the current size$/, () => {
    const size = quickviewPage.getProductSize
    assert.ok(
      size !== '',
      'The product size has not returned from quickviewTitle Container'
    )
  })
  this.When(/^I get the current quantity$/, () => {
    const qty = quickviewPage.getProductQty
    assert.ok(
      qty !== '',
      'The product qty has not returned from quickviewTitle Container'
    )
  })
  this.When(/^I select a size in Quick View$/, quickviewPage.setProductSize)

  this.Then(/^I can see the product details$/, () => {
    const productTitle = quickviewPage.getProductTitle()
    assert.ok(
      productTitle !== '',
      'The product has no Title in quickviewTitle Container'
    )
  })
  this.Then(/^I am redirected to the correct Product page$/, () => {
    const productTitle = quickviewPage.verifyURLisAsExpected()
    console.log(`productTitle is ${productTitle}`)
    console.log(
      `At product page from QuickView : ${
        process.env.prodName
      } = ${productTitle}`
    )
    assert.ok(
      process.env.prodName.toLowerCase() === productTitle.toLowerCase(),
      'Product Page title in QuickView not same as initial PLP Item selected'
    )
  })

  this.Then(/^I can see the login page$/, () => {
    const state = quickviewPage.checkLoginPageVisible
    assert.ok(state, 'Shopping Bag not displayed from QuickView Page')
  })
}
