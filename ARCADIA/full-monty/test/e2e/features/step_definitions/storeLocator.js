import * as storeLocator from '../../models/newPageObjects/storeLocator/storeLocatorPO'
import * as storeLocatorOLD from '../../models/newPageObjects/storeLocator'

const assert = require('assert')

module.exports = function() {
  this.Given(/^I select "(.*)" store$/, storeLocatorOLD.selectStore)
  this.Given(
    /^I navigate to "(.*)" store listing page$/,
    storeLocatorOLD.countryStores
  )
  this.Given(/^I select my country as "(.*)"$/, storeLocatorOLD.selectCountry)
  this.Given(
    /^I select a new country$/,
    storeLocatorOLD.selectRandomCountryByIndex
  )
  this.Given(
    /^I select a different country$/,
    storeLocatorOLD.selectRandomCountryByIndex2
  )
  this.When(/^I search for stores in "(.*)"$/, storeLocatorOLD.setSearch)
  this.When(/^I select go$/, storeLocatorOLD.selectGo)
  this.When(
    /^I click the find in store button$/,
    storeLocatorOLD.clickFindInStoreButton
  )
  this.When(
    /^I search for this product size near '(.*)'$/,
    storeLocatorOLD.searchForProductNearAddress
  )
  this.When(
    /^I click the locate me button$/,
    storeLocatorOLD.clickLocateMeButton
  )
  this.Then(
    /^I should see locations in "(.*)" displayed in search helper$/,
    storeLocatorOLD.hasSuggestion
  )
  this.Then(
    /^I can see "(.*)" in the list of stores$/,
    storeLocatorOLD.listOfStoresContains
  )
  this.Then(
    /^I should see a list of stores for the brand$/,
    storeLocatorOLD.listOfStoresForSelectedBrand
  )
  this.Then(
    /^I can see that "(.*)" store is minimised$/,
    storeLocatorOLD.storeIsMinimised
  )
  this.Then(
    /^I can see "(.*)" store display on top$/,
    storeLocatorOLD.storeOnTop
  )
  this.Then(
    /^I can see "(.*)" store expands to display store details$/,
    storeLocatorOLD.storeIsExpanded
  )
  this.Then(
    /^I can see a list showing where this product is available$/,
    storeLocatorOLD.storeListShownForProduct
  )

  this.When(/^I select the first Store result$/, () => {
    storeLocator.getFirstResultLoc()
    const name = storeLocator.getStoreName()
    assert(name === 'Norwich', 'Store name not as expected!')
    const dist = storeLocator.getStoreDist()
    assert(dist === '0.20 miles', 'Store dist not as expected!')
  })

  this.Then(
    /^I verify Store result has (.*) and (.*)$/,
    (firstName, firstDist) => {
      storeLocator.getFirstResultLoc()
      const name = storeLocator.getStoreName()
      assert(
        name.includes(firstName),
        `Store name not as expected! EXP:${firstName} - ACT:${name}`
      )
      const dist = storeLocator.getStoreDist()
      assert(
        dist === firstDist,
        `Store dist not as expected! EXP:${firstDist} - ACT:${dist}`
      )
    }
  )

  this.Then(/^I verify Store result has number (.*)$/, (storeNumber) => {
    const actualStoreNumber = storeLocator.getStoreNumber()
    assert(
      actualStoreNumber === storeNumber,
      `Store phone number not as expected! EXP:${storeNumber} - ACT:${actualStoreNumber}`
    )
  })

  this.Then(/^is open times (.*) and (.*)$/, (weekDays, openHours) => {
    const actualWeekDays = storeLocator.getStoreWeekdays()
    const actualOpenHours = storeLocator.getStoreOpenHours()
    console.log(`Store : ${storeLocator.getStoreName()}`)
    assert(
      actualWeekDays === weekDays,
      `Store week days info not as expected! EXP:${weekDays} - ACT:${actualWeekDays}`
    )
    assert(
      actualOpenHours === openHours,
      `Store opening hours not as expected! EXP:${openHours} - ACT:${actualOpenHours}`
    )
  })

  this.Then(/^I click the accordion icon in first result$/, () => {
    storeLocator.clickAccordionIcon()
  })
}
