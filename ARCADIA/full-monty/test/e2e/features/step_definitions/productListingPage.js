import { times } from 'ramda'

import * as productListingPage from '../../models/pageObjects/productListingPage'
import * as productListing from '../../models/newPageObjects/productListing/productListingBase'
import * as filterRefinements from '../../models/newPageObjects/productListing/filterRefinements'
import * as shared from '../../models/lib/shared'

module.exports = function() {
  this.Given(
    /^I have scrolled to the end of the list$/,
    productListingPage.infinityScrollToBottom
  )
  this.Given(/^I have scrolled to the end of the list "(.*)" times$/, (n) => {
    times(productListingPage.infinityScrollToBottom, n)
  })
  this.Given(
    /^I am viewing a list of products$/,
    productListingPage.isOnProductList
  )
  this.Given(
    /^I am viewing a list of more than 20 products$/,
    productListingPage.isOnProductListWithMoreThan20Products
  )
  this.Given(/^I filter the product list$/, productListingPage.filterProducts)
  this.Given(/^I filter by rating$/, productListingPage.filterByRating)

  this.Given(/^I apply these filters$/, filterRefinements.applyFilters)

  this.Given(/^I clear all filters$/, productListingPage.clearFilters)

  this.Given(/^I filter by colour$/, productListingPage.filterByColour)

  this.Given(
    /^I select a different available size to filter with$/,
    productListingPage.selectDifferentAvailableSize
  )
  this.Given(/^I filter by size$/, productListingPage.filterBySize)
  this.Given(/^I filter by price$/, productListingPage.filterByPrice)
  this.Given(
    /^I increase price min filter$/,
    productListingPage.increaseMinPrice
  )
  this.Given(
    /^I increase price max filter$/,
    productListingPage.increaseMaxPrice
  )
  this.Given(
    /^I can see a product description$/,
    productListingPage.hasDescription
  )
  this.Given(/^I can see a product pricing$/, productListingPage.hasPrice)
  this.Given(/^I can see a product image$/, productListingPage.hasImage)
  this.Given(
    /^I switch product grid view to (.*)$/,
    productListingPage.switchProductGridView
  )
  this.Given(
    /^I can see (.*) product at a time$/,
    productListingPage.gridViewHasNColumns
  )
  this.Given(/^I sort products by '(.*)'$/, productListingPage.sortProducts)
  this.Given(
    /^I can see a sorted list of products$/,
    productListingPage.containsProducts
  )
  this.Given(/^I search "(.*)" on products$/, (searchTerm) =>
    productListingPage.searchForProducts(searchTerm)
  )
  this.When(/^I select the back to top button$/, () =>
    shared.clickItem('BackToTop', 'returnButton')
  )
  this.When(
    /^I scroll down to the bottom of the product list$/,
    productListingPage.scrollToBottom
  )
  this.When(/^I select a product$/, productListingPage.selectProductInView)
  this.When(
    /^I view the product page$/,
    productListingPage.waitForProductDetail
  )
  this.When(
    /^I go back to the product list$/,
    productListing.navigateBackToProductListing
  )
  this.When(/I sort by '(.*)'$/, productListingPage.selectRatingDescending)
  this.When(/^I switch to Product view$/, productListingPage.switchProductView)
  this.When(/^I switch to Outfit view$/, productListingPage.switchOutfitView)
  this.When(
    /^I have clicked on the quickview icon$/,
    productListingPage.clickOnQuickviewIcon
  )
  this.Then(/^I see a back to top button$/, () =>
    shared.isVisible('BackToTop', 'returnButton')
  )
  this.Then(/^I am taken to the top of the page$/, shared.viewPageTop)
  this.Then(
    /^More products are loaded onto the page$/,
    productListingPage.productsLoaded
  )
  this.Then(/^I no longer see the back to top button$/, () =>
    shared.notVisible('BackToTop', 'returnButton')
  )
  this.Then(
    /^I can see the product I selected$/,
    productListingPage.lastVisitedItemIsVisible
  )
  this.Then(
    /^I am directed to the Product Listing page matching "(.*)"$/,
    productListingPage.checkProductListingHeaderMatchingText
  )
  this.Then(
    /^I can see bundle badge for "(.*)" in product listing$/,
    productListingPage.checkBundleBadge
  )
  this.Then(
    /^Filter button has (.*) filter$/,
    productListingPage.buttonHasFilters
  )
  this.Then(
    /^Filter button has no filters$/,
    productListingPage.buttonHasNoFilters
  )

  this.Then(/^All products have no ratings$/, productListingPage.noRatings)

  this.Then(
    /^Filter returns a product list$/,
    productListingPage.containsProducts
  )
  this.Then(/^I see the Product Images$/, productListingPage.verifyProductImgs)
  this.Then(/^I see the Modelled Images$/, productListingPage.verifyModelImgs)
}
