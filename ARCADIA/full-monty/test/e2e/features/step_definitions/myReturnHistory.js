import * as myReturnHistory from '../../models/newPageObjects/myAccount/returnHistory_po'
import assert from 'assert'

module.exports = function() {
  this.When(/^I select my returns$/, myReturnHistory.goToMyReturnHistory) // need to replace this line with the one above after dev complete
  this.Then(/^The page title reads "(.*)"$/, (returnsTitleText) => {
    assert.equal(
      myReturnHistory.getPageHeader(),
      returnsTitleText,
      `page header for returns not found, instead found: ${myReturnHistory.getPageHeader()}`
    )
  })
  this.Then(
    /^A list of returns is visible with correct number of lines$/,
    () => {
      const expectedNumberOfLines = 4
      const ordersWithWrongNumberOfLines = myReturnHistory
        .getReturnsList()
        .filter((item) => {
          return item.split('\n').length !== expectedNumberOfLines
        })
      assert.equal(
        ordersWithWrongNumberOfLines,
        0,
        `return orders in list did not contain expected number of lines`
      )
    }
  )
}
