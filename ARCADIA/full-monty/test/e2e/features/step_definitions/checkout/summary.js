import * as checkoutSummary from '../../../models/pageObjects/checkoutSummary'
import * as checkoutSummaryConfirmation from '../../../models/newPageObjects/checkout/CheckoutSummaryConfirmation_po'
import * as summaryLogin from '../../../models/newPageObjects/checkout/SummaryConfirmLogin_po'

const assert = require('assert')

module.exports = function() {
  this.Then(/^I validate the email "(.*)"$/, summaryLogin.enterEmailAndVerify)
  this.Then(
    /^I should see a validation error: "(.*)"$/,
    checkoutSummary.validationError
  )
  this.Then(
    /^I should see the description: "(.*)"$/,
    checkoutSummary.validationDescription
  )
  this.Then(/^I add the password "(.*)"$/, checkoutSummary.addPassword)
  this.Then(
    /^I add the confirmation password "(.*)"$/,
    checkoutSummary.addConfirmPassword
  )
  this.Then(/^I should see no validation errors$/, () => {
    assert.ok(
      browser.isVisible('.Input-validationMessage') !== true,
      'there was a validation message present, check screen shot'
    )
  })
  this.When(
    /^I edit the product on the summary page$/,
    checkoutSummary.clickEditProduct
  )
  this.When(
    /^I delete an item from my bag$/,
    checkoutSummaryConfirmation.deleteItemFromBag
  )
  this.When(
    /^I change the product quantity and save the product in the summary$/,
    checkoutSummary.updateQuantity
  )
  this.Then(
    /^I should see the edit product options$/,
    checkoutSummary.editProductOptionsPresent
  )
  this.Then(
    /^The product summary quantity should be updated$/,
    checkoutSummary.quantityUpdated
  )
  this.Then(
    /^I should see the promotion code "(.*)" on the summary page$/,
    checkoutSummary.promotionCodePresent
  )
  this.Given(
    /^I should see a email verification message: "(.*)"$/,
    checkoutSummary.validationDescription
  )

  this.Then(/^I should see a try another email option$/, () => {
    assert.ok(
      summaryLogin.tryAnotherEmailPresent() === true,
      'try another email link is not visible'
    )
  })
}
