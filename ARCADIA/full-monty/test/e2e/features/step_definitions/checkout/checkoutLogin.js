import * as checkoutLogin from '../../../models/newPageObjects/checkout/checkoutLogin'

module.exports = function() {
  this.Given(
    /^I have checked out as a guest to the delivery address page$/,
    checkoutLogin.checkoutAsGuest
  )
}
