import * as billingYourDetailPO from '../../../models/newPageObjects/checkout/BillingYourDetails'
import * as CheckoutPaymentCard from '../../../models/newPageObjects/checkout/CheckoutPaymentCard_po'
import * as customerProfile from '../../../models/lib/customerProfiler'
import * as creditCard from '../../../models/lib/creditCard'

module.exports = function() {
  this.Given(/^I enter "(.*)" billing details$/, (countryName) => {
    // retrieve country profile data
    customerProfile.setCustomerProfileDefaults(countryName)
    // set billing details
    billingYourDetailPO.enterBillingDetails()
  })
  this.Given(/^I pay with "(.*)"$/, (cardName) => {
    const cardType = creditCard.getCreditCardKey(cardName)
    CheckoutPaymentCard.setPaymentDetails(cardType)
  })
}
