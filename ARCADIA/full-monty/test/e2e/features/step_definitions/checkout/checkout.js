import * as delivery from '../../../models/pageObjects/delivery'
import * as deliveryOptions from '../../../models/newPageObjects/checkout/DeliveryOptions_po'
import * as shoppingBag from '../../../models/pageObjects/shoppingBag'
import * as payment from '../../../models/pageObjects/payment'
import * as checkoutLogin from '../../../models/newPageObjects/checkout/checkoutLogin'
import * as checkoutSummary from '../../../models/pageObjects/checkoutSummary'
import * as myAccount from '../../../models/pageObjects/myAccount'
import * as deliveryFindAddressPO from '../../../models/newPageObjects/checkout/FindAddress_po'
import * as summaryConfirmBasePO from '../../../models/newPageObjects/checkout/SummaryConfirmBase_po'
import * as summaryConfirm from '../../../models/newPageObjects/checkout/CheckoutSummaryConfirmation_po'
import * as summaryLogin from '../../../models/newPageObjects/checkout/SummaryConfirmLogin_po'
import * as login from '../../../models/newPageObjects/login_po'
import * as shared from '../../../models/lib/shared'
import * as client from '../../../models/lib/client'
import * as manualAddress from '../../../models/newPageObjects/checkout/ManualAddress_po'

const assert = require('assert')

const nominatedDayDeliverySelector = 'select[name="HOME_EXPRESS"]'

module.exports = function() {
  this.Given(
    /^the user (.*) with password (.*) has an empty shopping bag$/,
    myAccount.loginAndEmptyBasket
  )
  this.When(
    /^I complete the purchase with buy now pay later$/,
    payment.completePurchaseWithKlarnaAsAnApprovedUser
  )
  this.When(
    /^I complete the purchase with buy now pay later as a new Klarna user$/,
    payment.completePurchaseWithKlarnaAsANewUser
  )
  this.When(
    /^I complete the purchase with buy now pay later as an existing user$/,
    payment.completePurchaseWithKlarnaAsARegisteredUser
  )
  this.When(
    /^I enter the delivery Details "(.*)", "(.*)", "(.*)", "(.*)"$/,
    delivery.setDeliveryNameDetails
  )
  this.When(
    /^I select the delivery country "(.*)"$/,
    deliveryFindAddressPO.selectDeliveryCountry
  )
  this.When(
    /^I enter the find address postcode "(.*)"$/,
    deliveryFindAddressPO.enterPostcode
  )
  this.When(
    /^I enter the find address house number "(.*)"$/,
    deliveryFindAddressPO.enterHouseNumber
  )
  this.When(/^I enter and verify my email$/, delivery.acceptAndVerify)

  this.When(/^I verify my email$/, delivery.Verify)
  this.When(
    /^I submit the "Try another email address"$/,
    delivery.tryAnotherEmail
  )
  this.When(
    /^I checkout as the logged in user without checkout profile$/,
    () => {
      checkoutLogin.checkoutAsUser(process.env.USERNAME, process.env.PASSWORD)
    }
  )
  this.When(/^Confirm the order$/, delivery.confirmOrder)
  this.When(
    /^Confirm the order as an existing user$/,
    delivery.confirmOrderAsARegisteredUser
  )
  this.When(/^I verify as "(.*)"$/, delivery.verifyAsAUser)
  this.When(/^I verify as an existing user$/, delivery.verifyAsARegisteredUser)
  // this.When(
  //   /^I confirm the order as an existing user and merging products in bag$/,
  //   delivery.confirmOrderAsARegisteredUserAndMergeBag
  // )
  this.When(
    /^I log in as the existing user on the confirmation page$/,
    delivery.loginToConfirmationWithRegisteredUser
  )
  this.When(/^I merge the contents of the bag$/, delivery.mergeBag)
  this.Then(
    /^I should receive the payment can not be processed message$/,
    delivery.confirmPaymentNotProcessable
  )
  this.Then(/^the login form should be cleared$/, delivery.loginIsCleared)
  this.Then(/^I should have both items on the confirmation page$/, () =>
    shoppingBag.containsNumberOfProducts(2)
  )
  this.Then(
    /^I should be redirected to the complete order page$/,
    delivery.confirmationPagePresent
  )
  this.Then(
    /^I should be redirected from paypal to the complete order page$/,
    delivery.confirmationPagePresent
  )
  this.Then(
    /^I should be redirected from verified by visa to the complete order page$/,
    delivery.confirmationPagePresent
  )
  this.Then(
    /^the checkout button should be disabled$/,
    delivery.confirmOrderButtonDisabled
  )
  this.Then(
    /^I do not enter anything in to the postcode field$/,
    delivery.enterBlankInToPostcode
  )
  this.Then(/^I submit the Find Address$/, delivery.submitAddress)
  this.Then(
    /^I should see Find Address disabled$/,
    deliveryFindAddressPO.findAddressDisabled
  )
  this.Then(/^The "(.*)" link should be displayed$/, delivery.checkLinkText)
  this.Then(
    /^I should see following select your address option: Flat, 72 Springbank Road, LONDON SE13 6SX$/,
    delivery.suggestedAddressPresent
  )
  this.Then(/^I should see a clear form link$/, () => {
    assert.ok(
      manualAddress.clearFormlinkVisible(),
      'Clear form link is not visible'
    )
  })
  this.Then(
    /^I should have be presented with the manual address form$/,
    delivery.manunalAddressFormPresent
  )
  this.Then(/^I should see a find address link$/, delivery.findAddressExists)
  this.Then(
    /^I received the no-address error message$/,
    delivery.noAddressError
  )
  this.Then(
    /^I received an error message for "(.*)" as "(.*)"$/,
    delivery.validationError
  )
  this.Then(/^I checkout as the logged in user$/, checkoutLogin.checkout)
  this.Then(/^I am directed to the checkout login page$/, () => {
    assert.ok(login.loginPagePresent(), 'the login page was not present')
  })
  this.Then(
    /^I checkout as the user (.*) and password (.*)$/,
    checkoutLogin.checkoutAsUser
  )
  this.Then(
    /^I checkout as user to the billing page$/,
    checkoutLogin.checkoutAsLoggedInToBillingPage
  )
  this.Then(/^I provide delivery details$/, delivery.fillDeliveryForm)
  this.Then(/^I provide billing details$/, payment.fillBillingForm)
  this.Then(/^I enter the polish address$/, delivery.enterPolishAddress)
  this.Then(
    /^I provide bad billing details$/,
    payment.fillBillingFormWithBadVisa
  )
  this.Then(
    /^I provide new checkout account details$/,
    checkoutSummary.addNewCheckoutAccount
  )
  this.Then(/^I should see a checkout validation error: "(.*)"$/, (msg) => {
    client.includesText('.Message-message', msg)
  })
  this.Then(
    /^the terms and conditions should not be checked by default$/,
    () => {
      assert.ok(
        summaryConfirm.termsAndConditionsSelected() === false,
        'T&Cs is checked when it should not'
      )
    }
  )
  this.Then(/^the newsletter option should not be visible$/, () => {
    assert.ok(
      summaryLogin.newsletterOptionPresent() === false,
      'newsletter option was present, this should not be the case'
    )
  })
  this.Then(/^I view the below message$/, delivery.confirmationPagePresent)
  this.Then(/^I should see a Forgotten password option$/, () => {
    assert.ok(
      summaryLogin.forgottenPasswordSectionClosed() === true,
      `forgotten password visible: ${summaryLogin.forgottenPasswordSectionClosed()} did not match true`
    )
  })
  this.Then(
    /^the payment options should include "(.*)"$/,
    payment.paymentTypeAvailable
  )
  this.Then(
    /^the payment options should not include "(.*)"$/,
    payment.paymentTypeNotAvailable
  )
  this.Then(/^I can see at least one product displayed$/, () => {
    const productName = summaryConfirmBasePO.getProductName()
    assert.ok(
      productName.length > 0,
      `There are no products displayed in checkout summary`
    )
  })
  this.Then(/^the product price is more than zero$/, () => {
    const productPrices = summaryConfirmBasePO.getAllProductPrice()
    let isMoreThanZero = true

    productPrices.forEach((rawPrice) => {
      const productPriceStr = shared.removeCurrencySign(rawPrice)
      const productPrice = parseFloat(productPriceStr)

      if (isMoreThanZero && productPrice <= 0) {
        isMoreThanZero = false
      }
    })
    assert.ok(
      isMoreThanZero,
      `Expected all product prices to be more than zero. Actual: ${productPrices}`
    )
  })
  this.Then(/^the total price is more than zero$/, () => {
    let totalPriceStr = summaryConfirmBasePO.getTotalPrice()
    totalPriceStr = shared.removeCurrencySign(totalPriceStr)
    const totalPrice = parseFloat(totalPriceStr)
    assert.ok(
      totalPrice > 0,
      `Actual: ${totalPrice}. Total price should be more than 0`
    )
  })
  this.Then(/^I navigate to the checkout$/, delivery.navigateToCheckout)
  this.Then(/^I navigate to the payment checkout$/, payment.navigateToPayment)
  this.Then(/^I select Free Standard delivery$/, () => {
    deliveryOptions.setStandardDeliveryType()
  })
  this.Then(/^I select Express \/ Nominated delivery$/, () => {
    deliveryOptions.setExpressDeliveryType()
  })
  this.Then(/^Delivery Location Home Delivery should be selected$/, () => {
    assert.ok(
      deliveryOptions.homeDeliveryOptionSelected(),
      'home delivery option was not selected'
    )
  })
  this.Then(/^Delivery Type Standard Delivery should be selected$/, () => {
    assert.ok(
      deliveryOptions.standardDeliveryTypeSelected(),
      'standard delivery option was not selected'
    )
  })
  this.Then(/^Delivery Type Express Delivery should be selected$/, () => {
    assert.ok(
      deliveryOptions.expressDeliveryTypeSelected(),
      'express delivery option was not selected'
    )
  })
  this.Then(/^The Delivery charge is hidden$/, () => {
    assert.ok(
      deliveryOptions.deliveryPriceVisible() === false,
      'Delivery charge is visible when it should be hidden'
    )
  })
  this.Then(/^Express delivery options are visible$/, () => {
    assert.ok(
      deliveryOptions.expressDeliveryOptionsVisible(),
      'Express delivery option is hidden when it should be visible'
    )
  })
  this.Then(/^I select an express delivery option from the drop down$/, () =>
    deliveryOptions.setExpressDeliveryDayOption(1)
  )
  this.Then(
    /^The shown delivery costs should be the same as the option chosen$/,
    delivery.confirmSelectedExpressDeliveryDetails
  )
  this.Then(/^delivery date selected should match option chosen$/, () => {
    const selectedValue = deliveryOptions.getExpressDeliveryOption()
    const actual = browser.getText(
      `${nominatedDayDeliverySelector} option[value="${selectedValue}"]`
    )
    const expected = process.env.EXPRESS_DELIVERY_DATE
    assert.equal(
      actual,
      expected,
      `'${actual}' did not match the expected date: '${expected}'`
    )
  })
  this.Then(/^I select the title as "Miss"$/, delivery.selectTitle)
  this.Then(
    /^I select the delivery title as "Miss"$/,
    delivery.HomeDeliveryTitle
  )
  this.Then(/^I select the cardType as "Visa"$/, payment.paymentType)
  this.Then(/^I click the "What’s a CVV"$/, payment.openWhatsACVV)
  this.Then(
    /^I can view an modal window with CMS content$/,
    payment.whatsACVVModalOpen
  )
  this.Then(/^I select the expiryMonth as "01"$/, payment.expiryMonth)
  this.Then(/^I select the expiryYear as "2018"$/, payment.expiryYear)
  this.Then(
    /^I enter the guest email as "(.*)"$/,
    delivery.setCheckoutEmailAddress
  )
  this.Then(/^I click next$/, payment.clickNext)
  this.Then(
    /^I enter a value as "(.*)" for my "(.*)"$/,
    delivery.SetDeliveryDetail
  )
}
