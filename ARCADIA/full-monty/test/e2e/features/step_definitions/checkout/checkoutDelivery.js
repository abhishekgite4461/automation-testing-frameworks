import * as delivery from '../../../models/pageObjects/delivery'
import * as shoppingBag from '../../../models/pageObjects/shoppingBag'
import * as customerProfile from '../../../models/lib/customerProfiler'
import * as deliveryOptions from '../../../models/newPageObjects/checkout/DeliveryOptions_po'

module.exports = function() {
  this.Given(
    /^I have checked out with the default uk delivery and verified by visa billing info$/,
    delivery.checkoutWithDefaultVBVDetails
  )
  this.Given(/^I enter "(.*)" delivery details$/, (countryName) => {
    // retrieve country-specific profile and set that as default profile
    customerProfile.setCustomerProfileDefaults(countryName)
    // set delivery details
    shoppingBag.enterDefaultDeliveryInfo()
  })
  this.Given(/^Confirm the order with verified by visa$/, delivery.confirmVBV)
  this.Given(
    /^I have checked out with the default uk delivery and paypal billing info$/,
    delivery.checkoutWithPaypal
  )
  this.Given(
    /^I have checked out with the saved address and change the payment type to paypal and checkout$/,
    delivery.loggedInSavedProfilePaypalCheckout
  )
  this.Given(
    /^I select "Express \/ Nominated day" delivery type$/,
    deliveryOptions.setExpressDeliveryType
  )
  this.When(
    /^I update delivery option to "Collect from Store"$/,
    deliveryOptions.setCollectFromStoreDeliveryTypeOption
  )
  this.Then(
    /^delivery country should be unselected$/,
    delivery.confirmDeliveryCountryIsUnselected
  )
  this.Then(
    /^I should see "Collect from Store" delivery option selected$/,
    deliveryOptions.collectFromStoreOptionSelected
  )
}
