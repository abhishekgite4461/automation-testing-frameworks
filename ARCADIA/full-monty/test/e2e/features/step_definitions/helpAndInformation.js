import * as client from '../../models/lib/client'

module.exports = function() {
  this.Then(/^I should see e-Receipts page$/, () =>
    client.hasTitle('e-receipts')
  )
}
