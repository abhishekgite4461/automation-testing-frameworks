import * as myAccount from '../../models/pageObjects/myAccount'
import * as client from '../../models/lib/client'
import * as sidebar from '../../models/newPageObjects/navigation/sidebar'
import * as myAccountPo from '../../models/newPageObjects/myAccount/myAccount_po'
import * as topNavBar from '../../models/newPageObjects/navigation/topNavigationBar'
import * as myDetailsPo from '../../models/newPageObjects/myAccount/myDetails_po'
import * as orderHistoryDetails from '../../models/newPageObjects/myAccount/orderHistoryDetails_po'
import assert from 'assert'

module.exports = function() {
  this.Given(/^I log out$/, client.logout)
  this.Given(/^I am an authenticated customer$/, myAccount.authenticated)
  this.Given(
    /^I am an authenticated customer with checkout$/,
    myAccount.authenticatedWithCheckout
  )
  this.Given(
    /^The user clicks my checkout details link$/,
    myAccount.clickMyCheckoutDetails
  )
  this.Given(
    /^I log in with (.*) and password (.*)$/,
    myAccount.openLoginPageAndLogin
  )
  this.Given(
    /I log in as existing user$/,
    myAccount.openLoginPageAndLoginAsPreviouslyRegisteredUser
  )
  this.Given(/^I have registered as a new user$/, myAccount.registerRandomUser)
  this.Given(/^I have registered as a new Klarna user$/, () =>
    myAccount.registerRandomKlarnaUser('requireSignUp')
  )
  this.Given(/^I have registered as an approved Klarna user$/, () =>
    myAccount.registerRandomKlarnaUser('approved')
  )
  this.Given(
    /^I have registered as a new full profile user with card type (.*), card number (.*) and the cvv (.*)/,
    myAccount.registerRandomFullProfileUser
  )

  this.Given(
    /^login with clean basket for (.*) and password (.*)$/,
    myAccount.openLoginPageAndLoginAndEmptyBasket
  )
  this.Given(
    /^when prompted I login with the email (.*) and password (.*)$/,
    myAccount.loginWithoutOpeningMenu
  )
  this.Given(/^I open the menu$/, sidebar.openSidebar)
  this.Given(/^I navigate to (.*) page$/, client.goToPage)
  this.Given(/^I am on the registration page$/, myAccount.navigateToMyAccount)
  // my details
  this.Given(/^I am on My Details page$/, myAccount.goToMyDetailsPage)
  this.When(/^I select My Account$/, myAccount.clickMyAccount)
  this.When(/^I navigate to My Account$/, myAccount.navigateToMyAccount)
  this.When(
    /^I select the (.*) option from the list items on the (.*) page$/,
    myAccount.selectOptionFromMyAccountList
  )
  this.When(/^I change my current password$/, myAccount.changeCurrentPassword)
  this.When(
    /^I enter unequal values for my new password$/,
    myAccount.changePasswordUnequalValues
  )

  this.When(
    /^I submit the forgot password form with an email address that is not registered$/,
    myAccount.submitForgotPasswordFormNotRegistered
  )
  this.When(
    /^I enter an incorrect current password into the change password form$/,
    myAccount.enterIncorrectCurrentPassword
  )
  this.When(
    /^I enter a new password that is too short$/,
    myAccount.enterShortNewPassword
  )
  this.When(
    /^I enter my current password into the new password field$/,
    myAccount.enterCurrentPasswordAsNewPassword
  )
  this.When(
    /^I enter my username into the new password$/,
    myAccount.enterUsernameAsPassword
  )
  this.When(
    /^I enter my email (.*) into the new password$/,
    myAccount.enterEmailAsPassword
  )
  this.When(
    /^I log in with an unregistered account$/,
    myAccount.unregisteredAccountLogin
  )
  this.When(
    /^I enter an incorrect email address (.*)$/,
    myAccount.enterEmailAddress
  )
  this.When(/^I enter an email address (.*)$/, myAccount.enterEmailAddress)
  this.When(/^I enter an incorrect password (.*)$/, myAccount.enterPassword)
  this.When(
    /^I choose to view the entered password$/,
    myAccount.showEnteredPassword
  )
  // registration
  this.When(
    /^I enter the registration details requested with a password of less than 6 characters$/,
    myAccount.enterRegistrationFirstPassword
  )
  this.When(
    /^I enter the registration details requested without a password$/,
    myAccount.enterRegistrationEmail
  )
  this.When(
    /^I enter the registration details requested with a incorrectly formatted email address$/,
    myAccount.enterIncorrectFormatRegistrationEmail
  )
  this.When(
    /^I enter the registration details requested but leave the email blank$/,
    myAccount.enterBlankRegistrationEmail
  )
  this.When(
    /^I enter the registration details requested with a email already registered$/,
    myAccount.enterUsedRegistrationEmail
  )
  // my details
  this.When(
    /^I enter my details form without first name$/,
    myAccount.enterMyAccountDetailsWithBlankFirstName
  )
  this.When(
    /^I enter my details form without last name$/,
    myAccount.enterMyAccountDetailsWithBlankLastName
  )
  this.When(
    /^I enter my details form without email address$/,
    myAccount.enterMyAccountDetailsWithBlankEmail
  )
  this.When(
    /^I enter my details form with an invalid email address$/,
    myAccount.enterMyAccountDetailsWithInvalidEmail
  )
  this.When(
    /^I should see back to My Account$/,
    myAccount.hasBackToMyAccountLink
  )
  this.Then(/^I am directed to the login page$/, myAccount.isOnLoginPage)
  this.Then(
    /^I am directed to the my account page$/,
    myAccount.isOnMyAccountPage
  )

  this.Then(/^I can view (.*) option$/, myAccount.myAccountSectionIsShown)
  this.Then(
    /^I see the change password form$/,
    myAccount.changePasswordFormVisible
  )
  this.Then(
    /^I see a success message on the change password page$/,
    myAccount.changePasswordSuccessMessageVisible
  )
  this.Then(
    /^I see an error message outlining the unequal values$/,
    myAccount.unequalPasswordsMessageVisible
  )

  this.Then(
    /^I see an error message showing that my email address does not exist$/,
    myAccount.forgotPasswordErrorMessageEmailNotFound
  )

  this.Then(
    /^I see an error showing that my new password must be at least 6 characters$/,
    myAccount.newPasswordTooShortErrorVisible
  )
  this.Then(
    /^I see an error showing that my new password cannot be the same as my old password$/,
    myAccount.newPasswordEqualToCurrentErrorVisible
  )
  this.Then(
    /^I see an error showing that my new password cannot be the same as my username$/,
    myAccount.newPasswordEqualToUsernameErrorVisible
  )
  this.Then(
    /^I see email address or password error message$/,
    myAccount.emailOrPasswordErrorMessageVisible
  )
  this.Then(
    /^I see an email validation message "(.*)" displayed in login form$/,
    myAccount.incorrectEmailAddressMessageVisible
  )
  this.Then(
    /^I see a password validation message "(.*)" displayed in login form$/,
    myAccount.incorrectPasswordMessageVisible
  )
  // registration
  this.Then(
    /^I should get a validation message indicating my password is less than 6 characters$/,
    myAccount.registrationPassUnderSixChar
  )
  this.Then(
    /^I should get a validation message indicating I must enter a password$/,
    myAccount.registrationPasswordNotEnteredValidation
  )
  this.Then(
    /^I should receive a validation message indicating the email address format is incorrect$/,
    myAccount.registrationIncorrectEmailFormatVaidation
  )
  this.Then(
    /^I should receive a validation message indicating that I must enter an email$/,
    myAccount.registrationBlankEmailValidation
  )
  this.Then(
    /^I should receive a validation message indicating the email is already registered$/,
    myAccount.EmailAlreadyRegisteredValidation
  )
  this.Then(
    /^I should see the new to topshop message$/,
    myAccount.newToTopshopHeaderMessage
  )
  this.Then(
    /^I see the new to website message$/,
    myAccount.newToBrandWebsiteHeaderMessage
  )
  this.Then(
    /^I should see the newsletter option$/,
    myAccount.NewsletterOptionPresent
  )
  this.Then(
    /^I should see the newsletter option checked by default$/,
    myAccount.newsletterOptionChecked
  )
  this.Then(
    /^I should see the newsletter option unchecked by default$/,
    myAccount.newsletterOptionUnChecked
  )
  this.Then(
    /^I should see the currently entered password$/,
    myAccount.passwordIsShown
  )
  // my details
  this.Then(
    /^I should see my account details pre-filled$/,
    myAccount.hasExistingMyAccountDetailsPrefilled
  )
  this.Then(
    /^the account has the default my account details$/,
    myAccount.setMyAccountDefaultValues
  )
  this.Then(
    /^I should see validation message indicating I must enter my first name$/,
    myAccount.myDetailsBlankfirstNameValidation
  )
  this.Then(
    /^I should see validation message indicating I must enter my last name$/,
    myAccount.myDetailsBlanklastNameValidation
  )
  this.Then(
    /^I should see validation message indicating I must enter my email address$/,
    myAccount.myDetailsBlankEmailValidation
  )
  this.Then(
    /^I should see validation message indicating that I must enter a valid email address$/,
    myAccount.myDetailsInvalidEmailValidation
  )
  // My Orders
  this.Then(/^The page title reads My orders$/, myAccount.myOrdersTitle)
  this.When(/^I am on the My Orders page$/, myAccount.navigateToMyOrders)
  this.When(
    /^The order history list is empty: it reads There were no orders found$/,
    myAccount.myOrdersEmpty
  )

  this.Then(
    /^An order number is visible in order history$/,
    myAccount.orderNumberIsVisible
  )
  this.Then(/^An order date is visible in order history$/, () => {
    const hasOrderDate = myAccount.orderDateIsVisible()
    assert.ok(hasOrderDate, 'failed, A date is not visible on page')
  })
  this.Then(/^An order total is visible in order history$/, () => {
    const hasOrderTotal = myAccount.orderTotalIsVisible()
    assert.ok(hasOrderTotal, 'failed, An order total is not visible on page')
  })

  this.Then(
    /^A shipping address section is visible in order details$/,
    myAccount.shippingAddressSectionIsVisible
  )

  this.Then(
    /^An item code is present on order details$/,
    myAccount.itemCodeIsPresent
  )
  this.Then(
    /^An item price is present on order details$/,
    myAccount.priceIsPresent
  )

  this.Then(
    /^A tracking number is present on order details$/,
    myAccount.trackingNumberIsPresent
  )
  this.Then(
    /^A star card number is present on order details$/,
    myAccount.starCardNumberIsPresent
  )

  this.Then(
    /^the order im viewing should have the correct default delivery and billing address$/,
    myAccount.orderHistoryOrderHasDefaultAddresses
  )
  this.Then(
    /^my details should have the same delivery and billing address entered for the purchase$/,
    myAccount.ConfirmMyDetailsContainDefaultAddressesSaved
  )
  this.Then(
    /^I should be able to select able to select payment options in my checkout details$/,
    myAccount.confirmPaymentOptionsPresentInMyCheckOutDetails
  )
  this.When(/^I select the sign in link$/, topNavBar.clickSignInLink)
  this.Then(/^I should see the following menus$/, (table) => {
    // eslint-disable-next-line no-restricted-syntax
    for (const [menuOpts] of table.raw()) {
      assert.ok(
        myAccountPo.getMyAccountsOptions().includes(menuOpts),
        `${menuOpts} not found`
      )
    }
  })
  this.Given(/^I select My Details$/, myAccountPo.openMyDetailsMenu)
  this.When(/^I edit my account details$/, () => {
    myDetailsPo.updateDetails(
      'Dr',
      'TestFirstName',
      'lastNameTest',
      'emailtest@sample.com'
    )
  })
  this.Then(/^I should see message "(.*)"$/, (expectedMsg) => {
    assert.equal(myDetailsPo.getSaveChangesMessage(), expectedMsg)
  })

  this.When(/^I select My Orders$/, () => {
    orderHistoryDetails.openOrderHistoryDetails()
  })
}
