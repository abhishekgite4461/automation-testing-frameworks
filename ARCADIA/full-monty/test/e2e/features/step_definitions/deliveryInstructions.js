import * as delivery from '../../models/pageObjects/delivery'
import * as modalStoreLocMap from '../../models/newPageObjects/storeLocator/modalStoreLocatorMapPO'

module.exports = function() {
  this.When(
    /^I should see the mobile field for delivery updates$/,
    delivery.mobileUpdatesDeliveryFieldPresent
  )
  this.Then(
    /^I should see the delivery instructions text field$/,
    delivery.deliveryInstructionsTextFieldPresent
  )
  this.When(
    /^I enter a delivery updates mobile number$/,
    delivery.enterDefaultDeliveryUpdateMobile
  )
  this.When(
    /^I enter a standard delivery instructions note$/,
    delivery.enterDefaultDeliveryInfoText
  )
  this.Then(
    /^The number should be shown in the delivery updates mobile number$/,
    delivery.defaultDelUpdateMobilenumberPresent
  )
  this.Then(
    /^The standard delivery instructions note should be shown in the standard delivery instructions note field$/,
    delivery.defaultDelInstructionsNoteIsPresent
  )
  this.Then(/^I should be on the summary page$/, delivery.summaryPagePresent)
  this.When(
    /^I enter delivery instructions longer than 30 characters$/,
    delivery.enterLongDeliveryNote
  )
  this.Then(
    /^I should see i have no characters left to use$/,
    delivery.deliveryNoteValidationPresent
  )
  this.Then(/^I select Collect from Store$/, delivery.collectFromStore)
  this.Then(
    /^I select Standard Collection from Store$/,
    delivery.collectFromStoreStandard
  )
  this.Then(
    /^I select Express Collection from Store$/,
    delivery.collectFromStoreExpress
  )
  this.Then(
    /^I select Collect from ParcelShop$/,
    delivery.collectFromParcelShop
  )
  this.Then(
    /^I set location for map Search to (.*)$/,
    delivery.setLocForStoreSearch
  )
  this.Then(/^I verify the map is displayed$/, delivery.verifyMapIsDisplayed)
  this.Then(
    /^I verify the map is NOT displayed$/,
    delivery.verifyMapIsNotDisplayed
  )
  this.Then(/^I click on the GO button$/, delivery.clickOnGoButton)
  this.Then(
    /^I select the first Store prediction$/,
    modalStoreLocMap.getFirstPredictionLoc
  )
  this.Then(
    /^I select the first Store location$/,
    delivery.pickParcelShopLocation
  )
  this.Then(
    /^I select the first ParcelShop location$/,
    delivery.pickParcelShopLocation
  )
  this.Then(
    /^I select the Store collect Standard$/,
    delivery.selectStoreCollectStandard
  )
  this.Then(
    /^I select the Store collect Express$/,
    delivery.selectStoreCollectExpress
  )
  this.Then(
    /^I see the caption is added to the bill$/,
    delivery.checkParcelShopCaptionAdded
  )
  this.Then(
    /^I see the charge is added to the bill$/,
    delivery.checkParcelShopChargeAdded
  )
  this.Then(
    /^I verify the calculation to the bill$/,
    delivery.checkParcelShopChargeCalc
  )
  this.Then(
    /^I verify the express collection charge (.*) added to the bill$/,
    delivery.checkStoreExpressChargeAdded
  )
}
