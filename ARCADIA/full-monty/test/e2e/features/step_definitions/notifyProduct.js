import * as search from '../../models/pageObjects/search'
import * as productSizes from '../../models/newPageObjects/components/productSize'
import * as NotifyProduct from '../../models/pageObjects/NotifyProduct'

module.exports = function() {
  this.Given(/^I view the "(.*)" product$/, search.searchProduct)
  this.When(
    /^I request to receive an email when item back in stock$/,
    NotifyProduct.clickNotifyEmail
  )
  this.When(
    /^I wait for the notify product modal to appear$/,
    NotifyProduct.waitForNotifyEmailModal
  )
  this.When(/^I select size (.*)$/, productSizes.setSize)
  this.When(/^I enter my first name "(.*)"$/, NotifyProduct.enterFirstname)
  this.When(/^I enter my surname "(.*)"$/, NotifyProduct.enterSurname)
  this.When(/^I enter my email "(.*)"$/, NotifyProduct.enterEmail)
  this.When(
    /^I submit an email back in stock request$/,
    NotifyProduct.submitNotifyEmailForn
  )
  this.When(
    /^My email address is pre-filled with (.*)$/,
    NotifyProduct.seeEmailPreviewed
  )
  this.Then(
    /^I receive notification that my request has been submitted$/,
    NotifyProduct.receivedEmailStockMessage
  )
  this.Then(/^The submit CTA is disabled$/, NotifyProduct.checkButtonDisabled)
  this.Then(
    /^I receive an error message that my email must be valid$/,
    NotifyProduct.seeEmailValidation
  )
}
