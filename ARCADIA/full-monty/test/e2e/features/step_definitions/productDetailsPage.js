import * as productDetailsPage from '../../models/pageObjects/productDetailsPage'
import * as shared from '../../models/lib/shared'
import * as carouselPO from '../../models/newPageObjects/components/carousel'
import * as productSize from '../../models/newPageObjects/components/productSize'
import * as inline from '../../models/newPageObjects/productDetail/inlineConfirm'
import * as miniBag from '../../models/newPageObjects/productDetail/miniBagConfirm'
import * as shoppingbag from '../../models/pageObjects/shoppingBag'
import * as addtobagmodel from '../../models/newPageObjects/productDetail/addToBagModal'
import * as findinstore from '../../models/newPageObjects/productDetail/findInStore'

const assert = require('assert')

let inLineMsgNoItems

module.exports = function() {
  this.Given(/^I click on size "(.*)"$/, productDetailsPage.clickOnSize)
  this.Given(
    /^I should see the only size "(.*)" preselected$/,
    productDetailsPage.onlyVisibleSize
  )
  this.Given(
    /^I am on a product details page with a carousel$/,
    shoppingbag.goToProductPageWithCarousel
  )
  this.Given(
    /^I see the product sizes$/,
    productDetailsPage.hasSizeSelectionBLock
  )
  this.Given(/^I select a quantity$/, productDetailsPage.selectRandomQty)
  this.Given(
    /^I select the maximum quantity available$/,
    productDetailsPage.selectHighestQty
  )
  this.When(/^I click on the add to bag button of the PDP$/, () => {
    productDetailsPage.productTitle()
    productDetailsPage.AddToShoppingBag()
  })
  this.When(/^I select the (.*) arrow on the image$/, carouselPO.clickArrow)
  this.When(/^I double tap on the product image$/, carouselPO.doubleTapZoom)
  this.When(/^I zoom (.*) on product image$/, carouselPO.zoom)

  this.When(
    /^I click on carousel zoom icon$/,
    carouselPO.openCarouselFullScreenIcon
  )
  this.When(/^I click on the product$/, carouselPO.openCarouselFullScreenClick)
  this.When(
    /^I click on X to close the full screen$/,
    carouselPO.closeCarouselFullScreen
  )
  this.When(/^I select a size$/, () => {
    productSize.setSize()
  })
  this.When(/^I select the Size Guide$/, productDetailsPage.openSizeGuide)
  this.Then(
    /^I am directed to the Product details page for "(.*)"$/,
    productDetailsPage.checkProductName
  )
  this.Then(
    /^I navigate back to the Product page$/,
    productDetailsPage.goBacktoProductDisplayPage
  )
  this.Then(
    /^I see the product images displayed in a carousel$/,
    carouselPO.hasProductImages
  )
  this.Then(/^I see the product name$/, () =>
    shared.isVisible('ProductDetail', 'title')
  )
  this.Then(/^I see the product description$/, () =>
    shared.isVisible('ProductDescription')
  )
  this.Then(/^I see the product price$/, () =>
    shared.isVisible('HistoricalPrice', '-pdp')
  )
  this.Then(/^I see the product description contains "(.+)"$/, (text) => {
    const selector = '.ProductDescriptionExtras-list'
    assert.ok(browser.isVisible(selector))
    assert.ok(browser.getText(selector).indexOf(text) !== -1)
  })
  this.Then(
    /^The (.*) image is displayed in the carousel$/,
    carouselPO.hasSelectedImageDisplayed
  )
  this.Then(
    /^The (.*) carousel selector is highlighted$/,
    carouselPO.hasPaginationBulletHighlighted
  )
  this.Then(
    /^I see the zoomed (.*) view of the product image$/,
    productDetailsPage.zoomViewDisplayed
  )
  this.Then(
    /^confirmation message "(.*)" is displayed$/,
    productDetailsPage.confirmationMessageDisplayed
  )
  this.Then(
    /^the add to bag confirmation modal should include the product title$/,
    productDetailsPage.confirmationMessageIncludesProductTitle
  )
  this.Then(
    /^the add to bag confirmation modal should include the size (.*)$/,
    productDetailsPage.confirmationIncludesText
  )
  this.Then(
    /^the add to bag confirmation modal should include the product size added$/,
    productDetailsPage.confirmationIncludesText
  )
  this.Then(
    /^The product is opened in full screen$/,
    carouselPO.hasCarouselModalOpen
  )
  this.Then(
    /^The product comes to before screen$/,
    carouselPO.hasCarouselModalClose
  )
  this.Then(/^I can see the Size Guide$/, productDetailsPage.hasSizeGuide)
  this.Then(/^I see the inline message shown item\(s\) added to bag$/, () => {
    inLineMsgNoItems = inline.getInlineConfirmMessage()
    assert.ok(
      inLineMsgNoItems.indexOf(
        `${process.env.SELECTED_PRODUCT_QTY} item(s) added to bag`
      ) !== -1 ||
        inLineMsgNoItems.indexOf('Your item has been added to your bag') !== -1,
      `[PDP PAGE] - Inline Text did not match quantity added to shopping bag : ${inLineMsgNoItems}`
    )
  })
  this.Then(/^Minibag Pop's up$/, () => {
    // waits for fadeIn effect
    browser.pause(4000)
    assert.ok(miniBag.hasMiniBagOpen() === true, `Mini-bag did not appear`)
  })
  this.Then(/^I should see the product details in mini bag modal$/, () => {
    assert.equal(
      true,
      miniBag.getMiniBagImage() > 0,
      'Mini-Bag Product Image not displayed in Modal'
    )
    assert.equal(
      process.env.SELECTED_PRODUCT_TITLE.toLowerCase(),
      miniBag.getMiniBagTitle().toLowerCase()
    )
    assert.equal(process.env.SELECTED_PRODUCT_QTY, miniBag.getMiniBagQty())
    assert.equal(process.env.SELECTED_PRODUCT_SIZE, miniBag.getMiniBagSize())
    assert.ok(
      process.env.SELECTED_PRODUCT_PRICE.indexOf(miniBag.getMiniBagPrice()) !==
        -1,
      'Mini-Bag Product Price did not match'
    )
  })
  this.Given(/^mini bag disappears after few seconds$/, () => {
    // waits for fadeOut effect
    browser.pause(4000)
    assert.ok(miniBag.hasMiniBagOpen() === false, `Mini-bag still displayed`)
  })
  this.Then(/^I should see the maximum order quantity message$/, () => {
    browser.pause(4000)
    assert.equal(
      addtobagmodel.getModalAddMaxQty(),
      '×\nYou have exceeded the maximum order quantity for this item.'
    )
  })
  this.Then(
    /^I should see the message that the product is out of stock$/,
    () => {
      browser.pause(4000)
      assert.equal(
        addtobagmodel.getModalAddMaxQty(),
        '×\nSorry, this item is no longer available.'
      )
    }
  )

  this.When(/^I click on the find in store button of the PDP$/, () => {
    productDetailsPage.productTitle()
    productDetailsPage.getProductPrice()
    findinstore.clickFindInStore()
  })
  this.Then(/^I verify Find-in-store Pop's up$/, () => {
    // waits for fadeIn effect
    browser.pause(1000)
    findinstore.verifyFindInStoreModal()
  })
  this.Then(
    /^I verify the stock status is displayed$/,
    findinstore.verifyStockNotificationisdisplayed
  )
}
