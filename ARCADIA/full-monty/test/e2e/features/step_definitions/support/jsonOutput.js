const path = require('path')
const Cucumber = require('cucumber')

const JsonFormatter = Cucumber.Listener.JsonFormatter()
const fs = require('fs')

module.exports = function JsonOutputHook() {
  JsonFormatter.log = function(resultString = '') {
    const jsonFileLocation = path.join(
      __dirname,
      `../../../../../${process.env.CUCUMBER_JSON_PATH}`
    )

    if (jsonFileLocation !== 'undefined' && resultString.length > 2) {
      try {
        if (fs.existsSync(jsonFileLocation)) {
          const allResults = JSON.parse(
            fs.readFileSync(jsonFileLocation).toString()
          )
          allResults.push(JSON.parse(resultString)[0])

          fs.writeFileSync(jsonFileLocation, JSON.stringify(allResults))
        } else {
          fs.writeFileSync(jsonFileLocation, resultString)
        }
      } catch (e) {
        console.log('[CUCUMBER JSON] Unable to save file')
        console.log(e.message)
      }
    }
  }

  this.registerListener(JsonFormatter)
}
