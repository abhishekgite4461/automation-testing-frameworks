import * as productBundlesPage from '../../models/pageObjects/productBundlesPage'
import * as client from '../../models/lib/client'

module.exports = function() {
  this.Given(/^I navigate to "(.*)"$/, client.gotoHomepage)

  this.When(/^I click "(.*)"$/, productBundlesPage.clickBundlesLink)

  this.When(/^I add bundle item to bag$/, productBundlesPage.addItemToBag)

  this.When(
    /^I can see product current price in carousel$/,
    productBundlesPage.outfitPriceExists
  )
  this.Then(
    /^I view the confirmation message$/,
    productBundlesPage.addToBagSuccess
  )
  this.Then(
    /^I am redirected to a product details page$/,
    productBundlesPage.redirectedToProductDetails
  )

  this.Then(
    /^I can see the following product section elements$/,
    productBundlesPage.miniProductElementsAreShown
  )

  this.Then(
    /^I navigate to "(.*)" outfit details page$/,
    productBundlesPage.navigateToOutfit
  )
  this.When(
    /^I select the carousel item with price "(.*)"$/,
    productBundlesPage.selectCarouselByPrice
  )
  this.Then(
    /^I can see the total price as "£(.*)"$/,
    productBundlesPage.checkTotalPrice
  )
}
