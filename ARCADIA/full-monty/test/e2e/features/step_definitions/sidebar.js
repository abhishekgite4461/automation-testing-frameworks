import * as client from '../../models/lib/client'
import * as sidebar from '../../models/newPageObjects/navigation/sidebar'

module.exports = function() {
  const allMenuItems = '.Categories-listItem'
  const userDetailMenuItems = '.TopNavMenu-userDetails .Categories-listItem'
  const helpAndIndoMenuItems = '.TopNavMenu-helpAndInfo .Categories-listItem'

  this.When(/^I select "(.*)" link in the Your Details menu options$/, (arg) =>
    client.clickWithText(userDetailMenuItems, arg)
  )
  this.When(
    /^I select "(.*)" link in the Help & Information menu options$/,
    (arg) => client.clickWithText(helpAndIndoMenuItems, arg)
  )
  this.Then(/^I select category "(.*)" from the menu$/, sidebar.selectACategory)

  this.Then(
    /^I open the main menu item "(.*)" and the submenu "(.*)"$/,
    sidebar.selectMenuAndSubMenu
  )
  this.Then(/^I can see "(.*)" link in the menu options$/, (arg) =>
    sidebar.hasMenuItem(arg, allMenuItems, true)
  )
  this.Then(/^I can see "(.*)" link in the Your Details menu options$/, (arg) =>
    sidebar.hasMenuItem(arg, userDetailMenuItems, true)
  )
  this.Then(
    /^I can see "(.*)" link in the Help & Information menu options$/,
    (arg) => sidebar.hasMenuItem(arg, helpAndIndoMenuItems, true)
  )
}
