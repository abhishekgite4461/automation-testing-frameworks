import * as loginPO from '../../models/newPageObjects/login_po'
import * as bazaar from '../../models/newPageObjects/productDetail/bazaarVoiceReview'

const assert = require('assert')

module.exports = function() {
  this.When(/I click write a review$/, bazaar.clickWrite)
  this.Then(/I am redirected to the login$/, () => {
    loginPO.getLoginContainer()
    assert.ok(loginPO.loginPagePresent(), 'Login page was not present')
  })
  this.Then(/I am redirected to the review$/, () => {
    assert.ok(bazaar.reviewFormPresent(), 'Review page was not present')
  })
}
