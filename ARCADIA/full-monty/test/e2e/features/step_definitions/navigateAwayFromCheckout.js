import * as shoppingBag from '../../models/pageObjects/shoppingBag'
import * as manualDeliveryPO from '../../models/newPageObjects/checkout/ManualAddress_po'
import * as delivery from '../../models/pageObjects/delivery'
import * as deliveryPO from '../../models/newPageObjects/checkout/DeliveryBase_po'

module.exports = function() {
  this.Given(
    /^I have entered a delivery address$/,
    shoppingBag.enterDefaultDeliveryInfo
  )
  this.When(
    /^I have filled out the delivery details without continuing$/,
    shoppingBag.enterDefaultDeliveryInfoWithoutContinueing
  )
  this.Given(/^the auto address finder form is present$/, () => {
    if (manualDeliveryPO.findAddresslinkVisible()) {
      manualDeliveryPO.clickFindAddressFormlink()
    }
  })
  this.Then(
    /^the delivery page should not have saved any of my details$/,
    () => {
      deliveryPO.getDeliveryContainer()
      delivery.enterAddressManually()
      delivery.deliveryFieldsAreEmpty()
    }
  )
}
