import * as client from '../../models/lib/client'
import * as forms from '../../models/lib/_forms'
import * as shared from '../../models/lib/shared'

const assert = require('assert')

module.exports = function() {
  this.Given(/^I navigate to the (.*) page$/, client.openPage)
  this.Given(/^I am on the landing page$/, shared.openLandingPage)
  this.Given(
    /^The client url path is equal to (.*)$/,
    client.urlPathnameIsEqualTo
  )
  this.Given(
    /^Select field (.*) selected option is "(.*)" in the "(.*)" form$/,
    forms.selectedOptionIs
  )
  this.Given(
    /^Form field (.*) contains "(.*)" in the "(.*)" form$/,
    forms.formInputTextEquals
  )
  this.Given(/^I go to the (.*) page$/, client.goToPage)
  this.Given(/^I enable product swatches feature flag$/, () =>
    shared.enableFeature('FEATURE_SWATCHES')
  )
  this.Given(/^I enable big header feature flag$/, () =>
    shared.enableFeature('FEATURE_HEADER_BIG')
  )
  this.Given(/^I enable responsive layout feature flag$/, () =>
    shared.enableFeature('FEATURE_RESPONSIVE')
  )
  this.Given(/^I enable PUDO feature flag$/, () =>
    shared.enableFeature('FEATURE_PUDO')
  )
  this.Given(/^I enable order history message feature flag$/, () =>
    shared.enableFeature('FEATURE_ORDER_HISTORY_MSG')
  )

  this.Given(/^I scroll "(.*)" percent down the page$/, (percentage) =>
    shared.scrollPercentage(percentage)
  )
  this.Then(/^(.*) (.*) is visible$/, shared.isVisible)
  this.Then(/^(.*) (.*) is not visible$/, shared.notVisible)
  this.Then(/^The message "(.*)" is displayed$/, shared.messageDisplayed)
  this.Then(/^I am on the "(.*)" home page$/, client.gotoHomepage)

  this.Then(/^I pause for "(.*)"$/, shared.pause)
  this.Then(/^the page title contains the brand name "(.*)"$/, (brand) => {
    const pageTitle = browser.getTitle()
    assert.ok(
      pageTitle.indexOf(brand) !== -1,
      `${pageTitle} did not contain the brand name: ${brand}`
    )
  })
}
