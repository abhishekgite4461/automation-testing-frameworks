import * as sideBar from '../../models/newPageObjects/navigation/sidebar'
import * as topNavBarPO from '../../models/newPageObjects/navigation/topNavigationBar'
import * as shared from '../../models/lib/shared'

const assert = require('assert')

module.exports = function() {
  this.When(/^I click the burger icon$/, () => {
    topNavBarPO.clickBurgerIcon()
  })
  this.When(/^I search for "(.*)"$/, (searchTerm) => {
    topNavBarPO.search(searchTerm)
  })
  this.Then(
    /^The social media icons specified in the CMS will be displayed in the menu$/,
    () => {
      const numberOfSocialIcon = sideBar.getSocialIconsVisible()
      assert.ok(
        numberOfSocialIcon >= 1,
        `Social Icons found ${numberOfSocialIcon}`
      )
    }
  )
  this.Then(/^I can see logo in top navigation bar$/, () => {
    const logo = topNavBarPO.getLogo()
    assert.ok(logo.length === 1, `There should only be one logo displayed`)
  })
  this.Then(/^I can see search in top navigation bar$/, () =>
    shared.iconDisplayed('search', 'Header')
  )
  this.Then(/^I can see minibag in top navigation bar$/, () =>
    shared.iconDisplayed('shoppingCart', 'Header')
  )
}
