/**
 * Google Tag Manager Specific Step Definitions
 */

const assert = require('assert')

const getDataLayer = () => {
  const dataLayerObject = browser.execute('return dataLayer')
  return dataLayerObject.value
}

const waitForEvent = (event) => {
  browser.waitUntil(
    () => {
      const dataLayer = getDataLayer()
      const gtmEvent = dataLayer.filter((o) => o.event === event)
      return gtmEvent.length > 0
    },
    120000,
    `expected ${event} event to be triggered`
  )
}

const waitForUserStateChangeEvent = () => {
  waitForEvent('userState')
}

module.exports = function() {
  this.Then(/^the dataLayer will contain a "(.*)" event$/, (event) => {
    const dataLayer = getDataLayer()
    const dataLayerEvent = dataLayer.filter((o) => o.event === event)
    assert.ok(dataLayerEvent.length > 0, `no dataLayer event of ${event}`)
  })

  this.Then(
    /^the dataLayer will contain an ecommerce action event of step (.*)$/,
    (step) => {
      const dataLayer = getDataLayer()
      const ecommerceEvents = dataLayer.filter((o) => o.ecommerce !== undefined)
      assert.ok(ecommerceEvents.length > 0, 'no ecommerce action events')
      const ecommerceEventAction = ecommerceEvents.filter(
        (o) => o.ecommerce.actionField !== undefined
      )
      assert.ok(
        ecommerceEventAction.length > 0,
        'no ecommerce with action field'
      )
      assert.ok(
        ecommerceEventAction[
          ecommerceEventAction.length - 1
        ].ecommerce.actionField.checkout.step.toString() === step,
        `step ${
          ecommerceEventAction[ecommerceEventAction.length - 1].ecommerce
            .actionField.checkout.step
        } does not equal step ${step}`
      )
    }
  )

  this.Then(/^a Google Tag Manager Data Layer will be initialised$/, () => {
    const dataLayer = getDataLayer()
    assert.ok(dataLayer instanceof Array, 'dataLayer is not an array!')
    const objectsWithGtmJs = dataLayer.filter((o) => o.event === 'gtm.js')
    assert.ok(
      objectsWithGtmJs.length > 0,
      'dataLayer should have at least one gtm.js event'
    )
  })

  this.When(/^Google Tag Manager is initialised$/, () => {
    waitForEvent('gtm.js')
  })

  this.Then(
    /^the dataLayer will contain a detail view change ecommerce event$/,
    () => {
      const dataLayer = getDataLayer()
      const ecommerceEvent = dataLayer.filter((o) => o.ecommerce !== undefined)
      assert.ok(ecommerceEvent.length > 0, 'no ecommerce event')

      const currentEcommerceEvent = ecommerceEvent[ecommerceEvent.length - 1]
      assert.ok(
        currentEcommerceEvent.ecommerce.detail.products.length > 0,
        'No product'
      )
    }
  )

  this.Then(/^the dataLayer will contain a product view event$/, () => {
    const dataLayer = getDataLayer()
    const userChangeEvent = dataLayer.filter((o) => o.event === 'userState')
    assert.ok(userChangeEvent.length > 0, 'no user state change event')
  })

  this.Then(
    /^the dataLayer will contain a user change event of logout$/,
    () => {
      waitForUserStateChangeEvent()
      browser.pause(5000)
      const dataLayer = getDataLayer()
      const userChangeEvent = dataLayer.filter((o) => o.event === 'userState')
      assert.ok(
        userChangeEvent[userChangeEvent.length - 1].user.loggedIn === 'False',
        'user login state is not false'
      )
    }
  )

  this.When(
    /^the dataLayer will contain a page view event with pageType of "(.*)" and pageCategory of "(.*)"$/,
    (pageType, pageCategory) => {
      const dataLayer = getDataLayer()
      const pageEvent = dataLayer.filter((o) => o.event === 'pageView')
      assert.ok(pageEvent.length > 0, 'no page event')
      assert.ok(
        pageEvent[pageEvent.length - 1].pageType === pageType,
        `pageType incorrect expected ${pageType} but got ${
          pageEvent[0].pageType
        }`
      )
      assert.ok(
        pageEvent[pageEvent.length - 1].pageCategory === pageCategory,
        `pageCategory incorrect expected ${pageCategory} but got ${
          pageEvent[0].pageCategory
        }`
      )
    }
  )
}
