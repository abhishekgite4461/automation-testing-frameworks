Feature: Analytics - google tag manager - user tracking
  As a marketer
  I want to gather anlytics information
  So that I can explore how customers are using the site

  Background:
    Given the set up is done

  # Scenario: Logging in will cause user status change event
  #   And I log in with gtm@arcadiatest.com and password password1
  #   When Google Tag Manager is initialised
  #   Then the dataLayer will contain a product view event

  @mobile
  Scenario: Logging in and logging out will cause user state change event of logged out
    And I log in with gtm@arcadiatest.com and password password1
    And I am on the landing page
    And I open the menu
    When I select category "Sign out" from the menu
    And Google Tag Manager is initialised
    Then the dataLayer will contain a user change event of logout
