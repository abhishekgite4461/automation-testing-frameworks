@mobile
Feature: Analytics - google tag manager - basket tracking
As a marketer
I want to gather anlytics information
So that I can explore how customers are using the site

Background:
  Given the set up is done

Scenario: Add to Basket
  And The shopping bag contains a product
  And Google Tag Manager is initialised
  Then the dataLayer will contain a "addToBasket" event

Scenario: Remove from Basket
  And The shopping bag contains a product
  When I remove a product from the shopping bag
  And I confirm deletion
  And Google Tag Manager is initialised
  Then the dataLayer will contain a "removeFromBasket" event
