import fs from 'fs'
import * as brandURL from './models/lib/brandURL'
import * as client from './models/lib/client'

const request = require('superagent')

export const increaseBrowserHeight = () => {
  const offSet = 1000
  const width = browser.getViewportSize('width')
  const height = browser.getViewportSize('height')
  const heightOffset = height + offSet
  const dims = { width, height: heightOffset }
  client.setViewportSize(dims)
}

export const takeScreenshot = (path) => {
  try {
    increaseBrowserHeight()
    browser.saveScreenshot(path)
  } catch (e) {
    console.log(`[takeScreenshot] screenshot failed: ${e.message}`)
  }
}

export const getBrowserLog = (fileName) => {
  // disabling as iOS is unable to retrieve browser log
  const browserLogs = !browser.isMobile ? browser.log('browser').value : ''
  let filteredLogs = ''

  if (browserLogs.length) {
    // filter out couple of common logs
    // eslint-disable-next-line no-restricted-syntax
    for (const log of browserLogs) {
      if (
        log.message.includes('No localisation found') ||
        log.message.includes('Download the React DevTools') ||
        log.message.includes('opentag') ||
        log.message.includes('source":"console-api","stack":{"callFrames"') ||
        log.message.includes('sstats.topshop.com') ||
        log.message.includes('console.clear') ||
        log.message.includes('is not a valid email address') ||
        log.message.includes('Warning: Unknown prop `fallBackImage`') ||
        log.message.includes(
          'was loaded over HTTPS, but requested an insecure image'
        ) ||
        log.message.includes(
          'Experience Engine will now restart on QP view events'
        ) ||
        log.message.includes('vertical is deprecated')
      ) {
        // do not include in log
      } else {
        filteredLogs += `${log.message}\n\n`
      }
    }

    console.log(
      '---------------------[CONSOLE ERROR LOGS - start]--------------------------'
    )
    console.log(filteredLogs)
    console.log(
      '---------------------[CONSOLE ERROR LOGS - finish]--------------------------'
    )

    fs.writeFile(fileName, filteredLogs, (e) => {
      if (e) {
        console.log(
          '[getBrowserLog] Unable to save browser console log into file',
          e.message
        )
      }
    })
  }
}

/**
 * Retrieve URL
 * @param brandCode four letter brand code
 * @param environment the environment (stage, integration or production)
 */
export const getURL = (brandCode, environment) => {
  return brandURL.getURL(brandCode, environment)
}

/**
 * Retrieve app version
 */
export const getAppVersion = (url) => {
  request.get(`${url}/version`).end((error, response) => {
    if (!error && response.ok) {
      console.log(
        '\n\n---------------------[MONTY VERSION - start]--------------------------'
      )
      console.log(response.body)
      console.log(
        '---------------------[MONTY VERSION - finish]--------------------------\n\n'
      )
    }
  })
}
