import * as client from '../lib/client'
import * as shared from '../lib/shared'
import * as overlayPO from '../newPageObjects/components/overlay'

const assert = require('assert')
const l = require('../lib/localisation').init()

//-------------------------------
// Main container identifier
const storeLocator = '.StoreLocator'

// page element identifiers
const GoButton = '.UserLocator-goButton'
const countrySelector = '#CountrySelect'
const storeFinderCountrySelector =
  '.UserLocator-container #StoreFinderCountrySelect'
const storeLocatorCriteria = '.UserLocatorInput-inputField'
const storeLocatorSuggestions = '.UserLocatorInput-predictionsList'
const mapSearchFindMe =
  '.FindInStore button.UserLocatorInput-currentLocationButton'

export const getMainContainer = () => {
  overlayPO.waitForOverlay()
  try {
    browser.waitForVisible(storeLocator)
    return browser.$(storeLocator)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] container did not appear',
      err,
      'FAILED-TO-GET-CONTAINER'
    )
  }
}

export const selectCountry = (country) => {
  browser.waitForVisible(countrySelector)
  return client.selectByValue(countrySelector, country)
}

export const selectRandomCountryByIndex = () => {
  browser.waitForVisible(countrySelector)
  const optionList = browser
    .getText(countrySelector)
    .replace(/(\n)/g, ',')
    .split(',')

  // united kingdom doesn't have a store list on every brand but a geo lookup
  let selectableOptions = optionList.filter(
    (option) => option !== 'United Kingdom'
  )
  selectableOptions = selectableOptions.filter(
    (option) => option !== 'Portugal'
  )
  selectableOptions = selectableOptions.filter(
    (option) => option !== l`Choose country`
  )
  const optionToSelect = Math.floor(Math.random() * selectableOptions.length)
  console.log(
    `selecting the country to use: ${selectableOptions[optionToSelect]}`
  )
  selectCountry(selectableOptions[optionToSelect])
}
export const selectRandomCountryByIndex2 = () => {
  const container = getMainContainer()

  container.waitForVisible(storeFinderCountrySelector)

  // retrieve all options
  const optionList = container
    .getText(storeFinderCountrySelector)
    .replace(/(\n)/g, ',')
    .split(',')
  let selectableOptions = optionList.filter(
    (option) => option !== 'United Kingdom'
  )
  selectableOptions = selectableOptions.filter(
    (option) => option !== 'Portugal'
  )
  selectableOptions = selectableOptions.filter(
    (option) => option !== l`Choose country`
  )

  // randomly select an option
  const randomIndex = Math.floor(Math.random() * selectableOptions.length)
  const selectedCountry = selectableOptions[randomIndex]
  container.selectByVisibleText(storeFinderCountrySelector, selectedCountry)
}

export const setSearch = (value) => {
  client.click(storeLocatorCriteria)
  return client.keys(value)
}

export const hasSuggestion = (suggestion) => {
  browser.waitForExist('.UserLocatorInput-predictionsList')
  browser.waitForVisible('.UserLocatorInput-predictionsListItem')
  const text = browser.getText(storeLocatorSuggestions)
  assert(text.indexOf(suggestion) !== -1)
}

export const listOfStoresContains = (storeName) => {
  client.includesText('.Store-name', storeName)
}

export const listOfStoresForSelectedBrand = () => {
  browser.waitForVisible(
    '.StoreLocator-resultsContainer.StoreLocator-resultsContainer--fullHeight'
  )
  assert.ok(
    browser.isVisible(
      '.StoreLocator-resultsContainer.StoreLocator-resultsContainer--fullHeight'
    ),
    'search results where not visible'
  )
}

export const selectGo = () => {
  client.click(GoButton)
}

export const countryStores = (country) => {
  return client.openPage(`/store-locator?brand=12556&country=${country}`)
}

export const selectStore = (store) => {
  client.clickWithText('.Store-name', store)
}

const storeNameById = (firstStoreId) => {
  const firstStoreHeader = browser.elementIdElement(firstStoreId, '.Store-name')
  const firstStoreHeaderId = firstStoreHeader.value.ELEMENT
  const firstStoreNameObject = browser.elementIdText(firstStoreHeaderId)
  const firstStoreName = firstStoreNameObject.value

  return firstStoreName
}

const getFirstStoreId = () => {
  const stores = browser.elements('.Store')
  const firstStoreId = stores.value[0].ELEMENT

  return firstStoreId
}

export const clickFindInStoreButton = () => {
  browser.waitForVisible('.ProductDetail-findInStoreButton')
  client.click('.ProductDetail-findInStoreButton')
}

export const searchForProductNearAddress = (value) => {
  const predictionsItem = '.FindInStore .UserLocatorInput-predictionsListItem'
  const inputField = '.FindInStore .UserLocatorInput-inputField'
  browser.waitForVisible(inputField)
  browser.setValue(inputField, value)
  browser.waitForVisible(predictionsItem)
  const {
    value: { ELEMENT },
  } = browser.element(predictionsItem)
  browser.elementIdClick(ELEMENT)
  browser.waitForVisible('.FindInStore-form .Button.UserLocator-goButton', 3000)
  client.click('.FindInStore-form .Button.UserLocator-goButton')
}

export const clickLocateMeButton = () => {
  browser.waitForExist(mapSearchFindMe)
  return client.click(mapSearchFindMe)
}

export const storeIsMinimised = (storeName) => {
  const firstStoreId = getFirstStoreId()

  // The store at the top of the list is not the one provided as argument
  const firstStoreName = storeNameById(firstStoreId)
  assert.notEqual(firstStoreName, storeName)

  // The store at the top of the list has the accordion in expanded mode
  const firstStoreAccordion = browser.elementIdElement(
    firstStoreId,
    '.Accordion-title'
  )
  const firstStoreAccordionId = firstStoreAccordion.value.ELEMENT
  const firstStoreAccordionCSSClass = browser.elementIdAttribute(
    firstStoreAccordionId,
    'class'
  )
  assert.ok(firstStoreAccordionCSSClass.value.indexOf('is-expanded') !== -1)
  // There is only one expanded accordion in the list
  const expandedAccordions = browser.elements('.Accordion-title.is-expanded')
  assert.ok(expandedAccordions.value.length === 1)
}

export const storeOnTop = (storeName) => {
  const firstStoreId = getFirstStoreId()

  // The store at the top of the list is the one provided as argument
  const firstStoreName = storeNameById(firstStoreId)
  assert.equal(firstStoreName, storeName)
}

export const storeIsExpanded = (storeName) => {
  const firstStoreId = getFirstStoreId()

  // The store at the top of the list is the one provided as argument
  const firstStoreName = storeNameById(firstStoreId)
  assert.equal(firstStoreName, storeName)

  // The store at the top of the list has the accordion in expanded mode
  const firstStoreAccordion = browser.elementIdElement(
    firstStoreId,
    '.Accordion-title'
  )
  const firstStoreAccordionId = firstStoreAccordion.value.ELEMENT
  const firstStoreAccordionCSSClass = browser.elementIdAttribute(
    firstStoreAccordionId,
    'class'
  )
  assert.ok(firstStoreAccordionCSSClass.value.indexOf('is-expanded') !== -1)

  // There is only one expanded accordion in the list
  const expandedAccordions = browser.elements('.Accordion-title.is-expanded')
  assert.ok(expandedAccordions.value.length === 1)
}

export const storeListShownForProduct = () => {
  overlayPO.waitForOverlay()
  browser.waitForVisible('.Accordion.Accordion--storeLocator')
  const storeTextArray = browser.getText('.Accordion.Accordion--storeLocator')
  storeTextArray.forEach((obj) => {
    const instockResultsFlag = obj.indexOf('In Stock') !== -1
    const notAvailableResultsFlag =
      obj.indexOf('Not available at this store') !== -1
    assert.ok(
      instockResultsFlag || notAvailableResultsFlag,
      `${obj} store info did not show if it was in or out of stock at that location`
    )
  })
}
