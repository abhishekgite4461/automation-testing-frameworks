import * as shared from '../../lib/shared'

// identifiers
//-------------------------------
// Main container identifier
const filtersContainerClass = '.Filters'

// page element identifiers
const filterButtonClass = '.Filters-refineButton'
//-------------------------------

export const getFiltersContainer = () => {
  try {
    browser.waitForVisible(filtersContainerClass)
    return browser.element(filtersContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] Filters container did not appear',
      err,
      'FAILED-TO-GET-filters-CONTAINER'
    )
  }
}

export const getFilterCount = () => {
  const container = getFiltersContainer()
  container.waitForVisible(filterButtonClass)
  const count = container.getText(filterButtonClass).replace(/\D+/g, '')
  return count
}
