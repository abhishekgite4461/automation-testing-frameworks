import * as shared from '../../lib/shared'
import * as client from '../../lib/client'
import * as overlay from '../components/overlay'

const l = require('../../lib/localisation').init()

// identifiers
//-------------------------------
// Main container identifier
const mobileFiltersContainerClass = '.Refinements-content.is-shown'
const desktopFiltersContainerClass = '.PlpContainer-refinementListContainer'
// page element identifiers
const applyButtonClass = '.Refinements-applyButton'

const filtersContainerClass = client.hasDesktopLayout()
  ? desktopFiltersContainerClass
  : mobileFiltersContainerClass
//-------------------------------

export const getFiltersContainer = () => {
  overlay.waitForOverlay()

  try {
    browser.waitForVisible(filtersContainerClass)
    return browser.$(filtersContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] filters container did not appear',
      err,
      'FAILED-TO-GET-filters-CONTAINER'
    )
  }
}

export const selectFilter = (filterTextToClick) => {
  const filter = getFiltersContainer()

  // TODO: temporary workaround desktop filter scrolling while waiting for DES-310 fix
  if (browser.isVisible(desktopFiltersContainerClass)) {
    // scroll down couple of times just so that rating filter is visible on screen
    browser.scroll('.BackToTop-waypoint', 0, 0)
    overlay.waitUntilProductsAreLoaded()
    console.log('[DEBUG] scrolled down and loaded more products')
    browser.scroll('.BackToTop-waypoint', 0, 0)
    overlay.waitUntilProductsAreLoaded()
    browser.scroll('.BackToTop-waypoint', 0, 0)
    overlay.waitUntilProductsAreLoaded()
    console.log(
      "[DEBUG] and down again to load another batch of products just so we can actually click on 'Rating filter'"
    )

    // click on filter to expand filter options
    filter.click(`span=${filterTextToClick}`)
    // wait for filter css expand animation
    browser.pause(300)
    // scroll down again to reveal filter options
    browser.scroll('.BackToTop-waypoint', 0, 0)
    overlay.waitUntilProductsAreLoaded()
    browser.scroll('.BackToTop-waypoint', 0, 0)

    // mobile
  } else {
    filter.waitForVisible(`span=${filterTextToClick}`)
    filter.scroll(`span=${filterTextToClick}`, 0, 0)
    filter.click(`span=${filterTextToClick}`)
  }
}

export const selectRatingByAlt = (displayRating) => {
  const filters = getFiltersContainer()

  // shared.captureScreenshotAndError('','', 'selectRatingByAlt-' + displayRating)

  filters.getText(`.RatingImage[alt="${displayRating} out of 5 stars"]`)
  filters.waitForVisible(
    `.RatingImage[alt="${displayRating} out of 5 stars"]`,
    10000
  )
  // filters.click('.RatingImage[alt="' + displayRating + ' out of 5 stars"]')
  console.log(l`${displayRating} out of 5 stars`)
  filters.click(`.RatingImage[alt="${l`${displayRating} out of 5 stars`}"]`)
}

export const applyFilters = () => {
  const filters = getFiltersContainer()

  if (!filters.isVisible(desktopFiltersContainerClass)) {
    filters.waitForVisible(applyButtonClass)
    filters.click(applyButtonClass)
    overlay.waitForOverlay()
  }
}

export const selectAvailableSizes = (sizes = '14,M,m') => {
  const filters = getFiltersContainer()
  let sizeToSelect = ''
  // moving this to look at the size options, as the original way of looking up the span was getting items that were unclickable outside the filter
  filters.waitForVisible('.SizeOption-item')
  const optionsArray = filters.getText('.SizeOption-item')
  const sizesArray = sizes.split(',')

  let outerCount = 0
  while (outerCount < sizesArray.length) {
    let innerCount = 0
    const optionToCheck = sizesArray[outerCount]
    while (innerCount < optionsArray.length) {
      const pageOption = optionsArray[innerCount]
      if (pageOption.indexOf(optionToCheck) !== -1) {
        sizeToSelect = optionToCheck
        innerCount = optionsArray.length
        outerCount = sizesArray.length
        break
      }
      innerCount++
    }
    outerCount++
  }
  filters.click(`span=${sizeToSelect}`)
}

export const setMinimumPriceSlider = () => {
  const filters = getFiltersContainer()
  filters.waitForVisible('.Slider-handle.Slider-handle--minHandle')
  filters.click('.Slider-handle.Slider-handle--minHandle')
}

export const setMaxPriceSlider = () => {
  const filters = getFiltersContainer()
  filters.waitForVisible('.Slider-handle.Slider-handle--maxHandle')
  filters.click('.Slider-handle.Slider-handle--maxHandle')
}
