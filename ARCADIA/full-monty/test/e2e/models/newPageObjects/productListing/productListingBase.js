import * as shared from '../../lib/shared'
import * as overlayPO from '../components/overlay'
import * as client from '../../lib/client'

// identifiers
//-------------------------------
// Main container identifier
const productListingContainer = '.PlpContainer'
// page element identifiers

//-------------------------------

export const getProductListingContainer = () => {
  overlayPO.waitForOverlay()
  try {
    browser.waitForVisible(productListingContainer, 10000)
    overlayPO.waitForOverlay()

    return browser.element(productListingContainer)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] Product listing container did not appear',
      err,
      'FAILED-TO-GET-PRODUCT-LISTING-CONTAINER'
    )
  }
}

export const navigateBackToProductListing = () => {
  client.browserBack()
  // wait until product listing container is displayed
  getProductListingContainer()
}
