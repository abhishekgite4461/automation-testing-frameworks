import * as shared from '../../lib/shared'
import * as productListingBase from './productListingBase'

// identifiers
//-------------------------------
// Main container identifier

// page element identifiers
const productLink = '.Product-link'
const productInfo = '.Product-info'
//-------------------------------

export const getProducts = () => {
  const productListing = productListingBase.getProductListingContainer()

  try {
    browser.waitForVisible(productLink, 60000)
    browser.waitForVisible(productInfo, 60000)

    const links = productListing.getAttribute(productLink, 'href')
    return links
  } catch (err) {
    console.log(
      `[IMPORTANT ERROR INFO] the product that through this search issues was: ${
        process.env.SEARCH_TERM
      }`
    )
    shared.failWithScreenshotAndMessage(
      'Unable to extract product links from PLP',
      err,
      'FAILED-TO-EXTRACT-PRODUCST-FROM-PLP'
    )
  }
}
