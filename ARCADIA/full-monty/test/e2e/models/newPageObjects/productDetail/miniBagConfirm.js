// identifiers
//-------------------------------
// Main container identifier
const miniBagConfirmContainerClass = '.MiniBagConfirm'
// page element identifiers
const miniBagProductImage = '.OrderProducts-mediaLeft'
const miniBagProductTitle = '.OrderProducts-productName'
const miniBagProductQtySize = '.OrderProducts-label'
const miniBagProductPrice = '.MiniBagConfirm-content .HistoricalPrice .Price'
//-------------------------------
export const hasMiniBagOpen = () => {
  return browser.isVisible(miniBagConfirmContainerClass)
}

export const getMiniBagImage = () => {
  return browser.element(miniBagProductImage).getElementSize('width')
}

export const getMiniBagTitle = () => {
  return browser.element(miniBagProductTitle).getText()
}

export const getMiniBagQty = () => {
  return browser
    .element(miniBagProductQtySize)
    .getText()
    .replace(/x.*$/, '')
    .trim()
}

export const getMiniBagSize = () => {
  return browser
    .element(miniBagProductQtySize)
    .getText()
    .replace(/.*?size/, '')
    .trim()
}

export const getMiniBagPrice = () => {
  return browser.elements(miniBagProductPrice).getText()
}
