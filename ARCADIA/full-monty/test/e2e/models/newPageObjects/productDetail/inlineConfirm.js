/* eslint no-else-return: 0 */
import * as shared from '../../lib/shared'
import * as client from '../../lib/client'
import * as overlayPO from '../components/overlay'

// identifiers
//-------------------------------
// Main container identifier
const inlineConfirmContainerClass = '.InlineConfirm'
// page element identifiers
const inLineMessageDesktop = '.InlineConfirm-label'
const inLineMessageMobile = '.AddToBag-modal p'
const viewBagButtonClass = '.AddToBag-viewBag'
const checkoutNowButtonClass = '.AddToBag-goToCheckout'
//-------------------------------
export const getInlineConfirmContainer = () => {
  overlayPO.waitForOverlay()
  try {
    browser.waitForVisible(inlineConfirmContainerClass, 10000)
    return browser.element(inlineConfirmContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] Inline confirm container did not appear',
      err,
      'FAILED-TO-GET-INLINE-CONFIRM-CONTAINER'
    )
  }
}

export const getInlineConfirmMessage = () => {
  if (client.hasDesktopLayout()) {
    const inlineConfirmContainer = getInlineConfirmContainer()
    return inlineConfirmContainer.getText(inLineMessageDesktop)
  } else {
    return browser.getText(inLineMessageMobile)
  }
}

export const clickViewBagButton = () => {
  const inlineConfirmContainer = getInlineConfirmContainer()
  inlineConfirmContainer.click(viewBagButtonClass)
}

export const clickCheckoutNowButton = () => {
  const inlineConfirmContainer = getInlineConfirmContainer()
  inlineConfirmContainer.click(checkoutNowButtonClass)
}
