import * as shared from '../../lib/shared'
import * as overlayPO from '../components/overlay'

// identifiers
//-------------------------------
// Main container identifier
const productDetailContainer = '.PdpContainer'
// page element identifiers
const addToBagButtonClass = '.AddToBag'
//-------------------------------

export const getProductDetailContainer = () => {
  overlayPO.waitForOverlay()
  try {
    browser.waitForVisible(productDetailContainer, 10000)
    overlayPO.waitForOverlay()

    return browser.element(productDetailContainer)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] Product detail container did not appear',
      err,
      'FAILED-TO-GET-PRODUCT-DETAILS-CONTAINER'
    )
  }
}

export const clickAddToBagButton = () => {
  const container = getProductDetailContainer()
  container.waitForVisible(addToBagButtonClass)
  container.click(addToBagButtonClass)
}
