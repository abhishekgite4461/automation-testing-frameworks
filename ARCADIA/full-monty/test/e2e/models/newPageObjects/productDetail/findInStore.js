const assert = require('assert')
const l = require('../../lib/localisation').init()

// identifiers
//-------------------------------
// Main container identifier

// page element identifiers
const findInStoreButton = '.ProductDetail-findInStoreButtonDesktop'
const findInStoreModal = '.Modal--storeLocator .FindInStore'
const findInStoreTitle = '.FindInStore-title'
const StoreStockInformation = '.Store-stockInformation'
//-------------------------------

export const clickFindInStore = () => {
  browser.waitForVisible(findInStoreButton)
  browser.click(findInStoreButton)
}

export const verifyFindInStoreModal = () => {
  browser.waitForVisible(findInStoreModal)
  const act = browser.getText(findInStoreTitle).toLowerCase()
  const exp = l`Find in store`.toLowerCase()
  assert.ok(act.includes(exp), 'Error in Find in store modal title')
}

export const verifyStockNotificationisdisplayed = () => {
  browser.waitForVisible(findInStoreModal)
  const act = browser.getText(StoreStockInformation)
  const exp = [l`In Stock`, l`Not available at this store`]

  assert.ok(
    exp.includes(act[0]),
    'Error in Find in Stock Notification being displayed'
  )
}
