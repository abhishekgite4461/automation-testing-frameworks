import * as overlay from '../components/overlay'
import * as shared from '../../lib/shared'

// identifiers
//-------------------------------
// Main container identifier
const baazarContainerClass = '.ProductDetail-bvRow .BazaarVoice'

// page element identifiers
const noReviewWriteLinkId = '#BVRRDisplayContentNoReviewsID a'
const reviewWriteLinkId = '#BVRRBottomLinkWriteID a'
const reviewFormClass = '.BVRRForm'

//-------------------------------

export const getBaazarContainer = () => {
  overlay.waitForOverlay()

  try {
    // nb: wait for bit longer, BaazarVoice is really slow to load up
    browser.waitForVisible(baazarContainerClass)
    // wait until either one of the baazar container is visible
    if (browser.isExisting(noReviewWriteLinkId)) {
      browser.waitForVisible(noReviewWriteLinkId)
    } else if (browser.isExisting(reviewWriteLinkId)) {
      browser.waitForVisible(reviewWriteLinkId)
    }
    return browser.$(baazarContainerClass)
  } catch (e) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] Baazar container did not appear in PDP',
      e,
      'GET-BAAZAR-CONTAINER-FAILED'
    )
  }
}

export const clickWrite = () => {
  const container = getBaazarContainer()

  switch (true) {
    // if there are reviews
    case container.isVisible(reviewWriteLinkId):
      container.click(reviewWriteLinkId)
      break
    // if product does not have any review
    case container.isVisible(noReviewWriteLinkId):
      container.click(noReviewWriteLinkId)
      break
    default:
      console.log(
        '[clickWrite] Do nothing, Baazar Voice write review button is not visible'
      )
      break
  }
}

export const reviewFormPresent = () => {
  overlay.waitForOverlay()
  browser.waitForExist(reviewFormClass)
  return browser.isVisible(reviewFormClass)
}
