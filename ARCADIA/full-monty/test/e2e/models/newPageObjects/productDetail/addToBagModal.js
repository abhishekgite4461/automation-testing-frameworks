import * as shared from '../../lib/shared'

// identifiers
//-------------------------------
// Main container identifier
const modalClass = '.Modal.is-shown'
// page element identifiers
const viewBagButtonClass = '.AddToBag-viewBag'
const checkoutNowButtonClass = '.AddToBag-goToCheckout'
const continueShoppingButtonClass = '.AddToBag-goBack'
const modalCloseIconClass = '.Modal-closeIcon'
//-------------------------------

export const getModal = () => {
  try {
    browser.waitForVisible(modalClass, 10000)
    return browser.element(modalClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] Add to bag modal did not appear',
      err,
      'FAILED-TO-GET-ADDTOBAG-MODAL'
    )
  }
}

export const clickViewBagButton = () => {
  const modal = getModal()
  modal.click(viewBagButtonClass)
}

export const clickCheckoutNowButton = () => {
  const modal = getModal()
  modal.click(checkoutNowButtonClass)
}

export const clickContinueShoppingButton = () => {
  const modal = getModal()
  modal.click(continueShoppingButtonClass)
}

export const clickCloseModal = () => {
  const modal = getModal()
  modal.click(modalCloseIconClass)
}

export const getModalAddMaxQty = () => {
  return browser.getText(modalClass)
}
