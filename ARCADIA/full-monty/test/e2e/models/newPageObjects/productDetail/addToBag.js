import * as shared from '../../lib/shared'
import * as client from '../../lib/client'
import * as overlayPO from '../components/overlay'
import * as productListingPO from '../productListing/productList'
import * as shoppingBagPO from '../shoppingBag/ShoppingBagBase'
import * as topbarPO from '../../newPageObjects/components/topbar_po'
import * as search from '../../pageObjects/search'
import * as productSize from '../../newPageObjects/components/productSize'
import * as productDetailBase from './productDetailBase'

const assert = require('assert')

export const getEnvironmentFriendlyProductList = (
  productList = process.env.GOOD_PRODUCTS,
  highValue = null,
  lowValue = null
) => {
  // from a comma separated list provided (or using the default good products list)
  // this method, splits them in to an array, chooses one at random and returns the single term for searching
  // this method allows us to check search terms for bad or broken search terms without breaking the test

  // set the exit flag as false
  let exitFlag = false
  // split the provided products on , and save to array
  const splitProductArray = productList.split(',')
  // using the array length choose a random number
  let searchTermIndex = Math.floor(Math.random() * splitProductArray.length)
  // save the search term at the random number generated`s index position
  let searchTerm = splitProductArray[searchTermIndex]

  while (exitFlag === false) {
    // search for the search term selected and save the result
    // we will continue to do this until either running out of search terms or a search is successful
    console.log(`[ADD TO BAG] Search term: ${searchTerm}`)

    if (searchTerm === undefined) {
      // check to make sure we have not ran out of search terms
      assert.ok(
        false,
        'appears we ran out of search terms, failing gracefully so not to fall in to unknwon error state'
      )
    }
    exitFlag = search.searchProduct(searchTerm, highValue, lowValue)
    if (exitFlag === false) {
      // if the search failed take a screen shot for inspection later
      const screenShotTitle = `BAD-PRODUCT-${searchTerm}-`
      shared.captureScreenshotAndError(
        'BAD PRODUCT SELECTED',
        searchTerm,
        screenShotTitle
      )
      // remove the failed search term from the array of search terms
      splitProductArray.splice(searchTermIndex, 1)
      // choose a new search term from the array
      searchTermIndex = Math.floor(Math.random() * splitProductArray.length)
      // search for the selected search term and repeat until successful or there are no other search terms in the array
      searchTerm = splitProductArray[searchTermIndex]
    }
  }
  // when a search term has been found that returns a list of products
  // return the search term for use by the rest of the test
  return searchTerm
}

export const getProductLinksFromPLP = () => {
  const products = productListingPO.getProducts()
  // shuffles product list
  return shared.shuffle(products)
}

/**
 * Add product to bag
 *
 * TODO
 * 1. prevent items with low stocks from being added to bag
 * 2. ability to skip over products that did not meet minimum quantities
 */
export const addProductToBag = (minSizes = 1) => {
  // NB: required until we refactor add to bag.. wait until PDP page is fully loaded
  productDetailBase.getProductDetailContainer()

  const availableSizes = productSize.getSizes().length

  // make sure we have minimum number of sizes if passed in
  if (minSizes && availableSizes < minSizes) {
    const failMessage = `Product has ${availableSizes} available sizes. Unfortunately, that did not meet the required minimum ${minSizes} available sizes`
    shared.failWithScreenshotAndMessage(
      failMessage,
      'see above',
      'FAILED-ADD-TO-BAG--NO-AVAILABLE-SIZES-'
    )
    process.env.PRODUCT_ADDED = false
  } else {
    // randomly pick a size from a list of available sizes
    if (browser.isExisting('.ProductSizes.ProductSizes--pdp') === true) {
      const selectedSize = productSize.getARandomSize()
      productSize.setSize(selectedSize)
    }

    // attempt to add product to bag
    overlayPO.waitForOverlay()
    if (browser.isVisible('.AddToBag') !== true) {
      process.env.PRODUCT_ADDED = false
    } else {
      client.click('.AddToBag')
      overlayPO.waitForOverlay()

      client.waitForAddToBasket()

      // navigate back to PDP
      if (browser.isVisible('.AddToBag-goBack')) {
        process.env.ADD_TO_BASKET_CONFIRM_TEXT = browser.getText(
          '.AddToBag-modal p'
        )
        client.click('.AddToBag-goBack')
        console.log(
          `[ADD TO BAG] got the text from confirm modal: ${
            process.env.ADD_TO_BASKET_CONFIRM_TEXT
          }`
        )
      }

      // reload page to get rid of pesky minibag confirm
      if (browser.isVisible('.MiniBagConfirm')) {
        browser.refresh()
      }

      topbarPO.openShoppingBag()

      if (shoppingBagPO.checkoutButtonPresent()) {
        process.env.PRODUCT_ADDED = true
      } else {
        process.env.PRODUCT_ADDED = false
      }
    }
  }
}

export const searchAndAddProductToBag = (
  keywords = process.env.GOOD_PRODUCTS,
  minSizes = 1,
  isURL = false,
  priceRangeHigh = null,
  priceRangeLow = null
) => {
  // reset state
  process.env.PRODUCT_ADDED = 'false'
  browser.url(process.env.BASE_URL)
  // search for good products
  if (isURL) {
    search.searchProductByUrl(keywords)
  } else {
    process.env.SEARCH_TERM = getEnvironmentFriendlyProductList(
      keywords,
      priceRangeHigh,
      priceRangeLow
    )
  }

  // shuffles product list
  const shuffled = getProductLinksFromPLP()

  const searchResultLink = browser.getUrl()

  // attempts to add product to bag
  while (process.env.PRODUCT_ADDED === 'false' && shuffled.length > 0) {
    console.log(`[ADD TO BAG] products left to try = ${shuffled.length}`)
    const selectedProductLink = shuffled.pop()

    try {
      // navigate to PDP page
      const productUrl = selectedProductLink.replace(process.env.BASE_URL, '')
      browser.waitForVisible(`a[href='${productUrl}']`, 10000)
      browser.click(`a[href='${productUrl}']`)
      overlayPO.waitForOverlay()

      // add to bag
      addProductToBag(minSizes)
    } catch (err) {
      const errorMessage = `[ERROR AVERSION] failed to add product: ${selectedProductLink}. Navigating back to ${searchResultLink} to try again`
      shared.captureScreenshotAndError(errorMessage, err, 'FAILED-ADD-TO-BAG-')
      browser.url(searchResultLink)
    }
  }

  if (process.env.PRODUCT_ADDED === 'false') {
    shared.failWithScreenshotAndMessage(
      'Unable to add any of the products. Exiting. This should not happen',
      'no page error',
      'FAILED-TO-ADD-PRODUCTS'
    )
  }
}

export const addRandomProduct = () => {
  searchAndAddProductToBag(process.env.GOOD_PRODUCTS)
}

export const add8OrLessSizeRandomProduct = () => {
  searchAndAddProductToBag(process.env.GOOD_PRODUCTS)
}

export const addRandomProductWithHighStockValue = () => {
  const random = getEnvironmentFriendlyProductList()
  let successFlag = false
  let quantityOptions
  while (successFlag !== true) {
    searchAndAddProductToBag(random)
    topbarPO.openShoppingBag()
    shoppingBagPO.clickEditProduct()
    quantityOptions = browser.getText(
      '.Select.OrderProducts-quantities.is-selected'
    )
    if (quantityOptions.indexOf('2') === -1) {
      browser.click('.Button.OrderProducts-cancelButton')
      shoppingBagPO.completleyEmptyBag()
    } else {
      successFlag = true
      browser.click('.Button.OrderProducts-cancelButton')
    }
  }
}
