import * as client from '../../lib/client'
import * as overlayPO from '../components/overlay'
import * as shared from '../../lib/shared'
import * as keycodes from '../../lib/keycodes'
import * as topBarPO from '../components/topbar_po'

const assert = require('assert')

// identifiers
//-------------------------------
// Main container identifier
const shoppingBagContainerClass = '.MiniBag'
const bagContentContainerClass = '.MiniBag-content'
const shoppingBagModalClass = '.Modal.is-shown'

// page element identifiers
const closeModalClass = '.Modal-closeIcon'
const productNameClass = '.OrderProducts-productName'
const editProductClass = '.OrderProducts-editText'
const saveProductButtonClass = '.OrderProducts-saveButton'
const productRemoveClass = '.OrderProducts-deleteText'
const productRemoveConfirmationClass = '.OrderProducts-deleteButton'
const checkoutButtonClass = '.MiniBag-continueButton'
const productSizeAndQuantityClass = '.OrderProducts-productSize'
const productSizeOptionClass = '#bagItemSize'
const productQuantityOptionClass = '#bagItemQuantity'
const promotionCodeAccordianClass = '.PromotionCode .Accordion-icon'
const promotionCodeInputClass = '.Input-field.Input-field-promotionCode'
const promotionSubmitButtonClass = '.Button.PromotionCode-submit'
const promotionLoadingIconClass = '.Loader-image--button'
const totalPriceClass = '.MiniBag-rightCol.MiniBag-totalCost .Price'
const promoErrorClass = '.Message.is-shown.is-error'
const promoValidationError = '.Input-validationMessage'
const promoValidationMessageClass =
  '.Input-promotionCode .Input-validationMessage'
const deliveryOptionSelectId = '#miniBagDeliveryType'
const emptyBagClass = '.MiniBag-emptyBag'

//-------------------------------

export const getBagContainer = () => {
  overlayPO.waitForOverlay()
  try {
    // nb: this assumes there are products in shopping bag and waits until one displays
    browser.waitForVisible(bagContentContainerClass)
    overlayPO.waitForOverlay()
    return browser.element(shoppingBagContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] There are no products in the shopping bag',
      err,
      'FAILED-NO-PRODUCTS-IN-BAG'
    )
  }
}

export const shoppingBagContainerPresent = () => {
  return browser.isVisible(bagContentContainerClass)
}

export const getBagModal = () => {
  overlayPO.waitForOverlay()
  try {
    // checks for shopping bag content instead of the entire shopping bag container
    browser.waitForVisible(shoppingBagModalClass, 10000)
    return browser.element(shoppingBagModalClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] Shopping bag modal did not appear',
      err,
      'FAILED-TO-GET-MODAL-FOR-ADD-TO-BAG'
    )
  }
}

export const clickCheckout = () => {
  const bag = getBagContainer()
  bag.click(checkoutButtonClass)
}

export const checkoutButtonPresent = () => {
  const bag = getBagContainer()
  return bag.isVisible(checkoutButtonClass)
}

export const clickProductRemoveIcon = () => {
  const bag = getBagContainer()
  bag.waitForVisible(productRemoveClass)
  bag.click(productRemoveClass)
  overlayPO.waitForOverlay()
}

// confirm product removal, waits and closes the success notification
export const clickProductRemovalConfirmation = () => {
  // confirm product removal
  const removeProductModal = getBagModal()
  removeProductModal.click(productRemoveConfirmationClass)
  overlayPO.waitForOverlay()

  // closes the success notification
  const removeProductSuccessModal = getBagModal()
  // TODO: verifies message
  removeProductSuccessModal.click('.Button')
  overlayPO.waitForOverlay()
}

// remove without confirming removal
export const removeProduct = () => {
  clickProductRemoveIcon()
}

export const emptyBag = () => {
  while (shoppingBagContainerPresent() === true) {
    overlayPO.waitForOverlay()
    clickProductRemoveIcon()
    clickProductRemovalConfirmation()
  }
}

export const completleyEmptyBag = () => {
  overlayPO.waitForOverlay()
  clickProductRemoveIcon()
  clickProductRemovalConfirmation()
}

export const getSize = () => {
  const bag = getBagContainer()

  bag.waitForVisible(productSizeOptionClass)
  const sizes = bag.getText(`${productSizeOptionClass}>option`)

  // make sure value return is always an array
  return Array.isArray(sizes) ? sizes : [sizes]
}

export const getQuantity = () => {
  const bag = getBagContainer()

  bag.waitForVisible(productQuantityOptionClass)
  const quantities = bag.getText(`${productQuantityOptionClass}>option`)

  // make sure value return is always an array
  return Array.isArray(quantities) ? quantities : [quantities]
}

export const selectSize = (index) => {
  const bag = getBagContainer()

  bag.waitForVisible(productSizeOptionClass)
  bag.selectByIndex(productSizeOptionClass, index)
}

export const selectQuantity = (index) => {
  const bag = getBagContainer()

  bag.waitForVisible(productQuantityOptionClass)
  bag.selectByIndex(productQuantityOptionClass, index)
}

export const clickCloseModal = () => {
  const shoppingBagModal = getBagModal()
  shoppingBagModal.click(closeModalClass)
}

export const clickEditProduct = () => {
  const bag = getBagContainer()

  // save quantity and size before edit
  const raw = bag.getText(productSizeAndQuantityClass)

  process.env.BEFORE_EDIT_QUANTITY = raw.slice(0, raw.indexOf(' x'))
  process.env.BEFORE_EDIT_SIZE = raw.slice(raw.indexOf('size') + 'size '.length)

  bag.click(editProductClass)
  overlayPO.waitForOverlay()
  browser.waitForVisible(productSizeOptionClass)
}

export const clickSaveProduct = () => {
  const bag = getBagContainer()
  bag.click(saveProductButtonClass)

  overlayPO.waitForOverlay()
  browser.waitForVisible(editProductClass)

  // save quantity and size after edit
  const raw = browser.getText(productSizeAndQuantityClass)
  process.env.AFTER_EDIT_QUANTITY = raw.slice(0, raw.indexOf(' x'))
  process.env.AFTER_EDIT_SIZE = raw.slice(raw.indexOf('size') + 'size '.length)
}

export const openPromoCodeAccordion = () => {
  const bag = getBagContainer()
  bag.click(promotionCodeAccordianClass)
}

export const enterPromoCode = (value) => {
  const bag = getBagContainer()
  bag.waitForVisible(promotionCodeInputClass)
  bag.setValue(promotionCodeInputClass, value + keycodes.TAB)
  bag.waitForVisible(promotionSubmitButtonClass)
}

export const submitPromoCode = () => {
  const bag = getBagContainer()
  bag.waitForVisible(promotionSubmitButtonClass)
  bag.click(promotionSubmitButtonClass)
  overlayPO.waitForOverlay()
  bag.waitForVisible(promotionLoadingIconClass, 60000, true)
}

export const getTotalPrice = () => {
  const bag = getBagContainer()
  bag.waitForVisible(totalPriceClass)
  return bag.getText(totalPriceClass).replace('£', '')
}

export const promoValidationMessagePresent = () => {
  const bag = getBagContainer()
  return bag.isVisible(promoValidationError)
}

export const promoErrorMessagePresent = () => {
  const bag = getBagContainer()
  return bag.isVisible(promoErrorClass)
}

export const getPromoValidationText = () => {
  const bag = getBagContainer()
  return bag.getText(promoValidationMessageClass)
}

export const getPromotionCodeList = () => {
  const bag = getBagContainer()
  return bag.getText()
}

export const hasEdit = () => {
  const bag = getBagContainer()
  return bag.isVisible(editProductClass)
}

export const hasRemove = () => {
  const bag = getBagContainer()
  return bag.isVisible(productRemoveClass)
}

export const isNotEmpty = () => {
  topBarPO.openShoppingBag()
  client.waitForBasketLoaded()
  const bag = getBagContainer()
  return (
    bag.isVisible(emptyBagClass) === false || bag.isVisible(productNameClass)
  )
}

/**
 * nb: this function accepts partial delivery option text, make sure you pass in the full text if
 * there are multiple options with similar text
 */
export const selectDeliveryOption = (deliveryOption) => {
  const bag = getBagContainer()
  const deliveryOptions = bag.getText(deliveryOptionSelectId).split('\n')

  // determine index for dropdown option that contains partial or full text
  let count = 0
  let selectedIndex = -1
  // eslint-disable-next-line no-restricted-syntax
  for (const option of deliveryOptions) {
    if (option.includes(deliveryOption)) {
      selectedIndex = count
    }
    count++
  }

  if (selectedIndex > -1) {
    bag.selectByIndex(selectedIndex)
    overlayPO.waitForOverlay()
  } else {
    assert.ok(
      false,
      `'${deliveryOption}' did not match any of the available delivery options`
    )
  }
}
