import * as aliPayChinaUnionPaySofort from './worldpay_alipay_cupay_sofort'
import * as ideal from './worldpay_ideal'
import * as paypal from './paypal_po'

export const payWith = (cardName) => {
  switch (cardName) {
    case 'Alipay':
    case 'China UnionPay':
    case 'Sofort':
      aliPayChinaUnionPaySofort.confirmOrder()
      break
    case 'PayPal':
      paypal.confirmOrder()
      break
    case 'iDEAL':
      ideal.confirmOrder()
      break
    default:
      console.log('TODO: implement default confirm order()')
      // do nothing
      break
  }
}
