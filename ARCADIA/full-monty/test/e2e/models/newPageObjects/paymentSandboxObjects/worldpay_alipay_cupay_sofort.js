import * as shared from '../../lib/shared'
import * as overlayPO from '../components/overlay'

// identifiers
//-------------------------------
// main container identifier
const worldPayContainerTag = '.containercell'
// page element identifiers
const worldPayHeaderClass = '.header'
const paymentOutcomeTag = 'select[name="status"]'
const continueLinkTag = 'a[alt="Continue"]'
//-------------------------------
// TODO: WorldPay sandbox uses desktop layout even on mobile breakpoint, this will likely fail on actual production test.
export const getWorldpayContainer = () => {
  overlayPO.waitForOverlay()
  browser.waitForVisible(worldPayContainerTag)
  browser.waitForVisible(worldPayHeaderClass)
  try {
    return browser.$(worldPayContainerTag)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] WorldPay AliPay / China Union Pay / Sofort  sandbox did not appear',
      err,
      'FAILED-TO-GET-WORLDPAY-ALIPAYCUPAYSOFORT-CONTAINER'
    )
  }
}

export const setPaymentOutcome = (status) => {
  const container = getWorldpayContainer()
  container.$(paymentOutcomeTag).selectByVisibleText(status)
}

export const clickContinue = () => {
  const container = getWorldpayContainer()
  container.$(continueLinkTag).click()
}

export const confirmOrder = () => {
  setPaymentOutcome('Authorised')
  clickContinue()
}
