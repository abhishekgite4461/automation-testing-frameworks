import * as shared from '../../lib/shared'
import { getCredential } from '../../lib/credentials'

// identifiers
//-------------------------------
// header panel
const loaderContainerClass = '.loading'
const closeCookieImgId = '#_evh-ric-c'
// wallet panel
const walletContainerId = '#all-wallets-frame'
const walletMasterPassButtonClass = '.logo-button'
// frame
const walletFrame = 'MasterPass_wallet_frame'
const frameContainerClass = '.frame-stage'
// login
const emailInputId = '#email'
const passwordInputId = '#password'
const signIgnButtonId = '#signInButton'
// checkout dashboard
const finishShoppingButtonTag = 'button=Finish shopping'
// success
const successContainer = '.SummaryCompleted'

//-------------------------------
const switchToParentFrame = () => {
  browser.frameParent()
  console.log('[switchToParentFrame] set focus back to parent frame')
}

const switchToWalletFrame = () => {
  browser.pause(5000)
  try {
    browser.waitForVisible(`#${walletFrame}`)
    browser.frame(walletFrame)
    console.log('[switchToWalletFrame] set focus to frame')
  } catch (e) {
    console.log(
      "[ERROR AVERSION] Attempt to switch to frame failed, as we're probably already in the frame, failing silently for now..",
      e
    )
  }
}

const waitForLoader = () => {
  browser.waitForVisible(loaderContainerClass, 160000, true)
}

const closeCookieMessage = () => {
  if (browser.isVisible(closeCookieImgId)) {
    browser.click(closeCookieImgId)
  }
}

const getWalletPanelContainer = () => {
  try {
    browser.waitForVisible(walletContainerId, 30000)
    closeCookieMessage()
    return browser.element(walletContainerId)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] MasterPass sandbox did not appear',
      err,
      'FAILED-TO-GET-MASTERPASS-CONTAINER'
    )
  }
}

const selectWallet = () => {
  const container = getWalletPanelContainer()
  closeCookieMessage()
  container.click(walletMasterPassButtonClass)
}

const getFrameContainer = () => {
  try {
    browser.waitForVisible(frameContainerClass, 30000)
    return browser.element(frameContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] MasterPass sandbox frame did not appear',
      err,
      'FAILED-TO-GET-MASTERPASSFRAME-CONTAINER'
    )
  }
}

const clickFinishShoppingButton = () => {
  const container = getFrameContainer()
  container.waitForVisible(finishShoppingButtonTag)
  container.click(finishShoppingButtonTag)
}

const setEmailAddress = (emailAddress) => {
  const container = getFrameContainer()
  container.waitForVisible(emailInputId)
  container.setValue(emailInputId, emailAddress)
}

const setPassword = (password) => {
  const container = getFrameContainer()
  container.waitForVisible(passwordInputId)
  container.setValue(passwordInputId, password)
}

const clickSignInButton = () => {
  const container = getFrameContainer()
  container.waitForVisible(signIgnButtonId)
  container.click(signIgnButtonId)
}

const login = () => {
  const testAccount = getCredential('masterpass')
  setEmailAddress(testAccount.username)
  setPassword(testAccount.password)
  clickSignInButton()
}

export const confirmOrder = () => {
  waitForLoader()

  // select MasterPass as the wallet to use
  selectWallet()
  waitForLoader()

  // focus on the wallet frame
  switchToWalletFrame()

  // login as an existing MasterPass customer
  login()

  // switch back to parent frame and wait until overlay dissappears
  switchToParentFrame()
  waitForLoader()
  switchToWalletFrame()

  // checkout now
  clickFinishShoppingButton()

  // nb: wait until page is fully loaded before continuing as it will fail on success page wait otherwise because it took too long..
  switchToParentFrame()
  waitForLoader()
  browser.waitForVisible(loaderContainerClass, 30000, false)
  browser.waitForVisible(successContainer)
}
