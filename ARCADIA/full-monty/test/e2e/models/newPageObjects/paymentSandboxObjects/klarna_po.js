import * as shared from '../../lib/shared'
import * as overlayPO from '../components/overlay'

// identifiers
//-------------------------------
// main container identifier
const klarnaApplyCreditIframeId = 'klarna-credit-fullscreen'
const klarnaContainerId = '#signupGB'
const agreementContainerAttr = 'div[id="signupGB.steps.Agreement"]'
// page element identifiers
// nb: selector did not like id with period, using attr as a workaround
const dateOfBirthInputAttr = 'input[id="signupGB-dataCollection-dateOfBirth"]'
const confirmCheckBoxIAttr = 'div[id="signupGB-dataCollection-confirmCheckbox"]'
const signUpButtonId = '#signupGB-continueButton'

//-------------------------------

export const getKlarnaContainer = () => {
  overlayPO.waitForOverlay()
  try {
    browser.waitForVisible(klarnaContainerId)
    overlayPO.waitForOverlay()
    return browser.element(klarnaContainerId)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] KLARNA sandbox did not appear',
      err,
      'KLARNA-SANDBOX-FAILED'
    )
  }
}

/**
 * nb: breaking input into two section as it seems to be breaking when doing it all at once
 */
export const setDOB = () => {
  const date = '10/10/1980'
  let count = 5
  const klarnaContainer = getKlarnaContainer()

  while (klarnaContainer.getValue(dateOfBirthInputAttr) !== date && count > 0) {
    klarnaContainer.setValue(dateOfBirthInputAttr, date)
    browser.pause(500)
    console.log('[Klarna] set dob')
    count--
  }
}

export const clickConfirmCredit = () => {
  const klarnaContainer = getKlarnaContainer()
  klarnaContainer.click(confirmCheckBoxIAttr)
}

export const clickSubmitCreditApplication = () => {
  const klarnaContainer = getKlarnaContainer()
  klarnaContainer.click(signUpButtonId)
}

export const readAgreement = () => {
  const klarnaContainer = getKlarnaContainer()

  klarnaContainer.waitForVisible(agreementContainerAttr)

  browser.execute(() => {
    // retrieve container
    const content = document.getElementById('signupGB-agreement').parentElement
    // scroll to bottom
    content.scrollTop = content.scrollHeight
  })
}

export const clickSignAgreement = () => {
  const klarnaContainer = getKlarnaContainer()
  let count = 10

  // attempt to read agreement again for couple more times
  while (klarnaContainer.isEnabled(signUpButtonId) === false && count > 0) {
    readAgreement()
    browser.pause(500)
    count--
  }

  try {
    klarnaContainer.click(signUpButtonId)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] Unable to click sign agreement button',
      err,
      'FAILED-TO-CLICK-SIGN-AGREEMENT-BUTTON'
    )
  }
}

export const clickApplicationApprovedButton = () => {
  const klarnaContainer = getKlarnaContainer()
  klarnaContainer.click('button')
}

export const confirmOrderViaCreditApplication = () => {
  browser.waitForVisible(`#${klarnaApplyCreditIframeId}`)
  browser.frame(klarnaApplyCreditIframeId)

  // fill in the credit application form
  clickConfirmCredit()
  setDOB()

  clickSubmitCreditApplication()

  // agree pre-contract terms by clicking continue
  clickSubmitCreditApplication()

  // read and sign agreement
  clickSignAgreement()

  // approved!
  clickApplicationApprovedButton()

  // switch back to parent frame
  browser.frameParent()
}
