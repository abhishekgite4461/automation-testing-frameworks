import * as shared from '../../lib/shared'
import * as overlayPO from '../components/overlay'
import { getCredential } from '../../lib/credentials'

const assert = require('assert')

// identifiers
//-------------------------------
// main container identifier
const paypalLoginIFrameId = 'injectedUl'
const paypalSectionId = '#main'
// LOADING INDICATORS AND SPINNERS
const LoadingSpinnerId = '#preloaderSpinner'
const LoadingOverlayId = '#loading'
const anotherLoadingSpinnnerId = '#spinner'

// LOGIN PAGE ELEMENRTS
const loginInputId = '#email'
const passwordInputId = '#password'
const loginButtonId = '#btnLogin'
// OLD LOGIN LOOKUPS
const oldLoginInputId = '#login_password'
const confirmButtonTopId = '#confirmButtonTop'
const continueButtonId = '#continue_abovefold'

//-------------------------------

export const waitForPaypalLoader = () => {
  let count = 0
  while (
    (browser.isVisible(LoadingSpinnerId) === true ||
      browser.isVisible(LoadingOverlayId) === true ||
      browser.isVisible(anotherLoadingSpinnnerId) === true) &&
    count < 600
  ) {
    browser.pause(100)
    count++
  }
  if (count > 0) {
    console.log(
      `[waitForPaypalLoader] paypal processing loader was present for ${count.toString()} seconds`
    )
  }
}

export const waitForPaypalInterface = () => {
  let count = 0
  while (
    browser.isVisible(confirmButtonTopId) !== true &&
    browser.isVisible(continueButtonId) !== true &&
    browser.isVisible(oldLoginInputId) !== true &&
    count < 120
  ) {
    browser.pause(1000)
    count++
  }
  if (count > 0) {
    console.log(
      `[waitForPaypalInterface] paypal to choose what interface to throw at us for ${count.toString()} seconds`
    )
  }
}

export const getPaypalContainer = () => {
  overlayPO.waitForOverlay()
  waitForPaypalLoader()
  browser.frame()
  try {
    browser.waitForVisible(paypalSectionId, 10000)
    waitForPaypalLoader()
    waitForPaypalInterface()
    return browser.element(paypalSectionId)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] paypal sandbox did not appear',
      err,
      'FAILED-TO-GET-PAYPAL-CONTAINER'
    )
  }
}

export const clickConfirmPayment = () => {
  const paypalCheckout = getPaypalContainer()
  waitForPaypalLoader()

  // order summary in desktop mode
  if (paypalCheckout.isVisible(continueButtonId)) {
    paypalCheckout.click(continueButtonId)
  } else {
    paypalCheckout.click(confirmButtonTopId)
  }
}

export const getPaypalLoginSection = () => {
  overlayPO.waitForOverlay()
  waitForPaypalLoader()
  try {
    browser.waitForVisible(paypalSectionId, 10000)
    browser.waitForVisible(loginInputId, 10000)
    waitForPaypalLoader()
    return browser.element(paypalSectionId)
  } catch (err) {
    console.log(
      '[ERROR CAPTURE] paypal login container sandbox did not appear to be on the page within the ten second timeout, logging and failing test'
    )
    console.log(err)
    assert.ok(false, 'paypal login sandbox was never present')
  }
}

export const enterPaypalEmail = (email) => {
  const paypalSandbox = getPaypalLoginSection()
  paypalSandbox.setValue(loginInputId, email)
}

export const enterPaypalPassword = (password) => {
  const paypalSandbox = getPaypalLoginSection()
  paypalSandbox.setValue(passwordInputId, password)
}

export const clickLogin = () => {
  const paypalSandbox = getPaypalLoginSection()
  paypalSandbox.click(loginButtonId)
}

export const confirmOrder = () => {
  const testAccount = getCredential('paypal')

  waitForPaypalLoader()
  browser.frame(paypalLoginIFrameId)
  // get the login iframe and swap to use
  getPaypalLoginSection()
  enterPaypalEmail(testAccount.username)
  enterPaypalPassword(testAccount.password)
  clickLogin()
  clickConfirmPayment()
  waitForPaypalLoader()
  waitForPaypalInterface()
}
