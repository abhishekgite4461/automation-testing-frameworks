import * as shared from '../../lib/shared'
import * as overlayPO from '../components/overlay'

// identifiers
//-------------------------------
// main container identifier
const worldPayContainerId = '#main'
// select your bank page
const bankTag = '#idealIssuer'
const continueInputId = '#submitButton'
// in between page
const continueImgTag = 'img[alt="Continue"]'
// request info page
const requestContainerClass = '.container'
const requestInfoheaderTag = 'h2*=Request'
const authoriseInputTag = 'input[alt="Authorise"]'
//-------------------------------
export const getWorldpayContainer = () => {
  overlayPO.waitForOverlay()
  browser.waitForVisible(worldPayContainerId)
  browser.waitUntil(browser.getTitle().includes('iDeal'))
  try {
    return browser.$(worldPayContainerId)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] WorldPay iDEAL sandbox did not appear',
      err,
      'FAILED-TO-GET-WORLDPAY-IDEAL-CONTAINER'
    )
  }
}

export const getRequestInfoContainer = () => {
  overlayPO.waitForOverlay()
  browser.waitForVisible(requestContainerClass)
  browser.waitForVisible(requestInfoheaderTag)
  try {
    return browser.$(requestContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] WorldPay iDEAL sandbox request info page did not appear',
      err,
      'FAILED-TO-GET-WORLDPAY-IDEAL-REQUEST-INFO-CONTAINER'
    )
  }
}

export const selectYourBank = (bank) => {
  const container = getWorldpayContainer()
  container.waitForVisible(bankTag)
  container.selectByVisibleText(bankTag, bank)
}

export const clickContinueOnSelectYourBank = () => {
  const container = getWorldpayContainer()
  container.click(continueInputId)
}

export const clickContinue = () => {
  const container = getWorldpayContainer()
  if (container.isVisible(continueImgTag)) {
    container.click(continueImgTag)
  }
}

export const clickAuthorise = () => {
  const container = getRequestInfoContainer()
  container.click(authoriseInputTag)
}

export const confirmOrder = () => {
  selectYourBank('iDEAL Simulator')
  clickContinueOnSelectYourBank()
  clickAuthorise()
}
