import * as shared from '../../lib/shared'
import * as overlayPO from '../components/overlay'

// identifiers
//-------------------------------
// main container identifier
const worldpayContainerClass = '.secureWrapper.padding-20'
// HEADERS
const worldPayLogoRightClass = '.righty.logo.worldPayLogo'
// page element identifiers
const continueButtonClss = '.form .lefty'
const continueFormButtonClass = 'form input[name=continue]'

//-------------------------------
export const waitFor3DSecureForm = () => {
  try {
    browser.waitForVisible(worldPayLogoRightClass, 20000)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[WORLDPAY PO ERROR] - world pay form failed to ever load',
      err,
      'FAILED-WAITING-FOR-WORLD-PAY-TOP-RIGHT-LOGO'
    )
  }
}

export const getWorldpayContainer = () => {
  overlayPO.waitForOverlay()
  waitFor3DSecureForm()
  try {
    browser.waitForVisible(worldpayContainerClass, 60000)
    overlayPO.waitForOverlay()
    return browser.element(worldpayContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] worldpay sandbox did not appear',
      err,
      'FAILED-TO-GET-WORLDPAY-CONTAINER'
    )
  }
}

export const clickContinue = () => {
  const vbv = getWorldpayContainer()
  if (vbv.isVisible(continueButtonClss)) {
    vbv.click(continueButtonClss)
  } else if (vbv.isVisible(continueFormButtonClass)) {
    vbv.click(continueFormButtonClass)
  } else {
    console.log('[clickContinue] no matching world pay interface found')
  }
  browser.pause(1000)
}

export const confirmVBV = () => {
  // verify we're in WorldPay 3D Secure auth screen
  waitFor3DSecureForm()
  clickContinue()
  // redirect back to Monty Success page
}
