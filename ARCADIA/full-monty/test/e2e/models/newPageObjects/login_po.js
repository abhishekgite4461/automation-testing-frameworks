import * as modalPO from './components/overlay'
import * as shared from '../lib/shared'
import * as registrationPO from '../newPageObjects/registration_po'

const assert = require('assert')

// identifiers
//-------------------------------
// Main container identifier
const loginContainerClass = '.LoginContainer'
const loginInnerContainerClass = '.Login'
// page element identifiers
const emailInputClass = '.Input-field.Input-field-email'
const passwordInputClass = '.Input-field.Input-field-password'
const passwordToggleClass = '.Input-toggleButton'
const loginButtonClass = 'Button.Login-submitButton'
const loginErrorMsgClass = '.Message.is-shown.is-error'

const proceedButtonClass = '.Button.LoginContainer-guestCheckout--proceed'
//-------------------------------
const pageUrl = '/login'

export const getLoginContainer = () => {
  modalPO.waitForOverlay()
  try {
    browser.waitForVisible(loginInnerContainerClass, 10000)
    modalPO.waitForOverlay()
    return browser.element(loginInnerContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] login page container did not appear',
      err,
      'GET-LOGIN-CONTAINER-FAILED'
    )
  }
}

export const goToLoginPage = () => {
  browser.url(pageUrl)
  getLoginContainer()
}

export const getOuterContainer = () => {
  // email and password fields are in here and can throw error if not using this container
  getLoginContainer()
  return browser.element(loginContainerClass)
}

export const loginPagePresent = () => {
  modalPO.waitForOverlay()
  return browser.isVisible(loginInnerContainerClass)
}

export const enterEmail = (email) => {
  const loginForm = getLoginContainer()
  loginForm.setValue(emailInputClass, email)
  while (loginForm.getValue(emailInputClass) !== email) {
    loginForm.setValue(emailInputClass, email)
  }
}

export const enterPassword = (password) => {
  const loginForm = getLoginContainer()
  loginForm.setValue(passwordInputClass, password)
}

export const clickRegisterButton = () => {
  const loginForm = getLoginContainer()
  loginForm.click(loginButtonClass)
  modalPO.waitForOverlay()
}

export const loginErrorPresent = () => {
  const loginForm = getLoginContainer()
  return loginForm.isVisible(loginErrorMsgClass)
}

export const getLoginErrorText = () => {
  const loginForm = getLoginContainer()
  return loginForm.getText(loginErrorMsgClass)
}

export const loginAsUser = (username, password, shouldFail = false) => {
  let count = 0
  let exitFlag = false
  let errorMessage
  while (exitFlag !== true && count < 3) {
    count++
    enterEmail(username)
    enterPassword(password)
    clickRegisterButton()
    modalPO.waitForOverlay()
    if (shouldFail === true) {
      if (loginErrorPresent()) {
        errorMessage = getLoginErrorText()
      }
      if (
        errorMessage.indexOf(
          'The email address or password you entered has not been found'
        ) !== -1
      ) {
        exitFlag = true
      } else {
        exitFlag = false
        console.log(
          `[LOGIN PO] error message not what was expected, refreshing page and trying again, error message: ${getLoginErrorText()}`
        )
        browser.refresh()
      }
    } else {
      console.log(
        'checking for the login page present, if it is, refresh and try again'
      )
      if (loginPagePresent()) {
        console.log(
          '[LOGIN PO] login page still present, assuming there was error, refreshing'
        )
        browser.refresh()
      } else {
        exitFlag = true
      }
    }
  }
  if (count === 3) {
    shared.captureScreenshotAndError(
      '[PO ERROR CAPTURE] login failed three times in a row marking as failure',
      'login failed three times in a row',
      'login-failed-three-times-in-a-row'
    )
    registrationPO.registerNewUser(username, 'password1')
  }
}

export const clickProceed = () => {
  const loginForm = getOuterContainer()
  loginForm.click(proceedButtonClass)
  modalPO.waitForOverlay()
}

export const showEnteredPassword = () => {
  const loginForm = getLoginContainer()
  loginForm.click(passwordToggleClass)
}

export const isPasswordShown = () => {
  const loginForm = getLoginContainer()
  assert.equal(loginForm.getAttribute(passwordInputClass, 'type'), 'text')
}
