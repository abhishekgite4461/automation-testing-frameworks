import * as modalPO from '../components/overlay'
import * as shared from '../../lib/shared'

// identifiers
// -------------------------------
// Main container identifier
const myOrderHistoryDetailsContainerClass = '.OrderHistoryDetails'
const billingAddressSection = '.OrderHistoryDetailsAddress-billing'
const deliveryAddressSection = '.OrderHistoryDetailsAddress-shipping'
const pageHeaderTitle = '.AccountHeader-title'
const emptyOrderHistoryClass = '.OrderHistoryList-notFound'
const myOrderHistoryMenu = 'h3=My orders'
// page element identifiers

// -------------------------------
const pageUrl = '/my-account/order-history'

export const getMyOrderHistoryDetailsContainer = () => {
  modalPO.waitForOverlay()
  try {
    browser.waitForVisible(myOrderHistoryDetailsContainerClass, 10000)
    modalPO.waitForOverlay()
    return browser.element(myOrderHistoryDetailsContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] order history details container did not appear',
      err,
      'FAILED-TO-GET-ORDER-HISTORY-DETAILS-CONTAINER'
    )
  }
}

export const goToOrderHistoryPage = () => {
  browser.url(pageUrl)
  getMyOrderHistoryDetailsContainer()
}

export const getPageHeader = () => {
  const myDetails = getMyOrderHistoryDetailsContainer()
  return myDetails.getText(pageHeaderTitle)
}

export const emptyOrderHistoryPresent = () => {
  const myDetails = getMyOrderHistoryDetailsContainer()
  return myDetails.isVisible(emptyOrderHistoryClass)
}

export const getEmptyOrderHistoryText = () => {
  const myDetails = getMyOrderHistoryDetailsContainer()
  return myDetails.getText(emptyOrderHistoryClass)
}

export const getBillingAddressContainer = () => {
  const orderHistoryDetails = getMyOrderHistoryDetailsContainer()
  return orderHistoryDetails.element(billingAddressSection)
}

export const getBillingAddressText = () => {
  const orderHistoryDetails = getMyOrderHistoryDetailsContainer()
  return orderHistoryDetails.getText(billingAddressSection)
}

export const getDeliveryAddressContainer = () => {
  const orderHistoryDetails = getMyOrderHistoryDetailsContainer()
  return orderHistoryDetails.element(deliveryAddressSection)
}

export const getDeliveryAddressText = () => {
  const orderHistoryDetails = getMyOrderHistoryDetailsContainer()
  return orderHistoryDetails.getText(deliveryAddressSection)
}

export const openOrderHistoryDetails = () => {
  browser.element(myOrderHistoryMenu).click()
}
