import * as overlay from '../components/overlay'
import * as shared from '../../lib/shared'

// identifiers
//-------------------------------
// Main container identifier
const myOrderHistoryontainerClass = '.OrderHistoryList'
// page element identifiers
const historicalOrderClass = '.OrderElement'
const orderLimitMessageClass = '.OrderHistoryList-orderLimitMessage'
//-------------------------------
const pageUrl = '/my-account/order-history'

export const getMyOrderHistoryContainer = () => {
  overlay.waitForOverlay()
  try {
    browser.waitForVisible(myOrderHistoryontainerClass, 10000)
    overlay.waitForOverlay()
    return browser.element(myOrderHistoryontainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] order history container did not appear',
      err,
      'FAILED-TO-GET-ORDER-HISTORY-CONTAINER'
    )
  }
}

export const goToMyOrderHistory = () => {
  browser.url(pageUrl)
  getMyOrderHistoryContainer()
}

export const selectOrdersHistory = () => {
  const orderHistory = getMyOrderHistoryContainer()
  orderHistory.waitForVisible(historicalOrderClass)
  orderHistory.click(historicalOrderClass)
}

export const getOrderLimitMessage = () => {
  const orderHistory = getMyOrderHistoryContainer()
  orderHistory.waitForVisible(orderLimitMessageClass)
  return orderHistory.getText(orderLimitMessageClass)
}
