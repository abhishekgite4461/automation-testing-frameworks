import * as shared from '../../lib/shared'
import * as modalPO from '../components/overlay'

// identifiers
//-------------------------------
// Main container identifier
const checkoutDetailsContainerClass = '.CustomerDetails'

// page element identifiers
const pageHeaderTitle = '.AccountHeader-title'

const firstNameTextFieldClass = '.Input-field.Input-field-firstName'
const cityTextFieldClass = '.Input-field.Input-field-city'
const lastNameTextFieldClass = '.Input-field.Input-field-lastName'
const postcodeTextFieldClass = '.Input-field.Input-field-postcode'
const line2TextFieldClass = '.Input-field.Input-field-address2'
const line1TextFieldClass = '.Input-field.Input-field-address1'
const paymentOptionId = '#paymentOption'

//-------------------------------
const pageUrl = '/my-account/details'

export const getCheckoutDetailsContainer = () => {
  modalPO.waitForOverlay()
  try {
    browser.waitForVisible(checkoutDetailsContainerClass, 10000)
    modalPO.waitForOverlay()
    return browser.element(checkoutDetailsContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] checkout details container did not appear',
      err,
      'FAILED-TO-GET-CHECKOUT-DETAILS-CONTAINER'
    )
  }
}

export const goToCheckoutDetails = () => {
  browser.url(pageUrl)
  getCheckoutDetailsContainer()
}

export const getPageHeader = () => {
  const myDetails = getCheckoutDetailsContainer()
  return myDetails.getText(pageHeaderTitle)
}

export const getFirstNameValue = () => {
  const myDetails = getCheckoutDetailsContainer()
  return myDetails.getValue(firstNameTextFieldClass)
}

export const setFirstNameValue = (name) => {
  const myDetails = getCheckoutDetailsContainer()
  myDetails.setValue(firstNameTextFieldClass, name)
}

export const getLastNameValue = () => {
  const myDetails = getCheckoutDetailsContainer()
  return myDetails.getValue(lastNameTextFieldClass)
}

export const setLastNameValue = (name) => {
  const myDetails = getCheckoutDetailsContainer()
  myDetails.setValue(lastNameTextFieldClass, name)
}

export const getPostcodeNameValue = () => {
  const myDetails = getCheckoutDetailsContainer()
  return myDetails.getValue(postcodeTextFieldClass)
}

export const setPostcodeValue = (name) => {
  const myDetails = getCheckoutDetailsContainer()
  myDetails.setValue(postcodeTextFieldClass, name)
}

export const getCityNameValue = () => {
  const myDetails = getCheckoutDetailsContainer()
  return myDetails.getValue(cityTextFieldClass)
}

export const setCityValue = (name) => {
  const myDetails = getCheckoutDetailsContainer()
  myDetails.setValue(cityTextFieldClass, name)
}

export const getLine2NameValue = () => {
  const myDetails = getCheckoutDetailsContainer()
  return myDetails.getValue(line2TextFieldClass)
}

export const setLine2Value = (name) => {
  const myDetails = getCheckoutDetailsContainer()
  myDetails.setValue(line2TextFieldClass, name)
}

export const getLine1NameValue = () => {
  const myDetails = getCheckoutDetailsContainer()
  return myDetails.getValue(line1TextFieldClass)
}

export const setLine1Value = (name) => {
  const myDetails = getCheckoutDetailsContainer()
  myDetails.setValue(line1TextFieldClass, name)
}

export const getPaymentType = () => {
  const myDetails = getCheckoutDetailsContainer()

  let paymentType = myDetails.getText(paymentOptionId)
  paymentType = paymentType.split('\n')

  return paymentType
}
