import * as overlay from '../components/overlay'
import * as shared from '../../lib/shared'

// identifiers
//-------------------------------
// Main container identifier
const myReturnHistoryontainerClass = '.ReturnHistoryList'
// page element identifiers
const pageHeaderTitle = '.AccountHeader-title'
const returnOrderElement = '.OrderElement'

// const historicalReturnClass = '.ReturnHistoryElement'
// const returnLimitMessageClass = '.ReturnHistoryList-ReturnLimitMessage'
const pageUrl = '/my-account/return-history'

export const getMyReturnHistoryContainer = () => {
  overlay.waitForOverlay()
  try {
    browser.waitForVisible(myReturnHistoryontainerClass, 10000)
    overlay.waitForOverlay()
    return browser.element(myReturnHistoryontainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] return history container did not appear',
      err,
      'FAILED-TO-GET-RETURN-HISTORY-CONTAINER'
    )
  }
}

export const goToMyReturnHistory = () => {
  browser.url(pageUrl)
  getMyReturnHistoryContainer()
}

export const getPageHeader = () => {
  const container = getMyReturnHistoryContainer()
  return container.getText(pageHeaderTitle)
}

export const getReturnsList = () => {
  const container = getMyReturnHistoryContainer()
  return container.elements(returnOrderElement).getText()
}
