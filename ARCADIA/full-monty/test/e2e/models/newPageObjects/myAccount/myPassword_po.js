import * as modalPO from '../components/overlay'
import * as shared from '../../lib/shared'

// identifiers
//-------------------------------
// Main container identifier
const myPasswordContainerClass = '.ChangePassword'
// page element identifiers
const HeaderTitleClass = '.AccountHeader-title'
const currentPasswordTextFieldclass = '.Input-field.Input-field-oldPassword'
const newPasswordTextFieldClass = '.Input-field.Input-field-newPassword'
const newPasswordConfirmTextFieldClss =
  '.Input-field.Input-field-newPasswordConfirm'
const saveButtonClass = '.Button.ChangePassword-saveChanges'
const confirmMessageClass = '.Message.is-shown.is-confirm '
//-------------------------------
const pageUrl = '/my-account/my-password'
const assert = require('assert')

export const getMyPasswordContainer = () => {
  modalPO.waitForOverlay()
  try {
    browser.waitForVisible(myPasswordContainerClass, 10000)
    modalPO.waitForOverlay()
    browser.waitForExist(currentPasswordTextFieldclass)
    return browser.element(myPasswordContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] change password container did not appear',
      err,
      'FAILED-TO-GET-MY-PASSWORD-CONTAINER'
    )
  }
}

export const goToMyPassword = () => {
  browser.url(pageUrl)
  getMyPasswordContainer()
}

export const setCurrentPassword = (currentPassword) => {
  const passwordContainer = getMyPasswordContainer()
  passwordContainer.setValue(currentPasswordTextFieldclass, currentPassword)
}

export const setNewPassword = (newPassword) => {
  const passwordContainer = getMyPasswordContainer()
  passwordContainer.setValue(newPasswordTextFieldClass, newPassword)
}

export const setNewConfirmPassword = (newConfirmPassword) => {
  const passwordContainer = getMyPasswordContainer()
  passwordContainer.setValue(
    newPasswordConfirmTextFieldClss,
    newConfirmPassword
  )
}

export const saveButtonIsEnabled = () => {
  const passwordContainer = getMyPasswordContainer()
  return passwordContainer.isEnabled(saveButtonClass)
}

export const clickSavePasswordButton = () => {
  const passwordContainer = getMyPasswordContainer()
  if (saveButtonIsEnabled() !== true) {
    passwordContainer.waitForEnabled(saveButtonClass, 5000)
  }
  passwordContainer.click(saveButtonClass)
}

export const updatePassword = (
  oldPassword,
  newPassword,
  newConfirmPassword
) => {
  setCurrentPassword(oldPassword)
  setNewPassword(newPassword)
  setNewConfirmPassword(newConfirmPassword)
  clickSavePasswordButton()
  modalPO.waitForOverlay()
}

export const changePasswordFormElementsPresent = () => {
  const passwordContainer = getMyPasswordContainer()
  assert.ok(
    passwordContainer.isVisible(currentPasswordTextFieldclass),
    'current password form entry not found'
  )
  assert.ok(
    passwordContainer.isVisible(newPasswordTextFieldClass),
    'new password form entry not found'
  )
  assert.ok(
    passwordContainer.isVisible(newPasswordConfirmTextFieldClss),
    'new confirmation password form entry not found'
  )
}

export const waitForConfirmationMessage = () => {
  const passwordContainer = getMyPasswordContainer()
  passwordContainer.waitForVisible(confirmMessageClass)
}

export const getConfirmationMessage = () => {
  waitForConfirmationMessage()
  const passwordContainer = getMyPasswordContainer()
  return passwordContainer.getText(confirmMessageClass)
}

export const getPageHeaderTitleText = () => {
  const passwordContainer = getMyPasswordContainer()
  return passwordContainer.getText(HeaderTitleClass)
}
