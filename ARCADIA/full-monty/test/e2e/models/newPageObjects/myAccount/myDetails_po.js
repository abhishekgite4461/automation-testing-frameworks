import * as modalPO from '../components/overlay'
import * as shared from '../../lib/shared'

// identifiers
//-------------------------------
// Main container identifier
const myDetailsContainerClass = '.CustomerShortProfile'

// page element identifiers
const pageHeaderTitle = '.AccountHeader-title'
const firstNameTextFieldClass = '.Input-field.Input-field-firstName'
const emailTextFieldClass = '.Input-field.Input-field-email'
const titleSelectListId = '#title'
const lastNameTextFieldClass = '.Input-field.Input-field-lastName'
const submitButtonClass = '.Button.CustomerShortProfile-saveChanges'
const updatedDetailsMessage = '.Message-message'
//-------------------------------
const pageUrl = '/my-account/my-details'

export const getMyDetailsContainer = () => {
  modalPO.waitForOverlay()
  try {
    browser.waitForVisible(myDetailsContainerClass, 10000)
    modalPO.waitForOverlay()
    return browser.element(myDetailsContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] my details container did not appear',
      err,
      'FAILED-TO-GET-MY-DETAILS-CONTAINER'
    )
  }
}

export const goToMyDetails = () => {
  browser.url(pageUrl)
  getMyDetailsContainer()
}

export const getPageHeader = () => {
  const myDetails = getMyDetailsContainer()
  return myDetails.getText(pageHeaderTitle)
}

export const selectTitle = (title) => {
  const myDetails = getMyDetailsContainer()
  myDetails.selectByValue(titleSelectListId, title)
}

export const setFirstNameValue = (name) => {
  const myDetails = getMyDetailsContainer()
  myDetails.setValue(firstNameTextFieldClass, name)
}

export const setEmailValue = (name) => {
  const myDetails = getMyDetailsContainer()
  myDetails.setValue(emailTextFieldClass, name)
}

export const getEmailValue = () => {
  const myDetails = getMyDetailsContainer()
  return myDetails.getValue(emailTextFieldClass)
}

export const getFirstNameValue = () => {
  const myDetails = getMyDetailsContainer()
  return myDetails.getValue(firstNameTextFieldClass)
}

export const getLastNameValue = () => {
  const myDetails = getMyDetailsContainer()
  return myDetails.getValue(lastNameTextFieldClass)
}

export const setLastNameValue = (name) => {
  const myDetails = getMyDetailsContainer()
  myDetails.setValue(lastNameTextFieldClass, name)
}

export const saveProfileChanges = () => {
  const myDetails = getMyDetailsContainer()
  myDetails.waitForEnabled(submitButtonClass, 5000)
  myDetails.click(submitButtonClass)
  modalPO.waitForOverlay()
}

export const updateDetails = (title, firstName, lastName) => {
  selectTitle(title)
  setFirstNameValue(firstName)
  setLastNameValue(lastName)
  setEmailValue(shared.createRandomEmailAddress())
  saveProfileChanges()
}

export const getSaveChangesMessage = () => {
  return browser.element(updatedDetailsMessage).getText()
}
