import * as overlay from '../components/overlay'
import * as shared from '../../lib/shared'

// identifiers
//-------------------------------
// Main container identifier
const myAccountContainerClass = '.MyAccount'
// page element identifiers
const myAccountOptionTitleClass = '.AccountItem-title'
const myDetailsMenu = '.AccountItem-title=My details'
const myReturnsMenu = '.AccountItem-title=My Returns'
//-------------------------------
const pageUrl = '/my-account'

export const goToMyAccount = () => {
  browser.url(pageUrl)
  overlay.waitForOverlay()
}

export const getMyAccountContainer = () => {
  overlay.waitForOverlay()
  try {
    overlay.waitForOverlay()
    browser.waitForVisible(myAccountContainerClass, 10000)
    overlay.waitForOverlay()
    return browser.element(myAccountContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] myAccount container did not appear',
      err,
      'FAILED-TO-GET-MY-ACCOUNT-CONTAINER'
    )
  }
}

export const myAccountContainerPresent = () => {
  overlay.waitForOverlay()
  return browser.isVisible(myAccountContainerClass)
}

export const getMyAccountsOptions = () => {
  const myAccount = getMyAccountContainer()
  return myAccount.getText(myAccountOptionTitleClass)
}

export const getNumberOfAvailableOption = () => {
  const options = getMyAccountsOptions()
  return options.length()
}

export const openMyDetailsMenu = () => {
  const container = getMyAccountContainer()
  container.click(myDetailsMenu)
}

export const openMyReturnsMenu = () => {
  const container = getMyAccountContainer()
  container.click(myReturnsMenu)
}
