import * as overlay from './components/overlay'
import * as shared from '../lib/shared'

// identifiers
//-------------------------------
// Main container identifier
const registerContainerClass = '.Register'
// page element identifiers
const emailInputClass = '.Input-field.Input-field-email'
const passwordInputClass = '.Input-field.Input-field-password'
const passwordConfirmationInputClass =
  '.Input-field.Input-field-passwordConfirm'
// to interact with the checkbox you inspect the checkbox but have to click the span
const weeklyNewsletterCheckboxSpanClass = '.Checkbox-check'
const weeklyNewsletterCheckboxClass = '.Checkbox-field'
const registerButtonClass = 'Button.Register-saveChanges'
const loginErrorMsgClass = '.Message.is-shown.is-error'
// page url
const loginPageUrl = '/login'
//-------------------------------

export const getRegistrationContainer = () => {
  overlay.waitForOverlay()
  try {
    browser.waitForVisible(registerContainerClass, 10000)
    overlay.waitForOverlay()
    return browser.element(registerContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] registration page container did not appear',
      err,
      'FAILED-TO-GET-REGISTRATION-CONTAINER'
    )
  }
}

export const registrationPagePresent = () => {
  return browser.isVisible(registerContainerClass)
}

export const clickRegisterButton = () => {
  const registerForm = getRegistrationContainer()
  registerForm.click(registerButtonClass)
  overlay.waitForOverlay()
}

export const enterEmail = (email) => {
  const registerForm = getRegistrationContainer()
  console.log(`entering email address: ${email}`)
  registerForm.setValue(emailInputClass, email)
  while (registerForm.getValue(emailInputClass) !== email) {
    console.log(
      `got the following from the email value check, validation broken this by kciking in to fast: ${registerForm.getValue(
        emailInputClass
      )}`
    )
    browser.clearElement(emailInputClass)
    registerForm.setValue(emailInputClass, email)
  }
}

export const enterPassword = (password) => {
  const registerForm = getRegistrationContainer()
  registerForm.setValue(passwordInputClass, password)
}

export const enterConfirmationPassword = (confirmPassword) => {
  const registerForm = getRegistrationContainer()
  registerForm.setValue(passwordConfirmationInputClass, confirmPassword)
}

export const weeklyNewsletterIsChecked = () => {
  const registerForm = getRegistrationContainer()
  const result = registerForm.isSelected(weeklyNewsletterCheckboxClass)
  return result
}

export const uncheckWeeklyEmailCheckbox = () => {
  const registerForm = getRegistrationContainer()
  if (weeklyNewsletterIsChecked() === true) {
    registerForm.click(weeklyNewsletterCheckboxSpanClass)
  }
}

export const errorMessagePresent = () => {
  const registrationForm = getRegistrationContainer()
  return registrationForm.isVisible(loginErrorMsgClass)
}

export const weeklyNewsletterIsPresent = () => {
  const registerForm = getRegistrationContainer()
  return registerForm.isVisible(weeklyNewsletterCheckboxSpanClass)
}

export const getErrorText = () => {
  const registrationForm = getRegistrationContainer()
  return registrationForm.getText(loginErrorMsgClass)
}

export const registerNewUser = (email, password) => {
  browser.url(loginPageUrl)
  enterEmail(email)
  enterPassword(password)
  enterConfirmationPassword(password)
  uncheckWeeklyEmailCheckbox()
  clickRegisterButton()
  while (registrationPagePresent() && errorMessagePresent() !== true) {
    console.log(
      '[PO INFO] waiting for either registration page to move on or error message to be shown'
    )
  }

  // override for error from server in failed registration to try again
  if (registrationPagePresent() && errorMessagePresent()) {
    console.log('[ERROR AVERSION] Failed reg, trying to cope by re-registering')
    console.log(getErrorText())
    browser.refresh()
    enterEmail(email)
    enterPassword(password)
    enterConfirmationPassword(password)
    uncheckWeeklyEmailCheckbox()
    clickRegisterButton()
  }
}

export const enterBlankEmail = () => {
  const registerForm = getRegistrationContainer()
  registerForm.setValue(emailInputClass, ['\t'])
}

export const checkWeeklyEmailCheckbox = () => {
  const registerForm = getRegistrationContainer()
  if (weeklyNewsletterIsChecked() === false) {
    registerForm.click(weeklyNewsletterCheckboxSpanClass)
  }
}

export const registerButtonVisible = () => {
  const registerForm = getRegistrationContainer()
  return registerForm.isVisible(registerButtonClass)
}
