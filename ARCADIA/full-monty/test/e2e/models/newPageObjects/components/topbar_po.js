import * as shared from '../../lib/shared'
import * as client from '../../lib/client'
import * as overlay from '../components/overlay'
import * as addToBagModal from '../productDetail/addToBagModal'
import * as shoppingPO from '../shoppingBag/ShoppingBagBase'

const assert = require('assert')

// identifiers
//-------------------------------
// Main container identifier
const mobileHeaderContainerClass = '.Header-container'
const desktopHeaderContainerClass = '.HeaderBig'
const shoppingBagContainerClass = '.Drawer'

// page element identifiers
// mobile-only
const mobileShoppingBagButtonClass = '.Header-shoppingCartIconbutton'
const burgerMenuClass = '.Header-burgerButtonContainer'
const searchIconClass = '.Header-searchButton'
const menuIsOpenClass = '.TopNavMenu.is-open'
// desktop-only
const desktopShoppingBagButtonClass = '.ShoppingCart'
const searchBarCloseButtonClass = '.SearchBar-closeIconHidden'
const headerContainerClass = client.hasDesktopLayout()
  ? desktopHeaderContainerClass
  : mobileHeaderContainerClass
const shoppingBagButtonClass = client.hasDesktopLayout()
  ? desktopShoppingBagButtonClass
  : mobileShoppingBagButtonClass

//-------------------------------

export const getHeaderContainer = () => {
  overlay.waitForOverlay()

  try {
    browser.waitForVisible(headerContainerClass, 10000)
    return browser.element(headerContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] Header container did not appear',
      err,
      'FAILED-TO-GET-HEADER-CONTAINER'
    )
  }
}

export const closeSearchBar = () => {
  const header = getHeaderContainer()
  header.waitForVisible(searchBarCloseButtonClass)
  header.click(searchBarCloseButtonClass)
}

export const openShoppingBag = () => {
  // TODO: temporarily navigate back to homepage to click on button until miniBagConfirm z-index issue is
  // fixed in DES-293 and flash of topnavbar is fixed in DES-307
  if (client.hasDesktopLayout()) {
    browser.url('/')
  }

  // close add to bag modal if it is open (mobile-only)
  if (browser.isVisible('.Modal.is-shown')) {
    addToBagModal.clickCloseModal()
  }

  getHeaderContainer()

  if (browser.isVisible(`${shoppingBagContainerClass}.is-open`) === false) {
    // close search input if it is in focus
    if (browser.isVisible(searchBarCloseButtonClass)) {
      closeSearchBar()
    }

    overlay.waitForOverlay()
    browser.waitForVisible(shoppingBagButtonClass)
    browser.click(shoppingBagButtonClass)

    browser.waitForVisible(`${shoppingBagContainerClass}.is-open`)
  } else {
    console.log('[TOPBAR] Shopping bag is already open')
  }
}

export const openMenu = () => {
  const header = getHeaderContainer()
  header.click(burgerMenuClass)
  browser.waitForVisible(menuIsOpenClass)
}

export const openSearchBar = () => {
  const header = getHeaderContainer()
  header.click(searchIconClass)
  browser.waitForVisible(searchIconClass)
}

export const getNumberOfProductsFromShoppingBagIcon = () => {
  const header = getHeaderContainer()
  console.log(`number of products : ${header.getText(shoppingBagButtonClass)}`)
  return header.getText(shoppingBagButtonClass)
}

export const checkTotalOfTheShoppingBag = () => {
  const initValue1 = shoppingPO.getTotalPrice()
  process.env.initBasketValue = parseFloat(initValue1)
}

export const recheckTotalOfTheShoppingBag = () => {
  const valBasket1 = shoppingPO.getTotalPrice()
  const valBasket = parseFloat(valBasket1)
  assert.ok(
    process.env.initBasketValue > valBasket,
    'Basket value unchanged after Item deletion'
  )
}
