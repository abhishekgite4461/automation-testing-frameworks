const giftCardContainerClass = '.GiftCardContainer .Accordion-icon'
const giftCardNumberInputClass = '.Input-field.Input-field-giftCardNumber'
const giftCardPinInputClass = '.Input-field.Input-field-pin'
const confirmGiftCard = '.GiftCardContainer .Button'
const giftCardBannerClass = '.GiftCardContainer-banner'
const giftCardAmuntUsedClass = '.GiftCard-amountUsed'

export const openGiftCardSection = () => {
  browser.waitForVisible(giftCardContainerClass)
  browser.click(giftCardContainerClass)
  browser.waitForVisible(giftCardNumberInputClass)
}

export const enterGiftCard = (giftCard) => {
  browser.setValue(giftCardNumberInputClass, giftCard)
}

export const enterGiftCardPIN = (pin) => {
  browser.setValue(giftCardPinInputClass, pin)
}

export const clickConfirmGiftCard = () => {
  browser.click(confirmGiftCard)
}

export const getGiftCardMessageText = () => {
  browser.waitForVisible(giftCardBannerClass)
  return browser.getText(giftCardBannerClass)
}

export const getGiftCardAmountUsed = () => {
  browser.waitForVisible(giftCardAmuntUsedClass)
  return browser.getText(giftCardAmuntUsedClass)
}
