import * as client from '../../lib/client'
import * as shared from '../../lib/shared'
import * as overlayPO from './overlay'

const assert = require('assert')

// identifiers
//-------------------------------
// main container identifier
const carouselContainerClass = '.Carousel'
// page element identifiers
const slideClass = '.Carousel-item'
const selectedSlideClass = '.Carousel-item.is-selected'
const imageClass = '.Carousel-image'
const rightArrowClass = '.Carousel-arrow--right'
const leftArrowClass = '.Carousel-arrow--left'
const paginationBulletClass = '.Carousel-selector'
const selectedBulletClass = '.Carousel-selector.is-selected'
const carouselModal = '.Modal.is-shown'
const carouselModalImage = '.Carousel-images'
const closeCarouselModal = '.Modal.is-shown .Modal-closeIcon'
//-------------------------------
// carousel displays collapsed while screen is loading, this waits until carousel layout kicks in
export const waitForCarousel = () => {
  let counter = 0

  try {
    // waits until image height is more than 100px
    while (
      browser.getElementSize(imageClass, 'height')[0] < 100 &&
      counter < 1600
    ) {
      counter += 1
    }
  } catch (err) {
    assert.ok(false, 'Time out loading carousel image')
  }
}

export const getCarouselContainer = () => {
  overlayPO.waitForOverlay()
  waitForCarousel()
  try {
    browser.waitForVisible(carouselContainerClass, 10000)
    overlayPO.waitForOverlay()
    return browser.element(carouselContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] carousel did not appear',
      err,
      'FAILED-TO-GET-CAROUSEL-CONTAINER'
    )
  }
}

export const doubleTapZoom = () => {
  const carousel = getCarouselContainer()

  // 'double click' that works instead of
  carousel.click(selectedSlideClass)
  browser.pause(10)
  carousel.click(selectedSlideClass)

  // waits for CSS transition
  browser.pause(300)
}

export const hoverZoom = (state = 'in') => {
  const carousel = getCarouselContainer()

  if (state === 'in') {
    carousel.moveToObject(selectedSlideClass)
  } else {
    // hover on anything else other than carousel
    browser.moveToObject('.HeaderBig')
  }

  // waits for CSS transition
  browser.pause(300)
}

export const clickArrow = (direction) => {
  const carousel = getCarouselContainer()
  const selector = `.Carousel-arrow--${direction}`

  carousel.waitForVisible(selector)
  client.click(selector)
}

export const hasCorrectNumberOfSelector = () => {
  const carousel = getCarouselContainer()
  const slides = carousel.elements(slideClass)
  const selectors = carousel.elements(paginationBulletClass)

  assert.ok(
    slides.length === selectors.length,
    'Number of pagination bullets did not match number of carousel slides'
  )
}

export const hasArrow = () => {
  const carousel = getCarouselContainer()

  const rightArrow = carousel.element(rightArrowClass)
  const leftArrow = carousel.element(leftArrowClass)

  assert.ok(
    rightArrow.value !== null && leftArrow !== null,
    'Missing carousel arrow'
  )
}

export const hasProductImages = () => {
  const carousel = getCarouselContainer()

  const slides = carousel.elements(slideClass)
  const imageUrls = carousel.getAttribute(imageClass, 'src')

  assert.ok(
    slides.value.length === imageUrls.length,
    `${
      slides.value.length
    } expected Number of images did not match number of carousel slides of ${
      imageUrls.length
    }`
  )
}

export const hasSelectedImageDisplayed = (position) => {
  const carousel = getCarouselContainer()
  let index

  const selectedSlide = carousel.element(selectedSlideClass)
  const slides = carousel.elements(slideClass)

  switch (position) {
    case 'first':
      index = 0
      break
    case 'second':
      index = 1
      break
    default:
      index = slides.value.length - 1
  }

  assert.equal(
    selectedSlide.value.ELEMENT,
    slides.value[index].ELEMENT,
    'Did not display image for the selected carousel slide'
  )
}

export const hasPaginationBulletHighlighted = (position) => {
  const carousel = getCarouselContainer()
  let index

  const selectedBullet = carousel.element(selectedBulletClass)
  const bullets = carousel.elements(paginationBulletClass)

  switch (position) {
    case 'first':
      index = 0
      break
    case 'second':
      index = 1
      break
    default:
      index = bullets.length - 1
  }

  assert.equal(
    selectedBullet.value.ELEMENT,
    bullets.value[index].ELEMENT,
    'Did not highlight pagination bullet for the selected carousel slide'
  )
}

export const zoom = (state = 'in') => {
  // zoom in
  if (state === 'in') {
    if (client.hasDesktopLayout()) {
      hoverZoom('in')
    } else {
      doubleTapZoom()
    }
  } else if (state === 'out') {
    // zoom out(client.hasDesktopLayout()) ? hoverZoom('out') : doubleTapZoom()
    if (client.hasDesktopLayout()) {
      hoverZoom('out')
    } else {
      doubleTapZoom()
    }
  }
}

export const openCarouselFullScreenIcon = () => {
  browser.click('.Carousel-zoom')
}

export const openCarouselFullScreenClick = () => {
  const image = browser.element(carouselModalImage)

  image.click()
}

export const closeCarouselFullScreen = () => {
  const closeModal = browser.element(closeCarouselModal)
  closeModal.click()
}

export const hasCarouselModalOpen = () => {
  const modal = browser.element(carouselModal)
  const image = browser.element(carouselModalImage)
  const imageSize = image.getElementSize('width')
  console.log(`Image size : ${imageSize}`)
  assert.equal(true, modal.isVisible(), 'Product Image is not on full screen')
  assert.equal(true, imageSize > 0, 'Carousel Image not displayed on Modal')
}

export const hasCarouselModalClose = () => {
  const modal = browser.element(carouselModal)
  browser.pause(1500)
  assert.equal(false, modal.isVisible(), 'Carousel Modal did not close')
}
