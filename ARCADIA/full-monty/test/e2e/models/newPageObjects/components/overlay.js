/* globals browser:false */

// identifiers
//-------------------------------
// page element identifiers
const loaderOverlayClass = '.LoaderOverlay.is-shown'
const overlayMaskClass = '.ContentOverlay.ContentOverlay--modalOpen'
const loaderCircle = '.Loader-circle.Loader-circle1'
const productListLoaderClass = '.ProductList-loader .Loader-image'
const modal = '.Modal.is-shown.Modal--normal'

//-------------------------------

export const waitForOverlay = () => {
  browser.pause(200)
  browser.waitForVisible(loaderOverlayClass, 160000, true)
  browser.waitForVisible(loaderCircle, 160000, true)
  browser.pause(300)
}

export const waitUntilProductsAreLoaded = () => {
  browser.pause(200)
  // preloader might not exist/appear if products are loaded from cache
  if (browser.isVisible(productListLoaderClass)) {
    // and then wait some more until product loader disappears
    browser.waitUntil(
      browser.isVisibleWithinViewport(productListLoaderClass) === false,
      120000,
      '[Overlay] Product list loader is still visible'
    )
  }

  // wait for CSS transition
  browser.pause(1000)
}

export const getModalText = () => {
  return browser.getText(overlayMaskClass)
}

export const warningModalIsVisible = () => {
  return browser.isVisible(modal)
}

export const overlayIsVisible = () => {
  return browser.isVisible(overlayMaskClass)
}
