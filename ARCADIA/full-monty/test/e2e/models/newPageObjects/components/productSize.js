import * as shared from '../../lib/shared'
import * as overlay from '../components/overlay'

// identifiers
//-------------------------------
// Main container identifier
const sizeContainerClass = '.ProductDetail-sizeAndQuantity'

// page element identifiers
const buttonSizesListClass = '.ProductSizes-list'
const buttonSizeClass = '.ProductSizes-button'
const dropdownSizeClass = '#productSizes'
//-------------------------------

const getProductSizeContainer = () => {
  overlay.waitForOverlay()

  try {
    browser.waitForVisible(sizeContainerClass)
    return browser.$(sizeContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] Product size container did not appear',
      err,
      'FAILED-TO-GET-PRODUCT-SIZE-CONTAINER'
    )
  }
}

const hasSizeTile = () => {
  const container = getProductSizeContainer()
  console.log(`[hasSizeTile]: ${container.isVisible(buttonSizesListClass)}`)

  return container.isVisible(buttonSizesListClass)
}

const hasSizeDropdown = () => {
  const container = getProductSizeContainer()
  console.log(`[hasSizeTile]: ${container.isVisible(dropdownSizeClass)}`)
  return container.isVisible(dropdownSizeClass)
}

const getButtonSizes = () => {
  const container = getProductSizeContainer()
  let sizes = container.getText(buttonSizesListClass)

  // replace all options with special characters and low stock with | for use later to split the options
  sizes = sizes.replace(/((\w|\d)+(\n|\r)Low stock(\n|\r)?)/gim, '|')
  // replace any other special characters hanging around so we just have the available size options
  sizes = sizes.replace(/(?:\r\n|\r|\n)/g, '|')

  console.log(`[getButtonSizes] ${sizes}`)

  return sizes.split('|')
}

const getSelectSizes = () => {
  const container = getProductSizeContainer()
  let sizes = container.getText(dropdownSizeClass)

  // remove select list low and out of stock items
  sizes = sizes.replace(/Size/g, '|')
  // remove the first option which is not valid (todo may need to replace with the translation keyword)
  sizes = sizes.replace('Please select', '')
  // remove out of stock sizes
  sizes = sizes.replace(/.*: Out of stock.*?\n/gi, '')
  // remove low stock sizes and return as string
  sizes = sizes.replace(/.*: Low stock.*?\n/gi, '')
  sizes = sizes.replace(/(?:\r\n|\r|\n)/g, '')
  sizes = sizes.replace(/ /g, '')

  console.log(`[getSelectSizes] ${sizes}`)

  return sizes.split('|')
}

export const getSizes = () => {
  let sizes = []

  switch (true) {
    case hasSizeTile():
      sizes = getButtonSizes()
      break
    case hasSizeDropdown():
      sizes = getSelectSizes()
      break
    // defaults to one size
    default:
      sizes = ['ONE']
      break
  }

  return sizes
}

export const getARandomSize = () => {
  const sizes = getSizes()

  if (sizes.length === 0) {
    throw new Error('[getARandomSize] Exiting, no available sizes')
  }

  const randomSize = shared.shuffle(sizes).pop()
  return randomSize
}

const clickSizeTile = (size) => {
  const container = getProductSizeContainer()
  container.click(`${buttonSizeClass}=${size}`)
}

const selectSizeDropdown = (size) => {
  const container = getProductSizeContainer()
  container.selectByValue(dropdownSizeClass, size)
}

export const setSize = (size = getARandomSize()) => {
  console.log(`[setSize] ${size}`)
  switch (true) {
    case hasSizeTile():
      clickSizeTile(size)
      break
    case hasSizeDropdown():
      selectSizeDropdown(size)
      break
    // defaults to one size
    default:
      console.log('[setSize] Likely a one sized product, do nothing')
      break
  }

  process.env.SELECTED_PRODUCT_SIZE = size
}
