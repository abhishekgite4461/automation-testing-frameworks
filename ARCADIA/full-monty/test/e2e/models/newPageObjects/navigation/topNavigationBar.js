import * as shared from '../../lib/shared'
import * as overlay from '../components/overlay'
import * as searchPO from '../../pageObjects/search'
import * as client from '../../lib/client'

// identifiers
//-------------------------------
// main container identifier
const topNavContainerMob = '.Header-container'
const topNavContainerDes = '.HeaderBig'
// page element identifiers
const logoClass = '.BrandLogo-img'
const burgerMenu = '.BurgerButton'
const signInLinkClass = '.AccountIcon-link'
//-------------------------------
const containerClass = client.hasDesktopLayout()
  ? topNavContainerDes
  : topNavContainerMob

export const getTopNavContainer = () => {
  overlay.waitForOverlay()
  try {
    browser.waitForVisible(containerClass)
    return browser.element(containerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] Top Navigation bar did not appear',
      err,
      'FAILED-TO-GET-TOPNAVIGATIONBAR-CONTAINER'
    )
  }
}

export const getLogo = () => {
  const container = getTopNavContainer()
  return container.$$(logoClass)
}

export const clickBurgerIcon = () => {
  const container = getTopNavContainer()
  container.$(burgerMenu).click()
}

export const search = (product) => {
  process.env.SEARCH_TERM = product
  shared.openLandingPage()
  searchPO.searchProduct(product)
}

export const clickSignInLink = () => {
  const container = getTopNavContainer()
  container.click(signInLinkClass)
  overlay.waitForOverlay()
}
