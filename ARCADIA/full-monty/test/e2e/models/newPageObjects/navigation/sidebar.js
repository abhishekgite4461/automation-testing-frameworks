import * as client from '../../lib/client'
import * as shared from '../../lib/shared'
import * as overlayPO from '../../newPageObjects/components/overlay'

const assert = require('assert')

// identifiers
//-------------------------------
// main container identifier
const topNavMenuContainer = '.TopNavMenu'
// page element identifiers
const iconImageListContainer = '.TopNavMenu-socialLinks .ImageList'
const topNavMenuHide = '.TopNavMenu.is-open.hideinapp'
const topNavMenuOpen = '.TopNavMenu.is-open'
const burgerMenu = '.BurgerButton'
//-------------------------------

export const getSideBarContainer = () => {
  overlayPO.waitForOverlay()
  try {
    browser.waitForVisible(topNavMenuContainer)
    return browser.element(topNavMenuContainer)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] Top Navigation bar did not appear',
      err,
      'FAILED-TO-GET-SIDEMENU-CONTAINER'
    )
  }
}

export const openSidebar = () => {
  // this should only be opened if the menu is not already open
  if (browser.isVisible(topNavMenuHide) === false) {
    browser.waitForExist(burgerMenu)
    client.click(burgerMenu)
    browser.pause(500)
  }

  // wait until sidebar is displayed
  shared.isVisible('TopNavMenu', 'parentListBlock')
}

export const hasMenuItem = (element, selector, ignoreCase) => {
  const items = browser.getText(selector)
  let actual

  if (Array.isArray(items)) {
    actual =
      items.filter((item) => item.toLowerCase() === element.toLowerCase())[0] ||
      ''
  } else {
    actual = items
  }

  if (ignoreCase) {
    assert.equal(
      actual.toLowerCase(),
      element.toLowerCase(),
      `${actual.toLowerCase()} does not match menu item ${element.toLowerCase()}`
    )
  } else {
    assert.equal(
      actual,
      element,
      `${actual} does not match menu item ${element}`
    )
  }
}

export const selectACategory = (category) => {
  // verify side bar is open
  browser.waitForVisible(topNavMenuOpen)
  // verify that category link exist before clicking
  browser.waitForVisible(`=${category}`)
  browser.click(`=${category}`)
}

export const selectMenuAndSubMenu = (mainMenu, subMenu) => {
  browser.waitForVisible(topNavMenuOpen)
  browser.click(`button=${mainMenu}`)
  selectACategory(subMenu)
}

export const getSocialIconsVisible = () => {
  const container = getSideBarContainer()
  const icons = container.$(iconImageListContainer).$$('img')
  return icons.length
}
