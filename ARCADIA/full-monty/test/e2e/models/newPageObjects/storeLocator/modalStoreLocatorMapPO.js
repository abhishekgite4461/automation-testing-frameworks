/**
 * Created by colinmoore-hill on 16/01/2017.
 */

import * as client from '../../lib/client'
import * as shared from '../../lib/shared'
import * as overlayPO from '../../newPageObjects/components/overlay'

// identifiers
// -------------------------------
// Main container identifier
const modalStorelocator = '.Modal--storeLocator'
const mapContainer = '.StoreLocator-googleMapContainer '
const storeResultsContainer = '.StoreLocator-resultsContainer'

// page element identifiers
const assert = require('assert')
// Search Action
const modalTitle = '.CollectFromStore-title'
const searchCriteriaField = '#UserLocatorInput'
const resultList = '.CollectFromStore-storeList'
const predictList = '.UserLocatorInput-predictionsList'
// const predictListItem = '.UserLocatorInput-predictionsListItem'

const goButton = '.UserLocator-goButton'

// Map Display
const mapElem = '.GoogleMap-map'

// Result Listing
// const storeListing = '.CollectFromStore-storeList'
// const storelistItem = '.Store--collectFromStore'
const storelistItemAccordionTrigger = '.Accordion-icon'
// const storelistItemAccordion = '.Accordion-content '
const storelistItemAccordionOpen =
  '.Accordion Accordion--storeLocator  is-expanded'
// const storeListStatus = '.Store-success'

// Functions
//  - wait for containers
export const getMainContainer = () => {
  overlayPO.waitForOverlay()
  try {
    browser.waitForVisible(modalStorelocator)
    return browser.$(modalStorelocator)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] container did not appear',
      err,
      'FAILED-TO-GET-CONTAINER'
    )
  }
}
export const getMapContainer = () => {
  overlayPO.waitForOverlay()
  try {
    browser.waitForVisible(mapContainer)
    return browser.$(mapContainer)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] container did not appear',
      err,
      'FAILED-TO-GET-CONTAINER'
    )
  }
}
export const getResultContainer = () => {
  overlayPO.waitForOverlay()
  try {
    browser.waitForVisible(storeResultsContainer)
    return browser.$(storeResultsContainer)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] container did not appear',
      err,
      'FAILED-TO-GET-CONTAINER'
    )
  }
}

// get the modal title
export const getLocLookupTitle = () => {
  browser.waitForVisible(modalTitle)
  const productTitle = browser.getText(modalTitle)
  assert.ok(productTitle !== '', 'The product has no Title')
  assert.ok(
    productTitle === 'Where do you want to collect from?',
    'Title is not as expected'
  )
}

export const setSearch = (value) => {
  client.click('#UserLocatorInput')
  return client.keys(value)
}
// Enter location search criteria
export const triggerLocSearch = (loc) => {
  browser.waitForEnabled(searchCriteriaField, 5000)
  browser.setValue(searchCriteriaField, loc)
  browser.pause(2000)
  browser.waitForEnabled(predictList, 5000)
  console.log(
    browser.getText('.UserLocatorInput-predictionsListItem:nth-child(1) button')
  )
  browser.click('.UserLocatorInput-predictionsListItem:nth-child(1) button')
  browser.pause(1000)
  browser.click(goButton)
}

// Select First Store
export const getFirstResultLoc = () => {
  browser.waitForEnabled(resultList, 5000)
  const eley = browser.element('.Store--collectFromStore:nth-child(1)')
  eley.click('.Store-headerSelectButton')
}

export const getFirstPredictionLoc = () => {
  browser.waitForEnabled(predictList, 5000)
  const eley = browser.element(
    '.UserLocatorInput-predictionsListItem:nth-child(1)'
  )
  eley.click()
  browser.click(goButton)
}

export const verifyMapIsDisplayed = () => {
  let mapVisible = false
  browser.waitForVisible(mapElem)
  const res = browser.getElementSize(mapElem, 'height')
  if (res > 10) {
    mapVisible = true
  }
  assert.ok(mapVisible, 'MAP not being displayed.')
}

export const verifyMapIsNotDisplayed = () => {
  let mapVisible = false
  browser.waitForVisible(mapElem)
  const res = browser.getElementSize(mapElem, 'height')
  if (res < 10) {
    mapVisible = true
  }
  assert.ok(mapVisible, 'MAP not being displayed.')
}

// Working the Accordion
export const triggerTheAccordion = () => {
  browser.click(storelistItemAccordionTrigger)
}

export const verifyTheAccordionHasOpened = () => {
  const isOpen = browser.isVisible(storelistItemAccordionOpen)
  return isOpen
}
