/**
 * Created by colinmoore-hill on 24/03/2017.
 */

import * as shared from '../../lib/shared'
import * as overlayPO from '../../newPageObjects/components/overlay'

// identifiers
// -------------------------------
// Main container identifier
// const path = '/store-locator'
const storelocator = '.StoreLocator'
const mapContainer = '.StoreLocator-googleMapContainer '
const storeResultsContainer = '.StoreLocator-resultsContainer'

// page element identifiers
// const countrySelector ='#CountrySelect'
// const searchCriteriaBox ='.UserLocatorInput-inputField'

// Search Action
// Map Display
const mapMarker = '/div[1]/div/div/div/div/div[1]'
// Result Listing
// const storelistItem = '.Store--collectFromStore'
const storelistItemAccordionTrigger = '.Accordion-icon'

const storeName = '.Store-name'
const storeDist = '.Store-distance'
const storeNumber = '.Store-detailsSection span'
const storeWeekDays = '.Store-openingHoursRow span'
const storeOpeningHours = '.Store-openingHoursRow'
const accordionIcon = '.Accordion-icon'

// Functions
//  - wait for containers
export const getMainContainer = () => {
  overlayPO.waitForOverlay()
  try {
    browser.waitForVisible(storelocator)
    return browser.$(storelocator)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] container did not appear',
      err,
      'FAILED-TO-GET-CONTAINER'
    )
  }
}
export const getStoreResultsContainer = () => {
  overlayPO.waitForOverlay()
  try {
    browser.waitForVisible(storeResultsContainer)
    return browser.$(storeResultsContainer)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] container did not appear',
      err,
      'FAILED-TO-GET-CONTAINER'
    )
  }
}
// Select First Store
export const getFirstResultLoc = (ref = 1) => {
  const container = getStoreResultsContainer()
  const elem = `.Store:nth-child(${ref})`
  const eley = container.element(elem)
  eley.click(storelistItemAccordionTrigger)
}

export const getStoreName = (ref = 1) => {
  const container = getStoreResultsContainer()
  const elem = `.Store:nth-child(${ref})`
  const eley = container.element(elem).getText(storeName)
  return eley
}

export const getStoreDist = (ref = 1) => {
  const container = getStoreResultsContainer()
  const elem = `.Store:nth-child(${ref})`
  const eley = container.element(elem).getText(storeDist)
  return eley
}

export const getStoreNumber = (ref = 1) => {
  const container = getStoreResultsContainer()
  const elem = `.Store:nth-child(${ref})`
  const eley = container.element(elem).getText(storeNumber)[0]
  return eley
}

export const getStoreWeekdays = (ref = 1) => {
  const container = getStoreResultsContainer()
  const elem = `.Store:nth-child(${ref})`
  const eley = container.element(elem).getText(storeWeekDays)
  return eley[0]
}

export const getStoreOpenHours = (ref = 1) => {
  const container = getStoreResultsContainer()
  const elem = `.Store:nth-child(${ref})`
  let eley = container.element(elem).getText(storeOpeningHours)
  //
  eley = eley[1].slice(-11)
  return eley
}

export const clickAccordionIcon = (ref = 1) => {
  const container = getStoreResultsContainer()
  const elem = `.Store:nth-child(${ref})`
  container.element(elem).click(accordionIcon)
}

export const selectMapMarker = () => {
  browser.waitForEnabled(mapContainer, 5000)
  const mapElem = mapMarker
  const mark = browser.element(mapElem)
  mark.click()
}

export const setCountryStoreFinder = () => {}
