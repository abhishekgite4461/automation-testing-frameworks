/**
 * Created by colinmoore-hill on 20/12/2016.
 */
import * as overlayPO from '../components/overlay'
import * as shared from '../../lib/shared'
// identifier
// ------------------------------
// Main container identifier
const productQuickview = '.ProductQuickview'
// page element identifiers
const quickviewTitle = '.ProductQuickview-title'
const productpageLink = '.ProductQuickview-link'
const addtobagCTA = '.Button.AddToBag'
const addToBagViewBag = '.AddToBagConfirm-viewBag'
const addToBagCheckout = '.AddToBagConfirm-goToCheckout'
const miniShoppingBag = '.MiniBag-container'
const loginPage = '.Login'
const miniProductTitle = '.ShoppingBag .OrderProducts-productName'
const mainProductTitle = '.ProductDetail-title'
const sizeButtonContainerClass = '.ProductSizes-list'
const sizeButtonClass = '.ProductSizes-button'
const sizeDropdownId = '#productSizes'
const quickviewSizesValref = '.ProductSizes-item.is-active'
const selectCont = '#productQuantity'

//--------------------------------------------------------
export const getMainContainer = () => {
  overlayPO.waitForOverlay()
  try {
    browser.waitForVisible(productQuickview)
    return browser.$(productQuickview)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE - productQuickview] container did not appear',
      err,
      'FAILED-TO-GET-CONTAINER'
    )
  }
}
export const getProductTitle = () => {
  getMainContainer()
  const productTitle = browser.getText(quickviewTitle)
  return productTitle
}
export const getProductSize = () => {
  const quickviewSizesVal = browser.getText(quickviewSizesValref)
  return quickviewSizesVal
}

export const setProductSize = () => {
  const container = getMainContainer()

  switch (true) {
    case container.isVisible(sizeDropdownId):
      container.selectByIndex(sizeDropdownId, 1)
      process.env.prodSize = container.getValue(sizeDropdownId)
      break
    case container.isVisible(sizeButtonContainerClass):
      container.click(sizeButtonClass)
      process.env.prodSize = container.getText(sizeButtonClass)
      break
    default:
      console.log(
        '[setProductSize] unexpected scenario, skipping size selection'
      )
      break
  }
}

export const getProductQty = () => {
  const quickviewQtyVal = browser.getAttribute(
    '.Select-select',
    'aria-activedescendant'
  )
  return quickviewQtyVal
}
export const setProductQty = () => {
  const selectBox = browser.element(selectCont)
  const numIdx = 5
  selectBox.selectByIndex(numIdx)
}
export const clickProductPageLink = () => {
  browser.waitForVisible(productpageLink)
  browser.click(productpageLink)
}
export const verifyURLisAsExpected = () => {
  browser.waitForVisible(mainProductTitle)
  const productTitle = browser.getText(mainProductTitle)
  return productTitle
}
export const clickAddToBag = () => {
  browser.waitForVisible(addtobagCTA)
  const productTitle = browser.getText(quickviewTitle)
  browser.click(addtobagCTA)
  return productTitle
}
export const selectViewItemOpt = () => {
  browser.waitForVisible(addToBagViewBag)
  browser.click(addToBagViewBag)
}
export const selectCheckoutOpt = () => {
  const container = getMainContainer()
  container.waitForVisible(addToBagCheckout)
  container.click(addToBagCheckout)
}
export const checkShoppingBagVisible = () => {
  browser.waitForVisible(miniShoppingBag)
  const ShoppingBagVisible = browser.isVisible(miniShoppingBag)
  const miniProductName = browser.getText(miniProductTitle)
  console.log(`returning ${ShoppingBagVisible}${miniProductName}`)
  const responce = [ShoppingBagVisible, miniProductName]
  return responce
}
export const checkLoginPageVisible = () => {
  browser.waitForVisible(loginPage)
  const loginPageVisible = browser.isVisible(loginPage)
  return loginPageVisible
}
export const verifyOrderInShoppingBag = () => {
  browser.waitForVisible(miniShoppingBag)
  const miniProductOrder = browser.getText('.OrderProducts-label')
  return miniProductOrder
}
