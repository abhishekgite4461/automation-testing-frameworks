import * as orderConfirmPO from '../checkout/SummaryConfirmBase_po'
import * as overlayPO from '../components/overlay'
import * as shared from '../../lib/shared'

const assert = require('assert')

// identifiers
//-------------------------------
// Main container identifier
const orderCompleteContainerClass = '.SummaryConfirmation'
// page element identifiers
const reviewAndConfirmButtonClass = '.Button.SummaryConfirmation-paynow'
const errorMessageClass = '.Message.is-shown.is-error'
// CHECKBOX has problems around selecting, so we click labels and inspect the checkbox for knowing if it is selected, this is a wdio issue
const acceptTermsAndConditionCheckbox = '.Checkbox .Checkbox-field'
const acceptTermsAndConditionsCheckboxClass = '.Checkbox-field + span'
// delivery type option identifiers

//-------------------------------

export const getOrderCompleteContainer = () => {
  const summaryConfirm = orderConfirmPO.getSummaryConfirmContainer()
  try {
    summaryConfirm.waitForVisible(orderCompleteContainerClass, 10000)
    overlayPO.waitForOverlay()
    return summaryConfirm.element(orderCompleteContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] summary confirmation button and t and c container did not appear',
      err,
      'FAILED-TO-GET-ORDER-COMPLETE-CONTAINER'
    )
  }
}

export const summaryPagePresent = () => {
  return browser.isVisible(orderCompleteContainerClass)
}

export const confirmAndPayButtonEnabled = () => {
  const confirmationContainer = getOrderCompleteContainer()
  const result = confirmationContainer.isEnabled(reviewAndConfirmButtonClass)
  console.log(`[CHECKOUT PO INFO] confirm and pay button enabled?: ${result}`)
  return result
}

export const termsAndConditionsSelected = () => {
  const confirmationContainer = getOrderCompleteContainer()
  const result = confirmationContainer.isSelected(
    acceptTermsAndConditionCheckbox
  )
  console.log(`[CHECKOUT PO INFO] t&c selected?: ${result}`)
  return result
}

export const acceptTermsAndConditions = () => {
  const confirmationContainer = getOrderCompleteContainer()
  if (termsAndConditionsSelected() !== true) {
    overlayPO.waitForOverlay()
    confirmationContainer.click(acceptTermsAndConditionsCheckboxClass)
    overlayPO.waitForOverlay()
  }
}

export const clickConfirmAndPay = () => {
  const confirmationContainer = getOrderCompleteContainer()

  // make sure there are no overlays
  overlayPO.waitForOverlay()

  if (confirmAndPayButtonEnabled() === true) {
    confirmationContainer.click(reviewAndConfirmButtonClass)
  } else {
    console.log(
      '[PO ERROR AVERSION] button was not enabled, probably because the terms and conditions was not checked, going to check and set that and try again'
    )
    acceptTermsAndConditions()
    confirmationContainer.click(reviewAndConfirmButtonClass)
  }

  overlayPO.waitForOverlay()
}

export const getErrorMessage = () => {
  const confirmationContainer = getOrderCompleteContainer()
  confirmationContainer.waitForVisible(errorMessageClass)
  const result = confirmationContainer.getText(errorMessageClass)
  console.log(`[SUMMARY PO INFO] got error text: ${result}`)
  return result
}

export const waitForConfirmAndPayButtonEnabled = () => {
  let count = 0
  while (confirmAndPayButtonEnabled() === false && count < 200) {
    console.log(
      '[SUMMARY PO INFO] waiting for order pay now button to be enabled'
    )
    browser.pause(50)
    count++
  }
  if (count === 200) {
    assert.ok(
      false,
      'Pay now button never appeared to be enabled, check screen shot for more info'
    )
  }
}

export const deleteItemFromBag = () => {
  console.log('Ready to deleteItemFromBag')
  let items = browser.getText(
    '.OrderProducts-product .OrderProducts-media .OrderProducts-editContainer  .OrderProducts-deleteText'
  )
  browser.click('.OrderProducts-deleteIcon')
  overlayPO.waitForOverlay()
  browser.waitForVisible('.OrderProducts-modal')
  browser.click('.OrderProducts-deleteButton')
  overlayPO.waitForOverlay()
  // browser.waitForVisible('.OrderProducts-modal')
  browser.click('.OrderProducts-modal .Button')
  browser.waitForVisible('.OrderProducts')
  let res = false
  try {
    // Verify you cant see delete option in product listing when last in list.
    items = browser.getText(
      '.OrderProducts-product .OrderProducts-media .OrderProducts-editContainer .OrderProducts-deleteText'
    )
    console.log(`Icon Should not be here !${items}`)
  } catch (e) {
    console.log('Icon removed as expected')
    res = true
  }
  assert.ok(
    res,
    'Delete option availible on last option in bag on Summary Page'
  )
}
