import * as modalPO from '../components/overlay'
import * as shared from '../../lib/shared'

// identifiers
//-------------------------------
// Main container identifier
const giftCardContainerClass = '.GiftCardContainer'
// page element identifiers

//-------------------------------

export const getGiftCardContainer = () => {
  modalPO.waitForOverlay()
  try {
    browser.waitForVisible(giftCardContainerClass, 10000)
    modalPO.waitForOverlay()
    return browser.element(giftCardContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] gift card page container did not appear',
      err,
      'FAILED-TO-GET-GIFTCARD-CONTAINER'
    )
  }
}
