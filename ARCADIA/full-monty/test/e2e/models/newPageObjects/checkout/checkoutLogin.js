import * as shoppingBag from '../../pageObjects/shoppingBag'
import * as myAccount from '../../pageObjects/myAccount'
import * as login from '../../newPageObjects/login_po'
import * as overlayPO from '../../newPageObjects/components/overlay'
import * as header from '../../newPageObjects/components/topbar_po'
import * as bag from '../../newPageObjects/shoppingBag/ShoppingBagBase'

export const checkoutAsGuest = () => {
  header.openShoppingBag()
  bag.clickCheckout()
  login.clickProceed()
}

export const checkout = () => {
  header.openShoppingBag()
  bag.clickCheckout()
  overlayPO.waitForOverlay()
}

export const checkoutAsUser = (username, password) => {
  header.openShoppingBag()
  bag.clickCheckout()
  myAccount.login(username, password)
}

export const checkoutAsLoggedInToBillingPage = () => {
  checkout()
  shoppingBag.enterDefaultDeliveryInfo()
}
