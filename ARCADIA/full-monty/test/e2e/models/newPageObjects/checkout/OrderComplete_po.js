import * as overlayPO from '../components/overlay'

// identifiers
//-------------------------------
// Main container identifier
const orderCompleteContainerClass = '.SummaryCompleted'
// page element identifiers
const completeHeaderClass = '.SummaryCompleted-header'
const continueShoppingButtonClass = '.Button'
//-------------------------------
export const getOrderCompleteContainer = () => {
  overlayPO.waitForOverlay()
  browser.pause(3000)
  overlayPO.waitForOverlay()
  browser.waitForVisible(orderCompleteContainerClass, 10000)
  overlayPO.waitForOverlay()
  return browser.element(orderCompleteContainerClass)
}

export const waitForOrderComplete = () => {
  overlayPO.waitForOverlay()
  getOrderCompleteContainer()
}

export const getThankYouMessage = () => {
  const orderComplete = getOrderCompleteContainer()
  orderComplete.waitForVisible(completeHeaderClass, 10000)
  const message = orderComplete.getText(completeHeaderClass)
  return message
}

export const clickContinueShopping = () => {
  const orderComplete = getOrderCompleteContainer()
  orderComplete.click(continueShoppingButtonClass)
}
