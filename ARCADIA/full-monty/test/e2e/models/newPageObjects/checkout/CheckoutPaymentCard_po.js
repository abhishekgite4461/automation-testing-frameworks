import * as billingPO from '../checkout/BillingBase_po'
import * as overlayPO from '../components/overlay'
import * as shared from '../../lib/shared'
import * as client from '../../lib/client'

// identifiers
//-------------------------------
// Main container identifier
const cardDetailsContainerClass = '.CardDetails'
// page element identifiers
const cardTypeSelectId = '#paymentType'
const cardTypeRadioId = '#payment-type-'
const cardNumberInputClass = '.Input-field.Input-field-cardNumber'
const expiryMonthSelectId = '#expiryMonth'
const expiryYearSelectId = '#expiryYear'
const cvvInputClass = '.Input-field.Input-field-cvv'
//-------------------------------

export const getPaymentContainer = () => {
  overlayPO.waitForOverlay()
  try {
    browser.waitForVisible(cardDetailsContainerClass, 10000)
    overlayPO.waitForOverlay()
    return browser.element(cardDetailsContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] billing page container did not appear',
      err,
      'FAILED-TO-GET-PAYMENT-CONTAINER'
    )
  }
}

export const setPaymentTypebyValue = (paymentTypeValue) => {
  const paymentContainer = getPaymentContainer()
  if (['VISA', 'AMEX', 'MCARD', 'SWTCH'].includes(paymentTypeValue))
    paymentTypeValue = 'CARD'
  if (['ACCOUNT', 'STORECARD'].includes(paymentTypeValue))
    paymentTypeValue = 'ACCNT'

  if (client.hasDesktopLayout()) {
    paymentContainer.click(
      `${cardTypeRadioId + paymentTypeValue.toLowerCase()} + span`
    )
  } else {
    paymentContainer.selectByValue(cardTypeSelectId, paymentTypeValue)
  }
  overlayPO.waitForOverlay()
}

export const enterCardNumber = (cardNumber) => {
  const paymentContainer = getPaymentContainer()
  paymentContainer.setValue(cardNumberInputClass, cardNumber)
}

export const setExpiryMonth = (expMonth) => {
  const paymentContainer = getPaymentContainer()
  paymentContainer.selectByValue(expiryMonthSelectId, expMonth)
}

export const setExpiryYear = (expYear) => {
  const paymentContainer = getPaymentContainer()
  paymentContainer.selectByValue(expiryYearSelectId, expYear)
}

export const enterCVV = (cvv) => {
  const paymentContainer = getPaymentContainer()
  paymentContainer.setValue(cvvInputClass, cvv)
}

export const setPaymentDetails = (
  cardType,
  cardNumber,
  cvv,
  expMonth = 12,
  expYear = 2020
) => {
  console.log(
    `[PO INFO] using card details: ${cardType} ${cardNumber} ${expMonth} ${expYear} ${cvv}`
  )
  setPaymentTypebyValue(cardType)
  if (
    ['VISA', 'AMEX', 'MCARD', 'SWTCH', 'ACCOUNT', 'STORECARD'].includes(
      cardType
    )
  ) {
    enterCardNumber(cardNumber)
    setExpiryMonth(expMonth)
    setExpiryYear(expYear)
    enterCVV(cvv)
  }
  billingPO.clickReviewAndConfirm()
}

export const clickReviewAndConfirm = () => {
  billingPO.clickReviewAndConfirm()
}

export const setPaymentTypebyText = (paymentTypeText) => {
  const paymentContainer = getPaymentContainer()
  paymentContainer.selectByText(cardTypeSelectId, paymentTypeText)
  overlayPO.waitForOverlay()
}

export const getPaymentOptions = () => {
  const paymentContainer = getPaymentContainer()
  console.log(paymentContainer.getText(cardTypeSelectId))
  return paymentContainer.getText(cardTypeSelectId)
}
