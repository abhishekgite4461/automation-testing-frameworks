import * as deliveryBasePO from '../checkout/DeliveryBase_po'
import * as overlayPO from '../components/overlay'

// identifiers
//-------------------------------
// Main container identifier
const deliveryInstructionsContainerClass = '.DeliveryInstructions'
// page element identifiers
// delivery option identifiers - radio options can be inspected but you have to click the span because of wdio z index reading issues

// delivery type option identifiers

//-------------------------------

// delivery options

export const getDeliveryInstructionsContainer = () => {
  const deliveryContainer = deliveryBasePO.getDeliveryContainer()
  deliveryContainer.waitForVisible(deliveryInstructionsContainerClass)
  overlayPO.waitForOverlay()
  return deliveryContainer.element(deliveryInstructionsContainerClass)
}
