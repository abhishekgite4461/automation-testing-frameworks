import * as shared from '../../lib/shared'
import * as overlay from '../components/overlay'
import * as client from '../../lib/client'

// identifiers
//-------------------------------
// Main container identifier
const deliveryContainerClass = '.DeliveryContainer'
// page element identifiers
const titleSelectListId = '#title'
const firstNameTextFieldClass = '.Input-field.Input-field-firstName'
const surnameNameTextFieldClass = '.Input-field.Input-field-lastName'
const mobileTelephoneTextFieldClass =
  '.YourDetails-telephone.left-col-margin .Input-field.Input-field-telephone'
const desktopTelephoneTextFieldClass =
  '.FindAddress-telephone.left-col-margin .Input-field.Input-field-telephone'
const nextButtonClass = '.Button.DeliveryContainer-nextButton'

const telephoneTextFieldClass = client.hasDesktopLayout()
  ? desktopTelephoneTextFieldClass
  : mobileTelephoneTextFieldClass
//-------------------------------

export const getDeliveryContainer = () => {
  overlay.waitForOverlay()

  try {
    browser.waitForVisible(deliveryContainerClass)
    // nb adding more wait as overlay still appears every now and then
    overlay.waitForOverlay()
    return browser.element(deliveryContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] delivery page container did not appear to be on the page',
      err,
      'FAILED-TO-GET-DELIVERY-CONTAINER'
    )
  }
}

export const deliveryCheckoutPagePresent = () => {
  const container = getDeliveryContainer()
  return container.isVisible(titleSelectListId, 5000)
}

export const selectTitle = (title) => {
  const container = getDeliveryContainer()
  container.selectByValue(titleSelectListId, title)
}

export const getTitles = () => {
  const container = getDeliveryContainer()
  const result = container.getText(titleSelectListId)
  const formatted = result.split('\n')
  return formatted
}

export const enterDeliveryFirstName = (firstName) => {
  const container = getDeliveryContainer()
  process.env.BASE_FIRSTNAME = firstName
  container.setValue(firstNameTextFieldClass, firstName)
}

export const enterDeliverySurname = (surname) => {
  const container = getDeliveryContainer()
  process.env.BASE_LASTNAME = surname
  container.setValue(surnameNameTextFieldClass, surname)
}

export const enterDeliveryPhoneNumber = (phoneNumber) => {
  const container = getDeliveryContainer()
  container.setValue(telephoneTextFieldClass, phoneNumber)
}

export const clickNextButton = () => {
  const container = getDeliveryContainer()
  // nb: wait for overlay again as it reappears occasionally
  overlay.waitForOverlay()
  container.click(nextButtonClass)
  overlay.waitForOverlay()
}

export const enterDeliveryDetails = (title, firstName, surname, telephone) => {
  selectTitle(title)
  enterDeliveryFirstName(firstName)
  enterDeliverySurname(surname)
  enterDeliveryPhoneNumber(telephone)
}
