import * as overlayPO from '../components/overlay'
import * as shared from '../../lib/shared'

// identifiers
//-------------------------------
// Main container identifier
const summaryConfirmContainerClass = '.SummaryContainer'
// page element identifier - My Bag
const productContainerClass = '.OrderProducts-product'
const productNameClass = '.OrderProducts-productName'
const productTotalPriceClass = '.OrderProducts-productSubtotal .Price'
const totalPriceClass = '.Totals-total .Totals-amount .Totals-bold'

//-------------------------------

export const getSummaryConfirmContainer = () => {
  overlayPO.waitForOverlay()
  try {
    //  waitForSummaryConfirmContainer()
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      'summary confirmation container never appeared on the screen',
      err,
      'FAILED-TO-GET-SUMMARY-CONFIRM-CONTAINER'
    )
  }
  overlayPO.waitForOverlay()
  return browser.element(summaryConfirmContainerClass)
}

export const waitForSummaryConfirmContainer = () => {
  browser.waitForVisible(summaryConfirmContainerClass, 10000)
}

export const getSummaryText = () => {
  return browser.getText(summaryConfirmContainerClass)
}

export const getProducts = () => {
  const summaryContainer = getSummaryConfirmContainer()
  return summaryContainer.elements(productContainerClass)
}

export const getProductName = () => {
  const summaryContainer = getSummaryConfirmContainer()
  return summaryContainer.getText(productNameClass)
}

export const getAllProductPrice = () => {
  const summaryContainer = getSummaryConfirmContainer()
  const productPrices = summaryContainer.getText(productTotalPriceClass)
  return Array.isArray(productPrices) ? productPrices : [productPrices]
}

export const getTotalPrice = () => {
  const summaryContainer = getSummaryConfirmContainer()
  return summaryContainer.getText(totalPriceClass)
}
