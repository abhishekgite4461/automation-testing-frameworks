import * as deliveryBasePO from '../checkout/DeliveryBase_po'
import * as overlayPO from '../components/overlay'

// identifiers
//-------------------------------
// Main container identifier
const deliveryOptionsContainerClass = '.DeliveryContainer-location'
const deliveryTypeContainerClass = '.DeliveryType'
// page element identifiers
// delivery option identifiers - radio options can be inspected but you have to click the span because of wdio z index reading issues
const homeDeliveryOptionId = '#delivery-option-home'
const homeDeliveryOptionSpanId = '#delivery-option-home + span'
const collectFromStoreOptionId = '#delivery-option-store'
const collectFromStoreOptionSpanId = '#delivery-option-store + span'
const collectFromParcelShopOptionId = '#delivery-option-parcel'
const collectFromParcelShopOptionSpanId = '#delivery-option-parcel + span'
// delivery type options
const standardDeliveryTypeId = '#delivery-option-home_standard'
const standardDeliveryTypeSpanId = '#delivery-option-home_standard + span'
const expressDeliveryTypeId = '#delivery-option-home_express'
const expressDeliveryTypeSpanId = '#delivery-option-home_express + span'
const expressDeliveryDaySelectClass = '.ExpressDeliveryOptions .Select-select'
const expressDeliveryDaySelectContainer = '.ExpressDeliveryOptions'
// CollectFrom Store options
const collectFromStoreStandard = '#delivery-option-store_standard + span'
const collectFromStoreExpress = '#delivery-option-store_express + span'

// delivery type option identifiers
const deliveryOptionPrice = '.DeliveryType-price'
// collect from store - user locator
const cfsStoreSearchContainerClass = '.UserLocatorInput'
const cfsStoreSearchInputId = '#UserLocatorInput'
const cfsStoreSearchButtonClass = '.Button.UserLocator-goButton'
const cfsStorepredictionsItemClass = '.UserLocatorInput-predictionsListItem'

//-------------------------------

// delivery options

export const getDeliveryOptionsContainer = () => {
  const deliveryContainer = deliveryBasePO.getDeliveryContainer()
  deliveryContainer.waitForVisible(deliveryOptionsContainerClass)
  overlayPO.waitForOverlay()
  return deliveryContainer.element(deliveryOptionsContainerClass)
}

export const getDeliveryTypeContainer = () => {
  const deliveryContainer = deliveryBasePO.getDeliveryContainer()
  deliveryContainer.waitForVisible(deliveryTypeContainerClass)
  overlayPO.waitForOverlay()
  return deliveryContainer.element(deliveryTypeContainerClass)
}

// page interactions
export const getActiveDeliveryPrice = () => {
  const deliveryContainer = getDeliveryOptionsContainer()
  return deliveryContainer.getText(deliveryOptionPrice)
}

export const deliveryPriceVisible = () => {
  const deliveryContainer = getDeliveryOptionsContainer()
  return deliveryContainer.isVisible(deliveryOptionPrice)
}

export const homeDeliveryOptionSelected = () => {
  const deliveryOption = getDeliveryOptionsContainer()
  return deliveryOption.isSelected(homeDeliveryOptionId)
}

export const expressDeliveryOptionsVisible = () => {
  return browser.isVisible(expressDeliveryDaySelectContainer)
}

export const getExpressDeliveryOption = () => {
  return browser.getValue(expressDeliveryDaySelectClass)
}

export const setHomeDeliveryOption = () => {
  const deliveryOption = getDeliveryOptionsContainer()
  if (homeDeliveryOptionSelected() !== true) {
    deliveryOption.click(homeDeliveryOptionSpanId)
    overlayPO.waitForOverlay()
  }
}

export const collectFromStoreOptionSelected = () => {
  const deliveryOption = getDeliveryOptionsContainer()
  return deliveryOption.isSelected(collectFromStoreOptionId)
}

export const collectFromParcelShopOptionSelected = () => {
  const deliveryOption = getDeliveryOptionsContainer()
  return deliveryOption.isSelected(collectFromParcelShopOptionId)
}

export const collectFromStoreOptionExpressSelected = () => {
  const deliveryOption = getDeliveryOptionsContainer()
  return deliveryOption.isSelected(collectFromStoreExpress)
}

export const setCollectFromStoreDeliveryTypeOption = () => {
  const deliveryOption = getDeliveryOptionsContainer()
  if (collectFromStoreOptionSelected() !== true) {
    deliveryOption.click(collectFromStoreOptionSpanId)
    overlayPO.waitForOverlay()
  }
}

export const setCollectFromStoreDeliveryTypeStandard = () => {
  const deliveryOption = getDeliveryOptionsContainer()
  if (collectFromStoreOptionSelected() !== true) {
    deliveryOption.click(collectFromStoreStandard)
    overlayPO.waitForOverlay()
  }
}

export const setCollectFromStoreDeliveryTypeExpress = () => {
  const deliveryOption = getDeliveryOptionsContainer()
  if (collectFromStoreOptionExpressSelected() !== true) {
    deliveryOption.click(collectFromStoreExpress)
    overlayPO.waitForOverlay()
  }
}

export const setCollectFromParcelShopDeliveryTypeOption = () => {
  const deliveryOption = getDeliveryOptionsContainer()
  if (collectFromParcelShopOptionSelected() !== true) {
    deliveryOption.click(collectFromParcelShopOptionSpanId)
    overlayPO.waitForOverlay()
  }
}

export const setCollectFromStoreDeliveryTypeOptionExpress = () => {
  const deliveryOption = getDeliveryOptionsContainer()
  if (collectFromStoreOptionSelected() !== true) {
    deliveryOption.click(collectFromStoreExpress)
    overlayPO.waitForOverlay()
  }
}

export const setCollectFromStoreDeliveryTypeOptionStandard = () => {
  const deliveryOption = getDeliveryOptionsContainer()
  if (collectFromStoreOptionSelected() !== true) {
    deliveryOption.click(collectFromStoreStandard)
    overlayPO.waitForOverlay()
  }
}
// delivery Type

export const standardDeliveryTypeSelected = () => {
  const deliveryOption = getDeliveryTypeContainer()
  return deliveryOption.isSelected(standardDeliveryTypeId)
}

export const setStandardDeliveryType = () => {
  const deliveryOption = getDeliveryTypeContainer()
  if (standardDeliveryTypeSelected() !== true) {
    deliveryOption.click(standardDeliveryTypeSpanId)
    overlayPO.waitForOverlay()
  }
}

export const expressDeliveryTypeSelected = () => {
  const deliveryOption = getDeliveryTypeContainer()
  return deliveryOption.isSelected(expressDeliveryTypeId)
}

export const setExpressDeliveryType = () => {
  const deliveryOption = getDeliveryTypeContainer()
  if (expressDeliveryTypeSelected() !== true) {
    deliveryOption.click(expressDeliveryTypeSpanId)
    overlayPO.waitForOverlay()
    // wait until nominated day delivery option is visible
    deliveryOption.waitForVisible(expressDeliveryDaySelectClass, 5000)
  }
}

export const setExpressDeliveryDayOption = (index) => {
  const deliveryOption = getDeliveryTypeContainer()
  if (deliveryOption.isVisible(expressDeliveryDaySelectClass) === false) {
    setExpressDeliveryType()
  }

  // store selected delivery date
  process.env.EXPRESS_DELIVERY_DATE = deliveryOption
    .getText(`${expressDeliveryDaySelectClass}`)
    .split('\n')[index]
  deliveryOption.selectByIndex(expressDeliveryDaySelectClass, index)

  overlayPO.waitForOverlay()
}

// Collect from store
export const getCFSStoreSearchContainer = () => {
  const deliveryContainer = deliveryBasePO.getDeliveryContainer()
  deliveryContainer.waitForVisible(cfsStoreSearchContainerClass)
  overlayPO.waitForOverlay()
  return deliveryContainer.element(cfsStoreSearchContainerClass)
}

export const setCFSlocation = (city) => {
  const cfsSearchContainer = getCFSStoreSearchContainer()
  overlayPO.waitForOverlay()
  cfsSearchContainer.waitForVisible(cfsStoreSearchInputId)
  cfsSearchContainer.setValue(cfsStoreSearchInputId, city)
}

export const clickCFSSearchButton = () => {
  const cfsSearchContainer = getCFSStoreSearchContainer()
  cfsSearchContainer.click(cfsStoreSearchButtonClass)
  overlayPO.waitForOverlay()
}

export const selectCFSPredictionLocation = () => {
  const cfsSearchContainer = getCFSStoreSearchContainer()

  cfsSearchContainer.waitForVisible(cfsStorepredictionsItemClass)

  // select the first suggested location
  cfsSearchContainer.click(cfsStorepredictionsItemClass)
}

export const getCFSSearchLocation = () => {
  const cfsSearchContainer = getCFSStoreSearchContainer()
  return cfsSearchContainer.getValue(cfsStoreSearchInputId)
}

export const collectFromStoreStandardSelected = () => {
  const deliveryOption = getDeliveryOptionsContainer()
  return deliveryOption.isSelected(collectFromStoreStandard)
}
export const collectFromStoreExpressSelected = () => {
  const deliveryOption = getDeliveryOptionsContainer()
  return deliveryOption.isSelected(collectFromStoreExpress)
}

export const setCollectFromStoreStandard = () => {
  const deliveryOption = getDeliveryOptionsContainer()
  if (collectFromStoreStandardSelected() !== true) {
    deliveryOption.click(collectFromStoreStandard)
    overlayPO.waitForOverlay()
  }
}

export const setCollectFromStoreExpress = () => {
  const deliveryOption = getDeliveryOptionsContainer()
  if (collectFromStoreExpressSelected() !== true) {
    deliveryOption.click(collectFromStoreExpress)
    overlayPO.waitForOverlay()
  }
}
