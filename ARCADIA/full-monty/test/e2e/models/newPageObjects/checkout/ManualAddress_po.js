import * as deliveryFindAddressPO from '../checkout/FindAddress_po'
import * as overlay from '../components/overlay'

// identifiers
//-------------------------------
// Main container identifier
const manualAddressContainerClass = '.YourAddress-form'
const enterAddressManuallyLinkClass = '.FindAddress-link'
// page element identifiers
const addressLine1InputClass = '.Input-field.Input-field-address1'
const addressLine2InputClass = '.Input-field.Input-field-address2'
const postcodeInputClass = '.Input-field.Input-field-postcode'
const cityInputClass = '.Input-field.Input-field-city'
const clearFormLinkClass = '.YourAddress-link.YourAddress-link--left'
const findAddressLinkClass = '.YourAddress-link.YourAddress-link--right'

//-------------------------------

export const getManualAddressContainer = () => {
  overlay.waitForOverlay()
  if (browser.isVisible(enterAddressManuallyLinkClass)) {
    browser.waitForVisible(enterAddressManuallyLinkClass)
    browser.click(enterAddressManuallyLinkClass)
    overlay.waitForOverlay()
    console.log(
      '[PO INFO] manual address form was not present so clicking link to bring it up'
    )
  }
  browser.waitForVisible(manualAddressContainerClass, 10000)
  overlay.waitForOverlay()
  return browser.element(manualAddressContainerClass)
}

export const getManualAddressLinkText = () => {
  overlay.waitForOverlay()
  return browser.getText(enterAddressManuallyLinkClass)
}

export const selectDeliveryCountry = (country) => {
  deliveryFindAddressPO.selectDeliveryCountry(country)
  process.env.BASE_COUNTRY = country
}

export const enterAddressLine1 = (value) => {
  const manualAddressForm = getManualAddressContainer()
  process.env.BASE_LINE1 = value
  manualAddressForm.setValue(addressLine1InputClass, value)
}

export const enterAddressLine2 = (value) => {
  const manualAddressForm = getManualAddressContainer()
  process.env.BASE_LINE2 = value
  manualAddressForm.setValue(addressLine2InputClass, value)
}

export const enterPostCode = (value) => {
  const manualAddressForm = getManualAddressContainer()
  process.env.BASE_POSTCODE = value
  manualAddressForm.setValue(postcodeInputClass, value)
}

export const enterCityTown = (value) => {
  const manualAddressForm = getManualAddressContainer()
  process.env.BASE_CITY = value
  manualAddressForm.setValue(cityInputClass, value)
}

export const enterManualAddress = (country, line1, line2, postcode, city) => {
  selectDeliveryCountry(country)
  enterAddressLine1(line1)
  enterAddressLine2(line2)
  enterPostCode(postcode)
  enterCityTown(city)
}

export const clickManualAddressLink = () => {
  overlay.waitForOverlay()
  browser.element(enterAddressManuallyLinkClass).click()
  overlay.waitForOverlay()
}

export const clearFormlinkVisible = () => {
  const manualAddressForm = getManualAddressContainer()
  return manualAddressForm.isVisible(clearFormLinkClass)
}

export const clickClearFormlink = () => {
  const manualAddressForm = getManualAddressContainer()
  manualAddressForm.click(clearFormLinkClass)
}

export const findAddresslinkVisible = () => {
  const manualAddressForm = getManualAddressContainer()
  return manualAddressForm.isVisible(findAddressLinkClass)
}

export const clickFindAddressFormlink = () => {
  const manualAddressForm = getManualAddressContainer()
  manualAddressForm.click(findAddressLinkClass)
  overlay.waitForOverlay()
}
