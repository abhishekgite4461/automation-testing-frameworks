import * as overlay from '../components/overlay'
import * as shared from '../../lib/shared'

const assert = require('assert')

// identifiers
//-------------------------------
// Main container identifier
const findAddressContainerClass = '.FindAddress'
const findAddresslinkClass = '.YourAddress-link.YourAddress-link--right'
// page element identifiers
const selectCountrySelectId = '#country'
const findAddressPostcodeTextFieldClass = '.Input-field.Input-field-postCode'
const findAddressHouseNumberClass = '.Input-field.Input-field-houseNumber'
const findAddressButtonSelector = '.ButtonContainer-findAddress .Button'
const findAddressResultsId = '#findAddress'
// delivery option identifiers - radio options can be inspected but you have to click the span because of wdio z index reading issues

//-------------------------------

export const getFindAddressContainer = () => {
  try {
    browser.waitForVisible(findAddressContainerClass)
    overlay.waitForOverlay()

    if (browser.isVisible(findAddresslinkClass)) {
      overlay.waitForOverlay()
      browser.click(findAddresslinkClass)
      console.log(
        '[PO INFO] find address was not present so clicking the link to being it forward'
      )
    }

    return browser.element(findAddressContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[PO ERROR AVERSION] Find Address form were not present',
      err,
      'FAILED-TO-GET-FIND-ADDRESS-CONTAINER'
    )
  }
}

export const enterPostcode = (postcode) => {
  const findAddressForm = getFindAddressContainer()
  process.env.BASE_POSTCODE = postcode
  findAddressForm.setValue(findAddressPostcodeTextFieldClass, postcode)
}

export const enterHouseNumber = (houseNumber) => {
  const findAddressForm = getFindAddressContainer()
  findAddressForm.setValue(findAddressHouseNumberClass, houseNumber)
}

export const clickFindAddressButton = () => {
  // no need to click the link, fetching the form checks and clicks link if present
  const findAddressForm = getFindAddressContainer()
  findAddressForm.click(findAddressButtonSelector)
  overlay.waitForOverlay()
}

export const selectDeliveryCountry = (country) => {
  const findAddressForm = getFindAddressContainer()
  findAddressForm.selectByValue(selectCountrySelectId, country)
  // nb: overlay will display onChange, wait for that to end before continuing
  overlay.waitForOverlay()
}

export const findAddress = (postcode, housenumber, country) => {
  if (country !== null) {
    selectDeliveryCountry(country)
  }
  enterPostcode(postcode)
  enterHouseNumber(housenumber)
  clickFindAddressButton()
}

export const getSelectedDeliveryCountry = () => {
  const findAddressForm = getFindAddressContainer()
  return findAddressForm.getAttribute(
    selectCountrySelectId,
    'aria-activedescendant'
  )
}

export const findAddressDisabled = () => {
  const findAddressForm = getFindAddressContainer()
  assert.ok(
    findAddressForm.isEnabled(findAddressButtonSelector) === false,
    'Find address button should be disabled'
  )
}

export const getFindAddressResults = () => {
  const findAddressForm = getFindAddressContainer()
  const results = findAddressForm.getText(findAddressResultsId)
  console.log(
    `[PO INFO] Got the following results from the find address results: ${results}`
  )
  return findAddressForm.getText(findAddressResultsId)
}
