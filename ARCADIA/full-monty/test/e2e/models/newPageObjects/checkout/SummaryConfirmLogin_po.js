import * as orderConfirmPO from '../checkout/SummaryConfirmBase_po'
import * as summaryConfirmCheckout from '../checkout/CheckoutSummaryConfirmation_po'
import * as overlayPO from '../components/overlay'
import * as shared from '../../lib/shared'

const assert = require('assert')

// identifiers
//-------------------------------
// Main container identifier
const orderCompleteContainerClass = '.CheckoutSummaryAccount'
const forgottenPasswordContainer = '.ForgetPassword'
// page element identifiers
const emailInputClass = '.Input-field.Input-field-email'
const verifyEmailButtonClass = '.Button'
const passwordInputClass = '.Input-field.Input-field-password'
const passConfirmInputClass = '.Input-field.Input-field-passwordConfirm'
const newsletterSubCheckboxClass = '.Checkbox-field'
const inputVaidateSuccessClass = '.Input-validateIcon.Input-validateSuccess'
const tryAnoutherEmailLinkClass = '.CheckoutSummaryAccount-tryOtherEmailLink'
const validationMessageClass = '.Message.is-shown.is-error .Message-message'
const inputValidationTextClass = '.Input-validationMessage'

const openForgottenPasswordSectionClass = '.Accordion.is-expanded'
const closedForgottenPasswordSectionClass = '.Accordion'
const forgottenPasswordInputClass = '.Input-field.Input-field-email'
const forgottenPasswordResetButtonClass = '.Button.ForgetPassword-button'
const loginButtonClass = '.Button.SummaryConfirmation-paynow'
// CHECKBOX has problems around selecting, so we click labels and inspect the checkbox for knowing if it is selected, this is a wdio issue
//-------------------------------

export const getOrderCompleteContainer = () => {
  const summaryConfirm = orderConfirmPO.getSummaryConfirmContainer()
  overlayPO.waitForOverlay()
  try {
    summaryConfirm.waitForVisible(orderCompleteContainerClass, 10000)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[PO ERROR AVERSION] order summary logon fields were not present',
      err,
      'FAILED-TO-GET-ORDER-COMPLETE-CONTAINER'
    )
  }
  overlayPO.waitForOverlay()
  return summaryConfirm.element(orderCompleteContainerClass)
}

export const checkoutLogingPresent = () => {
  const summaryConfirm = orderConfirmPO.getSummaryConfirmContainer()
  overlayPO.waitForOverlay()
  return summaryConfirm.isVisible(orderCompleteContainerClass)
}

export const getContainerText = () => {
  const summaryLogin = getOrderCompleteContainer()
  summaryLogin.waitForVisible(passwordInputClass, 10000)
  return summaryLogin.getText()
}

export const getInputValidationText = () => {
  const summaryLogin = getOrderCompleteContainer()
  summaryLogin.waitForVisible(inputValidationTextClass)
  return summaryLogin.getText(inputValidationTextClass)
}

export const enterEmail = (email) => {
  const summaryLogin = getOrderCompleteContainer()
  summaryLogin.setValue(emailInputClass, email)
  summaryLogin.waitForVisible(inputVaidateSuccessClass, 10000)
}

export const getEmailFieldValue = () => {
  const summaryLogin = getOrderCompleteContainer()
  return summaryLogin.getValue(emailInputClass)
}

export const clickConfirmEmail = () => {
  const summaryLogin = getOrderCompleteContainer()
  summaryLogin.click(verifyEmailButtonClass)
  overlayPO.waitForOverlay()
}

export const enterEmailAndVerify = (email) => {
  enterEmail(email)
  clickConfirmEmail()
}

export const clickLogin = () => {
  const summaryLogin = getOrderCompleteContainer()
  summaryLogin.click(loginButtonClass)
  overlayPO.waitForOverlay()
}

export const enterPassword = (password = 'password1') => {
  overlayPO.waitForOverlay()
  const summaryLogin = getOrderCompleteContainer()
  summaryLogin.setValue(passwordInputClass, password)
}

export const loginButDontCheckout = (email, password) => {
  enterEmailAndVerify(email)
  enterPassword(password)
  clickLogin()
}

export const enterConfirmationPassword = (password = 'password1') => {
  overlayPO.waitForOverlay()
  const summaryLogin = getOrderCompleteContainer()
  summaryLogin.setValue(passConfirmInputClass, password)
  overlayPO.waitForOverlay()
}

export const registerNewAccount = (email, password = 'password1') => {
  enterEmailAndVerify(email)
  enterPassword(password)
  enterConfirmationPassword(password)
}

export const LoginAndCheckout = (email, password = 'password1') => {
  enterEmailAndVerify(email)
  enterPassword(password)
  clickLogin()
  summaryConfirmCheckout.clickConfirmAndPay()
}

export const waitForValidationMessage = () => {
  overlayPO.waitForOverlay()
  const summaryLogin = getOrderCompleteContainer()
  summaryLogin.waitForVisible(validationMessageClass, 10000)
}

export const getValidationMessage = () => {
  waitForValidationMessage()
  const summaryLogin = getOrderCompleteContainer()
  return summaryLogin.getText(validationMessageClass)
}

export const tryAnotherEmailPresent = () => {
  const summaryLogin = getOrderCompleteContainer()
  return summaryLogin.isVisible(tryAnoutherEmailLinkClass)
}

export const clickTryAnotherEmail = () => {
  if (tryAnotherEmailPresent()) {
    const summaryLogin = getOrderCompleteContainer()
    summaryLogin.click(tryAnoutherEmailLinkClass)
  } else {
    console.log(
      '[PO ERROR AVERSION] the try another email link was not present'
    )
    assert.ok(false, 'the try another email link was not present')
  }
}

export const getForgottenPasswordContainer = () => {
  const summaryLogin = getOrderCompleteContainer()
  return summaryLogin.element(forgottenPasswordContainer)
}

export const forgottenPasswordSectionClosed = () => {
  const forgottenPassword = getForgottenPasswordContainer()
  overlayPO.waitForOverlay()
  return forgottenPassword.isVisible(openForgottenPasswordSectionClass)
}

export const openForgottenPasswordSection = () => {
  if (forgottenPasswordSectionClosed() === true) {
    const forgottenPassword = getForgottenPasswordContainer()
    forgottenPassword.click(closedForgottenPasswordSectionClass)
  }
}

export const enterForgottenEmail = (email) => {
  openForgottenPasswordSection()
  const forgottenPassword = getForgottenPasswordContainer()
  forgottenPassword.setValue(forgottenPasswordInputClass, email)
}

export const clickResetPassword = () => {
  const forgottenPassword = getForgottenPasswordContainer()
  forgottenPassword.click(forgottenPasswordResetButtonClass)
}

export const resetPasswordButtonEnabled = () => {
  const forgottenPassword = getForgottenPasswordContainer()
  return forgottenPassword.isEnabled(forgottenPasswordResetButtonClass)
}

export const newsletterOptionSelected = () => {
  const summaryLogin = getOrderCompleteContainer()
  const result = summaryLogin.isSelected(newsletterSubCheckboxClass)
  console.log(`[CHECKOUT PO INFO] newsletter selected?: ${result}`)
  return result
}

export const newsletterOptionPresent = () => {
  const summaryLogin = getOrderCompleteContainer()
  const result = summaryLogin.isVisible(newsletterSubCheckboxClass)
  console.log(`[CHECKOUT PO INFO] newsletter visible?: ${result}`)
  return result
}
