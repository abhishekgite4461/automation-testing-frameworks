import * as overlayPO from '../components/overlay'
import * as shared from '../../lib/shared'
import * as client from '../../lib/client'
// identifiers
//-------------------------------
// Main container identifier
const paymentContainerClass = '.PaymentContainer'
// page element identifiers
const titleSelectId = '#title'
const firstNameInputClass = '.Input-field-firstName'
const lastNameInputClass = '.Input-field-lastName'
const desktopTelephoneNumberInputClass =
  '.FindAddress-telephone .Input-field-telephone'
const mobileTelephoneNumberInputClass =
  '.YourDetails-telephone .Input-field-telephone'
const billingCountrySelectId = '#country'

const houseNumberInputClass = '.Input-field-houseNumber'
const enterManualAddressButtonClass = '.FindAddress-link'

// full address
const address1InputClass = '.YourAddress-form .Input-field-address1'
const cityInputClass = '.YourAddress-form .Input-field-city'
const yourAddressPostcodeInputClass = '.YourAddress-form .Input-field-postcode'

// find address
const findAddressPostcodeInputClass = '.FindAddress-form .Input-field-postCode'
const findAddressButtonClass = '.FindAddress-button'
const findAddressSelectorId = '#findAddress' // if there are multiple addresses
const findAddresslinkClass = '.YourAddress-link.YourAddress-link--right'

const telephoneNumberInputClass = client.hasDesktopLayout()
  ? desktopTelephoneNumberInputClass
  : mobileTelephoneNumberInputClass

//-------------------------------

export const getBillingContainer = () => {
  overlayPO.waitForOverlay()
  try {
    browser.waitForVisible(paymentContainerClass, 10000)
    overlayPO.waitForOverlay()
    return browser.element(paymentContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] Billings page container did not appear to be on the page within the ten second timeout, logging and failing test',
      err,
      'FAILED-TO-GET-BILLING-PAGE-CONTAINER'
    )
  }
}

export const selectTitle = (title) => {
  const billingContainer = getBillingContainer()
  billingContainer.selectByValue(titleSelectId, title)
}

export const enterFirstName = (firstName) => {
  const billingContainer = getBillingContainer()
  billingContainer.setValue(firstNameInputClass, firstName)
}

export const enterLastName = (lastName) => {
  const billingContainer = getBillingContainer()
  billingContainer.setValue(lastNameInputClass, lastName)
}

export const enterPhoneNumber = (phoneNumber) => {
  const billingContainer = getBillingContainer()
  billingContainer.setValue(telephoneNumberInputClass, phoneNumber)
}

export const selectBillingCountry = (country) => {
  const billingContainer = getBillingContainer()
  billingContainer.selectByValue(billingCountrySelectId, country)
}

/*
  nb: Address dropdown will appear if there are more than one address from search
 */
export const selectAddress = () => {
  const billingContainer = getBillingContainer()

  // select the first address
  billingContainer.selectByIndex(findAddressSelectorId, 1)
}

export const enterAddressLine1 = (address) => {
  const billingContainer = getBillingContainer()
  billingContainer.setValue(address1InputClass, address)
}

export const setPostcode = (postcode) => {
  const billingContainer = getBillingContainer()
  let postcodeInputClass = ''

  if (billingContainer.isVisible(yourAddressPostcodeInputClass)) {
    postcodeInputClass = yourAddressPostcodeInputClass
  } else {
    postcodeInputClass = findAddressPostcodeInputClass
  }

  billingContainer.setValue(postcodeInputClass, postcode)
}

export const findAddressLinkPresent = () => {
  const billingContainer = getBillingContainer()
  return billingContainer.isVisible(findAddresslinkClass)
}

export const enterHouseNumber = (houseNumber) => {
  const billingContainer = getBillingContainer()
  billingContainer.setValue(houseNumberInputClass, houseNumber)
}

export const enterCity = (city) => {
  const billingContainer = getBillingContainer()
  billingContainer.setValue(cityInputClass, city)
}

export const clickEnterAddressManually = () => {
  const billingContainer = getBillingContainer()
  billingContainer.click(enterManualAddressButtonClass)
}

export const isManualAddressFormPresent = () => {
  const billingContainer = getBillingContainer()
  return billingContainer.isVisible(address1InputClass)
}

export const clickFindAddressButton = () => {
  const billingContainer = getBillingContainer()
  billingContainer.click(findAddressButtonClass)
  overlayPO.waitForOverlay()
}

export const enterBillingDetails = (
  title = process.env.TITLE,
  firstName = process.env.BASE_FIRSTNAME,
  lastName = process.env.BASE_LASTNAME,
  telephone = process.env.TELEPHONE,
  country = process.env.BASE_COUNTRY,
  postCode = process.env.BASE_POSTCODE
) => {
  const billingContainer = getBillingContainer()

  console.log(
    '[enterBillingDetails]: ',
    title,
    firstName,
    lastName,
    telephone,
    country,
    postCode
  )

  selectBillingCountry(country)
  setPostcode(postCode)

  // if manual address form not visible, note, enter address manually link might not be visible even when this form is
  if (isManualAddressFormPresent() === false) {
    clickFindAddressButton()

    if (billingContainer.isVisible(findAddressSelectorId) === true) {
      selectAddress()
    }
  }

  selectTitle(title)
  enterFirstName(firstName)
  enterLastName(lastName)
  enterPhoneNumber(telephone)
}
