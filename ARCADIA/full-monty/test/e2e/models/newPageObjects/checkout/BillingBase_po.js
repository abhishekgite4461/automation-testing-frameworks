import * as modalPO from '../components/overlay'
import * as shared from '../../lib/shared'

// identifiers
//-------------------------------
// Main container identifier
const billingContainerClass = '.PaymentContainer'
const myDetailsContainerclass = '.YourDetails'
// page element identifiers
const reviewAndConfirmButtonClass = '.Button.PaymentContainer-nextButton'
// delivery option identifiers - radio options can be inspected but you have to click the span because of wdio z index reading issues

// delivery type option identifiers

//-------------------------------

export const getBillingContainer = () => {
  modalPO.waitForOverlay()
  try {
    browser.waitForVisible(billingContainerClass, 10000)
    modalPO.waitForOverlay()
    return browser.element(billingContainerClass)
  } catch (err) {
    shared.failWithScreenshotAndMessage(
      '[ERROR CAPTURE] billing page container did not appear to be on the page within the ten second timeout, logging and failing test',
      err,
      'FAILED-TO-GET-BILLING-CONTAINER-BASE'
    )
  }
}

export const waitForBillingPage = () => {
  getBillingContainer()
  browser.waitForVisible(myDetailsContainerclass)
}

export const clickReviewAndConfirm = () => {
  const billingForm = getBillingContainer()
  billingForm.click(reviewAndConfirmButtonClass)
  modalPO.waitForOverlay()
}
