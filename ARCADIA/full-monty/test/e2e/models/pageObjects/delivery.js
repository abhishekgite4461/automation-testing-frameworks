/* globals browser:false */
import * as client from '../lib/client'
import * as forms from '../lib/_forms'
import * as shoppingBag from '../../models/pageObjects/shoppingBag'
import * as account from '../../models/pageObjects/myAccount'
import * as shared from '../lib/shared'
import * as overlayPO from '../newPageObjects/components/overlay'
import * as login from '../newPageObjects/login_po'
import * as deliveryPO from '../newPageObjects/checkout/DeliveryBase_po'
import * as deliveryFindAddressPO from '../newPageObjects/checkout/FindAddress_po'
import * as manualAddressPO from '../newPageObjects/checkout/ManualAddress_po'
import * as deliveryOptionsPO from '../newPageObjects/checkout/DeliveryOptions_po'
import * as PaymentMethodPO from '../newPageObjects/checkout/CheckoutPaymentCard_po'
import * as summaryConfirmPO from '../newPageObjects/checkout/CheckoutSummaryConfirmation_po'
import * as summaryBasePO from '../newPageObjects/checkout/SummaryConfirmBase_po'
import * as SummaryLogin from '../newPageObjects/checkout/SummaryConfirmLogin_po'
import * as orderComplete from '../newPageObjects/checkout/OrderComplete_po'
import * as paypal from '../newPageObjects/paymentSandboxObjects/paypal_po'
import * as worldpay from '../newPageObjects/paymentSandboxObjects/worldpay_po'
import * as modalStoreLocMap from '../newPageObjects/storeLocator/modalStoreLocatorMapPO'

const assert = require('assert')
const l = require('../lib/localisation').init()

export const navigateToCheckout = () => {
  client.openPage('/checkout')
  login.loginAsUser()
  login.clickProceed()
  shoppingBag.waitForDeliveryCheckoutPage()
}

export const confirmSelectedExpressDeliveryDetails = () => {
  const selector = 'select[name="HOME_EXPRESS"]'
  const value = deliveryOptionsPO.getExpressDeliveryOption()
  const expressDeliveryText = browser.getText(
    `${selector} option[value="${value}"]`
  )
  const splitText = expressDeliveryText.split(' ')[3]
  const expressDeliveryCost = browser.getText('.DeliveryType-price')
  const cost = expressDeliveryCost[1]
  // 20160818 NB: changing delivery option makes an order_summary requests,
  // once in awhile, this request is returned but did not populate express pricing
  // also seeing "unable to change delivery due to error in retrieving email from session" on a guest checkout
  assert.ok(
    splitText.indexOf(cost) !== -1,
    `${cost} did not match the select list cost: ${splitText}`
  )
}

export const HomeDeliveryTitle = () => {
  const titles = deliveryPO.getTitles()
  assert.ok(titles.indexOf('Miss') !== -1)
}

export const selectTitle = () => {
  deliveryPO.selectTitle('Miss')
}

export const acceptTermsAndCondition = () => {
  summaryConfirmPO.acceptTermsAndConditions()
}

export const setDeliveryNameDetails = (title, firstName, surname, phone) => {
  deliveryPO.selectTitle(title)
  deliveryPO.enterDeliveryFirstName(firstName)
  deliveryPO.enterDeliverySurname(surname)
  deliveryPO.enterDeliveryPhoneNumber(phone)
}

export const mobileUpdatesDeliveryFieldPresent = () => {
  assert.ok(
    browser.isVisible(
      '.DeliveryInstructions .Input-field.Input-field-smsMobileNumber'
    )
  )
}

export const deliveryInstructionsTextFieldPresent = () => {
  assert.ok(
    browser.isVisible(
      '.DeliveryInstructions .Input-field.Input-field-deliveryInstructions'
    )
  )
}

export const enterDefaultDeliveryUpdateMobile = () => {
  browser.waitForVisible(
    '.DeliveryInstructions .Input-field.Input-field-smsMobileNumber'
  )
  browser.setValue(
    '.DeliveryInstructions .Input-field.Input-field-smsMobileNumber',
    '07890778675'
  )
}

export const defaultDelUpdateMobilenumberPresent = () => {
  const expectedNumber = '07890778675'
  browser.waitForVisible(
    '.DeliveryInstructions .Input-field.Input-field-smsMobileNumber'
  )
  const actualTelephoneNumber = browser.getValue(
    '.DeliveryInstructions .Input-field.Input-field-smsMobileNumber'
  )
  assert.ok(
    actualTelephoneNumber === expectedNumber,
    `${actualTelephoneNumber} did not match expected: ${expectedNumber}`
  )
}

export const summaryPagePresent = () => {
  assert.ok(
    browser.isVisible('.SummaryContainer'),
    'summary page was not present'
  )
}

export const defaultDelInstructionsNoteIsPresent = () => {
  const expectedDeliveryNote = 'dont chuck it on the roof'
  browser.waitForVisible(
    '.DeliveryInstructions .Input-field.Input-field-deliveryInstructions'
  )
  const actualDeliveryNote = browser.getValue(
    '.DeliveryInstructions .Input-field.Input-field-deliveryInstructions'
  )
  assert.ok(
    actualDeliveryNote === expectedDeliveryNote,
    `${actualDeliveryNote} did not match expected: ${expectedDeliveryNote}`
  )
}

export const enterLongDeliveryNote = () => {
  const longDeliveryNote =
    'dont chuck it on the roof cause i wont find it and I hate it when delivery people do this'
  browser.waitForVisible(
    '.DeliveryInstructions .Input-field.Input-field-deliveryInstructions'
  )
  browser.setValue(
    '.DeliveryInstructions .Input-field.Input-field-deliveryInstructions',
    longDeliveryNote
  )
}

export const deliveryNoteValidationPresent = () => {
  assert.ok(
    browser.getText('.DeliveryInstructions-charsRemaining') ===
      '0 characters remaining',
    `${browser.getText(
      '.DeliveryInstructions-charsRemaining'
    )} did not match expected: 0 characters remaining`
  )
}

export const enterDefaultDeliveryInfoText = () => {
  const deliveryNote = 'dont chuck it on the roof'
  browser.waitForVisible(
    '.DeliveryInstructions .Input-field.Input-field-deliveryInstructions'
  )
  browser.setValue(
    '.DeliveryInstructions .Input-field.Input-field-deliveryInstructions',
    deliveryNote
  )
}

export const SetDeliveryDetail = (value, selectorName) => {
  try {
    forms.setFormInput(' ', selectorName, false)
    forms.setFormInput(value, selectorName, false)
  } catch (err) {
    shared.captureScreenshotAndError(
      '[ERROR AVERSTION] cvv was not present on page the first time round, refreshing and trying again',
      err,
      'NO-CVV-PRESENT-TAKEN-BEFORE-REFRESH-ATTEMPT'
    )
    browser.refresh()
    forms.setFormInput(' ', selectorName, false)
    forms.setFormInput(value, selectorName, false)
  }
}

export const setCheckoutEmailAddress = (email) => {
  SummaryLogin.enterEmail(email)
}

export const acceptAndVerify = () => {
  process.env.registeredEmail = account.createRandomEmailAddress()
  SummaryLogin.enterEmailAndVerify(process.env.registeredEmail)
  acceptTermsAndCondition()
  SummaryLogin.enterPassword()
  SummaryLogin.enterConfirmationPassword()
}

export const Verify = () => {
  SummaryLogin.clickConfirmEmail()
}

export const tryAnotherEmail = () => {
  SummaryLogin.clickTryAnotherEmail()
}

export const loginIsCleared = () => {
  browser.pause(500)
  const emailFieldText = SummaryLogin.getEmailFieldValue()
  assert.ok(
    emailFieldText === '',
    'email field was not empty, got: ',
    +emailFieldText
  )
}

export const confirmOrder = () => {
  acceptTermsAndCondition()
  summaryConfirmPO.clickConfirmAndPay()
}

export const confirmPaymentNotProcessable = () => {
  const expected =
    'Your payment cannot be accepted at this time. Please try again later.'
  const actual = summaryConfirmPO.getErrorMessage()
  assert.ok(
    actual === expected,
    `'${actual}' did not match the expected of: '${expected}'`
  )
}

// Enter confirmation detail but do not pay
export const enterConfirmationDetails = () => {
  SummaryLogin.loginButDontCheckout(process.env.USERNAME, process.env.PASSWORD)
}

export const enterConfirmationDetailsAsARegisteredCustomer = () => {
  SummaryLogin.loginButDontCheckout(process.env.USERNAME, process.env.PASSWORD)
}

export const verifyAsAUser = (username) => {
  SummaryLogin.enterEmailAndVerify(username)
}

export const verifyAsARegisteredUser = () => {
  SummaryLogin.enterEmailAndVerify(process.env.USERNAME)
}

export const confirmOrderAsARegisteredUser = () => {
  enterConfirmationDetailsAsARegisteredCustomer()
  // Accept terms and conditions
  acceptTermsAndCondition()
  summaryConfirmPO.clickConfirmAndPay()
}

export const waitUntilOrderAndPayNowButtonIsEnabled = () => {
  summaryConfirmPO.waitForConfirmAndPayButtonEnabled()
}

export const loginToConfirmationWithRegisteredUser = () => {
  enterConfirmationDetails(process.env.USERNAME, process.env.PASSWORD)
}

export const mergeBag = () => {
  overlayPO.waitForOverlay()
  browser.waitForVisible('.Modal .CheckoutContainer-errorMessage p')

  const message = browser.getText('.Modal .CheckoutContainer-errorMessage')
  const expectedMessage = l`You have items in your shopping bag from a previous visit.`
  assert.ok(
    message.includes(expectedMessage),
    `'${message}' did not match '${expectedMessage}'`
  )
  // verify at least one product is displayed in summary page
  browser.waitForVisible(
    '.SummaryContainer-orderProducts .OrderProducts-product'
  )
  overlayPO.waitForOverlay()

  const mergeBagButton =
    '.Modal .CheckoutContainer-errorMessage .CheckoutContainer-close'

  // TODO
  // there are two order_summary requests made
  // the first request returns 422 erModal .CheckoutContainer-errorMessage .CheckoutContainer-closens product in bag and merges with current
  // temporarily work around this issue
  try {
    client.click(mergeBagButton)
    overlayPO.waitForOverlay()
  } catch (err) {
    client.click(mergeBagButton)
    overlayPO.waitForOverlay()
  }
}

export const confirmOrderAsARegisteredUserAndMergeBag = () => {
  enterConfirmationDetails(process.env.USERNAME, process.env.PASSWORD)

  // merge bag
  mergeBag()

  // assert both products are in the bag
  shoppingBag.containsNumberOfProducts(2)

  // assert login successful
  const validationMsg = summaryBasePO.getSummaryText()
  assert.ok(
    validationMsg.indexOf('You successfully logged') !== -1,
    'Not logged in'
  )

  summaryConfirmPO.acceptTermsAndConditions()
  summaryConfirmPO.clickConfirmAndPay()
}

export const confirmationPagePresent = () => {
  if (summaryConfirmPO.summaryPagePresent()) {
    const errorMessage = `not looking for confirmation page as payment failed with: ${summaryConfirmPO.getErrorMessage()}`
    shared.failWithScreenshotAndMessage(
      'payment refused',
      errorMessage,
      'PAYMENT-REFUSED-SEE-ERROR-MESSAGE-'
    )
  } else {
    const actualSuccessMessage = orderComplete.getThankYouMessage()
    const expectedSuccessMessage = l`Thank You For Your Order`

    assert.equal(
      expectedSuccessMessage.toLowerCase(),
      actualSuccessMessage.toLowerCase(),
      `'${actualSuccessMessage}' did not match expected message '${expectedSuccessMessage}'`
    )
  }
}

export const manunalAddressFormPresent = () => {
  const addressForm = manualAddressPO.getManualAddressContainer()
  assert.ok(addressForm !== null, 'manual address form was not present')
}

export const threeDSecureAuthenticationFlow = () => {
  worldpay.confirmVBV()
  confirmationPagePresent()
}

export const clickContinueShopping = () => {
  orderComplete.clickContinueShopping()
}

export const confirmOrderButtonDisabled = () => {
  assert(summaryConfirmPO.confirmAndPayButtonEnabled() === false)
}

export const enterBlankInToPostcode = () => {
  deliveryFindAddressPO.enterPostcode(['\b'])
}

export const submitAddress = () => {
  deliveryFindAddressPO.clickFindAddressButton()
}

export const checkLinkText = (value) => {
  const currentLinkText = manualAddressPO.getManualAddressLinkText()
  assert.deepEqual(currentLinkText, value)
}

export const findAddressExists = () => {
  assert.ok(manualAddressPO.findAddresslinkVisible())
}

export const suggestedAddressPresent = () => {
  const results = deliveryFindAddressPO.getFindAddressResults()
  const value = 'Flat, 72 Springbank Road, LONDON SE13 6SX'
  assert.ok(
    results.includes(value),
    `Text "${value}" not found in "${results}"`
  )
}

export const noAddressError = () => {
  const errorMessage =
    'We are unable to find your address at the moment. Please enter your address manually.'
  const selector = '.Message-message'
  return client.includesText(selector, errorMessage)
}

export const validationError = (selectorName, errorMessage) => {
  return client.includesText(
    `.Input-${selectorName} span:nth-child(2)`,
    errorMessage
  )
}

export const fillDeliveryForm = () => {
  deliveryPO.selectTitle('Mr')
  deliveryPO.enterDeliveryFirstName('Bob')
  deliveryPO.enterDeliverySurname('Tester')
  deliveryPO.enterDeliveryPhoneNumber('+4412345678')
  manualAddressPO.enterManualAddress(
    'United Kingdom',
    'Home Street',
    'line 2',
    'se137gg',
    'London'
  )
}

export const clickNext = () => {
  deliveryPO.clickNextButton()
}

export const enterPolishAddress = () => {
  deliveryPO.selectTitle('Mr')
  deliveryPO.enterDeliveryFirstName('Bob')
  deliveryPO.enterDeliverySurname('Tester')
  deliveryPO.enterDeliveryPhoneNumber('01258776578')
  manualAddressPO.enterManualAddress(
    'Poland',
    'Home Street',
    'line 2',
    'se137gg',
    'walsaw'
  )
  deliveryPO.clickNextButton()
}

export const enterAddressManually = () => {
  if (process.env.ENVIRONMENT !== 'stage_international') {
    manualAddressPO.getManualAddressContainer()
  }
}

export const deliveryFieldsAreEmpty = () => {
  overlayPO.waitForOverlay()
  assert.ok(
    browser.getAttribute('.Input-field.Input-field-city', 'value') === '',
    'city is not blank'
  )
  // following assertion checks the default name entered as it is never blank and has a comma in it when looking blank
  assert.ok(
    browser.getAttribute('.Input-field.Input-field-firstName', 'value') !==
      'tester',
    'first name is not blank'
  )
  assert.ok(
    browser.getAttribute('.Input-field.Input-field-lastName', 'value') === '',
    'last name is not blank'
  )
  assert.ok(
    browser.getAttribute('.Input-field.Input-field-postcode', 'value') === '',
    'postcode is not blank'
  )
  assert.ok(
    browser.getAttribute('.Input-field.Input-field-address2', 'value') === '',
    'address 2 is not blank'
  )
  assert.ok(
    browser.getAttribute('.Input-field.Input-field-address1', 'value') === '',
    'address 1 is not blank'
  )
}

export const checkoutWithDefaultVBVDetails = () => {
  const cardNumber = '4917610000000000'
  const cvv = '123'
  let cardType = '3D'
  const is3DSecure = cardType.toUpperCase() === '3D'

  if (is3DSecure) {
    cardType = 'VISA'
  }

  if (is3DSecure) {
    shoppingBag.enter3DUkDeliveryAddress()
  } else {
    shoppingBag.enterDefaultDeliveryInfo()
  }
  // fill in credit card information
  shoppingBag.checkoutWithCustomCard(cardType.toUpperCase(), cardNumber, cvv)
  // confirm and pay
  browser.waitForVisible('.SummaryConfirmation-form')
  browser.waitForVisible('.SummaryConfirmation-form .Checkbox-check')
}

export const confirmVBV = () => {
  acceptTermsAndCondition()
  summaryConfirmPO.clickConfirmAndPay()
  threeDSecureAuthenticationFlow()
}

export const confirmWithPaypal = () => {
  acceptTermsAndCondition()
  summaryConfirmPO.clickConfirmAndPay()
  paypal.confirmOrder()
}

export const checkoutWithPaypal = () => {
  const cardType = 'PYPAL'

  shoppingBag.enterDefaultDeliveryInfo()
  PaymentMethodPO.setPaymentDetails(cardType)
}

export const loggedInSavedProfilePaypalCheckout = () => {
  const cardType = 'PYPAL'

  // fill in credit card information
  const changePaymentType = '.Payments-changeLink'
  client.click(changePaymentType)
  // NB: temporarily click change payment type link again
  // as user is redirected to billing screen with payment details in view mode
  // to remove once this is fixed
  if (browser.isVisible(changePaymentType)) {
    client.click(changePaymentType)
  }
  // select card type
  PaymentMethodPO.setPaymentDetails(cardType)
}

export const loggedInSavedProfileKlarnaCheckout = () => {
  // wait until overlay disappears as it is preventing next button click
  overlayPO.waitForOverlay()
  deliveryPO.clickNextButton()
}

export const selectCollectionPoint = (location) => {
  let count = 5

  while (count > 0 && deliveryOptionsPO.getCFSSearchLocation() === '') {
    console.log(
      `Attempting to select a location from search suggestion list`,
      count
    )
    // enter search location
    deliveryOptionsPO.setCFSlocation(location)
    // select from suggestion
    deliveryOptionsPO.selectCFSPredictionLocation()
    count--
  }

  // click search button
  deliveryOptionsPO.clickCFSSearchButton()
  // select and expand first store
  browser.waitForVisible('.StoreLocator-resultsContainer')
  browser.click('.StoreLocator-resultsContainer .Store')
  // wait accordion expand transition
  browser.pause(300)
  // save store text
  const storeLocal = browser.getText('.Store .Store-detailsTitle')
  process.env.COLLECT_FROM_STORE_ADDRESS = storeLocal
  // select store
  const storeButton = '.Store-selectButtonDetails'
  browser.waitForVisible(storeButton)
  client.click(storeButton)
  overlayPO.waitForOverlay()
}

export const setParcelShopToCollectFrom = (city) => {
  if (deliveryOptionsPO.collectFromParcelShopOptionSelected()) {
    selectCollectionPoint(city)
  } else {
    assert.ok(false, 'ParcelShop delivery option was not selected')
  }
}

export const choosenStorePresentOnConfirmation = () => {}

export const collectFromStoreChoiceExists = () => {
  browser.waitForVisible('.StoreDelivery-storeAddress')
  let actualStoreAddress = browser.getText('.StoreDelivery-storeAddress')
  let expectedStoreAddress = process.env.COLLECT_FROM_STORE_ADDRESS

  actualStoreAddress = actualStoreAddress.replace(/\n/g, ' ')
  actualStoreAddress = actualStoreAddress.replace(/,/g, '')
  expectedStoreAddress = expectedStoreAddress.replace(/\n/g, ' ')
  expectedStoreAddress = expectedStoreAddress.replace(/,/g, '')

  assert.equal(
    actualStoreAddress,
    expectedStoreAddress,
    `Actual address '${actualStoreAddress}' did not match expected address '${expectedStoreAddress}'`
  )
}

export const confirmDeliveryCountryIsUnselected = () => {
  const selectedDeliveryCountry = deliveryFindAddressPO.getSelectedDeliveryCountry()
  assert.equal(
    selectedDeliveryCountry,
    'default',
    `Expected: Please select your country (value=default), actual: ${selectedDeliveryCountry}`
  )
}

export const collectFromStore = () => {
  deliveryOptionsPO.setCollectFromStoreDeliveryTypeOption()
}

export const collectFromStoreStandard = () => {
  deliveryOptionsPO.setCollectFromStoreDeliveryTypeStandard()
}

export const collectFromStoreExpress = () => {
  deliveryOptionsPO.setCollectFromStoreDeliveryTypeExpress()
}

export const collectFromParcelShop = () => {
  deliveryOptionsPO.setCollectFromParcelShopDeliveryTypeOption()
}

export const setLocForStoreSearch = (loc) => {
  modalStoreLocMap.triggerLocSearch(loc)
}
export const verifyMapIsDisplayed = () => {
  modalStoreLocMap.verifyMapIsDisplayed()
}
export const verifyMapIsNotDisplayed = () => {
  modalStoreLocMap.verifyMapIsNotDisplayed()
}
export const clickOnGoButton = () => {
  modalStoreLocMap.clickOnGoButton()
}
export const pickParcelShopLocation = () => {
  modalStoreLocMap.getFirstResultLoc()
}

export const selectStoreCollectStandard = () => {
  deliveryOptionsPO.setCollectFromStoreDeliveryTypeStandard()
}
export const selectStoreCollectExpress = () => {
  deliveryOptionsPO.setCollectFromStoreDeliveryTypeExpress()
}

export const checkParcelShopCaptionAdded = () => {
  overlayPO.waitForOverlay()
  let exp = ''
  const cpt1 = 'Collect from ParcelShop'
  const cpt2 = 'Free Collect from ParcelShop'
  browser.pause(500)
  browser.waitForEnabled('.SimpleTotals')
  const orderValue = browser.getText(
    '.SimpleTotals-subTotal .SimpleTotals-groupRight'
  )
  const deliveryValueTotal = orderValue.substr(1)
  const dVT = parseFloat(deliveryValueTotal)
  if (dVT < 50.0) {
    exp = cpt1
  } else {
    exp = cpt2
  }
  const act = browser.getText('.SimpleTotals-delivery .SimpleTotals-groupLeft')
  console.log(
    `[checkParcelShopCaptionAdded] ACTUAL = ${act} : EXPECTED = ${exp}`
  )
  // assert.equal(act, exp, 'Caption for ParcelShop charge in Bill - NOT as Expected')
}

export const checkParcelShopChargeAdded = () => {
  let exp = ''
  const cpt1 = '4.00'
  const cpt2 = '0.00'

  browser.waitForEnabled('.SimpleTotals')
  let deliverySubTotalValue = browser.getText(
    '.SimpleTotals-subTotal .SimpleTotals-groupRight'
  )
  const deliveryChargeValue = browser.getText(
    '.SimpleTotals-delivery .SimpleTotals-groupRight'
  )
  deliverySubTotalValue = deliverySubTotalValue.substr(1)
  const dVT = parseFloat(deliverySubTotalValue)
  if (dVT < 50.0) {
    exp = cpt1
  } else {
    exp = cpt2
  }
  console.log(
    `[checkParcelShopChargeAdded] ACTUAL = ${deliveryChargeValue} : EXPECTED = ${exp}`
  )
  // assert.equal(deliveryChargeValue, exp, 'Delivery Charge for ParcelShop Collection - NOT as Expected')
}

export const checkStoreExpressChargeAdded = (exp) => {
  browser.waitForEnabled('.SimpleTotals')
  const deliveryChargeValue = browser.getText(
    '.SimpleTotals-delivery .SimpleTotals-groupRight'
  )
  const deliveryChargeValueStr = deliveryChargeValue.substr(1)
  const deliveryChargeValueNum = parseFloat(deliveryChargeValueStr)
  console.log(`orderDeliveryValue : ${deliveryChargeValueNum}`)
  const expNum = parseFloat(exp.substr(1))
  console.log(
    `[checkStoreExpressChargeAdded] ACTUAL = ${deliveryChargeValueNum} : EXPECTED = ${expNum}`
  )
  assert.equal(
    deliveryChargeValueNum,
    expNum,
    'Delivery Charge for Store Express Collection - NOT as Expected'
  )
}

export const checkParcelShopChargeCalc = () => {
  overlayPO.waitForOverlay()
  const deliverySubTotalValue = browser.getText(
    '.SimpleTotals-subTotal .SimpleTotals-groupRight'
  )
  let deliveryChargeValue = browser.getText(
    '.SimpleTotals-delivery .SimpleTotals-groupRight'
  )
  const deliveryTotalValue = browser.getText(
    '.SimpleTotals-total .SimpleTotals-groupRight'
  )

  process.env.ValueOfOrderBeforeDelCharge = deliverySubTotalValue
  console.log(`deliverySubTotalValue : ${deliverySubTotalValue}`)
  console.log(`orderDeliveryValue : ${deliveryChargeValue}`)
  console.log(`deliveryTotalValue : ${deliveryTotalValue}`)
  // Alter calculation value when 'FREE' is displayed
  if (deliveryChargeValue === 'Free') {
    deliveryChargeValue = '£0.00'
  }
  const deliverySubTotalValueStr = deliverySubTotalValue.substr(1)
  const deliverySubTotalValueNum = parseFloat(deliverySubTotalValueStr)

  const deliveryChargeValueStr = deliveryChargeValue.substr(1)
  const deliveryChargeValueNum = parseFloat(deliveryChargeValueStr)

  const totalOrderValueStr = deliveryTotalValue.substr(1)
  const totalOrderValueNum = parseFloat(totalOrderValueStr)

  const calcTot = deliverySubTotalValueNum + deliveryChargeValueNum

  console.log(
    `[checkParcelShopChargeCalc] Displayed - itemValues: ${deliverySubTotalValueNum} + orderDeliveryValue: ${deliveryChargeValueNum} = totalOrderValue = ${totalOrderValueNum}`
  )
  assert.equal(
    calcTot,
    totalOrderValueNum,
    `!!! The checkout calculation is incorrect !!! is ${totalOrderValueNum}Expected : ${calcTot}`
  )
}
