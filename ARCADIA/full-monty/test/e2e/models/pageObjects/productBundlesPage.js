/* globals browser:false */
import * as client from '../lib/client'
import * as search from '../pageObjects/search'
import * as overlayPO from '../newPageObjects/components/overlay'

const assert = require('assert')

export const goToProduct = () => {
  client.click('.BurgerButton-bar')
  client.clickWithText('.ListItemLink.is-active', 'Suits')
  client.clickWithText('.ListItemLink.is-active', 'Mens Tailored Fit Suits')
  return client.click('.ProductImages-image')
}

export const outfitPriceExists = () => client.element('.Carousel-item .Price')

export const selectCarouselProduct = () => {
  const text = browser.getHTML('.Carousel-item.is-selected .MiniProduct-title')
  browser.elements('.Carousel--flat .Carousel-image').then((elems) => {
    return browser.elementIdClick(elems.value[2].ELEMENT)
  })
  browser.pause(300)
  const newText = browser.getHTML(
    '.Carousel-item.is-selected .MiniProduct-title'
  )
  assert.notEqual(text[0], newText[0])
}

export const selectCarouselByPrice = (price) => {
  client.clickWithText('.Price-value', price)
  return client.includesText('.Carousel-item.is-selected .Price-value', price)
}

export const goToOutfitProduct = () => search.searchProduct(24821978)

export const selectMiniProduct = () => client.click('.MiniProduct-link')

export const clickBundlesLink = (link) => client.clickWithText('a', link)

export const redirectedToProductDetails = () => {
  browser.isExisting('.BVRRContainer')
}

export const selectSize = () => {
  client.scroll('.Select-select')
  return client.selectByIndex('.Select-select', 2)
}

export const addItemToBag = () => {
  return client.click('.Button.AddToBag')
}

export const addToBagSuccess = () => {
  return browser.waitForExist('.Button.Button-goToCheckout')
}

export const bundleElementsAreShown = () => {
  overlayPO.waitForOverlay()
  browser.isExisting('.Modal')
  browser.isExisting('.Carousel-image')
  browser.isExisting('.ProductDetail-title')
  browser.isExisting('.ProductDetail-description')
  browser.isExisting('.ProductDetail-productCode')
  browser.waitForText('a', 'Read reviews')
  browser.waitForText('a', 'See full details')
  return browser.isExisting('.Price-value')
}

export const miniProductElementsAreShown = () => {
  browser.isExisting('.MiniProduct')
  browser.isExisting('.MiniProduct-image')
  browser.isExisting('.MiniProduct-title')
  browser.isExisting('.HistoricalPrice--miniProduct')
  browser.isExisting('.Select-select')
  return browser.waitForText('a', 'View Product Information')
}

export const navigateToOutfit = (product) => {
  switch (product) {
    case 'Bundle Multi Fixed':
      search.searchProduct(24812126)
      break
    default:
      throw new Error('Must specify product to navigate to')
  }
  return browser.isExisting('.Bundles.is-fixed')
}

export const checkTotalPrice = (price) => {
  return client.includesText('.BundlesAddAll-price .Price-value', price)
}
