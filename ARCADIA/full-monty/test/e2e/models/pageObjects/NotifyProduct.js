import * as forms from '../lib/_forms'
import * as client from '../lib/client'

export const clickNotifyEmail = () => client.click('.NotifyProduct-button')

export const waitForNotifyEmailModal = () => {
  const selector = '.NotifyProduct-formSection'
  browser.waitForExist(selector)
}

export const enterFirstname = (value) =>
  client.setValue('.NotifyProduct-form [name="firstName"]', value)
export const enterSurname = (value) =>
  client.setValue('.NotifyProduct-form [name="surname"]', value)
export const enterEmail = (value) =>
  client.setValue('.NotifyProduct-form [name="email"]', value)

export const submitNotifyEmailForn = () =>
  client.click('.NotifyProduct-submitButton')

export const receivedEmailStockMessage = () => {
  client.includesText('h1', 'Submitted')
  client.existsNTimes('p', 1)
}

export const seeEmailPreviewed = (email) =>
  forms.formInputTextEquals('email', email, 'NotifyProduct-form')

export const checkButtonDisabled = () =>
  forms.checkButtonDisabled('NotifyProduct-submitButton', 'NotifyProduct-form')

export const seeEmailValidation = () =>
  forms.formInputValidationEquals(
    'email',
    'Please enter a valid email address.',
    'NotifyProduct-form'
  )
