import * as overlay from '../newPageObjects/components/overlay'
import * as confirmationLogin from '../../models/newPageObjects/checkout/SummaryConfirmLogin_po'
import * as account from '../pageObjects/myAccount'

const assert = require('assert')
const l = require('../lib/localisation').init()

export const validationError = (err) => {
  const text = confirmationLogin.getInputValidationText()
  assert.ok(text === err, `${text} received did not match expected: ${err}`)
}

export const validationDescription = (description) => {
  const messageText = confirmationLogin.getContainerText()
  const ldescription = l(description)
  console.log(`l description = ${ldescription}`)
  assert.ok(
    messageText.indexOf(ldescription) !== -1,
    `${messageText} did not include expected message: ${ldescription}`
  )
}

export const addPassword = (pwd) => {
  confirmationLogin.enterPassword(`${pwd}\u0009`)
}

export const addConfirmPassword = (pwd) => {
  confirmationLogin.enterConfirmationPassword(pwd)
  // tab is not invoking the validation, so clicking the summary header
  browser.click('.Payments')
}

export const clickEditProduct = () => {
  browser.waitForVisible('.OrderProducts-editText')

  const raw = browser.getText('.OrderProducts-label')
  const rawText = raw.toString()
  process.env.SUMMARY_QUANTITY_BEFORE_PRODUCT_EDIT = rawText
    .replace(',', '')
    .replace('Total:,,', '')
  console.log(process.env.SUMMARY_QUANTITY_BEFORE_PRODUCT_EDIT)
  browser.click('.OrderProducts-editText')
  overlay.waitForOverlay()
  browser.waitForVisible('.OrderProducts-formItem')
}

export const updateQuantity = () => {
  browser.selectByValue('#bagItemQuantity', 2)
  overlay.waitForOverlay()
  browser.click(
    '.Button.OrderProducts-saveButton.Button--secondary.Button--halfWidth'
  )
  overlay.waitForOverlay()
}

export const quantityUpdated = () => {
  const raw = browser.getText('.OrderProducts-label')
  const rawText = raw.toString()
  const newQuantity = rawText.replace(',', '').replace('Total:,,', '')
  console.log(newQuantity)
  assert.ok(
    process.env.SUMMARY_QUANTITY_BEFORE_PRODUCT_EDIT !== newQuantity,
    `${
      process.env.SUMMARY_QUANTITY_BEFORE_PRODUCT_EDIT
    } :did not appear that the quantity or size was updated: ${newQuantity}`
  )
}

export const promotionCodePresent = (code) => {
  browser.waitForVisible('.PromotionCode-codeTitle')
  const promotionTitleText = browser.getText('.PromotionCode-codeTitle')
  assert.ok(
    promotionTitleText.indexOf(code) !== -1,
    `${promotionTitleText} did not contain the expected code of ${code}`
  )
}

export const editProductOptionsPresent = () => {
  assert.ok(
    browser.isVisible('.Select.OrderProducts-sizes.is-selected'),
    'size options where not present'
  )
  assert.ok(
    browser.isVisible('.Select.OrderProducts-quantities.is-selected'),
    'quantity options where not present'
  )
  assert.ok(
    browser.isVisible('.OrderProducts-inlineButtons'),
    'button options where not present'
  )
}

export const addNewCheckoutAccount = () => {
  const email = account.createRandomEmailAddress()
  confirmationLogin.enterEmailAndVerify(email)
  addPassword('password1')
  addConfirmPassword('password1')
  assert.ok(
    browser.isVisible('.Input-validationMessage') !== true,
    'there was a validation message present, check screen shot'
  )
}
