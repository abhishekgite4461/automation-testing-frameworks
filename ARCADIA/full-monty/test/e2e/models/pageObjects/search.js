/* eslint no-else-return: 0 */

import * as client from '../lib/client'
import * as forms from '../lib/_forms'
import * as modalPO from '../newPageObjects/components/overlay'

export const buildRefinementUrl = (refinement, parameter1, parameter2) => {
  let url = browser.getUrl()
  if (url.indexOf('category') !== -1) {
    console.log(
      'was a category search result, using category refinement replacement'
    )
    url = `${url}?refinements=${refinement}%3A${parameter1}%2C${refinement}%3A${parameter2}`
  } else if (url.indexOf('search') !== -1) {
    console.log(
      'was a proper search result, using search refinement replacement'
    )
    url = `${url}&refinements=${refinement}%3A${parameter1}%2C${refinement}%3A${parameter2}`
  }
  console.log(`refinement created: ${url}`)
  return url
}

export const addPriceRefinementURL = (
  priceRangeHigh = 1,
  priceRangeLow = 500
) => {
  return buildRefinementUrl('price', priceRangeLow, priceRangeHigh)
}

export const searchProduct = (query, highValue = null, lowValue = null) => {
  const searchSelector = '.Header-searchButton'
  let counter = 0

  // go to homepage if in checkout flow
  if (
    browser.isVisible('.Header-shoppingCartIconbutton') === false ||
    browser.isVisible('.Header-searchButton') !== true ||
    browser.getAttribute('.Drawer', 'class') === 'Drawer is-open' ||
    browser.isVisible('.ContentOverlay.ContentOverlay--modalOpen')
  ) {
    client.openPage('/')
  }
  if (modalPO.overlayIsVisible()) {
    browser.refresh()
  }

  if (client.hasDesktopLayout()) {
    browser.waitForVisible('.HeaderBig #searchTermInput')
    browser.setValue('.HeaderBig #searchTermInput', query)
  } else {
    client.click(searchSelector)
    forms.setFormInput(query, 'searchTerm', 'SearchBar-form')
  }

  browser.keys(['\uE007'])
  // wait for CSS transition
  browser.pause(300)
  modalPO.waitForOverlay()

  if (highValue && lowValue) {
    console.log(`[SEARCH] HIGH: ${highValue} LOW: ${lowValue}`)
  }

  if (highValue !== null && lowValue !== null) {
    console.log('adding refinement')
    const refinedURL = addPriceRefinementURL(highValue, lowValue)
    console.log(refinedURL)
    browser.url(refinedURL)
  }
  // wait until search results, no results or product detail screen appears
  while (
    browser.isVisible('.NoSearchResults') === false &&
    browser.isVisible('.Product-link') === false &&
    browser.isVisible('.BVRRDisplayContent') === false &&
    counter < 20
  ) {
    counter++
    browser.pause(1000)
  }
  if (
    browser.isVisible('.NoSearchResults-message') ||
    browser.isVisible('.Carousel')
  ) {
    console.log(
      `[ERROR AVERSION] search failed, need to try resetting products and trying new - search query to error: ${query}`
    )
    console.log(`failed on the url: ${browser.getUrl()}`)
    return false
  } else {
    return true
  }
}

export const searchProductByUrl = (query) => {
  const newQuery = query.replace(/ /g, '+')
  client.openPage(`/search?q=${newQuery}`)
}
