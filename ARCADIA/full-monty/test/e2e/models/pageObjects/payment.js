import * as client from '../lib/client'
import * as shared from '../lib/shared'
import * as overlayPO from '../newPageObjects/components/overlay'
import * as billingPage from '../newPageObjects/checkout/CheckoutPaymentCard_po'
import * as checkoutPO from '../newPageObjects/checkout/SummaryConfirmLogin_po'
import * as billingBasePO from '../newPageObjects/checkout/BillingBase_po'
import * as giftcardPO from '../newPageObjects/components/giftcard_po'
import * as klarnaPO from '../newPageObjects/paymentSandboxObjects/klarna_po'

const assert = require('assert')

export const paymentType = () => {
  const selector = 'select[name="paymentType"]'
  client.selectByIndex(selector, 1)
  return client.hasValue(selector, 'VISA')
}

export const openWhatsACVV = () => {
  client.click('.CardDetails-link')
}

export const whatsACVVModalOpen = () => {
  // todo when ticket mon-812 is done then this can be completled.
}

export const expiryMonth = () => {
  const selector = 'select[name="expiryMonth"]'
  client.selectByIndex(selector, 1)
  return client.hasValue(selector, '01')
}

export const expiryYear = () => {
  const selector = 'select[name="expiryYear"]'
  client.selectByIndex(selector, 3)
  return client.hasValue(selector, '2018')
}

export const navigateToPayment = () => {
  client.openPage('/checkout/payment?isAnonymous=true')
  try {
    billingBasePO.waitForBillingPage()
  } catch (err) {
    shared.captureScreenshotAndError(
      '[ERROR AVERSION] checkout password bug aversion',
      err,
      'PASSWORD-BUG-AVERSION-SCREENSHOT'
    )
    browser.refresh()
    billingBasePO.waitForBillingPage()
  }
}

export const openGiftCardMenu = () => {
  giftcardPO.openGiftCardSection()
}

export const enterGiftCardAndPin = (giftCard, pin) => {
  giftcardPO.openGiftCardSection()
  giftcardPO.enterGiftCard(giftCard)
  giftcardPO.enterGiftCardPIN(pin)
  giftcardPO.clickConfirmGiftCard()
}

export const submitGiftCard = () => {
  overlayPO.waitForOverlay()
  client.click('.GiftCardContainer-row .Button')
  overlayPO.waitForOverlay()
}

export const discountSuccessMessageContains = (message) => {
  const bannerText = giftcardPO.getGiftCardMessageText()
  assert.ok(bannerText.indexOf(message) !== 1)
}

export const giftCardAmountEquals = (expectedAmount) => {
  const actualUsed = giftcardPO.getGiftCardAmountUsed()
  assert.ok(expectedAmount.indexOf(actualUsed) !== 1)
}

export const clickNext = () => {
  if (browser.isVisible('.DeliveryContainer-nextButton')) {
    browser.click('.DeliveryContainer-nextButton')
  } else if (browser.isVisible('.PaymentContainer-nextButton')) {
    browser.click('.PaymentContainer-nextButton')
  }
}

export const fillBillingForm = () => {
  billingPage.setPaymentDetails('VISA', '4917610000000000', '123')
}

export const fillBillingFormWithBadVisa = () => {
  billingPage.setPaymentDetails('VISA', '4444444444444444', '123', '09', '2017')
}

export const paymentTypeAvailable = (option) => {
  const availableOptions = billingPage.getPaymentOptions()
  assert.ok(
    availableOptions.indexOf(option) !== -1,
    `it appears '${option}' was not a part of available options: ${availableOptions}`
  )
}

export const paymentTypeNotAvailable = (option) => {
  const availableOptions = billingPage.getPaymentOptions()
  assert.ok(
    availableOptions.indexOf(option) === -1,
    `'${option}' is available when it should not \n ${availableOptions}`
  )
}

export const completePurchaseWithKlarna = () => {
  // select Klarna as the payment type
  billingPage.setPaymentTypebyValue('KLRNA')

  // wait until klarna iframe displays
  overlayPO.waitForOverlay()
  browser.waitForVisible('#klarna-credit-main')

  // click pay
  const paymentOptions = browser.element('#CardDetails')
  paymentOptions.waitForEnabled('.Button')
  paymentOptions.click('.Button')
}

export const completePurchaseWithKlarnaAsAnApprovedUser = () => {
  completePurchaseWithKlarna()

  if (checkoutPO.checkoutLogingPresent()) {
    checkoutPO.clickConfirmEmail()
    checkoutPO.enterPassword()
    checkoutPO.clickLogin()
  }
}

export const completePurchaseWithKlarnaAsANewUser = () => {
  completePurchaseWithKlarna()

  overlayPO.waitForOverlay()

  klarnaPO.confirmOrderViaCreditApplication()
}

export const completePurchaseWithKlarnaAsARegisteredUser = () => {
  // select Klarna as the payment type
  billingPage.setPaymentTypebyValue('KLRNA')

  // wait until klarna iframe displays
  overlayPO.waitForOverlay()
  browser.waitForVisible('#klarna-credit-main')

  const paymentOptions = browser.element('#CardDetails')

  // set email address
  browser.waitForVisible('#CardDetails .Input-field-email')
  browser.setValue('#CardDetails .Input-field-email', process.env.USERNAME)

  paymentOptions.waitForEnabled('.Button')
  paymentOptions.click('.Button')

  if (checkoutPO.checkoutLogingPresent()) {
    checkoutPO.clickConfirmEmail()
    checkoutPO.enterPassword()
    checkoutPO.clickLogin()
  }
}
