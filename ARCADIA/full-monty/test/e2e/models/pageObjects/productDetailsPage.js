/* eslint no-else-return: 0 */
/* eslint no-unreachable: 0 */
import * as client from '../lib/client'
import * as shared from '../lib/shared'
import * as topNavBar from '../../models/newPageObjects/navigation/topNavigationBar'
import * as overlay from '../../models/newPageObjects/components/overlay'

const assert = require('assert')

const prodTitle = '.ProductDetail-title'
const productPrice = '.ProductDetail-row .HistoricalPrice .Price'
const sizeErrorMessage = '.is-error .Message-message'
const bundleErrorMessage = '.BundlesAddToBag-error'

export const isBundle = () => {
  return browser.isExisting('.Bundles')
}

export const checkProductName = (text) => {
  try {
    browser.waitForExist('.ProductDetail-title', 20000)
  } catch (err) {
    shared.captureScreenshotAndError(
      '[ERROR AVERSTION] product details not available, re-seraching',
      err,
      'FAILED-PRODUCT-PAGE-FROM-SEARCH'
    )
    topNavBar.search(process.env.SEARCH_TERM)
    browser.waitForExist('.ProductDetail-title', 20000)
  }
  const titleText = browser.getHTML('.ProductDetail-title')
  assert.ok(titleText.includes(text))
}

export const goBacktoProductDisplayPage = () => {
  const text = process.env.srujanvar
  const builtURL = `/en/tsuk/product/${text}`
  browser.url(builtURL)
  browser.waitForExist('.ProductDetail-title', 20000)
}

function getScale(str) {
  const matrix = str.match(/(-?[0-9.]+)/g)
  return parseFloat(matrix[0], 10)
}

export const zoomViewDisplayed = (zoomState) => {
  const transformProp = browser.getCssProperty(
    '.Carousel-item.is-selected .Carousel-image',
    'transform'
  )
  const zoomScale = getScale(transformProp.value)
  const zoomScaleRounded = parseInt(zoomScale, 10)
  browser.pause(1000)
  if (zoomState === 'in') {
    assert.notEqual(
      zoomScaleRounded,
      1,
      `Actual zoom scale ${zoomScaleRounded} should not match 1`
    )

    if (client.hasDesktopLayout() === false) {
      shared.notVisible('Carousel', 'arrow')
      shared.notVisible('Carousel', 'selectors')
    }
  } else {
    assert.equal(
      zoomScaleRounded,
      1,
      `Actual zoom scale: ${zoomScaleRounded}, expected 1`
    )

    if (client.hasDesktopLayout() === false) {
      assert.ok(browser.isVisible('.Carousel-arrow--left'))
      assert.ok(browser.isVisible('.Carousel-arrow--right'))
      assert.ok(browser.isVisible('.Carousel-selectors'))
    }
  }
}

export const AddToShoppingBag = () => {
  const bundleButtonClass = '.Button.BundlesAddAll-button'

  // click on add all to bag if bundle
  if (browser.isExisting(bundleButtonClass)) {
    // retrieve products
    const products = browser.$$('.BundleProducts-item')
    // eslint-disable-next-line no-restricted-syntax
    for (const item of products) {
      item.selectByIndex('.Select-container select', 1)
    }

    browser.waitForVisible(bundleButtonClass)
    browser.click(bundleButtonClass)
  } else {
    client.click('.AddToBag')
  }

  overlay.waitForOverlay()
}

export const confirmationMessageDisplayed = (message) => {
  client.waitForAddToBasket()
  const text = browser.getText('.AddToBag-modal p')
  assert.ok(text === message)
}

export const removeSpecialCharactersFromString = (string) => {
  const replacedString = string
    .replace(/[*]/g, ' ')
    .replace(/-/g, ' ')
    .replace(/ & /g, ' ')
    .replace(/'/g, '')
    .toLowerCase()
  return replacedString
}

export const getProductTitle = () => {
  const productUrl = browser.getUrl()

  // build regex to extract product title from url
  const regex = /^[\w/:]+\/([\w-]*)-\d*/
  let productTitle = regex.exec(productUrl)[1]
  productTitle = removeSpecialCharactersFromString(productTitle)
  return productTitle
}

export const confirmationMessageIncludesProductTitle = () => {
  const productTitle = getProductTitle()

  // removing special characters from modal text
  console.log(process.env.ADD_TO_BASKET_CONFIRM_TEXT)
  const text = removeSpecialCharactersFromString(
    process.env.ADD_TO_BASKET_CONFIRM_TEXT
  )

  assert.ok(text.indexOf(productTitle) !== -1)
}

export const confirmationIncludesText = (text) => {
  assert.ok(
    process.env.ADD_TO_BASKET_CONFIRM_TEXT.indexOf(text) !== -1,
    `${text} was not found in the confirm text: ${
      process.env.ADD_TO_BASKET_CONFIRM_TEXT
    }`
  )
}

export const confirmationIncludesSize = () => {
  // beware of case sensitivity when sending in parts of a message to check
  client.waitForAddToBasket()
  const modalText = browser.getText('.AddToBag-modal p')
  assert.ok(
    modalText.indexOf(process.env.SELECTED_PRODUCT_SIZE) !== -1,
    `${
      process.env.SELECTED_PRODUCT_SIZE
    } WAS NOT FOUND IN THE MODAL TEXT: ${modalText}`
  )
}

export const clickOnSize = (size) => {
  client.click(`li=${size}`)
}

export const productTitle = () => {
  process.env.SELECTED_PRODUCT_TITLE = browser.getText(prodTitle).trim()
  return process.env.SELECTED_PRODUCT_TITLE
}

export const getProductPrice = () => {
  process.env.SELECTED_PRODUCT_PRICE = browser.elements(productPrice).getText()
  return process.env.SELECTED_PRODUCT_PRICE
}

export const hasSizeSelectionBLock = () => {
  const sizeBlock = browser.element('.ProductDetail-sizeAndQuantity')
  assert.equal(true, sizeBlock.isVisible(), 'Size Block not displayed')
}

export const openSizeGuide = () => {
  browser.click('.SizeGuide-link')
}

export const hasSizeGuide = () => {
  browser.refresh()
  const image = browser.element('.ImageList-image')
  const sizeGuideImg = image.getElementSize('width')
  console.log(`Image Width : ${sizeGuideImg}`)
  assert.equal(true, sizeGuideImg > 0, 'Image Size Guide Not Displayed')
}

export const selectRandomQty = () => {
  if (browser.element('.ProductQuantity').isVisible()) {
    const qtyDropdown = browser.elements('#productQuantity option')
    const listOfQty = qtyDropdown.getAttribute('value')
    const randQty = listOfQty[Math.floor(Math.random() * listOfQty.length)]
    console.log(`[selectRandomQty] selected: ${randQty}`)
    client.selectByValue('#productQuantity', randQty)
    process.env.SELECTED_PRODUCT_QTY = randQty
    return process.env.SELECTED_PRODUCT_QTY
  }
}

export const selectHighestQty = () => {
  if (browser.element('.ProductQuantity').isVisible()) {
    const qtyDropdown = browser.elements('#productQuantity option')
    const listOfQty = qtyDropdown.getAttribute('value')
    const HighQty = listOfQty[listOfQty.length - 1]
    console.log(`Highest qty : ${HighQty}`)
    client.selectByValue('#productQuantity', HighQty)
    process.env.SELECTED_PRODUCT_QTY = HighQty
    return process.env.SELECTED_PRODUCT_QTY
  }
}

// - gets all the product sizes
// - checks that there is only one size
// - checks that the size displayed is the one received as argument
export const onlyVisibleSize = (size) => {
  browser.waitForVisible('.ProductDetail-title')
  const { value: availableSizes } = browser.elements(
    '.ProductSizes-list li.ProductSizes-item'
  )
  assert.equal(availableSizes.length, 1)
  const onlySizeAvailableId = availableSizes[0].ELEMENT
  const { value: onlySizeAvailableText } = browser.elementIdText(
    onlySizeAvailableId
  )
  assert.equal(onlySizeAvailableText, size)
}

export const sizeErrorMessageVisible = () => {
  if (isBundle()) {
    return browser.isExisting(bundleErrorMessage)
  } else {
    return browser.isExisting(sizeErrorMessage)
  }
}
