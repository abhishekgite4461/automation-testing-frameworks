import * as client from '../lib/client'
import * as shared from '../lib/shared'
import * as screenshots from '../lib/screenshots'
import * as deliveryManualAddress from '../newPageObjects/checkout/ManualAddress_po'
import * as deliveryPO from '../../models/newPageObjects/checkout/DeliveryBase_po'
import * as billingYourDetailPO from '../../models/newPageObjects/checkout/BillingYourDetails'
import * as PaymentMethodPO from '../../models/newPageObjects/checkout/CheckoutPaymentCard_po'
import * as overlayPO from '../newPageObjects/components/overlay'
import * as addToBagPO from '../newPageObjects/productDetail/addToBag'
import * as topBarPO from '../newPageObjects/components/topbar_po'
import * as shoppingBagPO from '../newPageObjects/shoppingBag/ShoppingBagBase'
import * as search from '../pageObjects/search'

const assert = require('assert')

export const hasPromoCode = (value) => {
  client.includesText('.PromotionCode-list', value)
}

export const hasValidationError = (value) => {
  let counter = 0
  while (
    shoppingBagPO.promoValidationMessagePresent() !== true &&
    shoppingBagPO.promoErrorMessagePresent() !== true &&
    counter < 50
  ) {
    browser.pause(100)
    counter++
  }
  const PromoText = shoppingBagPO.getPromoValidationText()
  assert.ok(
    PromoText === value,
    `${PromoText} did not match the expected ${value}`
  )
}

export const hasServerError = (value) =>
  client.includesText('.PromotionCode .Message-message', value)

export const addAnotherPromotionCode = () =>
  client.click('.PromotionCode-addText')

export const removePromotionCode = () => {
  client.click('.PromotionCode-removeText')

  let count = 0
  while (
    browser.isVisible('.PromotionCode-removeText') === true &&
    count < 100
  ) {
    browser.pause(100)
    count++
  }
}

export const hasNPromoCodes = (n) =>
  client.existsNTimes('.PromotionCode-code', n)

export const shoppingBagIconIndicatesProduct = () => {
  const numberOfProducts = topBarPO.getNumberOfProductsFromShoppingBagIcon()
  assert.ok(
    numberOfProducts === '1',
    `number of products did not equal 1, but instead got: ${numberOfProducts}`
  )
}

export const searchAndGoToPDP = (products = process.env.GOOD_PRODUCTS) => {
  let exitFlag = false
  const searchTerm = addToBagPO.getEnvironmentFriendlyProductList(products)
  search.searchProductByUrl(searchTerm)
  const shuffled = addToBagPO.getProductLinksFromPLP()
  console.log(shuffled.length)
  const searchResultLink = browser.getUrl()

  while (exitFlag === false && shuffled.length > 0) {
    console.log(`[ADD TO BAG] products left to try = ${shuffled.length}`)
    const selectedProductLink = shuffled.pop()
    console.log(selectedProductLink)
    const target = selectedProductLink.split('/')
    const pos = target.length - 1
    process.env.srujanvar = target[pos]
    console.log(`SET process.env.srujanvar = ${process.env.srujanvar}`)
    try {
      // navigate to PDP page
      const productUrl = selectedProductLink.replace(process.env.BASE_URL, '')
      browser.waitForVisible(`a[href='${productUrl}']`, 10000)
      browser.click(`a[href='${productUrl}']`)
      overlayPO.waitForOverlay()
      browser.waitForVisible('.Carousel-image', 10000)
      exitFlag = true
    } catch (err) {
      const errorMessage = `[ERROR AVERSION] failed to navigate to product details page: ${selectedProductLink}. Navigating back to ${searchResultLink} to try again`
      let consoleLink = browser.getUrl()
      consoleLink = consoleLink.replace('http://', '')
      consoleLink = consoleLink.replace('.com', '')
      consoleLink = consoleLink.replace('.', '-')
      consoleLink = `FAILED-TO-LOAD-PDP-${consoleLink}`
      shared.captureScreenshotAndError(errorMessage, err, consoleLink)
      browser.url(searchResultLink)
      exitFlag = false
    }
  }
}

export const goToSpecificProductDetailsPage = () => {
  const selectedProductLink = process.env.srujanvar
  console.log(`selectedProductLink : ${selectedProductLink}`)
  browser.getUrl(selectedProductLink)
  // const productUrl = selectedProductLink.replace(process.env.BASE_URL, '')
  // browser.waitForVisible(`a[href='${productUrl}']`, 10000)
  // browser.click(`a[href='${productUrl}']`)
  // overlayPO.waitForOverlay()
  // browser.waitForVisible('.Carousel-image', 10000)
  // exitFlag = true
}

export const goToProductPageWithOnlyOneSize = () => {
  searchAndGoToPDP(process.env.SINGLE_SIZE_PRODUCTS)
}

export const goToProductPageWithEightOrMoreSizes = () => {
  searchAndGoToPDP(process.env.MULTY_SIZE_PRODUCTS)
}

export const goToProductPageWithCarousel = () => {
  let exitFlag = false
  while (exitFlag === false) {
    searchAndGoToPDP(process.env.GOOD_PRODUCTS)
    exitFlag = browser.isVisible('.Carousel-arrow--right')
  }
}

export const goToRandomProductsPage = () => {
  searchAndGoToPDP(process.env.COLOUR_SERACH_PRODUCTS)
}

export const goTospecificProductsPage = () => {
  searchAndGoToPDP(process.env.COLOUR_SERACH_PRODUCTS)
}

export const containsNumberOfProducts = (numberOfProducts) => {
  let filtered = []

  overlayPO.waitForOverlay()
  browser.waitForVisible('.OrderProducts-product')

  // wait bit more as this might be an interim state, which will then display overlay again before merging the new items in
  overlayPO.waitForOverlay()

  const text = browser.getText('.OrderProducts .OrderProducts-product')

  // remove empty entries
  if (Array.isArray(text)) {
    filtered = text.filter((product) => {
      return product.length >= 1 && product !== ''
    })
  }

  assert.ok(
    filtered.length >= numberOfProducts,
    `${numberOfProducts} was expected but instead we got: ${
      filtered.length
    } and products: ${text}`
  )
}

export const waitForDeliveryCheckoutPage = () => {
  const selector = '.FindAddress select[name="country"]'
  try {
    let count = 0
    while (
      browser.isVisible('.ExpressDeliveryOptions .Select-select') !== true &&
      browser.isVisible('.DeliveryContainer-location') !== true &&
      browser.isVisible(selector) !== true &&
      count < 200
    ) {
      browser.pause(100)
      count++
    }
  } catch (err) {
    assert.ok(false, 'page did not laod, check screenshot')
  }
}

export const enterDefaultDeliveryInfo = () => {
  deliveryPO.enterDeliveryDetails(
    process.env.TITLE,
    process.env.BASE_FIRSTNAME,
    process.env.BASE_LASTNAME,
    process.env.TELEPHONE
  )
  deliveryManualAddress.enterManualAddress(
    process.env.BASE_COUNTRY,
    '111 testing street',
    'Testolpolis',
    process.env.BASE_POSTCODE,
    'Testolpolis'
  )
  deliveryPO.clickNextButton()
}

export const enterDefaultDeliveryInfoWithoutContinueing = () => {
  deliveryPO.enterDeliveryDetails(
    process.env.TITLE,
    process.env.BASE_FIRSTNAME,
    process.env.BASE_LASTNAME,
    process.env.TELEPHONE
  )
  deliveryManualAddress.enterManualAddress(
    process.env.BASE_COUNTRY,
    '111 testing street',
    'Testolpolis',
    process.env.BASE_POSTCODE,
    'Testolpolis'
  )
}

export const enterPaymentDetailsProvided = (cardType, cardNumber, cvv) => {
  PaymentMethodPO.setPaymentDetails(cardType.toUpperCase(), cardNumber, cvv)
}

export const enterDefaultPaymentInfo = () => {
  // add card details
  enterPaymentDetailsProvided('VISA', '4917610000000000', '123')
}

export const billingFieldsAreEmpty = () => {
  overlayPO.waitForOverlay()
  assert.ok(
    browser.getAttribute('.Input-field.Input-field-cvv', 'value') === '',
    'cvv is not blank'
  )
  assert.ok(
    browser.getAttribute('.Input-field.Input-field-cardNumber', 'value') === '',
    'card number is not blank'
  )
}

export const billingFieldsEmpty = () => {
  billingFieldsAreEmpty()
}

export const enterDefaultAmexPaymentInfo = () => {
  PaymentMethodPO.setPaymentDetails('AMEX', '343434343434343', '1234', 6, 2020)
}

export const collectFromStoreCheckout = () => {
  // enter collect from store delivery information
  deliveryPO.enterDeliveryDetails(
    process.env.TITLE,
    process.env.BASE_FIRSTNAME,
    process.env.BASE_LASTNAME,
    process.env.TELEPHONE
  )
  deliveryPO.clickNextButton()

  // enter billing details
  billingYourDetailPO.enterBillingDetails(
    process.env.TITLE,
    process.env.BASE_FIRSTNAME,
    process.env.BASE_LASTNAME,
    process.env.TELEPHONE,
    process.env.BASE_COUNTRY,
    process.env.BASE_POSTCODE
  )

  // enter default payment
  enterDefaultPaymentInfo()
}

export const realCheckout = () => {
  enterDefaultDeliveryInfo()
  enterDefaultPaymentInfo()
}

export const realCheckoutAmex = () => {
  enterDefaultDeliveryInfo()
  enterDefaultAmexPaymentInfo()
}

export const enter3DUkDeliveryAddress = () => {
  deliveryPO.enterDeliveryDetails(
    process.env.TITLE,
    '3D.',
    'AUTHORISED',
    process.env.TELEPHONE
  )
  deliveryManualAddress.enterManualAddress(
    process.env.BASE_COUNTRY,
    '111 testing street',
    'Testolpolis',
    process.env.BASE_POSTCODE,
    'Testolpolis'
  )
  deliveryPO.clickNextButton()
}

export const checkoutWithCustomCard = (cardType, cardNumber, cvv) => {
  enterPaymentDetailsProvided(cardType, cardNumber, cvv)
}

export const checkoutToDeliveryPage = () => {
  topBarPO.openShoppingBag()
  client.click('.MiniBag-continueButton')
  client.click('.LoginContainer-guestCheckout--proceed')
}

export const shoppingBagTotalEquals = (value) => {
  client.includesText('.MiniBag-bagTotal', value)
}

export const waitForPormotionToBeApplied = () => {
  overlayPO.waitForOverlay()
  browser.waitForVisible('.PromotionCode-codeDescription')
  overlayPO.waitForOverlay()
}

export const totalReducedBy = (reduction) => {
  const reducedCost = parseFloat(
    parseFloat(process.env.TOTAL_PRICE_BEFORE_DISCOUNT).toFixed(2) -
      parseFloat(reduction).toFixed(2)
  ).toFixed(2)
  const reducedCostText = reducedCost.toString()
  const totalPrice = shoppingBagPO.getTotalPrice()
  assert.ok(
    reducedCostText === totalPrice,
    `${totalPrice} did not match what was expected of: ${reducedCostText}`
  )
}

export const shoppingBagTotalPercentageDiscount = (discountPercentage) => {
  // wait for added promotion code to be checked and applied
  waitForPormotionToBeApplied()

  // make sure total is visible and then get totals for calculations on percentage discount
  browser.waitForVisible('.MiniBag-bagTotal')
  const basketTotal = shoppingBagPO.getTotalPrice()

  // convert to ints for easier maths
  const subTotalInt = parseFloat(
    process.env.TOTAL_PRICE_BEFORE_DISCOUNT
  ).toFixed(2)
  const percentageInt = discountPercentage
  // double check int converstion figures

  // work out the discount from the basket and the percentage
  const discount = (percentageInt * subTotalInt / 100).toFixed(2)
  // remove discount figure from subtotal and compare to basket total to check they match
  const expectedTotal = subTotalInt - discount
  const expectedTotalString = expectedTotal.toFixed(2).toString()
  assert.ok(
    expectedTotalString === basketTotal,
    `${expectedTotalString} did not match what was got from the basket total: ${basketTotal}`
  )
}

export const shoppingBagPromotionDiscountHasBeenRemoved = () => {
  const basketTotal = shoppingBagPO.getTotalPrice()
  assert.ok(
    basketTotal === process.env.TOTAL_PRICE_BEFORE_DISCOUNT,
    `${basketTotal} did not match the expected: ${
      process.env.TOTAL_PRICE_BEFORE_DISCOUNT
    }`
  )
}

export const isEmpty = () => {
  topBarPO.openShoppingBag()
  client.waitForBasketLoaded()
  const message = 'Your shopping bag is empty.'
  let count = 0
  browser.waitForVisible('.MiniBag p', 10000)
  while (browser.getText('.MiniBag p') !== message && count < 5) {
    count++
  }
  client.includesText('.MiniBag p', message)
}

export const hasEdit = () => {
  assert.ok(browser.isVisible('.OrderProducts-editText'))
}

export const textFieldsExist = (textfieldLookUp) => {
  browser.waitForVisible(textfieldLookUp)
  const result = browser.isVisible(textfieldLookUp)
  assert.ok(result, `${textfieldLookUp} was not visible`)
}

export const textFieldDoesNotExist = (textfieldLookUp) => {
  const result = browser.isVisible(textfieldLookUp)
  assert.ok(
    result === false,
    `${textfieldLookUp} was visible when expecteing it not to be`
  )
}

export const enterAddressManuallyLinkExists = () => {
  assert.ok(browser.isVisible('.FindAddress-link'))
}

export const findAddressLinExists = () => {
  assert.ok(browser.isVisible('.ButtonContainer-findAddress .Button'))
}

export const dropDownExist = (dropDownLookup) => {
  const result = browser.isVisible(dropDownLookup)
  assert.ok(result)
}

export const closeModal = () => {
  client.click('body')
}

export const changeQuantityAndSave = () => {
  let count = 0
  let exitFlag = false

  const options = shoppingBagPO.getQuantity()

  while (count < options.length && exitFlag === false) {
    shoppingBagPO.selectQuantity(count)
    shoppingBagPO.clickSaveProduct()

    if (process.env.BEFORE_EDIT_QUANTITY !== process.env.AFTER_EDIT_QUANTITY) {
      exitFlag = true
    } else {
      shoppingBagPO.clickEditProduct()
    }
    count++
  }
}

export const changeSizeAndSave = () => {
  let count = 0
  let exitFlag = false

  // retrieve available sizes
  const sizes = shoppingBagPO.getSize()

  // loops through available sizes
  while (count < sizes.length && exitFlag === false) {
    // attemps to select size that is not out of stock or low in stock
    if (
      sizes[count].includes('Out of stock') === false ||
      sizes[count].includes('Low stock') === false
    ) {
      shoppingBagPO.selectSize(count)
      shoppingBagPO.clickSaveProduct()
      if (process.env.BEFORE_EDIT_SIZE !== process.env.AFTER_EDIT_SIZE) {
        exitFlag = true
      } else {
        shoppingBagPO.clickEditProduct()
      }
    }
    count++
  }
}

export const completelyEmptyBag = () => {
  // wait until shopping bag is visible before clicking on empty bag
  browser.waitForVisible('.MiniBag')
  client.click('.EditBag-right')
  browser.waitForVisible('.Button.EditBag-emptyBag')
  client.click('.Button.EditBag-emptyBag')
  overlayPO.waitForOverlay()
  browser.deleteCookie('bagCount')
  if (browser.isVisible('button=OK')) {
    client.click('button=OK')
  }
}

export const dismissModal = () => {
  if (browser.isVisible('.Modal.is-shown.Modal--normal')) {
    client.click('.Modal-closeIcon')
    overlayPO.waitForOverlay()
  }
}

export const openAndEmptyBag = () => {
  dismissModal()
  overlayPO.waitForOverlay()
  topBarPO.openShoppingBag()
  browser.waitForVisible('.MiniBag .Button')
  const basketText = browser.getText('.MiniBag')
  if (basketText.indexOf('Your shopping bag is currently empty') !== -1) {
    // do nothing basket is empty
  } else {
    shoppingBagPO.emptyBag()
  }
}

export const clickConfirmEmpty = () => {
  const miniBag = '.MiniBag'
  const emptyBagModal = '.EditBag-modal'

  // wait until empty bag modal is visible
  browser.waitForVisible(emptyBagModal)

  // DEBUG: i should be able to see empty bag modal
  screenshots.takeScreenshot('confirmEmpty-before emptying bag')

  // confirm that i would like to empty bag
  client.click('.EditBag-emptyBag')

  // wait until shopping bag is closed
  while (browser.isVisibleWithinViewport(miniBag) === true) {
    browser.pause(100)
  }

  // DEBUG: bag should be closed and count 0
  screenshots.takeScreenshot('confirmEmpty-after emptying bag')

  // bag count should be zero
  let bagCount = browser.getHTML('.Header-shoppingCartBadgeIcon', false)
  bagCount = parseInt(bagCount, 10)

  return assert.ok(bagCount === 0)
}

export const cancelEmpty = () => {
  browser.waitForVisible('.Modal-closeIcon')
  // this needs to pause after seeing it visible, as the is visible is
  // thrown as soon as it shows on the page and this could be before
  // it is completely rendered and causes flakiness.
  browser.pause(500)
  client.click('.Modal-closeIcon')
}

export const setDeliveryTypeInShoppingBag = (deliveryOption) => {
  // open shopping bag
  topBarPO.openShoppingBag()

  // select delivery option
  shoppingBagPO.selectDeliveryOption(deliveryOption)

  // guest checkout
  client.click('.MiniBag-continueButton')
  overlayPO.waitForOverlay()
  if (browser.isVisible('.LoginContainer-guestCheckout--proceed')) {
    client.click('.LoginContainer-guestCheckout--proceed')
    overlayPO.waitForOverlay()
    waitForDeliveryCheckoutPage()
  }
}

export const confirmPagePreFilledDefaultAddresses = () => {
  waitForDeliveryCheckoutPage()
  // get the selected delivery option for later checking if present
  if (browser.isVisible('.ExpressDeliveryOptions .Select-select') === true) {
    process.env.DELIVERY_DAY_BEFORE_CHANGE = browser.getValue(
      '.ExpressDeliveryOptions .Select-select'
    )
  }
  const deliveryAddress = browser.getText('.delivery.Details-addressDetails')
  assert.ok(
    deliveryAddress.indexOf(process.env.BASE_FIRSTNAME) !== false,
    `${process.env.BASE_FIRSTNAME} expected first name does not match`
  )
  assert.ok(
    deliveryAddress.indexOf(process.env.BASE_LASTNAME) !== false,
    `${process.env.BASE_LASTNAME} expected last name does not match`
  )
  assert.ok(
    deliveryAddress.indexOf(process.env.BASE_POSTCODE) !== false,
    `${process.env.BASE_POSTCODE} expected postcode does not match`
  )
  assert.ok(
    deliveryAddress.indexOf('Testolpolis') !== false,
    'address 2 does not match'
  )
  assert.ok(
    deliveryAddress.indexOf('111 testing street') !== false,
    'address 1 does not match'
  )

  const billingAddress = browser.getText('.billing.Details-addressDetails')
  assert.ok(
    billingAddress.indexOf(process.env.BASE_FIRSTNAME) !== false,
    `${process.env.BASE_FIRSTNAME} expected first name does not match`
  )
  assert.ok(
    billingAddress.indexOf(process.env.BASE_LASTNAME) !== false,
    `${process.env.BASE_LASTNAME} expected last name does not match`
  )
  assert.ok(
    billingAddress.indexOf(process.env.BASE_POSTCODE) !== false,
    `${process.env.BASE_POSTCODE} expected postcode does not match`
  )
  assert.ok(
    billingAddress.indexOf('Testolpolis') !== false,
    'address 2 does not match'
  )
  assert.ok(
    billingAddress.indexOf('111 testing street') !== false,
    'address 1 does not match'
  )
}

export const expressDeliveryOptionUpdated = () => {
  assert.ok(
    process.env.DELIVERY_DAY_AFTER_CHANGE !==
      process.env.DELIVERY_DAY_BEFORE_CHANGE,
    `Delivery day is still set as ${
      process.env.DELIVERY_DAY_AFTER_CHANGE
    } and was not updated`
  )
}

export const setDeliveryDayOnDeliveryPage = () => {
  waitForDeliveryCheckoutPage()
  process.env.DELIVERY_DAY_BEFORE_CHANGE = browser.getValue(
    '.ExpressDeliveryOptions .Select-select'
  )
  browser.waitForVisible('.ExpressDeliveryOptions')
  browser.waitForVisible('.ExpressDeliveryOptions .Select-select')
  browser.selectByIndex('.ExpressDeliveryOptions .Select-select', 3)
  overlayPO.waitForOverlay()
}

export const updateExpressDeliveryOptionOnCheckoutConfirmPage = () => {
  browser.waitForVisible('.Totals-delivery--changeText')
  browser.click('.Totals-delivery--changeText')
  browser.waitForVisible('.ExpressDeliveryOptions')
  browser.waitForVisible('.ExpressDeliveryOptions .Select-select')
  browser.selectByIndex('.ExpressDeliveryOptions .Select-select', 4)
  overlayPO.waitForOverlay()
  process.env.DELIVERY_DAY_AFTER_CHANGE = browser.getValue(
    '.ExpressDeliveryOptions .Select-select'
  )
  confirmPagePreFilledDefaultAddresses()
}

export const updateConfirmationDeliveryOptionAndcheckout = () => {
  browser.waitForVisible('.ExpressDeliveryOptions')
  browser.waitForVisible('.ExpressDeliveryOptions .Select-select')
  browser.selectByIndex('.ExpressDeliveryOptions .Select-select', 4)
  overlayPO.waitForOverlay()
  process.env.DELIVERY_DAY_AFTER_CHANGE = browser.getValue(
    '.ExpressDeliveryOptions .Select-select'
  )
  confirmPagePreFilledDefaultAddresses()
}

export const updateCheckoutDeliveryOptionAndCheckoutToConfirm = () => {
  // wait for and navigate to the change delivery link
  browser.waitForVisible(
    '.delivery.Details-address .delivery.Details-changeLink'
  )
  browser.click('.delivery.Details-address .delivery.Details-changeLink')

  browser.click('#delivery-option-home_standard + span')
  overlayPO.waitForOverlay()

  // click next to go to confirmation
  browser.click('.Button.DeliveryContainer-nextButton')
  overlayPO.waitForOverlay()
}
