import * as shared from '../lib/shared'
import * as client from '../lib/client'
import * as forms from '../lib/_forms'
import * as shoppingBag from '../pageObjects/shoppingBag'
import * as PaymentMethodPO from '../newPageObjects/checkout/CheckoutPaymentCard_po'
import * as checkoutLogin from '../newPageObjects/checkout/checkoutLogin'
import * as newLoginPO from '../newPageObjects/login_po'
import * as newRegisterPO from '../newPageObjects/registration_po'
import * as delivery from './delivery'
import * as overlayPO from '../newPageObjects/components/overlay'
import * as addToBagPO from '../newPageObjects/productDetail/addToBag'
import * as myAccountPO from '../newPageObjects/myAccount/myAccount_po'
import * as myDetailsPO from '../newPageObjects/myAccount/myDetails_po'
import * as checkoutDetailsPO from '../newPageObjects/myAccount/checkoutDetails_po'
import * as orderHistoryPO from '../newPageObjects/myAccount/orderHistory_po'
import * as orderHistoryDetailsPO from '../newPageObjects/myAccount/orderHistoryDetails_po'
import * as changePasswordPO from '../newPageObjects/myAccount/myPassword_po'

// identifiers
//-------------------------------

// page element identifiers
const orderHistoryDetailsPrefixClass = 'OrderHistoryDetailsAddress-'
const detailsPaymentClass = 'OrderHistoryDetailsPayment'
const assert = require('assert')
const l = require('../lib/localisation').init()

const myDetailsForm = 'MyAccount-wrapper'

/* Given */
export const goToMyDetailsPage = () => {
  myDetailsPO.goToMyDetails()

  const actualPageHeader = myDetailsPO.getPageHeader()
  const expectedPageHeader = l`My details`

  browser.waitUntil(
    myDetailsPO.getEmailValue() !== '',
    '[goToMyDetailsPage] Email input is still blank'
  )

  console.log(
    `[goToMyDetailsPage] email address: '${myDetailsPO.getEmailValue()}'`
  )

  assert.equal(
    expectedPageHeader.toLowerCase(),
    actualPageHeader.toLowerCase(),
    `'${actualPageHeader}' did not match expected page header '${expectedPageHeader}'`
  )
}

export const orderLimitMessagePresent = () => {
  const expectedOrderLimitMessage =
    'Displaying your last 20 orders. For your full order history, please contact customer service at 0344 984 0264. For international customers, please call +44 344 984 0264.'

  // MF-175 disabled order history message feature for now until the next black friday
  if (shared.getFeatureValue('FEATURE_ORDER_HISTORY_MSG') === 'true') {
    const actualOrderLimitMessage = orderHistoryPO.getOrderLimitMessage()
    assert.ok(
      actualOrderLimitMessage.includes(expectedOrderLimitMessage),
      `'${expectedOrderLimitMessage}' should be included within '${actualOrderLimitMessage}'`
    )
  }
}

export const openOrderHistoryAndConfirmPreviousOrder = () => {
  // access My Orders
  orderHistoryPO.goToMyOrderHistory()

  // verify order limit message displayed
  orderLimitMessagePresent()

  // select an order
  orderHistoryPO.selectOrdersHistory()
}

export const orderHistoryOrderHasDefaultAddresses = () => {
  // requires being on order history order details page before running
  const billingAddress = orderHistoryDetailsPO.getBillingAddressText()
  assert.ok(
    billingAddress.indexOf(process.env.BASE_FIRSTNAME) !== -1,
    `${billingAddress}billing address first name contained unexpected line: ${
      process.env.BASE_FIRSTNAME
    }`
  )
  assert.ok(
    billingAddress.indexOf('Testolpolis') !== -1,
    `${billingAddress}billing address contained unexpected line: testopolis`
  )
  assert.ok(
    billingAddress.indexOf(process.env.BASE_COUNTRY) !== -1,
    `${billingAddress}billing address contained unexpected line country: ${
      process.env.BASE_COUNTRY
    }`
  )

  if (process.env.ENVIRONMENT !== 'stage_international') {
    // international do not include the post/zip code in the address lines.
    assert.ok(
      billingAddress.indexOf(process.env.BASE_POSTCODE) !== -1,
      `${billingAddress}billing address contained unexpected linepost code: ${
        process.env.BASE_POSTCODE
      }`
    )
  }

  const deliveryAddress = orderHistoryDetailsPO.getDeliveryAddressText()
  assert.ok(
    deliveryAddress.indexOf(process.env.BASE_FIRSTNAME) !== -1,
    `${deliveryAddress}shipping address first name contained unexpected line: ${
      process.env.BASE_FIRSTNAME
    }`
  )
  assert.ok(
    deliveryAddress.indexOf('Testolpolis') !== -1,
    `${deliveryAddress}shipping address contained unexpected line: testopolis`
  )
  assert.ok(
    deliveryAddress.indexOf(process.env.BASE_COUNTRY) !== -1,
    `${deliveryAddress}shipping address contained unexpected line country: ${
      process.env.BASE_COUNTRY
    }`
  )
  if (process.env.ENVIRONMENT !== 'stage_international') {
    // international do not include the post/zip code in the address lines.
    assert.ok(
      deliveryAddress.indexOf(process.env.BASE_POSTCODE) !== -1,
      `${deliveryAddress}shipping address contained unexpected linepost code: ${
        process.env.BASE_POSTCODE
      }`
    )
  }
}

export const ConfirmMyDetailsContainDefaultAddressesSaved = () => {
  checkoutDetailsPO.goToCheckoutDetails()
  assert.ok(
    checkoutDetailsPO.getFirstNameValue() === process.env.BASE_FIRSTNAME,
    `${checkoutDetailsPO.getFirstNameValue()} does not match the expected name of ${
      process.env.BASE_FIRSTNAME
    }`
  )
  assert.ok(
    checkoutDetailsPO.getLastNameValue() === process.env.BASE_LASTNAME,
    `${checkoutDetailsPO.getLastNameValue()} does not match the expected name of ${
      process.env.BASE_LASTNAME
    }`
  )
  assert.ok(
    checkoutDetailsPO.getPostcodeNameValue() === process.env.BASE_POSTCODE,
    `${checkoutDetailsPO.getPostcodeNameValue()} does not match the expected postcode of ${
      process.env.BASE_POSTCODE
    }`
  )
  assert.ok(
    checkoutDetailsPO.getLine2NameValue() === 'Testolpolis',
    `${checkoutDetailsPO.getLine2NameValue()} does not match the expected line 2 of Testopolis`
  )
  assert.ok(
    checkoutDetailsPO.getLine1NameValue() === '111 testing street',
    `${checkoutDetailsPO.getLine1NameValue()} does not match the expected line 1 of 111 testing street`
  )
}

export const confirmPaymentOptionsPresentInMyCheckOutDetails = () => {
  checkoutDetailsPO.goToCheckoutDetails()
  assert.ok(
    checkoutDetailsPO.getPaymentType().length > 0,
    `There are no payment options available in my check details`
  )
}

export const clickMyCheckoutDetails = () =>
  shared.clickItemByText('My checkout details', 'AccountItem', 'title')

export const openLoginPage = () => {
  client.openPage('/login')
  overlayPO.waitForOverlay()
}

export const login = (
  username = 'myaccount@example.com',
  password = 'password1'
) => {
  newLoginPO.loginAsUser(username, password)
  if (newLoginPO.loginPagePresent()) {
    console.log(
      `error message from failed login if present: ${newLoginPO.getLoginErrorText()}`
    )
  }
  overlayPO.waitForOverlay()
}

export const authenticated = () => {
  openLoginPage()
  login()
}

export const authenticatedWithCheckout = () => {
  openLoginPage()
  const username = 'myaccount@example.com'
  const password = 'password1'
  login(username, password)
}

export const loginWithoutOpeningMenu = (username, password) => {
  login(username, password)
}

export const openLoginPageAndLogin = (username, password) => {
  openLoginPage()
  login(username, password)
}

export const openLoginPageAndLoginAsPreviouslyRegisteredUser = () => {
  openLoginPageAndLogin(process.env.USERNAME, process.env.PASSWORD)
}

export const createRandomEmailAddress = () => {
  const todaysDate = new Date().toJSON().replace(/\W+/g, '')
  const emailAddress = `${Math.random()
    .toString(36)
    .substring(7)}@${todaysDate}.com`
  console.log(
    `[MY_ACCOUNT] created email address for registration: ${emailAddress}`
  )
  return emailAddress
}

export const registerUser = (username = createRandomEmailAddress()) => {
  // because registering a new user can fail with a 422 without warning at a cule of stages
  // either registered or not registered. if it fails for any reason we regenerate a new email address and try again
  // we will retry this 3 times before failing the test
  // to note we also never try and register the email 'myaccount@example.com' if it is passed
  // this account has some serious issues on various environments

  let count = 0
  let exitFlag = false

  while (exitFlag === false && count < 3) {
    count++
    if (username !== 'myaccount@example.com') {
      try {
        newRegisterPO.registerNewUser(username, 'password1')
        myAccountPO.getMyAccountContainer()
      } catch (err) {
        if (browser.isVisible('.Message.is-shown.is-error')) {
          const pageErrorMessage = `[INFO] couldnt create account, for the following reason: ${browser.getText(
            '.Message.is-shown.is-error'
          )}`
          const screenshotName = `FAILED_REGISTRATION_AFTER_LOGIN_ATTEMPT_NUMBER_${count.toString()}-`
          shared.captureScreenshotAndError(
            pageErrorMessage,
            err,
            screenshotName
          )
          // regenerating email address for new attempt
          username = createRandomEmailAddress()
        }
      }
      // save that in ENV
      process.env.USERNAME = username
      process.env.PASSWORD = 'password1'
      exitFlag = true
    } else {
      exitFlag = true
    }
  }
  if (count === 3) {
    assert.ok(
      false,
      'FAILING AFTER 3 FAILED REGISTRATION ATTEMPTS, CHECK SCREENSHOTS'
    )
  }
}

export const registerRandomUser = () => {
  const randomEmail = createRandomEmailAddress()
  registerUser(randomEmail)
}

/**
 * Register a new Klarna user
 *
 * Status:
 * <email>@emaildomain.com - approved account
 * <email>_require_signup@emaildomain.com - require sign up
 * <email>_red@emaildomain.com - fail pre-auth
 */
export const registerRandomKlarnaUser = (status) => {
  const suffixes = {
    approved: '',
    requireSignUp: '_require_signup',
    failsPreAuth: '_red',
  }
  let email = createRandomEmailAddress()

  // determine email suffix, defaults to no suffixes. Klarna will treat this as an approved account
  const emailSuffix = suffixes[status] || ''

  // insert suffix to email address
  email =
    email.slice(0, email.indexOf('@')) +
    emailSuffix +
    email.slice(email.indexOf('@'))

  // register user
  registerUser(email)
}

export const registerRandomFullProfileUser = (cardType, cardNumber, cvv) => {
  const is3DSecure = cardType.toUpperCase() === '3D'

  if (is3DSecure) {
    cardType = 'VISA'
  }

  // register random user
  registerRandomUser()
  // go through checkout process to get a full profile user
  // add a product
  addToBagPO.addRandomProduct()
  // navigate to delivery
  checkoutLogin.checkout()
  // fill in delivery address
  if (is3DSecure) {
    shoppingBag.enter3DUkDeliveryAddress()
  } else {
    shoppingBag.enterDefaultDeliveryInfo()
  }
  // fill in credit card information
  PaymentMethodPO.setPaymentDetails(cardType.toUpperCase(), cardNumber, cvv)
  // confirm and pay
  delivery.confirmOrder()
  // verify that payment was successful (user redirected to success page, not doing anything fancy...)
  if (is3DSecure) {
    delivery.threeDSecureAuthenticationFlow()
  } else {
    // TODO reconfirm if this should be commented out
    // delivery.confirmationPagePresent()
  }
}

export const loginAndEmptyBasket = (username, password) => {
  openLoginPage()
  login(username, password)
  try {
    overlayPO.waitForOverlay()
    if (browser.isVisible('.Modal.is-shown.Modal--warning')) {
      client.click('.Button.CheckoutContainer-viewBag')
      overlayPO.waitForOverlay()
      shoppingBag.completelyEmptyBag()
    }
  } catch (err) {
    shared.captureScreenshotAndError(
      'failed login and clear bag',
      err,
      'FAILED-LOGIN-AND-CLEAR-BAG-'
    )
  }
}

export const openLoginPageAndLoginAndEmptyBasket = (username, password) => {
  openLoginPage()
  login(username, password)
  shoppingBag.openAndEmptyBag()
}

/**/

/* When */
export const selectOptionFromMyAccountList = (text) =>
  shared.clickItemByText(text, 'AccountItem', 'title')

const updatePassword = (
  oldPassword,
  newPassword,
  newPasswordConfirm = newPassword
) => {
  changePasswordPO.updatePassword(oldPassword, newPassword, newPasswordConfirm)
}

export const changeCurrentPassword = () => {
  updatePassword('password1', 'password2')
}

export const changePasswordUnequalValues = () => {
  updatePassword('password1', 'newPassword1', 'newPassword2')
  client.click('.Input-field.Input-field-oldPassword')
}

export const clickMyAccount = () => {
  browser.waitForVisible('a=My Account')
  client.click('a=My Account')
}

export const navigateToMyAccount = () => {
  myAccountPO.goToMyAccount()
}

export const submitForgotPasswordFormNotRegistered = () => {
  client.click('.Accordion-icon')
  forms.setFormInput(
    'fake-account-not-registered@test.com',
    'email',
    'ForgetPassword-form'
  )
  return client.click('.ForgetPassword-button')
}

export const enterIncorrectCurrentPassword = () =>
  updatePassword('wrongpassword1', 'password1')

export const enterShortNewPassword = () => updatePassword('password1', 'pass')

export const enterCurrentPasswordAsNewPassword = () =>
  updatePassword('password1', 'password1')

export const enterUsernameAsPassword = () =>
  updatePassword('password1', 'myaccount1@test.com')

export const enterEmailAsPassword = (email) => {
  updatePassword('password1', email)
}

export const enterEmailAddress = (email) => {
  const emailAddress = email === 'EMPTY' ? '' : email
  forms.setFormInput(emailAddress, 'email', 'Login-form')
  return browser.keys('\t')
}

export const enterPassword = (pass) => {
  const password = pass === 'EMPTY' ? '' : pass
  forms.setFormInput(password, 'password', 'Login-form')
  return browser.keys('\t')
}

export const unregisteredAccountLogin = () => {
  browser.refresh()
  client.openPage('/login')
  const email = createRandomEmailAddress()
  newLoginPO.loginAsUser(email, 'password1', true)
  return newLoginPO.getLoginErrorText()
}

export const enterRegistrationFirstPassword = () => {
  newRegisterPO.enterEmail('default_email@test.com')
  newRegisterPO.enterPassword('pass')
  newRegisterPO.enterConfirmationPassword('pass')
}

export const enterRegistrationEmail = () => {
  newRegisterPO.enterEmail('default_email@test.com')
  newRegisterPO.uncheckWeeklyEmailCheckbox()
  newRegisterPO.enterPassword(['\t'])
  newRegisterPO.enterConfirmationPassword(['\t'])
}

export const enterIncorrectFormatRegistrationEmail = () => {
  newRegisterPO.enterEmail('default_emailtest.com')
  newRegisterPO.enterPassword('password1')
}

export const enterBlankRegistrationEmail = () => {
  newRegisterPO.enterBlankEmail()
  newRegisterPO.enterPassword('password1')
}

export const enterUsedRegistrationEmail = () => {
  newRegisterPO.enterEmail('duplicate@example.com')
  newRegisterPO.enterPassword('password1')
  newRegisterPO.enterConfirmationPassword('password1')
  newRegisterPO.clickRegisterButton()
}

export const enterMyAccountDetails = (title, firstName, lastName, email) => {
  myDetailsPO.selectTitle(title)
  myDetailsPO.setFirstNameValue(firstName)
  myDetailsPO.setLastNameValue(lastName)
  myDetailsPO.setEmailValue(email)
  browser.keys('\t')
}

export const enterMyAccountDetailsWithBlankFirstName = () => {
  enterMyAccountDetails('Dr', ' ', 'Khaliquee', 'myaccount1@test.com')
}

export const enterMyAccountDetailsWithBlankLastName = () =>
  enterMyAccountDetails('Dr', 'Faisal', ' ', 'myaccount1@test.com')

export const enterMyAccountDetailsWithBlankEmail = () =>
  enterMyAccountDetails('Dr', 'Faisal', 'Khaliquee', ' ')

export const enterMyAccountDetailsWithInvalidEmail = () =>
  enterMyAccountDetails('Dr', 'Faisal', 'Khaliquee', 'invalidemail')

export const drillDownOrder = () => {
  client.click('.OrderElement')
}

export const showEnteredPassword = () => {
  newLoginPO.showEnteredPassword()
}

export const changePasswordFormVisible = () => {
  changePasswordPO.changePasswordFormElementsPresent()
}

const revertPasswordChange = () => {
  changePasswordPO.goToMyPassword()
  updatePassword('password2', 'password1')
}

export const changePasswordSuccessMessageVisible = () => {
  assert.ok(
    changePasswordPO.getConfirmationMessage() ===
      l`Your password has been successfully changed.`
  )
  revertPasswordChange()
  browser.waitForVisible('.Message.is-shown.is-confirm')
}

export const myAccountSectionIsShown = (linkText) => {
  const textToVerify = linkText.toLowerCase()
  browser.waitForVisible('.AccountItem-title')
  const textFromPage = browser.getText(`h3=${linkText}`).toLowerCase()
  assert.ok(textToVerify === textFromPage)
}

export const unequalPasswordsMessageVisible = () => {
  const confirmPasswordErrorId = '#newPasswordConfirm-error'

  browser.waitForVisible(confirmPasswordErrorId)

  const expected = l`Please ensure that both passwords match.`
  const actual = browser.getText(confirmPasswordErrorId)

  assert.equal(expected, actual, `'${expected}' did not match '${actual}'`)
}

export const isOnLoginPage = () => {
  assert.ok(newLoginPO.loginPagePresent(), 'the login page was not present')
}

export const isOnMyAccountPage = () =>
  client.urlPathnameIsEqualTo('/my-account')
export const isOnOrderDetailsPage = () =>
  client.urlPathnameMatchesPattern(/\/my-account\/order-history\/.*/)

export const forgotPasswordSuccessMessageVisible = () => {
  const selector = '.ForgetPassword-form .Message-message'
  const message =
    'Your password has been reset successfully. A new password has been e-mailed to you and should arrive shortly'
  browser.waitForVisible(selector)
  const messageText = browser.getText(selector)

  assert.ok(
    messageText === message,
    `${messageText} did not match expected ${message}`
  )
}

export const forgotPasswordErrorMessageEmailNotFound = () => {
  const selector =
    'p.Message-message=There is no account with that email address in our records. If you would like to use that email address, you can create an account with the New customers section of this page.'
  return shared.getText(selector)
}

export const incorrectPasswordErrorVisible = () => {
  return shared.getText('p.Message-message=Password needs to be changed')
}

export const newPasswordTooShortErrorVisible = () =>
  shared.getText(`span=${l`Please enter a password of at least 6 characters.`}`)

export const newPasswordEqualToCurrentErrorVisible = () =>
  shared.getText(
    `span=${l`Your new password can't be the same as your previous password.`}`
  )

export const newPasswordEqualToUsernameErrorVisible = () =>
  shared.getText(
    `span=${l`Please ensure that your password does not contain your email address.`}`
  )

export const incorrectEmailAddressMessageVisible = (message) =>
  forms.formInputValidationEquals('email', message, 'Login-form')

export const emailOrPasswordErrorMessageVisible = () => {
  const selector =
    'p.Message-message=The email address or password you entered has not been found. Please enter them again.'
  return shared.getText(selector)
}

export const navigateToMyOrders = () => {
  orderHistoryDetailsPO.goToOrderHistoryPage()
}
export const myOrdersTitle = () => {
  return orderHistoryDetailsPO.getPageHeader()
}
export const myOrdersEmpty = () => {
  return orderHistoryDetailsPO.getEmptyOrderHistoryText()
}

export const incorrectPasswordMessageVisible = (message) =>
  forms.formInputValidationEquals('password', message, 'Login-form')

export const registrationPassUnderSixChar = () => {
  return forms.formInputValidationEquals(
    'password',
    l`Please enter a password of at least 6 characters.`,
    'Register-form'
  )
}

export const registrationPasswordNotEnteredValidation = () => {
  return forms.formInputValidationEquals(
    'password',
    l`A password is required.`,
    'Register-form'
  )
}

export const registrationIncorrectEmailFormatVaidation = () => {
  return forms.formInputValidationEquals(
    'email',
    l`Please enter a valid email address.`,
    'Register-form'
  )
}

export const registrationBlankEmailValidation = () => {
  return forms.formInputValidationEquals(
    'email',
    l`An email address is required.`,
    'Register-form'
  )
}

export const EmailAlreadyRegisteredValidation = () => {
  const message = shared.getText(
    'p=An account with that email address already exists'
  )
  assert.equal(message, 'An account with that email address already exists')
}

export const newToTopshopHeaderMessage = () => {
  const headerMessage = shared.getText('h3=New Customer?')
  assert.ok(
    headerMessage === 'new customer?',
    `${headerMessage} did not match the expected of new customer?`
  )
}

export const newToBrandWebsiteHeaderMessage = () => {
  let headerMessage = shared.getText('h3=New Customer?')
  headerMessage = headerMessage.toLowerCase()
  assert.ok(
    headerMessage === 'new customer?',
    `${headerMessage} did not match the expected of new customer?`
  )
}

export const NewsletterOptionPresent = () => {
  assert.ok(
    newRegisterPO.weeklyNewsletterIsPresent(),
    'newsletter option was not present'
  )
}

export const newsletterOptionChecked = () => {
  assert.ok(
    newRegisterPO.weeklyNewsletterIsChecked(),
    'newsletter option was not checked'
  )
}

export const newsletterOptionUnChecked = () => {
  assert.ok(
    !newRegisterPO.weeklyNewsletterIsChecked(),
    'newsletter option was checked'
  )
}

export const hasExistingMyAccountDetailsPrefilled = () => {
  forms.selectedOptionIs('title', 'Mr', myDetailsForm)
  forms.formInputTextEquals('firstName', 'Faisal', myDetailsForm)
  forms.formInputTextEquals('lastName', 'Khaliquee', myDetailsForm)
  return forms.formInputTextEquals(
    'email',
    'myaccount-test2@arcadiatest.com',
    myDetailsForm
  )
}

export const setMyAccountDefaultValues = () => {
  goToMyDetailsPage()
  let counter = 0
  let myAccountSection = browser.element('.CustomerShortProfile')
  while (
    myAccountSection.getValue(`.${myDetailsForm} input[name="firstName"]`) !==
      'Faisal' &&
    counter < 100
  ) {
    myAccountSection = browser.element('.CustomerShortProfile')
    counter += 1
    browser.pause(100)
    console.log(
      myAccountSection.getValue(`.${myDetailsForm} input[name="firstName"]`)
    )
  }

  // this will only happen if the previous segment gets to its max count of 100
  if (counter === 100) {
    myDetailsPO.setFirstNameValue('Faisal')
    myDetailsPO.setLastNameValue('Khaliquee')
    myDetailsPO.saveProfileChanges()
    browser.waitForVisible('.Message.is-shown.is-confirm')
  }
  client.click('.AccountHeader-row > .NoLink')
}

export const myDetailsBlankfirstNameValidation = () =>
  forms.formInputValidationEquals(
    'firstName',
    'This field is required',
    myDetailsForm
  )

export const myDetailsBlanklastNameValidation = () =>
  forms.formInputValidationEquals(
    'lastName',
    'This field is required',
    myDetailsForm
  )

export const myDetailsBlankEmailValidation = () =>
  forms.formInputValidationEquals(
    'email',
    'An email address is required.',
    myDetailsForm
  )

export const myDetailsInvalidEmailValidation = () =>
  forms.formInputValidationEquals(
    'email',
    'Please enter a valid email address.',
    myDetailsForm
  )

export const hasBackToMyAccountLink = () =>
  client.includesText('.MyAccountSubcategory .NoLink', 'Back to My Account')

export const orderNumberIsVisible = () => {
  return client.textMatchesPattern(
    '.OrderHistoryElement-orderId',
    /Order number \d+/
  )
}

export const orderDateIsVisible = () => {
  browser.pause(2000)
  const orderDatePattern = /\d{2}\s+(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)[^\s]+\s+\d{4}/
  overlayPO.waitForOverlay()
  const orderInfo = browser.getText('.OrderElement dd.OrderElement-listItem')
  return (
    (Array.isArray(orderInfo) ? orderInfo : [orderInfo]).filter((val) =>
      orderDatePattern.test(val)
    ).length >= 1
  )
}

export const orderTotalIsVisible = () => {
  const priceRegEx =
    '[$£]{1}(\\d+\\.|\\.)?\\d{0,2}|(\\d+\\.|\\.)?\\d{0,2}[€]{1}'
  const orderInfo = browser.getText('.OrderElement')
  if (typeof orderInfo === 'string') {
    return new RegExp(priceRegEx).test(orderInfo)
  }
  return (
    orderInfo.filter((val) => {
      return new RegExp(priceRegEx).test(val)
    }).length === orderInfo.length
  )
}

export const productImageIsVisible = () => {
  return shared.isVisible('OrderHistoryDetailsElement img')
}
export const deliverySectionIsVisible = () => {
  return shared.isVisible(orderHistoryDetailsPrefixClass, 'delivery')
}
export const billingAddressSectionIsVisible = () => {
  return shared.isVisible(orderHistoryDetailsPrefixClass, 'billing')
}
export const shippingAddressSectionIsVisible = () => {
  return shared.isVisible(
    `${orderHistoryDetailsPrefixClass}-delivery .OrderHistoryDetailsAddress`,
    'details'
  )
}
export const paymentDetailsSectionIsVisible = () => {
  return shared.isVisible(detailsPaymentClass, 'body')
}
export const itemCodeIsPresent = () => {
  return client.textMatchesPattern(
    '.OrderHistoryDetailsElement-productCode',
    /Item code - \w+/
  )
}
export const priceIsPresent = () => {
  return client.textMatchesPattern(
    '.OrderHistoryDetailsElement-price',
    /Price: \w+/
  )
}
export const quantityIsPresent = () => {
  return client.textMatchesPattern(
    '.OrderHistoryDetailsElement-quantity',
    /Quantity: \w+/
  )
}
export const trackingNumberIsPresent = () => {
  return client.textMatchesPattern(
    '.OrderHistoryDetailsDelivery-trackingNumber',
    /Tracking number: \d+/
  )
}
export const starCardNumberIsPresent = () => {
  return client.textMatchesPattern(
    '.OrderHistoryDetailsPayment-cardNumber',
    /\*+\d+/
  )
}
export const totalIsPresent = () => {
  return client.textMatchesPattern(
    '.OrderHistoryDetailsPayment-totalCost',
    /Total: £\d+/
  )
}
export const passwordIsShown = () => {
  return newLoginPO.isPasswordShown()
}
