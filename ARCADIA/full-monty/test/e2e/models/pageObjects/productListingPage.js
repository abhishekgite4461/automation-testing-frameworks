/* globals browser:false */
/* eslint no-constant-condition: 0 */
import * as client from '../lib/client'
import * as shared from '../lib/shared'
import * as screenshot from '../lib/screenshots'
import * as overlay from '../newPageObjects/components/overlay'
import * as filterRefinements from '../newPageObjects/productListing/filterRefinements'
import * as filters from '../newPageObjects/productListing/filters'
import * as addToBagPO from '../newPageObjects/productDetail/addToBag'

const assert = require('assert')

let lastVisitedItemHref

export const infinityScrollToBottom = () => {
  while (true) {
    const tempWaypoint = browser.getLocation('.BackToTop')
    browser.scroll('.BackToTop', 0, 0)
    const newWaypoint = browser.getLocation('.BackToTop')
    if (tempWaypoint.y === newWaypoint.y) {
      break
    }
  }

  browser.scroll('.BackToTop-label', 0, 0)
}

export const scrollToBottom = () => {
  let offset = browser.execute('return window.document.body.offsetHeight').value
  let count = 0

  browser.scroll(0, offset)
  while (
    browser.isVisible('.PlpContainer-paginationPrev.is-shown') !== true &&
    count < 5
  ) {
    offset += offset
    browser.scroll(0, offset)
    overlay.waitForOverlay()

    browser.waitUntil(
      () => {
        return browser.isVisible('.ProductList-loader') === false
      },
      10000,
      'expected Loader progress bar to dissappear after 10s'
    )
    browser.scroll(0, offset)
    count++
  }
  browser.waitForExist('.PlpContainer-paginationPrev.is-shown', 5000)
}

export const selectProductInView = () => {
  const elements = browser.elements('.Product-link').value
  const scrollY = browser.execute('return window.scrollY').value
  const height = browser.getViewportSize('height')

  const lastVisitedItem = elements.find((element) => {
    const location = browser.elementIdLocation(element.ELEMENT).value
    return location.y > scrollY && location.y < scrollY + height
  })

  lastVisitedItemHref = browser.elementIdAttribute(
    lastVisitedItem.ELEMENT,
    'href'
  ).value
  browser.elementIdClick(lastVisitedItem.ELEMENT)
}

export const waitForProductDetail = () => {
  return browser.waitForExist('.ProductDetail')
}

export const productsLoaded = () => {
  const elements = browser.elements('.Product-link')
  assert.ok(
    elements.value.length > 20,
    `${elements.value.length} was not greater than 20, as expected`
  )
}

export const lastVisitedItemIsVisible = () => {
  browser.waitForExist('.ProductList')
  const elements = browser.elements('.Product-link').value
  const lastVisitedItem = elements.find((element) => {
    return (
      browser.elementIdAttribute(element.ELEMENT, 'href').value ===
      lastVisitedItemHref
    )
  })

  assert.equal(browser.elementIdDisplayed(lastVisitedItem.ELEMENT).value, true)
}

export const checkProductListingHeaderMatchingText = (text) => {
  const searchString = text.toLowerCase()

  // wait until product listing header is visible
  browser.waitForVisible('.PlpHeader-title')

  let result = browser.getText('.PlpHeader-title')
  result = result.toLowerCase()

  assert.ok(result === searchString)
}

export const checkBundleBadge = () => {
  browser.waitForVisible('.ProductList')
  return browser.waitForVisible(`.Product-badge`)
}

export const isOnCategoryTops = () => {
  addToBagPO.getEnvironmentFriendlyProductList('tops,shirts,tosps')
}

export const isOnProductList = () => {
  addToBagPO.getEnvironmentFriendlyProductList()
}

export const searchForProducts = (searchTerm) => {
  addToBagPO.getEnvironmentFriendlyProductList(searchTerm)
}

export const isOnProductListWithMoreThan20Products = () => {
  addToBagPO.getEnvironmentFriendlyProductList('shirt,shoes,tops,dresses')
}

export const filterProducts = () => {
  if (browser.isVisible('.Filters-refineButton')) {
    client.click('.Filters-refineButton')
    browser.waitForExist('.Refinements .is-shown')
  } else {
    console.log('in the desktop flow, not clicking refine as it isnt there')
  }
}

export const filterBy = (filter) => {
  filterRefinements.selectFilter(filter)
}

export const filterByRating = () => {
  filterBy('Rating')
}

export const selectRating = (rating) => {
  filterRefinements.selectRatingByAlt(rating)
}

export const backToTop = () => {
  let backToTopClicked = false
  const backToTopSelector = '.BackToTop-content'
  let count = 0
  while (
    browser.isVisible(backToTopSelector) !== true &&
    count < 5 &&
    backToTopClicked === false
  ) {
    try {
      browser.pause(500)
      if (browser.isVisible(backToTopSelector) !== true) {
        shared.scrollDown()
        browser.pause(500)
        shared.scrollDown()
        browser.pause(500)
        shared.scrollUp()
        browser.pause(500)
      }
      client.click(backToTopSelector)
      backToTopClicked = true
    } catch (err) {
      screenshot.forceScreenshot(
        'AVERTED-ERROR-back-to-top-button-failed-click-error-averted'
      )
    }
    count++
  }
}

export const buttonHasFilters = (expected) => {
  overlay.waitForOverlay()
  client.waitForProducts()
  const actual = filters.getFilterCount()
  assert.equal(
    actual,
    expected,
    `Actual filter count is ${actual} instead of ${expected}`
  )
}

export const checkProductRatings = (rating) => {
  let count = 0
  let starRating = ''
  overlay.waitForOverlay()
  client.waitForProducts()
  const ratingInt = parseInt(rating, 10)
  while (count < ratingInt) {
    starRating = count.toString()
    assert.ok(
      browser.isVisible(`img[alt="${starRating} out of 5 stars"]`) !== true,
      `img[alt="${starRating} out of 5 stars"] was visible when not expecting it to be`
    )
    count++
  }
}

export const clearFilters = () => {
  client.click('.Refinements-clearButton')
  browser.waitForExist('.ProductList')
  overlay.waitForOverlay()
}

export const noRatings = () => {
  overlay.waitForOverlay()
  client.waitForProducts()
  const elements = browser.elements('.Rating')
  assert.ok(elements.value.length === 0)
}

export const selectRatingDescending = (value) => {
  browser.waitForVisible('.Select-select')
  client.selectByValue('.SortSelector .Select-select', value)
  browser.waitForVisible('.ProductList')
}

export const filterByColour = () => {
  if (process.env.ENVIRONMENT === 'staging_international') {
    client.clickWithText('.Refinements-label', 'Color')
  } else {
    client.clickWithText('.Refinements-label', 'Colour')
  }
}

export const selectColour = (colour) => {
  const selection = colour.toLowerCase()
  filterRefinements.selectFilter(selection)
}

export const buttonHasNoFilters = () => {
  let text = browser.getText('.Filters-refineButton')
  text = text.toLowerCase()
  assert.equal(
    text,
    'filter',
    `${text} did not match the expected text of "filter"`
  )
}

export const containsProducts = () => {
  client.waitForProducts()
  if (browser.isVisible('.NoSearchResults')) {
    console.log(
      'filter returned no products, going to pass this for the time being as it happens and is not tant amount to a failure'
    )
    console.log('if this is happening a lot, investigate')
    assert.ok(true)
  } else {
    const elements = browser.elements('.Product-link')
    assert.ok(elements.value.length > 0)
  }
}

export const filterBySize = () => {
  client.clickWithText('.Refinements-label', 'Size')
}

export const selectSize = (size) => {
  filterRefinements.selectFilter(size)
}

export const selectAvailableSize = () => {
  filterRefinements.selectAvailableSizes('14,M,m')
}

export const selectDifferentAvailableSize = () => {
  filterRefinements.selectAvailableSizes('16,L,l')
}

export const filterByPrice = () => {
  filterRefinements.selectFilter('Price')
}

export const increaseMinPrice = () => {
  filterRefinements.setMinimumPriceSlider()
}

export const increaseMaxPrice = () => {
  filterRefinements.setMaxPriceSlider()
}

export const hasDescription = () => {
  browser.waitForExist('.Product-name')
  const elements = browser.elements('.Product-name')
  assert.ok(elements.value.length === 20)
}

export const hasImage = () => {
  browser.waitForExist('.ProductImages-image')
  const elements = browser.elements('.ProductImages-image')
  assert.ok(elements.value.length === 40)
}

export const hasPrice = () => {
  browser.waitForExist('.HistoricalPrice')
  const elements = browser.elements('.HistoricalPrice')
  assert.ok(elements.value.length === 20)
}

export const switchProductGridView = (cols) => {
  console.log(`num of cols : ${cols}`)

  if (client.hasDesktopLayout()) {
    switch (cols) {
      case '2': {
        const selector1 = '.GridSelector-buttonContainer:nth-child(1) button'
        browser.waitForExist('.ProductList')
        client.click(selector1)
        break
      }
      case '3': {
        const selector2 = '.GridSelector-buttonContainer:nth-child(2) button'
        browser.waitForExist('.ProductList')
        client.click(selector2)
        break
      }
      case '4': {
        const selector3 = '.GridSelector-buttonContainer:nth-child(3) button'
        browser.waitForExist('.ProductList')
        client.click(selector3)
        break
      }
      default: {
        browser.waitForExist('.ProductList')
        console.log('PLP grid buttons not loaded')
        break
      }
    }
  } else {
    const selector = `.GridSelector-buttonContainer:nth-child(${cols}) button`
    browser.waitForExist('.ProductList')
    client.click(selector)
  }
}

export const gridViewHasNColumns = (cols) => {
  browser.waitForExist('.ProductList')
  const classToCheck = `.Product.Product--col${cols}`
  browser.waitForVisible(classToCheck)
  assert.ok(
    browser.isVisible(classToCheck),
    `the number of columns laid out was not ${cols}`
  )
}

export const sortProducts = (value) => {
  browser.waitForVisible('.ProductList')
  browser.waitForVisible('.SortSelector .Select-select')
  browser.waitForEnabled('.SortSelector .Select-select')
  browser.pause(1000)
  browser.selectByVisibleText('.SortSelector .Select-select', value)
  browser.waitForVisible('.ProductList')
}

export const switchProductView = () => {
  browser.waitForExist('.ProductList')
  const selector = `.ProductViews.Filters-column  button:nth-child(1)`
  client.click(selector)
  overlay.waitForOverlay()
}

export const switchOutfitView = () => {
  browser.waitForExist('.ProductList')
  const selector = `.ProductViews.Filters-column  button:nth-child(2)`
  client.click(selector)
  overlay.waitForOverlay()
}

export const clickOnQuickviewIcon = () => {
  browser.waitForExist('.Product-quickViewButton')
  const selector = `.Product-quickViewButton`
  const prodName = browser.getText('.Product-name')
  process.env.prodName = prodName[0]
  client.click(selector)
  overlay.waitForOverlay()
}

export const verifyProductImgs = () => {
  browser.waitForExist('.ProductList')
  const selectorProd = `.ProductViews.Filters-column button:nth-child(1)`
  const selectorOutf = `.ProductViews.Filters-column button:nth-child(2)`
  const imgA = '.ProductImages > img:nth-child(1)'
  browser.waitForExist('.ProductList')
  const imgSrc = browser.getAttribute(imgA, 'src')
  const imgClasses = browser.getAttribute(imgA, 'class')
  const prodState = browser.getAttribute(selectorProd, 'aria-pressed')
  const outfState = browser.getAttribute(selectorOutf, 'aria-pressed')
  console.log(`prodState : ${prodState} - outfState : ${outfState}`)
  console.log(`imgSrc : ${imgSrc[0]} - imgClasses : ${imgClasses[0]}`)
  assert.ok(prodState === 'true')
  assert.ok(outfState === 'false')
  const ans = imgClasses.includes('is-hidden')
  assert.ok(ans === false)
}

export const verifyModelImgs = () => {
  browser.waitForExist('.ProductList')
  const selectorProd = '.ProductViews.Filters-column button:nth-child(1)'
  const selectorOutf = '.ProductViews.Filters-column button:nth-child(2)'
  const imgA = '.ProductImages > img:nth-child(2)'
  browser.waitForExist('.ProductList')
  const imgSrc = browser.getAttribute(imgA, 'src')
  const imgClasses = browser.getAttribute(imgA, 'class')
  const prodState = browser.getAttribute(selectorProd, 'aria-pressed')
  const outfState = browser.getAttribute(selectorOutf, 'aria-pressed')
  console.log(`prodState : ${prodState} - outfState : ${outfState}`)
  console.log(`imgSrc : ${imgSrc[0]} - imgClasses : ${imgClasses[0]}`)
  assert.ok(prodState === 'false')
  assert.ok(outfState === 'true')
  const ans = imgClasses.includes('is-hidden')
  // cannot validate images as not all items are modelled.
  assert.ok(ans === false)
}
