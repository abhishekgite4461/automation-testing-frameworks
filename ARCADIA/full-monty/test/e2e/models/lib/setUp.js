import * as client from '../../models/lib/client'
import * as shared from '../../models/lib/shared'
import * as customerProfile from '../../models/lib/customerProfiler'

export const setUpBrowser = () => {
  const modalSelector = '.Modal.Modal--normal'
  console.log(
    `[SETUP] BASE:${process.env.BASE_URL}, BREAKPOINT: ${
      process.env.BREAKPOINT
    }, BRAND: ${process.env.WDIO_BRAND}`
  )

  try {
    const environment = process.env.WDIO_ENVIRONMENT
    let requireBasicAuth = false

    /* nb: post requests fails when there are basic authentication credentials included in the URL. This affects all stage environment,
       to get around that, we now have to first load the page with basic authentication included in URL, and then reload the same URL again
       without it.
    */
    if (
      environment.includes('local') === false ||
      environment.includes('production') === false
    ) {
      requireBasicAuth = true
      client.gotoHomepage(process.env.WDIO_BRAND, requireBasicAuth)
    }
    client.gotoHomepage(process.env.WDIO_BRAND)

    client.setViewportSize()
    browser.waitForExist(modalSelector, 10000)
    // nb: pausing bit more while waiting for basic auth to be cached in browser
    browser.pause(2000)
  } catch (err) {
    console.log(err.message)
    browser.url('/')
    client.setViewportSize()
    browser.pause(10000)
    browser.waitForExist(modalSelector, 10000)
  }

  // SET ENVIRONMENT FLAGS
  process.env.USERNAME = ''
  process.env.PASSWORD = ''
  process.env.ADD_TO_BASKET_CONFIRM_TEXT = ''
  process.env.PRODUCT_ADDED = false

  // enable big header and responsive layout if on desktop breakpoint
  shared.enableResponsiveLayout()

  // retrieve and save country/brand-specific customer data to environment variable
  customerProfile.setCustomerProfileDefaultsCode(
    process.env.WDIO_BRAND.slice(-2)
  )
}
