import testAccount from '../../config/testAccount'

export const getCredential = (account) => {
  return testAccount[account.toLowerCase()] || {}
}
