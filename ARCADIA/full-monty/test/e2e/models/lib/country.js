import countries from '../../config/countries'

// Flips countries keys and value
const getCountriesByName = () => {
  return Object.keys(countries).reduce((obj, key) => {
    obj[countries[key]] = key
    return obj
  }, {})
}

export const getCountryCode = (countryName) => {
  const countries = getCountriesByName()
  return countries[countryName]
}
