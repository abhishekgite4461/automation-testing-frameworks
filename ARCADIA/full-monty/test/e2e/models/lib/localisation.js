import dictionaries from './../../../../src/shared/constants/dictionaries'
import * as localisation from './../../../../src/shared/lib/localisation'
import brands from '../../config/brands'

export const init = () => {
  const brandCode = process.env.WDIO_BRAND.toLowerCase()
  const brandObj = brands[brandCode]
  const language = brandObj.language
  const brandName = brandObj.brandName

  localisation.setDictionaries(dictionaries)
  return localisation.localise.bind(null, language, brandName)
}
