import creditCards from '../../config/creditCards'

// Flips credit card keys and value
const getCreditCardsByName = () => {
  return Object.keys(creditCards).reduce((obj, key) => {
    obj[creditCards[key]] = key
    return obj
  }, {})
}

export const getCreditCardKey = (cardName) => {
  const creditCards = getCreditCardsByName()
  return creditCards[cardName] || 'CARD'
}
