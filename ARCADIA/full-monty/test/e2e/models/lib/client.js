import URL from 'url'
import route from '../../config/route'
import { flatten, any } from 'ramda'
import * as shared from './shared'
import * as screenshots from '../../models/lib/screenshots'
import * as breakpoint from './breakpoint'
import * as overlay from '../newPageObjects/components/overlay'
import * as customerProfile from '../../models/lib/customerProfiler'
import * as brandURL from './brandURL'
import { setFeatureFlags } from './../../config/featureFlags'

const assert = require('assert')
/**
 * Resizes browser viewport when possible (ignored in mobile devices)
 * defaults to dimension set in breakpoint
 */
export const setViewportSize = (
  dimension = breakpoint.getDimension(process.env.BREAKPOINT)
) => {
  if (browser.isMobile === false) {
    browser.setViewportSize(dimension, true)
  }
}

/**
 * 1/11/16 iPad and desktop should have the same layout from discussion with UX guy
 * Note this flag is not the same as browser.isMobile which is based on device capabilities
 */
export const hasDesktopLayout = () => {
  return (
    process.env.WDIO_FORCE_MOBILE_ONLY_TEST !== 'true' &&
    (process.env.BREAKPOINT.includes('desktop') ||
      process.env.BREAKPOINT.includes('tablet'))
  )
}

export const moveToObject = (selector, x, y) => {
  browser.waitForVisible(selector)
  if (browser.isMobile) {
    console.log(
      '[INFO] to implement WDIO moveToObject equivalent that does not use touchMove()'
    )
  } else {
    browser.moveToObject(selector, x, y)
  }
}

export const click = (selector) => {
  overlay.waitForOverlay()
  browser.waitForVisible(selector)
  overlay.waitForOverlay()
  moveToObject(selector, 0, 0)
  browser.click(selector)
  overlay.waitForOverlay()
}

export const element = (selector) => () => browser.element(selector)

export const elements = (selector) => () => browser.elements(selector)

export const scroll = (selector) => () => browser.scroll(selector)

export const selectByIndex = (selector, i) => () =>
  browser.selectByIndex(selector, i)

export const selectByValue = (selector, value) => {
  browser.waitForVisible(selector)
  browser.selectByValue(selector, value)
}

export const setValue = (selector, value) => () => {
  browser.waitForVisible(selector)
  browser.setValue(selector, value)
}

export const openPage = (page) => {
  try {
    browser.url(page)
    overlay.waitForOverlay()
  } catch (err) {
    console.log('[SWALLOWED ERROR] failed to load the url trying again')
    browser.url(page)
    overlay.waitForOverlay()
  }
}

export const clearSession = () => {
  browser.deleteCookie()
  // clear browser console and session storage
  browser.execute(() => {
    if (console && console.clear) {
      console.clear()
    }
    window.localStorage.clear()
    return window.sessionStorage.clear()
  })
  setFeatureFlags()
}

export const logout = () => {
  // logout from app
  openPage('/logout')

  if (hasDesktopLayout() === false) {
    browser.waitForVisible('.BurgerButton')
  }
  overlay.waitForOverlay()

  // nb: logging out does not automatically clear cookies on WDIO,
  // need to manually remove the cookies and session stilll..
  clearSession()

  // go to homepage
  shared.openLandingPage()
}

export const browserBack = () => {
  browser.back()
  return browser.waitForExist('body')
}

export const clickWithText = (selector, text) => () => {
  const elements = browser.elements(selector)
  const results = Promise.all(
    elements.value.map((el) => {
      const elText = browser.elementIdText(el.ELEMENT)
      if (elText.value === text) {
        browser.elementIdClick(el.ELEMENT)
        return true
      }
      return false
    })
  )
  return any((e) => e, results)
    ? true
    : Promise.reject('Could not find element')
}

export const goToPage = (page) => openPage(route[page])
export const goToUrl = (url) => openPage(url)

export const gotoHomepage = (brandCode, requireBasicAuth = false) => {
  const url = brandURL.getURL(
    brandCode,
    process.env.WDIO_ENVIRONMENT,
    requireBasicAuth
  )

  // nb: retrieve and set customer test data, this needs to be after brandURL.getURL()!
  customerProfile.setCustomerProfileDefaultsCode(
    brandCode.substr(brandCode.length - 2)
  )

  browser.url(url)
  browser.waitForExist('body')
  if (overlay.warningModalIsVisible()) {
    console.log(
      'error was thrown going to the brand home page, going to refresh and try again'
    )
    console.log(overlay.getModalText)
    browser.click('.Modal-closeIcon')
    overlay.waitForOverlay()
    shared.captureScreenshotAndError(
      'error was thrown going to the brand home page, going to refresh and try again',
      overlay.getModalText(),
      'INVESTIGATE-MODAL-PRESENT-WHEN-GOING-TO-BRAND-HOME-PAGE-INVESTIGATE-'
    )
  }
}

export const keys = (keys) => {
  return browser.keys(keys)
}

export const reloadPage = () => {
  browser.refresh()
}

export const hasValue = (selector, value) => () => {
  const text = browser.getValue(selector)
  assert.ok(
    any((t) => t === value),
    flatten([text]),
    `Value ${value} not found for ${selector}`
  )
}

export const hasTitle = (title) => {
  // give browser bit more time to start loading page
  browser.pause(500)

  // page title is not populated immediately, wait until content is visible before retrieving
  browser.waitForVisible('.BurgerButton')

  const pageTitle = browser.getTitle()
  assert.ok(
    pageTitle.toLowerCase().indexOf(title.toLowerCase()) > 0,
    `${title} does not match page title: ${pageTitle}`
  )
}

export const exists = (selector) => () => {
  const e = browser.isExisting(selector)
  if (e) {
    return true
  }
  return Promise.reject('element does not exist')
}

export const doesNotExist = (selector) => {
  const e = browser.isExisting(selector)
  if (e) {
    return Promise.reject('element does not exist')
  }
  return true
}

export const isNotVisible = (selector) => () => {
  const e = browser.isVisible(selector)
  if (e) {
    return Promise.reject('element does not exist')
  }
  return true
}

export const existsNTimes = (selector, n) => () => {
  const e = browser.elements(selector)
  assert.equal(
    e.value.length,
    n,
    `found ${selector} ${e.value.length} times, and not ${n} as expected`
  )
}

export const getOptionWithText = (selector, text) => () => {
  return browser.selectByVisibleText(selector, text)
}

export const urlPathnameIsEqualTo = (expectedUrl) => {
  const url = browser.getUrl()
  const pathname = URL.parse(url).pathname
  browser.waitForExist('body')
  assert.equal(pathname, expectedUrl)
}

export const urlPathnameMatchesPattern = (expectedUrl) => {
  const url = browser.getUrl()
  const pathname = URL.parse(url).pathname
  browser.waitForExist('body')
  assert.ok(expectedUrl.test(pathname))
}

export const includesText = (selector, value) => {
  const valueToConfirm = value.toLowerCase()
  browser.waitForVisible(selector)
  const texts = browser.getText(selector)
  let receivedText
  if (typeof texts === 'object') {
    receivedText = texts.toString()
  } else {
    receivedText = texts
  }
  receivedText = receivedText.toLowerCase()
  const result = receivedText.indexOf(valueToConfirm) >= 0
  assert.ok(
    result === true,
    `${valueToConfirm} did not exist inside the text we got of: ${texts}`
  )
}

export const waitForAddToBasketModal = () => {
  overlay.waitForOverlay()
  let counter = 0
  let status = false

  while (
    browser.getAttribute('.Modal--normal', 'class') !==
      'Modal is-shown Modal--normal' &&
    counter < 300
  ) {
    browser.pause(100)
    counter++
  }

  const modalText = browser.getText('.Modal.is-shown.Modal--normal')

  if (modalText.indexOf('Remote 404') !== -1) {
    screenshots.forceScreenshot('REMOTE-404-ON-ADD-TO-BASKET-ERROR-AVERTION')
    browser.refresh()
    status = false
  } else if (modalText.indexOf('added') !== -1) {
    status = true
  } else {
    screenshots.forceScreenshot(
      'UNEXPECTED_RESULT-ON-ADD-TO-BASKET_MODAL-ERROR-AVERTION'
    )
    browser.refresh()
    status = false
  }

  return status
}

export const waitForInlineConfirm = () => {
  overlay.waitForOverlay()
  browser.waitForVisible('.InlineConfirm')
}

export const waitForAddToBasket = () => {
  if (hasDesktopLayout()) {
    waitForInlineConfirm()
  } else {
    waitForAddToBasketModal()
  }
}

export const waitforReview = () => {
  let counter = 0

  while (browser.isVisible('.BVRRDisplayContent') === false && counter < 50) {
    browser.pause(100)
    counter++
  }

  if (browser.isVisible('.BVRRDisplayContent') === false && counter > 0) {
    assert.ok(
      false,
      `Review is still not visible, PDP might not loaded. Exiting.`
    )
  }
}

export const waitForProducts = () => {
  let counter = 0

  while (
    browser.isVisible('.NoSearchResults') === false &&
    browser.isVisible('.Product-info') === false &&
    counter < 60
  ) {
    browser.pause(1000)
    counter++
    console.log('waiting for product results')
  }
}

export const waitForBasketLoaded = () => {
  let text = browser.getText('.Drawer.is-open')
  while (text.indexOf('Loading...') !== -1 || text === 'undefined') {
    browser.pause(250)
    text = browser.getText('.Drawer.is-open')
  }
}

export const textMatchesPattern = (selector, regex) => () => {
  const texts = browser.getText(selector)
  assert.ok(
    [].concat(texts).some((text) => regex.test(text)),
    `Could not find a match for ${regex} in ${selector}`
  )
}
