const breakpoints = {
  mobileS: [320, 480],
  mobileM: [375, 667],
  mobileL: [425, 974],
  mobileXL: [768, 992],
  tablet: [769, 992],
  desktopS: [993, 800],
  desktopM: [1280, 960],
  desktopL: [1920, 1280],
  desktop: [1280, 960],
}

export const getDimension = (type) => {
  return {
    width: breakpoints[type][0],
    height: breakpoints[type][1],
  }
}
