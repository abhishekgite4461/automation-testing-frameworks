import * as client from '../lib/client'
import * as screenshots from '../lib/screenshots'

const assert = require('assert')

export const openLandingPage = () => {
  browser.deleteCookie('bagCount')
  client.openPage('/')
  browser.refresh()

  // wait until menu button is visible
  return browser.waitForVisible('.Main-body')
}

export const clickItemByText = (itemText, location, option) => {
  const selector = `.${location}-${option}=${itemText}`
  browser.waitForVisible(selector)
  const scrollY = browser.getLocation(selector).y + 100
  const currWindowSize = browser.getViewportSize()

  if (currWindowSize.height < scrollY) {
    client.setViewportSize({
      width: currWindowSize.width,
      height: scrollY,
    })
  }

  client.click(selector)
  return browser.pause(300)
}

export const clickItem = (location, option) => {
  const selector = `.${location}-${option}`
  browser.waitForVisible(selector)
  client.click(selector)
  return browser.pause(300)
}

export const isVisible = (location, element) => {
  const selector = `.${location}${element ? `-${element}` : ''}`
  browser.waitForVisible(selector)
  assert.equal(
    browser.isVisible(selector),
    true,
    `Element '${selector}' expected to be visible but is hidden`
  )
}

export const notVisible = (location, element) => {
  const selector = `.${location}-${element}`
  browser.waitForVisible(selector, 1000, true)

  const elements = browser.elements(selector)
  elements.value.forEach((val) => {
    assert.equal(browser.elementIdDisplayed(val.ELEMENT).value, false)
  })
}

export const textVisible = (text, location, type) => {
  const selector = `.${location}-${type}`
  const elementText = browser.getText(selector)
  assert.equal(elementText, text)
}

export const messageDisplayed = (message) => {
  try {
    browser.getText(`p=${message}`)
  } catch (e) {
    assert.equal(e.type, 'NoSuchElement')
  }
}

export const iconDisplayed = (icon, location) => {
  const selector = `.${location}-${icon}Icon`
  const result = browser.getAttribute(selector, 'src')

  const expect = `${icon}-icon`.toLowerCase()
  // match to file name without extension in img src URL
  const actual = result.match(/([^/]+)(?=\.\w+$)/)[0].toLowerCase()

  assert.equal(expect, actual)
}

export const scrollDown = () => {
  const scrollY = browser.execute('return window.scrollY')
  browser.waitForVisible('.Product-name')
  browser.scroll(0, scrollY.value + 1000)
}

export const scrollUp = () => {
  const scrollY = browser.execute('return window.scrollY')

  browser.scroll(0, scrollY.value - 300)
}

/**
 * Scrolls a given % down the page.
 */
export const scrollPercentage = (percentage) => {
  const viewPortHeight = browser.execute('return window.innerHeight').value
  const documentHeight = browser.execute(
    'return Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight)'
  ).value
  if (viewPortHeight >= documentHeight) {
    return
  }
  const scrollValue = Math.round(
    percentage * (documentHeight / 100) - viewPortHeight
  )
  if (scrollValue <= 0) {
    return
  }
  browser.scroll(0, scrollValue)
}

export const viewPageTop = () => {
  const scrollY = browser.execute('return window.scrollY')
  assert.equal(scrollY.value, 0)
}

export const getText = (selector) => {
  browser.waitForVisible(selector)
  return browser.getText(selector)
}

export const failWithScreenshotAndMessage = (msg, err, screenshotTitle) => {
  screenshots.forceScreenshot(screenshotTitle)

  // remove screenshot from output if exists
  if (err && err.screenshot) delete err.screenshot

  console.log(err)
  console.log(browser.getUrl())

  assert.ok(false, msg)
}

export const captureScreenshotAndError = (msg, err, screenshotTitle) => {
  screenshots.forceScreenshot(screenshotTitle)

  // remove screenshot from output if exists
  if (err && err.screenshot) delete err.screenshot

  console.log(msg)
  console.log(err)
  console.log(browser.getUrl())
}

export const pause = (time) => {
  browser.pause(parseInt(time, 10))
}

/**
 * Shuffles list
 * based on https://github.com/coolaj86/knuth-shuffle
 */
export const shuffle = (array) => {
  let currentIndex = array.length
  let temporaryValue
  let randomIndex

  // While there remain elements to shuffle...
  while (currentIndex !== 0) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1

    // And swap it with the current element.
    temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  }

  return array
}

/**
 * Enable feature, this bypasses feature flag toggle in debug menu by enabling feature directly via cookie
 * @param flag feature flag to enable/disable
 * @param enable enable or disable feature
 */
export const enableFeature = (flag, enable = true) => {
  const features = browser.getCookie('featuresOverride') || {}
  features[flag] = enable
  browser.setCookie({
    name: 'featuresOverride',
    value: JSON.stringify(features),
  })
  browser.refresh()
}

export const getFeatureValue = (flag) => {
  browser.refresh()
  return browser.getCookie(flag)
}

/**
 * Enable big header and responsive layout if on desktop breakpoint
 */
export const enableResponsiveLayout = () => {
  if (client.hasDesktopLayout()) {
    enableFeature('FEATURE_HEADER_BIG')
    enableFeature('FEATURE_RESPONSIVE')
  }
}

export const removeCurrencySign = (price = '') => {
  return price.replace(/[^\d.]/g, '')
}

export const createRandomEmailAddress = () => {
  const todaysDate = new Date().toJSON().replace(/\W+/g, '')
  const emailAddress = `${Math.random()
    .toString(36)
    .substring(7)}@${todaysDate}.com`
  console.log(
    `[MY_ACCOUNT] created email address for registration: ${emailAddress}`
  )
  return emailAddress
}

export const getFilenameDate = () => {
  return new Date().toISOString().replace(':', '-')
}
