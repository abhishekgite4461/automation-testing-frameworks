import * as client from './client'
import * as shared from './shared'

const screenshotFilePath = `test/e2e/screenshots/`

export const increaseBrowserHeight = () => {
  const width = browser.getViewportSize('width')
  let height = browser.getViewportSize('height')
  if (height < 700) {
    height += 700
  }
  const dims = {
    width,
    height,
  }
  client.setViewportSize(dims)
}

export const takeScreenshot = (screenshotName) => {
  try {
    increaseBrowserHeight()
    const filePath = `${screenshotFilePath}ON-THE-FLY-${shared.getFilenameDate()}--${screenshotName}--.png`
    browser.saveScreenshot(filePath)
    client.setViewportSize()
  } catch (e) {
    console.log(`[takeScreenshot] screenshot failed: ${e.message}`)
  }
}

export const forceScreenshot = (screenshotName) => {
  try {
    increaseBrowserHeight()
    const filePath = `${screenshotFilePath}FORCED-SCREENSHOT-${
      process.env.BREAKPOINT
    }-${shared.getFilenameDate()}--${screenshotName}--.png`
    console.log(
      `[SCREENSHOT INFO] forced screenshot being saved to ${filePath}`
    )
    browser.saveScreenshot(filePath)
    client.setViewportSize()
  } catch (e) {
    console.log(`[forceScreenshot] screenshot failed: ${e.message}`)
  }
}
