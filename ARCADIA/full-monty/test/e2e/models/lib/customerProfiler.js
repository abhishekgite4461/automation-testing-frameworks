/**
 * Created by colinmoore-hill on 20/03/2017.
 */

import profiles from '../../config/customerProfiles'
import * as countryList from '../../models/lib/country'
import searchKeywords from '../../config/searchKeywords'

let profile = ''

export const validateInput = (countryCode) => {
  if (countryCode in profiles) {
    profile = profiles[countryCode]
  } else {
    profile = profiles.GB // Default to GB
  }
}

const getSearchKeyword = (brandCode = process.env.WDIO_BRAND) => {
  let keywords = {}

  switch (true) {
    case process.env.JENKINS_PRODUCTS:
      keywords = {
        common: process.env.JENKINS_PRODUCTS,
        multiSize: process.env.JENKINS_PRODUCTS,
        singleSize: process.env.JENKINS_PRODUCTS,
        colour: process.env.JENKINS_PRODUCTS,
      }
      break
    case brandCode.includes('br'):
      keywords = { ...searchKeywords.ts, ...searchKeywords.br }
      break
    default:
      keywords = searchKeywords.ts
      break
  }

  // save search keywords into global var
  process.env.GOOD_PRODUCTS = keywords.common
  process.env.MULTY_SIZE_PRODUCTS = keywords.multiSize
  process.env.SINGLE_SIZE_PRODUCTS = keywords.singleSize
  process.env.COLOUR_SERACH_PRODUCTS = keywords.colour
}

export const setCustomerProfileDefaultsCode = (countryCode = 'GB') => {
  if (countryCode.toUpperCase() === 'UK') {
    countryCode = 'GB'
  }

  // Set required customer profile
  validateInput(countryCode)

  // Populate the related Global Variables
  process.env.BASE_COUNTRY = profile.country
  process.env.BASE_POSTCODE = profile.postcode
  process.env.TELEPHONE = profile.telephone
  process.env.BASE_FIRSTNAME = profile.firstName
  process.env.BASE_LASTNAME = profile.lastName
  process.env.TITLE = profile.title

  // retrieve and save search keywords
  getSearchKeyword()
}

export const setCustomerProfileDefaults = (countryName = 'United Kingdom') => {
  const countryCode = countryList.getCountryCode(countryName)
  setCustomerProfileDefaultsCode(countryCode)
}
