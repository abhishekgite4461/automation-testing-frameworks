import brands from '../../config/brands'

const url = require('url')

const isStageEnv = (env) => {
  const stageEnv = [
    'stage',
    'integration',
    'showcase',
    'perf',
    'storelocator',
    'mrcms',
    'devops',
    'new-sg',
    'stage-json',
    'showcase-json',
    'integration-json',
    'stage-scrapi',
    'showcase-scrapi',
    'integration-scrapi',
  ]
  return stageEnv.includes(env)
}

const isProdEnv = (env) => {
  return env.includes('production')
}

const isNoAkamaiPreProd = (env) => {
  const noAkamaiPreprodEnv = ['no-cdn', 'montycms-next']
  return noAkamaiPreprodEnv.includes(env)
}

const isPreProd = (env) => {
  const preprodEnv = ['preprod']
  return preprodEnv.includes(env)
}

const getAuth = (env, requireBasicAuth) => {
  return isStageEnv(env) && requireBasicAuth ? 'monty:monty' : ''
}

const getHostname = (brandCode, env) => {
  const brand = brands[brandCode.toLowerCase()]
  let hostname

  switch (true) {
    case isProdEnv(env):
      hostname = brand.url
      break
    // nb: bypasses Akamai caching as it was preventing basic auth from appearing, please discuss with DevOps if needs to be replaced
    case isNoAkamaiPreProd(env):
      hostname = `${env}-${brand.url.replace(
        /\./g,
        '-'
      )}.digital-prod.arcadiagroup.co.uk`
      break
    case isStageEnv(env):
      hostname = `${env}-${brand.url.replace(
        /\./g,
        '-'
      )}.digital.arcadiagroup.co.uk`
      break
    default:
      hostname = `${env}.${brand.url}`
      break
  }

  return hostname
}

const getPort = (env) => {
  return isProdEnv(env) ||
    isStageEnv(env) ||
    isNoAkamaiPreProd(env) ||
    isPreProd(env)
    ? ''
    : '3000'
}

const getProtocol = (env) => {
  return isProdEnv(env) ||
    isStageEnv(env) ||
    isNoAkamaiPreProd(env) ||
    isPreProd(env)
    ? 'https'
    : 'http'
}

export const getURL = (
  brandCode = 'tsuk',
  env = 'local',
  requireBasicAuth = false
) => {
  const urlObject = {
    auth: getAuth(env, requireBasicAuth),
    hostname: getHostname(brandCode, env),
    port: getPort(env),
    protocol: getProtocol(env),
  }

  // store in env var
  process.env.BASE_URL_PORT = urlObject.port
  process.env.WDIO_ENVIRONMENT = env
  process.env.WDIO_BRAND = brandCode

  // extract full URL and save that as env var
  const fullUrl = url.format(urlObject)
  process.env.BASE_URL = fullUrl

  // nb: update WDIO baseURL when possible.
  if (
    typeof browser !== 'undefined' &&
    browser.options &&
    browser.options.baseUrl
  ) {
    browser.options.baseUrl = fullUrl
  }

  return fullUrl
}
