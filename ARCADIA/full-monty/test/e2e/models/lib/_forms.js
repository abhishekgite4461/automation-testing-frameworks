import * as client from './client'
import * as overlayPO from '../newPageObjects/components/overlay'

const assert = require('assert')

export const setFormInput = (value, fieldName, formName = false) => {
  const selector = !formName
    ? `.Input-field-${fieldName}`
    : `.${formName} input[name="${fieldName}"]`

  browser.waitForVisible(selector)
  // move to element
  client.moveToObject(selector, 0, 0)

  // wait until element is visible
  browser.waitForVisible(selector)
  // set input value
  browser.setValue(selector, value)
}

export const clearFormInput = (fieldName, formName) => {
  const selector = `.${formName} input[name="${fieldName}"]`
  browser.waitForExist(selector)
  // Webdriver does not work with empty vals. So we set one char and remove it.
  browser.setValue(selector, '$')
  return browser.keys(['\b'])
}

export const selectedOptionIs = (selectName, selectedOptionText, formName) => {
  overlayPO.waitForOverlay()
  const selector = `.${formName} select[name="${selectName}"]`
  browser.waitForExist(selector)
  const option = browser.isSelected(`[value="${selectedOptionText}"]`)
  assert(option)
}

export const formInputTextEquals = (fieldName, text, formName) => {
  overlayPO.waitForOverlay()
  const selector = `.${formName} input[name="${fieldName}"]`
  console.log(`using selector for form lookup: ${selector}`)
  let elementText = browser.getValue(selector)
  console.log(`[INFO] expecting: ${text}`)
  console.log(`[INFO] Got: ${elementText}`)
  let counter = 0
  while (elementText !== text && counter < 150) {
    elementText = browser.getValue(selector)
    console.log(elementText)
    console.log('[INFO] waiting for fields to be populated')
    counter++
  }
  assert.ok(
    elementText === text,
    `${text} did not match what we got: ${elementText}`
  )
}

export const checkButtonDisabled = (button, formName) => {
  const selector = `.${formName} .${button}`
  const disabled = browser.getAttribute(selector, 'disabled')
  assert.equal(disabled, 'true', `The ${button} is not disabled as expected!`)
}

export const formInputValidationEquals = (className, text, formName) => {
  const selector = `.${formName} .Input-${className} .Input-validationMessage`
  browser.waitForExist(selector)
  const act = browser.getText(selector)
  console.log(`text = ${text} & act = ${act}`)
  assert.equal(text, act, 'Email required notification not as expected!')
}
