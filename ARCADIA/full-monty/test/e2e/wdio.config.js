/* eslint no-console: 0 */

require('babel-register')
require('babel-polyfill')

const fs = require('fs')
const path = require('path')
const helper = require('./helper')
const shared = require('./models/lib/shared')
const { setFeatureFlags } = require('./config/featureFlags')

const environment = process.env.WDIO_ENVIRONMENT || 'local' // E.g. local, stage and integration
const brandCode = process.env.WDIO_BRAND || 'tsuk'
const specs = process.env.FEATURES_PATH
  ? process.env.FEATURES_PATH.split(/[, \n]/)
  : 'test/e2e/features/**/*.feature'
const browserName = process.env.BROWSER_NAME || 'chrome'
const maxInstances = process.env.MAX_INSTANCES || 1
const logLevel = process.env.WDIO_LOG_LEVEL || 'error'
const breakpoint = process.env.BREAKPOINT || 'mobileM'
const jenkinsProducts = process.env.JENKINS_PRODUCTS || null
const url = helper.getURL(brandCode, environment)
const REPORT_PATH = 'test/reports'

if (jenkinsProducts !== null) {
  console.log(`using jenkins products: ${jenkinsProducts}`)
  process.env.JENKINS_PRODUCTS_FLAG = true
  process.env.JENKINS_PRODUCTS = jenkinsProducts
}

process.env.BREAKPOINT = breakpoint

let cucumberTags
if (typeof process.env.CUCUMBER_TAGS !== 'undefined') {
  cucumberTags = process.env.CUCUMBER_TAGS.split(',')
} else {
  cucumberTags = ['~@Draft', '~@broken', '~@Flaky', '~@wip', '~@realCheckout']
}

exports.config = {
  //
  // ==================
  // Specify Test Files
  // ==================
  // Define which test specs should run. The pattern is relative to the directory
  // from which `wdio` was called. Notice that, if you are calling `wdio` from an
  // NPM script (see https://docs.npmjs.com/cli/run-script) then the current working
  // directory is where your package.json resides, so `wdio` will be called from there.
  //
  specs,
  // Patterns to exclude.
  exclude: [
    // 'path/to/excluded/files'
  ],

  //
  // ============
  // Capabilities
  // ============
  // Define your capabilities here. WebdriverIO can run multiple capabilities at the same
  // time. Depending on the number of capabilities, WebdriverIO launches several test
  // sessions. Within your capabilities you can overwrite the spec and exclude options in
  // order to group specific specs to a specific capability.
  //
  // First, you can define how many instances should be started at the same time. Let's
  // say you have 3 different capabilities (Chrome, Firefox, and Safari) and you have
  // set maxInstances to 1; wdio will spawn 3 processes. Therefore, if you have 10 spec
  // files and you set maxInstances to 10, all spec files will get tested at the same time
  // and 30 processes will get spawned. The property handles how many capabilities
  // from the same test should run tests.
  //
  maxInstances,
  //
  // If you have trouble getting all important capabilities together, check out the
  // Sauce Labs platform configurator - a great tool to configure your capabilities:
  // https://docs.saucelabs.com/reference/platforms-configurator
  //
  capabilities: [
    {
      browserName,
      chromeOptions: {
        // to run chrome headless the following flags are required
        // (see https://developers.google.com/web/updates/2017/04/headless-chrome)
        args: ['--headless', '--no-sandbox'],
        binary: '/usr/bin/chromium-browser',
      },
    },
  ],
  //
  // ===================
  // Test Configurations
  // ===================
  // Define all options that are relevant for the WebdriverIO instance here
  //
  // By default WebdriverIO commands are executed in a synchronous way using
  // the wdio-sync package. If you still want to run your tests in an  way
  // e.g. using promises you can set the sync option to false.
  sync: true,
  //
  // Level of logging verbosity: silent | verbose | command | data | result | error
  logLevel,
  //
  // Enables colors for log output.
  coloredLogs: true,
  //
  // Saves a screenshot to a given path if a command fails.
  screenshotPath: null,
  //
  // Set a base URL in order to shorten url command calls. If your url parameter starts
  // with "/", then the base url gets prepended.
  baseUrl: url,
  //
  // Default timeout for all waitFor* commands.
  waitforTimeout: 30000,
  //
  // Default timeout in milliseconds for request
  // if Selenium Grid doesn't send response
  connectionRetryTimeout: 90000,
  //
  // Default request retries count
  connectionRetryCount: 3,
  //
  // Test runner services
  // Services take over a specific job you don't want to take care of. They enhance
  // your test setup with almost no effort. Unlike plugins, they don't add new
  // commands. Instead, they hook themselves up into the test process.
  services: ['selenium-standalone'],

  seleniumInstallArgs: {
    drivers: { chrome: { version: '2.35' } },
  },
  seleniumArgs: {
    seleniumArgs: ['-port', '4444'],
    drivers: { chrome: { version: '2.35' } },
  },
  //
  //
  // Make sure you have the wdio adapter package for the specific framework installed
  // before running any tests.
  framework: 'cucumber',
  //
  // Test reporter for stdout.
  // The following are supported: dot (default), spec and xunit
  // see also: http://webdriver.io/guide/testrunner/reporters.html
  reporters: ['cucumber', 'junit-morganchristiansson'],
  reporterOptions: {
    outputDir: `${REPORT_PATH}/bdd/`,
    cucumber: {
      outputDir: `${REPORT_PATH}/bdd/`,
    },
    junit: {
      outputDir: `${REPORT_PATH}/bdd/`,
      suiteNameFormat: /(?!)/, // don't replace spaces with underscores (_)
      packageName: process.env.BREAKPOINT,
    },
  },
  //
  // If you are using Cucumber you need to specify the location of your step definitions.
  cucumberOpts: {
    require: ['test/e2e/features/step_definitions'], // <string[]> (file/dir) require files before executing features
    backtrace: true, // <boolean> show full backtrace for errors
    compiler: [], // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    dryRun: false, // <boolean> invoke formatters without executing steps
    failFast: false, // <boolean> abort the run on first failure
    format: ['pretty'], // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
    colors: true, // <boolean> disable colors in formatter output
    snippets: true, // <boolean> hide step definition snippets for pending steps
    source: true, // <boolean> hide source uris
    profile: [], // <string[]> (name) specify the profile to use
    strict: true, // <boolean> fail if there are any undefined or pending steps
    tags: cucumberTags, // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    timeout: 280000, // <number> timeout for step definitions
    ignoreUndefinedDefinitions: false, // <boolean> Enable this config to treat undefined definitions as warnings.
  },

  // =====
  // Hooks
  // =====
  // For more hooks, see: http://webdriver.io/guide/testrunner/configurationfile.html
  //
  // WebdriverIO provides a several hooks you can use to interfere the test process in order to enhance
  // it and build services around it. You can either apply a single function to it or an array of
  // methods. If one of them returns with a promise, WebdriverIO will wait until that promise got
  // resolved to continue.
  //
  // Gets executed once before all workers get launched.
  onPrepare: () => {
    // retrieve and output Monty version on non-local build. This is currently available on stage, showcase and production
    helper.getAppVersion(url)

    const cucumberJsonPath = `${
      this.config.reporterOptions.cucumber.outputDir
    }${shared.getFilenameDate()}-cucumber-js.json`
    process.env.CUCUMBER_JSON_PATH = cucumberJsonPath

    if (fs.existsSync(cucumberJsonPath)) {
      fs.unlinkSync(cucumberJsonPath)
    }
    try {
      fs.unlinkSync(`${REPORT_PATH}/rerun-fail.txt`)
    } catch (err) {
      console.error(`Error deleting rerun-fail.txt: ${err}`)
    }

    console.log(
      `[onPrepare SETUP] CUCUMBER_TAGS: ${this.config.cucumberOpts.tags}`
    )
  },
  // Cucumber specific hooks
  afterScenario: () => {
    browser.reload()
  },

  beforeFeature: () => {
    browser.url('/login')
    setFeatureFlags()
  },

  afterStep: (step) => {
    const name = step
      .getStep()
      .getName()
      .replace(/\s/g, '_')
      .replace(/\\/g, '')
    const error = step.getFailureException()
    const featureFile = path.relative('', step.getStep().getUri())

    if (error) {
      // retrieve and display browser console log
      const browserLogPath = `test/e2e/logs/ERROR_CONSOLELOG_${shared.getFilenameDate()}_${name}.log`
      helper.getBrowserLog(browserLogPath)

      // useful log
      const urlparser = require('url')
      const api = process.env.API_URL
        ? urlparser.parse(process.env.API_URL).host.split('.')[0]
        : process.env.API_URL

      console.error(
        `\n\n\n[FAILED IN] ${featureFile}:${step.getStep().getLine()}`
      )
      console.log(
        `[CONFIG] API: ${api}, Browser: ${
          process.env.BROWSER_NAME
        }, Breakpoint: ${process.env.BREAKPOINT}`
      )
      console.error(`[URL] ${browser.getUrl()}`)
      console.error(error.stack || error)

      fs.appendFileSync(`${REPORT_PATH}/rerun-fail.txt`, `${featureFile}\n`)

      const screenshotPath = `test/e2e/screenshots/ERROR_${
        process.env.BREAKPOINT
      }-${shared.getFilenameDate()}_${name}.png`
      helper.takeScreenshot(screenshotPath)

      console.log('=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n\n\n')
    }
  },
}
