import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import asynQueue from 'redux-async-queue'
import { apiMiddleware } from 'redux-api-middleware'
import createSagaMiddleware from 'redux-saga'
import { montyApiMiddleware } from '../../../src/shared/middleware/preprocess-redux-api-middleware'
import localiseMiddleware from '../../../src/shared/middleware/localise-middleware'
import analyticsListenerMiddleware from '../../../src/shared/analytics/middleware/analytics-middleware'
import { createStore } from 'redux'
import reducer from '../../../src/shared/lib/combine-reducers'

// same config we have in configure-store.js
const sagaMiddleware = createSagaMiddleware()
const middlewares = [
  montyApiMiddleware,
  apiMiddleware,
  thunk,
  asynQueue,
  sagaMiddleware,
  localiseMiddleware,
  analyticsListenerMiddleware,
]

/**
 * mockStoreCreator is a function to create a mock store. Use this is you don't need the whole initial state in your tests
 *
 * Usage:
 *  const store = mockStoreCreator(initialState)
 */
export const mockStoreCreator = configureMockStore(middlewares)

/**
 * getMockStoreWithInitialReduxState is a function to create a mock store but with the app initial state already set up
 * @param {*} initialState we can pass optionally initial state and will be merged with the reducer original initial state
 *
 * Usage:
 *    const store = getMockStoreWithInitialReduxState()
 *    const store = getMockStoreWithInitialReduxState({account: { user: { email: 'email@email.com' }}})
 */
export const getMockStoreWithInitialReduxState = (initialState = {}) => {
  const realStore = createStore(reducer, initialState)
  return mockStoreCreator(realStore.getState())
}
