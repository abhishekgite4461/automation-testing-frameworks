export const lfw = {
  pageId: 116957,
  pageName: 'London Fashion Week',
  baseline: '3',
  contentPath: '/cms/pages/json/json-0000116957/json-0000116957.json',
  seoUrl: '',
  settings: {
    template: 'lfw16',
    css: ['/assets/custom/Lfw16/images/lfw16.css'],
    js: [],
    options: '{}',
  },
  pageData: [
    {
      type: 'custom',
      data: {
        className: 'intro',
        markup:
          '<h1>Topshop at<br/><strong>London<br/>Fashion Week</strong></h1><p>This season, we’re giving you the chance to shop Topshop Unique’s catwalk collection directly from the runway. For the first time, selected styles from the catwalk show will be available to buy immediately online, in store and at our Spitafields Market showspace. Just tune in to Topshop.com at 2pm this Sunday 18th September to watch (and shop!) the show live, and keep scrolling to stay up to date with the other LFW shows we’re covering.</p>',
      },
    },
    {
      type: 'custom',
      data: {
        template: 'showspace',
        class: 'This is showspace component',
        markup: 'Just like an html block for showspace',
      },
    },
    {
      type: 'html',
      data: {
        class: 'Can put any string here',
        template: 'Can put any string here',
        markup: 'Just like an html block',
      },
    },
    {
      type: 'imagelist',
      data: {
        assets: [
          {
            target: '',
            alt: 'The eternal cool of adidas',
            link:
              '/en/tsuk/category/the-eternal-cool-of-adidas-5800473/home?TS=1470907814214&amp;intcmpid=MOBILE_WK51_TUES_ADIDAS',
            source:
              'http://media.topshop.com/wcsstore/ConsumerDirectStorefrontAssetStore/images/colors/color7/cms/pages/json/json-0000116661/images/1_MOBILE-THURS-EU_01.jpg',
          },
        ],
      },
    },
    {
      type: 'html',
      data: {
        class: 'Can put any string here',
        template: 'Can put any string here',
        markup: 'A normal html block',
      },
    },
  ],
  version: '1.6',
}
