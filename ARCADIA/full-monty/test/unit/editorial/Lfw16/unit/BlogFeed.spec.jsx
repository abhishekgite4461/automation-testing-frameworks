import React from 'react'
import { mount } from 'enzyme'
import { decode } from 'he'

import BlogFeed from '../../../../../src/custom/Lfw16/blogfeed/BlogFeed'

import blogFeedResponseMock from '../mocks/lfw-blogfeed-response.json'

const { WrappedComponent } = BlogFeed

test('<BlogFeed /> shows a BlogFeed class', () => {
  const component = mount(<WrappedComponent />)

  expect(component.find('.BlogFeed').length).toBe(1)
})

test('<BlogFeed /> shows initially the Loader', () => {
  const component = mount(<WrappedComponent />)

  expect(component.find('.BlogFeed').find('.BlogFeed-loader').length).toBe(1)
  expect(
    component
      .find('.BlogFeed')
      .find('.BlogFeed-loader')
      .find('img')
      .prop('src')
  ).toBe('/assets/custom/lfw16/images/spinner.gif')
})

test('<BlogFeed /> does not display anything in case of error from the microservice', () => {
  const component = mount(<WrappedComponent />)

  component.setState({
    blogPostCount: 3,
    feed: {
      errorMessage:
        '2016-09-16T13:00:07.519Z 7a9ff137-7c0d-11e6-8659-05e5b5f45d59 Task timed out after 6.00 seconds',
    },
  })

  expect(component.find('.BlogFeed').html()).toBe(
    '<div class="BlogFeed"></div>'
  )
})

test('<BlogFeed /> displays blog feed content', () => {
  const component = mount(<WrappedComponent />)

  component.setState({
    blogPostCount: 3,
    feed: blogFeedResponseMock,
  })

  expect(component.find('.BlogFeed').length).toBe(1)
  expect(component.find('.BlogFeed').find('.BlogFeed-blogPost').length).toBe(3)
  expect(
    component
      .find('.BlogFeed')
      .find('.BlogFeed-blogPost')
      .at(0)
      .find('.BlogFeed-origin').length
  ).toBe(1)
  expect(
    component
      .find('.BlogFeed')
      .find('.BlogFeed-blogPost')
      .at(0)
      .find('.BlogFeed-origin')
      .find('.BlogFeed-blogIcon').length
  ).toBe(1)
  expect(
    component
      .find('.BlogFeed')
      .find('.BlogFeed-blogPost')
      .at(0)
      .find('.BlogFeed-blogImage').length
  ).toBe(1)
  expect(
    component
      .find('.BlogFeed')
      .find('.BlogFeed-blogPost')
      .at(0)
      .find('.BlogFeed-blogTitle').length
  ).toBe(1)
  expect(
    component
      .find('.BlogFeed')
      .find('.BlogFeed-blogPost')
      .at(0)
      .find('.BlogFeed-blogText').length
  ).toBe(1)
  expect(
    component
      .find('.BlogFeed')
      .find('.BlogFeed-blogPost')
      .at(0)
      .find('.BlogFeed-blogLink').length
  ).toBe(1)
  expect(
    component
      .find('.BlogFeed')
      .find('.BlogFeed-blogPost')
      .at(0)
      .find('.BlogFeed-blogLink')
      .find('a')
      .find('.BlogFeed-readMoreIcon').length
  ).toBe(1)

  expect(
    component
      .find('.BlogFeed')
      .find('.BlogFeed-blogPost')
      .at(0)
      .find('.BlogFeed-blogImage')
      .prop('src')
  ).toBe(blogFeedResponseMock[0].image)
  expect(
    component
      .find('.BlogFeed')
      .find('.BlogFeed-blogPost')
      .at(0)
      .find('.BlogFeed-blogTitle')
      .text()
  ).toBe(blogFeedResponseMock[0].title)
  expect(
    component
      .find('.BlogFeed')
      .find('.BlogFeed-blogPost')
      .at(0)
      .find('.BlogFeed-blogText')
      .text()
  ).toBe(decode(blogFeedResponseMock[0].description))
  expect(
    component
      .find('.BlogFeed')
      .find('.BlogFeed-blogPost')
      .at(0)
      .find('.BlogFeed-blogLink')
      .prop('href')
  ).toBe(blogFeedResponseMock[0].link)
  expect(
    component
      .find('.BlogFeed')
      .find('.BlogFeed-blogPost')
      .at(0)
      .find('.BlogFeed-blogLink')
      .find('a')
      .find('.BlogFeed-readMoreIcon')
      .prop('src')
  ).toBe('/assets/custom/lfw16/images/ctaarrow.svg')
})

test('<BlogFeed /> displays the right number of feeds', () => {
  const component = mount(<WrappedComponent />)

  component.setState({
    blogPostCount: 5,
    feed: blogFeedResponseMock,
  })

  expect(component.find('.BlogFeed').find('.BlogFeed-blogPost').length).toBe(5)
})

test('<BlogFeed /> in case of no description for one feed it does display empty blog text', () => {
  const component = mount(<WrappedComponent />)

  component.setState({
    blogPostCount: 5,
    feed: [
      {
        title: 'title',
        link: 'link',
        image: 'image',
      },
    ],
  })

  expect(
    component
      .find('.BlogFeed')
      .find('.BlogFeed-blogPost')
      .at(0)
      .find('.BlogFeed-blogText')
      .text()
  ).toBe('')
})
