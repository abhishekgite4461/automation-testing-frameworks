/* eslint-disable react/no-multi-comp */
import React, { Component as mockComponent } from 'react'
import { mount } from 'enzyme'
import Lfw16 from 'src/custom/Lfw16/Lfw16'

jest.mock(
  'src/custom/Lfw16/showspace/Showspace',
  () =>
    class MockedShowspace extends mockComponent {
      render() {
        return <div className="MockedShowspace" {...this.props} />
      }
    }
)

jest.mock(
  'src/custom/Lfw16/tabtitle/TabTitle',
  () =>
    class MockedTabTitle extends mockComponent {
      render() {
        return <div className="MockedTabTitle" {...this.props} />
      }
    }
)

jest.mock('src/shared/components/common/CmsComponent/CmsComponent', () => {
  class MockedCmsComponent extends mockComponent {
    render() {
      return <div className="MockedCmsComponent" {...this.props} />
    }
  }
  // the only way of exporting default and named exports :(
  MockedCmsComponent.cmsTypes = [
    'imagelist',
    'carousel',
    'video',
    'iframe',
    'html',
  ]
  return MockedCmsComponent
})

const { WrappedComponent } = Lfw16

test('Lfw16 does not display anything if pageData is not an array', () => {
  const props = {
    pageContent: {},
    socialFeed: [],
    getCampaignSocialFeed: () => {},
  }

  const component = mount(<WrappedComponent {...props} />)

  expect(component.find('.Lfw16').length).toBe(0)
})

test('Lfw16 contains .Lfw16 with no children if pageData is an empty array', () => {
  const props = {
    pageContent: {
      pageData: [],
    },
    socialFeed: [],
    getCampaignSocialFeed: () => {},
  }

  const component = mount(<WrappedComponent {...props} />)

  expect(component.find('.Lfw16').length).toBe(1)
  expect(component.find('.Lfw16').children().length).toBe(0)
})

test('Lfw16 displays Showspace component with empty uniqueShopLink if the markup for the associated template is empty', () => {
  const props = {
    pageContent: {
      pageData: [
        {
          type: 'custom',
          data: {
            columns: 1,
            template: 'showspace',
            markup: '',
          },
        },
      ],
    },
    socialFeed: [],
    getCampaignSocialFeed: () => {},
  }

  const component = mount(<WrappedComponent {...props} />)

  expect(component.find('.Lfw16').length).toBe(1)
  expect(component.find('.Lfw16').children().length).toBe(1)
  expect(component.find('.Lfw16').find('.MockedShowspace').length).toBe(1)
  expect(
    component
      .find('.Lfw16')
      .find('.MockedShowspace')
      .props().uniqueShopLink
  ).toBe('')
})

test('Lfw16 displays Showspace component with empty uniqueShopLink if the markup for the associated template is an invalid JSON parsable string', () => {
  const props = {
    pageContent: {
      pageData: [
        {
          type: 'custom',
          data: {
            columns: 1,
            template: 'showspace',
            markup: "{ a: 'a'",
          },
        },
      ],
    },
    socialFeed: [],
    getCampaignSocialFeed: () => {},
  }

  const component = mount(<WrappedComponent {...props} />)

  expect(component.find('.Lfw16').length).toBe(1)
  expect(component.find('.Lfw16').children().length).toBe(1)
  expect(component.find('.Lfw16').find('.MockedShowspace').length).toBe(1)
  expect(
    component
      .find('.Lfw16')
      .find('.MockedShowspace')
      .props().uniqueShopLink
  ).toBe('')
})

test('Lfw16 displays Showspace component with expected uniqueShopLink if the markup for the associated template is a valid JSON parsable string', () => {
  const props = {
    pageContent: {
      pageData: [
        {
          type: 'custom',
          data: {
            columns: 1,
            template: 'showspace',
            markup:
              '{"uniqueShopLink": "https://www.topshop.com/en/tsuk/category/introducing-runway-to-retail-5843100/home?TS=1472547436372&intcmpid=MOBILE_LFWHUB_RUNWAYTORETAIL"}',
          },
        },
      ],
    },
    socialFeed: [],
    getCampaignSocialFeed: () => {},
  }

  const component = mount(<WrappedComponent {...props} />)

  expect(component.find('.Lfw16').length).toBe(1)
  expect(component.find('.Lfw16').children().length).toBe(1)
  expect(component.find('.Lfw16').find('.MockedShowspace').length).toBe(1)
  expect(
    component
      .find('.Lfw16')
      .find('.MockedShowspace')
      .props().uniqueShopLink
  ).toBe(
    'https://www.topshop.com/en/tsuk/category/introducing-runway-to-retail-5843100/home?TS=1472547436372&intcmpid=MOBILE_LFWHUB_RUNWAYTORETAIL'
  )
})

test('Lfw16 does not display the TabTitle component if the markup property of the associated template is empty', () => {
  const props = {
    pageContent: {
      pageData: [
        {
          type: 'custom',
          data: {
            columns: 1,
            template: 'tabtitle',
            markup: '',
          },
        },
      ],
    },
    socialFeed: [],
    getCampaignSocialFeed: () => {},
  }

  const component = mount(<WrappedComponent {...props} />)

  expect(component.find('.Lfw16').length).toBe(1)
  expect(component.find('.Lfw16').children().length).toBe(1)
  expect(component.find('.Lfw16').find('.MockedTabTitle').length).toBe(0)
})

test('Lfw16 does not display the TabTitle component if the markup property of the associated template is an invalid JSON parsable string', () => {
  const props = {
    pageContent: {
      pageData: [
        {
          type: 'custom',
          data: {
            columns: 1,
            template: 'tabtitle',
            markup: 'not valid JSON',
          },
        },
      ],
    },
    socialFeed: [],
    getCampaignSocialFeed: () => {},
  }

  const component = mount(<WrappedComponent {...props} />)

  expect(component.find('.Lfw16').length).toBe(1)
  expect(component.find('.Lfw16').children().length).toBe(1)
  expect(component.find('.Lfw16').find('.MockedTabTitle').length).toBe(0)
})

test('Lfw16 displays TabTitle component in case of valid template and markup property as JSON parsable string', () => {
  const props = {
    pageContent: {
      pageData: [
        {
          type: 'custom',
          data: {
            columns: 1,
            template: 'tabtitle',
            markup:
              '{"title":"Top Stories","icon":"/assets/custom/Lfw16/images/top-stories.svg","name":"topstories"}',
          },
        },
      ],
    },
    socialFeed: [],
    getCampaignSocialFeed: () => {},
  }

  const component = mount(<WrappedComponent {...props} />)

  expect(component.find('.Lfw16').length).toBe(1)
  expect(component.find('.Lfw16').children().length).toBe(1)
  expect(component.find('.Lfw16').find('.MockedTabTitle').length).toBe(1)
  expect(
    component
      .find('.Lfw16')
      .find('.MockedTabTitle')
      .props().title
  ).toBe('Top Stories')
  expect(
    component
      .find('.Lfw16')
      .find('.MockedTabTitle')
      .props().icon
  ).toBe('/assets/custom/Lfw16/images/top-stories.svg')
  expect(
    component
      .find('.Lfw16')
      .find('.MockedTabTitle')
      .props().name
  ).toBe('topstories')
})

test('Lfw16 does not show any custom component in case of unrecognized template', () => {
  const props = {
    pageContent: {
      pageData: [
        {
          type: 'custom',
          data: {
            columns: 1,
            template: 'unknown',
            markup: '',
          },
        },
      ],
    },
    socialFeed: [],
    getCampaignSocialFeed: () => {},
  }

  const component = mount(<WrappedComponent {...props} />)

  expect(component.find('.Lfw16').length).toBe(1)
  expect(component.find('.Lfw16').children().length).toBe(0)
})

test('Lfw16 shows CmsComponent in case of "type" not custom but belonging to the types handled by CmsComponent', () => {
  const props = {
    pageContent: {
      pageData: [
        {
          type: 'iframe',
          data: {
            columns: 1,
            template: 'unknown',
            markup: '',
          },
        },
      ],
    },
    socialFeed: [],
    getCampaignSocialFeed: () => {},
  }

  const component = mount(<WrappedComponent {...props} />)

  expect(component.find('.Lfw16').length).toBe(1)
  expect(component.find('.Lfw16').children().length).toBe(1)
  expect(component.find('.Lfw16').find('.MockedCmsComponent').length).toBe(1)
})

test('Lfw16 does not show anything in case of "type" not custom and not even handled by CmsComponents', () => {
  const props = {
    pageContent: {
      pageData: [
        {
          type: 'unknown',
          data: {
            columns: 1,
            template: 'unknown',
            markup: '',
          },
        },
      ],
    },
    socialFeed: [],
    getCampaignSocialFeed: () => {},
  }

  const component = mount(<WrappedComponent {...props} />)

  expect(component.find('.Lfw16').length).toBe(1)
  expect(component.find('.Lfw16').children().length).toBe(0)
})
