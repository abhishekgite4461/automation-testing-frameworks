import peeriusReducer from '../../../src/shared/reducers/common/peeriusReducer'
import { clickRecommendation } from '../../../src/shared/actions/components/RecommendationsActions'
import {
  setPeeriusCallback,
  trackPeerius,
  setPeeriusDomain,
} from '../../../src/shared/actions/common/peeriusActions'
import { setConfig } from '../../../src/shared/actions/common/configActions'
import { preCacheReset } from '../../../src/shared/actions/common/pageCacheActions'
import configureMockStore from '../lib/configure-mock-store'
import assert from 'assert'

describe('peeriusActions', () => {
  const previousEnv = process.env.PEERIUS_ENV
  const processBrowser = process.browser
  beforeAll(() => {
    jest.clearAllMocks()
    process.env.PEERIUS_ENV = 'uat'
    window.Peerius = {
      dynamic: jest.fn(),
      smartRecsSendClick: jest.fn(),
    }
    process.browser = true
    window.loadScripts = jest.fn()
  })

  afterAll(() => {
    process.env.PEERIUS_ENV = previousEnv
    delete window.Peerius
    process.browser = processBrowser
  })

  it('SET_PEERIUS_CALLBACK', () => {
    const callbackSpy = jest.fn()
    const state = { test: true }
    const action = setPeeriusCallback(callbackSpy)
    const expected = {
      ...state,
      smartRecs: callbackSpy,
    }

    expect(peeriusReducer(state, action)).toEqual(expected)
    expect(window.PeeriusCallbacks.smartRecs).toBe(callbackSpy)
  })

  it('TRACK_PEERIUS, when not dynamic', () => {
    const state = {
      domain: 'foo',
      test: true,
      dynamic: false,
      baseTrack: { test: true },
    }
    const track = { tracking: 'test' }
    const action = trackPeerius(track)
    const expected = {
      ...state,
      track: {
        ...state.baseTrack,
        ...track,
      },
      dynamic: true,
    }

    expect(peeriusReducer(state, action)).toEqual(expected)
    expect(window.loadScripts).toHaveBeenCalledWith([
      {
        src: `https://foo.peerius.com/tracker/peerius.page`,
      },
    ])
    expect(window.Peerius.dynamic).not.toHaveBeenCalled()
  })

  it('TRACK_PEERIUS, when dynamic', () => {
    const state = { test: true, dynamic: true, baseTrack: { test: true } }
    const track = { tracking: 'test' }
    const action = trackPeerius(track)
    const expected = {
      ...state,
      track: {
        ...state.baseTrack,
        ...track,
      },
    }

    expect(peeriusReducer(state, action)).toEqual(expected)
    expect(window.Peerius.extraXml).toEqual(
      expect.stringContaining('{"test":true,"tracking":"test"}')
    )
    expect(window.Peerius.dynamic).toHaveBeenCalled()
  })

  it('CLICK_RECOMMENDATION', () => {
    const state = { test: true }
    const action = clickRecommendation(1234)

    expect(peeriusReducer(state, action)).toEqual(state)
    expect(window.Peerius.smartRecsSendClick).toHaveBeenCalledWith(1234)
  })

  it('SET_CONFIG', () => {
    const state = { domain: null, baseTrack: { lang: null } }
    const config = {
      peeriusDomain: 'test',
      peeriusLang: 'en-gb',
    }
    const action = setConfig(config)
    const expected = {
      ...state,
      domain: config.peeriusDomain,
      baseTrack: {
        ...state.baseTrack,
        lang: config.peeriusLang,
      },
    }

    expect(peeriusReducer(state, action)).toEqual(expected)
  })

  it('SET_PEERIUS_DOMAIN', (done) => {
    const store = configureMockStore()

    store.subscribeUntilPasses(() => {
      assert.deepEqual(store.getState().peerius.domain, 'uat')
      done()
    })

    store.dispatch(setPeeriusDomain())
  })

  it('PRE_CACHE_RESET should wipe peerius user data', (done) => {
    const store = configureMockStore({
      peerius: {
        baseTrack: { user: { secret: 'secret' }, test: 'test' },
        test: 'test',
      },
    })

    store.subscribeUntilPasses(() => {
      assert.deepEqual(store.getState().peerius, {
        baseTrack: { test: 'test' },
        test: 'test',
      })
      done()
    })

    store.dispatch(preCacheReset())
  })
})
