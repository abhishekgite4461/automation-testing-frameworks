import configureStore from '../../../src/shared/lib/configure-store'
import * as StoreLocatorActions from '../../../src/shared/actions/components/StoreLocatorActions'

test('Store Locator reducer initialises google map with default values', () => {
  const store = configureStore()
  const { currentLat, currentLng, currentZoom } = store.getState().storeLocator
  expect(currentLat).toBeTruthy()
  expect(currentZoom).toBeTruthy()
  expect(currentLng).toBeTruthy()
})

test('initMap initialises google map widget using google js library', () => {
  window.google.maps = { Map: jest.fn(() => ({})) }
  const store = configureStore()
  store.dispatch(StoreLocatorActions.initMapWhenGoogleMapsAvailable())
  expect(global.window.google.maps.Map).toHaveBeenCalled()
  expect(window.map).toBeTruthy()
})
