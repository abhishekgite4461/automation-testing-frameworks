import { isEmpty } from 'ramda'
import { createStore } from 'redux'
import configureStore from '../../../src/shared/lib/configure-store'
import configureMockStore from '../lib/configure-mock-store'
import UserLocatorReducer from '../../../src/shared/reducers/components/UserLocatorReducer'
import { browserHistory } from 'react-router'
import * as actions from '../../../src/shared/actions/components/UserLocatorActions'

const searchTerm = 'Oxford Street'
const description = 'Oxford Street, London'
const place = {
  place_id: '1234',
  description: 'Oxford Street, London',
}

describe('UserLocatorActions', () => {
  const windowGoogleMaps = window.google.maps
  beforeAll(() => {
    window.google.maps = {
      places: {
        AutocompleteService: jest.fn(),
        AutocompleteSessionToken: jest.fn(),
        PlacesServiceStatus: { OK: 'OK' },
        PlacesService: jest.fn().mockImplementation(() => ({
          getDetails(query, callback) {
            callback(
              {
                geometry: {
                  location: {
                    lat() {
                      return 11
                    },
                    lng() {
                      return 22
                    },
                  },
                },
              },
              window.google.maps.places.PlacesServiceStatus.OK
            )
          },
        })),
      },
      GeocoderStatus: { OK: 'OK' },
    }
  })

  afterAll(() => {
    window.google.maps = windowGoogleMaps
  })

  it('RESET_SEARCH_TERM', () => {
    const store = configureStore({
      userLocator: {
        searchTerm: 'abc',
        predictions: [],
        selectedPlaceDetails: {},
        getCurrentLocationError: false,
      },
    })
    expect(store.getState().userLocator.searchTerm).toBe('abc')
    store.dispatch(actions.resetSearchTerm())
    expect(store.getState().userLocator.searchTerm).toBe('')
  })

  it('FILL_PREDICTIONS', () => {
    const store = createStore(UserLocatorReducer)
    store.dispatch(actions.fillPredictions(searchTerm, [{ description }]))
    expect(store.getState().predictions.length === 1).toBeTruthy()
    expect(store.getState().searchTerm).toBe(searchTerm)
    expect(store.getState().predictions[0].description).toBe(description)
  })

  it('RESET_PREDICTIONS', () => {
    const store = createStore(UserLocatorReducer)
    store.dispatch(actions.fillPredictions(searchTerm, [{ description }]))
    expect(store.getState().predictions.length === 1).toBeTruthy()
    store.dispatch(actions.resetPredictions())
    expect(store.getState().predictions.length === 0).toBeTruthy()
  })

  it('SET_SELECTED_PLACE', () => {
    const store = createStore(UserLocatorReducer)
    store.dispatch(actions.setSelectedPlace(place))
    expect(Object.is(store.getState().selectedPlaceDetails, place)).toBeTruthy()
  })

  it('SET_USER_LOCATOR_PENDING sets pending', () => {
    const store = createStore(UserLocatorReducer)
    expect(store.getState().pending).toBeFalsy()
    store.dispatch(actions.setUserLocatorPending(true))
    expect(store.getState().pending).toBeTruthy()
    store.dispatch(actions.setUserLocatorPending(false))
    expect(store.getState().pending).toBeFalsy()
  })

  it('RESET_SELECTED_PLACE', () => {
    const store = createStore(UserLocatorReducer)
    store.dispatch(actions.setSelectedPlace(place))
    expect(Object.is(store.getState().selectedPlaceDetails, place)).toBeTruthy()
    store.dispatch(actions.resetSelectedPlace())
    expect(isEmpty(store.getState().selectedPlaceDetails)).toBeTruthy()
  })

  it('GET_CURRENT_LOCATION_ERROR', (done) => {
    const store = configureStore()
    const fakeInput = { focus: jest.fn() }
    store.subscribe(() => {
      expect(store.getState().userLocator.getCurrentLocationError).toBeTruthy()
      done()
    })
    store.dispatch(actions.getCurrentLocationError(fakeInput))
  })

  it('searchStores navigates to store-locator passing coordinates when United Kingdom selected', () => {
    jest.spyOn(browserHistory, 'push').mockImplementation(() => {})
    const store = configureMockStore({
      userLocator: {
        searchTerm: '',
        predictions: [],
        selectedPlaceDetails: {
          place_id: '123',
        },
        getCurrentLocationError: false,
      },
      storeLocator: {
        selectedCountry: 'United Kingdom',
      },
    })

    store.dispatch(actions.searchStores())

    expect(browserHistory.push).toHaveBeenCalledWith({
      pathname: '/store-locator',
      query: {
        latitude: 11,
        longitude: 22,
      },
    })
  })

  it('searchStores navigates to store-locator passing country when United Kingdom not selected', () => {
    jest.spyOn(browserHistory, 'push').mockImplementation(() => {})
    const store = configureMockStore({
      userLocator: {
        searchTerm: '',
        predictions: [],
        selectedPlaceDetails: {},
        getCurrentLocationError: false,
      },
      storeLocator: {
        selectedCountry: 'Spain',
      },
    })

    store.dispatch(actions.searchStores())

    expect(browserHistory.push).toHaveBeenCalledWith({
      pathname: '/store-locator',
      query: {
        country: 'Spain',
      },
    })
  })

  const predictionsFake = ['Roma', 'Milano', 'Pisa']

  it('"autocomplete" fills the predictions once these are returned by the autocompletion service', () => {
    window.google.maps.places.AutocompleteService.mockImplementation(() => ({
      getPlacePredictions(query, callback) {
        callback(
          predictionsFake,
          window.google.maps.places.PlacesServiceStatus.OK
        )
      },
    }))
    const store = configureStore({ userLocator: { predictions: [] } })
    expect(store.getState().userLocator.predictions).toEqual([])
    store.dispatch(actions.autocomplete('abc'))
    expect(store.getState().userLocator.predictions).toEqual(predictionsFake)
  })

  it('"autocomplete" clears the predictions if the search term provided is empty', () => {
    const store = configureStore({
      userLocator: { predictions: predictionsFake },
    })
    expect(store.getState().userLocator.predictions).toEqual(predictionsFake)
    store.dispatch(actions.autocomplete(''))
    expect(store.getState().userLocator.predictions).toEqual([])
  })

  it('"onGetCurrentPositionSuccess" set the selected place', () => {
    const store = configureStore({
      userLocator: { selectedPlaceDetails: {} },
      forms: {
        userLocator: {
          fields: {
            userLocation: {
              value: '',
            },
          },
        },
      },
    })

    const placeDetails = { description: 'London, United Kingdom' }
    window.google.maps.Geocoder = jest.fn(() => ({
      geocode: (location, cb) => {
        cb([placeDetails], window.google.maps.GeocoderStatus.OK)
      },
    }))
    expect(store.getState().userLocator.selectedPlaceDetails).toEqual({})
    expect(store.getState().forms.userLocator.fields.userLocation.value).toBe(
      ''
    )

    store.dispatch(
      actions.onGetCurrentPositionSuccess({
        coords: { latitude: 123, longitude: 456 },
      })
    )

    expect(store.getState().userLocator.selectedPlaceDetails).toEqual(
      placeDetails
    )
    expect(store.getState().forms.userLocator.fields.userLocation.value).toBe(
      placeDetails.description
    )
  })
})
