import nock from 'nock'
import assert from 'assert'
import configureMockStore from '../lib/configure-mock-store'

jest.mock('src/shared/lib/checkout-utilities/klarna-utils')
import {
  loadFormRequest,
  authorizeRequest,
  reAuthorizeRequest,
} from 'src/shared/lib/checkout-utilities/klarna-utils'
import * as klarnaActions from '../../../src/shared/actions/common/klarnaActions'

jest.mock('react-router')

const paymentsMethodMock = {
  VISA: { label: 'Visa' },
  MCARD: { label: 'Mastercard' },
  AMEX: { label: 'American Express' },
  SWTCH: { label: 'Switch / Maestro' },
  ACCNT: { label: 'Account Card' },
  KLRNA: { label: 'Try before you buy' },
  PYPAL: { label: 'PayPal' },
}
const store = configureMockStore({ paymentMethods: paymentsMethodMock })

// KLARNA initial state
/* klarna:
{ sessionId: '',
  clientToken: '',
  loadFormPending: false,
  isUpdatingEmail: false,
  email: '',
  formSubmitted: false
} */

test('KLARNA-ACTIONS : Create New Session with Klarna, we expect back sessionID and klarnaTokenID', (done) => {
  nock('http://localhost:3000')
    .post('/api/klarna-session', { orderId: 1050034 })
    .reply(200, { sessionId: 12345, clientToken: 'abcdef' })

  store.subscribeUntilPasses(() => {
    assert.deepEqual(store.getState().loaderOverlay.visible, false)
    assert.deepEqual(store.getState().klarna.sessionId, 12345)
    assert.deepEqual(store.getState().klarna.clientToken, 'abcdef')
    done()
  })

  store.dispatch(klarnaActions.createSession({ orderId: 1050034 }, true))
})

test('KLARNA-ACTIONS : createSession action error, expect handle error via modal warning', (done) => {
  nock('http://localhost:3000')
    .post('/api/klarna-session')
    .reply(400, { error: { data: 'error' } })

  store.subscribeUntilPasses(() => {
    assert.deepEqual(store.getState().loaderOverlay.visible, false)
    assert.deepEqual(store.getState().modal.open, true)

    done()
  })

  store.dispatch(klarnaActions.createSession())
})

/*
  The loadForm function, use che Klarna javascript libs
  send a klarna-client-token and a html node container.
  The klarna js inject on automatic a input form (email) if validate the request.
 */
test('KLARNA-ACTION : After create a new session, load klarna email form ', async () => {
  loadFormRequest.mockReturnValue(Promise.resolve())

  await store.dispatch(
    klarnaActions.loadForm('validToken', 'container', {
      details: 'fakeDetails',
    })
  )

  assert.deepEqual(store.getState().klarna.shown, true)
  assert.deepEqual(store.getState().klarna.loadFormPending, false)
})

test('KLARNA-ACTION : load klarna email form error ', async () => {
  loadFormRequest.mockReturnValue(Promise.reject())
  const TMPstore = configureMockStore({ paymentMethods: paymentsMethodMock })

  await TMPstore.dispatch(
    klarnaActions.loadForm('NOT-validToken', 'container', {
      details: 'fakeDetails',
    })
  )

  assert.deepEqual(TMPstore.getState().klarna.shown, undefined)
  assert.deepEqual(TMPstore.getState().klarna.loadFormPending, false)
  assert.deepEqual(TMPstore.getState().modal.open, true)
})

/*
 The Authorize function (authorizeRequest), use che Klarna javascript libs
 send the update details (update checkout).
 The klarna js inject on automatic a input form (email) [???] if the request is approved by klarna.
 */
test('KLARNA-ACTIONS : load authorize function via klarna js libraries', async () => {
  authorizeRequest.mockReturnValue(
    Promise.resolve({ authorization_token: 'validToken' })
  )

  await store.dispatch(klarnaActions.authorize('validDetails'))

  expect(store.getState().klarna.authorizationToken).toBe('validToken')
})

test('KLARNA-ACTIONS : handle errors on klarna authorize', async () => {
  authorizeRequest.mockReturnValue(Promise.reject(false))
  const TMPstore = configureMockStore({ paymentMethods: paymentsMethodMock })

  await TMPstore.dispatch(klarnaActions.authorize('some-error-on-else'))

  assert.deepEqual(TMPstore.getState().klarna.authorizationToken, undefined)
  assert.deepEqual(TMPstore.getState().klarna.authorizePending, false)
  assert.deepEqual(TMPstore.getState().modal.open, true)
})

test('KLARNA-ACTIONS : setup checkout payments when klarna is not approved', () => {
  store.dispatch(klarnaActions.handleDisapproval())
  expect(store.getState().modal.open).toEqual(true)
  // VERY IMPORTANT TEST : klarna payment method is removed from list !!!
  expect(store.getState().paymentMethods.KLARNA).toEqual(undefined)
})

test('KLARNA ACTION : Update directly klarna session (error) ', (done) => {
  nock('http://localhost:3000')
    .put('/api/klarna-session', { orderId: 1050034 })
    .reply(400, { error: { data: 'error' } })

  store.subscribeUntilPasses(() => {
    assert.deepEqual(store.getState().loaderOverlay.visible, false)
    assert.deepEqual(store.getState().modal.open, true)
    assert.deepEqual(store.getState().paymentMethods.KLARNA, undefined)

    done()
  })

  store.dispatch(klarnaActions.directKlarnaUpdateSession({ orderId: 1050034 }))
})

// Test update and authorize Error, catch was missed before...
// klarnaActions.__Rewire__('assembleSessionPayload', () => {
//   return { sessionpayload: '123456' }
// })
test('KLARNA ACTION : update and authorize (error) ', (done) => {
  nock('http://localhost:3000')
    .post('/api/klarna-session')
    .reply(400, { error: { data: 'error' } })

  store.subscribeUntilPasses(() => {
    assert.deepEqual(store.getState().loaderOverlay.visible, false)
    assert.deepEqual(store.getState().modal.open, true)

    done()
  })

  store.dispatch(
    klarnaActions.updateAndAuthorize(
      { orderId: 1050034 },
      'validDetails',
      'newHash'
    )
  )
})

test('KLARNA ACTION : update and authorize ', (done) => {
  const TMPstore = configureMockStore({ paymentMethods: paymentsMethodMock })
  nock('http://localhost:3000')
    .post('/api/klarna-session')
    .reply(200, {})

  nock('http://localhost:3000')
    .put('/api/klarna-session', { sessionpayload: '123456' })
    .reply(200, {})

  TMPstore.subscribeUntilPasses(() => {
    assert.deepEqual(TMPstore.getState().loaderOverlay.visible, false)
    assert.deepEqual(TMPstore.getState().modal.open, false)

    done()
  })

  TMPstore.dispatch(
    klarnaActions.updateAndAuthorize(
      { orderId: 1050034 },
      'validDetails',
      'newHash'
    )
  )
})

// Reauthorized is only called when customer has changed something
// and has agreed to submit the order --> it submits the order on success
test('KLARNA ACTION : re-Authorize and Update Email Address (not-approved)', async () => {
  reAuthorizeRequest.mockReturnValue(
    Promise.resolve({ authorization_token: 'validToken' })
  )

  await store.dispatch(
    klarnaActions.reAuthorize(
      { updateDetails: 'validDetails' },
      { orderId: 1050034 },
      false,
      true
    )
  )

  assert.deepEqual(store.getState().loaderOverlay.visible, false)
  assert.deepEqual(store.getState().modal.open, true)
  assert.deepEqual(store.getState().paymentMethods.KLARNA, undefined)
})
