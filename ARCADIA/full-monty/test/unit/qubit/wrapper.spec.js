import { shallow } from 'enzyme'
import React from 'react'

import testComponentHelper from 'test/unit/helpers/test-component'
import generateMockFormProps from 'test/unit/helpers/form-props-helper'

import Product from 'src/shared/components/common/Product/Product'
import Header from 'src/shared/components/containers/Header/Header'
import HomeDeliveryV2 from 'src/shared/components/containers/CheckoutV2/Delivery/HomeDelivery'
import DeliveryPayment from 'src/shared/components/containers/CheckoutV2/DeliveryPayment/DeliveryPaymentContainer'
import MiniBag from 'src/shared/components/containers/MiniBag/MiniBag'
import ShoppingBag from 'src/shared/components/common/ShoppingBag/ShoppingBag'
import OrderProduct from 'src/shared/components/common/OrderProducts/OrderProduct'
import { WrappedLoginContainer } from 'src/shared/components/containers/CheckoutV2/LoginContainer'
import { WrappedPlpContainer } from 'src/shared/components/containers/PLP/PlpContainer'
import ProductImages from 'src/shared/components/common/ProductImages/ProductImages'
import AddToBag from 'src/shared/components/common/AddToBag/AddToBag'
import { WrappedProductDetail } from 'src/shared/components/containers/ProductDetail/ProductDetail'
import Espot from '../../../src/shared/components/containers/Espot/Espot'
import Imagecontainer from '../../../src/shared/components/containers/MegaNav/ImageContainer'

// mocks
import BundleProducts from 'src/shared/components/common/BundleProducts/BundleProducts'
import bundleMocksMultiple from './../../mocks/bundleMocksMultiple'
import {
  product,
  productStock,
  deliveryPayment,
} from './../../mocks/qubitMobileMocks'
import productMock from './../../mocks/product-detail'
import {
  initialMiniBagProps as initialProps,
  initialMiniBagFormProps as initialFormProps,
} from 'src/shared/components/containers/MiniBag/__tests__/miniBagMocks'
import ProductList from '../../../src/shared/components/containers/PLP/ProductList'
import products from '../../mocks/productMocks'

const renderProductComponent = testComponentHelper(Product.WrappedComponent)
const renderHeaderComponent = testComponentHelper(Header.WrappedComponent)
const renderPlpContainerComponent = testComponentHelper(WrappedPlpContainer)

const renderHomeDeliveryV2Component = testComponentHelper(
  HomeDeliveryV2.WrappedComponent
)
const renderDeliveryPaymentContainer = testComponentHelper(
  DeliveryPayment.WrappedComponent.WrappedComponent.WrappedComponent
)
const renderMiniBagComponent = testComponentHelper(MiniBag.WrappedComponent)
const renderHomeOrderProductComponent = testComponentHelper(
  OrderProduct.WrappedComponent
)

const renderLoginContainerV2 = testComponentHelper(WrappedLoginContainer)
const renderProductImagesComponent = testComponentHelper(ProductImages)
const renderShoppingBagComponent = testComponentHelper(
  ShoppingBag.WrappedComponent
)
const renderAddToBagComponent = testComponentHelper(AddToBag.WrappedComponent)
const renderProductDetailsComponent = testComponentHelper(
  WrappedProductDetail,
  { disableLifecycleMethods: true }
)
const renderBundleProductsComponent = testComponentHelper(
  BundleProducts.WrappedComponent
)
const renderImagecontainerComponent = testComponentHelper(
  Imagecontainer.WrappedComponent
)

describe('@Qubit Wrapper', () => {
  describe('@Product for mobile and desktop', () => {
    // WRAPPER REMOVED ??
    it.skip('wrapping ProductBanners component', () => {
      const { wrapper } = renderProductComponent(product.Product)
      const qubitWrapper = wrapper.find('.Product-link').getElement().props
        .children[1]
      expect(qubitWrapper.props.id).toBe(
        'Qubit-Product-ProductBanners-24393842'
      )
      expect(qubitWrapper.props.productId).toBe(24393842)
    })
    it('Wrapping all product details', () => {
      const { wrapper } = renderProductComponent(product.Product)
      const qubitWrapper = wrapper
        .find('.Product-link')
        .parent()
        .getElement()
      expect(qubitWrapper.props.id).toBe('qubit-PLP-product-tile')
    })
    it('Wrapping Product-meta class', () => {
      const { wrapper } = renderProductComponent(product.Product)
      const qubitWrapper = wrapper
        .find('.Product-meta')
        .parent()
        .getElement()
      expect(qubitWrapper.props.id).toBe('PLP-Trending-Product-24393842')
    })
  })

  describe('@Header for mobile and desktop', () => {
    const { wrapper } = renderHeaderComponent(product.HeaderMock)
    it('wrapping header component', () => {
      const qubitWrapper = wrapper
        .find('.Header')
        .parent()
        .getElement()
      expect(qubitWrapper.props.id).toBe('qubit-Header')
    })
    it('wrapping brandlogo in header component', () => {
      const qubitWrapper = wrapper
        .find('.Header-brandLogo')
        .parent()
        .getElement()
      expect(qubitWrapper.props.id).toBe('qubit-Header-brandLogo')
    })
    it('wrapping all the header container', () => {
      const qubitWrapper = wrapper
        .find('.Header-container')
        .parent()
        .getElement()
      expect(qubitWrapper.props.id).toBe('qubit-Header-container')
    })
  })

  describe('Delivery Instructions ', () => {
    it('wrapping DeliveryInstructions for V2 New User', () => {
      const { wrapper } = renderHomeDeliveryV2Component(product.HomeDelivery)
      const qubitWrapper = wrapper.find('.HomeDeliveryV2').getElement().props
        .children[2]
      expect(qubitWrapper.props.id).toBe(
        'qubit-HomeDelivery-DeliveryInstructions'
      )
    })
    it('wrapping DeliveryInstructions for V2 Returning User', () => {
      const { wrapper } = renderDeliveryPaymentContainer(deliveryPayment)
      const qubitWrapper = wrapper
        .find('#qubit-HomeDelivery-DeliveryInstructions')
        .getElement()
      expect(qubitWrapper.props.id).toBe(
        'qubit-HomeDelivery-DeliveryInstructions'
      )
    })
  })
  describe('@PlpContainer', () => {
    it('wrapping plpContainer if No Search Results for desktop and mobile', () => {
      const { wrapper } = renderPlpContainerComponent(
        product.PlpContainerNoResultsMock
      )
      const qubitWrapper = wrapper
        .find('.PlpContainer')
        .parent()
        .getElement()
      expect(qubitWrapper.props.id).toBe('qubit-Plp-NoResults')
    })
    it('NO QUBIT wrapping plpContainer if there are search results for desktop and mobile', () => {
      const { wrapper } = renderPlpContainerComponent(
        product.PlpContainerResultsMock
      )
      expect(wrapper.find('#qubit-Plp-NoResults').length).toEqual(0)
    })
    it('wrapping product list container', () => {
      const { wrapper } = renderPlpContainerComponent(
        product.PlpContainerNoResultsMock
      )
      const qubitWrapper = wrapper
        .find('.PlpContainer-productListContainer')
        .parent()
        .getElement()
      expect(qubitWrapper.props.id).toBe('qubit-plp-productListContainer')
    })
  })

  describe('@MiniBag', () => {
    it('wrapping h5 tag (with div in between)', () => {
      const { wrapper } = renderMiniBagComponent(product.MiniBagMock)
      const qubitWrapper = wrapper
        .find('h5.MiniBag-header')
        .parents()
        .get(1)
      expect(qubitWrapper.props.id).toBe('qubit-MiniBag-header')
    })
    it('wrapping MiniBag-summarySection', () => {
      const generateProps = (newFormProps) => ({
        ...initialProps,
        ...generateMockFormProps(initialFormProps, newFormProps),
      })
      const { wrapper } = renderMiniBagComponent(generateProps())
      const qubitWrapper = wrapper
        .find('.MiniBag-summarySection')
        .parent()
        .getElement()
      expect(qubitWrapper.props.id).toBe('qubit-mini-bag-summary')
    })
  })
  describe('@ProductImages', () => {
    it('wrapping img tag', () => {
      const { wrapper } = renderProductImagesComponent(
        product.ProductImagesMock
      )
      expect(
        wrapper.find(`#PLP-Social-Proof-${product.ProductImagesMock.productId}`)
          .length
      ).toBe(1)
    })
  })
  describe('@DeliveryOptions for Checkout v2', () => {
    it('NO QUBIT wrapping plpContainer if there are search results for desktop and mobile', () => {
      const { wrapper } = renderPlpContainerComponent(
        product.PlpContainerResultsMock
      )
      expect(wrapper.find('#qubit-Plp-NoResults').length).toEqual(0)
    })
  })
  describe('@orderProduct', () => {
    it('Qubit wrapper Order Product class : OrderProducts-mediaLeft ', () => {
      const { wrapper } = renderHomeOrderProductComponent(productStock)
      const qubitWrapper = wrapper
        .find('.OrderProducts-mediaLeft')
        .parent()
        .getElement()
      expect(qubitWrapper.props.id).toBe('qubit-OrderProducts-mediaLeft')
      expect(qubitWrapper.props.productId).toBe(92498234)
    })
    it('Qubit wrapper Order Product class : OrderProducts-media ', () => {
      const { wrapper } = renderHomeOrderProductComponent(productStock)
      const qubitWrapper = wrapper
        .find('.OrderProducts-media')
        .parent()
        .getElement()
      expect(qubitWrapper.props.id).toBe('qubit-OrderProducts-media')
      expect(qubitWrapper.props.productId).toBe(92498234)
    })
    it('Qubit wrapper Order Product class : OrderProducts-mediaBody ', () => {
      const { wrapper } = renderHomeOrderProductComponent(productStock)
      const qubitWrapper = wrapper.find('#qubit-OrderProducts-mediaBody')
      expect(qubitWrapper.props().productId).toBe(92498234)

      expect(qubitWrapper.find('.OrderProducts-productSize').length).toBe(1)
    })
  })
  describe('@LoginConatiner mobile and desktop', () => {
    it('wrapping all login Container Checkout V2', () => {
      const { wrapper } = renderLoginContainerV2({
        sendEvent: () => {},
        getOrderSummary: jest.fn(),
      })
      const qubitWrapper = wrapper
        .find('.LoginContainer')
        .parent()
        .getElement()
      expect(qubitWrapper.props.id).toBe('qubit-checkoutContainer')
    })
  })
  describe('@ShoppingBag desktop/mobile', () => {
    it('wrapping ShoppingBag class Container', () => {
      const { wrapper } = renderShoppingBagComponent({
        bagProducts: [],
        inCheckout: false,
        scrollMinibag: jest.fn(),
      })
      const qubitWrapper = wrapper
        .find('.ShoppingBag')
        .parent()
        .getElement()
      expect(qubitWrapper.props.id).toBe('qubit-shoppingbagdelivery')
    })
  })
  describe('@AddToBag desktop/mobile', () => {
    it('wrapping AddToBag div class Container', () => {
      const { wrapper } = renderAddToBagComponent({
        productId: '12345',
        quantity: 1,
        isMobile: false,
        addToBagIsReady: false,
        addToBag: jest.fn(),
        toggleModal: jest.fn(),
        toggleMiniBag: jest.fn(),
      })
      const qubitWrapper = wrapper
        .find('.AddToBag')
        .parent()
        .getElement()
      expect(qubitWrapper.props.id).toBe('qubit-pdp-add-to-bag')
    })
  })
  describe('@ProductDetails PDP', () => {
    const ProductDetailsinitialProps = {
      isFeatureCarouselThumbnailEnabled: true,
      updateShowItemsError: () => {},
      enableSizeGuideButtonAsSizeTile: true,
      enableDropdownForLongSizes: true,
      maximumNumberOfSizeTiles: 8,
      siteId: 12556,
      product: productMock,
      activeItem: productMock.items[0],
      isMobile: true,
      location: { pathname: '...' },
      region: 'uk',
      visited: [],
      sandboxPages: {
        mobileESpotPDPPos1: {},
      },
    }
    it('Wrapping HistoricalPrice', () => {
      const { wrapper } = renderProductDetailsComponent(
        ProductDetailsinitialProps
      )
      const qubitWrapper = wrapper.find('#qubit-pdp-HistoricalPrice')
      expect(qubitWrapper.length).toBe(1)
      expect(qubitWrapper.find('.HistoricalPrice--pdp').length).toBe(1)
    })
    it('Wrapping Espot Column 2 Position 2', () => {
      const { wrapper } = renderProductDetailsComponent(
        ProductDetailsinitialProps
      )
      let foundQubitWrappedEspot = false
      wrapper.find('.ProductDetail-espot').forEach((node) => {
        if (node.getElement().props.qubitid === 'qubit-pdp-EspotCol2Pos2') {
          foundQubitWrappedEspot = true
        }
      })
      expect(foundQubitWrappedEspot).toBe(true)
    })
    it('Wrapping Div .Product-title', () => {
      const { wrapper } = renderProductDetailsComponent(
        ProductDetailsinitialProps
      )
      expect(
        wrapper.find('#qubit-pdp-ProductDetail').find('.ProductDetail-title')
          .length
      ).toBe(1)
    })
  })
  describe('@BundleProducts desktop/mobile', () => {
    it('wrapping BundleProducts Div', () => {
      const initialProps = {
        items: bundleMocksMultiple.bundleSlots,
        carousel: {},
      }
      const { wrapper } = renderBundleProductsComponent(initialProps)
      const qubitWrapper = wrapper
        .find('.BundleProducts')
        .parent()
        .getElement()
      expect(qubitWrapper.props.id).toBe('qubit-pdp-BundleProducts')
    })
  })
  describe('@ProductList desktop', () => {
    it('adds content spots', () => {
      const espotsDataMock = [
        {
          id: 'productListItem1',
          Position: 1,
        },
        {
          id: 'productListItem2',
          Position: 3,
        },
        {
          id: 'productListItem3',
          Position: 6,
        },
      ]
      const { WrappedComponent } = ProductList
      const wrapper = shallow(
        <WrappedComponent
          products={products}
          grid={1}
          espotsData={espotsDataMock}
        />
      )
      wrapper.find(Espot).forEach((node) => {
        expect(node.getElement().props.qubitid).toBe(
          'qubit-plp-EspotProductList4'
        )
      })
    })
  })
  describe('@MegaNav ImageContainer', () => {
    const mockProps = {
      subcategory: {
        type: 'image',
        contentId: 'content_1',
        categoryId: '2146531',
        header: '',
        paddingTop: 0,
        label: 'Top Nav - Topshop Denim',
        seoUrl:
          'http://www.topshop.com/en/tsuk/category/new-in-this-week-2169932/top-nav-topshop-denim-7389540',
        image: {
          url: 'http://www.topshop.com/wcsstore/images/NEW_WK23_NEW_IN.jpg',
          height: 250,
          width: 870,
        },
      },
      className: 'MegaNav-imageContainer',
      apiEnvironment: 'prod',
      storeCode: 'tsuk',
    }
    it('render qubit wrapper around image', () => {
      const { wrapper } = renderImagecontainerComponent(mockProps)
      const qubitWrapper = wrapper.find('#qubit-MegaNav-imageContainer')
      expect(qubitWrapper.length).toBe(1)
    })
  })
})
