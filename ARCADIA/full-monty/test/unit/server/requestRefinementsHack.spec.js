import { translatePriceKeyInRefinements } from '../../../src/server/lib/requestRefinementHack'
import { compose, map, join, toPairs } from 'ramda'

const encode = compose(
  encodeURIComponent,
  JSON.stringify
)
const strigiyfyQuery = compose(
  join('&'),
  map(join('=')),
  toPairs,
  map(encode)
)

test('translatePriceKeyInRefinements should return a new url with a translated refinements query', () => {
  expect.assertions(4)
  const refinements = [
    { key: 'produkttyp', value: 'schuhe' },
    { key: 'preis', value: '10,20' },
  ]
  const req = {
    headers: { 'brand-code': 'tsde' },
    url: {
      query: { refinements: JSON.stringify(refinements), other: 'other' },
      pathname: '/path',
    },
  }

  const url = translatePriceKeyInRefinements(req)
  const expectedQuery = {
    refinements: JSON.stringify([
      { key: 'produkttyp', value: 'schuhe' },
      { key: 'price', value: '10,20' },
    ]),
    other: 'other',
  }
  const expectedQueryString = `?${strigiyfyQuery(expectedQuery)}`

  expect(url.query).toEqual(expectedQuery)
  expect(url.search).toBe(expectedQueryString)
  expect(url.path).toBe(`/path${expectedQueryString}`)
  expect(url.href).toBe(`/path${expectedQueryString}`)
})

test('translatePriceKeyInRefinements should return the original url if query is missing', () => {
  expect.assertions(4)
  const req = {
    headers: { 'brand-code': 'tsde' },
    url: { query: null, pathname: '/' },
  }

  const url = translatePriceKeyInRefinements(req)

  expect(url.query).toBe(null)
  expect(url.search).toBe('')
  expect(url.path).toBe('/')
  expect(url.href).toBe('/')
})
