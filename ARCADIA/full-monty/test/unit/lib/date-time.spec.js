import { getEnglishDate } from '../../../src/shared/components/common/StoreLocator/date-time'

test('getEnglishDate returns date in English format for a date in the future', () => {
  const D = global.Date
  global.Date = jest.fn(
    (...args) => (args.length ? new D(...args) : new D(2000, 0, 1))
  )

  expect(getEnglishDate('2016-10-23')).toBe('23rd October')
  expect(getEnglishDate('2016-10-11')).toBe('11th October')
  expect(getEnglishDate('2016-10-12')).toBe('12th October')
  expect(getEnglishDate('2016-10-13')).toBe('13th October')
  expect(getEnglishDate('2016-10-01')).toBe('1st October')
  expect(getEnglishDate('2016-01-02')).toBe('2nd January')
  expect(getEnglishDate('2016-05-31')).toBe('31st May')

  global.Date = D
})

test('getEnglishDate returns date in English format for today', () => {
  const D = global.Date
  global.Date = jest.fn(
    (...args) => (args.length ? new D(...args) : new D(2000, 0, 1))
  )

  expect(getEnglishDate('2000-01-01')).toBe('today, 1st January')

  global.Date = D
})

test('getEnglishDate returns date in English format for tomorrow', () => {
  const D = global.Date
  global.Date = jest.fn(
    (...args) => (args.length ? new D(...args) : new D(1999, 11, 31))
  )

  expect(getEnglishDate('2000-01-01')).toBe('tomorrow, 1st January')

  global.Date = D
})
