import { respInspect } from '../../../src/server/handlers/lib/scrapi-unwrap'

test('[INSPECT scrAPI RESPONSE] rejects if passed 4xx for resp.body.statusCode', (done) => {
  respInspect({})({
    body: { statusCode: 402, success: false, originalMessage: '' },
  })
    .then(done.fail)
    .catch(({ data, isBoom, isServer, output, message }) => {
      expect(data).toBe(null)
      expect(isBoom).toBe(true)
      expect(isServer).toBe(false)
      expect(typeof output).toBe('object')
      expect(output.statusCode).toBe(402)
      expect(typeof output.payload).toBe('object')
      expect(typeof output.headers).toBe('object')
      expect(message).toBe('Payment Required')
      done()
    })
})

test('[INSPECT scrAPI RESPONSE] rejects if passed 5xx for resp.body.statusCode', (done) => {
  respInspect({})({
    body: { statusCode: 500, message: 'failed', originalMessage: '' },
  })
    .then(done.fail)
    .catch(({ data, isBoom, isServer, output, message }) => {
      expect(data).toBe(null)
      expect(isBoom).toBe(true)
      expect(isServer).toBe(true)
      expect(typeof output).toBe('object')
      expect(output.statusCode).toBe(500)
      expect(typeof output.payload).toBe('object')
      expect(typeof output.headers).toBe('object')
      expect(message).toBe('failed')
      done()
    })
})

test('[INSPECT scrAPI RESPONSE] resolves if passed 2xx for resp.body.statusCode', (done) => {
  respInspect({})({ body: { success: true }, originalMessage: '' })
    .then(({ body }) => {
      expect(typeof body).toBe('object')
      expect(body.success).toBe(true)
      done()
    })
    .catch(done.fail)
})
