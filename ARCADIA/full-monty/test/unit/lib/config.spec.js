import { getBrandConfig } from '../../../src/server/config'

test('function should return config for m.topshop.com', () => {
  const hostname = 'm.topshop.com'
  expect(getBrandConfig(hostname).brandName).toBe('topshop')
  expect(getBrandConfig(hostname).storeCode).toBe('tsuk')
  expect(getBrandConfig(hostname).region).toBe('uk')
})

test('function should return config for m.fr.topshop.com', () => {
  const hostname = 'm.fr.topshop.com'
  expect(getBrandConfig(hostname).brandName).toBe('topshop')
  expect(getBrandConfig(hostname).storeCode).toBe('tsfr')
  expect(getBrandConfig(hostname).region).toBe('fr')
})
