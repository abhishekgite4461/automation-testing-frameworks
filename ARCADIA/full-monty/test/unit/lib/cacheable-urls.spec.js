import {
  getCacheExpiration,
  getEtagMethod,
  getCacheSettings,
} from '../../../src/shared/lib/cacheable-urls'

test('Arcadia API URLs return default TTL', () => {
  const assert = (path, timeout) => {
    expect(getCacheExpiration('scrAPI', path)).toBe(timeout)
  }
  assert('/api/site-options', 600)
  assert('/tsuk/navigation', 0)
  assert('/tsuk/navigation/page', 600)
  assert('/bruk/navigation/page', 600)
  assert('/tsuk/product/product1', 600)
  assert('/tsuk/products/product1', 600)
  assert('/tsuk/product?q=jacket', 600)
  assert('/tsfr/cms/content', 600)
  assert('/some-random', 0)
})

const assert = (path, timeout) => {
  test(`Monty API and static asset URLs return default TTL - ${path}`, () => {
    expect(getCacheExpiration('hapi', path)).toBe(timeout)
  })
}
assert('/', 600)
assert(
  '/?utm_medium=email&utm_source=MS&utm_campaign=UK_201852_A_Adhoc&utm_content=SS&utm_term=A2MO&subscriptionID=44079481&dtm_em=a38a6f1fc959eced3285d8e095ef0a1d',
  600
)
assert('/api/cms', 0)
assert('/api/cms/content', 600)
assert('/api/navigation', 0)
assert('/api/navigation/page', 600)
assert('/api/site-options', 600)
assert('/api/products/item', 600)
assert('/api/product/item', 600)
assert('/api/products?q=jacket', 600)
assert('/cmscontent', 600)
assert(
  '/cmscontent?storeCode=tsuk&brandName=topshop&cmsPageName=mobilePLPESpotPos1',
  600
)
assert('/api/stores-countries?brand=x', 600)
assert('/assets/tsuk/images/pic1', 86400)
assert('/assets/bruk/images/pic1', 86400)
assert('/assets/images/pic1', 0)
assert('/assets/tsuk/fonts/font1', 31536000)
assert('/assets/common/vendor.js', 31536000)
assert('/assets/common/bundle.js', 31536000)
assert('/assets/content/cms/script.65cc53237cb76554654f561c4e8b7969.js', 604800)
assert(
  '/assets/content/cms/styles-topshop.0e5c1d8577697eebbb250523764cd986.css',
  31536000
)
assert('/assets/topshop/styles.cfb0877c382c46b15f1923c62a042ece.css', 31536000)
assert('/assets/common/bundle.ed24c41a710ba0fcf48cccdeac8d4c5d.js', 31536000)
assert('/assets/common/vendor.31f48fa489353d06d4ded922817fd1c3.js', 31536000)
assert('/en/tsuk/category/shoes-430/loafers-5928805', 600)
assert(
  '/en/tsuk/category/the-anti-cliche-christmas-6030568/home?TS=1477557146856&amp;intcmpid=MOBILE_WK8_THURS_CHRISTMAS',
  600
)
assert('/en/tsuk/product/lucy-loafer-print-5809755', 600)
assert('/some-random', 0)

test('Monty static asset URLs return correct eTag method', () => {
  const assert = (path, method) => {
    expect(getEtagMethod('hapi', path)).toBe(method)
  }
  assert('/assets/tsuk/images/image.jpq', 'hash')
  assert('/assets/bruk/fonts/font.ttf', 'hash')
  assert('/assets/common/asset.ext', false)
  assert('/unmatched-path', false)
})

test('Monty API and static asset URLs return correct TTL IF values are set', () => {
  const homepageOriginalTTL = getCacheSettings('hapi', '/').expiration
  getCacheSettings('hapi', '/').expiration = 50
  const assert = (path, timeout) => {
    expect(getCacheExpiration('hapi', path)).toBe(timeout)
  }
  assert('/', 50)
  getCacheSettings('hapi', '/').expiration = homepageOriginalTTL
})
