import * as storage from '../../../src/client/lib/storage'
import 'mock-local-storage'

test('isStorageSupported should be true', () => {
  expect(storage.isStorageSupported()).toBeTruthy()
  expect(typeof sessionStorage.setItem).toBe('function')
})

test('set- & getPartialOrderSummary sets and returns partial order summary', () => {
  const data = { works: true }
  expect(storage.getPartialOrderSummary()).toBe(undefined)
  storage.setPartialOrderSummary(data)
  expect(storage.getPartialOrderSummary()).toEqual(data)
})

test('set- & getSelectedStore sets and returns selected store', () => {
  const data = { works: true }
  expect(typeof storage.getSelectedStore()).toBe('object')
  storage.setSelectedStore(data)
  expect(storage.getSelectedStore()).toEqual(data)
})

test('set- & getCacheData sets/gets prefixed localstorage', () => {
  const data = { works: true }
  const name = 'auth'
  storage.setCacheData(name, data)
  expect(localStorage.getItem(`cached_${name}`)).toBeTruthy()
  expect(storage.getCacheData(name)).toEqual(data)
})

test('clearCacheData clears only cached local storage', () => {
  localStorage.setItem(`data`, 'test')
  localStorage.setItem(`cached_auth`, 'test')
  localStorage.setItem(`cached_account`, 'test')
  expect(localStorage.length).toBe(3)
  storage.clearCacheData()
  expect(localStorage.length).toBe(1)
})

test('loadRecentlyViewedState loads the recently viewed items', () => {
  const mockedState = [{ test: {} }]
  localStorage.setItem('recentlyViewed', JSON.stringify(mockedState))
  expect(storage.loadRecentlyViewedState()).toEqual(mockedState)
  localStorage.clear()
})

test('loadRecentlyViewedState returns empty array when there is no recently viewed item', () => {
  expect(storage.loadRecentlyViewedState()).toEqual([])
})

test('saveRecentlyViewedState saves the recently viewed items', () => {
  const mockedState = [{ test: {} }]
  storage.saveRecentlyViewedState(mockedState)
  expect(localStorage.getItem('recentlyViewed')).toBe(
    JSON.stringify(mockedState)
  )
  localStorage.clear()
})
