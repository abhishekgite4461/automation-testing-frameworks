import {
  getAnalyticsBrandCode,
  getDate,
} from '../../../src/shared/lib/analytics/analytics'

describe('src/shared/lib/analytics/analytics', () => {
  beforeAll(() => {
    global.window.__INITIAL_STATE__ = {
      config: { isDaylightSavingTime: false },
    }
  })

  afterAll('after', () => {
    delete global.window.__INITIAL_STATE__
  })

  it('getAnalyticsBrandCode should take a brand abreviation capitaize it and if its burton "BR" change it to "BM"', () => {
    expect(getAnalyticsBrandCode('ts')).toBe('TS')
    expect(getAnalyticsBrandCode('br')).toBe('BM')
  })

  it('getDate returns the current date', () => {
    const D = global.Date
    global.Date = jest.fn(() => new D(1469443126665))

    expect('10:38 | Mon | 25-07-2016').toBe(getDate())

    global.Date = D
  })

  it('getDate returns a date in the specific format "10:17 | Fri | 22-07-2016"', () => {
    const reg = /([0-9]{2}[:][0-9]{2})([ ][|][ ])([A-z]{3})([ ][|][ ])([0-9]{2}[-][0-9]{2}[-][0-9]{4})/
    expect(getDate().match(reg)).toBeTruthy()
  })
})
