import paymentsHelper from '../../../src/server/lib/payments-helper'

const alipayMock = {
  url: {
    search:
      'paymentMethod=ALIPY/.../webapp/wcs/stores/servlet/PunchoutPaymentCallBack?orderId=1121709&',
  },
  query: {
    catalogId: '33057',
    policyId: '25091',
    storeId: '12556',
    langId: '-1',
    notifyShopper: '0',
    notifyOrderSubmitted: '0',
    tran_id: '359562',
    userApproved: '1',
    dummy: '',
    orderKey: 'ARCADIA^TSHOPECOMGBP^1121709_TS_UK_CN_1485189314970',
    paymentStatus: 'AUTHORISED',
    paymentAmount: '5000',
    paymentCurrency: 'GBP',
    mac: '416bcb9afa9b6d5466b3f189c3faf2bc',
    orderId: '1121709',
    authProvider: 'ALIPY',
  },
}

test('paymentsHelper should return a orderPayload object when the request objects payload is correct for a visa payment', () => {
  const mockRequest = {
    url: 'test',
    payload: {
      MD: 'test',
      PaRes: 'test',
    },
    query: {},
  }
  paymentsHelper(mockRequest, (data) => {
    expect(data).toEqual({ authProvider: 'VBV', md: 'test', paRes: 'test' })
  })
})

test('paymentsHelper should return a orderPayload object when the request objects query is correct for a visa payment', () => {
  const mockRequest = {
    url: {},
    payload: {},
    query: {
      MD: 'test',
      PaRes: 'test',
    },
  }
  paymentsHelper(mockRequest, (data) => {
    expect(data).toEqual({ authProvider: 'VBV', md: 'test', paRes: 'test' })
  })
})

test('paymentsHelper should return a orderPayload object when the request object is correct for a visa payment', () => {
  const mockRequest = {
    url: {},
    payload: {},
    query: {
      klarnaOrderId: '12345',
    },
  }
  paymentsHelper(mockRequest, (data) => {
    expect(data).toEqual({ authProvider: 'KLRNA', order: '12345' })
  })
})

test('paymentsHelper should return a orderPayload object when the request object is correct for a paypal payment', () => {
  const mockRequest = {
    url: {
      search: 'paymentMethod=PYPAL/orderId=asdf&',
    },
    payload: {},
    query: {
      policyId: 'asdf',
      payerId: 'laksdfj',
      userApproved: true,
      orderId: 'asdf',
      token: 'asdf',
      tranId: 'asdf',
      authProvider: 'PYPAL',
    },
  }
  paymentsHelper(mockRequest, (data) => {
    expect(data).toEqual({
      authProvider: 'PYPAL',
      orderId: 'asdf',
      payerId: undefined,
      policyId: 'asdf',
      token: 'asdf',
      tranId: undefined,
      userApproved: true,
    })
  })
})

test('Payments-Helpers return  complete object + orderID', () => {
  paymentsHelper(alipayMock, (data) => {
    expect(data.orderId).toEqual(alipayMock.query.orderId)
    expect(data.tran_id).toEqual('359562')
  })
})
