import {preserveSessionCookies} from './helpers'

function addBeforeStepsToFunction(suiteFunction) {
    return function() {
        before(function() {
            cy.task('mock-server-cleanup')
        })
        beforeEach(function() {
            preserveSessionCookies()
        })
        suiteFunction()
    }
}

function suite(suiteName, suiteFunction) {
    return describe(suiteName, addBeforeStepsToFunction(suiteFunction))
}
suite.skip = function(suiteName, suiteFunction) {
    return describe.skip(suiteName, addBeforeStepsToFunction(suiteFunction))
}

suite.only = function(suiteName, suiteFunction) {
    return describe.only(suiteName, addBeforeStepsToFunction(suiteFunction))
}

export default suite
