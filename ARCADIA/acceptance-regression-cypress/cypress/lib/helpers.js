import {
    getPDPRoutes,
    getPlpRoutes,
    getCommonRoutes,
} from '../../mock-server/routes'
import routes from '../constants/routes'
import suiteOverride from './suite'
import payments from '../fixtures/checkout/payments.json'

export const isMobileLayout = () => Cypress.env('BREAKPOINT') === 'mobile'
export const isDesktopLayout = () => Cypress.env('BREAKPOINT') !== 'mobile'

export const removeDomainFromFullLink = (fullLink) => {
    const matches = /http(s)?:\/\/(?:.*?)(\/.+)$/.exec(fullLink)
    return matches[2]
}

export const setViewport = () => {
    if (isMobileLayout()) cy.viewport('iphone-6')
}

export const setFeature = (feature) => {
    cy.window().then((window) => {
        const isEnabled = window.__qubitStore.getState().features.status[
            feature
        ]
        if (isEnabled) return cy.log(`${feature} is already turned on`)
        window.__qubitStore.dispatch({type: 'TOGGLE_FEATURE', feature})
        cy.log(`${feature} has been turned on`)
    })
}

export const preserveSessionCookies = () => {
    Cypress.Cookies.preserveOnce(
        'jsessionid',
        'authenticated',
        'featuresOverride'
    )
}

export const suite = suiteOverride

/*  TODO: These mock setups are getting more and more. Move them out into a
    seperate `mockHelpers.js` file to keep this file single-purposed.       */

export const setupClientMocksReturningDddpSubscriberJourneyToCheckoutWithDdpProd = () => {
    cy.server()
    cy.route(
        'POST',
        routes.addItemToBag,
        'fixture:pdp/ddpProduct/ddpProductAddToBag'
    )
    cy.route('GET', routes.paymentsAll, 'fixture:checkout/payments')
    cy.route(
        'GET',
        routes.checkoutOrderSummary,
        'fixture:checkout/order_summary---returningUserDdpProd'
    )
    cy.route(
        'GET',
        routes.account,
        'fixture:checkout/account--returningUserFullCheckoutProfile'
    )

    cy.route('POST', routes.login, 'fixture:login/fullCheckoutProfile')
    cy.route(
        'POST',
        routes.paymentsDeliveryUkBillingUk,
        'fixture:login/payments--ukBillingUkDelivery'
    )
}
export const setupClientMocksNewUserJourneyToCheckoutWithDdpProd = () => {
    cy.server()
    cy.route(
        'POST',
        routes.addItemToBag,
        'fixture:pdp/ddpProduct/ddpProductAddToBag'
    )
    cy.route('GET', routes.paymentsAll, 'fixture:checkout/payments')
    cy.route(
        'GET',
        routes.checkoutOrderSummary,
        'fixture:checkout/order_summary---newUserDdpProd'
    )
    cy.route('POST', routes.registerNewUser, 'fixture:checkout/register')
}

export const setupClientMocksReturningUserJourneyToCheckoutWithSingleSizeProd = () => {
    cy.server()
    cy.route(
        'POST',
        routes.addItemToBag,
        'fixture:pdp/singleSizeProduct/addToBag'
    )
    cy.route(
        'GET',
        routes.checkoutOrderSummary,
        'fixture:checkout/order_summary---returningUserSingleSizeProd'
    )
    cy.route(
        'GET',
        routes.account,
        'fixture:checkout/account--returningUserFullCheckoutProfile'
    )

    cy.route('POST', routes.login, 'fixture:login/fullCheckoutProfile')
    cy.route('GET', routes.paymentsAll, 'fixture:checkout/payments')
    cy.route(
        'POST',
        routes.paymentsDeliveryUkBillingUk,
        'fixture:login/payments--ukBillingUkDelivery'
    )
}

export const setupMocksForDeleteAdditionalDeliveryDetails = () => {
    cy.route(
        'DELETE',
        routes.checkoutOrderSummaryDeliveryAddress,
        'fixture:checkout/order_summary---removeDeliveryAddress'
    )
}

export const setupClientMocksNewUserJourneyToCheckoutWithSingleSizeProd = () => {
    cy.server()
    cy.route(
        'POST',
        routes.addItemToBag,
        'fixture:pdp/singleSizeProduct/addToBag'
    )
    cy.route(
        'GET',
        routes.checkoutOrderSummary,
        'fixture:checkout/order_summary---newUserSingleSizeProd'
    )
    cy.route('GET', routes.account, 'fixture:checkout/account--newUser')
    cy.route('POST', routes.registerNewUser, 'fixture:checkout/register')
    cy.route('GET', routes.paymentsAll, 'fixture:checkout/payments')
    cy.route(
        'POST',
        routes.paymentsDeliveryUkBillingUk,
        'fixture:login/payments--ukBillingUkDelivery'
    )
}

export const setUpMocksForStoreLocator = () => {
    cy.server()
    cy.route('' + 'GET', routes.storeLocator, 'fixture:general/storeLocator')
}

export const setupMocksForInitialRenderOnPdp = (productJsonResponse) => {
    const path = removeDomainFromFullLink(productJsonResponse.sourceUrl)
    const routes = getPDPRoutes({path, response: productJsonResponse})
    cy.task('mock-server-setup', routes).then(() => {
        cy.server()
        routes.map(cy.route)
    })
    return path
}

export const setupMocksForInitialRenderOnGenericPage = () => {
    const routes = getCommonRoutes()
    cy.task('mock-server-setup', routes).then(() => {
        cy.server()
        routes.map(cy.route)
    })
}

export const setupMocksForInitialRenderOnPlp = (searchTerm) => {
    const path = `/search/?q=${encodeURIComponent(searchTerm)}`
    const {routes, products} = getPlpRoutes(searchTerm)
    cy.task('mock-server-setup', routes).then(() => {
        cy.server()
        routes.map(cy.route)
    })
    return {path, products}
}

export const setupMocksForCheckout = (
    requiredOrderSummary,
    requiredCheckoutProfile
) => {
    cy.setCookie('authenticated', 'yes')
    const routes = getCommonRoutes().concat([
        {
            method: 'GET',
            url: '/api/checkout/order_summary',
            response: requiredOrderSummary,
        },
        {
            method: 'GET',
            url: '/api/account',
            response: requiredCheckoutProfile,
        },
        {
            method: 'GET',
            url: '/api/payments?value=all',
            response: payments,
        },
    ])

    cy.task('mock-server-setup', routes).then(() => {
        cy.server()
        routes.map(cy.route)
    })
}

export const setUpMocksForPeeriusRecommends = (recommends) => {
    cy.window().then((window) => {
        window.PeeriusCallbacks.smartRecs(recommends)
    })
}

export const setUpMocksForRecentlyViewed = (recentlyViewed) => {
    const setupRecentlyViewedItems = (window, prods) =>
        window.localStorage.setItem('recentlyViewed', JSON.stringify(prods))

    cy.window().then((window) => {
        setupRecentlyViewedItems(window, recentlyViewed)
    })
}

export const setupMocksForMyAccount = (requiredAccountProfile) => {
    cy.setCookie('authenticated', 'yes')
    const routes = getCommonRoutes().concat([
        {
            method: 'GET',
            url: '/api/account',
            response: requiredAccountProfile,
        },
        {
            method: 'GET',
            url: '/api/payments?value=all',
            response: payments,
        },
    ])

    cy.task('mock-server-setup', routes).then(() => {
        cy.server()
        routes.map(cy.route)
    })
}
