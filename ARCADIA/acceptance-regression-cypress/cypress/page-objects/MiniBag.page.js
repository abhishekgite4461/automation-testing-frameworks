import {isMobileLayout} from './../lib/helpers'
import CheckoutLogin from './CheckoutLogin.page'

const checkoutLogin = new CheckoutLogin()

export default class MiniBag {
    get checkoutButton() {
        return '.MiniBag-continueButton'
    }
    get miniBagCloseButton() {
        return '.MiniBag-closeButton'
    }
    get disappearedLoaderOverlay() {
        return '.LoaderOverlay[aria-busy="false"]'
    }
    get editProductButton() {
        return '.OrderProducts-editText'
    }
    get productContainer() {
        return '.OrderProducts-product'
    }
    get productSize() {
        return '.OrderProducts-product .OrderProducts-productSize'
    }
    get quantitySelectionDropdown() {
        return '.FormComponent-bagItemQuantity .Select-container .Select-select'
    }
    get saveChangesButton() {
        return '.OrderProducts-saveButton'
    }
    get sizeSelectionDropdown() {
        return '.FormComponent-bagItemSize .Select-container .Select-select'
    }
    get removeItemButton() {
        return 'span.OrderProducts-deleteLabel'
    }
    get removeItemModalSubmitButton() {
        return '.OrderProducts-modal > .Button'
    }

    get closeBagConfirmationModal() {
        return isMobileLayout()
            ? '.MiniBag-emptyBag > .Button'
            : '.ContentOverlay'
    }

    /**
     * USER ACTIONS **************************************************************
     */
    expectNumberOfProductsToBe(number) {
        cy.get(this.productContainer).should('have.length', number)
    }

    invokeCurrentProductSizeOfProductAs(number, variableToStore) {
        cy.get(this.productSize)
            .eq(number - 1)
            .invoke('text')
            .as(variableToStore)
    }

    expectSizeOfProductNumberToNotEqual(number, previousValue) {
        cy.get(this.productSize)
            .eq(number)
            .invoke('text')
            .should('not.be.undefined')
            .and('not.equal', previousValue)
    }

    expectProductAToHaveDifferentSizeThanProductB(prodA, prodB) {
        cy.get(this.productSize)
            .eq(prodA - 1)
            .invoke('text')
            .as('prodAsize')
        cy.get(this.productSize)
            .eq(prodB - 1)
            .invoke('text')
            .should('not.be.undefined')
            .and('not.equal', this.prodAsize)
    }

    editAndChangeSizeForProductNumberTo(number, size) {
        cy.get(this.editProductButton)
            .eq(number - 1)
            .click()
        return cy
            .get(this.sizeSelectionDropdown)
            .eq(number - 1)
            .select(size)
    }

    editAndChangeQuantityForProductNumberTo(number, quantity) {
        cy.get(this.editProductButton)
            .eq(number - 1)
            .click()
        cy.get(this.quantitySelectionDropdown)
            .eq(number - 1)
            .select(quantity)
    }

    removeProductFromBasketWithIndex(index) {
        return cy
            .get(this.removeItemButton)
            .eq(index)
            .click()
            .get(this.removeItemModalSubmitButton)
            .click()
    }

    saveChanges() {
        return cy.get(this.saveChangesButton).click()
    }

    moveToCheckout() {
        return cy
            .get(this.checkoutButton)
            .eq(0)
            .click()
            .get(checkoutLogin.checkoutContainer)
    }

    continueShopping() {
        return cy.get(this.closeBagConfirmationModal).click()
    }

    miniBagClose() {
        return cy.get(this.miniBagCloseButton).click()
    }
}
