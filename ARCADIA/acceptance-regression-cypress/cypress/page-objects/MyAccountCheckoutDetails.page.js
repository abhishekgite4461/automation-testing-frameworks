export default class MyAccountCheckoutDetails {
    get ddpSubscriptionContainer() {
        return '.DDPSubscription'
    }

    get ddpSubscriptionMessage() {
        return '.DDPSubscription-message'
    }

    get ddpSubscriptionCancel() {
        return 'a.DDPSubscription-cancel'
    }

    get ddpIcon() {
        return '.DDPSubscription-icon'
    }

    get ddpRenewLink() {
        return '.DDPSubscription-renewButton'
    }

    /**
     * USER ACTIONS **************************************************************
     */

    clickDdpRenewalLink() {
        return cy.get(this.ddpRenewLink).click({timeout: 10000})
    }
}
