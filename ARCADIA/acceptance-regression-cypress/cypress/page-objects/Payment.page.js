import {giftCard, visaCardPaymentDetails} from './../constants/paymentDetails'
import CheckoutHeader from './CheckoutHeader.page'

export default class Payment extends CheckoutHeader {
    get orderAndPayButton() {
        return 'button.Button.PaymentContainer-paynow'
    }
    get termsAndConditionsCheckboxSpan() {
        return '.FormComponent-isAcceptedTermsAndConditions .Checkbox-checkboxContainer'
    }
    get termsAndConditionsCheckboxInput() {
        return '.FormComponent-isAcceptedTermsAndConditions input[name="isAcceptedTermsAndConditions"]'
    }
    get cardNumberInput() {
        return 'input[name="cardNumber"]'
    }
    get cvvInput() {
        return '.CardPaymentMethod-cvvFieldContainer input[name="cvv"]'
    }
    get cvvForSavedCard() {
        return '.Input-field.Input-field-cvv'
    }

    get expiryMonthDropdown() {
        return 'select[name="expiryMonth"]'
    }
    get expiryYearDropdown() {
        return 'select[name="expiryYear"]'
    }

    get promoCodeInput() {
        return '#promotionCode-text'
    }
    get promoCodeApplyButton() {
        return '.PromotionCode-form > .Button'
    }
    get promoCodeHeader() {
        return '.PromotionCode-header'
    }
    get promoCodeTitle() {
        return '.PromotionCode-codeTitle'
    }
    get promoCodeErrorMessage() {
        return '.Message.is-shown.is-error.PromotionCode-message'
    }
    get giftCardHeader() {
        return '.GiftCards'
    }
    get giftCardInput() {
        return '.Input-field.Input-field-giftCardNumber'
    }
    get giftCardPinInput() {
        return '.Input-field.Input-field-pin.sessioncamexclude'
    }
    get applyCardButton() {
        return '.GiftCards .Button'
    }

    /**
     * USER ACTIONS **************************************************************
     */

    enterBasicCreditCardDetails() {
        return cy
            .get(this.cardNumberInput)
            .type(visaCardPaymentDetails.cardNumber)
            .get(this.expiryMonthDropdown)
            .select(visaCardPaymentDetails.expiryMonth)
            .get(this.expiryYearDropdown)
            .select(visaCardPaymentDetails.expiryYear)
            .get(this.cvvInput)
            .type(visaCardPaymentDetails.cvvNumber)
    }

    enterCVVNumber() {
        return cy
            .get(this.cvvForSavedCard)
            .type(visaCardPaymentDetails.cvvNumber)
    }

    tickTermsAndConditionCheckBox() {
        return cy.get(this.termsAndConditionsCheckboxSpan).click()
    }
    clickOrderButton() {
        return cy.get(this.orderAndPayButton).click()
    }

    enterPromoCodeAndApply(code) {
        return cy
            .get(this.promoCodeHeader)
            .click()
            .get(this.promoCodeInput)
            .type(code)
            .get(this.promoCodeApplyButton)
            .click()
    }
    verifyPromoCodeApplied(code) {
        return cy.get(this.promoCodeTitle).should('contain', code)
    }
    verifyPromoCodeErrorDisplayed() {
        return cy
            .get(this.promoCodeErrorMessage)
            .should(
                'contain',
                'The code you have entered has not been recognised.'
            )
    }
    enterGiftCardNumberAndPin() {
        return cy
            .get(this.giftCardHeader)
            .click()
            .get(this.giftCardInput)
            .type(giftCard.giftCardNumber)
            .get(this.giftCardPinInput)
            .type(giftCard.pin)
    }
    clickApplyCard() {
        return cy.get(this.applyCardButton).click()
    }

    resetGiftCardValues() {
        return cy
            .get(this.giftCardInput)
            .clear()
            .get(this.giftCardPinInput)
            .clear()
    }
}
