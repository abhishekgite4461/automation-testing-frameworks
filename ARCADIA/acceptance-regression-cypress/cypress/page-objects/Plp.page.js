import ProductDetailPage from './Pdp.page'
const Pdp = new ProductDetailPage()

export default class Plp {
    get quickViewButton() {
        return '.ProductQuickViewButton'
    }

    get productImage() {
        return '.ProductImages'
    }

    get firstImageAttribute() {
        return 'div.ProductImages:nth-child(1) > img'
    }

    /**
     * USER ACTIONS **************************************************************
     */
    selectQuickViewForItem(num) {
        cy.get(this.quickViewButton)
            .eq(num)
            .click()
            .get(Pdp.addToBagButton)
    }

    hoverOnFirstProduct() {
        cy.get(this.productImage)
            .first()
            .trigger('mouseover')
    }
}
