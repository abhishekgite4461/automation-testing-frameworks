import {isMobileLayout} from './../lib/helpers'

export default class StoreLocator {
    get sizeSelect() {
        return isMobileLayout()
            ? '.FindInStore .ProductSizes-item'
            : '.Select-container .Select-select'
    }

    get storeUserLocatorInput() {
        return isMobileLayout()
            ? '.StoreDeliveryV2 .UserLocatorInput-inputField'
            : '.CollectFromStore .UserLocatorInput-inputField'
    }

    get storeUserLocatorInputPredictions() {
        return isMobileLayout()
            ? '.StoreDeliveryV2 .UserLocatorInput-predictionsList > :nth-child(1) > button'
            : '.CollectFromStore .UserLocatorInput-predictionsList > :nth-child(1) > button'
    }

    get storeUserSelectedLocationGoButton() {
        return isMobileLayout()
            ? '.StoreDeliveryV2 .UserLocatorInput-inputContainer > .Button'
            : '.CollectFromStore-search .UserLocatorInput-inputContainer > .Button'
    }

    get selectStoreInDelivery() {
        return isMobileLayout()
            ? '.Store-TS0032 > .Accordion > > > > .Store-selectButtonContainer > .Button'
            : '.Store-TS0032 .Store-selectButtonContainer > .Button'
    }

    /**
     * USER ACTIONS **************************************************************
     */

    sizeSelector(size) {
        if (isMobileLayout()) {
            return cy
                .get(this.sizeSelect)
                .eq(0)
                .click()
        } else
            return cy
                .get(this.sizeSelect)
                .eq(1)
                .select(size)
    }

    storeLocatorSearchInDelivery(searchTerm) {
        cy.get(this.storeUserLocatorInput).type(searchTerm)
    }

    selectFirstStoreFromResultsListInDelivery() {
        cy.get(this.storeUserLocatorInputPredictions).click()
    }

    selectedLocationGoButtonInDelivery() {
        cy.get(this.storeUserSelectedLocationGoButton).click()
    }

    storeLocatorSearchInPdp(searchTerm) {
        cy.get('.FindInStore .UserLocatorInput-inputField').type(searchTerm)
    }

    selectFirstStoreFromResultsListInPdp() {
        cy.get(
            '.FindInStore .UserLocatorInput-predictionsList > :nth-child(1) > button'
        ).click()
    }

    selectedLocationGoButtonInPdp() {
        cy.get(
            '.FindInStore .UserLocatorInput-inputContainer > .Button'
        ).click()
    }

    selectStoreExpressDeliveryType() {
        cy.get(
            ':nth-child(1) > .FormComponent-deliveryMethod > .RadioButton-content'
        ).click()
    }

    selectCollectFromStoreDeliveryMethod() {
        cy.get('.DeliveryOption--store .RadioButton-content').click()
    }

    expandStoreDetailsFromList(index) {
        cy.get('.Store-name')
            .eq(index)
            .click({force: true})
    }

    selectStoreButton() {
        cy.get(this.selectStoreInDelivery).click({force: true})
    }

    closeStoreProductLocatorModal() {
        cy.get('.Modal > .Modal-closeIcon > span').click()
    }
}
