import Pdp from './Pdp.page'

export default class flexibleBundle extends Pdp {
    get miniproduct() {
        return '.MiniProduct'
    }
    get miniproductErrorMessage() {
        return '.BundlesSizes-error'
    }

    /**
     * USER ACTIONS **************************************************************
     */
    assertNumberOfProducts(numberExpected) {
        cy.get(this.miniproduct).should('have.length', numberExpected)
    }

    assertNumberOfAddToBagButtons(numberExpected) {
        cy.get(this.addToBagButton).should('have.length', numberExpected)
    }

    selectAvailableSizeForBundleItem(number) {
        cy.get(this.sizeSelectionDropdown)
            .eq(number - 1)
            .select('Size 12: Low stock')
    }

    expectErrorToAlsoSelectProduct(number) {
        cy.get(this.miniproduct)
            .eq(number - 1)
            .get(this.miniproductErrorMessage)
    }
}
