import {isMobileLayout} from './../lib/helpers'

export default class Pdp {
    get addToBagButton() {
        return '.AddToBag .Button'
    }
    get sizeSelectionDropdown() {
        return '.Select-container .Select-select'
    }
    get mobileViewBagButtonOnConfirmationModal() {
        return '.AddToBagConfirm-viewBag'
    }
    get miniproductErrorMessage() {
        return '.BundlesSizes-error'
    }
    get quantityDropdown() {
        return '#productQuantity'
    }
    get tileSizes() {
        return '.ProductSizes-list'
    }
    get errorMessage() {
        return '.is-error'
    }
    get productSize() {
        return '.ProductSizes-item'
    }
    get carouselNext() {
        return '.Carousel-arrow--right'
    }

    get findStoreButton() {
        return isMobileLayout()
            ? '.FindInStoreButton'
            : '.FindInStoreButton-desktopWrapper'
    }

    /**
     * USER ACTIONS **************************************************************
     */
    viewBagFromConfirmationModal() {
        if (isMobileLayout()) {
            return cy.get(this.mobileViewBagButtonOnConfirmationModal).click()
        }
    }

    selectFirstSizeFromTiles() {
        return cy
            .get(this.productSize)
            .first()
            .click()
    }

    addToBag() {
        return cy.get(this.addToBagButton).click()
    }

    changeProductQuantityTo(number) {
        return cy.get(this.quantityDropdown).select(number.toString())
    }

    clickNextOnImageCarousel() {
        return cy.get(this.carouselNext).click()
    }

    verifySizeOptionsLessThan(number) {
        return cy
            .get(this.tileSizes)
            .children()
            .should('have.length.lessThan', number)
    }

    verifySizeOptionsMoreThan(number) {
        return cy
            .get(this.sizeSelectionDropdown)
            .children()
            .should('have.length.greaterThan', number)
    }

    findInStore() {
        return cy.get(this.findStoreButton).click()
    }
}
