const userDetails = {
    email: 'testymctest@arcadiagroup.co.uk',
    password: 'monty1',
}

export default class Login {
    get registerEmailInput() {
        return '.Register .Input-field-email'
    }
    get registerPasswordInput() {
        return '.Register .Input-field-password'
    }
    get registerPasswordConfirmInput() {
        return '.Register .Input-field-passwordConfirm'
    }
    get registerButton() {
        return '.Register-saveChanges'
    }
    get loginEmailInput() {
        return '.Login .Input-field-email'
    }
    get loginPasswordInput() {
        return '.Login .Input-field-password'
    }
    get loginButton() {
        return '.Login-submitButton'
    }

    /**
     * USER ACTIONS *************************************************************
     */
    registerAsNewUser() {
        return cy
            .get(this.registerEmailInput)
            .type(userDetails.email)
            .get(this.registerPasswordInput)
            .type(userDetails.password)
            .get(this.registerPasswordConfirmInput)
            .type(userDetails.password)
            .get(this.registerButton)
            .click()
    }

    loginAsExistingUser() {
        return cy
            .get(this.loginEmailInput)
            .type(userDetails.email)
            .get(this.loginPasswordInput)
            .type(userDetails.password)
            .get(this.loginButton)
            .click()
    }
}
