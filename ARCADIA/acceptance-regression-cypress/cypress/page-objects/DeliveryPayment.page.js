import {visaCardPaymentDetails} from './../constants/paymentDetails'
import CheckoutHeader from './CheckoutHeader.page'

export default class DeliveryPayment extends CheckoutHeader {
    get cvvInput() {
        return 'input[name="cvv"]'
    }
    get termsAndConditionsCheckboxSpan() {
        return 'input[name="isAcceptedTermsAndConditions"] + span'
    }
    get orderAndPayButton() {
        return 'button.Button.PaymentContainer-paynow'
    }
    get paymentErrorMessage() {
        return '.Message.is-shown.is-error'
    }
    get useDDPNoticiationMessage() {
        return '.Notification'
    }
    get ddpHelpfulMessage() {
        return '.AddressBook-ddpNotice'
    }
    get ddpInlineNotificationAboveSavedAddress() {
        return '.DeliveryPaymentContainer-ddpAppliedToOrderMsg'
    }
    /**
     * USER ACTIONS ************************************************************
     */
    payAsReturningUser() {
        return cy
            .get(this.cvvInput)
            .type(visaCardPaymentDetails.cvvNumber)
            .get(this.termsAndConditionsCheckboxSpan)
            .click()
            .get(this.orderAndPayButton)
            .click()
    }
}
