import Login from './Login.page'

export default class CheckoutLogin extends Login {
    get checkoutContainer() {
        return '.CheckoutContainer'
    }
}
