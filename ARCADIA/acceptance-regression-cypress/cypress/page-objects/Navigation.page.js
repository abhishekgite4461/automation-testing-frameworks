import {isMobileLayout} from './../lib/helpers'

export default class Navigation {
    get searchIcon() {
        return isMobileLayout() ? '.Header-searchButton' : '.SearchBar-icon'
    }

    get searchTermInput() {
        return '.SearchBar-queryInput'
    }

    get performSearchButton() {
        return '.SearchBar-icon'
    }

    get noResultsSearchButton() {
        return '.NoSearchResults > .Button'
    }

    get pdpImage() {
        return '.is-selected > .Carousel-image'
    }

    get recentlyViewed() {
        return '.RecentlyViewed .ProductImages'
    }

    get peeriusRecommends() {
        return '.Recommendations .ProductCarousel-container > :nth-child(2) .Product-link  '
    }

    get miniBagIcon() {
        return isMobileLayout()
            ? '.Header-shoppingCartIconbutton'
            : '.ShoppingCart-icon'
    }

    /**
     * USER ACTIONS **************************************************************
     */

    clickSearchIcon() {
        return cy.get(this.searchIcon).click()
    }

    typeTermIntoSearchInput(term) {
        return cy.get(this.searchTermInput).type(term)
    }

    clickPerformSearch() {
        return cy.get(this.performSearchButton).click()
    }

    clickNoResultsSearchButton() {
        return cy.get(this.noResultsSearchButton).click()
    }

    clickPDPImage() {
        return cy.get(this.pdpImage).click()
    }

    clickRecentlyViewedImage() {
        return cy.get(this.recentlyViewed).click()
    }

    clickPeeriusRecommends() {
        return cy.get(this.peeriusRecommends).click()
    }

    openMiniBag() {
        cy.get(this.miniBagIcon).click()
    }
}
