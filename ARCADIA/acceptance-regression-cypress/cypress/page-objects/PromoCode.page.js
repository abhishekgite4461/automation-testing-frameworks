export default class PromoCode {
    get promoCodeInput() {
        return '#promotionCode-text'
    }
    get promoCodeApplyButton() {
        return '.PromotionCode-form > .Button'
    }
    get promoCodeHeader() {
        return '.PromotionCode-header'
    }
    get promoCodeTitle() {
        return '.PromotionCode-codeTitle'
    }
    get promoCodeErrorMessage() {
        return '.Message.is-shown.is-error.PromotionCode-message'
    }
    get promoCodeConfirmation() {
        return '.PromotionCode-codeConfirmation'
    }
    get promoCodeAddAnother() {
        return '.PromotionCode-addText'
    }

    /**
     * USER ACTIONS **************************************************************
     */

    typePromoCodeAndApply(code) {
        return cy
            .get(this.promoCodeInput)
            .type(code)
            .get(this.promoCodeApplyButton)
            .click()
    }

    enterPromoCodeAndApply(code) {
        return cy
            .get(this.promoCodeHeader)
            .click()
            .get(this.promoCodeInput)
            .type(code)
            .get(this.promoCodeApplyButton)
            .click()
    }

    enterAnotherPromoCodeAndApply(code) {
        return cy
            .get(this.promoCodeAddAnother)
            .click()
            .get(this.promoCodeInput)
            .type(code)
            .get(this.promoCodeApplyButton)
            .click()
    }

    verifyPromoCodeApplied(code) {
        return cy.get(this.promoCodeTitle).should('contain', code)
    }

    verifyPromoCodeErrorDisplayed() {
        return cy
            .get(this.promoCodeErrorMessage)
            .should(
                'contain',
                'The code you have entered has not been recognised.'
            )
    }
}
