export default class CheckoutHeader {
    get continueShoppingButton() {
        return '.Header-continueShopping'
    }

    get removeDeliveryAdressLink() {
        return '.AddressBookList-itemAction'
    }

    get addressBookItem() {
        return '.AddressBookList-item'
    }
}
