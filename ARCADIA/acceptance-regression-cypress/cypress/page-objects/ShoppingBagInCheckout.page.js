export default class ShoppingBagInCheckout {
    get ProductsInShoppingBag() {
        return 'div.OrderProducts-product'
    }
    get SizeLabelInShoppingBag() {
        return 'p.OrderProducts-productSize'
    }
}
