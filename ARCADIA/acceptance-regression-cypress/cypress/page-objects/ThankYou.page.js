export default class ThankYou {
    get continueShoppingButton() {
        return 'button.OrderComplete-button'
    }

    getThankyouPage() {
        return cy.get(this.continueShoppingButton)
    }
}
