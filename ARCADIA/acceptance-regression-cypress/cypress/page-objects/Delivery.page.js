import CheckoutHeader from './CheckoutHeader.page'
const personalDetails = {
    userFirstName: 'I Love',
    userSurname: 'Shopping',
    userPhoneNumber: '0987654321',
    title: 'Dr',
}

const deliveryAddressDetails = {
    ukPostCode: 'W1T 3NL',
    usPostCode: '90210',
    userCity: 'London',
    addressLineOne: 'Sesame Street',
    country: 'United Kingdom',
}

export default class Delivery extends CheckoutHeader {
    get titleDropdown() {
        return '.Select-select[name="title"]'
    }
    get firstNameInput() {
        return '.Input-field.Input-field-firstName'
    }
    get surnameInput() {
        return '.Input-field.Input-field-lastName'
    }
    get phoneNumberInput() {
        return '.Input-field.Input-field-telephone'
    }
    get enterAddressManually() {
        return '.FindAddress-link'
    }
    get addressLineOneInput() {
        return '[name=address1]'
    }
    get postCodeManualInput() {
        return '.YourAddress-form .Input-field.Input-field-postcode'
    }
    get cityManualInput() {
        return '.Input-field[name="city"]'
    }
    get proceedToPaymentButton() {
        return '.Button.DeliveryContainer-nextButton'
    }
    get countryDropdown() {
        return '.Select-select[name="country"]'
    }
    get firstNameInputValidationIcon() {
        return '.FormComponent-firstName .Input-validateIcon.Input-validateSuccess'
    }
    get surnameInputValidationIcon() {
        return '.FormComponent-lastName .Input-validateIcon.Input-validateSuccess'
    }
    get phoneNumberInputValidationIcon() {
        return '.FormComponent-telephone .Input-validateIcon.Input-validateSuccess'
    }
    get addressLineOneInputValidationIcon() {
        return '.FormComponent-address1 .Input-validateIcon.Input-validateSuccess'
    }
    get findAddressPostCodeInputValidationIcon() {
        return '.FindAddress .FormComponent-postCode .Input-validateIcon.Input-validateSuccess'
    }
    get yourAddressPostCodeInputValidationIcon() {
        return '.YourAddress-form .FormComponent-postcode .Input-validateIcon.Input-validateSuccess'
    }
    get cityInputValidationIcon() {
        return '.FormComponent-city .Input-validateIcon.Input-validateSuccess'
    }
    get ddpInlineNotificationAboveDeliveryOptions() {
        return '.Notification'
    }
    get ddpInlineNotificationBelowDeliveryOptions() {
        return '.DeliveryContainer-ddpAppliedToOrderMsg'
    }

    /**
     * USER ACTIONS ************************************************************
     */
    enterDeliveryDetailsManually() {
        return cy
            .get(this.enterAddressManually)
            .click()
            .get(this.titleDropdown)
            .select(personalDetails.title)
            .get(this.firstNameInput)
            .type(personalDetails.userFirstName)
            .get(this.surnameInput)
            .type(personalDetails.userSurname)
            .get(this.phoneNumberInput)
            .type(personalDetails.userPhoneNumber)
            .get(this.addressLineOneInput)
            .type(deliveryAddressDetails.addressLineOne)
            .get(this.postCodeManualInput)
            .type(deliveryAddressDetails.ukPostCode)
            .get(this.cityManualInput)
            .type(deliveryAddressDetails.userCity)
            .get(this.proceedToPaymentButton)
            .click()
    }

    enterTextIntoFirstnameField(text) {
        return cy.get(this.firstNameInput).type(text)
    }

    clickSurnameField() {
        return cy.get(this.surnameInput).click()
    }
}
