All the files inside this repository have been ported over from a legacy cypress
project. Thus, please do *not* use these files as reference when writing new
tests.

The feature files have been written in hindsight to give people a better idea of
what the GTM tests are actually doing. Should the GTM tests change, these might
turn out of sync.
