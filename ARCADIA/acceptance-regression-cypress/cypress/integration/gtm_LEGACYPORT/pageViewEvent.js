import './helpers/tcombSchemaExtensions'
import * as Schemas from './helpers/schemas'
import breakpoints from './../../constants/breakpoints'
import getLastGtmEventOfType from './helpers/getLastGtmEventOfType'
import {
    setupMocksForInitialRenderOnGenericPage,
    setupMocksForInitialRenderOnPlp,
    suite,
} from './../../lib/helpers'
import ProductListingPage from './../../page-objects/Plp.page'
const Plp = new ProductListingPage()

const searchTerm = 'find me something pretty'

suite('GTM analytics for pageView', () => {
    it('GTM analytics of home page', () => {
        setupMocksForInitialRenderOnGenericPage()
        cy.visit('/')

        getLastGtmEventOfType('user').then((event) => {
            expect(event.user.loggedIn).to.equal('False')
        })

        getLastGtmEventOfType('pageType').then((event) => {
            expect(event.pageCategory).to.equal('TS:Home Page')
            expect(event.pageType).to.equal('TS:Home Page')
        })
    })

    it('GTM analytics of plp page', () => {
        const {path} = setupMocksForInitialRenderOnPlp(searchTerm)
        cy.visit(path)

        getLastGtmEventOfType('user').then((event) => {
            expect(event.user.loggedIn).to.equal('False')
        })

        getLastGtmEventOfType('pageType').then((event) => {
            expect(event.pageCategory).to.equal('TS:Search')
            expect(event.pageType).to.equal('TS:Category Display')
        })
    })

    it('GTM analytics of Quick View modal', () => {
        cy.viewport(breakpoints.desktop.width, breakpoints.desktop.height)
        const {path, products} = setupMocksForInitialRenderOnPlp(searchTerm)
        const itemToSelect = 1
        cy.visit(path)
        cy.server()
        cy.route(
            'GET',
            `/api/products/${products[itemToSelect].productId}`,
            'fixture:pdp/multiSizeProduct/pdpInitialFetch'
        )
        cy.route(
            'GET',
            `/api/products?currentPage=2&q=${searchTerm}`,
            'fixture:plp/initialPageLoad'
        )

        Plp.selectQuickViewForItem(itemToSelect)
        getLastGtmEventOfType('ecommerce', 'detail').then((event) => {
            expect(event).to.be.tcombSchema(Schemas.QuickViewEventDetail)
        })
    })
})
