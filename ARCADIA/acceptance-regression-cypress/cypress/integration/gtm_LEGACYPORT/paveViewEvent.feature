Feature: GTM Analytics for page view

    Scenario: Home Page
        When I go to the home page
        Then there is a new "userState" event on the dataLayer
        And it contains a "user" object
        And the value of "loggedIn" on the user object is "False"
        And there is a "pageView" event on the dataLayer
        And the event has a "pageCategory" key of value "TS:Home Page"
        And the event has a "pageType" key of value "TS:Home Page"

    Scenario: Product Listing Page
        When I go to the PLP after a product search
        Then there is a new "userState" event on the dataLayer
        And it contains a "user" object
        And the value of "loggedIn" on the user object is "False"
        And there is a "pageView" event on the dataLayer
        And the event has a "pageCategory" key of value "TS:Search"
        And the event has a "pageType" key of value "TS:Category Display"

    Scenario: Quick View Modal (Desktop only)
        Given I use desktop viewport
        When I am on the PLP for jeans
        And I click the quick view icon
        Then there is a new "detail" event on the dataLayer
        And it contains an "ecommerce" object
        And the ecommerce object contains a valid currencyCode
        And the ecommerce objects contains a detail object
        And the detail object contains a "products" list
