Feature: Displaying DDP inline notifications in delivery page

  Scenario: Display DDP prompt in delivery page when new users have DDP in basket
    Given I am a new user
    And I have DDP in my bag
    When I am on the delivery page
    Then I should see the first inline DDP notification
    And the copy should be "Don't forget to select FREE express delivery to take advantage of your delivery subscription instantly!"
    And I should see the second inline DDP notification
    And the copy should be "Great news! Your delivery subscription has been applied to this order. Once placed, you can view your account to manage your subscription or start shopping to take advantage of your delivery perks."
    And the edit link and size should not be visible for DDP product
  
  Scenario: Display DDP prompt in delivery page when new users have mixed basket including DDP
    Given I am a new user
    And I have DDP in my bag along with any another physical product
    When I am on the delivery page
    Then I should see the first inline DDP notification
    And the copy should be "Don't forget to select FREE express delivery to take advantage of your delivery subscription instantly!"
    And I should see the second inline DDP notification
    And the copy should be "Great news! Your delivery subscription has been applied to this order. Once placed, you can view your account to manage your subscription or start shopping to take advantage of your delivery perks."
    And the edit link and size should not be visible for DDP product