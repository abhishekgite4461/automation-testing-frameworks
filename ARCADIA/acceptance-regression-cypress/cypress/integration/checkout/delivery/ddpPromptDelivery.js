import {suite, setFeature, setupMocksForCheckout} from './../../../lib/helpers'
import DeliveryPage from './../../../page-objects/Delivery.page'
import orderSummaryNewUserDdpProd from './../../../fixtures/checkout/order_summary---returningUserDdpProd'
import checkoutProfileNewUser from './../../../fixtures/checkout/account--newUser'
import orderSummaryNewUserSingleSizeAndDdp from './../../../fixtures/checkout/order_summary---newUserSingleSizeProdAndDdp'
import MiniBag from './../../../page-objects/MiniBag.page'
import ShoppingBag from './../../../page-objects/ShoppingBagInCheckout.page'

const deliveryPage = new DeliveryPage()
const miniBag = new MiniBag()
const shoppingBag = new ShoppingBag()

suite(
    'Display DDP notifications in delivery page when new users have DDP in basket',
    () => {
        it('Given I am a new user ', () => {})

        it('And I have DDP in my bag', () => {})

        it('When I am on the delivery page', () => {
            setupMocksForCheckout(
                orderSummaryNewUserDdpProd,
                checkoutProfileNewUser
            )
            cy.visit('checkout/delivery')
            setFeature('FEATURE_DDP')
        })

        it('And I should see the inline DDP notification', () => {
            cy.get(
                deliveryPage.ddpInlineNotificationBelowDeliveryOptions
            ).should('be.visible')
        })

        it('And the copy should be "Great news! Your delivery subscription has been applied to your order. Once placed, you can view your account to manage your subscription or start shopping to take advantage of your delivery perks."', () => {
            cy.get(
                deliveryPage.ddpInlineNotificationBelowDeliveryOptions
            ).should(
                'have.text',
                'Great news! Your delivery subscription has been applied to your order. Once placed, you can view your account to manage your subscription or start shopping to take advantage of your delivery perks.'
            )
        })

        it('And the edit link and size should not be visible for DDP product', () => {
            cy.get(miniBag.editProductButton).should('not.be.visible')
            cy.get(miniBag.productSize).should('not.be.visible')
        })
    }
)

suite(
    'Display DDP prompt in delivery page when new users have mixed basket including DDP',
    () => {
        it('Given I am a new user', () => {})

        it('And I have DDP in my bag along with any another physical product', () => {})

        it('When I am on the delivery page', () => {
            setupMocksForCheckout(
                orderSummaryNewUserSingleSizeAndDdp,
                checkoutProfileNewUser
            )
            cy.visit('checkout/delivery')
            setFeature('FEATURE_DDP')
        })

        it('And I should see the inline DDP notification', () => {
            cy.get(
                deliveryPage.ddpInlineNotificationBelowDeliveryOptions
            ).should('be.visible')
        })

        it('And the copy should be "Great news! Your delivery subscription has been applied to your order. Once placed, you can view your account to manage your subscription or start shopping to take advantage of your delivery perks."', () => {
            cy.get(
                deliveryPage.ddpInlineNotificationBelowDeliveryOptions
            ).should(
                'have.text',
                'Great news! Your delivery subscription has been applied to your order. Once placed, you can view your account to manage your subscription or start shopping to take advantage of your delivery perks.'
            )
        })

        it('And the edit link  and size should not be visible for DDP product', () => {
            cy.get(shoppingBag.ProductsInShoppingBag)
                .eq(0)
                .within(() => {
                    cy.get(miniBag.editProductButton).should('not.be.visible')
                    cy.get(miniBag.productSize).should('not.be.visible')
                })
        })
    }
)
