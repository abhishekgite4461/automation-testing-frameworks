import {suite, setFeature, setupMocksForCheckout} from './../../../lib/helpers'

import DeliveryPayment from './../../../page-objects/DeliveryPayment.page'
import orderSummaryWithDdpInBag from './../../../fixtures/checkout/order_summary---returningUserDdpProd'
import checkoutProfile from './../../../fixtures/checkout/account--returningUserFullCheckoutProfile'
import second from './../../../fixtures/checkout/order_summary---returningUserSingleSizeProdAndDdp.json'

const deliveryPayment = new DeliveryPayment()

suite(
    'Display DDP prompt in delivery-payment page when returning users have DDP in basket',
    () => {
        it('Given I am an authenticated returning user', () => {})

        it('And I have DDP in my guest bag', () => {})

        it('When I am on the delivery-payment page', () => {
            setupMocksForCheckout(orderSummaryWithDdpInBag, checkoutProfile)
            cy.visit('checkout/delivery-payment')
            setFeature('FEATURE_DDP')
        })

        it('And I should see the inline DDP notification', () => {
            cy.get(
                deliveryPayment.ddpInlineNotificationAboveSavedAddress
            ).should('be.visible')
        })

        it('And the copy should be "Great news! Your delivery subscription has been applied to your order. Once placed, you can view your account to manage your subscription or start shopping to take advantage of your delivery perks."', () => {
            cy.get(
                deliveryPayment.ddpInlineNotificationAboveSavedAddress
            ).should(
                'have.text',
                'Great news! Your delivery subscription has been applied to your order. Once placed, you can view your account to manage your subscription or start shopping to take advantage of your delivery perks.'
            )
        })
    }
)

suite(
    'Display DDP prompt in delivery-payment page when returning users have DDP and a physical product in basket',
    () => {
        it('Given I am an authenticated returning user', () => {})

        it('And I have DDP and any other physical product in my guest bag', () => {})

        it('When I am on the delivery-payment page', () => {
            setupMocksForCheckout(second, checkoutProfile)
            cy.visit('checkout/delivery-payment')
            setFeature('FEATURE_DDP')
        })

        it('And I should see the inline DDP notification', () => {
            cy.get(
                deliveryPayment.ddpInlineNotificationAboveSavedAddress
            ).should('be.visible')
        })

        it('And the copy should be "Great news! Your delivery subscription has been applied to your order. Once placed, you can view your account to manage your subscription or start shopping to take advantage of your delivery perks."', () => {
            cy.get(
                deliveryPayment.ddpInlineNotificationAboveSavedAddress
            ).should(
                'have.text',
                'Great news! Your delivery subscription has been applied to your order. Once placed, you can view your account to manage your subscription or start shopping to take advantage of your delivery perks.'
            )
        })
    }
)
