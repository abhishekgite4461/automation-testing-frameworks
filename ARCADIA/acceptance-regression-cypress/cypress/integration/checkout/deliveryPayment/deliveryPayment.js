import {
    setupMocksForInitialRenderOnGenericPage,
    setupMocksForDeleteAdditionalDeliveryDetails,
    suite,
    setupMocksForCheckout,
    setFeature,
} from '../../../lib/helpers'
import DeliveryPaymentPage from '../../../page-objects/DeliveryPayment.page'
import MiniBag from './../../../page-objects/MiniBag.page'
import orderSummaryWithDdpInBag from './../../../fixtures/checkout/order_summary---returningUserDdpProd'
import orderSummary from '../../../fixtures/checkout/order_summary---returningUserSingleSizeProd.json'
import checkoutProfile from '../../../fixtures/checkout/account--returningUserFullCheckoutProfile.json'

const deliveryPaymentPage = new DeliveryPaymentPage()
const miniBag = new MiniBag()

suite(
    'Delete additional delivery address from delivery and payment page',
    () => {
        it('Given I am returning user with two delivery addresses', () => {})

        it('And I am on delivery and payment page with items in shopping bag', () => {
            setupMocksForCheckout(orderSummary, checkoutProfile)
            cy.visit('checkout/delivery-payment')
        })

        it('When I click the "Delete" link for my additional delivery address', () => {
            setupMocksForInitialRenderOnGenericPage()
            setupMocksForDeleteAdditionalDeliveryDetails()

            cy.get(deliveryPaymentPage.removeDeliveryAdressLink).click()
        })

        it('Then the "Delete" link is no longer visible', () => {
            cy.get(deliveryPaymentPage.removeDeliveryAdressLink).should(
                'not.exist'
            )
        })

        it('And there is only one delivery address showing', () => {
            cy.get(deliveryPaymentPage.addressBookItem).should('have.length', 1)
        })
    }
)

suite(
    'Display helpful message for returning users when they have only DDP in basket for first time',
    () => {
        it('Given I am returning user with DDP in my basket', () => {})

        it('When I am in delivery-payment page', () => {
            setupMocksForCheckout(orderSummaryWithDdpInBag, checkoutProfile)
            cy.visit('checkout/delivery-payment')
            setFeature('FEATURE_DDP')
        })

        it('Then I should see the helpful message "Great news! Your delivery subscription has been applied to your order. Once placed, you can view your account to manage your subscription or start shopping to take advantage of your delivery perks."', () => {
            cy.get(
                deliveryPaymentPage.ddpInlineNotificationAboveSavedAddress
            ).should(
                'have.text',
                'Great news! Your delivery subscription has been applied to your order. Once placed, you can view your account to manage your subscription or start shopping to take advantage of your delivery perks.'
            )
        })

        it('And the edit link  and size should not be visible for DDP product', () => {
            cy.get(miniBag.editProductButton).should('not.be.visible')
            cy.get(miniBag.productSize).should('not.be.visible')
        })
    }
)
