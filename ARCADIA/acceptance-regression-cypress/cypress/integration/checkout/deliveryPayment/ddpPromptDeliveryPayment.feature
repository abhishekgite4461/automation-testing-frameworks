Feature: Displaying DDP prompt in delivery-payment page

  Scenario: Display DDP prompt in delivery-payment page when returning users have DDP in basket
    Given I am an authenticated returning user
    And I have DDP in my guest bag
    When I am on the delivery-payment page
    Then I should see the first inline DDP notification
    And the copy should be "Don't forget to select FREE express delivery to take advantage of your delivery subscription instantly!"
    And I should see the second inline DDP notification
    And the copy should be "Great news! Your delivery subscription has been applied to this order. Once placed, you can view your account to manage your subscription or start shopping to take advantage of your delivery perks."

  Scenario: Display DDP prompt in delivery-payment page when returning users have DDP and any other physical product in basket
    Given I am an authenticated returning user
    And I have DDP and any other physical product in my guest bag
    When I am on the delivery-payment page
    Then I should see the first inline DDP notification
    And the copy should be "Don't forget to select FREE express delivery to take advantage of your delivery subscription instantly!"
    And I should see the second inline DDP notification
    And the copy should be "Great news! Your delivery subscription has been applied to this order. Once placed, you can view your account to manage your subscription or start shopping to take advantage of your delivery perks."
  
  Scenario: Display DDP prompt in delivery-payment page for active DDP susbscribers
    Given I am an authenticated active DDP subscriber
    And I have an item in my bag
    When I am on the delivery-payment page
    Then I should see the DDP notification 
    And the copy should be "Don't forget to select FREE express delivery to take advantage of your delivery subscription instantly!"