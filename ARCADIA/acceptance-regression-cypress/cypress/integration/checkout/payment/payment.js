import {suite, setFeature, setupMocksForCheckout} from './../../../lib/helpers'
import orderSummaryNewUserDdpProd from './../../../fixtures/checkout/order_summary---returningUserDdpProd'
import checkoutProfileNewUser from './../../../fixtures/checkout/account--newUser'
import MiniBag from './../../../page-objects/MiniBag.page'

const miniBag = new MiniBag()

suite(
    'Size and edit link should not be available in payment page for DDP product',
    () => {
        it('Given I am a new user', () => {})

        it('I have only DDP in my bag', () => {})

        it('When I am in payments page', () => {
            setupMocksForCheckout(
                orderSummaryNewUserDdpProd,
                checkoutProfileNewUser
            )
            cy.visit('checkout/payment')
            setFeature('FEATURE_DDP')
        })

        it('Then edit link and size should not be available in Shopping Bag', () => {
            cy.get(miniBag.editProductButton).should('not.be.visible')
            cy.get(miniBag.productSize).should('not.be.visible')
        })
    }
)
