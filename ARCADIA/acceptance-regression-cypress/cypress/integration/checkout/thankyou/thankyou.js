import {
    setFeature,
    suite,
    setupMocksForCheckout,
    setupMocksForInitialRenderOnGenericPage,
} from './../../../lib/helpers'
import routes from './../../../constants/routes'
import order_summary from './../../../fixtures/checkout/order_summary---returningUserSingleSizeProdAndDdp.json'
import order from './../../../fixtures/checkout/order--returningUserSingleSizeProdAndDdpProd-visa'
import checkout_profile from './../../../fixtures/checkout/account--returningUserFullCheckoutProfile.json'
import DeliveryPaymentPage from './../../../page-objects/DeliveryPayment.page'
import ThankyouPage from './../../../page-objects/ThankYou.page'
import ShoppingBag from './../../../page-objects/ShoppingBagInCheckout.page'

const deliveryPayment = new DeliveryPaymentPage()
const thankyouPage = new ThankyouPage()
const shoppingBag = new ShoppingBag()
suite(
    'Edit link and size should not be available for DDP Product in thank you page',
    () => {
        it('Given I an authenticated returning user with DDP and a regular product in my bag', () => {})
        it('And I am in delivery payment page', () => {
            setupMocksForCheckout(order_summary, checkout_profile)
            cy.visit('checkout/delivery-payment')
            setFeature('FEATURE_DDP')
        })
        it('When I am in thank you page after completing the checkout', () => {
            setupMocksForInitialRenderOnGenericPage()
            cy.server()
            cy.route('POST', routes.checkoutOrder, order)
            cy.route(
                'POST',
                routes.paymentsDeliveryUkBillingUk,
                'fixture:login/payments--ukBillingUkDelivery'
            )
            cy.route('GET', routes.account, checkout_profile)
            deliveryPayment.payAsReturningUser()
            thankyouPage.getThankyouPage()
        })

        it('Then the size should not be available for DDP product', () => {
            cy.get(shoppingBag.ProductsInShoppingBag)
                .eq(1)
                .within(() => {
                    cy.get(shoppingBag.SizeLabelInShoppingBag).should(
                        'not.be.visible'
                    )
                })
        })
    }
)
