Feature: Wishlist

    Scenario: Adding product to wishlist from PLP shows it as marked on the PDP
        Given The wishlist feature is activated
        And I am logged in
        And I visit the PLP
        And I see wishlist icons on the product images
        When I click the wishlist icon on the product image
        And I click on the product image
        Then I land on the PDP
        And I see the wishlist icon
        And the wishlist icon is selected

    Scenario: Adding product to wishlist from PDP shows it in the wishlist, can be removed
        Given The wishlist feature is activated
        And I am logged in
        And I visit the PDP
        And I see the wishlist icon
        And I click the wishlist icon
        When I go to my wishlist via the navigation bar
        And I see the product I just added in my wishlist
        And I remove the item from my wishlist
        Then the item does not appear anymore in my wishlist
