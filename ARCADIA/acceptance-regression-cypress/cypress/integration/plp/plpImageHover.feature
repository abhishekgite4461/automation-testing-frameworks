Feature: PLP image hover

  Scenario: PLP hovering on primary image
    Given I am on Jeans PLP
    When I hover on the primary image of the first product
    Then the secondary image should be displayed
