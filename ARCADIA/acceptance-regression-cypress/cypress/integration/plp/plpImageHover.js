import {setupMocksForInitialRenderOnPlp, suite} from './../../lib/helpers'
import ProductListingPage from './../../page-objects/Plp.page'
import {isDesktopLayout} from '../../lib/helpers'
const Plp = new ProductListingPage()

isDesktopLayout() &&
    suite('PLP hovering on primary image', () => {
        it('Given I am on Jeans PLP', function() {
            const {path} = setupMocksForInitialRenderOnPlp('Jeans')
            cy.visit(path)
        })

        it('When I hover on the primary image of the first product', function() {
            Plp.hoverOnFirstProduct()
        })

        it('Then the secondary image should be displayed', function() {
            cy.get(Plp.firstImageAttribute)
                .should('have.attr', 'src')
                .then((src) => {
                    expect(src).to.contain('_F_1')
                })
        })
    })
