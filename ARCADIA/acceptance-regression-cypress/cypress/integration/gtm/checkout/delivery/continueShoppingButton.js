import {
    setupMocksForInitialRenderOnGenericPage,
    setupMocksForCheckout,
    isDesktopLayout,
    suite,
} from '../../../../lib/helpers'

import DeliveryPage from '../../../../page-objects/Delivery.page'
import orderSummary from '../../../../fixtures/checkout/order_summary---newUserSingleSizeProd.json'
import checkoutProfile from '../../../../fixtures/checkout/account--newUser.json'
import * as EventFilters from '../../../gtm_LEGACYPORT/helpers/filters'

const deliveryPage = new DeliveryPage()

suite('GTM Analytics for form input validations', () => {
    let formValidationFailure, formValidationSuccess

    it('Given I am new user', () => {})

    it('And I am on delivery page with items in shopping bag', () => {
        setupMocksForCheckout(orderSummary, checkoutProfile)
        cy.visit('/checkout/delivery')
    })

    it('When I enter an invalid input into the "firstname" field', () => {
        deliveryPage.enterTextIntoFirstnameField(' ')
        deliveryPage.clickSurnameField()
        cy.filterGtmEvents({
            filter: EventFilters.formValidation,
        }).then((event) => {
            formValidationFailure = event
        })
    })

    it('Then the datalayer has a new "formvalidation" event', () => {
        expect(formValidationFailure.event).to.equal('formValidation')
    })

    it('And the form validation status contains value "failure"', () => {
        expect(formValidationFailure.validationStatus).to.equal('failure')
    })

    it('When I enter text into "firstname" field', () => {
        deliveryPage.enterTextIntoFirstnameField('a')
        deliveryPage.clickSurnameField()
        cy.filterGtmEvents({
            filter: EventFilters.formValidation,
        }).then((event) => {
            formValidationSuccess = event
        })
    })

    it('Then the datalayer has a new "formvalidation" event', () => {
        expect(formValidationSuccess.event).to.equal('formValidation')
    })

    it('And the form validation status contains value "success"', () => {
        expect(formValidationSuccess.validationStatus).to.equal('success')
    })
})

isDesktopLayout() &&
    suite('GTM Analytics for continue shopping button in delivery page', () => {
        let continueShoppingButtonEvent

        it('When I click on the continue shopping button', () => {
            setupMocksForInitialRenderOnGenericPage()
            cy.get(deliveryPage.continueShoppingButton).click()
            cy.filterGtmEvents({filter: EventFilters.clickEvent}).then(
                (event) => {
                    continueShoppingButtonEvent = event
                }
            )
        })

        it('Then the datalayer has a new "clickevent" event', () => {
            expect(continueShoppingButtonEvent.event).to.equal('clickevent')
        })

        it('And event contains a "ec" with value "checkout"', () => {
            expect(continueShoppingButtonEvent.ec).to.equal('checkout')
        })

        it('And event contains a "ea" with value "continue shopping"', () => {
            expect(continueShoppingButtonEvent.ea).to.equal('continue shopping')
        })

        it('And event contains a "el" with value "delivery"', () => {
            expect(continueShoppingButtonEvent.el).to.equal('delivery')
        })
    })
