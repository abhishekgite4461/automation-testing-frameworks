import {
    suite,
    setupMocksForCheckout,
    setUpMocksForStoreLocator,
} from './../../../../lib/helpers'
import getLastGtmEventOfType from '../../../gtm_LEGACYPORT/helpers/getLastGtmEventOfType'
import * as EventFilters from '../../../gtm_LEGACYPORT/helpers/filters'
import * as Schemas from '../../../gtm_LEGACYPORT/helpers/schemas'
import '../../../gtm_LEGACYPORT/helpers/tcombSchemaExtensions'
import orderSummaryNewUser from './../../../../fixtures/checkout/order_summary---newUserSingleSizeProd.json'
import StoreLocatorPage from './../../../../page-objects/StoreLocator.page'
import checkoutProfileNewUser from './../../../../fixtures/checkout/account--newUser'
import routes from '../../../../constants/routes'

const strLoc = new StoreLocatorPage()

suite('GTM events for (collect from store) on delivery page ', () => {
    let switchDeliveryOptionEvent
    let storeLocatorApiResponseEvent
    let orderSummaryApiResponseEvent
    let orderSummaryDeliveryTypeEvent

    it('Given I am a new user on the delivery page', () => {
        setupMocksForCheckout(orderSummaryNewUser, checkoutProfileNewUser)
        cy.visit('checkout/delivery')
    })

    it('Then the expected page view and checkout step events are in the dataLayer', () => {
        cy.filterGtmEvents({
            filter: EventFilters.ecommerceCheckoutStep(2),
            timeout: 15000,
            name: 'CheckoutStep2',
        }).then((event) => {
            expect(event).to.be.tcombSchema(Schemas.CheckoutStep2)
        })
        cy.filterGtmEvents({filter: EventFilters.pageViewEvent}).then(
            (event) => {
                expect(event.pageCategory).to.equal('TS:Checkout')
                expect(event.pageType).to.equal('TS:Delivery Details')
            }
        )
    })

    it('When I select Collect From Store delivery method', () => {
        strLoc.selectCollectFromStoreDeliveryMethod()
    })

    it('Then there will be new event with value "deliveryOptionChanged"', () => {
        getLastGtmEventOfType('event', 'deliveryOptionChanged').then(
            (event) => {
                switchDeliveryOptionEvent = event
                expect(switchDeliveryOptionEvent.deliveryOption).to.equal(
                    'Collect from Store'
                )
            }
        )
    })

    it('When I perform a location search from the store locator', () => {
        setUpMocksForStoreLocator()
        strLoc.storeLocatorSearchInDelivery('London')
        strLoc.selectFirstStoreFromResultsListInDelivery()
        strLoc.selectedLocationGoButtonInDelivery()
    })

    it('Then there will be an "apiEndpoint" event with value "api/store-locator"', () => {
        setUpMocksForStoreLocator()
        cy.filterGtmEvents({
            filter: (dlItem) => dlItem.apiEndpoint === 'api/store-locator',
            timeout: 8000,
        }).then((event) => {
            storeLocatorApiResponseEvent = event
            expect(storeLocatorApiResponseEvent.event).to.equal('apiResponse')
            expect(storeLocatorApiResponseEvent.apiMethod).to.equal('GET')
        })
    })

    it('When I make my store selection', () => {
        setUpMocksForStoreLocator()
        cy.route(
            '' + 'PUT',
            routes.checkoutOrderSummary,
            'fixture:checkout/order_summary---newUserSingleSizeProdCollectFromStoreDeliveryMethod'
        ).as('orderSummaryStoreDelivery')

        // TODO remove this uncaught exception handling required for selectStoreButton once DES-4405 has been resolved
        Cypress.on('uncaught:exception', () => {
            return false
        })
        strLoc.expandStoreDetailsFromList(1)
        strLoc.selectStoreButton()
        cy.wait('@orderSummaryStoreDelivery') // Wait for the first order summary to complete
    })

    it('Then there will be an "apiEndpoint" event with value "api/checkout/order_summary"', () => {
        cy.filterGtmEvents({
            filter: (dlItem) =>
                dlItem.apiEndpoint === 'api/checkout/order_summary',
            timeout: 8000,
        }).then((event) => {
            orderSummaryApiResponseEvent = event
            expect(orderSummaryApiResponseEvent.event).to.equal('apiResponse')
            expect(orderSummaryApiResponseEvent.apiMethod).to.equal('PUT')
        })
    })

    it('When I change my delivery type to Collect From Store Express', () => {
        cy.server()
        cy.route(
            '' + 'PUT',
            routes.checkoutOrderSummary,
            'fixture:checkout/order_summary---newUserSingleSizeProdCollectFromStoreDeliveryExpressType'
        )
        strLoc.selectStoreExpressDeliveryType()
    })

    it('Then there will be a delivery method changed event indicating "Collect From Store Express"', () => {
        cy.filterGtmEvents({
            filter: (dlItem) => dlItem.event === 'deliveryMethodChanged',
            timeout: 8000,
        }).then((event) => {
            orderSummaryDeliveryTypeEvent = event
            expect(orderSummaryDeliveryTypeEvent.deliveryMethod).to.equal(
                'Collect From Store Express'
            )
        })
    })
})
