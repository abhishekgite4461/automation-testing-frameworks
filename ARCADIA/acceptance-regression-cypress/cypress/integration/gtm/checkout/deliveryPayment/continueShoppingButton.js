import {
    setupMocksForInitialRenderOnGenericPage,
    setupMocksForCheckout,
    isDesktopLayout,
    suite,
} from '../../../../lib/helpers'

import DeliveryPaymentPage from '../../../../page-objects/DeliveryPayment.page'
import orderSummary from '../../../../fixtures/checkout/order_summary---returningUserSingleSizeProd.json'
import checkoutProfile from '../../../../fixtures/checkout/account--returningUserFullCheckoutProfile.json'
import * as EventFilters from '../../../gtm_LEGACYPORT/helpers/filters'

const deliveryPaymentPage = new DeliveryPaymentPage()

isDesktopLayout() &&
    suite(
        'GTM Analytics for continue shopping button in delivery and payment page',
        () => {
            let continueShoppingButtonEvent

            it('Given I am returning user', () => {})

            it('And I am on delivery and payment page with items in shopping bag', () => {
                setupMocksForCheckout(orderSummary, checkoutProfile)
                cy.visit('/checkout/delivery-payment')
            })

            it('When I click on the continue shopping button', () => {
                setupMocksForInitialRenderOnGenericPage()
                /* Using {force:true} to click as sometime the overlay takes time to load
                    and the button is not clikable at that instance                         */
                cy.get(deliveryPaymentPage.continueShoppingButton).click({
                    force: true,
                })
                cy.filterGtmEvents({filter: EventFilters.clickEvent}).then(
                    (event) => {
                        continueShoppingButtonEvent = event
                    }
                )
            })

            it('Then the datalayer has a new "clickevent" event', () => {
                expect(continueShoppingButtonEvent.event).to.equal('clickevent')
            })

            it('And event contains a "ec" with value "checkout"', () => {
                expect(continueShoppingButtonEvent.ec).to.equal('checkout')
            })

            it('And event contains a "ea" with value "continue shopping"', () => {
                expect(continueShoppingButtonEvent.ea).to.equal(
                    'continue shopping'
                )
            })

            it('And event contains a "el" with value "delivery-payment"', () => {
                expect(continueShoppingButtonEvent.el).to.equal(
                    'delivery-payment'
                )
            })
        }
    )
