import {
    setupMocksForInitialRenderOnGenericPage,
    setupMocksForCheckout,
    suite,
} from '../../../../lib/helpers'
import routes from '../../../../constants/routes'
import PaymentPage from '../../../../page-objects/Payment.page'
import {checkout} from '../../../../fixtures'
import DeliveryPaymentPage from '../../../../page-objects/DeliveryPayment.page'
import orderSummary from '../../../../fixtures/checkout/order_summary---returningUserSingleSizeProd.json'
import checkoutProfileReturningUser from '../../../../fixtures/checkout/account--returningUserFullCheckoutProfile.json'

const paymentPage = new PaymentPage()
const deliveryPaymentPage = new DeliveryPaymentPage()

suite(
    'GTM Analytics for errorMessage for a returning user client side error when user clicks on confirm and pay button',
    () => {
        let checkoutFailedOrderGtmErrorEvent

        it('Given I am a returning  user', () => {})

        it('And I am on the Delivery&Payment page of Checkout', () => {
            setupMocksForCheckout(orderSummary, checkoutProfileReturningUser)
            cy.visit('/checkout/delivery-payment')
        })
        it('And I enter the payments details', () => {
            paymentPage.enterCVVNumber()
        })

        it('And I check terms and conditions checkbox', () => {
            setupMocksForInitialRenderOnGenericPage()
            paymentPage.tickTermsAndConditionCheckBox()
        })

        it('When I confirm my order', function() {
            setupMocksForInitialRenderOnGenericPage()
            cy.route({
                method: 'POST',
                url: routes.checkoutOrder,
                response: checkout.order422Error,
                status: 422,
            })
            cy.get(deliveryPaymentPage.orderAndPayButton).click()
        })

        it('And The order endpoint returns an error', function() {
            cy.get(deliveryPaymentPage.paymentErrorMessage)
        })

        it("Then There is a new 'errorMessage' event in the dataLayer", function() {
            cy.filterGtmEvents({
                filter: (dlItem) => dlItem.event === 'errorMessage',
                timeout: 3000,
            }).then((event) => {
                checkoutFailedOrderGtmErrorEvent = event
                expect(checkoutFailedOrderGtmErrorEvent.event).to.equal(
                    'errorMessage'
                )
            })
        })

        it('And event contains an errorMessage', function() {
            expect(
                typeof checkoutFailedOrderGtmErrorEvent.errorMessage
            ).to.equal('string')
            expect(
                checkoutFailedOrderGtmErrorEvent.errorMessage.length
            ).to.be.greaterThan(0)
        })
    }
)
