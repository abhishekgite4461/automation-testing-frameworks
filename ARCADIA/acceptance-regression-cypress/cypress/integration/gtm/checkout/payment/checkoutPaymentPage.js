import {
    setupMocksForInitialRenderOnGenericPage,
    setupMocksForCheckout,
    suite,
} from '../../../../lib/helpers'
import routes from '../../../../constants/routes'
import PaymentPage from '../../../../page-objects/Payment.page'
import getLastGtmEventOfType from '../../../gtm_LEGACYPORT/helpers/getLastGtmEventOfType'
import * as EventFilters from '../../../gtm_LEGACYPORT/helpers/filters'
import * as Schemas from '../../../gtm_LEGACYPORT/helpers/schemas'
import '../../../gtm_LEGACYPORT/helpers/tcombSchemaExtensions'
import ThankyouPage from '../../../../page-objects/ThankYou.page'
import orderSummary from '../../../../fixtures/checkout/order_summary---returningUserSingleSizeProd.json'
import checkoutProfileNewUser from '../../../../fixtures/checkout/account--returningUserFullCheckoutProfile.json'

const paymentPage = new PaymentPage()
const thankyouPage = new ThankyouPage()

suite('GTM Analytics for payment page events', () => {
    let confirmAndPayButtonErrorEvent, confirmAndPayButtonEvent

    it('Given I am a returning user', () => {})

    it('And I am in payments page with items in shopping bag', () => {
        setupMocksForCheckout(orderSummary, checkoutProfileNewUser)
        cy.visit('/checkout/payment')
    })

    it('Then the expected page view and checkout step events are in the dataLayer', () => {
        cy.filterGtmEvents({
            filter: EventFilters.ecommerceCheckoutStep(4),
            timeout: 15000,
            name: 'CheckoutStep4',
        }).then((event) => {
            expect(event).to.be.tcombSchema(Schemas.CheckoutStep4)
        })
        cy.filterGtmEvents({filter: EventFilters.pageViewEvent}).then(
            (event) => {
                expect(event.pageCategory).to.equal('TS:Order Submit Form')
                expect(event.pageType).to.equal('TS:Order Submit Form')
            }
        )
    })

    it('And I enter the payments details', () => {
        paymentPage.enterCVVNumber()
    })
    it('When I click on confirm and pay button without checking the terms and conditions checkbox', () => {
        setupMocksForInitialRenderOnGenericPage()
        paymentPage.clickOrderButton()
        getLastGtmEventOfType('errorMessage').then((event) => {
            confirmAndPayButtonErrorEvent = event
        })
    })
    it('Then The datalayer has a new "errorMessage" event', () => {
        expect(confirmAndPayButtonErrorEvent.event).to.equal('errorMessage')
    })
    it('And event contains a "errorMessage" with value "Error paying order"', () => {
        expect(confirmAndPayButtonErrorEvent.errorMessage).to.equal(
            'Error paying order'
        )
    })

    it('And I check terms and conditions checkbox', () => {
        setupMocksForInitialRenderOnGenericPage()
        paymentPage.tickTermsAndConditionCheckBox()
    })

    it('When I click on confirm and pay button and I am in thank you page', () => {
        setupMocksForInitialRenderOnGenericPage()
        cy.route(
            'POST',
            routes.checkoutOrder,
            'fixture:checkout/order--newUserSingleSizeProd-visa'
        )
        cy.route('GET', routes.account, 'fixture:checkout/account--newUser')
        paymentPage.clickOrderButton()
        thankyouPage.getThankyouPage()
    })

    it('Then the dataLayer has a new page view event with value "TS:Order Confirmed"', () => {
        cy.filterGtmEvents({
            filter: EventFilters.ecommercePurchase,
            timeout: 16000,
        }).then((event) => {
            expect(event).to.be.tcombSchema(Schemas.OrderConfirmed)
        })

        cy.filterGtmEvents({filter: EventFilters.pageViewEvent}).then(
            (event) => {
                expect(event.pageCategory).to.equal('TS:Order Confirmed')
                expect(event.pageType).to.equal('TS:Order Confirmed')
            }
        )
    })

    it('And The datalayer has a new "clickevent" event', () => {
        getLastGtmEventOfType('ea').then((event) => {
            confirmAndPayButtonEvent = event
            expect(confirmAndPayButtonEvent.event).to.equal('clickevent')
        })
    })
    it('And event contains a "ea" with value "clicked"', () => {
        expect(confirmAndPayButtonEvent.ea).to.equal('clicked')
    })
    it('And event contans a "ec" with value "checkout"', () => {
        expect(confirmAndPayButtonEvent.ec).to.equal('checkout')
    })
    it('And event contains a "el" with value "confirm-and-pay"', () => {
        expect(confirmAndPayButtonEvent.el).to.equal('confirm-and-pay')
    })
})
