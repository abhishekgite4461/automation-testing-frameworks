import '../../gtm_LEGACYPORT/helpers/tcombSchemaExtensions'
import getLastGtmEventOfType from '../../gtm_LEGACYPORT/helpers/getLastGtmEventOfType'
import Navigation from '../../../page-objects/Navigation.page'
import {
    setupMocksForInitialRenderOnPdp,
    setupMocksForInitialRenderOnGenericPage,
    setUpMocksForPeeriusRecommends,
    setUpMocksForRecentlyViewed,
    isDesktopLayout,
    suite,
} from '../../../lib/helpers'
import * as Schemas from '../../gtm_LEGACYPORT/helpers/schemas'
import routes from '../../../constants/routes'
import ProductDetailPage from '../../../page-objects/Pdp.page'
import StoreLocatorPage from '../../../page-objects/StoreLocator.page'
import multiSizeProduct from '../../../fixtures/pdp/multiSizeProduct/pdpInitialFetch'
import peeriusData from '../../../fixtures/pdp/peerius/peeriusRecommends.json'
import recentProds from '../../../fixtures/pdp/recentlyViewed/recentlyViewed.json'

const pdp = new ProductDetailPage()
const navigation = new Navigation()
const strLoc = new StoreLocatorPage()

suite('GTM events for initial PDP visit', function() {
    let pdpPageLoad
    let pageLoadEcommerceEvent

    it('Given I have navigated to a PDP', () => {
        const path = setupMocksForInitialRenderOnPdp(multiSizeProduct)
        setUpMocksForRecentlyViewed(recentProds)
        cy.visit(path)

        setUpMocksForPeeriusRecommends(peeriusData)
    })

    it('Then the dataLayer contains a new "pageView" event', function() {
        getLastGtmEventOfType('pageType').then((event) => {
            pdpPageLoad = event
        })
    })

    it('And event contains a "pageCategory" containing "TS: Prod Detail"', function() {
        expect(pdpPageLoad.pageCategory).to.contain('TS:Prod Detail')
    })

    it('And event contains a "pageType" with value "TS:Product Display"', function() {
        expect(pdpPageLoad.pageType).to.equal('TS:Product Display')
    })

    it('And the dataLayer has a new "eccomerce : detail" event', function() {
        getLastGtmEventOfType('ecommerce', 'detail').then((event) => {
            pageLoadEcommerceEvent = event
            expect(pageLoadEcommerceEvent).to.be.tcombSchema(
                Schemas.EcommerceDetail
            )
        })
    })

    it('And event contains a "sizesAvailable" with a numeric value', function() {
        expect(
            pageLoadEcommerceEvent.ecommerce.detail.products[0].sizesAvailable
        ).to.be.tcombSchema(Schemas.StringAvailableSizes)
    })

    it('And event contains a "currencyCode" with value "GBP"', function() {
        expect(pageLoadEcommerceEvent.ecommerce.currencyCode).to.equal('GBP')
    })
})

suite('GTM clickevent for PDP image carousel scroll', function() {
    let clickCarouselEvent

    it('I am on the PDP', function() {})

    it('When I click next in the image carousel', function() {
        pdp.clickNextOnImageCarousel()
        getLastGtmEventOfType('ea').then((event) => {
            clickCarouselEvent = event
        })
    })

    it('Then There is a new clickevent logged for image carousel', function() {
        expect(clickCarouselEvent.event).to.equal('clickevent')
    })

    it('And event contains a "ea" with value "next"', function() {
        expect(clickCarouselEvent.ea).to.equal('next')
    })

    it('And event contains a "ec" with value "pdp"', function() {
        expect(clickCarouselEvent.ec).to.equal('pdp')
    })

    it('And event contains a "el" with value "image carousel"', function() {
        expect(clickCarouselEvent.el).to.equal('image carousel')
    })
})

suite('GTM clickevent for find-in-store', function() {
    let findInStoreEvent
    let selectedSize
    let findStores
    let selectStore

    it('I am on the PDP', function() {})

    it('When I click the find in store button', function() {
        pdp.findInStore()
        getLastGtmEventOfType('ea').then((event) => {
            findInStoreEvent = event
        })
    })

    it('Then there is a new clickevent logged for find in store', function() {
        expect(findInStoreEvent.event).to.equal('clickevent')
    })

    it('And event contains a "ea" with value "findinstore-31600216"', function() {
        expect(findInStoreEvent.ea).to.equal('findinstore-31600216')
    })

    it('And event contains a "ec" with value "pdp"', function() {
        expect(findInStoreEvent.ec).to.equal('pdp')
    })

    it('And event contains a "el" with value of the product url', function() {
        const path = setupMocksForInitialRenderOnPdp(multiSizeProduct)
        expect(findInStoreEvent.el).contain(Cypress.config('baseUrl') + path)
    })

    it('When I select a size', function() {
        strLoc.sizeSelector('W2428')
        getLastGtmEventOfType('ea').then((event) => {
            selectedSize = event
        })
    })
    it('Then there is a new clickevent logged for selected size', function() {
        expect(selectedSize.event).to.equal('clickevent')
    })

    it('And event contains a "ea" with value "productsize-W2428"', function() {
        expect(selectedSize.ea).to.equal('productsize-W2428')
    })

    it('And event contains a "ec" with value "pdp"', function() {
        expect(selectedSize.ec).to.equal('pdp')
    })

    it('And event contains a "el" with value of the product url', function() {
        const path = setupMocksForInitialRenderOnPdp(multiSizeProduct)
        expect(selectedSize.el).contains(Cypress.config('baseUrl') + path)
    })

    it('When I search for stores in "London"', function() {
        setupMocksForInitialRenderOnPdp(multiSizeProduct)
        cy.route(
            '' + 'GET',
            routes.storeLocator,
            'fixture:general/storeLocator'
        )
        strLoc.storeLocatorSearchInPdp('London')
        strLoc.selectFirstStoreFromResultsListInPdp()
        strLoc.selectedLocationGoButtonInPdp()

        cy.filterGtmEvents({
            filter: (dlItem) => dlItem.ea === 'inStorePostcodeGo',
            timeout: 8000,
        }).then((event) => {
            findStores = event
        })
    })

    it('Then there is a new clickevent logged for selected size', function() {
        expect(findStores.event).to.equal('clickevent')
    })

    it('And event contains a "ea" with value "inStorePostcodeGo"', function() {
        expect(findStores.ea).to.equal('inStorePostcodeGo')
    })

    it('And event contains a "ec" with value "pdp"', function() {
        expect(findStores.ec).to.equal('pdp')
    })

    it('And event contains a "el" with value of the product code 31600216"', function() {
        expect(findStores.el).to.equal('31600216')
    })

    it('When I select the first store in the list', function() {
        strLoc.expandStoreDetailsFromList(3)
        cy.filterGtmEvents({
            filter: (dlItem) => dlItem.ea === 'inStorePostcodeResult',
            timeout: 8000,
        }).then((event) => {
            selectStore = event
        })
    })

    it('Then there is a new clickevent logged for selected store', function() {
        expect(selectStore.event).to.equal('clickevent')
    })

    it('And event contains a "ea" with value "inStorePostcodeResult"', function() {
        expect(selectStore.ea).to.equal('inStorePostcodeResult')
    })

    it('And event contains a "ec" with value "pdp"', function() {
        expect(selectStore.ec).to.equal('pdp')
    })

    it('And event contains a "el" with value of the product code 31600216"', function() {
        expect(selectStore.el).to.equal('31600216')
        strLoc.closeStoreProductLocatorModal()
    })
})

suite('GTM clickevent for recently viewed and recommends images', function() {
    let productImage

    it('Given I am on the PDP', function() {})

    it('When I click to load the recently viewed product image', function() {
        setupMocksForInitialRenderOnGenericPage()
        cy.route(
            'GET',
            '/api/products/7654321',
            'fixture:pdp/singleSizeProduct/pdpInitialFetch'
        )

        cy.route(
            'GET',
            '/api/products/en/tsuk/product/single-size-7654321',
            'fixture:pdp/singleSizeProduct/pdpInitialFetch'
        )
        navigation.clickRecentlyViewedImage()
        cy.filterGtmEvents({
            filter: (dlItem) => dlItem.event === 'productClick',
            timeout: 8000,
        }).then((event) => {
            productImage = event
        })
    })

    it('Then the dataLayer has a new "productClick" event', function() {
        expect(productImage.event).to.equal('productClick')
    })

    it('And event contains an "actionField" with value "PDP Recently Viewed"', function() {
        expect(productImage.ecommerce.click.actionField.list).to.equal(
            'PDP Recently Viewed'
        )
    })

    it('And event contains a "products.name" with value "Black Bucket Hat"', function() {
        expect(productImage.ecommerce.click.products[0].name).to.equal(
            'Black Bucket Hat'
        )
        setupMocksForInitialRenderOnPdp(multiSizeProduct)
        cy.go('back')
    })

    it('When I click to load the recommends product image', function() {
        setupMocksForInitialRenderOnGenericPage()
        cy.route(
            'GET',
            '/api/products/19903656',
            'fixture:pdp/peerius/peeriusRecommendsProduct'
        ),
        cy.route(
            'GET',
            '/api/products/en/tsuk/product/sweet-mint-eos-lip-balm-4399466',
            'fixture:pdp/peerius/peeriusRecommendsProduct'
        )

        navigation.clickPeeriusRecommends()
        cy.filterGtmEvents({
            filter: (dlItem) => dlItem.event === 'productClick',
            timeout: 8000,
        }).then((event) => {
            productImage = event
        })
    })

    it('Then the dataLayer has a new "productClick" event', function() {
        expect(productImage.event).to.equal('productClick')
    })

    it('And event contains an "actionField" with value "PDP Recently Viewed"', function() {
        expect(productImage.ecommerce.click.actionField.list).to.equal(
            'PDP Recommended Products'
        )
    })

    it('And event contains a "products.name" with value "Sweet Mint Eos Lip Balm"', function() {
        expect(productImage.ecommerce.click.products[0].name).to.equal(
            'Sweet Mint Eos Lip Balm'
        )
        setupMocksForInitialRenderOnPdp(multiSizeProduct)
        cy.go('back')
    })
})

isDesktopLayout() &&
    suite('GTM clickevent for desktop PDP image overlay load', function() {
        let productImage
        let path
        it('Given I am on the PDP', function() {
            path = setupMocksForInitialRenderOnPdp(multiSizeProduct)
        })

        it('When I click to load the product image overlay', function() {
            navigation.clickPDPImage()
            getLastGtmEventOfType('ea').then((event) => {
                productImage = event
            })
        })

        it('Then the dataLayer has a new "clickevent" event', function() {
            expect(productImage.event).to.equal('clickevent')
        })

        it('And event contains a "ea" with value "image overlay"', function() {
            expect(productImage.ea).to.equal('image overlay')
        })

        it('And event contains a "ec" with value "pdp"', function() {
            expect(productImage.ec).to.equal('pdp')
        })

        it('And event contains a "el" with a value of the product url', function() {
            expect(productImage.el).to.equal(path)
        })
    })
