Feature: GTM Analytics for available sizes and promotional discounts on PDP

    Scenario: Sizes available
        When I go to the PDP for "black jeans"
        Then there is a new "ecommerce" event in the dataLayer
        And it contains a "detail" object
        And the detail object contains a "products" list
        And the products list contains "sizesAvailable"
