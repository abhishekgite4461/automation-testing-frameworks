Feature: GTM Analytics for page view

    Scenario: Product Detail Page
        When I go to the PDP of a pair of "red jeans"
        Then there is a new "detail" event on the dataLayer
        And it contains an "ecommerce" object
        And the ecommerce object contains a valid currencyCode
        And the ecommerce objects contains a detail object
        And the detail object contains a "products" list
        Then there is a new "pageView" event on the dataLayer
        And the event has a "pageCategory" key of value "TS:Prod Detail"
        And the event has a "pageType" key of value "TS:Product Display"
