Feature: GTM Analytics for Click event on bundle PDP page

  Scenario: GTM Analytics for click event of size selection in bundle PDP
    Given I am on the bundle PDP
    When I select the size from the dropdown
    Then Then there is a new GTM click event logged for size selection
    And event contains a "ea" with value "clicked"
    And event contains a "ec" with value "bundle"
    And event contains a "el" with value "size selected Size 12: Low Stock"
