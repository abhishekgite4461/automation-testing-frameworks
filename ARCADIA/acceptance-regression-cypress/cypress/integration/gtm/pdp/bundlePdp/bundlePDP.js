import getLastGtmEventOfType from '../../../gtm_LEGACYPORT/helpers/getLastGtmEventOfType'
import {setupMocksForInitialRenderOnPdp, suite} from '../../../../lib/helpers'
import fixedBundlePDP from '../../../../fixtures/pdp/fixedBundle.json'
import BundlePdp from '../../../../page-objects/BundlePdp.page'

const bundlePdp = new BundlePdp()

suite(
    'GTM Analytics for click event of size selection in bundle PDP',
    function() {
        let selectBundleSizeEvent
        it('I am on the bundle PDP', function() {
            const path = setupMocksForInitialRenderOnPdp(fixedBundlePDP)
            cy.visit(path)
        })

        it('When I select the size from the dropdown', function() {
            bundlePdp.selectAvailableSizeForBundleItem(1)
            getLastGtmEventOfType('ea').then((event) => {
                selectBundleSizeEvent = event
            })
        })

        it('Then there is a new GTM click event logged for size selection', function() {
            expect(selectBundleSizeEvent.event).to.equal('clickevent')
        })

        it('And event contains a "ea" with value "clicked"', function() {
            expect(selectBundleSizeEvent.ea).to.equal('clicked')
        })

        it('And event contains a "ec" with value "bundle"', function() {
            expect(selectBundleSizeEvent.ec).to.equal('bundle')
        })

        it('And event contains a "el" with value "size selected Size 12: Low Stock"', function() {
            expect(selectBundleSizeEvent.el).to.equal(
                'selected size Size 12: Low stock'
            )
        })
    }
)
