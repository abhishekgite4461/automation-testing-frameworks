import Login from '../../../page-objects/Login.page'
import {
    setupMocksForInitialRenderOnGenericPage,
    setupClientMocksNewUserJourneyToCheckoutWithSingleSizeProd,
    suite,
} from '../../../lib/helpers'
import getLastGtmEventOfType from '../../gtm_LEGACYPORT/helpers/getLastGtmEventOfType'
import * as EventFilters from '../../gtm_LEGACYPORT/helpers/filters'
import routes from '../../../constants/routes'
import invalidLogon from '../../../fixtures/login/invalidLogon.json'
import invalidRegistration from '../../../fixtures/login/invalidRegistration.json'

const login = new Login()

suite('GTM Analytics for login success event', function() {
    let clickLoginEvent
    let loggedInSuccessUserState
    it('Given I visit the Login Page', function() {
        setupMocksForInitialRenderOnGenericPage()
        cy.visit('/login')
    })

    it('Then the dataLayer contains a pageViewEvent with value "TS:Register/Logon"', () => {
        cy.filterGtmEvents({
            filter: EventFilters.pageViewEvent,
            timeout: 15000,
        }).then((event) => {
            expect(event.pageCategory).to.equal('TS:Register/Logon')
            expect(event.pageType).to.equal('TS:Register/Logon')
        })
    })

    it('When I login as a returning user', function() {
        setupMocksForInitialRenderOnGenericPage()
        cy.server()
        cy.route(
            'POST',
            routes.login,
            'fixture:login/newUserCheckoutProfile--emptyBag.json'
        )
        cy.route('GET', routes.paymentsAll, 'fixture:checkout/payments')
        cy.route('GET', routes.getItems, 'fixture:login/shoppingBag--getItems')

        login.loginAsExistingUser()
        getLastGtmEventOfType('ea').then((event) => {
            clickLoginEvent = event
        })
    })

    it('Then the dataLayer has a new "clickevent" event', function() {
        expect(clickLoginEvent.event).to.equal('clickevent')
    })

    it('And it contains a "ea" with value "submit"', function() {
        expect(clickLoginEvent.ea).to.equal('submit')
    })

    it('And it contains a "ec" with value "signIn"', function() {
        expect(clickLoginEvent.ec).to.equal('signIn')
    })

    it('And it contains a "el" with value "sign-in-button"', function() {
        expect(clickLoginEvent.el).to.equal('sign-in-button')
    })

    it('And the dataLayer has a new "userState" event', function() {
        getLastGtmEventOfType('event', 'userState').then((event) => {
            loggedInSuccessUserState = event
        })
    })

    it('And it contains a "loggedIn" with value "True', function() {
        expect(loggedInSuccessUserState.user.loggedIn).to.equal('True')
    })

    it('And it contains a "ddpUser" with value "False', function() {
        expect(loggedInSuccessUserState.user.ddpUser).to.equal('False')
    })
})

suite('GTM Analytics for registration success event', function() {
    let clickRegisterEvent
    let registerSuccessUserStateEvent
    it('Given I visit the Login Page', function() {
        setupMocksForInitialRenderOnGenericPage()
        cy.visit('/login')
    })

    it('When I register as a new user', function() {
        setupClientMocksNewUserJourneyToCheckoutWithSingleSizeProd()
        setupMocksForInitialRenderOnGenericPage()
        login.registerAsNewUser()
        getLastGtmEventOfType('ea').then((event) => {
            clickRegisterEvent = event
        })
    })

    it('Then the dataLayer has a new "clickevent" event', function() {
        expect(clickRegisterEvent.event).to.equal('clickevent')
    })

    it('And it contains a "ea" with value "submit"', function() {
        expect(clickRegisterEvent.ea).to.equal('submit')
    })

    it('And it contains a "ec" with value "register"', function() {
        expect(clickRegisterEvent.ec).to.equal('register')
    })

    it('And it contains a "el" with value "register-in-button"', function() {
        expect(clickRegisterEvent.el).to.equal('register-in-button')
    })

    it('And the dataLayer has a new "userState" event', function() {
        getLastGtmEventOfType('event', 'userState').then((event) => {
            registerSuccessUserStateEvent = event
        })
    })

    it('And it contains a "loggedIn" with value "True', function() {
        expect(registerSuccessUserStateEvent.user.loggedIn).to.equal('True')
    })

    it('And it contains a "ddpUser" with value "False', function() {
        expect(registerSuccessUserStateEvent.user.ddpUser).to.equal('False')
    })
})

suite('GTM Analytics for registration failure event', function() {
    let clickRegistrationFailureEvent
    let registerFailureUserStateEvent
    it('Given I visit the Login Page', function() {
        setupMocksForInitialRenderOnGenericPage()
        cy.visit('/login')
    })

    it('When I attempt to register with an existing profile', function() {
        setupMocksForInitialRenderOnGenericPage()
        cy.server()
        cy.route({
            method: 'POST',
            url: routes.registerNewUser,
            response: invalidRegistration,
            status: 422,
        })
        login.registerAsNewUser()
        getLastGtmEventOfType('errorMessage').then((event) => {
            clickRegistrationFailureEvent = event
        })
    })

    it('Then the dataLayer has a new "errorMessage" event', function() {
        expect(clickRegistrationFailureEvent.event).to.equal('errorMessage')
    })

    it('And it contains a "errorMessage" with value "User registration failed"', function() {
        expect(clickRegistrationFailureEvent.errorMessage).to.equal(
            'User registration failed'
        )
    })

    it('And the dataLayer has a new "userState" event', function() {
        getLastGtmEventOfType('event', 'userState').then((event) => {
            registerFailureUserStateEvent = event
        })
    })

    it('And it contains a "loggedIn" with value "False', function() {
        expect(registerFailureUserStateEvent.user.loggedIn).to.equal('False')
    })
})

suite('GTM Analytics for login failure event', function() {
    let clickLogonFailureEvent
    let logonFailureUserStateEvent
    it('Given I visit the Login Page', function() {
        setupMocksForInitialRenderOnGenericPage()
        cy.visit('/login')
    })

    it('When I attempt to login with an invalid profile', function() {
        setupMocksForInitialRenderOnGenericPage()
        cy.server()
        cy.route({
            method: 'POST',
            url: routes.login,
            response: invalidLogon,
            status: 422,
        })
        login.loginAsExistingUser()
        getLastGtmEventOfType('errorMessage').then((event) => {
            clickLogonFailureEvent = event
        })
    })

    it('Then the dataLayer has a new "errorMessage" event', function() {
        expect(clickLogonFailureEvent.event).to.equal('errorMessage')
    })

    it('And it contains a "errorMessage" with value "User login failed"', function() {
        expect(clickLogonFailureEvent.errorMessage).to.equal(
            'User login failed'
        )
    })

    it('And the dataLayer has a new "userState" event', function() {
        getLastGtmEventOfType('event', 'userState').then((event) => {
            logonFailureUserStateEvent = event
        })
    })

    it('And it contains a "loggedIn" with value "False', function() {
        expect(logonFailureUserStateEvent.user.loggedIn).to.equal('False')
    })
})
