Feature: GTM Analytics for DDP User State

  Scenario: GTM Analytics for ddpUser state for non-authenticated non-DDP user
    Given I am a non-authenticated user
    And I do not have DDP subscription
    When I navigate through the website
    Then the datalayer has "userState" event
    And event does not contain property "ddpUser"

  Scenario: GTM Analytics for ddpUser state for authenticated non-DDP user
    Given I am an authenticated user
    And I do not have DDP subscription
    When I login
    Then the datalayer has "userState" event
    And event contains "ddpUser" with value "False"

  Scenario: GTM Analytis for ddpUser state for authenticated DDP user
    Given I am an authenticated user
    And I have DDP subscription
    When I login
    Then the datalayer has "userState" event
    And event contains "ddpUser" with value "True"