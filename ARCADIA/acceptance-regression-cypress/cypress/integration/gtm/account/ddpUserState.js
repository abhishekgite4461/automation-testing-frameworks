import {
    setupMocksForInitialRenderOnGenericPage,
    setFeature,
    suite,
} from './../../../lib/helpers'
import routes from './../../../constants/routes'
import Login from './../../../page-objects/Login.page'
import * as EventFilters from './../../gtm_LEGACYPORT/helpers/filters'

const login = new Login()

suite(
    'GTM Analytics for ddpUser state for non-authenticated non-DDP user',
    () => {
        let userStatusEvent
        it('Given I am a non-authenticated user', () => {})
        it('And I do not have DDP subscription', () => {
            setupMocksForInitialRenderOnGenericPage()
            cy.visit('/')
            setFeature('FEATURE_DDP')
        })

        it('When I navigate through the website', () => {
            cy.filterGtmEvents({
                filter: EventFilters.userStatus,
                timeout: 15000,
            }).then((userStatus) => {
                userStatusEvent = userStatus
            })
        })
        it('Then the datalayer has "userState" event', () => {
            expect(userStatusEvent.event).to.equal('userState')
        })
        it('And event does not contain property "ddpUser"', () => {
            expect(JSON.stringify(userStatusEvent.user)).to.not.contain(
                'ddpUser'
            )
        })
    }
)

suite('GTM Analytics for ddpUser state for authenticated non-DDP user', () => {
    let userStatusEvent
    it('Given I am an authenticated user', () => {
        setupMocksForInitialRenderOnGenericPage()
        cy.visit('/login', {failOnStatusCode: false})
    })
    it('And I do not have DDP subscription', () => {
        setFeature('FEATURE_DDP')
    })
    it('When I login', () => {
        setupMocksForInitialRenderOnGenericPage()
        cy.server()
        cy.route(
            'GET',
            routes.paymentsDeliveryUkBillingUk,
            'fixture:login/payments--ukBillingUkDelivery'
        )
        cy.route('GET', routes.getItems, 'fixture:login/shoppingBag--getItems')
        cy.route(
            'POST',
            routes.login,
            'fixture:login/fullCheckoutProfile--emptyBag'
        ).as('loginCompleted')
        login.loginAsExistingUser()
        cy.wait('@loginCompleted').then(() => {
            cy.filterGtmEvents({
                filter: EventFilters.userStatus,
                timeout: 30000,
            }).then((userStatus) => {
                userStatusEvent = userStatus
            })
        })
    })
    it('Then the datalayer has "userState" event', () => {
        expect(userStatusEvent.event).to.equal('userState')
    })
    it('And event contains "ddpUser" with value "False"', () => {
        expect(userStatusEvent.user.ddpUser).to.equal('False')
    })
})

suite('GTM Analytis for ddpUser state for authenticated DDP user', () => {
    let userStatusEvent

    it('Given I am an authenticated user', () => {
        setupMocksForInitialRenderOnGenericPage()
        cy.visit('/login', {failOnStatusCode: false})
    })
    it('And I have DDP subscription', () => {
        setFeature('FEATURE_DDP')
    })
    it('When I login', () => {
        setupMocksForInitialRenderOnGenericPage()
        cy.server()
        cy.route(
            'GET',
            routes.paymentsDeliveryUkBillingUk,
            'fixture:login/payments--ukBillingUkDelivery'
        )
        cy.route('GET', routes.getItems, 'fixture:login/shoppingBag--getItems')
        cy.route('POST', routes.login, 'fixture:login/fullCheckoutProfile').as(
            'loginCompleted'
        )
        login.loginAsExistingUser()
        cy.wait('@loginCompleted').then(() => {
            cy.filterGtmEvents({
                filter: EventFilters.userStatus,
                timeout: 30000,
            }).then((userStatus) => {
                userStatusEvent = userStatus
            })
        })
    })
    it('Then the datalayer has "userState" event', () => {
        expect(userStatusEvent.event).to.equal('userState')
    })
    it('And event contains "ddpUser" with value "True"', () => {
        expect(userStatusEvent.user.ddpUser).to.equal('True')
    })
})
