Feature: GTM Analytics for Login Click Events

  Scenario: Click event for user login success
    Given I visit Login Page
    When I log in as a returning user
    Then the dataLayer has a new "clickevent" event
    And it contains a "ea" with value "submit"
    And it contains a "ec" with value "signIn"
    And it contains a "el" with value "sign-in-button"

  Scenario: Click event for user login failure
    Given I visit Login Page
    When I attempt to login with an invalid password
    Then the dataLayer has a new "errorMessage" event
    And it contains a "errorMessage" with value "User login failed"
