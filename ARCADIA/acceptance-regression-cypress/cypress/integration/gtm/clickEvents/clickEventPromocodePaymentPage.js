import '../../gtm_LEGACYPORT/helpers/tcombSchemaExtensions'
import singleSizeProduct from './../../../fixtures/pdp/singleSizeProduct/pdpInitialFetch'
import {
    setupMocksForInitialRenderOnGenericPage,
    setupMocksForInitialRenderOnPdp,
    suite,
} from '../../../lib/helpers'
import routes from '../../../constants/routes'
import ProductDetailPage from '../../../page-objects/Pdp.page'
import MiniBag from '../../../page-objects/MiniBag.page'
import PromoCodePage from '../../../page-objects/PromoCode.page'
import CheckoutLogin from '../../../page-objects/CheckoutLogin.page'
import DeliveryPage from '../../../page-objects/Delivery.page'
import getLastGtmEventOfType from '../../gtm_LEGACYPORT/helpers/getLastGtmEventOfType'
import {promoCode} from '../../../constants/promoCode'

const pdp = new ProductDetailPage()
const miniBag = new MiniBag()
const checkoutLogin = new CheckoutLogin()
const deliveryPage = new DeliveryPage()
const promoCodePage = new PromoCodePage()

suite('GTM Analytics when Valid promo-code is applied on payment page', () => {
    let clickValidPromoCodeEvent
    it('Given I am logged in and I am on payment page', () => {
        cy.server()
        cy.route(
            'POST',
            routes.addItemToBag,
            'fixture:pdp/singleSizeProduct/addToBag'
        )
        cy.route(
            'GET',
            routes.checkoutOrderSummary,
            'fixture:checkout/order_summary---newUserSingleSizeProd'
        )
        cy.route('GET', routes.paymentsAll, 'fixture:checkout/payments')

        const path = setupMocksForInitialRenderOnPdp(singleSizeProduct)
        cy.visit(path)
        pdp.addToBag()
        pdp.viewBagFromConfirmationModal()
        miniBag.moveToCheckout()

        cy.route('POST', routes.registerNewUser, 'fixture:checkout/register')

        checkoutLogin.registerAsNewUser()

        deliveryPage.enterDeliveryDetailsManually()
    })

    it('When I apply a valid promo-code on payment page', function() {
        setupMocksForInitialRenderOnGenericPage()
        cy.route(
            'POST',
            routes.addPromotionCode,
            'fixture:payment/addPromoCodeOnPaymentsPage'
        )
        cy.route(
            'GET',
            routes.checkoutOrderSummary,
            'fixture:checkout/order_summary---newUserSingleSizeProd'
        )
        cy.route('GET', routes.account, 'fixture:checkout/account--newUser')
        promoCodePage.enterPromoCodeAndApply(promoCode.tsPromo)
        promoCodePage.verifyPromoCodeApplied(promoCode.tsPromo).then(() => {
            getLastGtmEventOfType('event').then((event) => {
                clickValidPromoCodeEvent = event
            })
        })
    })

    it('Then The dataLayer has a new "clickevent" event', function() {
        expect(clickValidPromoCodeEvent.event).to.equal('clickevent')
    })

    it('And event contains a "ea" with value "promoCodeApplied"', function() {
        expect(clickValidPromoCodeEvent.ea).to.equal('promoCodeApplied')
    })

    it('And event contains a "ec" with value "checkout"', function() {
        expect(clickValidPromoCodeEvent.ec).to.equal('checkout')
    })
})

// suite(
//     'GTM Analytics when Invalid promo-code is applied on payment page',
//     () => {
//         let clickInvalidPromoCodeEvent
//         it('Given I am logged in and I am on payment page', () => {})

//         it('When I apply a invalid promo-code on payment page', function() {
//             setupMocksForInitialRenderOnGenericPage()
//             cy.server()
//             cy.route({
//                 method: 'POST',
//                 url: routes.addPromotionCode,
//                 response: 'fixture:payment/failedPromoCodeOnPaymentsPage',
//                 status: 422,
//             })
//             promoCodePage.typePromoCodeAndApply('XXXXXXX')
//             promoCodePage.verifyPromoCodeErrorDisplayed().then(() => {
//                 getLastGtmEventOfType('event').then((event) => {
//                     clickInvalidPromoCodeEvent = event
//                 })
//             })
//         })

//         it('Then The dataLayer has a new "errorMessage" event', function() {
//             expect(clickInvalidPromoCodeEvent.event).to.equal('errorMessage')
//         })

//         it('And event contains a "errorMessage" with value "Error applying promo code in shopping bag."', function() {
//             expect(clickInvalidPromoCodeEvent.errorMessage).to.equal(
//                 'Error applying promo code in shopping bag.'
//             )
//         })
//     }
// )
