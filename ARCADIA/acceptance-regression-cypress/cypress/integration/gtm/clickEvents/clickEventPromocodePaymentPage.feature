Feature: GTM Analytics for promo-code in payment page


  Scenario: GTM Analytics when Valid promo-code is applied on payment page
    Given I am logged in and I am on payment page
    When I apply a valid promo-code on payment page
    Then The dataLayer has a new "clickevent" event
    And event contains a "ea" with value "promoCodeApplied"
    And event contains a "ec" with value "checkout"

  Scenario: GTM Analytics when Invalid promo-code is applied on payment page
    Given I am logged in and I am on payment page
    When I apply a invalid promo-code on payment page
    Then The dataLayer has a new "errorMessage" event
    And event contains a "errorMessage" with value "Error applying promo code in shopping bag."

