import getLastGtmEventOfType from '../../gtm_LEGACYPORT/helpers/getLastGtmEventOfType'
import Navigation from '../../../page-objects/Navigation.page'
import {
    setupMocksForInitialRenderOnGenericPage,
    isMobileLayout,
    suite,
} from '../../../lib/helpers'
import routes from './../../../constants/routes'

const navigation = new Navigation()

suite('GTM clickevent for search bar', function() {
    let clickSearchBarEvent
    it('Given I visit the home page', function() {
        setupMocksForInitialRenderOnGenericPage()
        cy.visit('/')
    })

    it('When I click on the search bar', function() {
        navigation.clickSearchIcon()
        getLastGtmEventOfType('ea').then((event) => {
            clickSearchBarEvent = event
        })
    })

    it('Then The dataLayer has a new "clickevent" event', function() {
        expect(clickSearchBarEvent.event).to.equal('clickevent')
    })

    it('And event contains a "ea" with value "searchbar"', function() {
        expect(clickSearchBarEvent.ea).to.equal('searchbar')
    })

    it('And event contains a "ec" with value "searchSelected"', function() {
        expect(clickSearchBarEvent.ec).to.equal('searchSelected')
    })

    it('And event contains a "el" with value "{baseUrl}/"', function() {
        expect(clickSearchBarEvent.el).to.equal(Cypress.config('baseUrl') + '/')
    })
})

isMobileLayout() &&
    suite('GTM clickevent for (no results) search again bar', function() {
        let clickSearchBarEvent
        const searchTerm = 'look for this'

        it('Given I visit the mobile home page', function() {
            setupMocksForInitialRenderOnGenericPage()
            cy.visit('/')
        })
        it('When I click on the search bar', function() {
            navigation.clickSearchIcon()
            getLastGtmEventOfType('ea').then((event) => {
                clickSearchBarEvent = event
            })
        })
        it('And I enter a search term that returns no results', function() {
            cy.server()
            setupMocksForInitialRenderOnGenericPage()
            cy.route(
                'GET',
                routes.productSearch(searchTerm),
                'fixture:plp/no-results'
            )
            navigation.typeTermIntoSearchInput(searchTerm)
            navigation.clickPerformSearch()
            navigation.clickNoResultsSearchButton()
            getLastGtmEventOfType('ea').then((event) => {
                clickSearchBarEvent = event
            })
        })
        it('Then the dataLayer has a new "clickevent" event', function() {
            expect(clickSearchBarEvent.event).to.equal('clickevent')
        })
        it('And event contains a "ea" with value "searchbar"', function() {
            expect(clickSearchBarEvent.ea).to.equal('searchbar')
        })
        it('And event contains a "ec" with value "searchSelected"', function() {
            expect(clickSearchBarEvent.ec).to.equal('searchSelected')
        })
        it('And event contains a "el" with value "{baseUrl}/search/?q=my+search+term"', function() {
            const searchTermWithPlusses = searchTerm.replace(
                new RegExp(' ', 'g'),
                '+'
            )
            expect(clickSearchBarEvent.el).to.equal(
                Cypress.config('baseUrl') +
                    '/search/?q=' +
                    searchTermWithPlusses
            )
        })
    })
