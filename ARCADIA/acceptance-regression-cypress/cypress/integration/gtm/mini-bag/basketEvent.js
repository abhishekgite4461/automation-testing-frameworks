import '../../gtm_LEGACYPORT/helpers/tcombSchemaExtensions'
import * as Schemas from '../../gtm_LEGACYPORT/helpers/schemas'
import ProductDetailPage from '../../../page-objects/Pdp.page'
import MiniBag from '../../../page-objects/MiniBag.page'
import PromoCodePage from '../../../page-objects/PromoCode.page'
import Navigation from '../../../page-objects/Navigation.page'
import {
    setupMocksForInitialRenderOnPdp,
    setupMocksForInitialRenderOnGenericPage,
    setupMocksForCheckout,
    suite,
} from '../../../lib/helpers'
import singleSizeProduct from '../../../fixtures/pdp/singleSizeProduct/pdpInitialFetch'
import * as EventFilters from '../../gtm_LEGACYPORT/helpers/filters'
import routes from '../../../constants/routes'
import singleSizeProductAddToBag from '../../../fixtures/pdp/singleSizeProduct/addToBag'
import getLastGtmEventOfType from '../../gtm_LEGACYPORT/helpers/getLastGtmEventOfType'
import {promoCode} from '../../../constants/promoCode'
import multiSizeProduct from '../../../fixtures/pdp/multiSizeProduct/pdpInitialFetch'
import multiSizeProductAddToBag from '../../../fixtures/pdp/multiSizeProduct/multiSizeAddToBag'
import orderSummary from '../../../fixtures/checkout/order_summary---newUserSingleSizeProd.json'
import customerAccount from '../../../fixtures/checkout/account--newUser.json'

const pdp = new ProductDetailPage()
const miniBag = new MiniBag()
const navigation = new Navigation()
const promo = new PromoCodePage()

suite('GTM analytics for add to bag events', () => {
    let bagDrawerEvent
    let addToBagClickEvent
    let addToBasketEvent
    let path

    it('Given I am on a PDP', () => {
        path = setupMocksForInitialRenderOnPdp(singleSizeProduct)
        cy.visit(path)
    })

    it('When I add an item to the basket', () => {
        cy.server()
        cy.route(
            'POST',
            routes.addItemToBag,
            'fixture:pdp/singleSizeProduct/addToBag'
        )
        pdp.addToBag()
    })

    it('Then the dataLayer has a new a "clickevent" event', function() {
        getLastGtmEventOfType('ea').then((event) => {
            addToBagClickEvent = event
            expect(addToBagClickEvent.ea).to.equal('addtobag')
        })
    })

    it('And it has a "ea" with value "addtobag"', function() {
        expect(addToBagClickEvent.ea).to.equal('addtobag')
    })

    it('And it has a "ec" with value "pdp"', function() {
        expect(addToBagClickEvent.ec).to.equal('pdp')
    })

    it('And it has a "el containing the product url', function() {
        expect(addToBagClickEvent.el).to.equal(Cypress.config('baseUrl') + path)
    })

    it('And it creates a new ecommerce event with value "addToBasket"', function() {
        setupMocksForInitialRenderOnPdp(singleSizeProduct)
        cy.filterGtmEvents({
            filter: EventFilters.addToBasketEvent,
            timeout: 8000,
        }).then((event) => {
            addToBasketEvent = event
            expect(event).to.be.tcombSchema(Schemas.AddToBasket)
        })
    })

    it('And it has a "quantity" add value equal to "1"', function() {
        expect(addToBasketEvent.ecommerce.add.products[0].quantity).to.equal(
            '1'
        )
    })

    it('And there is a new "bagDrawerDisplayed" event"', function() {
        pdp.viewBagFromConfirmationModal()
        cy.filterGtmEvents({
            filter: (dlItem) => dlItem.bagDrawerTrigger,
            timeout: 2000,
        }).then((event) => {
            bagDrawerEvent = event
            expect(bagDrawerEvent.event).to.equal('bagDrawerDisplayed')
        })
    })

    it('And it has a "bagDrawerTrigger" with value "add to bag', function() {
        expect(bagDrawerEvent.bagDrawerTrigger).to.equal('add to bag')
    })
})

suite('GTM analytics for increase basket quantity', () => {
    let editMiniBagProductButton

    it('When I Increase item quantity to 3', () => {
        setupMocksForInitialRenderOnGenericPage()
        cy.server()
        cy.route(
            'GET',
            `${routes.miniBagFetchSizesAndQuants}?catEntryId=${
                singleSizeProductAddToBag.products[0].catEntryId
            }`,
            'fixture:pdp/singleSizeProduct/fetchSizesAndQuantities'
        )
        cy.route(
            'PUT',
            routes.miniBagUpdateItem,
            'fixture:pdp/singleSizeProduct/updateQuantityTo3'
        )
        miniBag.editAndChangeQuantityForProductNumberTo(1, '3')
        miniBag.saveChanges()
    })

    it('Then the dataLayer has a new a "clickevent" event', function() {
        getLastGtmEventOfType('ea').then((event) => {
            editMiniBagProductButton = event
            expect(editMiniBagProductButton.event).to.equal('clickevent')
        })
    })

    it('And it has a "ea" with value "bagDrawEdit"', function() {
        expect(editMiniBagProductButton.ea).to.equal('bagDrawEdit')
    })

    it('And it has a "ec" with value "bagDrawer"', function() {
        expect(editMiniBagProductButton.ec).to.equal('bagDrawer')
    })

    it('And it has a "el containing productid', function() {
        expect(editMiniBagProductButton.el).to.equal(
            singleSizeProduct.productId
        )
    })

    it('And it creates a new "ecommerce" event with value "addToBasket"', function() {
        setupMocksForInitialRenderOnGenericPage()
        getLastGtmEventOfType('ecommerce').then((event) => {
            editMiniBagProductButton = event
            expect(editMiniBagProductButton.event).to.equal('addToBasket')
        })
    })

    it('And it has a "quantity" add remove value equal to "2"', function() {
        expect(
            editMiniBagProductButton.ecommerce.add.products[0].quantity
        ).to.equal('2')
    })
})

suite('GTM analytics for decreasing basket quantity', () => {
    let decreaseBag

    it('When I decrease the item quantity from 3 to 1', () => {
        setupMocksForInitialRenderOnGenericPage()
        cy.server()
        cy.route(
            'GET',
            `${routes.miniBagFetchSizesAndQuants}?catEntryId=${
                singleSizeProductAddToBag.products[0].catEntryId
            }`,
            'fixture:pdp/singleSizeProduct/fetchSizesAndQuantities'
        )
        cy.route(
            'PUT',
            routes.miniBagUpdateItem,
            'fixture:pdp/singleSizeProduct/updateQuantityTo1'
        )
        miniBag.editAndChangeQuantityForProductNumberTo(1, '1')
        miniBag.saveChanges()
    })

    it('Then it creates a new ecommerce event with value "removeFromBasket"', function() {
        setupMocksForInitialRenderOnGenericPage()
        getLastGtmEventOfType('ecommerce').then((event) => {
            decreaseBag = event
            expect(decreaseBag.event).to.equal('removeFromBasket')
        })
    })

    it('And it has a "quantity" remove value equal to "2"', function() {
        expect(decreaseBag.ecommerce.remove.products[0].quantity).to.equal('2')
    })
})

suite('GTM click event for promo code apply', () => {
    let clickValidPromoCodeEvent

    it('Given I have opened the mini-bag with an item in my basket', () => {})

    it('When I apply a valid promo code', () => {
        setupMocksForInitialRenderOnGenericPage()
        cy.server()
        cy.route(
            'POST',
            routes.addPromotionCode,
            'fixture:minibag/addPromoCodeOnMinibag'
        ).as('addingPromoCode')
        promo.enterPromoCodeAndApply(promoCode.tsPromo)
        cy.wait('@addingPromoCode').then(() => {
            cy.get(promo.promoCodeConfirmation).then(() => {
                getLastGtmEventOfType('ec').then((event) => {
                    clickValidPromoCodeEvent = event
                })
            })
        })
    })

    it('Then the dataLayer has a new a "clickevent" event', function() {
        expect(clickValidPromoCodeEvent.event).to.equal('clickevent')
    })

    it('And it has a "ea" with value "promoCodeApplied"', function() {
        expect(clickValidPromoCodeEvent.ea).to.equal('promoCodeApplied')
    })

    it('And it has a "ec" with value "shoppingBag"', function() {
        expect(clickValidPromoCodeEvent.ec).to.equal('shoppingBag')
    })

    it('And it has a "el containing the promo code entered', function() {
        expect(clickValidPromoCodeEvent.el).to.equal(promoCode.tsPromo)
    })
})

suite('GTM click event for invalid promo code apply', () => {
    let clickInvalidPromoCodeEvent

    it('Given I have opened the mini-bag with an item in my basket', () => {})

    it('When I apply an invalid promo code', () => {
        setupMocksForInitialRenderOnPdp(singleSizeProduct)
        cy.server()
        cy.route({
            method: 'POST',
            url: routes.addPromotionCode,
            response: 'fixture:payment/failedPromoCodeOnPaymentsPage',
            status: 422,
        }).as('addingPromoCode')
        promo.enterAnotherPromoCodeAndApply('XXXXXXXX')
        cy.wait('@addingPromoCode').then(() => {
            cy.get(promo.promoCodeConfirmation).then(() => {
                getLastGtmEventOfType('event', 'errorMessage').then((event) => {
                    clickInvalidPromoCodeEvent = event
                })
            })
        })
    })

    it('Then the dataLayer has a new a "errorMessage" event', function() {
        expect(clickInvalidPromoCodeEvent.event).to.equal('errorMessage')
    })

    it('And it has a "errorMessage" with value "Error applying promo code in shopping bag."', function() {
        expect(clickInvalidPromoCodeEvent.errorMessage).to.equal(
            'Error applying promo code in shopping bag.'
        )
    })
})

suite('GTM analytics for removing a basket item', () => {
    let removeFromBag

    it('Removing item from basket creates a new ecommerce event with value "removeFromBasket"', () => {
        setupMocksForInitialRenderOnGenericPage()
        cy.server()
        cy.route(
            'DELETE',
            `${routes.miniBagRemoveItem}?orderId=${
                singleSizeProductAddToBag.orderId
            }&orderItemId=${singleSizeProductAddToBag.products[0].orderItemId}`,
            'fixture:pdp/singleSizeProduct/removeFromBasket'
        )
        miniBag.removeProductFromBasketWithIndex(0)
    })

    it('Then it creates a new ecommerce event with value "removeFromBasket"', function() {
        setupMocksForInitialRenderOnGenericPage()
        getLastGtmEventOfType('ecommerce').then((event) => {
            removeFromBag = event
            expect(removeFromBag.event).to.equal('removeFromBasket')
        })
    })

    it('And it has a "quantity" value equal to "1"', function() {
        expect(removeFromBag.ecommerce.remove.products[0].quantity).to.equal(
            '1'
        )
        miniBag.continueShopping()
    })
})

suite('GTM analytics for opening the mini-bag from navigation bar', () => {
    let miniBagIcon

    it('Clicking the mini-bag records a new "bagDrawerDisplayed" event with value "bag icon', function() {
        navigation.openMiniBag()
        getLastGtmEventOfType('bagDrawerTrigger').then((event) => {
            miniBagIcon = event
        })
    })

    it('And it contains a "event" with the value "bagDrawerDisplayed"', function() {
        expect(miniBagIcon.event).to.equal('bagDrawerDisplayed')
    })

    it('And it contains a "bagDrawerTrigger" with the value "bag icon"', function() {
        expect(miniBagIcon.bagDrawerTrigger).to.equal('bag icon')
        miniBag.continueShopping()
    })
})

suite('GTM Analytics Updated size in Mini Bag', function() {
    let changeSizeEvent
    let closeMiniBagEvent

    it('Given I have a product added in Mini Bag', function() {
        const path = setupMocksForInitialRenderOnPdp(multiSizeProduct)
        cy.visit(path)
        pdp.selectFirstSizeFromTiles()
        cy.server()
        cy.route(
            'POST',
            routes.addItemToBag,
            'fixture:pdp/multiSizeProduct/multiSizeAddToBag'
        )
        pdp.addToBag()
    })

    it('When I edit the size of product in the Mini Bag', function() {
        setupMocksForInitialRenderOnGenericPage()
        pdp.viewBagFromConfirmationModal()
        cy.server()
        cy.route(
            'GET',
            `${routes.miniBagFetchSizesAndQuants}?catEntryId=${
                multiSizeProductAddToBag.products[0].catEntryId
            }`,
            'fixture:pdp/multiSizeProduct/multiSizeFetchItemAndSizes'
        )
        miniBag.editAndChangeSizeForProductNumberTo(1, 'W2528').then(() => {
            getLastGtmEventOfType('ea').then((event) => {
                changeSizeEvent = event
            })
        })
    })

    it('Then the dataLayer has a new "clickevent" event', function() {
        expect(changeSizeEvent.event).to.equal('clickevent')
    })

    it('And event contains a "ea" with value "sizeUpdate"', function() {
        expect(changeSizeEvent.ea).to.equal('sizeUpdate')
    })

    it('And event contains a "ec" with value "shoppingBag"', function() {
        expect(changeSizeEvent.ec).to.equal('shoppingBag')
    })

    it('When I select close button in minibag', function() {
        miniBag.miniBagClose()
    })

    it('Then the dataLayer has a new "clickevent" event', function() {
        getLastGtmEventOfType('event').then((event) => {
            closeMiniBagEvent = event
        })

        it('And event contains a "el" with value "close-button"', function() {
            expect(closeMiniBagEvent.ea).to.equal('close-button')
        })

        it('And event contains a "ea" with value "clicked"', function() {
            expect(closeMiniBagEvent.ea).to.equal('clicked')
        })
    })
})

suite('GTM analytics to capture checkout action step', () => {
    it('When I  open the mini bag and continue to checkout', () => {
        setupMocksForCheckout(orderSummary, customerAccount)
        navigation.openMiniBag()
        miniBag.moveToCheckout()
    })

    it('The dataLayer contains a new ecommerce actionField event with the value "CheckoutStep1"', () => {
        cy.filterGtmEvents({
            filter: EventFilters.ecommerceCheckoutStep(1),
            timeout: 15000,
            name: 'CheckoutStep1',
        }).then((event) => {
            expect(event).to.be.tcombSchema(Schemas.CheckoutStep1)
        })
    })
})
