Feature: PDP flexible bundle

    Scenario: PDP flexible bundle
        Given I am on a "flexible bundle" page
        And there are two products on the page
        And there are "2" Add To Bag buttons
        And I select an available size for bundle item number "1"
        When I select Add To Bag for product number "1"
        And I choose View Bag from the mobile only confirmation modal
        Then I see "1" products in the minibag
