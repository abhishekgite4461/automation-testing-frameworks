Feature: PDP size selection options

Scenario: PDP with size tiles
Given I am on a PDP with size tiles
And there are less than 9 sizes
Then the sizes will be displayed as tiles

Scenario: PDP with a dropdown
Given I am on a PDP with a dropdown size selection
And there are more than 8 sizes
Then the sizes will be displayed in a dropdown

Scenario: addToBag with no size selected shows an error message
Given I am on a PDP with no size selected
And I click AddToBag
Then I should see a "select a size" error message


