Feature: PDP fixed bundle

    Scenario: PDP fixed bundle
        Given I am on a "fixed bundle" page
        And there are two products on the page
        And there is "1" Add To Bag button
        And I select an available size for bundle item number "1"
        And I select Add To Bag
        And I should see an error that I also have to select a size for bundle item number "2"
        And I select an available size for bundle item number "2"
        And I select Add To Bag
        And I choose View Bag from the mobile only confirmation modal
        And I see "2" products in the minibag
        When I change the size of item number "1" in the minibag
        Then I see that item number "1" has a different size than it had before
        And item number "1" has a different size than item number "2"
