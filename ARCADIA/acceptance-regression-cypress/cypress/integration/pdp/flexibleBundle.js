import {setupMocksForInitialRenderOnPdp, suite} from './../../lib/helpers'
import flexibleBundle from './../../fixtures/pdp/flexibleBundle.json'
import routes from './../../constants/routes'
import BundlePdp from '../../page-objects/BundlePdp.page'
import MiniBag from './../../page-objects/MiniBag.page'

const bundlePdp = new BundlePdp()
const miniBag = new MiniBag()

suite('PDP flexible bundle', function() {
    it("Given I am on a 'flexible bundle' page", function() {
        const path = setupMocksForInitialRenderOnPdp(flexibleBundle)
        cy.visit(path)
    })

    it('And there are two products on the page', function() {
        bundlePdp.assertNumberOfProducts(2)
    })

    it("And there are '2' Add To Bag buttons", function() {
        bundlePdp.assertNumberOfAddToBagButtons(2)
    })

    it("And I select an available size for bundle item number '1'", function() {
        bundlePdp.selectAvailableSizeForBundleItem(1)
    })

    it("When I select Add To Bag for product number '1'", function() {
        cy.server()
        cy.route(
            'POST',
            routes.addItemToBag,
            'fixture:pdp/flexibleBundleAddToBag'
        )
        cy.get(bundlePdp.addToBagButton)
            .eq(0)
            .click()
    })

    it('And I choose View Bag from the mobile only confirmation modal', function() {
        bundlePdp.viewBagFromConfirmationModal()
    })

    it("Then I see '1' product in the minibag", function() {
        miniBag.expectNumberOfProductsToBe(1)
    })
})
