import {setupMocksForInitialRenderOnPdp, suite} from './../../lib/helpers'
import PDP from './../../page-objects/Pdp.page'
import TileSizePDP from './../../fixtures/pdp/tileSizePDP.json'
import DropdownPDP from './../../fixtures/pdp/dropdownSizeSelectionPDP.json'

const pdp = new PDP()

suite('PDP size selection options: PDP with size tiles', () => {
    it('Given I am on a PDP with size tiles', function() {
        const path = setupMocksForInitialRenderOnPdp(TileSizePDP)
        cy.visit(path)
    })

    it('And there are less than 8 sizes', function() {
        pdp.verifySizeOptionsLessThan(8)
    })

    it('Then the sizes will be displayed as tiles', function() {
        cy.get(pdp.tileSizes).should('be.visible')
    })
})

suite('PDP size selection options: PDP with a dropdown', function() {
    it('Given I am on a PDP with a dropdown size selection', function() {
        const path = setupMocksForInitialRenderOnPdp(DropdownPDP)
        cy.visit(path)
    })

    it('And there are more than 8 sizes', function() {
        pdp.verifySizeOptionsMoreThan(8)
    })

    it('Then the sizes will be displayed in a dropdown', function() {
        cy.get(pdp.sizeSelectionDropdown).should('be.visible')
    })
})

suite(
    'PDP size selection options: addToBag with no size selected shows an error message',
    function() {
        it('Given I am on a PDP with no size selected', function() {
            const path = setupMocksForInitialRenderOnPdp(DropdownPDP)
            cy.visit(path)
        })

        it('And I click AddToBag', function() {
            pdp.addToBag()
        })

        it('Then I should see a "select a size" error message', function() {
            cy.get(pdp.errorMessage).should('be.visible')
        })
    }
)
