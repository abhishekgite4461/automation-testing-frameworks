import {setupMocksForInitialRenderOnPdp, suite} from './../../lib/helpers'
import routes from './../../constants/routes'
import MiniBag from './../../page-objects/MiniBag.page.js'
import BundlePdp from './../../page-objects/BundlePdp.page'
import fixedBundlePDP from './../../fixtures/pdp/fixedBundle.json'
import fixedBundleAddToBag from './../../fixtures/pdp/fixedBundleAddToBag.json'

const bundlePdp = new BundlePdp()
const miniBag = new MiniBag()

suite('PDP fixed bundle', function() {
    it("Given I am on a 'fixed bundle' page", function() {
        const path = setupMocksForInitialRenderOnPdp(fixedBundlePDP)
        cy.visit(path)
    })

    it('And there are two products on the page', function() {
        bundlePdp.assertNumberOfProducts(2)
    })

    it("And there is '1' Add To Bag button", function() {
        bundlePdp.assertNumberOfAddToBagButtons(1)
    })

    it("And I select an available size for bundle item number '1'", function() {
        bundlePdp.selectAvailableSizeForBundleItem(1)
    })

    it('And I select Add To Bag', function() {
        bundlePdp.addToBag()
    })

    it("And I should see an error that I also have to select a size for bundle item number '2'", function() {
        bundlePdp.expectErrorToAlsoSelectProduct(2)
    })

    it("And I select an available size for bundle item number '2'", function() {
        bundlePdp.selectAvailableSizeForBundleItem(2)
    })

    it('And I select Add To Bag', function() {
        cy.server()
        cy.route('POST', routes.addItemToBag, 'fixture:pdp/fixedBundleAddToBag')
        bundlePdp.addToBag()
    })

    it('And I choose View Bag from the mobile only confirmation modal', function() {
        bundlePdp.viewBagFromConfirmationModal()
    })

    it("And I see '2' products in the minibag", function() {
        miniBag.expectNumberOfProductsToBe(2)
    })

    it("When I change the size of item number '1' in the minibag", function() {
        miniBag.invokeCurrentProductSizeOfProductAs(
            1,
            'prevSizeAndQuantOfProductOneInMinibag'
        )
        cy.server()
        cy.route(
            'GET',
            `${routes.miniBagFetchSizesAndQuants}?catEntryId=${
                fixedBundleAddToBag.products[0].catEntryId
            }`,
            'fixture:pdp/fixedBundeFetchItemAndSizes'
        )
        miniBag.editAndChangeSizeForProductNumberTo(1, '10')
        cy.route(
            'PUT',
            routes.miniBagUpdateItem,
            'fixture:pdp/fixedBundleUpdateSizeOfProductOne'
        )
        miniBag.saveChanges()
    })

    it("Then I see that item number '1' has a different size than it had before", function() {
        miniBag.expectSizeOfProductNumberToNotEqual(
            1,
            this.prevSizeAndQuantOfProductOneInMinibag
        )
    })

    it("And item number '1' has a different size than item number '2'", function() {
        miniBag.expectProductAToHaveDifferentSizeThanProductB(1, 2)
    })
})
