Feature: DDP Subscription Status Component

Scenario: DDP Subscription Status Component
    Given I am an authenticated user
    And I am a current DDP subscriber (isDDPUser = true)
    And the feature FEATURE_DDP is activated
    When I am on the Checkout & Delivery Details page of My Account
    Then I should see the DDP section
    And I should see copy "Your delivery subscription expires on <EXPIRY DATE>"
    And I should see a "Cancel" link
