import {setFeature, suite, setupMocksForMyAccount} from './../../../lib/helpers'
import MyAccountCheckoutDetails from './../../../page-objects/MyAccountCheckoutDetails.page'
import ddpInRenewalState from './../../../fixtures/checkout/account--ddpRenewalState.json'
import {ddpLinks} from './../../../constants/urls'

const myAccount = new MyAccountCheckoutDetails()
const ddpInactiveRenewalState =
    // Adding the following to change the state of DDP User
    Object.assign({}, ddpInRenewalState, {
        isDDPUser: false,
        ddpEndDate: '27 September 2018',
        isDDPRenewable: true,
    })

suite('DDP Subscription Status - Active Renewal', () => {
    it('Given I am an authenticated user', function() {})

    it('And I am a current DDP subscriber (isDDPUser = true)', () => {})

    it('When I am on the Checkout & Delivery Details page of My Account', () => {
        setupMocksForMyAccount(ddpInRenewalState)
        cy.visit('/my-account/details', {failOnStatusCode: false})
        setFeature('FEATURE_DDP')
    })

    it('Then I should see the DDP section', () => {
        cy.get(myAccount.ddpSubscriptionContainer).should('be.visible')
    })

    it("And I should see ddp icon and copy 'Your delivery subscription expires on <EXPIRY DATE>'", () => {
        cy.get(myAccount.ddpSubscriptionMessage)
            .should('be.visible')
            .should((message) => {
                expect(message).to.contain(ddpInRenewalState.ddpEndDate)
            })
        cy.get(myAccount.ddpIcon).should('be.visible')
    })

    it('And I should see a "Cancel" and "Renew" link', () => {
        cy.get(myAccount.ddpSubscriptionCancel).should('be.visible')
        cy.get(myAccount.ddpRenewLink).should('be.visible')
    })

    it('And renewal link should take me to approprite url', () => {
        myAccount.clickDdpRenewalLink()
        cy.url().should('include', ddpLinks.toshopDdpRenewalLink)
    })
    // NOTE: The cancel link is verified manually. Accessing new windows via Cypress is not possible in its current version.
})

suite('DDP Subscription Status - Inactive Renewal', () => {
    it('Given I am an authenticated user', function() {})

    it('And I am an inactive DDP subscriber (isDDPUser = false)', () => {})

    it('And I am in my DDP renewal window', () => {})
    it('When I am on the Checkout & Delivery Details page of My Account', () => {
        setupMocksForMyAccount(ddpInactiveRenewalState)
        cy.visit('/my-account/details', {failOnStatusCode: false})
        setFeature('FEATURE_DDP')
    })
    it("And I should see ddp icon and copy 'Your subscription has expired'", () => {
        cy.get(myAccount.ddpSubscriptionMessage)
            .should('be.visible')
            .should('have.text', 'Your subscription has expired')
        cy.get(myAccount.ddpIcon).should('be.visible')
    })
    it('And I should see a "Cancel" and "Renew" link', () => {
        cy.get(myAccount.ddpSubscriptionCancel).should('be.visible')
        cy.get(myAccount.ddpRenewLink).should('be.visible')
    })
})
