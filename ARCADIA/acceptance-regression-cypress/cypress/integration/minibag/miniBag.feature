Feature: Mini Bag Functionalites

  Scenario: User is not logged in and adds DDP Product to basket
    Given I am in home page
    And I add DDP product to my basket
    When I view the minibag
    Then the "edit" link should not be visible
    And size should not be visible

  Scenario: User is logged in and adds DDP product to basket
    Given I am logged in
    And I add DDP product to my basket
    When I view the minibag
    Then the "edit" link should not be visible
    And size should not be visible