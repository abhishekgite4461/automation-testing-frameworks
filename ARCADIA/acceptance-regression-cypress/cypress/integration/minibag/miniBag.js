import {
    suite,
    setupClientMocksNewUserJourneyToCheckoutWithDdpProd,
    setupMocksForInitialRenderOnPdp,
    setupMocksForInitialRenderOnGenericPage,
    setFeature,
} from './../../lib/helpers'
import singleSizeProduct from './../../fixtures/pdp/singleSizeProduct/pdpInitialFetch'
import ProductDetailPage from './../../page-objects/Pdp.page'
import MiniBag from './../../page-objects/MiniBag.page'

const pdp = new ProductDetailPage()
const minibag = new MiniBag()

function setupAddAndViewDdpInMinibag() {
    //TODO: write a mock which will have the product in MiniBag when we land on home page
    setupClientMocksNewUserJourneyToCheckoutWithDdpProd()
    const path = setupMocksForInitialRenderOnPdp(singleSizeProduct)
    cy.visit(path)
    setFeature('FEATURE_DDP')
    pdp.addToBag()
    pdp.viewBagFromConfirmationModal()
}

suite('User is not logged in and adds DDP Product to basket', () => {
    it(' Given I am in home page', () => {
        setupMocksForInitialRenderOnGenericPage()
        cy.visit('/')
    })

    it('And I add DDP product to my basket', () => {
        setupAddAndViewDdpInMinibag()
    })

    it('When I view the minibag', () => {})

    it('Then the "edit" link should not be visible', () => {
        cy.get(minibag.editProductButton).should('not.be.visible')
    })
    it('And size should not be visible', () => {
        cy.get(minibag.productSize).should('not.be.visible')
    })
})
