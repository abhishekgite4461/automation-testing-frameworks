export default {
    account: '/api/account',
    addItemToBag: '/api/shopping_bag/add_item',
    login: '/api/account/login',
    miniBagFetchSizesAndQuants:
        '/api/shopping_bag/fetch_item_sizes_and_quantities',
    miniBagUpdateItem: '/api/shopping_bag/update_item',
    miniBagRemoveItem: '/api/shopping_bag/delete_item',
    registerNewUser: '/api/account/register',
    checkoutOrderSummary: '/api/checkout/order_summary',
    checkoutOrderSummaryDeliveryAddress:
        '/api/checkout/order_summary/delivery_address',
    checkoutOrder: '/api/order',
    productSearch: (searchTerm) => `/api/products?q=${searchTerm}`,
    paymentsAll: '/api/payments?value=all',
    paymentsDeliveryUkBillingUk:
        '/api/payments?delivery=United Kingdom&billing=United Kingdom',
    cmsMobileOrderConfirmationEspotPos1:
        '/api/cms/pages/mobileOrderConfirmationESpotPos1',
    addPromotionCode: '/api/shopping_bag/addPromotionCode',
    delPromotionCode: '/api/shopping_bag/delPromotionCode',
    getItems: '/api/shopping_bag/get_items',
    giftCard: 'api/checkout/gift-card',
    storeLocator: '/api/store-locator*',
}
