export const visaCardPaymentDetails = {
    cardNumber: '4444333322221111',
    cvvNumber: '123',
    expiryMonth: '12',
    expiryYear: '2029',
}

export const giftCard = {
    giftCardNumber: '6337850394538531',
    pin: '1653',
}
