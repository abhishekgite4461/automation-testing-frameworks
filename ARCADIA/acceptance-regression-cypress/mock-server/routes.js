const fixtures = require('./../cypress/fixtures')

const commonRoutes = [
    {
        method: 'GET',
        url: '/api/navigation/categories',
        response: fixtures.general.categories,
    },
    {
        method: 'GET',
        url: '/api/desktop/navigation',
        response: fixtures.general.navigation,
    },
    {
        method: 'GET',
        url: '/api/site-options',
        response: fixtures.general.siteOptions,
    },
    {
        method: 'GET',
        url: '/api/footers',
        response: fixtures.general.footer,
    },
    {
        method: 'GET',
        url: '/api/stores-countries?brandPrimaryEStoreId=12556&cfsi=true',
        response: {},
    },
    {
        method: 'GET',
        url: /\/cmscontent(.*)/,
        response: fixtures.cms.cmscontent,
    },
    {
        method: 'GET',
        url: /\/cms\/pages\/(.*)/,
        response: fixtures.cms.cmscontent,
    },
    {
        method: 'GET',
        url: '/api/features',
        response: {
            features: {
                '0': 'FEATURE_SWATCHES',
                '1': 'FEATURE_PLP_REDIRECT_IF_ONE_PRODUCT',
                '2': 'FEATURE_PDP_SCROLL_TO_TOP',
                '3': 'FEATURE_PRODUCT_VIDEO',
                '4': 'FEATURE_SHOW_SIZE_GUIDE',
                '5': 'PASSWORD_SHOW_TOGGLE',
                '6': 'FEATURE_CFSI',
                '7': 'FEATURE_PDP_QUANTITY',
                '8': 'FEATURE_PUDO',
                '9': 'FEATURE_HEADER_BIG',
                '10': 'FEATURE_STORE_FINDER_HEADER_WITH_COUNTRY_SELECTOR',
                '11': 'FEATURE_RESPONSIVE_PRODUCT_VIEW',
                '12': 'FEATURE_ORDER_HISTORY_MSG',
                '13': 'FEATURE_RESPONSIVE',
                '14': 'FEATURE_NEW_CHECKOUT',
                '15': 'FEATURE_GEOIP',
                '16': 'FEATURE_SHOP_THE_LOOK',
                '17': 'FEATURE_SAVE_PAYMENT_DETAILS',
                '18': 'FEATURE_SHOW_FIT_ATTRIBUTE_LINKS',
                '19': 'FEATURE_TRANSFER_BASKET',
                '20': 'FEATURE_MY_CHECKOUT_DETAILS',
                '21': 'FEATURE_PRODUCT_DESCRIPTION_SEE_MORE',
                '22': 'FEATURE_ADDRESS_BOOK',
                '23': 'FEATURE_MEGA_NAV',
                '24': 'FEATURE_DESKTOP_RESET_PASSWORD',
                '25': 'FEATURE_CVV_HELP',
                '26': 'FEATURE_FULL_MONTY_ESPOTS',
                '27': 'FEATURE_LEGACY_CAT_HEADERS_IFRAME',
                '28': 'FEATURE_LEGACY_PAGES',
            },
        },
    },
    {
        method: 'POST',
        url: '/api/client-info',
        response: '',
    },
    {
        method: 'POST',
        url: '/api/client-error',
        response: '',
    },
    {
        method: 'POST',
        url: '/api/client-debug',
        response: '',
    },
    {
        method: 'GET',
        url: /\/collector\/(.*)/,
        response: '',
    },
    {
        method: 'POST',
        url: /\/collector\/(.*)/,
        response: '',
    },
    {
        method: 'GET',
        url: /api\/cms\/pages\/(.*)/,
        response: '',
    },
    {
        method: 'GET',
        url: /user\/(.*)/,
        response: '',
    },
    {
        method: 'GET',
        url: '/api/keep-alive',
        response: '',
    },
]

export const getPDPRoutes = ({path, response}) => {
    const routes = commonRoutes
    routes.push(
        {
            method: 'GET',
            url: `/api/products/${encodeURIComponent(path)}`,
            response: response,
        },
        {
            // this one is because cypress decodes urls automatically which results no match...
            method: 'GET',
            url: `/api/products/${path}`,
            response: response,
        }
    )

    if (response.seeMoreValue && response.seeMoreValue.length) {
        response.seeMoreValue.forEach((smv, i) => {
            routes.push({
                method: 'GET',
                url: `/api/products?endecaSeoValue=${response.seeMoreValue[
                    i
                ].seeMoreLink.substr(3)}`,
                response: fixtures.plp.noResults,
            })
        })
    }

    return routes
}

export const getCommonRoutes = () => commonRoutes

export const getPlpRoutes = (searchTerm) => {
    const routes = commonRoutes
    const productsToLoadIn = fixtures.plp.initialPageLoad
    routes.push({
        method: 'GET',
        url: `/api/products?q=${encodeURIComponent(searchTerm)}`,
        response: productsToLoadIn,
    })

    return {routes, products: productsToLoadIn.products}
}
