const {log} = console
const {red, green, blue} = require('chalk')
const {Server} = require('hapi')
const url = require('url')

const server = Server({
    port: process.env.CORE_API_PORT || 4000,
    host: '0.0.0.0',
    routes: {cors: true},
})

let mocks = {}

server.route({
    method: 'POST',
    path: '/mocks',
    handler: (request, h) => {
        const {
            payload: {method, url, response},
        } = request

        if (!method || !url) {
            log(
                red('ERROR'),
                'Missing parameters',
                method || 'missing method',
                url || 'missing url'
            )
            return h.response().code(422)
        }

        if (!mocks[method]) {
            mocks[method] = {}
        }

        mocks[method][url] = response

        log(blue('LOAD'), method, url)

        return h.response().code(201)
    },
})

server.route({
    method: 'DELETE',
    path: '/mocks',
    handler: (request, h) => {
        mocks = {}

        log(blue('RESET'))

        return h.response().code(200)
    },
})

server.route({
    method: 'GET',
    path: '/mocks',
    handler: (request, h) => {
        return h.response(mocks).code(200)
    },
})

server.route({
    method: '*',
    path: '/{any*}',
    handler: (request, h) => {
        const {
            url: {pathname, query},
        } = request
        const method = request.method.toUpperCase()
        let path = url.format({pathname, query})

        if (!mocks[method] || !mocks[method][path]) {
            log(red('MISSING'), method, path)
            return h.response().code(404)
        }

        const response = mocks[method][path]

        log(green('REQUEST'), method, path)

        return h.response(response).code(200)
    },
})

const start = () => {
    try {
        server.start()
    } catch (err) {
        log(err)
        process.exit(1)
    }

    log('Mock server running at:', server.info.uri)
}

start()
