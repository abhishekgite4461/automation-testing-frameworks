#!/bin/sh

cd /app
node ./mock-server/index.js >> /app/mock-server/output.log & 

# usually this will be $(npm bin)/cypress run or something similar. but we want to keep it flexible
exec "$@"
