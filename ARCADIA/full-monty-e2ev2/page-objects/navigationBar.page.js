import {
    waitForElementToBeVisible,
    waitForPartialUrl,
    waitForElementToExist,
} from '../support/lib/waitForWrappers'
import disableAnimationForElement from '../support/lib/disableAnimationForElement'
import {isDesktopLayout, isMobileLayout} from '../support/lib/shared'
import routes from '../support/constants/routes'
import MiniBag from './miniBag.page'
import SignInPage from './signIn.page'
import PLPPage from './plp.page'
import PDPPage from './pdp.page'
import {buildBrandBaseUrl} from '../support/lib/brandURL'

module.exports = class NavigationBar {
    constructor() {
        this.waitForFullyLoadedPage()
    }

    /**
     * ELEMENTS ******************************************************************
     */
    get burgerMenu() {
        return '.TopNavMenu'
    }
    get burgerMenuButton() {
        return '.BurgerButton'
    }
    get contentOverlay() {
        return '.ContentOverlay'
    }
    get desktopMiniBagIcon() {
        return '.ShoppingCart'
    }
    get desktopSignInLink() {
        return '.HeaderBig .AccountIcon-link'
    }
    get disappearedLoadingOverlay() {
        return '.LoaderOverlay[aria-busy="false"]'
    }
    get miniBagDrawer() {
        return '.Drawer'
    }
    get mobileMiniBagIcon() {
        return '.Header-shoppingCartIconbutton'
    }
    get mobileSignOutLink() {
        return '.ListItemLink[href*="logout"]'
    }
    get openedBurgerMenuSelector() {
        return '.TopNavMenu.is-open'
    }
    get desktopSearchInput() {
        return '.SearchBar-queryInput'
    }
    get mobileSearchBarButton() {
        return '.Header-searchButton'
    }
    get mobileSignInLink() {
        return '*=Register'
    }
    get searchBarInput() {
        return '.SearchBar-queryInput'
    }

    /**
     * ELEMENT ACTIONS ***********************************************************
     */
    waitForFullyLoadedPage() {
        waitForElementToExist(
            this.disappearedLoadingOverlay,
            'Navigation, initital page load'
        )
    }

    clickSignInLinkDesktop() {
        waitForElementToBeVisible(this.desktopSignInLink)
        browser.click(this.desktopSignInLink)
    }

    openBurgerMenu() {
        disableAnimationForElement(this.burgerMenu)
        waitForElementToBeVisible(this.burgerMenuButton)
        browser.click(this.burgerMenuButton)
        waitForElementToBeVisible(this.openedBurgerMenuSelector)
    }

    clickMobileSignInLink() {
        browser.click(this.mobileSignInLink)
        return new SignInPage()
    }

    waitForSignInLink() {
        browser.waitForExist(this.mobileSignOutLinkSelector, 10000, true)
    }

    mobileSignIn() {
        this.openBurgerMenu()
        this.clickMobileSignInLink()
        return new SignInPage()
    }

    performSearch(searchQuery) {
        try {
            if (isMobileLayout()) {
                browser.click(this.mobileSearchBarButton)
            }
            if (isDesktopLayout()) {
                browser.click(this.desktopSearchInput)
            }
        } catch (err) {
            if (
                err.message.includes(
                    'Other element would receive the click: <div class="ContentOverlay' +
                        ' ContentOverlay--modalOpen"'
                )
            ) {
                err.message = `Things are not going as expected. Most likely the user has items in the bag from a previous visit.\n${
                    err.message
                }`
            }
            throw err
        }
        browser.setValue(this.searchBarInput, searchQuery)
        browser.keys('Enter')
    }
    /**
     * USER ACTIONS **************************************************************
     */
    searchForSpecificItem(itemCode) {
        this.performSearch(itemCode)
        return new PDPPage()
    }

    searchForTerm(searchTerm) {
        this.performSearch(searchTerm)
        return new PLPPage()
    }

    goToSignInPage() {
        if (isDesktopLayout()) {
            // this.clickSignInLinkDesktop()
        } else if (isMobileLayout()) {
            this.mobileSignIn()
        }
        return new SignInPage()
    }

    chooseCategory(category) {
        browser.click(`button=${category}`)
    }

    chooseSubCategory(subCategory) {
        waitForElementToBeVisible(
            `a=${subCategory}`,
            `chooseSubCategory: The subcategory we were looking for (${subCategory}) wasn't visible.`
        )
        browser.click(`a=${subCategory}`)
        waitForPartialUrl(
            routes.category,
            'Navigation, choosing a subcategory: Expected to land on a category page.'
        )
    }

    goToCategoryPage(category) {
        if (isMobileLayout()) {
            this.openBurgerMenu()
            this.chooseCategory(category)
        }
    }

    goToSubCategoryPage(subCategory) {
        if (isMobileLayout()) {
            this.chooseSubCategory(subCategory)
        } else {
            browser.url(
                `${buildBrandBaseUrl(`${browser.options.brand}uk`)}${
                    browser.options.testData.subCategoryUrl
                }`
            )
        }
    }

    openMiniBag() {
        disableAnimationForElement(this.miniBagDrawer)
        disableAnimationForElement(this.contentOverlay)

        if (isDesktopLayout()) {
            if (!browser.getUrl().includes(routes.deliveryPayment)) {
                waitForElementToBeVisible(this.desktopMiniBagIcon)
                browser.click(this.desktopMiniBagIcon)
            }
        } else if (isMobileLayout()) {
            waitForElementToBeVisible(this.mobileMiniBagIcon)
            browser.click(this.mobileMiniBagIcon)
        }

        return new MiniBag()
    }
}
