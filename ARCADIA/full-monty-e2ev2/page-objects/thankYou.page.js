import {
    waitForElementToBeVisible,
    waitForElementToBeEnabled,
    waitForElementToExist,
} from '../support/lib/waitForWrappers'
import HomePage from './home.page'

module.exports = class ThankYouPage {
    constructor() {
        this.waitForFullyLoadedPage()
    }

    /**
     * ELEMENTS ******************************************************************
     */
    get deliveryDateText() {
        return '.OrderComplete-estimatedDelivery'
    }
    get disappearedLoadingOverlay() {
        return '.LoaderOverlay[aria-busy="false"]'
    }
    get orderNumberText() {
        return '.OrderComplete-orderNumber'
    }
    get promoCodeDiscountApplied() {
        return '.SimpleTotals-discount .SimpleTotals-groupRight'
    }
    get promoCodeDiscountName() {
        return '.SimpleTotals-discount .SimpleTotals-groupLeft'
    }
    get thankYouMessageText() {
        return 'h1*=Thank You'
    }
    get yourOrderDetailsQuantity() {
        return '.OrderProducts-label'
    }
    get continueShoppingCTAButtonSelector() {
        return '.OrderComplete-button'
    }

    /**
     * ELEMENT ACTIONS ***********************************************************
     */
    waitForFullyLoadedPage() {
        waitForElementToBeVisible(this.continueShoppingCTAButtonSelector)
        waitForElementToBeEnabled(this.continueShoppingCTAButtonSelector)
        waitForElementToExist(
            this.disappearedLoadingOverlay,
            'Thank You page, initial page load.'
        )
    }

    existsSuccessfulOrderMessage() {
        waitForElementToBeVisible(this.thankYouMessageText)
    }

    existsOrderNumber() {
        waitForElementToBeVisible(this.orderNumberText)
    }

    existsDeliveryDate() {
        waitForElementToBeVisible(this.deliveryDateText)
    }

    existsContinueShoppingCTA() {
        waitForElementToBeVisible(this.continueShoppingCTAButtonSelector)
    }

    getOrderQuantity() {
        const orderQuantity = browser.getText(this.yourOrderDetailsQuantity)
        // Note quantity string will show as follow `3 x size ONE` the below will extract the first character
        return orderQuantity.substring(0, 1)
    }

    existsDiscount() {
        waitForElementToBeVisible(this.promoCodeDiscountName)
        waitForElementToBeVisible(this.promoCodeDiscountApplied)
    }

    /**
     * USER ACTIONS **************************************************************
     */
    continueShopping() {
        waitForElementToBeVisible(
            this.continueShoppingCTAButtonSelector,
            "Thank You Page: Continue Shopping button didn't become visible."
        )
        browser.click(this.continueShoppingCTAButtonSelector)
        return new HomePage()
    }

    verifyOrderConfirmation() {
        this.existsSuccessfulOrderMessage()
        this.existsOrderNumber()
        this.existsDeliveryDate()
    }
}
