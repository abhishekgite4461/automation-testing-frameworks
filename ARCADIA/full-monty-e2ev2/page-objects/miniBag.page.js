import {
    waitForElementToBeVisible,
    waitForElementToExist,
    waitForElementToBeEnabled,
    waitForPartialUrl,
    waitForElementToNotExist,
} from '../support/lib/waitForWrappers'
import routes from '../support/constants/routes'
import {assert} from 'chai'
import {isMobileLayout, isDesktopLayout} from '../support/lib/shared'
import {deliveryOptionIds} from './../support/constants/deliveryOptions'

const checkoutButtonNotVisibleFailureExplanation =
    'Minibag: Checkout button ' +
    'is not visible. A common cause for this is the API returning an empty bag ' +
    'due to session timeout or the request taking too long.'
module.exports = class MiniBag {
    constructor() {
        this.waitForFullyLoadedPage()
    }
    get checkoutButtonNotVisibleFailureExplanation() {
        return (
            'Minibag: Checkout button is not visible. ' +
            'A common cause for this is the API returning an empty bag due to ' +
            'session timeout or the request taking too long.'
        )
    }
    /**
     * ELEMENTS ******************************************************************
     */
    get addAnotherPromoCode() {
        return '.PromotionCode-addText'
    }
    get checkoutContainer() {
        return '.CheckoutContainer'
    }
    get mobileCheckoutMiniBagHeader() {
        return '.CheckoutMiniBag-header'
    }
    get checkoutNowButton() {
        return '.Button.MiniBag-continueButton'
    }
    get desktopMiniBagCheckoutProductsSelector() {
        return '.CheckoutContainer .OrderProducts-productName'
    }
    get desktopCheckoutMiniBagHeader() {
        return '.CheckoutBagSide-title'
    }
    get disappearedLoadingOverlay() {
        return '.LoaderOverlay[aria-busy="false"]'
    }
    get mobileItemEditLink() {
        return '.OrderProducts-editText'
    }
    get desktopItemEditLink() {
        return '//*[contains(@class, "OrderProducts-editText")][1]'
    }
    get mobileItemSizeDropdown() {
        return '.Select-select[name="bagItemSize"]'
    }
    get desktopItemSizeDropdown() {
        return '//*[contains(@name, "bagItemSize")][1]'
    }
    get miniBagDeleteItemConfirmationModalButton() {
        return '.Modal.is-shown.Modal--normal .Button'
    }
    get miniBagDeleteItemModal() {
        return '.Modal.is-shown.Modal--normal'
    }
    get miniBagHeader() {
        return '.MiniBag-header'
    }
    get miniBagIndividualProduct() {
        return '.OrderProducts-product'
    }
    get mobileMiniBagProductsSelector() {
        return '.Drawer .OrderProducts-productName'
    }
    get promotionCodeConfirmationLabel() {
        return '.PromotionCode-codeConfirmation.is-shown'
    }
    get promotionCodeInputField() {
        return '.Input-field-promotionCode'
    }
    get promotionCodeApplied() {
        return '.PromotionCode-codeTitle'
    }
    get promotionCodeHeader() {
        return '.PromotionCode-header'
    }
    get promotionCodeSubmitButton() {
        return '.Button.PromotionCode-submit'
    }
    get appliedDiscount() {
        return '.MiniBag-discount'
    }
    get miniBagDeleteItem() {
        return 'button.OrderProducts-deleteText'
    }
    get miniBagDeleteItemModalButton() {
        return '.Modal.is-shown.Modal--normal .Button.OrderProducts-deleteButton'
    }
    get deliveryOptionDropdown() {
        return '.Select-select[name="miniBagDeliveryType"]'
    }
    get desktopMiniBagCheckoutProducts() {
        return $$(this.desktopMiniBagCheckoutProductsSelector)
    }
    get individualProductsInMiniBag() {
        return $$(this.miniBagIndividualProduct)
    }
    get miniBagItemSizeAndQuantity() {
        return '.OrderProducts-productSize'
    }
    get mobileMiniBagProducts() {
        return $$(this.mobileMiniBagProductsSelector)
    }
    get saveButton() {
        return '.OrderProducts-saveButton'
    }
    get itemQuantityDropDown() {
        return '.Select-select[name="bagItemQuantity"]'
    }

    /**
     * ELEMENT ACTIONS ***********************************************************
     */
    waitForFullyLoadedPage() {
        if (browser.getUrl().includes(routes.delivery)) {
            if (isMobileLayout()) {
                waitForElementToBeVisible(this.mobileCheckoutMiniBagHeader)
            } else {
                waitForElementToBeVisible(this.desktopCheckoutMiniBagHeader)
            }
        } else {
            waitForElementToBeVisible(
                this.miniBagHeader,
                'Minibag page, initial page load.'
            )
            waitForElementToBeVisible(
                this.checkoutNowButton,
                checkoutButtonNotVisibleFailureExplanation
            )
        }
    }

    enterPromoCode(promoCode) {
        waitForElementToExist(this.promotionCodeInputField)
        waitForElementToBeEnabled(this.promotionCodeInputField)
        browser.setValue(this.promotionCodeInputField, promoCode)
        browser.click(this.promotionCodeSubmitButton)
        waitForElementToNotExist(this.promotionCodeInputField)
    }

    changeProduct() {
        const editButton = isMobileLayout()
            ? this.mobileItemEditLink
            : this.desktopItemEditLink
        browser.click(editButton)
        waitForElementToExist(
            this.disappearedLoadingOverlay,
            'Minibag, after clicking on editing item.'
        )
    }
    /**
     * USER ACTIONS **************************************************************
     */
    addFirstPromotionCode(promoCode) {
        waitForElementToBeVisible(this.promotionCodeHeader)
        browser.click(this.promotionCodeHeader)
        this.enterPromoCode(promoCode)
    }

    addAnotherPromotionCode(promoCode) {
        waitForElementToBeVisible(
            this.addAnotherPromoCode,
            '"Add another" link not visible. Ensure a promo code was added previously.'
        )
        browser.click(this.addAnotherPromoCode)
        this.enterPromoCode(promoCode)
    }

    selectDeliveryOption(deliveryOption) {
        browser.selectByValue(
            this.deliveryOptionDropdown,
            deliveryOptionIds(deliveryOption, browser.options.brand)
        )
        waitForElementToExist(this.disappearedLoadingOverlay)
    }

    goToCheckout() {
        waitForElementToBeVisible(
            this.checkoutNowButton,
            checkoutButtonNotVisibleFailureExplanation
        )
        browser.click(this.checkoutNowButton)
        waitForPartialUrl(
            routes.checkout,
            'Minibag: expected transition to checkout.'
        )
        waitForElementToBeVisible(this.checkoutContainer)
    }

    clickRemoveItem() {
        waitForElementToBeVisible(this.miniBagDeleteItem)
        browser.click(this.miniBagDeleteItem)
        waitForElementToBeVisible(this.miniBagDeleteItemModal)
    }

    clickDeleteItemButtonFromModal() {
        waitForElementToBeVisible(this.miniBagDeleteItemModalButton)
        browser.click(this.miniBagDeleteItemModalButton)
        waitForElementToExist(
            this.disappearedLoadingOverlay,
            'Minibag, product removed'
        )
    }

    getProductsFromMiniBag() {
        const productList = []
        if (isMobileLayout()) {
            waitForElementToBeVisible(this.mobileMiniBagProductsSelector)
            this.mobileMiniBagProducts.forEach((element) => {
                productList.push(element.getText())
            })
        }
        if (isDesktopLayout()) {
            if (browser.getUrl().includes(routes.deliveryPayment)) {
                waitForElementToBeVisible(
                    this.desktopMiniBagCheckoutProductsSelector
                )
                this.desktopMiniBagCheckoutProducts.forEach((element) => {
                    productList.push(element.getText())
                })
            } else {
                waitForElementToBeVisible(this.mobileMiniBagProductsSelector)
                this.mobileMiniBagProducts.forEach((element) => {
                    productList.push(element.getText())
                })
            }
        }
        return productList
    }

    updateSize() {
        this.changeProduct()
        const itemSizeDropdown = isMobileLayout()
            ? this.mobileItemSizeDropdown
            : this.desktopItemSizeDropdown
        waitForElementToBeVisible(itemSizeDropdown)
        const dropdownOptions = $(itemSizeDropdown)
            .elements('option')
            .getText()
        const filterAvailableOptions = dropdownOptions.filter(
            (ele) =>
                ele !== browser.options.testData.multipleSizesProduct.size &&
                !ele.includes('stock')
        )
        browser.selectByValue(itemSizeDropdown, filterAvailableOptions[1])
        browser.click(this.saveButton)
        waitForElementToExist(
            this.disappearedLoadingOverlay,
            'Minibag, after updating the size.'
        )
        waitForElementToExist(this.mobileItemEditLink)
    }
    getItemQuantity() {
        let itemQuantity = browser.getText(this.miniBagItemSizeAndQuantity)
        itemQuantity = isMobileLayout() ? itemQuantity : itemQuantity[1]
        const currentItemQuantity = itemQuantity.match(/^\d+/g)
        return currentItemQuantity[0]
    }
    updateItemQuantity(itemQnt) {
        this.changeProduct()
        browser.element(this.itemQuantityDropDown).selectByValue(itemQnt)
        browser.click(this.saveButton)
        waitForElementToExist(
            this.disappearedLoadingOverlay,
            'Minibag, after updating the quantity.'
        )
        waitForElementToExist(this.mobileItemEditLink)
    }
    getItemSize() {
        let itemSize = browser.getText(this.miniBagItemSizeAndQuantity)
        itemSize = isMobileLayout() ? itemSize : itemSize[1]
        const currentItemSize = itemSize.match(/^\d+/g)
        return currentItemSize[0]
    }

    removeProductFromMiniBag() {
        this.clickRemoveItem()
        this.clickDeleteItemButtonFromModal()
    }

    checkMiniBagForProduct() {
        waitForElementToBeVisible(this.miniBagIndividualProduct)
        assert(
            this.individualProductsInMiniBag.length === 1,
            'There are either no products or too many in the mini bag.'
        )
    }

    getAppliedPromotionCodes() {
        const promoCodes = []
        waitForElementToBeVisible(
            this.promotionCodeApplied,
            'No applied promotion codes in the mini bag. (In case of feature ' +
                'promoCodeLink: It may take some time for the promo code to appear ' +
                'in the minibag. Perhaps the test environment was not fast enough.)'
        )
        $$(this.promotionCodeApplied).forEach((appliedPromo) => {
            promoCodes.push(appliedPromo.getText())
        })
        return promoCodes
    }
}
