import _ from 'lodash'
import {
    waitForElementToBeVisible,
    waitForElementToBeEnabled,
} from './../support/lib/waitForWrappers'
import disableAnimationForElement from './../support/lib/disableAnimationForElement'
import {isMobileLayout} from './../support/lib/shared'
import routes from './../support/constants/routes'

module.exports = class StoreLocator {
    constructor(skipFullPageLoad) {
        if (!skipFullPageLoad) {
            this.waitForFullyLoadedPage()
        }
    }

    /**
     * ELEMENTS ******************************************************************
     */
    get disappearedLoadingOverlaySelector() {
        return '.LoaderOverlay[aria-busy="false"]'
    }
    get modalMobileDiv() {
        return '.Modal'
    }
    get targetStoreSelectButton() {
        return '.Store-selectButtonContainer .Button'
    }
    get locationForDeliveryPointSearchInput() {
        return 'input.UserLocatorInput-inputField'
    }
    get locationSuggestion() {
        return '.UserLocatorInput-predictionsListItem'
    }
    get searchForStoresGoButton() {
        return '.Button.UserLocator-goButton'
    }
    get contentOverlay() {
        return '.ContentOverlay'
    }
    get storeListItems() {
        return '.Store-name'
    }
    get storeAddress() {
        return '.Store-address'
    }
    get closeIcon() {
        return isMobileLayout()
            ? '.Modal-closeIcon'
            : '.Modal--storeLocator .Modal-closeIcon'
    }

    /**
     * ELEMENT ACTIONS ***********************************************************
     */
    waitForFullyLoadedPage() {
        waitForElementToBeVisible(this.locationForDeliveryPointSearchInput)
    }

    expandAccordionInfoForStore(number) {
        waitForElementToBeVisible(this.storeListItems)
        disableAnimationForElement(this.modalMobileDiv)
        disableAnimationForElement(this.contentOverlay)
        $$(this.storeListItems)[number - 1].click()
    }

    /**
     * USER ACTIONS **************************************************************
     */
    searchAndSelectDeliveryPoint(location) {
        const mdfer =
            isMobileLayout() && _.includes(browser.getUrl(), routes.pdp)
                ? `${this.modalMobileDiv} `
                : ''
        const B = (selector) => `${mdfer}${selector}`

        waitForElementToBeEnabled(B(this.locationForDeliveryPointSearchInput))
        $(B(this.locationForDeliveryPointSearchInput)).setValue(location)

        // TODO: remove the PAUSE !!!
        browser.pause(200)
        waitForElementToBeVisible(B(this.locationSuggestion))
        waitForElementToBeEnabled(B(this.locationSuggestion))
        browser.click(B(this.locationSuggestion))

        waitForElementToBeVisible(B(this.searchForStoresGoButton))
        waitForElementToBeEnabled(B(this.searchForStoresGoButton))
        browser.click(B(this.searchForStoresGoButton))
        waitForElementToBeVisible(this.storeListItems)
    }

    chooseDeliveryPointAtIndex(number) {
        this.expandAccordionInfoForStore(number)
        waitForElementToBeVisible(this.targetStoreSelectButton)
        $$(this.targetStoreSelectButton)[number - 1].click()
    }

    verifyStoreData(number) {
        this.expandAccordionInfoForStore(number)
        waitForElementToBeVisible(this.storeAddress)
    }

    closeModal() {
        disableAnimationForElement(this.modalMobileDiv)
        disableAnimationForElement(this.contentOverlay)
        browser.click(this.closeIcon)
    }
}
