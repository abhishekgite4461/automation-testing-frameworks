import {
    waitForElementToBeVisible,
    waitForElementToBeSelected,
    waitForElementToExist,
    waitForPartialUrl,
} from '../support/lib/waitForWrappers'
import disableAnimationForElement from './../support/lib/disableAnimationForElement'
import routes from './../support/constants/routes'
import ThankYouPage from './thankYou.page'

/**
 * @desc test data
 */
const cvvNumber = '123'

module.exports = class DeliveryPaymentPage {
    constructor(skipFullPageLoad) {
        if (!skipFullPageLoad) {
            this.waitForFullyLoadedPage()
        } else {
            waitForPartialUrl(
                routes.deliveryPayment,
                'DeliveryPayment page: Route not as expected.'
            )
            waitForElementToExist(this.disappearedLoadingOverlay)
        }
    }

    /**
     * ELEMENTS ******************************************************************
     */
    get cvvSuccessfulValidationIcon() {
        return 'div.FormComponent-cvv span.Input-validateIcon.Input-validateSuccess'
    }
    get disappearedLoadingOverlay() {
        return '.LoaderOverlay[aria-busy="false"]'
    }
    get orderAndPayButton() {
        return 'button.Button.PaymentContainer-paynow'
    }
    get termsAndConditionsCheckboxInput() {
        return 'input[name="isAcceptedTermsAndConditions"]'
    }
    get termsAndConditionsCheckboxSpan() {
        return '.Checkbox-check'
    }
    get mergedBagButton() {
        return '.CheckoutContainer-close'
    }
    get cvvInput() {
        return 'input[name="cvv"]'
    }
    get changePaymentButton() {
        return '.PaymentMethodPreview-button'
    }
    get paymentMethodLabel() {
        return '.PaymentMethodOption-label'
    }
    get payPalRadioButton() {
        return '//*[contains(@value, "PYPAL")]//following::span'
    }
    get warningOverlayDiv() {
        return '.ContentOverlay'
    }
    get warningOverlayDivHidden() {
        return `${this.warningOverlayDiv}.is-hidden`
    }

    /**
     * ELEMENT ACTIONS ***********************************************************
     */
    waitForFullyLoadedPage() {
        waitForElementToBeVisible(this.orderAndPayButton)
        waitForElementToExist(this.disappearedLoadingOverlay)
    }

    enterCVVNumber() {
        // input cvv number and TAB out
        browser.setValue(this.cvvInput, cvvNumber)
        browser.keys('Tab')
        waitForElementToBeVisible(this.cvvSuccessfulValidationIcon)
    }

    /**
     * USER ACTIONS **************************************************************
     */
    closeWarningMergedBag() {
        waitForElementToBeVisible(
            this.mergedBagButton,
            'Merged bag popup button not visible.'
        )
        disableAnimationForElement(this.warningOverlayDiv)
        browser.click(this.mergedBagButton)
        waitForElementToExist(this.warningOverlayDivHidden)
    }

    providePaymentDetails() {
        this.enterCVVNumber()
    }

    changePaymentMethod() {
        waitForElementToBeVisible(this.changePaymentButton)
        browser.click(this.changePaymentButton)
        waitForElementToBeVisible(
            this.paymentMethodLabel,
            'There were no payment methods visible. Check if the click on the "change" button did work.'
        )
    }

    selectPaymentMethod(paymentMethod) {
        if (paymentMethod === 'PayPal') {
            waitForElementToBeVisible(this.payPalRadioButton)
            browser.scroll(this.payPalRadioButton)
            browser.click(this.payPalRadioButton)
        }
    }

    acceptTCs() {
        waitForElementToBeVisible(
            this.termsAndConditionsCheckboxSpan,
            undefined
        )
        browser.click(this.termsAndConditionsCheckboxSpan)
        waitForElementToBeSelected(this.termsAndConditionsCheckboxInput)
    }

    placeOrder(target) {
        browser.click(this.orderAndPayButton)
        if (process.env.PRODUCTION === 'true') {
            return waitForElementToBeVisible(this.errorMessage)
        }
        if (!target) return new ThankYouPage()
        // if (target) -- await redirection to target 3rd party page --
    }
}
