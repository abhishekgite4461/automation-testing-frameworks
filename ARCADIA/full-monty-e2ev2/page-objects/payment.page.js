import {expect} from 'chai'
import {
    waitForElementToBeVisible,
    waitForElementToBeEnabled,
    waitForElementToBeSelected,
    waitForElementToExist,
} from '../support/lib/waitForWrappers'
import {isMobileLayout} from '../support/lib/shared'
import ThankYouPage from './thankYou.page'

const visaCardPaymentDetails = {
    cardNumber: '4444333322221111',
    cvvNumber: '123',
    expiryMonth: '12',
    expiryYear: '2029',
}

const accountCardPaymentDetails = {
    cardNumber: '6000082000000005',
    cvvNumber: '123',
    expiryMonth: '12',
    expiryYear: '2029',
}

const billingNameAndPhoneDetails = {
    title: 'Dr',
    firstName: 'I Love',
    surname: 'Shopping',
    phoneNumber: '0987654321',
}

const amexCardPaymentDetails = {
    cardNumber: '378282246310005',
    cvvNumber: '1234',
    expiryMonth: '12',
    expiryYear: '2021',
}

const ukBillingAddressTestData = {
    firstName: 'I Love',
    surname: 'Shopping',
    phoneNumber: '0987654321',
    billingCountry: 'United Kingdom',
    addressLine1: 'Colegrave House, 68-70 Berners Street',
    postCode: 'W1T 3NL',
    city: 'London',
}

module.exports = class PaymentPage {
    constructor() {
        this.waitForFullyLoadedPage()
    }

    /**
     * ELEMENTS ******************************************************************
     */
    get cardNumberSuccessfulValidationIconSelector() {
        return '.FormComponent-cardNumber .Input-validateIcon.Input-validateSuccess'
    }
    get cvvSuccessfulValidationIcon() {
        return '.FormComponent-cvv .Input-validateIcon.Input-validateSuccess'
    }
    get orderAndPayButton() {
        return 'button.Button.PaymentContainer-paynow'
    }
    get termsAndConditionsCheckboxSpan() {
        return '.FormComponent-isAcceptedTermsAndConditions .Checkbox-checkboxContainer'
    }
    get termsAndConditionsCheckboxInput() {
        return '.FormComponent-isAcceptedTermsAndConditions input[name="isAcceptedTermsAndConditions"]'
    }
    get disappearedLoadingOverlay() {
        return '.LoaderOverlay[aria-busy="false"]'
    }
    get firstNameValidationIcon() {
        return '.FormComponent-firstName .Input-validateIcon.Input-validateSuccess'
    }
    get surnameValidationIcon() {
        return '.FormComponent-lastName .Input-validateIcon.Input-validateSuccess'
    }
    get phoneNumberValidationIcon() {
        return '.FormComponent-telephone .Input-validateIcon.Input-validateSuccess'
    }
    get addressLine1ValidationIcon() {
        return '.FormComponent-address1 .Input-validateIcon.Input-validateSuccess'
    }
    get postCodeValidationIcon() {
        return '.FormComponent-postcode .Input-validateIcon.Input-validateSuccess'
    }
    get cityValidationIcon() {
        return '.FormComponent-city .Input-validateIcon.Input-validateSuccess'
    }
    get postCodeInputValidationIcon() {
        return '.FormComponent-postCode .Input-validateIcon.Input-validateSuccess'
    }
    get findAddressButton() {
        return '.ButtonContainer-findAddress > .Button'
    }
    get findAddressLink() {
        return '.YourAddress-link.YourAddress-link--right'
    }
    get accountCardRadioButtonSelector() {
        return 'input.RadioButton-input[value="ACCNT"]'
    }
    get countryDropdown() {
        return '.Select-select[name=country]'
    }
    get expiryMonthDropdown() {
        return 'select[name="expiryMonth"]'
    }
    get expiryYearDropdown() {
        return 'select[name="expiryYear"]'
    }
    get titleDropdown() {
        return '.Select-select[name="title"]'
    }
    get cardNumberInput() {
        return 'input[name="cardNumber"]'
    }
    get cvvInput() {
        return '.CardPaymentMethod-cvvFieldContainer input[name="cvv"]'
    }
    get firstNameInput() {
        return '[name="firstName"]'
    }
    get surnameInput() {
        return '[name="lastName"]'
    }
    get mobilePhoneNumberInput() {
        return '.Input-field[name="telephone"]'
    }
    get desktopPhoneNumberInput() {
        return 'section.FindAddress [type="tel"]'
    }
    get addressLine1Input() {
        return '.Input-field[name="address1"]'
    }
    get postCodeInput() {
        return '.Input-field[name="postcode"]'
    }
    get findAddressFormPostCodeInput() {
        return '.Input-field.Input-field-postCode'
    }
    get cityInput() {
        return '.Input-field[name="city"]'
    }
    get sameAsDeliveryAddressSpan() {
        return '.FormComponent-deliveryAsBilling .Checkbox-checkboxContainer'
    }
    get expiryMonthSelect() {
        return '[name=expiryMonth]'
    }
    get expiryYearSelect() {
        return '[name=expiryYear]'
    }
    get accountCardRadioButton() {
        return '.PaymentDetails-method:nth-child(3) .PaymentMethodOption-content.RadioButton-content'
    }
    get errorMessage() {
        return '.Message.is-error'
    }

    /**
     * ELEMENT ACTIONS ***********************************************************
     */
    waitForFullyLoadedPage() {
        waitForElementToBeVisible(this.orderAndPayButton)
        waitForElementToBeEnabled(this.orderAndPayButton)
        waitForElementToExist(
            this.disappearedLoadingOverlay,
            'Payment page, initial page load.'
        )
    }

    enterCardNumber(cardNumber) {
        // input cvv number and TAB out
        browser.setValue(this.cardNumberInput, cardNumber)
        browser.keys('Tab')
        waitForElementToBeVisible(
            this.cardNumberSuccessfulValidationIconSelector
        )
    }

    enterCardCVVNumber(cvvNumber) {
        // input cvv number and TAB out
        browser.setValue(this.cvvInput, cvvNumber)
        browser.keys('Tab')
        waitForElementToBeVisible(this.cvvSuccessfulValidationIcon)
    }

    enterExpiryMonth(month) {
        browser.click(this.expiryMonthSelect, month)
    }

    enterExpiryYear(year) {
        browser.selectByValue(this.expiryYearSelect, year)
    }

    chooseAccountCardPayment() {
        browser.scroll(this.accountCardRadioButtonSelector)
        browser.click(this.accountCardRadioButton)
        waitForElementToBeSelected(this.accountCardRadioButtonSelector)
    }

    switchToUsingFindAddress() {
        browser.click(this.sameAsDeliveryAddressSpan)
        waitForElementToBeEnabled(this.findAddressLink)
        browser.scroll(this.findAddressLink)
        browser.click(this.findAddressLink)
        waitForElementToBeVisible(this.findAddressButton)
    }

    enterFindAddressFormUKPostCode(postCode) {
        waitForElementToBeEnabled(this.findAddressFormPostCodeInput)
        browser.setValue(this.findAddressFormPostCodeInput, postCode)
        browser.keys('Tab')
        waitForElementToBeVisible(this.postCodeInputValidationIcon)
        waitForElementToBeEnabled(this.findAddressButton)
    }

    enterUKBillingDetailsManualEntry() {
        browser.setValue(
            this.firstNameInput,
            ukBillingAddressTestData.firstName
        )
        waitForElementToBeVisible(this.firstNameValidationIcon)

        browser.setValue(this.surnameInput, ukBillingAddressTestData.surname)
        waitForElementToBeVisible(this.surnameValidationIcon)

        browser.setValue(
            this.addressLine1Input,
            ukBillingAddressTestData.addressLine1
        )
        waitForElementToBeVisible(this.addressLine1ValidationIcon)

        browser.setValue(this.postCodeInput, ukBillingAddressTestData.postCode)
        waitForElementToBeVisible(this.postCodeValidationIcon)

        browser.setValue(this.cityInput, ukBillingAddressTestData.city)
        waitForElementToBeVisible(this.cityValidationIcon)

        if (isMobileLayout()) {
            waitForElementToBeEnabled(this.mobilePhoneNumberInput)
            $(this.mobilePhoneNumberInput).setValue(
                billingNameAndPhoneDetails.phoneNumber
            )
            browser.keys('Tab')
            waitForElementToBeVisible(this.phoneNumberValidationIcon)
        } else {
            browser.setValue(
                this.desktopPhoneNumberInput,
                ukBillingAddressTestData.phoneNumber
            )
            browser.keys('Tab')
            waitForElementToBeVisible(this.phoneNumberValidationIcon)
        }
    }

    enterUSBillingDetailsManualEntry() {
        this.enterUKBillingDetailsManualEntry()
        browser.selectByValue(this.countryDropdown, 'United States')
    }

    findAddress() {
        browser.scroll(this.findAddressButton)
        browser.click(this.findAddressButton)
        waitForElementToBeVisible(this.findAddressLink)
        waitForElementToBeEnabled(this.findAddressLink)
    }

    provideUKBillingDetailsUsingFindAddress() {
        this.providePersonalDetails()
        this.enterFindAddressFormUKPostCode(ukBillingAddressTestData.postCode)
        this.findAddress()
    }
    /**
     * USER ACTIONS **************************************************************
     */
    providePaymentDetails(paymentType) {
        switch (paymentType.toLowerCase()) {
            case 'visa':
                this.enterCardNumber(visaCardPaymentDetails.cardNumber)
                this.enterCardCVVNumber(visaCardPaymentDetails.cvvNumber)
                break
            case 'account card':
                this.chooseAccountCardPayment()
                this.enterCardNumber(accountCardPaymentDetails.cardNumber)
                this.enterCardCVVNumber(accountCardPaymentDetails.cvvNumber)
                break
            case 'amex':
                browser.setValue(
                    this.cardNumberInput,
                    amexCardPaymentDetails.cardNumber
                )
                browser.setValue(
                    this.cvvInput,
                    amexCardPaymentDetails.cvvNumber
                )
                browser.selectByValue(
                    this.expiryMonthDropdown,
                    amexCardPaymentDetails.expiryMonth
                )
                browser.selectByValue(
                    this.expiryYearDropdown,
                    amexCardPaymentDetails.expiryYear
                )
                break
            default:
                this.enterCardNumber()
                this.enterCVVNumber()
        }
    }

    acceptTCs() {
        waitForElementToBeVisible(this.termsAndConditionsCheckboxSpan)
        browser.scroll(this.termsAndConditionsCheckboxSpan)
        browser.click(this.termsAndConditionsCheckboxSpan)
        waitForElementToBeSelected(this.termsAndConditionsCheckboxInput)
    }

    placeOrder() {
        waitForElementToBeVisible(this.orderAndPayButton)
        browser.click(this.orderAndPayButton)
        if (process.env.PRODUCTION === 'true') {
            waitForElementToBeVisible(this.errorMessage)
        } else {
            return new ThankYouPage()
        }
    }

    providePersonalDetails() {
        waitForElementToBeVisible(this.titleDropdown)
        browser.selectByVisibleText(
            this.titleDropdown,
            billingNameAndPhoneDetails.title
        )
        expect($(this.titleDropdown).getText('option:checked')).to.equal(
            billingNameAndPhoneDetails.title
        )

        browser.setValue(
            this.firstNameInput,
            billingNameAndPhoneDetails.firstName
        )
        browser.keys('Tab')
        waitForElementToBeVisible(this.firstNameValidationIcon)

        browser.setValue(this.surnameInput, billingNameAndPhoneDetails.surname)
        browser.keys('Tab')
        waitForElementToBeVisible(this.surnameValidationIcon)

        if (isMobileLayout()) {
            waitForElementToBeEnabled(this.mobilePhoneNumberInput)
            $(this.mobilePhoneNumberInput).setValue(
                billingNameAndPhoneDetails.phoneNumber
            )
            browser.keys('Tab')
            waitForElementToBeVisible(this.phoneNumberValidationIcon)
        } else {
            browser.setValue(
                this.desktopPhoneNumberInput,
                billingNameAndPhoneDetails.phoneNumber
            )
            browser.keys('Tab')
            waitForElementToBeVisible(this.phoneNumberValidationIcon)
        }
    }

    provideBillingDetails(billingCountry, deliveryType, addressEntryMethod) {
        if (
            billingCountry === 'UK' &&
            deliveryType === 'Collect from Store' &&
            addressEntryMethod === 'Find Address'
        ) {
            this.switchToUsingFindAddress()
            this.provideUKBillingDetailsUsingFindAddress()
        }
        if (
            billingCountry === 'UK' &&
            deliveryType === 'Collect from ParcelShop' &&
            addressEntryMethod === 'Find Address'
        ) {
            this.provideUKBillingDetailsUsingFindAddress()
        }
    }

    enterBillingDetails(billingCountry, addressEntryMethod) {
        if (billingCountry === 'UK' && addressEntryMethod === 'manual entry') {
            this.enterUKBillingDetailsManualEntry()
        }
        if (billingCountry === 'US' && addressEntryMethod === 'manual entry') {
            this.enterUSBillingDetailsManualEntry()
        }
    }
}
