import disableAnimationForElement from '../support/lib/disableAnimationForElement'
import {
    waitForElementToBeVisible,
    waitForElementToExist,
    waitForElementToBeInvisible,
    waitForElementToNotExist,
} from '../support/lib/waitForWrappers'
import PDPPage from './pdp.page'
import {searchForProductsSorted} from '../support/lib/apiRequests'
import {isMobileLayout, isDesktopLayout} from '../support/lib/shared'
import {expect} from 'chai'

module.exports = class ProductDetailsPage {
    constructor() {
        this.waitForFullyLoadedPage()
    }

    /**
     * ELEMENTS ******************************************************************
     */
    get disappearedLoadingOverlaySelector() {
        return '.LoaderOverlay[aria-busy="false"]'
    }
    get filterModal() {
        return '.Refinements-content.is-shown'
    }
    get individualProductSelector() {
        return '.Product'
    }
    get lastVisibileProductSelector() {
        return `${this.individualProductSelector}:last-of-type`
    }
    get modalContainerSelector() {
        return '.Modal'
    }
    get numberOfDisplayedResultsSelectorDesktop() {
        return '.Filters-totalResults'
    }
    get numberOfDisplayedResultsSelectorMobile() {
        return '.PlpHeader-total.PlpHeader-totalValue'
    }
    get productsListNameSelector() {
        return `.Product-name a`
    }
    get productsListSelector() {
        return '.ProductList-products'
    }
    get quickViewAddedToBagConfirmationBoxSelector() {
        return '.InlineConfirm'
    }
    get quickViewAddToBagButtonSelector() {
        return '.AddToBag .Button'
    }
    get quickViewButtonSelector() {
        return 'button.ProductQuickViewButton'
    }
    get quickViewSeeFullDetailsButtonSelector() {
        return '.ProductQuickview-link.Button.Button--secondary'
    }
    get sortOptionsDropdownSelector() {
        return '.Select-select[name="sortSelector"]'
    }
    get filterButton() {
        return '.Button.Filters-refineButton'
    }
    get modalCloseIcon() {
        return '.Modal.is-shown .Modal-closeIcon'
    }
    get filterModalApplyButton() {
        return '.Button.Refinements-applyButton'
    }
    get filterValuesWrapper() {
        return '.Accordion-wrapper'
    }
    get filterOptionMobile() {
        return '.Refinements-content .ValueOption-item'
    }
    get mobileFilterHeaderBase() {
        return '//* [contains (@class, "Refinements-content") ] //*[contains (@class, "RefinementList-accordionHeader") ]'
    }
    desktopFilterHeader(filterType) {
        return `//*[contains (@class, "RefinementList-label") ][.="${filterType}"]`
    }
    /* OBJECT ELEMENTS */
    get listOfProducts() {
        return $$(this.individualProductSelector)
    }
    filterCategory(filterType) {
        const filterGroups = $$(this.desktopFilterHeader(filterType))
        if (isDesktopLayout()) return filterGroups[0]
        if (isMobileLayout()) return filterGroups[1]
    }
    firstFilterValue(filterType) {
        if (isDesktopLayout())
            return $(
                `${this.desktopFilterHeader(
                    filterType
                )}/following:: *[contains (@class, "Checkbox-checkboxContainer") ]`
            )
        if (isMobileLayout())
            return $(
                `${
                    this.mobileFilterHeaderBase
                }[.="${filterType}"]//following:: button`
            )
    }
    waitForFirstCheckboxTickedToExist(filterType) {
        const xpathFirstCheckboxTicked = isDesktopLayout()
            ? `${this.desktopFilterHeader(
                  filterType
              )}/following:: *[contains (@class, "Checkbox-checkboxContainer") ]/ *[contains (@class, "Checkbox-field") ][@aria-checked="true"]`
            : `${
                  this.mobileFilterHeaderBase
              }// span[contains (text(), '${filterType}') ]//following:: button[@aria-pressed="true"]`
        waitForElementToExist(xpathFirstCheckboxTicked)
    }

    /**
     * ELEMENT ACTIONS ***********************************************************
     */
    checkProductListIsSorted(order) {
        let sortMode = ''
        if (order === 'Price - Low To High') {
            sortMode = 'Price Ascending'
        } else if (order === 'Price - High To Low') {
            sortMode = 'Price Descending'
        }

        const apiPriceList = searchForProductsSorted(
            browser.options.testData.currentCategoryId,
            sortMode
        ).map((e) => {
            return {
                productName: e.name,
                unitPrice: parseInt(e.unitPrice, 10),
            }
        })

        // returns array of values which don't match their counterpart in the api array
        const unmatchedValues = this.productsNameList.filter((val, index) => {
            return val !== apiPriceList[index].productName
        })

        // fail test if unsorted values array is not empty
        expect(unmatchedValues.length).to.equal(
            0,
            `Values in array are not sorted. \n
      Products returned from the API: ${apiPriceList} \n
      Product names on this page: ${this.productsNameList} \n
      Unmatched product names: ${unmatchedValues}`
        )
    }

    get productsNameList() {
        // getHTML() is necessary as getText() only returns strings when the
        // element is interactable. This approach is recommended by webdriverIO.
        return browser.getHTML(this.productsListNameSelector, false)
    }

    waitForFullyLoadedPage() {
        waitForElementToBeVisible(this.productsListSelector)
    }

    /**
     * USER ACTIONS **************************************************************
     */
    getNumberOfDisplayedResults() {
        if (isMobileLayout()) {
            return browser.getText(this.numberOfDisplayedResultsSelectorMobile)
        }
        if (isDesktopLayout()) {
            return browser
                .getText(this.numberOfDisplayedResultsSelectorDesktop)
                .replace(/\D+/, '') // digits only
        }
    }

    addToBagFromQuickView() {
        browser.click(this.quickViewAddToBagButtonSelector)

        waitForElementToExist(
            this.disappearedLoadingOverlaySelector,
            'PLP, after adding to bag in quick view.'
        )
        waitForElementToBeVisible(
            this.quickViewAddedToBagConfirmationBoxSelector
        )
    }

    applyFilters() {
        // Not necessary on desktop, filters get directly applied there
        if (isMobileLayout()) {
            waitForElementToBeVisible(this.filterModalApplyButton)
            browser.click(this.filterModalApplyButton)
            waitForElementToNotExist(this.filterModal)
        }
        // Wait for filters to get applied (new results should return)
        if (isDesktopLayout()) {
            this.waitForFullyLoadedPage()
        }
    }

    closeQuickView() {
        disableAnimationForElement(this.modalContainerSelector)
        browser.click(this.modalCloseIcon)
    }

    openFilters() {
        // Mobile only: Open the filters in case they aren't already open.
        // On desktop, filters are already visible without user interaction.
        if (!browser.isVisible(this.filterModal) && isMobileLayout()) {
            browser.click(this.filterButton)
            waitForElementToBeVisible(this.filterModal)
        }
    }

    selectSubcategoryProductFromPlp() {
        browser.click(`a=${browser.options.testData.subCategoryProduct.name}`)
        return new PDPPage()
    }

    selectLastImageFromPlp() {
        const lastVisibleProductLink = `${this.lastVisibileProductSelector} a`
        waitForElementToBeVisible(lastVisibleProductLink)
        browser.click(lastVisibleProductLink)
        return new PDPPage()
    }

    selectQuickView() {
        this.listOfProducts.map((product) => {
            if (
                product.element('header a').getText() ===
                browser.options.testData.subCategoryProduct.name
            ) {
                waitForElementToBeVisible(this.quickViewButtonSelector)
                product.$(this.quickViewButtonSelector).click()
            }
        })
        waitForElementToBeVisible(this.quickViewSeeFullDetailsButtonSelector)
        waitForElementToExist(
            this.disappearedLoadingOverlaySelector,
            'PLP, after selecting quick view.'
        )
        waitForElementToBeVisible(this.quickViewAddToBagButtonSelector)
    }

    sortByPrice(order) {
        const sortOptionsMap = {
            'Price - Low To High': 'Price Ascending',
            'Price - High To Low': 'Price Descending',
        }

        browser.selectByValue(
            this.sortOptionsDropdownSelector,
            sortOptionsMap[order]
        )
        waitForElementToExist(
            this.disappearedLoadingOverlaySelector,
            'PLP, after sorting products by price.'
        )
        waitForElementToBeVisible(this.productsListNameSelector)
    }

    selectDefaultProductFromPlp() {
        browser.click(`a=${browser.options.testData.defaultSearchTerm.name}`)
        return new PDPPage()
    }

    scrollToBottom() {
        waitForElementToBeVisible(this.lastVisibileProductSelector)
        // Scroll a bit further down to ensure new products are being loaded
        browser.scroll(this.lastVisibileProductSelector, 0, 1000)
    }

    waitForMoreProductsBeingLoaded() {
        waitForElementToBeVisible(
            `${this.individualProductSelector}:nth-child(21)`,
            'Product 21 not visible: more products have not been loaded.'
        )
    }

    filterProductsByGivenFilter(filterType) {
        browser.options.testData.plpDisplayedProducts.beforeFilter = this.getNumberOfDisplayedResults()
        this.openFilters()
        this.filterCategory(filterType).click()
        this.firstFilterValue(filterType).click()
        this.waitForFirstCheckboxTickedToExist(filterType)
        // Close the accordion with all filter options visible
        this.filterCategory(filterType).click()
        const visibleFilterOption = isMobileLayout()
            ? this.filterOptionMobile
            : this.filterValuesWrapper
        waitForElementToBeInvisible(visibleFilterOption)
    }
}
