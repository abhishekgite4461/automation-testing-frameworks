import {waitForElementToBeVisible} from '../support/lib/waitForWrappers'

module.exports = class MyAccount {
    constructor() {
        this.waitForFullyLoadedPage()
    }

    /**
     * ELEMENTS ******************************************************************
     */
    get myAccountContainer() {
        return '.MyAccount'
    }

    /**
     * ELEMENT ACTIONS ***********************************************************
     */
    waitForFullyLoadedPage() {
        waitForElementToBeVisible(
            this.myAccountContainer,
            'MyAccount page, initial page load.'
        )
    }
}
