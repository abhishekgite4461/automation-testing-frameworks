import {expect} from 'chai'
import _ from 'lodash'
import {
    waitForElementToBeVisible,
    waitForElementToBeEnabled,
    waitForElementToExist,
    waitForElementToBeSelected,
} from '../support/lib/waitForWrappers'
import {isDesktopLayout, isMobileLayout} from '../support/lib/shared'
import StoreLocator from './storeLocator.page'
import PaymentPage from './payment.page'

const personalDetails = {
    userFirstName: 'I Love',
    userSurname: 'Shopping',
    userPhoneNumber: '0987654321',
    title: 'Dr',
}

const deliveryAddressDetails = {
    ukPostCode: 'W1T 3NL',
    usPostCode: '90210',
    userCity: 'London',
    addressLineOne: 'Sesame Street',
}

module.exports = class DeliveryPage {
    constructor(skipFullPageLoadCheck) {
        if (!skipFullPageLoadCheck) {
            this.waitForFullyLoadedPage()
        }
    }

    /**
     * ELEMENTS ******************************************************************
     */
    get firstNameInputValidationIcon() {
        return '.FormComponent-firstName .Input-validateIcon.Input-validateSuccess'
    }
    get homeExpressRadio() {
        return '.DeliveryMethod [for=delivery-method-home_express] input'
    }
    get surnameInputValidationIcon() {
        return '.FormComponent-lastName .Input-validateIcon.Input-validateSuccess'
    }
    get phoneNumberInputValidationIcon() {
        return '.FormComponent-telephone .Input-validateIcon.Input-validateSuccess'
    }
    get addressLineOneInputValidationIcon() {
        return '.FormComponent-address1 .Input-validateIcon.Input-validateSuccess'
    }
    get findAddressPostCodeInputValidationIcon() {
        return '.FindAddress .FormComponent-postCode .Input-validateIcon.Input-validateSuccess'
    }
    get yourAddressPostCodeInputValidationIcon() {
        return '.YourAddress-form .FormComponent-postcode .Input-validateIcon.Input-validateSuccess'
    }
    get cityInputValidationIcon() {
        return '.FormComponent-city .Input-validateIcon.Input-validateSuccess'
    }
    get findAddressButton() {
        return '.ButtonContainer-findAddress > .Button'
    }
    get findAddressLink() {
        return '.YourAddress-link.YourAddress-link--right'
    }
    get CFSExpressDeliveryRadioButton() {
        return '.StoreDeliveryV2 [for="delivery-method-store_express"]'
    }
    get deliveryPointLocationInput() {
        return '#UserLocatorInput'
    }
    get desktopOrderSummaryContainer() {
        return '.CheckoutBagSide'
    }
    get disappearedLoadingOverlay() {
        return '.LoaderOverlay[aria-busy="false"]'
    }
    get homeDeliveryRadioButton() {
        return '[for="delivery-option-home"] [name="deliveryLocationType"]'
    }
    get orderSummaryButton() {
        return '.Button.QuickViewOrderSummary-button'
    }
    get searchForStoresGoButton() {
        return '.Button.UserLocator-goButton'
    }
    get homeExpressDeliveryDateDropdownSelector() {
        return 'select[name="home_express"]'
    }
    get countryDropdown() {
        return '.Select-select[name="country"]'
    }
    get titleDropdown() {
        return '.Select-select[name="title"]'
    }
    get addressLineOneInput() {
        return '[name=address1]'
    }
    get firstNameInput() {
        return '.Input-field.Input-field-firstName'
    }
    get homeExpressButton() {
        return '.DeliveryMethod [for=delivery-method-home_express]'
    }
    get homeExpressDeliveryDateDropdownOptions() {
        return $$(`${this.homeExpressDeliveryDateDropdownSelector} > option`)
    }
    get CFSExpressDeliveryLabel() {
        return '.FormComponent-deliveryMethod[for="delivery-method-store_express"]'
    }
    get cityManualInput() {
        return '.Input-field[name="city"]'
    }
    get enterAddressManually() {
        return '.FindAddress-link'
    }
    get desktopPhoneNumberInput() {
        return '.Input-field.Input-field-telephone'
    }
    get findAddressPostCodeInput() {
        return '.FindAddress .Input-field.Input-field-postCode'
    }
    get homeDeliveryLabel() {
        return '.FormComponent-deliveryLocationType[for="delivery-option-home"]'
    }
    get mobilePhoneNumberInput() {
        return '.DetailsForm .Input-field.Input-field-telephone'
    }
    get parcelShopDeliveryOptionDiv() {
        return '.DeliveryOption.DeliveryOption--parcelshop'
    }
    get proceedToPaymentButton() {
        return '.Button.DeliveryContainer-nextButton'
    }
    get surnameInput() {
        return '.Input-field.Input-field-lastName'
    }
    get yourAddressPostCodeInput() {
        return '.YourAddress-form .Input-field.Input-field-postcode'
    }
    get deliveryOption() {
        return '.FormComponent-deliveryLocationType'
    }
    get storeAddress() {
        return '.StoreDeliveryV2-storeAddress'
    }

    /**
     * ELEMENT ACTIONS ***********************************************************
     */
    waitForFullyLoadedPage() {
        if (isMobileLayout()) {
            waitForElementToBeVisible(this.orderSummaryButton)
        } else {
            waitForElementToBeVisible(this.desktopOrderSummaryContainer)
        }
        waitForElementToExist(
            this.disappearedLoadingOverlay,
            'Delivery Page initial page load.'
        )
    }

    enterFirstName() {
        waitForElementToBeEnabled(this.firstNameInput)
        browser.setValue(this.firstNameInput, personalDetails.userFirstName)
        browser.keys('Tab')
        waitForElementToBeVisible(this.firstNameInputValidationIcon)
    }

    enterCity() {
        waitForElementToBeEnabled(this.cityManualInput)
        browser.setValue(this.cityManualInput, deliveryAddressDetails.userCity)
        browser.keys('Tab')
        waitForElementToBeVisible(this.cityInputValidationIcon)
    }

    enterAddressLineOne() {
        browser.setValue(
            this.addressLineOneInput,
            deliveryAddressDetails.addressLineOne
        )
        browser.keys('Tab')
        waitForElementToBeVisible(this.addressLineOneInputValidationIcon)
    }

    enterSurname() {
        waitForElementToBeEnabled(this.surnameInput)
        browser.setValue(this.surnameInput, personalDetails.userSurname)
        browser.keys('Tab')
        waitForElementToBeVisible(this.surnameInputValidationIcon)
    }

    enterPhoneNumber() {
        if (isDesktopLayout()) {
            waitForElementToBeEnabled(this.desktopPhoneNumberInput)
            browser.setValue(
                this.desktopPhoneNumberInput,
                personalDetails.userPhoneNumber
            )
        } else if (isMobileLayout()) {
            waitForElementToBeEnabled(this.mobilePhoneNumberInput)
            browser.setValue(
                this.mobilePhoneNumberInput,
                personalDetails.userPhoneNumber
            )
        }
        browser.keys('Tab')
        waitForElementToBeVisible(this.phoneNumberInputValidationIcon)
    }

    enterPostCode(deliveryCountry, addressEntryMethod) {
        if (deliveryCountry === 'uk' && addressEntryMethod === 'find address') {
            browser.setValue(
                this.findAddressPostCodeInput,
                deliveryAddressDetails.ukPostCode
            )
            browser.keys('Tab')
            waitForElementToBeVisible(
                this.findAddressPostCodeInputValidationIcon
            )
            waitForElementToBeEnabled(this.findAddressButton)
        }

        if (addressEntryMethod === 'manual entry') {
            if (deliveryCountry === 'uk') {
                browser.setValue(
                    this.yourAddressPostCodeInput,
                    deliveryAddressDetails.ukPostCode
                )
            }
            if (deliveryCountry === 'us') {
                browser.setValue(
                    this.yourAddressPostCodeInput,
                    deliveryAddressDetails.usPostCode
                )
            }
            browser.keys('Tab')
            waitForElementToBeVisible(
                this.yourAddressPostCodeInputValidationIcon
            )
        }
    }

    enterTitle() {
        waitForElementToBeVisible(this.titleDropdown)
        browser.selectByVisibleText(this.titleDropdown, personalDetails.title)
        expect($(this.titleDropdown).getText('option:checked')).to.equal(
            personalDetails.title
        )
    }

    providePersonalDetails() {
        this.enterFirstName()
        this.enterSurname()
        this.enterTitle()
        this.enterPhoneNumber()
    }

    provideDeliveryAddress(deliveryCountry, addressEntryMethod) {
        if (
            deliveryCountry.toLowerCase() === 'uk' &&
            addressEntryMethod.toLowerCase() === 'find address'
        ) {
            this.enterPostCode(
                deliveryCountry.toLowerCase(),
                addressEntryMethod.toLowerCase()
            )
            this.findAddress()
        }
        if (
            deliveryCountry.toLowerCase() === 'us' &&
            addressEntryMethod.toLowerCase() === 'manual entry'
        ) {
            this.enterAddressLineOne()
            this.enterPostCode(
                deliveryCountry.toLowerCase(),
                addressEntryMethod.toLowerCase()
            )
            this.enterCity()
        }
        if (
            deliveryCountry.toLowerCase() === 'uk' &&
            addressEntryMethod.toLowerCase() === 'manual entry'
        ) {
            browser.scroll(this.enterAddressManually)
            browser.click(this.enterAddressManually)
            this.enterAddressLineOne()
            this.enterPostCode(
                deliveryCountry.toLowerCase(),
                addressEntryMethod.toLowerCase()
            )
            this.enterCity()
        }
    }

    proceedToPayment() {
        waitForElementToBeVisible(this.proceedToPaymentButton)
        browser.click(this.proceedToPaymentButton)
        return new PaymentPage()
    }

    findAddress() {
        browser.click(this.findAddressButton)
        waitForElementToBeVisible(this.findAddressLink)
        waitForElementToBeEnabled(this.findAddressLink)
    }

    chooseCFSExpressDeliveryType() {
        browser.click(this.CFSExpressDeliveryLabel)
        waitForElementToBeSelected(this.CFSExpressDeliveryRadioButton, 60000)
    }

    chooseHomeStandardDeliveryType() {}

    chooseExpressShippingType() {
        browser.scroll(this.homeExpressRadio)
        waitForElementToBeVisible(this.homeExpressButton)
        browser.click(this.homeExpressButton)
        waitForElementToBeSelected(this.homeExpressRadio)
        waitForElementToExist(this.disappearedLoadingOverlay)
    }

    chooseHomeExpressDeliveryOption() {
        waitForElementToBeVisible(this.homeDeliveryLabel)
        browser.click(this.homeDeliveryLabel)
        waitForElementToBeSelected(this.homeDeliveryRadioButton)
        waitForElementToExist(this.disappearedLoadingOverlay)
        this.chooseExpressShippingType()
    }
    /**
     * USER ACTIONS **************************************************************
     */
    provideDeliveryDetails(deliveryCountry, addressEntryMethod) {
        this.providePersonalDetails()
        this.provideDeliveryAddress(deliveryCountry, addressEntryMethod)
        this.proceedToPayment()
    }

    chooseDeliveryPointAtIndex(index) {
        const storeLocator = new StoreLocator()
        storeLocator.chooseDeliveryPointAtIndex(index)
    }

    chooseDeliveryType(type) {
        switch (type.toLowerCase()) {
            case 'collect from store express':
                this.chooseCFSExpressDeliveryType()
                break
            case 'home express':
                this.chooseHomeExpressDeliveryOption()
                break
            default:
                this.chooseHomeStandardDeliveryType()
        }
    }

    chooseDeliveryOptions(deliveryOption) {
        if (deliveryOption === 'Home Express') {
            this.chooseHomeExpressDeliveryOption()
        }
        if (deliveryOption === 'US Express') {
            this.chooseExpressShippingType()
        }
        if (deliveryOption === 'ParcelShop') {
            browser.click(this.parcelShopDeliveryOptionDiv)
            waitForElementToBeVisible(this.deliveryPointLocationInput)
        }
    }

    chooseDeliveryDate(dateNumber) {
        const dateToSelect = dateNumber - 1
        waitForElementToBeVisible(this.homeExpressDeliveryDateDropdownSelector)
        browser.selectByIndex(
            this.homeExpressDeliveryDateDropdownSelector,
            dateToSelect
        )
        waitForElementToExist(this.disappearedLoadingOverlay)
        const dateWeSelected = this.homeExpressDeliveryDateDropdownOptions[
            dateToSelect
        ]
        // eslint-disable-next-line no-unused-expressions
        expect(dateWeSelected.isSelected()).to.be.true
    }

    providePersonalDeliveryDetails() {
        this.providePersonalDetails()
        this.proceedToPayment()
    }

    selectDeliveryCountry(deliveryCountry) {
        waitForElementToBeVisible(this.countryDropdown)
        browser.selectByVisibleText(this.countryDropdown, deliveryCountry)
        waitForElementToExist(this.disappearedLoadingOverlay)
        expect($(this.countryDropdown).getText('option:checked')).to.equal(
            deliveryCountry
        )
    }

    /**
     *  Returns the "for" attribute of the selected DOM element
     * @returns {string} */
    getSelectedDeliveryOption() {
        waitForElementToExist(this.disappearedLoadingOverlay)
        const selection = _.find($$(this.deliveryOption), (option) => {
            return option.$('input').isSelected() === true
        })
        return selection.getAttribute('for')
    }

    getStoreAddress() {
        return $(this.storeAddress)
    }
}
