import {
    waitForElementToBeVisible,
    waitForElementToBeEnabled,
    waitForElementToExist,
} from '../support/lib/waitForWrappers'
import {generateRandomEmail} from '../support/lib/shared'
import DeliveryPage from './delivery.page'
import DeliveryPaymentPage from './deliveryPayment.page'
import MyAccountPage from './myAccount.page'

const userPassword = 'monty1'

module.exports = class SignIn {
    constructor() {
        this.waitForFullyLoadedPage()
    }

    /**
     * ELEMENTS ******************************************************************
     */
    get deliveryContainerSelector() {
        return '.DeliveryContainer'
    }
    get disappearedLoadingOverlaySelector() {
        return '.LoaderOverlay[aria-busy="false"]'
    }
    get signInButtonSelector() {
        return '.Login-submitButton'
    }
    get registerButton() {
        return '.Register-saveChanges'
    }
    get registerConfirmPasswordInput() {
        return '.Register .Input-field-passwordConfirm'
    }
    get registerEmailInput() {
        return '.Register .Input-field-email'
    }
    get registerPasswordInput() {
        return '.Register .Input-field-password'
    }
    get signInEmailInput() {
        return '.Login .Input-field-email'
    }
    get signInPasswordInput() {
        return '.Login .Input-field-password'
    }
    get weeklyEmailCheckBox() {
        return '.Checkbox-check'
    }

    /**
     * ELEMENT ACTIONS ***********************************************************
     */
    waitForFullyLoadedPage() {
        waitForElementToBeVisible(this.signInButtonSelector, undefined)
        waitForElementToBeEnabled(this.signInButtonSelector)
        waitForElementToExist(
            this.disappearedLoadingOverlaySelector,
            'Signin page, initial page load.'
        )
    }

    setRegisterEmail() {
        browser.setValue(this.registerEmailInput, generateRandomEmail())
    }

    setSignInEmail(email) {
        browser.setValue(this.signInEmailInput, email)
    }

    setRegisterPassword() {
        browser.setValue(this.registerPasswordInput, userPassword)
    }

    setSignInPassword(password) {
        browser.setValue(this.signInPasswordInput, password)
    }

    setConfirmRegisterPassword() {
        browser.setValue(this.registerConfirmPasswordInput, userPassword)
    }

    doNotSubscribeToNewsletter() {
        browser.click(this.weeklyEmailCheckBox)
    }

    clickRegisterButton() {
        browser.click(this.registerButton)
    }

    clickSignInButton() {
        browser.click(this.signInButtonSelector)
    }

    /**
     * USER ACTIONS **************************************************************
     */
    registerNewUser(location) {
        this.setRegisterEmail()
        this.setRegisterPassword()
        this.setConfirmRegisterPassword()
        this.doNotSubscribeToNewsletter()
        this.clickRegisterButton()

        return location !== 'during checkout'
            ? new MyAccountPage()
            : new DeliveryPage()
    }

    loginAsExistingUser(location, checkoutProfileType) {
        let email
        let password

        switch (checkoutProfileType) { // eslint-disable-line default-case
            case 'partial':
                email =
                    browser.options.testData
                        .userCredentialsPartialCheckoutProfile.email
                password =
                    browser.options.testData
                        .userCredentialsPartialCheckoutProfile.password
                break
            case 'merged bag':
                email =
                    browser.options.testData
                        .userCredentialsWithMergedBagCheckoutProfile.email
                password =
                    browser.options.testData
                        .userCredentialsWithMergedBagCheckoutProfile.password
                break
            case 'full':
                email =
                    browser.options.testData
                        .userCredentialsWithFullCheckoutProfile.email
                password =
                    browser.options.testData
                        .userCredentialsWithFullCheckoutProfile.password
                break
        }

        this.setSignInEmail(email)
        this.setSignInPassword(password)
        this.clickSignInButton()

        if (location !== 'during checkout') return new MyAccountPage()
        if (checkoutProfileType === 'full') return new DeliveryPaymentPage()
        // Skip page load check for merged bag due to the overlay
        if (checkoutProfileType === 'merged bag')
            return new DeliveryPaymentPage(true)
        return new DeliveryPage()
    }
}
