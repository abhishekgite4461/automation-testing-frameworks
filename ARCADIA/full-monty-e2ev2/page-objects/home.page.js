import {waitForElementToBeVisible} from '../support/lib/waitForWrappers'

module.exports = class HomePage {
    constructor() {
        this.waitForFullyLoadedPage()
    }

    /**
     * ELEMENTS ******************************************************************
     */
    get cmsContentFrame() {
        return '.Main-inner'
    }

    /**
     * ELEMENT ACTIONS ***********************************************************
     */
    waitForFullyLoadedPage() {
        waitForElementToBeVisible(
            this.cmsContentFrame,
            "Home Page: CMS Frame didn't load."
        )
    }
}
