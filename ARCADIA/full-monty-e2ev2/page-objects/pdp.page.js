import {
    waitForElementToBeVisible,
    waitForPartialUrl,
    waitForElementToBeEnabled,
    waitForElementToExist,
} from '../support/lib/waitForWrappers'
import disableAnimation from './../support/lib/disableAnimationForElement'
import routes from '../support/constants/routes'
import {isMobileLayout, isDesktopLayout} from '../support/lib/shared'
import SignInPage from './signIn.page'
import DeliveryPage from './delivery.page'
import DeliveryPaymentPage from './deliveryPayment.page'
import MiniBag from './miniBag.page'
import StoreLocator from './storeLocator.page'

const addToBagButtonNotVisible =
    'PDP: The Add to Bag button is not visible. ' +
    'A common cause of this is the product having been in stock at time of the ' +
    'product validation at the beginning of the test suite but turning out of ' +
    'stock later on.'

module.exports = class ProductDetailsPage {
    constructor(skipFullPageLoadCheck) {
        if (!skipFullPageLoadCheck) {
            this.waitForFullyLoadedPage()
        }
    }

    /**
     * ELEMENTS ******************************************************************
     */
    /** STRING SELECTORS */
    get addToBagButtonSelector() {
        return '.AddToBag button'
    }
    get addToBagConfirmationModalCloseIcon() {
        return '.Modal-closeIcon'
    }
    get buyItNowButton() {
        return '.Button.BuyNow'
    }
    get desktopAddToBagConfirmationModalDivSelector() {
        return '.InlineConfirm'
    }
    get disappearedLoadingOverlaySelector() {
        return '.LoaderOverlay[aria-busy="false"]'
    }
    get mainProductImage() {
        return '.ProductDetail-media .is-selected img'
    }
    get modalCheckoutNowButtonSelector() {
        return '.AddToBagConfirm-goToCheckout'
    }
    get sizeButtonsSelector() {
        return 'ProductSizes-item'
    }
    get modal() {
        return '.Modal'
    }
    get findInStoreButtonMobile() {
        return '.FindInStoreButton'
    }
    get findInStoreButtonDesktop() {
        return '.FindInStoreButton-desktop'
    }

    /**
     * ELEMENT ACTIONS ***********************************************************
     */
    waitForFullyLoadedPage() {
        waitForElementToBeVisible(
            this.addToBagButtonSelector,
            addToBagButtonNotVisible
        )
        waitForElementToBeVisible(this.mainProductImage)
        waitForElementToExist(
            this.disappearedLoadingOverlaySelector,
            'PDP, initial page load.'
        )
    }

    clickCheckoutNowButtonFromModal(userType) {
        waitForElementToBeVisible(this.modalCheckoutNowButtonSelector)
        if (isMobileLayout()) {
            browser.click(this.modalCheckoutNowButtonSelector)
        } else {
            const miniBag = new MiniBag()
            waitForElementToBeVisible(miniBag.checkoutNowButton)
            browser.click(miniBag.checkoutNowButton)
        }

        if (userType === 'a new') {
            return new DeliveryPage()
        } else if (
            userType === 'an anonymous' ||
            userType === 'returning user'
        ) {
            // TODO review the logic around returning user
            waitForPartialUrl(
                routes.checkoutLogin,
                'PDP, after checkout now from modal as returning user: Expected to land on checkout login.'
            )
            return new SignInPage()
        }
        return new DeliveryPaymentPage()
    }

    /**
     * USER ACTIONS **************************************************************
     */
    addToBag() {
        // Pause is necessary due to server-side render issues: When the product
        // page has finished loading for a product without sizes, we still
        // cannot click on the add to bag button immediately. There are race
        // conditions where it would raise an error "You need to select a size
        // first" although there are no sizes visible.
        browser.pause(700)
        browser.moveToObject(this.addToBagButtonSelector)
        browser.click(this.addToBagButtonSelector)
        if (isMobileLayout()) {
            waitForElementToBeVisible(this.modalCheckoutNowButtonSelector)
        }
        if (isDesktopLayout()) {
            waitForElementToBeVisible(
                this.desktopAddToBagConfirmationModalDivSelector
            )
        }
    }

    buyItNow() {
        browser.click(this.buyItNowButton)
        waitForPartialUrl(
            routes.checkout,
            'PDP, buy it now: expected checkout page.'
        )
    }

    dismissModal() {
        if (isMobileLayout()) {
            disableAnimation(this.modal)
            waitForElementToBeVisible(this.addToBagConfirmationModalCloseIcon)
            waitForElementToBeEnabled(this.addToBagConfirmationModalCloseIcon)
            browser.click(this.addToBagConfirmationModalCloseIcon)
        }
        waitForElementToBeVisible(
            this.addToBagButtonSelector,
            addToBagButtonNotVisible
        )
    }

    chooseItemSize() {
        const availableItemSizeSelector = `//*[contains(@class, "${
            this.sizeButtonsSelector
        }")][.="${browser.options.testData.multipleSizesProduct.size}"]`
        waitForElementToBeVisible(availableItemSizeSelector)
        browser.click(availableItemSizeSelector)
    }

    selectFindInStore() {
        if (isMobileLayout()) {
            waitForElementToBeVisible(this.findInStoreButtonMobile)
            browser.click(this.findInStoreButtonMobile)
        } else {
            waitForElementToBeVisible(this.findInStoreButtonDesktop)
            browser.click(this.findInStoreButtonDesktop)
        }
        return new StoreLocator()
    }
}
