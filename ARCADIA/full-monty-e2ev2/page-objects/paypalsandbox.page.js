import {
    waitForElementToBeVisible,
    waitForElementToBeEnabled,
} from './../support/lib/waitForWrappers'

const paypalTimeout = 60000
const advertisementFetchPause = 5000

module.exports = class PayPalSandbox {
    get emailInput() {
        return '#email'
    }

    get passwordInput() {
        return '#password'
    }

    get loginButton() {
        return '#btnLogin'
    }

    get payNowButton() {
        return '#confirmButtonTop'
    }

    get shippingAddress() {
        return '#shippingForm'
    }

    loginAndPay(userData) {
        waitForElementToBeVisible(this.emailInput, '', paypalTimeout)
        waitForElementToBeVisible(this.passwordInput, '', paypalTimeout)
        browser.setValue(this.emailInput, userData.username)
        browser.setValue(this.passwordInput, userData.password)
        browser.click(this.loginButton)
        waitForElementToBeVisible(this.shippingAddress, '', paypalTimeout)
        waitForElementToBeEnabled(this.shippingAddress, '', paypalTimeout)
        // Paypal does occassionally make another request to fetch an
        // advertisement - during that thime, the Pay Now button is blocked.
        // Furthermore, Paypal's loading overlay causes underlying elements
        // already to appear visible to Selenium although they are not yet to
        // the user. As we don't have any control over it, we have to pause.
        browser.pause(advertisementFetchPause)
        waitForElementToBeVisible(this.payNowButton, '', paypalTimeout)
        waitForElementToBeEnabled(this.payNowButton, '', paypalTimeout)
        browser.click(this.payNowButton)
    }
}
