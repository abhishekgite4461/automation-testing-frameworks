import {buildBrandBaseUrl} from './../support/lib/brandURL'
import {deliveryOptionIds} from './../support/constants/deliveryOptions'

export const userWithFullCheckoutProfilePayload = (orderId) => ({
    smsMobileNumber: '',
    remoteIpAddress: '127.0.0.1',
    cardCvv: '123',
    orderDeliveryOption: {
        orderId,
        shippingCountry: 'United Kingdom',
        shipCode: 'S',
        deliveryType: 'HOME_STANDARD',
        shipModeId: deliveryOptionIds('Home Standard', browser.options.brand),
    },
    deliveryInstructions: '',
    returnUrl: `${buildBrandBaseUrl(
        `${browser.options.brand}uk`
    )}/order-complete?paymentMethod=VISA`,
    deliveryAddress: {
        address1: '129-137 Marylebone Road',
        address2: null,
        city: 'London',
        state: null,
        country: 'United Kingdom',
        postcode: 'NW1 5QD',
    },
    deliveryNameAndPhone: {
        firstName: 'Test',
        lastName: 'Test',
        telephone: '09876543212',
        title: 'Mr',
    },
    billingDetails: {
        address: {
            address1: '129-137 Marylebone Road ',
            address2: null,
            city: 'London',
            state: null,
            country: 'United Kingdom',
            postcode: 'NW1 5QD',
        },
        nameAndPhone: {
            firstName: 'Test',
            lastName: 'Test',
            telephone: '09876543212',
            title: 'Mr',
        },
    },
    creditCard: {
        expiryYear: '2020',
        expiryMonth: '12',
        cardNumber: '4444333322221111',
        type: 'VISA',
    },
})
