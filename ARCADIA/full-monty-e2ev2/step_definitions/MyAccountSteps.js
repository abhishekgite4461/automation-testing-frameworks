import {Given} from 'cucumber'
import NavBarPage from './../page-objects/navigationBar.page'
import SignInPage from './../page-objects/signIn.page'

Given(
    /^I login "(.*)" as a user with "(.*)" checkout profile$/,
    (location, checkoutProfileType) => {
        if (location !== 'during checkout') {
            const navBar = new NavBarPage()
            navBar.goToSignInPage()
        }
        const signIn = new SignInPage()
        signIn.loginAsExistingUser(location, checkoutProfileType)
    }
)

Given(/^I register\s*"(.*)"$/, (location) => {
    if (location !== 'during checkout') {
        const navBar = new NavBarPage()
        navBar.goToSignInPage()
    } else {
        const signIn = new SignInPage()
        signIn.registerNewUser(location)
    }
})
