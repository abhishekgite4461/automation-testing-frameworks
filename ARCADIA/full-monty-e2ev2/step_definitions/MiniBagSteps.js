import {Given, When, Then} from 'cucumber'
import MiniBag from './../page-objects/miniBag.page'
import NavigationBar from './../page-objects/navigationBar.page'
import {waitForElementToBeVisible} from './../support/lib/waitForWrappers'
import {isMobileLayout} from './../support/lib/shared'
import {assert, expect} from 'chai'

Given(
    /^I choose the "(.*)" delivery option from the bag$/,
    (deliveryOption) => {
        const miniBag = new MiniBag()
        miniBag.selectDeliveryOption(deliveryOption)
    }
)

Given(/^I proceed to checkout from the bag$/, () => {
    const miniBag = new MiniBag()
    miniBag.goToCheckout()
})

When(/^I change the current size$/, () => {
    const miniBag = new MiniBag()
    miniBag.updateSize()
})

When(/^I enter the "(.*)" promotion code$/, (promoCode) => {
    const miniBag = new MiniBag()
    miniBag.addFirstPromotionCode(promoCode)
})

When(/^I add another promotion code "(.*)"$/, (promoCode) => {
    const miniBag = new MiniBag()
    miniBag.addAnotherPromotionCode(promoCode)
})

When(/^I remove the first product from the bag$/, () => {
    const miniBag = new MiniBag()
    miniBag.removeProductFromMiniBag()
})

Then(/^the bag "(.*)" contains both items$/, (miniBagLocation) => {
    if (isMobileLayout()) {
        const navbar = new NavigationBar()
        navbar.openMiniBag()
    }
    const products = [
        browser.options.testData.secondaryOneSizeProduct.name,
        browser.options.testData.product.name,
    ]
    const miniBag = new MiniBag()
    const productsInMiniBag = miniBag.getProductsFromMiniBag(miniBagLocation)
    assert.deepEqual(productsInMiniBag, products)
})

Then(/^I see the second product in the bag$/, () => {
    const products = [browser.options.testData.watchProduct.name]
    const miniBag = new MiniBag()
    const productsInMiniBag = miniBag.getProductsFromMiniBag()
    assert.deepEqual(productsInMiniBag, products)
})

Then(/^I see the new size of the product in the minibag$/, () => {
    const miniBag = new MiniBag()
    const itemNewSize = miniBag.getItemSize()
    expect(itemNewSize).to.not.equal(
        browser.options.testData.multipleSizesProduct.size
    )
})

Then(/^I see the product in the minibag$/, () => {
    const navbar = new NavigationBar()
    navbar.openMiniBag()
    const miniBag = new MiniBag()
    miniBag.checkMiniBagForProduct()
})

Then(/^I see the total price in the minibag is discounted$/, () => {
    const miniBag = new MiniBag()
    waitForElementToBeVisible(miniBag.appliedDiscount)
})

Then(/^I see the "(.*)" promotion code applied$/, (promoCode) => {
    const miniBag = new MiniBag()
    expect(miniBag.getAppliedPromotionCodes()).to.contain(promoCode)
})

When(/I change the item quantity to "(.*)"$/, (itemQnt) => {
    const miniBag = new MiniBag()
    miniBag.updateItemQuantity(itemQnt)
})

Then(/I see that the item quantity is "(.*)"$/, (itemQnt) => {
    const miniBag = new MiniBag()
    const miniBagNewQuantity = miniBag.getItemQuantity()
    expect(miniBagNewQuantity).to.equal(itemQnt)
})
