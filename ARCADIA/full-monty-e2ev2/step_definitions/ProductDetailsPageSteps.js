import {Given, When, Then} from 'cucumber'
import {assert} from 'chai'
import PDPPage from './../page-objects/pdp.page'

Given(/^I add the product to the bag$/, () => {
    const pdp = new PDPPage()
    pdp.addToBag()
})

Given(/^I dismiss the add to bag confirmation modal$/, () => {
    const pdp = new PDPPage(true)
    pdp.dismissModal()
})

Given(
    /^I proceed to checkout from the confirmation modal as a "(.*)"$/,
    (userType) => {
        const pdp = new PDPPage(true) // Skip wait for fully loaded due to modal
        pdp.clickCheckoutNowButtonFromModal(userType)
    }
)

Given(/^I select an available size$/, () => {
    const pdp = new PDPPage()
    pdp.chooseItemSize()
})

Given(/^I select to pick up the product from a store$/, () => {
    const pdp = new PDPPage()
    pdp.selectFindInStore()
})

When(/^I choose to buy it now$/, () => {
    const pdp = new PDPPage()
    pdp.buyItNow()
})

Then(/^I see the PDP page for that product$/, () => {
    assert.isOk(new PDPPage(), 'Redirection to PDP did not pass.')
})
