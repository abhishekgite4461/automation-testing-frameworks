import {Given, Then} from 'cucumber'
import {logoutFromTheUrl} from './../support/lib/logoutFromUrl'
import buildTargetPageUrl from './../support/lib/buildTargetPageUrl'
import {expect} from 'chai'

Given(
    /^I visit the "(.*)" "(.*)" page(?: with promo code "(.*)")*$/,
    (siteCountry, pageName, promoCode) => {
        let targetPageFullUrl = buildTargetPageUrl(
            browser.options.brand,
            siteCountry,
            pageName,
            browser.options.target
        )
        if (promoCode) targetPageFullUrl += `?ARCPROMO_CODE=${promoCode}`
        // workaround for testing uk site in prod but jenkins slaves not being in UK, so geoIP blows up
        // this would need some love when e2e can do non-uk sites
        if (process.env.PRODUCTION === 'true') {
          browser.setCookie({name: 'GEOIP', value: 'GB'})
        }
        browser.url(targetPageFullUrl)
    }
)

Given(/^Logout$/, () => {
    // Note: This is used as workaround for the merged bag and will be deleted
    logoutFromTheUrl()
})

Then(/^I should land on the "(.*)" page$/, (pageName) => {
    const mapPageNameToUrl = {delivery: '/checkout/delivery'}
    expect(browser.getUrl()).to.contain(mapPageNameToUrl[pageName])
})
