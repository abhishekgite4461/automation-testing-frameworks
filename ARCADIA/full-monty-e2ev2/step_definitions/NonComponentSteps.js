import {Given, Then} from 'cucumber'
import {assert} from 'chai'

Given(/^I have the cookie "(.*)"$/, (cookie) => {
    const storedCookie = browser.getCookie(cookie)
    assert.exists(storedCookie)
    assert.containsAllKeys(storedCookie, ['value', 'name'])
})

Then(/^I do not have the cookie "(.*)"$/, (cookie) => {
    const storedCookie = browser.getCookie(cookie)
    assert.notExists(storedCookie)
})
