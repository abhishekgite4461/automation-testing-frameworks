import {Given} from 'cucumber'
import NavBarPage from './../page-objects/navigationBar.page'
import {isMobileLayout} from '../support/lib/shared'
import {
    parentCategoryName,
    subCategoryName,
} from './../support/constants/categories'
import {oneSizeProductsSearchTerm} from './../support/constants/searchTerms'

Given(/^I view the bag$/, () => {
    // Only necessary on mobile. On desktop, minibag opens automatically
    // after adding a product to the bag
    if (isMobileLayout()) {
        const navBar = new NavBarPage()
        navBar.openMiniBag()
    }
})

Given(/^I search for a product code "(.+)"$/, (country) => {
    const navBar = new NavBarPage()
    if (country === 'in the US') {
        navBar.searchForSpecificItem(
            browser.options.testData.productUS.lineNumber
        )
    } else {
        navBar.searchForSpecificItem(
            browser.options.testData.product.lineNumber
        )
    }
})

Given(/^I search for a search term$/, () => {
    const navBar = new NavBarPage()
    navBar.searchForTerm(oneSizeProductsSearchTerm(browser.options.brand))
})

Given(/^I select a parent category from the menu$/, () => {
    const navBar = new NavBarPage()
    navBar.goToCategoryPage(parentCategoryName(browser.options.brand))
})

Given(/^I select a subcategory$/, () => {
    const navBar = new NavBarPage()
    navBar.goToSubCategoryPage(subCategoryName(browser.options.brand))
})
