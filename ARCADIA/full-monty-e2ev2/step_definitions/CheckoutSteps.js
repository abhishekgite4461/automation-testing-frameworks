import {Given, When, Then} from 'cucumber'
import {expect} from 'chai'
import {deliveryOptionsCheckoutForAttributes} from './../support/constants/deliveryOptions'
import ThankYouPage from './../page-objects/thankYou.page'
import DeliveryPage from './../page-objects/delivery.page'
import PaymentPage from './../page-objects/payment.page'
import DeliveryPaymentPage from './../page-objects/deliveryPayment.page'
import StoreLocator from './../page-objects/storeLocator.page'
import PayPalSandbox from './../page-objects/paypalsandbox.page'
import routes from './../support/constants/routes'

Given(/^I continue shopping$/, () => {
    const thankYouPage = new ThankYouPage()
    thankYouPage.continueShopping()
})

Given(/^I search for a delivery point in "(.*)"$/, (location) => {
    const storeLocator = new StoreLocator()
    storeLocator.searchAndSelectDeliveryPoint(location)
})

Given(/^I choose location number "(.*)" in the list$/, (index) => {
    const storeLocator = new StoreLocator(true)
    storeLocator.chooseDeliveryPointAtIndex(index)
})

Given(/^I close the pick from store modal$/, () => {
    const storeLocator = new StoreLocator(true)
    storeLocator.closeModal()
})

Given(
    /^I look at the details for location number "(.*)" in the list$/,
    (index) => {
        const storeLocator = new StoreLocator(true)
        storeLocator.verifyStoreData(index)
    }
)

Given(/^I choose "(.*)" as the delivery type$/, (option) => {
    const deliveryPage = new DeliveryPage()
    deliveryPage.chooseDeliveryType(option)
})

Given(
    /^I enter my "(.*)" delivery address using the "(.*)" functionality$/,
    (deliveryCountry, addressEntryMethod) => {
        const deliveryPage = new DeliveryPage()
        deliveryPage.provideDeliveryDetails(deliveryCountry, addressEntryMethod)
    }
)

Given(
    /^I enter my "(.*)" "(.*)" billing details using the "(.*)" functionality$/,
    (billingCountry, deliveryType, addressEntryMethod) => {
        const paymentPage = new PaymentPage()
        paymentPage.provideBillingDetails(
            billingCountry,
            deliveryType,
            addressEntryMethod
        )
    }
)

Given(/^I enter my "(.*)" payment details$/, (paymentType) => {
    const paymentPage = new PaymentPage()
    paymentPage.providePaymentDetails(paymentType.toLowerCase())
})

Given(
    /^I provide delivery and payment details as a "(.*)" user$/,
    (userType) => {
        if (userType === 'a new') {
            const deliveryPage = new DeliveryPage()
            deliveryPage.provideDeliveryDetails()
            const paymentPage = new PaymentPage()
            paymentPage.providePaymentDetails()
        } else {
            const deliveryPaymentPage = new DeliveryPaymentPage()
            deliveryPaymentPage.providePaymentDetails()
        }
    }
)

Given(/^I increase the item quantity to "(.*)"$/, (value) => {
    const delPaymentPage = new DeliveryPaymentPage()
    delPaymentPage.increaseItemQuantityTo(value)
})

Given(/^I choose "(.*)" as the delivery option$/, (deliveryOption) => {
    const deliveryPage = new DeliveryPage()
    deliveryPage.chooseDeliveryOptions(deliveryOption)
})

Given(/^I choose delivery date number "(.*)" in the list$/, (dateNumber) => {
    const deliveryPage = new DeliveryPage()
    deliveryPage.chooseDeliveryDate(dateNumber)
})

Given(/^I provide my personal delivery details$/, () => {
    const deliveryPage = new DeliveryPage()
    deliveryPage.providePersonalDeliveryDetails()
})

Given(/^I choose "(.*)" as the delivery country$/, (deliveryCountry) => {
    const deliveryPage = new DeliveryPage()
    deliveryPage.selectDeliveryCountry(deliveryCountry)
})

Given(/^I choose "(.*)" as the payment method$/, (paymentMethod) => {
    const deliveryPaymentPage = new DeliveryPaymentPage()
    deliveryPaymentPage.selectPaymentMethod(paymentMethod)
})

Given(/^I change the payment method$/, () => {
    // "Change" button for delivery method only exists on Delivery&Payment page
    if (browser.getUrl().includes(routes.deliveryPayment)) {
        const deliveryPaymentPage = new DeliveryPaymentPage()
        deliveryPaymentPage.changePaymentMethod()
    }
})

When(
    /^I place the order as a "(.*)" user(?: with "(.*)")*$/,
    (userType, target) => {
        if (userType === 'new') {
            const paymentPage = new PaymentPage()
            paymentPage.acceptTCs()
            paymentPage.placeOrder()
        } else {
            const deliveryPaymentPage = new DeliveryPaymentPage()
            deliveryPaymentPage.acceptTCs()
            deliveryPaymentPage.placeOrder(target)
        }
    }
)

When(/^I can login and pay on PayPal$/, (dataTable) => {
    const userData = dataTable.hashes()[0]
    const paypal = new PayPalSandbox()
    paypal.loginAndPay(userData)
})

Then(/^I get prompted to review my bag$/, () => {
    const delPaymentPage = new DeliveryPaymentPage(true)
    delPaymentPage.closeWarningMergedBag()
})

Then(/^I see "(.*)" items in my purchase$/, (expectedQty) => {
    const thankYouPage = new ThankYouPage()
    expect(thankYouPage.getOrderQuantity()).to.equal(expectedQty)
})

Then(/^I see the total price is discounted$/, () => {
    const thankYouPage = new ThankYouPage()
    thankYouPage.existsDiscount()
})

Then(/^I see the confirmation for my order$/, () => {
    // Don't run assertions in production since payment will have failed
    if (process.env.PRODUCTION !== 'true') {
        const thankYouPage = new ThankYouPage()
        thankYouPage.verifyOrderConfirmation()
    }
})

Then(/^I see the "(.*)" delivery option is selected$/, (option) => {
    const deliveryPage = new DeliveryPage()
    expect(deliveryPage.getSelectedDeliveryOption()).equals(
        deliveryOptionsCheckoutForAttributes(option)
    )
})

Then(/^I see the store address details pre-filled$/, () => {
    const deliveryPage = new DeliveryPage()
    expect(deliveryPage.getStoreAddress().value).not.equals(null)
})
