import {Given} from 'cucumber'
import {
    createUserWithFullCheckoutProfile,
    createUserWithPartialProfile,
    selectProductsOneSize,
} from './../support/lib/apiRequests'
import {
    oneSizeProductsSearchTerm,
    multipleSizeProductsSearchTerm,
    secondaryOneSizeProductSearchTerm,
    tertiaryOneSizeProductSearchTerm,
} from './../support/constants/searchTerms'
import logger from './../support/logger'

const secondaryOneSizeProductCategory = secondaryOneSizeProductSearchTerm(
    browser.options.brand
)
const watchesProductCategory = tertiaryOneSizeProductSearchTerm(
    browser.options.brand
)

const getTestProductFromCategory = (testDataObject, categoryName) => {
    testDataObject = selectProductsOneSize(categoryName)
    validateTestProduct(testDataObject)
    testDataObject.pdpUrl = testDataObject.sourceUrl.substring(
        testDataObject.sourceUrl.indexOf('product')
    )
    return testDataObject
}

const validateTestProduct = (testProduct) => {
    if (testProduct.productId === 0 || testProduct.name === '') {
        // If we tried to get a test product and failed, we need to abort the test run
        throw new Error('No test product found. Stopping the test run.')
    }
}

const getTestProductForCheckoutProfile = () => {
    browser.options.testData.product = selectProductsOneSize()
    validateTestProduct(browser.options.testData.product)
}

const validateCheckoutProfile = (testDataObjectKey) => {
    if (testDataObjectKey.email === '') {
        throw new Error(`No valid user profile created. Stopping the test run.`)
    }
}

const createUserWithFullCheckoutProfileWrapper = () => {
    // Attempt to create a checkout user profile only if we don't already have one
    if (
        browser.options.testData.userCredentialsWithFullCheckoutProfile
            .email === ''
    ) {
        browser.options.testData.userCredentialsWithFullCheckoutProfile = createUserWithFullCheckoutProfile()
        validateCheckoutProfile(
            browser.options.testData.userCredentialsWithFullCheckoutProfile
        )
    }
}

const createUserWithFullCheckoutProfileMergedBagWrapper = () => {
    // Attempt to create a checkout user profile only if we don't already have one
    if (
        browser.options.testData.userCredentialsWithMergedBagCheckoutProfile
            .email === ''
    ) {
        browser.options.testData.userCredentialsWithMergedBagCheckoutProfile = createUserWithFullCheckoutProfile(
            'Merged bag'
        )
        validateCheckoutProfile(
            browser.options.testData.userCredentialsWithMergedBagCheckoutProfile
        )
    }
}

const createUserWithPartialCheckoutProfileWrapper = () => {
    // Attempt to create a checkout user profile only if we don't already have one
    if (
        browser.options.testData.userCredentialsPartialCheckoutProfile.email ===
        ''
    ) {
        browser.options.testData.userCredentialsPartialCheckoutProfile = createUserWithPartialProfile()
        validateCheckoutProfile(
            browser.options.testData.userCredentialsPartialCheckoutProfile
        )
    }
}

Given(/^Setup for "(.*)"$/, (featureName) => {
    if (featureName === 'Buy it now') {
        browser.options.testData.secondaryOneSizeProduct = getTestProductFromCategory(
            browser.options.testData.secondaryOneSizeProduct,
            secondaryOneSizeProductCategory
        )
        validateTestProduct(browser.options.testData.secondaryOneSizeProduct)
    }

    if (
        featureName === 'Checkout Smoke Tests - 2 Consecutive Purchases Journey'
    ) {
        browser.options.testData.defaultSearchTerm = selectProductsOneSize(
            oneSizeProductsSearchTerm(browser.options.brand)
        )
        validateTestProduct(browser.options.testData.defaultSearchTerm)
        browser.options.testData.subCategoryProduct = selectProductsOneSize(
            'seo'
        )
        validateTestProduct(browser.options.testData.subCategoryProduct)
    }

    if (
        featureName ===
        'Checkout Smoke Tests - Full Checkout Profile Purchase Journeys'
    ) {
        getTestProductForCheckoutProfile()
        createUserWithFullCheckoutProfileWrapper()
        browser.options.testData.subCategoryProduct = selectProductsOneSize(
            'seo'
        )
        validateTestProduct(browser.options.testData.subCategoryProduct)
    }

    if (featureName === 'Checkout Smoke Tests - New User Purchase Journeys') {
        if (browser.options.brand === 'br') {
            logger.warn(
                'The US scenario of this feature will not work on Burton as ' +
                    'this is the only Arcadia brand without a store in the US. ' +
                    "Don't worry about a fail."
            )
        } else {
            browser.options.testData.productUS = selectProductsOneSize(
                undefined,
                'US'
            )
            validateTestProduct(browser.options.testData.productUS)
        }
        browser.options.testData.secondaryOneSizeProduct = getTestProductFromCategory(
            browser.options.testData.secondaryOneSizeProduct,
            secondaryOneSizeProductCategory
        )
    }

    if (
        featureName ===
        'Checkout Smoke Tests - Partial Checkout Profile Purchase Journeys'
    ) {
        browser.options.testData.secondaryOneSizeProduct = getTestProductFromCategory(
            browser.options.testData.secondaryOneSizeProduct,
            secondaryOneSizeProductCategory
        )
        createUserWithPartialCheckoutProfileWrapper()
    }

    if (featureName === 'Merged bag') {
        getTestProductForCheckoutProfile()
        browser.options.testData.secondaryOneSizeProduct = getTestProductFromCategory(
            browser.options.testData.secondaryOneSizeProduct,
            secondaryOneSizeProductCategory
        )
        createUserWithFullCheckoutProfileMergedBagWrapper()
    }

    if (featureName === 'Mini Bag - change content') {
        browser.options.testData.secondaryOneSizeProduct = getTestProductFromCategory(
            browser.options.testData.secondaryOneSizeProduct,
            secondaryOneSizeProductCategory
        )
        browser.options.testData.watchProduct = getTestProductFromCategory(
            browser.options.testData.watchProduct,
            watchesProductCategory
        )
        browser.options.testData.multipleSizesProduct = getTestProductFromCategory(
            browser.options.testData.multipleSizesProduct,
            multipleSizeProductsSearchTerm(browser.options.brand)
        )
    }

    if (featureName === 'PayPal') {
        getTestProductForCheckoutProfile()
        browser.options.testData.secondaryOneSizeProduct = getTestProductFromCategory(
            browser.options.testData.secondaryOneSizeProduct,
            secondaryOneSizeProductCategory
        )
        createUserWithFullCheckoutProfileMergedBagWrapper()
    }

    if (featureName === 'Pick From Store') {
        browser.options.testData.multipleSizesProduct = getTestProductFromCategory(
            browser.options.testData.multipleSizesProduct,
            multipleSizeProductsSearchTerm(browser.options.brand)
        )
    }

    if (featureName === 'Promo code link') {
        browser.options.testData.subCategoryProduct = selectProductsOneSize(
            'seo'
        )
        validateTestProduct(browser.options.testData.subCategoryProduct)
    }

    if (featureName === 'Promotion Code') {
        browser.options.testData.secondaryOneSizeProduct = getTestProductFromCategory(
            browser.options.testData.secondaryOneSizeProduct,
            secondaryOneSizeProductCategory
        )
        validateTestProduct(browser.options.testData.secondaryOneSizeProduct)
    }

    if (featureName === 'Quick View') {
        browser.options.testData.subCategoryProduct = selectProductsOneSize(
            'seo'
        )
        validateTestProduct(browser.options.testData.subCategoryProduct)
    }
})
