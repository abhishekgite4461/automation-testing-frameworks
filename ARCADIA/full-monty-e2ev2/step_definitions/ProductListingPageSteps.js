import _ from 'lodash'
import {Given, When, Then} from 'cucumber'
import ProductListingPage from './../page-objects/plp.page'
import {expect} from 'chai'

Given(/^I choose a subcategory product from the product listing page$/, () => {
    const plpPage = new ProductListingPage()
    plpPage.selectSubcategoryProductFromPlp()
})

Given(/^I choose an available product from the product listing page$/, () => {
    const plpPage = new ProductListingPage()
    plpPage.selectDefaultProductFromPlp()
})

Given(/^I choose to filter for products with size "(.*)"$/, (size) => {
    const plpPage = new ProductListingPage()
    plpPage.filterProductsBySize(size)
})

Given(/^I choose to filter for products with colour "(.*)"$/, (colour) => {
    const plpPage = new ProductListingPage()
    plpPage.filterProductsByColour(colour)
})

Given(
    /^I choose quick view for a product from the product listing page$/,
    () => {
        const plpPage = new ProductListingPage()
        plpPage.selectQuickView()
    }
)

When(/^I apply the filters$/, () => {
    const plpPage = new ProductListingPage()
    plpPage.applyFilters()
})

When(/^I sort by "(.*)"$/, (order) => {
    const plpPage = new ProductListingPage()
    plpPage.sortByPrice(order)
})

When(/^I add the product to the bag from the quick view modal$/, () => {
    const plpPage = new ProductListingPage()
    plpPage.addToBagFromQuickView()
})

When(/^I close the quick view modal$/, () => {
    const plpPage = new ProductListingPage()
    plpPage.closeQuickView()
})

Then(/^I see a list of products that matches the filter criteria$/, () => {
    const plpPage = new ProductListingPage()
    expect(Number(plpPage.getNumberOfDisplayedResults())).to.be.lessThan(
        Number(browser.options.testData.plpDisplayedProducts.beforeFilter)
    )
})

Then(/^I see a list of products sorted "(.*)"$/, (order) => {
    const plpPage = new ProductListingPage()
    plpPage.checkProductListIsSorted(order)
})

Given(/^I scroll down past the loaded list$/, () => {
    const plpPage = new ProductListingPage()
    plpPage.scrollToBottom()
})

Given(/^I see more products being loaded$/, () => {
    const plpPage = new ProductListingPage()
    plpPage.waitForMoreProductsBeingLoaded()
})

When(/^I click on the image of the last product$/, () => {
    const plpPage = new ProductListingPage()
    plpPage.selectLastImageFromPlp()
})

Given(/^I choose the given filter with the first value$/, (dataTable) => {
    const brandArray = _.find(dataTable.hashes(), (row) => {
        return row.Brand === browser.options.brand
    })

    const plpPage = new ProductListingPage()
    plpPage.filterProductsByGivenFilter(brandArray.FilterType)
})
