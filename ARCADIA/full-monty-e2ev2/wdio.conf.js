require('babel-polyfill')
require('babel-register')

require('dotenv').config()
const fse = require('fs-extra')
const path = require('path')
const circularjson = require('circular-json')
const logger = require('./support/logger')
const {setUp} = require('./support/lib/shared')
const {setupViewportSize} = require('./support/lib/shared')
const {buildBrandBaseUrl} = require('./support/lib/brandURL')
const {
    subCategoryUrl,
    multipleSizesCategoryUrl,
} = require('./support/constants/categories')

const REPORT_PATH = './reports'
const FEATURE_PATH = process.env.RETRY_ACTIVE
    ? './retry_mechanism/features'
    : './features'
const FAILED_SCENARIOS_OUTPUT = './retry_mechanism/fails'
const chromeOptions = {args: ['--disable-infobars', '--no-sandbox']}

const breakpoint = process.env.BREAKPOINT || 'mobile'
logger.info(`[SETUP] - BREAKPOINT: <${breakpoint}>`)

let cucumberTagExpression = process.env.CUCUMBER_TAGS
    ? `${process.env.CUCUMBER_TAGS} and not @pending and not @bug and not @wip`
    : 'not @pending and not @bug and not @wip'
cucumberTagExpression =
    process.env.PRODUCTION === 'true'
        ? `${cucumberTagExpression} and not @noproduction`
        : cucumberTagExpression
logger.info(`[SETUP] - CUCUMBER TAG EXPRESSION: <${cucumberTagExpression}>`)

const cucumberFeature = process.env.CUCUMBER_FEATURE
    ? `${FEATURE_PATH}/**/${process.env.CUCUMBER_FEATURE}.feature`
    : `${FEATURE_PATH}/**/*.feature`
logger.info(`[SETUP] - CUCUMBER FEATURE: <${cucumberFeature}>`)

const browserName = process.env.BROWSER || 'chrome'
logger.info(`[SETUP] - BROWSER: <${browserName}>`)

const headlessChrome = process.env.HEADLESS_CHROME !== 'false'
if (headlessChrome) chromeOptions.args.push('--headless')
logger.info(`[SETUP] - HEADLESS_CHROME: <${headlessChrome}>`)

const maxInstances = process.env.INSTANCES || 3
logger.info(`[SETUP] - INSTANCES: <${maxInstances}>`)

const logLevel = process.env.LOGLEVEL || 'error'
logger.info(`[SETUP] - LOGLEVEL: <${logLevel}>`)

const waitforTimeout = process.env.TIMEOUT * 1000 || 75000
logger.info(`[SETUP] - TIMEOUT: <${waitforTimeout / 1000} seconds>`)

const activeRetry = process.env.RETRY_ACTIVE === 'true'
logger.info(`[SETUP] - RETRY_ACTIVE: <${activeRetry}>`)

const target = process.env.TARGET || 'local'
logger.info(`[SETUP] - TARGET: <${target}>`)

const productionLike = process.env.PRODUCTION === 'true'
logger.info(`[SETUP] - PRODUCTION: <${productionLike}>`)

const brand = process.env.BRAND || 'ts'
logger.info(`[SETUP] - BRAND: <${brand}>`)

const debugOnFail = process.env.DEBUG_ON_FAIL === 'true'
logger.info(`[SETUP] - DEBUG_ON_FAIL: <${debugOnFail}>`)

logger.info('-----------------------------')

const desktopOnlyFeatures = [`${FEATURE_PATH}/**/quickView.feature`]
const mobileOnlyFeatures = [`${FEATURE_PATH}/**/buyItNow.feature`]

const testData = {
    product: {productId: 0},
    productUS: {productId: 0},
    subCategoryProduct: {productId: 0},
    secondaryOneSizeProduct: {productId: 0},
    watchProduct: {productId: 0},
    multipleSizesProduct: {productId: 0},
    defaultSearchTerm: {productId: 0},
    userCredentialsWithFullCheckoutProfile: {email: '', password: ''},
    userCredentialsPartialCheckoutProfile: {email: '', password: ''},
    userCredentialsWithMergedBagCheckoutProfile: {email: '', password: ''},
    pdpUrl: '',
    currentCategoryId: '',
    plpDisplayedProducts: {beforeFilter: 0, afterFilter: 0},
    subCategoryUrl: subCategoryUrl(brand),
    multipleSizesCategoryUrl: multipleSizesCategoryUrl(brand),
}

const currentScenario = ''
const currentFeatureScenarios = []
const printBrowserConsoleOutput = () =>
    logger.info('[BROWSER CONSOLE OUTPUT]', browser.log('browser'))
const saveCucumberJsonTo = (cucumberJson, fileName) => {
    fse.writeFileSync(
        path.join(__dirname, `${fileName}.json`),
        circularjson.stringify(cucumberJson)
    )
}
const retrieveFileNameFromPath = (fullPath) =>
    fullPath
        .split('/')
        .slice(-1)[0]
        .split('.')[0]

exports.config = {
    // ==================
    // Specify Test Files
    // ==================
    specs: [cucumberFeature],

    exclude: [
        // 'path/to/excluded/files'
    ],
    //
    // ============
    // Capabilities
    // ============
    maxInstances,

    capabilities: [
        {
            browserName,
            chromeOptions,
        },
    ],
    //
    // ===================
    // Test Configurations
    // ===================
    // Define all options that are relevant for the WebdriverIO instance here
    //
    // By default WebdriverIO commands are executed in a synchronous way using
    // the wdio-sync package. If you still want to run your tests in an async way
    // e.g. using promises you can set the sync option to false.
    sync: true,
    //
    // Level of logging verbosity: silent | verbose | command | data | result | error
    logLevel,
    //
    // Enables colors for log output.
    coloredLogs: true,
    //
    // Saves a screenshot to a given path if a command fails.
    screenshotPath: `./screenshots`,
    //
    // Set a base URL in order to shorten url command calls. If your url parameter starts
    // with "/", then the base url gets prepended.
    baseUrl: buildBrandBaseUrl(`${brand}uk`, target),
    //
    // Default timeout for all waitFor* commands.
    waitforTimeout,
    //
    // Default timeout in milliseconds for request
    // if Selenium Grid doesn't send response
    connectionRetryTimeout: 15000,
    //
    // Default request retries count
    connectionRetryCount: 3,
    //
    // Test runner services
    // Services take over a specific job you don't want to take care of. They enhance
    // your test setup with almost no effort. Unlike plugins, they don't add new
    // commands. Instead, they hook themselves up into the test process.
    services: ['selenium-standalone'],

    // hardcode to use older chrome drive as sort out ways to upgrade chrome
    // seleniumInstallArgs: { drivers: { chrome: { version: '2.35' } } },
    // seleniumArgs: { drivers: { chrome: { version: '2.35' } } },
    // Note this is a workaround for the selenium bug network peer
    seleniumInstallArgs: {version: '3.4.0'},
    seleniumArgs: {version: '3.4.0'},
    //
    // Framework you want to run your specs with.
    // The following are supported: Mocha, Jasmine, and Cucumber
    // see also: http://webdriver.io/guide/testrunner/frameworks.html
    //
    // Make sure you have the wdio adapter package for the specific framework installed
    // before running any tests.
    framework: 'cucumber',
    //
    // Test reporter for stdout.
    // The only one supported by default is 'dot'
    // see also: http://webdriver.io/guide/testrunner/reporters.html

    reporters: ['cucumber', 'junit-morganchristiansson', 'json-cucumber'],
    reporterOptions: {
        outputDir: `${REPORT_PATH}/bdd/`,
        cucumberJsonReporter: {
            verbose: true,
            deviceName: breakpoint,
        },
    },
    cucumberOpts: {
        // Create array of step definitions for cucumber to load in
        require: fse
            .readdirSync('./step_definitions')
            .map((filename) => `./step_definitions/${filename}`),
        backtrace: true, // <boolean> show full backtrace for errors
        compiler: [], // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
        dryRun: false, // <boolean> invoke formatters without executing steps
        failFast: false, // <boolean> abort the run on first failure
        format: ['pretty'], // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
        colors: true, // <boolean> disable colors in formatter output
        snippets: true, // <boolean> hide step definition snippets for pending steps
        source: true, // <boolean> hide source uris
        profile: [], // <string[]> (name) specify the profile to use
        strict: false, // <boolean> fail if there are any undefined or pending steps
        tagExpression: cucumberTagExpression,
        timeout: 220000, // <number> timeout for step definitions
        ignoreUndefinedDefinitions: true, // <boolean> Enable this config to treat undefined definitions as warnings.
    },

    // ==========
    // WDIO Hooks
    // ==========
    onPrepare: (globalConf) => {
        // Exclude features which are not compatible with mobile/desktop
        if (globalConf.breakpoint === 'mobile') {
            globalConf.exclude = desktopOnlyFeatures
        }
        if (globalConf.breakpoint === 'desktop') {
            globalConf.exclude = mobileOnlyFeatures
        }
        // Exclude PayPal scenario from runs in headless chrome
        if (globalConf.headlessChrome) {
            globalConf.exclude.push(`${FEATURE_PATH}/**/paypal.feature`)
        }
    },

    before: () => {
        // NOTE: This is a WDIO specific hook and should ideally not be used in our
        // test suite for setups. The reason for this is that WDIO spawns a new
        // selenium session for all spec files, even when they are marked as
        // excluded i.e. by a cucumber tag. Please use BackgroundSetupSteps instead.
    },

    // CUCUMBER HOOKS
    // ===============
    beforeFeature: (feature) => {
        setUp()
        setupViewportSize(breakpoint)
        browser.options.currentFeatureScenarios = feature.scenarios
        browser.options.currentRunningFeatureName = feature.name
    },

    beforeScenario: (scenario) => {
        if (scenario.name === 'User registers and makes a purchase') {
            setUp(buildBrandBaseUrl(`${brand}us`))
            setupViewportSize(breakpoint)
        }
        browser.options.currentScenario = scenario.name
    },

    // Session needs to be renewed when there are more than 1 scenarion in a
    // feature file to ensure a clean slate
    // eslint-disable-next-line no-unused-vars
    afterScenario: (scenario) => {
        if (
            // Don't reload session if there is only one scenario
            browser.options.currentFeatureScenarios.length > 1 &&
            // Don't reload session if finished scenario was the last one
            browser.options.currentFeatureScenarios.slice(-1)[0].name !==
                browser.options.currentScenario
        ) {
            browser.url('/logout')
            browser.reload()
            setUp()
            setupViewportSize(breakpoint)
        }
    },

    afterStep: (testResult) => {
        // Save failed feature names and data to a file
        // if (testResult.status === 'failed') {
        //     const fileName = `${FAILED_SCENARIOS_OUTPUT}/${retrieveFileNameFromPath(
        //         testResult.step.scenario.feature.uri
        //     )}_${testResult.step.line}`
        //     try {
        //         // saveCucumberJsonTo(testResult, fileName)
        //         logger.info(
        //             `Scenario "${testResult.step.scenario.name}" failed.
        //             Saved report in ${FAILED_SCENARIOS_OUTPUT}`
        //         )
        //     } catch (err) {
        //         logger.warn(err)
        //     }
        //     printBrowserConsoleOutput()
        // }

        logger.info("--------------------------------------------------------");
                logger.info(testResult);
                logger.info("--------------------------------------------------------");
        // Reinstantiate the browser session in case the feature file has another
        // scenario following up. This prevents the next feature from being
        // negatively affected.
        if (
            testResult.status === 'failed' &&
            browser.options.currentFeatureScenarios.length > 1 &&
            // Don't reload session if failing scenario was the last one
            browser.options.currentFeatureScenarios.slice(-1)[0].name !==
                browser.options.currentScenario &&
            // Below feature doesn't need reset as the next scenario in queue will
            // already set up its own session (due to it using US)
            browser.options.currentRunningFeatureName !==
                'Checkout Smoke Tests - New User Purchase Journeys'
        ) {
            logger.info(
                'Scenario failed. Reinstantiating the session to prevent ' +
                    'user steps from this scenario corrupting the next one.'
            )
            browser.reload()
        }

        if (testResult.status === 'failed' && debugOnFail) {
            logger.info('A step failed. Launching browser debug.', {testResult})
            browser.debug()
        }
    },

    // Our custom global properties
    testData, // --> browser.options.testData,
    breakpoint, // --> browser.options.breakpoint
    currentScenario, // --> browser.options.currentScenario
    currentFeatureScenarios, // --> browser.options.currentFeatureScenarios
    brand, // --> browser.options.brand
    target, // --> browser.options.target
    headlessChrome, // --> browser.options.headlessChrome
}
