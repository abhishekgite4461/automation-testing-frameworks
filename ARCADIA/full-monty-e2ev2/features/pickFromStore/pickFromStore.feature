@storelocator
@pdp
Feature: Pick From Store

  Background:
    Given Setup for "Pick From Store"

  Scenario: A user selects a delivery store on the PDP page
    Given I visit the "UK" "<multipleSizesProduct PDP>" page
    And I select an available size
    And I select to pick up the product from a store
    And I search for a delivery point in "Liverpool"
    And I look at the details for location number "3" in the list
    And I close the pick from store modal
    And I add the product to the bag
    And I dismiss the add to bag confirmation modal
    And I view the bag
    And I choose the "Collect From Store" delivery option from the bag
    When I proceed to checkout from the bag
    And I register "during checkout"
    Then I see the "Collect From Store" delivery option is selected
    # NOTE: Temporariliy disabling this step as the functionality is currently
    # disabled as of ticket DES-3847
    # And I see the store address details pre-filled
