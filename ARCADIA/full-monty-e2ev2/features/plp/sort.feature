@plp
Feature: PLP Sort

  Scenario: A user can sort by price
    Given I visit the "UK" "<multipleSizesCategory PLP>" page
    When I sort by "Price - High To High"
