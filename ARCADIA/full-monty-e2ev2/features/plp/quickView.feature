@plp
Feature: Quick View

  Background:
    Given Setup for "Quick View"

  Scenario: A user can add to the bag from quick view
    Given I visit the "UK" "subcategory" page
    And I choose quick view for a product from the product listing page
    When I add the product to the bag from the quick view modal
    And I close the quick view modal
    Then I see the product in the minibag
