@plp
Feature: PLP - A user can load more products

  Scenario: A user can scroll down the page to load more products
    Given I visit the "UK" "<multipleSizesCategory PLP>" page
    And I scroll down past the loaded list
    And I see more products being loaded
    When I click on the image of the last product
    Then I see the PDP page for that product
