@plp
@noproduction
Feature: PLP Filter

  Scenario: A user can filter by size and colour
    Given I visit the "UK" "<multipleSizesCategory PLP>" page
    And I choose the given filter with the first value
      | Brand | FilterType |
      | ts    | Size       |
      | tm    | Waist Size |
      | br    | Waist Size |
      | dp    | Shoe Size  |
      | ev    | Size       |
      | ms    | Size       |
      | wl    | Size       |
    And I choose the given filter with the first value
      | Brand | FilterType |
      | ts    | Colour     |
      | tm    | Colour     |
      | br    | Colour     |
      | dp    | Colour     |
      | ev    | Colour     |
      | ms    | Colour     |
      | wl    | Colour     |
    When I apply the filters
    Then I see a list of products that matches the filter criteria
