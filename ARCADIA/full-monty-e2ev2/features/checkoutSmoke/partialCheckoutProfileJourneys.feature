@noproduction
@smoke
Feature: Checkout Smoke Tests - Partial Checkout Profile Purchase Journeys

  Background:
    Given Setup for "Checkout Smoke Tests - Partial Checkout Profile Purchase Journeys"

  @storelocator
  Scenario: User adds to bag anonymously, logs in with partial profile and makes a purchase
    Given I visit the "UK" "<secondaryOneSizeProduct PDP>" page
    And I add the product to the bag
    And I dismiss the add to bag confirmation modal
    And I view the bag
    And I choose the "Collect From Store" delivery option from the bag
    And I proceed to checkout from the bag
    And I login "during checkout" as a user with "partial" checkout profile
  #  TODO: Using home express while no stores are selectable
  #   And I search for a delivery point in "London"
  #   And I choose location number "2" in the list
  #   And I choose "Collect From Store Express" as the delivery type
    And I choose "Home Express" as the delivery type
    And I enter my "UK" delivery address using the "Find Address" functionality
    And I enter my "UK" "Collect from Store" billing details using the "Find Address" functionality
    And I enter my "account card" payment details
    When I place the order as a "new" user
    Then I see the confirmation for my order
