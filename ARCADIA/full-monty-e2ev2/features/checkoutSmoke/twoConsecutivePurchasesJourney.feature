@noproduction
@smoke
Feature: Checkout Smoke Tests - 2 Consecutive Purchases Journey

  Background:
    Given Setup for "Checkout Smoke Tests - 2 Consecutive Purchases Journey"

  Scenario: User adds to bag anonymously, registers and makes 2 consecutive purchases
    Given I visit the "UK" "home" page
    And I search for a search term
    And I choose an available product from the product listing page
    And I add the product to the bag
    And I dismiss the add to bag confirmation modal
    And I view the bag
    And I proceed to checkout from the bag
    And I register "during checkout"
    And I choose "United Kingdom" as the delivery country
    And I choose "Home Express" as the delivery option
    And I choose delivery date number "2" in the list
    And I enter my "UK" delivery address using the "manual entry" functionality
    And I enter my "Poland" "Home Delivery" billing details using the "Find Address" functionality
    And I enter my "visa" payment details
    And I place the order as a "new" user
    And I see the confirmation for my order
    And I continue shopping
    And I select a parent category from the menu
    And I select a subcategory
    And I choose a subcategory product from the product listing page
    And I add the product to the bag
    And I proceed to checkout from the confirmation modal as a "logged in returning user"
    And I provide delivery and payment details as a "returning" user
    When I place the order as a "returning" user
    Then I see the confirmation for my order
