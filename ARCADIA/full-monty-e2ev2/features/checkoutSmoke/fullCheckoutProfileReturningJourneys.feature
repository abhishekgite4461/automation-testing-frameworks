@noproduction
@smoke
Feature: Checkout Smoke Tests - Full Checkout Profile Purchase Journeys

  Background:
    Given Setup for "Checkout Smoke Tests - Full Checkout Profile Purchase Journeys"

  Scenario: User logs in and makes a purchase
    Given I login "on visiting" as a user with "full" checkout profile
    And I select a parent category from the menu
    And I select a subcategory
    And I choose a subcategory product from the product listing page
    And I add the product to the bag
    And I proceed to checkout from the confirmation modal as a "logged in returning user"
    And I provide delivery and payment details as a "returning" user
    When I place the order as a "returning" user
    Then I see the confirmation for my order

  Scenario: User adds to bag anonymously, logs in and makes a purchase
    Given I visit the "UK" "subcategory" page
    And I choose a subcategory product from the product listing page
    And I add the product to the bag
    And I proceed to checkout from the confirmation modal as a "returning user"
    And I login "during checkout" as a user with "full" checkout profile
    And I provide delivery and payment details as a "returning" user
    When I place the order as a "returning" user
    Then I see the confirmation for my order
