@smoke
Feature: Checkout Smoke Tests - New User Purchase Journeys

  Background:
    Given Setup for "Checkout Smoke Tests - New User Purchase Journeys"

  @storelocator
  Scenario: User adds to bag anonymously, registers and makes a purchase
    Given I visit the "UK" "<secondaryOneSizeProduct PDP>" page
    And I add the product to the bag
    And I dismiss the add to bag confirmation modal
    And I view the bag
    And I proceed to checkout from the bag
    And I register "during checkout"
    And I choose "ParcelShop" as the delivery option
    And I search for a delivery point in "London"
    # This step has a potential to fail outisde of team monty's control. There
    # are known issues with WCS/ScrAPI returning an error when selecting a
    # ParcelShop in checkout.
    And I choose location number "2" in the list
    And I provide my personal delivery details
    And I enter my "UK" "Collect from ParcelShop" billing details using the "Find Address" functionality
    And I enter my "account card" payment details
    When I place the order as a "new" user
    Then I see the confirmation for my order

  # See README.md for error cause
  @bug
  Scenario: User registers and makes a purchase
    Given I visit the "US" "home" page
    And I search for a product code "in the US"
    And I add the product to the bag
    And I dismiss the add to bag confirmation modal
    And I view the bag
    And I proceed to checkout from the bag
    And I register "during checkout"
    And I choose "US Express" as the delivery option
    And I enter my "US" delivery address using the "manual entry" functionality
    And I enter my "amex" payment details
    When I place the order as a "new" user
    Then I see the confirmation for my order
