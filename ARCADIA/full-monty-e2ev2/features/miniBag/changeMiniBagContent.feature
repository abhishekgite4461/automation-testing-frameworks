@smoke
Feature: Mini Bag - change content

  Background:
    Given Setup for "Mini Bag - change content"

  Scenario: A user can remove items from the minibag
    Given I visit the "UK" "<secondaryOneSizeProduct PDP>" page
    And I add the product to the bag
    And I dismiss the add to bag confirmation modal
    And I visit the "UK" "<tertiaryOneSizeProduct PDP>" page
    And I add the product to the bag
    And I dismiss the add to bag confirmation modal
    And I view the bag
    When I remove the first product from the bag
    Then I see the second product in the bag

  Scenario: A user can change the size of a product in the minibag
    Given I visit the "UK" "<multipleSizesProduct PDP>" page
    And I select an available size
    And I add the product to the bag
    And I dismiss the add to bag confirmation modal
    And I view the bag
    When I change the current size
    And I change the item quantity to "3"
    Then I see the new size of the product in the minibag
    And I see that the item quantity is "3"
