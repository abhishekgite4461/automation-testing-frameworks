# NOTE: This feature only works when NOT using the browser in headless mode.
@noproduction
@paypal
Feature: PayPal

    Background:
        # Create user profile and add product to bag via API
        Given Setup for "PayPal"

    Scenario: PayPal
        Given I visit the "UK" "Checkout Login" page
        And I login "during checkout" as a user with "merged bag" checkout profile
        And I get prompted to review my bag
        And I change the payment method
        And I choose "PayPal" as the payment method
        When I place the order as a "returning" user with "PayPal"
        Then I can login and pay on PayPal
            | username                     | password |
            | newtest610@test.com          | passw0rd |
        And I see the confirmation for my order
