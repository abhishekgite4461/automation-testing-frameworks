@noproduction
Feature: Merged bag

  Background:
    Given Setup for "Merged bag"

  Scenario: A user is prompted to review the bag when proceeding to checkout
    Given I visit the "UK" "<secondaryOneSizeProduct PDP>" page
    And I add the product to the bag
    And I dismiss the add to bag confirmation modal
    And I view the bag
    And I proceed to checkout from the bag
    And I login "during checkout" as a user with "merged bag" checkout profile
    Then I get prompted to review my bag
    And the bag "in checkout" contains both items
