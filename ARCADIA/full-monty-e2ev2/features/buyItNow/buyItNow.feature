@pdp
Feature: Buy it now

  Background:
    Given Setup for "Buy it now"

  Scenario: A user can use "Buy it NOW" to go straight to checkout
    Given I visit the "UK" "<secondaryOneSizeProduct PDP>" page
    When I choose to buy it now
    And I register "during checkout"
    Then I should land on the "delivery" page
