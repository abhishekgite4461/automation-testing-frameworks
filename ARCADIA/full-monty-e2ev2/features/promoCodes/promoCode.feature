@promo
@noproduction
Feature: Promotion Code

  Background:
    Given Setup for "Promotion Code"

  Scenario: A user can add multiple promotion codes
    Given I visit the "UK" "<secondaryOneSizeProduct PDP>" page
    And I add the product to the bag
    And I dismiss the add to bag confirmation modal
    And I view the bag
    When I enter the "D2VJ6OB03W" promotion code
    And I add another promotion code "K50VS52ML7"
    Then I see the total price in the minibag is discounted
    And I see the "D2VJ6OB03W" promotion code applied
    And I see the "K50VS52ML7" promotion code applied
