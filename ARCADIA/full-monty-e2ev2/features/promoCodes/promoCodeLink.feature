@promo
@noproduction
Feature: Promo code link

  Background:
    Given Setup for "Promo code link"

  Scenario: A user clicking on a promo code link will get a discount
    Given I visit the "UK" "subcategory" page with promo code "D2VJ6OB03W"
    And I have the cookie "arcpromoCode"
    And I choose a subcategory product from the product listing page
    And I add the product to the bag
    And I dismiss the add to bag confirmation modal
    When I view the bag
    Then I see the "D2VJ6OB03W" promotion code applied
    And I see the total price in the minibag is discounted
    # TODO: The below step does not work on CoreAPI. Awaits fix via DES-2757
    # And I do not have the cookie "arcpromoCode"
