# full-monty End To End Test Suite [![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

## Instructions
- `git clone` the repository
- `npm install`
- `npm start`

**Note -** This repository is not part of our **full-monty** we still need to
build the actual app **[full-monty](https://github.com/ag-digital/full-monty)**
via `npm run e2e`.


## Running specific tests
- `npm run mobile` or `BREAKPOINT=mobile npm start` for mobile
- `npm run desktop` or `BREAKPOINT=desktop npm start` for desktop
- `CUCUMBER_FEATURE="quickView" npm run desktop` to run the feature file with
  the name "quickView" on desktop

### Running PayPal feature
The PayPal feature does **not** work with headless chrome. Run it with:
`CUCUMBER_FEATURE=paypal HEADLESS_CHROME=false npm start`

## Settings
All settings are being passed via environment variables: either by putting it in
front of the start script (i.e. `HEADLESS_CHROME=true BRAND=dp npm run desktop`)
or by placing them in an `.env` file in the root of the project.

| Setting        | Explanation  | Default  |
| ------------- |:-------------:| -----:|
| `BREAKPOINT=desktop`           | Test either `mobile` or `desktop` site of full-monty. | mobile |
| `CUCUMBER_FEATURE=mergedBag`   | Only run a specific feature file. Name needs to be exactly spelled. | - |
| `CUCUMBER_TAGS=@smoke`         | Tag expression to narrow down tests. Example: `@smoke and not @plp` will run all feature files marked with `@smoke` but exclude those labelled with `@plp`. | - |
| `HEADLESS_CHROME=false`        | Disables headless chrome. Running in headless mode is faster than regular mode. Note: Feature `paypal` does not work with headless chrome. | true |
| `BROWSER=browsername`          | Choose the browser you want to run the tests in. Must be one of the [selenium supported names](https://github.com/SeleniumHQ/selenium/wiki/DesiredCapabilities). | chrome |
| `TARGET=stage`                 | Environment which to run against. `local` goes against local development server on port 3000. Any argument **not** beginning with `http` will construct a link like `stage.m.topshop.com`, i.e. `TARGET=showcase BRAND=dp` ==> _showcase.m.dorothyperkins.com_. When the argument starts with `http`, it will go directly to that URL. (When using a complete URL, make sure that it does **not** end with a slash `/`!) Set it equal to `production` to run against the live website. | local |
| `INSTANCES=3`                  | Number of browser instances to run tests in parallel. | 3 |
| `BRAND=dp`                     | Brand to run tests on. |    ts |
| `LOGLEVEL=verbose`             | Change selenium output, useful for debugging. | error |
| `TIMEOUT=90`                   | Timeout in seconds for all `waitFor` commands. | 75 |
| `DEBUG_ON_FAIL=true`           | Launch WDIO `browser.debug()` when a step fails. | false |
| `PRODUCTION=true`              | Use this when running against production or a production-like environment, i.e. _preprod_. This will check that fake payments are being detected properly. | false |

## Retry Mechanism
The retry mechanism is being started by running `npm run retry`. Cucumber 4
generates after every step a report with information about the step, such as its
scenario name. We export this report to the folder `/fails` whenever a step
fails (wdio cucumber hook `afterStep()`). The node script `tagFeaturesAsRetry`
reads these reports and copies the original features into the folder
`retry_mechanism/features`. It also adds the tag `@retry` to the failed
scenarios. Next we tell wdio to only run the tagged scenarios
(`CUCUMBER_TAGS=@retry`) and to pick up the newly tagged features from the
`retry_mechanism/features` folder (`ACTIVE_RETRIES=true`).

## Features marked as @bug
**CoreAPI failures**:
- `newUserJourneys: User registers and makes a purchase - I search for a product
    code "in the US"`: When a product code is being entered into the search bar,
    the user should automatically be redirected to that PDP. Instead, on CoreAPI
    the user just reaches the PLP showing only one product which matches the
    product code.

**ScrAPI failures**:
- `newUserJourneys: User registers and makes a purchase - I search for a product
  code "in the US"`: ScrAPI says that this search term does not return any
  results although a product with this code exists.

## Different brands
All tests can be run and should pass on all 7 brands, except for the following:
- `newUserJourneys` scenario 1: This will not work for Burton as this is the
  only Arcadia brand which doesn't exist in the US.
- `sort`: Currently only works on Topshop because all brands have different
  values to sort by.
- `pickFromStore`: Feature is not active on all brands. Works on Topshop and
  Wallis.

## General style
- Only test what is really necessary - E2EV2 should be **as slim as possible**!
- If you need a specific user profile (i.e. a profile with a merged bag), don't
  create this via the UI (as it takes a lot of time) but do it as a Background
  setup step before the feature and create that user profile via API.

## Code style
- Since our products are ever changing, we need to collect a suitable product
  before the actual test starts (The same goes for creating user profiles and so
  on). This is why we use cucumber `Background` steps before the actual feature.
- Never use IDs as selectors (`#`). Reason: full-monty will soon be turning off
  IDs completely, see JIRA ticket PTM-138.)
- Don't use wdio element objects (`$`, `$$`, `elements`) in the page objects as
  common elements, just use strings with class getters.

```javascript
// Good
get contentOverlay() {
    return '.ContentOverlay'
}

browser.click(this.contentOverlay)

// Bad
get contentOverlay() {
    return $('.ContentOverlay')
}

this.contentOverlay.click()
```
- ‎The coding style should be flat - no deep levels of abstractions and folders!
- ‎Variables (i.e. `I enter the "TSCARD1" promotion code`) should be on the
  highest level possible. This is ideally the feature file (for example via
  [cucumber data tables](https://docs.cucumber.io/#data-tables)) or in a
  constants file if the data has to be used by steps across multiple feature
  files.
- Every page object should have a constructor which waits for characteristic
  page elements to be visible.
```javascript
class HomePage {
    constructor() {
        waitForElementTobeVisible(
            this.cmsContentFrame,
            "Home Page: CMS Frame didn't load."
        )
    }
}
```
- ‎Redirection to another page should happen via instantiating that `new` page
  object (which will call the `constructor()` method which will wait for the new
  page to be loaded.)
```javascript
browser.click(this.mobileMiniBagIcon)
return new MiniBag()
```
- We created wrappers around the regular WDIO wait commands in order to have
  better error logs. An example is `waitForVisible.js`. Use them extensively and
  also make use of the additional error hints you can provide as arguments, this
  will make debugging failed tests a lot easier.
- Before you interact with an element, always wait for it to be visible. That
  way when said element is not visible, our waitFor wrapper will throw a
  meaningful error instead of WDIO/Selenium just saying "_Couldn't click on
  element 44_".
