const fse = require('fs-extra')
const find = require('find')
const path = require('path')
const replace = require('replace')
const bunyan = require('bunyan')
const bformat = require('bunyan-format')
const formatOut = bformat({outputMode: 'short'})

const logger = bunyan.createLogger({
    name: 'retry_mechanism',
    stream: formatOut,
})

logger.info('Retry mechanism script launched.')

const locationOriginalFeatures = path.join(__dirname, './../features/')
const locationTaggedFeatures = path.join(__dirname, './features/')
const locationFailArtifacts = path.join(__dirname, './fails')
const locationIndividualFailArtifact = (failArtifact) => {
    return path.join(__dirname, './fails/', failArtifact)
}
const locationIndividualTaggedFeature = (fileName) => {
    return path.join(__dirname, './features/', fileName)
}

const copyAndTagOriginalFeature = (failArtifact) => {
    const fileContent = fse.readFileSync(
        locationIndividualFailArtifact(failArtifact),
        'utf8'
    )
    const content = JSON.parse(fileContent)
    const failedScenario = content.step.scenario.name
    const fileName = content.step.scenario.uri.split('/').slice(-1)[0]
    const taggedFeature = locationIndividualTaggedFeature(fileName)

    // Copy original feature to temp folder if it doesn't already exist
    const files = find.fileSync(fileName, locationTaggedFeatures)
    if (files.length === 0) copyOriginalFile(fileName, taggedFeature)
    addRetryTag(failedScenario, taggedFeature)
}

const addRetryTag = (failedScenario, taggedFeature) => {
    logger.info(
        `Adding @retry to the scenario "${failedScenario}" in <${taggedFeature}>`
    )
    replace({
        paths: [taggedFeature],
        regex: new RegExp(`Scenario: ${failedScenario}`, 'g'),
        replacement: `@retry\nScenario: ${failedScenario}`,
    })
}

const copyOriginalFile = (fileName, taggedFeature) => {
    logger.info(`Creating copy of failed feature "${fileName}"`)
    const originalFeatures = find.fileSync(fileName, locationOriginalFeatures)
    fse.copySync(originalFeatures[0], taggedFeature)
}

const iterateOverFailArtifacts = () => {
    const failArtifacts = fse.readdirSync(locationFailArtifacts)
    if (failArtifacts.length === 0) {
        logger.info('Success! No failed tests need to be retried.')
    } else {
        logger.warn('Found fails. Retagging them with @retry')
        failArtifacts.forEach(copyAndTagOriginalFeature)
    }
}

iterateOverFailArtifacts()
