# Custom colours
FNT_CLR=`tput bold``tput setaf 5`
RESET=`tput sgr0`


# Initial setup
#################
echo -e "${FNT_CLR}retry.sh: Emptying /features folder${RESET}"
rm -r retry_mechanism/features
mkdir retry_mechanism/features

# Run Number 1
################
echo -e "${FNT_CLR}retry.sh: Emptying /fails folder${RESET}"
rm -r retry_mechanism/fails
mkdir retry_mechanism/fails
echo -e "${FNT_CLR}retry.sh: First run: Starting WebdriverIO (will create files for each failed scenario inside /fails)${RESET}"
npm start
echo -e "${FNT_CLR}retry.sh: Launching node script to temporarily add @retry tags to failed scenarios${RESET}"
node retry_mechanism/tagFeaturesAsRetry.js


# Run Number 2
################
echo -e "${FNT_CLR}retry.sh: Emptying /fails folder${RESET}"
rm -r retry_mechanism/fails
mkdir retry_mechanism/fails
echo -e "${FNT_CLR}retry.sh: Second run: Retrying features marked with @retry (will create files for each failed scenario inside /fails)${RESET}"
RETRY_ACTIVE=true CUCUMBER_TAGS=@retry npm start
echo -e "${FNT_CLR}retry.sh: Emptying /features folder${RESET}"
rm -r retry_mechanism/features
mkdir retry_mechanism/features
echo -e "${FNT_CLR}retry.sh: Launching node script to temporarily add @retry tags to failed scenarios${RESET}"
node retry_mechanism/tagFeaturesAsRetry.js


# Run Number 3
#################
echo -e "${FNT_CLR}retry.sh: Emptying /fails folder${RESET}"
rm -r retry_mechanism/fails
mkdir retry_mechanism/fails
echo -e "${FNT_CLR}retry.sh: Third run: Retrying features marked with @retry${RESET}"
RETRY_ACTIVE=true CUCUMBER_TAGS=@retry npm start

echo -e "${FNT_CLR}retry.sh: Retry script finished${RESET}"
