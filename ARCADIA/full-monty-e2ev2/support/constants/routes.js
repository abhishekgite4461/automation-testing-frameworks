export default {
    category: '/category',
    checkout: '/checkout',
    checkoutLogin: '/checkout/login',
    collectFromStore: '/checkout/delivery/collect-from-store',
    delivery: '/checkout/delivery',
    deliveryPayment: '/checkout/delivery-payment',
    login: '/login',
    pdp: '/product/',
}
