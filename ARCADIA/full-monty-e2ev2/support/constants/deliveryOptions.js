export const deliveryOptionIds = (deliveryOption, brand) => {
    if (deliveryOption === 'Collect From Store') {
        switch (brand) {
            case 'wl':
                return 45017
            case 'ts':
                return 45019
            case 'br':
                return 45021
            case 'ms':
                return 45023
            case 'dp':
                return 45025
            case 'ev':
                return 45027
            case 'tm':
                return 45029
        }
    }
    if (deliveryOption === 'Home Standard') {
        switch (brand) {
            case 'wl':
                return 26501
            case 'ts':
                return 26504
            case 'br':
                return 26503
            case 'ms':
                return 26502
            case 'dp':
                return 26506
            case 'ev':
                return 26507
            case 'tm':
                return 26505
        }
    }
}

export const deliveryOptionsCheckoutForAttributes = (deliveryOption) => {
    switch (deliveryOption) {
        case 'Collect From Store':
            return 'delivery-option-store'
        case 'Home Delivery':
            return 'delivery-option-home'
        case 'Collect From ParcelShop':
            return 'delivery-option-parcelshop'
        default:
            return 'Delivery option unknown'
    }
}
