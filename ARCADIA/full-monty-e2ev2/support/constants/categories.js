export const subCategoryUrl = (brand) => {
    switch (brand) {
        case 'ts':
            return '/en/tsuk/category/bags-accessories-1702216/jewellery-469'
        case 'ms':
            return '/en/msuk/category/accessories-299050/jewellery-5889039'
        case 'dp':
            return '/en/dpuk/category/accessories-203537/jewellery-5875160'
        case 'tm':
            return '/en/tmuk/category/shoes-and-accessories-1928527/mens-sunglasses-4509117'
        case 'wl':
            return '/en/wluk/category/clothing-265973/view-all-accessories-7621732'
        case 'ev':
            return '/en/evuk/category/clothing-250468/view-all-jewellery-6922124'
        case 'br':
            return '/en/bruk/category/accessories-6566150/mens-bags-6566164'
    }
}

// NOTE: When you change these categoryUrls you also have to change the categoryIds!
export const multipleSizesCategoryUrl = (brand) => {
    switch (brand) {
        case 'ts':
            return '/en/tsuk/category/clothing-427/dresses-442'
        case 'ms':
            return '/en/msuk/category/dress-shop-299048/view-all-299126'
        case 'dp':
            return '/en/dpuk/category/shoes-boots-4849364/view-all-shoes-boots-2055245'
        case 'tm':
            return '/en/tmuk/category/jeans-7209202/shop-all-jeans-7209218'
        case 'wl':
            return '/en/wluk/category/dresses-265974/view-all-dresses-4717020'
        case 'ev':
            return '/en/evuk/category/clothing-250468/dresses-250498'
        case 'br':
            return '/en/bruk/category/clothing-281559/mens-jeans-281570'
    }
}

// NOTE: When you change these categoryIds you also have to change the categoryUrls!
export const multipleSizesCategoryIds = (brand) => {
    switch (brand) {
        case 'ts':
            return '203984,208523'
        case 'ms':
            return '208100,222486'
        case 'dp':
            return '208609,208737'
        case 'tm':
            return '207169,207170'
        case 'wl':
            return '209167,2564354'
        case 'ev':
            return '209427,209530'
        case 'br':
            return '208974,209024'
    }
}

export const parentCategoryName = (brand) => {
    switch (brand) {
        case 'ts':
            return 'Bags & Accessories'
        case 'tm':
            return 'Shoes & Accessories'
        case 'dp':
            return 'Accessories'
        case 'ms':
            return 'Accessories'
        case 'ev':
            return 'Clothing'
        case 'wl':
            return 'Clothing'
        case 'br':
            return 'Accessories'
    }
}

export const subCategoryName = (brand) => {
    switch (brand) {
        case 'ts':
            return 'Jewellery'
        case 'tm':
            return "Men's Sunglasses"
        case 'dp':
            return 'Jewellery'
        case 'ms':
            return 'Jewellery'
        case 'ev':
            return 'View All Jewellery'
        case 'wl':
            return 'View All Accessories'
        case 'br':
            return 'Mens Bags'
    }
}
