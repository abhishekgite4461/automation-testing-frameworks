export const oneSizeProductsSearchTerm = (brand) => {
    switch (brand) {
        case 'ts':
            return 'necklace'
        case 'ms':
            return 'necklace'
        case 'dp':
            return 'necklace'
        case 'tm':
            return 'sunglasses'
        case 'wl':
            return 'necklace'
        case 'ev':
            return 'necklace'
        case 'br':
            return 'bag'
    }
}

export const multipleSizeProductsSearchTerm = (brand) => {
    switch (brand) {
        case 'ts':
            return 'black'
        case 'ms':
            return 'black'
        case 'dp':
            return 'black'
        case 'tm':
            return 'blue jeans'
        case 'wl':
            return 'black'
        case 'ev':
            return 'black'
        case 'br':
            return 'shirt'
    }
}

export const secondaryOneSizeProductSearchTerm = (brand) => {
    switch (brand) {
        case 'ts':
            return 'socks'
        case 'ms':
            return 'socks'
        case 'dp':
            return 'scarf'
        case 'tm':
            return 'socks'
        case 'wl':
            return 'scarf'
        case 'ev':
            return 'scarf'
        case 'br':
            return 'socks'
    }
}

export const tertiaryOneSizeProductSearchTerm = (brand) => {
    switch (brand) {
        case 'ts':
            return 'watch'
        case 'ms':
            return 'earring'
        case 'dp':
            return 'watch'
        case 'tm':
            return 'watch'
        case 'wl':
            return 'ring'
        case 'ev':
            return 'ring'
        case 'br':
            return 'watch'
    }
}
