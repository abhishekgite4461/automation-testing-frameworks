const brandShortNames = {
    burton: 'br',
    evans: 'ev',
    missselfridge: 'ms',
    dorothyperkins: 'dp',
    topman: 'tm',
    topshop: 'ts',
    wallis: 'wl',
}

const brandLongNames = {
    br: 'burton',
    ev: 'evans',
    ms: 'missselfridge',
    dp: 'dorothyperkins',
    tm: 'topman',
    ts: 'topshop',
    wl: 'wallis',
}

const brandDetails = {
    breu: {
        url: 'm.eu.burton-menswear.com',
        brandName: 'burton',
        languageLocale: 'en-gb',
    },
    bruk: {
        url: 'm.burton.co.uk',
        brandName: 'burton',
        languageLocale: 'en-gb',
        languageUrlFragment: 'en',
    },
    evde: {
        url: 'm.evansmode.de',
        brandName: 'evans',
        languageLocale: 'de-DE',
        formal: true,
    },
    eveu: {
        url: 'm.euro.evansfashion.com',
        brandName: 'evans',
        languageLocale: 'en-gb',
    },
    evuk: {
        url: 'm.evans.co.uk',
        brandName: 'evans',
        languageLocale: 'en-gb',
        languageUrlFragment: 'en',
    },
    evus: {
        url: 'm.evansusa.com',
        brandName: 'evans',
        languageLocale: 'en-gb',
    },
    mseu: {
        url: 'm.euro.missselfridge.com',
        brandName: 'missselfridge',
        languageLocale: 'en-gb',
    },
    msfr: {
        url: 'm.missselfridge.fr',
        brandName: 'missselfridge',
        languageLocale: 'fr-fr',
    },
    msde: {
        url: 'm.missselfridge.de',
        brandName: 'missselfridge',
        languageLocale: 'de-DE',
    },
    msus: {
        url: 'm.us.missselfridge.com',
        brandName: 'missselfridge',
        languageLocale: 'en-gb',
    },
    msuk: {
        url: 'm.missselfridge.com',
        brandName: 'missselfridge',
        languageLocale: 'en-gb',
        languageUrlFragment: 'en',
    },
    dpeu: {
        url: 'm.euro.dorothyperkins.com',
        brandName: 'dorothyperkins',
        languageLocale: 'en-gb',
    },
    dpus: {
        url: 'm.us.dorothyperkins.com',
        brandName: 'dorothyperkins',
        languageLocale: 'en-gb',
    },
    dpuk: {
        url: 'm.dorothyperkins.com',
        brandName: 'dorothyperkins',
        languageLocale: 'en-gb',
        languageUrlFragment: 'en',
    },
    tmeu: {
        url: 'm.eu.topman.com',
        brandName: 'topman',
        languageLocale: 'en-gb',
    },
    tmfr: {
        url: 'm.fr.topman.com',
        brandName: 'topman',
        languageLocale: 'fr-fr',
    },
    tmuk: {
        url: 'm.topman.com',
        brandName: 'topman',
        languageLocale: 'en-gb',
        languageUrlFragment: 'en',
    },
    tmde: {
        url: 'm.de.topman.com',
        brandName: 'topman',
        languageLocale: 'de-DE',
    },
    tmus: {
        url: 'm.us.topman.com',
        brandName: 'topman',
        languageLocale: 'en-gb',
    },
    tseu: {
        url: 'm.eu.topshop.com',
        brandName: 'topshop',
        languageLocale: 'en-gb',
    },
    tsfr: {
        url: 'm.fr.topshop.com',
        brandName: 'topshop',
        languageLocale: 'fr-fr',
    },
    tsde: {
        url: 'm.de.topshop.com',
        brandName: 'topshop',
        languageLocale: 'de-DE',
    },
    tsus: {
        url: 'm.us.topshop.com',
        brandName: 'topshop',
        languageLocale: 'en-gb',
    },
    tsuk: {
        url: 'm.topshop.com',
        brandName: 'topshop',
        languageLocale: 'en-gb',
        languageUrlFragment: 'en',
    },
    wlde: {
        url: 'm.wallismode.de',
        brandName: 'wallis',
        languageLocale: 'de-DE',
        formal: true,
    },
    wluk: {
        url: 'm.wallis.co.uk',
        brandName: 'wallis',
        languageLocale: 'en-gb',
        languageUrlFragment: 'en',
    },
    wleu: {
        url: 'm.euro.wallisfashion.com',
        brandName: 'wallis',
        languageLocale: 'en-gb',
    },
    wlus: {
        url: 'm.wallisfashion.com',
        brandName: 'wallis',
        languageLocale: 'en-gb',
    },
}

export {brandShortNames, brandLongNames, brandDetails}
