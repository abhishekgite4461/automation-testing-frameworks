/**
 * Utility method to disable the slide-in/slide-out animation for things like
 * burgerMenu and minibag
 */
module.exports = (selector) => {
    browser.execute((sel) => {
        const menuElements = window.document.querySelectorAll(sel)
        menuElements.forEach((element) => {
            element.style.transition = 'none'
            element.style.webkitTransition = 'none'
        })
    }, selector)
}
