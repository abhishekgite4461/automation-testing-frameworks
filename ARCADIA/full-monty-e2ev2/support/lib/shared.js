import logger from './../logger'
import randomString from 'randomstring'
import routes from '../constants/routes'
import {brandLongNames} from './../constants/brands'

/**
 * Set the size of the viewport. Enables us to switch to desktop or mobile layouts
 */
export const setupViewportSize = (breakpoint) => {
    if (breakpoint === 'desktop') {
        browser.setViewportSize({width: 1024, height: 768})
    } else {
        browser.setViewportSize({width: 414, height: 736})
    }
}

/**
 * Do an initial setup for the browser session
 */
export const setUp = (baseUrl = '') => {
    const targetUrl = `${baseUrl}${routes.login}`
    browser.url(targetUrl)
    browser.setCookie({
        name: 'featuresOverride',
        value: JSON.stringify({
            FEATURE_NEW_CHECKOUT: true,
            FEATURE_QUBIT_HIDDEN: true,
            FEATURE_HEADER_BIG: true,
            FEATURE_RESPONSIVE: true,
        }),
    })
    // Set cookie to disable the "we are using cookies" message
    browser.setCookie({
        name: `${brandLongNames[browser.options.brand]}-cookie-message`,
        value: JSON.stringify(new Date().getTime() + 6000000),
    })
    browser.refresh()
}

/**
 * Create a random user email account
 */
export const generateRandomEmail = () => {
    const email = randomString
        .generate({
            length: 12,
            charset: `alphabetic`,
        })
        .toLowerCase()
    // eslint-disable-next-line no-console
    logger.info(`User Generated Email Address - <${email}@sample.org>`)
    return `${email}@sample.org`
}

/**
 * Utility for checking is the test run is happening using a mobile breakpoint value
 * @returns {boolean} Returns true if the config breakpoint value includes "mobile"
 */
export const isMobileLayout = () =>
    browser.options.breakpoint.includes('mobile')

/**
 * Utility for checking is the test run is happening using a desktop breakpoint value
 * @returns {boolean} Returns true if the config breakpoint value is "desktop"
 */
export const isDesktopLayout = () => browser.options.breakpoint === 'desktop'
