import {buildBrandBaseUrl} from './brandURL'

const baseUrl = `${buildBrandBaseUrl(`${browser.options.brand}uk`)}/api`
const baseUrlUS = (() => {
    // Avoid failure for burton: Burton doesn't have a US shop
    if (browser.options.brand !== 'br')
        return `${buildBrandBaseUrl(`${browser.options.brand}us`)}/api`
})()

export default {
    headers: {
        'Content-Type': 'application/json',
        'BRAND-CODE': `${browser.options.brand}uk`,
    },
    navigation: {
        mobile: {
            method: 'get',
            path: `${baseUrl}/navigation/categories`,
            status: 'done',
        },
        desktop: {
            method: 'get',
            path: `${baseUrl}/desktop/navigation`,
            status: 'done',
        },
    },
    myAccount: {
        account: {
            method: 'get',
            path: `${baseUrl}/account`,
            status: 'done',
        },
        register: {
            method: 'post',
            path: `${baseUrl}/account/register`,
            status: 'done',
        },
        login: {
            method: 'post',
            path: `${baseUrl}/account/login`,
            status: 'done',
        },
        logout: {
            method: 'delete',
            path: `${baseUrl}/account/logout`,
            status: 'done',
        },
    },
    shoppingBag: {
        addItem: {
            method: 'post',
            path: `${baseUrl}/shopping_bag/add_item`,
            status: 'done',
        },
        items: {
            method: 'get',
            path: `${baseUrl}/shopping_bag/get_items`,
            status: 'done',
        },
        deleteItem: {
            method: 'delete',
            path: `${baseUrl}/shopping_bag/delete_item`,
            status: 'done',
        },
    },
    products: {
        productDetailsPage: {
            method: 'get',
            path: (productId) => `${baseUrl}/products/${productId}`,
            pathUS: (productId) => `${baseUrlUS}/products/${productId}`,
            status: 'done',
        },
        productsListingPage: {
            method: 'get',
            path: `${baseUrl}/products`,
            pathUS: `${baseUrlUS}/products`,
            status: 'done',
        },
        productsListingSEO: {
            method: 'get',
            path: `${baseUrl}/products/seo`,
            status: 'done',
        },
    },
    orders: {
        payOrder: {
            method: 'post',
            path: `${baseUrl}/order`,
            status: 'TODO',
        },
        updateOrder: {
            method: 'put',
            path: `${baseUrl}/order`,
            status: 'TODO',
        },
        orderComplete: {
            method: 'post',
            path: `${baseUrl}/order-complete`,
            status: 'TODO',
        },
    },
    checkout: {
        orderSummary: {
            method: 'get',
            path: `${baseUrl}/checkout/order_summary`,
            status: 'TODO',
        },
        updateOrderSummary: {
            method: 'put',
            path: `${baseUrl}/checkout/order_summary`,
            status: 'TODO',
        },
        updateOrderSummaryBillingAddress: {
            method: 'put',
            path: `${baseUrl}/checkout/order_summary/billing_address`,
            status: 'TODO',
        },
        updateOrderSummaryDeliveryAddress: {
            method: 'post',
            path: `${baseUrl}/checkout/order_summary/delivery_address`,
            status: 'TODO',
        },
        orderSummaryDeleteDeliveryAddress: {
            method: 'delete',
            path: `${baseUrl}/checkout/order_summary/delivery_address`,
            status: 'TODO',
        },
        addGiftCard: {
            method: 'post',
            path: `${baseUrl}/checkout/gift-card`,
            status: 'TODO',
        },
        deleteGiftCard: {
            method: 'delete',
            path: `${baseUrl}/checkout/gift-card`,
            status: 'TODO',
        },
    },
    payments: {
        payment: {
            method: 'get',
            path: `${baseUrl}/payments`,
            status: 'TODO',
        },
        deletePayment: {
            method: 'delete',
            path: `${baseUrl}/payments`,
            status: 'TODO',
        },
    },
    klarna: {
        klarnaSession: {
            method: 'post',
            path: `${baseUrl}/klarna-session`,
            status: 'TODO',
        },
        updateKlarnaSession: {
            method: 'put',
            path: `${baseUrl}/klarna-session`,
            status: 'TODO',
        },
    },
}
