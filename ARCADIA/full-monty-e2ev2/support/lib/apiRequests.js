import logger from './../logger'
import axios from 'axios'
import endPoint from './apiRoutes'
import {parentCategoryName, subCategoryName} from './../constants/categories'
import {userWithFullCheckoutProfilePayload} from '../../fixtures/userFullCheckoutProfile'
import {generateRandomEmail} from './shared'
import {
    oneSizeProductsSearchTerm,
    multipleSizeProductsSearchTerm,
} from './../constants/searchTerms'

export const addItemToShoppingBag = (testProductPayload, token) => {
    logger.info(`Adding Item To Shopping Bag...`)
    const headers = {
        'Content-Type': 'application/json',
        'BRAND-CODE': `${browser.options.brand}uk`,
        Cookie: token,
    }
    logger.info(
        'Test Product to be added to the Shopping Bag <',
        testProductPayload,
        '>'
    )
    let orderId

    browser.call(() => {
        return axios({
            method: 'post',
            url: endPoint.shoppingBag.addItem.path,
            headers,
            data: testProductPayload,
        })
            .then((response) => {
                logger.info(
                    `Shopping Bag Order Number - <${response.data.orderId}>`
                )
                orderId = response.data.orderId
            })
            .catch((error) => {
                logger.error('Adding to bag failed', error)
                throw error
            })
    })
    return orderId
}

export const createUserWithFullCheckoutProfile = (scenarioType = undefined) => {
    let userCredentials = {
        email: '',
        password: '',
    }
    try {
        logger.info(
            'Attempting to create a user with full checkout profile via the API ...'
        )
        // Register user
        const {email, password, token} = createUserProfile()
        userCredentials = {email, password}

        // We add item to the bag
        const {productId, sku, quantity} = browser.options.testData.product
        const testProductPayload = {productId, sku, quantity}
        const orderId = addItemToShoppingBag(testProductPayload, token)

        // Place order
        placeOrder(orderId, token)

        // Add second order if scenario includes merged bag
        if (scenarioType === 'Merged bag')
            addItemToShoppingBag(testProductPayload, token)

        logger.info(
            'User with full checkout profile created. User credentials: <',
            userCredentials,
            '>'
        )
    } catch (error) {
        logger.error(
            'Attempting to create a user with full checkout profile via the API has failed!',
            error
        )
        throw error
    }

    return userCredentials
}

/**
 * Create a partial profile user, returns email,password and token
 */
export const createUserWithPartialProfile = () => {
    let userCredentials = {
        email: '',
        password: '',
    }

    try {
        logger.info(
            'Attempting to create a user with partial checkout profile via the API ...'
        )
        // Register user
        const {email, password} = createUserProfile()
        userCredentials = {email, password}

        logger.info(
            'User with partial checkout profile created. User credentials: <',
            userCredentials,
            '>'
        )
    } catch (error) {
        logger.error(
            'Attempting to create a user with partial checkout profile via the API has failed!',
            error
        )
        throw error
    }

    return userCredentials
}

export const getProductDetails = (productId, country) => {
    logger.info(`Starting search for product with productId <${productId}>`)

    try {
        return searchForProductId(productId, country)
    } catch (error) {
        logger.error(`Searching for product <${productId}> failed...`, error)
        throw error
    }
}
const extractSubcategorySEO = () => {
    const listOfCategories = navigationCategories()
    const categoryBagsAccessories = listOfCategories.find((obj) => {
        return obj.label === parentCategoryName(browser.options.brand)
    })
    const subcategories = categoryBagsAccessories.navigationEntries
    return subcategories.find((obj) => {
        return obj.label === subCategoryName(browser.options.brand)
    }).seoUrl
}

export const getProductsFromSubCategory = () => {
    const urlSeo = extractSubcategorySEO()
    logger.info('Fetching products from sub-category...')
    const headers = {
        'Content-Type': 'application/json',
        'BRAND-CODE': `${browser.options.brand}uk`,
    }

    let returnedProducts = []
    browser.call(() => {
        return axios({
            method: 'get',
            url: endPoint.products.productsListingSEO.path,
            headers,
            params: {seoUrl: urlSeo},
        })
            .then((response) => {
                returnedProducts = response.data.products
            })
            .catch((error) => {
                logger.error('Return products from sub-category failed...')
                throw error
            })
    })
    return returnedProducts
}

// TODO: Unify both of these functions. `searchForProductsId()` is practically
// just a copy of `searchForProducts()`.

export const searchForProducts = (searchTerm, country) => {
    logger.info(`Searching for products with search term <${searchTerm}>...`)

    let headers = {'Content-Type': 'application/json'}
    let apiUrl

    switch (country) {
        case 'US':
            headers['BRAND-CODE'] = `${browser.options.brand}us`
            apiUrl = endPoint.products.productsListingPage.pathUS
            break
        default:
            headers['BRAND-CODE'] = `${browser.options.brand}uk`
            apiUrl = endPoint.products.productsListingPage.path
    }

    let returnedProducts = []
    browser.call(() => {
        return axios({
            method: 'get',
            url: apiUrl,
            headers,
            params: {q: searchTerm},
        })
            .then((response) => {
                returnedProducts = response.data.products
            })
            .catch((error) => {
                logger.error(
                    `Searching for products with search term <${searchTerm}> failed...`
                )
                throw error
            })
    })
    // We need to add the country to each returned product in order to
    // make it accessible in the functions isValidTestProductOneSize
    // and isValidTestProductMultipleSizes
    returnedProducts.forEach((product) => {
        product.country = country
    })
    return returnedProducts
}

export const searchForProductId = (productId, country) => {
    logger.info(`Searching for product with productId <${productId}>...`)

    let headers = {'Content-Type': 'application/json'}
    let apiUrl

    switch (country) {
        case 'US':
            headers['BRAND-CODE'] = `${browser.options.brand}us`
            apiUrl = endPoint.products.productDetailsPage.pathUS(productId)
            break
        default:
            headers['BRAND-CODE'] = `${browser.options.brand}uk`
            apiUrl = endPoint.products.productDetailsPage.path(productId)
    }

    let returnedProduct = {}
    browser.call(() => {
        return axios({
            method: 'get',
            url: apiUrl,
            headers,
        })
            .then((response) => {
                returnedProduct = response.data
            })
            .catch((error) => {
                logger.error(
                    `Searching for product with productId <${productId}> failed...`
                )
                throw error
            })
    })
    return returnedProduct
}

export const createUserProfile = () => {
    logger.info(`Creating User Account...`)
    const headers = {
        'Content-Type': 'application/json',
        'BRAND-CODE': `${browser.options.brand}uk`,
    }
    const email = generateRandomEmail()
    const password = 'monty1'
    let token = ''

    browser.call(() => {
        return axios({
            method: 'post',
            url: endPoint.myAccount.register.path,
            headers,
            data: {
                email,
                password,
                passwordConfirm: password,
                subscribe: false,
            },
        })
            .then((response) => {
                token = response.headers['set-cookie']
                    .toString()
                    .replace(/;.*$/, '')
                logger.info(
                    `User account created successfully. User Token - <${token}>`
                )
            })
            .catch((error) => {
                logger.error('Registering user failed... ', error)
                throw error
            })
    })
    return {
        email,
        password,
        token,
    }
}

export const searchForProductsSorted = (categoryString, sortModeString) => {
    const headers = {
        'Content-Type': 'application/json',
        'BRAND-CODE': `${browser.options.brand}uk`,
    }
    let returnedProducts = []
    browser.call(() => {
        return axios({
            method: 'get',
            url: endPoint.products.productsListingPage.path,
            headers,
            params: {sort: sortModeString, category: categoryString},
        })
            .then((response) => {
                returnedProducts = response.data.products
            })
            .catch((error) => {
                logger.error('Searching for products failed... ', error)
                throw error
            })
    })
    return returnedProducts
}

export const placeOrder = (orderId, token) => {
    logger.info(`Paying Order <${orderId}>...`)
    const headers = {
        'Content-Type': 'application/json',
        'BRAND-CODE': `${browser.options.brand}uk`,
        Cookie: token,
    }
    browser.call(() => {
        return axios({
            method: 'post',
            url: endPoint.orders.payOrder.path,
            headers,
            data: userWithFullCheckoutProfilePayload(orderId),
        })
            .then((response) => {
                if (response.data.completedOrder) {
                    logger.info(
                        `Completed Order Number - <${
                            response.data.completedOrder.orderId
                        }>`
                    )
                }
            })
            .catch((error) => {
                logger.error(`Paying Order <${orderId}> failed... `, error)
                throw error
            })
    })
}

/**
 * @desc this fetches the category on mobile. Desktop navigation EP is different
 *       but the end result using the mobile EP works for both layouts
 */
export const navigationCategories = () => {
    logger.info(`Fetching mobile Categories...`)
    const headers = {
        'Content-Type': 'application/json',
        'BRAND-CODE': `${browser.options.brand}uk`,
    }

    let listOfAvailableCategories = []
    browser.call(() => {
        return axios({
            method: 'get',
            url: endPoint.navigation.mobile.path,
            headers,
        })
            .then((response) => {
                listOfAvailableCategories = response.data
            })
            .catch((error) => {
                logger.error('Fetching mobile categories failed...', error)
                throw error
            })
    })

    return listOfAvailableCategories
}

const targetProduct = {id: 0, sku: 0, quantity: 1, size: ''}

const saveTargetProductId = (id) => {
    targetProduct.id = id
}

const saveTargetProductSku = (sku) => {
    targetProduct.sku = sku
}

const saveTargetProductSize = (size) => {
    targetProduct.size = size
}

const isValidTestProductOneSize = ({productId, country}) => {
    logger.info('Validating test product has only one size')
    let currentProduct = {}

    try {
        // The search request returns an array, even when the result contains just 1 item
        currentProduct = getProductDetails(productId, country)
    } catch (error) {
        logger.error(
            `While searching for product with <${productId}> we encountered an error: `,
            error
        )
        throw error
    }
    if (
        (currentProduct.items[0].size === 'ONE' ||
            currentProduct.items[0].size === '000') &&
        currentProduct.items[0].quantity >= 5
    ) {
        // We need to preserve product details that are not available once this function returns
        saveTargetProductId(productId)
        saveTargetProductSku(currentProduct.items[0].sku)
        return true
    }

    return false
}

const isValidTestProductMultipleSizes = ({productId, country}) => {
    logger.info('Validating test product has multiple sizes')
    let currentProduct = {}

    try {
        // The search request returns an array, even when the result contains just 1 item
        currentProduct = getProductDetails(productId, country)
    } catch (error) {
        logger.error(
            `While searching for product with <${productId}> we encountered an error: `,
            error
        )
        throw error
    }

    let prodWithMultipleSizes
    if (
        currentProduct.items.length > 1 &&
        currentProduct.items.length <= 8 &&
        currentProduct.bundleProducts.length === 0
    ) {
        prodWithMultipleSizes = currentProduct.items.find((obj) => {
            return obj.quantity >= 5
        })
    }

    if (prodWithMultipleSizes) {
        // We need to preserve product details that are not available once this
        // function returns
        saveTargetProductId(productId)
        saveTargetProductSku(prodWithMultipleSizes.sku)
        saveTargetProductSize(prodWithMultipleSizes.size)
        return true
    }
    return false
}

/**
 * Utility method for finding a product with size 'ONE' and quantity >= 5
 * @returns {{productId: number, sku: string, quantity: number, sourceUrl: string}}
 * Take care that if no product matches the criteria this method will still
 * return an object, but with {{product.id = 0}}
 */

export const selectProductsOneSize = (
    searchTerm = oneSizeProductsSearchTerm(browser.options.brand),
    country = undefined
) => {
    let testProduct
    let searchResults

    try {
        if (searchTerm === 'seo') {
            searchResults = getProductsFromSubCategory()
        } else {
            searchResults = searchForProducts(searchTerm, country)
        }
    } catch (error) {
        logger.error(
            `While searching for <${searchTerm}> we encountered an error: `,
            error
        )
        throw error
    }

    // Search for the first product that has size 'ONE' and quantity >= 5
    if (searchTerm === multipleSizeProductsSearchTerm(browser.options.brand)) {
        testProduct = searchResults.find(isValidTestProductMultipleSizes)
    } else {
        testProduct = searchResults.find(isValidTestProductOneSize)
    }
    if (!testProduct)
        throw new Error('No testProduct found which fitted the criteria.')

    // We need to extend the testProduct object by adding in the missing details
    // and populating them with the previously saved data
    testProduct.productId = targetProduct.id
    testProduct.sku = targetProduct.sku
    testProduct.quantity = targetProduct.quantity
    testProduct.size = targetProduct.size

    return {
        productId: testProduct.productId,
        lineNumber: testProduct.lineNumber,
        sku: testProduct.sku,
        sourceUrl: testProduct.seoUrl,
        name: testProduct.name,
        quantity: testProduct.quantity,
        size: testProduct.size,
    }
}
