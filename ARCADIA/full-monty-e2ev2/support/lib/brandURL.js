import {brandDetails} from '../constants/brands'

const url = require('url')

const isStageEnv = (env) => {
    const stageEnv = [
        'stage',
        'stage-json',
        'integration',
        'integration-json',
        'showcase',
        'showcase-json',
        'perf',
        'storelocator',
        'mrcms',
        'devops',
        'new-sg',
    ]
    return stageEnv.includes(env)
}

const isProdEnv = (env) => {
    return env.includes('production')
}

const isNoAkamaiPreProd = (env) => {
    const noAkamaiPreprodEnv = ['no-cdn', 'montycms-next']
    return noAkamaiPreprodEnv.includes(env)
}

const isPreProd = (env) => {
    const preprodEnv = ['preprod']
    return preprodEnv.includes(env)
}

const getAuth = (env, requireBasicAuth) => {
    return isStageEnv(env) && requireBasicAuth ? 'monty:monty' : ''
}

const getHostname = (brandCode, target) => {
    const brand = brandDetails[brandCode.toLowerCase()]
    let hostname

    switch (true) {
        case isProdEnv(target):
            hostname = brand.url
            break
        // nb: bypasses Akamai caching as it was preventing basic auth from appearing, please discuss with DevOps if needs to be replaced
        case isNoAkamaiPreProd(target):
            hostname = `${target}.${brand.url.replace(
                /\./g,
                '-'
            )}.digital-prod.arcadiagroup.co.uk`
            break
        case isStageEnv(target):
            hostname = `${target}.${brand.url}`
            break
        default:
            hostname = `${target}.${brand.url}`
            break
    }

    return hostname
}

const getPort = (env) => {
    return isProdEnv(env) ||
        isStageEnv(env) ||
        isNoAkamaiPreProd(env) ||
        isPreProd(env)
        ? ''
        : '3000'
}

const getProtocol = (env) => {
    return isProdEnv(env) ||
        isStageEnv(env) ||
        isNoAkamaiPreProd(env) ||
        isPreProd(env)
        ? 'https'
        : 'http'
}

export const buildBrandBaseUrl = (
    brandCode = `${browser.options.brand}uk`,
    target = browser.options.target,
    requireBasicAuth = true
) => {
    // Add basic auth if it is a real full link
    if (/http(.*)/.test(target)) {
        let targetSplitUp = target.split('//')
        return `${targetSplitUp[0]}//monty:monty@${targetSplitUp[1]}`
    } else {
        const urlObject = {
            auth: getAuth(target, requireBasicAuth),
            hostname: getHostname(brandCode, target),
            port: getPort(target),
            protocol: getProtocol(target),
        }

        // extract full URL and save that as target var
        const fullUrl = url.format(urlObject)

        return fullUrl
    }
}

export const buildFullPDPUrl = (
    baseUrl,
    languageFragment,
    brandCode,
    productUrl
) => {
    return `${baseUrl}/${languageFragment}/${brandCode}/${productUrl}`
}
