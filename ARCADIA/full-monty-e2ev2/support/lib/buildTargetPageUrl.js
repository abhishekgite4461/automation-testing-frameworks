import {brandShortNames, brandDetails} from "./../constants/brands" // eslint-disable-line
import {buildBrandBaseUrl, buildFullPDPUrl} from './brandURL'
import {multipleSizesCategoryIds} from './../constants/categories'

module.exports = (brandName, siteCountry, pageName) => {
    const brandShortName = browser.options.brand
    const brandCode = `${brandShortName}${siteCountry.toLowerCase()}`
    const baseUrl = buildBrandBaseUrl(brandCode)

    switch (pageName) {
        case '<secondaryOneSizeProduct PDP>':
            return buildFullPDPUrl(
                baseUrl,
                brandDetails[brandCode].languageUrlFragment,
                brandCode,
                browser.options.testData.secondaryOneSizeProduct.pdpUrl
            )
        case '<tertiaryOneSizeProduct PDP>':
            return buildFullPDPUrl(
                baseUrl,
                brandDetails[brandCode].languageUrlFragment,
                brandCode,
                browser.options.testData.watchProduct.pdpUrl
            )
        case '<multipleSizesProduct PDP>':
            return buildFullPDPUrl(
                baseUrl,
                brandDetails[brandCode].languageUrlFragment,
                brandCode,
                browser.options.testData.multipleSizesProduct.pdpUrl
            )
        case 'sign-in':
            return `${baseUrl}/login`
        case 'subcategory':
            return `${baseUrl}${browser.options.testData.subCategoryUrl}`
        case '<multipleSizesCategory PLP>':
            browser.options.testData.currentCategoryId = multipleSizesCategoryIds(
                browser.options.brand
            )
            return `${baseUrl}${
                browser.options.testData.multipleSizesCategoryUrl
            }`
        case 'Checkout Login':
            return `${baseUrl}/checkout/login`
        default:
            return baseUrl
    }
}
