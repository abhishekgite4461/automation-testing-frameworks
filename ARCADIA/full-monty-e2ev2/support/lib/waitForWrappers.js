export const waitForElementToBeVisible = (
    selector,
    errorHint = '',
    timeout = browser.options.waitforTimeout
) => {
    browser.waitUntil(
        () => browser.isVisible(selector),
        timeout,
        `Expected the element with selector <${selector}> to become visible within ${timeout /
            1000} seconds. ${errorHint}`
    )
}

export const waitForElementToBeSelected = (
    selector,
    timeout = browser.options.waitforTimeout
) => {
    browser.waitUntil(
        () => browser.isSelected(selector),
        timeout,
        `Expected the element with selector <${selector}> to be <Selected> within ${timeout /
            20} seconds`
    )
}

export const waitForPartialUrl = (
    urlSegment,
    errorHint,
    timeout = browser.options.waitforTimeout
) => {
    browser.waitUntil(
        () => browser.getUrl().includes(urlSegment),
        timeout,
        `Expected the current url to contain <${urlSegment}>. Instead the current url is <${browser.getUrl()}> after ${timeout /
            1000} seconds. ${errorHint}`
    )
}

export const waitForElementToBeInvisible = (
    selector,
    errorHint = '',
    timeout = browser.options.waitforTimeout
) => {
    browser.waitUntil(
        () => browser.isVisible(selector, undefined, true),
        timeout,
        `Expected the element with selector <${selector}> to disappear within ${timeout /
            1000} seconds. ${errorHint}`
    )
}

export const waitForElementToExist = (
    selector,
    errorHint = '',
    timeout = browser.options.waitforTimeout
) => {
    browser.waitUntil(
        () => browser.waitForExist(selector, timeout),
        timeout,
        `Expected the element with selector <${selector}> to exist on the page <${browser.getUrl()}>.` +
            `The element was not available after ${timeout /
                1000} seconds. ${errorHint}`
    )
}

export const waitForElementToNotExist = (
    selector,
    errorHint = '',
    timeout = browser.options.waitforTimeout
) => {
    browser.waitUntil(
        () => !browser.isExisting(selector, timeout),
        timeout,
        `Expected the element with selector <${selector}> to not exist on the page <${browser.getUrl()}>.` +
            `The element was still present after ${timeout /
                1000} seconds. ${errorHint}`
    )
}

export const waitForElementToBeEnabled = (
    selector,
    errorHint = '',
    timeout = browser.options.waitforTimeout
) => {
    browser.waitUntil(
        () => browser.isEnabled(selector),
        20000,
        `Expected the element with selector <${selector}> to be <Enabled> within ${timeout /
            1000} seconds. ${errorHint}`
    )
}
