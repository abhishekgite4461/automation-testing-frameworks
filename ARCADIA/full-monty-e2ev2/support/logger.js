import bunyan from 'bunyan'
import bformat from 'bunyan-format'
const formatOut = bformat({outputMode: 'short'})

module.exports = bunyan.createLogger({
    name: 'e2ev2',
    stream: formatOut,
})
