@masterPass
Feature: MasterPass

  Scenario: Existing user with partial checkout profile uses MasterPass
    Given I am an existing user with partial checkout profile logged on the home page
    And I search for a product
    And I add a product to the shopping bag from the product detail page
    And I navigate to the delivery page from the modal
    And I enter the delivery details
    And I enter the billing details "MPASS" "" ""
    And I confirm and pay for the order
    And I create a MasterPass Account
    Then I should see the confirmation message "Thank You For Your Order"
