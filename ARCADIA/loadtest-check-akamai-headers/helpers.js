const argv = require('yargs').argv

const roundNumberToTwoDecimals = (num) => {
  return Math.round(num * 100) / 100
}

const calcuatePercentage = (smallPart, mainBucket) => {
  return smallPart / mainBucket * 100
}

const grabFirstCLIArgument = () => {
  return argv._[0]
}

const pushDualRunCookiesInHeadersFromResponseIntoArray = (
  response,
  ArrayToPushIn
) => {
  response.headers['set-cookie'].forEach((cookie) => {
    const matching = cookie.match('dual-run')
    matching && matching.length >= 1 && ArrayToPushIn.push(matching)
  })
}

module.exports = {
  roundNumberToTwoDecimals,
  calcuatePercentage,
  grabFirstCLIArgument,
  pushDualRunCookiesInHeadersFromResponseIntoArray,
}
