- `git clone`
- `npm install`
- Launch the script to just check Akamai: `node akamaiLoadTest.js
  NUMBEROFREQUESTS`
- Launch the script to check the full journey: `node wholeJourneyTests.js
  NUMBEROFREQUESTS`


# How to run the actual browser WDIO tests
- Run `node wdioshell.js NUMBEROFREQUESTS`, i.e. `node wdioshell.js 100`
- To change configuration about which browser to use, change lines 47 and 49 in
  `wdio.conf.js`
- Currently, the tests reload before grabbing the cookie and identifying the
  page. You can turn this on or off by commenting `browser.refresh()` in
  `e2ewdio.spec.js` line 9
- If you want to change the URL this test is going to, change the `baseUrl` in
  `wdio.conf.js`

## Settings
- Set the environment variable `HOST` to the website you want to access, i.e.
  `evans.co.uk` when you want to see how the brand Evans is doing
- Set the environment variable `DEVICE` to `desktop`, `mobile` or `tablet` to
  test with the respective device type (defaults to desktop)
- Set the environment variable `RETURNING` to `false` if you do not want to do a
  returning user scenario (i.e. refreshing after the first time landing on the
  page)
