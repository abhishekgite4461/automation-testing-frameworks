const deviceTypes = {
    desktop: {width: 1024, height: 768},
    mobile: {width: 414, height: 736},
    tablet: {width: 700, height: 736}
}

const checkCookieValueAndPage = (urlToVisit) => {
    browser.setViewportSize(deviceTypes[browser.options.deviceEmulated])
    browser.url(urlToVisit)
    browser.timeouts('page load', 30000)
    browser.options.returningScenario && browser.refresh()

    if (browser.isExisting('.Main-inner')) {
        console.log(urlToVisit + ': Monty page')
    } else {
        console.log(urlToVisit + ': Legacy page')
    }

    const cookie = browser.getCookie('dual-run')
    if (typeof cookie === 'array') {
        console.error(`Found multiple cookies on ${urlToVisit}`)
        console.error(cookie)
    } else {
        console.log(`${urlToVisit}: dual-run=${cookie.value}`)
        console.log(`${urlToVisit}: cookie-domain=${cookie.domain}<`)
    }
}

describe('Monty dual run investigation', function() {
    beforeEach(() => {
        const allCookies = browser.getCookie()
        allCookies.forEach((cookie) => {
            browser.deleteCookie(cookie.name)
        })

    })

    it (`Validating the dual-run cookie on m.${browser.options.hostToAccess}`, function() {
        checkCookieValueAndPage('http://' + 'm.' + browser.options.hostToAccess)
    })

    it (`Validating the dual-run cookie on www.${browser.options.hostToAccess}`, function() {
        checkCookieValueAndPage('http://' + 'www.' + browser.options.hostToAccess)
    })

    it (`Validating the dual-run cookie on ${browser.options.hostToAccess}`, function() {
        checkCookieValueAndPage('http://' + browser.options.hostToAccess)
    })
})
