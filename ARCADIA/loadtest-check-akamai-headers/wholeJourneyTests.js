const axios = require('axios')
const {
  roundNumberToTwoDecimals,
  calcuatePercentage,
  grabFirstCLIArgument,
  pushDualRunCookiesInHeadersFromResponseIntoArray,
} = require('./helpers')
const { userAgentHeaders } = require('./constants')

const allResults = []

const timesToRetry = grabFirstCLIArgument() || 10
console.log(`Requesting the page ${timesToRetry} times.`)
const timesToRetryArray = Array.apply(null, Array(timesToRetry)).map(
  Number.prototype.valueOf,
  0
)

timesToRetryArray.forEach(() => {
  axios('https://www.evans.co.uk', {
    headers: userAgentHeaders,
  })
    .then((result) => {
      allResults.push(result)
    })
    .catch((err) => {
      console.error(err)
    })
})

process.on('beforeExit', () => {
  const cookieArrayDualRun = []

  allResults.forEach((result) => {
    pushDualRunCookiesInHeadersFromResponseIntoArray(
      result,
      cookieArrayDualRun
    )
  })

  const legacyCookies = []
  const montyCookies = []
  const otherCookies = []

  cookieArrayDualRun.forEach((cookie) => {
    if (cookie.input.match('legacy')) {
      legacyCookies.push(cookie)
    } else if (cookie.input.match('monty')) {
      montyCookies.push(cookie)
    } else {
      otherCookies.push(cookie)
    }
  })
  console.log('=====================')
  console.info(
    `Legacy cookies: ${legacyCookies.length} | ${roundNumberToTwoDecimals(
      calcuatePercentage(legacyCookies.length, cookieArrayDualRun.length)
    )} %`
  )
  console.info(
    `Monty cookies: ${montyCookies.length} | ${roundNumberToTwoDecimals(
      calcuatePercentage(montyCookies.length, cookieArrayDualRun.length)
    )} %`
  )
  console.info(
    `Other cookies: ${otherCookies.length} | ${roundNumberToTwoDecimals(
      calcuatePercentage(otherCookies.length, cookieArrayDualRun.length)
    )} %`
  )

  // ========================================================================
  const legacyPage = []
  const montyPage = []
  const otherResult = []

  allResults.forEach((response) => {
    if (response.data.match('sp_23')) {
      legacyPage.push(response.data)
    } else if (response.data.match('Main-inner')) {
      montyPage.push(response.data)
    } else {
      otherResult.push(response.data)
    }
  })
  console.log('=====================')
  console.info(
    `Legacy page: ${legacyPage.length} | ${roundNumberToTwoDecimals(
      calcuatePercentage(legacyPage.length, allResults.length)
    )} %`
  )
  console.info(
    `Monty page: ${montyPage.length} | ${roundNumberToTwoDecimals(
      calcuatePercentage(montyPage.length, allResults.length)
    )} %`
  )
  console.info(
    `Other page: ${otherResult.length} | ${roundNumberToTwoDecimals(
      calcuatePercentage(otherResult.length, allResults.length)
    )} %`
  )
})
