const loadtest = require('loadtest')
const {
  roundNumberToTwoDecimals,
  calcuatePercentage,
  grabFirstCLIArgument,
  pushDualRunCookiesInHeadersFromResponseIntoArray,
} = require('./helpers')
const { userAgentHeaders } = require('./constants')

const timesToRetry = grabFirstCLIArgument() || 10
console.log(`Requesting the page ${timesToRetry} times.`)

const cookieArrayDualRun = []
const allCookiesInResponseHeader = []

const statusCallback = (error, result, latency) => {
  console.log(latency)
  allCookiesInResponseHeader.push(result.headers['set-cookie'])
  pushDualRunCookiesInHeadersFromResponseIntoArray(result, cookieArrayDualRun)
}
const loadtestOptions = {
  url: 'https://www.evans.co.uk',
  headers: userAgentHeaders,
  maxRequests: timesToRetry,
  statusCallback: statusCallback,
}

loadtest.loadTest(loadtestOptions, (error) => {
  error && console.error('Got an error: %s', error)
})

process.on('beforeExit', () => {
  const legacyCookies = []
  const montyCookies = []
  const otherCookies = []

  cookieArrayDualRun.forEach((cookie) => {
    if (cookie.input.match('legacy')) {
      legacyCookies.push(cookie)
    } else if (cookie.input.match('monty')) {
      montyCookies.push(cookie)
    } else {
      otherCookies.push(cookie)
    }
  })
  console.log('=====================')
  console.info(
    `Legacy cookies: ${legacyCookies.length} | ${roundNumberToTwoDecimals(
      calcuatePercentage(legacyCookies.length, cookieArrayDualRun.length)
    )} %`
  )
  console.info(
    `Monty cookies: ${montyCookies.length} | ${roundNumberToTwoDecimals(
      calcuatePercentage(montyCookies.length, cookieArrayDualRun.length)
    )} %`
  )
  console.info(
    `Other cookies: ${otherCookies.length} | ${roundNumberToTwoDecimals(
      calcuatePercentage(otherCookies.length, cookieArrayDualRun.length)
    )} %`
  )
})
