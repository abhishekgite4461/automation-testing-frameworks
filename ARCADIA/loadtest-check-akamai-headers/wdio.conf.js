const chromeFlags = ['--disable-infobars', '--no-sandbox', '--headless']
const checkIfMobileViewPortIsNecessary = () => process.env.DEVICE === 'mobile'
const checkIfTabletViewPortIsNecessary = () =>process.env.DEVICE === 'tablet'

checkIfMobileViewPortIsNecessary() && chromeFlags.push('--use-mobile-user-agent')
checkIfMobileViewPortIsNecessary() && chromeFlags.push('user-agent=Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3')
checkIfTabletViewPortIsNecessary() && chromeFlags.push('--force-tablet-mode')
checkIfTabletViewPortIsNecessary() && chromeFlags.push('Mozilla/5.0 (Linux; Android 6.0; vivo 1713 Build/MRA58K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.124 Mobile Safari/537.36')
console.log({chromeFlags})
const hostToAccess = process.env.HOST || 'evans.co.uk'
console.log(`HOST: ${hostToAccess}`)
const deviceEmulated = process.env.DEVICE || 'desktop'
console.log(`DEVICE TYPE: ${deviceEmulated}`)
const returningScenario = process.env.RETURNING !== 'false'
console.log(`RETURNING SCENARIO: ${returningScenario}`)

exports.config = {

    specs: [
        './wdioloadtest/*.spec.js'
    ],
    // Patterns to exclude.
    exclude: [
        // 'path/to/excluded/files'
    ],
    maxInstances: 1,
    //
    // If you have trouble getting all important capabilities together, check out the
    // Sauce Labs platform configurator - a great tool to configure your capabilities:
    // http://docs.saucelabs.com/reference/platforms-configurator
    //
    capabilities: [{
        // maxInstances can get overwritten per capability. So if you have an in-house Selenium
        // grid with only 5 firefox instances available you can make sure that not more than
        // 5 instances get started at a time.
        maxInstances: 5,
        //
        browserName: 'chrome',
        // '--proxy="5.39.48.34:443"', '--proxy-type=http'
        chromeOptions: {args: chromeFlags}
    }],
    //
    // ===================
    // Test Configurations
    // ===================
    sync: true,
    //
    // Level of logging verbosity: silent | verbose | command | data | result | error
    logLevel: 'error',
    //
    // Enables colors for log output.
    coloredLogs: true,
    //
    // Warns when a deprecated command is used
    deprecationWarnings: true,
    //
    // If you only want to run your tests until a specific amount of tests have failed use
    // bail (default is 0 - don't bail, run all tests).
    bail: 0,
    //
    // Saves a screenshot to a given path if a command fails.
    screenshotPath: './errorShots/',
    //
    // Set a base URL in order to shorten url command calls. If your `url` parameter starts
    // with `/`, the base url gets prepended, not including the path portion of your baseUrl.
    // If your `url` parameter starts without a scheme or `/` (like `some/path`), the base url
    // gets prepended directly.
    baseUrl: `http://${hostToAccess}`,
    //
    // Default timeout for all waitFor* commands.
    waitforTimeout: 30000,
    //
    // Default timeout in milliseconds for request
    // if Selenium Grid doesn't send response
    connectionRetryTimeout: 90000,
    //
    // Default request retries count
    connectionRetryCount: 3,
    //
    services: ['selenium-standalone'],

    seleniumInstallArgs: {version: '3.4.0'},
    seleniumArgs: {version: '3.4.0'},
    framework: 'mocha',
    mochaOpts: {
        ui: 'bdd',
        timeout: 30000
    },
    hostToAccess,
    deviceEmulated,
    returningScenario
}
