const shell = require('shelljs');
const {
    roundNumberToTwoDecimals,
    calcuatePercentage,
    grabFirstCLIArgument,
  } = require('./helpers')

if (!shell.which('git')) {
  shell.echo('Sorry, this script requires git');
  shell.exit(1);
}

const allStdOut = []

const timesToRetry = grabFirstCLIArgument() || 10
console.log(`Requesting the page ${timesToRetry} times.`)

shell.exec(`for run in {1..${timesToRetry}}; do npm run wdio; done`, function(code, stdout, stderr) {
    // Requrie wdio.conf to get the logs printing again
    require('./wdio.conf')
    console.log('=====================')
    allStdOut.push(stdout)

    const legacyPageCountWWW = stdout.match(new RegExp(`http://www.${process.env.HOST}: Legacy page`, "g")) || []
    const montyPageCountWWWW = stdout.match(new RegExp(`http://www.${process.env.HOST}: Monty page`, "g")) || []
    const legacyPageCountM = stdout.match(new RegExp(`http://m.${process.env.HOST}: Legacy page`, "g")) || []
    const montyPageCountM = stdout.match(new RegExp(`http://m.${process.env.HOST}: Monty page`, "g")) || []
    const legacyPageCountBase = stdout.match(new RegExp(`http://${process.env.HOST}: Legacy page`, "g")) || []
    const montyPageCountBase = stdout.match(new RegExp(`http://${process.env.HOST}: Monty page`, "g")) || []

    console.log({
        [`www.${process.env.HOST} Legacy page`]: legacyPageCountWWW.length,
        [`www.${process.env.HOST} Monty page`]: montyPageCountWWWW.length,
        [`m.${process.env.HOST} Legacy page`]: legacyPageCountM.length,
        [`m.${process.env.HOST} Monty page`]: montyPageCountM.length,
        [`${process.env.HOST} Legacy page`]: legacyPageCountBase.length,
        [`${process.env.HOST} Monty page`]: montyPageCountBase.length,
    })
    console.log('=====================')

    const legacyCookieCountWWW = stdout.match(new RegExp(`http://www.${process.env.HOST}: dual-run=legacy`, "g")) || []
    const montyCookieCountWWW = stdout.match(new RegExp(`http://www.${process.env.HOST}: dual-run=monty`, "g")) || []
    const mDirectCookieCountWWW = stdout.match(new RegExp(`http://www.${process.env.HOST}: dual-run=MDirect`, "g")) || []
    const mobileCookieCountWWW = stdout.match(new RegExp(`http://www.${process.env.HOST}: dual-run=Mobile`, "g")) || []
    const legacyCookieCountM = stdout.match(new RegExp(`http://m.${process.env.HOST}: dual-run=legacy`, "g")) || []
    const montyCookieCountM = stdout.match(new RegExp(`http://m.${process.env.HOST}: dual-run=monty`, "g")) || []
    const mDirectCookieCountM = stdout.match(new RegExp(`http://m.${process.env.HOST}: dual-run=MDirect`, "g")) || []
    const mobileCookieCountM = stdout.match(new RegExp(`http://m.${process.env.HOST}: dual-run=Mobile`, "g")) || []
    const legacyCookieCountBase = stdout.match(new RegExp(`http://${process.env.HOST}: dual-run=legacy`, "g")) || []
    const montyCookieCountBase = stdout.match(new RegExp(`http://${process.env.HOST}: dual-run=monty`, "g")) || []
    const mDirectCookieCountBase = stdout.match(new RegExp(`http://${process.env.HOST}: dual-run=MDirect`, "g")) || []
    const mobileCookieCountBase = stdout.match(new RegExp(`http://${process.env.HOST}: dual-run=Mobile`, "g")) || []

    console.log({
        [`www.${process.env.HOST} dual-run=legacy`]: legacyCookieCountWWW.length,
        [`www.${process.env.HOST} dual-run=monty`]: montyCookieCountWWW.length,
        [`www.${process.env.HOST} dual-run=MDirect`]: mDirectCookieCountWWW.length,
        [`www.${process.env.HOST} dual-run=Mobile`]: mobileCookieCountWWW.length,
        [`m.${process.env.HOST} dual-run=legacy`]: legacyCookieCountM.length,
        [`m.${process.env.HOST} dual-run=monty`]: montyCookieCountM.length,
        [`m.${process.env.HOST} dual-run=MDirect`]: mDirectCookieCountM.length,
        [`m.${process.env.HOST} dual-run=Mobile`]: mobileCookieCountM.length,
        [`${process.env.HOST} dual-run=legacy`]: legacyCookieCountBase.length,
        [`${process.env.HOST} dual-run=monty`]: montyCookieCountBase.length,
        [`${process.env.HOST} dual-run=MDirect`]: mDirectCookieCountBase.length,
        [`${process.env.HOST} dual-run=Mobile`]: mobileCookieCountBase.length,
    })

    console.log('=====================')

    let cookieDomainWWW = stdout.match(new RegExp(`http://www.${process.env.HOST}: cookie-domain=(.*)<`, "g")) || []
    let cookieDomainM = stdout.match(new RegExp(`http://m.${process.env.HOST}: cookie-domain=(.*)<`, "g")) || []
    let cookieDomainBase = stdout.match(new RegExp(`http://${process.env.HOST}: cookie-domain=(.*)<`, "g")) || []

    const clearDomainNameFromArray = (domainArray) => {
        return domainArray.map((cookie) => {
            return cookie.replace(/http(.*)cookie-domain=/, '').replace('<', '')
        })
    }
    cookieDomainWWW = clearDomainNameFromArray(cookieDomainWWW)
    cookieDomainM = clearDomainNameFromArray(cookieDomainM)
    cookieDomainBase = clearDomainNameFromArray(cookieDomainBase)

    const getCountsOfItems = (domainArrayCleaned) => {
        const counts = {}
        domainArrayCleaned.forEach((item) => {
            counts[item] = 1 + (counts[item] || 0)
        })
        return counts
    }

    console.log({
        [`www.${process.env.HOST} cookies`]: getCountsOfItems(cookieDomainWWW),
        [`m.${process.env.HOST} cookies`]: getCountsOfItems(cookieDomainM),
        [`${process.env.HOST} cookies`]: getCountsOfItems(cookieDomainBase),
    })


    console.log('=====================')

    const multipleCookiesWWW = stdout.match(new RegExp(`Found multiple cookies on http://www.${process.env.HOST}`, "g")) || []
    const multipleCookiesM = stdout.match(new RegExp(`Found multiple cookies on http://m.${process.env.HOST}`, "g")) || []
    const multipleCookiesBase = stdout.match(new RegExp(`Found multiple cookies on http://${process.env.HOST}`, "g")) || []
    console.log('Multiple cookies encountered:', {
        [`www.${process.env.HOST} Multiple cookies`]: multipleCookiesWWW.length,
        [`m.${process.env.HOST} Multiple cookies`]: multipleCookiesM.length,
        [`${process.env.HOST} Multiple cookies`]: multipleCookiesBase.length
    })

})
