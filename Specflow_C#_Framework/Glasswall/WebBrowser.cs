﻿using System;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;

namespace Glasswall
{
    public static class WebBrowser
    {
        private const string Key = "browser";

        public static ChromeDriver Current
        {
            get
            {
                if(!ScenarioContext.Current.ContainsKey(Key))
                 {
                    //Create and prepare a new instance of ChromeDriver
                    var chrome = new ChromeDriver();
                    // Implicit wait set to 30 sec
                    chrome.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
                    // PageLoad timeout set to 30 sec
                    chrome.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(30);
                    // Maximises the window
                    chrome.Manage().Window.Maximize();
                    ScenarioContext.Current[Key] = chrome;
                 }
                return ScenarioContext.Current[Key] as ChromeDriver;
            }
        }
    }
}
