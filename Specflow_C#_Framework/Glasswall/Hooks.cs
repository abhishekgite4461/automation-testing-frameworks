﻿using System;
using TechTalk.SpecFlow;

namespace Glasswall
{
    [Binding]
    public sealed class Hooks
    {
        [BeforeScenario]
        public void BeforeScenario()
        {
            
        }

        [AfterScenario]
        public void AfterScenario()
        {
            //This will Dispose the browser at the end of scenario
            WebBrowser.Current.Dispose();
        }
    }
}
