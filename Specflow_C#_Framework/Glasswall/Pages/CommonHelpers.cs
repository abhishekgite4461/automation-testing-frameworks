﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;


namespace Glasswall.Pages
{
    class CommonHelpers
    {

        public void verifyCurrentURL(string submenu_item)
        {
            string current_url = WebBrowser.Current.Url;
            try
            {
                // Following Switch statement will execute bassed on the given submenu_item
                switch (submenu_item)
                {
                    case "Team":
                        Assert.That(current_url, Is.EqualTo("https://www.glasswallsolutions.com/team/"));
                        break;
                    case "In the News":
                        Assert.That(current_url, Is.EqualTo("https://www.glasswallsolutions.com/news/"));
                        break;
                    case "Endorsements":
                        Assert.That(current_url, Is.EqualTo("https://www.glasswallsolutions.com/industry-endorsements/"));
                        break;
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public void verifyFirstImageURL()
        {
            string image_url = WebBrowser.Current.FindElement(By.ClassName("vc_single_image-img ")).GetAttribute("src");

            try
            { 
                // Following Assertion will check image url acquired in same as the given image url
                 Assert.That(image_url, Is.EqualTo("https://www.glasswallsolutions.com/wp-content/uploads/2016/10/Anne-Tiedemann.cropped-150x200.jpg"));
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        //Following will wait for a element till it is clickable by taking the locator and wait time (in seconds as input)
        public IWebElement waitUntilElementIsClickable(By Locator, int maxSeconds)
        {
            try
            { 
               return new WebDriverWait(WebBrowser.Current, TimeSpan.FromSeconds(maxSeconds)).Until(ExpectedConditions.ElementToBeClickable((Locator)));
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        //Following will hover on a element by taking the locator as input
        public void hoverActionOnElement(IWebElement Locator)
        {
            try
            { 
                new Actions(WebBrowser.Current).MoveToElement((Locator)).Build().Perform();
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
    }
}
