﻿using OpenQA.Selenium;
using System;

namespace Glasswall.Pages
{

    // This is the pageobject for homepage
    class HomePage
    {
        CommonHelpers commonHelpers = new CommonHelpers();

        public void goToPage()
        {
            try
            {
                WebBrowser.Current.Url = "https://www.glasswallsolutions.com";
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public void hoverOnMenuItem(string menu_item)
        {
            IWebElement company = WebBrowser.Current.FindElement(By.Id("menu-item-4817"));
            IWebElement news = WebBrowser.Current.FindElement(By.Id("menu-item-115"));

            try
            {   
                    // Following Switch statement will execute bassed on the given menu_item
                    switch (menu_item)
                    {
                        case "Company":
                            commonHelpers.hoverActionOnElement(company);
                            break;
                        case "News":
                            commonHelpers.hoverActionOnElement(news);
                            break;
                    }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public void clickOnSubMenuItem(string submenu_item)
        {
            try
            {
                // Following Switch statement will execute bassed on the given submenu_item
                switch (submenu_item)
                {
                    case "Team":
                        commonHelpers.waitUntilElementIsClickable(By.Id("menu-item-5718"), 5);
                        WebBrowser.Current.FindElement(By.Id("menu-item-5718")).Click();
                        break;
                    case "In the News":
                        commonHelpers.waitUntilElementIsClickable(By.Id("menu-item-5164"), 5);
                        WebBrowser.Current.FindElement(By.Id("menu-item-5164")).Click();
                        break;
                    case "Endorsements":
                        commonHelpers.waitUntilElementIsClickable(By.Id("menu-item-5866"), 5);
                        WebBrowser.Current.FindElement(By.Id("menu-item-5866")).Click();
                        break;
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
    }
}
