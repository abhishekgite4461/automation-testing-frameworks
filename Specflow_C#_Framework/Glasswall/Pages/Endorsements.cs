﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace Glasswall.Pages
{
    class Endorsements
    {
        public void verifyImageDisplayed()
        {
            try
            {
                // Following Assertion will check if a image is dislayed or not
                Assert.IsTrue(WebBrowser.Current.FindElement(By.CssSelector("[href*='http://www.ukitindustryawards.co.uk/static/winners-2017']")).Displayed);
                Console.Write("Image Present");
            }
            catch (Exception e)
            {
                Console.Write(e);
                throw (e);
            }
        }
    }
}
