﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace Glasswall.Pages
{
    class InTheNews
    {
        public void verifyTitle(string title)
        {
            string page_title = WebBrowser.Current.FindElement(By.Id("slide-34-layer-4")).Text;

            try
            { 
                //Following Assertion will compare if the acquire heading is equal to the input heading
                //The value stored in page_title or the input parameter 'title' are not actual heading of the page. As per the test required the variables are just named as title.
                Assert.That(page_title, Is.EqualTo(title));
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
    }
}
