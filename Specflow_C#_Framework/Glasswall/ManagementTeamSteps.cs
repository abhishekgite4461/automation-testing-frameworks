﻿using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium;   
using OpenQA.Selenium.Interactions;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using Glasswall.Pages;

namespace Glasswall
{
    [Binding]
    public class ManagementTeamSteps
    {
        [Given(@"I am on the homepage")]
        public void GivenIAmOnTheHomepage()
        {
            //WebBrowser.Current.Url = "https://www.glasswallsolutions.com";
            HomePage home = new HomePage();
            home.goToPage();
        }
        
        [Given(@"I hover on ""(.*)"" menu item")]
        public void GivenIHoverOnMenuItem(string menu_item)
        {
            Actions action = new Actions(WebBrowser.Current);
            //IWebElement mouseEvent = WebBrowser.Current.FindElement(By.CssSelector("[href*='https://www.glasswallsolutions.com/company/']"));

            switch (menu_item)
            {
                case "Company":
                    //IWebElement mouseEvent = WebBrowser.Current.FindElement(By.CssSelector("[href*='https://www.glasswallsolutions.com/company/']"));
                    IWebElement mouseEvent = WebBrowser.Current.FindElement(By.Id("menu-item-4817"));
                    action.MoveToElement(mouseEvent).Build().Perform();
                    //Thread.Sleep(5000);
                    break;
                case "News":
                    IWebElement mouseEvent2 = WebBrowser.Current.FindElement(By.Id("menu-item-115"));
                    action.MoveToElement(mouseEvent2).Build().Perform();
                    break;
            }


            //action.MoveToElement(mouseEvent).Perform();
        }
        
        [When(@"I click on the option ""(.*)""")]
        public void WhenIClickOnTheOption(string sub_menu_item)
        {
           //WebDriverWait wait = new WebDriverWait(WebBrowser.Current, TimeSpan.FromSeconds(5));
            switch (sub_menu_item)
            {
                case "Team":
                    WebDriverWait wait = new WebDriverWait(WebBrowser.Current, TimeSpan.FromSeconds(5));
                    wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("menu-item-5718")));
                    WebBrowser.Current.FindElement(By.Id("menu-item-5718")).Click();
                    break;
                case "In the News":
                    WebDriverWait wait2 = new WebDriverWait(WebBrowser.Current, TimeSpan.FromSeconds(5));
                    wait2.Until(ExpectedConditions.ElementToBeClickable(By.Id("menu-item-5164")));
                    WebBrowser.Current.FindElement(By.Id("menu-item-5164")).Click();
                    break;
                case "Endorsements":
                    WebDriverWait wait3 = new WebDriverWait(WebBrowser.Current, TimeSpan.FromSeconds(5));
                    wait3.Until(ExpectedConditions.ElementToBeClickable(By.Id("menu-item-5866")));
                    WebBrowser.Current.FindElement(By.Id("menu-item-5866")).Click();
                    break;
            }
        }
        
        [Then(@"the ""(.*)"" page should be displayed")]
        public void ThenThePageShouldBeDisplayed(string page)
        {
            string url = WebBrowser.Current.Url;
            //Assert.That(url, Is.EqualTo("https://www.glasswallsolutions.com/team/"));

            switch (page)
            {
                case "Team":
                    Assert.That(url, Is.EqualTo("https://www.glasswallsolutions.com/team/"));
                    break;
                case "In the News":
                    Assert.That(url, Is.EqualTo("https://www.glasswallsolutions.com/news/"));
                    break;
                case "Endorsements":
                    Assert.That(url, Is.EqualTo("https://www.glasswallsolutions.com/industry-endorsements/"));
                    break;
            }
        }
        
        [Then(@"the URL of the first image is present")]
        public void ThenTheURLOfTheFirstImageIsPresent()
        {
            string firstImage = WebBrowser.Current.FindElement(By.ClassName("vc_single_image-img ")).GetAttribute("src");
            Assert.That(firstImage, Is.EqualTo("https://www.glasswallsolutions.com/wp-content/uploads/2016/10/Anne-Tiedemann.cropped-150x200.jpg"));
        }

        [Then(@"the page title displayed is ""(.*)""")]
        public void ThenThePageTitleDisplayedIs(string title)
        {
            string page_title = WebBrowser.Current.FindElement(By.Id("slide-34-layer-4")).Text;
            Assert.That(page_title, Is.EqualTo(title));
        }

        [Then(@"the image for innovation award is displayed correctly")]
        public void ThenTheImageForInnovationAwardIsDisplayedCorrectly()
        {
            try
            {
                Assert.IsTrue(WebBrowser.Current.FindElement(By.CssSelector("[href*='http://www.ukitindustryawards.co.uk/static/winners-2017']")).Displayed);
                Console.Write("Image Present");
            }
            catch(Exception e)
            {
                Console.Write(e);
                throw (e);
            }
        }


    }
}
