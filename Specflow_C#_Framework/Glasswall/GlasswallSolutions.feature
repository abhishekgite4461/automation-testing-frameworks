﻿Feature: ManagementTeam
	In order to navigate to different pages on GlassWell website
	As a user
	I want to see the correct page

Scenario: User can visit the Team page correctly
	Given I am on the homepage
	And I hover on "Company" menu item
	When I click on the option "Team"
	Then the "Team" page should be displayed
	And the URL of the first image is present


Scenario: User can visit the News menu item
	Given I am on the homepage
	And I hover on "News" menu item
	When I click on the option "In the News"
	Then the "In the News" page should be displayed
	And the page title displayed is "IN THE NEWS"


Scenario: User can visit the Endorsments menu item
	Given I am on the homepage
	And I hover on "Company" menu item
	When I click on the option "Endorsements"
	Then the "Endorsements" page should be displayed
	And the image for innovation award is displayed correctly