﻿using System;
using TechTalk.SpecFlow;
using Glasswall.Pages;

namespace Glasswall
{
    [Binding]
    public class GlasswallSolutions
    {
        //Instances of the page objects
        HomePage homepage = new HomePage();
        CommonHelpers commonHelpers = new CommonHelpers();
        InTheNews inTheNews = new InTheNews();
        Endorsements endorsements = new Endorsements();
                
        [Given(@"I am on the homepage")]
        public void GivenIAmOnTheHomepage()
        {
            // This will execute the method goToPage and launch the webpage 
            homepage.goToPage();
        }
        
        [Given(@"I hover on ""(.*)"" menu item")]
        public void GivenIHoverOnMenuItem(string menu_item)
        {
            // This will execute the method hoverOnMenuItem based on the given menu item
            homepage.hoverOnMenuItem(menu_item);
        }
        
        [When(@"I click on the option ""(.*)""")]
        public void WhenIClickOnTheOption(string sub_menu_item)
        {
            // This will execute the method clickOnSubMenuItem based on the given sub-menu item
            homepage.clickOnSubMenuItem(sub_menu_item);
        }
        
        [Then(@"the ""(.*)"" page should be displayed")]
        public void ThenThePageShouldBeDisplayed(string page)
        {
            // This will execute the method verifyCurrentURL based on the given page input
            commonHelpers.verifyCurrentURL(page);
        }
        
        [Then(@"the URL of the first image is present")]
        public void ThenTheURLOfTheFirstImageIsPresent()
        {
            // This will execute the method verifyFirstImageURL and verify the URL of the image
            commonHelpers.verifyFirstImageURL();
        }

        [Then(@"the page title displayed is ""(.*)""")]
        public void ThenThePageTitleDisplayedIs(string title)
        {
            // This will execute the method verifyTitle and verify the title with the input title
            inTheNews.verifyTitle(title);
        }

        [Then(@"the image for innovation award is displayed correctly")]
        public void ThenTheImageForInnovationAwardIsDisplayedCorrectly()
        {
            // This will execute the method verifyImageDisplayed and verify if the image is displayed or not
            endorsements.verifyImageDisplayed();
        }
    }
}
