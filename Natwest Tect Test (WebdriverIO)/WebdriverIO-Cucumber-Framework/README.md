# WebdriverIO-Cucumber-Framework

## The Challenge
Create an E2E test that comprises the below steps:


1.  Login to https://www.saucedemo.com/ using the "standard_user" account

2.  Sort the products by Price (high to low)

3.  Add the cheapest & costliest products to your basket

4.  Open the basket

5.  Checkout

6.  Enter details and Finish the purchase


## Requirements
- Node version 8 or higher

## Setup
- Clone the git repo
- Run following command in terminal it will install all the npm modules
```sh
    $ npm install
``` 

## How to run the test
Following command will run all the 4 feature files parallely
```sh
    $ npm run test
``` 

## Framework Structure
- 'features' folder includes the feature files and other sub folders 'pageobjects' and 'step-definitions'
- 'pageobjects' folder contains page object files for each page involved in this testing project. The 'page.js' file contains the base page class which extends to other ***.page.js files
- 'step-definitions' folder contains the step definition file
- 'package.json' holds verious metadata about the project
- 'wdio.config.js' this config file contains all the necessary information to run the test suite

## Tools/Libraries
- WebdriverIO
- Chai
- Cucumber