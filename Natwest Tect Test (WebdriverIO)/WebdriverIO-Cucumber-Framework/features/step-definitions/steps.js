import { Given, When, Then } from 'cucumber';

import LoginPage from '../pageobjects/login.page';
import ProductsPage from '../pageobjects/products.page';
import CartPage from '../pageobjects/cart.page';
import CheckoutInformationPage from '../pageobjects/checkoutInformation.page';
import CheckoutOverviewPage from '../pageobjects/checkoutOverview.page';
import CheckoutCompletePage from '../pageobjects/checkoutComplete.page';


const pages = {
    login: LoginPage
}

Given(/^I am on the (\w+) page$/, (page) => {
    pages[page].open()
});

Given(/^I login with (\w+) and (.+)$/, (username, password) => {
    LoginPage.login(username, password)
    ProductsPage.productsPageHeaderIsDisplayed();
});

Given(/^sort the products by (.*)$/, (sortOption) => {
    ProductsPage.selectOptionFromProductsSortContainer(sortOption);
});

When(/^I add the cheapest and costliest product to the basket$/, () => {
    ProductsPage.clickAddToCartButtonForGivenProduct('Cheapest');
    ProductsPage.clickAddToCartButtonForGivenProduct('Costliest');
});

When(/^I open the basket$/, () => {
    ProductsPage.clickShoppingCartButton();
    CartPage.productsPageHeaderIsDisplayed();
});

When(/^I click on Checkout button$/, () => {
    CartPage.clickCheckoutButton();
});

Then(/^I enter (\w+), (\w+) and (\w+) & click continue$/, (firstname, lastname, zipcode) => {
    CheckoutInformationPage.checkoutInformationPageIsDisplayed();
    CheckoutInformationPage.enterDetails(firstname, lastname, zipcode);
    CheckoutInformationPage.clickContinueButton();

});

Then(/^I click Finish button on checkout overview page$/, () => {
    CheckoutOverviewPage.clickOverviewButton();
});

Then(/^I can see (.*)$/, (message) => {
    CheckoutCompletePage.thankYouMessageIsDisplayed(message);
});
