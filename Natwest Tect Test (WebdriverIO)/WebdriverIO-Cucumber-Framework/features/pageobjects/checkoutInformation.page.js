import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class CheckoutInformationPage extends Page {
    /**
     * define selectors using getter methods
     */
    get firstNameTextBox() { return $('#first-name') }
    get lastNameTextBox() { return $('#last-name')}
    get postcodeTextBox() { return $('#postal-code')}
    get continueButton() { return $('.btn_primary.cart_button')}
    

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    checkoutInformationPageIsDisplayed() {
        this.firstNameTextBox.waitForDisplayed({ timeout: 5000, timeoutMsg: "Checkout information pages is not displayed" })
    }

    enterDetails(firstname, lastname, zipcode) {
        this.firstNameTextBox.setValue(firstname);
        this.lastNameTextBox.setValue(lastname);
        this.postcodeTextBox.setValue(zipcode);
    }

    clickContinueButton() {
        this.continueButton.click();
    }
}

export default new CheckoutInformationPage();
