import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class CartPage extends Page {
    /**
     * define selectors using getter methods
     */
    get cartPageHeader() { return $('.shopping_cart_container') }
    get checkoutButton() { return $('.btn_action.checkout_button')}
    

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    productsPageHeaderIsDisplayed() {
        this.cartPageHeader.waitForDisplayed({ timeout: 5000, timeoutMsg: "Cart page header is not displayed" })
    }

    clickCheckoutButton() {
        browser.waitUntil(() => {
            return this.checkoutButton.isDisplayed();
        }, 10000, 'Checkout button is not displayed');
        this.checkoutButton.click();
    }
}

export default new CartPage();
