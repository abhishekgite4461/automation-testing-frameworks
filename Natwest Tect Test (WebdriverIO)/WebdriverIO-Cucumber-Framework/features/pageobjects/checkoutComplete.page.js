import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class CheckoutCompletePage extends Page {
    /**
     * define selectors using getter methods
     */
    
    get completeMessage() { return $('.complete-header')}
    

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    thankYouMessageText() {
        browser.waitUntil(() => {
            return this.completeMessage.isDisplayed();
        }, 10000, 'Thank you message is not displayed');
        return this.completeMessage.getText();
    }

    thankYouMessageIsDisplayed(message) {
        expect(this.thankYouMessageText()).to.be.equal(message);
    }


}

export default new CheckoutCompletePage();
