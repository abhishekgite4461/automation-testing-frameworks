import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class CheckoutOverviewPage extends Page {
    /**
     * define selectors using getter methods
     */
    
    get finishButton() { return $('.btn_action.cart_button')}
    

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */

    clickOverviewButton() {
        browser.waitUntil(() => {
            return this.finishButton.isDisplayed();
        }, 10000, 'Checkout Overview page is not displayed');
        this.finishButton.click();
    }
}

export default new CheckoutOverviewPage();
