import Page from './page';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class ProductsPage extends Page {
    /**
     * define selectors using getter methods
     */
    get productsPageHeader() { return $('.product_label') }
    get productsSortContainer() { return $('.product_sort_container') }
    get addToCartButtonForAllProducts() { return $$('.btn_primary.btn_inventory') }
    get shoppingCartButton() { return $('.shopping_cart_container')}

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    productsPageHeaderIsDisplayed() {
        this.productsPageHeader.waitForDisplayed({ timeout: 5000, timeoutMsg: "Products page header is not displayed" })
    }

    selectOptionFromProductsSortContainer(sortOption) {
        this.productsSortContainer.selectByVisibleText(sortOption);
    }

    clickAddToCartButtonForGivenProduct(product) {
        switch (product) {
            case "Cheapest":
                this.addToCartButtonForAllProducts[this.addToCartButtonForAllProducts.length - 1].click();
                break;
            case "Costliest":
                this.addToCartButtonForAllProducts[0].click();
                break;
        }
    }

    clickShoppingCartButton() {
        browser.waitUntil(() => {
            return this.shoppingCartButton.isDisplayed();
        }, 10000, 'Shopping Cart button is not displayed');
        this.shoppingCartButton.click();
    }

    /**
     * overwrite specifc options to adapt it to page object
     */
    open() {
        return super.open('');
    }
}

export default new ProductsPage();
