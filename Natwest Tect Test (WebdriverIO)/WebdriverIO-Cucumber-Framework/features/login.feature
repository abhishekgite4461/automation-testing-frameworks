Feature: 

  Background:
    Given I am on the login page
  
  Scenario Outline: As a user, I can log into the secure area
    Given I login with <username> and <password>
    And sort the products by Price (high to low)
    When I add the cheapest and costliest product to the basket
    And I open the basket
    And I click on Checkout button
    Then I enter <firstname>, <lastname> and <zipcode> & click continue
    And I click Finish button on checkout overview page
    And I can see <message>

    Examples:
      | username      | password     | firstname | lastname | zipcode | message                  |
      | standard_user | secret_sauce | John      | Wick     | 100001  | THANK YOU FOR YOUR ORDER |

