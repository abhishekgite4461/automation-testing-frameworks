const TagExpressionParser = require('@cucumber/tag-expressions');

function matchTags(tagExpression, tags) {
    const expressionNode = TagExpressionParser.default(tagExpression);
    return expressionNode.evaluate(tags);
}

module.exports = {
    toMatchTags: () => ({
        compare: (actual, expected) => ({
            pass: matchTags(actual, expected)
        })
    }),

    toMatchTag: () => ({
        compare: (actual, expected) => ({
            pass: matchTags(actual, [expected])
        })
    })
};
