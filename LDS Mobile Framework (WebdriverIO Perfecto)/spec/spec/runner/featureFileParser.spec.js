const path = require('path');
const _ = require('lodash');

describe('featureFileParser', () => {
    const parseFeatureFiles = require('../../../src/runner/featureFileParser');

    const resourcesRoot = '../../resources/runner/featureFileParser.spec';
    const noTags = path.join(__dirname, `${resourcesRoot}/noTags.feature`);
    const scenarioTagsFeatureFile = path.join(__dirname, `${resourcesRoot}/scenarioTags.feature`);
    const featureTagsFeatureFile = path.join(__dirname, `${resourcesRoot}/featureTags.feature`);
    const tagsEverywhere = path.join(__dirname, `${resourcesRoot}/tagsEverywhere.feature`);
    const backgroundFeatureFile = path.join(__dirname, `${resourcesRoot}/background.feature`);

    const tags = ((tagNames) => _.fromPairs(tagNames.map((tag) => [tag, 'Test Tag'])))(
        ['@tag1', '@tag2', '@tag3', '@tag4', '@tag5', '@tag6', '@tag7', '@sw-tag1', '@sw-tag2', '@nga3', '@android', '@ios', '@foobar']
    );

    const mungeName = (features) => {
        features.forEach((feature) => (feature.name = path.basename(feature.name)));
        return features;
    };

    it('parse a feature file without tags', (done) => {
        parseFeatureFiles([noTags], tags).then((features) => {
            expect(mungeName(features)).toEqual([{
                name: 'noTags.feature',
                switchTags: [],
                tags: [[]]
            }
            ]);
            done();
        }).catch((err) => {
            this.fail(err);
            done();
        });
    });

    it('parse a feature file with tags on the scenarios', (done) => {
        parseFeatureFiles([scenarioTagsFeatureFile], tags).then((features) => {
            expect(mungeName(features)).toEqual([{
                switchTags: [],
                tags: [['@tag1', '@tag2'], ['@tag3', '@tag4']],
                name: 'scenarioTags.feature'
            }]);
            done();
        }).catch((err) => {
            this.fail(err);
            done();
        });
    });

    it('parse a feature file with tags on the feature', (done) => {
        parseFeatureFiles([featureTagsFeatureFile], tags).then((features) => {
            expect(mungeName(features)).toEqual([{
                name: 'featureTags.feature',
                switchTags: ['@sw-tag1', '@sw-tag2'],
                tags: [['@sw-tag1', '@sw-tag2', '@tag3']]
            }]);
            done();
        }).catch((err) => {
            this.fail(err);
            done();
        });
    });

    it('parse a feature file with tags everywhere', (done) => {
        parseFeatureFiles([tagsEverywhere], tags).then((features) => {
            expect(mungeName(features)).toEqual([{
                switchTags: ['@sw-tag1', '@sw-tag2'],
                tags: [
                    ['@sw-tag1', '@sw-tag2', '@tag3', '@tag4', '@tag5'],
                    ['@sw-tag1', '@sw-tag2', '@tag3', '@tag6', '@tag7']
                ],
                name: 'tagsEverywhere.feature'
            }]);
            done();
        }).catch((err) => {
            this.fail(err);
            done();
        });
    });

    it('parse a feature file with a background step', (done) => {
        parseFeatureFiles([backgroundFeatureFile], tags).then((features) => {
            expect(mungeName(features)).toEqual([{
                switchTags: ['@sw-tag1', '@sw-tag2'],
                tags: [
                    ['@sw-tag1', '@sw-tag2', '@tag3', '@tag4', '@tag5'],
                    ['@sw-tag1', '@sw-tag2', '@tag3', '@tag6', '@tag7']
                ],
                name: 'background.feature'
            }]);
            done();
        }).catch((err) => {
            this.fail(err);
            done();
        });
    });
});
