describe('perfectoReservation', () => {
    process.env.PERFECTO_USER = 'perfectouser';
    process.env.PERFECTO_PWD = 'perfectopassword';
    process.env.PERFECTO_HOST = 'lloyds.perfectomobile.com';
    const {createPresetReservation} = require('../../../src/scripts/createDeviceReservations');
    it('When there is no preset device must not be reserved', (done) => {
        createPresetReservation()
            .then((res) => {
                expect(res).toEqual('Invalid preset or No devices to reserve');
                done();
            });
    });

    it('Device must not be reserved when preset doesnt exist', (done) => {
        createPresetReservation('test')
            .then((res) => {
                expect(res).toEqual('Invalid preset or No devices to reserve');
                done();
            });
    });

    it('Device must not be reserved when preset doesnt have devices', (done) => {
        createPresetReservation('parallelExecution')
            .then((res) => {
                expect(res).toEqual('Invalid preset or No devices to reserve');
                done();
            });
    });

    it('Device must be reserved when valid preset is used', (done) => {
        const {PerfectoRestClient} = require('../../../src/utils/perfectoRestClient');
        spyOn(PerfectoRestClient.prototype, 'removeDeviceReservations')
            .and
            .returnValue(Promise.resolve(''));
        spyOn(PerfectoRestClient.prototype, 'makeDeviceReservations')
            .and
            .returnValue(Promise.resolve(['1234']));

        createPresetReservation('weekendCompatibilityAndroid')
            .then(() => {
                done();
            });
    });

    it('Error Handling when device reservation fails', (done) => {
        const {PerfectoRestClient} = require('../../../src/utils/perfectoRestClient');
        spyOn(PerfectoRestClient.prototype, 'removeDeviceReservations')
            .and
            .returnValue(Promise.resolve(''));
        spyOn(PerfectoRestClient.prototype, 'makeDeviceReservations')
            .and
            // eslint-disable-next-line prefer-promise-reject-errors
            .returnValue(Promise.reject('Invalid device ID'));
        createPresetReservation('weekendCompatibilityAndroid')
            .then((res) => {
                expect(res).toEqual(false);
                done();
            });
    });

    it('Error Handling when get reservation fails', (done) => {
        const {PerfectoRestClient} = require('../../../src/utils/perfectoRestClient');
        spyOn(PerfectoRestClient.prototype, 'removeDeviceReservations')
            .and
            // eslint-disable-next-line prefer-promise-reject-errors
            .returnValue(Promise.reject('Access is denied'));
        createPresetReservation('weekendCompatibilityAndroid')
            .then((res) => {
                expect(res).toEqual('removeDeviceReservations failed');
                done();
            });
    });
});
