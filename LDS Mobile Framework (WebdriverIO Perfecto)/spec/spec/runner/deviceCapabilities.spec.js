describe('deviceCapabilties', () => {
    const deviceCapabilitiesList = require('../../resources/runner/deviceCapabilities.json');
    const {DeviceCapabilities} = require('../../../src/runner/deviceCapabilities');
    const {PerfectoRestClient} = require('../../../src/utils/perfectoRestClient');
    const tagExpressionMatchers = require('../../matchers/tagExpression.matcher');

    const Android6DeviceInfo = {osVersion: '6.0.1', os: 'Android', status: 'CONNECTED'};
    const Android8DeviceInfo = {osVersion: '8.0.1', os: 'Android', status: 'CONNECTED'};
    const Android71DeviceInfo = {osVersion: '7.1', os: 'Android', status: 'CONNECTED'};
    const iOSDeviceInfo = {osVersion: '6.0.1', os: 'iOS', status: 'CONNECTED'};
    let deviceCapabilities;
    process.env.PERFECTO_USER = 'perfectouser';
    process.env.PERFECTO_PWD = 'perfectopassword';

    describe('Tag expression for non perfecto devices', () => {
        beforeEach(() => {
            jasmine.addMatchers(tagExpressionMatchers);
            deviceCapabilities = new DeviceCapabilities(deviceCapabilitiesList);
            spyOn(PerfectoRestClient.prototype, 'getAllDeviceInfo').and.returnValue(Promise.resolve(iOSDeviceInfo));
        });

        it('@appShortcut logic is not used', () => deviceCapabilities.tagExpressionForDevice('android', false)
            .then((tagExpression) => {
                expect(tagExpression).not.toMatch('and not @appShortcut');
            }));
    });

    describe('@appShortcut tests for ', () => {
        beforeEach(() => {
            jasmine.addMatchers(tagExpressionMatchers);
            deviceCapabilities = new DeviceCapabilities(deviceCapabilitiesList);
        });

        describe('random device', () => {
            beforeEach(() => {
                spyOn(PerfectoRestClient.prototype, 'getAllDeviceInfo').and.returnValue(Promise.resolve(iOSDeviceInfo));
            });

            it('@appShortcut tests are not executed', () => deviceCapabilities.tagExpressionForDevice('ios', true)
                .then((tagExpression) => {
                    expect(tagExpression).toMatch('and not @appShortcut');
                }));
        });

        describe('android < 7.1 devices', () => {
            beforeEach(() => {
                spyOn(PerfectoRestClient.prototype, 'getAllDeviceInfo').and.returnValue(Promise.resolve(Android6DeviceInfo));
            });

            it('@appShortcut tests are not executed', () => deviceCapabilities.tagExpressionForDevice('android', true)
                .then((tagExpression) => {
                    expect(tagExpression).toMatch('and not @appShortcut');
                }));
        });

        describe('android > 7.1 devices', () => {
            beforeEach(() => {
                spyOn(PerfectoRestClient.prototype, 'getAllDeviceInfo').and.returnValue(Promise.resolve(Android8DeviceInfo));
            });

            it('@appShortcut must be executed', () => deviceCapabilities.tagExpressionForDevice('android', true)
                .then((tagExpression) => {
                    expect(tagExpression).not.toMatch('and not @appShortcut');
                }));
        });

        describe('iOS devices', () => {
            beforeEach(() => {
                spyOn(PerfectoRestClient.prototype, 'getAllDeviceInfo').and.returnValue(Promise.resolve(iOSDeviceInfo));
            });

            it('@appShortcut tests are not executed', () => deviceCapabilities.tagExpressionForDevice('ios', true)
                .then((tagExpression) => {
                    expect(tagExpression).toMatch('and not @appShortcut');
                }));
        });

        describe('android 7.1 device', () => {
            beforeEach(() => {
                spyOn(PerfectoRestClient.prototype, 'getAllDeviceInfo').and.returnValue(Promise.resolve(Android71DeviceInfo));
            });

            it('@appShortcut must be executed', () => deviceCapabilities.tagExpressionForDevice('ios', true)
                .then((tagExpression) => {
                    expect(tagExpression).not.toMatch('and not @appShortcut');
                }));
        });
    });

    describe('deviceCapabilities', () => {
        beforeEach(() => {
            jasmine.addMatchers(tagExpressionMatchers);
            deviceCapabilities = new DeviceCapabilities(deviceCapabilitiesList);
            spyOn(PerfectoRestClient.prototype, 'getAllDeviceInfo').and.returnValue(Promise.resolve(iOSDeviceInfo));
        });

        it('excludes all fingerprint tags for unknown devices', () => deviceCapabilities.tagExpressionForDevice('unknownDevice', true)
            .then((tagExpression) => {
                expect(tagExpression).not.toMatchTag('@fingerprintScannerAndFingerprint');
                expect(tagExpression).not.toMatchTag('@fingerprintScannerNoFingerprint');
                expect(tagExpression).not.toMatchTag('@fingerprintNoScanner');
                expect(tagExpression).not.toMatchTag('@tabletOnly');
                expect(tagExpression).toMatchTag('@mobileOnly');
            }));

        it('fingerprintEnabledAndEnrolled', () => deviceCapabilities.tagExpressionForDevice('fingerprintEnabledAndEnrolled', true)
            .then((tagExpression) => {
                expect(tagExpression).not.toMatchTag('@tabletOnly');
                expect(tagExpression).not.toMatchTag('@fingerprintScannerNoFingerprint');
                expect(tagExpression).not.toMatchTag('@fingerprintNoScanner');
                expect(tagExpression).toMatchTag('@mobileOnly');
                expect(tagExpression).toMatchTag('@fingerprintScannerAndFingerprint');
            }));

        it('fingerprintEnabledButNotEnrolled', () => deviceCapabilities.tagExpressionForDevice('fingerprintEnabledButNotEnrolled', true)
            .then((tagExpression) => {
                expect(tagExpression).not.toMatchTag('@fingerprintScannerAndFingerprint');
                expect(tagExpression).not.toMatchTag('@tabletOnly');
                expect(tagExpression).toMatchTag('@mobileOnly');
                expect(tagExpression).not.toMatchTag('@fingerprintNoScanner');
                expect(tagExpression).toMatchTag('@fingerprintScannerNoFingerprint');
            }));

        it('fingerprintNotEnabled', () => deviceCapabilities.tagExpressionForDevice('fingerprintNotEnabled', true)
            .then((tagExpression) => {
                expect(tagExpression).not.toMatchTag('@fingerprintScannerAndFingerprint');
                expect(tagExpression).not.toMatchTag('@tabletOnly');
                expect(tagExpression).not.toMatchTag('@fingerprintScannerNoFingerprint');
                expect(tagExpression).toMatchTag('@fingerprintNoScanner');
                expect(tagExpression).toMatchTag('@mobileOnly');
            }));

        it('deviceIsaTablet', () => deviceCapabilities.tagExpressionForDevice('tabletDevice', true)
            .then((tagExpression) => {
                expect(tagExpression).not.toMatchTag('@fingerprintScannerAndFingerprint');
                expect(tagExpression).not.toMatchTag('@fingerprintScannerNoFingerprint');
                expect(tagExpression).not.toMatchTag('@fingerprintNoScanner');
                expect(tagExpression).not.toMatchTag('@mobileOnly');
                expect(tagExpression).toMatchTag('@tabletOnly');
            }));

        it('deviceIsNotaTablet', () => deviceCapabilities.tagExpressionForDevice('notTabletDevice', true)
            .then((tagExpression) => {
                expect(tagExpression).not.toMatchTag('@fingerprintScannerAndFingerprint');
                expect(tagExpression).not.toMatchTag('@fingerprintScannerNoFingerprint');
                expect(tagExpression).not.toMatchTag('@fingerprintNoScanner');
                expect(tagExpression).not.toMatchTag('@tabletOnly');
                expect(tagExpression).toMatchTag('@mobileOnly');
            }));
    });
});
