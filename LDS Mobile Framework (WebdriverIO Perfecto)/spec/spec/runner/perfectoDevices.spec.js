const _ = require('lodash');

describe('perfectoDevices', () => {
    const perfectoDeviceList = require('../../../src/wdio_config/perfectoDeviceList');
    const compatibilityDevices = {
        preset1: {
            Android: [
                'android-device1',
                'android-device4'
            ],
            iOS: [
                'ios-device1'
            ]
        },
        preset2: {
            BNGA: {
                Android: {
                    LDS: ['android-device2'],
                    BOS: ['android-device4']
                },
                iOS: {
                    LDS: ['ios-device2'],
                    BOS: ['ios-device3']
                }
            },
            RNGA: {
                Android: {
                    LDS: ['android-device4'],
                    HFX: ['android-device5']
                },
                iOS: {
                    LDS: ['ios-device4'],
                    HFX: ['ios-device5']
                }
            }
        },
        preset3: {
            Android: {
                LDS: ['android-device6']
            },
            iOS: {}
        }
    };

    it('should return 2 arrays; one for Android and one for iOS devices', () => {
        const [Android, iOS] = perfectoDeviceList.flattenDeviceList(compatibilityDevices);
        expect(Android.constructor).toEqual(Array);
        expect(iOS.constructor).toEqual(Array);
        expect(Android.indexOf('android-device1')).toEqual(0);
        expect(iOS.indexOf('ios-device1')).toEqual(0);
    });

    it('should flatten and group all compatibility devices into iOS and Android', () => {
        const [Android, iOS] = perfectoDeviceList.flattenDeviceList(compatibilityDevices);
        expect(Android)
            .toEqual(['android-device1', 'android-device4', 'android-device2', 'android-device5', 'android-device6']);
        expect(iOS)
            .toEqual(['ios-device1', 'ios-device2', 'ios-device3', 'ios-device4', 'ios-device5']);
    });

    it('should not contain undefined devices in flattened list', () => {
        _.forEach(perfectoDeviceList.weeknightDeviceBreakdown, (value) => {
            const [presetAndroid, presetiOS] = perfectoDeviceList.flattenDeviceList({
                preset: value
            });
            expect(presetAndroid.includes(undefined))
                .toEqual(false);
            expect(presetiOS.includes(undefined))
                .toEqual(false);
        });
    });
});
