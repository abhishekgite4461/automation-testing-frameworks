/**
 * Ideally, we should only test the function which is exposed by cucumberOptsGenerator, namely
 * generateCucumberOpts, rather than testing the inner workings of the module.  When adding new
 * tests here, please try to add tests for this method only.
 */
const path = require('path');
const _ = require('lodash');

describe('groupFeatureFilesBySwitches', () => {
    const {groupFeatureFilesBySwitches} = require('../../../src/runner/cucumberOptsGenerator');
    const Switch = require('../../../src/enums/RNGA/switch.enum');

    const argv = {toggleSwitches: true};
    const switchTag = (_switch, state) => `@sw-${_switch.name.toLowerCase().replace('_', '-')}-${state}`;

    it('deduce the switch states of a single test', () => {
        const featureFiles = [{
            name: 'name1',
            switchTags: [switchTag(Switch.COA_NATIVE, 'on'), switchTag(Switch.LIGHT_LOGON, 'off')],
            tags: [['@switch1']]
        }];

        const getListOfRestApiFromSwitches = (switches) => switches.map((s) => s.restApiName);

        const groupedFiles = groupFeatureFilesBySwitches(argv, featureFiles);
        expect(getListOfRestApiFromSwitches(groupedFiles[0].switches.on)).toContain(Switch.COA_NATIVE.restApiName);
        expect(getListOfRestApiFromSwitches(groupedFiles[0].switches.off)).toContain(Switch.LIGHT_LOGON.restApiName);
    });

    it('ignore switches if server is STUB', () => {
        const featureFiles = [{
            name: 'name1',
            switchTags: [switchTag(Switch.ENROLMENT, 'on'), switchTag(Switch.LIGHT_LOGON, 'off')],
            tags: [['@switch1']]
        }];

        expect(groupFeatureFilesBySwitches({toggleSwitches: false}, featureFiles)).toEqual([{
            switches: null,
            files: [{
                name: 'name1',
                switchTags: ['@sw-enrolment-on', '@sw-light-logon-off'],
                tags: [['@switch1']]
            }]
        }]);
    });

    it('pass tags through unchanged', () => {
        const featureFiles = [{
            name: 'multipleScenarios.feature',
            switchTags: ['@featuretag1', '@featuretag2'],
            tags: [
                ['@featuretag1', '@featuretag2'],
                ['@scenario1tag1', '@scenario1tag2'],
                ['@scenario2tag1', '@scenario2tag2']]
        }];

        const groupedFiles = groupFeatureFilesBySwitches(argv, featureFiles);
        expect(groupedFiles).toEqual([jasmine.objectContaining({
            files: [{
                name: 'multipleScenarios.feature',
                tags: [
                    ['@featuretag1', '@featuretag2'],
                    ['@scenario1tag1', '@scenario1tag2'],
                    ['@scenario2tag1', '@scenario2tag2']
                ]
            }]
        })]);
    });

    it('group multiple tests by switch state', () => {
        const featureFiles = [{
            name: 'name1',
            switchTags: [switchTag(Switch.ENROLMENT, 'on'), switchTag(Switch.LIGHT_LOGON, 'off')],
            tags: ['@tag1']
        }, {
            name: 'name2',
            switchTags: [switchTag(Switch.ENROLMENT, 'on'), switchTag(Switch.LIGHT_LOGON, 'on')],
            tags: ['@tag2']
        }, {
            name: 'name3',
            switchTags: [switchTag(Switch.ENROLMENT, 'off'), switchTag(Switch.LIGHT_LOGON, 'off')],
            tags: ['@tag3']
        }, {
            name: 'name4',
            switchTags: [switchTag(Switch.LIGHT_LOGON, 'on'), switchTag(Switch.ENROLMENT, 'on')],
            tags: ['@tag4']
        }];

        const groupedFiles = groupFeatureFilesBySwitches(argv, featureFiles);
        expect(groupedFiles).toContain(jasmine.objectContaining({
            files: [{
                name: 'name1',
                tags: ['@tag1']
            }]
        }));
        expect(groupedFiles).toContain(jasmine.objectContaining({
            files: [{
                name: 'name2',
                tags: ['@tag2']
            }, {
                name: 'name4',
                tags: ['@tag4']
            }]
        }));
        expect(groupedFiles).toContain(jasmine.objectContaining({
            files: [{
                name: 'name3',
                tags: ['@tag3']
            }]
        }));
    });
});

describe('generateOpts', () => {
    const AppType = require('../../../src/enums/appType.enum');
    const {randomDevice} = require('../../../src/runner/commandlineArgumentParser');
    const {generateOpts} = require('../../../src/runner/cucumberOptsGenerator');

    const argv = () => ({
        server: ['SIT2.3'],
        app: ['NGA3'],
        type: [AppType.RNGA],
        brand: ['LDS'],
        platform: ['Android'],
        tags: ['@tag1'],
        buildSubfolder: 'buildSubFolder',
        deviceIds: () => [randomDevice]
    });

    it('handles multiple files in multiple groups', () => {
        const featureFiles = [{
            files: [{
                name: 'name1',
                tags: [['@featuretag1', '@rnga', '@nga3', '@tag1']]
            }, {
                name: 'name2',
                tags: [['@featuretag2', '@rnga', '@nga3', '@tag1']]
            }],
            switches: null
        }, {
            files: [{
                name: 'name3',
                tags: [['@featuretag3', '@rnga', '@nga3', '@tag1']]
            }, {
                name: 'name4',
                tags: [['@featuretag4', '@rnga', '@nga3', '@tag1']]
            }],
            switches: null
        }];

        expect(generateOpts(argv(), featureFiles)).toEqual([{
            switches: null,
            brands: ['LDS'],
            servers: ['SIT2.3'],
            opts: [{
                cucumberOpts: jasmine.objectContaining({
                    tagExpression: '(@tag1) and (not @pending and not @manual and not @appium) and (@nga3 and @rnga and (@lds or (not @hfx and not @bos and not @mbna)) and not @ios)',
                    specs: ['name1', 'name2']
                })
            }]
        }, {
            switches: null,
            brands: ['LDS'],
            servers: ['SIT2.3'],
            opts: [{
                cucumberOpts: jasmine.objectContaining({
                    tagExpression: '(@tag1) and (not @pending and not @manual and not @appium) and (@nga3 and @rnga and (@lds or (not @hfx and not @bos and not @mbna)) and not @ios)',
                    specs: ['name3', 'name4']
                })
            }]
        }]);
    });

    it('Handles tags in various orders', () => {
        const featureFiles = [{
            files: [{
                name: 'name1',
                tags: [['@featuretag1', '@rnga', '@nga3', '@tag1']]
            }, {
                name: 'name2',
                tags: [['@rnga', '@nga3', '@tag1', '@featuretag2']]
            }, {
                name: 'name3',
                tags: [['@nga3', '@tag1', '@featuretag3', '@rnga']]
            }, {
                name: 'name4',
                tags: [['@featuretag1', '@nga3', '@rnga', '@tag1']]
            }],
            switches: null
        }];

        expect(generateOpts(argv(), featureFiles)).toEqual([{
            switches: null,
            brands: ['LDS'],
            servers: ['SIT2.3'],
            opts: [{
                cucumberOpts: jasmine.objectContaining({
                    tagExpression: '(@tag1) and (not @pending and not @manual and not @appium) and (@nga3 and @rnga and (@lds or (not @hfx and not @bos and not @mbna)) and not @ios)',
                    specs: ['name1', 'name2', 'name3', 'name4']
                })
            }]
        }]);
    });
});

describe('generateCucumberOpts', () => {
    const AppType = require('../../../src/enums/appType.enum');
    const {randomDevice} = require('../../../src/runner/commandlineArgumentParser');
    const {generateCucumberOpts} = require('../../../src/runner/cucumberOptsGenerator');

    const resourcesRoot = path.join(__dirname, '../../resources/data/cucumberOptsGenerator.spec');
    const feature = path.join(resourcesRoot, 'feature.feature');
    const noMatchingTagsFeature = path.join(resourcesRoot, 'noMatchingTags.feature');

    const argv = () => ({
        server: ['SIT2.3'],
        app: ['NGA3'],
        type: [AppType.RNGA],
        brand: ['LDS'],
        platform: ['Android'],
        tags: '@tag1',
        buildSubfolder: 'buildSubFolder',
        deviceIds: () => [randomDevice]
    });

    const mungeGroupedOpts = (groupedOpts) => {
        groupedOpts.forEach((group) => group.opts.forEach((opt) => (opt.cucumberOpts.specs = opt.cucumberOpts.specs.map((spec) => spec.replace(resourcesRoot, '')))));
    };

    it('processes a single feature file', (done) => {
        generateCucumberOpts(
            Object.assign(argv(), {type: [AppType.RNGA_UAT]}),
            [feature]
        ).then((result) => {
            mungeGroupedOpts(result);
            expect(result).toEqual([jasmine.objectContaining({
                opts: [{
                    cucumberOpts: jasmine.objectContaining({
                        tagExpression: '(@tag1) and (not @pending and not @manual and not @appium) and (@nga3 and @rnga and (@lds or (not @hfx and not @bos and not @mbna)) and not @ios)'
                    })
                }]
            })]);
            done();
        }).catch((err) => done(err));
    });

    it('convert RNGA_UAT app type to RNGA with isUAT:true', (done) => {
        generateCucumberOpts(
            _.merge(argv(), {type: [AppType.RNGA_UAT]}),
            [feature]
        ).then((result) => {
            mungeGroupedOpts(result);
            expect(result).toEqual([jasmine.objectContaining({
                opts: [{
                    cucumberOpts: jasmine.objectContaining({
                        appType: 'RNGA',
                        isUAT: true
                    })
                }]
            })]);
            done();
        }).catch((err) => done(err));
    });

    it('set isUAT to false on RNGA app type', (done) => {
        generateCucumberOpts(argv(), [feature]).then((result) => {
            mungeGroupedOpts(result);
            expect(result).toEqual([jasmine.objectContaining({
                opts: [{
                    cucumberOpts: jasmine.objectContaining({
                        appType: 'RNGA',
                        isUAT: false
                    })
                }]
            })]);
            done();
        }).catch((err) => done(err));
    });

    it('handles multiple brands and a single feature file', (done) => {
        const argvWithMultipleBrands = argv();
        argvWithMultipleBrands.brand = ['LDS', 'HFX', 'BOS'];

        generateCucumberOpts(argvWithMultipleBrands, [feature]).then((result) => {
            mungeGroupedOpts(result);
            expect(result).toEqual([jasmine.objectContaining({
                opts: [{
                    cucumberOpts: jasmine.objectContaining({
                        specs: ['/feature.feature'],
                        brand: 'LDS'
                    })
                }, {
                    cucumberOpts: jasmine.objectContaining({
                        specs: ['/feature.feature'],
                        brand: 'HFX'
                    })
                }, {
                    cucumberOpts: jasmine.objectContaining({
                        specs: ['/feature.feature'],
                        brand: 'BOS'
                    })
                }]
            })]);
            done();
        }).catch((err) => done(err));
    });

    it('ignores feature files without matching tags', (done) => {
        generateCucumberOpts(argv(), [noMatchingTagsFeature]).then((result) => {
            expect(result).toEqual([]);
            done();
        }).catch((err) => done(err));
    });

    it('Handles a list of multiple devices', (done) => {
        const argvWithMultipleDevices = argv();
        argvWithMultipleDevices.deviceIds = () => ['11', '22', '33'];

        generateCucumberOpts(argvWithMultipleDevices, [feature]).then((result) => {
            mungeGroupedOpts(result);
            expect(result).toEqual([jasmine.objectContaining({
                opts: [{
                    cucumberOpts: jasmine.objectContaining({
                        specs: ['/feature.feature'],
                        deviceName: '11'
                    })
                }, {
                    cucumberOpts: jasmine.objectContaining({
                        specs: ['/feature.feature'],
                        deviceName: '22'
                    })
                }, {
                    cucumberOpts: jasmine.objectContaining({
                        specs: ['/feature.feature'],
                        deviceName: '33'
                    })
                }]
            })]);
            done();
        }).catch((err) => done(err));
    });

    it('Handles a function of multiple devices', (done) => {
        const argvWithMultipleDevices = argv();
        argvWithMultipleDevices.deviceIds = (platform, brand) => [`Device: platform(${platform})brand(${brand})`];

        generateCucumberOpts(argvWithMultipleDevices, [feature]).then((result) => {
            mungeGroupedOpts(result);
            expect(result).toEqual([jasmine.objectContaining({
                opts: [{
                    cucumberOpts: jasmine.objectContaining({
                        deviceName: 'Device: platform(Android)brand(LDS)'
                    })
                }]
            })]);
            done();
        }).catch((err) => done(err));
    });

    it('Handles a function of multiple devices with multiple platforms and brands', (done) => {
        const argvWithEverything = argv();
        argvWithEverything.deviceIds = (platform, brand) => [`Device: platform(${platform})brand(${brand})`];
        argvWithEverything.brand = ['LDS', 'HFX'];
        argvWithEverything.platform = ['Android', 'iOS'];

        generateCucumberOpts(argvWithEverything, [feature]).then((result) => {
            mungeGroupedOpts(result);
            expect(result).toEqual([jasmine.objectContaining({
                opts: [{
                    cucumberOpts: jasmine.objectContaining({
                        tagExpression: '(@tag1) and (not @pending and not @manual and not @appium) and (@nga3 and @rnga and (@lds or (not @hfx and not @bos and not @mbna)) and not @ios)',
                        specs: ['/feature.feature'],
                        platform: 'Android',
                        brand: 'LDS',
                        deviceName: 'Device: platform(Android)brand(LDS)'
                    })
                }, {
                    cucumberOpts: jasmine.objectContaining({
                        tagExpression: '(@tag1) and (not @pending and not @manual and not @appium) and (@nga3 and @rnga and (@lds or (not @hfx and not @bos and not @mbna)) and not @android)',
                        specs: ['/feature.feature'],
                        platform: 'iOS',
                        brand: 'LDS',
                        deviceName: 'Device: platform(iOS)brand(LDS)'
                    })
                }, {
                    cucumberOpts: jasmine.objectContaining({
                        tagExpression: '(@tag1) and (not @pending and not @manual and not @appium) and (@nga3 and @rnga and (@hfx or (not @lds and not @bos and not @mbna)) and not @ios)',
                        specs: ['/feature.feature'],
                        platform: 'Android',
                        brand: 'HFX',
                        deviceName: 'Device: platform(Android)brand(HFX)'
                    })
                }, {
                    cucumberOpts: jasmine.objectContaining({
                        tagExpression: '(@tag1) and (not @pending and not @manual and not @appium) and (@nga3 and @rnga and (@hfx or (not @lds and not @bos and not @mbna)) and not @android)',
                        specs: ['/feature.feature'],
                        platform: 'iOS',
                        brand: 'HFX',
                        deviceName: 'Device: platform(iOS)brand(HFX)'
                    })
                }]
            })]);
            done();
        }).catch((err) => done(err));
    });

    it('Passes through buildSubFolder unchanged', (done) => {
        generateCucumberOpts(_.merge(argv(), {buildSubfolder: 'foobar'}), [feature]).then((result) => {
            mungeGroupedOpts(result);
            expect(result).toEqual([jasmine.objectContaining({
                opts: [{
                    cucumberOpts: jasmine.objectContaining({
                        buildSubfolder: 'foobar'
                    })
                }]
            })]);
            done();
        }).catch((err) => done(err));
    });

    it('creates reasonable default cucumberOpts', (done) => {
        generateCucumberOpts(argv(), [feature]).then((result) => {
            mungeGroupedOpts(result);
            expect(result).toEqual([jasmine.objectContaining({
                opts: [{
                    cucumberOpts: jasmine.objectContaining({
                        timeout: 600000,
                        ignoreUndefinedDefinitions: false,
                        tagExpression: '(@tag1) and (not @pending and not @manual and not @appium) and (@nga3 and @rnga and (@lds or (not @hfx and not @bos and not @mbna)) and not @ios)',
                        specs: ['/feature.feature'],
                        buildSubfolder: 'buildSubFolder',
                        server: 'SIT2.3',
                        appVersion: 'NGA3',
                        appType: 'RNGA',
                        isUAT: false,
                        platform: 'Android',
                        brand: 'LDS'
                    })
                }]
            })]);
            done();
        }).catch((err) => done(err));
    });

    it('creates reasonable default opts', (done) => {
        generateCucumberOpts(argv(), [feature]).then((result) => {
            mungeGroupedOpts(result);
            expect(result).toEqual([{
                switches: null,
                brands: ['LDS'],
                servers: ['SIT2.3'],
                opts: jasmine.anything()
            }]);
            done();
        }).catch((err) => done(err));
    });

    it('creates platform specific tags', (done) => {
        generateCucumberOpts(_.merge(argv(), {platform: ['iOS']}), [feature]).then((result) => {
            mungeGroupedOpts(result);
            expect(result).toEqual([jasmine.objectContaining({
                opts: [{
                    cucumberOpts: jasmine.objectContaining({
                        tagExpression: '(@tag1) and (not @pending and not @manual and not @appium) and (@nga3 and @rnga and (@lds or (not @hfx and not @bos and not @mbna)) and not @android)'
                    })
                }]
            })]);
            done();
        }).catch((err) => done(err));
    });

    it('creates app specific tags', (done) => {
        generateCucumberOpts(_.merge(argv(), {app: ['NGA3']}), [feature]).then((result) => {
            mungeGroupedOpts(result);
            expect(result).toEqual([jasmine.objectContaining({
                opts: [{
                    cucumberOpts: jasmine.objectContaining({
                        tagExpression: '(@tag1) and (not @pending and not @manual and not @appium) and (@nga3 and @rnga and (@lds or (not @hfx and not @bos and not @mbna)) and not @ios)'
                    })
                }]
            })]);
            done();
        }).catch((err) => done(err));
    });

    it('creates type specific tags', (done) => {
        generateCucumberOpts(_.merge(argv(), {type: [AppType.BNGA]}), [feature]).then((result) => {
            mungeGroupedOpts(result);
            expect(result).toEqual([jasmine.objectContaining({
                opts: [{
                    cucumberOpts: jasmine.objectContaining({
                        tagExpression: '(@tag1) and (not @pending and not @manual and not @appium) and (@nga3 and @bnga and (@lds or (not @hfx and not @bos and not @mbna)) and not @ios)'
                    })
                }]
            })]);
            done();
        }).catch((err) => done(err));
    });
});
