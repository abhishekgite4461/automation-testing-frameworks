const _ = require('lodash');

describe('perfectoDeviceConfiguration', () => {
    const perfectoDeviceList = require('../../../src/wdio_config/perfectoDeviceList');

    const findDuplicate = function (arrayList) {
        let baseArray;
        _.forEach(arrayList, (v) => {
            if (v.length > 0) {
                if (!baseArray) {
                    baseArray = v;
                } else {
                    if (_.intersection(baseArray, v).length > 0) {
                        throw new Error(`found duplicate devices ${_.intersection(baseArray, v)}`);
                    }
                    baseArray = _.union(baseArray, v);
                }
            }
        });
        return false;
    };

    it('should not share devices for weeknight 6 PM jobs', () => {
        const [weeknightFunctionalStubsAndroid, weeknightFunctionalStubsiOS] = perfectoDeviceList.flattenDeviceList({
            weeknightFunctionalStubs: perfectoDeviceList.weeknightDeviceBreakdown.weeknightFunctionalStubs
        });
        const [weeknightFunctionalEnvironmentAndroid,
            weeknightFunctionalEnvironmentiOS] = perfectoDeviceList.flattenDeviceList({
            weeknightFunctionalEnvironment:
                perfectoDeviceList.weeknightDeviceBreakdown.weeknightFunctionalEnvironment
        });
        expect(findDuplicate([weeknightFunctionalStubsAndroid, weeknightFunctionalEnvironmentAndroid]))
            .toEqual(false);

        expect(findDuplicate([weeknightFunctionalStubsiOS, weeknightFunctionalEnvironmentiOS]))
            .toEqual(false);
    });

    it('should not share devices for weeknight 4 AM jobs', () => {
        const [weeknightFunctionalEnvironmentMasterAndroid,
            weeknightFunctionalEnvironmentMasteriOS] = perfectoDeviceList.flattenDeviceList({
            weeknightFunctionalEnvironmentMaster:
                perfectoDeviceList.weeknightDeviceBreakdown.weeknightFunctionalEnvironmentMaster
        });
        const [weeknightFunctionalEnvironmentBngaAndroid,
            weeknightFunctionalEnvironmentBngaiOS] = perfectoDeviceList.flattenDeviceList({
            weeknightFunctionalEnvironmentBnga:
                perfectoDeviceList.weeknightDeviceBreakdown.weeknightFunctionalEnvironmentBnga
        });

        expect(findDuplicate([weeknightFunctionalEnvironmentMasterAndroid, weeknightFunctionalEnvironmentBngaAndroid]))
            .toEqual(false);

        expect(findDuplicate([weeknightFunctionalEnvironmentMasteriOS, weeknightFunctionalEnvironmentBngaiOS]))
            .toEqual(false);
    });
});
