const AppType = require('../../../src/enums/appType.enum');
const perfectoDeviceList = require('../../../src/wdio_config/perfectoDeviceList.js');

describe('Command line argument parser', () => {
    const commandlineArgumentParser = require('../../../src/runner/commandlineArgumentParser');
    const {randomDevice} = commandlineArgumentParser;

    it('provides sensible defaults', () => {
        const defaults = commandlineArgumentParser.getArgv();
        // $0 is the 0th command line arg so contains the current path.
        delete defaults.$0;

        expect(defaults).toEqual({
            _: [],
            htmlReport: false,
            toggleSwitches: false,
            dryRun: false,
            brand: ['LDS'],
            b: ['LDS'],
            platform: ['Android'],
            p: ['Android'],
            server: ['STUB'],
            s: ['STUB'],
            app: ['NGA3'],
            a: ['NGA3'],
            type: [AppType.RNGA],
            tags: '@smokeNga3',
            t: '@smokeNga3',
            buildSubfolder: null,
            bs: null,
            deviceIds: jasmine.any(Function),
            perfectoReportingJobName: null,
            perfectoReportingJobNumber: null,
            perfectoExtraTags: [],
            perfectoEnableVideo: false,
            perfectoScreenshotOnErrorOnly: false,
            perfectoDescription: null,
            runModifiedNga3Features: false,
            dataRepository: 'local',
            maxInstances: 1,
            useInstalledApp: false,
            uia: false,
            failFast: false,
            preset: null,
            provider: ['perfecto'],
            pro: ['perfecto'],
            allureReports: false,
            ar: false,
            deviceVitals: false,
            dv: false,
            deviceVitalsIntervals: 10,
            ensureMobileNumber: false,
            reportPortal: false,
            rp: false,
            byPassEia: true
        });
    });

    it('converts randomDevice to the correct function', () => {
        const argv = commandlineArgumentParser.getArgv([
            '--provider', 'perfecto',
            '--deviceIds', 'randomDevice,ThisIsAnID']);
        expect(argv.deviceIds()).toEqual([randomDevice, 'ThisIsAnID']);
    });

    it('replaces $tags in preset tags with command line tags', () => {
        const argv = commandlineArgumentParser.getArgv([
            '--tags', '@team-core and ',
            '--preset', 'testPresetWithTags']);
        expect(argv.tags).toEqual('@team-core and not @environmentOnly and not @outOfScope');
    });

    it('replaces $tags in preset tags with command line tags when passed with alias', () => {
        const argv = commandlineArgumentParser.getArgv([
            '--t', '@team-core and ',
            '--preset', 'testPresetWithTags']);
        expect(argv.tags).toEqual('@team-core and not @environmentOnly and not @outOfScope');
    });

    it('doesn\'t change preset tags when it doesn\'t contain $tags', () => {
        const argv = commandlineArgumentParser.getArgv([
            '--preset', 'testPresetWithoutTags']);
        expect(argv.tags).toEqual('not @stubOnly and not @ibRegistration and @primary and @perfectoStability');
    });

    it('check UAT job use UAT devices only', () => {
        const argv = commandlineArgumentParser.getArgv(['--preset', 'weeknightUATRegressionRnga']);
        expect(argv.perfectoDescription).toEqual('Lloyds_UAT');
        expect(argv.deviceIds('iOS', 'LDS')).toEqual(perfectoDeviceList.weeknightUATRegression.RNGA.iOS.LDS);
    });

    it('check UAT job use UAT devices only', () => {
        const argv = commandlineArgumentParser.getArgv(['--preset', 'weeknightUATRegressionBnga']);
        expect(argv.perfectoDescription).toEqual('Lloyds_UAT');
        expect(argv.deviceIds('iOS', 'LDS')).toEqual(perfectoDeviceList.weeknightUATRegression.BNGA.iOS.LDS);
    });

    it('check FT Jobs dont use UAT devices', () => {
        const argv = commandlineArgumentParser.getArgv(['--preset', 'weekendCompatibilityAndroid']);
        expect(argv.deviceIds().weeknightUATRegression).toEqual(undefined);
    });

    it('check compatability devices dont use UAT devices only', () => {
        const argv = commandlineArgumentParser.getArgv([
            '--preset', 'weekendCompatibilityIos']);
        expect(argv.perfectoDescription).toEqual('Lloyds_FT');
    });

    it('should not allow to use remote data repository with stubs', () => {
        const parse = () => commandlineArgumentParser.getArgv([
            '--server', 'STUB',
            '--dataRepository', 'remote'
        ]);
        expect(parse).toThrow();
    });

    it('should allow local repository with stubs', () => {
        const parse = () => commandlineArgumentParser.getArgv([
            '--server', 'STUB',
            '--dataRepository', 'local'
        ]);
        expect(parse).not.toThrow();
    });
});
