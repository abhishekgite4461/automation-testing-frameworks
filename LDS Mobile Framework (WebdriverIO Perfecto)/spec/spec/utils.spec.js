describe('cartesianProduct', () => {
    const {cartesianProduct} = require('../../src/utils/utils');

    it('returns the product of arrays', () => {
        expect(cartesianProduct([1, 2], [3, 4])).toEqual([[1, 3], [1, 4], [2, 3], [2, 4]]);
    });

    it('copes with arrays of arrays', () => {
        expect(cartesianProduct([[1, 2]], [[3, 4], [5, 6]])).toEqual([
            [[1, 2], [3, 4]],
            [[1, 2], [5, 6]]
        ]);
    });
});
