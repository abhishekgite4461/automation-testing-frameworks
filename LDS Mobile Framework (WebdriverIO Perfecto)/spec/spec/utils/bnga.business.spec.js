describe('BNGA Account: ', () => {
    const AccountMatcher = require('../../../src/data/utils/accountMatcher');
    const accountMatcher = new AccountMatcher();

    const multipleBusinessData = require('../../resources/data/stubaccounts/stubaccount').BNGA.data;
    const singleBusinessData = {
        data: {
            VIEW_ONLY_DELEGATE: {
                'single busines view only account': {
                    CURRENT: 'some current account'
                }
            }
        }
    };
    describe('Multiple business accounts: ', () => {
        describe('Matches only accounts: ', () => {
            it('savings only account with VIEW_ONLY_DELEGATE mandate', () => {
                expect(Object.keys(accountMatcher.findMatchingBusiness(multipleBusinessData, 'VIEW_ONLY_DELEGATE', 'SAVINGS only'))[0])
                    .toEqual('view only savings only business');
            });

            it('Current only account with VIEW_ONLY_DELEGATE mandate', () => {
                expect(Object.keys(accountMatcher.findMatchingBusiness(multipleBusinessData, 'VIEW_ONLY_DELEGATE', 'CURRENT only'))[0])
                    .toEqual('view only current only business');
            });
        });

        describe('Match one of the accounts: ', () => {
            it('Current or Savings account with VIEW_ONLY_DELEGATE mandate', () => {
                expect(Object.keys(accountMatcher.findMatchingBusiness(multipleBusinessData, 'VIEW_ONLY_DELEGATE', 'CURRENT or SAVINGS'))[0])
                    .toEqual('view only multiple business');
            });

            it('Savings account in any business with FULL_SIGNATORY mandate', () => {
                expect(Object.keys(accountMatcher.findMatchingBusiness(multipleBusinessData, 'FULL_SIGNATORY', 'SAVINGS'))[0])
                    .toEqual('fullSignatory multiple business');
            });

            it('Matches business having creditCardWithViewTransactionDetails account with FULL_ACCESS_DELEGATE mandate', () => {
                expect(Object.keys(accountMatcher.findMatchingBusiness(multipleBusinessData, 'FULL_ACCESS_DELEGATE', 'CREDIT_CARD_WITH_TRANSACTION_DETAILS'))[0])
                    .toEqual('full delegate multiple businesses');
            });
        });

        describe('Match all of the accounts: ', () => {
            it('having Current and Savings account with VIEW_ONLY_DELEGATE mandate', () => {
                expect(Object.keys(accountMatcher.findMatchingBusiness(multipleBusinessData, 'VIEW_ONLY_DELEGATE', 'CURRENT and SAVINGS'))[0])
                    .toEqual('view only current and savings business');
            });

            it('mulitple accounts with and condition', () => {
                expect(Object.keys(accountMatcher.findMatchingBusiness(multipleBusinessData, 'VIEW_ONLY_DELEGATE', 'CURRENT and fixedTermDeposit and CreditCard'))[0])
                    .toEqual('view only multiple business');
            });
        });

        describe('Single business accounts: ', () => {
            it('Current and Savings with one business with VIEW_ONLY_DELEGATE mandate', () => {
                expect(() => accountMatcher.findMatchingBusiness(singleBusinessData.data, 'VIEW_ONLY_DELEGATE', 'CURRENT and SAVINGS'))
                    .toThrow(new Error('Unable to find matching Business for access: VIEW_ONLY_DELEGATE, accounts: CURRENT and SAVINGS'));
            });

            it('Current and Savings with one business with VIEW_ONLY_DELEGATE mandate', () => {
                expect(Object.keys(accountMatcher.findMatchingBusiness(singleBusinessData.data, 'VIEW_ONLY_DELEGATE', ['SINGLE_BUSINESS', 'CURRENT']))[0])
                    .toEqual('single busines view only account');
            });

            it('Single business data can match multiple business data request', () => {
                expect(Object.keys(accountMatcher.findMatchingBusiness(singleBusinessData.data, 'VIEW_ONLY_DELEGATE', 'CURRENT'))[0])
                    .toEqual('single busines view only account');
            });
        });

        describe('Error checks', () => {
            it('Current and Savings with one business with VIEW_ONLY_DELEGATE mandate', () => {
                expect(() => accountMatcher.findMatchingBusiness(singleBusinessData.data, 'VIEW_ONLY_DELEGATE', 'CURRENT and SAVINGS'))
                    .toThrow(new Error('Unable to find matching Business for access: VIEW_ONLY_DELEGATE, accounts: CURRENT and SAVINGS'));
            });

            it('Error : non existing account type', () => {
                expect(() => accountMatcher.findMatchingBusiness(multipleBusinessData, 'VIEW_ONLY_DELEGATE', 'Current1'))
                    .toThrow(new Error('Unable to find matching Business for access: VIEW_ONLY_DELEGATE, accounts: Current1'));
            });

            it('Error : non existing account type', () => {
                expect(() => accountMatcher.findMatchingBusiness(multipleBusinessData, 'VIEW_ONLY_DELEGATE1', 'CURRENT'))
                    .toThrow(new Error('Unable to find matching Business for access: VIEW_ONLY_DELEGATE1, accounts: CURRENT'));
            });

            it('Error non matching accounts must throw error', () => {
                expect(() => accountMatcher.findMatchingBusiness(multipleBusinessData, 'FULL_SIGNATORY', 'SAVINGS only'))
                    .toThrow(new Error('Unable to find matching Business for access: FULL_SIGNATORY, accounts: SAVINGS only'));
            });

            it('user having multiple business is returned for test that require single business', () => {
                expect(() => accountMatcher.findMatchingBusiness(multipleBusinessData, 'FULL_SIGNATORY', ['SINGLE_BUSINESS', 'SAVINGS']))
                    .toThrow(new Error('expected single business, returned multiple business'));
            });
        });
    });
});
