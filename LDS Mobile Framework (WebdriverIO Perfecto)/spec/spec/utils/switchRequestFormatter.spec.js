const SwitchRequestFormatter = require('../../../src/utils/switchRequestFormatter');
const Switch = require('../../../src/enums/RNGA/switch.enum');

describe('SwitchRequestFormatter', () => {
    it('should format switches properly', () => {
        const switchRequest = SwitchRequestFormatter.format(
            [Switch.ENROLMENT, Switch.LIGHT_LOGON],
            [Switch.INTERNATIONAL_PAYMENTS, Switch.DATA_CONSENT]
        );
        expect(switchRequest.length).toBe(4);
        expect(switchRequest).toContain({
            alias: Switch.ENROLMENT.restApiName,
            name: Switch.ENROLMENT.ibcName,
            global: Switch.ENROLMENT.isGlobal,
            state: 'on'
        });
        expect(switchRequest).toContain({
            alias: Switch.LIGHT_LOGON.restApiName,
            name: Switch.LIGHT_LOGON.ibcName,
            global: Switch.LIGHT_LOGON.isGlobal,
            state: 'on'
        });
        expect(switchRequest).toContain({
            alias: Switch.INTERNATIONAL_PAYMENTS.restApiName,
            name: Switch.INTERNATIONAL_PAYMENTS.ibcName,
            global: Switch.INTERNATIONAL_PAYMENTS.isGlobal,
            state: 'off'
        });
        expect(switchRequest).toContain({
            alias: Switch.DATA_CONSENT.restApiName,
            name: Switch.DATA_CONSENT.ibcName,
            global: Switch.DATA_CONSENT.isGlobal,
            state: 'off'
        });
    });

    it('should return empty array when no switches are passed', () => {
        const switchRequest = SwitchRequestFormatter.format([], []);
        expect(switchRequest.length).toBe(0);
    });
});
