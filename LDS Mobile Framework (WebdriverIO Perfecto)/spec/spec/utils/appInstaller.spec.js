describe('appInstaller', () => {
    const AppInstaller = require('../../../src/utils/appInstaller');
    const DeviceState = require('../../../src/utils/deviceState');
    const {resetTmpDir} = require('../../../src/utils/utils');

    const appInstallerFactory = (deviceState, installFunction,
        unInstallFunction) => new AppInstaller(deviceState, installFunction, unInstallFunction, 0);

    let deviceState;
    let installFunction;
    let unInstallFunction;

    beforeEach(() => {
        resetTmpDir();
        deviceState = new DeviceState('appInstallerTestID');
        installFunction = jasmine.createSpy('installFunction');
        unInstallFunction = jasmine.createSpy('unInstallFunction');
        global.browser = {
            pause: (ms) => ms
        };
    });

    afterEach(() => {
        resetTmpDir();
        global.browser = undefined;
    });

    it('updates device state when installing the app', () => {
        const appInstaller = appInstallerFactory(deviceState, installFunction, unInstallFunction);
        expect(appInstaller.installApp(true, true)).toBe(true);
        expect(deviceState.state).toEqual({
            appInstalled: true,
            instrument: true,
            sensorInstrument: true,
            enrolledUser: null
        });
        expect(unInstallFunction).toHaveBeenCalled();
        expect(installFunction).toHaveBeenCalledWith(true, true);
    });

    it('updates device state when installation fails', () => {
        const appInstaller = appInstallerFactory(deviceState, installFunction.and.throwError('Perfecto'), unInstallFunction);
        deviceState.setAppInstalled(true, false, false);
        expect(() => appInstaller.installApp(true, true)).toThrowError('Perfecto');
        expect(deviceState.state).toEqual({
            appInstalled: false,
            instrument: false,
            sensorInstrument: false,
            enrolledUser: null
        });
        expect(unInstallFunction).toHaveBeenCalled();
        expect(installFunction).toHaveBeenCalledWith(true, true);
    });

    it('updates device state when uninstallation fails', () => {
        const appInstaller = appInstallerFactory(deviceState, installFunction, unInstallFunction.and.throwError('Perfecto'));
        deviceState.setAppInstalled(true, false, false);
        expect(() => appInstaller.installApp(true, true)).toThrowError('Perfecto');
        expect(deviceState.state).toEqual({
            appInstalled: false,
            instrument: false,
            sensorInstrument: false,
            enrolledUser: null
        });
        expect(unInstallFunction).toHaveBeenCalled();
        expect(installFunction).not.toHaveBeenCalled();
    });

    it('doesn\'t always install with instrumentation', () => {
        const appInstaller = appInstallerFactory(deviceState, installFunction, unInstallFunction);
        expect(appInstaller.installApp(false, false)).toBe(true);
        expect(deviceState.state).toEqual({
            appInstalled: true,
            instrument: false,
            sensorInstrument: false,
            enrolledUser: null
        });
        expect(unInstallFunction).toHaveBeenCalled();
        expect(installFunction).toHaveBeenCalledWith(false, false);
    });

    it('returns false when not installing the app', () => {
        const appInstaller = appInstallerFactory(deviceState, installFunction, unInstallFunction);
        deviceState.setAppInstalled(true, true, true);
        expect(appInstaller.installApp(true, true)).not.toBe(true);
        expect(unInstallFunction).not.toHaveBeenCalled();
        expect(installFunction).not.toHaveBeenCalled();
    });

    it('resets enrolled user when installing the app', () => {
        deviceState.setAppInstalled(true, false, false);
        deviceState.setEnrolledUser('testUser');
        const appInstaller = appInstallerFactory(deviceState, installFunction, unInstallFunction);
        expect(appInstaller.installApp(true, true)).toBe(true);
        expect(deviceState.state).toEqual({
            appInstalled: true,
            instrument: true,
            sensorInstrument: true,
            enrolledUser: null
        });
        expect(unInstallFunction).toHaveBeenCalled();
        expect(installFunction).toHaveBeenCalledWith(true, true);
    });

    it('always installs the app when forceInstallApp is called', () => {
        deviceState.setAppInstalled(true, false, false);
        const appInstaller = appInstallerFactory(deviceState, installFunction, unInstallFunction);
        expect(appInstaller.forceInstallApp(false, false)).toBe(true);
        expect(deviceState.state).toEqual({
            appInstalled: true,
            instrument: false,
            sensorInstrument: false,
            enrolledUser: null
        });
        expect(unInstallFunction).toHaveBeenCalled();
        expect(installFunction).toHaveBeenCalledWith(false, false);
    });
});
