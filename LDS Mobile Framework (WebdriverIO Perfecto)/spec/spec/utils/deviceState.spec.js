describe('deviceState', () => {
    const DeviceState = require('../../../src/utils/deviceState');
    const {resetTmpDir} = require('../../../src/utils/utils');

    const deviceID = 'testDevice';
    const enrolledUser = 'testUser';

    beforeEach(() => {
        // Ensure empty tmp directory
        resetTmpDir();
    });

    afterEach(() => {
        // Clean up tmp directory
        resetTmpDir();
    });

    it('sets up default state when file is missing', () => {
        const deviceState = new DeviceState(deviceID);
        expect(deviceState.state).toEqual({
            appInstalled: false,
            instrument: false,
            sensorInstrument: false,
            enrolledUser: null
        });
    });

    it('remembers installation state in the same session', () => {
        const deviceState = new DeviceState(deviceID);
        deviceState.setAppInstalled(true, true, true);
        expect(deviceState.state).toEqual({
            appInstalled: true,
            instrument: true,
            sensorInstrument: true,
            enrolledUser: null
        });
    });

    it('remembers installation state between sessions', () => {
        const deviceState1 = new DeviceState(deviceID);
        deviceState1.setAppInstalled(true, true, true);
        const deviceState2 = new DeviceState(deviceID);
        expect(deviceState2.state).toEqual({
            appInstalled: true,
            instrument: true,
            sensorInstrument: true,
            enrolledUser: null
        });
    });

    it('handles simultaneous sessions', () => {
        const deviceState1 = new DeviceState(deviceID);
        const deviceState2 = new DeviceState(deviceID);
        deviceState1.setAppInstalled(true, true, true);
        deviceState2.setEnrolledUser(enrolledUser);
        expect(deviceState2.state).toEqual({
            appInstalled: true,
            instrument: true,
            sensorInstrument: true,
            enrolledUser
        });
        expect(deviceState1.state).toEqual(deviceState2.state);
    });

    describe('getEnrolledUser', () => {
        const deviceState = new DeviceState(deviceID);

        it('returns null when no user has been enrolled', () => {
            expect(deviceState.getEnrolledUser()).toBe(null);
        });

        it('remembers enrolled user in the same session', () => {
            deviceState.setEnrolledUser(enrolledUser);
            expect(deviceState.getEnrolledUser()).toBe(enrolledUser);
        });

        it('remembers enrolled user between sessions', () => {
            deviceState.setEnrolledUser(enrolledUser);
            const deviceState2 = new DeviceState(deviceID);
            expect(deviceState2.getEnrolledUser()).toBe(enrolledUser);
        });
    });

    describe('with no app', () => {
        const deviceState = new DeviceState(deviceID);

        beforeEach(() => {
            deviceState.setAppInstalled(false, false, false);
        });

        it('requires installation regardless', () => {
            expect(deviceState.isAppInstallationRequired(false, false)).toBe(true);
            expect(deviceState.isAppInstallationRequired(false, true)).toBe(true);
            expect(deviceState.isAppInstallationRequired(true, false)).toBe(true);
            expect(deviceState.isAppInstallationRequired(true, true)).toBe(true);
        });
    });

    describe('with no instruments', () => {
        const deviceState = new DeviceState(deviceID);

        beforeEach(() => {
            deviceState.setAppInstalled(true, false, false);
        });

        it('does not require installation when no instruments are required', () => {
            expect(deviceState.isAppInstallationRequired(false, false)).not.toBe(true);
        });

        it('requires installation when any kind of instrumentation is needed', () => {
            expect(deviceState.isAppInstallationRequired(false, true)).toBe(true);
            expect(deviceState.isAppInstallationRequired(true, false)).toBe(true);
            expect(deviceState.isAppInstallationRequired(true, true)).toBe(true);
        });
    });

    describe('with normal instruments', () => {
        const deviceState = new DeviceState(deviceID);

        beforeEach(() => {
            deviceState.setAppInstalled(true, true, false);
        });

        it('does not require installation when normal instrumentation is required', () => {
            expect(deviceState.isAppInstallationRequired(true, false)).toBe(false);
        });

        it('does not require installation when no instrumentation is needed', () => {
            expect(deviceState.isAppInstallationRequired(false, false)).toBe(false);
        });

        it('requires installation when sensor instrumentation is needed', () => {
            expect(deviceState.isAppInstallationRequired(false, true)).toBe(true);
            expect(deviceState.isAppInstallationRequired(true, true)).toBe(true);
        });
    });

    describe('with sensor instruments', () => {
        const deviceState = new DeviceState(deviceID);

        beforeEach(() => {
            deviceState.setAppInstalled(true, false, true);
        });

        it('does not require installation when sensor instruments are required', () => {
            expect(deviceState.isAppInstallationRequired(false, true)).not.toBe(true);
        });

        it('requires installation when no instrumentation is needed', () => {
            expect(deviceState.isAppInstallationRequired(false, false)).toBe(true);
        });

        it('requires installation when normal instrumentation is needed', () => {
            expect(deviceState.isAppInstallationRequired(true, false)).toBe(true);
            expect(deviceState.isAppInstallationRequired(true, true)).toBe(true);
        });
    });

    describe('with all instruments', () => {
        const deviceState = new DeviceState(deviceID);

        beforeEach(() => {
            deviceState.setAppInstalled(true, true, true);
        });

        it('does not require installation when all instruments are required', () => {
            expect(deviceState.isAppInstallationRequired(true, true)).not.toBe(true);
        });

        it('requires installation when no instrumentation is needed', () => {
            expect(deviceState.isAppInstallationRequired(false, false)).toBe(true);
        });
    });
});
