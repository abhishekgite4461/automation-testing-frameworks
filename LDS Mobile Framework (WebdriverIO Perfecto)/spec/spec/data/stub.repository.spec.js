describe('Stub repository', () => {
    const Stub = require('../../../src/enums/stub.enum');
    const StubRepository = require('../../../src/data/stub.repository');

    it('parses a valid stub tag', () => {
        const stubRepository = new StubRepository();
        stubRepository.set(['@stub-valid', '@sw-isengard-off']);

        expect(stubRepository.get()).toEqual(Stub.VALID);
    });

    it('fails on unknown stub tag', () => {
        const stubRepository = new StubRepository();
        expect(() => stubRepository.set(['@stub-unknownstub']))
            .toThrow(new Error('Missing @stub-XXX tag!'));
    });

    it('fails when no stub tag', () => {
        const stubRepository = new StubRepository();
        expect(() => stubRepository.set(['@sw-some-switch']))
            .toThrow(new Error('Missing @stub-XXX tag!'));
    });
});
