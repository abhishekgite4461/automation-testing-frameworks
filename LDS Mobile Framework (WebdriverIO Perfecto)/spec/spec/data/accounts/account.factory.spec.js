describe('accountFactory', () => {
    const AccountFactory = require('../../../../src/data/accounts/account.factory');

    let accountFactory;

    beforeEach(() => {
        accountFactory = new AccountFactory('./spec/resources/data/accounts');
    });

    it('returns global data when there is no data for the account', () => {
        const account = accountFactory.getAccount('RNGA', 'server', 'brand', 'username');
        expect(account).toEqual({
            account: 'Global Account'
        });
    });

    it('returns default stub data for unknown user', () => {
        const account = accountFactory.getAccount('RNGA', 'STUB', 'LDS', 'unknown');
        expect(account).toEqual({
            account: 'Stub Account'
        });
    });

    it('returns brand data for real environments', () => {
        const account = accountFactory.getAccount('RNGA', 'BR', 'LDS', 'unknown');
        expect(account).toEqual({
            account: 'Lloyds Account'
        });
    });

    it('combines global data with brand data and user data', () => {
        const account = accountFactory.getAccount('RNGA', 'BR', 'BOS', '123456789');
        expect(account).toEqual({
            account: 'Global Account',
            creditCard: 'BOS Card',
            phoneNumber: '+447123456789'
        });
    });

    it('returns global data when there is no data for the account', () => {
        const account = accountFactory.getAccount('BNGA', 'server', 'brand', 'username');
        expect(account).toEqual({
            account: 'Global Account'
        });
    });

    it('returns default stub data for unknown user', () => {
        const account = accountFactory.getAccount('BNGA', 'STUB', 'LDS', 'unknown');
        expect(account).toEqual({
            account: 'Stub Account'
        });
    });

    it('returns brand data for real environments', () => {
        const account = accountFactory.getAccount('BNGA', 'BR', 'LDS', 'unknown');
        expect(account).toEqual({
            account: 'Lloyds Account'
        });
    });

    it('combines global data with brand data and user data', () => {
        const account = accountFactory.getAccount('BNGA', 'BR', 'BOS', '123456789');
        expect(account).toEqual({
            account: 'Global Account',
            creditCard: 'BOS Card',
            cardReader: {
                pan: '1111111111130410'
            }
        });
    });
});
