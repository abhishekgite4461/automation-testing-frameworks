describe('localAccountDataSource', () => {
    const LocalAccountDataSource = require('../../../../src/data/accounts/account.dataSource.local');
    const UserType = require('../../../../src/enums/userType.enum');

    let localAccountDataSource;

    beforeEach(() => {
        localAccountDataSource = new LocalAccountDataSource('./spec/resources/data/accounts');
    });

    it('returns matching account from RNGA/accounts.csv when EIA is required', () => {
        const account = localAccountDataSource.getAccount('SIT03-B', 'RNGA', 'BOS', UserType.CURRENT, '+4407466831495', true);
        expect(account).toEqual({
            serverName: 'SIT03-B',
            appType: 'RNGA',
            brand: 'BOS',
            username: '781562368',
            password: 'password1234',
            memorableInformation: 't35ting',
            phoneNumber: '+4407466831495',
            userType: UserType.CURRENT
        });
    });

    it('returns matching account from RNGA/accounts.csv even if phone number isn\'t matching when EIA is not required', () => {
        const account = localAccountDataSource.getAccount('SIT03-B', 'RNGA', 'BOS', UserType.CURRENT, '+4407412345678', false);
        expect(account).toEqual({
            serverName: 'SIT03-B',
            appType: 'RNGA',
            brand: 'BOS',
            username: '781562368',
            password: 'password1234',
            memorableInformation: 't35ting',
            phoneNumber: '+4407466831495',
            userType: UserType.CURRENT
        });
    });

    it('doesn\'t return matching account from RNGA/accounts.csv when phone number isn\'t matching and EIA is required', () => {
        const account = localAccountDataSource.getAccount('SIT03-B', 'RNGA', 'BOS', UserType.CURRENT, '+4407412345678', true);
        expect(account).toEqual(null);
    });

    it('returns ibreg accounts from RNGA/accounts.ibreg.csv', () => {
        const account = localAccountDataSource.getAccount('PUT06', 'RNGA', 'LDS', UserType.IBREG_MANDATELESS, '+4407412345678', false);
        expect(account).toEqual({
            serverName: 'PUT06',
            appType: 'RNGA',
            brand: 'LDS',
            username: '813084632',
            password: 'tester1234',
            memorableInformation: 't35ting',
            userType: UserType.IBREG_MANDATELESS,
            firstName: 'Stephen',
            lastName: 'Huettl',
            emailId: 'abc@gmail.com',
            postCode: '',
            accountType1: 'Current_Savings',
            accountType2: 'Loan',
            accountType3: 'Mortgage',
            accountType4: 'Credit Card',
            accountNumber: '19979860',
            loanNumber: '987654321012',
            mortgageNumber: '98765432101234',
            creditCardNumber: '4751290182981234',
            sortCode: '774824',
            userId: 'huettl1234',
            userPassword: 'tester1234',
            confirmUserPassword: 'tester1234',
            memorableInfo: 't35ting',
            confirmMemorableInfo: 't35ting',
            activationCode: 'ACTCODE1',
            dobDate: '10',
            dobMonth: 'February',
            dobYear: '1937',
            dob: '10-Feb-37',
            blacklistedPassword: 'password1234'
        });
    });

    it('returns matching account from BNGA/accounts.csv', () => {
        const account = localAccountDataSource.getAccount('SIT03-B', 'BNGA', 'BOS', UserType.CURRENT);
        expect(account).toEqual({
            serverName: 'SIT03-B',
            appType: 'BNGA',
            brand: 'BOS',
            username: '781562368',
            password: 'password1234',
            memorableInformation: 't35ting',
            cardNumber: '1111111111130410',
            userType: UserType.CURRENT
        });
    });
});
