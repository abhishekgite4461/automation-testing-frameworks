describe('accountRepository', () => {
    const AccountFactory = require('../../../../src/data/accounts/account.factory');
    const AccountRepository = require('../../../../src/data/accounts/account.repository');
    const LocalAccountDataSource = require('../../../../src/data/accounts/account.dataSource.local');
    const UserType = require('../../../../src/enums/userType.enum');

    let accountFactory;
    let localAccountDataSource;
    let remoteAccountDataSource;
    let accountRepository;

    beforeEach(() => {
        accountFactory = new AccountFactory('./spec/resources/data/accounts');
        localAccountDataSource = new LocalAccountDataSource('./spec/resources/data/accounts');
        accountRepository = new AccountRepository(
            accountFactory,
            localAccountDataSource,
            remoteAccountDataSource
        );
    });

    describe('local', () => {
        beforeEach(() => {
            accountRepository.initialize('local');
        });

        it('always contains valid stub data for RNGA non-ibreg users', () => {
            accountRepository.assign('STUB', 'RNGA', 'LDS', UserType.VALID);

            expect(accountRepository.get()).toEqual({
                account: 'Stub Account',
                serverName: 'STUB',
                appType: 'RNGA',
                brand: 'LDS',
                username: '123456789',
                password: 'password1234',
                memorableInformation: 't35ting',
                newPostCode: 'IG2 7ES',
                phoneNumber: '+447123456789',
                userTypes: UserType.VALID,
                workNumber: '02011111111'
            });
        });

        it('always contains valid stub data for RNGA ibreg users', () => {
            accountRepository.assign('STUB', 'RNGA', 'LDS', UserType.IBREG_MANDATELESS);

            expect(accountRepository.get()).toEqual({
                account: 'Stub Account',
                serverName: 'STUB',
                appType: 'RNGA',
                brand: 'LDS',
                username: '123456789',
                password: 'password1234',
                memorableInformation: 't35ting',
                userTypes: UserType.IBREG_MANDATELESS,
                firstName: 'Stephen',
                lastName: 'James',
                emailId: 'lloydstestinguser@lloydsbanking.com',
                postCode: 'E112XX',
                ibregPostCode: 'EC1Y 4XX',
                accountType1: 'Current_Savings',
                accountType2: 'Loan',
                accountType3: 'Mortgage',
                accountType4: 'Credit_Card',
                accountNumber: '19979860',
                loanNumber: '987654321012',
                mortgageNumber: '98765432101234',
                creditCardNumber: '4751290287863962',
                sortCode: '774824',
                userId: '756767488',
                userPassword: 'tester1234',
                confirmUserPassword: 'tester1234',
                memorableInfo: 't35ting',
                confirmMemorableInfo: 't35ting',
                activationCode: 'actcode1',
                dobDate: '09',
                dobMonth: 'November',
                dobYear: '1983',
                dob: '09-Nov-1983',
                blacklistedPassword: 'password1234',
                ibregIncorrectPostcode: {
                    firstName: 'Stephen',
                    lastName: 'James',
                    emailId: '8cbkaoh4k183xg7yn6zuxr@g.com',
                    ibregPostCode: 'E7 9QL',
                    accountNumber: '06987168',
                    loanNumber: '100123427298',
                    creditCardNumber: '5521573655373913',
                    sortCode: '770110',
                    date: '20',
                    month: 'May',
                    year: '1963'
                },
                newPostCode: 'IG2 7ES',
                phoneNumber: '+447123456789',
                workNumber: '02011111111'
            });
        });

        it('always contains valid stub data for BNGA users', () => {
            accountRepository.assign('STUB', 'BNGA', 'LDS', UserType.VALID);

            expect(accountRepository.get()).toEqual({
                account: 'Stub Account',
                serverName: 'STUB',
                appType: 'BNGA',
                brand: 'LDS',
                username: '123456789',
                password: 'password1234',
                memorableInformation: 't35ting',
                cardReader: {
                    pan: '1111111111130410'
                },
                userTypes: UserType.VALID
            });
        });
    });
});
