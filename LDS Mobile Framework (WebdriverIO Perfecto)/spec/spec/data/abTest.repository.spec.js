describe('abTest repository', () => {
    const AbTestRepository = require('../../../src/data/abTest.repository');

    it('parses a valid ab tag', () => {
        const abTestRepository = new AbTestRepository();
        abTestRepository.set(['@ab-dark-url-button~Submit']);
        const abtests = abTestRepository.get();
        expect(abtests).toEqual({locationName: 'moreTabDarkURL', option: 'Submit'});
    });

    it('fails on unknown AB test', () => {
        const abTestRepository = new AbTestRepository();
        expect(() => abTestRepository.set(['@ab-unknownAbTest~Submit']))
            .toThrow(new Error('AB test is not supported'));
    });

    it('fails on unknown AB test option', () => {
        const abTestRepository = new AbTestRepository();
        expect(() => abTestRepository.set(['@ab-dark-url-button~submitt']))
            .toThrow(new Error('AB test/option is not supported'));
    });

    it('fails on ABtest option not being specified by ~', () => {
        const abTestRepository = new AbTestRepository();
        expect(() => abTestRepository.set(['@ab-dark-url-button-submit']))
            .toThrow(new Error('option not specified for AB test'));
    });
});
