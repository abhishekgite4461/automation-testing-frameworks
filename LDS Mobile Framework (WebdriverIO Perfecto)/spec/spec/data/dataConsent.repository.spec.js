describe('dataConsent repository', () => {
    const DataConsentRepository = require('../../../src/data/dataConsent.repository.js');

    it('parses a valid data consent tag', () => {
        const dataConsentRepository = new DataConsentRepository();
        dataConsentRepository.set(['@dc-performance-off', '@dc-marketing-on']);
        const dataConsent = dataConsentRepository.get();
        expect(dataConsent).toEqual({performance: false, marketing: true});
    });

    it('fails on unknown data consent test', () => {
        const dataConsentRepository = new DataConsentRepository();
        expect(() => dataConsentRepository.set(['@dc-sales-off']))
            .toThrow(new Error('Enum SALES not found!'));
    });

    it('returns empty object if only non data consent tags are found', () => {
        const dataConsentRepository = new DataConsentRepository();
        dataConsentRepository.set(['@ff-view-pin-enabled-on']);
        expect(JSON.stringify(dataConsentRepository.get())).toEqual(JSON.stringify({}));
    });

    it('fails on unknown data consent state', () => {
        const dataConsentRepository = new DataConsentRepository();
        expect(() => dataConsentRepository.set(['@dc-sales-unset']))
            .toThrow(new Error('invalid state for data consent, must be on or off'));
    });
});
