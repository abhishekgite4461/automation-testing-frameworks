const Switch = require('../../../src/enums/RNGA/switch.enum');

describe('Switch repository', () => {
    const SwitchRepository = require('../../../src/data/switch.repository');
    const defaultSwitches = {on: [], off: []};

    it('parses valid switches on STUB', () => {
        const switchRepository = new SwitchRepository();
        switchRepository.set(['@sw-enrolment-on', '@sw-light-logon-off'], true, 'LDS', defaultSwitches);

        const switches = switchRepository.get();
        expect(switches.on).toEqual([]);
        expect(switches.off).toEqual([Switch.LIGHT_LOGON]);
    });

    it('parses valid switches on Environment', () => {
        const switchRepository = new SwitchRepository();
        switchRepository.set(['@sw-enrolment-on', '@sw-light-logon-off'], false, 'LDS', defaultSwitches);

        const switches = switchRepository.get();
        expect(switches.on).toEqual([Switch.ENROLMENT]);
        expect(switches.off).toEqual([Switch.LIGHT_LOGON]);
    });

    it('fails on invalid switch tags', () => {
        const switchRepository = new SwitchRepository();

        expect(() => switchRepository.set(['@sw-invalidswitch'], true, 'LDS', defaultSwitches))
            .toThrow(new Error('Bad switch tag: @sw-invalidswitch'));
    });

    it('fails on unknown switches', () => {
        const switchRepository = new SwitchRepository();

        expect(() => switchRepository.set(['@sw-invalidswitch-on'], true, 'LDS', defaultSwitches))
            .toThrow(new Error('Enum INVALIDSWITCH not found!'));
    });

    it("doesn't set defaults", () => {
        const switchRepository = new SwitchRepository();

        switchRepository.set([], true, 'LDS', defaultSwitches);

        const switches = switchRepository.get();
        expect(switches.on).toEqual([]);
        expect(switches.off).toEqual([]);
    });

    it('adds the default switches to switch list', () => {
        const switchRepository = new SwitchRepository();
        const defaultSwitchList = {on: [Switch.LIGHT_LOGON], off: [Switch.ENROLMENT]};
        switchRepository.set([], false, 'LDS', defaultSwitchList);

        const switches = switchRepository.get();
        expect(switches.on).toEqual([Switch.LIGHT_LOGON]);
        expect(switches.off).toEqual([Switch.ENROLMENT]);
    });

    it('adds the default switches to switch list', () => {
        const switchRepository = new SwitchRepository();
        const defaultSwitchList = {on: [Switch.NGA_RESTRICT_ACCESS], off: [Switch.ENABLE_RNGA_VEF_CREDIT_CARD]};
        switchRepository.set(['@sw-enrolment-on', '@sw-light-logon-off'], false, 'LDS', defaultSwitchList);

        const switches = switchRepository.get();
        expect(switches.on).toEqual([Switch.ENROLMENT, Switch.NGA_RESTRICT_ACCESS]);
        expect(switches.off).toEqual([Switch.LIGHT_LOGON, Switch.ENABLE_RNGA_VEF_CREDIT_CARD]);
    });

    it('adds the default switches for a brand to switch list', () => {
        const switchRepository = new SwitchRepository();
        const defaultSwitchList = {LDS: {on: [Switch.NGA_RESTRICT_ACCESS], off: [Switch.ENABLE_RNGA_VEF_CREDIT_CARD]}};
        switchRepository.set(['@sw-enrolment-on', '@sw-light-logon-off'], false, 'LDS', defaultSwitchList);

        const switches = switchRepository.get();
        expect(switches.on).toEqual([Switch.ENROLMENT, Switch.NGA_RESTRICT_ACCESS]);
        expect(switches.off).toEqual([Switch.LIGHT_LOGON, Switch.ENABLE_RNGA_VEF_CREDIT_CARD]);
    });
});
