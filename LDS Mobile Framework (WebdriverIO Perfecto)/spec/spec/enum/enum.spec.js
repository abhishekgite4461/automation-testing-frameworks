describe('Enum', () => {
    const Enum = require('../../../src/enums/enum');

    const testEnum = new Enum({
        symbol1: {prop1: '1val1', prop2: '1val2'},
        symbol2: {prop1: '2val1', prop2: '2val2'}
    });

    it('keys', () => {
        expect(testEnum.keys()).toEqual(['symbol1', 'symbol2']);
    });

    it('contains', () => {
        const anotherEnumSymbol = new Enum({symbol1: {prop1: '1val1', prop2: '1val2'}}).symbol1;

        expect(testEnum.contains(testEnum.symbol1)).toBe(true);
        expect(testEnum.contains('symbol1')).toBe(false);
        expect(testEnum.contains(anotherEnumSymbol)).toBe(false);
    });

    it('fromString', () => {
        expect(testEnum.fromString('symbol1')).toBe(testEnum.symbol1);
        expect(() => testEnum.fromString('notASymbol')).toThrow(new Error('Enum notASymbol not found!'));
    });
});

describe('EnumSymbol', () => {
    const Enum = require('../../../src/enums/enum');

    const enumSymbol = new Enum({symbolName: {prop1: 'val1', prop2: 'val2'}}).symbolName;

    it('toString', () => {
        expect(enumSymbol.toString()).toEqual('symbolName');
    });

    it('name', () => {
        expect(enumSymbol.name).toEqual('symbolName');
    });

    it('valueOf', () => {
        expect(enumSymbol.valueOf()).toEqual('symbolName');
    });

    it('keys', () => {
        expect(enumSymbol.keys).toEqual(['prop1', 'prop2']);
    });
});
