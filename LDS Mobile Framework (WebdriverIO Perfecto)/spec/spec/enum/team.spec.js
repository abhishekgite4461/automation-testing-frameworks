describe('teams enum', () => {
    const teams = require('../../../src/enums/teams.enum');

    it('creates a tag for a team', () => {
        expect(teams.PLATFORM_ENGINEERING.tag).toEqual('@team-platform-engineering');
    });

    it('parses a team from a tag', () => {
        expect(teams.fromTag('@team-platform-engineering')).toEqual(teams.PLATFORM_ENGINEERING);
    });

    it('roundtrips tags', () => {
        expect(teams.fromTag('@team-platform-engineering').tag).toEqual('@team-platform-engineering');
        expect(teams.fromTag(teams.PLATFORM_ENGINEERING.tag)).toEqual(teams.PLATFORM_ENGINEERING);
    });

    it('teams are uppercase with underscores only', () => {
        teams.keys().forEach((key) => expect(key).toMatch(/^[A-Z_]+$/));
    });
});
