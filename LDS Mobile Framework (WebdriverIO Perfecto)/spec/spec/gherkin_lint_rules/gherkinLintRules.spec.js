const fs = require('fs');
const Gherkin = require('@cucumber/gherkin');
const path = require('path');
const {IdGenerator} = require('@cucumber/messages');

const resourcesRoot = '../../resources/gherkin_lint_rules';

const Parser = new Gherkin.Parser(new Gherkin.AstBuilder(IdGenerator.incrementing()));
const parse = (filename) => Parser.parse(fs.readFileSync(filename).toString()).feature;

describe('gherkin_lint_rules', () => {
    it('noUnknownTags should fail on an unknown tag', () => {
        const rule = require('../../../src/gherkin_lint_rules/noUnknownTags').run;
        const featureFile = parse(path.join(__dirname, `${resourcesRoot}/unknownTags.feature`));
        expect(rule(featureFile)).toEqual([{
            message: 'The following tag is not recognised, please correct or add to the tag list: @unknownTag1',
            rule: 'no-unknown-tags',
            line: 1
        }, {
            message: 'The following tag is not recognised, please correct or add to the tag list: @unknownTag2',
            rule: 'no-unknown-tags',
            line: 9
        }, {
            message: 'The following feature flag tag is not recognised, please correct or add '
            + 'to the feature flag list: @ff-unknown-on',
            rule: 'no-unknown-tags',
            line: 9
        }]);
    });

    it('all tags should be camelCase', () => {
        const tagList = require('../../../src/gherkin_lint_rules/tagList');
        const camelCase = /^@[a-z]+[a-zA-Z0-9]*$/;
        const verifyTagIsCamelCase = (tag) => {
            if (!camelCase.test(tag)) {
                throw new Error(`Tags should be camelCase, but found: ${tag}`);
            }
        };

        Object.keys(tagList).forEach((tag) => verifyTagIsCamelCase(tag));
    });

    it('fail on illegal tag combinations', () => {
        const rule = require('../../../src/gherkin_lint_rules/noIllegalTagCombinations').run;
        const featureFile = parse(path.join(__dirname, `${resourcesRoot}/illegalTags.feature`));

        expect(rule(featureFile)).toEqual([jasmine.objectContaining({
            rule: 'no-illegal-tag-combinations',
            line: 5
        }), jasmine.objectContaining({
            rule: 'no-illegal-tag-combinations',
            line: 10
        }), jasmine.objectContaining({
            rule: 'no-illegal-tag-combinations',
            line: 15
        })]);
    });

    it('fail on illegal @nga3 tags', () => {
        const rule = require('../../../src/gherkin_lint_rules/noIllegalNga3Tags').run;
        const featureFile = parse(path.join(__dirname, `${resourcesRoot}/illegalNga3Tags.feature`));

        expect(rule(featureFile)).toEqual([jasmine.objectContaining({
            rule: 'no-illegal-nga3-tags',
            line: 4
        })]);
    });

    it('fail on a feature file with a switch tag on a scenario', () => {
        const rule = require('../../../src/gherkin_lint_rules/noSwitchTagsOnScenarios').run;
        const featureFile = parse(path.join(__dirname, `${resourcesRoot}/scenarioSwitchTags.feature`));

        expect(rule(featureFile)).toEqual([jasmine.objectContaining({
            rule: 'no-switch-tags-on-scenarios',
            line: 3
        })]);
    });

    it('fail on a scenario without @-team tags', () => {
        const rule = require('../../../src/gherkin_lint_rules/teamTagsRequired').run;
        const featureFile = parse(path.join(__dirname, `${resourcesRoot}/teamTags.feature`));

        expect(rule(featureFile)).toEqual([jasmine.objectContaining({
            rule: 'team-tags-required',
            line: 0
        })]);
    });
});
