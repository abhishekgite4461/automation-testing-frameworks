const nock = require('nock');

const sessions = require('./resources/appiumMockSession');
const ElementTypes = require('./resources/elementTypes.enum');
const CommandTypes = require('./resources/commandTypes.enum');

const SESSIONID = '37d6e8e9-697b-4f04-9019-049591694836';

const genericResponse = {
    sessionId: SESSIONID,
    status: 0,
    value: null
};

const pathFilter = (path) => {
    const pathWithouSessionId = path.replace(/\/wd\/hub\/session\/37d6e8e9-697b-4f04-9019-049591694836/g, '');
    const genericActionPathFilter = (pathToFilter) => (pathToFilter.match(
        /\/(element\/[a-z][A-Z]*\/(clear|click|value)|(appium\/(app\/(launch|reset)|device\/(press_keycode|hide_keyboard)))|timeouts|keys|execute)/gi
    ) ? '/genericElementAction' : pathToFilter);
    return genericActionPathFilter(pathWithouSessionId);
};

const bodyFilter = (body) => {
    let returnBody = body;

    const checkForInvisibleElement = (value) => (value ? (value.indexOf('spinnerLoadingTitle') > -1
            || value.indexOf('dialogLoadingTitle') > -1) : false);

    const wrapBodyInValue = (b) => {
        let returnVal = b;
        if (typeof b === 'string') returnVal = JSON.parse(b);
        return returnVal.value ? returnVal : {value: returnVal};
    };

    if (body) {
        const {value} = wrapBodyInValue(body);
        if (typeof value !== 'string') returnBody = body;
        else if (checkForInvisibleElement(value)) returnBody = ElementTypes.INVISIBLE_ELEMENT.body;
        else if (value.indexOf('Classic') > -1) returnBody = ElementTypes.CLASSIC.body;
        else if (value.indexOf('arrangementBalanceComments') > -1
            || value.indexOf('fromBalanceLabel') > -1) returnBody = ElementTypes.THOUSAND_POUNDS.body;
        else returnBody = ElementTypes.VISIBLE_ELEMENT.body;
    }

    return returnBody;
};

module.exports = () => {
    const appiumMock = nock('http://127.0.0.1:4723', {encodedQueryParams: true})
        .filteringRequestBody(bodyFilter)
        .filteringPath(pathFilter)
        .post('/wd/hub/session')
        .reply(200, sessions)
        .persist();

    ElementTypes.keys().forEach((elementType) => {
        const elementIDPath = `/element/${ElementTypes[elementType].element.value.ELEMENT}`;
        const element = ElementTypes[elementType];
        appiumMock.post('/element', element.body)
            .reply(200, element.element)
            .persist()
            .get(`${elementIDPath}/text`)
            .reply(200, {sessionId: SESSIONID, status: 0, value: element.text})
            .persist()
            .get(`${elementIDPath}/displayed`)
            .reply(200, {sessionId: SESSIONID, status: 0, value: element.visible})
            .persist();
    });

    CommandTypes.keys().forEach((c) => {
        const command = CommandTypes[c];
        appiumMock.intercept(command.path, command.method)
            .reply(200, Object.assign(genericResponse, command.response))
            .persist();
    });
    return appiumMock;
};
