const Enum = require('../../../enums/enum');

const CommandTypes = new Enum({
    EXECUTE_SYNC: {
        path: '/execute/sync', response: {value: ''}, method: 'POST'
    },
    GENERIC_ACTIONS: {
        path: '/genericElementAction', response: {}, method: 'POST'
    },
    ORIENTATION: {
        path: '/orientation', response: {value: 'POTRAIT'}, method: 'GET'
    },
    WINDOW_SIZE: {
        path: '/window/current/size', response: {value: {height: 1794, width: 1080}}, method: 'GET'
    },
    WINDOW_RECT: {
        path: '/window/rect', response: {value: {height: 1794, width: 1080}}, method: 'GET'
    },
    DELETE: {
        path: new RegExp('^$'), response: {}, method: 'DELETE'
    },
    ELEMENTS: {
        path: '/elements', response: {status: 0, value: [{ELEMENT: 'visible'}]}, method: 'POST'
    },
    CONTEXT: {
        path: '/context', response: {value: 'NATIVE_APP'}, method: 'POST'
    }
});

module.exports = CommandTypes;
