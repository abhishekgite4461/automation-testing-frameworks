const Enum = require('../../../enums/enum');

const generateElementWithID = (ELEMENT) => ({ELEMENT, value: {ELEMENT}, status: 0, sessionId: '37d6e8e9-697b-4f04-9019-049591694836'});

const ElementTypes = new Enum({
    VISIBLE_ELEMENT: {
        body: 'visibleElement', element: generateElementWithID('visible'), text: '(Available: £1000.00)', visible: true
    },
    INVISIBLE_ELEMENT: {
        body: 'invisibleElement', element: generateElementWithID('invisible'), text: 'Invisible', visible: false
    },
    EASY_SAVER: {
        body: 'easySaver', element: generateElementWithID('easySaver'), text: 'Easy Saver', visible: true
    },
    CLASSIC: {
        body: 'classic', element: generateElementWithID('classic'), text: 'Classic', visible: true
    },
    ONE_POUND: {
        body: 'onePound', element: generateElementWithID('onePound'), text: '£1.00', visible: true
    },
    THOUSAND_POUNDS: {
        body: 'thousandPounds', element: generateElementWithID('thousandPounds'), text: '(Available: £1000.00)', visible: true
    },
    ACCOUNT_NUMBER: {
        body: 'accountNumber', element: generateElementWithID('accountNumber'), text: '20058468', visible: true
    },
    SORT_CODE: {
        body: 'sortCode', element: generateElementWithID('sortCode'), text: '77-48-24', visible: true
    }
});

module.exports = ElementTypes;
