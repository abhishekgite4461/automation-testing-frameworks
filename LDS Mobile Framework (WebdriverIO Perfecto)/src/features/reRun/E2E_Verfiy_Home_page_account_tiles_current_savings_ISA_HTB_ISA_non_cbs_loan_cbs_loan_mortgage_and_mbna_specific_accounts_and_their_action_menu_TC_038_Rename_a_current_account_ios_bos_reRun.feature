@team-aov @ios @bos @reRun@nga3 @ios @bos @reRun@rnga @ios @bos @reRun@sw-enrolment-on @ios @bos @reRun@sw-light-logon-on @ios @bos @reRun@sw-direct-debit-nga-on @ios @bos @reRun@sw-amend-standing-order-on @ios @bos @reRun@optFeature @ios @bos @reRun
Feature: E2E_Verfiy Home page , account tiles (current ,savings ,ISA ,HTB ISA ,non-cbs loan ,cbs loan ,mortgage and mbna specific accounts) and their action menu



  @stub-enrolled-valid @ff-rename-accounts-enabled-on @otherAccount @abhi @ios @bos @reRun
  Scenario Outline: TC_038_Rename a current account
	Given I am an enrolled "rename_savings_account" user logged into the app
	And I select the "rename account" option in the action menu of <accountType> account
	Then I should be on Rename account page
	And I should see <accountType> account name in text field
	And the Rename button is disabled
	When I enter new account name <newAccountName> in the text field
	And I select Rename button
	Then confirmation box is displayed

	Examples: 
	|accountType	|newAccountName|
	|saving	|Savings Account 2|
