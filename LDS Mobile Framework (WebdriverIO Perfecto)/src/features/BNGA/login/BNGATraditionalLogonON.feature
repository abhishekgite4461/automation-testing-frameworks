@team-login @sw-enrolment-on @sw-light-logon-on @bnga @nga3
#NGA Commercial - Device Enrolment switch is set to ON and Device is not yet enrolled
#This feature file cover Traditional Logon process where Device enrolment switches are turned ON and user has to authenticate via Username, password and MI

Feature: User uses traditional Logon for logging into the APP(using Username, Password and Memorable Information)
  In Order to access my accounts online
  As a Business banking user
  I want to log into my app using username, password and MI

#------------------------------------------- Main Success scenario---------------------------#

  @stub-valid
  Scenario: TC_001_Validating stay button on FSCS tile winback popup on enter MI page
    Given I am a business banking user on the MI page
    When I select FSCS tile in MI page
    Then I choose the stay option on the fscs popup
    And I should be on enter MI page
    When I select FSCS tile in MI page
    And I choose the leave option on the fscs popup
    Then I should see fscs page in the mobile browser

  @stub-incorrectmi-incorrect-password
  Scenario: TC_002_Validating stay button on FSCS tile winback popup on enter Re enter MI page
    Given I am a business banking user on the MI page
    And I enter incorrect MI
    And I should see incorrect MI error message in re-enter MI page
    When I select the fscs tile on the re-enter MI page
    And I choose the stay option on the fscs popup
    Then I should be on the re-enter MI page
    When I select FSCS tile in MI page
    And I choose the leave option on the fscs popup
    Then I should see fscs page in the mobile browser

  # Stub not available in BNGA
  @stub-incorrectmi-correct-password @pending
  Scenario: TC_003_Login to RNGA app with Wrong MI and enter the incorrect MI & correct password second time
    Given I am a business banking user on the MI page
    When I enter incorrect MI
    And I should see incorrect MI error message in re-enter MI page
    And I enter correct MI in re-enter MI page
    Then I should see the fscs tile in the re-enter password page

#------------------------------------------- Exceptional scenario------------------------------#

  #MPT-676
  @stub-business-banking
  Scenario: TC_01_Exception_Scenario: User logs into business app to enrol new device and user has already reached maximum number of enrolled device count
    Given I am a business banking user
    When I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    Then I should be shown an error page logged out with error message for maximum number of devices

  #MPT-677 #MPT-663
  @stub-card-reader-respond-card-no-not-editable
  Scenario: TC_02_Exception_Scenario: Last 5 digit of the card field non-editable on Card Respond screen
    Given I am a business banking user
    When I navigate to the login screen
    And I login with my username and password
    And I provide valid card number and passcode in the the card reader identify screen
    And I create my memorable information
    And I navigated back on card reader respond screen
    And I should see the last 5 digit of my authentication or debit card pre-populated and <string>
    Then I should see 6 digit challenge code on the card reader respond screen

  #MPT-4600
  @stub-soft-token-mi-not-setup @android
  Scenario: TC_03_Exception Scenario: User issued with soft-token as secondary authentication token is trying to login to Business NGA app using correct username and password(MG 2486)
    Given I am a soft token without mi bnga user
    When I navigate to the login screen
    And I login with my username and password
    Then I should be shown an error page logged out with error message <HC-322>
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  #MPT-4601
  @stub-soft-token-mi-already-setup @android
  Scenario: TC_04_Exception Scenario: User issued with soft-token as secondary authentication token is trying to login to Business NGA app using correct username and password(MG 2486)and MI
    Given I am a soft token with mi bnga user
    When I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    Then I should be shown an error page logged out with error message <HC-322>
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay
