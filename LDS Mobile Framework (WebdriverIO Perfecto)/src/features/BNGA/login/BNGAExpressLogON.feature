@team-mpt @sw-enrolment-on @sw-light-logon-on @bnga @nga3
#This feature file cover Express Logon process where Device is fully enrolled and Light Logon switch is turned ON

Feature: Enrolled User perform seamless logon(Express Logon) using MI
  In Order to access my accounts using seamless logon
  As a Business banking user
  I want to log into my app using only my Memorable information

#------------------------------------------- Main Success scenario---------------------------#

  #TODO: MPT-2365
  #MPT-774, MPT-828
  @primary @sanity @stub-single-business-full-signatory-enrolled
  Scenario: TC_001 Main Success Scenario : User having access to single business successfully login to enrolled business app using seamless logon i.e. via providing their memorable information
    Given I am on MI screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I enter correct MI
    Then I should be on the home page

  #TODO: MPT-2365
  #MPT-774, MPT-828
  @primary @stub-enrolled-valid
  Scenario: TC_002 Main Success Scenario : User having access to multiple businesses successfully login to enrolled business app using seamless logon i.e. via providing their memorable information
    Given I am an enrolled "multiple business" user on the MI page
    When I enter correct MI
    Then I should see business selection screen

  @stub-valid
  Scenario: TC_004_Validating stay button on FSCS tile winback popup on enter MI page
    Given I am a enrolled business user on the MI page
    When I select FSCS tile in MI page
    And I choose the stay option on the fscs popup
    Then I should be on enter MI page
    When I choose the leave option on the fscs popup
    Then I should see fscs page in the mobile browser

#------------------------------------------- Variation scenario-----------------------------#

  @stub-correctmi-correct-password @primary
  Scenario: TC_003_Variation_Scenario: User enter incorrect MI and has not yet reached maximum incorrect attempts(MG-400)
    Given I am on MI screen as FULL_SIGNATORY access user with CURRENT account
    When I enter incorrect MI
    Then I should see incorrect MI error message in re-enter MI page
    When I enter correct MI in re-enter MI page
    And I enter correct password in confirm password page
    And I verify card number and enter passcode in the card reader respond screen
    And I am shown Enrolment success screen

 #------------------------------------------- Exception scenario-----------------------------#

  @manual @primary
  Scenario: TC_004_Exception_Scenario: User has exceeded maximum incorrect authentication attempts to enter MI(MG-400 & MG-401)
    Given I am an enrolled "single business" user on the MI page
    When I enter incorrect MI
    And Maximum number of attempts exceeded
    Then I should be on revoked error page
    #Note: the maximum number is configurable value in IBC

  @manual
  Scenario: TC_005_Exception_Scenario: User is logging into an enrolled app where the enrolment status is changed to "Un-enrolled" or Dormant in IBC due to security issues (MG-2147)
    Given I am an enrolled business user
    When my app is de-enrolled in IBC
    Then I should see an dormant message on Error page logged out screen(MG-2147)
    And I should see an option to login back to app

  @manual
  Scenario: TC_006_Exception_Scenario: User is logging into an enrolled app after 6 months due to which app enrolment status is dormant
    Given I am an enrolled BNGA user logging into the app after 6 months
    And my app status is changed to dormant using IBC
    When I launch the enrolled app
    Then I should see an dormant message on Error page logged out screen(MG-2158)
    And I should see an option to login back to app

  @manual
  Scenario: TC_007_Exception_Scenario: Login to enrolled BNGA app with password status as revoked (MG-395)
    Given I am an enrolled "password revoked" user
    When I launch the enrolled app
    Then I should see an appropriate message on Error page logged out screen(MG-395)
    And I should see an option to login back to app

  @manual
  Scenario: TC_008_Exception_Scenario: Login to enrolled BNGA app with suspended mandate [MG-393]
    Given I am an enrolled "suspended" user
    When I launch the enrolled app
    Then I should see an suspended message on Error page logged out screen

  @manual
  Scenario: TC_009_Exception_Scenario: Login to enrolled BNGA app with inactive mandate [MG-394]
    Given I am an enrolled "inactive" user
    When I launch the enrolled app
    Then I should see an inactive message on Error page logged out screen

  #MPT-774, MPT-828
  @stub-enrolled-valid
  Scenario: TC_010_Exception_Scenario : User choose help on MI page
    Given I am an enrolled "single business" user on the MI page
    And I select the tooltip link on the MI page
    Then I should be on the MI tooltip page
