@team-mpt @bnga @nga3
#Device Enrolment switch is ON and user is not yet enrolled for their device
Feature: Device enrolment using Card Reader when MI is not setup

  In Order to access my accounts online using seamless login
  As a Business banking user without MI setup
  I want to complete my device enrolment process

  #------------------------------------------- Main Success scenario-----------------------------------------------------------------#
  #MPT-1542, MPT-1543
  @secondary @stub-card-reader-identify-correct-passcode
  #Note: Testers should validate the successful enrolment record using IBC application and ensure the device alias name is unique and status of the device is enrolled
  Scenario Outline:TC_001_Main Success Scenario: User(without MI setup & access to single business) successfully create MI and enrol their business app using Card Reader
    Given I am a <userType> user
    And I am on card reader identify screen
    And I provide valid card number and passcode in the the card reader identify screen
    And I create my memorable information
    And I verify last five pre-populated card digits is non-editable in the card reader respond screen
    And I provide valid passcode in the card reader respond screen
    When I select continue button on the enrolment confirmation page
    Then I verify the default time in the security auto logout settings page
    When I choose my security auto logout settings
    Then I should be on the home page
    Examples:
      | userType                                      |
      | single business full signatory without MI     |
      | single business full delegate without MI      |
      | single business view only delegate without MI |

  @primary @stub-card-reader-identify-multiple-businesses
  #Note: Testers should validate the successful enrolment record using IBC application and ensure the device alias name is unique and status of the device is enrolled
  Scenario Outline:TC_002_Main Success Scenario: User(without MI setup & access to multiple business) successfully create MI and enrol their business app using Card Reader
    Given I am a <userType> user
    And I am on card reader identify screen
    And I provide valid card number and passcode in the the card reader identify screen
    And I create my memorable information
    And I verify last five pre-populated card digits is non-editable in the card reader respond screen
    And I provide valid passcode in the card reader respond screen
    When I select continue button on the enrolment confirmation page
    Then I verify the default time in the security auto logout settings page
    When I choose my security auto logout settings
    Then I should see business selection screen with available business
    Examples:
      | userType                     |
      | multiple business without MI |

  @primary @manual
  Scenario: TC_003_Additional Scenario: Verify unique device alias name in IBC
    Given I am a multiple business without MI user
    And I am on card reader identify screen
    And I provide valid card number and passcode in the the card reader identify screen
    When I create my memorable information
    Then the system should create unique device alias name

  @primary @stub-card-reader-identify-correct-passcode
  Scenario: TC_003_Additional Scenario:  User shouldn't be allowed to edit the last 5 digit of the card number on Card Reader Respond screen
    Given I am a multiple business without MI user
    And I am on card reader identify screen
    And I provide valid card number and passcode in the the card reader identify screen
    When I create my memorable information
    Then I should be shown card reader respond screen
    And I verify last five pre-populated card digits is non-editable in the card reader respond screen
    And I should see 6 digit challenge code on the card reader respond screen

  #MPT-784, MPT-684
  @manual
  Scenario: TC_004_Additional Scenario:  User should be shown numeric keyboard
    Given I am a multiple business without MI user
    And I am on card reader identify screen
    When I try to enter the pass-code or Card number
    Then I should be shown platform specific numeric keyboard

  @stub-card-reader-identify-correct-passcode
  Scenario: TC_005_Additional Scenario:  Continue button should be enabled when user enters last 5 digits of card number and 8 digit passcode
    Given I am a multiple business without MI user
    And I am on card reader identify screen
    And I have entered the last 5 digits of valid card number in the card reader identify page
    When I enter the correct 8 digit passcode from the card reader in the card reader identify page
    Then I should see the continue button enabled in the card reader identify page

  #------------------------------------------- Exceptional scenario-----------------------------------------------------------------#
  @primary @manual
  Scenario: TC_006_Exception Scenario : User logs into business app to enrol new device and user has already reached maximum number of enrolled device count i.e. 5(MG-2157)
    Given I am a multiple business with MI bnga user
    And I have already enrolled my app on 5 different devices
    And all of them are in active state
    When I am logging into app to enrol 6th device
    Then I should be shown an maximum devices enrolled error message on an error page logged out screen(MG-2157)

  @primary @stub-card-reader-identify-incorrect-passcode
  Scenario: TC_007_Exception Scenario: User is entering incorrect pass-code and has not yet reached max attempts (MG-400)
    Given I am a FULL_SIGNATORY user
    And I am on card reader identify screen
    When I have entered the last 5 digits of valid card number in the card reader identify page
    And I enter the incorrect 8 digit passcode from the card reader in the card reader identify page
    And I select the continue button on the card reader Identify or respond page
    Then I should be shown incorrect passcode error banner in the card reader identify page

  @manual
  Scenario: TC_008_Exception Scenario: User is entering incorrect Pass-code and reached maximum number of incorrect attempts (MG-1190)
    Given I am a multiple business without MI user
    And I am on card reader identify screen
    And I have entered the last 5 digits of valid card number in the card reader identify page
    When I enter the incorrect 8 digit passcode consequently for x number of times
    Then I should be shown an maximum number of attempts reached error message on error page logged out
    When I select an option to login back to app on error page logged out
    Then I should see my app is initialized from start point
    #Note: x is configurable value

  #MPT-660,MPT-650
  @stub-card-reader-identify-invalid-card
  Scenario: TC_009_Exception Scenario: User has entered invalid last 5 digit of card number in the Card Reader Identify(MG-1188)
    Given I am a multiple business without MI user
    And I am on card reader identify screen
    And I have entered the last 5 digits of invalid card number in the card reader identify page
    When I enter the correct 8 digit passcode from the card reader in the card reader identify page
    And I select continue in the card reader identify page
    Then I should be shown invalid card error banner in the card reader identify page

  #MPT-660,MPT-655
  @stub-card-reader-identify-invalid-card
  Scenario: TC_010_Exception Scenario: User has entered last 5 digit of card which is already expired(MG-1188)
    Given I am a card expired multiple business without MI user
    And I am on card reader identify screen
    And I have entered the last 5 digits of expired card number in the card reader identify page
    When I enter the correct 8 digit passcode from the card reader in the card reader identify page
    And I select continue in the card reader identify page
    Then I should be shown card expired error banner in the card reader identify page

  #MPT-660,,MPT-650
  @stub-card-reader-identify-suspended-card
  Scenario: TC_011_Exception Scenario: User has entered the last 5 digit of card number which is already suspended(MG-1186)
    Given I am a card suspended multiple business without MI user
    And I am on card reader identify screen
    And I have entered the last 5 digits of suspended card number in the card reader identify page
    When I enter the correct 8 digit passcode from the card reader in the card reader identify page
    And I select continue in the card reader identify page
    Then I should be shown card suspended error banner in the card reader identify page

  #MPT-660,MPT-650
  @stub-card-reader-identify-expired-card
  Scenario: TC_012_Exception Scenario: User has entered the last 5 digit of card number which is already expired and new card is already issued by bank(MG-1189)
    Given I am a new card multiple business without MI user
    And I am on card reader identify screen
    And I have entered the last 5 digits of old expired card number in the card reader identify page
    When I enter the correct 8 digit passcode from the card reader in the card reader identify page
    And I select continue in the card reader identify page
    Then I should be shown card expired new card issued error banner in the card reader identify page

  #MPT-660,MPT-650
  @stub-card-reader-identify-new-card @stubOnly
  Scenario: TC_013_Exception Scenario: User has two or more cards ending with same last 5 digits and is trying to provide the card number on Card Reader Identify screen(MG-1187)
    Given I am a card already exist multiple business without MI user
    And I am on card reader identify screen
    And I have entered the last 5 digits of old expired card number in the card reader identify page
    When I enter the correct 8 digit passcode from the card reader in the card reader identify page
    And I select continue in the card reader identify page
    Then I should be shown card already exist error banner in the card reader identify page

  # MPT-784,MPT-684
  @stub-card-reader-identify-correct-passcode
  Scenario: TC_014_Additional Scenario:  User clicks the 'Having trouble with your card reader?' link from the Card Reader IDENTIFY screen
    Given I am a multiple business without MI user
    And I am on card reader identify screen
    When I choose an option Having trouble with your credit reader from card reader identify screen
    Then I should see pop-up with Close and Continue option
    And on selection of close option I should stay on the card reader screen
    And on selection of continue option I should navigate to browser

  @secondary @stub-card-reader-identify-correct-passcode
  Scenario: TC_015_Validate the pre-auth header menu in the card reader identify page in BNGA
    Given I am a multiple business without MI user
    And I am on card reader identify screen
    When I select the contact us option from the card reader identify page pre auth header
    Then I should be on pre auth call us home page overlay
