@team-mpt @bnga @nga3 @stub-card-reader-identify-correct-passcode
Feature: User
  In order to use Memorable information for logon
  As a BNGA user
  I want to be able to create my memorable information page

  Background:
    Given I am a current account without MI user
    And I provide my correct username and password
    And I provide valid card number and passcode in the the card reader identify screen
    And I am on Create Memorable Info page

  #MPT-686,MPT-650
  Scenario Outline: TC001 - MI validation - Error - Incorrect characters entered in MI field
    When I enter "<inputValue>" in Memorable Info Field on the Create Memorable Info page
    And I should see Memorable Info Tip on the Create Memorable Info page
    And I tap out of the Field on the Create Memorable Info page
    Then I validate Confirm Memorable Info Field is not enabled on the Create Memorable Info page

    Examples:
      | inputValue |
      | test1      |
      | 1234567    |
      | memory     |

  #MPT-686,MPT-650
  Scenario Outline: TC002 – MI validation - Maximum characters allowed in MI field
    When I enter "<longMI>" in Memorable Info Field on the Create Memorable Info page
    Then I validate maximum characters allowed for Memorable Info Field should be "<maxAllowed>" on the Create Memorable Info page

    Examples:
      | longMI           | maxAllowed |
      | abcdefghij123456 | 15         |

  #MPT-686,MPT-650
  @manual
  Scenario Outline: TC003 – MI validation - Error - Must not be same as UserID or password [MSG-263]
    When I enter "<type>" in Memorable Info Field on the Create Memorable Info page
    Then I should see error banner on the Create Memorable Info page

    Examples:
      | type     |
      | userid   |
      | password |

  #MPT-686,MPT-650
  Scenario: TC004 - confirm MI is enabled after successful MI validation
    When I enter valid Memorable Info on the Create Memorable Info page
    And I should see Memorable Info Tip on the Create Memorable Info page
    And I tap out of the Field on the Create Memorable Info page
    Then I validate Confirm Memorable Info Field is enabled on the Create Memorable Info page

  #MPT-686,MPT-650
  Scenario: TC005 – Customer can confirm the MI - Not Matching
    When I enter valid Memorable Info on the Create Memorable Info page
    And I tap out of the Field on the Create Memorable Info page
    And I enter "a" in Confirm Memorable Info Field on the Create Memorable Info page
    Then I should see Confirm Memorable Info Tip on the Create Memorable Info page

  #MPT-686,MPT-650
  Scenario: TC007 – Customer can confirm the MI - Matching
    When I enter valid Memorable Info on the Create Memorable Info page
    And I tap out of the Field on the Create Memorable Info page
    And I enter valid Confirm Memorable Info on the Create Memorable Info page
    Then I should see Confirm Memorable Info Tip on the Create Memorable Info page

  #MPT-686,MPT-650
  @manual
  Scenario: TC008 - Default masked and Masked when Customer goes back to the MI field
    When I enter valid Memorable Info on the Create Memorable Info page
    And I validate the Memorable Info Field is masked on the Create Memorable Info page
    And I tap out of the Field on the Create Memorable Info page
    And I select Memorable Info on the Create Memorable Info page
    Then I validate the Memorable Info Field is masked on the Create Memorable Info page

  #MPT-686,MPT-650
  @manual
  Scenario: TC009 - Default masked and Masked when Customer goes back to the Confirm MI field
    When I enter valid Memorable Info on the Create Memorable Info page
    And I tap out of the Field on the Create Memorable Info page
    And I enter valid Confirm Memorable Info on the Create Memorable Info page
    And I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page
    And I tap out of the Field on the Create Memorable Info page
    And I select Confirm Memorable Info on the Create Memorable Info page
    Then I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page

  #MPT-686,MPT-650
  Scenario: TC010 - Customer can unmask and mask MI field
    When I enter valid Memorable Info on the Create Memorable Info page
    And I select HideShow Button on Memorable Info Field on the Create Memorable Info page
    And I validate the Memorable Info Field is not masked on the Create Memorable Info page
    And I select HideShow Button on Memorable Info Field on the Create Memorable Info page
    Then I validate the Memorable Info Field is masked on the Create Memorable Info page

  #MPT-686,MPT-650
  Scenario: TC011 - Customer can unmask and mask Confirm MI field
    When I enter valid Memorable Info on the Create Memorable Info page
    And I tap out of the Field on the Create Memorable Info page
    And I enter valid Confirm Memorable Info on the Create Memorable Info page
    And I select HideShow Button on Confirm Memorable Info Field on the Create Memorable Info page
    And I validate the Confirm Memorable Info Field is not masked on the Create Memorable Info page
    And I select HideShow Button on Confirm Memorable Info Field on the Create Memorable Info page
    Then I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page

  #MPT-686,MPT-650
  Scenario: TC018 - Find Out More Dialog
    When I select Memorable Info on the Create Memorable Info page
    And I select FindOutMore Link in Memorable Info hint text on the Create Memorable Info page
    Then I validate Memorable Info tips dialog on the Create Memorable Info page
    And I should be able to close Memorable Info tips dialog on the Create Memorable Info page

  Scenario: TC019 - Validate the contact us option on the create mi page
    When I select the contact us option from the create memorable information page pre auth header
    Then I should be on pre auth call us home page overlay
