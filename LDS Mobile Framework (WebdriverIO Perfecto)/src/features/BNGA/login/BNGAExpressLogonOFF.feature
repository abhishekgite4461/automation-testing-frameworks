@team-mpt @sw-enrolment-off @sw-light-logon-off @bnga @nga3 @stubOnly @stub-enrolled-valid @ios
#stubOnly since testing involves toggling the Light logon switch OFF after enrolling into the device
Feature: Express login with DeviceEnrolment switch OFF and Light logon switch OFF

  #MPT-683
  Scenario: TC_001_Verify enrolled user lands on login page on selecting ok button in express logon unavailable warning message
    Given I am a current user
    When I select Ok button in express logon unavailble warning message
    Then I should be on the Login page
