@team-mpt @nga3 @bnga @stub-valid
Feature: BNGA welcome screen
  In order to have a better user experience
  As a BNGA Customer
  I want to see a re-designed welcome screen

  Background:
    Given I am a current user
    And I am at first welcome screen

  #---------------------------------------Main Success scenario---------------------------------------#
    #MPT-1849, MPT-4224
  @primary
  Scenario: TC-001 Main Success Scenario : User should be able to view options on first welcome screen
    Then I should be shown the screen with following welcome benefits screen options
      | welcomeScreenOptions  |
      | makeYourBankingEasier |
      | concernedAboutFraud   |
      | dataPrivacy           |
      | becomeACustomer             |

  #MPT-4229, MPT-4224
  @instrument
  Scenario: TC-002 Exception Scenario : User should be able to view data privacy screen
    And I choose an option to view data privacy screen
    Then Data privacy screen should be displayed
    When I choose to view data privacy notice
    Then I am displayed a warning with option to proceed or cancel
    When I choose to continue on the warning message
    Then I should be displayed data privacy site

  Scenario: TC-003 Exception Scenario: User should be able to view cookie policy
    And I choose an option to view data privacy screen
    Then Data privacy screen should be displayed
    When I choose to view the cookies policy
    Then I am displayed a warning with option to proceed or cancel

  #MPT-1849, MPT-4224
  @primary
  Scenario: TC-004 Exception Scenario : User should be able to view options on second welcome screen
    And I choose an option to continue to the next screen
    Then I should be shown the screen with following welcome screen options
    |welcomeScreenOptions        |
    |onlineBankingRegistrationLink|
    |infoAboutCardReader          |
    |whyYouNeedACard              |

  #MPT-1849, MPT-4224
  @primary
  Scenario: TC-005 Exception Scenario : User should be able to choose an option to register for online banking
    And I choose an option to continue to the next screen
    And I choose register for online banking option
    Then I am displayed a warning with option to proceed or cancel

  #MPT-1849, MPT-4224
  Scenario: TC-006 Exception Scenario : User should be able to view more information about card reader
    And I choose an option to continue to the next screen
    And I choose to view more information about the card reader
    Then I should be displayed an overlay on welcome screen

   #MPT-1849, MPT-4224
  Scenario: TC-007 Exception Scenario : User should be able to view why you need card option
    And I choose an option to continue to the next screen
    And I choose why you need card option
    Then I should be displayed an overlay on welcome screen
