@team-mpt @sw-enrolment-off @sw-light-logon-off @bnga @nga3
#NGA Commercial - Device Enrolment switch is set to OFF and Device is not yet enrolled
#This feature file cover Traditional Logon process where Device enrolment switches are turned OFF and user has to authenticate via Username, password and MI

Feature: User uses traditional Logon for logging into the APP(using Username, Password and Memorable Information)
  In Order to access my accounts online
  As a Business banking user
  I want to log into my app using username, password and MI

#------------------------------------------- Main Success scenario---------------------------#

  @stub-valid @primary
  Scenario Outline: TC_001 Main Success Scenario : User having access to single business successfully login into Business banking app using correct credentials and access homepage
    Given I am a <userType> user
    When I login with my username and password
    And I enter correct MI
    Then I should see business selection screen with available business
    Examples:
      | userType                                   |
      | single business full signatory with MI     |
      | single business full delegate with MI      |
      | single business view only delegate with MI |
      | single business connected user with MI     |

  @stub-valid @primary
  Scenario: TC_002 Main Success Scenario : User having access to multiple businesses should see business selection screen on logon
    Given I am a multiple business with MI user
    When I login with my username and password
    And I enter correct MI
    Then I should see business selection screen

  @stub-valid @primary
  Scenario Outline: TC_003 Main Success Scenario : User having access to multiple businesses successfully login into Business banking app using correct credentials
    Given I am a FULL_SIGNATORY user
    When I login with my username and password
    And I enter correct MI
    And I select first business of <accessLevel> from selection screen
    Then I should be on the home page
    Examples:
      | accesslevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  @stub-valid @secondary
  Scenario: TC_004_User should see the Password field characters are masked
    Given I am a single business full signatory with MI user
    When I enter the password "pass"
    Then I should see the password field is masked

  #MOB3-1143 MOB3-4116
  @stub-valid @secondary
  Scenario: TC_005_User should see the Memorable characters are masked
    Given I am a multiple business with MI user
    And I login with my username and password
    When I enter 2 characters of MI
    Then I should see the MI information is masked

#------------------------------------------- Exceptional scenario------------------------------#

  @stub-incorrect-credentials @primary
  Scenario Outline: TC_006 Exception_Scenario: User login to app with incorrect username(MG-358)
    Given I am a multiple business with MI user
    When I login with an incorrect username "<username>"
    Then I should see an incorrect username or password error message
    Examples:
      | username     |
      | garbage      |
      | garbage-/_+( |

  @stub-incorrect-credentials @primary
  Scenario Outline: TC_007 Exception_Scenario: User login to app with incorrect password(MG-358)
    Given I am a multiple business with MI user
    When I login with an incorrect password "<password>"
    Then I should see an incorrect username or password error message
    Examples:
      | password               |
      | garbage                |
      | garbage-/_+(           |
      | garbagegarbagegarbagee |

  @stub-hard-token @android
  Scenario: TC_008.1 Exception Scenario: User issued with hard-token as secondary authentication token is trying to login to Business NGA app using correct username and password(MG 2486)
    Given I am a hard token bnga user
    When I login with my username and password
    Then I should be shown an error page logged out with error message
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  @stub-hard-token @ios
  Scenario: TC_008.2 Exception Scenario: User issued with hard-token as secondary authentication token is trying to login to Business NGA app using correct username and password(MG 2486)
    Given I am a hard token bnga user
    When I login with my username and password
    Then I should see an error message for hard token user

  @stub-revoked-password
  Scenario: TC_009 Exception_Scenario: User login to business app with password status as revoked (MG-395)
    Given I am a revoked user
    When I login with my username and password
    Then I should be on revoked error page
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  #MOB3-2491,MOB3-12510
  @stub-suspended
  Scenario: TC_010_Exception_Scenario: User login to business app with suspended user
    Given I am a suspended user
    When I login with my username and password
    Then I should be on suspended error page
    And I should see the telephone number displayed in the error page
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  #MOB3-2491,MOB3-12510
  @stub-mandateless
  Scenario: TC_011_Exception_Scenario: Retail User login to business app with retail username and password and don't have mandate for business channel
    Given I am a mandateless user
    When I login with my username and password
    Then I should see a not registered to IB error message in login page

  #MPT-787, MPT-831
  @primary @manual
  Scenario: TC_012_Exception_Scenario: User enter incorrect MI and has not yet reached maximum incorrect attempts(MG-400)
    Given I am a single business full signatory with MI user
    And I login with my username and password
    When I enter incorrect MI
    Then I should see incorrect MI error message in re-enter MI page
    When I enter correct MI in re-enter MI page
    And I enter incorrect password in confirm password page
    Then I should see incorrect password error message in re-enter MI page
    When I enter correct MI in re-enter MI page
    And I enter correct password in confirm password page
    And I verify card number and enter passcode in the card reader respond screen
    Then I should be on the home page

  @manual
  Scenario: TC_013_Exception_Scenario: User has exceeded maximum incorrect authentication attempts to enter MI(MG-401)
    Given I am a single business full signatory with MI user
    And I login with my username and password
    When I enter incorrect MI
    Then I should see incorrect MI error message in re-enter MI page
    When I enter incorrect MI in re-enter MI page
    Then I should be on the confirm password page
    When I enter incorrect password in confirm password page
    Then I should see incorrect password error message in re-enter MI page
    When I enter incorrect MI in re-enter MI page
    Then I should be on the confirm password page
    When I enter incorrect password in confirm password page
    Then I should see incorrect password error message in re-enter MI page
    When I enter incorrect MI in re-enter MI page
    Then I should be on revoked error page
    When I select an option to login back to app in the revoked error page
    Then I should see my app is initialized from start point i.e. splash screen

  @manual @primary
  Scenario Outline: TC_014_Security_Scenario : User Login to Business app in jailbroken/rooted mobile device
    Given I am a "<userType>" user
    When I login with my username and password
    Then I should be on jailbroken/rooted error page
    Examples:
      | userType               |
      | Malware                |
      | Tampered               |
      | Jailbroken/Rooted      |
      | Data on App/OS Version |

  #MOB3-1135
  @stub-valid
  Scenario: TC_016_Validate the pre-auth header menu in the login page in BNGA
    Given I am a multiple business with MI user
    When I select the contact us option from the login page pre auth header
    Then I should be on pre auth call us home page overlay

  #MOB3-1135
  @stub-valid @secondary
  Scenario: TC_017_Validate the password field is masked
    Given I am a multiple business with MI user
    When I enter my username and password
    Then I should see the password field is masked

  #MOB3-1135
  @stub-valid
  Scenario: TC_018_Login Username can be a maximum of 30 characters
    Given I am a multiple business with MI user
    When I enter the username "withmorethanthirtycharactersinusername"
    Then I should be restricted from entering more than 30 characters in username text box

  #MOB3-1135
  @manual
  Scenario: TC_020_Login Password can be a maximum of 20 characters
    Given I am a multiple business with MI user
    When I enter the password "withmorethantwentycharactersinpassword"
    Then I should be restricted from entering more than 20 characters in password text box

  #MOB3-2493
  @stub-valid @instrument
  Scenario: TC_021_Verify that user is taken to the forget username/password mobile browser page while clicking on "Forgotten your login details"
    Given I am a multiple business with MI user
    When I select the forget your login link
    Then I should see forget username page in the mobile browser

  #MOB3-1143 MOB3-4116
  @stub-valid @secondary
  Scenario: TC_022_User should see the Memorable characters are masked
    Given I am a multiple business with MI user
    And I login with my username and password
    When I select the tooltip link on the MI page
    Then I should be on the MI tooltip page

  #MOB3-2491,MOB3-12510
  @stub-inactive @secondary
  Scenario: TC_023_Login User’s mandate is inactive
    Given I am a inactive user
    When I login with my username and password
    Then I should be on inactive error page
    When I click on Logon to Mobile Banking button in the error page
    Then I should be on the environment selector page

  @stub-inactive
  Scenario: TC_024_Login User’s mandate is inactive
    Given I am a inactive user
    When I login with my username and password
    Then I should be on inactive error page
    And I should see the telephone number displayed in the error page
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  @stub-valid
  Scenario: TC_025_Login to BNGA app with less than minimum characters required for password field
    Given I am a multiple business with MI user
    When I enter valid username
    And I enter the password "Incor"
    Then I should see the continue button is disabled

  @stub-valid
  Scenario: TC_026_Validate Login to BNGA app with less than minimum characters required for username field
    Given I am a multiple business with MI user
    When I enter the username ""
    And I enter valid password
    Then I should see the continue button is disabled

  @manual
  Scenario: TC_027_Should be able to Copy and paste contents in username field
    Given I am a current user
    When I enter the username "user"
    And I copy the content in the username field
    And I paste the content in the username field
    Then I should see the username field filled with "useruser"

  @manual
  Scenario: TC_028_User cannot copy the password field content
    Given I am a current user
    And I enter the password "pass"
    And I copy the content in the password field
    When I paste the content in the username field
    Then I should not see the username field filled with "pass"

  #since the password field is masked, should actually match with the no of characters
  @manual
  Scenario: TC_029_User should be able to only paste contents in password field
    Given I am a current user
    And I enter the username "user"
    And I copy the content in the username field
    When I paste the content in the password field
    Then I should see the password field filled with "user"

  @manual
  Scenario: TC_030_Login to BNGA app with a incorrect password consecutively for X times so that the account is revoked (MG-395)
    Given I am a current user
    When I login with an incorrect password X times
    Then I should be on revoked error page
    And I should see an option to login back to app
    When I select an option to login back to app
    Then I should see my app is initialized from start point i.e. splash screen
    #NOTE : X is configurable value and it is set to 4 for business

  #MPT-787, MPT-831
  @stub-incorrectmi-incorrect-password
  Scenario: TC_025_Exception_Scenario: User enter incorrect MI and incorrect password(MG-400)
    Given I am a multiple business with MI user
    And I login with my username and password
    When I enter incorrect MI
    Then I should see incorrect MI error message in re-enter MI page
    When I enter incorrect MI in re-enter MI page
    Then I should be on the confirm password page
    When I enter incorrect password in confirm password page
    Then I should see incorrect password error message in re-enter MI page

  #MPT-787, MPT-831
  @stub-correctmi-correct-password
  Scenario: TC_026_Exception_Scenario: User enter incorrect MI, correct MI and correct password(MG-400)
    Given I am a multiple business with MI user
    And I login with my username and password
    When I enter incorrect MI
    Then I should see incorrect MI error message in re-enter MI page
    When I enter correct MI in re-enter MI page
    And I enter correct password in confirm password page
    And I verify card number and enter passcode in the card reader respond screen
    Then I should see business selection screen

  #MPT-682
  @stub-card-reader-respond-card-no-not-editable
  Scenario: TC_027_Exception_Scenario: User is creating MI during traditional Logon
    Given I am a business banking user
    And I login with my username and password
    And I provide valid card number and passcode in the the card reader identify screen
    And I create my memorable information
    Then I should see business selection screen

  #MPT-4600
  @stub-soft-token-mi-not-setup @android
  Scenario: TC_028_Exception Scenario: User issued with soft-token as secondary authentication token is trying to login to Business NGA app using correct username and password(MG 2486)
    Given I am a soft token without mi bnga user
    When I navigate to the login screen
    And I login with my username and password
    Then I should be shown an error page logged out with error message <HC-322>
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  #MPT-4601
  @stub-soft-token-mi-already-setup @android
  Scenario: TC_029_Exception Scenario: User issued with soft-token as secondary authentication token is trying to login to Business NGA app using correct username and password(MG 2486)
    Given I am a soft token with mi bnga user
    When I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    Then I should be shown an error page logged out with error message <HC-322>
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay
