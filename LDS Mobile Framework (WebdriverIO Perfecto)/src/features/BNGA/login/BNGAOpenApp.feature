@team-mpt @bnga @nga3
#This feature file cover App launch and its respective error scenarios.

Feature: Ability for User to initiate app launch from device

#------------------------------------------- Exception scenario-----------------------------#
  #MPT-672
  @stub-app-launch-failed @stubOnly
  Scenario Outline: TC_001_Exception_Scenario:  Validate the background image and error message due to technical error on launching the app
    Given I relaunch the app and select platform <platform> and app <app>
    Then I should see App launch screen being displayed
    And I should see technical error being displayed
    Examples:
      |platform                   | app             |
      |App Launch Error Scenarios | Technical Error |

  @environmentOnly
  Scenario: TC_002_Exception_Scenario:Validate the background image and error message due to internet connectivity issue on launching the app
    Given my internet connection is OFF
    And I am a multiple business with MI user
    Then I should see App launch screen being displayed
    And I should see internet connection error being displayed
    And I should turn wifi on
