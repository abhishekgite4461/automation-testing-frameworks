@team-mpt @bnga @nga3
Feature: BNGA User de-enrols from the app
  In order to de-enrol my app from my device
  As a BNGA user
  I want to be able to proceed with reset mobile banking

  Background:
    Given I am on MI screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I enter correct MI

  # MPT-716
  @stub-single-business-full-signatory-enrolled
  Scenario: TC_001 Entry Point Scenario: User should be able to access de-register option from the app Settings page
    When I navigate to settings page via more menu
    And I select the reset app option from the settings page
    Then I should be on the reset app Page

  #MPT-726
  @primary @sanity @stub-single-business-full-signatory-enrolled
  Scenario: TC_002 Main Success Scenario: User de-enrols the BNGA app successfully
    When I navigate to settings page via more menu
    And I select the reset app option from the settings page
    And I should be on the reset app Page
    And I select reset app button from the reset app page
    And I am shown winback with Cancel and Ok option
    And I select confirm button from the reset app confirmation pop up
    Then I should be on the logout page
    And I should be displayed a log-off complete message - HC030
  #Note: After every de-enrolment following activities must be performed.
  #1.   Entry in the IBC device enrolment table must be updated to DENROL state
  #2.   Log-off API is call
  #3.   Delete the token from security core
  #4.   All client side cache must be cleared.

  #MPT-726
  @stub-single-business-full-signatory-enrolled
  Scenario: TC_003 Exception Scenario: User cancels de-enrolment
    When I navigate to settings page via more menu
    And I select the reset app option from the settings page
    And I should be on the reset app Page
    And I select reset app button from the reset app page
    And I am shown winback with Cancel and Ok option
    And I select cancel button from the reset app confirmation pop up
    Then I should be on the reset app Page

  #MPT-713
  @stub-enrolled-valid
  Scenario Outline: TC_004 Exception Scenario: User is logged off due to inactivity and displayed message (HC002)
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    And I select the desired "<autoLogOffTime>" from the auto log off page
    And I navigate to settings page via more menu
    And I select the reset app option from the settings page
    And I should be on the reset app Page
    And I select reset app button from the reset app page
    And I am shown winback with Cancel and Ok option
    Then I should see a log off alert 15 seconds before the "<inactiveTime>" auto logoff time
    And I select the logoff option from the logoff alert pop up
    And I should be on the logout page
    Examples:
      |businessName             |autoLogOffTime   |inactiveTime             | accessLevel     |
      |fullSignatoryBusiness    |1 Minute         |1.01                     |full Signatory   |
      |fullDelegateBusiness     |2 Minute         |2.01                     |full Delegate    |

  @stub-single-business-full-signatory-enrolled
  Scenario Outline: TC_005 Exception Scenario: User is logged off due backgrounding the app and displayed message (HC002)
    When I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    And I select the desired "<autoLogOffTime>" from the auto log off page
    And I put the app in the background
    And I launch the app from the background
    Then I should be on the logout page

    Examples:
      | autoLogOffTime  |
      | logoffImmediate |

  @pending
  Scenario: TC_006 Exception Scenario: User has no internet connection
    And I am on Reset Mobile Banking screen
    And I have no internet connection
    When I reset the app
    Then I must be logged-off
    And I must be displayed the Logoff complete page with message (HC002)
    And my app must not be reset

  @pending
  Scenario: TC_007 Exception Scenario: Connection error from server
    And I am on Reset Mobile Banking screen
    And I reset the app
    When there is a connection error from server
    Then I must be logged-off
    And I must be displayed the Logoff complete page with message (HC002)
    And my app must not be reset
