@team-mpt @bnga @nga3
Feature: Reset Password
  In order to change my password
  As a BNGA user
  I want to see an option to reset my password using app

  #MPT-1035, MPT-2976
  @secondary @sanity @stub-enrolled-valid
  Scenario Outline: TC_001 Main Success Scenario: User reset the password successfully using app
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I navigate to the re-set password page
    And I enter my new valid password in the reset password page
    And I select the submit button in the reset password page
    Then I should see a password updated pop up in the reset password page
    And I should be on the logout page

    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  #MPT-1035
  @stub-enrolled-valid
  Scenario: TC_002 Main Success Scenario: User wants to view tips on password security
    Given I am an enrolled "valid" user logged into the app
    When I select a business from the business selection screen
    And I navigate to the re-set password page
    And I close the tips for a good password accordion from the reset password page
    Then I should not see the password security tips

  #MPT-905
  @ios @stub-enrolled-valid
  Scenario: TC_003_Exception_Scenario: User provides non-identical password in the screen [HC-010]
    Given I am an enrolled "valid" user logged into the app
    And I select first signatory business from selection screen
    And I navigate to the re-set password page
    And I enter two non identical password in the reset password page
    And I select the submit button in the reset password page
    Then I should see a non-identical error message on the reset password page

  #MPT-905
  @ios @stub-reset-password-personal-info
  Scenario Outline: TC_004_Exception_Scenario: User wish to set the password same as their personal information[MG-3418]
    Given I am an enrolled "valid" user logged into the app
    And I select first signatory business from selection screen
    And I navigate to the re-set password page
    When I set my new password as <personalInfo>
    And I select the submit button in the reset password page
    Then I should see a no personal details error banner on the reset password page

    Examples:
      |personalInfo          |
      |registered_name       |
      |date_of_birth         |
      |email_address         |

  #MPT-905
  @stub-reset-password-insecure-pwd  @pending
  Scenario Outline: TC_005_Exception_Scenario: User wish to set the password same as Memorable Information [MG-265]
    Given I am an enrolled "valid" user logged into the app
    And I select first signatory business from selection screen
    And I navigate to the re-set password page
    When I set my new password as <personalInfo>
    And I select the submit button in the reset password page
    Then I should see a non-secure error banner on the reset password page

    Examples:
      |personalInfo          |
      |memorable_information |

  #MPT-905
  @secondary @stubOnly @stub-reset-password-prev-pwd  @pending
  Scenario: TC_006_Exception_Scenario: User enters password that has been used in last five instances [MG-515]
    Given I am an enrolled "valid" user logged into the app
    And I select first signatory business from selection screen
    And I navigate to the re-set password page
    And I enter new password which I have used in last five instances
    And I select the submit button in the reset password page
    Then I should see a last 5 password error banner on the reset password page

  #MPT-905
  @stub-reset-password-easy-guess  @pending
  Scenario Outline: TC_007_Exception_Scenario: User enters new password that is too simple e.g ‘password1’ or ‘abcd1234’ [MG-3419]
    Given I am an enrolled "valid" user logged into the app
    And I select first signatory business from selection screen
    And I navigate to the re-set password page
    When I set my new password as simple password "<simple_password>"
    And I select the submit button in the reset password page
    Then I should see an easy-to-guess passwords error banner on the reset password page

    Examples:
      |simple_password|
      |password1          |
      |abcd1234           |
