@team-login @nga3 @bnga @sw-enrolment-on @sw-light-logon-on @manual @optFeature @primary

Feature: As an BNGA User I should be able to login via fingerprint and
  I want to be informed of any fingerprint change in my device
  so that I can decide to login into APP using biometric prompt or not

#  Precondition:
#  Device is not enrolled
#  Device is bio-metric compatible with no registered bio
#  Bio-metric switch is ON

  Scenario:TC_01 User click 'Turn on bio' button on bio-metric screen, in enrollment journey
    Given I am a multiple business with MI user
    And I verify card number and enter passcode in the card reader respond screen
    And I select continue button on the enrolment confirmation page
    Then I verify the default time in the security auto logout settings page
    When I choose my security auto logout settings
    And I click 'Turn on bio' button on biometric screen
    Then I should see 'No registered bio' popup with OK button
    When I click on OK button on No registered bio popup
    And I click on Ask me later button
    Then I should see business selection screen with available business
    And I should be navigated to homepage

  Scenario:TC_02 User click 'OK' button on No registered bio popup, in enrollment journey
    Given I am a single business with MI user
    And I verify card number and enter passcode in the card reader respond screen
    And I select continue button on the enrolment confirmation page
    Then I verify the default time in the security auto logout settings page
    When I choose my security auto logout settings
    And I click 'Turn on bio' button on biometric screen
    Then I should see 'No registered bio' popup with OK button
    When I click on OK button on No registered bio popup
    And I should see Biometric screen with 3 buttons
    And I click on Ask me later button
    Then I should be on the home page

  Scenario:TC_03 User click 'Turn on bio' button on bio-metric screen, in logon journey
    Given I am an enrolled "multiple business" user on the MI page
    When I enter correct MI
    And I see biometric screen
    And I click 'Turn on bio' button on biometric screen
    Then I should see 'No registered bio' popup with OK button
    When I click on Ask me later button
    Then I should see business selection screen with available business
    And I should be navigated to homepage

  Scenario:TC_04 User click 'OK' button on No registered bio popup, in logon journey
    Given I am an enrolled "single business" user on the MI page
    And I enter correct MI
    And I see biometric screen
    And I click 'Turn on bio' button on biometric screen
    And I see 'No registered bio' popup with OK button
    When I click on OK button on No registered bio popup
    And I should see Biometric screen with 3 buttons
    And I click on Ask me later button
    Then I should be on the home page

  Scenario:TC_05 User toggle 'Sign in with bio' button, in Settings(Security) journey
    Given I am on home screen as multiple business user with current account and <any> access
    When I navigate to settings page from home page
    And I select security settings from settings menu
    And I toggle 'Sign in with bio' button
    Then I should see 'No registered bio' popup with OK button

  Scenario:TC_06 User click 'OK' button on No registered bio popup, in Settings(security) journey
    Given I am on home screen as single business user with current account and <any> access
    When I navigate to settings page via more menu
    And I select security settings from settings menu
    And I toggle 'Sign in with bio' button
    And I see 'No registered bio' popup with OK button
    And I click on OK button on No registered bio popup
    And I should see security settings page
    And toggle should be OFF

  Scenario:TC_07 User opens the app after making some change to registered bio (addition/update/deletion)
    Given I am on home screen as multiple business user with current account and <any> access
    When I authenticate using biometric
    Then I should see 'Bio changed' popup with OK buttons
    And I should be navigated to MI screen upon click of OK button

  Scenario:TC_08 User opens the app after making some change to registered bio (addition/update/deletion), enters valid MI
    Given I am on home screen as single business user with current account and <any> access
    When I authenticate using biometric
    Then I should see 'Bio changed' popup with OK buttons
    And I should be navigated to MI screen upon click of OK button
    When I enter valid MI
    Then I should see 'Before we confirm' popup with 2 buttons

#  Precondition:
#  Device is not enrolled
#  Device is bio-metric incompatible
#  Bio-metric switch is ON

  Scenario:TC_09 User opens the app, enters correct password + MI + EIA/OTP/Passcode
    Given I am a multiple business with MI user
    And I verify card number and enter passcode in the card reader respond screen
    And I select continue button on the enrolment confirmation page
    Then I verify the default time in the security auto logout settings page
    When I choose my security auto logout settings
    Then I should not see biometric screen with 3 buttons
    And I should see business selection screen with available business
    And I should be navigated to homepage

  Scenario:TC_10 Existing(before change) user opens the latest app, enters correct MI
    Given I am an enrolled "single business" user on the MI page
    And I open biometric incompatible app
    And I enter correct MI
    Then I should not see biometric screen with 3 buttons
    And I should be on the home page
