@team-mpt @bnga @nga3
Feature: Device enrolment using Card Reader when MI is already setup

  In Order to access my accounts online using seamless login
  As a Business banking user with Full signatory, Delegate View or Delegate Full access
  I want to complete my device enrolment providing correct passcode generated using Card Reader

  #------------------------------------------- Main Success scenario-----------------------------------------------------------------#

  # MOB3-10134,MOB3-2209
  @primary @sanity @stub-single-business-full-signatory-unenrolled
  #Note: Testers should validate the successful enrolment record using IBC application and ensure the device alias name is unique and status of the device is enrolled
  # Default time needs to be verified manually
  Scenario Outline:TC_001_Main Success Scenario: User(with MI setup & access to single business) successfully enrols their business app using Card Reader respond passcode
    Given I am a <userType> user
    And I provide my correct username, password and MI
    When I verify card number and enter passcode in the card reader respond screen
    Then I should see contact us option in enrolment confirmation page
    When I select continue button on the enrolment confirmation page
    Then I should see contact us option in the security auto logout settings page
    # And I verify the default time in the security auto logout settings page
    When I choose my security auto logout settings
    Then I should be on the home page
    Examples:
      | userType                                   |
      | single business full signatory with MI     |
      | single business full delegate with MI      |
      | single business view only delegate with MI |
      | single business connected user with MI     |

  #Note: Testers should validate the successful enrolment record using IBC application and ensure the device alias name is unique and status of the device is enrolled
  @primary @stub-card-reader-respond-correct-passcode
  Scenario Outline:TC_002_Main Success Scenario: User(with MI setup & access to multiple business) successfully enrols their business app using Card Reader respond passcode
    Given I am a <userType> user
    And I provide my correct username, password and MI
    And I verify card number and enter passcode in the card reader respond screen
    And I select continue button on the enrolment confirmation page
    Then I verify the default time in the security auto logout settings page
    When I choose my security auto logout settings
    Then I should see business selection screen
    Examples:
      | userType                  |
      | multiple business with mi |

  @primary @stub-card-reader-respond-correct-passcode
  Scenario Outline:TC_003_Main Success Scenario: Verify the Card Reader respond screen is displayed when correct memorable information is provided
    Given I am a <accessLevel> with single auth or debit card and with MI user
    When I provide my correct username, password and MI
    Then I should be shown card reader respond screen
    And I should see the last 5 digit of my authentication or debit card pre-populated and editable
    And I should see 6 digit challenge code on the card reader respond screen
    And I should be prompted to enter the 8 digit passcode in card reader respond screen
    And I should see the continue button is disabled in card reader respond screen
    Examples:
      | accessLevel        |
      | full signatory     |
      | full delegate      |
      | delegate view only |

  #MOB3-9034
  @manual
  Scenario: TC_004_Main Success Scenario: User should be shown numeric keyboard
    Given I am a multiple business with MI user
    And I am on card reader respond screen
    When I try to enter the pass-code or Card number
    Then I should be shown platform specific numeric keyboard

  #------------------------------------------- Exceptional scenario-----------------------------------------------------------------#

  @primary @manual
  Scenario: TC_005_Exception Scenario : User logs into business app to enrol new device and user has already reached maximum number of enrolled device count i.e. 5(MG-2157)
    Given I am a multiple business with MI user
    And I have already enrolled my app on 5 different devices
    And all of them are in active state
    When I am logging into app to enrol 6th device
    Then I should be shown an appropriate error message on an error page logged out screen(MG-2157)

  @primary @stub-card-reader-respond-incorrect-passcode
  Scenario: TC_006_Exception Scenario: User is entering incorrect pass-code and has not yet reached max attempts (MG-400)
    Given I am a multiple business with MI user
    And I am on card reader respond screen
    And I have entered the last 5 digits of valid card number in the card reader respond page
    When I enter the incorrect 8 digit passcode from the card reader in the card reader respond page
    And I select the continue button on the card reader Identify or respond page
    Then I should be shown incorrect passcode error banner in the card reader respond page

  @manual
  Scenario: TC_007_Exception Scenario: User is entering incorrect Pass-code and reached maximum number of incorrect attempts (MG-1190)
    Given I am a multiple business with MI user
    And I am on card reader respond screen
    And I have entered the last 5 digits of valid card number in the card reader respond page
    When I enter the incorrect 8 digit passcode consequently for x number of times
    Then I should be shown an appropriate error message on error page logged out
    When I select an option to login back to app on error page logged out
    Then I should see my app is initialized from start point
    #Note: x is configurable value

  #MPT-654, MPT-670,MPT-3102 - open defect
  @stub-card-reader-respond-cardnumber-invalid
  Scenario: TC_008_Exception Scenario: User has entered invalid last 5 digit of card number in the Card Reader Respond(MG-1188)
    Given I am a multiple business with MI user
    And I am on card reader respond screen
    And I have entered the last 5 digits of invalid card number in the card reader respond page
    When I submit the correct 8 digit passcode from the card reader in the card reader respond page
    Then I should be shown invalid card error banner in the card reader respond page

  #MPT-654, MPT-670,MPT-3102 - open defect
  @stub-card-reader-respond-cardnumber-invalid
  Scenario: TC_009_Exception Scenario: User has entered last 5 digit of card which is already expired(MG-1188)
    Given I am a card expired multiple business with MI user
    And I am on card reader respond screen
    And I have entered the last 5 digits of expired card number in the card reader respond page
    When I submit the correct 8 digit passcode from the card reader in the card reader respond page
    Then I should be shown card expired error banner in the card reader respond page

  #MPT-654, MPT-670,MPT-3102 - open defect
  @stub-card-reader-respond-card-suspended
  Scenario: TC_010_Exception Scenario: User has entered the last 5 digit of card number which is already suspended(MG-1186)
    Given I am a card suspended multiple business with MI user
    And I am on card reader respond screen
    And I have entered the last 5 digits of suspended card number in the card reader respond page
    When I submit the correct 8 digit passcode from the card reader in the card reader respond page
    Then I should be shown card suspended error in the error page

  #MPT-654, MPT-670,MPT-3102 - open defect
  # TODO change the stub names MPT-4253
  @stub-card-reader-respond-card-invalid
  Scenario: TC_011_Exception Scenario: User has entered the last 5 digit of card number which is already expired and new card is already issued by bank(MG-1189)
    Given I am a new card multiple business with MI user
    And I navigate to the login screen
    And I login with my username and password
    Then I should be shown card expired and new card issued error in the error page

  #MPT-654, MPT-670,MPT-3102 - open defect
  @stub-card-reader-respond-new-card @stubOnly
  Scenario: TC_012_Exception Scenario: User has two or more cards ending with same last 5 digits and is trying to provide the card number on Card Reader Respond screen(MG-1187)
    Given I am a card already exist multiple business without MI user
    And  I am on card reader respond screen
    And I have entered the last 5 digits of old expired card number in the card reader respond page
    When I submit the correct 8 digit passcode from the card reader in the card reader respond page
    Then I should be shown card already exist error banner in the card reader respond page

  #MPT-777, MPT-110
  @stub-card-reader-respond-correct-passcode @instrument
  Scenario: TC_013_Exception Scenario: User clicks the 'Having trouble with your card reader?' link from the Card Reader RESPOND screen
    Given I am a multiple business with mi user
    And I am on card reader respond screen
    When I select Having trouble with your credit reader link from card reader respond screen
    Then I should see problem with your card reader pop-up with close and continue option
    And I should stay on the card reader screen on selection of close option in problem with your card reader pop-up
    And I should navigate to browser on selection of continue option in problem with your card reader pop-up

  @manual
  Scenario:TC_014_Additional Scenario: Verify splash screen when the user launches the app
    Given I launch the app
    Then I should be able to see splash screen
    And I should see Authenticating your device security loader on the splash screen

  @manual
  #MOB3-9034
  Scenario:TC_015_Additional Scenario: Verify enrolment status and unique device alias name using IBC
    Given I am logged into IBC
    When I search for devices enrolled for User
    Then I should see record with status as Enrolled
    And I should see unique device alias name against the record

  @stub-card-reader-respond-correct-passcode
  Scenario:TC_016_Additional Scenario: User should see continue button disabled when user has entered less than 5 digit card number and 8 digit passcode
    Given I am a multiple business with mi user
    And I am on card reader respond screen
    When I enter 7 digit passcode and 4 digit card number in the card reader respond page
    Then I should see the continue button disabled in the card reader respond page

  @stub-card-reader-respond-correct-passcode
  Scenario:TC_017_Additional Scenario: User should see continue button should be enabled only when user has entered 5 digit card number and 8 digit passcode
    Given I am a multiple business with mi user
    And I am on card reader respond screen
    And I have entered the last 5 digits of valid card number in the card reader respond page
    When I enter the correct 8 digit passcode from the card reader in the card reader respond page
    Then I should see the continue button enabled in the card reader respond page

  @stub-card-reader-respond-correct-passcode
  Scenario:TC_018_Additional Scenario: User should see continue button disabled when the passcode field is left blank
    Given I am a multiple business with mi user
    And I am on card reader respond screen
    When I have entered the last 5 digits of valid card number in the card reader respond page
    And I leave the passcode field empty in the card reader respond page
    Then I should see the continue button disabled in the card reader respond page

  @stub-card-reader-respond-correct-passcode
  Scenario:TC_019_Additional Scenario: User should see continue button disabled when the last five digits of card number field is left blank
    Given I am a multiple business with mi user
    And I am on card reader respond screen
    When I leave the last five card digits field empty in the card reader respond page
    And I enter the correct 8 digit passcode from the card reader in the card reader respond page
    Then I should see the continue button disabled in the card reader respond page

  @secondary @stub-card-reader-respond-correct-passcode
  Scenario:TC_020_Additional Scenario: Validate the pre-auth header menu in the card reader respond page in BNGA
    Given I am a multiple business with mi user
    And I am on card reader respond screen
    When I select the contact us option from the card reader respond page pre auth header
    Then I should be on pre auth call us home page overlay
