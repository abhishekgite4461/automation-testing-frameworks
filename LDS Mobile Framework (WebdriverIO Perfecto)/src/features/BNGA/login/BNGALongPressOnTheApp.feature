@team-mpt @bnga @nga3 @stub-enrolled-valid @android @appShortcut
Feature: Verify AppShortcuts on the long press of the app
  Background:
    Given I am a current user
    When I select back key on the device
    And I put the app in the background

  Scenario: TC_01_Verify shortcut to Branch Finder and ourWebsite on long press of app launcher
    When I long press the app from shortcut
    Then I should see three shortcuts from the app
    When I select Branch Finder from shortcut
    Then I should be displayed with map screen opened
    When I long press the app from shortcut
    And I select our website from preauth shortcut
    Then I should be displayed with the website opened
    When I long press the app from shortcut
    And I select contact us from shortcut
    And I enter correct MI
    Then I should see business selection screen with available business
    When I select a business from the business selection screen
    Then I should be displayed with the Support page opened

  Scenario Outline: TC_04_Verify shortcut to Branch Finder and ourWebsite on long press of app launcher Outside UK
    When I set the location as "<OutsideUKAddress>" which is outsite UK in device
    And I long press the app from shortcut
    Then I should see three shortcuts from the app
    When I select Branch Finder from shortcut
    Then I should be displayed with map screen opened
    When I select back key on the device
    And I select back key on the device
    And I long press the app from shortcut
    And I select our website from preauth shortcut
    Then I should be displayed with the website opened
    And I reset back the original location of the device

    Examples:
      | OutsideUKAddress |
      | Chennai          |

  Scenario: TC_02_Verify shortcut to Branch Finder and ourWebsite on long press of app launcher when internet is OFF
    Given my internet connection is OFF
    When I long press the app from shortcut
    Then I should see three shortcuts from the app
    When I select Branch Finder from shortcut
    Then I should see internet connection error being displayed
    When I select back key on the device
    And I long press the app from shortcut
    And I select our website from preauth shortcut
    Then I should see internet connection error being displayed in Browser
    And I should turn wifi on

  @manual
  Scenario Outline: TC_03_Verify shortcut to Branch Finder and ourWebsite on long press of app launcher When Maps app not installed
    When I launch settings in device
    And I search for the navigation app <AppManager>
    And I search for the navigation app <mapApp>
    And I disable the app
    And I long press the app from shortcut
    Then I should see three shortcuts from the app
    When I select Branch Finder from shortcut
    Then I should be displayed with map screen opened
    When I long press the app from shortcut
    And I select our website from preauth shortcut
    Then I should be displayed with the website opened
    Examples:
      | mapApp | AppManager  |
      | Maps   | Apps        |
