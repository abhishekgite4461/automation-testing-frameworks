@team-mpt @bnga @nga3 @stub-enrolled-valid
Feature: Display Product Hub to access list of products

  In Order to apply for new products on the go
  As a full signatory Business User
  I want to be able to easily access information about products relevant to me quickly and easily from within my app

  Background:
    Given I am an enrolled "valid" user logged into the app
    And I select first signatory business from selection screen

  #------------------------------------------ Main Success scenario---------------------------#

  #MPT-665, MPT-653
  Scenario: TC_001 Main Success Scenario : User navigate to Product hub page from Homepage
    When I select apply tab from bottom navigation bar
    Then the Featured option is shown as expanded
    And the other options are shown as closed accordion
    And I should see the product hub home page with product categories
      | productCategoryLabels  |
      | featured               |
      | businessCurrentAccount |
      | businessSavingsAccount |
      | cards                  |
      | loans                  |
      | mortgages              |
      | businessInsurance      |
      | international          |
      | businessGuidesAndInfo  |

  #MPT-665, MPT-653
  Scenario: TC_002 Main Success Scenario : User select product links from Product Hub homepage categories
    And I select apply tab from bottom navigation bar
    When I select business loan from the product categories
    Then I should be navigated to business loan mobile browser

   #Note: No win backs to be shown when Customer clicks on a link from Product Hub which takes the customer out of the App. For example, business insurance link takes customer out of the app
   #Note: The Product hub items order will be different for RBB & SME customers.
