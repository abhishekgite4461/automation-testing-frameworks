@team-pi @nga3 @bnga @bos @pending @manualOnly
Feature: Customer is displayed with three Product Hub Entry point
  As a Product Owner
  I want to see which variation of product hub label performs best in the BNGA BoS A/B test.
  so that I have insight into the results of changing the respective entry point label

  #These scenarios are manual only since there is an interaction between 3rd party provider Adobe Target.
  #Purpose: To set up an AB test for the product hub entry point on the bottom navigation in BNGA

  Background:
    Given I am a BOS BNGA  customer
    And I am eligible for the BNGA product hub AB test

  #PIDI-1849, PIDI-1846, PIDI-1851, PIDI-1852

  @primary
  Scenario:AC01 - Variant A product hub copy 'Apply'
    When I am part of variant group A
    And I log in to the mobile app
    Then I should see the copy as 'Apply' on the bottom navigation

  @primary
  Scenario:AC02 - Variant B product hub copy 'Products'
    When I am part of variant group B
    And I log in to the mobile app
    Then I should see the copy as 'Products' on the bottom navigation

  @primary
  Scenario:AC03 - Variant C product hub copy 'Offers'
    When I am part of variant group C
    And I log in to the mobile app
    Then I should see the copy as 'Offers' on the bottom navigation

  @primary
  Scenario:AC04 - Header for product hub.
    When I am part of variant group A, B or C
    And I click on the product hub from the bottom nav
    Then I should see the same copy on the product hub page title

  @primary
  Scenario:AC05 - Default experience on bottom nav for Product hub
    When There is no live test is available from adobe or I am not part of any AB test
    And I log in to the mobile app
    Then I should see the copy as 'Apply' on the bottom navigation

  @secondary
  Scenario:AC06 - Interactions for 'Apply'
    When I am part of variant group A
    And I select 'Apply' in the bottom nav
    Then an interaction should be recorded in adobe target

  @secondary
  Scenario:AC07 - Interactions for 'Products'
    When I am part of variant group B
    And I select 'Products' in the bottom nav
    Then an interaction should be recorded in adobe target

  @secondary
  Scenario:AC08 - Interactions for 'Offers'
    When I am part of variant group C
    And I select 'Offers' in the bottom nav
    Then an interaction should be recorded in adobe target

  @secondary
  Scenario:AC09 - Impressions for 'Apply'
    When I am part of variant group A
    And I log in to the mobile app
    And land on the home page in the test
    Then an impression should be recorded

  @secondary
  Scenario:AC10 - Impressions for 'Products'
    When I am part of variant group B
    And I log in to the mobile app
    And land on the home page in the test
    Then an impression should be recorded

  @secondary
  Scenario:AC11 - Impressions for 'Offers'
    When I am part of variant group C
    And I log in to the mobile app
    And land on the home page in the test
    Then an impression should be recorded
