@team-statements @nga3 @bnga @stub-enrolled-valid @stubOnly @optFeature @sw-enrolment-on @sw-light-logon-on
Feature: User views Current Account Transaction Details
  In order that I am better informed about a specific transaction
  As a BNGA user
  I should be able to view transaction details for my current account transactions

  Scenario Outline: TC_001 Main Success Scenario: User views Posted Transaction Details for current account debit card transactions and view Help with this transaction
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select CURRENT account tile from home page
    And I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | date                    |
      | vtdAmount               |
      | <typeOfTransaction>     |
      | transactionDescription  |
      | dateOfTransaction       |
      | cardEndingNumber        |
      | unsureTransactionButton |
    And I select the help with this transaction link in VTD screen
    And I must be displayed the Unsure About Transactions Help and Info screen
    When I select to contact customer service
    Then I am displayed the call us businessAccounts for the selected option
    Examples:
      | accessLevel          | transactionType       | typeOfTransaction       |
      | FULL_SIGNATORY       | contact Less Purchase | contactLessPurchaseText |
      | FULL_ACCESS_DELEGATE | chip And Pin Purchase | chipAndPinText          |
      | FULL_SIGNATORY       | online Debit Card     | onlinePhoneMailText     |

  Scenario Outline: TC_002 Main Success Scenario: User views Posted Transaction Details for current account payment transactions
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select CURRENT account tile from home page
    And I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | date                    |
      | vtdAmount               |
      | <typeOfTransaction>     |
      | transactionDescription  |
      | transferPaymentsButton  |
      | unsureTransactionButton |
    Examples:
      | accessLevel          | transactionType          | typeOfTransaction    |
      | FULL_ACCESS_DELEGATE | faster Payments Incoming | fasterPaymentsInText |
      | FULL_SIGNATORY       | mobile Payments Inbound  | mobilePaymentInText  |
      | FULL_ACCESS_DELEGATE | bill Payment             | billPaymentText      |
      | FULL_SIGNATORY       | bank Giro Credit         | bankGiroCreditText   |
      | FULL_ACCESS_DELEGATE | cash Deposited In IDM    | cashText             |

  Scenario Outline: TC_003 Main Success Scenario: User views Posted Transaction Details for current account payment transactions with extra fields
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select CURRENT account tile from home page
    And I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | vtdCloseButton            |
      | date                      |
      | vtdAmount                 |
      | <typeOfTransaction>       |
      | transactionDescription    |
      | transferPaymentsButton    |
      | unsureTransactionButton   |
      | <transactionDetailField1> |
      | <transactionDetailField2> |
    Examples:
      | accessLevel          | transactionType          | typeOfTransaction     | transactionDetailField1 | transactionDetailField2 |
      | FULL_SIGNATORY       | faster Payments Outgoing | fasterPaymentsOutText | dateOfTransaction       | timeOfTransaction       |
      | FULL_ACCESS_DELEGATE | mobile Payments Outgoing | mobilePaymentOutText  | dateOfTransaction       | timeOfTransaction       |
      | FULL_SIGNATORY       | transfers                | transferText          | sortCode                | accountNumber           |
      | FULL_ACCESS_DELEGATE | transfersOut             | transferText          | sortCode                | accountNumber           |

  #STAMOB-1027. Commented the all tab step as currently all tab is OFF due to production issue. Will uncomment all tab is enabled
  Scenario Outline: TC_004 Main Success Scenario: User views Pending Transaction Details for current account debit card and cheque transactions
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select CURRENT account tile from home page
#    Then I should be on the allTab of statements page
    And I tap on the pending transactions accordion
    And I select pending Direct Debit on statements page
    Then I should see pending transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | vtdAmount               |
      | transactionDescription  |
      | dateOfTransaction       |
      | debitDisclaimerText     |
      | unsureTransactionButton |
    And I close the pending transactions VTD
    When I select pending Cheque on statements page
    Then I should see pending transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | vtdAmount               |
      | notYetAvailable         |
      | chequesText             |
      | transactionDescription  |
      | depositedOn             |
      | availableBy             |
      | creditDisclaimerText    |
      | unsureTransactionButton |
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |
