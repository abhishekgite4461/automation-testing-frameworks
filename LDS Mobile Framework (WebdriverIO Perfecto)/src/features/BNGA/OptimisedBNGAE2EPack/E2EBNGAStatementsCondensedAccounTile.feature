@team-statements @nga3 @bnga @stub-enrolled-valid @optFeature @ff-pca-alltab-enabled-off @sw-enrolment-on @sw-light-logon-on
@android
Feature: E2E_verify RNGA current and previous month transactions
  #STAMOB-1027 Multiple business user while clicking on account tile app is getting logged off
  @environmentOnly
  Scenario Outline: TC_01 View current and previous month transactions for current and savings account
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I navigate to <type> account to validate accountNumber and select <accountType> account
    Then I should be displayed <type> account name action menu and last four digits of account number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
      | availableFunds      |
      | <typeField>         |
    When I view <month> transactions of <type> account
    Then I should be displayed <month> statements transactions
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    When I scroll down on the transactions
    Then I should be displayed current balance in the header alongside account Name
    And I should not be displayed account tile in statements page
    And I should be displayed month tabs in place of account tile
    When I scroll back to the top of my transactions
    Then The current balance will no longer display in the header and the account tile will reappear
    And I should scroll down to the end of the transactions to view <output> message of that <month> tab
    Examples:
      | type    | month    | output        | accountType | typeField         |
      #| CURRENT | allTab   | noCurrentInOuts | current     | overDraftLimit |
      | CURRENT | current  | currentInOuts | current     | overDraftLimit    |
      | CURRENT | previous | currentInOuts | current     | overDraftLimit    |
      | SAVINGS | current  | savingEndOf   | saving      | interestRateLabel  |
      | SAVINGS | previous | savingEndOf   | saving      | interestRateLabel  |

  @environmentOnly
  Scenario Outline: TC_02 User views dormant account message
    Given I am on home screen as FULL_SIGNATORY access user with <type> account
    When I select <type> account tile from home page
    Then I should be displayed a message
    When I scroll down on the transactions
    Then The account tile should not be hidden
    Examples:
      | type            |
      | CURRENT_DORMANT |
      | SAVINGS_DORMANT |

  @noExistingData @stubOnly
  Scenario Outline: TC_03 Main Success Scenario: User views recent transactions for credit cards and charge cards
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD account
    When I navigate to <type> account to validate accountNumber and select <accountType> account
    And I should be on the recent of statements page
    Then I should be displayed <type> account name action menu and last four digits of creditCard number in the header
    And I should not be displayed business name on the account tile
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
      | availableCredit     |
      | creditLimit         |
      | overDueAmount       |
    When I scroll down on the transactions
    Then I should be displayed current balance in the header alongside account Name
    And I should not be displayed account tile in statements page
    And I should be displayed month tabs in place of account tile
    When I scroll back to the top of my transactions
    Then The current balance will no longer display in the header and the account tile will reappear
    And I should be displayed recent transactions with following fields
      | fieldsInRecentTransaction  |
      | nextStatementDueDate       |
      | optionToFilterTransactions |
      | transactionsSinceMessage   |
    When I filter the transactions to view <typeFilter> transaction for recent
    Then I should see following fields for <typeFilter> in statements summary
      | fieldsInStatementsSummary |
      | clearFilter               |
      | <typeFilterTransactions>  |
    And I should be displayed <total> for <typeFilter> transactions
    And I should be displayed the <typeFilter> transactions for the month selected
    When I clear the filter
    Then I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | transactionType             |
      | firstTranscationAmount      |
    And I should scroll down to the end of the transactions to view <output> message
    When I filter the transactions to view <typeFilter> transaction for previous
    Then I should see following fields for <typeFilter> in statements summary
      | fieldsInStatementsSummary |
      | clearFilter               |
      | <typeFilterTransactions>  |
    And I should be displayed <total> for <typeFilter> transactions
    And I should be displayed the <typeFilter> transactions for the month selected
    When I clear the filter
    Then I should be displayed last month statement with the following fields in statements summary
      | fieldsInStatementsSummary   |
      | statementDateDescription    |
      | statementBalanceDescription |
      | minimumPaymentDescription   |
      | dueDateDescription          |
      | optionToFilterTransactions  |
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | transactionType             |
      | firstTranscationAmount      |
    And I should scroll down to the end of the transactions to view <output> message
    Examples:
      | type       | output      | typeFilter | typeFilterTransactions | total                        | accountType |
      | CREDITCARD | creditEndOf | cardEnding | typeCardFilter         | cardEndingTransactionTotal   | creditCard  |
      | CREDITCARD | creditEndOf | account    | typeAccountFilter      | nocardEndingTransactionTotal | creditCard  |
      | CHARGECARD | chargeEndOf | cardEnding | typeCardFilter         | cardEndingTransactionTotal   | chargeCard  |
      | CHARGECARD | chargeEndOf | account    | typeAccountFilter      | nocardEndingTransactionTotal | chargeCard  |

  @stubOnly
  # Treasury account statements aren't loading on lower environments, so can't test on environment
  Scenario: TC_004_Display treasury accounts information on account tile and header in statements page
    Given I am on home screen as VIEW_ONLY_DELEGATE access user with FIXED_TERM_DEPOSIT_WITH_MULTIPLE account
    And I navigate to FIXED_TERM_DEPOSIT_WITH_MULTIPLE account to validate accountNumber and select FIXED_TERM_DEPOSIT_WITH_MULTIPLE account
    Then I should be displayed FIXED_TERM_DEPOSIT_WITH_MULTIPLE account name action menu and last four digits of account number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile    |
      | accountBalance         |
      | grossInterestRateLabel |
      | warningAlert           |
    When I scroll down on the transactions
    Then I should be displayed current balance in the header alongside account Name
    And I should not be displayed account tile in statements page
    When I scroll back to the top of my transactions
    Then The current balance will no longer display in the header and the account tile will reappear
