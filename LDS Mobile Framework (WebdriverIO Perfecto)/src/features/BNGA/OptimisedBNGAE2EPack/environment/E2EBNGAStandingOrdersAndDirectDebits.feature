@team-statements @nga3 @bnga @environmentOnly @optFeature @stub-enrolled-valid @sw-enrolment-on @sw-light-logon-on
# Webviews on iOS takes to the log off page. So, executed only on environment
Feature: User wants to view standing order
  In order to view my standing order
  As a BNGA authenticated customer
  I want to be able to view standing order within my app

  #PJO-4685
  @defect @pending
  Scenario Outline: TC_01 Main Success Scenario: User navigates to standing orders functionality (webview page) from account tile menu options
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select the "standing order" option in the action menu of CURRENT account
    Then I should be on the standing Order webview page
    And I select dismiss modal using close button
    When I select the "direct debit" option in the action menu of CURRENT account
    Then I should be on the direct Debit webview page
    And I select dismiss modal using close button
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  @noExistingData
  Scenario Outline: TC_02 Main Success Scenario: User enters standing orders webview page from VTD
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select CURRENT account tile from home page
    And I select <type> on statements page
    And I tap on <button> in vtd screen
    Then I should be on the <type> webview page
    And I should have the option to navigate to home screen
    Examples:
      | accessLevel          | type           | button                    |
      | FULL_SIGNATORY       | standing Order | manageStandingOrderButton |
      | FULL_ACCESS_DELEGATE | standing Order | manageStandingOrderButton |
      | VIEW_ONLY_DELEGATE   | standing Order | manageStandingOrderButton |
      | FULL_SIGNATORY       | direct Debit   | viewDirectDebit           |
      | FULL_ACCESS_DELEGATE | direct Debit   | viewDirectDebit           |
      | VIEW_ONLY_DELEGATE   | direct Debit   | viewDirectDebit           |
