@team-payments @nga3 @bnga @environmentOnly @sw-enrolment-on @sw-light-logon-on @sw-fingerprint-android-on @sw-touch-id-on @optFeature @ff-pca-alltab-enabled-off

Feature: Verify amount field decimal validation, maximum available balance and perform success transfer journey

#COSE-7,72,67,PAYMOB-13
  Scenario Outline: TC_001: Verify amount field decimal validation, maximum available balance and perform success transfer journey and view transaction
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    When I enter <amount> in the amount field in the payment hub page
    Then I should see only 2 decimal places in the amount field in the payment hub page
    And I clear the amount field in the payment hub page
    When I enter amount greater than available balance in the remitter account in the payment hub page
    Then I should be able to close the insufficient money in your account error message in the payment hub page
    When I clear the amount field in the payment hub page
    And I submit the transfer with all the mandatory fields with any from account and to account with amount value 10005.00
    Then I should see internet transaction limit exceeded error message in payment hub page
    And I should see the values pre-populated in the payment hub page for transfer with amount value 10005.00
    When I clear the amount field in the payment hub page
    And I submit transfer with from account <fromAccount> and to account <toAccount> with amount 1.00
    And I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page
    When I select the view transaction option in the transfer success page
    And I should see the - £1.00 deducted from latest transaction
    And I navigate to home page
    Then I should see the corresponding 1.00 deducted in the remitter <fromAccount> and recipient accounts <toAccount>
    And I navigate to logoff via more menu
    And I should be on the logout page
    Examples:
      | accessLevel    | fromAccount | toAccount | amount |
      | FULL_SIGNATORY | CURRENT     | SAVINGS   | 0.01   |
