@team-login @nga3 @bnga @sw-enrolment-on @sw-light-logon-on @manual @optFeature @primary
#Fingerprint gets deleted hence manual
Feature: As an BNGA User I should be able to login via fingerprint and
  I want to be informed of any fingerprint change in my device
  so that I can decide to login into AFP or not

  Scenario Outline: TC_001 Relaunch app immediately after opted in and authenticate with fingerprint
    Given I am a fingerprints registered BNGA-user
    And I have opted-in for fingerprint authentication
    When I relaunch the app
    And I start the Login journey
    Then I should be prompted to Login via fingerprint
    When I use a wrong fingerprint for authentication
    Then I should not be authenticated or given access to my account
    When I use correct fingerprint I should be able to login
    Then I should see business selection screen
    When I select a business of <accessLevel> from selection screen
    Then I should be on the home page
    When I submit the password reset request
    And I log out of the NGA app in the password reset journey
    Then I must be authenticated only by MI to login to NGA when i relaunch the app
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  Scenario: TC_002 Fingerprint change detected
    When I launch the app
    And a change in my fingerprints is detected
    Then I must be informed about change in fingerprint with a native pop-up before my login journey
    When I select OK button on the pop-up
    Then I must be taken to MI page to authenticate a part of security
    When I successfully login via MI
    Then I must be displayed the fingerprint change risk pop-up message
    When I tap on the Yes button to confirm the risk message
    Then I must be displayed the AFP ON pop-up message
