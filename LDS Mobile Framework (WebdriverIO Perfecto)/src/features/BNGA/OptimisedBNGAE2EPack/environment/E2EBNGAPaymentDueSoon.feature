@team-statements @nga3 @bnga @stub-enrolled-valid @optFeature @sw-enrolment-on @sw-light-logon-on @environmentOnly @defect @pending
Feature: E2E Due Soon Payments
  As a NGA Retail customer,
  I want to view my pending payment transactions,
  So that I am better informed about my transactions

  # STAMOB-1061
  Scenario Outline: TC_001_View payments due soon options in contextual menu on statements page and validate due soon fields
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I select <type> account tile from home page
    And I select the viewPaymentsDueSoon option from the action menu in statements page
    Then I should see dueSoon tab in statements screen
    And I should validate fields in due soon screen
      | fieldsToValidate            |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    And I should not see payments due soon option on the action menu
    When I select one of the pending payment transaction
    Then I should see upcoming transaction details for that transaction with the following fields
      | fieldsInDetailScreen  |
      | dueSoonLozenge        |
      | datePendingPayment    |
      | vtdCloseButton        |
      | vtdAmount             |
      | outgoingPaymentText   |
      | payeeField            |
      | sortCodeAccountNumber |
      | referenceField        |
      | changeOrDeleteText    |
    When I close the pending transactions VTD
    Then I should see dueSoon tab in statements screen
    And I navigate to logoff via more menu
    And I should be on the logout page
    Examples:
      | type      |
      | CURRENT   |
      | SAVINGS   |
