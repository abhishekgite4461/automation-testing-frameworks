@team-mpt @nga3 @bnga @sw-data-consent-off @optFeature @sw-enrolment-off @sw-light-logon-off

Feature: E2E pack with Logon, Home page

  @stub-valid
  # Verify the scenario for other access level manually
  Scenario Outline: TC_001 Main Success Scenario : User having access to multiple businesses should see business selection screen on logon
    Given I reinstall the app
    And I am a MULTIPLE_BUSINESS and CURRENT user
    And I enter my username and password
    And I select continue button on login screen
    And I enter correct MI
    And I should see business selection screen
    When I select first business of <accessLevel> from selection screen
    Then I should be on the home page
    Examples:
      | accessLevel    |
      | FULL_SIGNATORY |

  @stub-enrolled-valid @stubOnly
  Scenario:TC_002 Main Success Scenario: User should be shown Business selection screen when they logon successfully and have access to more than one business(Full Signatory)
    Given I reinstall the app
    And I am a MULTIPLE_BUSINESS and CURRENT user
    When I select Ok button in express logon unavailable warning message
    And I login with my username and password
    And I enter correct MI
    Then I should see business selection screen with available business
    And I should see all the multiple business full signatory with MI businesses listed

  @stub-incorrect-credentials @unEnrolled
  Scenario Outline: TC_003 Exception_Scenario: User login to app with incorrect username(MG-358)
    Given I am a CURRENT user
    When I login with an incorrect username "<username>"
    Then I should see an incorrect username or password error message
    Examples:
      | username     |
      | smbIkbn      |
      | smbIkbn-0.74 |
