@team-statements @bnga @nga3 @sw-data-consent-on @optFeature @sw-enrolment-on @sw-light-logon-on
Feature: Create Native Interstitial page to select all consents
  In order to provide consent for my cookie usage
  As a customer
  I must be prompted to make a selection

  Preconditions for Data Consent
  Data Consent Switch is ON
  User has not selected the Data Consent on the device previously
  User is not shown Whats New screen
  User is not shown Enrolment journey
  User is not shown Touch ID registration prompt
  User is not shown Touch ID change prompt
  User is not performing any App sign journey

  #TODO STAMOB-1118 STUB is borken
  @stubOnly @stub-analytics-consent-single-business
  Scenario: TC001 Main Success Scenario : User with single business access accepts data consent on interstitial
    Given I submit "correct" MI as "single business data consent" user on the MI page
    And I have not made a selection for my data consent usage already on that device
    And I see a native manage BNGA data consent interstitial page without a back button
    When I select the option to accept the data consent
    Then I should be on the home page

  #TODO STAMOB-1118 STUB is borken
  @stubOnly @stub-analytics-consent-single-business
  Scenario: TC002 Negative Scenario: User with single business access does not accept data consent on interstitial
    Given I am on MI screen as SINGLE_BUSINESS business user with CURRENT account
    And I enter correct MI
    And I have not made a selection for my data consent usage already on that device
    And I see a native manage BNGA data consent interstitial page without a back button
    When I select the option not to accept the data consent
    Then I should be on the home page

  #TODO STAMOB-1118 STUB is borken
  @stubOnly @stub-analytics-consent-unset
  Scenario: TC003 Main Success Scenario: User with access to multiple business and different level of access, accepts data consent on interstitial
    Given I am on MI screen as FULL_SIGNATORY access user with CURRENT account
    And I enter correct MI
    And I have not made a selection for my data consent usage already on that device
    And I see a native manage BNGA data consent interstitial page without a back button
    When I select the option to accept the data consent
    Then I should see business selection screen
    #Note: The value of the tag would be set to 1 once accepted and 0 if not set or rejected. This is manual testing
    #BNGA user: The user must be with different access level (Full Signatory, Full Delegate, View Only Delegate) to different businesses.

  @stub-enrolled-valid @environmentOnly
  Scenario Outline: TC004 Main Success Scenario: User does not accept data consent in settings menu
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I have navigated to Personal details from settings
    And I select the manage data consent
    And I have accepted the data consent policy earlier
    And I now I have declined to give consent to data consent settings
    When I confirm the changes to my data consent settings
    Then I should be on the your personal details page
    And I select the manage data consent
    And now I have accepted to give consent to data consent settings
    When I confirm the changes to my data consent settings
    Then I should be on the your personal details page
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |

  @environmentOnly
  #App reset can't be stubbed
  Scenario Outline: TC005 Main Success Scenario : App reset to clear Privacy Management settings
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I have navigated to Personal details from settings
    And I select the manage data consent
    When I have set my privacy setting already on device
    And I reset the app
    And I relaunch the app
    And I am on home screen as <accessLevel> access user with CURRENT account
    And I have navigated to Personal details from settings
    And I select the manage data consent
    Then The data consent settings on that device must be cleared
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | VIEW_ONLY_DELEGATE   |

  @environmentOnly
  #can't be stubbed
  Scenario Outline: TC006 Main Success Scenario : Uninstall App to clear Privacy Management settings
    Given I reinstall the app
    And I am on home screen as <accessLevel> access user with CURRENT account
    When I have navigated to Personal details from settings
    And I select the manage data consent
    Then The data consent settings on that device must be cleared
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
