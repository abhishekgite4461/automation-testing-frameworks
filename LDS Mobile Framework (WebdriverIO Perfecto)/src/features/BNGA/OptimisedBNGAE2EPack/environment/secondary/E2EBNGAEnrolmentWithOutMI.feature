@team-mpt @bnga @nga3 @secondary @sw-enrolment-on @sw-light-logon-on @optFeature @stub-card-reader-identify-correct-passcode @exhaustiveData
#Device Enrolment switch is ON and user is not yet enrolled for their device
Feature: Device enrolment using Card Reader when MI is not setup

  In Order to access my accounts online using seamless login
  As a Business banking user without MI setup
  I want to complete my device enrolment process

  #------------------------------------------- Main Success scenario-----------------------------------------------------------------#
  #MPT-1542, MPT-1543
 #Note: Testers should validate the successful enrolment record using IBC application and ensure the device alias name is unique and status of the device is enrolled
  Scenario:TC_001_Main Success Scenario: User(without MI setup & access to single business) successfully create MI and enrol their business app using Card Reader
    Given I am a SINGLE_BUSINESS and FULL_SIGNATORY user
    And I am on card reader identify screen
    When I select the contact us option from the card reader identify page pre auth header
    Then I should be on pre auth call us home page overlay
    And I select dismiss modal using close button
    And I provide valid card number and passcode in the the card reader identify screen
    And I create my memorable information
    And I verify last five pre-populated card digits is non-editable in the card reader respond screen
    And I provide valid passcode in the card reader respond screen
    When I select continue button on the enrolment confirmation page
    Then I verify the default time in the security auto logout settings page
    When I choose my security auto logout settings
    Then I should be on the home page
