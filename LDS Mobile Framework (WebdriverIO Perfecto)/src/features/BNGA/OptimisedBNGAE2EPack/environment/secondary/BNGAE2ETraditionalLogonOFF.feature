@team-mpt @sw-enrolment-off @sw-light-logon-off @bnga @nga3 @secondary @optFeature
#NGA Commercial - Device Enrolment switch is set to OFF and Device is not yet enrolled
#This feature file cover Traditional Logon process where Device enrolment switches are turned OFF and user has to authenticate via Username, password and MI

Feature: User uses traditional Logon for logging into the APP(using Username, Password and Memorable Information)
  In Order to access my accounts online
  As a Business banking user
  I want to log into my app using username, password and MI

 #MOB3-1143,MOB3-4116,MOB3-1135
  @stub-valid
  Scenario: TC_01_User should see the Password ,Memorable characters are masked and tooltip link on MI page
    Given I am a MULTIPLE_BUSINESS and FULL_SIGNATORY user
    When I enter my username and password
    Then I should see the password field is masked
    And I select continue button on login screen
    When I enter 2 characters of MI
    Then I should see the MI information is masked
    When I select the tooltip link on the MI page
    Then I should be on the MI tooltip page

  #MOB3-2491,MOB3-12510
  @stub-inactive @stubOnly
  Scenario: TC_02_Login User’s mandate is inactive
    Given I am a inactive user
    When I login with my username and password
    Then I should be on inactive error page
    When I click on Logon to Mobile Banking button in the error page
    Then I should be on the environment selector page
