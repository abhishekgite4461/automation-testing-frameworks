@team-payments @nga3 @bnga @stub-enrolled-valid @ff-share-payment-receipt-on @optFeature @sw-enrolment-on @sw-light-logon-on
Feature: Share Payment Receipt
  As a RNGA authenticated user
  I want to share the payment success receipt using the compatible apps on my device

  # App is logging off for FULL Delegate in STUB. Will make it active once log off is resolved.
  Scenario Outline: TC_01 Able to share the Payment Receipt using any of the app via native share panel
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I am on the payment hub page
    And I choose the from account as CURRENT and to beneficiary as uKBank in the payment hub page
    When I enter 1.00 in the amount field in the payment hub page
    And I select calendar picker option for future date payment
    And I submit future date payment with date as <days> days after today
    And I select the confirm option in the review payment page
    And I enter a correct password
    Then I am shown Payment Successful page
    And I should see share receipt option on the payment success page
    And I select share receipt icon on payment success screen
    And I should see the Bank Icon at the top of the screen
    And I should see the first 4 digits of the recipient account number is masked
    And I should see the date value on the share preview screen
    And I should see the date value on the share preview screen
    When I select Share on the Share Preview screen
    Then I should see the native OS share panel
    Examples:
      | accessLevel          | days |
      | FULL_SIGNATORY       |  0   |
#      | FULL_ACCESS_DELEGATE |  0   |

  # Its manual, since we have to verify the Image and Text from the Other User's end
  @manual
  Scenario Outline: TC_02 Verify the share Receipt from the end of other User
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I am on the payment hub page
    And I submit the payment with from account CURRENT and to beneficiary uKBank from beneficiary list with amount 1.00 and reference
    When I select the confirm option in the review payment page
    Then I am shown Payment Successful page
    When I select the share option on the payment success page
    And I select Share on the Share Preview screen
    Then I should see the native OS share panel
    When I select the gmail app on the share panel
    Then I should be able to share the Payment receipt to the concerned person along with the pre-defined text
    And the receiver should get both the text and image of the payment
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
