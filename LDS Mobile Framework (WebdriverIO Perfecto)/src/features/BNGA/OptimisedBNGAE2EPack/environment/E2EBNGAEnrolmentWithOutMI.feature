@team-mpt @nga3 @bnga @sw-enrolment-on @sw-light-logon-on @sw-data-consent-on @optFeature
Feature: E2E pack withOut MI and incorrect passcode scenarios for single and multiple businesses.

  @stub-card-reader-identify-multiple-businesses @exhaustiveData
  Scenario Outline: TC_001_Main Success Scenario: User(without MI setup & access to multiple business) successfully create MI and enrol their business app using Card Reader
    Given I am a MULTIPLE_BUSINESS and FULL_SIGNATORY user
    Then I am on card reader identify screen
    When I provide valid card number and passcode in the the card reader identify screen
    Then I am on Create Memorable Info page
    When I create my memorable information
    Then I should be shown card reader respond screen
    And I verify last five pre-populated card digits is non-editable in the card reader respond screen
    When I provide valid passcode in the card reader respond screen
    And I select continue button on the enrolment confirmation page
    Then I verify the default time in the security auto logout settings page
    When I choose my security auto logout settings
    Then I should see switch business screen with <accessLevel> business
    When I select a business from the business selection screen
    Then I should be on the home page
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |

  @manual
  Scenario: TC_003_Additional Scenario: Verify unique device alias name in IBC
    Given I am a MULTIPLE_BUSINESS user
    And I am on card reader identify screen
    And I provide valid card number and passcode in the the card reader identify screen
    When I create my memorable information
    Then the system should create unique device alias name
