@team-aov @nga3 @bnga @sw-enrolment-on @sw-light-logon-on @sw-data-consent-on @optFeature @stub-enrolled-valid
Feature: E2E pack for Homepage scenarios

  Scenario: TC_001_Verify the current and saving account details on the account's tile displayed on homepage
    Given I am on home screen as MULTIPLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I should see switch business icon on home page
    And I navigate and validate the options in the CURRENT account in below table in the home page
      | accountFields      |
      | businessName       |
      | accountNumber      |
      | sortCode           |
      | accountBalance     |
      | accountInfoBalance |
    When I select the action menu of CURRENT account
    Then I should see the below options in the action menu of the CURRENT account
      | optionsInActionMenu      |
      | viewTransactionsOption   |
      | transferAndPaymentOption |
      | chequeDepositOption      |
      | standingOrderOption      |
      | directDebitOption        |
      | viewPendingPaymentOption |
    And I close the action menu in the home page
    And I navigate and validate the options in the SAVINGS account in below table in the home page
      | accountFields  |
      | businessName   |
      | accountNumber  |
      | sortCode       |
      | accountBalance |
    When I select the action menu of SAVINGS account
    Then I should see the below options in the action menu of the SAVINGS account
      | optionsInActionMenu      |
      | viewTransactionsOption   |
      | transferAndPaymentOption |
      | chequeDepositOption      |
      | viewPendingPaymentOption |
      | viewInterestRateOption   |
    And I close the action menu in the home page

  Scenario Outline: TC_002_Verify the credit card account details on the account's tile displayed on homepage
    Given I am on home screen as MULTIPLE_BUSINESS business user with CREDITCARD account and <accessLevel> access
    And I navigate and validate the options in the CREDITCARD account in below table in the home page
      | accountFields            |
      | creditCardAccountNumber  |
      | creditCardAccountBalance |
      | availableCredit          |
    When I select the action menu of CREDITCARD account
    Then I should see the below options in the action menu of the CREDITCARD account
      | optionsInActionMenu    |
      | viewTransactionsOption |
    When I close the action menu in the home page
    And I navigate and validate the options in the CHARGECARD account in below table in the home page
      | accountFields            |
      | creditCardAccountNumber  |
      | creditCardAccountBalance |
      | availableCredit          |
    And I select the action menu of CHARGECARD account
    Then I should see the below options in the action menu of the CHARGECARD  account
      | optionsInActionMenu    |
      | viewTransactionsOption |
    And I close the action menu in the home page
    Examples:
      | accessLevel        |
      | FULL_SIGNATORY     |
      | VIEW_ONLY_DELEGATE |

  @stubOnly
  #DPL-2172
  # Bug raised for viewInterestRateOption option being missing under options #STAMOB-1058
  Scenario Outline: TC003_Verify the 32 day notice account details on the account's tile displayed on homepage
    Given I am on home screen as MULTIPLE_BUSINESS business user with TREASURY_32DAYNOTICE account and <accessLevel> access
    When I navigate and validate the options in the TREASURY_32DAYNOTICE account in below table in the home page
      | accountFields                   |
      | businessName                    |
      | thirtyTwoDayNoticeAccountNumber |
      | accountBalance                  |
    And I select the action menu of TREASURY_32DAYNOTICE account
    Then I should see the below options in the action menu of the TREASURY_32DAYNOTICE account
      | optionsInActionMenu    |
      | viewTransactionsOption |
      # | viewInterestRateOption   |
    Examples:
      | accessLevel          |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  @stubOnly
  #DPL-2173
  Scenario Outline: TC004_Verify the fixed term deposit account details on the account's tile displayed on homepage
    Given I am on home screen as MULTIPLE_BUSINESS business user with FIXED_TERM_DEPOSIT account and <accessLevel> access
    When I navigate and validate the options in the FIXED_TERM_DEPOSIT account in below table in the home page
      | accountFields  |
      | businessName   |
      | accountBalance |
    And I select the action menu of FIXED_TERM_DEPOSIT account
    Then I should see the below options in the action menu of the FIXED_TERM_DEPOSIT account
      | optionsInActionMenu    |
      | viewTransactionsOption |
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |

  Scenario: TC_005_Verify tabs on bottom navigation bar for all 3 access levels
    Given I am on home screen as MULTIPLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When bottom navigation bar is displayed
    Then I should see the below options in the bottom nav
      | optionsInBottomNav |
      | home               |
      | apply              |
      | payAndTransfer     |
      | support            |
      | more               |
    When I select apply tab from bottom navigation bar
    Then I should be on the product hub page
    When I select the Payment Hub option in the bottom navigation
    Then I should be on the payment hub page
    And I select dismiss modal using close button
    When I select support tab from bottom navigation bar
    Then I should be on the support hub page
    When I select more menu
    Then I should be on more menu
    And I select home tab from bottom navigation bar
    When I select switch business option on home page
    And I select Go Go Pizza business with FULL_ACCESS_DELEGATE access from the business selection screen
    Then I should be on the home page
    And bottom navigation bar is displayed
    And I should see the below options in the bottom nav
      | optionsInBottomNav |
      | home               |
      | apply              |
      | payAndTransfer     |
      | support            |
      | more               |
    When I select apply tab from bottom navigation bar
    Then I should see not authorised to apply for products error page
    When I select the Payment Hub option in the bottom navigation
    Then I should be on the payment hub page
    And I select dismiss modal using close button
    When I select support tab from bottom navigation bar
    Then I should be on the support hub page
    When I select more menu
    Then I should be on more menu
    And I select home tab from bottom navigation bar
    When I select switch business option on home page
    And I select Apollo Property Limited business with VIEW_ONLY_DELEGATE access from the business selection screen
    Then I should be on the home page
    And bottom navigation bar is displayed
    And I should see the below options in the bottom nav
      | optionsInBottomNav |
      | home               |
      | apply              |
      | payAndTransfer     |
      | support            |
      | more               |
    When I navigate to CURRENT account on home page
    And I select the action menu of CURRENT account
    Then I should not see Transfer and payment option in the action menu
    And I should not see deposit cheque option in action menu
    When I close the action menu in the home page
    And I select apply tab from bottom navigation bar
    Then I should see not authorised to apply for products error page
    When I select the Payment Hub option in the bottom navigation
    Then I must see payments hub unavailable error message page
    And I select dismiss modal using close button
    When I select support tab from bottom navigation bar
    Then I should be on the support hub page
    When I select more menu
    Then I should be on more menu

  @stubOnly
  Scenario Outline: TC_006_Verify the credit card with overdue amount on the account's tile displayed on homepage
    Given I am on home screen as MULTIPLE_BUSINESS business user with CREDITCARD account and <accessLevel> access
    And I navigate and validate the options in the CREDITCARD account in below table in the home page
      | accountFields |
      | overdueAmount |
    And I navigate and validate the options in the CHARGECARD account in below table in the home page
      | accountFields |
      | overdueAmount |
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  Scenario Outline: TC_004_ User switches business and is displayed relevant Business Name on homepage account tiles
    Given I am on home screen as MULTIPLE_BUSINESS business user with <accountType> account and <accessLevel1> access
    When I select switch business option on home page
    And I select <business1> business with <accessLevel1> access from the business selection screen
    Then I verify <business1> name on the account tiles in the homepage
    And I select switch business option on home page
    When I select <business2> business with <accessLevel2> access from the business selection screen
    Then I verify <business2> name on the account tiles in the homepage
    Examples:
      | accountType | accessLevel1   | business1                     | accessLevel2         | business2   |
      | CURRENT     | FULL_SIGNATORY | Equity Investment Instruments | FULL_ACCESS_DELEGATE | Go Go Pizza |
