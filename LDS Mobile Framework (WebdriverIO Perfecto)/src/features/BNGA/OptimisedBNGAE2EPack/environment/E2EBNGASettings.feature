@team-mpt @nga3 @bnga @stub-enrolled-valid @optFeature @sw-enrolment-on @sw-light-logon-on @sw-touch-id-on @sw-fingerprint-android-on
Feature: BNGA user views settings menu
  In order to view and change my personal, business and security settings
  As a BNGA user
  I want to view my settings menu

  Scenario Outline: TC-001 - Main Success Scenario - User wants to view their settings menu options
    Given I am on home screen as MULTIPLE_BUSINESS business user with CURRENT account and <accessLevel> access
    When I navigate to settings page via more menu
    Then I should see the below options in the settings page
      | optionsInSettingsPage  |
      | personalDetailsButton  |
      | businessDetailsButton  |
      | securitySettingsButton |
      | resetAppButton         |
      | legalInfo              |
    When I select the legal info option from the settings page
    Then I am on the legal information page
    And I navigate back
    When I select your personal details option in settings page
    Then I should be shown the list of personal details
      | personalDetail |
      | Full Name      |
      | User ID        |
      | Mobile Number  |
      | Home Number    |
      | Work Number    |
     # | Work Extension Number |
    And I navigate back
    When I select the your Business Details option in settings page
    Then I should be shown the list of business details
      | businessDetail                 |
      | business Name                  |
      | business RegisteredAddressText |
      | business Address               |
      | business Postcode              |
    And I navigate back
    When I select the security settings option from the settings page
    Then I should see the security settings page
    And I should see the following options in security details page
      | securityDetailsOptions  |
      | User ID                 |
      | App version             |
      | Device name             |
      | Device type             |
      | Forgotten your password |
      | Auto logoff             |
      | Finger Print/Touch id   |
    And I select the forgotten password option from the security settings page
    And I should be on the password reset page
    And I navigate back
    When I select the autoLog off settings option from the security settings page
    Then I should be on the autoLog off settings page
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |
