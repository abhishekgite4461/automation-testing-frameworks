@team-mpt @nga3 @bnga @sw-enrolment-on @sw-light-logon-on @sw-data-consent-on @optFeature @sw-fingerprint-android-on @sw-touch-id-on
Feature: E2E pack with Enrolment, settings, reset-password and de-enrolment the app

  @stub-single-business-full-signatory-unenrolled
  # Default time needs to be verified manually
  Scenario Outline: TC_001_Main Success Scenario: User Successfully enrols and de-enrols the app_Single business user
    Given I reinstall the app
    And I am on MI screen as SINGLE_BUSINESS business user with CURRENT account and <accessLevel> access
    And I enter correct MI
    When I verify card number and enter passcode in the card reader respond screen
    Then I should see contact us option in enrolment confirmation page
    When I select continue button on the enrolment confirmation page
    Then I should see contact us option in the security auto logout settings page
    # And I verify the default time in the security auto logout settings page
    When I choose my security auto logout settings
    And I opt out for android fingerprint from the interstitial
    Then I should be on the home page
    When I navigate to legal info from settings page via more menu
    Then I should see the following links on legal info page
      | fieldsOnLegalInfoScreen    |
      | legalInformationText       |
      | legalAndPrivacy            |
      | cookieUseAndPermissions    |
      | thirdPartyAcknowledgements |
    When I navigate to settings page via more menu
    Then I should not see switch business icon on More screen
    When I select the reset app option from the settings page
    Then I should be on the reset app Page
    When I select reset app button from the reset app page
    And I am shown winback with Cancel and Ok option
    And I select confirm button from the reset app confirmation pop up
    Then I should be on the logout page
    And I should be displayed a log-off complete message - HC030
    Examples:
      | accessLevel    |
      | FULL_SIGNATORY |

  @stub-valid
  Scenario Outline: TC_002_Main Success Scenario: User(with MI setup & access to multiple business) successfully enrols de-enrols the app_Multiple business user
    Given I reinstall the app
    And I am on MI screen as MULTIPLE_BUSINESS business user with CURRENT account and <accessLevel> access
    When I enter correct MI
    Then I should be shown card reader respond screen
    When I select the contact us option from the card reader respond page pre auth header
    Then I should be on pre auth call us home page overlay
    And I select dismiss modal using close button
    And I should see 6 digit challenge code on the card reader respond screen
    And I should be prompted to enter the 8 digit passcode in card reader respond screen
    And I should see the continue button is disabled in card reader respond screen
    When I have entered the last 5 digits of valid card number in the card reader respond page
    Then I verify card number and enter passcode in the card reader respond screen
    When I select continue button on the enrolment confirmation page
    And I choose my security auto logout settings
    And I opt out for android fingerprint from the interstitial
    Then I should see business selection screen
    When I select first business of <accessLevel> from selection screen
    Then I should be on the home page
    When I navigate to settings page via more menu
    And I select the reset app option from the settings page
    Then I should be on the reset app Page
    When I successfully reset the app
    Then I should be on the logout page
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |

   # --------------- Light logon scenarios ------------

  @environmentOnly
  Scenario Outline: TC_003 Main Success Scenario : User having access to multiple business successfully login to enrolled business app using seamless logon i.e. via providing their memorable information
    Given I reinstall the app
    And I am on home screen as MULTIPLE_BUSINESS business user with CURRENT account and <accessLevel> access
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I relaunch the app
    And I select the environment
    And I enter incorrect MI
    Then I should see incorrect MI error message in re-enter MI page
    When I enter correct MI in re-enter MI page
    And I select the consentOptedIn button for the performancePrefs section
    And I confirm my selection on the data consent page
    Then I should see business selection screen
    When I select first business of <accessLevel> from selection screen
    Then I should be on the home page
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |

    #------------------- Negative and Exception Scenarios------------------

  @exhaustiveData @stub-business-banking @stubOnly
  Scenario: TC_004_Exception Scenario : User logs into business app to enrol new device and user has already reached maximum number of enrolled device count i.e. 5(MG-2157)
    Given I am a MULTIPLE_BUSINESS user
    When I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    Then I should be shown an error page logged out with error message for maximum number of devices

  @manual
  Scenario: TC_005_Exception_Scenario: Enrolled User has exceeded maximum incorrect authentication attempts to enter MI(MG-400 & MG-401)
    Given I am a CURRENT user
    When I enter incorrect MI
    Then I should be on suspended error page

  @manual
  Scenario: TC_006_Exception_Scenario: User enter incorrect MI and has not yet reached maximum incorrect attempts(MG-400)
    Given I am a SINGLE_BUSINESS user
    And I login with my username and password
    When I enter incorrect MI
    Then I should see incorrect MI error message in re-enter MI page
    When I enter correct MI in re-enter MI page
    And I enter incorrect password in confirm password page
    Then I should see incorrect password error message in re-enter MI page
    When I enter correct MI in re-enter MI page
    And I enter correct password in confirm password page
    And I verify card number and enter passcode in the card reader respond screen
    Then I should be on the home page

  @manual
  Scenario Outline: TC_007_Security_Scenario : User Login to Business app in jailbroken/rooted mobile device
    Given I am a "<userType>" user
    When I login with my username and password
    Then I should be on jailbroken/rooted error page
    Examples:
      | userType               |
      | Malware                |
      | Tampered               |
      | Jailbroken/Rooted      |
      | Data on App/OS Version |

  @manual
  Scenario: TC008_Customer should be displayed email form after selecting app feedback
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I select support tab from bottom navigation bar
    When I select "provide app feedback" option in the support hub page
    Then I should be displayed a feedback form

  # Marked as exhaustive since 8 digit incorrect passcode may corrupt the test data
  @exhaustiveData @stub-card-reader-respond-incorrect-passcode
  Scenario Outline: TC_009_Incorrect Passcode validation on Card Reader Respond page for multiple business user
    Given I am on MI screen as MULTIPLE_BUSINESS business user with CURRENT account and <accessLevel> access
    When I enter correct MI
    Then I should be shown card reader respond screen
    And I should see 6 digit challenge code on the card reader respond screen
    And I should be prompted to enter the 8 digit passcode in card reader respond screen
    And I should see the continue button is disabled in card reader respond screen
    When I have entered the last 5 digits of valid card number in the card reader respond page
    And I enter the incorrect 8 digit passcode from the card reader in the card reader respond page
    And I select the continue button on the card reader Identify or respond page
    Then I should be shown incorrect passcode error banner in the card reader respond page
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
