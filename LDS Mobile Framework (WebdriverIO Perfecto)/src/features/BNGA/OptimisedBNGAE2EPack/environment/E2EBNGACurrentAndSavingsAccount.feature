@team-statements @nga3 @bnga @stub-enrolled-valid @optFeature @ff-pca-alltab-enabled-off @sw-enrolment-on @sw-light-logon-on @sw-touch-id-on @sw-fingerprint-android-on
@environmentOnly @ios
  # separate feature file is created for android for all the scenarios with condensed account tile changes.
  # Will be common scenarios for both once condensed account tile is implemented in iOS
Feature: E2E BNGA Current and Savings Account Statements
  As a BNGA authenticated customer with current & savings accounts,
  I want to be able to view statements for the nominated accounts & months

  #STAMOB-1027 Multiple business user while clicking on account tile app is getting logged off
  Scenario Outline: TC_001_User views current month and previous months transactions for current and savings account
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I select <type> account tile from home page
    And I view <month> transactions of <type> account
    Then I should be displayed <month> statements transactions
    And I should see following fields in statements list
      | fieldsInStatementsList         |
      | firstTranscationDate           |
      | firstTranscationDescription    |
      | firstTranscationAmount         |
      | firstTransactionRunningBalance |
    And I should scroll down to the end of the transactions to view <output> message of that <month> tab
    Examples:
      | type    | month    | output        |
      #| CURRENT | allTab   | noCurrentInOuts |
      | CURRENT | current  | currentInOuts |
      | CURRENT | previous | currentInOuts |
      | SAVINGS | current  | savingEndOf   |
      | SAVINGS | previous | savingEndOf   |

  #DPL-945, DPL-947
  Scenario Outline: TC-002 User views dormant account message
    Given I am on home screen as FULL_SIGNATORY access user with <type> account
    And I select <type> account tile from home page
    Then I should be displayed a message
    And I should not see any transactions
    Examples:
      | type            |
      | CURRENT_DORMANT |
      | SAVINGS_DORMANT |
