@team-statements @nga3 @bnga @stub-enrolled-valid @optFeature @sw-enrolment-on @sw-light-logon-on @environmentOnly
Feature: BNGA User views Call Us home page
  In order that I can contact the bank
  As a BNGA user
  I want to see the option to contact the bank

  Scenario Outline: TC_001 Main Success Scenario: User views Business Account call us page when logged into the app
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I navigate to call us home page
    Then I should be able to view these call us tiles
      | callUsTiles           |
      | businessAccountsTile  |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should not see security and travel emergency label
    And I should not see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
      | medicalAssistAbroadTab |
    When I select the <option> tile on call us home page
    Then I am displayed the call us <page> for the selected option
    And I am able to view the call us button
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    Examples:
      | option              | page                |
      | Business accounts   | business Accounts   |
      | Internet banking    | internet Banking    |
      | Other banking query | other Banking Query |
