@team-pi @nga3 @bnga @lds @pending @manualOnly
Feature: Customer is displayed with three options for payments in action menu.
  As a Product Owner
  I want to see which variation of the action menu performs best in the BNGA Lloyds A/B test.
  so that I have insight into the results of changing the respective entry point label

  #These scenarios are manual only since there is an interaction between 3rd party provider Adobe Target.
  #Purpose: To set up an AB test in the actions menu on Lloyds BNGA

  Background:
    Given I am a Lloyds BNGA customer
    And I am eligible for the BNGA actions menu AB test

  #PIDI-1847, PIDI-1848, PIDI-1853, PIDI-1854

  @primary
  Scenario:AC01 - Variant A 'Payments and transfers'
    When I am part of variant group A
    And I log in to the mobile app
    And I select the actions menu on my business account
    Then the second option should read as 'Payments and transfers'

  @primary
  Scenario:AC02 - Variant B 'Transfer and pay'
    When I am part of variant group B
    And I log in to the mobile app
    And I select the actions menu on my business account
    Then the second option should read as 'Transfer and pay'

  @primary
  Scenario:AC03 - Variant C 'Make a payment'
    When I am part of variant group C
    And I log in to the mobile app
    And I select the actions menu on my business account
    Then the second option should read as 'Make a payment'

  @primary
  Scenario:AC04 - Default experience
    When I am not part of any AB test
    And I log in to the mobile app
    And I select the actions menu on my business account
    Then the second option should read as 'Payments and transfers'

  @secondary
  Scenario:AC05 - Interactions for 'Payments and transfers'
    When I am part of variant group A
    And I log in to the mobile app
    And I select'Payments and transfers' in the actions menu on my business account
    Then an interaction should be recorded

  @secondary
  Scenario:AC06 - Interactions for 'Transfer and pay'
    When I am part of variant group B
    And I log in to the mobile app
    And I select 'Transfer and pay' in the actions menu on my business account
    Then an interaction should be recorded

  @secondary
  Scenario:AC07 - Interactions for 'Make a payment'
    When I am part of variant group C
    And I log in to the mobile app
    And I select 'Make a payment' in the actions menu on my business account
    Then an interaction should be recorded

  @secondary
  Scenario:AC08 - Impressions for 'Payments and transfers'
    When I am part of variant group A
    And I land on the actions menu on my business account
    Then an impression should be recorded

  @secondary
  Scenario:AC09 - Impressions for 'Transfer and pay'
    When I am part of variant group B
    And I land on the actions menu on my business account
    Then an impression should be recorded

  @secondary
  Scenario:AC10 - Impressions for 'Make a payment'
    When I am part of variant group C
    And I land on the actions menu on my business account
    Then an impression should be recorded
