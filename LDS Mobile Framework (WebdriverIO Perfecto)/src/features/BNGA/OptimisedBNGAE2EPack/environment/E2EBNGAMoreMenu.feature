@team-mpt @nga3 @bnga @sw-enrolment-on @sw-light-logon-on @sw-data-consent-on @optFeature
Feature: User wants to access services/ sales journey using a menu
  In Order to efficiently carry out my everyday banking needs with ease
  As a Business App user
  I want to easily use and discover functionality that is provided by the app through more menu

  # MOB3-10203, #MPT-671
  @stub-enrolled-valid
  Scenario: TC_001_ Main Success: Validate more menu options displayed for a multiple businesses user (All 3 access levels)
    Given I am on home screen as MULTIPLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I select more menu
    Then I should see the below options in the more menu
      | optionsInMorePage  |
      | moreSwitchBusiness |
      | yourProfile        |
      | settings           |
      | chequeScanning     |
      | depositCheque      |
      | viewDepositHistory |
      | logout             |
    And  I select switch business icon on more menu
    When I select Go Go Pizza business with FULL_ACCESS_DELEGATE access from the business selection screen
    And I select more menu
    Then I should see the below options in the more menu
      | optionsInMorePage  |
      | moreSwitchBusiness |
      | yourProfile        |
      | settings           |
      | chequeScanning     |
      | depositCheque      |
      | viewDepositHistory |
      | logout             |
    And  I select switch business icon on more menu
    When I select Apollo Property Limited business with VIEW_ONLY_DELEGATE access from the business selection screen
    And I select more menu
    Then I should see the below options in the more menu
      | optionsInMorePage  |
      | moreSwitchBusiness |
      | yourProfile        |
      | settings           |
      | chequeScanning     |
      | viewDepositHistory |
      | logout             |

  # MOB3-11767 , one-level access
  @stub-single-business-full-delegate-enrolled
  Scenario Outline: TC_003_ Main Success: Validate more menu options displayed for a single business user with full signatory and full delegate businesses
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and <accessLevel> access
    When I select more menu
    And I should not see switch business icon on More screen
    Then I should see the below options in the more menu
      | optionsInMorePage  |
      | yourProfile        |
      | settings           |
      | chequeScanning     |
      | depositCheque      |
      | viewDepositHistory |
      | logout             |
    Examples:
      | accessLevel          |
      | FULL_ACCESS_DELEGATE |

  @stub-single-business-view-only-enrolled
  Scenario Outline: TC_004_ Main Success: Validate more menu options displayed for a single business user with view only business
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and <accessLevel> access
    When I select more menu
    And I should not see switch business icon on More screen
    Then I should see the below options in the more menu
      | optionsInMorePage  |
      | yourProfile        |
      | settings           |
      | chequeScanning     |
      | viewDepositHistory |
      | logout             |
    Examples:
      | accessLevel        |
      | VIEW_ONLY_DELEGATE |

  # MOB3-10203
  @stub-enrolled-valid
  Scenario Outline: TC_004_ User switches business and is displayed relevant Business Name
    Given I am on home screen as MULTIPLE_BUSINESS business user with <accountType> account and <accessLevel1> access
    And I select more menu
    Then I verify name of the <business1> on the more menu
    And  I select switch business icon on more menu
    When I select <business2> business with <accessLevel2> access from the business selection screen
    And I select more menu
    Then I verify name of the <business2> on the more menu
    Examples:
      | accountType | accessLevel1   | business1                     | accessLevel2         | business2   |
      | CURRENT     | FULL_SIGNATORY | Equity Investment Instruments | FULL_ACCESS_DELEGATE | Go Go Pizza |
