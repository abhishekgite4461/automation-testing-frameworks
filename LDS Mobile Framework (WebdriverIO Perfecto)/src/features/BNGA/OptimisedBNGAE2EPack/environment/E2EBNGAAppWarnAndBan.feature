@team-delex @nga3 @bnga @optFeature @appWarnBan @sit02Job
Feature: E2E_Verfiy app warn and ban screens for non supported Commercial app versions

  #Marked @pending, as no commercial app warned fas part of Business release 20.06
  @android @pending
  Scenario Outline: Verify app Warn Screens for single business enrolled user
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I relaunch the app
    And I change an app version to <appVersion> on environment selection page
    And I select the environment
    And I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay
    And I select close button on pre auth call us home page overlay
    When I choose to update the app from the warn screen
    And I navigate to app store screen
    And I navigate back to app from app store
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to continue without updating from the warn screen
    Then I should be on enter MI page
    When I enter correct MI
    Then I should be on the home page
    When I reset the app
    Then I should be on the logout page
    Examples:
      | appVersion         |
      | 52                 |

  #Marked @pending, as no commercial app warned as part of Business release 20.06
  @android @pending
  Scenario Outline: Verify app Warn Screens for single business unenrolled user
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I reset the app
    And I relaunch the app
    And I change an app version to <appVersion> on environment selection page
    And I select the environment
    And I navigate to the login screen
    And I login with my username and password
    And I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay
    And I select close button on pre auth call us home page overlay
    When I choose to update the app from the warn screen
    And I navigate to app store screen
    And I navigate back to app from app store
    And I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to continue without updating from the warn screen
    Then I should be on enter MI page
    And I enter correct MI
    When I finish enrolment
    And I accept interstitial during logon
    Then I should be on the home page
    When I reset the app
    Then I should be on the logout page
    Examples:
      | appVersion         |
      | 52                 |

   #Marked @pending, as no commercial app warned as part of Business release 20.06
  @ios @pending
  Scenario Outline: Verify app Warn Screens for enrolled user for iOS
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I relaunch the app
    And I change an app version to <appVersion> on environment selection page
    And I select the environment
    And I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to update the app from the warn screen
    And I navigate to app store screen
    And I validate the app version <expectedAppVersion> on app store page
    And I navigate back to app from app store
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to continue without updating from the warn screen
    Then I should be on enter MI page
    When I enter correct MI
    Then I should be on the home page
    When I reset the app
    Then I should be on the logout page
    Examples:
      | appVersion         | expectedAppVersion |
      | 52.0               | 54.02              |

   #Marked @pending, as no commercial app warned as part of Business release 20.06
  @ios @pending
  Scenario Outline: Verify app Warn Screens for unenrolled user for iOS
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I reset the app
    And I relaunch the app
    And I change an app version to <appVersion> on environment selection page
    And I select the environment
    And I navigate to the login screen
    And I login with my username and password
    And I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to update the app from the warn screen
    And I navigate to app store screen
    And I validate the app version <expectedAppVersion> on app store page
    And I navigate back to app from app store
    And I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to continue without updating from the warn screen
    Then I should be on enter MI page
    And I enter correct MI
    When I finish enrolment
    And I accept interstitial during logon
    Then I should be on the home page
    When I reset the app
    Then I should be on the logout page
    Examples:
      | appVersion         | expectedAppVersion |
      | 52.0               | 54.02              |

  @android @genericAccount
  Scenario Outline: Verify app Ban Screens for single business enrolled user
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I relaunch the app
    And I change an app version to <appVersion> on environment selection page
    And I select the environment
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay
    And I select close button on pre auth call us home page overlay
    When I choose to update the app in the ban screen
    And I navigate to app store screen
    And I navigate back to app from app store
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    And I choose to use the Mobile Browser Banking
    And I validate url from mobile browser
    When I navigate back to app from mobile browser
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    Examples:
      | appVersion         |
      | 52                 |
  @android @unEnrolled
  Scenario Outline: Verify app Ban Screens for single business unenrolled user
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I reset the app
    And I relaunch the app
    And I change an app version to <appVersion> on environment selection page
    And I select the environment
    And I navigate to the login screen
    And I login with my username and password
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay
    And I select close button on pre auth call us home page overlay
    When I choose to update the app in the ban screen
    And I navigate to app store screen
    And I navigate back to app from app store
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    And I choose to use the Mobile Browser Banking
    And I validate url from mobile browser
    When I navigate back to app from mobile browser
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    Examples:
      | appVersion         |
      | 52                 |

  @ios @genericAccount
  Scenario Outline: Verify app ban Screens for enrolled user for iOS
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I relaunch the app
    And I change an app version to <appVersion> on environment selection page
    And I select the environment
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I choose to update the app in the ban screen
    And I navigate to app store screen
    And I validate the app version <expectedAppVersion> on app store page
    And I navigate back to app from app store
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I choose to use the Mobile Browser Banking
    And I navigate to mobile browser
    Then I validate url from mobile browser
    When I navigate back to app from mobile browser
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    Examples:
      | appVersion         | expectedAppVersion |
      | 39.0               | 54.02              |

  #The below scenarios are marked as '@manual', as they are OS and platform specific
  #only for Android 4.4 users
  @manual
  Scenario: Verify OS Version Warn Screens for Android 4.4 enrolled user
    Given I am enrolled commercial NGA user on Android 4.4
    And feature flag isKitKatWarnEnabled is enabled
    When I launch the app
    Then I should be shown Android OS version 4.4 warn screen
    When I choose to continue in the version warn screen
    Then I should be on enter MI page
    When I enter correct MI
    Then I should be on the home page

  #only for Android 4.4 users
  @manual
  Scenario: Verify app Warn Screens for Android 4.4 enrolled user
    Given I am enrolled commercial NGA user on Android 4.4
    And I am using commercial NGA warned app version
    And feature flag isKitKatWarnEnabled is enabled
    When I launch the app
    Then I should be shown NGA warn screen for Android 4.4
    When I choose to continue in the app warn screen
    Then I should be on enter MI page
    When I enter correct MI
    Then I should be on the home page

  #only for Android 4.4 users
  @manual
  Scenario: Verify app Ban Screens for Android 4.4 enrolled user
    Given I am enrolled commercial NGA user on Android 4.4
    And I am using commercial NGA banned app version
    And feature flag isKitKatWarnEnabled is enabled
    When I launch the app
    Then I should be shown NGA ban screen for Android 4.4
    When I choose to use the Mobile Browser Banking
    Then I should be shown mobile browser
    And I validate url/ui from mobile browser
    When I navigate back to app from mobile browser
    Then I should be shown NGA ban screen for Android 4.4
