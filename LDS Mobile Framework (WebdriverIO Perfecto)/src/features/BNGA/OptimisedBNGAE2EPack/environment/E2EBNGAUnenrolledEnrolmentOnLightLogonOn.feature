@team-mpt @nga3 @bnga @sw-enrolment-on @sw-light-logon-on @optFeature @unEnrolled @stub-card-reader-identify-incorrect-passcode @exhaustiveData
Feature: E2E pack withOut MI and incorrect passcode scenarios for single and multiple businesses.

  Scenario: TC_002_Main Success Scenario: User(without MI setup & access to multiple business) successfully create MI and enrol their business app using Card Reader
    Given I am a CURRENT user
    And I am on card reader identify screen
    When I enter the incorrect 8 digit passcode from the card reader in the card reader identify page
    And I select the continue button on the card reader Identify or respond page
    Then I should be shown incorrect passcode error banner in the card reader identify page
    And I close the incorrect passcode error banner
