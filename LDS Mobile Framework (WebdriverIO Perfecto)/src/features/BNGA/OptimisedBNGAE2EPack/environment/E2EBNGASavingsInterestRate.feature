@team-statements @nga3 @bnga @stub-enrolled-valid @optFeature @sw-enrolment-on @sw-light-logon-on @environmentOnly @defect
Feature: E2E Saving Interest Rate Details
  As a NGA Customer
  I should be able to view interest rate for my saving account
  So that I am aware of the interest rate on my saving account

  #STAMOB-1308
  Scenario: TC_001_Interest Rate validation for Savings Account
    Given I am on home screen as FULL_SIGNATORY access user with SAVINGS account
    And I navigate to SAVINGS account on home page
    And I tap and hold on the SAVINGS account tile to Savings Interest Rate Details screen
    Then I should see the following fields in the details screen
      | fieldsInDetailsScreen   |
      | accountName             |
      | accountBalance          |
      | interestRateLabel       |
      | interestRateDescription |
    When I close the interest rate details screen using the close button
    And I select View Interest Details option of SAVINGS account from action menu
    Then I should see the following fields in the details screen
      | fieldsInDetailsScreen   |
      | accountName             |
      | accountBalance          |
      | interestRateLabel       |
      | interestRateDescription |
    And I close the interest rate details screen using the close button
    When I select SAVINGS account tile from home page
    And I select the View Interest Details option from the action menu in statements page
    Then I should be on Savings Interest Rate Details screen
    And I should see the following fields in the details screen
      | fieldsInDetailsScreen   |
      | accountName             |
      | accountBalance          |
      | interestRateLabel       |
      | interestRateDescription |
    When I close the interest rate details screen using the close button
    And I navigate to logoff via more menu
    Then I should be on the logout page
