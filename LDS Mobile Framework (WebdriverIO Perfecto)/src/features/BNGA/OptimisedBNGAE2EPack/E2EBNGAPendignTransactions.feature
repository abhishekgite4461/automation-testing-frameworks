@team-statements @nga3 @bnga @stub-enrolled-valid @stubOnly @optFeature @sw-enrolment-on @sw-light-logon-on
Feature: E2E BNGA Pending Transactions
  In order to view the statements for the nominated credit card
  As a NGA authenticated customer with credit card
  I want to view my credit card transactions for each statement period

  Scenario Outline: TC_001 Main Success Scenario: User views pending transactions for credit and charge cards
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD account
    And I select <type> account tile from home page
    And I tap on the pending transactions accordion
    Then I should see expanded pending transactions accordion
    And I should see following fields in the pending transaction accordion
      | fieldsInPendingTransactionAccordion |
      | pendingTransactionTab               |
      | pendingLabel                        |
      | pendingTransactionDescription       |
      | pendingTransactionAmount            |
    Examples:
      | type                                  |
      | CREDITCARD_WITH_PENDING_TRANSACTIONS  |
      | CHARGECARD_WITH_PENDING_TRANSACTIONS  |

    #STAMOB-1027. Commented the all tab step as currently all tab is OFF due to production issue. Will uncomment all tab is enabledScenario: TC_002 Main Success Scenario: User views pending transactions for debit card
  Scenario: TC_002 Main Success Scenario: User views pending transactions for debit card
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I select CURRENT account tile from home page
#    Then I should be on the allTab of statements page
    When I tap on the pending transactions accordion
    Then I should see expanded pending transactions accordion
    And I should see following fields in the pending transaction accordion
      | fieldsInPendingTransactionAccordion |
      | pendingTransactionTab               |
      | pendingLabel                        |
      | pendingDebitTransactionDescription  |
      | pendingDebitTransactionAmount       |
      | noteText                            |
      | pendingChequeTransactionDescription |
      | pendingChequeTransactionAmount      |
    When I swipe to see previous month transactions on the account statement page
    Then I should see minimised pending transactions accordion
