@team-statements @nga3 @bnga @stub-enrolled-valid @optFeature @noExistingData @sw-enrolment-on @sw-light-logon-on @stubOnly @ios
  # separate feature file is created for android for all the scenarios with condensed account tile changes.
  # Will be common scenarios for both once condensed account tile is implemented in iOS
Feature: Credit and Charge card statements
  In order that I can view my statement balance, payment due on my mobile device
  As a BNGA user
  I want to see my business card account(s) statement summary

  Scenario Outline: TC_001 Main Success Scenario: User views recent transactions for credit cards and charge cards
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD account
    When I select <type> account tile from home page
    Then I should be displayed recent transactions with following fields
      | fieldsInRecentTransaction  |
      | nextStatementDueDate       |
      | optionToFilterTransactions |
      | transactionsSinceMessage   |
    When I filter the transactions to view <typeFilter> transaction for recent
    Then I should see following fields for <typeFilter> in statements summary
      | fieldsInStatementsSummary |
      | clearFilter               |
      | <typeFilterTransactions>  |
    And I should be displayed <total> for <typeFilter> transactions
    And I should be displayed the <typeFilter> transactions for the month selected
    When I clear the filter
    Then I should see following fields in statements list
      | fieldsInStatementsList        |
      | firstTranscationDate          |
      | firstTranscationDescription   |
      | transactionType               |
      | firstTranscationAmount        |
    And I should scroll down to the end of the transactions to view <output> message
    When I filter the transactions to view <typeFilter> transaction for previous
    Then I should see following fields for <typeFilter> in statements summary
      | fieldsInStatementsSummary |
      | clearFilter               |
      | <typeFilterTransactions>  |
    And I should be displayed <total> for <typeFilter> transactions
    And I should be displayed the <typeFilter> transactions for the month selected
    When I clear the filter
    Then I should be displayed last month statement with the following fields in statements summary
      | fieldsInStatementsSummary   |
      | statementDateDescription    |
      | statementBalanceDescription |
      | minimumPaymentDescription   |
      | dueDateDescription          |
      | optionToFilterTransactions  |
    And I should see following fields in statements list
      | fieldsInStatementsList        |
      | firstTranscationDate          |
      | firstTranscationDescription   |
      | transactionType               |
      | firstTranscationAmount        |
    And I should scroll down to the end of the transactions to view <output> message
    Examples:
      | type         | output         | typeFilter  | typeFilterTransactions | total    |
      | CREDITCARD   | creditEndOf    | cardEnding  | typeCardFilter         | cardEndingTransactionTotal    |
      | CREDITCARD   | creditEndOf    | account     | typeAccountFilter      | nocardEndingTransactionTotal   |
      | CHARGECARD   | chargeEndOf    | cardEnding  | typeCardFilter         | cardEndingTransactionTotal   |
      | CHARGECARD   | chargeEndOf    | account     | typeAccountFilter      | nocardEndingTransactionTotal |
