@team-login @nga3 @bnga @sw-enrolment-on @sw-light-logon-on @manual @optFeature @primary

Feature: As an BNGA User I should be able to login via fingerprint and
  I want to be informed of any fingerprint change in my device
  so that I can decide to login into APP using biometric prompt or not

#  Precondition:
#  Device is bio-metric compatible
#  NGA Retail/Business - Device Enrollment switch is ON
#  NGA Retail/Business - Light Logon switch is ON
#  Bio-metric switch is ON
#  ff isBiometricOptInRepromptEnabled is ON

  Scenario:TC_01 User clicks on No button on Before we confirm popup, during enrollment journey
    Given I am a multiple business with MI user
    And I provide my correct username, password and MI
    And I verify card number and enter passcode in the card reader respond screen
    And I select continue button on the enrolment confirmation page
    Then I verify the default time in the security auto logout settings page
    When I choose my security auto logout settings
    And I click 'Turn ON Bio' button on biometric screen
    And I see before we confirm popup with yes and no buttons
    And I click No button
    Then I should see Bio not turned on popup
    And I should get re-prompted after n number of days
    When I click on Ask me later button
    Then I should see business selection screen with available business
    And I should be navigated to homepage

  Scenario:TC_02 User clicks on No button on Before we confirm popup, upon logon
    Given I am an enrolled "single business" user on the MI page
    When I enter correct MI
    And I see biometric screen
    And I click 'Turn ON Bio' button on bio-metric screen
    And I see 'Before we confirm' popup with yes and no buttons
    And I click No button
    Then I should see 'Bio not turned on' popup
    And I should get re-prompted after n number of days
    When I click on Ask me later button
    And I should be on the home page

  Scenario:TC_03 User clicks on No button on Before we confirm popup, in Settings(Security) journey
    Given I am on home screen as multiple business user with current account and <any> access
    When I navigate to settings page via more menu
    And I select security settings from settings menu
    And I toggle 'Sign in with bio' button to ON
    And I see 'Before we confirm' popup with Yes and No buttons
    And I click on No button
    Then I should see Bio not turned On popup
    And I should get re-prompt only if re-prompt state was stored while enrollment/logon
