@team-core @team-login @sw-enrolment-on @sw-light-logon-on @bnga @nga3 @pending
#This feature file cover Express Logon process where Device is fully enrolled and Light Logon switch is turned ON

Feature: Enrolled User performs seamless logon(Express Logon)
         In Order to access my accounts using seamless logon
         As a Business banking user
         I want to log into my app using Memorable information or Biometrics

#------------------------------------------- Main Success scenario---------------------------#

  @primary
  Scenario: TC_001 Main Success Scenario : User having access to single business successfully login to enrolled business app using seamless logon i.e. via providing their memorable information
    Given I am an enrolled "single business" user on the MI page
    When I enter correct MI
    Then I should be on the home page

  @primary
  Scenario: TC_002 Main Success Scenario : User having access to multiple businesses successfully login to enrolled business app using seamless logon i.e. via providing their memorable information
    Given I am an enrolled "multiple business" user on the MI page
    When I enter correct MI
    Then I should see business selection screen

  @manual
  Scenario: TC_003 Main Success Scenario : User having access to single business logs in  successfully using Touch ID/Android fingerprint
    Given I am an enrolled "single business" user opted in for Touch ID
    And I have opted in for Touch id on this device
    When I launch BNGA app
    Then I should be displayed Touch ID login prompt
    When I authenticate using correct Fingerprint
    Then i should be displayed authenticating using fingerprint message
    And I should be on the home page

  @manual
  Scenario: TC_004 Main Success Scenario : User having access to multiple businesses logs in using Touch ID/Android fingerprint
    Given I am an enrolled "multiple business" user opted in for Touch ID
    And I have opted in for Touch id on this device
    When I launch BNGA app
    Then I should be displayed Touch ID login prompt
    When I authenticate using correct Fingerprint
    Then i should be displayed authenticating using fingerprint message
    And I should see business selection screen
