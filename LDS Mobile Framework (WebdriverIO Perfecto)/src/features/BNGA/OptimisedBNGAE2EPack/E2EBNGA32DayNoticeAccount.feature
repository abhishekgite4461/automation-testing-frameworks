@team-statements @nga3 @bnga @stub-enrolled-valid @stubOnly @optFeature
Feature: 32 Day Notice Account
  In order that I can am aware of my 32 day notice accounts
  As a BNGA authenticated user,
  I want to be able to view the details of my 32 day notice accounts

  Scenario Outline: TC_001 Main Success Scenario: User views details of 32 day notice account with one/multiple settlement account
    Given I am on home screen as <accessLevel> access user with <type> account
    When I select <type> account tile from home page
    Then I should be displayed with <field> in statements summary
    And I should be displayed the 32 day notice account page with the following details
      | fieldsInTreasuryAccount  |
      | aerGrossInterestRate     |
      | basicTaxRateStatus       |
      | balanceLastUpdated       |
      | aerInfoLink              |
      | nominatedAccountInfoLink |
    Examples:
      | accessLevel          | type                                 | field                            |
      | FULL_SIGNATORY       | TREASURY_32DAYNOTICE                 | nominatedAccount                 |
      | FULL_ACCESS_DELEGATE | TREASURY_32DAYNOTICE                 | nominatedAccount                 |
      | VIEW_ONLY_DELEGATE   | TREASURY_32DAYNOTICE                 | nominatedAccount                 |
      | FULL_SIGNATORY       | TREASURY_32DAYNOTICE_WITH_SETTLEMENT | multipleSettlementAccountTextMsg |
      | FULL_ACCESS_DELEGATE | TREASURY_32DAYNOTICE_WITH_SETTLEMENT | multipleSettlementAccountTextMsg |
      | VIEW_ONLY_DELEGATE   | TREASURY_32DAYNOTICE_WITH_SETTLEMENT | multipleSettlementAccountTextMsg |
