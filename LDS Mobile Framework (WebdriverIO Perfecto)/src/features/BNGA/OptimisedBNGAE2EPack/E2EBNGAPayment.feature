@team-payments @nga3 @bnga @sw-enrolment-on @sw-light-logon-on @optFeature

Feature: Payment

  # Commented below lines in the script due to IBC limit is keep on changing
  # Adding 30 second wait for environment execution to allow time for switch toggling
  @sanity @environmentOnly
  Scenario Outline:TC_001_Main Success Scenario: Verify amount field decimal validation, maximum available balance and perform success payment journey using password
    Given I wait for 30 seconds
    And I am on home screen as MULTIPLE_BUSINESS business user with CURRENT account and <accessLevel> access
    And I am on the payment hub page
    And I select the from account as <fromAccount> in the payment hub page
    And I select the to beneficiary as <toAccount> in the payment hub page
    When I enter <amount> in the payment amount field in the payment hub page
    Then I should see only 2 decimal places in the payment amount field in the payment hub page
    And I should see the continue button enabled in the payment hub page
    When I clear the amount field in the payment hub page
    And I select the from account as <fromAccount> in the payment hub page
    And I select the to beneficiary as ukCompanyBeneficiary in the payment hub page
    And I enter payment amount greater than available balance in the remitter account in the payment hub page
    Then I am unable to edit the Reference field and displays the stored reference set
    And I should be able to close the insufficient money in your account error message in the payment hub page
    And I clear the amount field in the payment hub page
#    When I submit the payment with all the mandatory fields with any from account and to account with amount value 10005.00
#    Then I should see internet transaction limit exceeded error message in payment hub page
#    And I should see the values pre-populated in the payment hub page for transfer with amount value 10005.00
#    And I clear the amount field in the payment hub page
    When I submit the payment with from account <fromAccount> and to beneficiary <toAccount> from beneficiary list with amount 1.00 and reference
    Then I should see the corresponding details in the review payment page
    And I select the confirm option in the review payment page
    When I enter a correct password
    Then I should see a payment confirmed screen
    And I should see view transaction and make another payment option in the payment success page
    And I select dismiss modal using close button
    And I navigate to logoff via more menu
    And I should be on the logout page
    Examples:
      | accessLevel          | fromAccount | toAccount        | amount          |
      | FULL_SIGNATORY       | CURRENT     | firstBeneficiary | 0.019999        |
      | FULL_ACCESS_DELEGATE | CURRENT     | uKBank           | 9999999999.9999 |

  @stub-bnga-payment-card-reader-success @stubOnly
  Scenario Outline: TC_002_Main Success Scenario: User Successfully Makes a Payment using Card Reader
    Given I am on home screen as MULTIPLE_BUSINESS business user with CURRENT account and <accessLevel> access
    And I am on the payment hub page
    And I submit the payment with all the mandatory fields with from account as <fromAccount> and <toAccount> beneficiary with amount value <amount> and reference
    When I select the confirm option in the review payment page
    Then I should be prompted to enter the 8 digit passcode in card reader respond screen
    And I provide valid passcode in the card reader authentication screen
    And I should see a payment confirmed screen
    And I should see an option make another transfer
    And I should see an option to view transactions
    And I select dismiss modal using close button
    And I navigate to logoff via more menu
    And I should be on the logout page
    Examples:
      | accessLevel    | fromAccount | toAccount        | amount |
      | FULL_SIGNATORY | CURRENT     | firstBeneficiary | 1      |

  #PAYMOB-1256 CBS Error is appearing on red banner rather than unsuccessful page on android
  #EnvironmentOnly Tag should be removed once the PAYMOB-1425 is fixed
  @stub-bnga-cbs-payment @environmentOnly
  Scenario: TC_003_Exception Scenario: Show an error message MG-385 when CBS Prohibitive indicator is loaded on either the remitting or beneficiary account
    Given I am on home screen as MULTIPLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I am on the payment hub page
    And I submit the payment with from account CURRENT and to beneficiary cbsPayee from beneficiary list with amount 1.00 and reference
    When I select the confirm option in the review payment page
    And I enter a correct password
    Then I should see cbs prohibitive error message in payment unsuccessful page
    And I select dismiss modal using close button
    And I navigate to logoff via more menu
    And I should be on the logout page

  # EnvironmentOnly Tag should be removed once the PAYMOB-1425 is fixed
  # Marked as exhaustive data due to DRE team keeps on changing the limit.
  @stub-bnga-payment-declined @environmentOnly @exhaustiveData
  Scenario: TC_004: Display payment declined error message for make payment (MG-368 / 369 / 375 / 376)
    Given I am on home screen as MULTIPLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I am on the payment hub page
    When I submit the payment with from account CURRENT and to beneficiary firstBeneficiary from beneficiary list with amount 90 and reference
    And I select the confirm option in the review payment page
    And I enter a correct password
    Then I should see the declined error message in the payment decline page
    And I should see pre auth and back to logon in the payment decline page

  # EnvironmentOnly Tag should be removed once the PAYMOB-1425 is fixed
  # Pending tag should be removed once the payment error messaged confirmed from Payments team.
  @stub-bnga-payment-review @ios @environmentOnly @pending
  Scenario: TC_005: Display payment under review error message for make payment (MG-379/ 372)
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I fetch the payment limit for CURRENT account from IBC
    And I am on the payment hub page
    When I submit the payment with from account CURRENT and to beneficiary firstBeneficiary from beneficiary list with amount 2.50 and reference
    And I select the confirm option in the review payment page
    And I enter a correct password
    Then I should see the payment hold error message in the payment on hold page
    And I should see transfer and payment button in the payment on hold page
    And I select dismiss modal using close button
    And I navigate to logoff via more menu
    And I should be on the logout page

  @stub-mpa-payment
  # COSE-518 .Pre-requisite business should be registered for MPA User Limit and Two/Three to approve payment control settings should turned on for the user
  # NOTE- Once the transfer request is submitted , tester should verify the notification on desktop for the final approvers .
  # MPA applies for interBusiness accounts transfer and not for intrabusinesses transfer
  # MPA applies for payments to external beneficiaries
  Scenario Outline: TC_006_Main Success Scenario: User makes a successful payment via MPA
    Given I am on home screen as MULTIPLE_BUSINESS business user with CURRENT_MPA account and FULL_SIGNATORY access
    And I am on the payment hub page
    When I submit the payment with from account as <fromAccount> of <fromBusiness> and to beneficiary <beneficiary> with amount value 9.00
    Then I should not see MPA approval message in the review payment page
    When I select the edit option in the review payment page
    And I submit the payment with from account as <fromAccount> of <fromBusiness> and to beneficiary <beneficiary> with amount value 11
    Then I should see MPA approval message in the review payment page
    When I select the confirm option in the MPA review payment page
    Then I should see make another payment option in the MPA payment submitted for approval page
    When I navigate back to home page from MPA approval submitted page
    Then I should see the corresponding 11.00 not deducted in the remitter <fromAccount>
    When I navigate to logoff via more menu
    Then I should be on the logout page
    Examples:
      | fromAccount | fromBusiness   | beneficiary      |
      | CURRENT_MPA | equity | firstBeneficiary |

  #MOB3-14228
  @stub-bnga-no-eligible-accounts @noExistingData
  Scenario: TC_007_Exception Scenario: User has no eligible accounts to make a payment or transfer as Full Signatory\Full delegate from Home Page entry point
    Given I am on home screen as MULTIPLE_BUSINESS business user with CREDITCARD account and FULL_SIGNATORY access
    When I navigate to pay and transfer from home page
    Then I should see the no remitter eligible error message
    And I should see Apply for other accounts button on no eligible accounts page
    And I select dismiss modal using close button
    And I select switch business option on home page
    And I select business with CREDITCARD account and FULL_ACCESS_DELEGATE access from business selection screen
    When I navigate to pay and transfer from home page
    Then I should see the no remitter eligible error message
    And I select dismiss modal using close button
    And I select the action menu of CREDITCARD account
    And I should not see Transfer and payment option in the action menu
    And I close the action menu in the home page
    And I navigate to logoff via more menu
    And I should be on the logout page

  # MOB3-12486, 14066 COSE-49
  @stub-enrolled-valid
  Scenario Outline: TC_008_User initiate Payments & Transfers from Actions menu / from Homepage then pre-populate the From field based on the last visible account tile from Homepage
    Given I am on home screen as MULTIPLE_BUSINESS business user with CURRENT account and <accessLevel> access
    And I select <account> account tile from home page
    And I select the action menu from statements page
    When I select the transfer and payment option in the action menu
    Then I should see the from field pre-populated with <account> account
    And I select dismiss modal using close button
    When I navigate to pay and transfer from home page
    Then I should see the from field pre-populated with <account> account
    And I select dismiss modal using close button
    And I navigate to logoff via more menu
    And I should be on the logout page
    Examples:
      | accessLevel          | account |
      | FULL_SIGNATORY       | CURRENT |
      | FULL_ACCESS_DELEGATE | CURRENT |

  # MOB3-12486
  @stub-enrolled-valid
  Scenario Outline: TC_009_User initiate Payments & Transfers from Homepage and the last visible account tile is not eligible for payments then next eligible account should be pre-populated in From field
    Given I am on home screen as MULTIPLE_BUSINESS business user with <account> account and <accessLevel> access
    And I obtain the default primary account
    And I select <account> account tile from home page
    When I navigate to pay and transfer from home page
    Then I should see the from field pre-populated with defaultPrimary account
    And I select dismiss modal using close button
    And I navigate to logoff via more menu
    And I should be on the logout page
    Examples:
      | accessLevel          | account    |
      | FULL_SIGNATORY       | CHARGECARD |
      | FULL_ACCESS_DELEGATE | CREDITCARD |

  # COSE-48 Dynamic accounts will get displayed
  @stub-enrolled-valid @manual
  Scenario Outline: TC_010_View other business accounts in remitting account
    Given I am an enrolled "multiple business" user logged into the app
    When I select <business> business with <accessLevel> access from the business selection screen
    And I choose the remitter account on the payment hub page
      | accounts                    |
      | current                     |
      | savingAccountNoTransactions |
    Then I should be shown accounts eligible to make a payment or transfer available from the business currently selected
    And I can see all eligible remitting accounts from the Businesses I have <access> access to
    And the accounts are grouped by business
    Examples:
      | business              | accessLevel    |
      | fullSignatoryBusiness | full Signatory |
      | fullDelegateBusiness  | full Delegate  |

  # Below commented line required for manual validations
  # MOB3-12479
  @stub-enrolled-valid @ff-pca-alltab-enabled-off
  Scenario Outline: TC_011_View other business accounts in recipient account
    Given I am on home screen as MULTIPLE_BUSINESS business user with CURRENT account and <accessLevel> access
    And I select <accountTile> account tile from home page
    And I choose the remitter account on the payment hub page
    And I select <accountTile> Account on payment hub page
    When I choose a recepient account on the payment hub page
    Then I should be shown accounts eligible to make a transfer to, from the business currently selected
#    And I can see all eligible recipient accounts from the Businesses I have <accessLevel> access to
#    And the accounts should be grouped by business
    Examples:
      | accessLevel          | accountTile |
      | FULL_SIGNATORY       | CURRENT     |
      | FULL_SIGNATORY       | SAVINGS     |
      | FULL_ACCESS_DELEGATE | CURRENT     |
      | FULL_ACCESS_DELEGATE | SAVINGS     |

  # MOB3-14072 COSE-48
  @stub-enrolled-valid
  Scenario Outline: TC_012: Display/Doesn't display list of all business eligible/non-eligible remitting accounts in the from field
    Given I am on home screen as MULTIPLE_BUSINESS business user with CURRENT account and <accessLevel> access
    When I choose the remitter account on the payment hub page
    Then I should see the below accounts in the view remitter page
      | accounts |
      | CURRENT  |
      | SAVINGS  |
    And I should not see the below accounts in the view remitter page
      | accounts        |
      | CURRENT_DORMANT |
      | CREDITCARD      |
    And I select dismiss modal using close button
    And I navigate to logoff via more menu
    And I should be on the logout page
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |

  #COSE-553, COSE-554
  @sanity @stub-bnga-payment-success-password @exhaustiveData
  Scenario Outline: TC_013 Make Future Dated Payment and verify pending payment is not allowed to select as beneficiary account
    Given I am on home screen as MULTIPLE_BUSINESS business user with <fromAccount> account and FULL_SIGNATORY access
    And I am on the payment hub page
    And I select the from account as <fromAccount> in the payment hub page
    And I select the to beneficiary as <toAccount> in the payment hub page
    And I enter amount in the amount field as 1.00
    And I select calendar picker option for future date payment
    And I should be allowed to select a payment date up to 31 days from current day
    And I select cancel in the calendar pop up
    And I select calendar picker option for future date payment
    And I select the date as <days> from today's date from calendar view
    And I select Continue button on payment hub page
    Then I should see the review payment page
    And I am shown 'When' the payment will be made on payment review page
    And I am shown information of payment date if it falls on a holiday
    When I select the confirm option in the review payment page
    And I enter a correct password
    Then I should see view transaction and make another payment option in the payment success page
    And I select the make another payment option in the payment success page
    When I select the recipient account on the payment hub page
    Then I should not be able to select pending payment from view recipient page
    And I select dismiss modal using close button
    And I navigate to logoff via more menu
    And I should be on the logout page
    Examples:
      | fromAccount | toAccount         | days |
      | CURRENT     | firstBeneficiary  | 2    |

  #TODO: Merge for environment once LDS env is up to add recipients
  # MOB3-12757
  @stub-enrolled-valid
  Scenario: TC_014_Main Success: User Searches for Internal Accounts/Recipients
    Given I am on home screen as MULTIPLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I choose the recipient account on the payment hub page
    Then I verify search results for each search term
      | searchType | searchTerm | results        |
      | account    | 30         | businessSaving |
      | recipient  | AT         | uKBank         |
    And I click on cancel searching
    And I select dismiss modal using close button
    And I navigate to logoff via more menu
    And I should be on the logout page

  # PAYMOB-285, 286
  @stub-enrolled-valid
  Scenario: TC_015 Add New Company Beneficiary account and Make Payment
    Given I am on home screen as MULTIPLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I navigate to add new recipient option using payment hub
    And I choose to Add new payee on search recipient page
    When I enter whitelisted company details
    Then I select continue button on add payee page
    And I select a result
    And I should see UK Company Review Details page
    And I have the ability to add a reference 5434298992481650, 5434298992481650 and retype the reference
    And I select continue on UK review details page
    And I enter a correct password
    And I am on the Make Payment screen with newly added beneficiary selected
    When I enter 1.00 in the payment amount field in the payment hub page
    And I select the continue option in the payment hub page
    And I select the confirm option in the review payment page
    And I enter a correct password
    Then I am shown Payment Successful page
