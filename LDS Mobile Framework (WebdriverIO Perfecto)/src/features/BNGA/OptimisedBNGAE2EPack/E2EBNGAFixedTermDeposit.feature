@team-statements @nga3 @bnga @stub-enrolled-valid @stubOnly @optFeature @sw-enrolment-on @sw-light-logon-on
Feature: Fixed Term Deposit
  In order that I can am aware of my fixed term deposit accounts
  As a BNGA authenticated user,
  I want to be able to view the details of my fixed term deposit accounts

  #MOB3-14590
  Scenario Outline: TC_001 Main Success Scenario: User views details of fixed term deposit account with one settlement account
    Given I am on home screen as <accessLevel> access user with FIXED_TERM_DEPOSIT account
    When I select <type> account tile from home page
    Then I should be displayed with <field> in statements summary
    And I should be displayed the fixed term deposit account page with the following details
      | fieldsInTreasuryAccount                |
      | aerGrossInterestRate                   |
      | basicTaxRateStatus                     |
      | accountOpenedOn                        |
      | currentMaturityInstructions            |
      | dateInstructionReceived                |
      | maturityDate                           |
      | daysToMaturity                         |
      | estimatedGrossInterestForTermOfDeposit |
      | aerInfoLink                            |
      | nominatedAccountInfoLink               |
    Examples:
      | accessLevel          | type                             | field                            |
      | FULL_SIGNATORY       | FIXED_TERM_DEPOSIT               | nominatedAccount                 |
      | FULL_ACCESS_DELEGATE | FIXED_TERM_DEPOSIT               | nominatedAccount                 |
      | VIEW_ONLY_DELEGATE   | FIXED_TERM_DEPOSIT               | nominatedAccount                 |
      | FULL_SIGNATORY       | FIXED_TERM_DEPOSIT_WITH_MULTIPLE | multipleSettlementAccountTextMsg |
      | FULL_ACCESS_DELEGATE | FIXED_TERM_DEPOSIT_WITH_MULTIPLE | multipleSettlementAccountTextMsg |
      | VIEW_ONLY_DELEGATE   | FIXED_TERM_DEPOSIT_WITH_MULTIPLE | multipleSettlementAccountTextMsg |

  #DPL-2842
  @manual
  Scenario: TC_006 Scenario: User should see Brand Logo for Fixed Term deposit account
    Given I am on home screen as FULL_SIGNATORY access user with FIXED_TERM_DEPOSIT account
    When I select TREASURY_32DAYNOTICE account tile from home page
    Then I should see brand logo for Fixed Term deposit account
