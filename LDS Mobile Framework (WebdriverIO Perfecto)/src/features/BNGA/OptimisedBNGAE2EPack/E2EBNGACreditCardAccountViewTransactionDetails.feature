@team-statements @nga3 @bnga @stub-enrolled-valid @stubOnly @optFeature @sw-enrolment-on @sw-light-logon-on
Feature: User views Current Account Transaction Details & Help with this transaction
  In order that I am better informed about a specific transaction
  As a BNGA user
  I should be able to view transaction details for my current account transactions
  I want to be able to know what do to if Help with this transaction

  Scenario Outline: TC_001 Main Success Scenario: User views Posted Transaction Details for credit card transactions and view Help with this transaction
    Given I am on home screen as <accessLevel> access user with <type> account
    When I select <type> account tile from home page
    And I tap on the pending transactions accordion
    And I select pending Credit Card on statements page
    Then I should see pending transaction details for that transaction with the following fields
      | fieldsInDetailScreen     |
      | vtdCloseButton           |
      | vtdAmount                |
      | transactionDescription   |
      | dateOfTransaction        |
      | creditCardDisclaimerText |
      | unsureTransactionButton  |
    When I close the VTD page
    And I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | date                    |
      | vtdAmount               |
      | <typeOfTransaction>     |
      | transactionDescription  |
      | dateOfTransaction       |
      | cardEndingNumber        |
      | unsureTransactionButton |
    And I select the help with this transaction link in VTD screen
    And I must be displayed the Unsure About Transactions Help and Info screen
    When I select to contact customer service
    Then I am displayed the call us businessAccounts for the selected option
    Examples:
      | accessLevel          | type                | transactionType                   | typeOfTransaction       |
      | FULL_SIGNATORY       | CREDITCARD_WITH_VTD | contact Less Purchase Credit Card | contactLessPurchaseText |
      | FULL_ACCESS_DELEGATE | CREDITCARD_WITH_VTD | chip And Pin Purchase Credit Card | chipAndPinText          |
      | VIEW_ONLY_DELEGATE   | CREDITCARD_WITH_VTD | manual Or Paper Credit Card       | manualPaperText         |
      | FULL_SIGNATORY       | CREDITCARD_WITH_VTD | online Credit Card                | onlinePhoneMailText     |
      | FULL_ACCESS_DELEGATE | CREDITCARD_WITH_VTD | interest Credit Card              | creditCardText          |
      | VIEW_ONLY_DELEGATE   | CHARGECARD          | charges Credit Card               | creditCardText          |
