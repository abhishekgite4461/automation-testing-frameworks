@team-pi @nga3 @bnga @sw-enrolment-on @sw-light-logon-on @sw-mobile-chat-ios-on @sw-mobile-chat-android-on @ff-mobile-chat-on @stubOnly @stub-multiple-business-full-signatory @manual
#Android tag can be removed once iOS stories are implemented
Feature: Verify user successfully taken to chat window page
  In order to resolve my query quickly
  As a Retail NGA User
  I want to allow customer to engage with LBG agents in Connect in their own time and anytime

  Scenario Outline: TC_001 Chat to Us Entry Point is displayed for multiple full signatory customers
    Given I am on home screen as MULTIPLE_BUSINESS business user with CURRENT account and <accessLevel> access
    When I select support tab from bottom navigation bar
    Then I should see message us option
    When I select message us option from support hub home page
    And I send my question "ATM" to agent or bot to answer it
    Then I validate chat modal window and close it
    Examples:
      | accessLevel        |
      | FULL_SIGNATORY     |
      | FULL_DELEGATE      |
      | VIEW_ONLY_DELEGATE |
