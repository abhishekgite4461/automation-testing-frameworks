@team-mpt @sw-enrolment-off @sw-light-logon-off @bnga @nga3 @primary
Feature: Business selection feature provides ability to view list of businesses when user has access to more than one business with device enrolment OFF

  In Order to view all my businesses in single view
  As a business banking User with access to more than one business
  I want to be able to view all business accounts I wish to view prior to accessing my Homepage

 #------------------------------------------- Main Success scenario-----------------------------------------------------------------#

  @stub-enrolled-valid
  Scenario Outline:TC_001_Main Success Scenario: User should be shown Business selection screen when they logon successfully and have access to more than one business(combination of full signatory, Delegate and View only)
    Given I am a <userType> user
    When I select Ok button in express logon unavailable warning message
    And I login with my username and password
    And I enter correct MI
    Then I should see business selection screen with available business
    And I should see all the FULL_SIGNATORY businesses listed
    And I should see all the delegate and view only businesses listed
    Examples:
      | userType                           |
      | Multiple business all access levels|

  @stub-enrolled-valid
  Scenario:TC_002_Main Success Scenario: User should be shown Business selection screen when they logon successfully and have access to more than one business(Full Signatory)
    Given I am a multiple business full signatory with MI user
    When I select Ok button in express logon unavailable warning message
    And I login with my username and password
    And I enter correct MI
    Then I should see business selection screen with available business
    And I should see all the multiple business full signatory with MI businesses listed

  @stub-multiple-business-delegate-view-only
  Scenario:TC_002_Main Success Scenario: User should be shown Business selection screen when they logon successfully and have access to more than one business(Delegate and View only)
    Given I am a multiple business delegate and view only user
    When I select Ok button in express logon unavailable warning message
    And I login with my username and password
    And I enter correct MI
    Then I should see business selection screen with available business
    And I should see all the multiple business delegate and view only businesses listed
