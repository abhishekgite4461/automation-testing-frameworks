@team-mpt @nga3 @bnga @sw-enrolment-on @sw-light-logon-on
Feature: User wants to view accounts, access services/ sales journey when logged in
  In order to access important services and sales journeys
  As a BNGA customer
  I want to able to access the Homepage

  #MPT-679
  @stub-single-business-full-signatory-enrolled @primary
  Scenario: TC_001_View Homepage as a Full Signatory User
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    Then bottom navigation bar is displayed
    And I should see the below options in the bottom nav
      | optionsInBottomNav       |
      | home                     |
      | apply                    |
      | payAndTransfer           |
      | support                  |
      | more                     |
    And I should not see switch business icon on home page
    And I should not see everyday offers tile on home page

  #MPT-679
  @stub-single-business-full-delegate-enrolled @primary
  Scenario: TC_002_View Homepage as a Full Delegate User
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_ACCESS_DELEGATE access
    Then bottom navigation bar is displayed
    And I should see the below options in the bottom nav
      | optionsInBottomNav       |
      | home                     |
      | apply                    |
      | payAndTransfer           |
      | support                  |
      | more                     |
    And I should not see switch business icon on home page
    When I select apply tab from bottom navigation bar
    Then I should see not authorised to apply for products error page

  #MPT-679
  @stub-single-business-view-only-enrolled @primary
  Scenario: TC_003_View Homepage as a View Only User
    Given I am an enrolled "Single Business View Only With MI" user on the MI page
    When I enter correct MI
    Then bottom navigation bar is displayed
    And I should see the below options in the bottom nav
      | optionsInBottomNav       |
      | home                     |
      | apply                    |
      | payAndTransfer           |
      | support                  |
      | more                     |
    And I should not see switch business icon on home page
    When I select apply tab from bottom navigation bar
    Then I should see not authorised to apply for products error page
    And I select home tab from bottom navigation bar
    When I select payAndTransfer tab from bottom navigation bar
    Then I should see can't make payment error page

  # MOB3-10216
  @stub-single-business-full-signatory-enrolled @primary
  Scenario: TC_004_Main Scenario:View Homepage as a Full Signatory User
    Given I am an enrolled "Single Business Full Signatory With MI" user on the MI page
    And I enter correct MI
    Then I should not see any business selection screen
    And I should not see switch business icon on home page

  # MOB3-10216
  @stub-single-business-full-delegate-enrolled
  Scenario: TC_005_Main Scenario:View Homepage as a Full Delegate User
    Given I am an enrolled "Single Business Full Delegate With MI" user on the MI page
    And I enter correct MI
    Then I should not see any business selection screen
    And I should not see switch business icon on home page

  # MOB3-10216
  @stub-single-business-view-only-enrolled
  Scenario: TC_006_Main Scenario:View Homepage as a View Only User
    Given I am an enrolled "Single Business View Only With MI" user on the MI page
    And I enter correct MI
    Then I should not see any business selection screen
    And I should not see switch business icon on home page

  # MOB3-10216, #MPT-671
  @stub-multiple-business-full-signatory
  Scenario: TC_007_Variation Scenario:View Homepage from Business Selection as a Full Signatory User
    Given I am an enrolled "Multiple Business Full Signatory With MI" user on the MI page
    And I enter correct MI
    And I should see switch business screen
    When I select a business from the business selection screen
    And I should see switch business icon on home page

  # MOB3-10216, #MPT-671
  @stub-multiple-business-full-delegate @primary
  Scenario: TC_008_Variation Scenario:View Homepage from Business Selection as a Full Delegate User
    Given I am an enrolled "Multiple Business Full Delegate With MI" user on the MI page
    And I enter correct MI
    And I should see switch business screen
    When I select a business from the business selection screen
    And I should see switch business icon on home page

  # MOB3-10216, #MPT-671
  @stub-multiple-business-view-only
  Scenario: TC_009_Variation Scenario:View Homepage from Business Selection as a View Only User
    Given I am an enrolled "Multiple Business View Only With MI" user on the MI page
    And I enter correct MI
    And I should see switch business screen
    When I select a business from the business selection screen
    And I should see switch business icon on home page

  # MOB3-4118, DPL-945
  @stub-enrolled-valid @primary
  Scenario: TC010_Verify the current account details on the account's tile displayed on homepage
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I navigate and validate the options in the CURRENT account in below table in the home page
      | accountFields      |
      | businessName       |
      | accountNumber      |
      | sortCode           |
      | accountBalance     |
      | accountInfoBalance |
    When I select the action menu of CURRENT account
    Then I should see the below options in the action menu of the CURRENT account
      | optionsInActionMenu       |
      | viewTransactionsOption    |
      | transferAndPaymentOption  |
      | chequeDepositOption       |
      | standingOrderOption       |
      | directDebitOption         |
      | viewPendingPaymentOption  |
    And I close the action menu in the home page

  # MOB3-2309, DPL-947
  @stub-enrolled-valid @primary
  Scenario: TC011_Verify the savings account details on the account's tile displayed on homepage
    Given I am on home screen as FULL_SIGNATORY access user with SAVINGS account
    And I navigate and validate the options in the SAVINGS account in below table in the home page
      | accountFields            |
      | businessName             |
      | accountNumber            |
      | sortCode                 |
      | accountBalance           |
    When I select the action menu of SAVINGS account
    Then I should see the below options in the action menu of the SAVINGS account
      | optionsInActionMenu      |
      | viewTransactionsOption   |
      | transferAndPaymentOption |
      | chequeDepositOption      |
      | viewPendingPaymentOption |
      | viewInterestRateOption   |
    And I close the action menu in the home page

  # MOB3-2308, DPL-1006
  @stub-enrolled-valid @primary
  Scenario: TC012_Verify the credit card account details on the account's tile displayed on homepage
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD account
    And I navigate and validate the options in the CREDITCARD account in below table in the home page
      | accountFields           |
      | creditCardAccountNumber |
      | creditCardAccountBalance|
      | availableCredit         |
      | overdueAmount           |
    And I select the action menu of CREDITCARD account
    Then I should see the below options in the action menu of the CREDITCARD account
      | optionsInActionMenu    |
      | viewTransactionsOption |
    And I close the action menu in the home page

  # Need to perform dry run by Android team as new switch case on home page got added for Account Number
  @stub-enrolled-valid @primary @ios
  Scenario: TC015_Verify the 32 day notice account details on the account's tile displayed on homepage
    Given I am on home screen as FULL_SIGNATORY access user with TREASURY_32DAYNOTICE account
    When I navigate and validate the options in the TREASURY_32DAYNOTICE account in below table in the home page
      | accountFields    |
      | businessName     |
      | thirtyTwoDayNoticeAccountNumber  |
      | accountBalance |
     #|interestRate|
    And I select the action menu of TREASURY_32DAYNOTICE account
    Then I should see the below options in the action menu of the TREASURY_32DAYNOTICE account
      | optionsInActionMenu      |
      | viewTransactionsOption   |
      | viewInterestRateOption   |

  # MOB3-2311
  @stub-enrolled-valid @primary
  Scenario: TC016_Verify the fixed term deposit account details on the account's tile displayed on homepage
    Given I am on home screen as FULL_SIGNATORY access user with FIXED_TERM_DEPOSIT account
    When I navigate and validate the options in the FIXED_TERM_DEPOSIT account in below table in the home page
      | accountFields  |
      | businessName   |
      | accountBalance |
    And I select the action menu of FIXED_TERM_DEPOSIT account
    Then I should see the below options in the action menu of the FIXED_TERM_DEPOSIT account
      | optionsInActionMenu    |
      | viewTransactionsOption |

  @stub-enrolled-valid @primary
  Scenario Outline: TC017_Access to Payment Hub via Action Menu as View Only Delegate
    Given I am on home screen as VIEW_ONLY_DELEGATE access user with <accountTypes> account
    When I navigate to <accountTypes> account on home page
    And I select the action menu of <accountTypes> account
    Then I should not have access to Payments and Transfers
    Examples:
      | accountTypes |
      | CURRENT      |
      | SAVINGS      |

  @stub-enrolled-valid @primary
  Scenario: TC018_Access to Payment Hub from bottom nav as View Only Delegate
    Given I am on home screen as VIEW_ONLY_DELEGATE access user with CURRENT account
    When I navigate to pay and transfer from home page
    Then  I must see payments hub unavailable error message page
