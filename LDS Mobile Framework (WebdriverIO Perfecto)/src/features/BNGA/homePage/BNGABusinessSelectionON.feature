@team-mpt @bnga @nga3
Feature: Business selection feature provides ability to view list of businesses when user has access to more than one business with device enrolment ON

  In Order to view all my businesses in single view
  As a business banking User with access to more than one business
  I want to be able to view all business accounts I wish to view prior to accessing my Homepage

 #------------------------------------------- Main Success scenario-----------------------------------------------------------------#
  @stub-enrolled-valid @primary
  Scenario Outline:TC_001_Main Success Scenario: User should be shown Business selection screen when they logon successfully and have access to more than one business(combination of full signatory, Delegate and View only)
    Given I am an enrolled "<userType>" user on the MI page
    And I enter correct MI
    Then I should see business selection screen with available business
    And I should see all the FULL_SIGNATORY businesses listed
    And I should see all the delegate and view only businesses listed
    Examples:
      | userType                           |
      | Multiple business all access levels|

  #MPT-788
  @stub-enrolled-valid
  Scenario Outline:TC_002_Main Success Scenario: User should be shown Business selection screen when they logon successfully and have access to more than one business(Full Signatory)
    Given I am an enrolled "<userType>" user on the MI page
    And I enter correct MI
    Then I should see business selection screen with available business
    And I should see all the FULL_SIGNATORY businesses listed
    Examples:
      | userType                                         |
      | Multiple business full signatory only            |

  #MPT-788
  @stub-multiple-business-delegate-view-only
  Scenario Outline:TC_003_Main Success Scenario: User should be shown Business selection screen when they logon successfully and have access to more than one business(Delegate and View only)
    Given I am an enrolled "<userType>" user on the MI page
    And I enter correct MI
    Then I should see business selection screen with available business
    And I should see all the delegate and view only businesses listed
    Examples:
      | userType                                 |
      | Multiple business delegate and view only |

  # MOB3-10216
  @stub-multiple-business-view-only @primary
  Scenario Outline: TC_004_User want to view accounts for another business they must first navigate back to the business selection screen and then select this business
    Given I am an enrolled "<userType>" user on the MI page
    And I enter correct MI
    Then I should see business selection screen with available business
    When I select a business from the business selection screen
    And I select switch business option on home page
    Then I should see switch business screen
    Examples:
      | userType                           |
      | Multiple business view only with MI|
