@bnga @nga3 @sw-history-cheque-off @team-ics @team-payments @stub-enrolled-valid @stubOnly

Feature: Deposit History (Business)
  In order to see the deposited cheques history
  As a Business NGA User
  I want to view the entry point for mobile cheque history journey

  #MPT-3715: AC-1
  Scenario Outline: TC_001_Scenario: Verify cheque history entry point not shown for full Signatory, full Delegate and delegateViewOnly Businesses when ChequeHistory switch is OFF
    Given I login to app as a "current and savings account" user
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I select more menu
    Then I should not be shown Cheque History option
    Examples:
      | businessName              | accessLevel     |
      | fullSignatoryBusiness     | full Signatory  |
      | fullDelegateBusiness      | full Delegate   |
      | delegateViewOnlyBusiness  | view Only       |
