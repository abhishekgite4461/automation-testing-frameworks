@team-payments @nga3 @stub-current-and-savings-account @bnga
Feature: Deposit History
  In order to check the status of the cheques I have deposited via BNGA
  As a Business NGA User
  I want to check my deposit history using deposit history journey

  #MPT3784, MPT-2276 Verify the Deposit History page in BNGA app - AC01
  Scenario Outline: TC_001 Verify deposit history page
    Given I login to app as a "current and savings account" user
    And I select <businessName> business with <accessLevel> access from the business selection screen
    When I navigate to deposit history page via more menu
    Then I see the below fields on deposit history page
      |historyPageDetails|
      |depositDate|
      |depositReference|
      |depositStatus|
      |depositAmount|
    Examples:
      | businessName              | accessLevel        |
      | fullSignatoryBusiness     | full Signatory     |
      | fullDelegateBusiness      | full Delegate      |
      | delegateViewOnlyBusiness  | view only Delegate |

  #MPT3784, MPT-2276 Verify the Deposit History page in BNGA app - AC02
  Scenario Outline: TC_002 Verify account picker in deposit history page
    Given I login to app as a "current and savings account" user
    And I select <businessName> business with <accessLevel> access from the business selection screen
    When I navigate to deposit history page via more menu
    Then I see the below account picker fields on deposit history page
      |accountPickerList |
      |accountType       |
      |sortCode          |
      |accountNumber     |
      |ledgerBalance     |
      |availableBalance  |
    Examples:
      | businessName              | accessLevel        |
      | fullSignatoryBusiness     | full Signatory     |
      | fullDelegateBusiness      | full Delegate      |
      | delegateViewOnlyBusiness  | view only Delegate |

  #MPT3836, MPT-2276 - Verify the Deposit History Details page - AC01
  #TODO: Android tag should be removed once the MPT-4656 is fixed
  @android
  Scenario Outline: TC_003 Verify deposit history review page
    Given I login to app as a "current and savings account" user
    And I select <businessName> business with <accessLevel> access from the business selection screen
    When I navigate to deposit history page via more menu
    And I select an item in deposit history list
    Then I see the below fields in deposit review page
      |depositReviewFields|
      |depositBusiness    |
      |depositStatus      |
      |depositAccountType |
      |depositedDate      |
      |depositAmount      |
      |depositReference   |
    Examples:
      | businessName              | accessLevel        |
      | fullSignatoryBusiness     | full Signatory     |
      | fullDelegateBusiness      | full Delegate      |
      | delegateViewOnlyBusiness  | view only Delegate |

  #MPT-3715: AC-1
  Scenario Outline: TC_004_Scenario - Verify cheque history entry point shown for full Signatory, full Delegate and delegateViewOnly Businesses when ChequeHistory switch is ON
    Given I login to app as a "current and savings account" user
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I select more menu
    Then I should be shown Cheque History option
    Examples:
      | businessName              | accessLevel        |
      | fullSignatoryBusiness     | full Signatory     |
      | fullDelegateBusiness      | full Delegate      |
      | delegateViewOnlyBusiness  | view only Delegate |

  #MPT3784 Verify the cheque transaction reference in deposit history page after a successful deposit
  @secondary @imageInjection @environmentOnly
  Scenario Outline: TC_005 Success Scenario: Verify user is able to view reference text in transaction history
    Given I login to app as a "current and savings" user
    And I select fullSignatoryBusiness business with full Signatory access from the business selection screen
    And I navigate to cheque deposit page via more menu
    And I deposit a cheque <image> of <amount> with <reference>
    When I confirm the cheque deposit
    And I select view deposit history button
    Then I am able to view the <reference> in the transaction history
    Examples:
      |image                       |amount  |reference       |
      |DepositSuccess/FrontOfCheque|50      |testreference   |

  #MPT-4788
  Scenario Outline: TC_006_History transactions for account change
    Given I login to app as a "current and savings account" user
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to deposit history page via more menu
    And I change the account which is displayed in the history page
    Then I should see history details for the newly selected account
    Examples:
      | businessName              | accessLevel        |
      | fullSignatoryBusiness     | full Signatory     |
      | fullDelegateBusiness      | full Delegate      |
      | delegateViewOnlyBusiness  | view only Delegate |
