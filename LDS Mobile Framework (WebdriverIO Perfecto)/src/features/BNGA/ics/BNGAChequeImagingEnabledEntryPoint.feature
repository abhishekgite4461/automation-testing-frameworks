@bnga @nga3 @sw-cheque-imaging-enabled-off @team-ics @team-payments @stub-enrolled-valid @stubOnly

Feature: Cheque Deposit (Business)
  In order to Deposit Cheque using BNGA app
  As a Business NGA User
  I want to view the entry point for mobile cheque deposit journey

  #MPT-2263 & MPT-1524: AC-1 MPT-3715: AC-1
  Scenario Outline: TC_001_Scenario: Verify cheque entry points not shown for full Signatory Business and full Delegate Business user when ChequeImagingEnabled switch is OFF
    Given I login to app as a "current and savings account" user
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I select more menu
    Then I should not be shown Cheque Deposit option
    And I should not be shown Cheque History option
    Examples:
      | businessName              | accessLevel     |
      | fullSignatoryBusiness     | full Signatory  |
      | fullDelegateBusiness      | full Delegate   |

  #MPT-2263 & MPT-1524 : AC-3 MPT-3715: AC-1
  Scenario Outline: TC_002_Scenario - Verify cheque entry points not shown for delegateViewOnlyBusiness when ChequeImagingEnabled switch is OFF
    Given I login to app as a "current and savings account" user
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I select more menu
    Then I should not be shown Cheque Deposit option
    And I should not be shown Cheque History option
    Examples:
      | businessName              | accessLevel     |
      | delegateViewOnlyBusiness  | view Only       |

  #MPT-2263 & MPT-1524: AC-2 for view only. AC-2 for full access will be covered as part MPT-2564.
  Scenario Outline: TC_002_Scenario - Verify cheque entry point not shown from action menu when ChequeImagingEnabled switch is OFF
    Given I login to app as a "current and savings account" user
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to <type> account on home page
    And I select the action menu of <type> account
    Then I should not see deposit cheque option in action menu
    Examples:
      | businessName              | accessLevel     |type     |
      | delegateViewOnlyBusiness  | view Only       |current  |
