@bnga @nga3 @team-payments @stub-enrolled-valid @sensorInstrument

Feature:  Cheque Deposit (Business)
  In order to have funds available in my account within the new 2 day clearing model.
  As a Business NGA User with signatory or full delegate access
  I want to deposit a cheque

  #MPT-2564: AC-1,AC-4 and AC-5
  Scenario Outline:TC_001 User initiates deposit cheque verify the details shown on screen
    Given I am an enrolled "valid" user logged into the app
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to cheque deposit page via more menu
    Then I see below fields on cheque deposit page
      |chequeDepositOptions |
      |depositAccount       |
      |chequeAmountField    |
      |chequeReferenceField |
      |captureFrontOfChequeButton|
      |captureBackOfImageButton  |
    When I select the cheque amount field
    Then I should see help text with Cheque Max and Daily limit
    When I select the cheque reference field
    Then I should see reference help text as per copy deck
    Examples:
      | businessName              | accessLevel     |
      | fullSignatoryBusiness     | full Signatory  |

  #MPT-2564: AC-2
  Scenario Outline:TC_002 User initiates deposit cheque from business account actions menu
  Business Scenario: Verify cheque deposit option entry point for valid accounts in account action menu
    Given I am an enrolled "valid" user logged into the app
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to <type> account on home page
    And I select the action menu of <type> account
    And I select deposit cheque option in action menu
    Then I should be on the cheque deposit screen
    Examples:
      | businessName         | accessLevel    |type    |
      | fullSignatoryBusiness| full Signatory |current |

  #MPT-2564: AC-3-pending-need to find another logic for the last line of this scenario.
  Scenario Outline: TC_003 Business Scenario: User should be able to deposit cheque for selected business accounts only
    Given I am an enrolled "valid" user logged into the app
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to cheque deposit page via more menu
    And I select the account navigation icon in deposit cheque page
    Then I should be shown only the eligible accounts for depositing a cheque
    Examples:
      | businessName              | accessLevel     |
      | fullSignatoryBusiness     | full Signatory  |

   #MPT-2603 AC01 AC02
  @secondary
  Scenario Outline: TC_004: Validate the user is enabled to capture image after providing camera permission and no permission requested second time
    Given I reinstall the app
    And I login to app as a "current and savings account" user
    And I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to cheque deposit page via more menu
    And I select on the front of camera button
    When I select allow in camera permission pop up
    And I close the ics view demo screen
    Then I should be allowed to capture an image
    And I close the sdk camera
    And I relaunch the app and login as a "current and savings account" user
    And I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to cheque deposit page via more menu
    When I select on the front of camera button
    Then I should not see camera permission pop up
    Examples:
      | businessName              | accessLevel     |
      | fullSignatoryBusiness     | full Signatory  |
      | fullDelegateBusiness      | full Delegate   |

   #MPT-2603 AC03 AC04
  @secondary
  Scenario Outline: TC_005: Validate the user is enabled to capture image when modified the camera settings after deny to camera permission
    Given I reinstall the app
    And I login to app as a "current and savings account" user
    And I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to cheque deposit page via more menu
    And I select on the front of camera button
    When I deny in camera permission pop up and select front of camera
    And I select the checkbox and click deny again
    Then dialog box should pop up with settings option to enable camera
    When I select settings and enable camera permission
    Then I should be able to use camera on cheque deposit page
    Examples:
      | businessName              | accessLevel     |
      | fullSignatoryBusiness     | full Signatory  |
      | fullDelegateBusiness      | full Delegate   |

   #MPT-2603 AC05
  @manual @primary
  Scenario Outline: TC_006: Verifying ics view demo screens are displayed and scrollable when selected
    Given I login to app as a "current and savings account" user
    And I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to cheque deposit page via more menu
    When I select view demo link on cheque deposit page
    Then I see ics demo on a scrollable page in landscape with help text
    And I should be able to navigate between demo screens
    Examples:
      | businessName              | accessLevel     |
      | fullSignatoryBusiness     | full Signatory  |
      | fullDelegateBusiness      | full Delegate   |

   #MPT-2603 AC05
  @manual @secondary
  Scenario Outline: TC_007: Verifying view demo screens are displayed and scrollable on first use of cheque deposit
    Given I login to app as a "current and savings account" user
    And I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to cheque deposit page via more menu
    When I select on the front of camera button
    Then I see ics demo on a scrollable page in landscape with help text
    And I should be able to navigate between demo screens
    Examples:
      | businessName              | accessLevel     |
      | fullSignatoryBusiness     | full Signatory  |
      | fullDelegateBusiness      | full Delegate   |

   #MPT-2603 AC06
  @manual @primary
  Scenario Outline: TC_008: Verifying the retake and use options while capturing front and back of the cheque
    Given I login to app as a "current and savings account" user
    And I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to cheque deposit page via more menu
    And I select on the front of camera button
    When I capture the front cheque image
    Then I should see option to re-take and use the image
    And I accept the cheque image
    When I capture the back cheque image
    Then I should see option to re-take and use the image
    Examples:
      | businessName              | accessLevel     |
      | fullSignatoryBusiness     | full Signatory  |
      | fullDelegateBusiness      | full Delegate   |

  #MPT-2934 AC1 - Verify the Cheque Review Page NEED TO RUN THIS IN PERFECTO ONLY
  @primary @imageInjection
  Scenario Outline: TC_009 Verify Review deposit page
    Given I login to app as a "current and savings" user
    And I select fullSignatoryBusiness business with full Signatory access from the business selection screen
    When I navigate to cheque deposit page via more menu
    And I deposit a valid cheque <image> of <amount> with <reference>
    And I select review deposit button
    Then I should be able to see review deposit screen
    And I see below fields on review deposit page
      |reviewDepositOptions |
      |reviewBusinessName   |
      |reviewDepositAccount |
      |reviewChequeAmount   |
      |reviewReferenceText  |
      |reviewFrontofCheque  |
      |reviewFrontChequeMagnifierIcon |
      |reviewBackofCheque             |
      |reviewBackChequeMagnifierIcon  |
      |reviewCancelButton             |
      |reviewDepositConfirmButton     |
    Examples:
      |image                       |amount  |reference       |
      |DepositSuccess/FrontOfCheque|12      | Success screen |

  #MPT-2934 AC2 - Verify the cheque zoom functionality NEED TO RUN THIS IN PERFECTO ONLY
  @imageInjection
  Scenario Outline: TC_010 Verifying the zoom functionality of the cheques in the Review Deposit Page
    Given I login to app as a "current and savings" user
    And I select fullDelegateBusiness business with full Delegate access from the business selection screen
    When I navigate to cheque deposit page via more menu
    And I deposit a valid cheque <image> of <amount> with <reference>
    And I select review deposit button
    And I select thumbnail for front image
    Then I should be able to see the full screen image
    When I close the cheque image
    Then I should be able to see review deposit screen
    When I select thumbnail for back image
    Then I should be able to see the full screen image
    Examples:
      |image                       |amount  |reference       |
      |DepositSuccess/FrontOfCheque|12      | Success screen |

  #MPT-2934 AC3 - Click the cancel button on the Review Deposit Page NEED TO RUN THIS IN PERFECTO ONLY
  @secondary @imageInjection
  Scenario Outline: TC_011 Verifying the review cancel button functionality
    Given I login to app as a "current and savings" user
    And I select fullDelegateBusiness business with full Delegate access from the business selection screen
    When I navigate to cheque deposit page via more menu
    And I deposit a valid cheque <image> of <amount> with <reference>
    And I select review deposit button
    And I click on the cancel button on the review deposit page
    Then I should be on the cheque deposit screen
    Examples:
      |image                       |amount  |reference       |
      |DepositSuccess/FrontOfCheque|12      | Success screen |

  #MPT-3789 & MPT-1655: AC-1 :
  #Note : Android and iOS behaviour is different for limit testing.
  @android
  Scenario Outline:TC_012 Minimum amount validation in cheque amount field
    Given I am an enrolled "valid" user logged into the app
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to cheque deposit page via more menu
    And I enter cheque amount less than minimum <invalidAmount>
    Then I should not be displayed with entered <invalidAmount>
    Examples:
      | businessName         | accessLevel    |invalidAmount |
      | fullSignatoryBusiness| full Signatory |0.00          |
      | fullDelegateBusiness | full Delegate  |0.00          |

  @ios @imageInjection
  Scenario Outline:TC_012 Minimum amount validation in cheque amount field
    Given I am an enrolled "valid" user logged into the app
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to cheque deposit page via more menu
    And I deposit a valid cheque <image> of <lessThanMinimumAmount> with <reference>
    Then I should not be shown enabled review deposit button
    When I enter correct cheque transaction limit <validAmount>
    Then I should be shown enabled review deposit button
    Examples:
      | businessName         | accessLevel    |image                       |lessThanMinimumAmount |reference           |validAmount |
      | fullSignatoryBusiness| full Signatory |DepositSuccess/FrontOfCheque|0.00                  |MinAmountValidation |0.01        |
      | fullDelegateBusiness | full Delegate  |DepositSuccess/FrontOfCheque|0.00                  |MinAmountValidation |0.01        |

  #MPT-3789 & MPT-1655: AC-2
  #Note : Android and iOS behaviour is different for limit testing.
  @android
  Scenario Outline: TC_013 User has entered amount greater than cheque transaction limit
    Given I am an enrolled "valid" user logged into the app
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to cheque deposit page via more menu
    And I deposit a cheque <image> of <amount> with <reference>
    Then I should see transaction limit error message in cheque deposit page
    Examples:
      | businessName         | accessLevel    |image                          |amount  |reference           |
      | fullSignatoryBusiness| full Signatory |LimitErrorMessage/FrontOfCheque|555     |Limit error message |
      | fullDelegateBusiness | full Delegate  |LimitErrorMessage/FrontOfCheque|555     |Limit error message |

  @ios
  Scenario Outline: TC_013 User has entered amount greater than cheque transaction limit
    Given I am an enrolled "valid" user logged into the app
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to cheque deposit page via more menu
    And I enter an amount more than the ics mobile cheque deposit transaction limit <chequeMaxAmount>
    Then I should see transaction limit error message in cheque deposit page
    Examples:
      | businessName         | accessLevel    |chequeMaxAmount |
      | fullSignatoryBusiness| full Signatory |501             |
      | fullDelegateBusiness | full Delegate  |501             |

  #MPT-3789 & 1655: AC-3
  @manual
  Scenario Outline: TC_014 Server is not able to return the limit information
    Given I am an enrolled "valid" user logged into the app
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to cheque deposit page via more menu
    And retrieve limits service has failed our timed out
    Then do not perform FCM mobile cheque deposit transaction limit validation
    Examples:
      | businessName         | accessLevel     |
      | fullSignatoryBusiness| full Signatory  |
      | fullDelegateBusiness | full Delegate   |

  #MPT-3789: AC-4
  Scenario Outline: TC_015 Reference field accepts appropriate alphanumeric value of length 18
    Given I am an enrolled "valid" user logged into the app
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to cheque deposit page via more menu
    And I select the cheque reference field
    And I enter correct alphanumeric characters of length 18 as <reference>
    Then I should be able to view the 18 characters in reference
    Examples:
      | businessName         | accessLevel    |reference         |
      | fullDelegateBusiness | full Delegate  |testDepositJourney|
      | fullDelegateBusiness | full Delegate  |testDepositJourney|

  #MPT-3789: cheque daily limit validation.
  #Note:@pending for Android due to the perfecto_ticket:03061353-"App not allowing to login when sensonr instrument is true" is resovled.
  @environmentOnly @imageInjection @ios
  Scenario Outline:TC_016 cheque daily limit validation
    Given I am a multiple business with MI user
    When I login with my username and password
    And I enter correct MI
    Then I should see business selection screen
    When I select <businessName> business with <accessLevel> access from the business selection screen
    And I navigate to cheque deposit page via more menu
    And I deposit cheques <image> of <amount> with <reference> for daily limit validation
    And I confirm the cheque deposit
    Then I should receive an daily limit error message for last cheque <chequeOrder>
    Examples:
      | businessName         | accessLevel     |image                     |amount  |reference       |chequeOrder|
      | fullSignatoryBusiness| full Signatory  |DailyLimit/DailyLimit1    |400     | First Cheque   | 1         |
      | fullDelegateBusiness | full Signatory  |DailyLimit/DailyLimit2    |400     | Second Cheque  | 2         |
      | fullSignatoryBusiness| full Signatory  |DailyLimit/DailyLimit3    |400     | Third Cheque   | 3         |

    #MPT-2935, MPT-2270  AC-5, AC-7, AC-1 Verify the cheque deposit success page NEED TO RUN ONLY ON PERFECTO ONLY
  @imageInjection
  Scenario Outline: TC_017 Verify Cheque Deposit Success page
    Given I login to app as a "current and savings" user
    And I select fullSignatoryBusiness business with full Signatory access from the business selection screen
    When I navigate to cheque deposit page via more menu
    And I deposit a valid cheque <image> of <amount> with <reference>
    And I select review deposit button
    And I confirm the cheque deposit
    Then I see below fields on deposit success screen
      |successDepositOptions             |
      |successBusinessName               |
      |successSortCode                   |
      |successDepositAccount             |
      |dueDate                           |
      |amountDeposited                   |
      |successReferenceText              |
      |fastChequeIcon                    |
      |viewDepositHistoryButton          |
      |depositAnotherCheque              |
    Examples:
      |image                       |amount  |reference       |
      |DepositSuccess/FrontOfCheque|12      | Success screen |

#MPT-2935, MPT-2270 AC-2 Verify the Deposit another cheque button on the Cheque Deposit Success page NEED TO RUN ONLY ON PERFECTO ONLY
  @imageInjection
  Scenario Outline: TC_018 Verify the Deposit another cheque button on the Cheque Deposit Success page
    Given I login to app as a "current and savings" user
    And I select fullSignatoryBusiness business with full Signatory access from the business selection screen
    When I navigate to cheque deposit page via more menu
    And I deposit a valid cheque <image> of <amount> with <reference>
    And I select review deposit button
    And I confirm the cheque deposit
    And I select confirm button if popup screen displayed for amount mismatch
    And I click on the deposit another cheque button
    Then I see below fields on cheque deposit page
      |chequeDepositOptions |
      |depositAccount       |
    Examples:
      |image                       |amount  |reference       |
      |DepositSuccess/FrontOfCheque|12      | Success screen |

#MPT-2935, MPT-2270 AC-3 Verify the Deposit History button on the Cheque Deposit Success page NEED TO RUN ONLY ON PERFECTO ONLY
  @imageInjection
  Scenario Outline: TC_019 Verify the Deposit History button on the Cheque Deposit Success page
    Given I login to app as a "current and savings" user
    And I select fullSignatoryBusiness business with full Signatory access from the business selection screen
    When I navigate to cheque deposit page via more menu
    And I deposit a valid cheque <image> of <amount> with <reference>
    And I select review deposit button
    And I confirm the cheque deposit
    And I select confirm button if popup screen displayed for amount mismatch
    And I click on the view deposit history button
    Then I see the below fields on deposit history page
      |historyPageDetails|
      |depositDate|
      |depositStatus|
    Examples:
      |image                       |amount  |reference       |
      |DepositSuccess/FrontOfCheque|12      | Success screen |

#MPT-2935, MPT-2270 AC-8, AC-9, AC-4 Verify the Deferred amount on the main balance after successful cheque deposit NEED TO RUN ONLY ON PERFECTO ONLY
  @imageInjection @environmentOnly
  Scenario Outline: TC_020 Verify the Deferred credit on the main balance, after successful cheque deposit
    Given I login to app as a "current and savings" user
    And I select fullSignatoryBusiness business with full Signatory access from the business selection screen
    When I navigate to cheque deposit page via more menu
    And I capture the main balance from account tile
    And I deposit a valid cheque <image> of <amount> with <reference>
    And I select review deposit button
    And I confirm the cheque deposit
    And I navigate back to cheque deposit home screen
    Then I should see an updated deferred balance
    Examples:
      |image                       |amount  |reference       |
      |DepositSuccess/FrontOfCheque|12      | Success screen |

  #MPT-2270 AC-6
  @imageInjection @environmentOnly
  Scenario Outline: TC_022 Negative Scenario: Verify duplicate cheque error message
    Given I login to app as a "current and savings" user
    And I select fullSignatoryBusiness business with full Signatory access from the business selection screen
    When I navigate to cheque deposit page via more menu
    And I deposit a cheque <image> of <amount> with <reference>
    And I confirm the cheque deposit
    Then I should receive an duplicate cheque error message on the failure screen for duplicate cheque <chequeOrder>
    Examples:
      |image                          |amount  |reference         |chequeOrder|
      |DuplicateCheque/FrontOfCheque  |10      | Success screen   | 1         |
      |DuplicateCheque/FrontOfCheque  |10      | duplicate cheque | 2         |

#MPT-3987 AC-1 Verify IQA SDK error messages on front or back camera NEED TO RUN ONLY ON PERFECTO.
  @imageInjection @secondary @manual
  Scenario: TC_021 Negative Scenario: Verify IQA SDK error messages on front or back camera
    Given I login to app as a "current and savings" user
    When I select fullSignatoryBusiness business with full Signatory access from the business selection screen
    And I navigate to cheque deposit page via more menu
    And I select on the front of camera
    And I capture the front cheque image
    Then I should see an appropriate iqa sdk error messages

#MPT-3987 AC-2 Verify hard-coded SDK error messages on front or back camera NEED TO RUN ONLY ON PERFECTO.
  @imageInjection @secondary @manual
  Scenario: TC_022 Negative Scenario: Verify hard-coded SDK error messages on front camera
    Given I login to app as a "current and savings" user
    When I select fullSignatoryBusiness business with full Signatory access from the business selection screen
    And I navigate to cheque deposit page via more menu
    And I select on the front of camera
    And I capture the front cheque image
    Then I should see an appropriate hard-coded sdk error messages

  #MPT-4270 App crash is happening in cheque deposit journey at response page
  #Verifying if the win-back pop's up whenever the user tries to move away from the cheque deposit journey
  Scenario Outline: TC_021 Verify the win-back popup's and should be on the cheque deposit page when clicked on stay option
    Given I login to app as a "current and savings" user
    And I select fullSignatoryBusiness business with full Signatory access from the business selection screen
    When I navigate to cheque deposit page via more menu
    And I enter correct alphanumeric characters of length 18 as testDepositJourney
    And I select <bottomTabBarIcons> tab from bottom navigation bar
    Then I should see winback option in the middle of the screen with stay and leave option
    And on selection of stay option I should stay on cheque deposit homepage
    Examples:
      |bottomTabBarIcons|
      |home             |
      |apply            |
      |payAndTransfer   |
      |support          |
      |more             |

  #MPT-4270 App crash is happening in cheque deposit journey at response page
  #Verifying if the win-back pop's up whenever the user tries to move away from the cheque deposit journey
  Scenario Outline: TC_040 Verify the win-back popup's and should be on the more menu page when clicked on leave option
    Given I am an enrolled "current and savings account" user logged into the app
    And I select fullDelegateBusiness business with full Delegate access from the business selection screen
    When I navigate to cheque deposit page via more menu
    And I enter correct alphanumeric characters of length 18 as testDepositJourney
    And I select <bottomTabBarIcons> tab from bottom navigation bar
    And I choose to Leave option on winback dialogue
    Then I should be on more menu
    Examples:
      |bottomTabBarIcons|
      |home             |
      |apply            |
      |payAndTransfer   |
      |support          |
      |more             |
