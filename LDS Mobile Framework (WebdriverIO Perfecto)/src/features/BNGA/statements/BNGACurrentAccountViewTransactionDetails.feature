@team-statements @nga3 @bnga @stub-enrolled-valid @stubOnly
Feature: User views Current Account Transaction Details
  In order that I am better informed about a specific transaction
  As a BNGA user
  I should be able to view transaction details for my current account transactions

  #MOB3-12859
  @primary
  Scenario Outline: TC_001 Main Success Scenario: User views Posted Transaction Details for current account debit card transactions
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select CURRENT account tile from home page
    And I swipe to see previous month transactions on the account statement page
    And I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | date                    |
      | vtdAmount               |
      | <typeOfTransaction>     |
      | transactionDescription  |
      | dateOfTransaction       |
      | cardEndingNumber        |
      | unsureTransactionButton |
    Examples:
      | accessLevel          | transactionType       | typeOfTransaction       |
      | FULL_SIGNATORY       | contact Less Purchase | contactLessPurchaseText |
      | FULL_ACCESS_DELEGATE | chip And Pin Purchase | chipAndPinText          |
      | FULL_SIGNATORY       | online Debit Card     | onlinePhoneMailText     |

  @primary
  Scenario Outline: TC_002 Main Success Scenario: User views Posted Transaction Details for current account payment transactions
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select CURRENT account tile from home page
    And I swipe to see previous month transactions on the account statement page
    And I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | date                    |
      | vtdAmount               |
      | <typeOfTransaction>     |
      | transactionDescription  |
      | transferPaymentsButton  |
      | unsureTransactionButton |
    Examples:
      | accessLevel          | transactionType          | typeOfTransaction    |
      | FULL_ACCESS_DELEGATE | faster Payments Incoming | fasterPaymentsInText |
      | FULL_SIGNATORY       | mobile Payments Inbound  | mobilePaymentInText  |
      | FULL_ACCESS_DELEGATE | bill Payment             | billPaymentText      |
      | FULL_SIGNATORY       | bank Giro Credit         | bankGiroCreditText   |
      | FULL_ACCESS_DELEGATE | cash Deposited In IDM    | cashText             |

  #MOB3-14011, 14012, 14013 COSE-228, 312
  @primary
  Scenario Outline: TC_003 Main Success Scenario: User views Posted Transaction Details for current account payment transactions with extra fields
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select CURRENT account tile from home page
    And I swipe to see previous month transactions on the account statement page
    And I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | vtdCloseButton            |
      | date                      |
      | vtdAmount                 |
      | <typeOfTransaction>       |
      | transactionDescription    |
      | transferPaymentsButton    |
      | unsureTransactionButton   |
      | <transactionDetailField1> |
      | <transactionDetailField2> |
    Examples:
      | accessLevel          | transactionType          | typeOfTransaction     | transactionDetailField1 | transactionDetailField2 |
      | FULL_SIGNATORY       | faster Payments Outgoing | fasterPaymentsOutText | dateOfTransaction       | timeOfTransaction       |
      | FULL_ACCESS_DELEGATE | mobile Payments Outgoing | mobilePaymentOutText  | dateOfTransaction       | timeOfTransaction       |
      | FULL_SIGNATORY       | transfers                | transferText          | sortCode                | accountNumber           |
      | FULL_ACCESS_DELEGATE | transfersOut             | transferText          | sortCode                | accountNumber           |

  Scenario Outline: TC_004 Main Success Scenario: User views Posted Transaction Details for current account direct debit transaction
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select CURRENT account tile from home page
    And I swipe to see previous month transactions on the account statement page
    And I select direct Debit on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | date                    |
      | vtdAmount               |
      | directDebitText         |
      | transactionDescription  |
      | referenceDirectDebit    |
      | viewDirectDebit         |
      | unsureTransactionButton |
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  Scenario Outline: TC_005 Main Success Scenario: User views Posted Transaction Details for current account standing order transaction
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select CURRENT account tile from home page
    And I swipe to see previous month transactions on the account statement page
    And I select standing Order on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | vtdCloseButton            |
      | date                      |
      | vtdAmount                 |
      | transactionDescription    |
      | standingOrderText         |
      | manageStandingOrderButton |
      | unsureTransactionButton   |
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  Scenario Outline: TC_006 Main Scenario: VIEW_ONLY_DELEGATE User views Posted Transaction Details for current account payment transactions with no payment option
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select CURRENT account tile from home page
    And I swipe to see previous month transactions on the account statement page
    And I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | date                    |
      | vtdAmount               |
      | <typeOfTransaction>     |
      | transactionDescription  |
      | unsureTransactionButton |
    And I should not see payment and transfer option
    Examples:
      | accessLevel        | transactionType          | typeOfTransaction     |
      | VIEW_ONLY_DELEGATE | faster Payments Outgoing | fasterPaymentsOutText |
      | VIEW_ONLY_DELEGATE | faster Payments Incoming | fasterPaymentsInText  |
      | VIEW_ONLY_DELEGATE | mobile Payments Inbound  | mobilePaymentInText   |
      | VIEW_ONLY_DELEGATE | mobile Payments Outgoing | mobilePaymentOutText  |
      | VIEW_ONLY_DELEGATE | transfers                | transferText          |
      | VIEW_ONLY_DELEGATE | bill Payment             | billPaymentText       |
      | VIEW_ONLY_DELEGATE | bank Giro Credit         | bankGiroCreditText    |
      | VIEW_ONLY_DELEGATE | cash Deposited In IDM    | cashText              |

  @android
  Scenario Outline: TC_009 Exception Scenario: User navigates to Payments and Transfers journey
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select CURRENT account tile from home page
    And I swipe to see previous month transactions on the account statement page
    And I select <transactionType> on statements page
    And I see posted transaction detail screen and select payments and transfer
    Then I should be on the payment hub page of that account
    Examples:
      | accessLevel          | transactionType          |
      | FULL_SIGNATORY       | faster Payments Outgoing |
      | FULL_ACCESS_DELEGATE | faster Payments Incoming |
      | FULL_SIGNATORY       | mobile Payments Inbound  |
      | FULL_ACCESS_DELEGATE | mobile Payments Outgoing |
      | FULL_SIGNATORY       | transfers                |
      | FULL_ACCESS_DELEGATE | bill Payment             |
      | FULL_SIGNATORY       | bank Giro Credit         |
      | FULL_ACCESS_DELEGATE | cash Deposited In IDM    |

  @android
  Scenario Outline: TC_010 Exception Scenario: User navigates back to VTD from payments and transfers journey
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select CURRENT account tile from home page
    And I swipe to see previous month transactions on the account statement page
    And I select <transactionType> on statements page
    And I see posted transaction detail screen and select payments and transfer
    And I see payment hub page and tap on close icon
    Then I should be on the statements page
    Examples:
      | accessLevel          | transactionType          |
      | FULL_SIGNATORY       | faster Payments Outgoing |
      | FULL_ACCESS_DELEGATE | mobile Payments Inbound  |

  #Test data/Stub is not available. Need to test on live account
  @manual
  Scenario Outline: TC_011 Exception Scenario: User navigates to Standing Order and Direct Debit journey from VTD screen
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select CURRENT account tile from home page
    And I swipe to see previous month transactions on the account statement page
    And I select <transactionType> on statements page
    And I tap on <buttonType> in vtd screen
    Then I should be on the <orderType> web view page
    Examples:
      | accessLevel          | transactionType | buttonType                | orderType     |
      | FULL_SIGNATORY       | standing Order  | manageStandingOrderButton | standingOrder |
      | FULL_ACCESS_DELEGATE | direct Debit    | viewDirectDebit           | directDebit   |
      | VIEW_ONLY_DELEGATE   | standing Order  | manageStandingOrderButton | standingOrder |

  #Test data/Stub is not available. Need to test on live account
  @manual
  Scenario Outline: TC_012 Exception Scenario: User navigates back from Standing Order and Direct Debit
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select CURRENT account tile from home page
    And I swipe to see previous month transactions on the account statement page
    And I select <transactionType> on statements page
    And I tap on <buttonType> in vtd screen
    And I navigate back from standing order and direct debit page
    Then I should be displayed VTD of that transaction
    Examples:
      | accessLevel          | transactionType | buttonType                |
      | FULL_SIGNATORY       | standing Order  | manageStandingOrderButton |
      | FULL_ACCESS_DELEGATE | direct Debit    | viewDirectDebit           |
      | VIEW_ONLY_DELEGATE   | standing Order  | manageStandingOrderButton |

  @manual
  Scenario: TC_013 Exception Scenario:  User views VTD message for no network (MG-208)
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I select cash Withdrawal on statements page with no network available
    Then I should be displayed a message with yellow banner

  @android
  Scenario: TC_014 Exception Scenario: User closes Posted Transaction Details
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I select CURRENT account tile from home page
    And I select one of the posted transaction
    And I close the posted transactions
    Then I should be on the allTab of statements page

  Scenario: TC_015_View google maps for Chip and Pin Card Purchase with Cash Back Posted Transaction Details fields (Chip and PIN Purchase - Cashback)
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I select CURRENT account tile from home page
    And I swipe to see previous month transactions on the account statement page
    And I select chip And PIN Purchase With Cash Back on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | date                    |
      | vtdCloseButton          |
      | vtdAmount               |
      | cashBack                |
      | chipAndPinText          |
      | payeeField              |
      | dateOfTransaction       |
      | cardEndingNumber        |
      | cashBackReferenceField  |
      | googleMap               |
      | unsureTransactionButton |

  @manual
  Scenario: TC_016_Exception Scenario: User is displayed Google maps for chip and pin or contactless transaction
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I select CURRENT account tile from home page
    And I select a debit transaction
    And The authorisation method is Chip & Pin or Contactless
    And There is only one location response returned
    Then I will see a map displayed of the location for that transaction
    And A location pin will identify the location with address details of the merchant

  @manual
  Scenario: TC_017_Exception Scenario: User is not displayed Google maps for chip and pin or contactless transaction for multiple location results
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I select CURRENT account tile from home page
    And I select a debit transaction
    And the authorisation method is Chip & Pin or Contactless
    And there is multiple location responses returned
    Then I should not be displayed google maps for the transaction on the VTD

  Scenario: TC_018_Display Exchange Rate with no Additional Information
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I select CURRENT account tile from home page
    And I swipe to see previous month transactions on the account statement page
    And I select exchange Rate With No Additional Information on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | date                    |
      | vtdCloseButton          |
      | vtdAmount               |
      | transferText            |
      | payeeField              |
      | exchangeRate            |
      | transferPaymentsButton  |
      | unsureTransactionButton |
    And I should not see additional information on VTD page

  Scenario: TC_019_No Rate or Additional information shown
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I select CURRENT account tile from home page
    And I swipe to see previous month transactions on the account statement page
    And I select no Exchange Rate Or Additional Information on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | date                    |
      | vtdCloseButton          |
      | vtdAmount               |
      | transferText            |
      | payeeField              |
      | transferPaymentsButton  |
      | unsureTransactionButton |
    And I should not see exchange rate or additional information on VTD page

  Scenario: TC_020_Display Exchange Rate and additional information
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I select CURRENT account tile from home page
    And I swipe to see previous month transactions on the account statement page
    And I select exchange Rate And Additional Information on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen        |
      | date                        |
      | vtdCloseButton              |
      | vtdAmount                   |
      | transferText                |
      | payeeField                  |
      | payeeFieldAdditionalDetails |
      | exchangeRate                |
      | transferPaymentsButton      |
      | unsureTransactionButton     |

  Scenario: TC_021_Display additional information and no Exchange Rate
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I select CURRENT account tile from home page
    And I swipe to see previous month transactions on the account statement page
    And I select additional Information And No Exchange Rate on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen        |
      | date                        |
      | vtdCloseButton              |
      | vtdAmount                   |
      | transferText                |
      | payeeField                  |
      | payeeFieldAdditionalDetails |
      | transferPaymentsButton      |
      | unsureTransactionButton     |
    And I should not see exchange rate on VTD page

  Scenario: TC_022_Display date of the account is debited when transaction is processed on a non-working day (7 day clearing - weekend / bank holidays)
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I select CURRENT account tile from home page
    When I select account Debited on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | date                    |
      | vtdCloseButton          |
      | vtdAmount               |
      | onlinePhoneMailText     |
      | payeeField              |
      | dateOfTransaction       |
      | cardEndingNumber        |
      | accountDebitedDate      |
      | unsureTransactionButton |

  Scenario: TC_023_Display date of the account is credited when transaction is processed on a non-working day (7 day clearing - weekend / bank holidays)
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I select CURRENT account tile from home page
    When I select refund on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | date                    |
      | vtdCloseButton          |
      | vtdAmount               |
      | onlinePhoneMailText     |
      | payeeField              |
      | dateOfTransaction       |
      | cardEndingNumber        |
      | accountCreditedDate     |
      | unsureTransactionButton |

  Scenario: TC_024_Do not display date of the account is credited when transaction is processed on a non-working day (7 day clearing - weekend / bank holidays)
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I select CURRENT account tile from home page
    When I select cash Withdrawal In ATM on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | date                    |
      | vtdCloseButton          |
      | vtdAmount               |
      | unsureTransactionButton |
      | cashPointText           |
      | payeeField              |
      | payeeFieldCardNumber    |
      | dateOfTransaction       |
      | cardEndingNumber        |
    And I should see no other date of account debited or credited
