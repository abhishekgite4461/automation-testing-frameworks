@team-statements @nga3 @bnga @stub-enrolled-valid @stubOnly
Feature: Payments Due Soon
  In order that I am better informed about my outgoing payments
  As a BNGA user
  I want to view payments due soon

  @primary @sanity
  Scenario Outline: TC_001 Main Success Scenario: User views payments due soon for current account via contextual menu on home screen
    Given I am on home screen as FULL_SIGNATORY access user with <type> account
    And I navigate to <type> account on home page
    And I select viewPaymentsDueSoon option in the action menu of <type> account
    Then I should see dueSoon tab in statements screen
    And I should see following fields in due soon page
      | fieldsToValidate            |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    And I should not see payments due soon option on the action menu
    When I select one of the pending payment transaction
    Then I should see upcoming transaction details for that transaction with the following fields
      | fieldsInDetailScreen  |
      | dueSoonLozenge        |
      | datePendingPayment    |
      | vtdCloseButton        |
      | vtdAmount             |
      | outgoingPaymentText   |
      | payeeField            |
      | sortCodeAccountNumber |
      | referenceField        |
      | changeOrDeleteText    |
    Examples:
      | type                  |
      | CURRENT |
      | SAVINGS |

  Scenario Outline: TC_002 Entry Point Scenario: User views payment due soon via contextual menu on statement screen
    Given I am on home screen as <accessLevel> access user with <type> account
    And I select <type> account tile from home page
    When I select the viewPaymentsDueSoon option from the action menu in statements page
    Then I should be displayed the statement page with all the outgoing payments due in the next 30 days
    Examples:
      | accessLevel          | type                  |
      | FULL_SIGNATORY       | CURRENT               |
      | FULL_ACCESS_DELEGATE | SAVINGS |

  Scenario Outline: TC_003 Exceptional Scenario: User views payments due soon but has no payments due in next 30 days (HC- 050)
    Given I am on home screen as FULL_SIGNATORY access user with <type> account
    And I select <type> account tile from home page
    When I select the viewPaymentsDueSoon option from the action menu in statements page
    Then I should see dueSoon tab in statements screen
    And I should see no pending payments message in statements screen
    Examples:
      | type                         |
      | CURRENT_WITH_NO_TRANSACTIONS |
      | SAVINGS_WITH_NO_TRANSACTIONS |

  ###### UI Validations
  Scenario Outline: TC-004: Payments due soon option should not be displayed in actions menu for non-eligible accounts on home screen
    Given I am on home screen as FULL_SIGNATORY access user with <type> account
    When I navigate to <type> account on home page
    Then I should not see payments due soon option on the action menu of <type> account
    Examples:
      | type                              |
      | CREDITCARD                        |
      | SAVINGS_NOT_ELIGIBLE_FOR_PAYMENTS |

  Scenario Outline: TC_005: Payments due soon option should not be displayed in actions menu for non-eligible accounts on statements page
    Given I am on home screen as FULL_SIGNATORY access user with <type> account
    When I select <type> account tile from home page
    Then I should not see payments due soon option on the action menu
    Examples:
      | type                              |
      | CREDITCARD                        |
      | SAVINGS_NOT_ELIGIBLE_FOR_PAYMENTS |

  Scenario: TC_006: Payments due soon option should not be displayed in actions menu once user navigates to current month from due soon tab
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I select CURRENT account tile from home page
    And I select the viewPaymentsDueSoon option from the action menu in statements page
    And I swipe to see current month transactions on the account statement page
    Then I should not see payments due soon option on the action menu

  Scenario: TC_007: Close Pending Transaction Details With Close Button
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I select CURRENT account tile from home page
    And I select the viewPaymentsDueSoon option from the action menu in statements page
    And I select one of the pending payment transaction
    And I close the pending transactions VTD
    Then I should see dueSoon tab in statements screen
