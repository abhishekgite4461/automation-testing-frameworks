@team-statements @nga3 @bnga @stub-enrolled-valid @stubOnly
Feature: Saving Interest Rate Details
  As a NGA Customer
  I should be able to view interest rate for my saving account
  So that I am aware of the interest rate on my saving account

  #MOB3-13522, 13576
  @primary @sanity
  Scenario: TC_001 Main Success Scenario: User views single tier interest rate on home page account tile when hide interest rate error flag is false
    Given I am on home screen as FULL_SIGNATORY access user with SAVINGS account
    And I navigate to SAVINGS account on home page
    When I tap and hold on the SAVINGS account tile to Savings Interest Rate Details screen
    Then I should be on Savings Interest Rate Details screen
    And I should see the following fields in the details screen
      | fieldsInDetailsScreen   |
      | accountName             |
      | accountBalance          |
      | interestRateLabel       |
      | interestRateDescription |

  @ios
  Scenario: TC_002 Entry Point Scenario: User views single tier interest rate on statement screen account tile when hide interest rate error flag is false
    Given I am on home screen as FULL_SIGNATORY access user with SAVINGS account
    And I select SAVINGS account tile from home page
    When I tap and hold on the SAVINGS account tile to Savings Interest Rate Details screen
    Then I should be on Savings Interest Rate Details screen
    When I close the interest rate details screen using the close button
    Then The savings interest rate details screen should be closed
    And I should be on the selected SAVINGS account tile on the home page

  @android
  Scenario: TC_003 Entry Point Scenario: User views single tier interest rate on statement screen account tile when hide interest rate error flag is false
    Given I am on home screen as FULL_SIGNATORY access user with SAVINGS account
    When I select SAVINGS account tile from home page
    Then I should see the Interest rate next to Interest rate label on account tile

  Scenario: TC_004 Entry Point Scenario: User views single tier interest rate from actions menu on home page when hide interest rate error flag is false
    Given I am on home screen as FULL_SIGNATORY access user with SAVINGS account
    And I navigate to SAVINGS account on home page
    When I select View Interest Details option of SAVINGS account from action menu
    Then I should be on Savings Interest Rate Details screen
    When I close the interest rate details screen using the close button
    Then The savings interest rate details screen should be closed
    And I should be on the selected SAVINGS account tile on the home page

  Scenario: TC_005 Entry Point Scenario: User views single tier interest rate on by actions menu on statement screen when hide interest rate error flag is false
    Given I am on home screen as FULL_SIGNATORY access user with SAVINGS account
    When I select SAVINGS account tile from home page
    And I select the View Interest Details option from the action menu in statements page
    Then I should be on Savings Interest Rate Details screen
    When I close the interest rate details screen using swipe action
    Then The savings interest rate details screen should be closed
    And I should be on the statements page of the selected SAVINGS account

  @manual
  Scenario: TC_006 Exception Scenario: User views two tier interest rates
    Given I am on home screen as FULL_SIGNATORY access user with SAVINGS account
    When I select SAVINGS account tile from home page
    And I select View Interest Details option of SAVINGS account from action menu
    Then I should be on Savings Interest Rate Details screen
    And I should see the following fields in the details screen
      | fieldsInDetailsScreen     |
      | accountName               |
      | accountBalance            |
      | interestRateForFirstTier  |
      | interestRateForSecondTier |

  Scenario: TC_007 Exception Scenario: User Views multi-tier (three tier) interest rates
    Given I am on home screen as FULL_SIGNATORY access user with SAVINGS_DORMANT account
    And I select SAVINGS_DORMANT account tile from home page
    When I select the View Interest Details option from the action menu in statements page
    Then I should be on Savings Interest Rate Details screen
    And I should see the following fields in the details screen
      | fieldsInDetailsScreen     |
      | accountName               |
      | accountBalance            |
      | interestRateForFirstTier  |
      | interestRateForSecondTier |
      | interestRateForThirdTier  |

  @ios
  Scenario: TC_008 Exception Scenario: User Views multi-tier (three tier) interest rates
    Given I am on home screen as FULL_SIGNATORY access user with SAVINGS_DORMANT account
    And I select SAVINGS_DORMANT account tile from home page
    When I select View Interest Details option of SAVINGS_DORMANT account from action menu
    Then I should be on Savings Interest Rate Details screen
    And I should see the following fields in the details screen
      | fieldsInDetailsScreen     |
      | accountName               |
      | accountBalance            |
      | interestRateForFirstTier  |
      | interestRateForSecondTier |
      | interestRateForThirdTier  |

  @android
  Scenario: TC_009 Exception Scenario: User Views multi-tier (three tier) interest rates
    Given I am on home screen as FULL_SIGNATORY access user with SAVINGS_DORMANT account
    And I select SAVINGS_DORMANT account tile from home page
    When I tap on View rates link
    Then I should be on Savings Interest Rate Details screen
    And I should see the following fields in the details screen
      | fieldsInDetailsScreen     |
      | accountName               |
      | accountBalance            |
      | interestRateForFirstTier  |
      | interestRateForSecondTier |
      | interestRateForThirdTier  |

  Scenario: TC_010 Exception Scenario: User does not see interest rate on account tile on home page when hide interest rate error flag is true
    Given I am on home screen as FULL_SIGNATORY access user with SAVINGS_WITH_NO_TRANSACTIONS account
    And I navigate to SAVINGS_WITH_NO_TRANSACTIONS account on home page
    When I tap and hold on the SAVINGS_WITH_NO_TRANSACTIONS account tile to Savings Interest Rate Details screen
    Then I should not be on Savings Interest Rate Details screen

  @ios
  Scenario: TC_011 Exception Scenario: User does not see interest rate on account tile on statement page when hide interest rate error flag is true
    Given I am on home screen as FULL_SIGNATORY access user with SAVINGS_WITH_NO_TRANSACTIONS account
    When I select SAVINGS_WITH_NO_TRANSACTIONS account tile from home page
    And I tap and hold on the SAVINGS_WITH_NO_TRANSACTIONS account tile to Savings Interest Rate Details screen
    Then I should not be on Savings Interest Rate Details screen

  Scenario: TC_012 Exception Scenario: User does not see view interest rate entry point in actions menu on home page
    Given I am on home screen as FULL_SIGNATORY access user with SAVINGS_WITH_NO_TRANSACTIONS account
    When I navigate to SAVINGS_WITH_NO_TRANSACTIONS account on home page
    Then I should not see view interest rate details option in the action menu of SAVINGS_WITH_NO_TRANSACTIONS account

  Scenario: TC_013 Exception Scenario: User does not see view interest rate entry point in actions menu on statement page
    Given I am on home screen as FULL_SIGNATORY access user with SAVINGS_WITH_NO_TRANSACTIONS account
    When I select SAVINGS_WITH_NO_TRANSACTIONS account tile from home page
    And I should be on the current month statements tab
    Then I should not see the view interest rate details option from the action menu in the statement page

  Scenario Outline: TC_014 Alternate Scenario: User views interest rate value on home page account tile for all when hide interest rate error flag is false
    Given I am on home screen as FULL_SIGNATORY access user with <type> account
    When I navigate to <type> account on home page
    Then I should see <output> interest rate value on account tile
    Examples:
      | type                         | output       |
      | SAVINGS_WITH_NO_TRANSACTIONS | notAvailable |
      | SAVINGS                      | percent      |
      | SAVINGS_DORMANT              | tapAndHold   |

  @ios
  Scenario Outline: TC_015 Alternate Scenario: User views interest rate value on statement page account tile for single tier when hide interest rate error flag is false
    Given I am on home screen as FULL_SIGNATORY access user with <type> account
    When I select <type> account tile from home page
    Then I should see <output> interest rate value on account tile
    Examples:
      | type                         | output       |
      | SAVINGS_WITH_NO_TRANSACTIONS | notAvailable |
      | SAVINGS                      | percent      |
      | SAVINGS_DORMANT              | tapAndHold   |

  @android
  Scenario Outline: TC_016 Alternate Scenario: User views interest rate value on statement page account tile for single tier when hide interest rate error flag is false
    Given I am on home screen as FULL_SIGNATORY access user with <type> account
    When I select <type> account tile from home page
    Then I should see <output> interest rate value on account tile
    Examples:
      | type                         | output        |
      | SAVINGS_WITH_NO_TRANSACTIONS | notAvailable  |
      | SAVINGS                      | percent       |
      | SAVINGS_DORMANT              | viewRatesLink |
