@team-statements @nga3 @bnga @stub-enrolled-valid @stubOnly
Feature: Fixed Term Deposit
  In order that I can am aware of my fixed term deposit accounts
  As a BNGA authenticated user,
  I want to be able to view the details of my fixed term deposit accounts

  #MOB3-14590
  @primary
  Scenario Outline: TC_001 Main Success Scenario: User views details of fixed term deposit account with one settlement account
    Given I am on home screen as <accessLevel> access user with FIXED_TERM_DEPOSIT account
    And I select FIXED_TERM_DEPOSIT account tile from home page
    Then I should be displayed the fixed term deposit account page with the following details
      | fieldsInTreasuryAccount                |
      | aerGrossInterestRate                   |
      | nominatedAccount                       |
      | basicTaxRateStatus                     |
      | accountOpenedOn                        |
      | currentMaturityInstructions            |
      | dateInstructionReceived                |
      | maturityDate                           |
      | daysToMaturity                         |
      | estimatedGrossInterestForTermOfDeposit |
      | aerInfoLink                            |
      | nominatedAccountInfoLink               |
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  @primary
  Scenario Outline: TC_002 Variation Scenario: User views details of fixed term deposit account with multiple settlement account (MG3102)
    Given I am on home screen as <accessLevel> access user with FIXED_TERM_DEPOSIT_WITH_MULTIPLE account
    And I select FIXED_TERM_DEPOSIT_WITH_MULTIPLE account tile from home page
    Then I should be displayed the fixed term deposit account page with the following details
      | fieldsInTreasuryAccount                |
      | multipleSettlementAccountTextMsg       |
      | aerGrossInterestRate                   |
      | basicTaxRateStatus                     |
      | accountOpenedOn                        |
      | currentMaturityInstructions            |
      | dateInstructionReceived                |
      | maturityDate                           |
      | daysToMaturity                         |
      | estimatedGrossInterestForTermOfDeposit |
      | aerInfoLink                            |
      | nominatedAccountInfoLink               |
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  #MOB3-14652
  Scenario Outline: TC_003 Exception Scenario: User views AER & Settlement Account details
    Given I am on home screen as FULL_SIGNATORY access user with FIXED_TERM_DEPOSIT account
    And I select FIXED_TERM_DEPOSIT account tile from home page
    When I view the <link> details
    Then I must be displayed <type> overlay giving me information about gross interest rate
    And I select close button in info page
    And I should be navigated back to treasury account page
    Examples:
      | link             | type             |
      | aer              | aer              |
      | nominatedAccount | nominatedAccount |

  Scenario: TC_004 Exception Scenario: User is shown a message (MG-3101) that the information for treasury accounts cannot be displayed
    Given I am on home screen as FULL_SIGNATORY access user with FIXEDTERMDEPOSIT_PRODUCT_NOT_AVAILABLE account
    And I select FIXEDTERMDEPOSIT_PRODUCT_NOT_AVAILABLE account tile from home page
    Then I must be displayed a message informing that the product is not currently available
