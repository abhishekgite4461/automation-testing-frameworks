@team-statements @nga3 @bnga @stub-enrolled-valid @stubOnly
Feature: Credit and Charge card statements
  In order that I can view my statement balance, payment due on my mobile device
  As a BNGA user
  I want to see my business card account(s) statement summary

  #MOB3-2707, COSE-175
  @primary
  Scenario Outline: TC-001 Main Success Scenario: User views recent transactions for credit cards and charge cards
    Given I am on home screen as FULL_SIGNATORY access user with <type> account
    When I select <type> account tile from home page
    Then I should be displayed recent transactions with following fields
      | fieldsInRecentTransaction  |
      | nextStatementDueDate       |
      | optionToFilterTransactions |
      | transactionsSinceMessage   |
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | transactionType             |
      | firstTranscationAmount      |
    And I should scroll down to the end of the transactions to view <output> message
    Examples:
      | type       | output      |
      | CREDITCARD | creditEndOf |
      | CHARGECARD | chargeEndOf |

  @primary
  Scenario: TC-002 Main Success Scenario: User views immediate last month statement for credit cards
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD account
    When I select CREDITCARD account tile from home page
    And I navigate to previous 1 months transactions on the account statement page
    Then I should be displayed last month statement with the following fields in statements summary
      | fieldsInStatementsSummary   |
      | statementDateDescription    |
      | statementBalanceDescription |
      | minimumPaymentDescription   |
      | dueDateDescription          |
      | optionToFilterTransactions  |
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | transactionType             |
      | firstTranscationAmount      |
    And I should scroll down to the end of the transactions to view creditEndOf message

  @primary
  Scenario: TC-003 Main Success Scenario: User views last month’s statement for charge cards
    Given I am on home screen as FULL_SIGNATORY access user with CHARGECARD account
    When I select CHARGECARD account tile from home page
    And I navigate to previous 1 months transactions on the account statement page
    Then I should be displayed last month statement with the following fields in statements summary
      | fieldsInStatementsSummary    |
      | statementDateDescription     |
      | statementBalanceDescription  |
      | minimumPaymentDescription    |
      | dueDateDescription           |
      | optionToFilterTransactions   |
      | directDebitAmountDescription |
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | transactionType             |
      | firstTranscationAmount      |
    And I should scroll down to the end of the transactions to view creditEndOf message

  @primary
  Scenario Outline: TC-004 Main Success Scenario: User views previous months statements
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD account
    When I select <type> account tile from home page
    And I navigate to previous 3 months transactions on the account statement page
    Then I should scroll down to the end of the transactions to view <output> message
    Examples:
      | type                                       | output      |
      | CREDITCARD_WITH_ACCOUNT_LEVEL_TRANSACTIONS | creditEndOf |
      | CHARGECARD                                 | chargeEndOf |

  # No test data
  @pending
  Scenario: TC-005 Variation Scenario: User views information about statement balance – charge cards only
    Given I am on home screen as FULL_SIGNATORY access user with CHARGECARD account
    And I select CHARGECARD account tile from home page
    And I am displayed last month statement
    When I view information about statement balance
    Then I should be displayed the information about statement balance overlay
    When I close about statement balance overlay
    Then The about statement balance overlay must be closed

  @primary
  Scenario Outline: TC-006 Variation Scenario: User views filtered transactions for card ending
    Given I am on home screen as FULL_SIGNATORY access user with <type> account
    And I select <type> account tile from home page
    When I filter the transactions to view <typeFilter> transaction for <month>
    Then I should see following fields for <typeFilter> in statements summary
      | fieldsInStatementsSummary |
      | clearFilter               |
      | typeCardFilter            |
      | cardEndingTransactionTotal|
    And I should be displayed the <typeFilter> transactions for the month selected
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | transactionType             |
      | firstTranscationAmount      |
    And I should not be able to navigate to other months and accounts
    Examples:
      | type       | typeFilter | month    |
      | CREDITCARD | cardEnding | recent   |
      | CREDITCARD | cardEnding | previous |
      | CHARGECARD | cardEnding | recent   |
      | CHARGECARD | cardEnding | previous |

  @primary @sanity
  Scenario Outline: TC-007 Variation Scenario: User views filtered transactions for account level transactions
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD account
    And I select <type> account tile from home page
    When I filter the transactions to view <typeFilter> transaction for <month>
    Then I should see following fields for <typeFilter> in statements summary
      | fieldsInStatementsSummary |
      | clearFilter               |
      | typeAccountFilter         |
    And I should be displayed the <typeFilter> transactions for the month selected
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | transactionType             |
      | firstTranscationAmount      |
    And I should not be able to navigate to other months and accounts
    Examples:
      | type       | typeFilter | month    |
      | CREDITCARD | account    | recent   |
      | CREDITCARD | account    | previous |
      | CHARGECARD | account    | recent   |
      | CHARGECARD | account    | previous |

  @secondary
  Scenario Outline: TC-008 Variation Scenario: User clears filtered transactions
    Given I am on home screen as FULL_SIGNATORY access user with <type> account
    And I select <type> account tile from home page
    And I filter the transactions to view <typeFilter> transaction for <month>
    When I clear the filter
    Then I should be displayed all transactions for the selected month
    Examples:
      | type       | typeFilter | month    |
      | CREDITCARD | account    | recent   |
      | CREDITCARD | cardEnding | previous |
      | CHARGECARD | account    | recent   |
      | CHARGECARD | account    | previous |

  Scenario Outline: TC-009 Exception Scenario: User tries to view transactions older than 6 months & is displayed message HC212
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD account
    And I select <type> account tile from home page
    When I navigate to previous 7 months transactions on the account statement page
    Then I should be displayed that online statements only go back 6 months message
    Examples:
      | type       |
      | CREDITCARD |
      | CHARGECARD |

  # test data specific
  @manual
  Scenario Outline: TC-010 Exception Scenario: User tries to view transactions older than account opening month
    Given I login to app as a "credit card" user
    And I select fullSignatoryBusiness business with full Signatory access from the business selection screen
    When I select <type> account tile from home page
    And I navigate to a month older than account opening month
    Then I should not see the calendar month older than account opening month
    Examples:
      | type       |
      | CREDITCARD |
      | CHARGECARD |

  Scenario Outline: TC-011 Exception Scenario (Entry Point): User does not have view transactions options in actions menu in statements page
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD account
    When I select <type> account tile from home page
    And I select the action menu from statements page
    Then I should not see view transaction option
    Examples:
      | type       |
      | CREDITCARD |
      | CHARGECARD |

  Scenario Outline: TC-012 Entry Point Scenario: User navigates to credit/charge card statements from view transactions menu option in actions menu on home page
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD account
    When I navigate to <type> account on home page
    And I select the "view transaction" option in the action menu of <type> account
    Then I should be displayed recent transactions
    Examples:
      | type       |
      | CREDITCARD |
      | CHARGECARD |

  #COSE-179
  Scenario Outline: TC-013 User views filter
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD account
    When I select <type> account tile from home page
    And I view the filter
    Then I should be displayed the filter overlay
    And I should be displayed the following options
      | filterOptions              |
      | accountTransactions        |
      | cardEnding                 |
      | cardEndingTransactionTotal |
      | cancelButton               |
    When I close the filter
    Then The filer must be closed and all transactions should be displayed for the month
    Examples:
      | type       |
      | CREDITCARD |
      | CHARGECARD |

  #MOB3-13357, COSE-177
  Scenario Outline: TC-014 Exception Scenario: User views statement for month that has no transactions & is displayed message HC018
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD account
    And I select <type> account tile from home page
    And I view <month> transactions of <type> account
    Then I should not see any transaction
    And I should not see the filter button
    And I should see <output> no transactions message in the month tab
    Examples:
      | type                            | month    | output     |
      | CREDITCARD_WITH_NO_TRANSACTIONS | recent   | CREDITCARD |
      | CREDITCARD_WITH_NO_TRANSACTIONS | previous | CREDITCARD |

  Scenario: TC-015 Exception Scenario: User has only account-level transactions (HC 055)
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD account
    And I select CREDITCARD_WITH_ACCOUNT_LEVEL_TRANSACTIONS account tile from home page
    When I view the filter
    Then I should see a message stating that there are no card holder transactions to filter
    When I close the error message dialog box
    Then I should be displayed recent transactions

  Scenario Outline: TC-016 Exception Scenario (Entry Point): User does not have view transactions options in actions menu in statements page
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD account
    When I select <type> account tile from home page
    And I select the action menu from statements page
    Then I should not see view transaction option
    Examples:
      | type       |
      | CREDITCARD |
      | CHARGECARD |

  Scenario: TC-017 Exception Scenario: User has too many transactions (HC 056)
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD account
    When I select CREDITCARD_WITH_TOO_MANY_TRANSCATIONS account tile from home page
    And I view the filter
    Then I should see a message stating that the filter cannot be used as there are too many transactions to display on the page
    When I close the error message dialog box
    Then I should be displayed recent transactions
