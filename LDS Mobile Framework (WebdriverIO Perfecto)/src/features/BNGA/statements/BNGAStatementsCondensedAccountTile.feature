@team-statements @nga3 @bnga @stub-enrolled-valid @android
Feature: Condensed Account Tile Statements
  As a NGA commercial customer,
  I want my account balances to show in the account tile on my statements page
  So that I can understand what monies are available

  #STAMOB-836,842
  Scenario Outline: TC_001_Display current account information on account tile and header in statements page
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I navigate to CURRENT account to validate accountNumber and select current account
    And I should be on the allTab of statements page
    Then I should be displayed CURRENT account name action menu and last four digits of account number in the header
    And I should not be displayed business name on the account tile
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
      | availableFunds      |
      | overDraftLimit      |
    Examples:
      | accessLevel          |
      | FULL_ACCESS_DELEGATE |
      | FULL_SIGNATORY       |

  #STAMOB-884,879
  Scenario Outline: TC_002_Display credit card account information on account tile and header in statements page
    Given I am on home screen as <accessLevel> access user with <accountName> account
    When I navigate to <accountName> account to validate accountNumber and select <type> account
    And I should be on the recent of statements page
    Then I should be displayed <accountName> account name action menu and last four digits of creditCard number in the header
    And I should not be displayed business name on the account tile
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
      | availableCredit     |
      | creditLimit         |
      | overDueAmount       |
    Examples:
      | accessLevel          | accountName | type       |
      | FULL_ACCESS_DELEGATE | CREDITCARD  | creditCard |
      | VIEW_ONLY_DELEGATE   | CHARGECARD  | chargeCard |

  #STAMOB-836, 842
  Scenario Outline: TC_003_Display savings account information on account tile and header in statements page
    Given I am on home screen as <accessLevel> access user with SAVINGS account
    When I navigate to SAVINGS account to validate accountNumber and select saving account
    And I should be on the currentMonth of statements page
    Then I should be displayed SAVINGS account name action menu and last four digits of account number in the header
    And I should not be displayed business name on the account tile
    And I should see account details with the following fields
      | fieldsInAccountTile     |
      | accountBalance          |
      | availableFunds          |
      | interestRateLabel       |
      | interestRateDescription |
    Examples:
      | accessLevel        |
      | FULL_SIGNATORY     |
      | VIEW_ONLY_DELEGATE |

  #STAMOB-903,925
  Scenario Outline: TC_004_Display treasury accounts information on account tile and header in statements page
    Given I am on home screen as <accessLevel> access user with <type> account
    And I navigate to <type> account to validate accountNumber and select <type> account
    Then I should be displayed <type> account name action menu and last four digits of account number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile    |
      | accountBalance         |
      | grossInterestRateLabel |
      | <warningAlert>         |
    Examples:
      | accessLevel          | type                                 | warningAlert   |
      | FULL_ACCESS_DELEGATE | FIXED_TERM_DEPOSIT                   | noWarningAlert |
      | VIEW_ONLY_DELEGATE   | FIXED_TERM_DEPOSIT_WITH_MULTIPLE     | warningAlert   |
      | FULL_ACCESS_DELEGATE | TREASURY_32DAYNOTICE                 | warningAlert   |
      | FULL_SIGNATORY       | TREASURY_32DAYNOTICE_WITH_SETTLEMENT | warningAlert   |

  #STAMOB-906
  @pending
  Scenario Outline: TC_005_Display current balance in the header for treasury deposits when user scroll up in statements page
    Given I am on home screen as <accessLevel> access user with <type> account
    And I select <type> account tile from home page
    When I scroll down on the transactions
    Then I should be displayed current balance in the header alongside account Name
    And I should not be displayed account tile in statements page
    When I scroll back to the top of my transactions
    Then The current balance will no longer display in the header and the account tile will reappear
    Examples:
      | accessLevel          | type                             |
      | FULL_ACCESS_DELEGATE | FIXED_TERM_DEPOSIT               |
      | VIEW_ONLY_DELEGATE   | FIXED_TERM_DEPOSIT_WITH_MULTIPLE |

  #STAMOB-841,886,842,887,905,906,927,930
  Scenario Outline: TC_006_Display current balance in the header when user scroll up in statements page
    Given I am on home screen as <accessLevel> access user with <type> account
    When I select <type> account tile from home page
    And I should be on the <monthsTab> of statements page
    And I scroll down on the transactions
    Then I should be displayed current balance in the header alongside account Name
    And I should not be displayed account tile in statements page
    And I should be displayed month tabs in place of account tile
    When I scroll back to the top of my transactions
    Then The current balance will no longer display in the header and the account tile will reappear
    Examples:
      | accessLevel          | type       | monthsTab    |
      | FULL_ACCESS_DELEGATE | CREDITCARD | recent       |
      | VIEW_ONLY_DELEGATE   | CURRENT    | allTab       |
      | FULL_SIGNATORY       | SAVINGS    | currentMonth |

  #STAMOB-971
  @pending
  Scenario Outline: TC_007_User able to access the action menu in statements page
    Given I am on home screen as <accessLevel> access user with <type> account
    And I select <type> account tile from home page
    Then I should be on the <monthsTab> of statements page
    When I select the action menu from statements page
    Then I should be displayed action menu with list of options
    Examples:
      | accessLevel          | type       | monthsTab    |
      | FULL_ACCESS_DELEGATE | CURRENT    | recent       |
      | VIEW_ONLY_DELEGATE   | SAVINGS    | allTab       |
      | FULL_SIGNATORY       | CREDITCARD | currentMonth |

  @secondary
  Scenario Outline: TC_008_View loading of transactions for previous months
    Given I am on home screen as <accessLevel> access user with <type> account
    And I select <type> account tile from home page
    And I scroll down on the transactions
    And I swipe to see previous month transactions on the account statement page
    Then I should be on the same previous month statements page
    And I should not be displayed account tile in statements page
    Examples:
      | accessLevel          | type       |
      | FULL_ACCESS_DELEGATE | SAVINGS    |
      | VIEW_ONLY_DELEGATE   | CREDITCARD |
      | FULL_SIGNATORY       | CURRENT    |
