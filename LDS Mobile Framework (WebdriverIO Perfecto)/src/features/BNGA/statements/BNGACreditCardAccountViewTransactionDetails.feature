@team-statements @nga3 @bnga @stub-enrolled-valid @stubOnly
Feature: User views Current Account Transaction Details
  In order that I am better informed about a specific transaction
  As a BNGA user
  I should be able to view transaction details for my current account transactions

  #MOB3-12860
  @primary
  Scenario Outline: TC_001 Main Success Scenario: User views Posted Transaction Details for credit card transactions
    Given I am on home screen as <accessLevel> access user with <type> account
    When I select <type> account tile from home page
    And I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | date                    |
      | vtdAmount               |
      | <typeOfTransaction>     |
      | transactionDescription  |
      | dateOfTransaction       |
      | cardEndingNumber        |
      | unsureTransactionButton |
    Examples:
      | accessLevel          | type                | transactionType                   | typeOfTransaction       |
      | FULL_SIGNATORY       | CREDITCARD_WITH_VTD | contact Less Purchase Credit Card | contactLessPurchaseText |
      | FULL_ACCESS_DELEGATE | CREDITCARD_WITH_VTD | chip And Pin Purchase Credit Card | chipAndPinText          |
      | VIEW_ONLY_DELEGATE   | CREDITCARD_WITH_VTD | manual Or Paper Credit Card       | manualPaperText         |
      | FULL_SIGNATORY       | CREDITCARD_WITH_VTD | online Credit Card                | onlinePhoneMailText     |
      | FULL_ACCESS_DELEGATE | CREDITCARD_WITH_VTD | interest Credit Card              | creditCardText          |
      | VIEW_ONLY_DELEGATE   | CREDITCARD_WITH_VTD | charges Credit Card               | creditCardText          |

  @manual @secondary
  Scenario Outline: TC_002 Main Success Scenario: User views Posted Transaction Details for credit card transactions
    Given I am on home screen as <accessLevel> access user with <type> account
    When I select <type> account tile from home page
    And I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | date                    |
      | vtdAmount               |
      | <typeOfTransaction>     |
      | transactionDescription  |
      | dateOfTransaction       |
      | cardEndingNumber        |
      | unsureTransactionButton |
    Examples:
      | accessLevel          | type                | transactionType                         | typeOfTransaction       |
      | FULL_SIGNATORY       | CREDITCARD_WITH_VTD | magnetic Stripe Credit Card             | contactLessPurchaseText |
      | FULL_ACCESS_DELEGATE | CREDITCARD_WITH_VTD | authorisation Method Not Available Card | creditCardText          |
      | VIEW_ONLY_DELEGATE   | CREDITCARD_WITH_VTD | foreign Currency Online Card            | onlinePhoneMailText     |

  Scenario Outline: TC_003 Main Success Scenario: User views Posted Transaction Details for credit card payment transactions
    Given I am on home screen as <accessLevel> access user with <type> account
    When I select <type> account tile from home page
    And I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | date                    |
      | vtdAmount               |
      | <typeOfTransaction>     |
      | transactionDescription  |
      | dateOfTransaction       |
      | cardEndingNumber        |
      | unsureTransactionButton |
    Examples:
      | accessLevel        | type                | transactionType                 | typeOfTransaction |
      | FULL_SIGNATORY     | CREDITCARD_WITH_VTD | direct Debit In Card            | creditCardText    |
      | VIEW_ONLY_DELEGATE | CREDITCARD_WITH_VTD | cheque Payment                  | creditCardText    |
      | VIEW_ONLY_DELEGATE | CREDITCARD_WITH_VTD | cash Withdrawal ATM Credit Card | chipAndPinText    |
      | FULL_SIGNATORY     | CREDITCARD_WITH_VTD | cash Payment At Branch          | creditCardText    |

  @manual
  Scenario Outline: TC_004 Main Success Scenario: User views Posted Transaction Details for credit card payment transactions
    Given I am on home screen as <accessLevel> access user with <type> account
    When I select <type> account tile from home page
    And I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | date                    |
      | vtdAmount               |
      | <typeOfTransaction>     |
      | transactionDescription  |
      | dateOfTransaction       |
      | cardEndingNumber        |
      | unsureTransactionButton |
    Examples:
      | accessLevel          | type                | transactionType                    | typeOfTransaction |
      | FULL_ACCESS_DELEGATE | CREDITCARD_WITH_VTD | money Transfer Credit Card         | creditCardText    |
      | VIEW_ONLY_DELEGATE   | CREDITCARD_WITH_VTD | cash Withdrawal Branch Credit Card | creditCardText    |
      | FULL_ACCESS_DELEGATE | CREDITCARD_WITH_VTD | bank Giro Payment                  | creditCardText    |
      | FULL_ACCESS_DELEGATE | CREDITCARD_WITH_VTD | electronic Payment                 | creditCardText    |
      | FULL_ACCESS_DELEGATE | CREDITCARD_WITH_VTD | balance Transfer                   | creditCardText    |
      | VIEW_ONLY_DELEGATE   | CREDITCARD_WITH_VTD | refunds                            | creditCardText    |
      | FULL_ACCESS_DELEGATE | CREDITCARD_WITH_VTD | rewards                            | creditCardText    |
      | VIEW_ONLY_DELEGATE   | CREDITCARD_WITH_VTD | cash Withdrawal Branch Credit Card | creditCardText    |

  @manual
  Scenario: TC_006 Exception Scenario:  User views VTD message for no network (MG-208)
    Given I am on home screen as FULL_ACCESS_DELEGATE access user with CREDITCARD_WITH_VTD account
    When I select cash Withdrawal on statements page with no network available
    Then I should be displayed a message with yellow banner

  #STAMOB-47
  Scenario Outline: TC_007 Main Success Scenario: User views Posted Transaction Details for credit card transactions
    Given I am on home screen as <accessLevel> access user with CREDITCARD_WITH_VTD account
    When I select CREDITCARD_WITH_VTD account tile from home page
    And I select contact Less Purchase Credit Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | date                    |
      | vtdAmount               |
      | contactLessPurchaseText |
      | transactionDescription  |
      | googleMap               |
      | dateOfTransaction       |
      | cardEndingNumber        |
      | unsureTransactionButton |
    Examples:
      | accessLevel          |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  @manual
  Scenario: TC_008_Exception Scenario: User is displayed Google maps for chip and pin or contactless transaction
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD_WITH_VTD account
    And I select CREDITCARD_WITH_VTD account tile from home page
    And I select a debit transaction
    And The authorisation method is Chip & Pin or Contactless
    And There is only one location response returned
    Then I will see a map displayed of the location for that transaction
    And A location pin will identify the location with address details of the merchant

  @manual
  Scenario: TC_009_Exception Scenario: User is not displayed Google maps for chip and pin or contactless transaction for multiple location results
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD_WITH_VTD account
    And I select CREDITCARD_WITH_VTD account tile from home page
    And I select a debit transaction
    And the authorisation method is Chip & Pin or Contactless
    And there is multiple location responses returned
    Then I should not be displayed google maps for the transaction on the VTD
