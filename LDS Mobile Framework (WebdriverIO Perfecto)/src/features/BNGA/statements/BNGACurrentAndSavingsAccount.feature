@team-statements @nga3 @bnga @stub-enrolled-valid @stubOnly
Feature: BNGA Current and Savings Account Statements
  As a BNGA authenticated customer with current & savings accounts,
  I want to be able to view statements for the nominated accounts & months

  #MOB3-11107,12829,11108
  @primary @sanity
  Scenario Outline: TC-001 Main Success Scenario: User views current month and previous months transactions for current and savings account
    Given I am on home screen as FULL_SIGNATORY access user with <type> account
    When I select <type> account tile from home page
    And I view <month> transactions of <type> account
    Then I should be displayed <month> statements transactions
    And I should see following fields in statements list
      | fieldsInStatementsList         |
      | firstTranscationDate           |
      | firstTranscationDescription    |
      | firstTransactionRunningBalance |
      | firstTranscationAmount         |
    And I should scroll down to the end of the transactions to view <output> message of that <month> tab
    Examples:
      | type    | month    | output        |
      | CURRENT | allTab   | currentInOuts |
      | CURRENT | current  | currentInOuts |
      | CURRENT | previous | currentInOuts |
      | SAVINGS | current  | savingEndOf   |
      | SAVINGS | previous | savingEndOf   |

  #MOB3-13074, 13075
  @primary
  Scenario Outline: TC-002 Main Success Scenario: User views dormant account message
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT_DORMANT account
    When I select <type> account tile from home page
    Then I should be displayed a message
    And I should not see any transactions
    Examples:
      | type            |
      | CURRENT_DORMANT |
      | SAVINGS_DORMANT |

  #MOB3-11107,12829,11108
  Scenario Outline: TC-003 Entry Point Scenario: User navigates to statements from view transactions menu option in actions menu on home page
    Given I am on home screen as FULL_SIGNATORY access user with <type> account
    When I navigate to <type> account on home page
    And I select the "view transaction" option in the action menu of <type> account
    Then I should be on the <tab> of statements page
    And I should see following fields in statements list
      | fieldsInStatementsList         |
      | firstTranscationDate           |
      | firstTranscationDescription    |
      | firstTranscationAmount         |
      | firstTransactionRunningBalance |
    Examples:
      | type    | tab          |
      | CURRENT | allTab       |
      | SAVINGS | currentMonth |

  Scenario Outline: TC-004 Exception Scenario (Entry Point): User does not have view transactions options in actions menu in statements page
    Given I am on home screen as FULL_SIGNATORY access user with <type> account
    When I select <type> account tile from home page
    And I select the action menu from statements page
    Then I should not see view transaction option
    Examples:
      | type    |
      | CURRENT |
      | SAVINGS |

  #MOB3-11220,11108
  @primary
  Scenario Outline: TC-005 Negative Scenario: User views statement for month that has no transactions & is displayed message HC018
    Given I am on home screen as FULL_SIGNATORY access user with <type> account
    When I select <type> account tile from home page
    And I view <month> transactions of <type> account
    And I should see <output> no transactions message in <month> tab of that account
    Examples:
      | type                         | month    | output          |
      | CURRENT_WITH_NO_TRANSACTIONS | allTab   | noCurrentInOuts |
      | CURRENT_WITH_NO_TRANSACTIONS | current  | current         |
      | CURRENT_WITH_NO_TRANSACTIONS | previous | current         |
      | SAVINGS_WITH_NO_TRANSACTIONS | current  | saving          |
      | SAVINGS_WITH_NO_TRANSACTIONS | previous | saving          |

  @manual
  Scenario Outline: TC-006 Negative Scenario: User tries to view transactions older than 7 years & is displayed message HC211
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I select <type> account tile from home page
    And I navigate to a month more than 7 years from the current month
    Then I should not see the previous calendar month to the left of the month tab
    And I should see online statements only go back seven years message in the month tab
    Examples:
      | type    |
      | CURRENT |
      | SAVINGS |
