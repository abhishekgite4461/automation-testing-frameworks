@team-statements @nga3 @bnga @stub-enrolled-valid @stubOnly
Feature: Unsure About Transactions
  In order for me to take appropriate steps
  As a BNGA user with current & credit card account & posted transactions
  I want to be able to know what do to if I am unsure about a transaction

  @primary
  Scenario Outline: TC_001 Main Success Scenario: User views Unsure About Transactions Help & Info
    Given I am on home screen as <accessLevel> access user with <type> account
    When I select <type> account tile from home page
    And I select one of the posted transaction
    And I select the help with this transaction link in VTD screen
    Then I must be displayed the Unsure About Transactions Help and Info screen
    Examples:
      | accessLevel          | type                |
      | FULL_SIGNATORY       | CURRENT             |
      | FULL_ACCESS_DELEGATE | CREDITCARD_WITH_VTD |
      | VIEW_ONLY_DELEGATE   | CHARGECARD          |

  @primary
  Scenario Outline: TC_002 Main Success Scenario: User navigates to Click to call journey
    Given I am on home screen as <accessLevel> access user with <type> account
    And I select <type> account tile from home page
    When I select one of the posted transaction
    And I select the help with this transaction link in VTD screen
    And I select to contact customer service
    Then I am displayed the call us businessAccounts for the selected option
    Examples:
      | accessLevel          | type                |
      | FULL_SIGNATORY       | CURRENT             |
      | FULL_ACCESS_DELEGATE | CREDITCARD_WITH_VTD |
      | VIEW_ONLY_DELEGATE   | CHARGECARD          |

  Scenario Outline: TC_003 Alternative Scenario: User navigates Back to VTD
    Given I am on home screen as <accessLevel> access user with <type> account
    And I select <type> account tile from home page
    When I select one of the posted transaction
    And I select the help with this transaction link in VTD screen
    And I select to go back
    Then I should be displayed VTD page of that transaction
    Examples:
      | accessLevel          | type                |
      | FULL_SIGNATORY       | CURRENT             |
      | FULL_ACCESS_DELEGATE | CREDITCARD_WITH_VTD |
      | VIEW_ONLY_DELEGATE   | CHARGECARD          |

  Scenario Outline: TC_004 Alternative Scenario: User navigates back to Unsure About Transactions Help & Info screen
    Given I am on home screen as <accessLevel> access user with <type> account
    And I select <type> account tile from home page
    When I select one of the posted transaction
    And I select the help with this transaction link in VTD screen
    And  I select to contact customer service
    And I select to go back from Click To Call journey
    Then I must be navigated back to the previous screen
    Examples:
      | accessLevel          | type                |
      | FULL_SIGNATORY       | CURRENT             |
      | FULL_ACCESS_DELEGATE | CREDITCARD_WITH_VTD |
      | VIEW_ONLY_DELEGATE   | CHARGECARD          |
