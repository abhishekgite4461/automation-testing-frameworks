@team-statements @nga3 @bnga @environmentOnly @primary
Feature: User wants to view standing order
  In order to view my direct debit
  As a BNGA authenticated customer
  I want to be able to view direct debit within my app

  Scenario Outline: TC-01 Main Success Scenario: User navigates to direct debit functionality (webview page) from account tile menu options
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select the "direct debit" option in the action menu of CURRENT account
    Then I should be on the direct Debit webview page
    And I should have the option to navigate to home screen
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  Scenario Outline: TC-02 Main Success Scenario: User enters direct debit webview page from VTD
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I select CURRENT account tile from home page
    And I select direct Debit on statements page
    And I tap on viewDirectDebit in vtd screen
    Then I should be on the direct debits webview page
    And I should have the option to navigate to home screen
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |
