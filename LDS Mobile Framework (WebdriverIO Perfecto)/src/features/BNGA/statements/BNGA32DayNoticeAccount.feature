@team-statements @nga3 @bnga @stub-enrolled-valid @stubOnly
Feature: 32 Day Notice Account
  In order that I can am aware of my 32 day notice accounts
  As a BNGA authenticated user,
  I want to be able to view the details of my 32 day notice accounts

  #MOB3-14523
  @primary
  Scenario Outline: TC_001 Main Success Scenario: User views details of 32 day notice account with one settlement account
    Given I am on home screen as <accessLevel> access user with TREASURY_32DAYNOTICE account
    And I select TREASURY_32DAYNOTICE account tile from home page
    Then I should be displayed the 32 day notice account page with the following details
      | fieldsInTreasuryAccount  |
      | aerGrossInterestRate     |
      | nominatedAccount         |
      | basicTaxRateStatus       |
      | balanceLastUpdated       |
      | aerInfoLink              |
      | nominatedAccountInfoLink |
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  @primary
  Scenario Outline: TC_002 Variation Scenario: User views details of 32 day notice account with multiple settlement account (MG3102)
    Given I am on home screen as <accessLevel> access user with TREASURY_32DAYNOTICE_WITH_SETTLEMENT account
    When I select TREASURY_32DAYNOTICE_WITH_SETTLEMENT account tile from home page
    Then I should be displayed the 32 day notice account page with the following details
      | fieldsInTreasuryAccount          |
      | multipleSettlementAccountTextMsg |
      | aerGrossInterestRate             |
      | basicTaxRateStatus               |
      | balanceLastUpdated               |
      | aerInfoLink                      |
      | nominatedAccountInfoLink         |
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  #MOB3-14652
  Scenario Outline: TC_003 Exception Scenario: User views AER & Settlement Account details
    Given I am on home screen as FULL_SIGNATORY access user with TREASURY_32DAYNOTICE account
    And I select TREASURY_32DAYNOTICE account tile from home page
    When I view the <link> details
    Then I must be displayed <type> overlay giving me information about gross interest rate
    And I select close button in info page
    And I should be navigated back to treasury account page
    Examples:
      | link             | type             |
      | aer              | aer              |
      | nominatedAccount | nominatedAccount |

  Scenario: TC_004 Exception Scenario: User is shown a message (MG-3101) that the information for treasury accounts cannot be displayed
    Given I am on home screen as FULL_SIGNATORY access user with TREASURY_32DAYNOTICE_PRODUCT_NOT_AVAILABLE account
    And I select TREASURY_32DAYNOTICE_PRODUCT_NOT_AVAILABLE account tile from home page
    Then I must be displayed a message informing that the product is not currently available

  #Test data not available
  @manual
  Scenario: TC_005 Exception Scenario: User is not shown account details & shown a message (MG-3095) when there are more than 10 accounts
    Given I am on home screen as FULL_SIGNATORY access user with TREASURY_32DAYNOTICE account
    When I navigate to 32day Notice More Than 10 Accounts account on home page
    Then I must be displayed only one account tile with a message informing that I have 11 accounts
    When I view the account for more information
    Then I should be displayed message on the treasury account details page asking the customer to contact the bank for more information on their account

  #DPL-2842
  @manual
  Scenario: TC_006 Scenario: User should see Brand Logo for 32DAYNOTICE account
    Given I am on home screen as FULL_SIGNATORY access user with TREASURY_32DAYNOTICE account
    When I select TREASURY_32DAYNOTICE account tile from home page
    Then I should see brand logo for 32DAYNOTICE account
