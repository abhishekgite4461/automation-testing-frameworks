@team-statements @nga3 @bnga @stubOnly
Feature: Pending Transactions
  In order to view the statements for the nominated credit card
  As a NGA authenticated customer with credit card
  I want to view my credit card transactions for each statement period

  #MOB3-11700, #MOB3-12862
  # Commented all tab debit pending transactions validation - MQE-893
  @primary @stub-enrolled-valid
  Scenario Outline: TC_001 Main Success Scenario: User views pending transactions for debit, credit and charge cards
    Given I am on home screen as <accessLevel> access user with <type> account
    And I select <type> account tile from home page
    And I should be on the <tab> of statements page
    When I tap on the pending transactions accordion
    Then I should see pending <pendingType> transaction with below detail in statements
      | detail                                     |
      | pendingTransactionTab                      |
      | pendingLabel                               |
      | pending<pendingType>TransactionDescription |
      | pending<pendingType>TransactionAmount      |
    When I select <transactionType> on statements page
    Then I should see pending transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | vtdAmount               |
      | transactionDescription  |
      | dateOfTransaction       |
      | <text>                  |
      | unsureTransactionButton |
    When I close the pending transactions VTD
    Then I should be displayed pending transactions for that <transactionType>
    Examples:
      | accessLevel        | type                                 | tab          | pendingType | transactionType      | text                     |
      | FULL_SIGNATORY     | CURRENT                              | currentMonth | Debit       | pending Direct Debit | debitDisclaimerText      |
      | FULL_SIGNATORY     | CREDITCARD_WITH_PENDING_TRANSACTIONS | recent       | Credit      | pending Credit Card  | creditCardDisclaimerText |
      | VIEW_ONLY_DELEGATE | CHARGECARD_WITH_PENDING_TRANSACTIONS | recent       | Credit      | pending Credit Card  | creditCardDisclaimerText |

  #MOB3-12861
  @primary @stub-enrolled-valid
  Scenario Outline: TC_002 Main Success Scenario: User views Pending Transaction Details for current account cheque transactions
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I select CURRENT account tile from home page
    Then I should be on the currentMonth of statements page
    When I tap on the pending transactions accordion
    Then I should see pending Cheque transaction with below detail in statements
      | detail                              |
      | pendingLabel                        |
      | noteText                            |
      | pendingChequeTransactionDescription |
      | pendingChequeTransactionAmount      |
    When I select pending Cheque on statements page
    Then I should see pending transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | vtdAmount               |
      | notYetAvailable         |
      | chequesText             |
      | payeeField              |
      | depositedOn             |
      | availableBy             |
      | creditDisclaimerText    |
      | unsureTransactionButton |
    When I close the pending transactions VTD
    And I swipe to see previous month transactions on the account statement page
    Then I should see minimised pending transactions accordion
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  @stub-enrolled-valid
  Scenario Outline: TC_004 Exception Scenario: User does not have pending transactions
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I select <type> account tile from home page
    Then I should be displayed the statement for <month> without any pending transaction accordion
    Examples:
      | type                                    | month  |
      | CREDITCARD_WITHOUT_PENDING_TRANSACTIONS | recent |

  #STAMOB-50,48
  @stub-pending-transactions-not-retrieved
  Scenario Outline: TC_005 Pending debit, cheque and credit transactions cannot be retrieved
    Given I am on MI screen as SINGLE_BUSINESS business user with CURRENT account
    And I enter correct MI
    When I select <type> account tile from home page
    And I tap on the pending transactions accordion
    Then I should see an <errorMessage> error message in place of the transaction
    Examples:
      | type                                        | errorMessage |
      | currentAccountWithPendingTransactionsFailed | debitCheque  |
      | creditCardWithPendingTransactionsFailed     | credit       |

  #STAMOB-50,48
  @pending @stub-pending-transactions-not-retrieved
  Scenario Outline: TC_006 View Pending debit only, cheque only transactions
    Given I am on MI screen as SINGLE_BUSINESS business user with CURRENT account
    And I enter correct MI
    When I select <type> account tile from home page
    And I tap on the pending transactions accordion
    Then I should see <pendingType> transactions only
    Examples:
      | type                                          | pendingType |
      | currentAccountWithNoDebitPendingTransactions  | chequeOnly  |
      | currentAccountWithNoChequePendingTransactions | debitOnly   |
