@team-payments @nga3 @bnga
Feature: User wants to transfer money to another account
  In order to move money between accounts
  As a BNGA customer
  I want to be able to select the remitting account and recipient account and move the desired amount between accounts I have access to

  # COSE-7, 72
  @stub-enrolled-valid
  Scenario Outline: TC_001: Show amount field when User select "From" and "To" field on Transfer & Payment screen
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    When I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    Then I should see the continue button disabled in the payment hub page
    Examples:
      | fromAccount | toAccount |
      | CURRENT     | SAVINGS   |

  # COSE-7, 72
  @stub-enrolled-valid
  Scenario Outline: TC_002: Does'nt enable the continue button if user enters invalid amount
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    When I enter <amount> in the amount field in the payment hub page
    Then I should see the continue button disabled in the payment hub page
    Examples:
      | fromAccount | toAccount | amount |
      | CURRENT     | SAVINGS   | 0      |

  # COSE-7, 72
  @stub-enrolled-valid @primary
  Scenario Outline: TC_003: Displays only 2 decimal digits for the amount field on Transfer & Payment screen
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    And I choose the from account as CURRENT and to account as SAVINGS in the payment hub page
    When I enter <amount> in the amount field in the payment hub page
    Then I should see only 2 decimal places in the amount field in the payment hub page
    And I should see the continue button enabled in the payment hub page
    Examples:
      | amount          |
      | 0.019999        |
      | 9999999999.9999 |

  # COSE-7, 72
  @stub-enrolled-valid @primary
  Scenario Outline: TC_004: Enter amount greater than available balance for Transfer
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    When I enter amount greater than available balance in the remitter account in the payment hub page
    Then I should be able to close the insufficient money in your account error message in the payment hub page
    Examples:
      | fromAccount | toAccount  |
      | CURRENT     | SAVINGS    |

  # COSE-8, PAYMOB-12
  @stub-enrolled-valid
  Scenario Outline: TC_005: choose edit option in the Review transfer page
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I am on the payment hub page
    And I submit the transfer with all the mandatory fields with from account as <fromAccount> and to account as <toAccount> with amount value <amount>
    When I select the edit option in the review transfer page
    Then I should see the values pre-populated in the payment hub page for transfer with amount value <amount>
    Examples:
      | accessLevel           | fromAccount | toAccount      | amount |
      | FULL_SIGNATORY        | CURRENT     | SAVINGS        | 1.00   |
      | FULL_ACCESS_DELEGATE  | CURRENT     | SAVINGS        | 1.00   |

  # COSE-8, PAYMOB-12
  @stub-enrolled-valid
  Scenario Outline: TC_006: choose header back option in the Review transfer page
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I am on the payment hub page
    And I submit transfer with from account <fromAccount> and to account <toAccount> with amount <amount>
    Then I should see the corresponding details in the review transfer page
    When I select the header back option in the review transfer page
    Then I should see the values pre-populated in the payment hub page for transfer with amount value <amount>
    Examples:
      | accessLevel    | fromAccount  | toAccount      | amount |
      | FULL_SIGNATORY | CURRENT      | SAVINGS | 1.00   |

  # COSE-67, PAYMOB-13
  @sanity @stub-enrolled-valid @primary
  Scenario Outline: TC_007: Transfer Success and Amount Update scenario
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I am on the payment hub page
    And I submit transfer with from account <fromAccount> and to account <toAccount> with amount <amount>
    When I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page
    When I select dismiss modal using close button
    Then I should see the corresponding <amount> deducted in the remitter <fromAccount> and recipient accounts <toAccount>
    Examples:
      | accessLevel          | fromAccount | toAccount | amount |
      | FULL_SIGNATORY       | CURRENT     | SAVINGS   | 1.00   |
      | FULL_ACCESS_DELEGATE | CURRENT     | SAVINGS   | 1.00   |

  # COSE-67, PAYMOB-13
  @stub-enrolled-valid @primary
  Scenario Outline: TC_008: choose view transaction in the transfer success page
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I am on the payment hub page
    And I submit the transfer with all the mandatory fields with from account as <fromAccount> and to account as <toAccount> with amount value <amount>
    And I select the confirm option in the review transfer page
    When I select the view transaction option in the transfer success page
    And I should see the - £<amount> in the current month statement of the remitting account
    Examples:
      | accessLevel           | fromAccount  | toAccount  | amount |
      | FULL_SIGNATORY        | CURRENT      | SAVINGS    | 1.00   |
      | FULL_ACCESS_DELEGATE  | CURRENT      | SAVINGS    | 1.00   |

  # COSE-67, PAYMOB-13
  @stub-enrolled-valid
  Scenario Outline: TC_009: Ability to make another transfer after successful transfer
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I am on the payment hub page
    And I submit the transfer with all the mandatory fields with from account as <fromAccount> and to account as <toAccount> with amount value <amount>
    And I select the confirm option in the review transfer page
    When I select the make another transfer option in the transfer success page
    Then I should be navigated to the payment hub page with from account as <fromAccount> in the payment hub page
    Examples:
      | accessLevel           | fromAccount | toAccount      | amount |
      | FULL_SIGNATORY        | CURRENT     | businessSaving | 1.00   |
      | FULL_ACCESS_DELEGATE  | CURRENT     | SAVINGS        | 1.00   |

  #COSE-70, 66. Pre-requisite: User registered for MPA and 'Two/Three to authorize' & 'User Limit with 9 pounds' is set for two different businesses respectively
  #NOTE- Once the transfer request is submitted , tester should verify the notification on desktop for the final approvers
  @stub-mpa-transfer @primary @stubOnly
  Scenario Outline: TC_010_Alternate Success: User successfully sends Transfer approval via Multi Party Authority for defined user limit
    Given I am an enrolled "MPA" user on the MI page
    When I enter correct MI
    And I select <fromBusiness> business with FULL_SIGNATORY access from the business selection screen
    And I am on the payment hub page
    And I submit transfer with from account as <fromAccount> of <fromBusiness> and differing business account as <toAccount> of <toBusiness> with value <amount>
    And I select the confirm option in the MPA review transfer page
    Then I should see make another transfer option in the MPA transfer submitted for approval page
    When I navigate back to home page from MPA approval submitted page
    Then I should see the corresponding <amount> not deducted in the remitter <fromAccount>
    Examples:
      | fromAccount                  | fromBusiness   | toAccount                    | toBusiness     | amount |
      | currentAccountForPayment     | firstBusiness  | secondBusinessCurrentAccount | secondBusiness | 1.00   |
      | secondBusinessCurrentAccount | secondBusiness | currentAccountForPayment     | firstBusiness  | 10.00  |

  #COSE-70, 66. Pre-requisite business should be registered for MPA and user's limit is set as 9 pounds
  #NOTE- Approval messages should not be displayed since the Transfer is within the limit.
  @stub-mpa-transfer
  Scenario: TC_011_Alternate Success: No Approval for the MPA Transfer below the defined User Limit
    Given I am an enrolled "MPA User Limit" user on the MI page
    When I enter correct MI
    And I select secondBusiness business with full Signatory access from the business selection screen
    And I am on the payment hub page
    And I submit transfer with from account as secondBusinessCurrentAccount of secondBusiness and differing business account as currentAccountForPayment of firstBusiness with value 9.00
    And I select the confirm option in the review transfer page
    Then I should see view transaction and make another transfer option in the transfer success page
    When I navigate back to home page from transfer success page
    Then I should see the corresponding 9.00 being deducted in the remitter secondBusinessCurrentAccount

  #COSE-66
  @stub-mpa-transfer
  Scenario: TC_012_Verify User is able to Edit the Transfer from Review Approval Page
    Given I am an enrolled "MPA" user on the MI page
    And I enter correct MI
    And I select firstBusiness business with full Signatory access from the business selection screen
    And I am on the payment hub page
    And I submit transfer with from account as currentAccountForPayment of firstBusiness and differing business account as secondBusinessCurrentAccount of secondBusiness with value 5.00
    When I select the edit option in the MPA review transfer page
    Then I should see the values pre-populated in the payment hub page for transfer with amount value 5.00

  #COSE-70
  @manual
  Scenario: TC_013_Alternate Success: Display Approval required notification for MPA on desktop
    Given that the customer has submitted a transfer where MPA is required [as determined by business rule GIBBR547]
    When the Authoriser logs on to their desktop account
    Then they will be able to approve/ reject the transfer which in turn will update the balance on both App and Desktop accordingly.

  #COSE-69
  @stub-enrolled-valid
  Scenario: TC_014_Alternate Success: Display Notice account warning message
    Given I am an enrolled "notice account" user on the MI page
    When I enter correct MI
    Then I should see switch business screen
    When I select a business from the business selection screen
    Then I am on the payment hub page
    When I submit the transfer with all the mandatory fields with from account as noticeAccount and to account as current with amount value 1.0
    Then I am shown notice warning message in the payment hub page
    And I should see the cancel and OK option in the payment hub page winback

  #COSE-69
  @stub-enrolled-valid
  Scenario: TC_015_Alternate Success: Select Cancel on Notice account warning message
    Given I am an enrolled "notice account" user on the MI page
    When I enter correct MI
    Then I should see switch business screen
    When I select a business from the business selection screen
    Then I am on the payment hub page
    When I submit the transfer with all the mandatory fields with from account as noticeAccount and to account as current with amount value 1.0
    Then I am shown notice warning message in the payment hub page
    When I select the stay option in the payment hub page winback
    Then I should be on the payment hub page

  #COSE-69
  @stub-enrolled-valid
  Scenario: TC_016_Alternate Success: Select OK on Notice account warning message
    Given I am an enrolled "notice account" user on the MI page
    When I enter correct MI
    Then I should see switch business screen
    When I select a business from the business selection screen
    Then I am on the payment hub page
    When I submit the transfer with all the mandatory fields with from account as noticeAccount and to account as current with amount value 1.0
    Then I am shown notice warning message in the payment hub page
    When I select the leave option in the payment hub page winback
    Then I should be on the review transfer page

  #COSE-68
  @stub-bnga-transfer-limit-exceeded
  Scenario: TC_017_Exception Scenario: Greater than Internet transaction limit - MG-454
    Given I am an enrolled "current" user on the MI page
    And I enter correct MI
    And I am on the payment hub page
    When I submit the transfer with all the mandatory fields with from account as current and to account as instant savings with amount value 50.00
    Then I should see internet transaction limit exceeded error message in payment hub page
    And I should see the values pre-populated in the payment hub page for transfer with amount value 50.00

  #COSE-68
  @stub-enrolled-valid
  Scenario Outline: TC_018_Exception Scenario: Amount field entered is greater than balance – MG-384/ 426
    Given I am an enrolled "current" user on the MI page
    And I enter correct MI
    And I select fullSignatoryBusiness business with full Signatory access from the business selection screen
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    When I enter amount greater than available balance in the remitter account in the payment hub page
    Then I should be able to close the insufficient money in your account error message in the payment hub page
    Examples:
      | fromAccount | toAccount |
      | current     | saving    |

  #COSE-68
  @stub-bnga-cbs-transfer
  Scenario: TC_019_Exception Scenario: CBS Prohibitive indicator is loaded on either the remitting or beneficiary account – MG-385
    Given I am on home screen as FULL_SIGNATORY access user with CBS_CURRENT account
    And I am on the payment hub page
    When I submit the transfer with all the mandatory fields with from account as CBS_CURRENT and to account as SAVINGS with amount value 1.00
    And I select the confirm option in the review transfer page
    Then I should see cbs prohibitive error message in payment hub page
    And I should see the values pre-populated in the payment hub page for transfer with amount value 1.00

  #COSE-68
  @stubOnly @stub-bnga-transfer-internet-lost
  Scenario: TC_020_Exception Scenario: Display internet connection lost error message when wifi is turned off before submitting the transfer request
    Given I am an enrolled "current" user on the MI page
    And I enter correct MI
    And I am on the payment hub page
    And I submit the transfer with all the mandatory fields with any from account and to account with amount value 1.00
    When I select the confirm option in the review transfer page
    Then I should see the internet connection lost error in payment hub page

   #COSE-71
  @stubOnly @stub-enrolled-valid
  Scenario: TC_021_Exception Scenario: Display "Transfer Failure" screen when internet connection is lost after submitting the transfer request (HC-254 & HC-252)
    Given I am an enrolled "current" user logged into the app
    When I select a business from the business selection screen
    Then I am on the payment hub page
    When I choose the from account as transferFailureAccount and to account as current in the payment hub page
    And I submit with amount 250.00 in the payment hub page
    And I select the confirm option in the review transfer page
    Then I should see the internet connection lost error in transfer failure page
    And I should see the transfer and payment option in the transfer failure page

  # PAYMOB-277
  @environmentOnly
  Scenario Outline: TC_022_Amount should be updated after successful transfer to another business
    Given I am an enrolled "multiple business" user on the MI page
    When I enter correct MI
    And I provide valid passcode in the card reader respond screen
    And I select continue button on the enrolment confirmation page
    And I verify the default time in the security auto logout settings page
    And I choose my security auto logout settings
    And I opt out for android fingerprint from the interstitial
    And I select <fromBusiness> business with <accessLevel> access from the business selection screen
    And I am on the payment hub page
    And I select <toAccount> account of <toBusiness> business with <amount> amount
    And I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page
    When I select dismiss modal using close button
    Then I should see the corresponding <amount> being deducted in the remitter <fromAccount>
    Examples:
      | fromAccount     | fromBusiness     | toAccount        | toBusiness    | amount | accessLevel  |
      | businessCurrent | Support Services | noticeAccountBus | householdGoods| 1.00   | full Delegate|
