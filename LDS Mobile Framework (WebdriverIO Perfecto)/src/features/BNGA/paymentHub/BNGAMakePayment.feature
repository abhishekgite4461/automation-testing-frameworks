@team-payments @nga3 @bnga
Feature: Make Payment
  In order to send the money
  As an BNGA user
  I want to be able to make a payment to existing personal or corporate beneficiary online

  # PAYMOB-63, PAYMOB-7
  @primary @sanity @stub-bnga-payment-success-password
  Scenario:TC_001_Main Success Scenario: User Successfully Makes a Payment using Password
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    And I submit the payment with from account CURRENT and to beneficiary firstBeneficiary from beneficiary list with amount 1.00 and reference
    When I select the confirm option in the review payment page
    Then I should be prompted to enter my password
    And I enter a correct password
    And I should see a payment confirmed screen
    And I should see an option make another transfer
    And I should see an option to view transactions

  # PAYMOB-63, PAYMOB-7
  @primary  @stub-bnga-payment-card-reader-success
  Scenario: TC_002_Main Success Scenario: User Successfully Makes a Payment using Card Reader
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    And I submit the payment with from account CURRENT and to beneficiary firstBeneficiary from beneficiary list with amount 1.00 and reference
    When I select the confirm option in the review payment page
    Then I should be prompted to enter the 8 digit passcode in card reader respond screen
    And I provide valid passcode in the card reader authentication screen
    And I should see a payment confirmed screen
    And I should see an option make another transfer
    And I should see an option to view transactions

    # COSE-516,COSE-80
  @primary @stub-bnga-payment-success-password
  Scenario Outline:TC_004: Verify amount field level validations
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to beneficiary as <toAccount> in the payment hub page
    When I enter <amount> in the payment amount field in the payment hub page
    Then I should see only 2 decimal places in the payment amount field in the payment hub page
    And I should see the continue button enabled in the payment hub page
    Examples:
      | fromAccount | toAccount        | amount          |
      | CURRENT     | firstBeneficiary | 0.019999        |
      | CURRENT     | firstBeneficiary | 9999999999.9999 |

  # PAYMOB-70,10
  @primary @stub-enrolled-valid
  Scenario: TC_005: Amount field entered is greater than balance – MG-384/ 426 during Payment
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    When I select the from account as CURRENT in the payment hub page
    And I select the to account from the beneficiary list
    And I enter payment amount greater than available balance in the remitter account in the payment hub page
    Then I should be able to close the insufficient money in your account error message in the payment hub page

  #PAYMOB-70,10
  # Stub needs to be updated for iOS
  @primary @stub-bnga-payment-limit-exceeded
  Scenario: TC_006: Amount field entered is greater than the Internet transaction limit(MG-454)
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    When I submit the payment with all the mandatory fields with corresponding from account as CURRENT and to account from beneficiary list with amount value 5000 and reference
    Then I should see internet transaction limit exceeded error message in payment hub page

  #PAYMOB-70,10
  @primary @stub-bnga-account-daily-limit @defect
  Scenario: TC_028: Amount field entered is greater than the account daily limit
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    When I submit the payment with all the mandatory fields with corresponding from account as CURRENT and to account from beneficiary list with amount value 100 and reference
    Then I should see account daily limit exceeded error message in payment hub page

  #PAYMOB-10
  @primary @stub-bnga-cbs-payment
  Scenario: TC_007: Show an error message MG-385 when CBS Prohibitive indicator is loaded on either the remitting or beneficiary account
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    When I submit the payment with all the mandatory fields with corresponding from account as CURRENT and to account from beneficiary list with amount value 1 and reference
    And I select the confirm option in the review payment page
    Then I should see cbs prohibitive error message in payment unsuccessful page

  # PAYMOB-8,72
  @primary @stub-bnga-payment-declined @stubOnly
  Scenario: TC_008: Display payment declined error message for make payment (MG-368 / 369 / 375 / 376)
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    When I submit the payment with from account CURRENT and to beneficiary firstBeneficiary from beneficiary list with amount 300 and reference
    And I select the confirm option in the review payment page
    Then I should see the declined error message in the payment decline page
    And I should see pre auth and back to logon in the payment decline page

  # PAYMOB-11,64
  @stub-bnga-payment-review
  Scenario: TC_009: Display payment under review error message for make payment (MG-379/ 372)
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    And I submit the payment with all the mandatory fields with from account as CURRENT and firstBeneficiary beneficiary with amount value 250 and reference
    And I select the confirm option in the review payment page
    Then I should see the payment hold error message in the payment on hold page
    And I should see transfer and payment button in the payment on hold page

    # COSE-527, PAYMOB-69
  @stub-bnga-payment-card-reader-invalid-passcode
  Scenario: TC_010: Incorrect Passcode on Card Reader Sign (MG-245)
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    And I submit the payment with all the mandatory fields with from account as CURRENT and firstBeneficiary beneficiary with amount value 1 and reference
    When I select the confirm option in the review payment page
    And I enter the incorrect 8 digit passcode from the card reader in the card reader authentication page
    Then I should be shown incorrect passcode error banner in the card reader identify page

  # COSE-80, COSE-516
  @primary @stub-enrolled-valid
  Scenario: TC011_Reference field not editable
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    When I choose the from account as CURRENT and to beneficiary as whiteListedPayee in the payment hub page
    And I enter 1.00 in the amount field in the payment hub page
    Then I am unable to edit the Reference field and displays the stored reference set

  # COSE-517, COSE-81
  @stub-enrolled-valid
  Scenario: TC012_Validate Payment details when payment date is selected as Today's date
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    When I submit the payment with from account CURRENT and to beneficiary uKBank from beneficiary list with amount 1.00 and reference
    Then I should see the corresponding details in the review payment page
    When I select the confirm option in the review payment page
    Then I should see a payment confirmed screen

  # COSE-80, COSE-81
  @stub-enrolled-valid
  Scenario Outline: TC013_User select Edit and Back option from Confirm Payment screen
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page
    When I submit the payment with from account <fromAccount> and to beneficiary <toAccount> from beneficiary list with amount <amount> and reference
    And I select the edit option in the review payment page
    Then I should see the values pre-populated in the payment hub page for payment with amount value <amount>
    And I select the continue option in the payment hub page
    When I select the header back option in the review payment page
    Then I should see the values pre-populated in the payment hub page for payment with amount value <amount>
    Examples:
      | fromAccount | toAccount | amount |
      | CURRENT     | uKBank    | 1.00   |

  @stub-mpa-payment @primary @stubOnly
  # COSE-518 .Pre-requisite business should be registered for MPA and Two/Three to approve payment control settings should turned on for the user
  # NOTE- Once the payment request is submitted , tester should verify the notification on desktop for the final approvers.
  # MPA applies for payments to external beneficiaries
  Scenario Outline: TC_014: User makes a successful payment via MPA
    Given I am on MI screen as FULL_SIGNATORY access user with CURRENT account
    And I enter correct MI
    And I select <fromBusiness> business with FULL_SIGNATORY access from the business selection screen
    And I am on the payment hub page
    When I submit the payment with from account as <fromAccount> of <fromBusiness> and to beneficiary <beneficiary> with amount value <amount>
    And I select the confirm option in the MPA review payment page
    And I should see make another payment option in the MPA payment submitted for approval page
    And I navigate back to home page from MPA approval submitted page
    Then I should see the corresponding <amount> not deducted in the remitter <fromAccount>
    Examples:
      | fromAccount | fromBusiness   | beneficiary      | amount |
      | CURRENT     | firstBusiness  | firstBeneficiary | 1.00   |
      | CURRENT     | secondBusiness | firstBeneficiary | 10.00  |

  @stub-mpa-payment
  # COSE-518 .Pre-requisite business should be registered for MPA and User Limit with 9 pounds is set for the user
  # NOTE- Since the Payment is within the limit, approval should not be required.
  Scenario: TC_014: User makes a successful payment via MPA
    Given I am an enrolled "MPA" user on the MI page
    And I enter correct MI
    And I select secondBusiness business with FULL_SIGNATORY access from the business selection screen
    And I am on the payment hub page
    When I submit the payment with from account as secondBusinessCurrentAccount of secondBusiness and to account firstBeneficiary with amount value 9.00
    And I select the confirm option in the review payment page
    And I should see view transaction and make another payment option in the payment success page
    And I navigate back to home page from payment success page
    Then I should see the corresponding 9.00 being deducted in the remitter secondBusinessCurrentAccount
