@team-payments @nga3 @bnga
Feature: Delete Recipient
  In order to delete a recipient
  As an BNGA user
  I want to be able to access manage recipient options

  Background:
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I navigate to the remitter account on the payment hub page
    And I select remitting AccountWithExternalBeneficiary Account from the account list
    And I select the recipient account on the payment hub page

  # COSE-86,87
  @manual @primary @sanity @stub-enrolled-valid
  Scenario: TC_001_Main Success Scenario: User deletes a recipient
    Given I delete the selected recipient
    When I select to confirm delete a recipient
    Then I should see the recipient deleted message
    And the recipient is removed from the recipient list

  # COSE-86,87
  @primary @stub-enrolled-valid
  Scenario: TC_002_Exception Scenario: User must not be able to delete a recipient with Pending Payment
    When I select manage on an pending payment account
    Then I am given the option to view the pending payments for the account
    And the pay recipient option is disabled
    And the delete recipient is disabled

  # COSE-86,87
  @stub-enrolled-valid
  Scenario: TC_003_Exception Scenario: User keeps recipient
    Given I am on manage actions for a ukBankRecipient
    When I select to delete a recipient
    And I select to keep the recipient
    Then I am taken back to the recipient list
    And  the recipient is still available

  # COSE-87, PAYMOB-57
  @stub-bnga-beneficiary-with-standing-order
  Scenario Outline:TC_004 Dismiss deleting user with a standing order set up message
    Given I am on manage actions for a <type>
    When I select to delete a recipient
    And I am asked for confirmation if I want to delete the recipient
    And I select to confirm delete a recipient
    Then I am should be shown a message saying recipient could not be deleted due to standing order
    When I close the message
    Then I am taken back to the recipient list
    Examples:
      | type   |
      | uKBank |

  # COSE-87, PAYMOB-57
  @stub-bnga-beneficiary-with-standing-order
  Scenario Outline:TC_005 View Standing order
    Given I am on manage actions for a <type>
    When I select to delete a recipient
    And I am asked for confirmation if I want to delete the recipient
    And I select to confirm delete a recipient
    Then I am should be shown a message saying recipient could not be deleted due to standing order
    When I select view message
    Then I am taken to the standing order page
    Examples:
      | type   |
      | uKBank |
