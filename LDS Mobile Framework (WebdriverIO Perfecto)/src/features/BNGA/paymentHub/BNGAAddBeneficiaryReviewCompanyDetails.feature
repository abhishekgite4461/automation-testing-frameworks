@team-payments @nga3 @bnga @stub-enrolled-valid
Feature: Add new Company account beneficiary
  In order to pay an external Company recipient
  As a NGA authenticated customer
  I want to be able to set up a recipient for that specific account

  Background:
    Given I am on home screen as FULL_ACCESS_DELEGATE access user with CURRENT account
    And I navigate to add new recipient option using payment hub
    And I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
    And I enter whitelisted company details
    And I select continue button on add payee page

  #PAYMOB-79, 312
  @primary
  Scenario: TC_001 Display pre-populated details on the add beneficiary review page
    And I am on UK Company Search results page
    When I select a result
    And I should see UK Company Review Details page
    And the selected Recipient Name, Sort Code, Account Number should be displayed

  #PAYMOB-79, 312
  @primary
  Scenario Outline: TC_002 Add Reference to UK Company Recipient
    And I am on UK Company Search results page
    When I select a result
    And I should see UK Company Review Details page
    And the selected Recipient Name, Sort Code, Account Number should be displayed
    And I have the ability to add a reference <reference>,<reEnterReference> and retype the reference
    Examples:
      |reference|reEnterReference|
      |test     |test            |

  #PAYMOB-79, 312
  @manual @secondary
  Scenario Outline: TC_003 Max Character for reference fields
    And I have entered recipient name|sort code|account number of a whitelisted company
    And I choose to Continue
    And the Search results page is displayed
    And the UK Company Review Details page is displayed
    When I enter the 19th character in the <field>
    Then no character should be inputted
    Examples:
      | field              |
      | Reference          |
      | Re-enter Reference |

  #PAYMOB-79, 312
  Scenario Outline: TC_004 Show error message when references are empty or they mismatch
    And I am on UK Company Search results page
    When I select a result
    And I should see UK Company Review Details page
    And the selected Recipient Name, Sort Code, Account Number should be displayed
    And I have the ability to add a reference <reference>, <reEnterReference> and retype the reference
    And I should not be able to continue
    And I should see an error message
    Examples: References
      |reference|reEnterReference|
      |         |                |
      | test    | test1          |

  #PAYMOB-79, 312
  @manual @secondary
  Scenario: TC_005 Unrecognised Reference Format
    And I have entered recipient name|sort code|account number of a whitelisted company
    And I choose to Continue
    And the Search results page is displayed
    And the UK Company Review Details page is displayed
    And I enter an invalid Reference
    When I choose to Continue
    Then I am displayed an error message

  #PAYMOB-79, 312
  Scenario: TC_006 Cancel Journey
    And I am on UK Company Search results page
    When I select a result
    And I should see UK Company Review Details page
    And I select Cancel on UK Review Details page
    Then I should see the leave and Stay option in the payment hub page winback

  #PAYMOB-79, 312
  @ios
  Scenario: TC_009 User confirms cancel Journey
    And I am on UK Company Search results page
    When I select a result
    And I should see UK Company Review Details page
    And I select Cancel on UK Review Details page
    Then I should see the leave and Stay option in the payment hub page winback
    When I select the leave option in the payment hub page winback
    Then I am on search recipient page

  #PAYMOB-79, 312
  Scenario: TC_010 User does not cancel Journey
    And I am on UK Company Search results page
    When I select a result
    And I should see UK Company Review Details page
    Then I select Cancel on UK Review Details page
    And I should see the leave and Stay option in the payment hub page winback
    And I select the stay option in the payment hub page winback
    And I should see UK Company Review Details page

  #PAYMOB-79, 312
  Scenario: TC_011 User goes back
    And I am on UK Company Search results page
    When I select a result
    And I should see UK Company Review Details page
    And I go back in the journey
    And I am on UK Company Search results page

  #PAYMOB-78, 308
  # TODO: iOS tag will be removed once the feature has been implemented in Android PAYMOB-1956
  @primary @ios
  Scenario: TC_012 User searches for Companies to add
    And I am on UK Company Search results page
    Then I should see below UK List fields populated
      | fields                                            |
      | paymentHubAddUkAccountSelectionPayeeNameValue     |
      | paymentHubAddUkAccountSelectionSortCodeValue      |
      | paymentHubAddUkAccountSelectionAccountNumberValue |
    And list of matching companies with the notes
      | results                                    |
      | paymentHubAddUkAccountSelectionCompanyList |
      | businessBeneficiaryPayeeNotes              |

  #PAYMOB-78, 308
  @ios
  Scenario: TC_013 User goes back from the Company Search Result Screen
    And I am on UK Company Search results page
    And I go back in the journey
    And I should be on the New UK beneficiary details page
    And I should see the following details in New UK beneficiary details page
      | details                                  |
      | paymentHubAddUkAccountPayeeNameLabel     |
      | paymentHubAddUkAccountAccountNumberLabel |
