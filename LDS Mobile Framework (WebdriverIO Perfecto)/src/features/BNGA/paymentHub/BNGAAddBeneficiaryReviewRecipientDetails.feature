@team-payments @nga3 @bnga @stub-enrolled-valid

Feature: Review added Uk beneficiary
  As a BNGA User
  I want to be able to view the details I entered
  So that I can verify I am adding the correct beneficiary

  Background:
    Given I am on home screen as FULL_ACCESS_DELEGATE access user with CURRENT account
    And I navigate to add new recipient option using payment hub
    And I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page

  #PAYMOB-81, 275
  @primary
  Scenario Outline: TC_001 Display add recipient
    Then I should see the following details in New UK beneficiary details page
      | details             |
      | beneficiaryTileIcon |
      | payeeNameUk         |
      | sortCodeField       |
      | accountNumberUk     |
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    And I should see UK Company Review Details page
    And the selected Recipient Name, Sort Code, Account Number should be displayed
    Examples: Valid entries
      |sortCode|accountNumber|recipientName|
      |779506   |60074163     |abcd1234    |

  #PAYMOB-81, 275
  @primary
  Scenario Outline: TC_002 Add reference to recipient
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    And I should see UK Company Review Details page
    And the selected Recipient Name, Sort Code, Account Number should be displayed
    Then I should see to add a reference <reference>
    Examples: Valid entries
      | reference | sortCode | accountNumber | recipientName |
      | test      | 779508   | 87654322      | abcd1234      |

  #PAYMOB-81, 275
  Scenario Outline: TC_003 Go Back
    Then I should be on the New UK beneficiary details page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    And I should see UK Company Review Details page
    When I go back in the journey
    Then I should be on the New UK beneficiary details page
    Examples: Valid entries
      | sortCode | accountNumber | recipientName |
      | 779508   | 87654322      | abcd1234      |

  #PAYMOB-81, 275
  Scenario Outline: TC_004 Display winback
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    When I select Cancel on UK Review Details page
    Then I should see a winback option on top of the screen with Cancel and OK option
    Examples: Valid entries
      | sortCode | accountNumber | recipientName |
      | 779508   | 60074163      | abcd1234      |

  #PAYMOB-81, 275
  Scenario Outline: TC_005 User confirms cancel Journey
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    And I should see UK Review Details page
    When I select Cancel on UK Review Details page
    Then I should see a winback option on top of the screen with Cancel and OK option
    When I select the leave option in the payment hub page winback
    Then I am on search recipient page
    Examples: Valid entries
      | sortCode | accountNumber | recipientName |
      | 779508   | 60074163      | abcd1234      |

  #PAYMOB-81, 275
  Scenario Outline: TC_006 User does not cancel Journey
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    And I should see UK Review Details page
    When I select Cancel on UK Review Details page
    Then I should see a winback option on top of the screen with Cancel and OK option
    And I select the stay option in the payment hub page winback
    And I should see UK Review Details page
    Examples: Valid entries
      | sortCode | accountNumber | recipientName |
      | 779508   | 60074163      | abcd1234      |

  #PAYMOB-81, 275
  @primary
  Scenario Outline: TC_007 Display Security Link
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    And I should see UK Review Details page
    When I select the security link
    And I should see a warning overlay with the ability to close
    Examples: Valid entries
      | sortCode | accountNumber | recipientName |
      | 779508   | 60074163      | abcd1234      |
