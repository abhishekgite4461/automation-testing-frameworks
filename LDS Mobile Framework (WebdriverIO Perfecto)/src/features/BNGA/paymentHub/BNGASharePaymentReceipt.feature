@team-payments @nga3 @bnga @stub-enrolled-valid @ff-share-payment-receipt-on
Feature: Share Payment Receipt
  As a BNGA authenticated user
  I want to share the payment success receipt using the compatible apps on my device

  # PAYMOB-1208
  @primary @genericAccount
  Scenario Outline: TC_01 Able to share Payment Receipt using native OS share panel
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I am on the payment hub page
    And I submit the payment with from account CURRENT and to beneficiary uKBank from beneficiary list with amount 1.00 and reference
    When I select the confirm option in the review payment page
    Then I am shown Payment Successful page
    When I select the share option on the payment success page
    And I select Share on the Share Preview screen
    Then I should see the native OS share panel
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |

  # PAYMOB-1208
  @primary @genericAccount
  Scenario Outline: TC_02 Able to see the details on share preview screen for the 'Immediate' and 'Future Dated' Payment success page
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I am on the payment hub page
    And I choose the from account as CURRENT and to beneficiary as uKBank in the payment hub page
    When I enter 1.00 in the amount field in the payment hub page
    And I select calendar picker option for future date payment
    And I submit future date payment with date as <days> days after today
    And I select the confirm option in the review payment page
    Then I am shown Payment Successful page
    And I should see share receipt option on the payment success page
    And I should see the Bank Icon at the top of the screen
    And I should see the first 4 digits of the recipient account number is masked
    And I should see the date value on the share preview screen

    Examples:
      | accessLevel          | days |
      | FULL_SIGNATORY       | 0    |
      | FULL_SIGNATORY       | 2    |
      | FULL_ACCESS_DELEGATE | 0    |
      | FULL_ACCESS_DELEGATE | 2    |

  # PAYMOB-1208
  @secondary @genericAccount @pending
  Scenario Outline: TC_03 Able to see the details on share preview screen when payment is done from differing business
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I am on the payment hub page
    And I submit the payment with from account as CURRENT of <otherBusiness> and to beneficiary uKBank with amount value 1.00
    And I select the to account from the beneficiary list
    And I submit the payment with amount value 1.00 and reference
    When I select the confirm option in the review payment page
    Then I am shown Payment Successful page
    And I should see share receipt option on the payment success page
    And I should see the Bank Icon at the top of the screen
    And I should see the first 4 digits of the recipient account number is masked
    And I should see the date value on the share preview screen

    Examples:
      | accessLevel          | otherBusiness    |
      | FULL_SIGNATORY       | zavaitarBusiness |
      | FULL_ACCESS_DELEGATE | apolloBusiness   |

  # Manual, since we have to test with other different apps specific to the device
  # PAYMOB-1208
  @manual
  Scenario Outline: TC_04 Able to see the pre-defined text and image in the compatible apps
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I am on the payment hub page
    And I choose the from account as CURRENT and to beneficiary as uKBank in the payment hub page
    When I enter 1.00 in the amount field in the payment hub page
    And I select Continue button on payment hub page
    And I select the confirm option in the review payment page
    Then I am shown Payment Successful page
    When I select the share option on the payment success page
    And I select Share on the Share Preview screen
    Then I should see the native OS share panel
    When I select a compatible app that allows population of predefined text with the image to be sent
    Then I should see the copy associated to the imaged when shared with the selected app
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |

  # Manual, since we have to test with other different apps specific to the device
  # PAYMOB-1208
  @manual
  Scenario Outline: TC_05 Able to see only the image with the incompatible apps
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I am on the payment hub page
    And I choose the from account as CURRENT and to beneficiary as uKBank in the payment hub page
    When I enter 1.00 in the amount field in the payment hub page
    And I select Continue button on payment hub page
    And I select the confirm option in the review payment page
    Then I am shown Payment Successful page
    When I select the share option on the payment success page
    And I select Share on the Share Preview screen
    Then I should see the native OS share panel
    When I select a incompatible app that doesn't allow the population of predefined text with the image to be sent
    Then I should see only the image of the payment receipt being shared with the selected app
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
