@team-payments @nga3 @bnga @stub-enrolled-valid
Feature: In order to pay a recipient on future date
  As a BNGA user
  I want me able to set a payment to be made in the future
  So that the money goes out on the desired day

  Background:
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I am on the payment hub page

# COSE-553, COSE-554
  @primary @sanity
  Scenario Outline: TC_001 Make Future Dated Payment
    Given I choose the from account as <fromAccount> and to beneficiary as <toAccount> in the payment hub page
    And I enter amount in the amount field as 1.00
    And I select calendar picker option for future date payment
    And I select the date as <days> from today's date from calendar view
    When I select Continue button on payment hub page
    Then I should see the review payment page
    And I am shown 'When' the payment will be made on payment review page
    And I am shown information of payment date if it falls on a holiday
    When I select the confirm option in the review payment page
    Then I should see view transaction and make another payment option in the payment success page
    Examples:
      | fromAccount | toAccount | days |
      | CURRENT     | uKBank    | 2    |

  # COSE-553
  @primary
  Scenario Outline: TC_002 Ability to select date for future dated payment calendar view
    Given I choose the from account as <fromAccount> and to beneficiary as <toAccount> in the payment hub page
    And I enter amount in the amount field as 1.00
    When I select calendar picker option for future date payment
    Then I should see a calendar view
    When I select next month chevron from the top header of calendar view
    Then I should be able to navigate to next month calendar
    When I select previous month chevron from the top header of calendar view
    Then I should be able to navigate to previous month calendar
    And I should be allowed to select a payment date up to 31 days from current day
    And other dates must not be able to be selected
    Examples:
      | fromAccount | toAccount |
      | CURRENT     | uKBank    |

  # COSE-553
  @secondary
  Scenario Outline: TC_003 User select tomorrow date from calendar picker option
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter amount in the amount field as 1.00
    And I select calendar picker option for future date payment
    When I select the date as <days> from today's date from calendar view
    Then the future dated payment should display Tomorrow in text
    Examples:
      | fromAccount  | toAccount   | days |
      | CURRENT      | uKBank      | 1    |

  # COSE-518
  @stub-mpa-payment
  Scenario Outline: TC_004: Ability to do Future Dated Payment - MPA User
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter amount in the amount field as <amount>
    And I select calendar picker option for future date payment
    And I select the date as <days> from today's date from calendar view
    When I select Continue button on payment hub page
    Then I am shown 'When' the payment will be made on payment review page
    When I select the confirm option in the MPA review payment page
    Then I should see make another payment option in the MPA payment submitted for approval page
    And I navigate back to home page from MPA approval submitted page
    Examples:
      | fromAccount              | toAccount        | amount | days |
      | currentAccountForPayment | firstBeneficiary | 1.00   | 2    |
