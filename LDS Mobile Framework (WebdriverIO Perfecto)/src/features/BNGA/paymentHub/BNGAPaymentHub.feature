@team-payments @nga3 @bnga
Feature: User wants to select recipients to pay/transfer
  In order to pay or transfer money
  As a BNGA customer
  I want to be able to select the remitting account and recipient account

  # MOB3-14066, 12479
  @stub-enrolled-valid @primary
  Scenario Outline: TC_001_Main Success Scenario: Select remitting and recipient account to make a transfer
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I am on the payment hub page
    When I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I should see amount field in the payment hub page
    And I am not shown payment date in the payment hub
    Examples:
      | accessLevel          | fromAccount | toAccount |
      | FULL_SIGNATORY       | CURRENT     | CURRENT   |
      | FULL_SIGNATORY       | CURRENT     | SAVINGS   |
      | FULL_ACCESS_DELEGATE | CURRENT     | CURRENT   |
      | FULL_ACCESS_DELEGATE | CURRENT     | SAVINGS   |

  # MOB3-14066, 12479
  @stub-enrolled-valid @primary
  Scenario Outline: TC_002_Main Success Scenario: Select remitting and recipient account to make a payment
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I am on the payment hub page
    When I choose the from account as <fromAccount> and to beneficiary as <toAccount> in the payment hub page
    And I should see amount field in the payment hub page
    And I am shown payment date in the payment hub
    Examples:
      | accessLevel                 | fromAccount   | toAccount |
      | FULL_SIGNATORY              | CURRENT       | uKBank    |
      | FULL_ACCESS_DELEGATE        | CURRENT       | uKBank    |

  @stub-enrolled-valid @primary
  Scenario: TC_003_Exception Scenario: User should not be allowed to transfer amount in the same account
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I am on the view recipient page with defaultPrimary remitter account
    Then I should not see the defaultPrimary account for fullSignatoryBusiness view recipient page

  # MOB3-14229
  @stub-bnga-no-beneficiary
  Scenario: TC_004_Exception Scenario: Verify user is able to see no beneficiaries or recipients set up message
    Given I am an enrolled "no recipients available" user on the MI page
    And I enter correct MI
    And I am on the payment hub page
    When I select the recipient account on the payment hub page
    Then I see a message that I have no recipients set up on the account

  # PAYMOB-151, MOB3-14439
  @stub-bnga-due-soon @primary
  Scenario: TC_005_Exception Scenario: Verify pending payment is not allowed to select as the UK beneficiary account in the payment hub page
    Given I am an enrolled "current" user on the MI page
    And I enter correct MI
    And I am on the payment hub page
    When I select the recipient account on the payment hub page
    Then I should not be able to select pending payment from view recipient page

  #MOB3-14228
  @primary @stub-bnga-no-eligible-accounts
  Scenario: TC_006_Exception Scenario: User has no eligible accounts to make a payment or transfer as Full Signatory from Home Page entry point
    Given I am on home screen as FULL_SIGNATORY access user with CREDITCARD account
    And I select payment hub from home page
    Then I should see the no remitter eligible error message
    And I should see Apply for other accounts button on no eligible accounts page

  # MOB3-14228, COSE-49
  @stub-bnga-no-eligible-accounts
  Scenario: TC_007_Exception Scenario: User has no option to make a payment or transfer in the Action Menu as Full Signatory
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I select the action menu of creditCard account
    Then I should not see Transfer and payment option in the action menu

  # MOB3-14228 COSE-49
  @stub-bnga-no-eligible-accounts @stubOnly @primary
  Scenario: TC_008_Exception Scenario: User has no eligible accounts to make a payment or transfer as Full Delegate from Home Page entry point
    Given I am on home screen as FULL_ACCESS_DELEGATE access user with CREDITCARD account
    When I navigate to pay and transfer from home page
    Then I should see the no remitter eligible error message

  #MOB3-14228 COSE-49
  @stub-bnga-no-eligible-accounts @stubOnly @primary @defect
  Scenario Outline: TC_009_Exception Scenario: User has no option to make a payment or transfer in the Action Menu Full Delegate
    Given I am on home screen as <accessLevel> access user with <type> account
    When I select the action menu of <type> account
    Then I should not see Transfer and payment option in the action menu
    Examples:
      | accessLevel          | type        |
      | FULL_ACCESS_DELEGATE | CREDITCARD |
     # | FULL_ACCESS_DELEGATE | CHARGECARD |

  # MOB3-12486, 14066 COSE-49
  @stub-enrolled-valid @primary
  Scenario Outline: TC_010_User initiate Payments & Transfers from Actions menu then pre-populate the From field based on the last visible account tile from Homepage
    Given I am on home screen as <accessLevel> access user with <type> account
    When I select <fromAccount> account tile from home page
    And I select the action menu of <fromAccount> account
    And I select the transfer and payment option in the action menu
    Then I should see the from field pre-populated with <fromAccount> account
    Examples:
      | accessLevel          | type    | fromAccount |
      | FULL_SIGNATORY       | CURRENT | CURRENT     |
      | FULL_SIGNATORY       | SAVINGS | SAVINGS     |
      | FULL_ACCESS_DELEGATE | CURRENT | CURRENT     |
      | FULL_ACCESS_DELEGATE | SAVINGS | SAVINGS     |

  # MOB3-12486, 14066
  # IOS App doesn't show correct default account in payment hub
  @stub-enrolled-valid @primary
  Scenario Outline: TC_011_User initiate Payments & Transfers from Homepage then pre-populate the From field based on the last visible account tile from Homepage
    Given I am on home screen as <accessLevel> access user with <type> account
    When I select <fromAccount> account tile from home page
    And I navigate to pay and transfer from home page
    Then I should see the from field pre-populated with <fromAccount> account
    Examples:
      | accessLevel          | type    | fromAccount |
      | FULL_SIGNATORY       | CURRENT | CURRENT     |
      | FULL_SIGNATORY       | SAVINGS | SAVINGS     |
      | FULL_ACCESS_DELEGATE | CURRENT | CURRENT     |
      | FULL_ACCESS_DELEGATE | SAVINGS | SAVINGS     |

  # MOB3-12486
  @stub-enrolled-valid @primary
  Scenario Outline: TC_012_User initiate Payments & Transfers from Homepage and the last visible account tile is not eligible for payments then next eligible account should be pre-populated in From field
    Given I am on home screen as <accessLevel> access user with <type> account
    And I obtain the default primary account
    When I select <fromAccount> account tile from home page
    And I navigate to pay and transfer from home page
    Then I should see the from field pre-populated with defaultPrimary account
    Examples:
      | accessLevel          | type       | fromAccount |
      | FULL_SIGNATORY       | CREDITCARD | CREDITCARD  |
      | FULL_SIGNATORY       | CHARGECARD | CHARGECARD  |
      | FULL_ACCESS_DELEGATE | CREDITCARD | CREDITCARD  |
      | FULL_ACCESS_DELEGATE | CHARGECARD | CHARGECARD  |

  # COSE-48 Dynamic accounts will get displayed
  @stub-enrolled-valid @primary @manual
  Scenario Outline: TC_013_View other business accounts in remitting account
    Given I am on home screen as <accessLevel> access user with <type> account
    And I choose the <fromAccount> account on the payment hub page
    Then I should be shown accounts eligible to make a payment or transfer available from the business currently selected
    And I can see all eligible remitting accounts from the Businesses I have <access> access to
    And the accounts are grouped by business
    Examples:
      | accessLevel           | type    | fromAccount                  |
      | FULL_SIGNATORY        | CURRENT | CURRENT                      |
      | FULL_ACCESS_DELEGATE  | SAVINGS | savingAccountNoTransactions  |

  # MOB3-12479
  @stub-enrolled-valid @primary
  Scenario Outline: TC_014_View other business accounts in recipient account
    Given I am on home screen as <accessLevel> access user with <type> account
    And I select <fromAccount> account tile from home page
    And I choose the remitter account on the payment hub page
    And I select <fromAccount> Account on payment hub page
    When I choose a recepient account on the payment hub page
    Then I should be shown accounts eligible to make a transfer to, from the business currently selected
#    And I can see all eligible recipient accounts from the Businesses I have <accessLevel> access to
#    And the accounts should be grouped by business
    Examples:
      | accessLevel           | type    | fromAccount  |
      | FULL_SIGNATORY        | CURRENT | CURRENT      |
      | FULL_SIGNATORY        | SAVINGS | SAVINGS      |
      | FULL_ACCESS_DELEGATE  | CURRENT | CURRENT      |
      | FULL_ACCESS_DELEGATE  | SAVINGS | SAVINGS      |

  # MOB3-14072 COSE-48
  @stub-enrolled-valid @primary
  Scenario Outline: TC_015: Display list of all business eligible remitting accounts in the from field
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I choose the remitter account on the payment hub page
    Then I should see the below accounts in the view remitter page
      | accounts                    |
      | current                     |
      | savingAccountNoTransactions |
      #| savingDormant               |
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |

  # MOB3-14072
  @stub-enrolled-valid @primary
  Scenario Outline: TC_016: Doesn't display list of all business non-eligible remitting accounts in the from field
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I choose the remitter account on the payment hub page
    Then I should not see the below accounts in the view remitter page
      | accounts   |
      #| treasuryDeposit |
      | creditCard |
      | chargeCard |
      #| loan            |
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |

  @stub-enrolled-valid
  Scenario Outline: TC_017_Main Success Scenario: make payment
    Given I am a existing paym beneficiary user on the home page
    And I select a business from the business selection screen
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to beneficiary as <toAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    And I select the continue option in the payment hub page
    And I select the confirm option in the review payment page
    And I should see enter password dialog
    And I should be given options to proceed or cancel
    And I choose to cancel the process on enter password dialog
    And I select the confirm option in the review payment page
    And I should be displayed option for forgotten password
    When I enter the password "abcde" into a dialog
    Then I see password is masked on enter password dialog
    And the option to continue on enter password dialogue should not be enabled
    When I enter the password "abcdef" into a dialog
    And the option to continue on enter password dialogue is enabled
    And I choose forgotten your password option
    Then I am displayed a warning message
    And I should see forgotten password winback with options on top of the screen with Cancel and OK option
    When I choose to cancel on the warning message
    And I enter the password "abcdef" into a dialog
    And I submit password on enter password dialog
    Then I should be displayed an error message for wrong password
    And I should be given an option to enter the password again
    Examples:
      | fromAccount              | toAccount           | amount |
      | currentAccountForPayment | passwordBeneficiary | 1.00   |

  @stub-enrolled-valid
  Scenario Outline: TC_018_Customer enters incorrect password
    Given I am an enrolled "valid" user logged into the app
    And I select a business from the business selection screen
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to beneficiary as <toAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    And I select the continue option in the payment hub page
    And I select the confirm option in the review payment page
    When I enter the password "withmorethantwentycharactersinpassword" into a dialog
    Then I should be restricted from entering more than 20 characters in password dialog text box
    Examples:
      | fromAccount              | toAccount           | amount |
      | currentAccountForPayment | passwordBeneficiary | 1.00   |

  @exhaustiveData @manual
  Scenario: TC_019_Customer enters incorrect password consecutively 6 times on enter password dialog
    Given I am an enrolled "valid" user logged into the app
    And I select a business from the business selection screen
    And I am on the payment hub page
    When I choose the from account as <fromAccount> and to beneficiary as <toAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    And I select the continue option in the payment hub page
    And I select the confirm option in the review payment page
    And I should see enter password dialog
    And I have entered wrong password 5 times consecutively
    And I choose to proceed having entered the wrong password the 6th time
    Then I should be displayed revoked error page

  #PAYMOB-1220 , TODO: PAYMOB-1187
  @stub-mpa-payment @stubOnly @ios
  Scenario: TC_20_Alternate Success: User successfully sends Transfer approval via Multi Party Authority for defined user limit
    Given I am on home screen as FULL_SIGNATORY access user with MPACURRENT account
    And I am on the payment hub page
    When I submit the payment with from account MPACURRENT and to beneficiary firstBeneficiary from beneficiary list with amount 10 and reference
    And I select the confirm option in the review payment page
    Then I should not see share receipt option on the payment success page
    And I should see make another payment option in the MPA payment submitted for approval page
