@team-payments @nga3 @bnga @stub-enrolled-valid
Feature: User wants to search recipients to pay/transfer
  In order to quickly find recipients I want to pay
  As a BNGA customer
  I want to be able to search for recipients account

  Background:
    Given I am an enrolled "valid" user on the MI page
    And I enter correct MI
    And I should see business selection screen
    And I select a business from the business selection screen

  #TODO: Merge for environment once LDS env is up to add recipients
  # MOB3-12757
  @primary
  Scenario: TC_001_Main Success: User Searches for Internal Accounts/Recipients
    When I choose the recipient account on the payment hub page
    Then I verify search results for each search term
      | searchType | searchTerm    | results                              |
      | account    | account       | currentAccountForPayment             |
      | account    | failure       | arrangementUpdateFailure             |
      | recipient  | due           | savingsAccountNotEligibleForPayments |
      | recipient  | Atwel         | uKBank                               |

  # MOB3-12757
  @manual
  Scenario: TC_002 Dynamic Searching per character added (Visual Verification)
    Given I choose the recipient account on the payment hub page
    When I verify search results for each search term
    Then the <searchTerm> characters of <results> are highlighted in <searchType> category
      | searchType | searchTerm | results        |
      | account    | sin        | currentAccount |
      | account    | 30         | businessSaving |
      | recipient  | AT         | uKBank         |

  # MOB3-12757
  Scenario: TC_003_Exception: No matching results
    When I choose the recipient account on the payment hub page
    Then I verify search results for each search term
      | searchType | searchTerm | results     |
      | account    | bun        | no results  |
      | recipient  | ELN        | no results  |

  # MOB3-12757
  Scenario: TC_004_Exception: Clearing a Search
    Given I choose the recipient account on the payment hub page
    Then I verify search results for each search term
      | searchType | searchTerm | results       |
      | account    | sin        | currentAccount|
    When I click on clear searching
    Then I verify search results for each search term
      | searchType | searchTerm | results        |
      | account    | 30         | businessSaving |

  # MOB3-12757
  Scenario: TC_005 Cancelling a search
    Given I choose the recipient account on the payment hub page
    Then I verify search results for each search term
      | searchType | searchTerm | results        |
      | account    | sin        | currentAccount |
    And I click on cancel searching
    And I see list of Accounts in search page

  # MOB3-12757
  @manual
  Scenario: TC_006 Display Keyboard
    Given I choose the recipient account on the payment hub page
    And I am on search recipient page
    When I choose the option to search payees
    Then the native search keyboard is displayed

  # MOB3-12757
  @manual
  Scenario: TC_007 Select Search/Go Keyboard
    Given I choose the recipient account on the payment hub page
    And I am on search recipient page
    And I choose the option to search payees
    And the native keyboard gets displayed while entering a search term
    When I tap on the <search/go> button
    Then the keyboard disappears
    And I see list of Accounts in search page that meets the search criteria

  # MOB3-12757
  Scenario: TC_008 Select a result
    Given I choose the recipient account on the payment hub page
    When I verify search results for each search term
      | searchType | searchTerm | results        |
      | account    | sin        | currentAccount |
    And I select the search resultant account
      | results        |
      | currentAccount |
    Then I should be on the payment hub page
