@nga3 @bnga @team-payments @stub-enrolled-valid @sw-add-beneficiary-off @primary @ff-add-beneficiary-enabled-off

Feature: Add new Beneficiary
  As a LBG native business app user
  I want to be able to add a new recipent,
  So that I can pay someone I've not paid previously using the native app

  #PAYMOB-76, 274
  Scenario Outline: TC_001 Verify User cannot add beneficiary when switch is off
    Given I am on home screen as <accessLevel> access user with CURRENT account
    Then I should be on the home page
    And I navigate to the remitter account on the payment hub page
    And I select CURRENT Account on payment hub page
    When I select the recipient account on the payment hub page
    Then I should not see an option to add a new recipient
    Examples:
      | accessLevel          |
      | FULL_ACCESS_DELEGATE |
      | FULL_SIGNATORY       |
