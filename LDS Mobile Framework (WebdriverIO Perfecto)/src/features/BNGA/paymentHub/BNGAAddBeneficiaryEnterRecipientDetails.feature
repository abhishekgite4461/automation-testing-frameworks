@team-payments @nga3 @bnga @stub-enrolled-valid
Feature: Add new Uk beneficiary details
    As a BNGA user
    I want the ability to capture relevant data with the validation rules so that I can add recipients correctly
    I want the ability to confirm that the recipient has been added.

  #PAYMOB-80, 273
  Scenario: TC_001 Ability to add basic details when creating new recipient.
    Given I am on home screen as FULL_ACCESS_DELEGATE access user with CURRENT account
    And I navigate to add new recipient option using payment hub
    Then I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I should see the following details in New UK beneficiary details page
      | details             |
      | beneficiaryTileIcon |
      | payeeNameUk         |
      | sortCodeField       |
      | accountNumberUk     |

  #PAYMOB-285, 286, #PAYMOB-1127 Marked as defect for Android stub.
  @primary
  Scenario Outline: TC_002 Successfully Adds New Recipient Company Account
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I navigate to add new recipient option using payment hub
    And I choose to Add new payee on search recipient page
    And I enter whitelisted company details
    And I select continue button on add payee page
    And I am on UK Company Search results page
    And I select a result
    And I should see UK Company Review Details page
    And the selected Recipient Name, Sort Code, Account Number should be displayed
    And I have the ability to add a reference 5404633753077758, 5404633753077758 and retype the reference
    And I select continue on UK review details page
    And I am displayed password confirmation screen
    And I enter a correct password
    Then I am on the Make Payment screen with newly added beneficiary selected
    Examples: Enter references
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |

  #PAYMOB-80, 273
  Scenario: TC_003 User cancels journey
    Given I am on home screen as FULL_ACCESS_DELEGATE access user with CURRENT account
    And I navigate to add new recipient option using payment hub
    And I am on search recipient page
    Then I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I should be on the New UK beneficiary details page
    When I select dismiss modal using close button
    Then I should be on the home page

  #PAYMOB-80, 273
  Scenario: TC_004 User clicks back
    Given I am on home screen as FULL_ACCESS_DELEGATE access user with CURRENT account
    And I navigate to add new recipient option using payment hub
    And I am on search recipient page
    Then I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I should be on the New UK beneficiary details page
    When I go back in the journey
    Then I am on search recipient page

 #PAYMOB-80, 273
  @ios @primary
  Scenario Outline: TC_005 Switch eligible remitting accounts to another business from add basic details page
    Given I am on home screen as FULL_ACCESS_DELEGATE access user with CURRENT account
    And I navigate to the remitter account on the payment hub page
    And I select eligibleRemitting Account on payment hub page
    And I select the recipient option in the payment hub page
    And I choose to Add new payee on search recipient page
    And I select the remitting account
    Then I should see list of eligible accounts that can add a new recipient
    When I select account as <account> of different <differentBusiness> business
    Then I am on the payment hub page
    And I should see the from field pre-populated with <account> account
    Examples:
      | account           | differentBusiness |
      | eligibleRemitting | estateBusiness    |

 #PAYMOB-80, 273
  Scenario Outline: TC_006 Continue when valid information entered into all fields
    Given I am on home screen as FULL_ACCESS_DELEGATE access user with CURRENT account
    And I navigate to add new recipient option using payment hub
    And I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    Then I select continue button on add payee page
    Examples: Valid entries
      |sortCode|accountNumber|recipientName|
      |779506   |60074163     |abcd1234    |

 #PAYMOB-285, 286
  @manual @secondary
  Scenario: TC_007 User should receive confirmation SMS
    Given I have a valid Phone number stored
    When I complete authentication
    Then I receive a confirmation SMS

 #PAYMOB-285, 286
  @manual @secondary
  Scenario: TC_008 User should not receive confirmation SMS
    Given I have an invalid Phone number stored
    When I complete authentication
    Then I should not receive SMS

 #PAYMOB-285, 286
  @primary @environmentOnly
  Scenario Outline: TC_009 Time lapse message validation after making payment
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I navigate to add new recipient option using payment hub
    And I choose to Add new payee on search recipient page
    And I enter whitelisted company details
    And I select continue button on add payee page
    And I am on UK Company Search results page
    And I select a result
    And I should see UK Company Review Details page
    And the selected Recipient Name, Sort Code, Account Number should be displayed
    And I have the ability to add a reference 5404633753077758, 5404633753077758 and retype the reference
    And I select continue on UK review details page
    And I am displayed password confirmation screen
    And I enter a correct password
    Then I am on the Make Payment screen with newly added beneficiary selected
    When I enter amount in the amount field as 1
    Then I should be able to continue with the payment
    And I select the confirm option in the review payment page
    And I should see a payment confirmed screen
    And I select the make another payment option in the payment success page
    When I submit the transfer with all the mandatory fields with from account as CURRENT and to account as ukCompanyBeneficiary with amount value 10
    Then I should see 60 minutes lapse error message on payment hub
    Examples: Enter references
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |

  #PAYMOB-285, 286 From account balance should be more than £1000
  @primary @environmentOnly
  Scenario Outline: TC_009 Time lapse and over payment message validations on payment hub
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I navigate to add new recipient option using payment hub
    And I choose to Add new payee on search recipient page
    And I enter whitelisted company details
    And I select continue button on add payee page
    And I am on UK Company Search results page
    And I select a result
    And I should see UK Company Review Details page
    And the selected Recipient Name, Sort Code, Account Number should be displayed
    And I have the ability to add a reference 5404633753077758, 5404633753077758 and retype the reference
    And I select continue on UK review details page
    And I am displayed password confirmation screen
    And I enter a correct password
    Then I am on the Make Payment screen with newly added beneficiary selected
    When I enter amount in the amount field as 1001
    Then I should see 60 minutes lapse error message with over payment warning on payment hub
    Examples: Enter references
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |

  #PAYMOB-1226 TODO: Pending tag will be removed once the bug PAYMOB-1226 is fixed
  Scenario Outline: TC_015 Add New UK Beneficiary, Delete, Add the same Payee and Make Payment
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I navigate to add new recipient option using payment hub
    And I choose to Add new payee on search recipient page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    Then I should see UK Review Details page
    When I select continue on UK review details page
    Then I should be prompted to enter my password
    And I enter a correct password
    And I am on the Make Payment screen with newly added beneficiary selected
    And I select the recipient account on the payment hub page
    And I search recipient account <recipientName> on payment hub page to delete
    And I delete the selected recipient
    And I am asked for confirmation if I want to delete the recipient
    When I select to confirm delete a recipient
    And I navigate back to home page after deleting the recipient
    And I navigate to add new recipient option using payment hub
    And I choose to Add new payee on search recipient page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    Then I should see UK Review Details page
    When I select continue on UK review details page
    Then I should be prompted to enter my password
    And I enter a correct password
    And I am on the Make Payment screen with newly added beneficiary selected
    When I enter 1.00 in the payment amount field in the payment hub page
    And I select the continue option in the payment hub page
    And I select the confirm option in the review payment page
    Then I am shown Payment Successful page
    Examples: Valid entries
      | sortCode | accountNumber | recipientName      |
      | 779508   | 60074163      | ATWELL MARTIN LTD  |
