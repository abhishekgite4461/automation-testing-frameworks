@team-statements @nga3 @bnga @stub-enrolled-valid @stubOnly
Feature: BNGA User views Call Us home page
  In order that I can contact the bank
  As a BNGA user
  I want to see the option to contact the bank

  #MOB3-10179
  @primary
  Scenario: TC_001 Main Success Scenario: User views Business Account call us page when logged into the app
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I navigate to call us home page
    Then I should be able to view these call us tiles
      | callUsTiles           |
      | businessAccountsTile  |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should not see security and travel emergency label
    And I should not see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
      | medicalAssistAbroadTab |

  #MOB3-10711, 10596
  @primary
  Scenario Outline: TC_002 Main Success Scenario: User selects an option to call customer service when logged into the app
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I navigate to call us home page
    When I select the <option> tile on call us home page
    Then I am displayed the call us <page> for the selected option
    And I am able to view the call us button
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    Examples:
      | option              | page              |
      | Business accounts   | businessAccounts  |
      | Internet banking    | internetBanking   |
      | Other banking query | otherBankingQuery |

  Scenario: TC_004 Alternative Scenario: User views Business Account call us page when not logged into the app
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I select the contact us option from the MI page pre auth header
    Then I should be on pre auth call us home page overlay
    And I should be able to view these call us tiles
      | callUsTiles           |
      | businessAccountsTile  |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should be able to view close button on call us page
    And I should not see security and travel emergency label
    And I should not see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
      | medicalAssistAbroadTab |

  Scenario Outline: TC_005 Alternative Scenario: User selects an option to call customer service when not logged into the app
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    When I select the contact us option from the MI page pre auth header
    And I select the <option> tile on call us home page
    Then I am displayed the call us <page> for the selected option
    And I am able to view the call us button
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    Examples:
      | option              | page              |
      | Business accounts   | account           |
      | Internet banking    | internetBanking   |
      | Other banking query | otherBankingQuery |
