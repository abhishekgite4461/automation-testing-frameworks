@team-mpt @nga3 @bnga @stub-enrolled-valid
Feature: Display Legal Info

  @primary @ios
  Scenario Outline: TC_001_Main Success Scenario - User views legal information
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I navigate to the legal info option from the settings page
    Then I should see the following links on legal info page
      |fieldsOnLegalInfoScreen       |
      |legalInformationText          |
      |legalAndPrivacy               |
      |cookieUseAndPermissions       |
      |thirdPartyAcknowledgements    |
    Examples:
      | accessLevel           |
      | FULL_SIGNATORY        |
      | FULL_ACCESS_DELEGATE  |
      | VIEW_ONLY_DELEGATE    |

  @manual
  Scenario: TC_002_Alternate Scenario - User views legal and privacy information
    Given I login to app as a "current and savings account" user
    And I select fullSignatoryBusiness business with full Signatory access from the business selection screen
    When I navigate to the legal info option from the settings page
    And I select the legal and privacy link
    Then I should see the legal and privacy information copy
    When I select to view any external link
    Then I must be displayed the win-back option
    When I select to cancel to go outside the app
    Then the win-back pop-up must be dismissed
    And I must be displayed the same page
    When I select to continue to view the information
    Then the win-back pop-up must be dismissed
    And I must be displayed the information in a new window

  @manual
  Scenario: TC_003_Alternate Scenario - User views cookie use and permissions information
    Given I login to app as a "current and savings account" user
    And I select fullSignatoryBusiness business with full Signatory access from the business selection screen
    Then I should see the cookie use and permissions information copy
    When I select to view any external link
    Then I must be displayed the win-back option
    When I select to cancel to go outside the app
    Then the win-back pop-up must be dismissed
    And I must be displayed the same page
    When I select to continue to view the information
    Then the win-back pop-up must be dismissed
    And I must be displayed the information in a new window
