@team-mpt @nga3 @bnga
Feature: Logoff initiated via user
  In order to terminate my app session
  As a BNGA logged in user
  I want to log-off from the BNGA app

  Background:
    Given I am an enrolled "current" user logged into the app

  # MPT-750,719
  @stub-enrolled-valid @primary
  Scenario: TC001_User initiated logs off from the app
    And I select a business from the business selection screen
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I select the contact us option from the logoff page pre auth header
    Then I should be on pre auth call us home page overlay

  #MPT-721, MPT-745
  @stubOnly @stub-survey-link
  Scenario: TC002_User should see NPS survey button on Logoff screen
    When I select a business from the business selection screen
    And I navigate to logoff via more menu
    Then I should see the NPS survey button on the logout page

  #MPT-721, MPT-745
  @manual
  Scenario: TC003_User should be able to navigate to survey screen from the logoff screen
    And I select a business from the business selection screen
    And I navigate to logoff via more menu
    When I select the NPS survey button on the logout page
    Then I should be navigated to mobile browser with 3rd party NPS URLs
    And I verify client app has passed correct parameters with NPS survey URL as mentioned in below table
      | Brand          | NPS URL                                                                                 | cper id |
      | LloydsBank     | http://survey.euro.confirmit.com/wix/p1844758208.aspx?brand=LloydsBank&cper=12345678    | ocsID   |
      | BankOfScotland | http://survey.euro.confirmit.com/wix/p1844758208.aspx?brand=BankOfScotland&cper=12345678| ocsID   |
