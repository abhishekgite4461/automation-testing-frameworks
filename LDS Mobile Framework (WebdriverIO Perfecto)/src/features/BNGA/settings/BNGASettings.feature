@team-statements @nga3 @bnga @stub-enrolled-valid @stubOnly
Feature: BNGA user views settings menu
  In order to view and change my personal, business and security settings
  As a BNGA user
  I want to view my settings menu

  #COSE-182, STAMOB-1
  @primary
  Scenario Outline: TC-001 - Main Success Scenario - User wants to view their settings menu options
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I navigate to settings page from home page
    Then I should see the below options in the settings page
      | optionsInSettingsPage  |
      | personalDetailsButton  |
      | businessDetailsButton  |
      | securitySettingsButton |
      | resetAppButton         |
      | legalInfo              |
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  #COSE-183
  @primary @pending
  Scenario Outline: TC-002 - Main Success Scenario - User views personal details
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I navigate to settings page from home page
    When I select your personal details option in settings page
    Then I should be shown the list of personal details
      | personalDetail |
      | Full Name      |
      | User ID        |
      | Mobile Number  |
      | Home Number    |
      | Work Number    |
    And All the digits in the contact numbers must not be displayed and masked
    And Only the first four and last two digits in the contact numbers must be displayed
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  @manual
  Scenario Outline: TC-003 – Exception Scenario - User is not displayed Home and Work number
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I navigate to settings page from home page
    When I select your personal details option in settings page
    And I have not given my home and work number
    Then I should not see the Home number and Work number field
    And I should be shown the list of personal details
      | personalDetail         |
      | Full Name              |
      | User ID                |
      | yourContactDetailsText |
      | Mobile Number          |
    And All the digits in the contact numbers must not be displayed and masked
    And Only the first four and last two digits in the contact numbers must be displayed
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  @manual
  Scenario Outline: TC-004 – Exception Scenario - User is not displayed Mobile number
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I navigate to settings page from home page
    When I select your personal details option in settings page
    And I have not given my mobile number
    Then I should not see the mobile number field
    And I should be shown the list of personal details
      | personalDetail         |
      | Full Name              |
      | User ID                |
      | yourContactDetailsText |
      | yourContactNumbersText |
    And All the digits in the contact numbers must not be displayed and masked
    And Only the first four and last two digits in the contact numbers must be displayed
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  #COSE-181, STAMOB-3
  @primary
  Scenario Outline: TC-005 - Main Success Scenario - User views business details
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I navigate to settings page from home page
    When I select the your Business Details option in settings page
    Then I should be shown the list of business details
      | businessDetail                 |
      | business Name                  |
      | business RegisteredAddressText |
      | business Address               |
      | business Postcode              |
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  #MPT-2976, MPT-2977
  @primary
  Scenario Outline: TC-005 - Main Success Scenario - User wants to view their security settings menu options
    Given I am on home screen as <accessLevel> access user with CURRENT account
    When I navigate to settings page from home page
    And I select the security settings option from the settings page
    Then I should see the security settings page
    And I should see the following options in security details page
      | securityDetailsOptions  |
      | User ID                 |
      | App version             |
      | Device name             |
      | Device type             |
      | Forgotten your password |
      | Auto logoff             |

    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |
