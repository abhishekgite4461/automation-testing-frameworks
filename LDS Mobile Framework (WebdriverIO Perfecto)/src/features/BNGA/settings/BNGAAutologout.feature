@team-mpt @nga3 @bnga
Feature: Verify auto logout settings

  #MPT-2976
  @stub-single-business-full-signatory-enrolled @primary
  Scenario Outline: TC-001 - Auto log off - Immediate from security settings
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    And I select the desired "<autoLogOffTime>" from the auto log off page
    And I put the app in the background
    And I launch the app from the background
    Then I should be on the logout page

    Examples:
      | autoLogOffTime  |
      | logoffImmediate |

  @stub-single-business-full-signatory-enrolled @primary
  Scenario Outline: AC01 TC_002_To verify if the user is getting logged off from the app due to inactivity as per the selected autolog off time settings
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    And I select the desired "<autoLogOffTime>" from the auto log off page
    Then I should see a log off alert 15 seconds before the "<autoLogOffTime>" auto logoff time
    And I must be logged out of the app after the selected auto logoff time

    Examples:
      | autoLogOffTime |
      | 1 Minute       |

  #MPT-754, MPT-717
  @stub-enrolled-valid @primary
  Scenario Outline: AC01 TC_003_To verify if the user is getting logged off from the app after selecting log off option from the log off alert pop up
    Given I am an enrolled "valid" user logged into the app
    When I select a business from the business selection screen
    And I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    And I select the desired "<autoLogOffTime>" from the auto log off page
    Then I should see a log off alert 15 seconds before the "<autoLogOffTime>" auto logoff time
    And I select the logoff option from the logoff alert pop up
    And I should be on the logout page

    Examples:
      | autoLogOffTime |
      | 2 Minute       |

  #MPT-754, MPT-717
  @stub-enrolled-valid
  Scenario Outline: AC01 TC_004_To verify if the user is not getting logged off from the app after selecting continue option from the log off alert pop up
    Given I am an enrolled "valid" user logged into the app
    When I select a business from the business selection screen
    And I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    And I select the desired "<autoLogOffTime>" from the auto log off page
    And I should see a log off alert 15 seconds before the "<autoLogOffTime>" auto logoff time
    And I select the continue option from the logoff alert pop up
    Then I should be on the security settings page after the log out time selected

    Examples:
      | autoLogOffTime |
      | 1 Minute       |

  @stub-enrolled-valid
  Scenario: TC-005 - Confirm button disabled before selecting the auto logout timings
    Given I reinstall the app
    And I am an enrolled "valid" user logged into the app
    When I select a business from the business selection screen
    And I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    Then I must see the confirm button disabled in auto logout settings page

  @manual
  Scenario: TC_006_To verify if the user is able to view successful auto log off toast message post updating the auto log off timer settings
    Given I am an enrolled "valid" user logged into the app
    When I select a business from the business selection screen
    And I navigate to security settings page from home page
    And I select save option from the security settings page
    Then I should be able to see an auto log off settings successfully updated toast message for 3 seconds

  @manual @primary
  Scenario Outline: TC_007_To verify if the user is able to successfully get logged off after the default auto logoff timeout in unenrolled state
    Given I am a valid user
    And I enter my username and password
    When I navigate to <pageName> screen
    And I am inactive for default auto log off time i.e 10 minutes
    Then I must see the logout page with session timed out message
    And I must be able to log back into app using "Log back into app" option

    Examples:
      | pageName            |
      | enter MI            |
      | card reader identity|
      | card reader respond |
      | business selection  |

  @manual @primary
  Scenario Outline: TC_008_To verify if the user is able to successfully get logged off after the default auto logoff timeout in enrolled state
    Given I am an enrolled "valid" user logged
    And my auto-logoff time is set to <autoLogOffTime>
    And I am displayed Memorable Information screen
    When I <actionToBePerformed>
    Then I must see the logout page with session timed out message
    And I must be able to log back into app using "Log back into app" option

    Examples:
      | autoLogOffTime |actionToBePerformed            |
      | 10 Minute      |remain inactive for 10 minutes |
      |logoffImmediate |exit and relaunch the app      |
