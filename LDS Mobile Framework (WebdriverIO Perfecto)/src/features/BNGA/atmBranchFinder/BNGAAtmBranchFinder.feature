@team-statements @team-mpt @nga3 @bnga @manual
#ATM branch finder taken out of the application to the default maps on the device, can't be validated in automation.
# Marking it as manual
Feature: User wants to search for nearby ATM or Branches
  In order to withdraw money or visit a bank branch
  As a BNGA user
  I want to search for nearby ATM or Bank Branch location in the BNGA app

  @primary
  Scenario Outline: TC_001 Main Success Scenario: User views searchresults for Branches and ATMs
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I select support tab from bottom navigation bar
    And I am on the atm and branch finder page
    When I select the <tile_type> option on the atm and branch finder page
    Then I should be taken out of the application to the default maps on the device
    And I am displayed the <search_result>
    Examples:
      | accessLevel          | tile_type       | search_result |
      | FULL_SIGNATORY       | search branches | branches only |
      | FULL_ACCESS_DELEGATE | find nearby ATM | ATMs only     |
      | VIEW_ONLY_DELEGATE   | search branches | branches only |

  Scenario Outline: TC_002 Main Success Scenario: User searches branch based on brands Lloyds, Halifax and BOS
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I select support tab from bottom navigation bar
    And I am on the atm and branch finder page
    When I search based on <brand_type> brand
    Then I am displayed the results based on the <branch_type> brands
    Examples:
      | brand_type | branch_type     |
      | Lloyds     | Lloyds          |
      | BoS        | Halifax and BoS |
