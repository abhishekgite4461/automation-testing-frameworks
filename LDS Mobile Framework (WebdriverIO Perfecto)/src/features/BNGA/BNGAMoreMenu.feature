@team-payments @nga3 @bnga
Feature: User wants to access services/ sales journey using a menu
  In order to access important services and sales journeys
  As a BNGA customer
  I want to able to access a menu that provides

  # MOB3-10203, #MPT-671
  @stub-multiple-business-full-signatory @primary
  Scenario: TC_001_ Main Success: Full signatory user with multiple businesses displayed under the More Menu when user is fully authenticated
    Given I am an enrolled "Multiple Business Full Signatory With MI" user on the MI page
    When I enter correct MI
    Then I should see switch business screen
    When I select a business from the business selection screen
    And I select more menu
    Then I should see the below options in the more menu
      | optionsInMorePage    |
      | moreSwitchBusiness   |
      | yourProfile          |
      | settings             |
      | depositCheque        |
      | viewDepositHistory   |
      | logout               |

# MOB3-10203, #MPT-671
  @stub-multiple-business-full-delegate
  Scenario: TC_002_ Variation Success: Full access delegate with multiple businesses displayed under the More Menu when user is fully authenticated
    Given I am an enrolled "Multiple Business Full Delegate With MI" user on the MI page
    When I enter correct MI
    Then I should see switch business screen
    When I select a business from the business selection screen
    And I select more menu
    Then I should see the below options in the more menu
      | optionsInMorePage    |
      | Switch Business      |
      | Business name        |
      | Your profile         |
      | Settings and Info    |
      | Deposit cheque       |
      | View deposit history |
      | Log off              |

# MOB3-10203, #MPT-671
  @stub-multiple-business-view-only
  Scenario: TC_003_ Variation Success: View only delegate user with multiple businesses displayed global menu when user is fully authenticated
    Given I am an enrolled "Multiple Business View Only With MI" user on the MI page
    When I enter correct MI
    Then I should see switch business screen
    When I select a business from the business selection screen
    And I select more menu
    Then I should see the below options in the more menu
      | optionsInMorePage    |
      | Switch Business      |
      | Business name        |
      | Your profile         |
      | Settings and Info    |
      | Deposit cheque       |
      | View deposit history |
      | Log off              |

  # MOB3-11767
  @stub-single-business-full-signatory-enrolled @primary
  Scenario: TC_005_ Don't display switch business option when user has access to only one business
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select more menu
    Then I should not see switch business icon on More screen

  # MOB3-10203
  @stub-enrolled-valid
  Scenario Outline: TC_004_ User switches business and is displayed relevant Business Name
    Given I am on MI screen as MULTIPLE_BUSINESS business user with <accountType> account and <accessLevel1> access
    When I enter correct MI
    And I select <business1> business with <accessLevel1> access from the business selection screen
    And I select more menu
    Then I verify name of the <business1> on the more menu
    And  I select switch business icon on more menu
    When I select <business2> business with <accessLevel2> access from the business selection screen
    And I select more menu
    Then I verify name of the <business2> on the more menu
    Examples:
      | accountType | accessLevel1   | business1                     | accessLevel2         | business2   |
      | CURRENT     | FULL_SIGNATORY | Equity Investment Instruments | FULL_ACCESS_DELEGATE | Go Go Pizza |

  # MOB3-11767
  # Not In scope yet
  @pending
  Scenario: TC_006_ Business Selection Menu
    Given I am an enrolled "valid" user logged into the app
    And I am on Business Selection
    When I select menu option from Native header
    Then I should see the below options in the more menu
      | optionsInMorePage |
      | Contact us        |
      | Log off           |
