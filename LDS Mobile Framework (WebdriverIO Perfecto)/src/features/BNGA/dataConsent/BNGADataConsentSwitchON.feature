@team-statements @bnga @nga3 @sw-data-consent-on
Feature: Create Native Interstitial page to select all consents
  In order to provide consent for my cookie usage
  As a customer
  I must be prompted to make a selection

  Preconditions for Data Consent
  Data Consent Switch is ON
  User has not selected the Data Consent on the device previously
  User is not shown Whats New screen
  User is not shown Enrolment journey
  User is not shown Touch ID registration prompt
  User is not shown Touch ID change prompt
  User is not performing any App sign journey

  #TODO STAMOB-1118 STUB is borken
  @primary @stub-analytics-consent-single-business
  Scenario: TC001 Main Success Scenario : User with single business access accepts data consent on interstitial
    Given I submit "correct" MI as "single business data consent" user on the MI page
    And I have not made a selection for my data consent usage already on that device
    And I see a native manage BNGA data consent interstitial page without a back button
    When I select the option to accept the data consent
    Then I should be on the home page
    #Note: The value of the tag would be set to 1 once accepted and 0 if not set or rejected. This is manual testing
    # the cookies setting stored in the app must be set to 1

  #TODO STAMOB-1118 STUB is borken
  @primary @stub-analytics-consent-unset
  Scenario: TC002 Main Success Scenario: User with access to multiple business and different level of access, accepts data consent on interstitial
    Given I submit "correct" MI as "multiple business data consent" user on the MI page
    And I have not made a selection for my data consent usage already on that device
    And I see a native manage BNGA data consent interstitial page without a back button
    When I select the option to accept the data consent
    Then I should see business selection screen
    #Note: The value of the tag would be set to 1 once accepted and 0 if not set or rejected. This is manual testing
    #BNGA user: The user must be with different access level (Full Signatory, Full Delegate, View Only Delegate) to different businesses.
    #the cookies setting stored in the app must be set to 1

  #TODO STAMOB-1118 STUB is borken
  @primary @stub-analytics-consent-single-business
  Scenario: TC003 Negative Scenario: User with single business access does not accept data consent on interstitial
    Given I submit "correct" MI as "single business data consent" user on the MI page
    And I have not made a selection for my data consent usage already on that device
    And I see a native manage BNGA data consent interstitial page without a back button
    When I select the option not to accept the data consent
    Then I should be on the home page
    #Note: The value of the tag would be set to 1 once accepted and 0 if not set or rejected. This is manual testing
    #the cookies setting stored in the app must be set to 1

  @stub-analytics-consent-unset
  Scenario: TC004 Negative Scenario: User with access to multiple business and different level of access, does not accept data consent on interstitial
    Given I submit "correct" MI as "multiple business data consent" user on the MI page
    And I have not made a selection for my data consent usage already on that device
    And I see a native manage BNGA data consent interstitial page without a back button
    When I select the option not to accept the data consent
    Then I should see business selection screen
    #Note: The value of the tag would be set to 1 once accepted and 0 if not set or rejected. This is manual testing
    #BNGA user: The user must be with different access level (Full Signatory, Full Delegate, View Only Delegate) to different businesses.

  #android only
  @secondary @stub-analytics-consent-unset @android
  Scenario: TC005 Exceptional Scenario: Verify that the device Back button is suppressed on interstitial page
    Given I login to app as a "data consent" user
    And I see a native manage BNGA data consent interstitial page without a back button
    When I select back key on the device
    Then No action must be taken on the data consent page

  @secondary @stub-analytics-consent-unset @android
  Scenario: TC006 – Exceptional Scenario: User selects to stay on native data consent interstitial page
    Given I am on MI screen as FULL_SIGNATORY access user with CURRENT account
    And I enter correct MI
    And I see a native manage BNGA data consent interstitial page without a back button
    And I select the cookies policy link on the interstitial page
    And I'm presented with a winback option on data consent page
    When I opt to stay on the data consent page
    Then I should remain on the native data consent interstitial page
    When I select the cookies policy link on the interstitial page
    And I opt to leave the data consent page
    Then I must be taken out of my journey to external page

  @secondary @stub-analytics-consent-unset
  Scenario: TC007 – Validation Scenario: Verify that the confirm button is disabled if no selection is made
    Given I am on MI screen as FULL_SIGNATORY access user with CURRENT account
    And I enter correct MI
    And I see a native manage BNGA data consent interstitial page without a back button
    When I have not made a selection for my option and try to confirm my choice
    Then The confirm button should be disabled

  @primary @stub-enrolled-valid
  Scenario: TC008 Main Success Scenario : User navigates to manage data consent in settings menu
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I have navigated to Personal details from settings
    When I select the manage data consent
    Then I should be displayed the manage data consent page
    And I should have option to make changes to my data consent settings

  @primary @stub-enrolled-valid
  Scenario Outline: TC009 Main Success Scenario: User does not accept data consent in settings menu
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I have navigated to Personal details from settings
    And I select the manage data consent
    And I have accepted the data consent policy earlier
    And I now I have declined to give consent to data consent settings
    When I confirm the changes to my data consent settings
    Then I should be on the your personal details page
    And I select the manage data consent
    And now I have accepted to give consent to data consent settings
    When I confirm the changes to my data consent settings
    Then I should be on the your personal details page
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
      | VIEW_ONLY_DELEGATE   |

  #Note: BNGA user: The user must be with different access level (Full Signatory, Full Delegate, View Only Delegate) to different businesses
  # And the cookies setting stored in the app must be set to 0
  #android only
  @secondary @stub-enrolled-valid @android
  Scenario: TC010- Validation Scenario: Verify that the device back button is not suppressed for data consent on settings menu
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I have navigated to Personal details from settings
    When I select the manage data consent
    And I select back key on the device
    Then I should be on the your personal details page

  @manual
  Scenario: TC011 - Setting tag value for data consent
    Given I navigate to the manage data consent page
    When I make a selection for my data consent for that device
    And I confirm my selection on the data consent page
    Then the data consent setting stored in the app must be 1

  @stub-enrolled-valid
  Scenario: TC012 – Validation Scenario: No winback if no changes are made on data consent page in settings menu
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I have navigated to Personal details from settings
    And I select the manage data consent
    When I navigate away from the manage data consent page
    Then I should not be shown the winback option

  #android only
  @stub-enrolled-valid
  Scenario: TC013 – Validation Scenario: No winback if no changes are made on data consent page in settings menu and navigate back via back button
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I have navigated to Personal details from settings
    And I select the manage data consent
    When I navigate from the manage data consent page via device back button
    Then I should be on the your personal details page

  #Redesign bug, will be covered part of their backlog
  #android only
  @stub-enrolled-valid @pending
  Scenario: TC014 - Native back button triggers a Winback prompt when changes have been made to data consent in settings menu
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I have navigated to Personal details from settings
    And I select the manage data consent
    And I have made changes to the data consent
    When I navigate away from the manage data consent page
    Then I should be shown the header winback option

  #android only
  @stub-enrolled-valid @android
  Scenario: TC015 – User stays on the manage BNGA data consent page in settings menu
    Given I am on home screen as FULL_SIGNATORY access user with CURRENT account
    And I have navigated to Personal details from settings
    And I select the manage data consent
    And I select the cookies policy link on the consent page
    And I should be shown the winback option
    When I opt to stay on the data consent page
    Then I must be on the manage BNGA data consent page
    And I select the cookies policy link on the consent page
    When I opt to leave the data consent page
    Then I must be taken out of my journey to external page
