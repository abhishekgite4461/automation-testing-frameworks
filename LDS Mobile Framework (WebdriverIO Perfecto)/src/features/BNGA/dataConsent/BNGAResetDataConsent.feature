@team-statements @bnga @nga3 @sw-data-consent-on @android @environmentOnly
  #MQE-830 BNGA new framework changes should accommodate data consent scenarios
Feature: Reset Data Consent
  In order to ensure that my device does not have my privacy settings saved
  As a BNGA customer
  I want my privacy setting cleared when I reset my app

  @primary
  Scenario Outline: TC001 Main Success Scenario : App reset to clear Privacy Management settings
    Given I am on home screen as <accessLevel> access user with CURRENT account
    And I have navigated to Personal details from settings
    And I select the manage data consent
    When I have set my privacy setting already on device
    And I reset the app
    And I relaunch the app
    And I am on home screen as <accessLevel> access user with CURRENT account
    And I have navigated to Personal details from settings
    And I select the manage data consent
    Then The data consent settings on that device must be cleared
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | VIEW_ONLY_DELEGATE   |

  Scenario Outline: TC002 Main Success Scenario : Uninstall App to clear Privacy Management settings
    Given I reinstall the app
    And I am on home screen as <accessLevel> access user with CURRENT account
    When I have navigated to Personal details from settings
    And I select the manage data consent
    Then The data consent settings on that device must be cleared
    Examples:
      | accessLevel          |
      | FULL_SIGNATORY       |
      | FULL_ACCESS_DELEGATE |
