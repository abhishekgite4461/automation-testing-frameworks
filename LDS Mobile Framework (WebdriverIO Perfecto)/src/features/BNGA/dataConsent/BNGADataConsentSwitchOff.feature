@team-statements @bnga @nga3  @stub-enrolled-valid @sw-data-consent-off
Feature: Data Consent prompt switch is turned off
  In order to provide consent for my cookie usage
  As a customer
  I must be prompted to make a selection

  Preconditions for Data Consent prompt switch is turned off
  Data Consent Switch is OFF
  User has not selected the Data Consent on the device previously
  User is not shown Whats New screen
  User is not shown Enrolment journey
  User is not shown Touch ID registration prompt
  User is not shown Touch ID change prompt
  User is not performing any App sign journey

  Scenario Outline: TC001 Main Success Scenario – User is not displayed Data Consent Interstitial
    Given I am on MI screen as <accessLevel> business user with CURRENT account
    And I enter correct MI
    And I should not see the data consent interstitial
    And I select <businessName> business with <accessLevel> access from the business selection screen
    And I have navigated to Personal details from settings
    Then The link to the data consent isn't displayed
    Examples:
      | businessName             | accessLevel          |
      | fullSignatoryBusiness    | FULL_SIGNATORY       |
      | fullDelegateBusiness     | FULL_ACCESS_DELEGATE |
      | delegateViewOnlyBusiness | VIEW_ONLY_DELEGATE   |
