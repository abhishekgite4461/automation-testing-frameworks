@team-pi @nga3 @bnga @sw-enrolment-on @sw-light-logon-on @sw-mobile-chat-ios-on @sw-mobile-chat-android-on @ff-mobile-chat-on @stubOnly @android
#Android tag can be removed once iOS stories are implemented
Feature: Verify user successfully taken to chat window page
  In order to resolve my query quickly
  As a Retail NGA User
  I want to allow customer to engage with LBG agents in Connect in their own time and anytime

  @stub-single-business-full-signatory-enrolled
  Scenario: TC_001 Chat to Us Entry Point is displayed for single full signatory business
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select support tab from bottom navigation bar
    Then I should see message us option

  @stub-single-business-full-delegate-enrolled
  Scenario: TC_002 Chat to Us Entry Point is displayed for single full delegate business
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_ACCESS_DELEGATE access
    When I select support tab from bottom navigation bar
    Then I should see message us option

  @stub-single-business-view-only-enrolled
  Scenario: TC_003 Chat to Us Entry Point not displayed for single view only customers
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select support tab from bottom navigation bar
    Then I should not see message us option

  @stub-multiple-business-full-signatory
  Scenario: TC_004 Chat to Us Entry Point is displayed for multiple full signatory customers
    Given I am an enrolled "multiple business" user logged into the app
    And I select a business from the business selection screen
    When I select support tab from bottom navigation bar
    Then I should see message us option

  @stub-multiple-business-full-delegate
  Scenario: TC_004 Chat to Us Entry Point is displayed for multiple full delegate customers
    Given I am an enrolled "multiple business" user logged into the app
    And I select a business from the business selection screen
    When I select support tab from bottom navigation bar
    Then I should see message us option

  @stub-multiple-business-view-only
  Scenario: TC_004 Chat to Us Entry Point is not displayed for multiple view only customers
    Given I am an enrolled "multiple business" user logged into the app
    And I select a business from the business selection screen
    When I select support tab from bottom navigation bar
    Then I should not see message us option
