@team-mpt @nga3 @bnga @stub-enrolled-valid @ff-bnga-support-hub-on @primary
Feature: Verify user successfully taken to the support hub page
  As a customer
  I want access to a support hub
  So that I can find the assistance I need And resolve any queries

  Background:
    Given I am an enrolled "current" user logged into the app
    And I select a business from the business selection screen
    And I select support tab from bottom navigation bar

  Scenario: TC001_Verify customer Displayed the corresponding landing pages through support hub
    When I select call us on support hub page
    Then I should be on the call us home page
    And I navigate back
    When I select "reset password" option in the support hub page
    Then I should be on the password reset page
    And I navigate back
    When I select "atm and branch" option in the support hub page
    Then I should be on the atm and branch finder page

  @manual
  Scenario: TC002_Exception Scenario-User does not have email account set up on the device
    When I select "provide app feedback" option in the support hub page
    Then I should be displayed an error message in an overlay informing that I do not have an email account set up
    And I select to dismiss the overlay
    And I should be on the support hub page

  @manual
  Scenario: TC002_Customer should be displayed email form after selecting app feedback
    When I select the 'App feedback' option in the support hub page
    Then I should be displayed a feedback form
