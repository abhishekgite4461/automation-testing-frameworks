@team-login @nga3 @bnga @stub-enrolled-valid @pending
Feature: Validate the options and account tiles in the home page - BNGA
#This feature file covers only validating the generic option in the action menu, respective feature team will validate the entry points.

  Scenario: AC01 TC_001_Validating the options on the home page
    Given I am an enrolled "current" user logged into the app
    And I should see the below correct options in the home page
      | optionsInHomePage |
      | callUsTile        |
