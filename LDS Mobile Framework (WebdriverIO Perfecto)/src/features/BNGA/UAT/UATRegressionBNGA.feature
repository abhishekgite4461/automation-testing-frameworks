@team-uat @bnga @nga3 @environmentOnly
Feature: BNGA UAT Regression

  Scenario: BNGA Account Overview
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    Then bottom navigation bar is displayed
    And I should see the below options in the bottom nav
      | optionsInBottomNav |
      | home               |
      | apply              |
      | payAndTransfer     |
      | support            |
      | more               |
    And I navigate and validate the options in the CURRENT account in below table in the home page
      | accountFields      |
      | accountNumber      |
      | sortCode           |
      | accountBalance     |
      | accountInfoBalance |
    When I select CURRENT account tile from home page
    Then I should be displayed current statements transactions
    When I navigate back
    And I select the action menu of CURRENT account
    Then I should see the below options in the action menu of the CURRENT account
      | optionsInActionMenu      |
      | viewTransactionsOption   |
      | chequeDepositOption      |
      | standingOrderOption      |
      | directDebitOption        |
      | viewPendingPaymentOption |
    When I close the action menu in the home page
    Then I navigate and validate the options in the SAVING account in below table in the home page
      | accountFields  |
      | accountNumber  |
      | sortCode       |
#      | interestRate       |
      | accountBalance |
#      | accountInfoBalance |
    When I select SAVING account tile from home page
    Then I should be displayed current statements transactions
    And I navigate back
    When I select the action menu of SAVING account
    Then I should see the below options in the action menu of the SAVING account
      | optionsInActionMenu    |
      | viewTransactionsOption |
      | chequeDepositOption    |
      | viewInterestRateOption |
    When I close the action menu in the home page
    Then bottom navigation bar is displayed

  Scenario: BNGA View FSCS tile on MI page
    Given I am on MI screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select FSCS tile in MI page
    And I select leave app in MI page
    Then I should see mobile banking page

  Scenario: BNGA Validate More hub Options
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select more tab from bottom navigation bar
    Then I should see the below options in the more menu
      | optionsInMorePage  |
      | yourProfile        |
      | settings           |
      | depositCheque      |
      | viewDepositHistory |
      | logout             |
    When I select the your profile option on more menu
    Then I should be on the your personal details page
    When I navigate back
    And I select settings from more menu
    Then I should be on the settings page
    When I navigate back
    And I navigate to cheque deposit page via more menu
    Then I should be on the cheque deposit screen
    When I navigate back
    And I navigate to deposit history page via more menu
    Then I should be on deposit history page
    And I navigate back
    And I navigate to logoff via more menu

  Scenario: BNGA Setting page validations
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select more menu
    And I select settings from more menu
    And I select your personal details option in settings page
    Then I should be on the your personal details page
    When I navigate back
    And I select the your Business Details option in settings page
    Then I should be shown the list of business details
      | businessDetail                 |
      | business Name                  |
      | business RegisteredAddressText |
      | business Address               |
      | business Postcode              |
    When I navigate back
    And I select the Your Security Details option in settings page
    Then I should see the security settings page
    When I navigate back
    And I select the reset app option from the settings page
    And I select reset app button from the reset app page
    And I select cancel button from the reset app confirmation pop up
    Then I should be on the reset app Page
    When I navigate back
    And I navigate to the legal info option from the settings page
    Then I am on the legal information page

  Scenario: BNGA User Profile View
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select more menu
    And I select settings from more menu
    And I select the your Business Details option in settings page
    Then I should be shown the list of business details
      | businessDetail                 |
      | business Name                  |
      | business RegisteredAddressText |
      | business Address               |
      | business Postcode              |
    When I navigate back
    And I select more tab from bottom navigation bar
    And I navigate to profile page via more menu
    Then I should be on the your personal details page
    And I should be shown the list of personal details
      | personalDetail |
      | Full Name      |
      | User ID        |
      | Mobile Number  |
      | Home Number    |
      | Work Number    |
    When I navigate to manage data consent page
    Then I must be on the manage BNGA data consent page

  Scenario: BNGA Validate Call Us
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select support tab from bottom navigation bar
    And I select call us on support hub page
    Then I should be on the call us home page
    And I should be able to view these call us tiles
      | callUsTiles           |
      | businessAccountsTile  |
      | internetBankingTile   |
      | otherBankingQueryTile |
    When I select the Business accounts tile on call us home page
    Then I am displayed the call us businessAccounts for the selected option
    And I am able to view the call us button
    When I navigate back
    And I select the Internet banking tile on call us home page
    Then I am displayed the call us internetBanking for the selected option
    And I am able to view the call us button
    When I navigate back
    And I select the Other banking query tile on call us home page
    Then I am displayed the call us otherBankingQuery for the selected option
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    And I am able to view the call us button

  Scenario: BNGA Support Hub Validation
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select support tab from bottom navigation bar
    And I select call us on support hub page
    Then I should be on the call us home page
    When I navigate back
    And I select "reset password" option in the support hub page
    Then I should be on the password reset page
    When I navigate back
    And I am on the atm and branch finder page
    Then I am displayed the following search options
      | searchOption       |
      | searchBranchesTile |
      | findNearbyAtmTile  |
    When I navigate back
    And I select "provide app feedback" option in the support hub page
    Then I should be displayed an error message in an overlay informing that I do not have an email account set up
    And I select to dismiss the overlay
    And I should be on the support hub page

  Scenario: BNGA Product hub Overview
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select apply tab from bottom navigation bar
    Then the Featured option is shown as expanded
    And the other options are shown as closed accordion
    And I should see the product hub home page with product categories
      | productCategoryLabels  |
      | featured               |
      | businessCurrentAccount |
      | businessSavingsAccount |
      | cards                  |
      | loansAndFinance        |
      | mortgages              |
      | businessInsurance      |
      | international          |
      | businessGuidesAndInfo  |

  Scenario Outline: BNGA Saving Account Check interest rates
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select <type> account tile from home page
    And I select View Interest Details option of <type> account from action menu
    Then I should see the following fields in the details screen
      | fieldsInDetailsScreen   |
      | accountName             |
      | accountBalance          |
      | interestRateLabel       |
      | interestRateDescription |
    And I close the interest rate details screen using the close button
    Examples:
      | type   |
      | SAVING |

  Scenario Outline: BNGA Current Account Make Transfer from Home Page
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I am on the payment hub page
    And I submit the transfer with all the mandatory fields with from account as <fromAccount> and to account as <toAccount> with amount value 0.01
    And I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page
    When I select the view transaction option in the transfer success page
    And I should see the - £0.01 in the current month statement of the remitting account
    Examples:
      | fromAccount | toAccount |
      | CURRENT     | SAVING    |

  Scenario Outline: BNGA Current Account Make transfers through action menu
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select the "transfer and payment" option in the action menu of <fromAccount> account
    And I submit transfer with from account <fromAccount> and to account <toAccount> with amount 0.01
    And I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page
    When I select the make another transfer option in the transfer success page
    Then I should be navigated to the payment hub page with from account as <fromAccount> in the payment hub page
    Examples:
      | fromAccount | toAccount |
      | CURRENT     | SAVING    |

  Scenario Outline: BNGA Saving Account Make Transfer from Home Page
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I am on the payment hub page
    And I submit the transfer with all the mandatory fields with from account as <fromAccount> and to account as <toAccount> with amount value 0.01
    And I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page
    When I select the make another transfer option in the transfer success page
    Then I should be navigated to the payment hub page with from account as <fromAccount> in the payment hub page
    Examples:
      | fromAccount | toAccount |
      | SAVING      | CURRENT   |

  Scenario Outline: BNGA Saving Account Make transfers through action menu
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select the "transfer and payment" option in the action menu of <fromAccount> account
    And I submit transfer with from account <fromAccount> and to account <toAccount> with amount 0.01
    And I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page
    When I select the view transaction option in the transfer success page
    And I should see the - £0.01 in the current month statement of the remitting account
    Examples:
      | fromAccount | toAccount |
      | SAVING      | CURRENT   |

  Scenario Outline: BNGA Make payments to Existing Retail Beneficiary
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I am on the payment hub page
    When I select recipient <recipientName> to make payment to from searched list of recipients
    And I submit the payment with amount value <amount> and reference
    And I select the confirm option in the review payment page
    And I confirm the correct authentication password
    Then I should see the corresponding details in the payment success page
    And I should see share receipt option on the payment success page
    When I select the view transaction option in the payment success page
    And I select the first transaction in the account statement page
    Then I validate <recipientName> and <amount> details in VTD page
    When I select the help with this transaction link in VTD screen
    Then I must be displayed the Unsure About Transactions Help and Info screen
    And I navigate back
    And I close the posted transactions
    Examples:
      | recipientName | amount |
      | AUTOHFX02     | 0.01   |

  Scenario Outline: BNGA Make payments to Existing Commercial Beneficiary
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I am on the payment hub page
    When I select recipient <recipientName> to make payment to from searched list of recipients
    And I submit the payment with amount value <amount> and reference
    And I select the confirm option in the review payment page
    And I confirm the correct authentication password
    Then I should see the corresponding details in the payment success page
    And I should see share receipt option on the payment success page
    And I should see view transaction and make another payment option in the payment success page
    When I select the make another payment option in the payment success page
    Then I should be navigated to the payment hub page with from account as CURRENT in the payment hub page
    Examples:
      | recipientName | amount |
      | SEAUTOCOM     | 0.01   |

  Scenario: BNGA Transaction Details Saving and current account
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select CURRENT account tile from home page
    Then I should be displayed current statements transactions
    When I select one of the posted transaction
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdContainer            |
      | date                    |
      | vtdCloseButton          |
      | vtdAmount               |
      | unsureTransactionButton |
    When I select the help with this transaction link in VTD screen
    Then I must be displayed the Unsure About Transactions Help and Info screen
    When I navigate back
    And I close the posted transactions
    Then I should be on the statements page of the selected CURRENT account
    When I swipe to see previous month transactions on the account statement page
    And I select one of the posted transaction
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdContainer            |
      | date                    |
      | vtdCloseButton          |
      | vtdAmount               |
      | unsureTransactionButton |
    And I close the posted transactions
    And I select home tab from bottom navigation bar
    When I select SAVING account tile from home page
    Then I should be on the statements page of the selected SAVING account
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    When I swipe to see previous month transactions on the account statement page
    Then I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |

  Scenario Outline: BNGA Make Future Date Payment to Existing Retail Beneficiary and check Payments due soon
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I am on the payment hub page
    When I select recipient <recipientName> to make payment to from searched list of recipients
    And I enter amount in the amount field as <amount>
    And I select calendar picker option for future date payment
    And I select the date as <days> from today's date from calendar view
    And I should be able to continue with the payment
    And I select the confirm option in the review payment page
    And I confirm the correct authentication password
    Then I should see the payment successful message page
    When I select the view transaction option in the payment success page
    And I select the viewPaymentsDueSoon option from the action menu in statements page
    Then I should see dueSoon tab in statements screen
    And I should see following fields in due soon page
      | fieldsToValidate            |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    And I should not see payments due soon option on the action menu
    When I select one of the pending payment transaction
    Then I should see upcoming transaction details for that transaction with the following fields
      | fieldsInDetailScreen  |
      | dueSoonLozenge        |
      | datePendingPayment    |
      | vtdCloseButton        |
      | vtdAmount             |
      | outgoingPaymentText   |
      | payeeField            |
      | sortCodeAccountNumber |
      | referenceField        |
      | changeOrDeleteText    |
    Examples:
      | recipientName | amount | days |
      | FUAUTOHFX02   | 0.01   | 2    |

  Scenario Outline: BNGA Make Future Date Payment to Existing Commercial Beneficiary and check Payments due soon
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I am on the payment hub page
    When I select recipient <recipientName> to make payment to from searched list of recipients
    And I enter amount in the amount field as <amount>
    And I select calendar picker option for future date payment
    And I select the date as <days> from today's date from calendar view
    And I should be able to continue with the payment
    And I select the confirm option in the review payment page
    And I confirm the correct authentication password
    Then I should see the payment successful message page
    When I select the share option on the payment success page
    And I am on the Share Preview screen
    Then I should see the Bank Icon at the top of the screen
    And I should see the first 4 digits of the recipient account number is masked
    Examples:
      | recipientName | amount | days |
      | FUAUTOCOM     | 0.01   | 2    |

  Scenario Outline: BNGA Delete Existing beneficiary
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I navigate to pay and transfer from home page
    And I delete <number>, <recipientName> beneficiary from the list
    And I click on cancel searching
    And I navigate back to home page from payment hub
    Examples:
      | recipientName | number |
      | UATAUTODEL    | first  |

  Scenario: Branch and ATM Finder
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select support tab from bottom navigation bar
    And I am on the atm and branch finder page
    Then I am displayed the following search options
      | searchOption       |
      | searchBranchesTile |
      | findNearbyAtmTile  |
    And I am displayed a note message stating clicking search option links take you outside the app

  Scenario Outline: BNGA Leave payments before confirm
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I am on the payment hub page
    When I select recipient <recipientName> to make payment to from searched list of recipients
    And I submit the payment with amount value <amount> and reference
    And I select dismiss modal using close button
    Examples:
      | recipientName | amount |
      | AUTOHFX02     | 0.01   |

  Scenario Outline: BNGA Customer log out due to inactivity
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    And I select the desired "<autoLogOffTime>" from the auto log off page
    Then I should see a log off alert 15 seconds before the "<autoLogOffTime>" auto logoff time
    And I must be logged out of the app after the selected auto logoff time
    When I select back to logon button on the logout page
    And I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    And I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    And I select the desired "<inactivityTime>" from the auto log off page
    Examples:
      | autoLogOffTime | inactivityTime |
      | 1 Minute       | 10 Minute      |

  Scenario: BNGA Legal Information Page Validation
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select more menu
    And I select settings from more menu
    And I navigate to the legal info option from the settings page
    Then I am on the legal information page
    And I should see the following links on legal info page
      | fieldsOnLegalInfoScreen    |
      | legalInformationText       |
      | legalAndPrivacy            |
      | cookieUseAndPermissions    |
      | thirdPartyAcknowledgements |
    When I select the legal and privacy link
    Then I should be shown the legal and privacy information page
    And I navigate back
    When I select the cookie use and permissions link
    Then I should be shown the cookie use and permissions page
    And I navigate back
    When I select the third party acknowledgements link
    Then I should be shown the third party acknowledgements page

  Scenario: BNGA View and Delete StandingOrder for internal accounts
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select the "standing order" option in the action menu of CURRENT account
    Then I should be on the standing orders page
    And I should see the following fields on standing orders page
      | fieldsOnStandingOrderScreen |
      | dueDate                     |
      | reference                   |
      | paidTo                      |
      | standingOrderTab            |
      | directDebitTab              |
    When I select delete option in the standing order page
    And I select delete standing order in the delete standing order confirmation page
    Then I should see standing order deleted successfully message in view standing orders page

  Scenario: BNGA Change Password
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select support tab from bottom navigation bar
    And I select "reset password" option in the support hub page
    Then I should be on the password reset page
    And I navigate back
    When I navigate to settings page from home page
    And I select on Security settings link on settings page
    And I select the forgotten password option from the security settings page
    Then I should be on the password reset page
    When I enter my new valid password in the reset password page
#    And I select the submit button in the reset password page
#    Then I should see a password updated pop up in the reset password page
#    And I should be on the logout page
#    When I select back to logon button on the logout page
#    And I am a single business user on the home page

  Scenario: BNGA App de-register
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I navigate to settings page from home page
    And I select the reset app option from the settings page
#    And I successfully reset the app
#    Then I should be on the logout page

  Scenario Outline: BNGA view last month’s statement for charge cards and credit cards
    Given I am on home screen as SINGLE_BUSINESS business user with CURRENT account and FULL_SIGNATORY access
    When I select <type> account tile from home page
    And I navigate to previous 1 months transactions on the account statement page
    Then I should be displayed last month statement with the following fields in statements summary
      | fieldsInStatementsSummary   |
      | statementDateDescription    |
      | statementBalanceDescription |
      | minimumPaymentDescription   |
      | dueDateDescription          |
      | optionToFilterTransactions  |
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | transactionType             |
      | firstTranscationAmount      |
    And I should scroll down to the end of the transactions to view <output> message
    Examples:
      | type       | output      |
      | CREDITCARD | creditEndOf |
      | CHARGECARD | chargeEndOf |
