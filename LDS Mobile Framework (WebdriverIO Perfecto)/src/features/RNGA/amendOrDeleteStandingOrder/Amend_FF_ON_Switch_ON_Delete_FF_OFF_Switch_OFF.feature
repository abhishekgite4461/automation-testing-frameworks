@rnga @nga3 @team-payment-aggregator @stubOnly @sw-amend-standing-order-on @ff-delete-standing-order-off @sw-delete-standing-order-off
Feature: Native Standing Order SCA - Amend - FeatureFlag ON, Switch ON - Delete - FeatureFlag OFF, Switch OFF
  In Order to amend standing order using NGA app
  As a Retail NGA user
  I want to be able to amend or delete native standing order using nga app

  @stub-enrolled-valid
  #PAT-850, PAT-954, PAT-879, PAT-836, PAT-872, PAT-851, PAT-882, PAT-837, PAT-873, PAT-838, PAT-881
  Scenario: TC001 - Action Menu: User should see Native Amend Standing order page when Amend - FeatureFlag ON, Switch ON - Delete - FeatureFlag OFF, Switch OFF
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and without delete button
    And I should see standing orders set up for Adam Anderson account with amend and without delete button
    And I should see standing orders set up for Instant Cash ISA account without amend and without delete button
    When I select the set up standing order button
    Then I should see the from field pre-populated with islamicCurrent account

  @stub-enrolled-valid
  #PAT-850, PAT-954, PAT-879, PAT-851, PAT-882, PAT-837, PAT-873, PAT-838, PAT-881, PAT-1009, PAT-1010
  Scenario Outline: TC002 - Standing order - Success: User should see Native Amend Standing order page when Amend - FeatureFlag ON, Switch ON - Delete - FeatureFlag OFF, Switch OFF
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see a Standing Order set up success message
    When I select view standing orders in the standing order setup successful page
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and without delete button
    And I should see standing orders set up for Adam Anderson account with amend and without delete button
    And I should see standing orders set up for Instant Cash ISA account without amend and without delete button
    When I select the set up standing order button
    Then I should see the from field pre-populated with islamicCurrent account

    Examples:
      | fromAccount    | Amount | toAccount                        | frequency        |
      | islamicCurrent | 2      | currentAccountWithNoTransactions | Every two months |

  @stub-standing-order
  #PAT-850, PAT-954, PAT-879, PAT-878, PAT-893, PAT-851, PAT-882, PAT-837, PAT-873, PAT-838, PAT-881
  Scenario Outline: TC003 - Delete recipient with Standing order: User should see Native Amend Standing order page when Amend - FeatureFlag ON, Switch ON - Delete - FeatureFlag OFF, Switch OFF
    Given I am an enrolled "valid" user logged into the app
    When I navigate to the remitter account on the payment hub page
    And I select remitting AccountWithExternalBeneficiary Account from the account list
    And I select the recipient account on the payment hub page
    And I am on manage actions for a <type>
    And I select to delete a recipient
    And I am asked for confirmation if I want to delete the recipient
    And I select to confirm delete a recipient
    And I select view message
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and without delete button
    And I should see standing orders set up for Adam Anderson account with amend and without delete button
    And I should see standing orders set up for Instant Cash ISA account without amend and without delete button
    When I select the set up standing order button
    Then I should see the from field pre-populated with islamicCurrent account

    Examples:
      | type            |
      | ukBankRecipient |

  @stub-so-onhold
  #PAT-850, PAT-954, PAT-879, PAT-851, PAT-882, PAT-837, PAT-873, PAT-838, PAT-881
  Scenario Outline: TC004 - Standing order under review: User should see Native Amend Standing order page when Amend - FeatureFlag ON, Switch ON - Delete - FeatureFlag OFF, Switch OFF
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    And I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I review the Standing Order
    And I enter a correct password
    Then I should see the standing order under review page
    When I select standing order option on standing order under review page
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and without delete button
    And I should see standing orders set up for Adam Anderson account with amend and without delete button
    And I should see standing orders set up for Instant Cash ISA account without amend and without delete button
    When I select the set up standing order button
    Then I should see the from field pre-populated with islamicCurrent account

    Examples:
      | fromAccount    | Amount | toAccount                        | frequency        |
      | islamicCurrent | 2      | currentAccountWithNoTransactions | Every two months |
