@rnga @nga3 @team-payment-aggregator @stubOnly @ff-amend-standing-order-off @sw-amend-standing-order-off @sw-delete-standing-order-off
Feature: Native Standing Order SCA - Amend - FeatureFlag OFF, Switch OFF - Delete - FeatureFlag ON, Switch OFF
  In Order to amend standing order using NGA app
  As a Retail NGA user
  I want to be able to amend or delete native standing order using nga app

  @stub-enrolled-valid
  #PAT-850, PAT-954, PAT-879, PAT-836, PAT-872
  Scenario: TC001 - Action Menu: User should see SO Webview page when Amend - FeatureFlag OFF, Switch OFF - Delete - FeatureFlag ON, Switch OFF
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the amend standing order webview

  @stub-enrolled-valid
  #PAT-850, PAT-954, PAT-879
  Scenario Outline: TC002 - Standing order - Success: User should see SO Webview page when Amend - FeatureFlag OFF, Switch OFF - Delete - FeatureFlag ON, Switch OFF
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see a Standing Order set up success message
    When I select view standing orders in the standing order setup successful page
    Then I should see the amend standing order webview

    Examples:
      | fromAccount    | Amount | toAccount                        | frequency        |
      | islamicCurrent | 2      | currentAccountWithNoTransactions | Every two months |

  @stub-standing-order
  #PAT-850, PAT-954, PAT-879, PAT-878, PAT-893
  Scenario Outline: TC003 - Delete recipient with Standing order: User should see SO Webview page when Amend - FeatureFlag OFF, Switch OFF - Delete - FeatureFlag ON, Switch OFF
    Given I am an enrolled "valid" user logged into the app
    When I navigate to the remitter account on the payment hub page
    And I select remitting AccountWithExternalBeneficiary Account from the account list
    And I select the recipient account on the payment hub page
    And I am on manage actions for a <type>
    And I select to delete a recipient
    And I am asked for confirmation if I want to delete the recipient
    And I select to confirm delete a recipient
    And I select view message
    Then I should see the amend standing order webview

    Examples:
      | type            |
      | ukBankRecipient |

  @stub-so-onhold
  #PAT-850, PAT-954, PAT-879
  Scenario Outline: TC004 - Standing order under review: User should see SO Webview page when Amend - FeatureFlag OFF, Switch OFF - Delete - FeatureFlag ON, Switch OFF
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    And I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I review the Standing Order
    And I enter a correct password
    Then I should see the standing order under review page
    When I select standing order option on standing order under review page
    Then I should see the amend standing order webview

    Examples:
      | fromAccount    | Amount | toAccount                        | frequency        |
      | islamicCurrent | 2      | currentAccountWithNoTransactions | Every two months |
