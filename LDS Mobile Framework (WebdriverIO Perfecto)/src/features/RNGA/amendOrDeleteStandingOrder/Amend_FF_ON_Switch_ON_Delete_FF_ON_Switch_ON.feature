@rnga @nga3 @team-payment-aggregator @stubOnly @sw-amend-standing-order-on @sw-delete-standing-order-on
Feature: Native Standing Order SCA - Amend - FeatureFlag ON, Switch ON - Delete - FeatureFlag ON, Switch ON
  In Order to amend standing order using NGA app
  As a Retail NGA user
  I want to be able to amend or delete native standing order using nga app

  @primary @stub-enrolled-valid
  #PAT-850, PAT-954, PAT-879, PAT-836, PAT-872, PAT-851, PAT-882, PAT-847, PAT-883, PAT-837, PAT-873, PAT-838, PAT-881, PAT-965
  Scenario: TC001 - Action Menu: User should see Native Amend Standing order page when Amend - FeatureFlag ON, Switch ON - Delete - FeatureFlag ON, Switch ON
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    And I should see standing orders set up for Adam Anderson account with amend and with delete button
    And I should see standing orders set up for Instant Cash ISA account without amend and without delete button
    When I select the set up standing order button
    Then I should see the from field pre-populated with islamicCurrent account

  @stub-enrolled-valid
  #PAT-850, PAT-954, PAT-879, PAT-851, PAT-882, PAT-847, PAT-883, PAT-837, PAT-873, PAT-838, PAT-881, PAT-1009, PAT-1010, PAT-965
  Scenario Outline: TC002 - Standing order - Success: User should see Native Amend Standing order page when Amend - FeatureFlag ON, Switch ON - Delete - FeatureFlag ON, Switch ON
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see a Standing Order set up success message
    When I select view standing orders in the standing order setup successful page
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    And I should see standing orders set up for Adam Anderson account with amend and with delete button
    And I should see standing orders set up for Instant Cash ISA account without amend and without delete button
    When I select the set up standing order button
    Then I should see the from field pre-populated with islamicCurrent account

    Examples:
      | fromAccount    | Amount | toAccount                        | frequency        |
      | islamicCurrent | 2      | currentAccountWithNoTransactions | Every two months |

  @primary @stub-standing-order
  #PAT-850, PAT-954, PAT-879, PAT-878, PAT-893, PAT-851, PAT-882, PAT-847, PAT-883, PAT-837, PAT-873, PAT-838, PAT-881, PAT-965
  Scenario Outline: TC003 - Delete recipient with Standing order: User should see Native Amend Standing order page when Amend - FeatureFlag ON, Switch ON - Delete - FeatureFlag ON, Switch ON
    Given I am an enrolled "valid" user logged into the app
    When I navigate to the remitter account on the payment hub page
    And I select remitting AccountWithExternalBeneficiary Account from the account list
    And I select the recipient account on the payment hub page
    And I am on manage actions for a <type>
    And I select to delete a recipient
    And I am asked for confirmation if I want to delete the recipient
    And I select to confirm delete a recipient
    And I select view message
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    And I should see standing orders set up for Adam Anderson account with amend and with delete button
    And I should see standing orders set up for Instant Cash ISA account without amend and without delete button
    When I select the set up standing order button
    Then I should see the from field pre-populated with islamicCurrent account

    Examples:
      | type            |
      | ukBankRecipient |

  @stub-so-onhold
  #PAT-850, PAT-954, PAT-879, PAT-851, PAT-882, PAT-847, PAT-883, PAT-837, PAT-873, PAT-838, PAT-881, PAT-1009, PAT-1010, PAT-965
  Scenario Outline: TC004 - Standing order under review: User should see Native Amend Standing order page when Amend - FeatureFlag ON, Switch ON - Delete - FeatureFlag ON, Switch ON
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    And I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I review the Standing Order
    And I enter a correct password
    Then I should see the standing order under review page
    When I select standing order option on standing order under review page
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    And I should see standing orders set up for Adam Anderson account with amend and with delete button
    And I should see standing orders set up for Instant Cash ISA account without amend and without delete button
    When I select the set up standing order button
    Then I should see the from field pre-populated with islamicCurrent account

    Examples:
      | fromAccount    | Amount | toAccount                        | frequency        |
      | islamicCurrent | 2      | currentAccountWithNoTransactions | Every two months |

  @primary @stub-enrolled-valid
  #PAT-844, PAT-886
  Scenario: TC005 - Amend Standing Order - Success: User should be able to amend the Standing order with password validation
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    When I select amend button for the uKBank standing order
    Then I should be on the payment hub page
    When I update the standing order amount field with value as 5
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see a Standing Order set up success message

  @primary @stub-manage-payments-amend-so-no-step-up
  #PAT-844, PAT-886
  Scenario Outline: TC006 - Amend Standing Order - Success: User should be able to amend the Standing order without password validation
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    When I select amend button for the uKBank standing order
    Then I should be on the payment hub page
    When I update the standing order <standingOrderField> field with value as <correspondingValue>
    And I review the Standing Order
    Then I should not be prompted to enter my password
    And I should see a Standing Order set up success message

    Examples:
      | standingOrderField | correspondingValue |
      | nextPaymentDate    | Monthly            |
#      | paymentEndDate     | Monthly            |
      | frequency          | Weekly             |

  @primary @stub-enrolled-valid
  #PAT-966, PAT-1006
  Scenario: TC007 - Delete Standing Order: User should be able to delete the Standing order
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    When I select delete button for the ATWELL MARTIN LTD standing order
    Then I should see winback with Keep and Delete option
    And I should see standing order name ATWELL MARTIN LTD in the winback
    When I select the Keep option in winback
    Then I should see the Standing order screen for native amend standing order
    When I select delete button for the ATWELL MARTIN LTD standing order
    And I select the Delete option in winback
    Then I should not see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button

  #_____________________________________________________________________________________________________________________

  #   TECHNICAL VALIDATION SCENARIOS
  #_____________________________________________________________________________________________________________________

  @stub-enrolled-valid
  #PAT-841, PAT-875
  Scenario: TC008 - Amend Standing Order: User should see the Standing order page when user amends a standing order
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    When I select amend button for the ATWELL MARTIN LTD standing order
    Then I should be on the payment hub page
    And I should see the below fields for standing order are editable
      | amount          |
      | nextPaymentDate |
      | paymentEndDate  |
      | frequency       |
    And I should see the below fields for standing order are not editable
      | fromAccount |
      | toAccount   |
      | reference   |
    And I should see the continue button disabled in the payment hub page

  @stub-enrolled-valid
  #PAT-841, PAT-875
  Scenario Outline: TC009 - Amend Standing Order: User should see the continue button enabled for SO when user updates any field
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    When I select amend button for the ATWELL MARTIN LTD standing order
    Then I should be on the payment hub page
    And I should see the continue button disabled in the payment hub page
    When I update the standing order <standingOrderField> field with value as <correspondingValue>
    Then I should see the continue button enabled in the payment hub page

    Examples:
      | standingOrderField | correspondingValue |
      | amount             | 5                  |
      | nextPaymentDate    | Monthly            |
      | paymentEndDate     | Monthly            |
      | frequency          | Weekly             |

  @stub-enrolled-valid
  #PAT-842, PAT-884
  Scenario: TC010 - Amend Standing Order: User should see the selected values for SO on review SO page
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    When I select amend button for the ATWELL MARTIN LTD standing order
    Then I should be on the payment hub page
    And I should see the continue button disabled in the payment hub page
    When I update the standing order amount field with value as 5
    And I get the field values for the Standing order set up
    And I select the continue option in the payment hub page
    Then I should be on the review Standing order page
    And I should see the below fields on the Standing order review page
      | remitterName             |
      | remitterSortCode         |
      | remitterAccountNumber    |
      | benefeciaryName          |
      | benefeciarySortCode      |
      | benefeciaryAccountNumber |
      | amount                   |
      | reference                |
      | standingOrderDates       |
      | frequency                |
    When I select Edit option on standing order review page
    Then I should be on the payment hub page

  @stub-enrolled-valid
  #PAT-843, PAT-885
  Scenario: TC011 - Amend Standing Order: User should see the password pop-up when the user has amended the amount for the Standing order
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    When I select amend button for the ATWELL MARTIN LTD standing order
    Then I should be on the payment hub page
    When I update the standing order amount field with value as 5
    And I review the Standing Order
    Then I should be prompted to enter my password

  @stub-manage-payments-amend-so-no-step-up
  #PAT-843, PAT-885
  Scenario Outline: TC012 - Amend Standing Order: User should not see the password pop-up when the user has amended any field apart from amount for the Standing order
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    When I select amend button for the ATWELL MARTIN LTD standing order
    Then I should be on the payment hub page
    When I update the standing order <standingOrderField> field with value as <correspondingValue>
    And I review the Standing Order
    Then I should not be prompted to enter my password

    Examples:
      | standingOrderField | correspondingValue |
      | nextPaymentDate    | Monthly            |
      | paymentEndDate     | Monthly            |
      | frequency          | Weekly             |

  @stub-manage-payments-no-standing-order
  #PAT-839, PAT-874
  Scenario: TC013 - No Standing Orders: User should see Native Amend Standing order page with No Standing orders in the list
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see no standing orders in the list
    When I select the set up standing order button
    Then I should see the from field pre-populated with islamicCurrent account

  @stub-manage-payments-fetch-so-failed-outcome
  #PAT-840, PAT-888
  Scenario: TC014 - Fetch SO Failed Outcome - Action Menu: User should see error banner on Amend SO page when fetch SO failed outcome
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see error banner on native amend standing orders page
    And I should see the islamicCurrent standing orders account name

  @stub-manage-payments-fetch-so-failed-outcome
  #PAT-840, PAT-888
  Scenario Outline: TC015 - Fetch SO Failed Outcome - Standing Order success: User should see error banner on Amend SO page when fetch SO failed outcome
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see a Standing Order set up success message
    When I select view standing orders in the standing order setup successful page
    Then I should see the Standing order screen for native amend standing order
    And I should see error banner on native amend standing orders page
    And I should see the islamicCurrent standing orders account name

    Examples:
      | fromAccount    | Amount | toAccount                        | frequency        |
      | islamicCurrent | 2      | currentAccountWithNoTransactions | Every two months |

  @stub-manage-payments-fetch-so-forced-logout
  #PAT-840, PAT-888
  Scenario: TC016 - Fetch SO Forced Logout - Action Menu: User should be logged out on Amend SO page when fetch SO forced logout
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the logged off error screen

  @stub-manage-payments-fetch-so-forced-logout
  #PAT-840, PAT-888
  Scenario Outline: TC017 - Fetch SO Forced Logout - Standing Order success: User should be logged out on Amend SO page when fetch SO forced logout
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see a Standing Order set up success message
    When I select view standing orders in the standing order setup successful page
    Then I should see the logged off error screen

    Examples:
      | fromAccount    | Amount | toAccount                        | frequency        |
      | islamicCurrent | 2      | currentAccountWithNoTransactions | Every two months |

  @stub-manage-payments-fetch-so-details-failed-outcome
  #PAT-840, PAT-888
  Scenario: TC018 - Fetch SO Details - Failed Outcome: User should see error banner on Amend SO page when fetch SO details failed outcome
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    When I select amend button for the ATWELL MARTIN LTD standing order
    And I should see error banner on native amend standing orders page
    And I should see the Standing order screen for native amend standing order

  @stub-manage-payments-fetch-so-details-forced-logout
  #PAT-840, PAT-888
  Scenario: TC019 - Fetch SO Details - Forced Logout: User should be logged out on Amend SO page when fetch SO details forced logout
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    When I select amend button for the ATWELL MARTIN LTD standing order
    Then I should see the logged off error screen

  @stub-manage-payments-amend-so-verify-failed-outcome
  #PAT-862, PAT-890
  Scenario Outline: TC020 - Amend SO Verify - Failed Outcome: User should see error banner on Amend SO page when Amend SO Verify failed outcome
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    When I select amend button for the ATWELL MARTIN LTD standing order
    Then I should be on the payment hub page
    When I update the standing order <standingOrderField> field with value as <correspondingValue>
    And I select the continue option in the payment hub page
    Then I should see error banner on native amend standing orders page

    Examples:
      | standingOrderField | correspondingValue |
      | amount             | 5                  |
      | nextPaymentDate    | Monthly            |
      | paymentEndDate     | Monthly            |
      | frequency          | Weekly             |

  @stub-manage-payments-amend-so-verify-forced-logout
  #PAT-862, PAT-890
  Scenario Outline: TC021 - Amend SO Verify - Forced Logout: User should be logged out Amend SO page when Amend SO Verify forced logout
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    When I select amend button for the ATWELL MARTIN LTD standing order
    Then I should be on the payment hub page
    When I update the standing order <standingOrderField> field with value as <correspondingValue>
    And I select the continue option in the payment hub page
    Then I should see the logged off error screen

    Examples:
      | standingOrderField | correspondingValue |
      | amount             | 5                  |
      | nextPaymentDate    | Monthly            |
      | paymentEndDate     | Monthly            |
      | frequency          | Weekly             |

  @stub-manage-payments-amend-so-verify-try-again
  #PAT-862, PAT-890
  Scenario Outline: TC022 - Amend SO Verify - Try Again: User should see error banner on Amend SO page when Amend SO Verify try again
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    When I select amend button for the ATWELL MARTIN LTD standing order
    Then I should be on the payment hub page
    When I update the standing order <standingOrderField> field with value as <correspondingValue>
    And I select the continue option in the payment hub page
    Then I should see error banner on native amend standing orders page

    Examples:
      | standingOrderField | correspondingValue |
      | amount             | 5                  |
      | nextPaymentDate    | Monthly            |
      | paymentEndDate     | Monthly            |
      | frequency          | Weekly             |

  @stub-manage-payments-fulfill-amend-so-failed-outcome
  #PAT-864, PAT-889
  Scenario: TC023 - Fulfill Amend SO - Failed Outcome: User should see error banner on Review Amended SO page when fulfill amend SO failed outcome
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    When I select amend button for the ATWELL MARTIN LTD standing order
    Then I should be on the payment hub page
    When I update the standing order amount field with value as 5
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see standing order not set up error message

  @stub-manage-payments-fulfill-amend-so-forced-logout
  #PAT-864, PAT-889
  Scenario: TC024 - Fulfill Amend SO - Forced Logout: User should see be logged out on Review Amended SO page when fulfill amend SO forced logout
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    When I select amend button for the ATWELL MARTIN LTD standing order
    Then I should be on the payment hub page
    When I update the standing order amount field with value as 5
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see the logged off error screen

  @stub-manage-payments-fulfill-amend-so-try-again
  #PAT-864, PAT-889
  Scenario: TC025 - Fulfill Amend SO - Try Again: User should see an error message on amend SO page when fulfill amend SO try again
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    When I select amend button for the ATWELL MARTIN LTD standing order
    Then I should be on the payment hub page
    When I update the standing order amount field with value as 5
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see error banner on native amend standing orders page
    And I should be on the payment hub page

  @stub-manage-payments-fulfill-amend-so-wrong-password
  #PAT-864, PAT-889
  Scenario: TC026 - Fulfill Amend SO - Wrong Password: User should see wrong password error message on Review Amended SO page when fulfill amend SO wrong password
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    When I select amend button for the ATWELL MARTIN LTD standing order
    Then I should be on the payment hub page
    When I update the standing order amount field with value as 5
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should be prompted to enter my password

  @stub-manage-payments-delete-so-failed-outcome
  #PAT-971, PAT-1057
  Scenario: TC027 - Delete Standing Order - Failed Outcome: User should see error banner when deleting SO and receive failed outcome
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    When I select delete button for the ATWELL MARTIN LTD standing order
    And I select the Delete option in winback
    Then I should see error banner on native amend standing orders page

  @stub-manage-payments-delete-so-forced-logout
  #PAT-971, PAT-1057
  Scenario: TC028 - Delete Standing Order - Forced Logout: User should be logged out when deleting SO and receive forced logout
    Given I am an enrolled "current" user logged into the app
    When I select the "standing order" option in the action menu of islamicCurrent account
    Then I should see the Standing order screen for native amend standing order
    And I should see the islamicCurrent standing orders account name
    And I should see standing orders set up for ATWELL MARTIN LTD account with amend and with delete button
    When I select delete button for the ATWELL MARTIN LTD standing order
    And I select the Delete option in winback
    Then I should see the logged off error screen
