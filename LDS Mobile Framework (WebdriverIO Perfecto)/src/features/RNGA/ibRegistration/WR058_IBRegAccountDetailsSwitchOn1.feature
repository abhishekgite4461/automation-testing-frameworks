@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3 @stub-ibreg-mandateless
Feature: Native IB Registration Account Details - Switch ON 1
  In order to register for Internet Banking
  As a RNGA pre-authenticated customer
  I want to enter my details on account details page

  Background:
    Given I am a ibreg_mandateless user
    And I click on Register button
    And I enter Personal Details on the IB Registration Personal Details page
    And I click Next Button on the IB Registration Personal Details page

  #MOB3-3735, MOB3-3803, MQE-48
  Scenario Outline: TC001 – Customer account data entry is validated - Invalid Sort Code – Sort Code Data should not persist
    When I enter valid AccountNumber on the IB Registration Account Details page
    And I enter "<sortCode>" in "<fieldName>" Field on the IB Registration Account Details page
    And I tap out of the field on the IB Registration Account Details page
    And I verify the ErrorMessage "<HC-178>" on the IB Registration Account Detail page
    And I validate Next Button is not enabled on the IB Registration Account Details page
    And I navigate back to IB Registration Account Details page from Personal Details page
    Then I validate only sort code should be blank on the IB Registration Account Details page

    Examples:
      | sortCode | fieldName | HC-178                  |
      | 78985    | sortCode  |  valid sort code        |

  #MOB3-3735, MOB3-3803, MQE-48
  Scenario Outline: TC002 – Customer account data entry is validated - Invalid Account Number – Account Number Data should not persist
    When I enter valid SortCode on the IB Registration Account Details page
    And I enter "<accountNumber>" in "<fieldName>" Field on the IB Registration Account Details page
    And I verify the ErrorMessage "<HC-178>" on the IB Registration Account Detail page
    And I validate Next Button is not enabled on the IB Registration Account Details page
    And I navigate back to IB Registration Account Details page from Personal Details page
    Then I validate only account number should be blank on the IB Registration Account Details page

    Examples:
      | accountNumber | fieldName     | HC-178                                   |
      | 2127426       | accountNumber | Please enter a valid account number      |

  #MOB3-3735, MOB3-3803
  Scenario: TC003 – Customer Can enter Account details
    When I enter Account Details on the IB Registration Account Details page
    And I validate Next Button is enabled on the IB Registration Account Details page
    And I click Next Button on the IB Registration Account Details page
    Then I should be on the IB Registration Create Secure Logon Details page

  #MOB3-3735, MOB3-3803
  Scenario: TC004 – Persist User data once all mandatory character fields have been met
    When I enter Account Details on the IB Registration Account Details page
    And I navigate back to IB Registration Account Details page from Personal Details page
    Then I validate Account details on the IB Registration Account Details page

  @secondary
  #MOB3-3735, MOB3-3803
  Scenario Outline: TC005 – Data Should not persist when fields have errors
    When I enter "<sortCode>" in "<fieldName1>" Field on the IB Registration Account Details page
    And I enter "<accountNumber>" in "<fieldName2>" Field on the IB Registration Account Details page
    And I navigate back to IB Registration Account Details page from Personal Details page
    Then I validate Account details should be blank on the IB Registration Account Details page

    Examples:
      | fieldName1 | fieldName2    | sortCode | accountNumber |
      | sortCode   | accountNumber | 78985    | 2127426       |

  #MOB3-3735, MOB3-3803, MQE-48
  #TODO : Change error message parameter to message id once the message id is released
  Scenario Outline: TC006 – Customer leaves a mandatory field blank
    When I click Sort Code Field on the IB Registration Account Details page
    And I tap out of the field on the IB Registration Account Details page
    And I verify the ErrorMessage "<expectedSortCodeErrorMessage>" on the IB Registration Account Detail page
    And I enter valid SortCode on the IB Registration Account Details page
    And I click Account Number Field on the IB Registration Account Details page
    And I tap out of the field on the IB Registration Personal Details page
    Then I verify the ErrorMessage "<expectedAccountNoErrorMessage>" on the IB Registration Account Detail page

    Examples:
      | expectedSortCodeErrorMessage             | expectedAccountNoErrorMessage           |
      | valid sort code                          | Please enter a valid account number     |

  #MOB3-3735, MOB3-3803, MQE-48
  #TODO : Change error message parameter to message id once the message id is released
  Scenario Outline: TC007 – Customer deletes them before leaving the field
    When I enter valid SortCode on the IB Registration Account Details page
    And I clear the SortCode Field on the IB Registration Account Details page
    And I click Account Number Field on the IB Registration Account Details page
    And I verify the ErrorMessage "<expectedSortCodeErrorMessage>" on the IB Registration Account Detail page
    And I enter valid SortCode on the IB Registration Account Details page
    And I enter valid AccountNumber on the IB Registration Account Details page
    And I clear the Account Number Field on the IB Registration Account Details page
    And I tap out of the field on the IB Registration Account Details page
    Then I verify the ErrorMessage "<expectedAccountNoErrorMessage>" on the IB Registration Account Detail page

    Examples:
      | expectedSortCodeErrorMessage  | expectedAccountNoErrorMessage           |
      | valid sort code               | Please enter a valid account number     |

  #MOB3-3735, MOB3-3803, MQE-48
  #TODO : Change error message parameter to message id once the message id is released
  @secondary
  Scenario Outline: TC008 – Customer deletes character and taps out
    When I enter valid SortCode on the IB Registration Account Details page
    And I delete characters from SortCode Field on the IB Registration Account Details page
    And I verify the ErrorMessage "<expectedSortCodeErrorMessage>" on the IB Registration Account Detail page
    And I clear the SortCode Field on the IB Registration Account Details page
    And I enter valid SortCode on the IB Registration Account Details page
    And I enter valid AccountNumber on the IB Registration Account Details page
    And I delete characters from AccountNumber Field on the IB Registration Account Details page
    Then I verify the ErrorMessage "<expectedAccountNoErrorMessage>" on the IB Registration Account Detail page

    Examples:
      | expectedSortCodeErrorMessage             | expectedAccountNoErrorMessage           |
      | valid sort code                          | Please enter a valid account number     |

  #MOB3-3735, MOB3-3803, MQE-48
  Scenario Outline: TC009 – Mandatory characters validation for Credit card - Leaving blank
    When I select Account Type "<accountType>" on the IB Registration Account Details page
    And I click CreditCard Number Field on the IB Registration Account Details page
    And I tap out of the field on the IB Registration Account Details page
    Then I verify the ErrorMessage "<HC-178>" on the IB Registration Account Detail page

    Examples:
      | accountType | HC-178                                          |
      | Credit Card | Please enter a valid credit card number         |

  #MOB3-3735, MOB3-3803, MQE-48
  @secondary
  Scenario Outline: TC010 – Mandatory characters validation for Credit card - Less than minimum characters
    When I select Account Type "<accountType>" on the IB Registration Account Details page
    And I enter "<creditCardNumber>" in "<fieldName>" Field on the IB Registration Account Details page
    Then I verify the ErrorMessage "<HC-178>" on the IB Registration Account Detail page

    Examples:
      | accountType | fieldName        | creditCardNumber | HC-178                                          |
      | Credit Card | creditCardNumber | 1234567890       | Please enter a valid credit card number         |

  #MOB3-3735, MOB3-3803, MQE-48
  Scenario Outline: TC011 – Mandatory characters validation for Credit card - Delete character
    When I select Account Type "<accountType>" on the IB Registration Account Details page
    And I enter valid CreditCard on the IB Registration Account Details page
    And I validate CreditCard Number on the IB Registration Account Details page
    And I delete characters from CreditCard Field on the IB Registration Account Details page
    Then I verify the ErrorMessage "<HC-178>" on the IB Registration Account Detail page

    Examples:
      | accountType | HC-178                                          |
      | Credit Card | Please enter a valid credit card number         |

  #MOB3-3735, MOB3-3803
  Scenario Outline: TC012 – Mandatory characters validation for Credit card - Minimum Length
    When I select Account Type "<accountType>" on the IB Registration Account Details page
    And I enter "<creditCardNumber>" in "<fieldName>" Field on the IB Registration Account Details page
    Then I should not see ErrorMessage on the IB Registration Account Details page

    Examples:
      | accountType | fieldName        | creditCardNumber |
      | Credit Card | creditCardNumber | 378282246310005  |

  #MOB3-3735, MOB3-3803
  Scenario Outline: TC013 – Mandatory characters validation for Credit card - Maximum Length
    When I select Account Type "<accountType>" on the IB Registration Account Details page
    And I enter "<creditCardNumber>" in "<fieldName>" Field on the IB Registration Account Details page
    Then I validate maximum characters of CreditCard Field on the IB Registration Account Details page
    And I should not see ErrorMessage on the IB Registration Account Details page

    Examples:
      | accountType | fieldName        | creditCardNumber  |
      | Credit Card | creditCardNumber | 37828224631000511 |

  #MOB3-3735, MOB3-3803
  Scenario Outline: TC014 – Customer can see help text for credit card
    When I select Account Type "<accountType>" on the IB Registration Account Details page
    And I click CreditCard Number Field on the IB Registration Account Details page
    Then I should see CreditCard Tip with "<tipMessage>" value on the IB Registration Account Details page

    Examples:
      | accountType | tipMessage              |
      | Credit Card | This is the long number |

  @secondary
  #MOB3-3735, MOB3-3803
  Scenario Outline: TC015 – Customer is able to select different account types
    When I select Account Type "<accountType1>" on the IB Registration Account Details page
    And I enter valid LoanNumber on the IB Registration Account Details page
    And I select Account Type "<accountType2>" on the IB Registration Account Details page
    And I enter valid MortgageNumber on the IB Registration Account Details page
    And I select Account Type "<accountType3>" on the IB Registration Account Details page
    Then I enter valid CreditCard on the IB Registration Account Details page

    Examples:
      | accountType1 | accountType2 | accountType3 |
      | Loan         | Mortgage     | Credit Card  |
