@sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3 @team-mpt @stub-ibreg-mandateless
Feature: Native IB Registration Welcome Benefits - Switch ON
  In order to register for Internet Banking
  As a RNGA authenticated customer
  I want to navigate to New Welcome Benefits page

  @primary
  #MOB3-3427, MOB3-3428
  Scenario: TC001 – Access IB registration Welcome screen
    Given I am a ibreg_mandateless user
    And I should be on the Welcome Benefits page

  @android
  #MOB3-3427, MOB3-3428
  Scenario: TC002 - Navigate to fraud guarantee_android
    Given I am a ibreg_mandateless user
    And I should be on the Welcome Benefits page
    When I navigate to fraud guarantee
    Then I should see fraud guarantee page in the mobile browser
    And I select back key on the device
    And I should be on the Welcome Benefits page

  @android
  #MOB3-3427, MOB3-3428
  Scenario: TC003 - Navigate to apply for bank account_android
    Given I am a ibreg_mandateless user
    And I should be on the Welcome Benefits page
    When I navigate to new customer
    Then I should see new customer page in the mobile browser
    And I select back key on the device
    And I should be on the Welcome Benefits page

  @android
  #MOB3-3427, MOB3-3428
  Scenario: TC004 - Navigate to app demo_android
    Given I am a ibreg_mandateless user
    And I should be on the Welcome Benefits page
    When I navigate to app demo
    Then I should see app demo page in the mobile browser
    And I select back key on the device
    And I should be on the Welcome Benefits page

  @ios
  #MOB3-3427, MOB3-3428
  Scenario: TC005 - Navigate to fraud guarantee_iOS
    Given I am a ibreg_mandateless user
    And I should be on the Welcome Benefits page
    When I navigate to fraud guarantee
    Then I should see fraud guarantee page in the mobile browser

  @ios
  #MOB3-3427, MOB3-3428
  Scenario: TC006 - Navigate to apply for bank account_iOS
    Given I am a ibreg_mandateless user
    And I should be on the Welcome Benefits page
    When I navigate to new customer
    Then I should see new customer page in the mobile browser

  @ios
  #MOB3-3427, MOB3-3428
  Scenario: TC007 - Navigate to app demo_iOS
    Given I am a ibreg_mandateless user
    And I should be on the Welcome Benefits page
    When I navigate to app demo
    Then I should see app demo page in the mobile browser

  @ios
  #MOB3-3427, MOB3-3428
  Scenario: TC008 - Navigate to IB registration journey_iOS
    Given I am a ibreg_mandateless user
    And I should be on the Welcome Benefits page
    When I click on Register button
    Then I should be on the IB Registration Personal Details page
    And I click Back Button on the IB Registration Personal Details page
    And I should be on the Welcome Benefits page
    And I click on Login button
    And I should be on the Login page

  #MOB3-3427, MOB3-3428
  Scenario: TC009 - Navigate to log-in page
    Given I am a ibreg_mandateless user
    When I click on Login button
    Then I should be on the Login page

  @android @secondary
  #MOB3-3427, MOB3-3428
  Scenario: TC010 - Navigate to IB registration journey from Log-in Page, Back button_android
    Given I am a ibreg_mandateless user
    When I click on Login button
    Then I should be on the Login page
    And I click on Register for Internet Banking in login page
    And I should be on the IB Registration Personal Details page
    And I click Back Button on the IB Registration Personal Details page
    And I should be on the Login page
    And I select back key on the device
    And I should be on the Welcome Benefits page

  @ios @secondary
  #MOB3-3427, MOB3-3428
  Scenario: TC011 - Navigate to IB registration journey from Log-in Page, Back button_iOS
    Given I am a ibreg_mandateless user
    When I click on Login button
    Then I should be on the Login page
    And I click on Register for Internet Banking in login page
    And I should be on the IB Registration Personal Details page
    And I click Back Button on the IB Registration Personal Details page
    And I should be on the Login page
