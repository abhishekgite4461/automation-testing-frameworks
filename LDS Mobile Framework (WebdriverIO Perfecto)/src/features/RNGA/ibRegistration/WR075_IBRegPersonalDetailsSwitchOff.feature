@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @sw-enable_youth-off @rnga @nga3 @stub-youth
Feature: Native IB Registration Personal Details - Switch OFF
  In order to register for Internet Banking
  As a RNGA authenticated customer
  I want to enter my details on personal details page

  @ios
  #MOB3-3801, MOB3-3682
  Scenario Outline: TC001 - Customer between 11-15yrs when switch is off - Error Message : MG-2700
    Given I am a ibreg_youth_o2opass user
    When I click on Register button
    And I enter "<date>" "<month>" "<year>" in DOB on the IB Registration Personal Details page
    And I should see youth customer DOB error on the IB Registration Personal Details page
    And I click Back Button on DOB error message popup on the IB Registration Personal Details page
    Then I should not see youth customer DOB error on the IB Registration Personal Details page

    Examples:
      | date | month | year |
      | 18   | June  | 2005 |

  @ios
  #Different Calendars on different devices. Need to find out a way to handle this
  #MOB3-3801, MOB3-3682
  Scenario Outline: TC002 - Customer enters age less than 11 and sees error message HC-164
    Given I am a ibreg_youth_o2opass user
    When I click on Register button
    And I enter "<date>" "<month>" "<year>" in DOB on the IB Registration Personal Details page
    And I should see youth customer DOB error on the IB Registration Personal Details page
    And I click Back Button on DOB error message popup on the IB Registration Personal Details page
    Then I should not see youth customer DOB error on the IB Registration Personal Details page

    Examples:
      | date | month | year |
      | 18   | June  | 2010 |

  @manual @android
  #MOB3-3801, MOB3-3682
  Scenario Outline: TC003 - Customer between 11-15yrs when switch is off - Error Message : MG-2700
    Given I am a ibreg_youth_o2opass user
    When I click on Register button
    And I enter "<date>" "<month>" "<year>" in DOB on the IB Registration Personal Details page
    And I should see youth customer DOB error on the IB Registration Personal Details page
    And I click Back Button on DOB error message popup on the IB Registration Personal Details page
    Then I should not see youth customer DOB error on the IB Registration Personal Details page

    Examples:
      | date | month | year |
      | 18   | June  | 2005 |

  @manual @android
  #Different Calendars on different devices. Need to find out a way to handle this
  #MOB3-3801, MOB3-3682
  Scenario Outline: TC004 - Customer enters age less than 11 and sees error message HC-164
    Given I am a ibreg_youth_o2opass user
    When I click on Register button
    And I enter "<date>" "<month>" "<year>" in DOB on the IB Registration Personal Details page
    And I should see youth customer DOB error on the IB Registration Personal Details page
    And I click Back Button on DOB error message popup on the IB Registration Personal Details page
    Then I should not see youth customer DOB error on the IB Registration Personal Details page

    Examples:
      | date | month | year |
      | 18   | June  | 2010 |
