@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3 @stubOnly
Feature: Native IB Registration Activation Code - Enrolment ON 2
  In order to register for Internet Banking
  As a RNGA pre-authenticated customer
  I want to enter code on enter activation code page

  Background:
    Given I am a ibreg_ultralite user
    And I click on Login button
    And I login with my username and password

  @stub-ibreg-activation-code-not-valid @secondary
  #MOB3-4291, MOB3-5727
  Scenario Outline: TC001 -  Enter activation code - Code Not valid
    When I enter "abcde123" in Activation Code Field on IB Registration Activation Code page
    And I click on Confirm Code Button on IB Registration Activation Code page
    Then I should see ErrorMessage as "<HC-188>" on the IB Registration Activation Code page

    Examples:
      | HC-188                                            |
      | The activation code you've entered isn't correct. |

  @stub-ibreg-request-new-code-less-than-3-days
  #MOB3-4292, MOB3-5729
  Scenario: TC002 - Request new code – Previous code requested less than 3 days ago
    When I click Request New Activation Code button on the IB Registration Activation Code page
    Then I should be on the IB Registration HelpDesk page

  @stub-ibreg-activation-success @secondary
  #MOB3-4292, MOB3-5729
  Scenario: TC003 - Request new code – Previous code requested more than 3 days ago (020 check passed)
    When I click Request New Activation Code button on the IB Registration Activation Code page
    Then I should be on the IB Registration HelpDesk page

  @stub-ibreg-request-new-code-020-failed @secondary
  #MOB3-4292, MOB3-5729
  Scenario: TC004 - Request new code – Previous code requested more than 3 days ago (020 check failed)
    When I click Request New Activation Code button on the IB Registration Activation Code page
    Then I should be on the IB Registration HelpDesk page

  @stub-ibreg-request-new-code-limit-exceeded
  #MOB3-4292, MOB3-5729
  Scenario: TC005 - Request new code - already at the maximum number of requests online
    When I click Request New Activation Code button on the IB Registration Activation Code page
    Then I should be on the IB Registration HelpDesk page

  @manual
  #MOB3-4292, MOB3-5729
  Scenario: TC006 - Request new code - already at the maximum number of requests online - Status invalid
    When I click Request New Activation Code button on the IB Registration Activation Code page
    Then I should be on the IB Registration HelpDesk page
    And I validate the original activation code should be given an invalid status

  @stub-ibreg-activation-code-attempts-exceeded
  #MOB3-4292, MOB3-5729
  Scenario: TC007 - Activation code entered more than allowable attempts - cannot request more online
    When I enter "abcde123" in Activation Code Field on IB Registration Activation Code page
    And I click on Confirm Code Button on IB Registration Activation Code page
    Then I should be on the IB Registration HelpDesk page

  @manual
  #MOB3-4292, MOB3-5729
  Scenario Outline: TC008 - Customer Request New code (020 check passed) after 5 incorrect attempts
    When I click Request New Activation Code button on the IB Registration Activation Code page
    Then I should see ErrorMessage as "<HC-189>" on the IB Registration HelpDesk page

    Examples:
      | HC-189                                                                                    |
      | We've generated a new one for you and you'll receive it in the post in the next few days. |

  @manual
  #MOB3-4292, MOB3-5729
  Scenario: TC009 - Customer Request New code (020 check passed) after 5 incorrect attempts - Status Invalid
    When I click Request New Activation Code button on the IB Registration Activation Code page
    Then I should be on the IB Registration HelpDesk page
    And I validate the original activation code should be given an invalid status

  @manual
  #MOB3-4292, MOB3-5729
  Scenario: TC010 - Customer Request New code (020 check failed) after 5 incorrect attempts
    When I enter "abcde123" in Activation Code Field on IB Registration Activation Code page
    Then I should be on the IB Registration HelpDesk page

  @stub-ibreg-activation-code-expired-020-pass
  #MOB3-4292, MOB3-5729
  Scenario: TC011 - Customer Enters Code & Code Is Expired (020 Check Passed)
    When I enter "abcde123" in Activation Code Field on IB Registration Activation Code page
    And I click on Confirm Code Button on IB Registration Activation Code page
    Then I should be on the IB Registration HelpDesk page

  @stub-ibreg-activation-code-expired-020-failed
  #MOB3-4292, MOB3-5729
  Scenario: TC0012 - Customer Enters Code & Code Is Expired (020 Check Failed)
    When I enter "abcde123" in Activation Code Field on IB Registration Activation Code page
    And I click on Confirm Code Button on IB Registration Activation Code page
    Then I should be on the IB Registration HelpDesk page

  @stub-ibreg-activation-success
  #MOB3-4293, MOB3-5725
  Scenario: TC0013 - Customer authentication via Activation code will see success screen
    When I enter valid Activation Code on IB Registration Activation Code page
    And I click on Confirm Code Button on IB Registration Activation Code page
    And I enter correct memorable information in IB Registration confirm Activation code page
    Then I should be on the IB Registration Success page

  @stub-ibreg-activation-code-create-mi @secondary
   #MOB3-4293, MOB3-5725
   # There is an android defect which is raised in JIRA MPT-5128
  Scenario: TC014 - Entered activation code – Create MI
    When I enter "abcde123" in Activation Code Field on IB Registration Activation Code page
    And I click on Confirm Code Button on IB Registration Activation Code page
    And I enter Create Memorable Info Details on the IB Registration Create Memorable Info page
    And I select Next Button on IB Registration Create Memorable Info page
    And I select continue on the IB Registration Success page
    And I select confirm button in auto logout settings page
    Then I should be on the home page
