@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3 @stub-ibreg-activation-success @stubOnly
Feature: Native IB Registration Activation Code - Enrolment ON 1
  In order to register for Internet Banking
  As a RNGA pre-authenticated customer
  I want to enter code on enter activation code page

  Background:
    Given I am a ibreg_ultralite user
    And I click on Login button
    And I login with my username and password
    And I should be on the IB Registration Activation Code page

  #MOB3-4290, MOB3-5730
  Scenario Outline: TC001 - Customer authentication via Activation code will see success screen
    And I should see contact us option on the IB Registration Activation Code page
    When I enter valid Activation Code on IB Registration Activation Code page
    And I click on Confirm Code Button on IB Registration Activation Code page
    Then I should see contact us option on the IB Registration Activation Code enter mi page
    When I enter correct memorable information in IB Registration confirm Activation code page
    Then I should see contact us option on the IB Registration Success page
    And I should see the success Message as "<HC-201>" on the IB Registration Success page
    And I should see the footer Message as "<HC-203>" on the IB Registration Success page

    Examples:
      | HC-201                                                        | HC-203                              |
      | Only you can access your account from the app on this device. | password and memorable information. |

  #TODO remove ios tag once defect MOB3-14507 is fixed.
  @primary
  #MOB3-4290, MOB3-5730
  Scenario: TC002 - Customer sees Security log-off Setting page after Success screen (NGA Retail - Device Enrolment ON)
    And I should see contact us option on the IB Registration Activation Code page
    When I enter valid Activation Code on IB Registration Activation Code page
    And I click on Confirm Code Button on IB Registration Activation Code page
    And I enter correct memorable information in IB Registration confirm Activation code page
    And I select continue on the IB Registration Success page
    Then I should see contact us option on the auto logout settings page
    When I select confirm button in auto logout settings page
    Then I should be on the home page

  #MOB3-4291, MOB3-5727
  Scenario: TC003 - Customer Enter activation code - Next Button enabled
    When I enter "abcd123" in Activation Code Field on IB Registration Activation Code page
    And I should see Confirm Code Button is not enabled on the IB Registration Activation Code page
    And I enter "a1b2c3d4" in Activation Code Field on IB Registration Activation Code page
    Then I should see Confirm Code Button is enabled on the IB Registration Activation Code page

  @secondary
  #MOB3-4291, MOB3-5727
  Scenario: TC004 - Customer Enter activation code - Client capitalises the Characters
    When I enter valid Activation Code on IB Registration Activation Code page
    Then I validate Activation Code on IB Registration Activation Code page

  @ios
  #MOB3-4291, MOB3-5727
  Scenario Outline: TC005 - Enter activation code - invalid characters - iOS only
    When I enter "abcd" in Activation Code Field on IB Registration Activation Code page
    Then I should see ErrorMessage as "<HC-188>" on the IB Registration Activation Code page

    Examples:
      | HC-188                                            |
      | The activation code you've entered isn't correct. |
