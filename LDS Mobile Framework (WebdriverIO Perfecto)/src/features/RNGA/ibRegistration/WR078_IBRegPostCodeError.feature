@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3 @stub-ibreg-incorrect-postcode
Feature: Native IB Registration Postcode Error Page
  In order to register for Internet Banking
  As a RNGA authenticated customer
  I want to error page when post code is not correct

  Background:
    Given I am a ibreg_incorrect_postcode user
    And I click on Login button
    And I click on Register for Internet Banking in login page
    And I enter valid personal details with "e111bb" postcode on the IB Registration Personal Details page
    And I click Next Button on the IB Registration Personal Details page
    And I enter Account Details on the IB Registration Account Details page
    And I click Next Button on the IB Registration Account Details page

  @primary
  #MOB3-3741, MOB3-3808
  Scenario: TC001 – Postcode match check
    Then "e111bb" PostCode appears on the IB Registration Postcode Error page

  @secondary
  #MOB3-3741, MOB3-3808
  Scenario: TC002 – Next button inactive until Postcode changes
    And Next Button is not enabled on the IB Registration Postcode Error page
    And I enter valid PostCode on the IB Registration Postcode Error page
    Then Next Button is enabled on the IB Registration Postcode Error page

  @secondary
  #MOB3-3741, MOB3-3808
  Scenario: TC003 – Next button inactive until Post code left blank
    And Next Button is not enabled on the IB Registration Postcode Error page
    And I clear the Postcode field on the IB Registration Postcode Error page
    Then Next Button is enabled on the IB Registration Postcode Error page

  #MOB3-3741, MOB3-3808
  Scenario: TC004 – Entering wrong Postcode multiple times
    And I clear and enter "Temp 13" in the Postcode field on the IB Registration Postcode Error page
    And I click next button on the IB Registration Postcode Error page
    And I am on the IB Registration Postcode Error page
    And I clear the Postcode field on the IB Registration Postcode Error page
    And  I clear and enter "Test 14" in the Postcode field on the IB Registration Postcode Error page
    And I click next button on the IB Registration Postcode Error page
    Then I am on the IB Registration Postcode Error page

  @secondary
  #MOB3-3741, MOB3-3808
  Scenario: TC005 – Entering incorrect Postcode - Verify min(2) and max(8) length of Postcode Field
    And I clear and enter "T" in the Postcode field on the IB Registration Postcode Error page
    And Next Button is not enabled on the IB Registration Postcode Error page
    And I clear and enter "T1" in the Postcode field on the IB Registration Postcode Error page
    And Next Button is not enabled on the IB Registration Postcode Error page
    And I clear and enter "tes12 ter3" in the Postcode field on the IB Registration Postcode Error page
    And I validate maximum characters of PostCode Field on the IB Registration Postcode Error page

  #MOB3-3741, MOB3-3808
  Scenario: TC006 – Verify Uppercase and spaces for Postcode
    # Then I am displayed no valid postcode error message on the account tile
    And I clear and enter "e111bb" in the Postcode field on the IB Registration Postcode Error page
    Then I validate "e111bb" is in Upper Case on the IB Registration Postcode Error page

  #DPL-3919
  Scenario: TC007 – Verify postcode format error validation
    Given I am a ibreg_incorrect_postcode user
    And I click on Login button
    And I click on Register for Internet Banking in login page
    And I enter valid personal details with "Test12" postcode on the IB Registration Personal Details page
    Then I am displayed no valid postcode error message on the account tile
