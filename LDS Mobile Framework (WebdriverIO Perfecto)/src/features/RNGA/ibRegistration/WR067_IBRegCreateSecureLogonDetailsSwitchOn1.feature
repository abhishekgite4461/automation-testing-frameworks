@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3 @stub-ibreg-mandateless
Feature: Native IB Registration Create Secure Logon Details - Switch ON 1
  In order to register for Internet Banking
  As a RNGA authenticated customer
  I want to enter my details on create secure logon page

  Background:
    Given I am a ibreg_mandateless user
    And I enter Personal and Account Details for IB Registration

  #MOB3-4826, MOB3-3736
  Scenario: TC001 – Show UserId help text when entering User ID
    When I click UserId Field on the IB Registration Create Secure Logon Details page
    Then I should see UserId Tip on the IB Registration Create Secure Logon Details page

  #MOB3-4826, MOB3-3736
  Scenario: TC002 – Show Password help text when entering Password
    When I click Password Field on the IB Registration Create Secure Logon Details page
    Then I should see Password Tip on the IB Registration Create Secure Logon Details page

  #MOB3-4826, MOB3-3736
  Scenario: TC003 – Show Confirm Password help text when entering Confirm Password
    When I enter valid Password on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I click Confirm Password Field on the IB Registration Create Secure Logon Details page
    Then I should see Confirm Password Tip on the IB Registration Create Secure Logon Details page

  @secondary
  #MOB3-4826, MOB3-3736
  Scenario Outline: TC004 – Check button is visible when the criteria is met
    When I enter "<userId1>" in UserId Field on the IB Registration Create Secure Logon Details page
    And  I should not see a check button in User ID Field on the IB Registration Create Secure Logon Details page
    And I make the UserID field blank on the IB Registration Create Secure Logon Details page
    And I enter "<userId2>" in UserId Field on the IB Registration Create Secure Logon Details page
    Then I should see a check button in User ID Field on the IB Registration Create Secure Logon Details page

    Examples:
      | userId1 | userId2    |
      | user    | userid1234 |

  #MOB3-4826, MOB3-3736
  Scenario Outline: TC005 – Check will not take place until characters are added/removed
    When I enter "<userId>" in UserId Field on the IB Registration Create Secure Logon Details page
    And I click check button on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I click UserId Field on the IB Registration Create Secure Logon Details page
    And I should not see a check button in User ID Field on the IB Registration Create Secure Logon Details page
    And I delete characters from UserId Field on the IB Registration Create Secure Logon Details page
    And I should see a check button in User ID Field on the IB Registration Create Secure Logon Details page
    And I click check button on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I click UserId Field on the IB Registration Create Secure Logon Details page
    Then I should not see a check button in User ID Field on the IB Registration Create Secure Logon Details page

    Examples:
      | userId       |
      | userid123456 |

  #MOB3-4826, MOB3-3736
  Scenario: TC006 – Next button enabled when all the fields are completed
    When I enter valid UserId on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    And I tap out and should see next button is not enabled on the IB Registration Create Secure Logon Details page
    And I enter valid ConfirmPassword on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    Then I should see Next Button is enabled on the IB Registration Create Secure Logon Details page

  #MOB3-4826, MOB3-3736
  Scenario Outline: TC007 – UserId Initial Copy visible - User enters a successful UserId
    When I enter "<userId>" in UserId Field on the IB Registration Create Secure Logon Details page
    And I should see UserId Tip on the IB Registration Create Secure Logon Details page
    And I make the UserID field blank on the IB Registration Create Secure Logon Details page
    And I enter valid UserId on the IB Registration Create Secure Logon Details page
    Then I should see UserId Tip on the IB Registration Create Secure Logon Details page

    Examples:
      | userId  |
      | userid1 |

  @secondary
  #MOB3-4826, MOB3-3736
  Scenario Outline: TC008 – Max. 30 Characters
    When I enter "<userId30Chars>" in UserId Field on the IB Registration Create Secure Logon Details page
    Then I check the max length of UserId Field on the IB Registration Create Secure Logon Details page

    Examples:
      | userId30Chars                    |
      | abcdefghijklmnopqrstuvwxyz123456 |

  @secondary
  #MOB3-4826, MOB3-3736
  Scenario: TC009 – Cannot be the same as Password
    When I enter UserID in Password Field on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter valid UserId on the IB Registration Create Secure Logon Details page
    Then I should see UserId Tip on the IB Registration Create Secure Logon Details page
    And I should not see a check button in User ID Field on the IB Registration Create Secure Logon Details page

  #MOB3-4826, MOB3-3736
  Scenario: TC010 – Jump to password field, after User ID is confirmed as unique
    When I enter valid UserId on the IB Registration Create Secure Logon Details page
    And I click check button on the IB Registration Create Secure Logon Details page
    Then I should see Password Tip on the IB Registration Create Secure Logon Details page

  @android
  #MOB3-4826, MOB3-3736
  Scenario Outline: TC011 –  Error - entered details are invalid - User ID
    When I enter "<userId1>" in UserId Field on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I should see an error icon on the IB Registration Create Secure Logon Details page
    And I make the UserID field blank on the IB Registration Create Secure Logon Details page
    And I enter "<userId2>" in UserId Field on the IB Registration Create Secure Logon Details page
    Then I should see a check button in User ID Field on the IB Registration Create Secure Logon Details page
    And I should see UserId Tip on the IB Registration Create Secure Logon Details page

    Examples:
      | userId1 | userId2   |
      | userid1 | userid123 |

  @android
  #MOB3-4826, MOB3-3736
  Scenario Outline: TC012 – Min. 9 Characters
    When I enter "<userId1>" in UserId Field on the IB Registration Create Secure Logon Details page
    And I click UserId Field on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I should see an error icon on the IB Registration Create Secure Logon Details page
    And I make the UserID field blank on the IB Registration Create Secure Logon Details page
    And I enter "<userId2>" in UserId Field on the IB Registration Create Secure Logon Details page
    And I click UserId Field on the IB Registration Create Secure Logon Details page
    Then I should see a check button in User ID Field on the IB Registration Create Secure Logon Details page

    Examples:
      | userId1  | userId2   |
      | userid12 | userid123 |

  #For iOS, there is no accessibility ID for the field level error message, Hence to be validated manually
  @android
  #MOB3-4826, MOB3-3736
  Scenario Outline: TC013 –  Error - entered details are invalid - User ID
    When I enter "<userId1>" in UserId Field on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I should see an error icon on the IB Registration Create Secure Logon Details page
    And I make the UserID field blank on the IB Registration Create Secure Logon Details page
    And I enter "<userId2>" in UserId Field on the IB Registration Create Secure Logon Details page
    Then I should see a check button in User ID Field on the IB Registration Create Secure Logon Details page

    Examples:
      | userId1 | userId2   |
      | userid1 | userid123 |

  #For iOS, there is no accessibility ID for the field level error message, Hence to be validated manually
  @android
  #MOB3-4826, MOB3-3736
  Scenario Outline: TC014 – Min. 9 Characters
    When I enter "<userId1>" in UserId Field on the IB Registration Create Secure Logon Details page
    And I click UserId Field on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I should see an error icon on the IB Registration Create Secure Logon Details page
    And I make the UserID field blank on the IB Registration Create Secure Logon Details page
    And I enter "<userId2>" in UserId Field on the IB Registration Create Secure Logon Details page
    And I click UserId Field on the IB Registration Create Secure Logon Details page
    Then I should see a check button in User ID Field on the IB Registration Create Secure Logon Details page

    Examples:
      | userId1  | userId2   |
      | userid12 | userid123 |
