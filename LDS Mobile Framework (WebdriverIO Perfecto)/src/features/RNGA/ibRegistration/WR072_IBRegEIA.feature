@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3 @sw-ibreg_eia-on @stubOnly
Feature: Native IB Registration through EIA - Switch ON
  In order to register for Internet Banking
  As a RNGA pre-authenticated customer
  I want to register a user through EIA

  #TODO: remove ios tag once the defect MOB3-14507 is fixed.
  @stub-ibreg-eia-success @primary
  #MOB3-4164, MOB3-5435
  Scenario Outline: TC001 - Customer Authentication via EIA will see Registration success screen - Retail Customer message - HC-201 + HC-203
    Given I am a ibreg_eia_success user
    And I complete EIA registration for Internet Banking
    And I choose call me now option
    And I should see the success Message as "<HC-201>" on the IB Registration Success page
    And I should see the footer Message as "<HC-203>" on the IB Registration Success page
    And I select continue on the IB Registration Success page
    And I choose my security auto logout settings
    Then I should be on the home page

    Examples:
      | HC-201                                                        | HC-203                              |
      | Only you can access your account from the app on this device. | password and memorable information. |

  @stub-ibreg-eia-commercial-mandate @lds @secondary
  #MOB3-4164, MOB3-5435
  Scenario Outline: TC002 - Customer Authentication via EIA will see Registration success screen - Business Customer with valid password - HC-179
    Given I am a ibreg_eia_commercial_mandate user
    And I enter Personal and Account Details for IB Registration
    And I click Next Button on the IB Registration Create Secure Logon Details page
    And I enter Create Memorable Info Details on the IB Registration Create Memorable Info page
    And I select Next Button on IB Registration Create Memorable Info page
    And I click Agree Button on IB Registration TermsAndConditions page
    When I choose call me now option
    And I should see the success Message as "<HC-179>" on the IB Registration Success page
    And I select continue on the IB Registration Success page
    And I choose my security auto logout settings
    Then I should be on the home page

    Examples:
      | HC-179                                                        |
      | Only you can access your account from the app on this device. |

  @primary @stub-ibreg-eia-ineligible-o2opass
  #MOB3-4164, MOB3-5435
  Scenario: TC003 - EIA Non eligible - sim swap, does not answer and no retry, wrong code entered, Phone unreachable - 020 check Pass
    Given I am a ibreg_eia_ineligible_o2opass user
    When I complete EIA registration for Internet Banking
    And I choose call me now option
    Then I should be on the IB Registration LetterSent page
    And I should see contact us option on the IB Registration LetterSent page

  @primary @stub-ibreg-eia-ineligible-o2ofail
  #MOB3-4164, MOB3-5435
  Scenario: TC004 - EIA Non eligible - sim swap, blacklisted, does not answer and no retry, wrong code entered, Phone unreachable - 020 check Fail
    Given I am a ibreg_eia_ineligible_o2ofail user
    When I complete EIA registration for Internet Banking
    And I choose call me now option
    Then I should be on the IB Registration HelpDesk page

  @stub-ibreg-youth-o2ofail @primary
  #MOB3-4164, MOB3-5435
  Scenario: TC005 - Customer 11-15 - 020 o2o check Fails
    Given I am a ibreg_youth_o2ofail user
    When I complete EIA registration for Internet Banking
    Then I should be on the IB Registration HelpDesk page

  @stub-ibreg-youth-o2opass @primary
  #MOB3-4164, MOB3-5435
  Scenario: TC006 - Customer 11-15 - 020 check passes
    Given I am a ibreg_youth_o2opass user
    When I complete EIA registration for Internet Banking
    Then I should be on the IB Registration LetterSent page

  @stub-ibreg-eia-success
  #MOB3-4164, MOB3-5435
  Scenario: TC007 - Customer select to continue on Winback pop-up
    Given I am a ibreg_eia_success user
    And I complete EIA registration for Internet Banking
    When I select the cancel option on the EIA page
    And I accept the pop-up message on the EIA page
    Then I should be on the Login page

  @stub-ibreg-eia-success
  #MOB3-4164, MOB3-5435
  Scenario: TC008 - Customer select to Cancel on Winback pop-up
    Given I am a ibreg_eia_success user
    And I complete EIA registration for Internet Banking
    When I select the cancel option on the EIA page
    And I cancel the pop-up message on the EIA page
    Then I should be on the EIA page

  @stub-ibreg-eia-fraud @secondary
  #MOB3-4164, MOB3-5435
  Scenario Outline: TC009 - Customer registration journey is suspended when fraud is detected - MG-1997
    Given I am a ibreg_eia_fraud user
    When I complete EIA registration for Internet Banking
    And I choose call me now option
    Then I should see ErrorMessage as "<MG-1997>" on the IB Registration HelpDesk page

    Examples:
      | MG-1997                             |
      | Sorry but we can't register you for |

  @stub-ibreg-eia-success
  Scenario: TC010 - Validate the redesigned preauth header change for the Ib reg journey
    Given I am a ibreg_eia_success user
    When I click on Register button
    Then I should see contact us option on the IB Registration Personal Details page
    When I enter Personal Details on the IB Registration Personal Details page
    And I click Next Button on the IB Registration Personal Details page
    Then I should see contact us option on the IB Registration Account Details page
    When I enter Account Details on the IB Registration Account Details page
    And I click Next Button on the IB Registration Account Details page
    Then I should see contact us option on the IB Registration Create Secure Logon Details page
    When I enter Secure Logon Details on the IB Registration Create Secure Logon Details page
    And I click Next Button on the IB Registration Create Secure Logon Details page
    Then I should see contact us option on the IB Registration Create Memorable Info page
    When I enter Create Memorable Info Details on the IB Registration Create Memorable Info page
    And I select Next Button on IB Registration Create Memorable Info page
    Then I should see contact us option on the IB Registration TermsAndConditions page
    When I click Agree Button on IB Registration TermsAndConditions page
    Then I should see contact us option on the EIA page
    When I choose call me now option
    Then I should see contact us option on the IB Registration Success page
    When I select continue on the IB Registration Success page
    Then I should see contact us option on the auto logout settings page
    When I choose my security auto logout settings
    Then I should be on the home page
