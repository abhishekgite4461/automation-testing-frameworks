@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3 @stub-ibreg-mandateless
Feature: Native IB Registration Create MI - Switch ON
  In order to register for Internet Banking
  As a RNGA pre-authenticated customer
  I want to enter my details on create memorable information page

  Background:
    Given I am a ibreg_mandateless user
    And I click on Register button
    And I enter Personal Details on the IB Registration Personal Details page
    And I click Next Button on the IB Registration Personal Details page
    And I enter Account Details on the IB Registration Account Details page
    And I click Next Button on the IB Registration Account Details page
    And I enter Secure Logon Details on the IB Registration Create Secure Logon Details page
    And I click Next Button on the IB Registration Create Secure Logon Details page

  @secondary
  #MOB3-3960, MOB3-5054
  Scenario Outline: TC001 - MI validation - Error - Incorrect characters entered in MI field
    When I enter "<inputValue>" in Memorable Info Field on the IB Registration Create Memorable Info page
    And I should see Memorable Info Tip on the Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    Then I validate Confirm Memorable Info Field is not enabled on the IB Registration Create Memorable Info page

    Examples:
      | inputValue |
      | test1      |
      | 1234567    |
      | memory     |

  #MOB3-3960, MOB3-5054
  Scenario Outline: TC002 – MI validation - Maximum characters allowed in MI field
    When I enter "abcdefghij123456" in Memorable Info Field on the IB Registration Create Memorable Info page
    Then I validate maximum characters allowed for Memorable Info Field should be "<maxAllowed>" on the IB Registration Create Memorable Info page

    Examples:
      | maxAllowed |
      | 15         |

  @secondary
  #MOB3-3960, MOB3-5054
  Scenario: TC003 – MI validation - Error - Must not be same as UserID
    When I enter UserID in Memorable Info Field on the IB Registration Create Memorable Info page
    And I should see Memorable Info Tip on the Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    Then I validate Confirm Memorable Info Field is not enabled on the IB Registration Create Memorable Info page

  @secondary
  #MOB3-3960, MOB3-5054
  Scenario: TC004 – MI validation - Error - Must not be same as Password
    When I enter password in Memorable Info Field on the IB Registration Create Memorable Info page
    And I should see Memorable Info Tip on the Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    Then I validate Confirm Memorable Info Field is not enabled on the IB Registration Create Memorable Info page

  #MOB3-3960, MOB3-5054
  Scenario: TC005 - confirm MI is enabled after successful MI validation
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I should see Memorable Info Tip on the Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    Then I validate Confirm Memorable Info Field is enabled on the IB Registration Create Memorable Info page

  #MOB3-3960, MOB3-5054
  Scenario: TC006 – Customer can confirm the MI - Not Matching
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I enter "a" in Confirm Memorable Info Field on the Create Memorable Info page
    Then I should see Confirm Memorable Info Tip on the Create Memorable Info page

  #MOB3-3960, MOB3-5054
  Scenario: TC007 – Customer can confirm the MI - Matching
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I enter valid Confirm Memorable Info on the Create Memorable Info page
    Then I should see Confirm Memorable Info Tip on the Create Memorable Info page

  @ios
  #MOB3-3960, MOB3-5054
  Scenario: TC008 - Default masked and Masked when Customer goes back to the MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I validate the Memorable Info Field is masked on the Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I select Memorable Info on the Create Memorable Info page
    Then I validate the Memorable Info Field is masked on the Create Memorable Info page

  @ios
  #MOB3-3960, MOB3-5054
  Scenario: TC009 - Default masked and Masked when Customer goes back to the Confirm MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I enter valid Confirm Memorable Info on the Create Memorable Info page
    And I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I select Confirm Memorable Info on the Create Memorable Info page
    Then I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page

  #MOB3-3960, MOB3-5054
  Scenario: TC010 - Customer can unmask and mask MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I select HideShow Button on Memorable Info Field on the Create Memorable Info page
    And I validate the Memorable Info Field is not masked on the Create Memorable Info page
    And I select HideShow Button on Memorable Info Field on the Create Memorable Info page
    Then I validate the Memorable Info Field is masked on the Create Memorable Info page

  @ios
  #MOB3-3960, MOB3-5054
  Scenario: TC011 - Customer can unmask and mask Confirm MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I enter valid Confirm Memorable Info on the Create Memorable Info page
    And I select HideShow Button on Confirm Memorable Info Field on the Create Memorable Info page
    And I validate the Confirm Memorable Info Field is not masked on the Create Memorable Info page
    And I select HideShow Button on Confirm Memorable Info Field on the Create Memorable Info page
    Then I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page

  @ios
  #MOB3-3960, MOB3-5054
  Scenario: TC012 - Customer removes all the characters
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I clear the Memorable Info Field on the IB Registration Create Memorable Info page
    And I enter "a" in Memorable Info Field on the IB Registration Create Memorable Info page
    Then I validate the Memorable Info Field is masked on the Create Memorable Info page

  #MOB3-3960, MOB3-5054
  Scenario: TC013 - Default masked and Masked when Customer goes back to the MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I validate the Memorable Info Field is masked on the Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I select Memorable Info on the Create Memorable Info page
    Then I validate the Memorable Info Field is masked on the Create Memorable Info page

  #MOB3-3960, MOB3-5054
  Scenario: TC014 - Default masked and Masked when Customer goes back to the Confirm MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I enter valid Confirm Memorable Info on the Create Memorable Info page
    And I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I select Confirm Memorable Info on the Create Memorable Info page
    Then I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page

  #MOB3-3960, MOB3-5054
  Scenario: TC015 - Customer can unmask and mask MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I select HideShow Button on Memorable Info Field on the Create Memorable Info page
    And I validate the Memorable Info Field is not masked on the Create Memorable Info page
    And I select HideShow Button on Memorable Info Field on the Create Memorable Info page
    Then I validate the Memorable Info Field is masked on the Create Memorable Info page

  #MOB3-3960, MOB3-5054
  Scenario: TC016 - Customer can unmask and mask Confirm MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I enter valid Confirm Memorable Info on the Create Memorable Info page
    And I select HideShow Button on Confirm Memorable Info Field on the Create Memorable Info page
    And I validate the Confirm Memorable Info Field is not masked on the Create Memorable Info page
    And I select HideShow Button on Confirm Memorable Info Field on the Create Memorable Info page
    Then I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page

  #MOB3-3960, MOB3-5054
  Scenario: TC017 - Customer removes all the characters
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I clear the Memorable Info Field on the IB Registration Create Memorable Info page
    And I enter "a" in Memorable Info Field on the IB Registration Create Memorable Info page
    Then I validate the Memorable Info Field is masked on the Create Memorable Info page

  #MOB3-3960, MOB3-5054
  Scenario: TC018 - Find Out More Dialog
    When I select Memorable Info on the Create Memorable Info page
    And I select FindOutMore Link in Memorable Info hint text on the IB Registration Create Memorable Info page
    Then I validate Memorable Info tips dialog on the Create Memorable Info page
    And I should be able to close Memorable Info tips dialog on the Create Memorable Info page
