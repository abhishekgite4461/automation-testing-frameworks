@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3
Feature: Native IB Registration Account Details - Switch ON 3
  In order to register for Internet Banking
  As a RNGA pre-authenticated customer
  I want to enter my details on account details page

  #MOB3-3742, MOB3-3809
  @stub-ibreg-commercial-with-valid-password @lds @stubOnly @secondary
  Scenario Outline: TC001 - Commercial Mandate exists with a valid password
    Given I am a ibreg_commercial_with_valid_password user
    When I enter Personal and Account Details for IB Registration
    And I should see Account Validation Message as "<HC-172>" on the IB Registration Create Secure Logon Details page
    And I should see Commercial User Id pre-populated on the IB Registration Create Secure Logon Details page
    Then I should see Next Button is enabled on the IB Registration Create Secure Logon Details page

    Examples:
      | HC-172                                                                                           |
      | You're already set up for Business Internet Banking, so your User ID and password already exist. |

  #MOB3-3742, MOB3-3809
  @stub-ibreg-commercial-with-revoked-password @stubOnly @lds @secondary
  Scenario Outline: TC002 - Commercial Mandate Exists with password revoked
    Given I am a ibreg_commercial_with_revoked_password user
    When I enter Personal and Account Details for IB Registration
    And I should see Account Validation Message as "<HC-173>" on the IB Registration Create Secure Logon Details page
    And I should see Commercial User Id pre-populated on the IB Registration Create Secure Logon Details page
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    Then I should see Confirm Password Field is enabled on the IB Registration Create Secure Logon Details page

    Examples:
      | HC-173                                                                                                              |
      | You're already set up for Business Internet Banking, so your User ID already exists. Please provide a new password. |

  #MOB3-3742, MOB3-3809
  @stub-ibreg-commercial-bos @bos
  Scenario: TC003 - BOS Commercial mandate user
    Given I am a ibreg_commercial_bos user
    When I enter Personal and Account Details for IB Registration
    Then I enter Secure Logon Details on the IB Registration Create Secure Logon Details page

  #MOB3-3739, MOB3-3805
  @stub-ibreg-mandateless
  Scenario: TC004 – Customer can navigate to "Create User ID & Password Page" after successfully account validation
    Given I am a ibreg_mandateless user
    When I enter Personal and Account Details for IB Registration
    Then I should be on the IB Registration Create Secure Logon Details page

  #MOB3-3739, MOB3-3805
  @stub-ibreg-youth-failing-uniqueness-check @manual @secondary
  Scenario Outline: TC005 – Failed Uniqueness check message for 11-15yrs customer
    Given I am a ibreg_youth_failing_uniqueness_check user
    When I click on Register button
    And I enter Personal Details on the IB Registration Personal Details page
    And I click Next Button on the IB Registration Personal Details page
    And I should be on the IB Registration Account Details page
    And I enter Account Details on the IB Registration Account Details page
    And I click Next Button on the IB Registration Account Details page
    Then I should see CMS ErrorMessage as "<MG-2708>" on the IB Registration HelpDesk page

    Examples:
      | MG-2708                                                                                                                                                                                                                                                                                                                                                                                                                                           |
      | To finish registering for Internet Banking, please call us on 0345 300 0116 (or +44 1733 232 030 from a mobile or outside the UK). We need to ask you a few questions before you can start banking online. This is to help keep Internet Banking safe for you. Phone lines are open Monday to Friday – 7am to 10pm; Saturday and Sunday – 8am to 6pm. Please ask the bill payer's permission before you call. Calls may be recorded or monitored. |

  #MOB3-3739, MOB3-3805
  #TODO: remove ios tag once DPL-964 is fixed.
  @stub-ibreg-failing-uniqueness-check @secondary @ios
  Scenario Outline: TC006 – Failed Uniqueness check message for 16 and above customer
    Given I am a ibreg_failing_uniqueness_check user
    When I enter Personal and Account Details for IB Registration
    Then I verify the ErrorMessage "<HC-165>" on the IB Registration Account Detail page

    Examples:
      | HC-165                                                                                         |
      | Sorry, the account or card details you've entered are not correct. Please check and try again. |

  #MOB3-3739, MOB3-3805
  @manual
  Scenario: TC007 - Customer sees error message when there is a technical problem
    Given I am a ibreg_failing_uniqueness_check user
    When there is an un-identified technical problem
    Then I should be shown the default error message
    And I should NOT be able to continue

  #MOB3-3739, MOB3-3805
  @stub-ibreg-retail-mandate @primary
  Scenario Outline: TC008 - Customer with existing Retail mandate is taken to log-in page
    Given I am a ibreg_retail_mandate user
    When I enter Personal and Account Details for IB Registration
    Then I should be on the Login page
    And I should see Error Message on the Login page
    And I should not see Register for Internet Banking button on the Login page

    Examples:
      | HC-161                                                  |
      | If you can't remember those, please tap 'Forgotten your |

  #MOB3-3739, MOB3-3805
  @stub-ibreg-application-in-progress @secondary
  Scenario Outline: TC009 - Customer is informed if application is already in progress
    Given I am a ibreg_application_in_progress user
    When I enter Personal and Account Details for IB Registration
    Then I should see ErrorMessage as "<HC-170>" on the IB Registration HelpDesk page

    Examples:
      | HC-170                                                        |
      | and your details are being processed. Please try again later. |

  #MOB3-3739, MOB3-3805
  @stub-ibreg-suspended
  Scenario Outline: TC010 - Customer is informed if mandate is suspended
    Given I am a ibreg_suspended user
    When I enter Personal and Account Details for IB Registration
    Then I should see ErrorMessage as "<MG-297>" on the IB Registration HelpDesk page

    Examples:
      | MG-297                                 |
      | already exists and has been suspended. |

  #MOB3-3740, MOB3-3806
  @stub-ibreg-ineligible-product
  Scenario Outline: TC011 – Entire product ineligible for the brand
    Given I am a ibreg_ineligible_product user
    When I enter Personal and Account Details for IB Registration
    Then I should see ErrorMessage as "<MG-294>" on the IB Registration HelpDesk page

    Examples:
      | MG-294                              |
      | account that makes you eligible for |

  #MOB3-3740, MOB3-3806
  @stub-ibreg-mandateless
  Scenario: TC012 – Customer with both eligible and ineligible product can continue the journey
    Given I am a ibreg_mandateless user
    When I enter Personal and Account Details for IB Registration
    Then I should be on the IB Registration Create Secure Logon Details page
