@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @sw-enable_youth-on @rnga @nga3 @stub-ibreg-mandateless
Feature: Native IB Registration Personal Details - Switch ON 1
  In order to register for Internet Banking
  As a RNGA authenticated customer
  I want to enter my details on personal details page

  Background:
    Given I am a ibreg_mandateless user
    And I click on Register button
    And I should be on the IB Registration Personal Details page

  @primary
  #MOB3-3801, MOB3-3682
  Scenario: TC001 - Customer can enter personal details
    When I enter Personal Details on the IB Registration Personal Details page
    And I click Next Button on the IB Registration Personal Details page
    Then I should be on the IB Registration Account Details page

  #  TC002 - Personal Page Validation - First Name - Leave Blank
  #  TC004 - Personal Page Validation - First Name - Delete all and Verify
  #  TC005- Personal Page Validation - First Name - Validate Max Length
  #MOB3-3801, MOB3-3682, MQE-48
  Scenario Outline: TC003 - Personal Page Validation - First Name - First character Capitalization
    When I leave First Name Field blank on the IB Registration Personal Details page
    And I should see ErrorMessage as "<expectedErrorMessage>" on the IB Registration Personal Details page
    And I enter valid FirstName on the IB Registration Personal Details page
    Then I validate the first character is capitalized of First Name Field on the IB Registration Personal Details page
    When I clear the First Name Field on the IB Registration Personal Details page
    And I tap out of the field on the IB Registration Personal Details page
    Then I should see ErrorMessage as "<expectedErrorMessage>" on the IB Registration Personal Details page
    When I enter "abcdefghijabcdefghijabcdefghija" in First Name Field on the IB Registration Personal Details page
    Then I validate maximum characters of First Name Field on the IB Registration Personal Details page
    Examples:
      | expectedErrorMessage                   |
      | Please enter a valid first name        |

  #MOB3-3801, MOB3-3682, MQE-48
  Scenario Outline: TC006 - Personal Page Validation - Last Name - Leave Blank
    When I leave Last Name Field blank on the IB Registration Personal Details page
    Then I should see ErrorMessage as "<expectedErrorMessage>" on the IB Registration Personal Details page

    Examples:
      | expectedErrorMessage                   |
      | Please enter a valid last name         |

  #MOB3-3801, MOB3-3682
  Scenario: TC007 - Personal Page Validation - Last Name - First character Capitalization
    When I enter valid LastName on the IB Registration Personal Details page
    Then I validate the first character is capitalized of Last Name Field on the IB Registration Personal Details page

  #MOB3-3801, MOB3-3682, MQE-48
  Scenario Outline: TC008 - Personal Page Validation - Last Name - Delete all and Verify
    When I enter valid LastName on the IB Registration Personal Details page
    And I clear the Last Name Field on the IB Registration Personal Details page
    And I tap out of the field on the IB Registration Personal Details page
    Then I should see ErrorMessage as "<expectedErrorMessage>" on the IB Registration Personal Details page

    Examples:
      | expectedErrorMessage                 |
      | Please enter a valid last name       |

  #MOB3-3801, MOB3-3682
  Scenario: TC009 - Personal Page Validation - Last Name - Validate Max Length
    When I enter "abcdefghijabcdefghijabcdefghija" in Last Name Field on the IB Registration Personal Details page
    Then I validate maximum characters of Last Name Field on the IB Registration Personal Details page

  #MOB3-3801, MOB3-3682, MQE-48
  Scenario Outline: TC010 - Personal Page Validation - Email - Leave blank
    When I enter valid FirstName on the IB Registration Personal Details page
    And I enter valid LastName on the IB Registration Personal Details page
    And I leave EmailId Field blank on the IB Registration Personal Details page
    Then I should see ErrorMessage as "<expectedErrorMessage>" on the IB Registration Personal Details page

    Examples:
      | expectedErrorMessage                   |
      | Please enter a valid email address     |

  #MOB3-3801, MOB3-3682, MQE-48
  @secondary
  Scenario Outline: TC011 - Personal Page Validation - Email - Incorrect Email Id
    When I enter valid FirstName on the IB Registration Personal Details page
    And I enter valid LastName on the IB Registration Personal Details page
    And I enter "john@gmail..com" in Email Field on the IB Registration Personal Details page
    Then I should see ErrorMessage as "<expectedErrorMessage>" on the IB Registration Personal Details page

    Examples:
      | expectedErrorMessage                  |
      | Please enter a valid email address    |

  #MOB3-3801, MOB3-3682, MQE-48
  Scenario Outline: TC012 - Personal Page Validation - Email - incorrect Email Id
    When I enter valid FirstName on the IB Registration Personal Details page
    And I enter valid LastName on the IB Registration Personal Details page
    And I enter "john@gmailcom" in Email Field on the IB Registration Personal Details page
    Then I should see ErrorMessage as "<expectedErrorMessage>" on the IB Registration Personal Details page

    Examples:
      | expectedErrorMessage                 |
      | Please enter a valid email address   |

  #MOB3-3801, MOB3-3682, MQE-48
  Scenario Outline: TC013 - Personal Page Validation - Email - Delete All
    When I enter valid FirstName on the IB Registration Personal Details page
    And I enter valid LastName on the IB Registration Personal Details page
    And I enter valid EmailId on the IB Registration Personal Details page
    And I clear the EmailId Field on the IB Registration Personal Details page
    And I tap out of the field on the IB Registration Personal Details page
    Then I should see ErrorMessage as "<expectedErrorMessage>" on the IB Registration Personal Details page

    Examples:
      | expectedErrorMessage                 |
      | Please enter a valid email address   |
