@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @sw-enable_youth-on @rnga @nga3 @stub-ibreg-mandateless
Feature: Native IB Registration Personal Details - Switch ON 2
  In order to register for Internet Banking
  As a RNGA authenticated customer
  I want to enter my details on personal details page

  Background:
    Given I am a ibreg_mandateless user
    And I click on Register button
    And I should be on the IB Registration Personal Details page

  #MOB3-3801, MOB3-3682, MQE-48
  Scenario Outline: TC001 - Personal Page Validation - Email - Delete All
    When I enter valid FirstName on the IB Registration Personal Details page
    And I enter valid LastName on the IB Registration Personal Details page
    And I enter valid EmailId on the IB Registration Personal Details page
    And I clear the EmailId Field on the IB Registration Personal Details page
    And I tap out of the field on the IB Registration Personal Details page
    Then I should see ErrorMessage as "<expectedErrorMessage>" on the IB Registration Personal Details page

    Examples:
      | expectedErrorMessage                  |
      | Please enter a valid email address    |

  #MOB3-3801, MOB3-3682
  Scenario: TC002 - Personal Page Validation - Email - Valid Data
    When I enter valid FirstName on the IB Registration Personal Details page
    And I enter valid LastName on the IB Registration Personal Details page
    And I enter valid EmailId on the IB Registration Personal Details page
    Then I should not see ErrorMessage on the IB Registration Personal Details page

  @secondary
  #MOB3-3801, MOB3-3682
  Scenario Outline: TC003 - Personal Page Validation - Date of Birth - Default
    When I enter valid FirstName on the IB Registration Personal Details page
    And I enter valid LastName on the IB Registration Personal Details page
    And I enter valid EmailId on the IB Registration Personal Details page
    And I open Date Picker on the IB Registration Personal Details page
    And I click on ok button of Date Picker on the IB Registration Personal Details page
    Then I validate the default date with "<yearDelta>" on DOB Field on the IB Registration Personal Details page

    Examples:
      | yearDelta |
      | 25        |

  #MOB3-3801, MOB3-3682
  Scenario: TC004 - Personal Page Validation - Post Code - Leaving Blank
    When I enter valid FirstName on the IB Registration Personal Details page
    And I enter valid LastName on the IB Registration Personal Details page
    And I enter valid EmailId on the IB Registration Personal Details page
    And I enter valid DOB on the IB Registration Personal Details page
    And I click PostCode Field on the IB Registration Personal Details page
    And I tap out of the field on the IB Registration Personal Details page
    Then I should not see ErrorMessage on the IB Registration Personal Details page
    And I validate Next Button is enabled on the IB Registration Personal Details page

  #MOB3-3801, MOB3-3682, MQE-48
  #TODO: remove ios tag once DPL-964 is fixed.
  @secondary @ios
  Scenario Outline: TC005 - Personal Page Validation - Post Code - Single Character
    And I enter valid FirstName on the IB Registration Personal Details page
    And I enter valid LastName on the IB Registration Personal Details page
    And I enter valid EmailId on the IB Registration Personal Details page
    And I enter valid DOB on the IB Registration Personal Details page
    When I enter "e" in PostCode Field on the IB Registration Personal Details page
    Then I should see ErrorMessage as "<expectedErrorMessage>" on the IB Registration Personal Details page
    And I close the Error Message on the IB Registration Personal Details page
    And I validate Next Button is not enabled on the IB Registration Personal Details page

    Examples:
      | expectedErrorMessage                |
      | Please enter a valid postcode       |

  #MOB3-3801, MOB3-3682
  Scenario: TC006 - Personal Page Validation - Post Code - Double Characters
    When I enter valid FirstName on the IB Registration Personal Details page
    And I enter valid LastName on the IB Registration Personal Details page
    And I enter valid EmailId on the IB Registration Personal Details page
    And I enter valid DOB on the IB Registration Personal Details page
    And I enter "e1" in PostCode Field on the IB Registration Personal Details page
    Then I should not see ErrorMessage on the IB Registration Personal Details page

  #MOB3-3801, MOB3-3682
  Scenario: TC007 - Personal Page Validation - Post Code - Upper Case
    When I enter valid FirstName on the IB Registration Personal Details page
    And I enter valid LastName on the IB Registration Personal Details page
    And I enter valid EmailId on the IB Registration Personal Details page
    And I enter valid DOB on the IB Registration Personal Details page
    And I enter "e158hi" in PostCode Field on the IB Registration Personal Details page
    Then I validate "e158hi" is in Upper Case on the IB Registration Personal Details page

  #MOB3-3801, MOB3-3682
  Scenario: TC008 - Personal Page Validation - Post Code - Maximum  Length
    When I enter valid FirstName on the IB Registration Personal Details page
    And I enter valid LastName on the IB Registration Personal Details page
    And I enter valid EmailId on the IB Registration Personal Details page
    And I enter valid DOB on the IB Registration Personal Details page
    And I enter "ha9d0hl12" in PostCode Field on the IB Registration Personal Details page
    Then I validate maximum characters of PostCode Field on the IB Registration Personal Details page
    And I should not see ErrorMessage on the IB Registration Personal Details page
    And I validate Next Button is enabled on the IB Registration Personal Details page

  @android
  #MOB3-3801, MOB3-3682
  Scenario Outline: TC009 - Personal Page Validation - Date of Birth blank Android only
    When I enter valid FirstName on the IB Registration Personal Details page
    And I enter valid LastName on the IB Registration Personal Details page
    And I enter valid EmailId on the IB Registration Personal Details page
    And I open Date Picker on the IB Registration Personal Details page
    And I click on cancel button of Date Picker on the IB Registration Personal Details page
    And I tap out of the field on the IB Registration Personal Details page
    Then I should see ErrorMessage as "<expectedErrorMessage>" on the IB Registration Personal Details page
    And I close the Error Message on the IB Registration Personal Details page

    Examples:
      | expectedErrorMessage                   |
      | Please enter a valid date of birth     |

  @ios @secondary
  #MOB3-3801, MOB3-3682
  Scenario Outline: TC010 - Customer between 11-15yrs - Switch on
    Given I am a ibreg_youth_o2opass user
    When I click on Register button
    And I enter Personal Details on the IB Registration Personal Details page
    And I enter "<date>" "<month>" "<year>" in DOB on the IB Registration Personal Details page
    And I should not see youth customer DOB error on the IB Registration Personal Details page
    And I click Next Button on the IB Registration Personal Details page
    Then I should be on the IB Registration Account Details page

    Examples:
      | date | month | year |
      | 18   | June  | 2005 |

  @manual @android @secondary
  #Different Calendars on different devices. Need to find out a way to handle this
  #MOB3-3801, MOB3-3682
  Scenario Outline: TC011 - Customer between 11-15yrs - Switch on
    Given I am a ibreg_youth_o2opass user
    When I click on Register button
    And I enter Personal Details on the IB Registration Personal Details page
    And I enter "<date>" "<month>" "<year>" in DOB on the IB Registration Personal Details page
    And I should not see youth customer DOB error on the IB Registration Personal Details page
    And I click Next Button on the IB Registration Personal Details page
    Then I should be on the IB Registration Account Details page

    Examples:
      | date | month | year |
      | 18   | June  | 2005 |

  #MOB3-3801, MOB3-3682
  Scenario Outline: TC012 - Personal Page Validation - Email - Maximum  Length
    When I enter valid FirstName on the IB Registration Personal Details page
    And I enter valid LastName on the IB Registration Personal Details page
    And I enter "<emailWithMaxLength>" in Email Field on the IB Registration Personal Details page
    Then I validate maximum characters of Email Field on the IB Registration Personal Details page
    And I should not see ErrorMessage on the IB Registration Personal Details page

    Examples:
      | emailWithMaxLength                                                                |
      | abcdefghijklmnopqrstuvwxyz1234567890-qwertyuiopasdfghjklzxcvbnm@lloydsbanking.com |
