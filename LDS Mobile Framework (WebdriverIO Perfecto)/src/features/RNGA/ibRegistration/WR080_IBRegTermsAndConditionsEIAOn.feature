@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3 @sw-ibreg-eia-on @stub-ibreg-eia-success
Feature: Native IB Registration Terms & Conditions - EIA ON
  In order to register for Internet Banking
  As a RNGA authenticated customer
  I want to navigate to Terms and Conditions page

  Background:
    Given I am a ibreg_eia_success user
    And I click on Register button
    And I enter Personal Details on the IB Registration Personal Details page
    And I click Next Button on the IB Registration Personal Details page
    And I enter Account Details on the IB Registration Account Details page
    And I click Next Button on the IB Registration Account Details page
    And I enter Secure Logon Details on the IB Registration Create Secure Logon Details page
    And I click Next Button on the IB Registration Create Secure Logon Details page
    And I enter Create Memorable Info Details on the IB Registration Create Memorable Info page
    When I select Next Button on IB Registration Create Memorable Info page

  @primary
  #MOB3-3961, MOB3-5297
  Scenario: TC001 - Customer can view Terms & Condition page
    Then I should see footer section on the IB Registration TermsAndConditions page

  #MOB3-3961, MOB3-5297
  Scenario: TC002 - View Terms and Conditions and select Back
    And I should see footer section on the IB Registration TermsAndConditions page
    And I click Back Button on the IB Registration TermsAndConditions page
    Then I should be on the IB Registration Create Memorable Info page

  @manual @secondary
  #MOB3-3961, MOB3-5297
  Scenario: TC003 - View Terms and Conditions and Select Public Site Link
    And I select any public site link
    Then I should be told that I am navigating away from the app

  #MOB3-3961, MOB3-5297
  Scenario: TC004 - Agree Button should be always active, Accept Terms and Conditions and navigate to EIA
    And I click Agree Button on IB Registration TermsAndConditions page
    Then I should be on the EIA page

  @manual
  #MOB3-3961, MOB3-5297
  Scenario: TC005- Customer must be able to see the legal paragraphs on smaller devices
    Then I should be able to see at least the first 2 paragraphs of the T&C details on device screen
    And if the device screen is relatively small, customers should be able to scroll down to see more details.
