@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3 @stub-ibreg-userid-not-unique
Feature: Native IB Registration Create Secure Logon Details - Switch ON 2
  In order to register for Internet Banking
  As a RNGA authenticated customer
  I want to enter my details on create secure logon page

  Background:
    Given I am a ibreg_userid_not_unique user
    And I enter Personal and Account Details for IB Registration

  #MOB3-4826, MOB3-3736
  Scenario Outline: TC001 – User ID is not unique - I can select the one system generated.
    When I enter "<userId>" in UserId Field on the IB Registration Create Secure Logon Details page
    And I click check button on the IB Registration Create Secure Logon Details page
    And I should see UserID options are visible on IB Registration Create Secure Logon Details page
    And I select User Id from UserID options on the IB Registration Create Secure Logon Details page
    And I enter valid password and confirm password details on the IB Registration Create Secure Logon Details page
    Then I should see Next Button is enabled on the IB Registration Create Secure Logon Details page

    Examples:
      | userId    |
      | qwerty123 |

  #MOB3-4826, MOB3-3736
  Scenario Outline: TC002 – Customer decides not to select alternative User ID suggested by system
    When I enter "<userId>" in UserId Field on the IB Registration Create Secure Logon Details page
    And I click check button on the IB Registration Create Secure Logon Details page
    And I should see UserID options are visible on IB Registration Create Secure Logon Details page
    And I click Back Button in UserID options on the IB Registration Create Secure Logon Details page
    And I should see UserId Field with "<userId>" value on the IB Registration Create Secure Logon Details page
    And I click UserId Field on the IB Registration Create Secure Logon Details page
    And I should see UserId Tip on the IB Registration Create Secure Logon Details page
    And I should not see a check button in User ID Field on the IB Registration Create Secure Logon Details page
    And I delete characters from UserId Field on the IB Registration Create Secure Logon Details page
    Then I should see a check button in User ID Field on the IB Registration Create Secure Logon Details page

    Examples:
      | userId      |
      | qwerty12345 |

  @secondary
  #MOB3-4826, MOB3-3736
  Scenario Outline: TC003 – Jump to password field, after selecting a system generated username
    When I enter "<userId>" in UserId Field on the IB Registration Create Secure Logon Details page
    And I click check button on the IB Registration Create Secure Logon Details page
    And I should see UserID options are visible on IB Registration Create Secure Logon Details page
    And I select User Id from UserID options on the IB Registration Create Secure Logon Details page
    And I should see Password Tip on the IB Registration Create Secure Logon Details page
    And I enter valid password and confirm password details on the IB Registration Create Secure Logon Details page
    Then I should see Next Button is enabled on the IB Registration Create Secure Logon Details page

    Examples:
      | userId    |
      | qwerty123 |

  #MOB3-4826, MOB3-3736
  Scenario Outline: TC004 – Stay in User ID field if User ID is not unique and customer opts to create their own.
    When I enter "<userId>" in UserId Field on the IB Registration Create Secure Logon Details page
    And I click check button on the IB Registration Create Secure Logon Details page
    And I should see UserID options are visible on IB Registration Create Secure Logon Details page
    And I click Back Button in UserID options on the IB Registration Create Secure Logon Details page
    Then I should see UserId Tip on the IB Registration Create Secure Logon Details page

    Examples:
      | userId    |
      | qwerty123 |

  #MOB3-4826, MOB3-3736
  Scenario Outline: TC005 – Loading indicator is visible on clicking check button
    When I enter "<userId>" in UserId Field on the IB Registration Create Secure Logon Details page
    And I click check button on the IB Registration Create Secure Logon Details page
    Then I should see a loading indicator on the IB Registration Create Secure Logon Details page

    Examples:
      | userId     |
      | userid1234 |
