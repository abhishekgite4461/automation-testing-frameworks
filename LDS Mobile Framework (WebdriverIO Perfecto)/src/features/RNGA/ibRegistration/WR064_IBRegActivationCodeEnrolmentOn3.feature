@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3 @stubOnly
Feature: Native IB Registration Activation Code - Enrolment ON 3
  In order to register for Internet Banking
  As a RNGA pre-authenticated customer
  I want to see the activation code letter sent screen

  @stub-ibreg-letter-sent-commercial-existing-password @lds @secondary
  #MOB3-4292, MOB3-5729
  Scenario Outline: TC001 - Commercial mandate exists - letter sent screen
    Given I am a ibreg_commercial_with_valid_password user
    When I enter Personal and Account Details for IB Registration
    And I click Next Button on the IB Registration Create Secure Logon Details page
    And I enter Create Memorable Info Details on the IB Registration Create Memorable Info page
    And I select Next Button on IB Registration Create Memorable Info page
    And I click Agree Button on IB Registration TermsAndConditions page
    Then I should be on the IB Registration LetterSent page
    And I verify the ErrorMessage "<HC-176> on the IB Registration Letter Sent page

    Examples:
      | HC-176                                                                   |
      | existing Business User ID and password, followed by your activation code |

  @stub-ibreg-letter-sent-commercial-new-password @lds @secondary
  #MOB3-4292, MOB3-5729
  Scenario Outline: TC002 - Commercial mandate exists - letter sent screen (new password set)
    Given I am a ibreg_commercial_with_revoked_password user
    When I enter Personal and Account Details for IB Registration
    And I enter valid password and confirm password details on the IB Registration Create Secure Logon Details page
    And I click Next Button on the IB Registration Create Secure Logon Details page
    And I enter Create Memorable Info Details on the IB Registration Create Memorable Info page
    And I select Next Button on IB Registration Create Memorable Info page
    And I click Agree Button on IB Registration TermsAndConditions page
    Then I should be on the IB Registration LetterSent page
    And I verify the ErrorMessage "<HC-177> on the IB Registration Letter Sent page

    Examples:
      | HC-177                                                                                        |
      | existing Business User ID and the password you just created, followed by your activation code |

  @stub-ibreg-commercial-bos @bos
  #MOB3-4292, MOB3-5729
  Scenario Outline: TC003 - Commercial mandate exists - letter sent screen (BOS)
    Given I am a ibreg_commercial_bos user
    When I enter Personal and Account Details for IB Registration
    And I enter Secure Logon Details on the IB Registration Create Secure Logon Details page
    And I click Next Button on the IB Registration Create Secure Logon Details page
    And I enter Create Memorable Info Details on the IB Registration Create Memorable Info page
    And I select Next Button on IB Registration Create Memorable Info page
    And I click Agree Button on IB Registration TermsAndConditions page
    Then I should be on the IB Registration LetterSent page
    And I verify the ErrorMessage "<HC-183> on the IB Registration Letter Sent page

    Examples:
      | HC-183                                     |
      | and password you just created, followed by |
