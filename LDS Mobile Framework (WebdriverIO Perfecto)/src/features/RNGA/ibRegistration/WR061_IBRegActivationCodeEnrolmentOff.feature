@team-mpt @sw-enrolment-off @sw-ibreg_mvp-on @rnga @nga3 @stub-ibreg-activation-success @stubOnly @primary @pending
Feature: Native IB Registration Activation Code - Enrolment OFF
  In order to register for Internet Banking
  As a RNGA pre-authenticated customer
  I want to enter code on enter activation code page

  #TODO pending should be removed once the defect MOB3-13577 fixed
  #MOB3-4290, MOB3-5730
  Scenario: TC001 - Customer authentication via Activation code will see success screen (NGA Retail - Device Enrolment OFF)
    Given I am a ibreg_ultralite user
    When I login with my username and password
    And I enter valid Activation Code on IB Registration Activation Code page
    And I click on Confirm Code Button on IB Registration Activation Code page
    And I enter correct MI
    Then I should be on the home page
