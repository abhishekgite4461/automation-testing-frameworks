@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3 @sw-ibreg-eia-off @primary @stubOnly
Feature: Native IB Registration Terms & Conditions - EIA OFF
  In order to register for Internet Banking
  As a RNGA authenticated customer
  I want to navigate to Terms and Conditions page

  @stub-ibreg-adult-o2opass
  #MOB3-3961, MOB3-5297
  Scenario: TC001 - Accept Terms and Conditions and navigate to Letter Sent page
    Given I am a ibreg_adult_o2opass user
    When I complete EIA registration for Internet Banking
    Then I should be on the IB Registration LetterSent page

  @stub-ibreg-adult-o2ofail
  #MOB3-3961, MOB3-5297
  Scenario: TC002 - Accept Terms and Conditions and navigate to HelpDesk page
    Given I am a ibreg_adult_o2ofail user
    When I complete EIA registration for Internet Banking
    Then I should be on the IB Registration HelpDesk page
