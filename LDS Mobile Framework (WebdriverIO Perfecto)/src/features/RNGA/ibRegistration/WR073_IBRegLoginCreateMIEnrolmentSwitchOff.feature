@team-mpt @sw-enrolment-off @sw-ibreg_mvp-on @rnga @nga3 @stub-ibreg-create-mi-within-grace-period
Feature: Native IB Registration Create MI after login - Switch ON
  In order to register for Internet Banking
  As a RNGA pre-authenticated customer
  I want to enter my details on create memorable information page after login

  Background:
    Given I am a ibreg_ultralite user
    When I login with my username and password
    And I should be on the IB Registration Create Memorable Info page

  #MOB3-2502, MOB3-1151
  Scenario Outline: TC001 - MI validation - Error - Incorrect characters entered in MI field
    When I enter "<inputValue>" in Memorable Info Field on the IB Registration Create Memorable Info page
    And I should see Memorable Info Tip on the Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    Then I validate Confirm Memorable Info Field is not enabled on the IB Registration Create Memorable Info page

    Examples:
      | inputValue |
      | test1      |
      | 123456     |
      | memory     |
      | me1234     |

  #MOB3-2502, MOB3-1151
  Scenario Outline: TC002 – MI validation - Maximum characters allowed in MI field
    When I enter "<longMI>" in Memorable Info Field on the IB Registration Create Memorable Info page
    Then I validate maximum characters allowed for Memorable Info Field should be "<maxAllowed>" on the IB Registration Create Memorable Info page

    Examples:
      | longMI           | maxAllowed |
      | abcdefghij123456 | 15         |

  #MOB3-2502, MOB3-1151
  Scenario: TC003 – MI validation - Error - Must not be same as UserID
    When I enter UserID in Memorable Info Field on the IB Registration Create Memorable Info page
    And I should see Memorable Info Tip on the Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    Then I validate Confirm Memorable Info Field is not enabled on the IB Registration Create Memorable Info page

  #MOB3-2502, MOB3-1151
  Scenario Outline: TC004 – MI validation - Error - Must not be same as Password
    When I enter "<password>" in Memorable Info Field on the IB Registration Create Memorable Info page
    And I should see Memorable Info Tip on the Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    Then I validate Confirm Memorable Info Field is not enabled on the IB Registration Create Memorable Info page

    Examples:
      | password     |
      | password1234 |

  #MOB3-2502, MOB3-1151
  Scenario: TC005 - confirm MI is enabled after successful MI validation
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I should see Memorable Info Tip on the Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    Then I validate Confirm Memorable Info Field is enabled on the IB Registration Create Memorable Info page

  @manual
  #MOB3-2502, MOB3-1151
  Scenario: TC006 - Customer can paste into MI fields, but not copy from them
    When I click Memorable Info Field on the IB Registration Create Memorable Info page
    And I paste text in Memorable Info Field the IB Registration Create Memorable Info page
    And I make the Memorable Info Field blank on the IB Registration Create Memorable Info page
    And I enter valid Memorable Info on the IB Registration Create Memorable Info page
    Then I should not be able to copy the text from the Memorable Info Field on the IB Registration Create Memorable Info page

  #MOB3-2502, MOB3-1151
  Scenario Outline: TC007 – Customer can confirm the MI - Not Matching
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I enter "<inputValue>" in Confirm Memorable Info Field on the Create Memorable Info page
    Then I should see Confirm Memorable Info Tip on the Create Memorable Info page
    Examples:
      | inputValue |
      | a          |

  #MOB3-2502, MOB3-1151
  Scenario: TC008 – Customer can confirm the MI - Matching
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I enter valid Confirm Memorable Info on the Create Memorable Info page
    Then I should see Confirm Memorable Info Tip on the Create Memorable Info page

  #MOB3-2502, MOB3-1151
  Scenario: TC009 - Default masked, masked when Customer tabs out of field and Masked when Customer goes back to the MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I validate the Memorable Info Field is masked on the Create Memorable Info page
    And I select HideShow Button on Memorable Info Field on the Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I validate the Memorable Info Field is masked on the Create Memorable Info page
    And I select Memorable Info on the Create Memorable Info page
    Then I validate the Memorable Info Field is masked on the Create Memorable Info page

  #MOB3-2502, MOB3-1151
  Scenario: TC010 - Default masked, masked when Customer tabs out of field and Masked when Customer goes back to the Confirm MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I enter valid Confirm Memorable Info on the Create Memorable Info page
    And I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page
    And I select HideShow Button on Confirm Memorable Info Field on the Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page
    And I select Confirm Memorable Info on the Create Memorable Info page
    Then I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page

  #MOB3-2502, MOB3-1151
  Scenario: TC011 - Customer can unmask and mask MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I select HideShow Button on Memorable Info Field on the Create Memorable Info page
    And I validate the Memorable Info Field is not masked on the Create Memorable Info page
    And I select HideShow Button on Memorable Info Field on the Create Memorable Info page
    Then I validate the Memorable Info Field is masked on the Create Memorable Info page

  #MOB3-2502, MOB3-1151
  Scenario: TC012 - Customer can unmask and mask Confirm MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I enter valid Confirm Memorable Info on the Create Memorable Info page
    And I select HideShow Button on Confirm Memorable Info Field on the Create Memorable Info page
    And I validate the Confirm Memorable Info Field is not masked on the Create Memorable Info page
    And I select HideShow Button on Confirm Memorable Info Field on the Create Memorable Info page
    Then I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page

  #MOB3-2502, MOB3-1151
  Scenario Outline: TC013 - Customer removes all the characters from MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I clear the Memorable Info Field on the IB Registration Create Memorable Info page
    And I enter "<inputValue>" in Memorable Info Field on the IB Registration Create Memorable Info page
    And I validate the Memorable Info Field is masked on the Create Memorable Info page
    And I clear the Memorable Info Field on the IB Registration Create Memorable Info page
    And I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I select HideShow Button on Memorable Info Field on the Create Memorable Info page
    And I clear the Memorable Info Field on the IB Registration Create Memorable Info page
    And I enter "<inputValue>" in Memorable Info Field on the IB Registration Create Memorable Info page
    And I validate the Memorable Info Field is not masked on the Create Memorable Info page

    Examples:
      | inputValue |
      | a          |

  #MOB3-2502, MOB3-1151
  Scenario Outline: TC014 - Customer removes all the characters from Confirm MI field
    When I enter valid Memorable Info on the Create Memorable Info page
    And I enter valid Confirm Memorable Info on the Create Memorable Info page
    And I clear the Confirm Memorable Info Field on the IB Registration Create Memorable Info page
    And I enter "<inputValue>" in Confirm Memorable Info Field on the Create Memorable Info page
    And I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page
    And I clear the Confirm Memorable Info Field on the IB Registration Create Memorable Info page
    And I enter valid Confirm Memorable Info on the Create Memorable Info page
    And I select HideShow Button on Confirm Memorable Info Field on the Create Memorable Info page
    And I clear the Confirm Memorable Info Field on the IB Registration Create Memorable Info page
    And I enter "<inputValue>" in Confirm Memorable Info Field on the Create Memorable Info page
    And I validate the Confirm Memorable Info Field is not masked on the Create Memorable Info page

    Examples:
      | inputValue |
      | ab         |

  #MOB3-2502, MOB3-1151
  Scenario: TC015 - Default masked, masked when Customer tabs out of field and Masked when Customer goes back to the MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I validate the Memorable Info Field is masked on the Create Memorable Info page
    And I select HideShow Button on Memorable Info Field on the Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I validate the Memorable Info Field is masked on the Create Memorable Info page
    And I select Memorable Info on the Create Memorable Info page
    Then I validate the Memorable Info Field is masked on the Create Memorable Info page

  #MOB3-2502, MOB3-1151
  Scenario: TC016 - Default masked, masked when Customer tabs out of field and Masked when Customer goes back to the Confirm MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I enter valid Confirm Memorable Info on the Create Memorable Info page
    And I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page
    And I select HideShow Button on Confirm Memorable Info Field on the Create Memorable Info page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page
    And I select Memorable Info on the Create Memorable Info page
    Then I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page

  #MOB3-2502, MOB3-1151
  Scenario: TC017 - Customer can unmask and mask MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I select HideShow Button on Memorable Info Field on the Create Memorable Info page
    And I validate the Memorable Info Field is not masked on the Create Memorable Info page
    And I select HideShow Button on Memorable Info Field on the Create Memorable Info page
    Then I validate the Memorable Info Field is masked on the Create Memorable Info page

  #MOB3-2502, MOB3-1151
  Scenario: TC018 - Customer can unmask and mask Confirm MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I enter valid Confirm Memorable Info on the Create Memorable Info page
    And I select HideShow Button on Confirm Memorable Info Field on the Create Memorable Info page
    And I validate the Confirm Memorable Info Field is not masked on the Create Memorable Info page
    And I select HideShow Button on Confirm Memorable Info Field on the Create Memorable Info page
    Then I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page

  #MOB3-2502, MOB3-1151
  Scenario Outline: TC019 - Customer removes all the characters from MI field
    When I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I clear the Memorable Info Field on the IB Registration Create Memorable Info page
    And I enter "<inputValue>" in Memorable Info Field on the IB Registration Create Memorable Info page
    And I validate the Memorable Info Field is masked on the Create Memorable Info page
    And I clear the Memorable Info Field on the IB Registration Create Memorable Info page
    And I enter valid Memorable Info on the IB Registration Create Memorable Info page
    And I select HideShow Button on Memorable Info Field on the Create Memorable Info page
    And I clear the Memorable Info Field on the IB Registration Create Memorable Info page
    And I enter "<inputValue>" in Memorable Info Field on the IB Registration Create Memorable Info page
    And I validate the Memorable Info Field is not masked on the Create Memorable Info page

    Examples:
      | inputValue |
      | a          |

  @android @manual
  #MOB3-2502, MOB3-1151
  Scenario Outline: TC020 - Customer removes all the characters from Confirm MI field
    When I enter valid Confirm Memorable Info on the Create Memorable Info page
    And I clear the Confirm Memorable Info Field on the IB Registration Create Memorable Info page
    And I enter "<inputValue>" in Confirm Memorable Info Field on the Create Memorable Info page
    And I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page
    And I clear the Confirm Memorable Info Field on the IB Registration Create Memorable Info page
    And I enter valid Confirm Memorable Info on the Create Memorable Info page
    And I select HideShow Button on Confirm Memorable Info Field on the Create Memorable Info page
    And I clear the Confirm Memorable Info Field on the IB Registration Create Memorable Info page
    And I enter "<inputValue>" in Confirm Memorable Info Field on the Create Memorable Info page
    And I validate the Confirm Memorable Info Field is not masked on the Create Memorable Info page

    Examples:
      | inputValue |
      | a          |

  #MOB3-2502, MOB3-1151
  Scenario: TC021 - Find Out More Dialog
    When I select Memorable Info on the Create Memorable Info page
    And I select FindOutMore Link in Memorable Info hint text on the IB Registration Create Memorable Info page
    Then I validate Memorable Info tips dialog on the Create Memorable Info page
    And I should be able to close Memorable Info tips dialog on the Create Memorable Info page

  @secondary
  #MOB3-2502, MOB3-1151
  Scenario: TC022 - MI Created with device enrolment off
    When I enter Create Memorable Info Details on the IB Registration Create Memorable Info page
    And I select Next Button on IB Registration Create Memorable Info page
    Then I should be on the home page
