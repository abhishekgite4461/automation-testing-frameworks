@team-mpt @sw-ibreg-mvp-off @sw-enrolment-off @nga3 @rnga @stub-ibreg-mandateless @pending
Feature: Native IB Registration Welcome Benefits - MVP OFF - Enrolment OFF
  In order to register for Internet Banking
  As a RNGA authenticated customer
  I want to navigate to New Welcome Benefits page

  #In App Registration journey is not yet implemented. Pending on MComm team
  #MOB3-3427, MOB3-3428
  Scenario: TC001 - Navigate to log-in page (RNGA Switch OFF)
    Given I am a ibreg_mandateless user
    When I should be on the Login page
    And I click on Register for Internet Banking in login page
    Then I should see Webview Journey Personal Details Page
