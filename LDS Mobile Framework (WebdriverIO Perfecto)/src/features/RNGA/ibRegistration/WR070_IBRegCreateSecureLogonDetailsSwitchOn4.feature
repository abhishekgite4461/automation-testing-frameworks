@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3
Feature: Native IB Registration Create Secure Logon Details - Switch ON 4
  In order to register for Internet Banking
  As a RNGA authenticated customer
  I want to enter my details on create secure logon page

  @stub-ibreg-password-blacklisted @secondary
  #MOB3-4993, MOB3-3737
  Scenario: TC001 - Error - password is blacklisted (Server call)
    Given I am a ibreg_password_blacklisted user
    When I enter Personal and Account Details for IB Registration
    And I enter blacklisted Secure Logon Details on the IB Registration Create Secure Logon Details page
    And I click Next Button on the IB Registration Create Secure Logon Details page
    Then I should see Password Tip on the IB Registration Create Secure Logon Details page

  @stub-ibreg-mandateless
  #MOB3-5051, MOB3-3958
  Scenario: TC002 - Default masked for Password field
    Given I am a ibreg_mandateless user
    When I enter Personal and Account Details for IB Registration
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    Then I should see the Password Field is masked on the IB Registration Create Secure Logon Details page

  @stub-ibreg-mandateless
  #MOB3-5051, MOB3-3958
  Scenario: TC003 - Default masked for Confirm Password field
    Given I am a ibreg_mandateless user
    When I enter Personal and Account Details for IB Registration
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter valid ConfirmPassword on the IB Registration Create Secure Logon Details page
    Then I should see the Confirm Password Field is masked on the IB Registration Create Secure Logon Details page

  @stub-ibreg-mandateless
  #MOB3-5051, MOB3-3958
  Scenario: TC004 - Customer can unmask Password field
    Given I am a ibreg_mandateless user
    When I enter Personal and Account Details for IB Registration
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    And I should see the Password Field is masked on the IB Registration Create Secure Logon Details page
    And I click HideShow Button on Password Field on the IB Registration Create Secure Logon Details page
    Then I should see the Password Field is not masked on the IB Registration Create Secure Logon Details page

  @stub-ibreg-mandateless
  #MOB3-5051, MOB3-3958
  Scenario: TC005 - Customer can unmask Confirm Password field
    Given I am a ibreg_mandateless user
    When I enter Personal and Account Details for IB Registration
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter valid ConfirmPassword on the IB Registration Create Secure Logon Details page
    And I should see the Confirm Password Field is masked on the IB Registration Create Secure Logon Details page
    And I click HideShow Button on Confirm Password Field on the IB Registration Create Secure Logon Details page
    Then I should see the Confirm Password Field is not masked on the IB Registration Create Secure Logon Details page

  @stub-ibreg-mandateless
  #MOB3-5051, MOB3-3958
  Scenario: TC006 - Customer tabs out of the Password field
    Given I am a ibreg_mandateless user
    When I enter Personal and Account Details for IB Registration
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    And I click HideShow Button on Password Field on the IB Registration Create Secure Logon Details page
    And I should see the Password Field is not masked on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I should see the Password Field is masked on the IB Registration Create Secure Logon Details page
    And I click Password Field on the IB Registration Create Secure Logon Details page
    Then I should see the Password Field is masked on the IB Registration Create Secure Logon Details page

  @stub-ibreg-mandateless
  #MOB3-5051, MOB3-3958
  Scenario: TC007 - Customer tabs out of the Confirm Password field
    Given I am a ibreg_mandateless user
    When I enter Personal and Account Details for IB Registration
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter valid ConfirmPassword on the IB Registration Create Secure Logon Details page
    And I click HideShow Button on Confirm Password Field on the IB Registration Create Secure Logon Details page
    And I should see the Confirm Password Field is not masked on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I should see the Confirm Password Field is masked on the IB Registration Create Secure Logon Details page
    And I click Confirm Password Field on the IB Registration Create Secure Logon Details page
    Then I should see the Confirm Password Field is masked on the IB Registration Create Secure Logon Details page

  @stub-ibreg-mandateless
  #MOB3-5051, MOB3-3958
  Scenario: TC008 - Customer removes all the characters from Password Field - Selection is Hidden
    Given I am a ibreg_mandateless user
    When I enter Personal and Account Details for IB Registration
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    And I should see the Password Field is masked on the IB Registration Create Secure Logon Details page
    And I make the Password Field blank on the IB Registration Create Secure Logon Details page
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    Then I should see the Password Field is masked on the IB Registration Create Secure Logon Details page

  @stub-ibreg-mandateless
  #MOB3-5051, MOB3-3958
  Scenario: TC009 - Customer removes all the characters from Confirm Password field - Selection is Hidden
    Given I am a ibreg_mandateless user
    When I enter Personal and Account Details for IB Registration
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter valid ConfirmPassword on the IB Registration Create Secure Logon Details page
    And I should see the Confirm Password Field is masked on the IB Registration Create Secure Logon Details page
    And I make the Confirm Password field blank on the IB Registration Create Secure Logon Details page
    And I enter valid ConfirmPassword on the IB Registration Create Secure Logon Details page
    Then I should see the Confirm Password Field is masked on the IB Registration Create Secure Logon Details page

  @stub-ibreg-mandateless
  #MOB3-5051, MOB3-3958
  Scenario: TC010 - Customer removes all the characters from Password Field - Selection is Shown
    Given I am a ibreg_mandateless user
    When I enter Personal and Account Details for IB Registration
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    And I click HideShow Button on Password Field on the IB Registration Create Secure Logon Details page
    And I should see the Password Field is not masked on the IB Registration Create Secure Logon Details page
    And I make the Password Field blank on the IB Registration Create Secure Logon Details page
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    Then I should see the Password Field is not masked on the IB Registration Create Secure Logon Details page

  @stub-ibreg-mandateless
  #MOB3-5051, MOB3-3958
  Scenario: TC011 - Customer removes all the characters from Confirm Password Field - Selection is Shown
    Given I am a ibreg_mandateless user
    When I enter Personal and Account Details for IB Registration
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter valid ConfirmPassword on the IB Registration Create Secure Logon Details page
    And I click HideShow Button on Confirm Password Field on the IB Registration Create Secure Logon Details page
    And I should see the Confirm Password Field is not masked on the IB Registration Create Secure Logon Details page
    And I make the Confirm Password field blank on the IB Registration Create Secure Logon Details page
    And I enter valid ConfirmPassword on the IB Registration Create Secure Logon Details page
    Then I should see the Confirm Password Field is not masked on the IB Registration Create Secure Logon Details page

  @stub-ibreg-mandateless
  #MOB3-5051, MOB3-3958
  Scenario Outline: TC012 - Customer can re-mask Password
    Given I am a ibreg_mandateless user
    When I enter Personal and Account Details for IB Registration
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    And I should see the HideShow Label as "<hideShowLabelText1>" on Password Field on the IB Registration Create Secure Logon Details page
    And I click HideShow Button on Password Field on the IB Registration Create Secure Logon Details page
    And I should see the Password Field is not masked on the IB Registration Create Secure Logon Details page
    And I should see the HideShow Label as "<hideShowLabelText2>" on Password Field on the IB Registration Create Secure Logon Details page
    And I click HideShow Button on Password Field on the IB Registration Create Secure Logon Details page
    Then I should see the Password Field is masked on the IB Registration Create Secure Logon Details page

    Examples:
      | hideShowLabelText1 | hideShowLabelText2 |
      | Show               | Hide               |

  @stub-ibreg-mandateless
  #MOB3-5051, MOB3-3958
  Scenario Outline: TC013 - Customer can re-mask Confirm Password
    Given I am a ibreg_mandateless user
    When I enter Personal and Account Details for IB Registration
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter valid ConfirmPassword on the IB Registration Create Secure Logon Details page
    And I should see the HideShow Label as "<hideShowLabelText1>" on Confirm Password Field on the IB Registration Create Secure Logon Details page
    And I click HideShow Button on Confirm Password Field on the IB Registration Create Secure Logon Details page
    And I should see the Confirm Password Field is not masked on the IB Registration Create Secure Logon Details page
    And I should see the HideShow Label as "<hideShowLabelText2>" on Confirm Password Field on the IB Registration Create Secure Logon Details page
    And I click HideShow Button on Confirm Password Field on the IB Registration Create Secure Logon Details page
    Then I should see the Confirm Password Field is masked on the IB Registration Create Secure Logon Details page

    Examples:
      | hideShowLabelText1 | hideShowLabelText2 |
      | Show               | Hide               |
