@team-mpt @sw-ibreg-mvp-off @sw-enrolment-on @nga3 @rnga @stub-valid @pending @secondary
Feature: Native IB Registration Welcome Benefits - Switch OFF
  In order to register for Internet Banking
  As a RNGA authenticated customer
  I want to navigate to New Welcome Benefits page

  #In App Registration journey is not yet implemented. Pending on MComm team
  #This is a webview journey
  #MOB3-3427, MOB3-3428
  Scenario: TC001 - Navigate to IB registration journey (RNGA switch Off)
    Given I am a ibreg_mandateless user
    And I should be on the Welcome Benefits page
    When I click on Register button
    Then I should see Webview Journey Personal Details Page
