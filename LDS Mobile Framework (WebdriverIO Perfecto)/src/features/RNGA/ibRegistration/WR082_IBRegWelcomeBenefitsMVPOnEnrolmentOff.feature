@sw-ibreg-mvp-on @sw-enrolment-off @nga3 @rnga @stub-ibreg-mandateless @team-mpt @primary
Feature: Native IB Registration Welcome Benefits - MVP ON - Enrolment OFF
  In order to register for Internet Banking
  As a RNGA authenticated customer
  I want to navigate to New Welcome Benefits page

  #MOB3-3427, MOB3-3428
  Scenario: TC001 - Navigate to log-in page (RNGA Switch ON)
    Given I am a ibreg_mandateless user
    And I should be on the Login page
    When I click on Register for Internet Banking in login page
    Then I should be on the IB Registration Personal Details page
