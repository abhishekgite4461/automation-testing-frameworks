@sw-card_management-on @rnga @nga3 @stub-enrolled-typical @team-pas @primary
Feature: Native Card Controls - Switch ON
  As a NGA authenticated customer
  I want to view the list of options available in the global menu
  So that I can access the entry points to specific journeys

  #MOB3-5899
  Scenario: TC001 - Validate old card control options not available in the more menu
    Given I am an enrolled "enrolled_valid" user logged into the app
    When I select more menu
    Then I should not see the below options in the more menu
      |optionsInMorePage  |
      |lostStolenCard      |
      |replacementCardPin  |

  #MOB3-5899
  Scenario: TC002 - Verify the old card control options not available on the account's tile displayed on homepage
    Given I am an enrolled "enrolled_valid" user logged into the app
    And I navigate to current account on home page
    When I select the action menu of current account
    Then I should not see the below options in the action menu of the current account
      | optionsInActionMenu         |
      | lostOrStolenCardOption      |
      | replacementCardAndPinOption |

  #MOB3-5899
  Scenario: TC003 - Validate the card management option available in the global menu
    Given I am an enrolled "enrolled_valid" user logged into the app
    When I select more menu
    Then I should see the below options in the more menu
      | optionsInMorePage |
      | cardManagement      |

  #MOB3-5899
  Scenario: TC004 - Verify the card management on the account's tile displayed on homepage
    Given I am an enrolled "enrolled_valid" user logged into the app
    And I navigate to current account on home page
    When I select the action menu of current account
    Then I should see the below options in the action menu of the current account
      | optionsInActionMenu  |
      | cardManagementOption |
