@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3 @stub-ibreg-mandateless
Feature: Native IB Registration Account Details - Switch ON 2
  In order to register for Internet Banking
  As a RNGA pre-authenticated customer
  I want to enter my details on account details page

  Background:
    Given I am a ibreg_mandateless user
    And I click on Register button
    And I enter Personal Details on the IB Registration Personal Details page
    And I click Next Button on the IB Registration Personal Details page

  #MOB3-3735, MOB3-3803, MQE-48
  Scenario Outline: TC001 – Mandatory characters validation for loan account - Leaving blank
    When I select Account Type "<accountType>" on the IB Registration Account Details page
    And I click Loan Number Field on the IB Registration Account Details page
    And I tap out of the field on the IB Registration Account Details page
    Then I verify the ErrorMessage "<HC-178>" on the IB Registration Account Detail page

    Examples:
      | accountType | HC-178                                  |
      | Loan        | Please enter a valid loan number        |

  #MOB3-3735, MOB3-3803, MQE-48
  @secondary
  Scenario Outline: TC002 – Mandatory characters validation for loan account - Less than Minimum characters
    When I select Account Type "Loan" on the IB Registration Account Details page
    And I enter "12345678" in "loanNumber" Field on the IB Registration Account Details page
    Then I verify the ErrorMessage "<HC-178>" on the IB Registration Account Detail page

    Examples:
      | HC-178                                   |
      | Please enter a valid loan number         |

  #MOB3-3735, MOB3-3803, MQE-48
  Scenario Outline: TC003 – Mandatory characters validation for loan account - Delete characters
    When I select Account Type "Loan" on the IB Registration Account Details page
    And I enter valid LoanNumber on the IB Registration Account Details page
    And I delete characters from LoanNumber Field on the IB Registration Account Details page
    Then I verify the ErrorMessage "<HC-178>" on the IB Registration Account Detail page

    Examples:
      | HC-178                                   |
      | Please enter a valid loan number         |

  #MOB3-3735, MOB3-3803
  Scenario: TC004 – Mandatory characters validation for loan account - Minimum Length
    When I select Account Type "Loan" on the IB Registration Account Details page
    And I enter "378282246310" in "loanNumber" Field on the IB Registration Account Details page
    Then I should not see ErrorMessage on the IB Registration Account Details page

  #MOB3-3735, MOB3-3803
  Scenario: TC005 – Mandatory characters validation for loan account - Maximum Length
    When I select Account Type "Loan" on the IB Registration Account Details page
    And I enter "3782822463100052" in "loanNumber" Field on the IB Registration Account Details page
    And I validate maximum characters of LoanNumber Field on the IB Registration Account Details page
    Then I should not see ErrorMessage on the IB Registration Account Details page

  #MOB3-3735, MOB3-3803
  Scenario: TC006 – Customer can see help text for Loan account
    When I select Account Type "Loan" on the IB Registration Account Details page
    And I click Loan Number Field on the IB Registration Account Details page
    Then I should see Loan Tip with "This is the 12-digit reference number that appears on your loan documents, including monthly statements." value on the IB Registration Account Details page

  #MOB3-3735, MOB3-3803, MQE-48
  Scenario Outline: TC007 – Mandatory characters validation for mortgage account - Leave blank
    When I select Account Type "Mortgage" on the IB Registration Account Details page
    And I click Mortgage Number Field on the IB Registration Account Details page
    And I tap out of the field on the IB Registration Account Details page
    Then I verify the ErrorMessage "<HC-178>" on the IB Registration Account Detail page

    Examples:
      | HC-178                                       |
      | Please enter a valid mortgage number         |

  #MOB3-3735, MOB3-3803, MQE-48
  @secondary
  Scenario Outline: TC008 – Customer can see help text for mortgage account - Less than Minimum characters
    When I select Account Type "Mortgage" on the IB Registration Account Details page
    And I enter "12345678" in "mortgageNumber" Field on the IB Registration Account Details page
    Then I verify the ErrorMessage "<HC-178>" on the IB Registration Account Detail page

    Examples:
      | HC-178                                       |
      | Please enter a valid mortgage number         |

  #MOB3-3735, MOB3-3803, MQE-48
  Scenario Outline: TC009 – Customer can see help text for mortgage account - Delete characters
    When I select Account Type "Mortgage" on the IB Registration Account Details page
    And I enter valid MortgageNumber on the IB Registration Account Details page
    And I delete characters from MortgageNumber Field on the IB Registration Account Details page
    Then I verify the ErrorMessage "<HC-178>" on the IB Registration Account Detail page

    Examples:
      | HC-178                                       |
      | Please enter a valid mortgage number         |

  #MOB3-3735, MOB3-3803
  Scenario: TC010 – Mandatory characters validation for mortgage account - Minimum Length
    When I select Account Type "Mortgage" on the IB Registration Account Details page
    And I enter "37828224631046" in "mortgageNumber" Field on the IB Registration Account Details page
    Then I should not see ErrorMessage on the IB Registration Account Details page

  #MOB3-3735, MOB3-3803
  Scenario: TC011 – Mandatory characters validation for mortgage account - Maximum Length
    When I select Account Type "Mortgage" on the IB Registration Account Details page
    And I enter "3782822463100052" in "mortgageNumber" Field on the IB Registration Account Details page
    And I validate maximum characters of MortgageNumber Field on the IB Registration Account Details page
    Then I should not see ErrorMessage on the IB Registration Account Details page

  #MOB3-3735, MOB3-3803
  Scenario: TC012 – Customer can see help text for mortgage account
    When I select Account Type "Mortgage" on the IB Registration Account Details page
    And I click Mortgage Number Field on the IB Registration Account Details page
    Then I should see Mortgage Tip with "This is the 14-digit account number that appears on your mortgage documents, including your annual statement." value on the IB Registration Account Details page

  #MOB3-3735, MOB3-3803
  Scenario: TC013 – Persist Credit Card data once all mandatory character fields have been met
    When I select Account Type "Credit Card" on the IB Registration Account Details page
    And I enter valid CreditCard on the IB Registration Account Details page
    And I navigate back to IB Registration Account Details page from Personal Details page
    Then I validate CreditCard Number on the IB Registration Account Details page

  #MOB3-3735, MOB3-3803
  Scenario: TC014 – Data Should not persist for Credit Card when fields have errors
    When I select Account Type "Credit Card" on the IB Registration Account Details page
    And I enter "1234567890" in "creditCardNumber" Field on the IB Registration Account Details page
    And I navigate back to IB Registration Account Details page from Personal Details page
    Then I validate CreditCard should be blank on the IB Registration Account Details page

  #MOB3-3735, MOB3-3803
  Scenario: TC015 – Persist Loan Number data once all mandatory character fields have been met
    When I select Account Type "Loan" on the IB Registration Account Details page
    And I enter valid LoanNumber on the IB Registration Account Details page
    And I navigate back to IB Registration Account Details page from Personal Details page
    Then I validate LoanNumber on the IB Registration Account Details page

  #MOB3-3735, MOB3-3803
  Scenario: TC016 – Data Should not persist for Loan Number when fields have errors
    When I select Account Type "Loan" on the IB Registration Account Details page
    And I enter "12345678" in "loanNumber" Field on the IB Registration Account Details page
    And I navigate back to IB Registration Account Details page from Personal Details page
    Then I validate LoanNumber should be blank on the IB Registration Account Details page

  #MOB3-3735, MOB3-3803
  Scenario: TC017 – Persist Mortgage Number data once all mandatory character fields have been met
    When I select Account Type "Mortgage" on the IB Registration Account Details page
    And I enter valid MortgageNumber on the IB Registration Account Details page
    And I navigate back to IB Registration Account Details page from Personal Details page
    Then I validate MortgageNumber on the IB Registration Account Details page

  #MOB3-3735, MOB3-3803
  Scenario: TC018 – Data Should not persist for Mortgage Number when fields have errors
    When I select Account Type "Mortgage" on the IB Registration Account Details page
    And I enter "12345678" in "mortgageNumber" Field on the IB Registration Account Details page
    And I navigate back to IB Registration Account Details page from Personal Details page
    Then I validate MortgageNumber should be blank on the IB Registration Account Details page
