@team-mpt @sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3 @stub-ibreg-mandateless
Feature: Native IB Registration Create Secure Logon Details - Switch ON 3
  In order to register for Internet Banking
  As a RNGA authenticated customer
  I want to enter my details on create secure logon page

  Background:
    Given I am a ibreg_mandateless user
    And I enter Personal and Account Details for IB Registration

  #MOB3-4993, MOB3-3737
  Scenario Outline: TC001 – Max Length of Password Field
    When I enter valid UserId on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter "<longPassword>" in Password Field on the IB Registration Create Secure Logon Details page
    Then I check max length of Password Field on the IB Registration Create Secure Logon Details page

    Examples:
      | longPassword               |
      | abcdefghijklmnopqrstuvwxyz |

  #MOB3-4993, MOB3-3737
  Scenario: TC002 - Confirm password is enabled when customer has entered a valid password
    When I enter valid UserId on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    Then I should see Confirm Password Field is enabled on the IB Registration Create Secure Logon Details page

  @manual
  #MOB3-4993, MOB3-3737
  Scenario: TC003 - Customer can paste into password fields, but not copy from them
    When I click Password Field on the IB Registration Create Secure Logon Details page
    And I paste text in Password Field the IB Registration Create Secure Logon Details page
    And I make the Password Field blank on the IB Registration Create Secure Logon Details page
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    Then I should not be able to copy the text from the Password Field on the IB Registration Create Secure Logon Details page

  #MOB3-4993, MOB3-3737
  Scenario: TC004 - Confirm Password disabled when Password field is empty
    When I enter valid UserId on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I click Password Field on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    Then I should see Confirm Password Field is not enabled on the IB Registration Create Secure Logon Details page

  #MOB3-4993, MOB3-3737
  Scenario: TC005 - Confirm password disabled when customer changes Password field
    When I enter valid Password on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I should see Confirm Password Field is enabled on the IB Registration Create Secure Logon Details page
    And I make the Password Field blank on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    Then I should see Confirm Password Field is not enabled on the IB Registration Create Secure Logon Details page

  #MOB3-4993, MOB3-3737
  Scenario Outline: TC006 - Confirm password must match the password
    When I enter valid Password on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter "<confirmPassword>" in Confirm Password Field on the IB Registration Create Secure Logon Details page
    Then I should see Confirm Password Tip on the IB Registration Create Secure Logon Details page

    Examples:
      | confirmPassword |
      | randomtext      |

  #MOB3-4993, MOB3-3737
  Scenario Outline: TC007  - Next button disabled when User Id field has invalid values
    When I enter "<userId>" in UserId Field on the IB Registration Create Secure Logon Details page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I enter valid ConfirmPassword on the IB Registration Create Secure Logon Details page
    Then I tap out and should see next button is not enabled on the IB Registration Create Secure Logon Details page

    Examples:
      | userId |
      | test   |

  #MOB3-4993, MOB3-3737
  Scenario Outline: TC008  - Next button disabled when Password field has invalid values
    When I enter valid UserId on the IB Registration Create Secure Logon Details page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I enter "<password>" in Password Field on the IB Registration Create Secure Logon Details page
    Then I tap out and should see next button is not enabled on the IB Registration Create Secure Logon Details page

    Examples:
      | password |
      | pass     |

  #MOB3-4993, MOB3-3737
  Scenario Outline: TC009  - Next button disabled when User Id field has invalid values
    When I enter valid UserId on the IB Registration Create Secure Logon Details page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    And I tap out of the Field on the IB Registration Create Memorable Info page
    And I enter "<password>" in Confirm Password Field on the IB Registration Create Secure Logon Details page
    Then I tap out and should see next button is not enabled on the IB Registration Create Secure Logon Details page

    Examples:
      | password |
      | pass     |

  #MOB3-4993, MOB3-3737
  Scenario: TC010  - Password cannot include customer’s full first name
    When I enter first name in Password Field on the IB Registration Create Secure Logon Details page
    Then I should see Password Tip on the IB Registration Create Secure Logon Details page

  #MOB3-4993, MOB3-3737
  Scenario: TC011  - Password cannot include customer’s full last name
    When I enter last name in Password Field on the IB Registration Create Secure Logon Details page
    Then I should see Password Tip on the IB Registration Create Secure Logon Details page

  #MOB3-4993, MOB3-3737
  Scenario Outline: TC012  - Password cannot include customer’s date of birth
    When I enter DOB with date format as "<dateFormat>" in Password Field on the IB Registration Create Secure Logon Details page
    Then I should see Password Tip on the IB Registration Create Secure Logon Details page

    Examples:
      | dateFormat |
      | yyyy       |
      | ddmmyy     |
      | ddmmyyyy   |

  #MOB3-4993, MOB3-3737
  Scenario Outline: TC013  - Cannot be the same as the customers User Id
    When I enter "<userId>" in UserId Field on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter "<userId>" in Password Field on the IB Registration Create Secure Logon Details page
    Then I should see Password Tip on the IB Registration Create Secure Logon Details page

    Examples:
      | userId    |
      | userid123 |

  @secondary
  #MOB3-4993, MOB3-3737
  Scenario Outline: TC014  - Password is not case sensitive
    When I enter valid UserId on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter "<password>" in Password Field on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter "<password>" in Confirm Password Field on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    Then I should see Next Button is enabled on the IB Registration Create Secure Logon Details page

    Examples:
      | password   |
      | TESter1234 |

  @secondary
  #MOB3-4993, MOB3-3737
  Scenario: TC015  - Find Out More Dialog
    When I click Password Field on the IB Registration Create Secure Logon Details page
    Then I should see FindOutMore Link beneath Password on the IB Registration Create Secure Logon Details page

  @android
  #MOB3-4993, MOB3-3737
  Scenario: TC016 – Error - no values entered in password field
    When I enter valid UserId on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I click Password Field on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I should see an error icon on the IB Registration Create Secure Logon Details page
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    Then I should not see an error icon on the IB Registration Create Secure Logon Details page

  @android
  #MOB3-4993, MOB3-3737
  Scenario Outline: TC017 – Min Length of Password Field
    When I enter valid UserId on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter "<password1>" in Password Field on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I should see an error icon on the IB Registration Create Secure Logon Details page
    And I make the Password Field blank on the IB Registration Create Secure Logon Details page
    And I enter "<password2>" in Password Field on the IB Registration Create Secure Logon Details page
    Then I should not see an error icon on the IB Registration Create Secure Logon Details page

    Examples:
      | password1 | password2 |
      | pass123   | pass1234  |

  @android
  #MOB3-4993, MOB3-3737
  Scenario Outline: TC018 – Error - Password details invalid
    When I enter valid UserId on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter "<password>" in Password Field on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    Then I should see an error icon on the IB Registration Create Secure Logon Details page

    Examples:
      | password     |
      | abcdefghijkl |

  #For iOS, there is no accessibility ID for the field level error message, Hence to be validated manually
  @android
  #MOB3-4993, MOB3-3737
  Scenario: TC019 – Error - no values entered in password field
    When I enter valid UserId on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I click Password Field on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I should see an error icon on the IB Registration Create Secure Logon Details page
    And I enter valid Password on the IB Registration Create Secure Logon Details page
    Then I should not see an error icon on the IB Registration Create Secure Logon Details page

  #For iOS, there is no accessibility ID for the field level error message, Hence to be validated manually
  @android
  #MOB3-4993, MOB3-3737
  Scenario Outline: TC020 – Min Length of Password Field
    When I enter valid UserId on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter "<password1>" in Password Field on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I should see an error icon on the IB Registration Create Secure Logon Details page
    And I make the Password Field blank on the IB Registration Create Secure Logon Details page
    And I enter "<password2>" in Password Field on the IB Registration Create Secure Logon Details page
    Then I should not see an error icon on the IB Registration Create Secure Logon Details page

    Examples:
      | password1 | password2 |
      | pass123   | pass1234  |

  #For iOS, there is no accessibility ID for the field level error message, Hence to be validated manually
  @android
  #MOB3-4993, MOB3-3737
  Scenario Outline: TC021 – Error - Password details invalid
    When I enter valid UserId on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    And I enter "<password>" in Password Field on the IB Registration Create Secure Logon Details page
    And I tap out of the field on the IB Registration Create Secure Logon Details page
    Then I should see an error icon on the IB Registration Create Secure Logon Details page

    Examples:
      | password     |
      | abcdefghijkl |
