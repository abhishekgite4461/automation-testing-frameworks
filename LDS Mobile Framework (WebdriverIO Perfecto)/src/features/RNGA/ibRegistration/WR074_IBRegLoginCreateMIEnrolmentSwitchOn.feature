@sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3 @team-mpt @stubOnly @primary
Feature: Native IB Registration Create MI after login - Switch ON
  In order to register for Internet Banking
  As a RNGA pre-authenticated customer
  I want to enter my details on create memorable information page after login

  Background:
    Given I am a ibreg_create_mi user
    And I click on Login button
    And I login with my username and password
    And I should be on the IB Registration Create Memorable Info page

  @stub-ibreg-create-mi-within-grace-period
  #MOB3-2502, MOB3-1151
  Scenario: TC001 - MI Created with device enrolment on within grace period
    When I enter Create Memorable Info Details on the IB Registration Create Memorable Info page
    And I select Next Button on IB Registration Create Memorable Info page
    And I select continue button on the no security call needed page
    And I select continue button on the enrolment confirmation page
    And I select confirm button in auto logout settings page
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page

  @stub-ibreg-create-mi-after-grace-period
  #MOB3-2502, MOB3-1151
  Scenario: TC002 - MI Created with device enrolment on - outside the grace period
    When I enter Create Memorable Info Details on the IB Registration Create Memorable Info page
    And I select Next Button on IB Registration Create Memorable Info page
    And I choose call me now option
    And I select continue button on the enrolment confirmation page
    And I select confirm button in auto logout settings page
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page
