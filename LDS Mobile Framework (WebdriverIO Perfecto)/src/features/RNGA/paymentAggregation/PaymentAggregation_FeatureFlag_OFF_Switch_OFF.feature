@rnga @nga3 @team-payment-aggregator @stubOnly @sw-account-aggregation-on @stub-enrolled-typical
Feature: Open banking payment aggregation - Switch OFF -Feature Flag OFF
  In Order to make payment to my aggregated external bank current accounts using NGA app
  As a Retail NGA user
  I want to be able to make payment to my aggregated external bank current accounts using my app

  #PAT-60, PAT-61
  Scenario: TC001 – Main Scenario: User should not see the aggregated current account in the from list when Switch is OFF & Feature flag is OFF
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    When I get the list of all aggregated external accounts on home page
    And I am on the payment hub page
    Then I should not see the aggregated accounts in from list on payment hub page

    #PAT-37, PAT-427
  Scenario: TC002 -Payment Aggregation: User should not see the Payments and transfers option in action menu of external current account tile when Switch is OFF & Feature flag is OFF
    Given I am an enrolled "current" user logged into the app
    And I should see external accounts loaded
    And I navigate to external bank extBankAIB Current account tile
    When I select action menu button on external bank extBankAIB Current account tile
    Then I should see external bank name extBankAIB on action menu header
    And I should not see Transfer and payment option in the action menu
