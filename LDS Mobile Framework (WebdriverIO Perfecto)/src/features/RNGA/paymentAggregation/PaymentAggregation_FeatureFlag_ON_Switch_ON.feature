@rnga @nga3 @team-payment-aggregator @stubOnly @ff-payment-aggregation-on @sw-payment-aggregation-on
Feature: Open banking payment aggregation - Switch ON -Feature Flag ON
  In Order to make payment to my aggregated external bank current accounts using NGA app
  As a Retail NGA user
  I want to be able to make payment to my aggregated external bank current accounts using my app

  @manual
  #PAT-494, PAT-50, PAT-590, PAT-32, PAT-200, PAT-697
  Scenario: TC001 - Main Success Scenario: Customer sees a message when making the payment,
  customer sees a success screen when payment request is successful,
  refresh aggregated account balances after payment is successful,
  customer see payment reflected in account statement

    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    When I select from account as <fromAggBank> with <fromAccNumber> and to account as <toAggBank> with <toAccNumber> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should be navigated to the mobile app of the provider corresponding to the selected From account
    When I have successfully authenticated the payment
    And I am redirected back to my NGA app
    Then I should see the making payment screen as the payment is being processed
    And I should see payment success screen with details of payment and a button to make another payment
    When I close the payment success screen
    And I am on the payment hub page
    Then I should see the account balances refreshed for the From and To accounts
    When I view transactions for the From and To accounts
    Then I should see my payment shown on my transactions statement

  @manual
  #PAT-590, PAT-32
  Scenario: TC002 - Main Success Scenario: customer has option to make another payment
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    When I select from account as <fromAggBank> with <fromAccNumber> and to account as <toAggBank> with <toAccNumber> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should be navigated to the mobile app of the provider corresponding to the selected From account
    When I have successfully authenticated the payment
    And I am redirected back to my NGA app
    Then I should see the making payment screen as the payment is being processed
    And I should see payment success screen with details of payment and a button to make another payment
    When I click the make another payment button on payment success screen
    Then I should be taken to payments hub with the remitting account pre-selected as the same aggregated account

  #_____________________________________________________________________________________________________________________

  #   TECHNICAL VALIDATION SCENARIOS
  #_____________________________________________________________________________________________________________________

  @stub-payment-aggregation-initiate-payment-failure @primary
  #PAT-148, PAT-26
  Scenario Outline: TC002 – Aggregated From List: Customer can choose a 'From' account that is an aggregated account
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I get the list of all aggregated external accounts on home page
    When I am on the payment hub page
    Then I should see the aggregated accounts in from list on payment hub page
    When I select debitor account with sort code <sortCode> and account number <accNumber> in the payment hub page
    Then I should see the selected aggregated account <sortCode> with <accNumber> as a remitting account in payment hub
    Examples:
      | sortCode           | accNumber          |
      | extBankAIBSortCode | extBankAIBAccount1 |

  @stub-enrolled-typical @primary
  #PAT-149, PAT-28
  Scenario Outline: TC003 – Aggregated To List: Customer can choose a 'To' account that is an aggregated account
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    When I select debitor account with sort code <fromAccSortCode> and account number <fromAccNumber> in the payment hub page
    And I select To on payment hub page
    Then I should not see the selected from account in To list
    When I select creditor account with sort code <toAccSortCode> and account number <toAccNumber> in the payment hub page
    Then I should see the selected aggregated account <toAccSortCode> with <toAccNumber> in To account in payment hub page
    Examples:
      | fromAccSortCode    | fromAccNumber      | toAccSortCode      | toAccNumber        |
      | extBankAIBSortCode | extBankAIBAccount1 | extBankHFXSortCode | extBankHFXAccount1 |

  @stub-enrolled-typical
  #PAT-31, PAT-230
  Scenario Outline: TC_004-Payment Aggregation: Do not show amount field when the "To" field is not selected on Transfer & Payment screen
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    When I select debitor account with sort code <fromAccSortCode> and account number <fromAccNumber> in the payment hub page
    Then I should not see amount field in the payment hub page
    Examples:
      | fromAccSortCode    | fromAccNumber      |
      | extBankAIBSortCode | extBankAIBAccount1 |

  @stub-enrolled-typical
  #PAT-31, PAT-230
  Scenario Outline: TC_005-Payment Aggregation: Doesnt enable the continue button if user enters invalid amount
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select from account as <fromAccSortCode> with <fromAccNumber> and to account as <toAccSortCode> with <toAccNumber> in the payment hub page
    When I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    Then I should see the continue button disabled in the payment hub page
    Examples:
      | fromAccSortCode    | fromAccNumber      | toAccSortCode      | toAccNumber        | amount | referenceText  |
      | extBankAIBSortCode | extBankAIBAccount1 | extBankHFXSortCode | extBankHFXAccount1 | 0      | automated Test |

  @stub-enrolled-typical
  #PAT-31, PAT-230
  Scenario Outline: TC_006-Payment Aggregation: Show amount field when User select "From" and "To" field on Transfer & Payment screen
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select from account as <fromAccSortCode> with <fromAccNumber> and to account as <toAccSortCode> with <toAccNumber> in the payment hub page
    Then I should see amount field in the payment hub page
    And I should see reference field in the payment hub page
    And I should see the continue button disabled in the payment hub page
    Examples:
      | fromAccSortCode    | fromAccNumber      | toAccSortCode      | toAccNumber        |
      | extBankAIBSortCode | extBankAIBAccount1 | extBankHFXSortCode | extBankHFXAccount1 |

  @stub-enrolled-typical
  #PAT-31, PAT-230
  Scenario Outline: TC_007-Payment Aggregation: Help text should not be displayed when user enters amount on Transfer & Payment screen
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select from account as <fromAccSortCode> with <fromAccNumber> and to account as <toAccSortCode> with <toAccNumber> in the payment hub page
    When I select the amount field in the payment hub page
    Then I should not see the help text for amount field in the payment hub page
    Examples:
      | fromAccSortCode    | fromAccNumber      | toAccSortCode      | toAccNumber        |
      | extBankAIBSortCode | extBankAIBAccount1 | extBankHFXSortCode | extBankHFXAccount1 |

  @manual
  #PAT-31, PAT-230
  # Validating the numeric keyboard is a part of ui testing , hence manual
  Scenario Outline: TC_008-Payment Aggregation: Displays the platform specific keyboard on Transfer & Payment screen
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select from account as <fromAccSortCode> with <fromAccNumber> and to account as <toAccSortCode> with <toAccNumber> in the payment hub page
    When I select the amount field in the payment hub page
    Then I should see the numeric keyboard in the payment hub page
    Examples:
      | fromAccSortCode    | fromAccNumber      | toAccSortCode      | toAccNumber        |
      | extBankAIBSortCode | extBankAIBAccount1 | extBankHFXSortCode | extBankHFXAccount1 |

  @stub-enrolled-typical
  #PAT-31, PAT-230
  Scenario Outline: TC_009-Payment Aggregation: Displays only 2 decimal digits for the amount field on Transfer & Payment screen
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select from account as <fromAccSortCode> with <fromAccNumber> and to account as <toAccSortCode> with <toAccNumber> in the payment hub page
    When I enter <amount> in the amount field in the payment hub page
    Then I should see only 2 decimal places in the amount field in the payment hub page
    And I should see the continue button enabled in the payment hub page
    Examples:
      | fromAccSortCode    | fromAccNumber      | toAccSortCode      | toAccNumber        | amount          |
      | extBankAIBSortCode | extBankAIBAccount1 | extBankHFXSortCode | extBankHFXAccount1 | 0.019999        |
      | extBankAIBSortCode | extBankAIBAccount1 | extBankHFXSortCode | extBankHFXAccount1 | 9999999999.9999 |

  @stub-enrolled-typical
  #PAT-31, PAT-230
  Scenario Outline: TC_010-Payment Aggregation: Auto-populate decimal digits for integer amount on Transfer & Payment screen
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select from account as <fromAccSortCode> with <fromAccNumber> and to account as <toAccSortCode> with <toAccNumber> in the payment hub page
    When I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    And I should see only 2 decimal places in the amount field in the payment hub page
    Examples:
      | fromAccSortCode    | fromAccNumber      | toAccSortCode      | toAccNumber        | amount | referenceText |
      | extBankAIBSortCode | extBankAIBAccount1 | extBankHFXSortCode | extBankHFXAccount1 | 1      | automatedTest |

  @stub-enrolled-typical
  #PAT-31, PAT-230
  Scenario Outline: TC_011-Payment Aggregation: Enter amount greater than available balance for Transfer
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select from account as <fromAccSortCode> with <fromAccNumber> and to account as <toAccSortCode> with <toAccNumber> in the payment hub page
    And I enter amount greater than available balance in the aggregated remitter account in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see winback with Cancel and Continue option
    Examples:
      | fromAccSortCode    | fromAccNumber      | toAccSortCode      | toAccNumber        |
      | extBankAIBSortCode | extBankAIBAccount1 | extBankHFXSortCode | extBankHFXAccount1 |

  @stub-payment-aggregation-redirect-screen-delay
  #PAT-36, PAT-234, PAT-237, PAT-195
  Scenario Outline: TC_012-Payment Aggregation: Customer shown winback on continuing to redirect screen
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    When I select from account as <fromAccSortCode> with <fromAccNumber> and to account as <toAccSortCode> with <toAccNumber> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the Redirecting screen for account aggregation
    And I should see the remitter bank name <fromAggBank> on the redirect screen
    Examples:
      | fromAccSortCode    | fromAccNumber      | toAccSortCode      | toAccNumber        | amount | referenceText  | fromAggBank |
      | extBankAIBSortCode | extBankAIBAccount1 | extBankNTWSortCode | extBankNTWAccount1 | 10     | automated Test | extBankAIB  |

  @manual
  #PAT-33, PAT-283
  Scenario Outline: TC_013-Payment Aggregation: Redirecting user to external browser screen
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select from account as <fromAccSortCode> with <fromAccNumber> and to account as <toAccSortCode> with <toAccNumber> in the payment hub page
    When I enter <amount> in the amount field in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should be navigated to the mobile browser for the designated webpage of the selected provider bank
    Examples:
      | fromAccSortCode    | fromAccNumber      | toAccSortCode      | toAccNumber        | amount |
      | extBankAIBSortCode | extBankAIBAccount1 | extBankNTWSortCode | extBankNTWAccount1 | 10     |

  @stub-payment-aggregation-initiate-payment-failure
  #PAT-33, PAT-283
  Scenario Outline: TC_014-Payment Aggregation: Technical error while redirecting user to external browser screen
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select from account as <fromAccSortCode> with <fromAccNumber> and to account as <toAccSortCode> with <toAccNumber> in the payment hub page
    When I enter <amount> in the amount field in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    And I should see error screen while redirecting user to external browser screen
    Examples:
      | fromAccSortCode    | fromAccNumber      | toAccSortCode      | toAccNumber        | amount |
      | extBankAIBSortCode | extBankAIBAccount1 | extBankNTWSortCode | extBankNTWAccount1 | 10     |

  @manual
  #PAT-250, PAT-251, PAT-326
  Scenario Outline: TC_015-Payment Aggregation: Verify the app auto logoff time should increase to 5 mins and user session is still active
    Given I am an enrolled "current" user logged into the app
    And I have set my app auto logout time to "<auto-logoff_time>"
    When I am on the payment hub page
    And I select from account as <fromAggBank> with <fromAccNumber> and to account as <toAggBank> with <toAccNumber> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should be navigated to the mobile browser for the designated webpage of the selected provider bank
    When I manually switch back to the nga app from mobile browser
    And I manually switch back to the mobile browser from nga app
    And I have not completed the Payment journey
    Then I choose to come back to NGA app within "<comeback_time>"
    And I should see an unsuccessful information text on redirect screen
    When I navigate to home page
    Then I should be on the home page
    Examples:
      | fromAggBank | toAggBank  | fromAccNumber      | toAccNumber        | amount | auto-logoff_time | comeback_time |
      | extBankAIB  | extBankHFX | extBankAIBAccount1 | extBankHFXAccount1 | 10     | immediate        | 5 mins        |
      | extBankAIB  | extBankHFX | extBankAIBAccount1 | extBankHFXAccount1 | 10     | 1 min            | 5 mins        |
      | extBankAIB  | extBankHFX | extBankAIBAccount1 | extBankHFXAccount1 | 10     | 2 mins           | 5 mins        |
      | extBankAIB  | extBankHFX | extBankAIBAccount1 | extBankHFXAccount1 | 10     | 5 mins           | 5 mins        |
      | extBankAIB  | extBankHFX | extBankAIBAccount1 | extBankHFXAccount1 | 10     | 10 mins          | 10 mins       |

  @manual
  #PAT-250, PAT-251
  Scenario Outline: TC_016-Payment Aggregation: Verify the app auto logoff time should increase to 5 mins and user session is expired
    Given I am an enrolled "current" user logged into the app
    And I have set my app auto logout time to "<auto-logoff_time>"
    When I am on the payment hub page
    And I select from account as <fromAggBank> with <fromAccNumber> and to account as <toAggBank> with <toAccNumber> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should be navigated to the mobile browser for the designated webpage of the selected provider bank
    When I have not completed the Payment journey
    And I choose to come back to NGA app within "<comeback_time>"
    Then I should see user logged out error screen
    And Auto logout time should revert to original setting
    Examples:
      | fromAggBank | toAggBank  | fromAccNumber      | toAccNumber        | amount | auto-logoff_time | comeback_time |
      | extBankAIB  | extBankHFX | extBankAIBAccount1 | extBankHFXAccount1 | 10     | immediate        | 10 mins       |
      | extBankAIB  | extBankHFX | extBankAIBAccount1 | extBankHFXAccount1 | 10     | 1 min            | 10 mins       |
      | extBankAIB  | extBankHFX | extBankAIBAccount1 | extBankHFXAccount1 | 10     | 2 min            | 10 mins       |
      | extBankAIB  | extBankHFX | extBankAIBAccount1 | extBankHFXAccount1 | 10     | 5 mins           | 10 mins       |
      | extBankAIB  | extBankHFX | extBankAIBAccount1 | extBankHFXAccount1 | 10     | 10 mins          | 15 mins       |

  @stub-enrolled-typical
  #PAT-34, PAT-284
  Scenario Outline: TC_017-Payment Aggregation: - User does not complete browser journey and switch back to NGA app
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select from account as <fromAccSortCode> with <fromAccNumber> and to account as <toAccSortCode> with <toAccNumber> in the payment hub page
    When I enter <amount> in the amount field in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    And I manually switch back to the nga app from mobile browser without completing the journey
    Then I should see the redirect screen with authentication not completed message
    Examples:
      | fromAccSortCode    | fromAccNumber      | toAccSortCode      | toAccNumber        | amount |
      | extBankAIBSortCode | extBankAIBAccount1 | extBankNTWSortCode | extBankNTWAccount1 | 10     |

  @stub-arrangements-loan-only
  #PAT-37, PAT-427
  Scenario Outline: TC_018-Payment Aggregation: User should be able to see the Payments and transfers option in action menu of external current account tile
    Given I am an enrolled "current" user logged into the app
    And I should see external accounts loaded
    And I navigate to external bank extBankAIB Current account tile
    When I select action menu button on external bank extBankAIB Current account tile
    Then I should see external bank name extBankAIB on action menu header
    And I should see below options on external bank account action menu pop up
      | View transactions      |
      | Payments and transfers |
      | Manage accounts        |
    When I select Payments and transfers option from the action menu
    Then I should see the selected aggregated account <fromAccSortCode> with <fromAccNumber> as a remitting account in payment hub
    When I select creditor account with sort code <toAccSortCode> and account number <toAccNumber> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see winback with Cancel and Continue option
    Examples:
      | fromAccSortCode    | fromAccNumber      | toAccSortCode      | toAccNumber        | amount |
      | extBankAIBSortCode | extBankAIBAccount1 | extBankNTWSortCode | extBankNTWAccount1 | 10     |

  @stub-enrolled-typical
  #PAT-188, PAT-429
  Scenario Outline: TC019 – Aggregated To Own PCA: Customer can choose a 'To' account that is an aggregated account or own eligible PCA account
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    When I select debitor account with sort code <fromAccSortCode> and account number <fromAccNumber> in the payment hub page
    And I select To on payment hub page
    Then I should not see the selected from account in To list
    And I should see the <eligiblePCA> in To list on recipient page
    When I select creditor account as <eligiblePCA> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see winback with Cancel and Continue option
    When I select the Cancel option in winback
    And I select To on payment hub page
    Then I should not see the selected from account in To list
    When I select creditor account with sort code <toAccSortCode> and account number <toAccNumber> in the payment hub page
    Then I should see the selected aggregated account <toAccSortCode> with <toAccNumber> in To account in payment hub page
    Examples:
      | fromAccSortCode    | fromAccNumber      | toAccSortCode      | toAccNumber        | eligiblePCA | amount | referenceText |
      | extBankAIBSortCode | extBankAIBAccount1 | extBankNTWSortCode | extBankNTWAccount1 | Classic     | 110    | automation    |

  @stub-payment-aggregation-eligible-pca-and-ext-pca
  #PAT-61, PAT-430
  Scenario Outline: TC020 – one Aggregated and one PCA: Customer can choose a 'To' account that is an aggregated account or own eligible PCA account
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    When I select debitor account with sort code <sortCode> and account number <accNumber> in the payment hub page
    And I select To on payment hub page
    And I should see the <eligiblePCA> in To list on recipient page
    And I select creditor account as <eligiblePCA> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see winback with Cancel and Continue option
    Examples:
      | sortCode           | accNumber          | amount | referenceText | eligiblePCA |
      | extBankHFXSortCode | extBankNTWAccount3 | 20     | automation    | Current     |

  @stub-payment-aggregation-ineligible-payment-holdings
  #PAT-61, PAT-430
  Scenario: TC021 – Payment Aggregation: Ineligible payments holdings payment hub unavailable error page
    Given I am an enrolled "current" user logged into the app
    When I navigate to pay and transfer from home page
    Then I must see payments hub unavailable error message page

  @stub-some-dormant-accounts
  #PAT-490, PAT-461
  Scenario Outline: TC022 - Aggregated To Own Savings Account: Customer can choose a 'To' account that is an own account (Savings)
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    When I select debitor account with sort code <sortCode> and account number <accNumber> in the payment hub page
    And I select To on payment hub page
    Then I should see the <eligibleOwnAccount> in To list on recipient page
    When I select creditor account as <eligibleOwnAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see winback with Cancel and Continue option
    Examples:
      | sortCode           | accNumber          | eligibleOwnAccount | amount | referenceText |
      | extBankAIBSortCode | extBankAIBAccount1 | Easy Saver         | 0.09   | Test          |

  @stub-payment-aggregation-providers-not-supported
  #PAT-127
  Scenario Outline: TC023 - Not supported aggregated account: Customer can choose a from account that is not supported for pisp
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    When I select debitor account with sort code <sortCode> and account number <accNumber> in the payment hub page
    And I select To on payment hub page
    Then I should see the <eligiblePCA> in To list on recipient page
    When I select creditor account as <eligiblePCA> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see error message for <nonEligiblePISPBank> on redirect screen
    Examples:
      | sortCode           | accNumber          | eligiblePCA | amount | referenceText | nonEligiblePISPBank |
      | extBankAIBSortCode | extBankAIBAccount1 | Current     | 0.09   | Test          | extBankAIB          |
