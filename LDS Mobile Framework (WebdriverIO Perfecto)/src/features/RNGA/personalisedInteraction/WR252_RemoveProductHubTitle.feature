@team-pi @nga3 @rnga @secondary @stub-enrolled-valid
Feature: Android Remove product hub title
  As a PO
  I want to remove the title on the product hub
  So that the product hub is in line with the rest of the app

  #PI-684
  Scenario: AC01 - Remove Product hub title
    Given I am an enrolled "current" user logged into the app
    When I select apply tab from bottom navigation bar
    Then I should not see Discover and apply title on product hub page
