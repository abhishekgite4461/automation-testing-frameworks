@team-pi @nga3 @rnga @stubOnly @stub-enrolled-valid @outOfScope
 #The below scenarios has been made to out of scope since we have completed the AB Testing for LogOff scenarios and removed the feature from LIVE.
Feature: Customer is displayed with two variances of the log off button
  In order to make it easier for customers to log out from the mobile app
  As a Product Owner
  I want to show two variances of the log off button

  #These scenarios are stubOnly since there is an interaction between 3rd party provider Adobe.
  Background:
    Given I am an enrolled "current" user logged into the app

  #PI-264
  Scenario: AC01 - User testing for group A on more menu
    When I am part of the A test group
    And I click on the more menu in the bottom nav
    Then the log off button should appear in the top right corner of the screen

  #PI-264
  Scenario: AC02 - User testing for group B on more menu
    When I am part of the B test group
    And I click on the more menu in the bottom nav
    Then the log off button should appear at the bottom of the screen

  #PI-264
  Scenario: AC03 - User testing for group B on more menu (small screens)
    When I am part of the B test group
    And I click on the more menu in the bottom nav
    Then the log off button should appear as a sticky button at the bottom of the screen

  #PI-264
  Scenario: AC04 - Fallback/failure scenario - Default view on log off button
    When there is no live test available (performance data consent switched off or AB testing switched off or cannot connect to Adobe or any other connectivity error)
    And I click on the more menu in the bottom nav
    Then the log off button should appear at the bottom of the screen

  #PI-264 #PI-847 Android_Tablet: Logout button is disappearing
  Scenario: AC05 - User testing for group A on more menu (big screens/tablets)
    When I am part of the A test group
    And I click on the more menu in the bottom nav
    Then the log off button should appear in the top right corner of the screen
    When I rotate the screen or change the orientation to landscape
    Then the log off button should appear in the top right corner of the screen

  #PI-264 PI-589 AB Testing | RNGA | Log Off Button disappears when app is brought from background to front
  Scenario: AC06 - Seeing logoff after sending app to background and bringing it back
    When I am part of the A test group
    And I click on the more menu in the bottom nav
    Then the log off button should appear in the top right corner of the screen
    When I put the app in the background
    And I launch the app from the background
    Then the log off button should appear in the top right corner of the screen

  #PI-449
  Scenario: AC07 - User interaction with Tests A/B
    When I am a part of the A or B test group
    Then my interaction with Test A or B should only be measured once I have clicked on the log off button on the more screen
    And the conversion rate in adobe target should increase by 1

  #PI-449
  Scenario: AC08 - User Impression with Tests A/B
    When I am a part of the A or B test group
    Then my impression with Test A or B should only be measured once I have clicked on the more button on bottom nav and seen the AB test
    And the visitors count in adobe target should increase by 1
