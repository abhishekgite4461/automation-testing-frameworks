@team-pi @nga3 @rnga @stubOnly @stub-enrolled-valid @pending
Feature: In order to show priority service messages to customers in the mobile app
  As a product owner
  I want to add some pre-approved messages to the app
  So that I can notify customers when there is a service outage

  #These scenarios are stubOnly since there is an interaction between 3rd party provider Adobe target.
  Background:
    Given I am an enrolled "current" user logged into the app

  #PidI-564 and  #PIDI-565
  @ab-priority-message~normal
  Scenario:AC01 User can view and select pre-approved messages
    Then I should see a pre-approved message
    And I should be able to select pre-approved message

  Scenario:AC02 Planned outage message
    Then I should see a message in the P1 placement with a CTA saying
    |'We’re working on our app'|

  Scenario:AC03 Unplanned outage message
    Then I should see a message in the P1 placement with a CTA saying
    |'Some parts of our app aren’t working at the moment. Find out more'.|

  Scenario:AC04 Service restored message
    Then I should see a message in the P1 placement saying
    |'Our app is working as usual'|

  #PIDI-943
  Scenario: AC05 Customer clicks on stay option in winback (planned)
    When I select on the planned service message
      |'We’re working on our app'|
    And I select on the stay option on the winback
    Then I should see a message in the P1 placement with a CTA saying

  #PIDI-943
  Scenario: AC06 Customer clicks on stay option in winback (unplanned)
    When I select on the unplanned service message
      |'Some parts of our app aren’t working at the moment. Find out more'.|
    And I select on the stay option on the winback
    Then I should see a message in the P1 placement with a CTA saying
