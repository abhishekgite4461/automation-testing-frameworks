@team-pi @manual @nga3 @rnga @ff-maintenance-screen-enabled-on @sw-enable-placement-rnga-android-on @sw-enable-placement-rnga-ios-on
  # PIDI-1729 & PIDI-1650
  # Functionality : Placement before log in with CTA
  # This will be introduced in both platforms android and iOS and in both RNGA & BNGA in canoeing release
  # Adobe Activity - Maintenance Screen

Feature: To create the placement before log in.
  As a Retail and/or business app customer
  I want to see a placement before I log in to the app
  So that I can be made aware of important communications that may effect me

  Background:
    Given I am a mobile app user
    And the bank wants to communicate something urgent to me

  @primary
  Scenario: TC_01 User sees placement with copy
    When I launch the app
    Then I should be displayed with the placement screen
    And it should contain copy about the issue

  @secondary
  Scenario: TC_02 User sees placement with Primary CTA
    When I launch the app
    And I am displayed with the placement screen
    Then I should see a primary call to action button

  @secondary
  Scenario: TC_03 Behaviour of Primary CTA
    When I am on the placement screen
    And I select the CTA
    Then I should be taken to the app launch splash screen and subsequently MI (if the placement has been taken down then customer will go to MI screen to log in but if it is still present then they will come back to the placement)

  @secondary
  Scenario: TC_04 User sees placement with secondary CTA
    When I launch the app
    And I am displayed with the placement screen
    Then I should also see a secondary call to action button

  @secondary
  Scenario: TC_05 URLs in CTA
    When I launch the app
    And I am displayed with the placement screen that contains a CTA
    Then the CTA should be able to link out to public urls

  @secondary
  Scenario: TC_06 Frequency of comms screen
    When I try to log in
    Then I should see the placement every time until it has been removed
