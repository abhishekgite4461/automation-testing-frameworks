@team-pi @nga3 @rnga @stubOnly @ff-voice-id-on
Feature: In order to enable customers to use voiceiD as a method of authentication
  As a Product Owner
  I want to create a native page to allow customers to register for voiceiD

  Background:
    Given I am an enrolled "current" user logged into the app

  @stub-voice-id
  Scenario:AC01 Create page for VoiceID
    When I select Voice ID account tile
    Then I should be on the native register VoiceID page

  @stub-voice-id
  Scenario:AC02 bottom nav for android and not now for iOS
    When I select Voice ID account tile
    Then I should be on the native register VoiceID page
    When I select the bottom nav or not now button
    Then I should be on the home page

  @stub-voice-id
  Scenario:AC03 Voice ID Register button
    When I select Voice ID account tile
    Then I should be on the native register VoiceID page
    And I validate VoiceID page
    When I select Voice ID register button on the page
    Then I validate the call made for voice id is secure call
    And I should be on the home page

  @stub-voice-id
  Scenario:AC04 Back button not exist in Voice Id page
    When I select Voice ID account tile
    Then I should be on the native register VoiceID page
    And there should not be any back button on the page

  #Remove @pending once STUB is available in master
  @stub-voice-id-no-mobile-number @pending
  Scenario:AC05 Verify update phone number navigates to Update phone number page when mobile number is not available
    When I select Voice ID account tile
    Then I should be on the native register VoiceID page
    And I validate VoiceID page
    When I select Voice ID register button on the page
    Then I should be advised that I need to register a mobile phone number
    And I should be given an option to register a mobile phone
    And I should be given an option to continue with a non-authenticated call to Customer Services
    When I select the option to register a mobile phone number
    Then I should be shown the update phone number page

  #Remove @pending once STUB is available in master
  @stub-voice-id-no-mobile-number @pending
  Scenario:AC05 Make unsecure call when mobile number is not available
    When I select Voice ID account tile
    Then I should be on the native register VoiceID page
    And I validate VoiceID page
    When I select Voice ID register button on the page
    Then I should be advised that I need to register a mobile phone number
    And I should be given an option to register a mobile phone
    And I should be given an option to continue with a non-authenticated call to Customer Services
    When I select the option to continue with call
    Then I validate the call made for voice id is unsecure call
    And I should be on the home page
