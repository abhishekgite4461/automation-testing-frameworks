@team-pi @nga3 @rnga @stub-enrolled-valid @environmentOnly
Feature: Customer is displayed with welcome message
  In order to carry out personalisation in the mobile app
  As a Product Owner
  I want to show a welcome message to customers

  Background:
    Given I am an enrolled "current" user logged into the app

  #PI-277 #PI-265 #PI-277 #PI-265
  @pending
  Scenario: AC01 - Welcome message replaces 'home'
    And I should be on the home page
    Then Home text in the header should be replaced with a welcome message
    When I navigate to support and return back to home screen
    Then I should not see welcome message again

  #PI-277
  @pending
  Scenario: AC03 - Long First name if does not fit to the header tile will be removed and only welcome message be displayed
    And my first name does not meet the character limit of the header 
    When I land on the home screen
    Then I should be greeted with a welcome message and the time of day in the header above my account tile (no first name)

  #PI-265 #PI-277 #PI-265
  @primary @android
  Scenario: AC04 - Greeting messages
    And I navigate to profile page via more menu
    And The manage data consent link is displayed
    When I navigate to manage data consent page
    And I select the consentOptedIn button for the marketingPrefs section
    And I select the consentOptedIn button for the performancePrefs section
    And I confirm my selection on the data consent page
    Then I should be on the your personal details page
    When I navigate to logoff via more menu
    Then I should be on the logout page
    And I relaunch the app and login as a "current" user
    And I should be greeted with time of day with users first name
    And I verify users first name

  #PI-265
  @manualOnly
  Scenario: AC05 - Fallback/failure scenario
    When Adobe target does not send back a relevant time of day greeting due to a technical error
    Then I should be greeted with a welcome message without the time of day
