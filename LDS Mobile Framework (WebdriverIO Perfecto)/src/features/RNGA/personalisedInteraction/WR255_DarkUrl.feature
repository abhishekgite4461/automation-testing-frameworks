@team-pi @nga3 @rnga @stub-enrolled-valid
  #PIDI-479 Android | Put Dark URL link behind the feature flag 'isDarkUrlEntryEnabled'
Feature: Move/Put Dark URL link behind the feature flag isDarkUrlEntryEnabled
  As a team member
  I want to move/Put Dark URL link behind the feature flag 'isDarkUrlEntryEnabled' in Android
  So that the feature dark url behaves the same way as in iOS

  Background:
    Given I am an enrolled "current" user logged into the app
    When I navigate to settings page via more menu

  @ff-dark-url-enabled-on
  Scenario: TC_001 - Dark URL should be there when the ff is switched on
    Then I should see the dark url link

  Scenario: TC_002 - Dark URL should not be there when the ff is switched off
    Then I should not see the dark url link
