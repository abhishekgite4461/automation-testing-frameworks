@team-pi @nga3 @rnga @stubOnly @stub-enrolled-valid @outOfScope
#The below scenarios has been made to out of scope since we have completed the AB Testing for SupportHub scenarios and removed the feature from LIVE.
Feature: Customer is displayed with two Support Hub Entry point
  As a product owner
  I want to test two options of the Support Hub Entry point with live customers
  So that I can decide on the best option for the customer

  #These scenarios are stubOnly since there is an interaction between 3rd party provider Adobe.
  Background:
    Given I am an enrolled "current" user logged into the app

  #PI-276 and  #PI-327
  Scenario:AC01 - User testing for group A on bottom nav for Support hub
    When I am part of the A test group
    And I view the bottom nav bar
    Then the 4th tab should read "Support"

  Scenario:AC02 - Header for group A user testing.
    When I am part of the A test group
    And I navigate to the support hub
    Then the header must read "Support"

  Scenario:AC03 - User testing for group B on bottom nav for Support hub**
    When I am part of the B test group
    And I view the bottom nav bar
    Then the 4th tab  should read "Help"

  Scenario:AC04 - Header for group B user testing.
    And I am part of the B test group
    And I navigate to the support hub
    Then the header must read "Help".

  Scenario:AC05 - Default view on bottom nav for Support hub**
    When There is no live test is available (performance data consent switched off or AB testing switched off or cannot connect to Adobe or any other connectivity error)
    And I view the bottom nav bar
    Then the 4th tab should read "Support"

  Scenario:AC06 - Recording interaction with Support hub ABn test
    When I am part of the A or B test group
    And I click on 'Support' or 'Help' on the bottom nav bar
    Then my interaction with the test should be sent to Adobe target

  Scenario:AC07 - Not recording interaction for default users
    When I am not a part of any ABn test groups
    And I click on the default ** option on the bottom nav bar
    Then my interaction should not be be sent to Adobe target
