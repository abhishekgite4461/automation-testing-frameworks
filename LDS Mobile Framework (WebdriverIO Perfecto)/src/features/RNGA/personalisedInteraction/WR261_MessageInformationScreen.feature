@team-pi @manual @nga3 @rnga
  # PIDI-2387 & PIDI-2396
  # Functionality : Native Landing Page for COVID-19 Updates
  # This will be introduced in both platforms android and iOS and in RNGA in CVD-2 release
  # Adobe activity - "Message Information COVID-19" in QA Workspace and "P1 - Freeform text - COVID19" activity in QA Workspace
  # These scenarios are manual only since there is an interaction between 3rd party provider Adobe Target.

Feature: To create the Native Landing Page for COVID-19 Updates
  As a Retail app customer
  I want to see a Native Landing Page for COVID-19 Updates
  So that I can be made aware of important communications regarding the Covid-19 updates and bank services

  Background:
    Given I am a mobile app user
    And the bank wants to communicate something regarding the Covid-19 updates

  @primary
  Scenario: TC_01 User sees Native Landing Page for COVID-19 Updates
    When I navigate to home page
    Then I should be displayed with the P1 message or lead.
    And it should contain the deeplink for Native Landing Page for COVID-19 Update
    When I tap on the P1 Message or lead
    Then I should be able to navigate to Native Landing Page for Covid-19 Update.
    And I should be able to see the native screen which shows the information and communications regarding Covid-19 and bank services.

  @secondary
  Scenario: TC_02 User does not see Native Landing Page for COVID-19 Updates
    When "Message Information COVID-19" in QA Workspace is OFF
    And "P1 - Freeform text - COVID19" activity in QA Workspace is ON
    Then I should be displayed with the P1 message
    When I tap on the P1 Message
    Then I am not displayed the Native Landing Page for Covid-19 Update
    And P1 Message disappears
