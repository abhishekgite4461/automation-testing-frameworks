@team-pi @nga3 @rnga @stubOnly @stub-enrolled-valid @pending
Feature: Customer is displayed with three Product Hub Entry point
  As a product owner
  I want to test three options of the Product Hub Entry point with live customers
  So that I can decide on the best option for the customer

  #These scenarios are stubOnly since there is an interaction between 3rd party provider Adobe.
  Background:
    Given I am an enrolled "current" user logged into the app

  #PI-334 and  #PI-328

  Scenario:AC01 - User testing for group A on bottom nav for Product hub
    When I am part of the A test group
    And I view the bottom nav bar
    Then the 2nd tab must read "Apply"

  Scenario:AC02 - Header for group A user testing.
    When I am part of the A test group
    And I navigate to the product hub
    Then the header must read "Apply".

  Scenario:AC03 - User testing for group B on bottom nav for Product hub**
    When I am part of the B test group
    And I view the bottom nav bar
    Then the 2nd tab must read "Offers"

  Scenario:AC04 - Header for group B user testing.
    When I am part of the B test group
    And I navigate to the product hub
    Then the header must read "Offers".

  Scenario:AC05 - User testing for group C on bottom nav for Product hub**
    When I am part of the C test group
    And I view the bottom nav bar
    Then the 2nd tab must read "Products"

  Scenario:AC06 - Header for group C user testing.
    When I am part of the C test group
    And I navigate to the product hub
    Then the header must read "Products".

  Scenario:AC07 - Default view on bottom nav for Product hub**
    When There is no live test is available from adobe
    And I view the bottom nav bar
    Then the 2nd tab must read "Apply"

  Scenario:AC08 - Recording interaction with ABn test
    When I am part of the A or B test group
    And I click on any the variances within an ABn test
    Then my interaction with the test should be sent to Adobe target

  Scenario:AC09 - Recording interaction with Product hub ABn test
    When I am part of the A, B or C test group
    And I click on 'Apply', 'Offers' or 'Products' on the bottom nav bar
    Then my interaction with the test should be sent to Adobe target

  Scenario:AC10 - Not recording interaction for default users
    When I am not a part of any ABn test groups
    And I click on the default option on the bottom nav bar
    Then my interaction should not be be sent to Adobe target
