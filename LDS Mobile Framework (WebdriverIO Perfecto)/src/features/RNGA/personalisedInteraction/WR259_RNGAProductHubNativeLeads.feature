@team-pi @nga3 @rnga @manual
Feature: Customer will be shown
  As a RNGA customer
  I want relevant leads shown to me in my product and offers page
  So that I can see my personalised offers on that page

  Background:
    Given I am an enrolled "CURRENT" user logged into the app
    And I select product tab from bottom navigation bar

    # Activity name in adobe target - "ProductHub Lead - default experience".
    # Workspace name in adobe target - "Personalised Interaction - QA"
    #PIDI-1200, #PIDI-1194
  Scenario:TC01 - Display content/native lead in Product Hub
    When the activity in the Adobe Target is On
    And Target has returned a lead with the relevant content for an offer
    And the app receives this JSON content
    Then the app will insert the content in the relevant template
    And the native lead in product hub is displayed

  Scenario:TC02 - Do not display content/native lead in Product Hub
    When the activity in the Adobe Target is off
    Then the native lead in product hub is not displayed
