#Remove ios tag once Android is implemented PIEQ-90
@team-pi @nga3 @rnga @stubOnly @stub-cheque-deposit @ff-cheque-deposit-on @ios @primary
Feature: In order to show priority messages to customers in the mobile app
  As a product owner
  I want a placement to be created
  Note:We have created a Native Interstitial page for Cheque Scanning which is accessed via this lead.
  This provides some basic information around Cheque Scanning and lets them access the Cheque Scanning functionality

  Background:
    Given I am an enrolled "current" user logged into the app

  Scenario:AC01 Customer Selects Deposit Cheque
    When I select cheque promotion account tile
    Then I validate the Details in Cheque Promotion landing page
    When I select Deposit Cheque for the Cheque promotion page
    Then I should be on the cheque deposit screen

  Scenario:AC02 Customer Selects Demo
    When I select cheque promotion account tile
    Then I can see view demo button in Promotion page
    When I select the View Demo button
    Then I should see cheque imaging demo
    And I close the demo should return to the cheque promotion screen
    When I select the back header option in the cheque deposit page
    Then I should be on the home page

  Scenario:AC03 Customer Selects back button from Deposit Cheque page
    When I select cheque promotion account tile
    And I select Deposit Cheque for the Cheque promotion page
    Then I should be on the cheque deposit screen
    When I select the back header option in the cheque deposit page
    Then I should be on more menu
    When I select home tab from bottom navigation bar
    Then I should be on the home page
