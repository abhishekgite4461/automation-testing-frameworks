@team-pi @nga3 @rnga @stubOnly @stub-enrolled-valid
Feature: In order to show priority messages to customers in the mobile app
  As a product owner
  I want a placement to be created
  Note: This story is to create the placement with the prescribed behaviours. Any Test specific behaviours will be created as part of each AB test story

  #These scenarios are stubOnly since there is an interaction between 3rd party provider Adobe target.
  Background:
    Given I am an enrolled "current" user logged into the app

  #PI-276 and  #PI-327
  #Remove Android tag once defect PIDI-451 is fixed
  @ab-priority-message~normal
  Scenario:AC01 Dismiss button
    Then I should see normal placement in between the header and my first account tile
    And I should see a dismiss button in the top right corner of the placement
    And The priority message should be invisible when scrolling down and will display back while scroll up the page

  #Remove Android tag once defect PIDI-451 is fixed
  @ab-priority-message~normal @primary
  Scenario:AC02 Clicking the dismiss button
    When I should see normal placement in between the header and my first account tile
    And I select the priority dismiss button
    Then the placement should not be displayed in the top of the home page

  @ab-priority-message~normal @primary
  Scenario:AC03 Navigating away from the home page
    When I should see normal placement in between the header and my first account tile
    And I navigate away from the home page and come back
    Then I should see normal placement in between the header and my first account tile|

  @ab-priority-message~callToAction
  Scenario:AC07 Group B Variance
    When I should see Call to action placement in between the header and my first account tile
    Then I should see an upgrade now header for group B call to action

  #Remove Android tag once defect PIDI-451 is fixed
  @ab-priority-message~callToAction @primary
  Scenario:AC08 Group B dismissing the app upgrade message
    When I should see Call to action placement in between the header and my first account tile
    And I select the priority dismiss button
    And the app upgrade message should disappear for that session
    Then I should not see the placement again within the same session

  @manual
  Scenario:AC09 User not part of a test group
    When I am part of neither test group A or group B
    And I view the home page after logging in to the app
    Then I should not see an app upgrade message

  @manual
  Scenario:AC10 Not seeing the Priority 1 placement
    When I am not eligible for a personalisation message in Adobe Target for the P1 mbox
    And I log in to the mobile app
    Then I should not see the priority 1 placement tile on the home page

  @manual
  Scenario:AC11 Group A clicking the app upgrade message
    When I view the home page after logging in to the app
    And I select the app upgrade message placement
    Then I should be taken out of the app to the app store to upgrade my app

  @manual
  Scenario:AC12 Group B clicking the app upgrade message
    When I view the home page after logging in to the app
    And I select the 'Update now' text in the placement
    Then I should be taken out of the app to the app store to upgrade my app

  @manual
  Scenario:AC13 Group A interaction with test
    When I am a user who is part of test group A
    And I am on the home page after logging in to the app
    And I select the app upgrade message
    Then a positive interaction should be sent to Target

  @manual
  Scenario:AC14 Group B interaction with test
    When I am a user who is part of test group B
    And I am on the home page after logging in to the app
    And I select the 'Update now' text in the placement
    Then a positive interaction should be sent to Target

  @manual
  Scenario:AC15 Group A interaction with dismiss button
    When I am a user who is part of test group A
    And I view the home page after logging in to the app
    And I select the dismiss button
    Then a negative interaction should be sent to Target

  @manual
  Scenario:AC16 Group B interaction with dismiss button
    When I am a user who is part of test group B
    And I view the home page after logging in to the app
    And I select the dismiss button
    Then a negative interaction should be sent to Target

  @manual
  Scenario:AC17 Group A impressions
    When I am a user who is part of test group A
    And I view the home page after logging in to the app
    And I have a priority 1 message available to me
    Then an impression should be recorded per session

  @manual
  Scenario:A18 Group B impressions
    When I am a user who is part of test group B
    And I view the home page after logging in to the app
    And I have a priority 1 message available to me
    Then an impression should be recorded per session

  @manual
  Scenario:A19 Tapping on payment hub after viewing priority
    When I am a user who is part of test group B
    And I have a priority 1 message available to me
    And Tap on the payment hub form home page
    Then Tap back on the home page to view the priority
