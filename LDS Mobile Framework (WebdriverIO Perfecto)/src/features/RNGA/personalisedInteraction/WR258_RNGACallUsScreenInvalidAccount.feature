@team-pi @nga3 @rnga @pending
Feature: Remove unnecessary screens from the 'call us' page in the mobile app and show customer only relevant screen
  As a RNGA customer
  I want to only see relevant screens on the call us page
  So that I am not shown screens which are not required

  #PIDI-364
  Background:
    Given I am a RNGA user
    And I do not have any valid accounts

    # current behavior is that user is able to click on 'your accounts' and then is taken to a blank page because
    # they do not have any valid accounts. So we are removing this 'your accounts' button in the call us screen for users with invalid accounts.
  Scenario:AC01 - User without valid account
    When I click on the 'call us' button in support hub
    Then I should not see the 'your accounts' button on the following screen

  Scenario:AC02 - display of buttons
    When I click on the 'call us' button in support hub
    Then I should see the 'New products' button stretched across the top and the two bottom buttons remain the same.
