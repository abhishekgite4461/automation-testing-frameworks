@team-pi @nga3 @rnga @stub-enrolled-valid @sw-ab-testing-on @stubOnly @android
Feature: The message us button can be renamed using an ABtest.
  As a team wanting to run an ABTest
  I want to know that the Message Us button an be relabelled
  So that I know wich label is more effective.

  Background:
    Given I am an enrolled "CURRENT" user logged into the app

  @primary @ab-message-us-button~messagingTitleMessage
  Scenario: TC_001 - message button is labelled chat
    When I select support tab from bottom navigation bar
    Then the label is Message us for the message button

  @secondary @ab-message-us-button~messagingTitleChat
  Scenario: TC_002 - message button is labelled message us
    When I select support tab from bottom navigation bar
    Then the label is Chat to us for the message button
