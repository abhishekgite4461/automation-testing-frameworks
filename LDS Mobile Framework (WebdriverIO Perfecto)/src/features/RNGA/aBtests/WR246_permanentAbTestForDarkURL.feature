@team-pi @nga3 @rnga @stub-enrolled-valid @sw-ab-testing-on @ff-dark-url-enabled-on @stubOnly
Feature: The dark URL button can be renamed using an ABtest.
  As a team wanting to run an ABTest
  I want to know that the dark URL an be relabelled
  So that I know that the app can be assigned an AB test in Adobe target.

  Background:
    Given I am an enrolled "current" user logged into the app

  @primary @ab-dark-url-button~Submit
  Scenario: TC_001 - Dark URL button is renamed to Submit in stub mode
    When I navigate to settings page via more menu
    And I navigate to the dark URL page
    Then I should see a button labelled Submit to enter the URL

  @secondary @ab-dark-url-button~Open
  Scenario: TC_002 - Dark URL button is renamed to Open in stub mode
    When I navigate to settings page via more menu
    And I navigate to the dark URL page
    Then I should see a button labelled Open to enter the URL
