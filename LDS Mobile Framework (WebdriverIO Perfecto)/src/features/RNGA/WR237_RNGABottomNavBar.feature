@team-mpt @rnga @nga3 @stub-enrolled-typical @ff-beta-feedback-on
Feature: User accesses the bottom navigation bar and more menu
  In order to access the entry points to specific journeys
  As an NGA authenticated customer
  I want to view the options available on the bottom navigation bar and more menu

  @primary
  Scenario: TC_001_Main Success: Check selected tab on bottom navigation bar is highlighted
    Given I am an enrolled "current" user logged into the app
    And bottom navigation bar is displayed
    Then I verify selected tab on bottom navigation bar is highlighted
      | optionsInBottomNav |
      | home               |
      | apply              |
      | support            |
      | more               |

  @android
  Scenario: TC_002_Main Success_Scenario: Winback is displayed when device back button is pressed
    Given I am an enrolled "current" user logged into the app
    Then I verify stay on app functionality for device back button on each tab
      | optionsInBottomNav |
      | home               |
      | apply              |
      | support            |
      | more               |

  #MORE-952
  Scenario: TC_003 Verify report issue button is available
    Given I am an enrolled "current" user logged into the app
    Then I should see report issue button
    When I select apply tab from bottom navigation bar
    Then I should see report issue button
    When I select more tab from bottom navigation bar
    Then I should see report issue button
