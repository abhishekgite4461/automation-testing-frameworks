@rnga @nga3 @team-outboundmessaging @sw-enrolment-on @appiumOnly @android

Feature: User is able to change their telephone using Native app
  In order to let bank know about my new telephone number
  As an RNGA customer
  I want to change my telephone number

  Background:
    Given I am an enrolled "current" user logged into the app
    And I navigate to profile page via more menu

  @secondary
  Scenario: TC001 Customer navigates to the update telephone number page
    When I view the change telephone page
    Then I should see advisory text underneath the telephone area

  @secondary @stub-enrolled-typical
  Scenario Outline: TC002 Customer navigates to the update telephone number page
    When I have changed a <homeNumber> telephone number
    Then I should see advisory text underneath the telephone area on confirmation screen
    Examples:
      | homeNumber |
      | 7704124945 |

  @secondary @manual
  Scenario: TC003 Update telephone number page scrolls if using a smaller device
    When I should be able to scroll the page
    Then the Continue button should be stuck to the bottom of the page

  @primary
  Scenario: TC004 Customer navigates to the update telephone number page
    When I have changed a telephone number
    And I have selected to continue
    And I view the confirm number page
    And I have selected to confirm
    And I should be able to enter password in prompt
    Then I should be able see Change of Telephone confirmation screen
