@team-crowbar @nga3 @rnga @environmentOnly @wkWebview @ff-wkwebview-flag-on @instrument @pending @sw-enable-ios-wkwebview-on @manual
Feature: E2E_Webview_Verify RNGA for Replacement Card and Pin Journey

  Scenario: TC_001_User selects replacement card and pin option
    Given I am an current account user logged into the app
    When I select the Replace card & PIN option from card management page
    Then I should be on Replacement cards and PINs page
    When I select Replace card option
    And I select Pin option
    And I select Continue button
    Then I should be on Confirm your selections page
    When I select Previous button
    Then I should be back on Replacement cards and PINs page
    When I select Continue button
    And I select "Yes" to confirm my address details are correct
    And I enter my password
    And I select Continue button
    Then I should be on confirmation of request page
    And I should see Contact us accordian
    And i should see Help and support accordian
    When I select back option in header
    Then I should be on home page OR More menu
