@team-crowbar  @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @wkWebview @environmentOnly @ff-wkwebview-flag-on @instrument @pending @sw-enable-ios-wkwebview-on
Feature: E2E_Webview_Verify RNGA for Standing Order and Direct Debit

  Scenario Outline: TC_001 Create, Amend and Delete Standing Order
    Given I am a valid user on the home page
    And I select the "standing order" option in the action menu of <accountName> account
    When I should be on the view standing orders page
    Then I select setup a standing order in the standing orders tab of standing order and direct debits page
    And I select recipient <recipientName> to make transfer to from searched list of recipients
    And I enter <amount> in the amount field in the payment hub page for the selected account
    And I have chosen to make this a Standing Order option
    And I select standing order frequency <frequency> in standing order page
    And I select the Start Date for standing order
    And I enter the <createSO> reference for the standing order
    And I review the Standing Order
    And I enter a correct password
    When I should be on standing order created successful page
    Then I select view standing orders in standing order setup successful page
    And I amend the standing order with reference <createSO> on view standing orders page
    And I change the reference <amendSO> for the standing order
    And I change the amount <soAmount> for the standing order
    And I select amend in amend standing order page
    When I should be on confirm standing order amendments page
    Then I enter password in amend standing order page
    And I select confirm amendments button in the amend standing order confirmation page
    When I should be on standing order changed page
    Then I select view standing orders in standing order amendment success page
    And I delete the standing order with reference <amendSO> on view standing orders page
    When I should be on delete standing order page
    Then I enter password in delete standing order page
    And  I select delete standing order in the delete standing order confirmation page
    And I should see standing order deleted successfully message in view standing orders page

    Examples:
      | accountName | recipientName | amount | createSO | amendSO | soAmount | frequency |
      | current     | Saver         | 0.01   | AUTOSO   | AMENDSO | 0.02     | Monthly   |

  Scenario: TC_002 View and Delete Direct debits & View Standing Orders
    Given I am a direct debit current user on the home page
    When I select the "direct debit" option in the action menu of current account
    Then I should be on the direct Debit webview page
    When I select cancel direct debit button in the standing order and direct debit page
    And I confirm cancel in the cancel direct debit page
    Then I should see direct debit cancelled message
    When I validate the standing order tab is displayed in the standing order and direct debit page
    And I select the standing order tab
    Then i should be on standing order tab of the standing order and direct debit page
