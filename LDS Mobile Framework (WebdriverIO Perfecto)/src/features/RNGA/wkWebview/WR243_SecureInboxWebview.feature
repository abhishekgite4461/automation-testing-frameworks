@team-crowbar @nga3 @rnga @environmentOnly @mbnaSuite @noExistingData @pending @wkWebview @ff-wkwebview-flag-on @sw-enable-ios-wkwebview-on @instrument @manual
Feature: User wants to access Inbox and view unread correspondence from the bank
  In order to access correspondence from the bank
  As a RNGA Authenticated Customer
  I want to view my Inbox

  Scenario: TC_001 User can access Secure inbox and view unread correspondence from the bank
    Given I am an enrolled "valid non youth inbox" user logged into the app
    When I navigate to inbox page via more menu
    Then I should be on the inbox webview page
    When I choose a correspondence option from the inbox home page
    Then I should be taken to the message
    And I should be presented with an option to download as a PDF
    When I choose to download the correspondence as a PDF from the inbox page
    Then PDF is downloaded to my device
    When I select the back button from the message inbox page
    Then I am on inbox home page
    When I select back button
    Then I should be on the home page
