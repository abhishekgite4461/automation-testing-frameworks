@team-crowbar @nga3 @rnga @environmentOnly @wkWebview @ff-wkwebview-flag-on @instrument @pending @sw-enable-ios-wkwebview-on @manual
Feature: E2E_Webview_Verify RNGA for Lost or Stolen Card Journey

  Background:
    Given I am an current account user logged into the app

  Scenario: TC_001_User selects lost card option
    When I select the lost or stolen cards option from card management page
    Then I should be on the lost or stolen cards page
    When I select "Lost" option
    And I select Cancel card option
    And I select New card option
    And I select New pin option
    And I select Continue button
    Then I should be on Confirm your selections page
    When I select Previous button
    Then I should be back on lost or stolen card page
    When I select Continue button
    And I select "Yes" to confirm my address details are correct
    And I enter my password
    And I select Continue button
    Then I should be on confirmation of request page
    And I should see Contact us accordian
    And i should see Help and support accordian
    When I select back option in header
    Then I should be on home page OR More menu

  Scenario: TC_002_User selects stolen card option
    When I select the lost or stolen cards option from card management page
    Then I should be on the lost or stolen cards page
    When I select "Stolen" option
    And I select Cancel card option
    And I select New card option
    And I select New pin option
    And I select Continue button
    Then I should be on Confirm your selections page
    When I select Previous button
    Then I should be back on lost or stolen card page
    When I select Continue button
    And I select "Yes" to confirm my address details are correct
    And I enter my password
    And I select Continue button
    Then I should be on confirmation of request page
    And I should see Contact us accordian
    And i should see Help and support accordian
    When I select back option in header
    Then I should be on home page OR More menu
