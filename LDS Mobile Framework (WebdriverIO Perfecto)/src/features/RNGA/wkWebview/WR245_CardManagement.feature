@team-crowbar @nga3 @rnga @wkWebview @environmentOnly @ff-wkwebview-flag-on @instrument @pending @sw-enable-ios-wkwebview-on @manual
Feature: Webview_Verify RNGA for Card Management

  Background:
    Given I am an current account user logged into the app

  Scenario: TC_001_User enters Card Management journey from More menu
    When I select More from bottom nav bar
    And I select Card Management option
    Then I should be on Card Management page

  Scenario: TC_001_User enters Card Management journey from Actions Menu
    When I select the "Card Management" option in the action menu of Current account
    Then I should be on Card Management page
