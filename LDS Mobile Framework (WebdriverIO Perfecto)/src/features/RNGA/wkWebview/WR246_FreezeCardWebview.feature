@team-crowbar @nga3 @rnga @environmentOnly @wkWebview @ff-wkwebview-flag-on @instrument @pending @sw-enable-ios-wkwebview-on @manual
Feature: E2E_Webview_Verify RNGA for Freeze Card Journey

  Background:
    Given I am a multiple account user logged into the app (e.g. 2 current accounts or 1 current account and 1 credit card)

  Scenario: TC_001_User selects replacement card and pin option
    When I select Card Freezes option from card management page
    Then I should be on Card Freezes page
    When I select Freeze abroad toggle
    Then Freeze abroad toggle should be selected
    When I de-select Freeze abroad toggle
    Then Freeze abroad toggle should be de-selected
    When I select Freeze online and remote toggle
    Then Freeze online and remote toggle should be selected
    When I select Freeze at tills and terminals toggle
    Then Freeze at tills and terminals toggle should be selected
    When I select Help and Support option
    Then I should be on Help and Support page
    When I select back in header
    Then I should be on Card Freezes page
    When I swipe left on card
    Then I should be on different account
    When I select Freeze abroad toggle
    Then Freeze abroad toggle should be selected
    When I select Report a card lost or stolen option
    Then I should be on Lost or Stolen cards page
