@rnga @nga3 @team-servicing @stubOnly @stub-arrangements-loan-only @sw-enrolment-on @sw-account-aggregation-off @secondary
Feature: Account aggregation - Switch OFF
  In Order to access all my accounts including external bank current accounts using NGA app
  As a Retail NGA user
  I want to be able to add external bank accounts using my app

  #MOBS-591, MOBS-864
  Scenario: TC001 – Main Scenario: User should not see the OB Promo tile on the homepage when the Switch is OFF
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I am not shown Open Banking promo tile
