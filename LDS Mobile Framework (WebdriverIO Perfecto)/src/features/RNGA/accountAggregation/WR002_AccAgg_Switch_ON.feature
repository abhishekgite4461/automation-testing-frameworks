@rnga @nga3 @team-servicing @stubOnly @sw-enrolment-on @sw-account-aggregation-on
Feature: Open banking account aggregation - Switch ON
  In Order to access all my accounts including external bank current accounts using NGA app
  As a Retail NGA user
  I want to be able to add external bank accounts using my app

  @stub-open-banking-auto-load-external-accounts-success @secondary
  #MOBS-24, MOBS-25, MOBS-428, MOBS-448, MOBS-450, MOBS-444, MOBS-473, MOBS-531, MOBS-482, MOBS-472, MOBS-591, MOBS-864
  Scenario: TC001 – Main Success Scenario: User should see the OB Promo tile V2 on the homepage when Switch is ON
    Given I submit "correct" MI as "current" user on the MI page
    Then I should see ghost tile for external accounts load
    When I select the ghost tile to validate it is not clickable
    And I wait for ghost tile to disappear
    Then I should see external accounts loaded
    And I am shown Open Banking promo tile
    And I am shown the option to add external accounts
    And I am not shown the option to view my external accounts
    And I should be shown the list of added external bank account tiles above the Open Banking Promo Tile

  @team-aov @manual @secondary
  #MOBS-90, MOBS-19, MOBS-112, MOBS-257, MOBS-167, MOBS-322, MOBS-24, MOBS-25, MOBS-428, MOBS-448
  Scenario Outline: TC002 – Main Success Scenario: User should Add external accounts successfully
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the Redirecting screen for account aggregation
    And I should see the provider bank name extBankAIB on the redirect screen
    And I should be navigated to the mobile browser for the designated webpage of the selected provider bank
    When I aggregate "<countOfAccounts>" of my external account from the external provider website
    Then I see deeplink is triggered after account is successfully aggregated
    And I should be redirected back to the NGA app
    And I should be on the home page
    And I should see ghost tile for external accounts load
    And I wait for ghost tile to disappear
    And I should see external accounts loaded
    And I should receive an email on my email address with T&C as an attachment

    Examples:
      | countOfAccounts |
      | one             |
      | more than one   |

  @team-aov @stub-open-banking-ob-consent-provided
  #MOBS-70, MOBS-144, MOBS-119, MOBS-145, MOBS-710, MOBS-711, MOBS-787, MOBS-788
  Scenario: TC003 – Main Success Scenario: User should be able to view Externally added accounts on the Account Overview page
    Given I am an enrolled "current" user logged into the app
    Then I should see external accounts loaded
    And I should see below fields in external provider bank extBankNTW with account type Credit
      | creditCardNumber |
      | availableCredit  |
    And I should see below fields in external provider bank extBankNTW with account type Savings
      | sortCode      |
      | accountNumber |
    And I should see below fields in external provider bank extBankNTW with account type Current Account
      | sortCode      |
      | accountNumber |

  @team-aov @stub-open-banking-ob-consent-provided @secondary
  #MOBS-764, MOBS-791, MOBS-919, MOBS-940, MOBS-927, MOBS-942
  Scenario: TC004 - Main Success Scenario: User should be able to remove external bank accounts from Consent Management screen
    Given I am an enrolled "current" user logged into the app
    Then I should see external accounts loaded
    When I select action menu button on external bank extBankNTW Credit account tile
    Then I should see external bank name extBankNTW on action menu header
    When I select Manage accounts option from the action menu
    Then I should be on Consent settings page
    And I should see Authorised account for extBankBarclays on consent settings page
    When I select Manage button for extBankBarclays account on consent settings page
    Then I should see Consent settings pop up for extBankBarclays
    When I select Remove option on Consent settings pop up
    Then I should see remove alert on Consent settings page
    And I should see winback with Close and Remove option
    When I select the Remove option in winback
    And I wait for spinner to disappear
    Then I should see Revoked account for extBankBarclays on consent settings page

  @stub-open-banking-balance-not-available @secondary
  #MOBS-161, MOBS-200, MOBS-162, MOBS-199, MOBS-824
  Scenario: TC005 - Main Success Scenario: User should be able to view action menu options and transactions page for external bank current, credit and savings account
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I select action menu button on external bank extBankBarclays Credit account tile
    And I should see external bank name extBankBarclays on action menu header
    And I should see below options on external bank account action menu pop up
      | View transactions |
      | Manage accounts   |
    When I select View transactions option from the action menu
    Then I should see the transactions for the externally added account
    When I navigate to home page
    Then I should be on the home page
    And I select action menu button on external bank extBankBarclays Savings account tile
    And I should see external bank name extBankBarclays on action menu header
    And I should see below options on external bank account action menu pop up
      | View transactions |
      | Manage accounts   |
    When I select View transactions option from the action menu
    Then I should see the transactions for the externally added account
    When I navigate to home page
    Then I should be on the home page
    And I select action menu button on external bank extBankHFX Current account tile
    And I should see external bank name extBankHFX on action menu header
    And I should see below options on external bank account action menu pop up
      | View transactions |
      | Manage accounts   |
    When I select View transactions option from the action menu
    Then I should see the transactions for the externally added account

  @manual @secondary
  #MOBS-441, MOBS-478, MOBS-440, MOBS-475, MOBS-653, MOBS-646, MOBS-995, MOBS-1026
  Scenario: TC006 - Main Success Scenario: User able to renew consent via notification when consent is about to expired
    Given I reset the date close to extBank external account consent expiry date
    And I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    When I navigate to external bank extBank error tile
    And I select action menu button on external bank extBankNTW Current account tile
    Then I should see external bank name extBank on action menu header
    And I should see below options on external bank account action menu pop up
      | View transactions     |
      | Renew account sharing |
      | Manage accounts       |
    When I select Renew account sharing option from the action menu
    Then I should not see the Set up in 3 steps screen for account aggregation
    And I should see the details for renew consent important information screen for extBank
    When I close the account aggregation screen
    Then I should be on the home page
    When I navigate to external bank extBank error tile
    Then I should "see" notification on external bank extBank account tile
    And I should "see" notification message for "consent will expire in x days/today"  on external bank extBank account tile
    When I select notification button on external account tile for extBank bank
    Then I should not see the Set up in 3 steps screen for account aggregation
    And I should see the details for renew consent important information screen for extBank
    When I agree to renew my consent for open banking
    Then I can proceed to the external provider website
    When I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should be navigated to the mobile browser for the designated webpage of the selected provider bank
    When I aggregate "<countOfAccounts>" of my external account from the external provider website
    Then I see deeplink is triggered after account is successfully aggregated
    And I should be redirected back to the NGA app
    And I should be on the home page
    And I should see ghost tile for external accounts load
    And I wait for ghost tile to disappear
    And I should see external accounts loaded
    When I navigate to external bank extBank error tile
    Then I should "see" notification on external bank extBank account tile
    And I should "see" notification message for "consent renewed" on external bank extBank account tile

  @manual @secondary
  #MOBS-439, MOBS-474
  Scenario: TC007 - Renew Consent - User able to renew consent when consent is about to expired via Add journey
    Given I reset the date close to extBank external account consent expiry date
    And I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    And I am shown the option to add external accounts
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBank from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the Redirecting screen for account aggregation
    And I should see the provider bank name extBank on the redirect screen
    And I should be navigated to the mobile browser for the designated webpage of the selected provider bank
    When I aggregate "<countOfAccounts>" of my external account from the external provider website
    Then I see deeplink is triggered after account is successfully aggregated
    And I should be redirected back to the NGA app
    And I should be on the home page
    And I should see ghost tile for external accounts load
    And I wait for ghost tile to disappear
    And I should see external accounts loaded
    When I navigate to external bank extBank error tile
    Then I should see Recently added flag on external bank extBank Credit account tile

  @manual @secondary
  Scenario: TC008 - Main Success Scenario: User able to renew consent when consent is already expired via renew consent button on expired tile
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    When I navigate to external bank extBankBarclays error tile
    And I select renew account sharing button on external bank extBankBarclays Credit account tile
    Then I should see the details for renew consent important information screen for extBankBarclays
    When I agree to renew my consent for open banking
    Then I can proceed to the external provider website
    When I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    And I select the Continue option in winback
    And I should be navigated to the mobile browser for the designated webpage of the selected provider bank
    When I aggregate "<countOfAccounts>" of my external account from the external provider website
    Then I see deeplink is triggered after account is successfully aggregated
    And I should be redirected back to the NGA app
    And I should be on the home page
    And I should see ghost tile for external accounts load
    And I wait for ghost tile to disappear
    And I should see external accounts loaded

  @manual @secondary
  Scenario: TC009 - Main Success Scenario: User able to renew consent when consent is already expired via Add journey
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankBarclays from the list
    Then I should see the details for renew consent important information screen for extBankBarclays
    When I agree to renew my consent for open banking
    Then I can proceed to the external provider website
    When I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    And I select the Continue option in winback
    And I should be navigated to the mobile browser for the designated webpage of the selected provider bank
    When I aggregate "<countOfAccounts>" of my external account from the external provider website
    Then I see deeplink is triggered after account is successfully aggregated
    And I should be redirected back to the NGA app
    And I should be on the home page
    And I should see ghost tile for external accounts load
    And I wait for ghost tile to disappear
    And I should see external accounts loaded

  @manual
  #MOBS-90, MOBS-19, MOBS-112, MOBS-257, MOBS-167, MOBS-322
  Scenario: TC010 – Exception Scenario: User should be able to able to update/remove the accounts for the already added External provider accounts
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    When I run the account aggregation journey
    And I aggregate "<Account1>" and "<Account2>" from the external provider website
    Then I see deeplink is triggered after account is successfully aggregated
    And I should be redirected back to the NGA app
    And I should be on the home page
    And I should see ghost tile for external accounts load
    And I wait for ghost tile to disappear
    And I should see external accounts loaded
    And I should see both the accounts added for the external provider on the homepage
    When I run the account aggregation journey
    And I aggregate "<Account1>" from the external provider website for the same provider again
    Then I see deeplink is triggered after account is successfully aggregated
    And I should be redirected back to the NGA app
    And I should be on the home page
    And I should see ghost tile for external accounts load
    And I wait for ghost tile to disappear
    And I should see external accounts loaded
    And I should not see "<Account2>" on the homepage
    And I should see "<Account1>" on the homepage

  @manual
  #MOBS-90, MOBS-19, MOBS-112, MOBS-257, MOBS-167, MOBS-322
  Scenario: TC011 – Exception Scenario: User should be able to able to add/update the other accounts for the already added External provider accounts
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    When I run the account aggregation journey
    And I aggregate "<Account1>" from the external provider website
    Then I see deeplink is triggered after account is successfully aggregated
    And I should be redirected back to the NGA app
    And I should be on the home page
    And I should see ghost tile for external accounts load
    And I wait for ghost tile to disappear
    And I should see external accounts loaded
    And I should see "<Account1>" on the homepage
    And I should not see "<Account2>" on the homepage
    When I run the account aggregation journey
    And I aggregate "<Account1>" and "<Account2>" from the external provider website for the same provider again
    Then I see deeplink is triggered after account is successfully aggregated
    And I should be redirected back to the NGA app
    And I should be on the home page
    And I should see ghost tile for external accounts load
    And I wait for ghost tile to disappear
    And I should see external accounts loaded
    And I should see "<Account1>"  and "<Account2>" on the homepage

  @manual
  #MOBS-90, MOBS-19, MOBS-83, MOBS-180
  Scenario Outline: TC012 - Exception Scenario: Verify the app auto logoff time should increase to 5 mins and user session is still active
    Given I am an enrolled "current" user logged into the app
    And I have set my app auto logout time to "<auto-logoff_time>"
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the Redirecting screen for account aggregation
    And I should be navigated to the mobile browser for the designated webpage of the selected provider bank
    When I have not completed the Open banking journey
    And I choose to come back to NGA app within "<comeback_time>"
    Then I should see an unsuccessful information text on redirect screen
    And I am given the option to navigate to home page
    When I navigate to home page
    Then I should be on the home page

    Examples:
      | auto-logoff_time | comeback_time |
      | immediate        | 5 mins        |
      | 1 min            | 5 mins        |
      | 2 min            | 5 mins        |
      | 5 mins           | 5 mins        |
      | 10 mins          | 10 mins       |

  @manual
  #MOBS-90, MOBS-19, MOBS-83, MOBS-180, MOBS-118, MOBS-241
  Scenario Outline: TC013 - Exception Scenario: Verify the app auto logoff time should increase to 5 mins and user session is expired
    Given I am an enrolled "current" user logged into the app
    And I have set my app auto logout time to "<auto-logoff_time>"
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the Redirecting screen for account aggregation
    And I should be navigated to the mobile browser for the designated webpage of the selected provider bank
    When I have not completed the Open banking journey
    And I choose to come back to NGA app within "<comeback_time>"
    Then I should see user logged out error screen

    Examples:
      | auto-logoff_time | comeback_time |
      | immediate        | 10 mins       |
      | 1 min            | 10 mins       |
      | 2 min            | 10 mins       |
      | 5 mins           | 10 mins       |
      | 10 mins          | 15 mins       |

  @manual
  #MOBS-118, MOBS-241
  Scenario: TC014 - Exception Scenario: User has completed aggregation journey and automatic deeplink is triggered back to NGA app when timeout has reached
    Given I am an enrolled "current" user logged into the app
    And I have set my app auto logout time to immediate or 1 mins or 2 mins or 5 mins
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the Redirecting screen for account aggregation
    And I should see the provider bank name extBankAIB on the redirect screen
    And I should be navigated to the mobile browser for the designated webpage of the selected provider bank
    And I aggregate my external account from the external provider website in more than 5 mins
    When I see deeplink is triggered after account is successfully aggregated
    Then I should be displayed the NGA app
    And I should see user logged out error screen

  #_____________________________________________________________________________________________________________________

  #   TECHNICAL VALIDATION SCENARIOS
  #_____________________________________________________________________________________________________________________

  @stub-open-banking-no-external-accounts
  #MOBS-84, MOBS-20
  Scenario: TC015 – Account Overview: User eligible for everyday offers should see the Open banking promo tile on Homepage just before the Everyday Offers tile
    Given I am an enrolled "current" user logged into the app
    Then I should see the Open Banking Promo tile before the every day offers tile

  @stub-open-banking-no-rewards
  #MOBS-84, MOBS-20
  Scenario: TC016 – Account Overview: User not eligible for everyday offers should be able to see Open banking promo tile at the end of homepage
    Given I am an enrolled "current" user logged into the app
    Then I am not shown the every day offers tile on home page
    And I should see the Open Banking Promo tile at the end of homepage

  @stub-open-banking-auto-load-external-accounts-failure
  #MOBS-482, MOBS-472
  Scenario: TC017 - Account Overview: User initiated View accounts journey and there is technical error while fetching accounts
    Given I am an enrolled "current" user logged into the app
    Then I should see the error banner on home page

  @stub-open-banking-get-providers-error
  #MOBS-68, MOBS-256, MOBS-71, MOBS-148
  Scenario: TC018 - Account Overview: User initiated View accounts journey and there is technical error at fetching list of providers
    Given I am an enrolled "current" user logged into the app
    When I proceed to add my external account
    Then I should see the error banner on home page

  @stub-open-banking-ob-consent-provided
  #MOBS-70, MOBS-144, MOBS-119, MOBS-145
  Scenario: TC019 – Account Overview: User who has already added external bank accounts should not see Recently added tag after 24 hours
    Given I am an enrolled "current" user logged into the app
    And I have added my external accounts more than 24 hours ago
    Then I should see external accounts loaded
    And I should not see Any flag on external bank extBankNTW Credit account tile
    And I should not see Any flag on external bank extBankNTW Savings account tile
    And I should not see Any flag on external bank extBankAIB Current account tile

  @stub-open-banking-balance-not-available
  #MOBS-792
  Scenario: TC020 – Account Overview: Verify recently added flag for recently added current, credit and savings accounts
    Given I am an enrolled "current" user logged into the app
    Then I should see external accounts loaded
    And I should see Recently added flag on external bank extBankBarclays Credit account tile
    And I should see Recently added flag on external bank extBankBarclays Savings account tile
    And I should see Recently added flag on external bank extBankHFX Current account tile

  @stub-open-banking-renewed-for-90-days
  #MOBS-792
  Scenario: TC021 – Account Overview: Verify renewed for 90 days flag for renewed current, credit and savings accounts
    Given I am an enrolled "current" user logged into the app
    Then I should see external accounts loaded
    And I should see Renewed for 90 days flag on external bank extBankBarclays Credit account tile
    And I should see Renewed for 90 days flag on external bank extBankBarclays Savings account tile
    And I should see Renewed for 90 days flag on external bank extBankAIB Current account tile

  @stub-open-banking-balance-not-available
  #MOBS-792
  Scenario: TC022 – Account Overview: Verify when balance not available on current, credit and savings accounts
    Given I am an enrolled "current" user logged into the app
    Then I should see external accounts loaded
    And I should see balance N/A on external bank extBankBarclays Credit account tile
    And I should see balance N/A on external bank extBankBarclays Savings account tile
    And I should see balance N/A on external bank extBankHFX Current account tile

  @stub-open-banking-error-account
  #MOBS-385, MOBS-287
  Scenario: TC023 – Account Overview: Verify error account tile when it fails to fetch account details for current, credit and savings accounts
    Given I am an enrolled "current" user logged into the app
    Then I should see external error accounts loaded
    And I should see full error tile for external bank extBankBarclays
    And I should see full error tile for external bank extBankNTW
    And I should see full error tile for external bank extBankHFX

  @stub-open-banking-expired-account
  Scenario: TC024 – Account Overview: Verify expired account tiles for current, credit and savings accounts
    Given I am an enrolled "current" user logged into the app
    Then I should see external expired accounts loaded
    And I should see expired error tile for external bank extBankBarclays
    And I should see expired error tile for external bank extBankNTW
    And I should see expired error tile for external bank extBankHFX

  @manual
  #MOBS-374, MOBS-377, MOBS-612, MOBS-590
  Scenario: TC025 - Account Overview: Verify the external accounts balance on homepage is refreshed after successful payment is made to external account
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    And I am shown the option to add external accounts
    When I successfully make a transfer to any account
    And I navigate to home page
    Then I should see external accounts balance is not refreshed
    When I successfully make a payment to my external aggregated account with the matching sort code and account number
    And I navigate to home page
    Then I should see external accounts balance is refreshed
    And I should see the updated account balance for the externally added account

  @stub-open-banking-no-external-accounts
  #MOBS-24, MOBS-25
  Scenario: TC026 – Open Banking Screen: User should see the Open Banking details
    Given I am an enrolled "current" user logged into the app
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    And I should see details for open banking
    And I can proceed to the secure and protected screen
    And I cannot go back to the previous page
    When I close the account aggregation screen
    Then I should be on the home page

  @stub-open-banking-no-external-accounts
  #MOBS-21, MOBS-27
  Scenario: TC027 – Secure and Protected Screen: User should see the Secure and protected details
    Given I am an enrolled "current" user logged into the app
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    And I should see details for secure and protected
    And I can go back to the previous page
    And I can proceed to the set up in 3 steps screen
    When I go back in the journey
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I close the account aggregation screen
    Then I should be on the home page

  @stub-open-banking-no-external-accounts
  #MOBS-653, MOBS-646
  Scenario: TC028 – Set up in 3 Steps Screen: User should see the Set up in 3 Steps screen when consent is not held
    Given I am an enrolled "current" user logged into the app
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    And I can go back to the previous page
    And I can proceed to the account provider screen
    When I go back in the journey
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I close the account aggregation screen
    Then I should be on the home page

  @stub-open-banking-ob-consent-provided
  #MOBS-653, MOBS-646
  Scenario: TC029 - Set up in 3 Steps Screen: User should see the Set up in 3 Steps screen via add journey when consent is held
    Given I am an enrolled "current" user logged into the app
    Then I should see external accounts loaded
    When I proceed to add my external account
    Then I should see the Set up in 3 steps screen for account aggregation
    And I should see details for set up in 3 steps
    And I cannot go back to the previous page
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I go back in the journey
    Then I should see the Set up in 3 steps screen for account aggregation

  @stub-open-banking-no-external-accounts @secondary
  #MOBS-16, MOBS-28, MOBS-718, MOBS-789
  Scenario: TC030 – Account Provider Screen: User should see the Provider list in Account Provider screen
    Given I am an enrolled "current" user logged into the app
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    And I should see all the external account provider names and logos
    And I should see footer information text
    And I can go back to the previous page
    When I go back in the journey
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I close the account aggregation screen
    Then I should be on the home page

  @stub-open-banking-no-external-accounts @secondary
  #MOBS-22, MOBS-89
  Scenario: TC031 – Important Information Screen: User should see the Open Banking T&Cs And Consent details
    Given I am an enrolled "current" user logged into the app
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    And I should see the details for important information
    And I should see the selected provider bank name extBankAIB
    When I have not agreed to the T&C for open banking
    And I have not agreed to the consent for open banking
    Then I cannot proceed to the external provider website
    When I agree to the T&C for open banking
    Then I cannot proceed to the external provider website
    When I disagree to the T&C for open banking
    And I agree to the consent for open banking
    Then I cannot proceed to the external provider website
    When I agree to the T&C and Consent for open banking
    Then I can proceed to the external provider website
    And I can go back to the previous page
    When I go back in the journey
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I close the account aggregation screen
    Then I should be on the home page

  @stub-open-banking-ob-consent-provided @secondary
  #MOBS-242, MOBS-105
  Scenario: TC032 – Important Information Screen: User already held consent with open banking and proceed to add external account
    Given I am an enrolled "current" user logged into the app
    Then I should see external accounts loaded
    When I proceed to add my external account
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    And I should see the selected provider bank name extBankAIB
    And I should not see checkbox for T&C for open banking
    When I have not agreed to the consent for open banking
    Then I cannot proceed to the external provider website
    When I agree to the consent for open banking
    Then I can proceed to the external provider website
    When I go back in the journey
    And I select an external provider bank extBankBOS from the list
    Then I should see the Important information screen for account aggregation
    And I should see the selected provider bank name extBankBOS
    And I should see checkbox for T&C for open banking
    When I agree to the T&C and Consent for open banking
    Then I can proceed to the external provider website

  @stub-open-banking-no-external-accounts
  #MOBS-68, MOBS-256
  Scenario: TC033 - Redirecting Screen: User should be logged out of the app in case of any un-handled error at redirection
    Given I am an enrolled "current" user logged into the app
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankM&S from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see user logged out error screen

  @stub-open-banking-redirect-screen-failure
  #MOBS-68, MOBS-256
  Scenario: TC034 - Redirecting Screen: Technical error while redirecting user to external browser screen
    Given I am an enrolled "current" user logged into the app
    Then I should see external accounts loaded
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the redirect error screen for redirect failure

  @stub-open-banking-no-external-accounts
  #MOBS-139, MOBS-140
  Scenario: TC035 – Consent Management - Settings: User should be able to see an entry point to remove external bank accounts from app settings
    Given I am an enrolled "current" user logged into the app
    When I navigate to settings page via more menu
    Then I am shown the option to Remove Provider
    And I should see remove provider options at the end of the page
    When I select Remove Provider tile
    Then I should be on Consent settings page

  @stub-open-banking-auto-load-external-accounts-success
  #MOBS-847, MOBS-848, MOBS-919, MOBS-940, MOBS-932, MOBS-941
  Scenario: TC036 – Consent Management: Verify Authorised external account on Consent settings screen
    Given I am an enrolled "current" user logged into the app
    When I navigate to settings page via more menu
    And I select Remove Provider tile
    Then I should be on Consent settings page
    And I should see the details for consent settings
    And I should see Authorised account for extBankBarclays on consent settings page
    And I should see Expires message for extBankBarclays account on consent settings page
    And I should see Manage button for extBankBarclays account on consent settings page
    When I select Manage button for extBankBarclays account on consent settings page
    Then I should see Consent settings pop up for extBankBarclays
    And I should see below options on Consent settings pop up
      | Renew  |
      | Remove |
    When I select Remove option on Consent settings pop up
    Then I should see remove alert on Consent settings page
    And I should see winback with Close and Remove option
    When I select the Close option in winback
    Then I should be on Consent settings page
    And I should see Authorised account for extBankBarclays on consent settings page
    And I should see Authorised account for extBankBarclays on consent settings page
    When I select Manage button for extBankBarclays account on consent settings page
    Then I should see Consent settings pop up for extBankBarclays
    When I select Remove option on Consent settings pop up
    And I select the Remove option in winback
    Then I wait for spinner to disappear
    And I should see Revoked account for extBankBarclays on consent settings page

  @stub-open-banking-auto-load-external-accounts-success
  #MOBS-847, MOBS-848, MOBS-919, MOBS-940
  Scenario: TC037 – Consent Management: Verify Expired external account on Consent settings screen
    Given I am an enrolled "current" user logged into the app
    When I navigate to settings page via more menu
    And I select Remove Provider tile
    Then I should be on Consent settings page
    And I should see Expired message for extBankBOS account on consent settings page
    And I should see Expired account for extBankBOS on consent settings page
    And I should see Manage button for extBankBOS account on consent settings page
    When I select Manage button for extBankBOS account on consent settings page
    Then I should see Consent settings pop up for extBankBOS
    And I should see below options on Consent settings pop up
      | Renew  |
      | Remove |
    When I select Remove option on Consent settings pop up
    And I select the Remove option in winback
    Then I wait for spinner to disappear

  @stub-open-banking-auto-load-external-accounts-success
  #MOBS-847, MOBS-848, MOBS-919, MOBS-940, MOBS-992, MOBS-996
  Scenario: TC038 – Consent Management: Verify Failed to fetch account details on Consent Management screen
    Given I am an enrolled "current" user logged into the app
    When I navigate to settings page via more menu
    And I select Remove Provider tile
    Then I should be on Consent settings page
    And I should see Failed account for extBankSantander on consent settings page

  @stub-open-banking-get-accounts-error
  #MOBS-993, MOBS-997, MOBS-932, MOBS-941
  Scenario: TC039 – Consent Management: Verify account should be removed even when get accounts is failing
    Given I am an enrolled "current" user logged into the app
    Then I should see the error banner on home page
    And I should not see external accounts loaded
    When I navigate to settings page via more menu
    And I select Remove Provider tile
    Then I should be on Consent settings page
    And I should see Authorised account for extBankBarclays on consent settings page
    And I should see Expires message for extBankBarclays account on consent settings page
    And I should see Manage button for extBankBarclays account on consent settings page
    And I should not see red error banner on the page
    When I select Manage button for extBankBarclays account on consent settings page
    And I select Remove option on Consent settings pop up
    And I select the Remove option in winback
    And I wait for spinner to disappear
    Then I should see red error banner on the page
    And I should see Revoked account for extBankBarclays on consent settings page

  @stub-open-banking-renewed-for-90-days
  #MOBS-161, MOBS-200, MOBS-162, MOBS-199
  Scenario: TC040 - View Transactions - External Account tile: User should be able to see the transactions for the externally added account
    Given I am an enrolled "current" user logged into the app
    Then I should see external accounts loaded
    When I navigate to external bank extBankBarclays Credit account tile
    And I select the extBankBarclays Credit external account tile
    Then I should see the transactions for the externally added account
    When I navigate to home page
    Then I should be on the home page
    When I navigate to external bank extBankBarclays Savings account tile
    And I select the extBankBarclays Savings external account tile
    Then I should see the transactions for the externally added account
    When I navigate to home page
    Then I should be on the home page
    When I navigate to external bank extBankAIB Current account tile
    And I select the extBankAIB Current external account tile
    Then I should see the transactions for the externally added account

  @manual
  #MOBS-374, MOBS-377, MOBS-612, MOBS-590
  Scenario: TC041 - View Transactions: Verify the external accounts balance on homepage is refreshed after successful payment is made to external account
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I navigate to external bank extBank Current account tile
    And I select the extBank Current external account tile
    And I should see the transactions for the externally added account
    When I successfully make a transfer to any account
    And I close the payment and transfers modal window
    Then I should see transactions page is not refreshed
    When I successfully make a payment to my external aggregated account with the matching sort code and account number
    And I close the payment and transfers modal window
    Then I should see the transactions for the externally added account
    And I should see transactions page is not refreshed
    When I navigate to home page
    Then I should see the updated account balance for the externally added account
    When I select the extBank Current external account tile
    Then I should see the transactions for the externally added account
    And I should see the updated account balance and transaction for the externally added account

  @stub-arrangements-loan-only
  #MOBS-1032, MOBS-1034
  Scenario: TC042 - View Transactions - Deeplink: User should be able to see the transactions for the externally added account
    Given I am an enrolled "current" user logged into the app
    Then I should see external accounts loaded
    When I select the extBankNTW Credit external account tile
    Then I should see the transactions for the externally added account
    When I select the failure deep link for renewing consent
    Then I should be on the home page
    When I select the extBankNTW Credit external account tile
    Then I should see the transactions for the externally added account
    When I select the success deep link for renewing consent
    Then I should see the details for renew consent important information screen for extBankNTW

  @stub-open-banking-view-transactions-error
  #MOBS-1032, MOBS-1034
  Scenario: TC043 - View Transactions - Error: User should be logged out when view transaction error occurs
    Given I am an enrolled "current" user logged into the app
    Then I should see external accounts loaded
    When I select the extBankNTW Credit external account tile
    Then I should see the logged off error screen

  @stub-open-banking-expired-account
  #MOBS-425, MOBS-453, MOBS-426, MOBS-454, MOBS-653, MOBS-646, MOBS-793
  Scenario: TC044 - Renew Consent: User shown and clicks renew consent CTA On expired external account error tile
    Given I am an enrolled "current" user logged into the app
    Then I should see external expired accounts loaded
    And I should see expired error tile for external bank extBankBarclays
    And I am shown renew account sharing button on external account tile for extBankBarclays
    When I select renew account sharing button on external bank extBankBarclays Credit account tile
    Then I should not see the Set up in 3 steps screen for account aggregation
    And I should see the Important information screen for account aggregation
    And I should see the details for renew consent important information screen for extBankBarclays
    And I have not agreed to renew my consent on important information screen
    And I cannot proceed to the external provider website
    When I agree to renew my consent for open banking
    Then I can proceed to the external provider website
    And I cannot go back to the previous page
    When I close the account aggregation screen
    Then I should be on the home page

  @stub-open-banking-expired-account
  #MOBS-425, MOBS-453, MOBS-927, MOBS-942
  Scenario: TC045 - Renew Consent: User shown and clicks settings icon on expired external account error tile
    Given I am an enrolled "current" user logged into the app
    Then I should see external expired accounts loaded
    And I should see expired error tile for external bank extBankBarclays
    When I select settings button on external bank extBankBarclays Current account tile
    Then I should be on Consent settings page

  @stub-open-banking-no-external-accounts
  #MOBS-476, MOBS-532
  Scenario: TC046 - Renew Consent: User does not complete renew consent journey for expired and comes back to the app
    Given I am an enrolled "current" user logged into the app
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    And I should not see the Secure and protected screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankBarclays from the list
    Then I should see the Important information screen for account aggregation
    And I should see the details for renew consent important information screen for extBankBarclays
    When I agree to renew my consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    And I return back to the nga app from mobile browser without completing the journey
    Then I should see the redirect error screen for incomplete journey
    And I see the incomplete renew consent journey message on redirect screen
    And I should see home button on redirect screen
    When I select home on redirect screen
    Then I should be on the home page

  @stub-open-banking-expired-account
  #MOBS-476, MOBS-532
  Scenario: TC047 - Renew Consent: User does not complete renew consent journey for expired and switches to app
    Given I am an enrolled "current" user logged into the app
    Then I should see external expired accounts loaded
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    And I should not see the Secure and protected screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankHFX from the list
    Then I should see the Important information screen for account aggregation
    And I should see the details for renew consent important information screen for extBankHFX
    When I agree to renew my consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    And I manually switch back to the nga app from mobile browser without completing the journey
    Then I see the incomplete renew consent journey message on redirect screen
    And I should see home button on redirect screen
    When I close the account aggregation screen
    Then I should be on the home page

  @stub-open-banking-ob-consent-provided
  #MOBS-477, MOBS-533, MOBS-455, MOBS-427
  Scenario: TC048 - Renew Consent: User adds provider when consent is already expired
    Given I am an enrolled "current" user logged into the app
    Then I should see external accounts loaded
    When I proceed to add my external account
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankBarclays from the list
    Then I should see the details for renew consent important information screen for extBankBarclays
    And I have not agreed to renew my consent on important information screen
    And I cannot proceed to the external provider website
    When I agree to renew my consent for open banking
    Then I can proceed to the external provider website
    When I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Cancel option in winback
    Then I should see the details for renew consent important information screen for extBankBarclays

  @stub-open-banking-auto-load-external-accounts-success
  #MOBS-919, MOBS-940, MOBS-920, MOBS-933
  Scenario: TC049- Renew Consent: User adds provider when consent is already expired
    Given I am an enrolled "current" user logged into the app
    When I navigate to settings page via more menu
    And I select Remove Provider tile
    Then I should be on Consent settings page
    And I should see Expired message for extBankBOS account on consent settings page
    And I should see Expired account for extBankBOS on consent settings page
    And I should see Manage button for extBankBOS account on consent settings page
    When I select Manage button for extBankBOS account on consent settings page
    Then I should see Consent settings pop up for extBankBOS
    And I should see below options on Consent settings pop up
      | Renew  |
      | Remove |
    When I select Renew option on Consent settings pop up
    Then I should see the details for renew consent important information screen for extBankBOS

  @stub-open-banking-consent-no-consent
  #MOBS-473, MOBS-531
  Scenario: TC050 - Auto Load: User should not see ghost tile when user does not have any external accounts aggregated
    Given I am an enrolled "current" user logged into the app
    Then I should not see ghost tile for external accounts load
    And I am shown Open Banking promo tile
    When I navigate to settings page via more menu
    And I select Remove Provider tile
    Then I should be on Consent settings page
    And I should not see any external accounts on the consent settings page

  @stub-open-banking-consent-api-failed
  #MOBS-473, MOBS-531
  Scenario: TC051 - Auto Load: User should not see ghost tile when consent api failed to fetch consent status
    Given I am an enrolled "current" user logged into the app
    Then I should not see ghost tile for external accounts load
    And I am shown Open Banking promo tile
    When I navigate to settings page via more menu
    And I select Remove Provider tile
    Then I should be on Consent settings page
    And I should not see any external accounts on the consent settings page

  @manual
  #MOBS-473, MOBS-531
  Scenario: TC052 - Auto Load: User should see ghost tile when navigating back from browser journey
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the Redirecting screen for account aggregation
    And I should see the provider bank name extBankAIB on the redirect screen
    And I should be navigated to the mobile browser for the designated webpage of the selected provider bank
    When I aggregate my external account from the external provider website
    Then I see deeplink is triggered after account is successfully aggregated
    And I should be redirected back to the NGA app
    And I should be on the home page
    And I should see ghost tile for external accounts load

  # Known defect in iOS (MOBS-778). Marking as android only.
  @stub-open-banking-auto-scroll-to-loading-tile @android
  #MOBS-621, MOBS-610
  Scenario: TC053 - Auto Scroll: Auto scroll to ghost tile when deeplinks trigger
    Given I am an enrolled "current" user logged into the app
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    And I trigger the deeplink url
    Then I should see ghost tile for external accounts load
    When I wait for ghost tile to disappear
    Then I should see external accounts loaded

  @stub-open-banking-offshore-customer @lds
  #MOBS-501, MOBS-631
  Scenario: TC054 - Offshore Customer: Verify account aggregation journey not visible to Offshore customers
    Given I am an enrolled "current" user logged into the app
    Then I should not see ghost tile for external accounts load
    And I should not see external accounts loaded
    And I am not shown Open Banking promo tile
    When I navigate to settings page from home page
    Then I am not shown the option to Remove Provider

  @stub-open-banking-onshore-customer @lds
  #MOBS-501, MOBS-631
  Scenario: TC055 - Onshore Customer: Verify account aggregation journey visible to Onshore customers
    Given I am an enrolled "current" user logged into the app
    Then I should see external accounts loaded
    And I am shown Open Banking promo tile
    When I navigate to settings page from home page
    Then I am shown the option to Remove Provider

  @manual @secondary
  #MOBS-557, MOBS-559, MOBS-859
  Scenario: TC056 – Aggregation App to App: User should Add external accounts successfully via Universal Links and should see Ghost tile on clicking Close button
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the Redirecting screen for account aggregation
    And I should see the provider bank name extBankAIB on the redirect screen
    And I should be navigated to the mobile browser/app for the selected provider bank
    When I aggregate "<countOfAccounts>" of my external account from the external provider website
    Then I see universal link is triggered after account is successfully aggregated
    And I should be redirected back to the NGA app
    And I should see loading indicator in the app
    And I should see aggregation successful native page
    When I select the close button at the bottom of the page
    Then I should be on the home page
    And I should see ghost tile for external accounts load
    When I wait for ghost tile to disappear
    Then I should see external accounts loaded

  @manual @secondary
  #MOBS-557, MOBS-559, MOBS-859
  Scenario: TC057 – Aggregation App to App: User should Add external accounts successfully via Universal Links and should see Ghost tile on clicking Cross button
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the Redirecting screen for account aggregation
    And I should see the provider bank name extBankAIB on the redirect screen
    And I should be navigated to the mobile browser/app for the selected provider bank
    When I aggregate "<countOfAccounts>" of my external account from the external provider website
    Then I see universal link is triggered after account is successfully aggregated
    And I should be redirected back to the NGA app
    And I should see loading indicator in the app
    And I should see aggregation successful native page
    When I close the account aggregation screen
    Then I should be on the home page
    And I should see ghost tile for external accounts load
    When I wait for ghost tile to disappear
    Then I should see external accounts loaded

  @manual @secondary
  #MOBS-557, MOBS-559, MOBS-859
  Scenario: TC058 – Aggregation App to App: User should see aggregation not successful screen via Universal Links and no action takes place on clicking close button
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the Redirecting screen for account aggregation
    And I should see the provider bank name extBankAIB on the redirect screen
    And I should be navigated to the mobile browser/app for the selected provider bank
    When I aggregate "<countOfAccounts>" of my external account from the external provider website
    And I see that the account aggregation is not successful
    Then I see universal link is triggered after account aggregation fails
    And I should be redirected back to the NGA app
    And I should see loading indicator in the app
    And I should see aggregation not successful error page
    When I select the close button at the bottom of the page
    Then I should be on the home page
    And I should not see ghost tile for external accounts load
    And I should see external accounts loaded

  @manual @secondary
  #MOBS-557, MOBS-559, MOBS-859
  Scenario: TC059 – Aggregation App to App: User should see aggregation not successful screen via Universal Links and no action takes place on clicking cross icon
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the Redirecting screen for account aggregation
    And I should see the provider bank name extBankAIB on the redirect screen
    And I should be navigated to the mobile browser/app for the selected provider bank
    When I aggregate "<countOfAccounts>" of my external account from the external provider website
    And I see that the account aggregation is not successful
    Then I see universal link is triggered after account aggregation fails
    And I should be redirected back to the NGA app
    And I should see loading indicator in the app
    And I should see aggregation not successful error page
    When I close the account aggregation screen
    Then I should be on the home page
    And I should not see ghost tile for external accounts load
    And I should see external accounts loaded

  @stub-open-banking-add-external-account-lead
  #MOBS-1032, MOBS-1034
  Scenario: TC060 - Account Aggregation Lead: User should be able to see the Account aggregation journey on clicking the button in the Lead
    Given I submit "correct" MI as "current" user on the MI page
    And an interstitial lead is displayed
    When I select the add external account link in the lead
    Then I should see the Open Banking screen for account aggregation
