@team-panm @nga3 @rnga @stub-analytics-consent-unchanged @mbnaSuite
Feature: Reset Data Consent
  In order to ensure that my device does not have my privary settings saved
  As a customer
  I want my privacy setting cleared when i reset my app

  #------------------------------------------ Main Success scenario---------------------------#

  #MOB3-12743/12843
  @primary
  Scenario: TC_001 Main Success Scenario : App reset to clear Privacy Management settings
    Given I submit "correct" MI as "valid" user on the MI page
    When I have set my privacy setting already for the app
    And I reset the app
    Then I should be on the logout page with reset app complete message
    When I select back to logon button on the logout page
    And I am a current user on the MI page
    And I enter correct MI
    Then The Privacy Management settings on that device must be cleared

  Scenario: TC_002 Main Success Scenario : Uninstall App to clear Privacy Management settings
    Given I reinstall the app
    When I relaunch the app
    And I submit "correct" MI as "valid" user on the MI page
    Then The Privacy Management settings on that device must be cleared
