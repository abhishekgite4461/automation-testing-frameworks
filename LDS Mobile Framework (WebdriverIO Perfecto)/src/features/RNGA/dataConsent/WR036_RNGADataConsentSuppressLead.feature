@team-panm @nga3 @rnga @primary @stub-analytics-consent-unset-bigprompt
Feature: Suppress Leads when Data Consent interstitial to select all consents
  In order to provide consent for my cookie usage
  As a customer
  I must be prompted to make a selection

# Preconditions for Data Consent
# Data Consent Switch is ON
# User has not selected the Data Consent on the device previously
# User is not shown Whats New screen
# User is not shown Enrolment journey
# User is not shown Touch ID registration prompt
# User is not shown Touch ID change prompt
# User is not performing any App sign journey

  #------------------------------------------ Main Success scenario---------------------------#

  #MOB3-12742/12840
  Scenario: TC_001 Suppress Leads when Data Consent interstitial is available
    Given I submit "correct" MI as "valid" user on the MI page
    Then Only the data consent interstitial is displayed
