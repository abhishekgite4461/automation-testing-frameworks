@team-panm @nga3 @rnga @stub-analytics-consent-unset-unenrolled @mbnaSuite
Feature: Manage Data Consent from my profile
  In order to manage my data consents on the device
  As a customer
  I must be provided with the options to make changes to my consent

  #MOB3-13398, MOB3-13397, 13395, 13396
  Background:
    Given I am an unenrolled valid user on the home page
    And I navigate to profile page via more menu

  #------------------------------------------ Main Success scenario---------------------------#

  @primary
  Scenario: TC_001 Main Success Scenario : Main Success_Confirmation of cookie consents
    Given The manage data consent link is displayed
    When I navigate to manage data consent page
    And I make a new selection for my consents
    And I confirm my selection on the data consent page
    Then I should be on the your personal details page

  @primary
  Scenario: TC_002 Main Success Scenario : Confirmation of cookie consents
    Given The manage data consent link is displayed
    When I navigate to manage data consent page
    And I have not made a selection for my consent
    Then I am taken to the manage data consent page with a back button visible
    And I make a new selection for my consents
    And The confirm option is then enabled
    When I confirm my selection on the data consent page
    Then I should be on the your personal details page

  @secondary
  Scenario: TC_003 - Verify the confirm button is disabled if no consent is selected
    Given I navigate to manage data consent page
    When I have not made a selection for my consent
    Then The confirm button should be disabled

  @android @secondary
  Scenario: TC_004 - Verify that the device back button is not suppressed
    Given I navigate to manage data consent page
    When I select back key on the device
    Then I should be on the your personal details page

  @manual
  Scenario: TC_005 - Setting tag value for data consent
    Given I navigate to the manage data consent page
    When I make a selection for my consents
    And I confirm my selection on the data consent page
    Then The cookies setting stored in the app must be 1 for all the options
  #Note: The value of the tag would be set to 1 once accepted and 0 if not set or rejected.. This is manual testing only
