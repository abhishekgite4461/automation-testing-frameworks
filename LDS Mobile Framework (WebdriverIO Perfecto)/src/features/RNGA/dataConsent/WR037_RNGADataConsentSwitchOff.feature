@team-panm @nga3 @rnga @primary @sw-data-consent-off @stub-analytics-consent-unset @mbnaSuite
Feature: Data Consent prompt switch is turned off
  In order to provide consent for my cookie usage
  As a customer
  I must be prompted to make a selection

# Preconditions for Data Consent
# Data Consent Switch is OFF

  #------------------------------------------ Main Success scenario---------------------------#

  #MOB3-12743/12843
  Scenario: TC_001 - Switch Off - Data Consent Interstitial and Your Profile page entry point
    Given I submit "correct" MI as "valid" user on the MI page
    Then I should not see the data consent interstitial
    When I navigate to profile page via more menu
    Then The link to the data consent isn't displayed
