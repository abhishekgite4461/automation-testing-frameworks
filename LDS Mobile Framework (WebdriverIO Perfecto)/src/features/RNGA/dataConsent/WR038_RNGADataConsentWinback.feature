@team-panm @nga3 @rnga @secondary @stub-enrolled-typical @mbnaSuite
Feature: winback customers to privacy management page from native entry
  As a customer
  I want to be shown a winback
  So that I can decide if I want to continue with the privacy management setting

  Background:
    Given I am an enrolled "valid" user logged into the app
    And I navigate to profile page via more menu
    And I navigate to manage data consent page

  #------------------------------------------ Variation scenario---------------------------#

  Scenario: TC_001 - No win back via back button if no changes are made
    Given I navigate back
    Then I should be on the your personal details page

  @android
  Scenario: TC_002 No win back via device back button if no changes are made
    Given I select back key on the device
    Then I should be on the your personal details page

  Scenario: TC_003 - No win back via bottom navigation bar if no changes are made
    Given I select support tab from bottom navigation bar
    Then I should be on the support hub page

  Scenario: TC_004 - native back button triggers a Winback prompt
    Given I toggle on the marketingPrefs consent option
    When I navigate back
    Then I see a Winback option on the data consent page

  @android
  Scenario: TC_005 - device back button triggers a Winback prompt
    Given I toggle on the performancePrefs consent option
    When I select back key on the device
    Then I see a Winback option on the data consent page

  @pending
  Scenario: TC_006 - bottom navigation bar triggers a Winback prompt
    Given I toggle on the marketingPrefs consent option
    When I select apply tab from bottom navigation bar
    Then I see a Winback option on the data consent page
  #Note: This is yet to be resolved by the redesign team

  Scenario: TC_007 Win back to preferences hub after global back button
    Given I toggle on the performancePrefs consent option
    And I navigate back
    And I see a Winback option on the data consent page
    When I opt to stay on the data consent page
    Then I should remain on the manage data consent page

  @android
  Scenario: TC_008 Win back to preferences hub after device back button
    Given I toggle on the performancePrefs consent option
    And I select back key on the device
    And I see a Winback option on the data consent page
    When I opt to stay on the data consent page
    Then I should remain on the manage data consent page

  @pending
  Scenario: TC_009 Win back to preferences after selecting call us on bottom navigation bar
    Given I toggle on the performancePrefs consent option
    And I select support tab from bottom navigation bar
    And I see a Winback option on the data consent page
    When I opt to stay on the data consent page
    Then I should remain on the manage data consent page
  #Note: This is yet to be resolved by the redesign team

  Scenario: TC_010 decline win back after after global back button
    Given I toggle on the performancePrefs consent option
    And I navigate back
    And I see a Winback option on the data consent page
    When I opt to leave the data consent page
    Then I should be on the your personal details page

  @android
  Scenario: TC_011 decline win back after using device back button
    Given I toggle on the performancePrefs consent option
    And I select back key on the device
    And I see a Winback option on the data consent page
    When I opt to leave the data consent page
    Then I should be on the your personal details page

  @pending
  Scenario: TC_012 decline win back after selecting call us on bottom navigation bar
    Given I toggle on the performancePrefs consent option
    And I select support tab from bottom navigation bar
    And I see a Winback option on the data consent page
    When I opt to leave the data consent page
    Then I should be on the call us home page
  #Note: This is yet to be resolved by the redesign team

  Scenario: TC_013 - Display winback after clicking on interstitial data management cookie policy link
    When I select the cookies policy link
    Then I see an external Winback option on the data consent page

  Scenario: TC_014 Win back to preferences after selecting cookie policy link
    Given I toggle on the performancePrefs consent option
    And I select the cookies policy link
    And I see an external Winback option on the data consent page
    When I opt to stay on the data consent page
    Then I should remain on the manage data consent page

  @instrument
  Scenario: TC_015 decline win back after selecting cookie policy link
    Given I toggle on the performancePrefs consent option
    And I select the cookies policy link
    And I see an external Winback option on the data consent page
    When I opt to leave the data consent page
    Then I must be taken out of my journey to external page
