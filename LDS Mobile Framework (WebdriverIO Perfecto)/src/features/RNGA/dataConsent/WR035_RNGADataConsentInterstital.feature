@team-panm @nga3 @rnga @stub-analytics-consent-unset @instrument @mbnaSuite
Feature: Create Native Interstitial page to select all consents
  In order to provide consent for my cookie usage
  As a customer
  I must be prompted to make a selection

# Preconditions for Data Consent
# Data Consent Switch is ON
# User has not selected the Data Consent on the device previously
# User is not shown Whats New screen
# User is not shown Enrolment journey
# User is not shown Touch ID registration prompt
# User is not shown Touch ID change prompt
# User is not performing any App sign journey

  #MOB3-13393/13394/13396/13395
  Background:
    Given I submit "correct" MI as "valid" user on the MI page

  #------------------------------------------ Main Success scenario---------------------------#

  @primary
  Scenario: TC001 Main Success Scenario : Display Prompt for consent
    Given I have not made a selection for my cookie usage already on that device
    And I see a native data consent interstitial page without a back button and global menu on the header
    And I see two options for selection
    When I select the option to accept all consents
    Then I should be on the home page

  @primary
  Scenario: TC002 Main Success Scenario : Display Prompt for consent
    Given I have not made a selection for my cookie usage already on that device
    And I see a native data consent interstitial page without a back button and global menu on the header
    And I see two options for selection
    And I select the option to manage consent
    When I should be taken to the my customize consents page
    And I make a new selection for my consents
    And The confirm option is then enabled
    And I confirm my selection on the data consent page
    Then I should be on the home page

  #------------------------------------------ Variation scenario---------------------------#

  @manual
  Scenario: TC_003 - Setting tag value on the native data consent interstitial page
    Given I see a native data consent interstitial page
    When I select the option to accept all consents
    Then The cookies setting stored in the app must be 1 for all the options
   #Note: The value of the tag would be set to 1 once accepted and 0 if not set or rejected.. This is manual testing only

  @android @secondary
  Scenario: TC_004 Verify that the device Back button is suppressed
    Given I see a native data consent interstitial page
    When I select back key on the device
    Then No action must be taken on the interstitial page

  @secondary
  Scenario: TC_005 - Display winback after clicking on cookie policy link
    Given I see a native data consent interstitial page
    When I select the cookies policy link on the interstitial page
    Then I see an external Winback option on the data consent page

  @secondary
  Scenario: TC_006 Winback to preferences after selecting cookie policy link
    Given I see a native data consent interstitial page
    And I select the cookies policy link on the interstitial page
    And I see an external Winback option on the data consent page
    When I opt to stay on the data consent page
    Then I should remain on the native data consent interstitial page

  @secondary
  Scenario: TC_007 decline win back after selecting cookie policy link
    Given I see a native data consent interstitial page
    And I select the cookies policy link on the interstitial page
    And I see an external Winback option on the data consent page
    When I opt to leave the data consent page
    Then I must be taken out of my journey to external page

  @android @secondary
  Scenario: TC_008 Device back button disabled
    Given I am on the manage consent page
    When I select back key on the device
    Then No action must be taken on the manage consent page

  @secondary
  Scenario: TC_009 Verify that the confirm button is disabled if no selection is made
    Given I am on the manage consent page
    When I have not made a selection for all my options
    Then The confirm button should be disabled

  @manual
  Scenario: TC_010 - Setting tag value on the manage data consent page
    Given I am on the manage consent page
    When I select the option to accept all consents
    Then The cookies setting stored in the app must be 1 for all the options
   #Note: The value of the tag would be set to 1 once accepted and 0 if not set or rejected.. This is manual testing only

  @secondary
  Scenario: TC_011 - Display winback after clicking on cookie policy link
    Given I am on the manage consent page
    When I select the cookies policy link
    Then I see an external Winback option on the data consent page

  @secondary
  Scenario: TC_012 Winback to preferences after selecting cookie policy link
    Given I am on the manage consent page
    And I select the cookies policy link
    And I see an external Winback option on the data consent page
    When I opt to stay on the data consent page
    Then I should remain on the manage consent interstitial page

  @secondary
  Scenario: TC_013 decline win back after selecting cookie policy link
    Given I am on the manage consent page
    And I select the cookies policy link
    And I see an external Winback option on the data consent page
    When I opt to leave the data consent page
    Then I must be taken out of my journey to external page
