@team-uat @rnga @nga3 @environmentOnly @mbna @ff-webview-debug-on
Feature: UAT Regression MBNA

  @uatDaily
  Scenario Outline: MBNA Full IB Registration
    Given I am a ibreg_eia_success user
    When I navigate to the login screen
    And I complete EIA registration for Internet Banking
    And I choose to receive EIA call on <number>
    And I choose call me now option
    And I capture and enter the 4 digit pin for eia call
    Then I should see the success Message as "<HC-201>" on the IB Registration Success page
    And I should see the footer Message as "<HC-203>" on the IB Registration Success page
    When I select continue on the IB Registration Success page
    And I choose my security auto logout settings
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page
    Examples:
      | number | HC-201                                                        | HC-203                              |
      | Work   | Only you can access your account from the app on this device. | password and memorable information. |

  @uatDaily
  Scenario Outline: MBNA User registration with EIA
    Given I am a mbna user
    When I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I choose to receive EIA call on <number>
    And I choose call me now option
    And I capture and enter the 4 digit pin for eia call
    And I select continue button on the enrolment confirmation page
    And I choose my security auto logout settings
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page
    Examples:
      | number |
      | Work   |

  @uatDaily
  Scenario: MBNA Log off app via more menu
    Given I am a valid user on the home page
    When I navigate to logoff via more menu
    Then I should be on the logout page

  @uatDaily
  Scenario Outline: MBNA Single Credit Card Account Overview
    Given I am a valid user on the home page
    And bottom navigation bar is displayed
    Then I should see the below options in the bottom nav
      | optionsInBottomNav |
      | home               |
      | apply              |
      | payAndTransfer     |
      | support            |
      | more               |
    When I navigate and validate the options in the <creditCard> account in below table in the home page
      | accountFields            |
      | creditCardAccountNumber  |
      | creditCardAccountBalance |
      | availableCredit          |
      | minimumPayment           |
      | statementBalance         |
      | dueDate                  |
    And I should see the below options in the account overview screen
      |optionsInAccountOverviewScreen|
      |payCreditCard                 |
      |manageCreditLimitAction       |
      |viewTransactions              |
      |cardManagement                |
      |pdfStatement                  |
    And I select the action menu of <creditCard> account
    Then I should see the below options in the action menu of the <creditCard> account
      | optionsInActionMenu        |
      | payCreditCardOption        |
      | balanceMoneyTransferOption |
      | manageCreditLimitOption    |
      | cardManagementOption       |
      | pdfStatementsOption        |
    And I close the action menu in the home page
    And bottom navigation bar is displayed
    Examples:
      | creditCard      |
      | firstCreditCard |

  @uatDaily
  Scenario Outline: MBNA Multiple Credit Card Account Overview
    Given I am a valid user on the home page
    And bottom navigation bar is displayed
    Then I should see the below options in the bottom nav
      | optionsInBottomNav |
      | home               |
      | apply              |
      | payAndTransfer     |
      | support            |
      | more               |
    When I navigate and validate the options in the <creditCard> account in below table in the home page
      | accountFields            |
      | creditCardAccountNumber  |
      | creditCardAccountBalance |
      | availableCredit          |
      | minimumPayment           |
      | statementBalance         |
      | dueDate                  |
    And I select the action menu of <creditCard> account
    Then I should see the below options in the action menu of the <creditCard> account
      | optionsInActionMenu        |
      | viewTransactionsOption     |
      | payCreditCardOption        |
      | balanceMoneyTransferOption |
      | manageCreditLimitOption    |
      | cardManagementOption       |
      | pdfStatementsOption        |
    And I close the action menu in the home page
    And bottom navigation bar is displayed
    Examples:
      | creditCard       |
      | firstCreditCard  |
      | secondCreditCard |

  @uatDaily
  Scenario Outline: MBNA In App search
    Given I am a valid user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the <search> link as search result
    When I tap on the <search> link
    Then I should be on <search> page as per the searched keyword
    Examples:
      | keyword     | search                    |
      | post		| change Your Address       |
      | change      | change Your Phone Number  |

  @uatDaily
  Scenario: MBNA Validate Call Us page details
    Given I am a valid user on the home page
    When I navigate to call us home page
    Then I should be on the call us home page
    And I should be able to view these call us tiles
      | callUsTiles           |
      | personalAccountsTile  |
      | newProductEnquireTile |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should see security and travel emergency label
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
    When I select the Personal accounts tile on call us home page overlay
    And I select the Credit account tile on your accounts page
    Then I should be on the secure call page
    When I navigate back
    And I navigate back
    And I select new product enquiry tile on call us home page
    And I select individual sales option on new product page and validate callus page
      | salesOptions     |
      | newCreditAccount |
    And I navigate back
    And I select the Internet banking tile on call us home page overlay
    Then I should be on the secure call page
    When I navigate back
    And I select the Other banking query tile on call us home page overlay
    Then I should be on the secure call page
    When I navigate back
    And I select the Suspected fraud tile on call us home page overlay
    Then I should be on the secure call page
    When I navigate back
    And I select the Lost or stolen cards  tile on call us home page overlay
    Then I should be on the secure call page
    When I navigate back
    And I select the Emergency cash abroad tile on call us home page overlay
    Then I should be on the secure call page

  @uatDaily
  Scenario: MBNA ATM Finder
    Given I am a mbna user on the home page
    When I select support tab from bottom navigation bar
    And I am on the atm and branch finder page
    Then I should not see search branch tile option
    And I am displayed the following search options
      | searchOption      |
      | findNearbyAtmTile |
    And I am displayed a note message stating clicking search option links take you outside the app

  @uatDaily
  Scenario: MBNA Setting page validations
    Given I am a mbna user on the home page
    When I navigate to settings page from home page
    And I select your personal details option in settings page
    Then I should be on the your personal details page
    When I navigate back
    And I select the how we contact you option from the settings page
    Then I should be shown the screen with following options
      | howWeContactYouOptions |
      | onlinePaperPreference  |
      | marketingPreference    |
    When I navigate back
    And I select on Security settings link on settings page
    Then I should see the security settings page
    And I should see the below options in the security settings page
      | securitySettingsOptions                |
      | securityDetailsUserIDLabel             |
      | securityDetailsDeviceTypeLabel         |
      | securityDetailsDeviceNameLabel         |
      | securityDetailsAppVersionLabel         |
      | securityDetailsAutoLogOffButton        |
      | securityDetailsForgottenPasswordButton |
    When I navigate back
    And I select the reset app option from the settings page
    And I select reset app button from the reset app page
    And I select cancel button from the reset app confirmation pop up
    Then I should be on the reset app Page
    When I navigate back
    And I navigate to the legal info option from the settings page
    Then I am on the legal information page

  @uatDaily
  Scenario: MBNA User profile Validation
    Given I am a valid user on the home page
    When I navigate to profile page via more menu
    And I should be shown the list of personal details
      | personalDetail |
      | Full Name      |
      | User ID        |
      | Mobile Number  |
      | Home Number    |
      | Work Number    |
      | Address        |
      | Postcode       |
      | Email address  |
    And I navigate back
    And I select more menu
    And I select settings from more menu
    And I select your personal details option in settings page
    And I should be shown the list of personal details
      | personalDetail |
      | Full Name      |
      | User ID        |
      | Mobile Number  |
      | Home Number    |
      | Work Number    |
      | Address        |
      | Postcode       |
      | Email address  |
    And The manage data consent link is displayed
    Then I navigate to manage data consent page
    And I make a new selection for my consents
    And I confirm my selection on the data consent page
    And I should be on the your personal details page
    And I navigate back

  @uatDaily
  Scenario: MBNA Legal information All tab validations
    Given I am a mbna user on the home page
    When I navigate to settings page from home page
    And I navigate to the legal info option from the settings page
    And I select the legal and privacy link
    And I navigate back
    And I select the cookie use and permissions link
    And I navigate back
    And I select the third party acknowledgements link
    And I navigate back
    Then I am on the legal information page

  @uatDaily @iOSObjectTreeOpt
  Scenario: MBNA View Single Credit Card statement
    Given I am a mbna user on the home page
    When I select mbna account tile from home page
    And I swipe to previous 3 months transactions on the account statement page
    And I should see credit transaction with below details in credit card statements
      | detail                      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
      | firstCreditIndicator        |
    And I select payment Received on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | date                    |
      | vtdCloseButton          |
      | vtdAmount               |
      | unsureTransactionButton |
      | creditCardText          |
      | payeeField              |
      | cardEndingNumber        |
      | dateOfTransaction       |

  @uatDaily @iOSObjectTreeOpt
  Scenario Outline: MBNA View Multiple Credit Card statement
    Given I am a valid user on the home page
    When I select <creditCard> account tile from home page
    And I swipe to previous 3 months transactions on the account statement page
    And I should see credit transaction with below details in credit card statements
      | detail                      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
      | firstCreditIndicator        |
    And I select payment Received on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | date                    |
      | vtdCloseButton          |
      | vtdAmount               |
      | unsureTransactionButton |
      | creditCardText          |
      | payeeField              |
      | cardEndingNumber        |
      | dateOfTransaction       |
    Examples:
      | creditCard       |
      | firstCreditCard  |
      | secondCreditCard |

  @uatDaily
  Scenario: MBNA Customer successfully views credit card PIN
    Given I am a valid user on the home page
    When I select support tab from bottom navigation bar
    And I select the View PIN option from the Help or Support menu
    Then I choose the View PIN option
    And I successfully enter my password
    When I select to reveal the PIN
    Then the PIN number should be displayed for also long as I am holding the reveal PIN button

  @uatDaily
  Scenario: MBNA Change Marketing preference
    Given I am a valid user on the home page
    When I navigate to profile page via more menu
    And I select marketing preferences link
    And I'm on the marketing hub page
    And I opt in to all the marketing prefernces
    Then I submit my preferences
    And I select home tab from bottom navigation bar
    And I should be on the home page
    When I select more menu
    And I navigate to profile page via more menu
    And I select marketing preferences link
    And I'm on the marketing hub page
    And I opt out to all the marketing prefernces
    Then I submit my preferences

  @uatDaily
  Scenario Outline: MBNA Change home number
    Given I am a valid user on the home page
    When I navigate to profile page via more menu
    And I choose to update a phone number
    And I have submitted request to change <homeNumber> home number
    Then The request has been successfully processed
    And I should be shown a success screen
    And I should be shown an option to go back to my personal details
    And I navigate back to contact details
    Examples:
      | homeNumber |
      | 7787105633 |

  @uatDaily
  Scenario: MBNA Change Email details
    Given I am a mbna user on the home page
    And I have submitted a request to change my email address
    When The request has been successfully processed
    Then I should be shown a success message with prompt to close the message
    And I dismiss the prompt

  @uatDaily
  Scenario Outline: MBNA Log off due to Inactivity
    Given I am a mbna user on the home page
    And I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    And I select the desired "<autoLogOffTime>" from the auto log off page
    Then I should see a log off alert 15 seconds before the "<autoLogOffTime>" auto logoff time
    And I must be logged out of the app after the selected auto logoff time
    When I select back to logon button on the logout page
    And I am a mbna user on the home page
    And I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    And I select the desired "<inactivityTime>" from the auto log off page
    Examples:
      | autoLogOffTime | inactivityTime |
      | 1 Minute       | 10 Minute      |

  @webview
  Scenario: MBNA Change of Address
    Given I am a valid user on the home page
    When I select support tab from bottom navigation bar
    And I select "change of address" option in the support hub page
    Then I should be on the your personal details page
    And I should be shown the list of personal details
      | personalDetail |
      | Full Name      |
      | User ID        |
      | Mobile Number  |
      | Home Number    |
      | Work Number    |
      | Address        |
      | Postcode       |
      | Email address  |
    When I choose to update address from the personal details
    Then I should be on the new address update page
    And I should see an option to search my new address using postcode
    When I proceed to call customer care
    Then I should be on the secure call page
    When I go back to previous page
    And I replace existing with new post code of my new address
    And I perform a postcode search to see the matching addresses
    Then I should see list of addresses for entered postcode
    When I select my new address
    Then I should be asked to confirm my new address
    And I should see my new address on Map
    And I should see my new address selected for confirmation
    And I should see my new address is in title case
    When I confirm my new address
    Then I should see final instructions before updating my address
    When I confirm to update my new address
    Then I should see address change successful message
    And I can proceed to homepage after my address got updated
    When I proceed to homepage after my address got updated
    Then I should be on the home page

  @webview
  Scenario: MBNA Change Paperless settings
    Given I am a valid user on the home page
    Then I navigate to settings page via more menu
    When I navigate to online and paper preferences from the settings page
    Then I should be on the manage your online and paper preference webview page
    When I select update preference from manage your online and paper preference webview page
    Then I am on confirm your paperless preferences webview page
    When I accept the terms & condition and submit order paper preference
    Then I should see update preference confirmation page

  @webview
  Scenario: MBNA Freeze Card transactions from Card Management
    Given I am a valid user on the home page
    When I navigate to card management page via more menu
    Then I should be on card management page
    When I select "Freeze card transactions" option in card management page
    Then I should be on the Freeze card transactions page
    And I select all options available on Freeze card transactions page

  @webview
  Scenario: MBNA Replace Card and Pin from Card management, More and Support Tab
    Given I am a valid user on the home page
    When I navigate to creditCard account on home page
    And I select creditCard account tile from home page
    And I tap to view the action menu
    And I select "Card Management" option in the action menu of creditCard account
    Then I should be on card management page
    And I should see below options on card management page
      | fieldsInDetailScreen  |
      | freezCardTransactions |
      | lostAndStolenCards    |
      | replaceCardAndPin     |
    When I select "replacement card and pin" option in card management page
    Then I should be on the replacement card and pin page
    When I navigate to card management page via more menu
    Then I should be on card management page
    When I select "replacement card and pin" option in card management page
    Then I should be on the replacement card and pin page
    When I select support tab from bottom navigation bar
    And I select "replacement card and pin" option in the support hub page
    Then I should be on the replacement card and pin page
    When I select replace cards and pin options on the replacement cards and pin page
    And I select to continue on the lost and stolen page
    Then I should be on the card replace and pin details confirmation page
    And I should be shown below fields on the card replace and pin details confirmation page

  @webview
  Scenario: MBNA View PDF statement for single Credit Card
    Given I am a valid user on the home page
    When I navigate to creditCard account on home page
    And I select the action menu of creditCard account
    Then I select pdfStatementsOption from contextual menu
    And I should be on the PDF statements webview page for creditCard account
    And I should be able to view the PDF Statements for current year

  @webview
  Scenario Outline: MBNA View PDF statement for Multiple Credit Card
    Given I am a valid user on the home page
    When I navigate to creditCard account on home page
    And I select the action menu of <accountName> account
    Then I select pdfStatementsOption from contextual menu
    And I should be on the PDF statements webview page for creditCard account
    And I should be able to view the PDF Statements for current year
    Examples:
      | accountName      |
      | firstCreditCard  |
      | secondCreditCard |

  @webview
  Scenario: MBNA Gambling block from Card Management
    Given I am a valid user on the home page
    When I navigate to card management page via more menu
    Then I should be on card management page
    When I select "Freeze card transactions" option in card management page
    Then I should be on the Freeze card transactions page
    When I select freeze gambling option in Freeze card transactions page
    Then I should be on Ready to Freeze Gambling page
    And I select Freeze button in Ready to Freeze Gambling page
    And I select Cancel button in Almost there popup in Freeze Gambling page

  @uatDaily
  Scenario: MBNA Change Password from Support Hub and Settings Menu
    Given I am a valid user on the home page
    When I select support tab from bottom navigation bar
    And I select "reset password" option in the support hub page
    Then I should be on the password reset page
    When I navigate to settings page from home page
    And I select on Security settings link on settings page
    And I select the forgotten password option from the security settings page
    Then I should be on the password reset page
    When I enter my new valid password in the reset password page
#    And I select the submit button in the reset password page
#    Then I should see a password updated pop up in the reset password page
#    And I should be on the logout page
#    When I select back to logon button on the logout page
#    And I am a valid user on the home page

  @uatDaily
  Scenario: MBNA App de-register
    Given I am a valid user on the home page
    When I navigate to settings page from home page
    And I select the reset app option from the settings page
#    And I successfully reset the app
#    Then I should be on the logout page

  @uatInitiative
  Scenario: TC_06 View debit and credit transaction in credit card statements
    Given I am a current user on the home page
    When I navigate to creditCard account to validate accountNumber and select creditCard account
    And I should be on the recent of statements page
    Then I should be displayed creditCard account name action menu and last four digits of creditCard number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
      | availableCredit     |
      | creditLimit         |
      | overDueAmount       |
    And I should see transactions since message on recent tab
    And I should see debit transaction with below details in credit card statements
      | detail                      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    When I scroll down on the transactions
    Then I should be displayed current balance in the header alongside account Name
    And I should not be displayed account tile in statements page
    And I should be displayed month tabs in place of account tile
    When I scroll back to the top of my transactions
    Then The current balance will no longer display in the header and the account tile will reappear
    And I swipe to previous 3 months transactions on the account statement page
    And The previously billed info panel appears

  Scenario Outline: Search for change your address
    Given I am a valid user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the change Your Address link as search result
    When I tap on the change Your Address link
    Then I should be on change Your Address page as per the searched keyword
    Examples:
      | keyword     |
      | post		|
      | code		|
      | letter		|
      | house		|
      | changing	|
      | details		|

  Scenario Outline: Search for making a payment
    Given I am a valid user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the make Payment link as search result
    When I tap on the make Payment link
    Then I should be on pay And Transfer page as per the searched keyword
    Examples:
      | keyword       |
      | give          |
      | send          |
      | transfer      |
      | transfers     |
      | bill          |

  Scenario Outline: No search result found page
    Given I am a valid user on the home page
    When I tap on the In-App search box
    Then I should be able to see the pre defined suggested search results
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the No Search Result found page
    When I tap on the help and support button
    Then I should land into Support Hub Page
    Examples:
      | keyword     |
      | ASDAXYZ		|

  @webview
  Scenario: RNGA Lost and Stolen Card from Card management, More and Support Tab
    Given I am a valid user on the home page
    When I navigate to card management page via more menu
    Then I should be on card management page
    When I select "Lost and stolen cards" option in card management page
    Then I should be on the lost and stolen page
    When I navigate to card management page via more menu
    Then I should be on card management page
    When I select "Lost and stolen cards" option in card management page
    Then I should be on the lost and stolen page
    When I select support tab from bottom navigation bar
    And I select "lost and stolen card" option in the support hub page
    Then I should be on the lost and stolen page
    When I select lost option on the lost and stolen page
    And I select cards option on the lost and stolen page
    And I select to continue on the lost and stolen page
    Then I should be on the card replace and pin details confirmation page
    And I should be shown below fields on the card replace and pin details confirmation page
      | fieldsInDetailScreen |
      | password             |
      | continue             |
