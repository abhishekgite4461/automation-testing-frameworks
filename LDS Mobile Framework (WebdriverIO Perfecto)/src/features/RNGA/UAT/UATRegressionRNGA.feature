@team-uat @rnga @nga3 @environmentOnly @ff-webview-debug-on
Feature: UAT Regression

  #Test account must have phone number matching device
  @pending
  Scenario Outline: RNGA Full IB Registration
    Given I am a ibreg_eia_success user
    When I complete EIA registration for Internet Banking
    And I choose to receive EIA call on <number>
    And I choose call me now option
    And I capture and enter the 4 digit pin for eia call
    Then I should see the success Message as "<HC-201>" on the IB Registration Success page
    And I should see the footer Message as "<HC-203>" on the IB Registration Success page
    When I select continue on the IB Registration Success page
    And I choose my security auto logout settings
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page
    Examples:
      | number | HC-201                                                        | HC-203                              |
      | Work   | Only you can access your account from the app on this device. | password and memorable information. |

  #Test account must have phone number matching device
  @uatDaily
  Scenario Outline: RNGA User registration with EIA
    Given I am a valid user
    When I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I choose to receive EIA call on <number>
    And I choose call me now option
    And I capture and enter the 4 digit pin for eia call
    And I select continue button on the enrolment confirmation page
    And I choose my security auto logout settings
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page
    Examples:
      | number |
      | Work   |

  @uatDaily
  Scenario: RNGA Log off app via more menu
    Given I am a valid user on the home page
    When I navigate to logoff via more menu
    Then I should be on the logout page

  @uatDaily
  Scenario: RNGA View FSCS tile on MI page
    Given I am a valid user
    When I select FSCS tile in MI page
    And I select leave app in MI page
    Then I should see mobile banking page

  @uatDaily
  Scenario: RNGA Account Overview
    Given I am a valid user on the home page
    And bottom navigation bar is displayed
    Then I should be greeted with time of day with users first name
    And I should see search icon on the home page
    And I should see the below options in the bottom nav
      | optionsInBottomNav |
      | home               |
      | apply              |
      | payAndTransfer     |
      | support            |
      | more               |
    And I navigate and validate the options in the current account in below table in the home page
      | accountFields   |
      | accountTileLogo |
      | accountNumber   |
      | sortCode        |
      | accountBalance  |
    When I select the action menu of current account
    Then I should see the below options in the action menu of the current account
      | optionsInActionMenu               |
      | viewTransactionsOption            |
      | transferAndPaymentOption          |
      | standingOrderAndDirectDebitOption |
      | viewSpendingInsights              |
      | viewUpcomingPayment               |
      | sendMoneyOutsideTheUKOption       |
      | chequeDepositOption               |
      | renameAccountOption               |
      | changeAccountTypeOption           |
      | applyForOverdraftOption           |
      | orderPaperStatementOption         |
      | cardManagementOption              |
      | saveTheChangeOption               |
      | sendBankDetailsOption             |
    When I close the action menu in the home page
    Then I navigate and validate the options in the instantSaving account in below table in the home page
      | accountFields   |
      | accountTileLogo |
      | accountNumber   |
      | sortCode        |
      | accountBalance  |
    When I select the action menu of instantSaving account
    Then I should see the below options in the action menu of the instantSaving account
      | optionsInActionMenu           |
      | viewTransactionsOption        |
      | transferAndPaymentOption      |
      | chequeDepositOption           |
      | orderPaperStatementOption     |
      | cardManagementOption          |
      | renewYourSavingsAccountOption |
      | viewPendingPaymentOption      |
      | viewInterestRateOption        |
      | renameAccountOption           |
    When I close the action menu in the home page
    Then I navigate and validate the options in the isa account in below table in the home page
      | accountFields  |
      | accountNumber  |
      | sortCode       |
      | accountBalance |
    When I select the action menu of isa account
    Then I should see the below options in the action menu of the isa account
      | optionsInActionMenu           |
      | viewTransactionsOption        |
      | transferAndPaymentOption      |
      | chequeDepositOption           |
      | topupIsaOption                |
      | orderPaperStatementOption     |
      | cardManagementOption          |
      | renewYourSavingsAccountOption |
      | viewPendingPaymentOption      |
      | viewInterestRateOption        |
    When I close the action menu in the home page
    Then I navigate and validate the options in the creditCard account in below table in the home page
      | accountFields            |
      | creditCardAccountNumber  |
      | creditCardAccountBalance |
      | availableCredit          |
    When I select the action menu of creditCard account
    Then I should see the below options in the action menu of the creditCard account
      | optionsInActionMenu        |
      | viewTransactionsOption     |
      | payCreditCardOption        |
      | balanceMoneyTransferOption |
      | manageCreditLimitOption    |
      | cardManagementOption       |
      | pdfStatementsOption        |
    When I close the action menu in the home page
    Then bottom navigation bar is displayed
    And I am shown the Covid19 tile on home page

  @uatDaily
  Scenario Outline: RNGA In App search
    Given I am a valid user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the <search> link as search result
    When I tap on the <search> link
    Then I should be on <search> page as per the searched keyword
    Examples:
      | keyword | search                   |
      | post    | change Your Address      |
      | change  | change Your Phone Number |

  @uatDaily
  Scenario Outline: RNGA Validate Call Us page details
    Given I am a valid user on the home page
    When I navigate to call us home page
    Then I should be on the call us home page
    And I should be able to view these call us tiles
      | callUsTiles           |
      | yourAccountsTile      |
      | newProductEnquireTile |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I select your accounts tile on call us home page
    When I select the <account_type> tile on your accounts page
    Then I should be on the secure call page
    And I am able to view the call us button
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    And I am shown the below self service options
      | serviceOption          |
      | Reset your password    |
      | Lost or stolen cards   |
      | Replacement cards PINS |
    When I navigate back
    And I navigate back
    And I select new product enquiry tile on call us home page
    Then I am shown the list of sales option
      | salesOptions        |
      | newCurrentAccount   |
      | newSavingsAccount   |
      | newIsaAccount       |
      | newCreditAccount    |
      | newLoanAccount      |
      | newMortgagesAccount |
    And I select individual sales option on new product page and validate callus page
      | salesOptions        |
      | newCurrentAccount   |
      | newSavingsAccount   |
      | newIsaAccount       |
      | newCreditAccount    |
      | newLoanAccount      |
      | newMortgagesAccount |
    When I navigate back
    And I select the internet banking tile on call us home page
    Then I should be on the secure call page
    And I am shown the below self service options
      | serviceOption          |
      | Reset your password    |
      | Lost or stolen cards   |
      | Replacement cards PINS |
    When I navigate back
    And I select the other banking query tile on call us home page
    Then I should be on the secure call page
    And I am shown the below self service options
      | serviceOption          |
      | Reset your password    |
      | Lost or stolen Cards   |
      | Replacement cards PINS |
    When I navigate back
    And I should see security and travel emergency label
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
      | medicalAssistAbroadTab |
    Examples:
      | account_type    |
      | Current account |

  @uatDaily
  Scenario: RNGA Call US CMA Survey
    Given I am a valid user on the home page
    When I select support tab from bottom navigation bar
    And I select call us on support hub page
    And I navigate back
    Then I should see "CMA Tile" in the support hub home page

  @uatDaily
  Scenario: RNGA Setting page validations
    Given I am a valid user on the home page
    When I navigate to settings page from home page
    And I select your personal details option in settings page
    Then I should be on the your personal details page
    And I should be shown the list of personal details
      | personalDetail |
      | Full Name      |
      | User ID        |
      | Mobile Number  |
      | Home Number    |
      | Work Number    |
      | Address        |
      | Postcode       |
      | Email address  |
    When I navigate back
    And I select the how we contact you option from the settings page
    Then I should be shown the screen with following options
      | howWeContactYouOptions  |
      | onlinePaperPreference   |
      | statementFreqPreference |
      | marketingPreference     |
    When I navigate back
    And I select modify spending rewards from the settings menu page
    Then I should be on the modify spending rewards webview page
    When I navigate back
    And I select on Security settings link on settings page
    Then I should see the security settings page
    And I should see the following options in security details page
      | securityDetailsOptions  |
      | User ID                 |
      | App version             |
      | Device name             |
      | Device type             |
      | Forgotten your password |
      | Auto logoff             |
      | Finger Print/Touch id   |
    When I navigate back
    And I select pay a contact settings from the settings menu
    Then I should be on the register for pay a contact page
    When I navigate back
    And I select the reset app option from the settings page
    And I select reset app button from the reset app page
    And I select cancel button from the reset app confirmation pop up
    Then I should be on the reset app Page
    When I navigate back
    And I navigate to the legal info option from the settings page
    Then I should see the following links on legal info page
      | fieldsOnLegalInfoScreen    |
      | legalInformationText       |
      | legalAndPrivacy            |
      | cookieUseAndPermissions    |
      | thirdPartyAcknowledgements |
    When I navigate back
    Then I am shown the option to Remove Provider
    When I select Remove Provider tile
    Then I should be on Consent settings page

  @uatDaily
  Scenario: RNGA Branch and ATM Finder
    Given I am a valid user on the home page
    When I select support tab from bottom navigation bar
    And I am on the atm and branch finder page
    Then I am displayed the following search options
      | searchOption       |
      | searchBranchesTile |
      | findNearbyAtmTile  |
    And I am displayed a note message stating clicking search option links take you outside the app

  @uatDaily
  Scenario Outline: RNGA Saving Account Check interest rates
    Given I am a valid user on the home page
    When I navigate to <type> account on home page
    And I select View Interest Details option of <type> account from action menu
    Then I should see the following fields in the details screen
      | fieldsInDetailsScreen   |
      | accountName             |
      | accountBalance          |
      | interestRateLabel       |
      | interestRateDescription |
    And I close the interest rate details screen using the close button
    Examples:
      | type          |
      | instantSaving |

  @uatDaily
  Scenario Outline: RNGA Delete Existing beneficiary from Current Account
    Given I am a valid user on the home page
    When I navigate to pay and transfer from home page
    Then I delete <number>, <recipientName> beneficiary from the list
    Examples:
      | recipientName    | number |
      | TESTRAPIDNEW-HXI | first  |
      | AUTOHFX02        | second |

  @uatDaily
  Scenario Outline: RNGA Delete Existing beneficiary from Saving Account
    Given I am a valid user on the home page
    When I navigate to instantSaving account on home page
    And I select the "transfer and payment" option in the action menu of instantSaving account
    Then I delete <number>, <recipientName> beneficiary from the list
    Examples:
      | recipientName | number |
      | AUTOHFX02     | second |
      | AUTOHFX       | second |

  @uatDaily
  Scenario Outline: RNGA Leave Transfer before confirm
    Given I am a valid user on the home page
    When I select the "transfer and payment" option in the action menu of <fromAccount> account
    And I submit transfer with from account <toAccount> and to account <fromAccount> with amount 0.01
    And I select dismiss modal using close button
    And I select the leave option in the payment hub page winback
    Then I should be on the home page
    Examples:
      | fromAccount | toAccount     |
      | current     | instantSaving |

  @uatDaily @iOSObjectTreeOpt
  Scenario Outline: RNGA Current Account Make transfers through action menu & VTD Validation
    Given I am a valid user on the home page
    When I select the "transfer and payment" option in the action menu of <fromAccount> account
    And I submit transfer with from account <fromAccount> and to account <toAccount> with amount 0.01
    And I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page
    When I select the view transaction option in the transfer success page
    Then I should see account details with the following fields
      | fieldsInAccountTile      |
      | overDraftLimitLabel      |
      | overDraftLimitValue      |
      | balanceAfterPendingLabel |
      | balanceAfterPending      |
      | accountTrueBalance       |
    And I should be on the allTab of statements page
    And I search with 0.01 in search dialog box
    And I select one of the posted transaction
    And I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | date                    |
      | vtdCloseButton          |
      | vtdAmount               |
      | transferText            |
      | transactionDescription  |
      | unsureTransactionButton |
      | transferPaymentsButton  |
      | sortCode                |
      | accountNumber           |
    And I select the help with this transaction link in VTD screen
    And I must be displayed the Unsure About Transactions Help and Info screen
    Examples:
      | fromAccount | toAccount     |
      | current     | instantSaving |

  @uatDaily
  Scenario Outline: RNGA Current Account Make Transfer through bottom navigation bar
    Given I am a valid user on the home page
    When I select payAndTransfer tab from bottom navigation bar
    And I submit transfer with from account <fromAccount> and to account <toAccount> with amount 0.01
    And I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page
    When I select the make another transfer option in the transfer success page
    Then I should be navigated to the payment hub page with from account as <fromAccount> in the payment hub page
    Examples:
      | fromAccount | toAccount     |
      | current     | instantSaving |

  @uatDaily
  Scenario Outline: RNGA Saving Account Make transfers through action menu
    Given I am a valid user on the home page
    When I select the "transfer and payment" option in the action menu of <fromAccount> account
    And I submit transfer with from account <fromAccount> and to account <toAccount> with amount 0.01
    And I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page
    When I select the make another transfer option in the transfer success page
    Then I should be navigated to the payment hub page with from account as <fromAccount> in the payment hub page
    Examples:
      | fromAccount   | toAccount |
      | instantSaving | current   |

  @uatDaily
  Scenario Outline: RNGA Saving Account Make Transfer through bottom navigation bar
    Given I am a valid user on the home page
    When I select payAndTransfer tab from bottom navigation bar
    And I submit transfer with from account <fromAccount> and to account <toAccount> with amount 0.01
    And I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page
    When I select the view transaction option in the transfer success page
    Then I should see the - £0.01 in the current month statement of the remitting account
    Examples:
      | fromAccount   | toAccount |
      | instantSaving | current   |

  @uatDaily
  Scenario Outline: RNGA Verify Remaining Allowance
    Given I am a valid user on the home page
    And I am on the payment hub page
    And I submit the isa transfer with all the mandatory fields with from account as <fromAccount> and corresponding to account as <toAccount> with amount value <amount>
    And I select the confirm option in the review transfer page
    Then I should see the transfer successful message page
    When I select the make another transfer option in the transfer success page
    And  I submit the isa transfer with all the mandatory fields with corresponding from account as <toAccount> and to account as <fromAccount> with amount value <amount>
    Then I should see withdraw from ISA to non ISA warning message on review transfer screen
    And I select ISA transfer agreement checkbox and confirm option in the review transfer page
    And I select the make another transfer option in the transfer success page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I verify the remaining allowance from ISA account after Transaction for the amount value <amount>
    Examples:
      | fromAccount | toAccount | amount |
      | current     | isa       | 1      |

  @uatDaily
  Scenario Outline: RNGA Current Account Pay Credit Card Bill
    Given I am a valid user on the home page
    When I am on the payment hub - pay a credit card page
    Then I select dismiss modal using close button
    When I navigate to creditCard account on home page
    And I select the "pay a credit card" option in the action menu of creditCard account
    And I enter amount greater than the balance of the credit card account in the payment hub page
    Then I should see Outbound or Insufficient credit card payment validation error message
    When I am on the payment hub page with data prepopulated
    And I clear the amount field in the payment hub page
    And I submit the future dated payment for a credit card with all the mandatory fields with corresponding amount value <amount>
    And I select the confirm option in the review payment page
    Then I should see the corresponding details in the creditCard success page
    Examples:
      | amount |
      | 0.01   |

  @uatDaily
  Scenario Outline: RNGA Saving Account Pay Credit Card Bill
    Given I am a valid user on the home page
    When I navigate to creditCard account on home page
    And I select the "pay a credit card" option in the action menu of creditCard account
    Then I select dismiss modal using close button
    When I select payAndTransfer tab from bottom navigation bar
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter amount greater than the balance of the credit card account in the payment hub page
    Then I should see Outbound or Insufficient credit card payment validation error message
    When I am on the payment hub page with data prepopulated
    And I clear the amount field in the payment hub page
    And I submit the future dated payment for a credit card with all the mandatory fields with corresponding amount value <amount>
    And I select the confirm option in the review payment page
    Then I should see the corresponding details in the creditCard success page
    Examples:
      | fromAccount   | toAccount  | amount |
      | instantSaving | creditCard | 0.01   |

  @uatDaily @iOSObjectTreeOpt
  Scenario: RNGA View Credit Card statement
    Given I am a valid user on the home page
    When I select creditCard account tile from home page
    Then I should be on the recent of statements page
    And I navigate back
    When I navigate to creditCard account on home page
    Then I navigate via actions menu to see the credit card transaction details of creditCard account
    And I should see transactions since message on recent tab
    And I swipe to previous 1 months transactions on the account statement page
    And The previously billed info panel appears
    And I should see credit transaction with below details in credit card statements
      | detail                      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
      | firstCreditIndicator        |
    And I select payment Received on statements page
    And I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | date                    |
      | vtdCloseButton          |
      | vtdAmount               |
      | unsureTransactionButton |
      | creditCardText          |
      | payeeField              |
      | cardEndingNumber        |
      | dateOfTransaction       |
    And I select the help with this transaction link in VTD screen
    And I must be displayed the Unsure About Transactions Help and Info screen
    And I should see a list of below cleared transaction help options
      | fieldsInHelpPage         |
      | infoHeading              |
      | dontRecogniseTransaction |
      | expectingRefund          |
      | multipleDates            |
      | covid19Support           |
      | checkTheGuidanceHeading  |
      | disputeMessage           |
      | contactUs                |
    And I select below options to view the related helpDetails page
      | helpPageOptions          |
      | dontRecogniseTransaction |
      | expectingRefund          |
      | multipleDates            |
      | covid19Support           |
    And I select below links under guidance to view related page
      | helpPageOptions          |
      | disputeMessage           |
      | contactUs                |

  @uatDaily
  Scenario: RNGA Customer successfully views credit card PIN
    Given I am a valid user on the home page
    When I select support tab from bottom navigation bar
    And I select the View PIN option from the Help or Support menu
    Then I choose the View PIN option
    And I successfully enter my password
    When I select to reveal the PIN
    Then the PIN number should be displayed for also long as I am holding the reveal PIN button

  @uatDaily @iOSObjectTreeOpt
  Scenario Outline: RNGA Current Account View Pending payment
    Given I am a valid user on the home page
    When I navigate to pay and transfer from home page
    And I select recipient <recipientName> to make payment to from searched list of recipients
    And I select calendar picker option for future date payment
    And I select the date as <days> from today's date from calendar view
    And I submit the payment with amount value <amount> and reference
    And I select the confirm option in the review payment page
    And I should see Payment Scheduled success page
    And I should see share receipt option on the payment success page
    And I select the view transaction option in the payment success page
    And I select the viewUpcomingPayment option from the action menu in statements page
    Then I should see the Future payment transaction for <recipientName> and <amount>
    Examples:
      | recipientName  | amount | days |
      | SELPENDINGBENE | 0.01   | 5    |

  @uatDaily @iOSObjectTreeOpt
  Scenario Outline: RNGA Saving Account View Pending payment
    Given I am a valid user on the home page
    When I navigate to <type> account on home page
    And I select the "transfer and payment" option in the action menu of <type> account
    And I select recipient <recipientName> to make payment to from searched list of recipients
    And I select calendar picker option for future date payment
    And I select the date as <days> from today's date from calendar view
    And I submit the payment with amount value <amount> and reference
    And I select the confirm option in the review payment page
    And I should see Payment Scheduled success page
    And I should see share receipt option on the payment success page
    And I select the view transaction option in the payment success page
    And I select the viewPaymentsDueSoon option from the action menu in statements page
    And I should see dueSoon tab in statements screen
    Then I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    When I select pending Payment on statements page
    Then I should see upcoming transaction details for that transaction with the following fields
      | fieldsInDetailScreen  |
      | dueSoonLozenge        |
      | datePendingPayment    |
      | vtdCloseButton        |
      | vtdAmount             |
      | outgoingPaymentText   |
      | payeeField            |
      | sortCodeAccountNumber |
      | referenceField        |
      | changeOrDeleteText    |
    Examples:
      | type          | recipientName  | amount | days |
      | instantSaving | SELPENDINGBENE | 0.01   | 5    |

  @uatDaily
  Scenario Outline: RNGA Leave FPO payments before confirm
    Given I am a valid user on the home page
    And I am on the payment hub page
    When I select recipient <recipientName> to make payment to from searched list of recipients
    And I submit the payment with amount value <amount> and reference
    And I select dismiss modal using close button
    And I select the leave option in the payment hub page winback
    Then I should be on the home page
    Examples:
      | recipientName | amount |
      | AUTOHFX02     | 0.01   |

  @uatDaily @iOSObjectTreeOpt
  Scenario Outline: RNGA Current Account Make payments to newly added beneficiary
    Given I am a valid user on the home page
    And I am on the payment hub page
    When I select the from account as current in the payment hub page
    And I select the recipient option in the payment hub page
    Then I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    And I should see Checking Details Spinner with relevant bank name
    And I continue to payment hub page from COP response page
    And I am on the Make Payment screen with newly added beneficiary selected
    And I submit the payment with amount value <amount> and reference
    And I select the confirm option in the review payment page
    And I should be prompted to enter my password
    When I enter a correct password
    Then I should see the corresponding details in the payment success page
    And I should see share receipt option on the payment success page
    When I select the view transaction option in the payment success page
    Then I should be on the allTab of statements page
    And I search with <recipientName> in search dialog box
    And I select one of the posted transaction
    And I validate <recipientName> and <amount> details in VTD page
    Examples:
      | sortCode | accountNumber | recipientName | amount |
      | 117940   | 10013663      | AUTOHFX02     | 0.01   |
#      | 301775   | 12509960      | AUTOHFX02       | 0.01   |

  @uatDaily @iOSObjectTreeOpt
  Scenario Outline: RNGA Current Account Make Future dated payments to newly added beneficiary
    Given I am a valid user on the home page
    And I am on the payment hub page
    When I select the from account as current in the payment hub page
    And I select the recipient option in the payment hub page
    Then I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    And I should see Checking Details Spinner with relevant bank name
    And I continue to payment hub page from COP response page
    And I am on the Make Payment screen with newly added beneficiary selected
    And I select calendar picker option for future date payment
    And I select the date as <Days> from today's date from calendar view
    And I submit the payment with amount value <amount> and reference
    And I select the confirm option in the review payment page
    And I should be prompted to enter my password
    When I enter a correct password
    Then I should see the corresponding details in the payment success page
    When I select the share option on the payment success page
    And I am on the Share Preview screen
    Then I should see the following details populated on the share preview screen
      | detail                   |
      | bankIcon                 |
      | paymentSentHeader        |
      | payerName                |
      | beneficiaryName          |
      | beneficiarySortCode      |
      | beneficiaryAccountNumber |
      | referenceValue           |
      | amountValue              |
      | paymentReceiptDateField  |
      | sharePreviewShareButton  |
    And I should see the first 4 digits of the recipient account number is masked
    And I should see the date value on the share preview screen
    And I validate native OS panel and navigate back
    And I should see the full name of the user who logged in against from field
    And I select home tab from bottom navigation bar
    When I select current account tile from home page
    And I select the viewUpcomingPayment option from the action menu in statements page
    Then I should see the Future payment transaction for <recipientName> and <amount>
    Examples:
      | sortCode | accountNumber | recipientName | amount | Days |
      | 117940   | 10013663      | AUTOHFX02     | 0.01   | 5    |
#      | 301775   | 12509960      | AUTOHFX02       | 0.01   | 5    |

  @uatDaily @iOSObjectTreeOpt
  Scenario Outline: RNGA Current Account Make payments to Existing beneficiary
    Given I am a valid user on the home page
    And I am on the payment hub page
    And I select the from account as current in the payment hub page
    When I select recipient <recipientName> to make payment to from searched list of recipients
    And I submit the payment with amount value <amount> and reference
    And I select the confirm option in the review payment page
    Then I should see the corresponding details in the payment success page
    And I should see share receipt option on the payment success page
    When I select the view transaction option in the payment success page
    Then I should be on the allTab of statements page
    And I search with <recipientName> in search dialog box
    And I select one of the posted transaction
    And I validate <recipientName> and <amount> details in VTD page
    And I close the posted transactions
    Examples:
      | recipientName | amount |
      | AUTOHFX02     | 0.01   |

  @uatDaily
  Scenario Outline: RNGA Saving Account Make payments to newly added beneficiary
    Given I am a valid user on the home page
    And I am on the payment hub page
    When I select the from account as instantSaving in the payment hub page
    Then I validate the available balance of saving account
    And I select the recipient account on the payment hub page
    And I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
#    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    And I should see Checking Details Spinner with relevant bank name
    And I continue to payment hub page from COP response page
    And I am on the Make Payment screen with newly added beneficiary selected
    And I submit the payment with amount value <amount> and reference
    And I select the confirm option in the review payment page
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see the corresponding details in the payment success page
    When I select the share option on the payment success page
    And I am on the Share Preview screen
    Then I should see the following details populated on the share preview screen
      | detail                   |
      | bankIcon                 |
      | paymentSentHeader        |
      | payerName                |
      | beneficiaryName          |
      | beneficiarySortCode      |
      | beneficiaryAccountNumber |
      | referenceValue           |
      | amountValue              |
      | paymentReceiptDateField  |
      | sharePreviewShareButton  |
    And I validate native OS panel and navigate back
    And I should see the first 4 digits of the recipient account number is masked
    And I should see the full name of the user who logged in against from field
    Examples:
      | sortCode | accountNumber | recipientName | amount |
      | 117940   | 10013663      | AUTOHFX02     | 0.01   |
#      | 301775   | 12509960      | AUTOHFX02       | 0.01   |

  @uatDaily
  Scenario Outline: RNGA Saving Account Make Future dated payments to newly added beneficiary
    Given I am a valid user on the home page
    When I select the "transfer and payment" option in the action menu of instantSaving account
    And I select the recipient account on the payment hub page
    Then I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    And I should see Checking Details Spinner with relevant bank name
    And I continue to payment hub page from COP response page
    And I am on the Make Payment screen with newly added beneficiary selected
    And I select calendar picker option for future date payment
    And I select the date as <Days> from today's date from calendar view
    And I submit the payment with amount value <amount> and reference
    And I select the confirm option in the review payment page
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see the corresponding details in the payment success page
    And I should see share receipt option on the payment success page
    When I select the view transaction option in the payment success page
    Then I should see the - £0.01 in the current month statement of the remitting account
    Examples:
      | sortCode | accountNumber | recipientName | amount | Days |
      | 117940   | 10013663      | AUTOHFX02     | 0.01   | 5    |
#      | 301775   | 12509960      | AUTOHFX02       | 0.01   | 5    |

  @uatDaily
  Scenario Outline: RNGA Saving Account Make payments to Existing beneficiary
    Given I am a valid user on the home page
    When I navigate to <type> account on home page
    And I select the "transfer and payment" option in the action menu of instantSaving account
    And I select recipient <recipientName> to make payment to from searched list of recipients
    And I select view payment limits link
    And I am shown following account daily limits for the account in view payments limit page
      | detail                 |
      | onlineMobileBanking    |
      | ukPaymentsDailyLimit   |
      | ukPaymentResetMessage  |
      | transferYourOwnAccount |
      | telephonyBanking       |
      | ukTelephonyBanking     |
    And I select back buttom from view payment limit page
    And I submit the payment with amount value <amount> and reference
    And I select the confirm option in the review payment page
    Then I should see the corresponding details in the payment success page
    And I should see share receipt option on the payment success page
    When I select the make another payment option in the payment success page
    Then I should be navigated to the payment hub page with from account as instantSaving in the payment hub page
    And I select recipient <recipientName> to make payment to from searched list of recipients
    And I select view payment limits link
    And I validate <amount> is deducted from daily payment limit after transaction
    Examples:
      | type          | recipientName | amount |
      | instantSaving | AUTOHFX02     | 0.01   |

  @uatDaily
  Scenario Outline: RNGA COP Match new payee immediate payment
    Given I am a valid user on the home page
    And I am on the payment hub page
    When I select the from account as current in the payment hub page
    And I select the recipient option in the payment hub page
    Then I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <invalidRecipientName>
    And I select continue button on add payee page
    And I should see Checking Details Spinner with relevant bank name
    And I should see Account name not match message
    And I validate share details options on COP response page and navigate back
    And I see the options to change Details or Continue
    And I navigate back on New UK beneficiary details page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    When I select continue button on add payee page
    Then I should see Checking Details Spinner with relevant bank name
    And I am shown Payee matches message
    And I am on the Make Payment screen with newly added beneficiary selected
    And I submit the payment with amount value <amount> and reference
    And I select the confirm option in the review payment page
    And I should be prompted to enter my password
    When I enter a correct password
    Then I should see the corresponding details in the payment success page
    And I should see share receipt option on the payment success page
    When I select the view transaction option in the payment success page
    Then I should be on the allTab of statements page
    And I search with <recipientName> in search dialog box
    And I select one of the posted transaction
    And I validate <recipientName> and <amount> details in VTD page
    Examples:
      | sortCode | accountNumber | invalidRecipientName | recipientName      | amount |
      | 117940   | 10013663      | AUTOHFX02            | H TESTRAPIDNEW-HXI | 0.01   |
#      | 301775   | 12509960      | AUTOHFX02              | DigitalTestBR Test | 0.01   |

  @uatDaily
  Scenario Outline: RNGA COP Match new payee future dated payment
    Given I am a valid user on the home page
    And I am on the payment hub page
    When I select the from account as current in the payment hub page
    And I select the recipient option in the payment hub page
    Then I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <invalidRecipientName>
    And I select continue button on add payee page
    And I should see Checking Details Spinner with relevant bank name
    And I should see Account name still not match message
    And I see the options to change Details or Continue
    And I navigate back on New UK beneficiary details page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    When I select continue button on add payee page
    Then I should see Checking Details Spinner with relevant bank name
    And I am shown Payee matches message
    And I am on the Make Payment screen with newly added beneficiary selected
    And I select calendar picker option for future date payment
    And I select the date as <Days> from today's date from calendar view
    And I submit the payment with amount value <amount> and reference
    And I select the confirm option in the review payment page
    And I should be prompted to enter my password
    When I enter a correct password
    Then I should see the corresponding details in the payment success page
    When I select the share option on the payment success page
    And I am on the Share Preview screen
    Then I should see the Bank Icon at the top of the screen
    And I should see the first 4 digits of the recipient account number is masked
    And I should see the date value on the share preview screen
    And I should see the full name of the user who logged in against from field
    And I select home tab from bottom navigation bar
    When I select current account tile from home page
    And I select the viewUpcomingPayment option from the action menu in statements page
    Then I should see the Future payment transaction for <recipientName> and <amount>
    Examples:
      | sortCode | accountNumber | invalidRecipientName | recipientName      | amount | Days |
      | 117940   | 10013663      | AUTOHFX02            | H TESTRAPIDNEW-HXI | 0.01   | 5    |
#      | 301775   | 12509960      | AUTOHFX02              | DigitalTestBR Test | 0.01   | 5    |

  @uatDaily @iOSObjectTreeOpt
  Scenario: RNGA View Faster Payments Outgoing Posted Transaction Details
    Given I am a valid user on the home page
    When I select current account tile from home page
    And I select FPO on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | date                    |
      | vtdCloseButton          |
      | vtdAmount               |
      | unsureTransactionButton |
      | transferPaymentsButton  |
      | fasterPaymentsOutText   |
      | payeeField              |
      | dateOfTransaction       |
      | timeOfTransaction       |
    And I select the help with this transaction link in VTD screen
    And I must be displayed the Unsure About Transactions Help and Info screen
    And I should see a list of below cleared transaction help options
      | fieldsInHelpPage         |
      | infoHeading              |
      | dontRecogniseTransaction |
      | expectingRefund          |
      | multipleDates            |
      | covid19Support           |
      | checkTheGuidanceHeading  |
      | disputeMessage           |
      | contactUs                |
    And I select below options to view the related helpDetails page
      | helpPageOptions          |
      | dontRecogniseTransaction |
      | expectingRefund          |
      | multipleDates            |
      | covid19Support           |
    And I select below links under guidance to view related page
      | helpPageOptions          |
      | disputeMessage           |
      | contactUs                |

  @uatDaily
  Scenario Outline: RNGA Whitelisted biller journey for future dated payment
    Given I am a valid user on the home page
    When I navigate to add new recipient option using payment hub
    And I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    Then I am on UK Company Search results page
    When I select the biller <whitelistedBiller> from the biller list
    And I validate reference field label
    And I enter <amount> in the payment amount field in the payment hub page
    And I enter the wrong reference value for the biller
    Then I validate the error banner message for wrong reference value
    And I enter the <correctReferenceFormat> reference value for the biller
    And I select calendar picker option for future date payment
    And I should see a calendar view
    And I should be allowed to select a payment date up to 31 days from current day
    When I select previous month chevron from the top header of calendar view
    And I select the date as 7 from today's date from calendar view
    And I select Continue button on payment hub page
    Then I should see the review payment page
    And I am shown 'When' the payment will be made on payment review page
    When I select the confirm option in the review payment page
    And I enter a correct password
    Then I am shown Payment Successful page
    Examples:
      | sortCode | accountNumber | recipientName | whitelistedBiller     | amount | correctReferenceFormat |
      | 772900   | 00000000      | HALIFAX CARD  | HALIFAX PLATINUM CARD | 0.01   | 5253030018263467       |

  @uatDaily @iOSObjectTreeOpt
  Scenario Outline: RNGA Make P2P payment to newly added beneficiary
    Given I am a valid user on the home page
    And I am on the payment hub page
    When I select the from account as current in the payment hub page
    And I select the recipient option in the payment hub page
    Then I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
    And I choose UK mobile number from account category page
    And I have entered valid <mobileNumber> <amount> and <referenceText>
    And I select continue in add new UK mobile number page
    And I select continue on Confirm Contact page
    And I select Confirm on the Confirm Payment page
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I am shown Payment Successful page
    And I should see share receipt option on the payment success page
    When I select the view transaction option in the payment success page
    Then I should be on the allTab of statements page
    And I search with <recipientName> in search dialog box
    And I select one of the posted transaction
    And I validate <recipientName> and <amount> details in VTD page
    And I close the posted transactions
    Examples:
      | mobileNumber | amount | referenceText | recipientName    |
      | 07789109688  | 1.00   | TESTING       | TESTRAPIDNEW-HXI |
#      | 07440710675  | 1.00   | TESTING       | D TEST           |

  @uatDaily
  Scenario Outline: RNGA Make P2P payment to Existing beneficiary
    Given I am a valid user on the home page
    When I navigate to pay and transfer from home page
    And I select the from account as current in the payment hub page
    And I select recipient <recipientName> to make payment to from searched list of recipients
    And I submit the P2P payment with amount value <amount> and reference
    And I select the confirm option in the review payment page
    Then I should see the corresponding details in the payment success page
    And I am shown Payment Successful page
    When I select the share option on the payment success page
    And I am on the Share Preview screen
    Then I should see the following details populated on the share preview screen
      | detail                  |
      | bankIcon                |
      | paymentSentHeader       |
      | payerName               |
      | nameAndNumberPreview    |
      | referenceValue          |
      | amountValue             |
      | sharePreviewShareButton |
    And I validate native OS panel and navigate back
    And I should see the full name of the user who logged in against from field
    Examples:
      | recipientName    | amount |
      | TESTRAPIDNEW-HXI | 1.00   |
#      | D TEST           | 1.00   |

  @uatDaily
  Scenario Outline: RNGA Leave P2P payments before confirm
    Given I am a valid user on the home page
    And I am on the payment hub page
    When I select recipient <recipientName> to make payment to from searched list of recipients
    And I submit the P2P payment with amount value <amount> and reference
    And I select dismiss modal using close button
    And I select the leave option in the payment hub page winback
    Then I should be on the home page
    Examples:
      | recipientName    | amount |
      | TESTRAPIDNEW-HXI | 1.00   |
#      | D TEST           | 1.00   |

  @uatDaily @iOSObjectTreeOpt
  Scenario: RNGA View Mobile Payment Outbound Posted Transaction Details
    Given I am a valid user on the home page
    When I select current account tile from home page
    And I select MPO on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | date                    |
      | vtdCloseButton          |
      | vtdAmount               |
      | unsureTransactionButton |
      | transferPaymentsButton  |
      | mobilePaymentOutText    |
      | payeeField              |
      | dateOfTransaction       |
      | timeOfTransaction       |
    And I select the help with this transaction link in VTD screen
    And I must be displayed the Unsure About Transactions Help and Info screen

  @uatDaily @iOSObjectTreeOpt
  Scenario: RNGA View statements for current and savings account
    Given I am a valid user on the home page
    When I select current account tile from home page
    And I select one of the posted transaction
    Then I validate the options in the VTD page
    And I close the posted transactions
    And I swipe to see previous month transactions on the account statement page
    And I select one of the posted transaction
    And I validate the options in the VTD page
    And I close the posted transactions
    And I select home tab from bottom navigation bar
    When I select instantSaving account tile from home page
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |

  @uatDaily
  Scenario Outline: RNGA Create StandingOrder for internal accounts NO existing SO
    Given I am a valid user on the home page
    And I select the "standing order & direct debit" option in the action menu of current account
    When I am shown a message that I have no direct debits set up under the relevant header
    And I should see no standing orders in the list
    And I select the set up standing order button
    Then I should see the from field pre-populated with current account
    And I choose the from account as current and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I provide Reference for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see a Standing Order set up success message
    When I select the make another payment option in the payment success page
    Then I should be navigated to the payment hub page with from account as current in the payment hub page
    And I select dismiss modal using close button
    And I select the "standing order & direct debit" option in the action menu of current account
    Examples:
      | toAccount     | Amount | frequency |
      | instantSaving | 1      | Monthly   |

  @uatDaily
  Scenario Outline: RNGA Create StandingOrder for External accounts
    Given I am a valid user on the home page
    And I select the "standing order & direct debit" option in the action menu of current account
    When I select the set up standing order button
    And I select recipient <recipientName> to make payment to from searched list of recipients
    And I have chosen to make this a Standing Order option
    And I provide Reference for a standing order
    And I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see a Standing Order set up success message
    When I select view standing orders in the standing order setup successful page
    Examples:
      | recipientName | Amount | frequency |
      | Autohfx02     | 1      | Quarterly |

  @uatDaily
  Scenario Outline: RNGA Amend and Delete StandingOrder for internal & External accounts
    Given I am a valid user on the home page
    And I select the "standing order" option in the action menu of current account
    When  I click on amend in the standing order and direct direct page
    And I change the reference <amendSO> of existing standing order
    And I change the amount <soAmount> of existing standing order
    And I select amend in amend standing order page
    Then I should be on confirm standing order amendments page
    When I enter password in amend standing order page
    And I select confirm amendments button in the amend standing order confirmation page
    Then I should be on standing order changed page
    When I select view standing orders in the standing order amend success page
    Then I should be on the standing orders page
    When I select delete option in the standing order page
    Then I should be on delete standing order page
    When I enter password in delete standing order page
    And I select delete standing order in the delete standing order confirmation page
    Then I should see standing order deleted successfully message in view standing orders page
    Examples:
      | amendSO | soAmount |
      | AUTOSOI | 0.02     |
      | AUTOSOE | 0.02     |

  @uatDaily
  Scenario: RNGA Change Marketing Preference
    Given I am a valid user on the home page
    When I navigate to profile page via more menu
    And I select marketing preferences link
    And I'm on the marketing hub page
    And I opt in to all the marketing prefernces
    Then I submit my preferences
    And I select the option to return to accounts
    And I should be on the home page
    When I navigate to settings page from home page
    And I select the how we contact you option from the settings page
    Then I should be shown the screen with following options
      | howWeContactYouOptions  |
      | onlinePaperPreference   |
      | statementFreqPreference |
      | marketingPreference     |
    When I select Marketing preferences
    And I'm on the marketing hub page
    And I opt out to all the marketing prefernces
    Then I submit my preferences

  @uatDaily
  Scenario Outline: RNGA Change home number
    Given I am a valid user on the home page
    When I navigate to profile page via more menu
    And I choose to update a phone number
    And I have submitted request to change <homeNumber> home number
    Then The request has been successfully processed
    And I should be shown a success screen
    And I should be shown an option to go back to my personal details
    And I navigate back to contact details
    Examples:
      | homeNumber |
      | 7787105633 |

  @uatDaily
  Scenario: RNGA Change Email details
    Given I am a valid user on the home page
    And I have submitted a request to change my email address
    When The request has been successfully processed
    Then I should be shown a success message with prompt to close the message
    And I dismiss the prompt

  @uatDaily
  Scenario Outline: RNGA Customer log out due to inactivity
    Given I am a valid user on the home page
    And I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    And I select the desired "<autoLogOffTime>" from the auto log off page
    Then I should see a log off alert 15 seconds before the "<autoLogOffTime>" auto logoff time
    And I must be logged out of the app after the selected auto logoff time
    When I select back to logon button on the logout page
    And I am a valid user on the home page
    And I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    And I select the desired "<inactivityTime>" from the auto log off page
    Examples:
      | autoLogOffTime | inactivityTime |
      | 1 Minute       | 10 Minute      |

  @uatDaily
  Scenario: RNGA validate share dealing
    Given I am a valid user on the home page
    When I select apply tab from bottom navigation bar
    And I close the opened Featured option from product hub menu
    Then "Share dealing" category should display the below products
      | productLabels      |
      | shareDealingOption |
      | marketsAndInsights |

  @uatDaily
  Scenario: RNGA validate insurance
    Given I am a valid user on the home page
    When I select apply tab from bottom navigation bar
    And I close the opened Featured option from product hub menu
    Then "Insurance" category should display the below products
      | productLabels   |
      | homeInsurance   |
      | carInsurance    |
      | vanInsurance    |
      | travelInsurance |

  @webview
  Scenario: RNGA Spending Rewards
    Given I am a valid user on the home page
    When I select the view all offers button from home page
    Then I should be on the spending rewards webview page
    And I should see below options in the everyday offers page
      | fieldEveryDayOffersScreen |
      | expiredOffers             |
      | settings                  |
      | findOutMore               |
    And I should be on New Tab selected by default
    When I select Active tab on everyday offers main page
    Then I should be on Active tab
    When I select expired offers option on everyday offers main page
    Then I should be on expired offers page
    When I select everyday offers link to navigate everyday offers main page
    And I select setting option from everyday offers main page
    Then I should be on everyday offers setting page
    When I select everyday offers link to navigate everyday offers main page
    And I select find out more option from everyday offers main page
    Then I should be on find out more everyday offer page

  @webview
  Scenario: RNGA View Inbox
    Given I am a valid user on the home page
    When I navigate to inbox page via more menu
    Then I should be on the inbox webview page
    And I select the message to view
    And I choose View PDF to see the message
    And I navigate back
    And I select the message to view
    And I choose to archive message in Inbox
    And I undo archive message in Inbox
    And I verify the Undo Archive Message

  @webview
  Scenario: RNGA View Inbox of Current and Saving accounts
    Given I am a valid user on the home page
    When I navigate to inbox page via more menu
    Then I should be on the inbox webview page
    And I select inbox folders on inbox webview page
    And I select current account from inbox folders
    And I see that mails for selected current account are filtered on inbox webview page
    And I select inbox folders on inbox webview page
    And I select instantSaving account from inbox folders
    And I see that mails for selected instantSaving account are filtered on inbox webview page

  @webview
  Scenario Outline: RNGA Apply for Loan and get decline decision.
    Given I am a valid user on the home page
    When I select apply tab from bottom navigation bar
    Then I should be on the product hub page
    When I select the loan calculator of the Loans option in the product hub page
    Then I should be on the apply loan page
    When I select continue button to apply loan
    Then I should be on the loan purpose page
    When I select Loan purpose option on the loan purpose page
    And  I select continue button to apply loan on loan purpose page
    Then I should be on the loan calculator page
    When I enter loan <amount> and <term> for calculating my loan eligibility on loan calculator page
    And  I select calculate button on loan calculator page
    Then I should see the future change option on loan calculator page
    When I select future change option on loan calculator page
    And  I select continue application on loan calculator page
    Then I should be on your application page
    And  I select employment status drop down on your application page
    And  I select regular monthly income option on your application page
    And  I select spending and expenses closest match option
    And  I enter monthly income <income> on your application page
    And  I enter dependent <dependents> on your application page
    And  I select childcare cost option on your application page
    And  I select contact details closest match option
    When I select terms and conditions and submit the loan application to check eligibility
    Then I should be on loan decline page
    Examples:
      | amount | term | income | dependents |
      | 12000  | 12   | 50     | 4          |

  @webview
  Scenario: RNGA Book appointment for Loan
    Given I am a valid user on the home page
    When I select apply tab from bottom navigation bar
    Then I should be on the product hub page
    When I select the loan calculator of the Loans option in the product hub page
    Then I should be on product hub loan page
    When I select Book an Appointment option on product hub loan page
    Then I should be on find a branch page
    When I enter postCode and select find a branch
    Then I should be shown branch postCode results
    When I select and continue on find a branch page
    Then I should be on choose an appointment time page

  @webview
  Scenario: RNGA Click on Help and Support from Apply Overdraft page
    Given I am a valid user on the home page
    When I select apply tab from bottom navigation bar
    Then I should be on the product hub page
    When I select overdrafts option in the product hub page
    And I navigate to product hub overdraft page
    And I should see help section on apply overdraft page
    And I select need help button on apply overdraft page
    Then I should see the below details in help and support section
      | optionsinHelpandSupport   |
      | callDetailsLabel          |
      | bookAppointmentLabel      |
      | findYourNearestBranchLink |
      | fromOutsideUk             |
      | ukNumber                  |
      | outsideUKNumber           |
    And I select find your nearest branch on apply overdraft page
    And I select leave app on apply overdraft page

  @webview
  Scenario: RNGA Apply for Access savings product
    Given I am a valid user on the home page
    When I select apply tab from bottom navigation bar
    Then I should be on the product hub page
    When I select the Compare savings accounts of the Savings in the product hub page
    And I select saving account on compare savings account page
    And I select Accept All cookies in webview
    And I select apply now to open savings account
    And I select terms and conditions of savings account openings
    And I select Open Account on Savings Account Openings Page
    Then I should see that saving account is open
    And I check account number and sort code are displayed on success page and home page

  @webview
  Scenario: RNGA Renew Saving Account.
    Given I am a valid user on the home page
    When I select the action menu of saving account
    And I select Renew your savings account option from the action menu
    Then I should be on renew savings account page
    When I choose the renew saving account and proceed to renew it
    And I select apply now to renew savings account
    Then I should be on confirm your selection page
    When I select terms and conditions to renew savings account
    And I confirm to renew saving account
    Then I should be on renewed saving account success page

  @webview
  Scenario: RNGA Apply for Cash ISA savings product.
    Given I am a valid user on the home page
    When I select apply tab from bottom navigation bar
    Then I should be on the product hub page
    When I select the Compare savings accounts of the Savings in the product hub page
    And I select cashISA account on compare savings account page
    And I select Accept All cookies in webview
    And I select apply now to open savings account
    And I select terms and conditions of savings account openings
    And I select Open Account on Savings Account Openings Page
    Then I should see that cashISA account is open
    And I check account number and sort code are displayed on success page and home page

  @webview
  Scenario Outline: RNGA Add International Beneficiary
    Given I am a valid user on the home page
    And  I navigate to current account on home page
    When I select the "international payment" option in the action menu of current account
    Then I should be on pay and transfer page
    When I select the add a new international recipient on the international payment webview page
    Then I should be on country of recipients bank page
    When I select country recipient bank details
    Then I should be on select country page
    And  I search and select the country with name <selectCountry>
    And  I enter recipient name <recipientName>
    And  I enter recipient bank address <recipientBankAddress>
    And  I enter recipient BIC code <BICcode>
    And  I enter recipient IBAN details <IBAN>
    When I continue further to add International recipient
    Then I should be on payment currency warning page
    When I select the currency change warning OK button on payment currency warning page
    Then I should see new recipient details in to account with the IBAN details <IBAN>
    And  I enter the IP amount <amount> to be sent
    And  I select sending recipient <currency> to be sent
    And  I enter IP <reference> details
    When I select continue on International Payment Page
    Then I should be on review payment page
    And  I select option to receive EIA call on <number> to add International recipient
    When I select confirm button on confirm international payment page
    Then I am shown International beneficiary Successful page
    Examples:
      | BICcode  | recipientBankAddress | IBAN                   | recipientName   | amount|reference |number        |currency|selectCountry|
      | LOYDJEH1 | LBG International    | GB95LOYD30166334615701 | Mr CoreUAT Test | 1.01  |UATIP-7722|+44744****480 |EUR     | JERSEY      |

  @webview
  Scenario: RNGA Delete International Beneficiary
    Given I am a valid user on the home page
    And  I navigate to current account on home page
    When I select the "international payment" option in the action menu of current account
    Then I should be on pay and transfer page
    When  I select international recipient button on pay and transfer page
    Then I should be on your international recipients page
    When  I select delete recipient button on your international recipients page
    Then I should be on delete international recipients page
    When I select delete beneficiary cross icon button on delete international recipients page
    Then I should verify select international recipient button is not visible on page

  @webview
  Scenario Outline: RNGA Make International Payment
    Given I am a valid user on the home page
    And I navigate to current account on home page
    When I select the "international payment" option in the action menu of current account
    Then I should be on the international payment webview page
    When I select International Payment Recipients
    And I select Make a Payment to a first International Payment Recipient
    And I enter the IP amount <amount> to be sent
    And I select continue on International Payment Page
    Then I should see confirm International Payment Page
    When I select terms and conditions on confirm international payment page
    And I enter correct password in IP payment confirmation page
#    And I select confirm button on confirm international payment page
#    Then I verify successful International Payment
    Examples:
      | amount |
      | 2      |

# Test account must have phone number matching device
  @webview
  Scenario Outline: RNGA P2P registration
    Given I am a valid user on the home page
    When I navigate to settings page from home page
    And I select pay a contact settings from the settings menu
    Then I should be on the register for pay a contact page
    When I choose to register for pay a contact
    And I capture and enter the 4 digit pin after <miliseconds> for eia call in webview
    Then I should see registration successful screen
    And I deregister the mobile number from P2P
    Examples:
      | miliseconds |
      | 17000       |

  @pending
  Scenario: RNGA Change of Address from Support hub
    Given I am a valid user on the home page
    When I select support tab from bottom navigation bar
    And I select "change of address" option in the support hub page
    Then I should be on the your personal details page
    And I should be shown the list of personal details
      | personalDetail |
      | Full Name      |
      | User ID        |
      | Mobile Number  |
      | Home Number    |
      | Work Number    |
      | Address        |
      | Postcode       |
      | Email address  |
    When I choose to update address from the personal details
    Then I should be on the address update page
    And I should see an option to search my new address using postcode
    When I proceed to call customer care
    Then I should be on the secure call page
    When I go back to previous page
    And I replace existing with new post code of my new address
    And I perform a postcode search to see the matching addresses
    Then I should see list of addresses for entered postcode
    When I select my new address
    Then I should be asked to confirm my new address
    And I should see my new address on Map
    And I should see my new address selected for confirmation
    And I should see my new address is in title case
    When I confirm my new address
    Then I should see final instructions before updating my address
    When I confirm to update my new address
    Then I should see address change successful message
    And I can proceed to homepage after my address got updated
    When I proceed to homepage after my address got updated
    Then I should be on the home page

  @webview
  Scenario: RNGA Order Paper Statements
    Given I am a valid user on the home page
    When I select the "order paper statements" option in the action menu of current account
    Then I should be able to view order paper statements webview page
    When I select statement and proceed to order paper statements
    Then I should be on confirm your order page
    And should see below option on confirm your order page
      | fieldsInDetailScreen |
      | total                |
      | changeOrder          |
      | deliverAddress       |
      | updateAddress        |
      | confirmAndPay        |
    When I select choose an account from make payment from panel
    Then I should see details of current account in make payment from panel

# Below script will work on only Android devices
  @webview
  Scenario: RNGA Lost and Stolen Card from Card management, More and Support Tab
    Given I am a valid user on the home page
    When I navigate to creditCard account on home page
    And I select the action menu of creditCard account
    And I select "Card Management" option in the action menu of creditCard account
    Then I should be on card management page
    And I should see below options on card management page
      | fieldsInDetailScreen  |
      | freezCardTransactions |
      | lostAndStolenCards    |
      | replaceCardAndPin     |
    When I select "Lost and stolen cards" option in card management page
    Then I should be on the lost and stolen page
    When I navigate to card management page via more menu
    Then I should be on card management page
    When I select "Lost and stolen cards" option in card management page
    Then I should be on the lost and stolen page
    When I select support tab from bottom navigation bar
    And I select "lost and stolen card" option in the support hub page
    Then I should be on the lost and stolen page
    When I select lost option on the lost and stolen page
    And I select cards option on the lost and stolen page
    And I select to continue on the lost and stolen page
    Then I should be on the card replace and pin details confirmation page
    And I should be shown below fields on the card replace and pin details confirmation page
      | fieldsInDetailScreen |
      | password             |
      | continue             |

  # Below script will work on only Android devices
  @webview
  Scenario: RNGA Freeze Card transactions from Card Management
    Given I am a valid user on the home page
    When I navigate to card management page via more menu
    Then I should be on card management page
    When I select "Freeze card transactions" option in card management page
    Then I should be on the Freeze card transactions page
    When I select all options available on Freeze card transactions page
    And I select report a card lost or stolen now option on Freeze card transactions page
    Then I should be on the lost and stolen page
    When I select lost option on the lost and stolen page
    And I select cards option on the lost and stolen page
    And I select to continue on the lost and stolen page
    Then I should be on the card replace and pin details confirmation page
    And I should be shown below fields on the card replace and pin details confirmation page
      | fieldsInDetailScreen |
      | password             |
      | continue             |

# Below script will work on only Android devices
  @webview
  Scenario: RNGA Replace Card and Pin from Card management, More and Support Tab
    Given I am a valid user on the home page
    When I navigate to creditCard account on home page
    And I select the action menu of creditCard account
    And I select "Card Management" option in the action menu of creditCard account
    Then I should be on card management page
    And I should see below options on card management page
      | fieldsInDetailScreen  |
      | freezCardTransactions |
      | lostAndStolenCards    |
      | replaceCardAndPin     |
    When I select "replacement card and pin" option in card management page
    Then I should be on the replacement card and pin page
    When I navigate to card management page via more menu
    Then I should be on card management page
    When I select "replacement card and pin" option in card management page
    Then I should be on the replacement card and pin page
    When I select support tab from bottom navigation bar
    And I select "replacement card and pin" option in the support hub page
    Then I should be on the replacement card and pin page
    When I select replace cards and pin options on the replacement cards and pin page
    And I select to continue on the lost and stolen page
    Then I should be on the card replace and pin details confirmation page
    And I should be shown below fields on the card replace and pin details confirmation page
      | fieldsInDetailScreen |
      | password             |
      | continue             |

  @webview
  Scenario: View PDF statement for Credit Card
    Given I am a valid user on the home page
    When I navigate to creditCard account on home page
    And I select the action menu of creditCard account
    And I select pdfStatementsOption from contextual menu
    Then I should be on the PDF statements webview page for creditCard account
    And I should be able to view the PDF Statements for current year

  @webview
  Scenario: RNGA Outbound Message
    Given I am a valid user on the home page
    When I navigate to settings page from home page
    Then I should see the Notifications option
    And I select the real time alerts option from the Settings page
    And I should be on the notifications setting page
    When I select the smart alerts enable in notification page
    Then I should shown smart alerts toggle button is enabled

  @webview
  Scenario: RNGA Gambling block from Card Management
    Given I am a valid user on the home page
    When I navigate to card management page via more menu
    Then I should be on card management page
    When I select "Freeze card transactions" option in card management page
    Then I should be on the Freeze card transactions page
    When I select freeze gambling option in Freeze card transactions page
    Then I should be on Ready to Freeze Gambling page
    And I select Freeze button in Ready to Freeze Gambling page
    And I select Cancel button in Almost there popup in Freeze Gambling page

  @uatDaily
  Scenario Outline: RNGA Rename account
    Given I am a valid user on the home page
    And I select the "rename account" option in the action menu of current account
    Then I should be on Rename account page
    And I should see current account name in text field
    And the Rename button is disabled
    When I enter new account name <newAccountName> in the text field
    And I select Rename button
    Then confirmation box is displayed
    When I select Back to your accounts button on confirmation box
    Then I should be on the home page
    And new account name <newAccountName> is displayed
    And I reset the account name from <newAccountName> to its original name of current
    When I select the "rename account" option in the action menu of instantSaving account
    Then I should be on Rename account page
    And I should see instantSaving account name in text field
    And the Rename button is disabled
    When I select dismiss modal using close button
    Then I should be on the home page
    When I select the "rename account" option in the action menu of isa account
    Then I should be on Rename account page
    And I should see isa account name in text field
    And the Rename button is disabled
    When I enter new account name <newAccountName> in the text field
    And I select Rename button
    Then confirmation box is displayed
    When I select Back to your accounts button on confirmation box
    Then I should be on the home page
    And new account name <newAccountName> is displayed
    And I reset the account name from <newAccountName> to its original name of isa
    Examples:
      | newAccountName  |
      | Account Renamed |

  @uatDaily
  Scenario: RNGA Change Password from Support Hub and Settings Menu
    Given I am a valid user on the home page
    When I select support tab from bottom navigation bar
    And I select "reset password" option in the support hub page
    Then I should be on the password reset page
    When I navigate to settings page from home page
    And I select on Security settings link on settings page
    And I select the forgotten password option from the security settings page
    Then I should be on the password reset page
    When I enter my new valid password in the reset password page
#    And I select the submit button in the reset password page
#    Then I should see a password updated pop up in the reset password page
#    And I should be on the logout page
#    When I select back to logon button on the logout page
#    And I am a valid user on the home page

  @uatDaily
  Scenario: RNGA App de-register
    Given I am a valid user on the home page
    When I navigate to settings page from home page
    And I select the reset app option from the settings page
#    And I successfully reset the app
#    Then I should be on the logout page

  @webview
  Scenario: RNGA Auth PCA - Change Account Type - Another current Account
    Given I am a valid user on the home page
    When I select the "change account type" option in the action menu of current account
    Then I should be on the change account type page
    When I select change this account button on account type page
    Then I should be on the update your email address page
    When I select email correction option on update your email address page
    And I select continue button on update your email address page
    And I select continue button on before you continue page
    Then I should be on select account you want page
    When I select find out more button on select account you want page
    Then I should be on the account you want page
    When I select continue button on the account you want page
    Then I should be on the your account offer page
    When I tick the option to complete application on your account offer page
    And I select complete application button on your account offer page
    Then I verify current account open successful message

  @pending @uatInitiative
  Scenario Outline: Verify app Warn Screens for unenrolled user
    Given I am a valid user
    When I navigate to the login screen
    And I login with my username and password
    And I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to update the app from the warn screen
    And I navigate to app store screen
    And I validate app name in app store
    And I navigate back to app from app store
    And I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to continue without updating from the warn screen
    And I enter correct MI
    And I choose to receive EIA call on <number>
    And I choose call me now option
    And I capture and enter the 4 digit pin for eia call
    And I select continue button on the enrolment confirmation page
    And I choose my security auto logout settings
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page
    Examples:
      | number |
      | Work   |

  @pending @uatInitiative
  Scenario: Verify app Warn Screens for enrolled user clicking on continue without updating
    Given I am a valid user on the home page
    Then I should be on the home page
    When I navigate to logoff via more menu
    Then I should be on the logout page

  @pending @uatInitiative
  Scenario: Verify app Warn Screens for enrolled user and clicking on update the app
    # first step to be defined whenever needed
#    Given I am a valid user on the home page for light login
    Given I am a valid user on the home page
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to update the app from the warn screen
    And I navigate to app store screen
    And I validate app name in app store
    And I navigate back to app from app store
    And I navigate to the warn screen

  @pending @uatInitiative
  Scenario: Verify app Ban Screens for unenrolled user
    Given I am a valid user
    When I navigate to the login screen
    And I login with my username and password
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I choose to update the app in the ban screen
#    And I navigate to app store screen
#    Then I validate app name in app store
    And I navigate back to app from app store
    And I navigate to the ban screen
    And I choose to use the Mobile Browser Banking
    And I navigate to mobile browser
    Then I validate url from mobile browser
    When I navigate back to app from mobile browser
    And I navigate to the ban screen

  @pending @uatInitiative
  Scenario: Verify app Ban Screens for enrolled user
    Given I am a valid user on the home page
#    When I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I choose to update the app in the ban screen
#    And I navigate to app store screen
#    Then I validate app name in app store
    And I navigate back to app from app store
    And I choose to use the Mobile Browser Banking
    And I navigate to mobile browser
    Then I validate url from mobile browser
    When I navigate back to app from mobile browser
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking

  @webview
  Scenario: RNGA Opt in reward extras offer for opt out URCA customers
    Given I am a valid user on the home page
    When I select the "Add your Reward Extras" option in the action menu of ultimateReward account
    Then I verify details on add reward extra landing page
    And I expand offer accordion on add reward extra landing page
    And I select choose this option for an offer on add reward extra landing page
    And I should be on the lets get started page
    When I select the continue button on lets get started page
    Then I should be on the Your Email page
    And I select Yes button for email address confirmation on Your Email page
    When I select the continue button on Your Email page
    Then I should be on the Legal Bits page
    And I select the I confirm checkbox in the legal bits add offer page

  @webview
  Scenario: RNGA Track your Reward Extras for URCA customers
    Given I am a valid user on the home page
    When I select the "Track your Reward Extras" option in the action menu of ultimateReward account
    Then I should be on the Reward Extra Tracker Page
    And I select the show my offer progress in reward extra page

  @webview
  Scenario: RNGA Add Your Reward Extras for RCA customers
    Given I am a valid user on the home page
    When I select the "Add your Reward Extras" option in the action menu of current account
    Then I verify details on add reward extra landing page
    And I expand offer accordion on add reward extra landing page
    And I verify the offer unavailability message for RCA

  @webview
  Scenario: RNGA User should able to change paperless preferences
    Given I am a valid user on the home page
    When I navigate to settings page via more menu
    And I navigate to online and paper preferences from the settings page
    Then I should be on the manage your online and paper preference webview page
    When I select update preference from manage your online and paper preference webview page
    Then I am on confirm your paperless preferences webview page
    When I accept the terms & condition and submit order paper preference
    Then I should see update preference confirmation page

  @pending @uatInitiative
  Scenario: RNGA Direct debits validation
    Given I am a valid user on the home page
    When I navigate to current account on home page
    And I select the "standing order & direct debit" option in the action menu of current account
    Then I am shown a message that I have no direct debits set up under the relevant header

  @uatInitiative
  Scenario: TC_06 View debit and credit transaction in credit card statements
    Given I am a current user on the home page
    When I navigate to creditCard account to validate accountNumber and select creditCard account
    And I should be on the recent of statements page
    Then I should be displayed creditCard account name action menu and last four digits of creditCard number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
      | availableCredit     |
      | creditLimit         |
      | overDueAmount       |
    And I should see transactions since message on recent tab
    And I should see debit transaction with below details in credit card statements
      | detail                      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    When I scroll down on the transactions
    Then I should be displayed current balance in the header alongside account Name
    And I should not be displayed account tile in statements page
    And I should be displayed month tabs in place of account tile
    When I scroll back to the top of my transactions
    Then The current balance will no longer display in the header and the account tile will reappear
    And I swipe to previous 3 months transactions on the account statement page
    And The previously billed info panel appears

  @webview
  Scenario Outline: RNGA Apply Overdraft and get decline decision.
    Given I am a valid user on the home page
    When I select overdraft option from the current account action menu
    Then I navigate to product hub overdraft page
    When I select apply tab from bottom navigation bar
    Then I should be on the product hub page
    When I select overdrafts option in the product hub page
    And I navigate to product hub overdraft page
    And I select let me just type link on the overdraft page
    And I enter overdraft amount <amount> on the product hub overdraft page
    And I enter overdraft days <days> on the product hub overdraft page
    And I select apply on product hub overdraft page
    And I navigate to Overdrafts details page
    And I select pay back button on overdraft page
    And I select employment status <status> on overdraft page
    And I enter monthly after tax income <aftertax> on overdraft page
    And I enter monthly outgoings <expenses> on overdraft page
    And I enter childcare cost <childcare> on overdraft page
    And I submit the application for overdraft
    Then I should see overdraft decline page
    Examples:
      | aftertax | expenses | childcare | status  | amount | days |
      | 1000     | 300      | 100       | Retired | 1000   | 2    |

  @webview
  Scenario Outline: RNGA Apply Overdraft for Joint Account and get decline decision.
    Given I am a valid user on the home page
    When I select overdraft option from the current account action menu
    Then I navigate to product hub overdraft page
    When I select apply tab from bottom navigation bar
    Then I should be on the product hub page
    When I select overdrafts option in the product hub page
    And I navigate to product hub overdraft page
    And I select let me just type link on the overdraft page
    And I enter overdraft amount <amount> on the product hub overdraft page
    And I enter overdraft days <days> on the product hub overdraft page
    And I select apply on product hub overdraft page
    And I navigate to Overdrafts details page
    And I select pay back button on overdraft page
    And I select life changing event
    And I select employment status <status> on overdraft page
    And I enter monthly after tax income <aftertax> on overdraft page
    And I enter monthly outgoings <expenses> on overdraft page
    And I enter childcare cost <childcare> on overdraft page
    And I submit the application for overdraft
    And I navigate to Overdrafts partner details page
    And I select life changing event
    And I select employment status <status> on overdraft page
    And I enter overdraft amount for joint account  <aftertax> on product hub overdraft page
    And I enter monthly outgoings for joint account <expenses> on product hub overdraft page
    And I enter childcare cost for joint account <childcare> on product hub overdraft page
    And I submit the application for overdraft
    Then I should see overdraft decline page
    Examples:
      | aftertax | expenses | childcare | status  | amount | days |
      | 100      | 300      | 100       | Retired | 1000   | 2    |

  Scenario Outline: TC_03 Pay Credit Card through bottom navigation bar by Another UK Bank Account journey
    Given I am a credit card and current account user on the home page
    And I am on the payment hub page
    When I choose the from account as anotherUKBankAccount and to account as masterCreditCard in the payment hub page
    Then I am shown payment date in the payment hub
    When I enter amount greater than the balance of the credit card account in the payment hub page
    Then I should see credit card payment validation error message
    When I dismiss the error message
    And I submit the pccmc with all the mandatory fields with corresponding amount value 0.01
    And I should see the select provider button enabled in the payment hub page
    And I select the select provider button in the payment hub page
    Then I should be on select your bank account page and verify fallowing headers and footer details populated on the page
      | yourBankAccount |
      | bankNotListed   |
      | payUKDebitCard  |
    When I select another UK <bankAccount> from the list displayed
    Then I should see the following details populated on the confirm payment screen
      | confirmPreviewDetails |
      | fromAccountName       |
      | toAccountName         |
      | toSortCode            |
      | amountValue           |
      | paymentMessage        |
    When I select the confirm button on confirm payment page
    Then I should see the fallowing details on approve your payment page
      | approveYourPayment |
      | dataLineInfoOne    |
      | dataLineInfoTwo    |
      | termsAndCondition  |
      | dataPrivacyNotice  |
    When I select check box option to continue the payment on approve your payment page
    And I select continue button on approve your payment page
    Then I should see the go to your bank dialog box on approve your payment page
    When I select the dialog box continue button to finish your payment
    Then I should be on unfinished payment page
    Examples:
      | bankAccount |
      | Barclays    |

  Scenario Outline: Pay Credit Card with future dated payments by another UK Bank Account
    Given I am a valid user on the home page
    And I am on the payment hub page
    When I choose the from account as anotherUKBankAccount and to account as masterCreditCard in the payment hub page
    Then I am shown payment date in the payment hub
    When I submit the pccmc with all the mandatory fields with corresponding amount value 0.01
    And I select calendar picker option for future date payment
    And I select the date as <Days> from today's date from calendar view
    And I select the select provider button in the payment hub page
    Then I should be on select your bank account page
    When I select another UK <bankAccount> from the list displayed
    Then I should see the following details populated on the confirm payment screen
      | previewDetails  |
      | fromAccountName |
      | toAccountName   |
      | toSortCode      |
      | amountValue     |
      | dateValue       |
    When I select the confirm button on confirm payment page
    Then I should see the fallowing hyperlinks on important information page
      | verifyHyperLinks  |
      | termsAndCondition |
      | dataPrivacyNotice |
    When I select check box option to continue the payment on important information payment page
    And I select continue button on important information payment page
    Then I should see the finish your payment dialog box on important information payment page
    When I select the dialog box continue button to finish your payment
    Then I should be on unfinished payment page
    Examples:
      | bankAccount | Days |
      | Wood Bank   | 5    |

  @webview
  Scenario Outline: RNGA customer with NO PCA account is being pushed to unauth PCA journey and receive refer decision
    Given I am a valid user on the home page
    When I select apply tab from bottom navigation bar
    Then I should be on the product hub page
    And  I select the Current accounts in the product hub page
    When I select find out more button on select account you want page
    Then I should be on the account you want page
    When I select continue button on the account you want page
    And  I select continue button on before you start page
    Then I should be on your details page
    And  I select your title to apply account on your details page
    And  I enter your first name <firstName> to apply account on your details page
    And  I enter your middle name <middleName> to apply account on your details page
    And  I enter your last name <lastName> to apply account on your details page
    And  I enter your dob date <dd> to apply account on your details page
    And  I enter your dob months <mm> to apply account on your details page
    And  I enter your dob years <yyyy> to apply account on your details page
    And  I select your gender type to apply account on your details page
    And  I select your marital status to apply account on your details page
    And  I select your nationality to apply account on your details page
    And  I select your country of birth to apply account on your details page
    And  I select where you live to enter the fields details to apply account on your details page
    And  I enter where you live house number <housenumber> field details to apply account on your details page
    And  I enter where you live house name <housename> field details to apply account on your details page
    And  I enter where you live Street <street> field details to apply account on your details page
    And  I enter where you live City <City> field details to apply account on your details page
    And  I enter where you live County <County> field details to apply account on your details page
    And  I enter where you live post code <postcode> field details to apply account on your details page
    And  I select when did you move to your current address details to apply account on your details page
    And  I enter phone number <mobile> details to apply account on your details page
    And  I enter email address <email> details to apply account on your details page
    And  I select your marketing choices details
    And  I enter income and expense <monthly>, <mortgages> and <childCareCost> details to apply account on your details page
    When I select continue button on your details page
    Then I should be on review your details page
    When  I select continue button on review your details page
    Then I should be on your account offer page
    And  I select confirm checkbox to open this account
    When I select continue button on your account offer page
    Then I should verify refer decision message on page
    Examples:
      | firstName | middleName | lastName | dd | mm | yyyy | postcode | housenumber | housename    | street      | City   | County         | mobile     | email                             | monthly | mortgages | childCareCost |
      | Simon     | lloyds     | Turner   | 01 | 01 | 1970 | EC1Y 4XX | 48          | The Pentagon | Chiswell St | London | United Kingdom | 7448027480 | nilesh.kaingade@lloydsbanking.com | 3000    | 123       | 4000          |

  @webview
  Scenario Outline: RNGA customer with PCA account is being pushed to unauth PCA journey and receive refer decision
    Given I am a valid user on the home page
    When I select apply tab from bottom navigation bar
    Then I should be on the product hub page
    And  I select the Current accounts in the product hub page
    And  I select to open another current account
    When I select find out more button on select account you want page
    Then I should be on the account you want page
    And  I select continue button on the account you want page
    When I select continue button on before you start page
    Then I should be on your details page
    And  I select your title to apply account on your details page
    And  I enter your first name <firstName> to apply account on your details page
    And  I enter your middle name <middleName> to apply account on your details page
    And  I enter your last name <lastName> to apply account on your details page
    And  I enter your dob date <dd> to apply account on your details page
    And  I enter your dob months <mm> to apply account on your details page
    And  I enter your dob years <yyyy> to apply account on your details page
    And  I select your gender type to apply account on your details page
    And  I select your marital status to apply account on your details page
    And  I select your nationality to apply account on your details page
    And  I select your country of birth to apply account on your details page
    And  I select where you live to enter the fields details to apply account on your details page
    And  I enter where you live house number <housenumber> field details to apply account on your details page
    And  I enter where you live house name <housename> field details to apply account on your details page
    And  I enter where you live Street <street> field details to apply account on your details page
    And  I enter where you live City <City> field details to apply account on your details page
    And  I enter where you live County <County> field details to apply account on your details page
    And  I enter where you live post code <postcode> field details to apply account on your details page
    And  I select when did you move to your current address details to apply account on your details page
    And  I enter phone number <mobile> details to apply account on your details page
    And  I enter email address <email> details to apply account on your details page
    And  I select your marketing choices details
    And  I enter income and expense <monthly>, <mortgages> and <childCareCost> details to apply account on your details page
    When I select continue button on your details page
    Then I should be on review your details page
    When I select continue button on review your details page
    Then I should be on your account offer page
    And  I select confirm checkbox to open this account
    When I select continue button on your account offer page
    Then I should verify refer decision message on page
    Examples:
      | firstName | middleName | lastName | dd | mm | yyyy | postcode | housenumber | housename    | street      | City   | County | mobile     | email                             | monthly | mortgages | childCareCost |
      | Simon     | lloyds     | Turner   | 01 | 01 | 1970 | EC1Y 4XX | 48          | The Pentagon | Chiswell St | London | UK     | 7448027480 | nilesh.kaingade@lloydsbanking.com | 3000    | 123       | 4000          |

  @uatInitiative
  Scenario: TC_001_Customer able to see Covid Hub under support hub
    Given I am a valid user on the home page
    When I select support tab from bottom navigation bar
    And I select Corona virus help & support Hub link
    Then I should be on covid hub native page

  @webview
  Scenario Outline: RNGA Customer is able to upgrade Reward Current Account to Ultimate Reward Current Account
    Given I am a valid user on the home page
    When I select the "change account type" option in the action menu of current account
    Then I should be on the change account type page
    When I select change this account button on account type page
    Then I should be on the update your email address page
    When I select email correction option on update your email address page
    And I select continue button on update your email address page
    And I select continue button on before you continue page
    Then I should be on select account you want page
    When I select find out more button on select account you want page
    Then I should be on the account you want page
    When I select continue button on the account you want page
    Then I should be on before your continue page for RCA Account upgrade
    And I select age and residence confirmation options on before you continue page for RCA Account upgrade
    When I select continue button on before you continue page for RCA Account upgrade
    Then I should be on your needs page
    And I verify family travel insurance header
    And I select options for family travel insurance on your needs page
    When I select on continue button on your needs page
    Then I verify vehicle breakdown cover header
    And I select options for vehicle breakdown cover on your needs page
    When I select on continue button on your needs page
    Then I verify mobile phone insurance header
    And I select options for mobile phone insurance on your needs page
    When I select on continue button on your needs page
    Then I verify home emergency cover header
    And I select options for home emergency cover on your needs page
    When I select on continue button on your needs page
    Then I validate summary header on your needs page
    When I select on continue button on your needs page
    Then I should be on your eligibility page
    When I select continue button on your eligibility page for RCA Account upgrade
    Then I verify family travel insurance header
    And I select date of birth <day> <month> <year> and resident for family member
    And I select continue button on your eligibility page for RCA Account upgrade
    And I select medical condition buttons for self and partner
    When I select continue button on your eligibility page for RCA Account upgrade
    Then I select vehicle breakdown final check button on your eligibility page
    When I select continue button on your eligibility page for RCA Account upgrade
    Then I select mobile phone insurance final check button on your eligibility page
    When I select continue button on your eligibility page for RCA Account upgrade
    Then I select home emergency cover one last check button on your eligibility page
    When I select continue button on your eligibility page for RCA Account upgrade
    Then I validate summary header for your eligibility page
    When I select continue button on your eligibility page for RCA Account upgrade
    Then I should be on the summary page for RCA Account upgrade
    When I select continue button on summary page for RCA Account upgrade
    Then I should be on the your account offer page
    When I select the option to complete application on your account offer page
    And I select complete application button on your account offer page
    Then I verify current account open successful message
    When I select the add offer to receive reward extras on thank you page
    Then I verify details on add reward extra landing page
    Examples:
      | day | month    | year |
      | 01  | January  | 1970 |

  @uatInitiative
  Scenario: RNGA Halifax my extra tile
    Given I am a valid user on the home page
    When I am shown the reward hub entry point tile on home page
    And I select Find out how button on reward hub entry point tile
    Then I should be on the halifax reward hub page

  @webview
  Scenario: RNGA View Spending Insights
    Given I am a valid user on the home page
    When I select the "view spending insights" option in the action menu of current account
    Then I should be able to view spending insights webview page
    When I select Debit card in view spending insights webview page
    Then I should be on Debit Card Insight webview page
