@rnga @nga3 @team-platform-engineering @rebootDevice @stub-valid
Feature: Reboot device

  @ios
  Scenario: Install Baseline App
    Given I reinstall baseline app

  Scenario: Reboot device
    Given I reboot my device
