@team-statements @nga3 @rnga @stub-enrolled-valid @ff-travel-disputes-on @sw-travel-disputes-android-on @ff-ios-help-enhancements2-on @sw-travel-disputes-ios-on
Feature: Help with this transaction
  In order for me to take appropriate steps
  As a RNGA user with current & credit card account & posted transactions
  I want to be able to know what do to if I am unsure about a transaction

  #STAMOB- 1991, 1995, 2098
  @primary
  Scenario Outline: TC_001 Main Success Scenario: User views help with this transaction for posted transactions
    Given I am an enrolled "current and savings account" user logged into the app
    And I select <type> account tile from home page
    When I select <transaction> on statements page
    And I select the help with this transaction link in VTD screen
    Then I must be displayed the Unsure About Transactions Help and Info screen
    And I should see a list of below cleared transaction help options
      | fieldsInHelpPage         |
      | infoHeading              |
      | dontRecogniseTransaction |
      | <field>                  |
      | expectingRefund          |
      | multipleDates            |
      | covid19Support           |
      | checkTheGuidanceHeading  |
      | disputeMessage           |
      | contactUs                |
    And I select below options to view the related helpDetails page
      | helpPageOptions          |
      | dontRecogniseTransaction |
      | expectingRefund          |
      | multipleDates            |
      | covid19Support           |
    And I select below links under guidance to view related page
      | helpPageOptions          |
      | disputeMessage           |
      | contactUs                |
    Examples:
      | type                                       | field            | transaction                       |
      | current Account With View Transactions     | travelDisputes   | contact Less Purchase             |
      | credit Card Account With View Transactions | noTravelDisputes | chip And Pin Purchase Credit Card |

  #click on weblinks to be tested manually as tap on weblinks in help details page is not working through automation as they are implemented as html hyperlinks
  @manual
  Scenario Outline: TC_002 Main Success Scenario: User views web pages from help details page of posted transactions
    Given I am an enrolled "current and savings account" user logged into the app
    And I select <type> account tile from home page
    When I select one of the posted transaction
    And I select the help with this transaction link in VTD screen
    Then I select below links in the helpDetails page to view respective web pages
      | helpDetailsPageLinks     |
      | dontRecogniseTransaction |
      | expectingRefund          |
      | covid19Support           |
    Examples:
      | type                                       |
      | current Account With View Transactions     |
      | credit Card Account With View Transactions |

  #STAMOB- 1991, 1996, 2099
  @primary
  Scenario Outline: TC_003 Main Success Scenario: User views help with this transaction screen for pending transactions
    Given I am an enrolled "current and savings account" user logged into the app
    And I select <type> account tile from home page
    When I tap on the pending transactions accordion
    And I select <transactionType> on statements page
    And I select the help with this transaction link in VTD screen
    Then I must be displayed the Unsure About Transactions Help and Info screen
    And I should see a list of below pending transaction help options
      | fieldsInHelpPage         |
      | infoHeading              |
      | whatIsPendingTransaction |
      | cancelPendingTransaction |
      | dontRecogniseTransaction |
      | covid19Support           |
      | checkTheGuidanceHeading  |
      | disputeMessage           |
      | contactUs                |
    And I should not see travelDisputes option
    And I select below options to view the related helpDetails page
      | helpPageOptions          |
      | whatIsPendingTransaction |
      | cancelPendingTransaction |
      | dontRecogniseTransaction |
      | covid19Support           |
    And I select below links under guidance to view related page
      | helpPageOptions          |
      | disputeMessage           |
      | contactUs                |
    Examples:
      | type                                   | transactionType      |
      | currentAccountWithPendingTransactions  | pending Cheque       |
      | currentAccountWithPendingTransactions  | pending Direct Debit |
      | creditCardWithPendingDebitTransactions | pending Credit Card  |

  #STAMOB-2350
  #click on weblinks to be tested manually as tap on weblinks in help details page is not working through automation as they are implemented as html hyperlinks
  @manual
  Scenario Outline: TC_004 Main Success Scenario: User views web pages from help details page of pending transactions
    Given I am an enrolled "current and savings account" user logged into the app
    And I select <type> account tile from home page
    When I tap on the pending transactions accordion
    And I select <transactionType> on statements page
    And I select the help with this transaction link in VTD screen
    Then I select below links in the helpDetails page to view respective web pages
      | helpDetailsPageLinks     |
      | dontRecogniseTransaction |
      | covid19Support           |
    Examples:
      | type                                   | transactionType      |
      | currentAccountWithPendingTransactions  | pending Cheque       |
      | currentAccountWithPendingTransactions  | pending Direct Debit |
      | creditCardWithPendingDebitTransactions | pending Credit Card  |

  Scenario Outline: TC_005 Alternative Scenario: User navigates Back to VTD
    Given I am an enrolled "current and savings account" user logged into the app
    And I select <type> account tile from home page
    When I select one of the posted transaction
    And I select the help with this transaction link in VTD screen
    And I select to go back
    Then I should be displayed VTD page of that transaction
    Examples:
      | type                                       |
      | current Account With View Transactions     |
      | credit Card Account With View Transactions |

  @pending
  Scenario Outline: TC_006 Alternative Scenario: User navigates back to Unsure About Transactions Help & Info screen
    Given I am an enrolled "current and savings account" user logged into the app
    And I select <type> account tile from home page
    When I select one of the posted transaction
    And I select the help with this transaction link in VTD screen
    And  I select to contact customer service
    And I select to go back from Click To Call journey
    Then I must be navigated back to the previous screen
    Examples:
      | type                                       |
      | current Account With View Transactions     |
      | credit Card Account With View Transactions |
