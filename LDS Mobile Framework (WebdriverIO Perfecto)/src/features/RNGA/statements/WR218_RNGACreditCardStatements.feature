@nga3 @rnga @team-statements @stub-enrolled-valid @mbnaSuite
Feature: Credit card statement
  In order to view the statements for the nominated credit card
  As a NGA authenticated customer with credit card
  I want to view my credit card transactions for each statement period

  @primary
  Scenario: TC_001 View debit transaction in credit card statements
    Given I am an enrolled "valid" user logged into the app
    When I select creditCard With Credit Transactions account tile from home page
    Then I should see transactions since message on recent tab
    And I should see debit transaction with below details in credit card statements
      | detail                      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |

  @primary
  Scenario: TC_002 View credit transaction in credit card statements
    Given I am an enrolled "credit card" user logged into the app
    When I select creditCard With Credit Transactions account tile from home page
    Then I should see transactions since message on recent tab
    And I should see credit transaction with below details in credit card statements
      | detail                      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
      | firstCreditIndicator        |

  # verification of description second line can not be validated in automation
  @manual
  Scenario: TC_003 Transaction description longer than one line
    Given I am an enrolled "valid" user logged into the app
    When I select creditCard account tile from home page
    Then I should see transactions since message on recent tab
    And I must see credit card transaction description continue on second line

  # MOB3-3163, MOB3-2603 spinner image can not be captured in automation
  @manual
  Scenario: TC_004 Loading spinner does not display
    Given I am an enrolled "valid" user logged into the app
    When I select creditCard account tile from home page
    Then I should see loading indicator whilst the transactions load
    And I should see all the transactions for the selected period once loaded
    And I should not see any loading indicator when I scroll through the transaction list

  # MOB3-3163, MOB3-2603 spinner image can not be captured in automation
  @manual
  Scenario: TC_005 Loading spinner displays (>50<100 transactions)
    Given I am an enrolled "valid" user logged into the app
    When I select creditCard account tile from home page
    And I scroll past the first 50 transactions in the statement
    Then I should see a loading spinner icon briefly whilst transaction are loading
    And I should see the next 50 transactions

  # MOB3-3163, MOB3-2603 spinner image can not be captured in automation
  @manual
  Scenario: TC_006 Loading spinner displays (100+ transactions)
    Given I am an enrolled "valid" user logged into the app
    When I select creditCard account tile from home page
    And I scroll past the first 100 transactions in the statement
    Then I should see a loading spinner icon briefly whilst transaction are loading
    And I should see the next 50 transactions

  @primary
  Scenario: TC_007 View previously billed period information panel
    Given I am an enrolled "credit card" user logged into the app
    When I select creditCard With Credit Transactions account tile from home page
    And I swipe to previous 3 months transactions on the account statement page
    Then The previously billed info panel appears

  # MOB3-3376
  @secondary
  Scenario Outline: TC_008 view recent credit card transactions via different routes
    Given I am an enrolled "valid" user logged into the app
    When  I navigate to creditCard account on home page
    And I navigate via <option> to see the credit card transaction details of creditCard account
    Then I should see transactions since message on recent tab
    Examples:
      | option          |
      | account         |
      | actions menu    |

  # MOB3-7361
  @secondary
  Scenario: TC_009 Customer successfully views transactions of previous months
    Given I am an enrolled "credit card" user logged into the app
    When I select creditCard With Credit Transactions account tile from home page
    And I swipe to previous 3 months transactions on the account statement page
    Then I should see credit transaction with below details in credit card statements
      | detail               |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |

  # MOB3-3376, MOB3-3372, MOB3-4269, MOB3-4246
  @secondary
  Scenario: TC_010 View credit card transaction via actions menu
    Given I am an enrolled "credit card" user logged into the app
    And I navigate to creditCard account on home page
    When I select the "view transaction" option in the action menu of creditCard account
    Then I should see transactions since message on recent tab
    And I should see debit transaction with below details in credit card statements
      | detail                      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |

  # MOB3-4251, MOB3-4234
  @secondary
  Scenario: TC_011 View end of Transaction message HC-022
    Given I am an enrolled "credit card" user logged into the app
    When I select creditCard With Credit Transactions account tile from home page
    Then I should see transactions since message on recent tab
    And  I should scroll down to the end of the transactions to view creditEndOf message

  # MOB3-4170, MOB3-3499 unique id needs to be added for iOS month tab MOB3-5817
  @manual
  Scenario: TC_012 tapping through months without year
    Given I am an enrolled "credit card" user logged into the app
    When I select "creditCard" account tile from home page
    Then I should see transactions since message on recent tab
    When I click on previous month tab
    Then I should see previous month transaction details in statement page
    And I should see months tabs without year

  # MOB3-4170, MOB3-3499 unique id needs to be added for iOS month tab MOB3-5817
  @manual
  Scenario: TC_013 tapping through months with year
    Given I am an enrolled "credit card" user logged into the app
    When I select "creditCard" account tile from home page
    Then I should see transactions since message on recent tab
    When I click on previous month tab
    Then I should see previous month transaction details in statement page
    When I navigate to previous year tabs
    And I should see months tabs with year

  # MOB3-4174, MOB3-3724
  @environmentOnly
  Scenario: TC_014 swiping through months without year
    Given I am an enrolled "credit card" user logged into the app
    When I select creditCard account tile from home page
    And I should see month tabs without year

 # MOB3-174, MOB3-3724
  @manual
  Scenario: TC_015 swiping through months with year
    Given I am an enrolled "credit card" user logged into the app
    When I select "creditCard" account tile from home page
    Then I should be on the recent statements tab
    When I swipe right on recent month transaction
    Then I should see previous month transaction details in statement page
    When I navigate to previous year tabs
    And I should see months tabs with year

# MOB3-2609, MOB3-4348
  @secondary
  Scenario: TC_016 No transactions for previous months HC-018
    Given I am an enrolled "credit card with no previous month transactions" user logged into the app
    When I select creditCardWithNoTransactions account tile from home page
    And I swipe to previous 1 months transactions on the account statement page
    Then I should see no transactions message
    And I should not see transactions header

# MOB3-2609, MOB3-4348
  @secondary
  Scenario: TC_017 Verify no transactions to display in the current month HC-018
    Given I am an enrolled "credit card with no current month transactions" user logged into the app
    When I select creditCardWithNoTransactions account tile from home page
    Then I should see no transactions message
    And I should not see transactions header

  # MOB3-5206, MOB3-4259
  @manual
  Scenario: TC_018 Verify error overlay when statements are failed to load
    Given I am an enrolled "credit card" user logged into the app
    When I select creditCard account tile from home page
    And I navigate to a tab unable to load statements
    Then I should be able to view error notification
    And I should be able to close the error notification

  # MOB3-4183
  Scenario: TC_019 No View transactions entry in actions menu for credit card account
    Given I am an enrolled "credit card" user logged into the app
    When I select creditCard account tile from home page
    And I select the action menu from statements page
    Then I should not see view transaction option

  # MOB3-3545, MOB3-4235
  @manual @secondary
  Scenario: TC_020 Viewing transactions from the start of the account
    Given I am an enrolled "credit card open for less than 6 months" user logged into the app
    When I select pending creditCard account tile from home page
    And I scroll to all available months and view the transactions for each month
    And I scroll to the oldest available month
    Then I should see transactions in that month or no transactions message if no transactions were made
    And I should see a blank tab to the left of the selected month

  Scenario: TC_021 Viewing transactions only for past 6 months
    Given I am an enrolled "credit card" user logged into the app
    When I select creditCard account tile from home page
    And I swipe to previous 7 months transactions on the account statement page
    Then I see a message advising that online statements are available only for 6 months
