@team-statements @nga3 @rnga @stub-enrolled-valid
Feature: Current Account View Transaction Details
  As a NGA Customer
  I should be able to view transaction details for my current account
  So that I am better informed about a specific transaction

  Background:
    Given I am an enrolled "statement_valid" user logged into the app
    And I select current Account With View Transactions account tile from home page

  @stubOnly
  Scenario: TC_002_Close Posted Transaction Details
    And I select one of the posted transaction
    When I close the posted transactions
    Then I should be on the allTab of statements page

  @primary @stubOnly
  Scenario: TC_004_View Standing Order Posted Transaction Details fields
    When I select standing Order on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | date                      |
      | vtdCloseButton            |
      | vtdAmount                 |
      | unsureTransactionButton   |
      | manageStandingOrderButton |
      | standingOrderText         |
      | payeeField                |

  @primary @stubOnly
  Scenario: TC_005_View Direct Debit Posted Transaction Details fields
    When I select direct Debit on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | date                      |
      | vtdCloseButton            |
      | vtdAmount                 |
      | unsureTransactionButton   |
      | viewDirectDebit           |
      | directDebitText           |
      | payeeField                |
      | referenceDirectDebit      |

  #MOB3-3440,3444
  @secondary @stubOnly
  Scenario Outline: TC_006_View cash and cheque deposited in Branch Posted Transaction Details fields (DEP)
    When I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | date                      |
      | vtdCloseButton            |
      | vtdAmount                 |
      | unsureTransactionButton   |
      | transferPaymentsButton    |
      | depositText               |
      | payeeField                |
    Examples:
      | transactionType              |
      | cash Deposit In Branch       |
      | cheque Deposit In Branch     |

  @primary @stubOnly
  Scenario: TC_007_View Cash Deposited in IDM Posted Transaction Details fields (CSH)
    When I select cash Deposited In IDM on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | date                      |
      | vtdCloseButton            |
      | vtdAmount                 |
      | unsureTransactionButton   |
      | transferPaymentsButton    |
      | cashText                  |
      | payeeField                |

  @primary @stubOnly
  Scenario: TC_008_View Cash Withdrawal in ATM Posted Transaction Details fields (Cashpoint)
    When I select cash Withdrawal In ATM on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | date                      |
      | vtdCloseButton            |
      | vtdAmount                 |
      | unsureTransactionButton   |
      | cashPointText             |
      | payeeField                |
      | payeeFieldCardNumber      |
      | dateOfTransaction         |
      | cardEndingNumber          |

  @secondary @stubOnly
  Scenario: TC_009_View Cash Withdrawal in Branch Posted Transaction Details fields (Debit card)
    When I select cash Withdrawal In Branch on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | date                      |
      | vtdCloseButton            |
      | vtdAmount                 |
      | unsureTransactionButton   |
      | debitCardText             |
      | payeeField                |

  @stubOnly
  Scenario: TC_010_View Cheque Withdrawal Posted Transaction Details fields (CHQ)
    When I select cheque Withdrawal In Branch on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | date                      |
      | vtdCloseButton            |
      | vtdAmount                 |
      | unsureTransactionButton   |
      | chequeText                |
      | payeeField                |

  #MOB3-3442,3443
  @primary @stubOnly
  Scenario: TC_011_View Faster Payments Incoming Posted Transaction Details fields (Faster Payments Incoming)
    When I select faster Payments Incoming on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | transferPaymentsButton       |
      | fasterPaymentsInText         |
      | payeeField                   |

  @primary @stubOnly
  Scenario: TC_012_View Faster Payments Outgoing Posted Transaction Details fields (Faster Payments Outgoing)
    When I select faster Payments Outgoing on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | transferPaymentsButton       |
      | fasterPaymentsOutText        |
      | payeeField                   |
      | dateOfTransaction            |
      | timeOfTransaction            |

  @secondary @stubOnly
  Scenario: TC_013_View Mobile Payment Inbound Posted Transaction Details fields (Mobile Payment Inbound)
    When I select mobile Payments Inbound on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | transferPaymentsButton       |
      | mobilePaymentInText          |
      | payeeField                   |

  @primary @stubOnly
  Scenario: TC_014_View Mobile Payment Outbound Posted Transaction Details fields (Mobile Payment Outbound)
    When I select mobile Payments Outgoing on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | transferPaymentsButton       |
      | mobilePaymentOutText         |
      | payeeField                   |
      | dateOfTransaction            |
      | timeOfTransaction            |

  @primary @stubOnly
  Scenario Outline: TC_015_View Transfers Out Posted Transaction Details fields (Transfers IN & OUT )
    When I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | transferPaymentsButton       |
      | transferText                 |
      | payeeField                   |
      | sortCode                     |
      | accountNumber                |
    Examples:
      | transactionType              |
      | transfers                    |
      | transfersOut                 |

  @secondary @stubOnly
  Scenario: TC_016_View Bill Payment Posted Transaction Details fields (Bill Payment)
    When I select bill Payment on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | transferPaymentsButton       |
      | billPaymentText              |
      | payeeField                   |

  @primary @stubOnly
  Scenario: TC_017_View Bank Giro Credit Posted Transaction Details fields (BGC)
    When I select bank Giro Credit on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | transferPaymentsButton       |
      | bankGiroCreditText           |
      | payeeField                   |

  #MOB3-4182,4757
  @primary @stubOnly
  Scenario: TC_018_View Online Debit Card Posted Transaction Details fields (Online/phone/mail order purchase)
    When I select online Debit Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | onlinePhoneMailText          |
      | payeeField                   |
      | dateOfTransaction            |
      | cardEndingNumber             |

  @secondary @stubOnly
  Scenario: TC_019_View ContactLess Card Purchase Posted Transaction Details fields (Contactless purchase)
    When I select contact Less Purchase on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | contactLessPurchaseText      |
      | payeeField                   |
      | dateOfTransaction            |
      | cardEndingNumber             |

  @primary @stubOnly
  Scenario: TC_020_View Chip and Pin Card Purchase Posted Transaction Details fields (Chip and PIN Purchase)
    When I select chip And Pin Purchase on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | chipAndPinText               |
      | payeeField                   |
      | dateOfTransaction            |
      | cardEndingNumber             |

  @secondary @stubOnly
  Scenario: TC_021_View Chip and Pin Card Purchase with Cash Back Posted Transaction Details fields (Chip and PIN Purchase - Cashback)
    When I select chip And Pin Purchase With Cash Back on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | cashBack                     |
      | chipAndPinText               |
      | payeeField                   |
      | dateOfTransaction            |
      | cardEndingNumber             |
      | cashBackReferenceField       |
      | googleMap                    |
      | unsureTransactionButton      |

  #MOB3-8317,8204
  @secondary @stubOnly
  Scenario Outline: TC_022_User navigates to Payments and Transfers journey
    And I select <transactionType> on statements page
    When I see posted transaction detail screen and select payments and transfer
    Then I should be on the payment hub page of that account
    Examples:
      | transactionType                     |
      | faster Payments Incoming            |
      | faster Payments Outgoing            |
      | mobile Payments Inbound             |
      | mobile Payments Outgoing            |
      | transfers                           |
      | bill Payment                        |
      | bank Giro Credit                    |

  #COSE-593
  @stubOnly
  Scenario Outline: TC_023_User navigates to home from payments and transfers journey
    When I select <transactionType> on statements page
    And I see posted transaction detail screen and select payments and transfer
    And I see payment hub page and tap on close icon
    Then I should be on the statements page
    Examples:
      | transactionType                     |
      | faster Payments Incoming            |
      | faster Payments Outgoing            |
      | mobile Payments Inbound             |
      | mobile Payments Outgoing            |
      | transfers                           |
      | bill Payment                        |
      | bank Giro Credit                    |

  #MOB3-9633,9636
  @manual
  Scenario: TC_024_Display VTD with a message for no network (MG-208)
    When I select cash Withdrawal on statements page with no network available
    Then I should be displayed a message with yellow banner

  #MOB3-8336,8337
  #Test data is not available. Need to test on live account
  @exhaustiveData @environmentOnly
  Scenario Outline: TC_025_User Views Standing Order and Direct Debit from VTD screen
    When I select <transactionType> on statements page
    And I tap on <buttonType> in vtd screen
    Then I should be on the <orderType> web view page
    Examples:
      | transactionType  | buttonType                   | orderType          |
      | standing Order   | manageStandingOrderButton    | standingOrder      |
      | direct Debit     | viewDirectDebit              | directDebit        |

  @exhaustiveData @environmentOnly
  Scenario Outline: TC_026_User navigates back from Standing Order and Direct Debit
    And I select <transactionType> on statements page
    When I tap on <buttonType> in vtd screen
    And I navigate back from standing order and direct debit page
    Then I should be displayed VTD of that transaction
    Examples:
      | transactionType  | buttonType                   |
      | standing Order   | manageStandingOrderButton    |
      | direct Debit     | viewDirectDebit              |

  @stubOnly
  Scenario: TC_027 Exception Scenario: User with transaction type CHG and description Daily OD fee is displayed overdraft copy
    When I select dailyOdFee on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | chargeText                   |
      | payeeField                   |
      | dailyODFeeDescription        |
      | furtherDetailsLink           |
      | unsureTransactionButton      |

  #MOB3-14014
  @manual
  Scenario: TC_028 Exception Scenario: User registers for spending rewards for CASHBACK - BGC transaction and view offers from VTD
    When I select cashBackBankGiroCredit on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | bankGiroCreditText           |
      | payeeField                   |
      | cashBackDescription          |
      | cashBackReferenceField       |
      | registerSpendingRewardsButton|
    And The amount value should be equal to the cash back amount displayed in the transaction
    When I register for spending rewards
    Then I should be opted in for spending rewards
    When I select cashBackBankGiroCredit on statements page
    Then I should be displayed view your every day offers button
    When I tap on view your offers
    Then I should be displayed spending rewards hub

  @manual
  Scenario: TC_030 Exception Scenario: User is not displayed spending rewards hub link & spending rewards copy for BGC transaction other than CASHBACK
    When I select to view the transaction details for the BCG transaction not described as cash back
    Then I should NOT see the spending rewards copy stating that I have earned £<amount> cashback displayed in the VTD
    And I should NOT see the spending rewards link in the VTD

  @manual
  Scenario: TC_032_Exception Scenario: User is displayed Google maps for chip and pin or contactless transaction
    When I select a debit transaction
    And The authorisation method is Chip & Pin or Contactless
    And There is only one location response returned
    Then I will see a map displayed of the location for that transaction
    And A location pin will identify the location with address details of the merchant

  @manual
  Scenario: TC_033_Exception Scenario: User is not displayed Google maps for chip and pin or contactless transaction for multiple location results
    When I select a debit transaction
    And the authorisation method is Chip & Pin or Contactless
    And there is multiple location responses returned
    Then I should not be displayed google maps for the transaction on the VTD

  @stubOnly
  Scenario: TC_034_Display Exchange Rate with no Additional Information
    When I select exchange Rate With No Additional Information on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen       |
      | date                       |
      | vtdCloseButton             |
      | vtdAmount                  |
      | transferText               |
      | payeeField                 |
      | exchangeRate               |
      | transferPaymentsButton     |
      | unsureTransactionButton    |
    And I should not see additional information on VTD page

  @stubOnly
  Scenario: TC_035_No Rate or Additional information shown
    When I select no Exchange Rate Or Additional Information on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen       |
      | date                       |
      | vtdCloseButton             |
      | vtdAmount                  |
      | transferText               |
      | payeeField                 |
      | transferPaymentsButton     |
      | unsureTransactionButton    |
    And I should not see exchange rate or additional information on VTD page

  @stubOnly
  Scenario: TC_036_Display Exchange Rate and additional information
    When I select exchange Rate And Additional Information on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen       |
      | date                       |
      | vtdCloseButton             |
      | vtdAmount                  |
      | transferText               |
      | payeeField                 |
      | payeeFieldAdditionalDetails|
      | exchangeRate               |
      | transferPaymentsButton     |
      | unsureTransactionButton    |

  @stubOnly
  Scenario: TC_037_Display additional information and no Exchange Rate
    When I select additional Information And No Exchange Rate on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen       |
      | date                       |
      | vtdCloseButton             |
      | vtdAmount                  |
      | transferText               |
      | payeeField                 |
      | payeeFieldAdditionalDetails|
      | transferPaymentsButton     |
      | unsureTransactionButton    |
    And I should not see exchange rate on VTD page

  @stubOnly
  Scenario: TC_038_Display date of the account is debited when transaction is processed on a non-working day (7 day clearing - weekend / bank holidays)
    When I select account Debited on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | onlinePhoneMailText          |
      | payeeField                   |
      | dateOfTransaction            |
      | cardEndingNumber             |
      | accountDebitedDate           |
      | unsureTransactionButton      |

  @stubOnly
  Scenario: TC_039_Display date of the account is credited when transaction is processed on a non-working day (7 day clearing - weekend / bank holidays)
    When I select account Credited on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | onlinePhoneMailText          |
      | payeeField                   |
      | dateOfTransaction            |
      | cardEndingNumber             |
      | accountCreditedDate          |
      | unsureTransactionButton      |

  @stubOnly
  Scenario: TC_040_Do not display date of the account is credited when transaction is processed on a non-working day (7 day clearing - weekend / bank holidays)
    When I select cash Withdrawal In ATM on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | date                      |
      | vtdCloseButton            |
      | vtdAmount                 |
      | unsureTransactionButton   |
      | cashPointText             |
      | payeeField                |
      | payeeFieldCardNumber      |
      | dateOfTransaction         |
      | cardEndingNumber          |
    And I should see no other date of account debited or credited
