@team-statements @nga3 @rnga @stub-enrolled-valid @manual @android
  #Tagging manual as we have to implement image comparison
Feature: Transaction Logos
  As a NGA Customer
  I should be able to see merchant logos on my transactions
  So that I can recognise transactions more quickly

  #STAMOB-1260
  @primary
  Scenario Outline: TC_001_Display default logo when merchant logo is not provided
    Given I am an enrolled "statement_valid" user logged into the app
    When I select current account tile from home page
    Then I should be displayed default logo next to the <type> of transaction
    When I select <type> on statements page
    Then I should be displayed default logo of that transaction in VTD screen
    When I scroll down to load all the transactions in all tab
    Then The more transactions are loaded with logos
    Examples:
      | type                     |
      | standingOrder            |
      | directDebit              |
      | cashPoint                |
      | Transfer                 |
      | fasterPaymentOut         |
      | fasterPaymentIn          |
      | mobilePaymentIn          |
      | mobilePaymentOut         |
      | chequeWithdrawalInBranch |
      | billPayment              |
      | bankGiroCredit           |
      | interest                 |
      | charge                   |
      | directDebit              |
      | chipAndPin               |
      | contactlessPurchase      |
      | onlinePhoneMailPurchase  |

  #STAMOB-1260
  @primary
  Scenario: TC_002_Display merchant logo when merchant logo is provided
    Given I am an enrolled "statement_valid" user logged into the app
    When I select current account tile from home page
    Then I should be displayed merchant logo next to the transaction
    When I select transaction on statements page
    Then I should be displayed merchant logo in VTD page same as in statements page
    When I scroll down to load all the transactions in all tab
    Then The more transactions are loaded with logos

  #STAMOB-1550
  @primary
  Scenario: TC_003_Display default logo or merchant logo in search transactions
    Given I am an enrolled "statement_valid" user logged into the app
    When I select current account tile from home page
    And I tap on search icon in all tab
    And I enter the <keyword> in search dialog box
    Then I should be displayed either a merchant logo or a default logo next to the transaction
    When I select one of the posted transaction
    Then I should be displayed with same logo as in statements page
    When I close the posted transactions
    And I tap on search more transactions
    Then I should be displayed further more transactions loaded with either a merchant logo or a default logo

  #STAMOB-1260
  Scenario: TC_004_Display card logo when both default and merchant logos are not available
    Given I am an enrolled "statement_valid" user logged into the app
    When I select current account tile from home page
    Then I should be displayed debit card logo next to the transaction
    When I select transaction on statements page
    Then I should be displayed debit card logo in VTD page same as in statements page

  Scenario: TC_005_Display default logo when merchant logo is not loaded
    Given I am an enrolled "statement_valid" user logged into the app
    When I select current account tile from home page
    And The app fails to load the merchant logo
    Then I should be displayed default logo next to the transaction

  Scenario: TC_006_Display Logo fading when loaded
    Given I am an enrolled "statement_valid" user logged into the app
    When I select current account tile from home page
    And The merchant logo is being loaded
    Then I should be displayed default logo for the transaction while the merchant logo loads
    And I should be displayed the merchant logo once loading is completed and it should fade in nicely
