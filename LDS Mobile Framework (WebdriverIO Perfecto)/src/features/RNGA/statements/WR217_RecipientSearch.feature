@team-payments @nga3 @rnga @stub-enrolled-typical
Feature: Search for accounts and recipients
  As a NGA authenticated customer
  In order to quickly find an existing recipient or account of mine
  I want to be able to search for a specific recipient account

  # MOB3-619
  Scenario: TC_001 Search Field availability
    Given I am an enrolled "valid" user logged into the app
    When I choose the recipient account on the payment hub page
    Then I am on search recipient page
    And I see option to search payees
    And I see list of Accounts in search page

  @manual
  Scenario: TC_002 Show existing accounts / recipients ordered before searching and Results
    Given I am an enrolled "valid" user logged into the app
    When I navigate to the payment hub page
    And I choose current account with recipients in payment hub
    Then I choose option to select recipient
    And I see option to search payees
    And results are categorised by accounts and recipients
    And sorted by Numerical then alphabetical order with each category

  # MOB3-619, MOB3-6671
  @primary
  Scenario: TC_003 Dynamic Searching per character added (Visual Verification)
    Given I am an enrolled "valid" user logged into the app
    When I choose the recipient account on the payment hub page
    Then I verify search results for each search term
      | searchType | searchTerm | results    |
      | account    | cla        | current    |
      | account    | sic        | current    |
      | recipient  | Ada        | adam       |
      | recipient  | Ander      | adam       |
      | recipient  | ili        | no results |

  # MOB3-619
  @manual @secondary
  Scenario Outline: TC_004 Dynamic Searching per character added (Visual Verification)
    Given I am an enrolled "valid" user logged into the app
    When I choose to pay from my current account with recipients
    And I activate payee search bar
    And I enter <searchTerm> into my search bar
    Then the <searchTerm> characters of <results> are highlighted in <searchType> category
    Examples:
      | searchType | searchTerm | results    |
      | account    | cla        | current    |
      | account    | sic        | current    |
      | recipient  | Ada        | adam       |
      | recipient  | Ander      | adam       |
      | recipient  | ili        | no results |

  # MOB3-619
  @pending @manual
  Scenario: TC_005 Search after deleting characters
    Given I am an enrolled "valid" user logged into the app
    When I choose to pay from my current account with recipients
    And I activate payee search bar
    And I enter "savings" into my search bar
    And I remove 3 characters from search bar
    Then I see search results for "savi"

  # MOB3-8428, 8441
  Scenario Outline: TC_006: Results are found, add a new recipient option is available
    Given I am an enrolled "valid" user logged into the app
    And I navigate to the remitter account on the payment hub page
    And I select <eligibleRemitting> Account on payment hub page
    When I select the recipient account on the payment hub page
    And I verify search results for each search term
      | searchType | searchTerm | results |
      | account    | eas        | saving  |
    Then I should be given the option to add a new recipient at the end of the list
    Examples:
      | eligibleRemitting  |
      | eligible Remitting |

  # MOB3-8428, 8441
  Scenario Outline: TC_007: Results are found, cannot add a new recipient
    Given I am an enrolled "valid" user logged into the app
    And I navigate to the remitter account on the payment hub page
    And I select <ineligibleRemitting> Account on payment hub page
    When I select the recipient account on the payment hub page
    And I verify search results for each search term
      | searchType | searchTerm | results |
      | account    | Cla        | current |
    Then I should not be given the option to add a new recipient at the end of the list
    Examples:
      | ineligibleRemitting  |
      | ineligible Remitting |

   #MOB3-8428, 8441
  Scenario Outline: TC_008: No Results are found, add a new recipient
    Given I am an enrolled "valid" user logged into the app
    And I navigate to the remitter account on the payment hub page
    And I select <eligibleRemitting> Account on payment hub page
    When I select the recipient account on the payment hub page
    And I verify search results for each search term
      | searchType | searchTerm | results    |
      | recipient  | ifgh       | no results |
    Then I should be given the option to add a new recipient at the end of the list
    Examples:
      | eligibleRemitting  |
      | eligible Remitting |

  #MOB3-8428, 8441
  Scenario Outline: TC_009: No Results are found, cannot add a new recipient
    Given I am an enrolled "valid" user logged into the app
    And I navigate to the remitter account on the payment hub page
    And I select <ineligibleRemitting> Account on payment hub page
    When I select the recipient account on the payment hub page
    And I verify search results for each search term
      | searchType | searchTerm | results    |
      | recipient  | ifgh       | no results |
    Then I should not be given the option to add a new recipient at the end of the list
    Examples:
      | ineligibleRemitting  |
      | ineligible Remitting |

  # MOB3-619
  Scenario: TC_010 Cancelling a search
    Given I am an enrolled "valid" user logged into the app
    And I am on the payment hub page
    When I select the recipient account on the payment hub page
    Then I verify search results for each search term
      | searchType | searchTerm | results |
      | account    | Easy        | saving  |
    And I click on cancel searching
    And I see list of Accounts in search page

  @pending @manual
  Scenario: TC_011 Add new recipient option is removed once search bar is engaged
    Given I am an enrolled "valid" user logged into the app
    When I choose to pay from my savings account
    And I activate payee search bar
    Then the 'Add new recipient' option should no longer be shown

  # MOB3-619
  Scenario: TC_012 Delete all criteria and active in Search Bar
    Given I am an enrolled "valid" user logged into the app
    And I am on the payment hub page
    When I select the recipient account on the payment hub page
    Then I verify search results for each search term
      | searchType | searchTerm | results |
      | account    | Easy        | saving |
    And I click on clear searching
    And I see list of Accounts in search page
