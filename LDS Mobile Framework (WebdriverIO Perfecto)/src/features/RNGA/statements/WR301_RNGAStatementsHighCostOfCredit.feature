@team-statements @nga3 @rnga @stub-rnga-hcc
Feature: High Cost Of Credit
  As a NGA Retail customer,
  I want my Statements page to display my balance after pending,
  So that I am always informed of the limit of my overdraft facility

  #STAMOB-1198, R_STAMOB-1149
  #automation will be completed along with total pending story which will be in kenya release
  @primary @manual
  Scenario Outline: Display Balance After Pending on account tile (AC1 AC2 AC3)
    Given I am an enrolled "current" user logged into the app
    When I navigate to <type> account to validate balanceAfterPending and select <type> account
    Then I should be on the allTab of statements page
    And I should see account details with the following fields
      | fieldsInAccountTile      |
      | balanceAfterPendingLabel |
      | overDraftLimitLabel      |
      | <message>                |
      | accountTrueBalance       |
    And The balanceAfterPending value should be same with the value in Account overview page
    And The balance after pending value should be <symbol> with difference between account balance and the total value of pending transactions
    Examples:
      | type                                            | symbol   | message            |
      | currentAccountWithPendingLessThanAccountBalance | positive | noOverDraftMessage |
      | currentAccountWithPendingMoreThanAccountBalance | negative | overDraftMessage   |
      | currentAccountWithAccountBalanceNegative        | negative | overDraftMessage   |

  #STAMOB-1248, R_STAMOB-1155
  Scenario Outline: Display Overdraft Limit on account tile (AC1 AC2 AC3)
    Given I am an enrolled "current" user logged into the app
    When I navigate to <type> account to validate overDraftLimit and select <type> account
    Then I should be on the allTab of statements page
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | overDraftLimitLabel |
      | overDraftLimitValue |
    And The <overDraft> value should be same with the value in Account overview page
    Examples:
      | type                              | overDraft        |
      | currentAccountWithUnusedOverDraft | overDraftLimit   |
      | currentAccountWithUsedOverDraft   | overDraftLimit   |
      | currentAccountWithNoOverDraft     | noOverDraftLimit |
