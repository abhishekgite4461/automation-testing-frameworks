@team-statements @nga3 @rnga @stub-enrolled-valid
Feature: Mortgage Statements
  In order to be better informed about my mortgage
  As a NGA Retail customer,
  I want to view my mortgage account details and statement

    #MOB3-2796,2795,2799,2801
  @primary
  Scenario: TC_001_View mortgage details
    Given I am an enrolled "mortgage_account" user logged into the app
    When I select mortgage account tile from home page
    Then I should be on the mortgage account statement summary
    And I should see following fields for minimised mortgage details
      | fieldsInMortgageDetails   |
      | nextPaymentDue            |
      | remainingFullTerm         |
    When I view full mortgage details
    Then I should see following fields for expanded mortgage details
      | fieldsInMortgageDetails      |
      | nextPaymentDue               |
      | remainingFullTerm            |
      | mortgageType                 |
      | mortgageHolders              |
      | propertyAddress              |
      | mortgageOpenedDate           |
      | arrearsBalance               |
      | currentBalance               |
      | currentMonthlyPayment        |
      | minimumMonthlyPayment        |
      | interestRateAsOfDate         |
      | staticDisclaimerText         |
      | currentBalanceInfoLink       |
      | monthlyPaymentsInfoLink      |

  Scenario: TC_002_Mortgage statement entry from actions menu on home screen
    Given I am an enrolled "mortgage_account" user logged into the app
    And I navigate to mortgage account on home page
    When I select the "view transaction" option in the action menu of mortgage account
    Then I should be on the mortgage account statement summary

  #MOB3-8928,8927
  @primary
  Scenario Outline: TC_003_View current mortgage balance info
    Given I am an enrolled "mortgage_account" user logged into the app
    And I select mortgage account tile from home page
    When I view full mortgage details for <type> and select the currentBalanceInfo
    Then I should be displayed the currentBalanceInfo as a layover
    When I select close button in info page
    And I select <type> monthlyPaymentsInfo link to view info
    Then I should be displayed the monthlyPaymentInfo as a layover
    Examples:
      | type                  |
      | mortgageSummaryTab    |
      | mortgageSubAccountTab |

  #DPL-2209
  @secondary
  Scenario Outline: TC_004_User contacts customer care thru click to call
    Given I am an enrolled "mortgage_account" user logged into the app
    And I select mortgage account tile from home page
    And I view full mortgage details for <type> and select the currentBalanceInfo
    When I navigate to click to call journey
    Then I should be displayed the click to call journey
    Examples:
      | type                  |
      | mortgageSummaryTab    |
      | mortgageSubAccountTab |

  #STAMOB-438
  Scenario Outline: TC_005_Back to Mortgage statements from Click To Call journey
    Given I am an enrolled "mortgage_account" user logged into the app
    And I select mortgage account tile from home page
    And I view full mortgage details for <type> and select the currentBalanceInfo
    When I navigate to click to call journey
    And I select back button on click to call journey
    Then I should be navigated back to <mortgageTab> with full mortgage details
    Examples:
      | type                  | mortgageTab            |
      | mortgageSummaryTab    | mortgageSummaryTab     |
      | mortgageSubAccountTab | subAccountDetailsTab   |

  Scenario Outline: TC_006_Close current mortgage balance info
    Given I am an enrolled "mortgage_account" user logged into the app
    And I select mortgage account tile from home page
    And I view full mortgage details for <type> and select the currentBalanceInfo
    When I select close button in info page
    Then I should be displayed <mortgageTab> with full mortgage details
    Examples:
      | type                  | mortgageTab            |
      | mortgageSummaryTab    | mortgageSummaryTab     |
      | mortgageSubAccountTab | subAccountDetailsTab   |

  Scenario Outline: TC_007_Close monthly payment info
    Given I am an enrolled "mortgage_account" user logged into the app
    And I select mortgage account tile from home page
    And I view full mortgage details for <type> and select the monthlyPaymentsInfo
    When I select close button in info page
    Then I should be displayed <mortgageTab> with full mortgage details
    Examples:
      | type                  | mortgageTab              |
      | mortgageSummaryTab    | mortgageSummaryTab       |
      | mortgageSubAccountTab | subAccountDetailsTab     |

  #MOB3-2802,2803
  @primary
  Scenario Outline: TC_008_View mortgage transactions
    Given I am an enrolled "mortgage_account" user logged into the app
    When I select <type> account tile from home page
    Then I should be on the mortgage account statement summary
    And I should see following fields in statements list
      | fieldsInStatementsList         |
      | firstTranscationDate           |
      | firstTranscationDescription    |
      | firstTransactionRunningBalance |
      | firstTranscationAmount         |
    Examples:
      | type                  |
      | mortgage              |

  @primary
  Scenario: TC_009_Display end of transactions message for summary and sub account tab (HC021)
    Given I am an enrolled "mortgage_account" user logged into the app
    When I select mortgage account tile from home page
    Then I should scroll down to the end of the transactions to view mortgageEndOf message
    When I select sub account tab
    And I tap on any of the sub account
    Then I should scroll down to the end of the transactions to view mortgageEndOf message

  @stubOnly
  Scenario: TC_010_Display Summary and Sub account tab no transaction message (HC018)
    Given I am an enrolled "mortgage_account" user logged into the app
    When I select mortgage No Transactions account tile from home page
    Then I should see mortgage no transactions message in the summary tab
    When I select sub account tab
    And I tap on any of the sub account
    Then I should see mortgage no transactions message in the details tab

  #MOB3-2804,2805
  @primary
  Scenario: TC_011_View mortgage sub-accounts list
    Given I am an enrolled "mortgage_account" user logged into the app
    And I select mortgage account tile from home page
    When I select sub account tab
    Then I should see summary of sub account with the following fields in the summary for each sub-account
      | fieldsInSubAccountsList         |
      | subAccountTypeNo                |
      | mortgageSubAccountBalance       |
      | mortgageSubAccountType          |
      | interestRateAsAtDate            |
      | subAccountCurrentMonthlyPayment |
      | nextPaymentDueOnDate            |
      | disclaimerText                  |

  #MOB3-9212, 9705
  Scenario: TC_012_View minimised sub-account details
    Given I am an enrolled "mortgage_account" user logged into the app
    And I select mortgage account tile from home page
    When I select sub account tab
    And I tap on any of the sub account
    Then I should be displayed with details tab
    And I should see following fields for minimised mortgage details
      | fieldsInMortgageDetails     |
      | nextPaymentDue              |
      | remainingFullTerm           |

  @primary
  Scenario: TC_013_View full sub-account details
    Given I am an enrolled "mortgage_account" user logged into the app
    And I select mortgage account tile from home page
    And I select sub account tab
    And I tap on any of the sub account
    When I tap on sub account accordion in details tab
    Then I should see following fields for expanded mortgage details
      | fieldsInMortgageDetails         |
      | nextPaymentDue                  |
      | remainingFullTerm               |
      | mortgageType                    |
      | subAccountMortgageOpenedDate    |
      | paymentType                     |
      | originalAmount                  |
      | originalFullTerm                |
      | subAccountInterestRateAsAtDate  |
      | currentMonthlyPaymentDetails    |
      | minimumMonthlyPayment           |
      | remainingAmount                 |
      | mortgageRateHistoryTitle        |
      | mortgageRateHistoryDate         |
      | mortgageRateHistoryType         |
      | mortgageRateHistoryRate         |
      | staticDisclaimerText            |
      | currentBalanceInfoLink          |
      | monthlyPaymentsInfoLink         |

  #MOB3-2806,2807
  Scenario: TC_014_View sub-account transactions
    Given I am an enrolled "mortgage_account" user logged into the app
    And I select mortgage account tile from home page
    And I select sub account tab
    When I tap on any of the sub account
    Then I should be displayed details tab with following fields in the sub-account transactions
      | fieldsInStatementsList            |
      | firstTranscationDate              |
      | firstTranscationDescription       |
      | firstTransactionRunningBalance    |
      | firstTranscationAmount            |

  #MOB3-9146,9145
  # scenario for android is covered in condensed account tile feature file
  @stubOnly @primary @ios
  Scenario: TC_015 Repossessed Mortgage account tile on home screen
    Given I am an enrolled "mortgage_account" user logged into the app
    When I select rePossessedMortgage account tile from home page
    Then I should be displayed the following fields of rePossessedMortgage account
      | fieldsForMortgage                        |
      | offsetRepossessedMortgageName            |
      | offsetRepossessedMortgageNumber          |
      | offsetRepossessedMortgageBalance         |
      | offsetRepossessedMortgageMonthlyPayment  |
      | offsetRepossessedMortgageBalanceAsAtDate |
    And I should see the warning alert on the account tile on statement page

  @primary @stubOnly
  Scenario: TC_016 User is displayed repossessed Mortgage message (MG-1514)
    Given I am an enrolled "mortgage_account" user logged into the app
    When I select rePossessedMortgage account tile from home page
    Then I should not be displayed any summary and sub-account details tabs
    And I should see a rePossessedMessage on the statement page

  @stubOnly
  Scenario: TC_018_User is displayed repossessed Mortgage alert overlay (MG-1514) on statement page
    Given I am an enrolled "mortgage_account" user logged into the app
    And I select rePossessedMortgage account tile from home page
    When I tap on the warning alert on the account tile
    Then I must be displayed an alert overlay with a rePossessedMessage

  @stubOnly
  Scenario: TC_019_User closes repossessed Mortgage alert overlay (MG-1514) on statement page
    Given I am an enrolled "mortgage_account" user logged into the app
    And I select rePossessedMortgage account tile from home page
    And I tap on the warning alert on the account tile
    When I tap on the close alert overlay
    Then I should not be displayed any summary and sub-account details tabs
    And I should see a rePossessedMessage on the statement page

  #MOB3-12488,12489
  @stubOnly @primary
  Scenario: TC_020 Offset Mortgage account tile on home screen
    Given I am an enrolled "mortgage_account" user logged into the app
    When I select offSetMortgage account tile from home page
    Then I should be displayed the following fields of offSetMortgage account
      |fieldsForMortgage        |
      |offsetRepossessedMortgageName  |
      |offsetRepossessedMortgageNumber     |
      |offsetRepossessedMortgageBalance     |
      |offsetRepossessedMortgageMonthlyPayment          |
      |offsetRepossessedMortgageBalanceAsAtDate         |

  @stubOnly @primary
  Scenario: TC_021 User is displayed offset Mortgage message (MG-1514)
    Given I am an enrolled "mortgage_account" user logged into the app
    When I select offSetMortgage account tile from home page
    Then I should not be displayed any summary and sub-account details tabs
    And I should see a offSetMessage on the statement page
