@team-statements @nga3 @rnga @stub-enrolled-valid
Feature: Loan statements
  As a NGA Retail customer,
  I want to view loans statements,
  So that I am better informed about my transactions

  #MOB3-2685,5973,7165
  @primary
  Scenario: TC_001_View personal loan(CBS) account details on account tile
    Given I am an enrolled "statement_valid" user logged into the app
    When I navigate to personal Cbs Loan account on home page
    Then I should see account details with the following fields
      | fieldsInAccountTile    |
      | accountName       |
      | accountNumber     |
      | sortCode          |
      | accountBalance    |
    When I select personal Cbs Loan account tile from home page
    Then I should see the message displayed and should not see action menu on account tile

  #MOB3-5342, 7331
  @secondary
  Scenario: TC_002_No view transactions in contextual menu in loan statements
    Given I am an enrolled "statement_valid" user logged into the app
    And I select personal Loan account tile from home page
    When I select the action menu from statements page
    Then I should not see view transaction option
    And I should see below options in contextual menu
      | fieldsInLoansActionMenu       |
      | loanAdditionalPaymentOption   |
      | loanRepaymentHolidayOption    |
      | loanClosureOption             |
      | loanBorrowMoreOption          |
      | loanAnnualStatementsOption    |

  #MOB3-5341,7324
  @primary
  Scenario: TC_003_Opening and closing loan details accordion (+ -)
    Given I am an enrolled "statement_valid" user logged into the app
    When I select personal Loan account tile from home page
    Then I tap on the plus sign to expand the accordion with following loan details
      | fieldsForLoanAccount   |
      | nextRepaymentDue       |
      | remainingTerm          |
      | minusAccordion         |
      | openingDate            |
      | monthlyRepaymentAmount |
      | originalLoanTerm       |
      | noteOnAnnualStatement  |
    And I tap on minus sign to close the accordion and should see the following loan details
      | fieldsForLoanAccount   |
      | plusAccordion          |
      | nextRepaymentDue       |
      | remainingTerm          |

  #MOB3-6519,7329
  @primary
  Scenario: TC_005_View loan details & current and Previous year transactions
    Given I am an enrolled "statement_valid" user logged into the app
    When I select personal Loan account tile from home page
    Then I should be on the current year loan statements tab
    And I should see following fields in loan details closed accordion
      | fieldsForLoanAccount   |
      | nextRepaymentDue       |
      | remainingTerm          |
    And I should see following fields in statements list
      | fieldsInStatementsList        |
      | firstTranscationDate          |
      | firstTranscationDescription   |
      | firstTranscationAmount        |
    And I should scroll down to the end of the transactions to view loanEndOf message
    When I swipe to see previous year transactions on the account statement page
    Then I should be on the same previous year statements page
    And I should see following fields in statements list
      | fieldsInStatementsList        |
      | firstTranscationDate          |
      | firstTranscationDescription   |
      | firstTranscationAmount        |
    And  I should scroll down to see the next set of transactions loaded for personal Loan account for previous year

  #its difficult to get the count for next set of transactions
  @secondary @manual
  Scenario: TC_006_View loading of transactions in the current year
    Given I am an enrolled "statement_valid" user logged into the app
    When I select personal Loan account tile from home page
    Then I should be on the current year loan statements tab
    And I should scroll down to see the next set of transactions loaded for personal Loan account

  Scenario: TC_007_Do not display next year tab
    Given I am an enrolled "statement_valid" user logged into the app
    When I select personal Loan account tile from home page
    Then I should not see the next calendar year to the right of the current year tab

  Scenario: TC_008_Display of year tab
    Given I am an enrolled "statement_valid" user logged into the app
    And I select personal Loan account tile from home page
    And I should see previous year tab to the left of the current year tab in statements
    When I navigate to a previous year
    Then I should see the year tab heading in the format <YYYY>
    And I should see the future year to right of the selected previous year

  @secondary @stubOnly
  Scenario: TC_009_Display of previous calendar year tab until the beginning of the loan
    Given I am an enrolled "statement_valid" user logged into the app
    And I select personal Loan account tile from home page
    When I navigate to a previous year
    Then I should see the previous years until the year the loan started

  #Test data dependency
  @manual
  Scenario: TC_010_Display of previous calendar year tab for an account opened in current year
    Given I am an enrolled "statement_valid" user logged into the app
    When I select personal Loan account tile from home page
    Then I should not see the previous calendar year to the left of the current year tab

  Scenario: TC_011_No Transactions message
    Given I am an enrolled "statement_valid" user logged into the app
    When I select loanWithNoTransactions account tile from home page
    Then I should see loan no transactions message in current year tab
