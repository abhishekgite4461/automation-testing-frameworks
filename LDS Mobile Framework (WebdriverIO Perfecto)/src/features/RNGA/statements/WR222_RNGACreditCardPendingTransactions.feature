@nga3 @rnga @team-statements
Feature: Pending Transactions
  In order to view the statements for the nominated credit card
  As a NGA authenticated customer with credit card
  I want to view my credit card transactions for each statement period

  #STAMOB-51,49
  @primary @stub-enrolled-valid
  Scenario: TC_001 View Credit Card Pending Transaction Details
    Given I am an enrolled "statement_valid" user logged into the app
    When I select credit Card With Pending Debit Transactions account tile from home page
    Then I should be on the recent of statements page
    When I tap on the pending transactions accordion
    Then I should see pending Credit transaction with below detail in statements
      | detail                              |
      | pendingLabel                        |
      | pendingCreditTransactionDescription |
      | pendingCreditTransactionAmount      |
    When I select pending Credit Card on statements page
    Then I should see pending transaction details for that transaction with the following fields
      | fieldsInDetailScreen     |
      | vtdCloseButton           |
      | vtdAmount                |
      | creditCardText           |
      | payeeField               |
      | dateOfTransaction        |
      | creditCardDisclaimerText |
      | unsureTransactionButton  |
    When I close the pending transactions VTD
    Then I should be displayed pending transactions for that pending Credit Card

  #MOB3-4327
  @stub-enrolled-valid
  Scenario: TC_002 Verify open and close pending transactions accordion
    Given I am an enrolled "credit card with pending transactions" user logged into the app
    When I select credit Card With Pending Debit Transactions account tile from home page
    Then I should see minimised pending transactions accordion
    When I tap on the minimised pending transactions accordion
    Then I should see expanded pending transactions accordion
    And I should see a pending transactions header
    And I should see pending transactions
    When I tap on the expanded pending transactions accordion
    Then I should see minimised pending transactions accordion

  #MOB3-4327
  @stub-enrolled-valid
  Scenario: TC_003 Verify accordion is open when user scrolls through the previous months
    Given I am an enrolled "statement_valid" user logged into the app
    When I select credit Card With Pending Debit Transactions account tile from home page
    And I tap on the pending transactions accordion
    Then I should see expanded pending transactions accordion
    When I swipe to previous 1 months transactions on the account statement page
    And I scroll back to the recent tab
    Then I should see transactions since message on recent tab
    And I should see expanded pending transactions accordion

  #MOB3-4330,3716, STAMOB-50,48
  # will remove manual tag once stub issue is fixed
  @stub-pending-transactions-not-retrieved @manual
  Scenario: TC_004 Pending credit transactions cannot be retrieved
    Given I am a pending transactions not retrieved user on the home page
    When I select creditCardWithPendingTransactionsFailed account tile from home page
    And I tap on the pending transactions accordion
    Then I should see an credit error message in place of the transaction

  #MOB3-4326
  @stub-enrolled-valid
  Scenario: TC_005 Verify do not display pending transactions accordion
    Given I am an enrolled "credit card without pending transactions" user logged into the app
    When I select creditCardAccountWithViewTransactions account tile from home page
    Then I should not see the pending transactions accordion
    And I should see the cleared transactions
