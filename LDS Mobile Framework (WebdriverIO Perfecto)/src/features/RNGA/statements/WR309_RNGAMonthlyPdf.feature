@team-statements @nga3 @rnga @ff-monthly-pdf-on @sw-monthly-pdf-android-on @primary @android @stub-enrolled-valid
Feature: Monthly PDFs on PCA
  In order for me to take appropriate steps
  As a RNGA user with current account statements
  I should be able to view PDF statements

 #STAMOB-1524,1535
  @environmentOnly
  Scenario: TC_001_Download transactions journey from contextual menu on home screen
    Given I am an enrolled "current" user logged into the app
    When I navigate to current account on home page
    And I select the action menu of current account
    Then I am able to view downloadTransactionsOption in contextual menu
    When I select downloadTransactionsOption from contextual menu
    Then I must be displayed the month selection screen
    When I enter any previous month and year on the month selection screen
    And I select close button in month selection screen
    Then I should be on the home page
    When I select the action menu of current account
    And I select downloadTransactionsOption from contextual menu
    Then I must be displayed the month selection screen with default values
    When I enter any previous month and year on the month selection screen
    And I select view button on the month selection screen
    Then I should see winback with Go back and Proceed option
    When I select the Proceed option in winback
    And I select back key on the device
    Then I must be displayed the month selection screen
    And I must be displayed the previously selected month and year on the month selection screen

  @environmentOnly
  Scenario: TC_002_Download transactions journey from contextual menu on statements screen
    Given I am an enrolled "current" user logged into the app
    When I select current account tile from home page
    And I select the downloadTransactionsOption option from the action menu in statements page
    Then I must be displayed the month selection screen
    When I enter any previous month and year on the month selection screen
    And  I select close button in month selection screen
    Then I should be on the statements page
    When I select the downloadTransactionsOption option from the action menu in statements page
    Then I must be displayed the month selection screen with default values
    When I enter any previous month and year on the month selection screen
    And I select view button on the month selection screen
    Then I should see winback with Go back and Proceed option
    When I select the Proceed option in winback
    And I select back key on the device
    Then I must be displayed the month selection screen
    And I must be displayed the previously selected month and year on the month selection screen

  Scenario: TC_003_Download transactions winback digital inbox navigation from contextual menu on home screen
    Given I am an enrolled "current" user logged into the app
    When I navigate to current account on home page
    And I select the action menu of current account
    Then I am able to view downloadTransactionsOption in contextual menu
    When I select downloadTransactionsOption from contextual menu
    Then I must be displayed the month selection screen
    When I select view button on the month selection screen
    Then I should see winback with Go back and Proceed option
    When I select the Digital Inbox option in winback
    Then I must be displayed the digital inbox screen
    When I select NGA header back button in digital inbox screen
    Then I should be on more menu

  Scenario: TC_004_Download transactions winback digital inbox navigation from action on statements screen
    Given I am an enrolled "current" user logged into the app
    When I select current account tile from home page
    And I select the downloadTransactionsOption option from the action menu in statements page
    Then I must be displayed the month selection screen
    When I select view button on the month selection screen
    Then I should see winback with Go back and Proceed option
    When I select the Digital Inbox option in winback
    Then I must be displayed the digital inbox screen
    When I select NGA header back button in digital inbox screen
    Then I should be on more menu

  Scenario: TC_005_Download transactions winback go back navigation from contextual menu on home screen
    Given I am an enrolled "current" user logged into the app
    When I navigate to current account on home page
    And I select the action menu of current account
    Then I am able to view downloadTransactionsOption in contextual menu
    When I select downloadTransactionsOption from contextual menu
    Then I must be displayed the month selection screen
    When I select view button on the month selection screen
    Then I should see winback with Go back and Proceed option
    When I select the Go back option in winback
    Then I must be displayed the month selection screen

  Scenario: TC_006_Download transactions winback go back navigation from action on statements screen
    Given I am an enrolled "current" user logged into the app
    When I select current account tile from home page
    And I select the downloadTransactionsOption option from the action menu in statements page
    Then I must be displayed the month selection screen
    When I select view button on the month selection screen
    Then I should see winback with Go back and Proceed option
    When I select the Go back option in winback
    Then I must be displayed the month selection screen

  #PDF preview screen validation is not compatible with frame work
  @environmentOnly @manual
  Scenario: TC_007_PDF preview screen zoom and share from contextual menu on home screen
    Given I am an enrolled "current" user logged into the app
    When I navigate to current account on home page
    And I select the action menu of current account
    Then I am able to view downloadTransactionsOption in contextual menu
    When I select downloadTransactionsOption from contextual menu
    Then I must be displayed the month selection screen
    When I enter any previous month and year on the month selection screen
    And I select view button on the month selection screen
    Then I should see winback with Go back and Proceed option
    When I select the Proceed option in winback
    Then I should be on selected month PDF Preview screen
    And I should be able to scroll and zoom PDF preview screen
    When I select more option on PDF preview screen header
    Then I should see native share actions/extensions
    And I should be able to share the PDF
    When I select back key on the device
    Then I should be on selected month PDF Preview screen
    When I select header back button in PDF Preview screen
    Then I must be displayed the previously selected month and year on the month selection screen
