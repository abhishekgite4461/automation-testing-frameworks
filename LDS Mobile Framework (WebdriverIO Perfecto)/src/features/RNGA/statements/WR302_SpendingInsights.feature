@team-statements @nga3 @rnga @stub-enrolled-valid @sw-spendinginsights-android-on @ff-spendinginsights-enabled-on
@sw-spendinginsights-ios-on @environmentOnly
Feature: Spending Insights
  As a NGA Retail customer,
  I want to be able to access spending insights
  So that I can understand my spending month on month

  #STAMOB-1420,1419
  @primary
  Scenario: TC_001_View Spending Insights option in contextual menu on home screen
    Given I am an enrolled "current" user logged into the app
    And I navigate to current account on home page
    When I select the action menu of current account
    Then I should see viewSpendingInsights option in the action menu
    When I select viewSpendingInsights option
    Then I should be displayed spendingInsights web view page
    When I tap on back button in spending insights page
    Then I should be on the home page

  Scenario: TC_002_View Spending Insights options in contextual menu on statements screen
    Given I am an enrolled "current" user logged into the app
    When I select current account tile from home page
    And I select the viewSpendingInsights option from the action menu in statements page
    Then I should be displayed spendingInsights web view page
    When I tap on back button in spending insights page
    Then I should be navigated back to statements page
