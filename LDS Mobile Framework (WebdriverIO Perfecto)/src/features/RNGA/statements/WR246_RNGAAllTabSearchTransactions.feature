@team-statements @nga3 @rnga @sw-search-transactions-on @android
Feature: Search Transactions
  As a NGA Retail customer,
  I want to search my current account transactions,
  So that I can quickly find transactions of interest to me

  #STAMOB-710, 711
  @primary @stub-enrolled-valid
  Scenario Outline: TC_001_Display Search option under All Tab and able to search the transactions
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to current Account With View Transactions account to validate accountNumber and select current Account With View Transactions account
    Then I should be on the allTab of statements page
    And I should see search icon in all tab
    When I tap on search icon in all tab
    Then I should be displayed current Account With View Transactions account name last four digits of account number in the header and no action menu for that current Account With View Transactions name
    And I should be displayed search page with below options
      | fieldsInSearchPage |
      | searchInstruction  |
      | searchDialogBox    |
      | backButton         |
    When I enter the <keyword> in search dialog box
    Then I should be displayed matching transactions with that <keyword> in the search list
    And I should be displayed search page with below options
      | fieldsInSearchPage            |
      | clearSearch                   |
      | searchResultsCount            |
      | searchResultsTransactionsList |
    Examples:
      | keyword      |
      | fin          |
      | 10           |
      | waitrose 132 |

  #STAMOB-712
  @pending @stub-enrolled-valid
  Scenario: TC_002_Display native keyboard for search
    Given I am an enrolled "current and savings account" user logged into the app
    When I select current Account With View Transactions account tile from home page
    And I tap on search icon in all tab
    And I select search
    Then I should be displayed native search keyboard
    When I select return on native keyboard
    Then The native keyboard should be dismissed

  #STAMOB-711
  @stub-enrolled-valid
  Scenario: TC_003_User clears the search results
    Given I am an enrolled "current and savings account" user logged into the app
    When I select current Account With View Transactions account tile from home page
    And I search with virgin in search dialog box
    And I clear the search results
    Then The search word virgin entered should be cleared
    And The results list should be reset

  #Pre condition: User should have more than 60 transactions
  #STAMOB-713
  @primary @stub-enrolled-valid @pending
  Scenario Outline: TC_004_User should be able to search further more transactions
    Given I am an enrolled "current and savings account" user logged into the app
    When I select current Account With View Transactions account tile from home page
    And I search with <keyword> in search dialog box
    Then I should be displayed matching transactions with that <keyword> in the search list
    And I should be displayed search more transactions option
    When I tap on search more transactions
    Then I should be displayed further more transactions available with the same <keyword> in the results
    And The search results count should be updated and to date is updated to last date
    Examples:
      | keyword |
      | fin     |
      | 10      |

  @stub-enrolled-valid
  Scenario: TC_005_Display no more transactions to search message
    Given I am an enrolled "current and savings account" user logged into the app
    When I select current Account With View Transactions account tile from home page
    And I search with wait in search dialog box
    And I tap on search more transactions till all the transactions are loaded
    Then I should not be displayed search more transactions option
    And I am displayed a message that there are no more transactions

  @stub-enrolled-valid
  Scenario: TC_006_User able to navigate back to all tab from search page
    Given I am an enrolled "current and savings account" user logged into the app
    When I select current account tile from home page
    And I tap on search icon in all tab
    And I select to go back
    Then I should be on the allTab of statements page

  @pending @stub-enrolled-valid
  Scenario: TC_007_User able to view all the results in search page when all the transactions are loaded under all tab
    Given I am an enrolled "current and savings account" user logged into the app
    When I select current Account With View Transactions account tile from home page
    And I scroll down to load all the transactions in all tab
    And I search with wait in search dialog box
    Then I should be displayed all the transactions available with the same word wait in the results
    And I should not be displayed search more transactions option

  @primary @stub-enrolled-valid
  Scenario: TC_008_User should be able to view the transaction details in search page
    Given I am an enrolled "current and savings account" user logged into the app
    When I select current Account With View Transactions account tile from home page
    And I search with virgin in search dialog box
    And I select one of the posted transaction
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdContainer            |
      | date                    |
      | vtdCloseButton          |
      | vtdAmount               |
      | transactionTypeField    |
      | transactionDescription  |
      | unsureTransactionButton |
    When I close the posted transactions
    Then I should be on the search page with transactions

    #################### Error Scenarios #############################
  #STAMOB-750
  @stub-rnga-search-error
  Scenario: TC_009_Display error message when unable to receive a response from the server while searching
    Given I am an enrolled "current and savings account" user logged into the app
    When I select currentAccountWithPendingDebitTransactions account tile from home page
    And I search with fin in search dialog box
    And I tap on search more transactions
    Then I am displayed an error message with retry option and red banner

  @stub-enrolled-valid
  Scenario: TC_010_Display no transactions message in search page when user has no transactions
    Given I am an enrolled "current and savings account" user logged into the app
    When I select current Account With No Transactions account tile from home page
    And I search with fin in search dialog box
    Then I should be displayed no transaction message

  @stub-rnga-search-error
  Scenario: TC_011_User not displayed with search option when transactions retrieve fails in all tab
    Given I am an enrolled "current and savings account" user logged into the app
    When I select current Account With View Transactions account tile from home page
    Then I should see error message saying please try again later
    And I should not see the option to search

  @stub-enrolled-valid
  Scenario: TC_012_User displayed with a no results found message when searching further more transactions
    Given I am an enrolled "current and savings account" user logged into the app
    When I select current Account With View Transactions account tile from home page
    And I search with abcd in search dialog box
    And I tap on search more transactions
    Then I should be displayed with a message no results have been found in this date range
    And I should be displayed search more transactions option

    ############## UI Validations #####################

  @pending @stub-enrolled-valid
  Scenario: TC_013_Display search icon above pending transactions accordion when user has pending transactions
    Given I am an enrolled "current and savings account" user logged into the app
    When I select current account tile from home page
    And I should see minimised pending transactions accordion
    Then I should be displayed search icon above pending transactions accordion

  @pending @stub-enrolled-valid
  Scenario: TC_014_Display search icon above posted transactions
    Given I am an enrolled "current and savings account" user logged into the app
    When I select current account tile from home page
    Then I should be displayed search icon above posted transactions

  #spinner image can not be captured in automation
  @manual @stub-enrolled-valid
  Scenario: TC_015_Display search further more transactions at the end of the search results page
    Given I am an enrolled "current and savings account" user logged into the app
    When I select current account tile from home page
    And I search with virgin in search dialog box
    Then I should be displayed search further more transactions button at the end of the search page
    When I tap on search more transactions
    Then I should see loading indicator whilst the transactions load
