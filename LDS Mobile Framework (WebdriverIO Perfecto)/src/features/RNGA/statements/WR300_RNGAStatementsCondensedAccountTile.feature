@team-statements @nga3 @rnga @android
Feature: Condensed Account Tile Statements
  As a NGA Retail customer,
  I want my account balances to show in the account tile on my statements page
  So that I can understand what monies are available

  #STAMOB-831,832
  @stub-enrolled-valid @primary
  Scenario: TC_001_Display current account information on account tile and header in statements page
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to current account to validate accountNumber and select current account
    And I should be on the allTab of statements page
    Then I should be displayed current account name action menu and last four digits of account number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | balanceAfterPending |
      | overDraftLimit      |

  #STAMOB-869,870
  @stub-enrolled-valid @primary
  Scenario: TC_002_Display credit card account information on account tile and header in statements page
    Given I am an enrolled "credit card" user logged into the app
    When I navigate to creditCard account to validate accountNumber and select creditCard account
    And I should be on the recent of statements page
    Then I should be displayed creditCard account name action menu and last four digits of creditCard number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
      | availableCredit     |
      | creditLimit         |
      | overDueAmount       |

  #STAMOB-831,832,889,913,914
  @stub-enrolled-valid @primary
  Scenario: TC_003_Display savings account information on account tile and header in statements page
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to saving account to validate accountNumber and select saving account
    And I should be on the currentMonth of statements page
    Then I should be displayed saving account name action menu and last four digits of account number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile     |
      | accountBalance          |
      | availableFunds          |
      | interestRateLabel       |
      | interestRateDescription |

  @stub-enrolled-valid @primary
  Scenario: TC_004_Display ISA account information on account tile and header in statements page
    Given I am an enrolled "cash Isa" user logged into the app
    When I navigate to cashIsa account to validate accountNumber and select cashIsa account
    And I should be on the currentMonth of statements page
    Then I should be displayed cashIsa account name action menu and last four digits of account number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile     |
      | accountBalance          |
      | remainingAllowanceLabel |
      | remainingAllowanceISA   |
      | interestRateLabel       |
      | interestRateDescription |

  @stub-enrolled-valid @primary
  Scenario: TC_005_Display Help To Buy ISA account information on account tile and header in statements page
    Given I am an enrolled "htb Isa" user logged into the app
    When I navigate to htbIsa account to validate accountNumber and select htbIsa account
    And I should be on the currentMonth of statements page
    Then I should be displayed htbIsa account name action menu and last four digits of account number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
      | interestRateLabel   |
      | htbInterestRate     |

  #STAMOB-891,889
  @stub-enrolled-valid @primary
  Scenario: TC_006_Display account information on account tile and header in statements page for loan account
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to personal Loan account to validate accountNumber and select personal Loan account
    And I should be on the currentYear of statements page
    Then I should be displayed personal Loan account name action menu and last four digits of loan number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
      | originalLoan        |

  #STAMOB-913,914
  @stub-enrolled-valid @primary
  Scenario: TC_007_Display account information on account tile and header in statements page for mortgage account
    Given I am an enrolled "mortgage_account" user logged into the app
    When I navigate to mortgage account to validate accountNumber and select mortgage account
    And I should be on the mortgageSummary of statements page
    Then I should be displayed mortgage account name action menu and last four digits of mortgageAccount number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile    |
      | accountBalance         |
      | monthlyPayment         |
      | balanceAsOf            |
      | NotFinalSettlementText |

  #STAMOB-907,908, 909, #MOB3-10221
  @stub-enrolled-valid @primary
  Scenario: TC_008_Display insurance account information on account tile and header in statements page and hide account tile when user scroll up
    Given I am an enrolled "home_insurance" user logged into the app
    When I select homeInsurance account tile from home page
    Then I should be displayed homeInsurance account name last four digits of policy number in the header and no action menu for that homeInsurance name
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | policyCoverType     |
      | insuredAddress      |
      | periodOfCover       |
    When I scroll down on the transactions
    Then I should not be displayed account tile in statements page
    When I scroll back to the top of my transactions
    Then The account tile will reappear

  #STAMOB-833,871,915,921,915,892
  @stub-enrolled-valid @primary
  Scenario Outline: TC_009_Display current balance in the header when user scroll up in statements page
    Given I am an enrolled "current and savings account" user logged into the app
    When I select <type> account tile from home page
    And I should be on the <monthsTab> of statements page
    And I scroll down on the transactions
    Then I should be displayed current balance in the header alongside account Name
    And I should not be displayed account tile in statements page
    And I should be displayed month tabs in place of account tile
    When I scroll back to the top of my transactions
    Then The current balance will no longer display in the header and the account tile will reappear
    Examples:
      | type          | monthsTab       |
      | current       | allTab          |
      | creditCard    | recent          |
      | saving        | currentMonth    |
      | personal Loan | currentYear     |
      | mortgage      | mortgageSummary |

  #STAMOB-971
  @stub-enrolled-valid @secondary @pending
  Scenario Outline: TC_010_User able to access the action menu in statements page
    Given I am an enrolled "current and savings account" user logged into the app
    When I select <type> account tile from home page
    Then I should be on the <monthsTab> of statements page
    When I select the action menu from statements page
    Then I should be displayed action menu with list of options
    Examples:
      | type          | monthsTab       |
      | current       | allTab          |
      | creditCard    | recent          |
      | saving        | currentMonth    |
      | personal Loan | currentYear     |
      | mortgage      | mortgageSummary |

  #STAMOB-919
  @stub-scottish-widows-2-accounts @primary
  Scenario: TC_011_Display pension in the header when user scroll up in statements page
    Given I am an enrolled "Scottish Widows" user logged into the app
    When I navigate to scottishWidows account to validate accountNumber and select scottishWidows account
    Then I should be displayed scottishWidows account name last four digits of policy number in the header and no action menu for that scottishWidows name
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
      | fundValueDate       |

  @stub-enrolled-valid @secondary
  Scenario Outline: TC_012_Validate condensed account tile state when user navigates to previous months
    Given I am an enrolled "current and savings account" user logged into the app
    When I select <type> account tile from home page
    And I scroll down on the transactions
    And I swipe to see previous month transactions on the account statement page
    Then I should be on the same previous month statements page
    And I should not be displayed account tile in statements page
    Examples:
      | type          |
      | current       |
      | saving        |
      | personal Loan |
      | creditCard    |

  @stub-enrolled-valid @secondary
  Scenario: TC_013_View personal loan(CBS) account details on account tile
    Given I am an enrolled "statement_valid" user logged into the app
    When I navigate to personal Cbs Loan account to validate accountNumber and select personal Cbs Loan account
    Then I should be displayed personal Cbs Loan account name last four digits of loan number in the header and no action menu for that personal Cbs Loan name
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
    When I scroll down on the transactions
    Then The account tile should not be hidden

  @stub-enrolled-valid @secondary
  Scenario Outline: TC_014_Display offset and repossessed mortgage account information on account tile and header in statements page
    Given I am an enrolled "mortgage_account" user logged into the app
    When I navigate to <type> account to validate accountNumber and select <type> account
    Then I should be displayed <type> account name last four digits of mortgageAccount number in the header and no action menu for that <type> name
    And I should see account details with the following fields
      | fieldsInAccountTile    |
      | accountBalance         |
      | monthlyPayment         |
      | balanceAsOf            |
      | NotFinalSettlementText |
    When I scroll down on the transactions
    Then The account tile should not be hidden
    Examples:
      | type                |
      | rePossessedMortgage |
      | offSetMortgage      |

  @stub-enrolled-valid @secondary
  Scenario: TC_015_Validate condensed account tile state when user navigates to sub account and details tab for mortgage account
    Given I am an enrolled "mortgage_account" user logged into the app
    When I select mortgage account tile from home page
    And I scroll down on the transactions
    And I select sub account tab
    Then I should not be displayed account tile in statements page
    When I tap on any of the sub account
    Then I should be displayed with details tab
    And I should not be displayed account tile in statements page
