@team-statements @nga3 @rnga @stub-enrolled-valid @sw-upcoming-payments-on
Feature: Pending Payments
  As a NGA Retail customer,
  I want to view my pending payment transactions,
  So that I am better informed about my transactions

  #MOB3-1458,6070,6069,1457
  @primary @environmentOnly
  Scenario: TC_001_View Upcoming Payments options in contextual menu on home screen
    Given I am an enrolled "statement_valid" user logged into the app
    And I navigate to current Account With View Transactions account on home page
    When I select the action menu of current Account With View Transactions account
    Then I should see viewUpcomingPayment option in the action menu
    When I select viewUpcomingPayment option
    And I should be displayed upcomingPayments web view page

  #MOB3-6071,1459
  @primary
  Scenario: TC_002_View payments due soon options in contextual menu on statements page and validate due soon fields
    Given I am an enrolled "statement_valid" user logged into the app
    And I select saving account tile from home page
    When I select the viewPaymentsDueSoon option from the action menu in statements page
    Then I should see dueSoon tab in statements screen
    And I should validate fields in due soon screen
      | fieldsToValidate            |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    When I select one of the pending payment transaction
    Then I should see upcoming transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | dueSoonLozenge          |
      | datePendingPayment      |
      | vtdCloseButton          |
      | vtdAmount               |
      | outgoingPaymentText     |
      | payeeField              |
      | sortCodeAccountNumber   |
      | referenceField          |
      | changeOrDeleteText      |

  @secondary
  Scenario Outline: TC_003_Payments due soon option should not be displayed in contextual menu for credit account on home screen
    Given I am an enrolled "statement_valid" user logged into the app
    When I navigate to <type> account on home page
    Then I should not see payments due soon option on the action menu of <type> account
    Examples:
      | type       |
      | current    |
      | creditCard |

  Scenario Outline: TC_004_Payments due soon option should not be displayed in contextual menu for credit account on statements page
    Given I am an enrolled "statement_valid" user logged into the app
    When I select <type> account tile from home page
    Then I should not see viewPaymentsDueSoon option in the action menu of statements page
    Examples:
      | type       |
      | current    |
      | creditCard |

  #COSE-593
  Scenario: TC_005_Payments due soon option should not be displayed in contextual menu once user navigates to current month from due soon tab
    Given I am an enrolled "statement_valid" user logged into the app
    And I select saving account tile from home page
    And I select the viewPaymentsDueSoon option from the action menu in statements page
    When I swipe to see current month transactions on the account statement page
    Then I should not see viewPaymentsDueSoon option in the action menu of statements page

  @secondary
  Scenario: TC_006_No pending payments message in due soon tab
    Given I am an enrolled "statement_valid" user logged into the app
    And I select saving Account With No Pending Payments account tile from home page
    When I select the viewPaymentsDueSoon option from the action menu in statements page
    Then I should see dueSoon tab in statements screen
    And I should see no pending payments message in statements screen

  Scenario: TC_007_Close Pending Transaction Details With Close Button
    Given I am an enrolled "statement_valid" user logged into the app
    And I select saving account tile from home page
    And I select the viewPaymentsDueSoon option from the action menu in statements page
    And I select one of the pending payment transaction
    When I close the pending transactions
    Then I should see dueSoon tab in statements screen

  #web view
  @environmentOnly @manual
  Scenario: TC_008_View upcoming payments web page and validate options in the web page
    Given I am an enrolled "statement_valid" user logged into the app
    And I select current Account With View Transactions account tile from home page
    When I select the viewUpcomingPayment Option from the action menu in statements page
    Then I should be displayed upcoming payments web view page
    And I should be displayed below fields in the page
      | options   |
      | dropDown  |
      | viewAllDirectDebitStandingOrderButton |
    When I select date range from the date picker
    Then I should be displayed Standing Order Direct Debit and Future Dated payments of that account

  #web view
  @environmentOnly @manual
  Scenario: TC_009_User navigates to standing order and direct debit page via upcoming payments page
    Given I am an enrolled "statement_valid" user logged into the app
    And I select current Account With View Transactions account tile from home page
    And I select the viewUpcomingPayment Option from the action menu in statements page
    When I tap on view all direct debits and standing orders button
    Then I should be navigated to direct debit standing order page

  ########################### Exploratory scenarios ####################################
  #STAMOB-593
  @android
  Scenario: TC_010_Navigate back to statements page from Upcoming payments web page
    Given I am an enrolled "statement_valid" user logged into the app
    When I select current Account With View Transactions account tile from home page
    And I select the viewUpcomingPayment option from the action menu in statements page
    And I tap on back button in upcoming payments page
    Then I should be navigated back to statements page

  #STAMOB-616
  Scenario: TC_011_View Pending Payment via Payments & Transfer page
    Given I am an enrolled "statement_valid" user logged into the app
    When I select current Account With View Transactions account tile from home page
    And I choose the recipient account on the payment hub page
    And I select manage on an pending payment account
    And I select to view pending payment option
    Then I should see dueSoon tab in statements screen
    And I should validate fields in due soon screen
      | fieldsToValidate            |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
