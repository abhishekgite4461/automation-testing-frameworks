@team-statements @nga3 @rnga @stub-enrolled-valid
Feature: Saving Interest Rate Details
  As a NGA Customer
  I should be able to view interest rate for my saving account
  So that I am aware of the interest rate on my saving account

  @primary
  Scenario: TC_001_Display single tier interest rate when error flag is false and entry point is tap & hold(long press) on home page account tile
    Given I am an enrolled "savings account" user logged into the app
    And I navigate to saving account on home page
    When I tap and hold on the saving account tile to Savings Interest Rate Details screen
    Then I should see the following fields in the details screen
      | fieldsInDetailsScreen   |
      | accountName             |
      | accountBalance          |
      | interestRateLabel       |
      | interestRateDescription |
    When I close the interest rate details screen using the close button
    Then I should be on the home page

  @primary @ios
  Scenario Outline: TC_002_Display single tier interest rate when error flag is false and entry point is tap & hold(long press) on statement screen account tile
    Given I am an enrolled "current and savings account" user logged into the app
    And I select <type> account tile from home page
    When I tap and hold on the saving account tile to Savings Interest Rate Details screen
    And I should be on Savings Interest Rate Details screen
    Then I close the interest rate details screen using the close button
    Examples:
      | type   |
      | saving |

  @primary @android
  Scenario: TC_003_Display single tier interest rate when error flag is false and interest rate shown next to Interest rate label on statement screen account tile
    Given I am an enrolled "current and savings account" user logged into the app
    When I select saving account tile from home page
    Then I should see the Interest rate next to Interest rate label on account tile

  @primary
  Scenario: TC_004_Display single tier interest rate when error flag is false and entry point is view interest rate details option in actions menu
    Given I am an enrolled "savings account" user logged into the app
    And I navigate to saving account on home page
    When I select View Interest Details option of saving account from action menu
    Then I should be on Savings Interest Rate Details screen
    When I close the interest rate details screen using the close button
    Then I should be on the home page

  @stubOnly
  Scenario: TC_005_Display two tier interest rates when error flag is false & entry point is View Interest Rate Detail
    Given I am an enrolled "statement_valid" user logged into the app
    And I select youngSaver account tile from home page
    When I select the View Interest Details option from the action menu in statements page
    Then I should be on Savings Interest Rate Details screen
    And I should see the following fields in the details screen
      | fieldsInDetailsScreen     |
      | accountName               |
      | accountBalance            |
      | interestRateForFirstTier  |
      | interestRateForSecondTier |

  @stubOnly @ios
  Scenario: TC_006_Display two tier interest rates when error flag is false & entry point is View Interest Rate Detail
    Given I am an enrolled "statement_valid" user logged into the app
    And I select youngSaver account tile from home page
    When I select the View Interest Details option from the action menu in statements page
    Then I should be on Savings Interest Rate Details screen
    And I should see the following fields in the details screen
      | fieldsInDetailsScreen     |
      | accountName               |
      | accountBalance            |
      | interestRateForFirstTier  |
      | interestRateForSecondTier |

  @stubOnly @android
  Scenario: TC_007_Display two tier interest rates when error flag is false & entry point is View rates on account tile
    Given I am an enrolled "statement_valid" user logged into the app
    And I select youngSaver account tile from home page
    And I should see the View rates link next to Interest rate label on account tile
    When I tap on View rates link
    Then I should be on Savings Interest Rate Details screen
    And I should see the following fields in the details screen
      | fieldsInDetailsScreen     |
      | accountName               |
      | accountBalance            |
      | interestRateForFirstTier  |
      | interestRateForSecondTier |

  @stubOnly
  Scenario: TC_008_Display multi tier (three tier) interest rates when error flag is false & entry point is View Interest Rate Detail
    Given I am an enrolled "statement_valid" user logged into the app
    And I select instantCashIsa account tile from home page
    When I select the View Interest Details option from the action menu in statements page
    Then I should be on Savings Interest Rate Details screen
    And I should see the following fields in the details screen
      | fieldsInDetailsScreen     |
      | accountName               |
      | accountBalance            |
      | interestRateForFirstTier  |
      | interestRateForSecondTier |
      | interestRateForThirdTier  |

  @stubOnly
  Scenario: TC_009_Hide Interest Rate when error flag is true and entry point is tap & hold(long press) and action menu
    Given I am an enrolled "statement_valid" user logged into the app
    And I navigate to savingAccountWithNoInterestRate account on home page
    When I tap and hold on the savingAccountWithNoInterestRate account tile to Savings Interest Rate Details screen
    Then I should not be on Savings Interest Rate Details screen
    And I should not see view interest rate details option in the action menu

  @stubOnly
  Scenario: TC_010_Hide Interest Rate when error flag is true and entry point is action menu on statement page
    Given I am an enrolled "statement_valid" user logged into the app
    When I select savingAccountWithNoInterestRate account tile from home page
    And I should be on the current month statements tab
    Then I should not see View Interest Details option in the action menu of statements page

  @stubOnly @android
  Scenario Outline: TC_011_Display interest rate value on statements page account tile
    Given I am an enrolled "statement_valid" user logged into the app
    When I select <type> account tile from home page
    Then I should be on the current month statements tab
    And I should see <output> interest rate value on account tile
    Examples:
      | type                            | output        |
      | savingAccountWithNoInterestRate | notAvailable  |
      | easySaver                       | percent       |
      | youngSaver                      | viewRatesLink |

  @stubOnly @ios
  Scenario Outline: TC_012_Display interest rate value on statements page account tile
    Given I am an enrolled "statement_valid" user logged into the app
    When I select <type> account tile from home page
    Then I should be on the current month statements tab
    And I should see <output> interest rate value on account tile
    Examples:
      | type                            | output       |
      | savingAccountWithNoInterestRate | notAvailable |
      | easySaver                       | percent      |
      | youngSaver                      | tapAndHold   |

  @android
  Scenario: TC_013_User shouldn't be able to long press on to account tile to view Savings Interest Rate Details modal
    Given I am an enrolled "current and savings account" user logged into the app
    And I select saving account tile from home page
    When I tap and hold on the account tile to Savings Interest Rate Details screen
    Then I should not be on Savings Interest Rate Details screen
