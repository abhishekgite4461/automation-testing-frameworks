@team-statements @nga3 @rnga @stub-rnga-hcc @primary
Feature: Remaining OverDraft
  As a NGA Retail customer,
  I want my Statements page to display remaining overdraft,
  So that I am always informed of the limit of my overdraft facility

  #STAMOB-1668, 1669, 1691, 1692
  Scenario: TC_01 Display overdraft remaining and progress bar
    Given I am a current user on the home page
    When I select currentAccountWithUsedOverDraft account tile from home page
    Then I should see account details with the following fields
      | fieldsInAccountTile      |
      | balanceAfterPendingLabel |
      | balanceAfterPendingValue |
      | overDraftLimitLabel      |
      | overDraftLimitValue      |
      | overDraftRemainingLabel  |
      | overDraftRemainingValue  |
    And I should see the remaining overdraft limit progress bar

  #STAMOB-1668, 1669, 1691, 1692
  Scenario Outline: TC_02 Do not display overdraft remaining (Overdraft not in use & no overdraft) and no progress bar
    Given I am a current user on the home page
    When I select <type> account tile from home page
    Then I should see account details with the following fields
      | fieldsInAccountTile      |
      | balanceAfterPendingLabel |
      | balanceAfterPendingValue |
      | overDraftLimitLabel      |
      | overDraftLimitValue      |
    And I should not see the remaining overdraft limit progress bar
    Examples:
      | type                              |
      | currentAccountWithUnusedOverDraft |
      | currentAccountWithNoOverDraft     |
