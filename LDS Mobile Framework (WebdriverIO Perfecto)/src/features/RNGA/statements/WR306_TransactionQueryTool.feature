@team-statements @nga3 @rnga @android @ff-tqt-on @sw-tqt-android-on @manual
#Stubs can't be created and to run on environment, need to login on live person and clear the chat every time
Feature: Transaction query tool
  As a NGA customer
  I want to access a virtual assistant (Chatbot) from the Transaction help screen
  So that I can get help relating to a transaction displayed on my statement

  @primary
  Scenario Outline: TC_001_Display welcome message for posted and pending transactions
    Given I am an enrolled "current" user logged into the app
    And I select current account tile from home page
    And I select <transactionType> on statements page
    When I launch the Chatbot
    Then I should see a question populated within the welcome message
    Examples:
      | transactionType                |
      | cleared debit card transaction |
      | pending debit card transaction |

  @primary
  Scenario Outline: TC_002_Send transaction details and initiate VA for posted and pending transactions
    Given I am an enrolled "current" user logged into the app
    And I select current account tile from home page
    And I select <transactionType> on statements page
    And I have launched the chat bot and welcome message has appeared
    When I select a bubble option <option>
    Then I should see a response auto-populated with the following details
      | fieldsToValidate |
      | <recognition>    |
      | date             |
      | amount           |
      | transaction Type |
      | payment Type     |
      | merchant Name    |
    Examples:
      | transactionType                | option | recognition     |
      | cleared debit card transaction | Yes    | recognise       |
      | cleared debit card transaction | No     | don't recognise |
      | pending debit card transaction | Yes    | recognise       |
      | pending debit card transaction | No     | don't recognise |

  @secondary
  Scenario Outline: TC_003_Don't show VA(chat bot) for cheque pending transactions & credit card transactions
    Given I am an enrolled "current & credit card" user logged into the app
    And I select <accountType> account tile from home page
    And I select <transactionType> on statements page
    And I select the help with this transaction link in VTD screen
    And I must be displayed the Unsure About Transactions Help and Info screen
    When I tap on contact us link
    Then I should be on the call us home page
    Examples:
      | accountType | transactionType            |
      | current     | pending Cheque transaction |
      | credit card | Non debit                  |
      | credit card | contact less               |
