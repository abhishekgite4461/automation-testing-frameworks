@nga3 @rnga @team-statements @sw-totalpending-android-on @ff-exposed-pendingtransactions-on
Feature: Exposed Pending Transactions
  In order to view the statements for the nominated credit card
  As a NGA customer
  I want to see an exposed view of my pending transactions

  #STAMOB-1729, 1734, 1732
  @primary @stub-enrolled-valid
  Scenario: TC_001 View Debit and Cheque Pending Transaction Details under all tab
    Given I am an enrolled "current" user logged into the app
    When I select current Account With Pending Transactions account tile from home page
    Then I should not see the pending transactions accordion
    And I should see exposed pending transactions ordered below
      | detail                                     |
      | unclearedChequesHeader                     |
      | totalChequesPending                        |
      | chequeNoteText                             |
      | exposedPendingChequeTransactionDescription |
      | exposedPendingChequeTransactionAmount      |
      | chevronOnPendingChequeLine                 |
      | pendingHeader                              |
      | totalPending                               |
      | exposedPendingDebitTransactionDescription  |
      | exposedPendingDebitTransactionAmount       |
      | accountBalanceHeader                       |
      | chevronOnPendingTransactionLine            |
    When I select exposed Pending Cheque on statements page
    Then I should see pending transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | vtdAmount               |
      | notYetAvailable         |
      | chequesText             |
      | payeeField              |
      | depositedOn             |
      | availableBy             |
      | creditDisclaimerText    |
      | unsureTransactionButton |
    When I close the pending transactions
    Then I should be on the allTab of statements page
    When I select exposed Pending Direct Debit on statements page
    Then I should see pending transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | vtdAmount               |
      | debitCardText           |
      | payeeField              |
      | dateOfTransaction       |
      | debitDisclaimerText     |
      | unsureTransactionButton |
    When I close the pending transactions
    Then I should be on the allTab of statements page

  #STAMOB-1733
  @stub-enrolled-valid @pending
  Scenario Outline: TC_002 Display Total Balance for Pending Transactions
    Given I am an enrolled "statement_valid" user logged into the app
    When I select current Account With Pending Transactions account tile from home page
    Then I should see total <type> balance in the <header> which is sum of all <type> transactions
    Examples:
      | type          | header                 |
      | chequePending | unclearedChequesHeader |
      | debitPending  | pendingHeader          |

  @primary @stub-enrolled-valid
  Scenario: TC_003 View Debit and Cheque Pending Transaction Details under current month tab
    Given I am an enrolled "current" user logged into the app
    When I select current Account With Pending Transactions account tile from home page
    And I swipe to see previous month transactions on the account statement page
    Then I should not see the pending transactions accordion
    And I should see exposed pending transactions ordered below
      | detail                                     |
      | unclearedChequesHeader                     |
      | totalChequesPending                        |
      | chequeNoteText                             |
      | exposedPendingChequeTransactionDescription |
      | exposedPendingChequeTransactionAmount      |
      | chevronOnPendingChequeLine                 |
      | pendingHeader                              |
      | totalPending                               |
      | exposedPendingDebitTransactionDescription  |
      | exposedPendingDebitTransactionAmount       |
      | accountBalanceHeader                       |
      | chevronOnPendingTransactionLine            |

  #STAMOB-1826
  @stub-pending-transactions-not-retrieved
  Scenario: TC_004 Pending debit, cheque transactions cannot be retrieved
    Given I am a pending transactions not retrieved user on the home page
    When I select currentAccountWithPendingTransactionsFailed account tile from home page
    Then I should see chequePending error message under unclearedChequesHeader respectively
    And I should see debitPending error message under pendingHeader respectively

  #STAMOB-1826
  @stub-pending-transactions-not-retrieved
  Scenario Outline: TC_005 Pending debit or cheque transactions are not retrieved
    Given I am a pending transactions not retrieved user on the home page
    When I select <type> account tile from home page
    Then I should see <errorMessage> error message under <header> respectively
    Examples:
      | type                                              | errorMessage  | header                 |
      | currentAccountWithChequePendingTransactionsFailed | chequePending | unclearedChequesHeader |
      | currentAccountWithDebitPendingTransactionsFailed  | debitPending  | pendingHeader          |

  @stub-pending-chequeonly-debitonly
  Scenario Outline: TC_006 View Pending debit only, cheque only transactions
    Given I am a pending chequeonly debitonly user on the home page
    When I select <type> account tile from home page
    Then I should see <pendingType> pending transactions
    Examples:
      | type                                          | pendingType |
      | currentAccountWithNoDebitPendingTransactions  | chequeOnly  |
      | currentAccountWithNoChequePendingTransactions | debitOnly   |

  #MOB3-4326
  @stub-enrolled-valid @pending
  Scenario: TC_007 Verify do not display pending transactions accordion
    Given I am an enrolled "statement_valid" user logged into the app
    When I select current Account With View Transactions account tile from home page
    Then I should not see the pending or cheque transactions
    And I should see the cleared transactions
