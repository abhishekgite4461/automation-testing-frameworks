@team-statements @nga3 @rnga @stub-enrolled-valid @stubOnly
Feature: Credit Card View Transaction Details
  As a NGA Customer
  I should be able to view transaction details for credit card account
  So that I am better informed about a specific transaction

  Background:
    Given I am an enrolled "statement_valid" user logged into the app
    And I select credit Card Account With View Transactions account tile from home page

  #MOB3-6274,6275
  @primary
  Scenario: TC_001_View Online Credit Card Posted Transaction Details fields (Online/phone/mail order purchase)
    When I select online Credit Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | onlinePhoneMailText          |
      | payeeField                   |
      | cardEndingNumber             |
      | dateOfTransaction            |

  #STAMOB-47
  @primary
  Scenario: TC_002_View ContactLess Card Purchase Posted Transaction Details fields (Contactless purchase)
    When I select contact Less Purchase Credit Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | contactLessPurchaseText      |
      | payeeField                   |
      | googleMap                    |
      | cardEndingNumber             |
      | dateOfTransaction            |
      | unsureTransactionButton      |

  @primary
  Scenario: TC_003_View Chip and PIN Card Purchase Posted Transaction Details fields (Chip and PIN Purchase)
    When I select chip And Pin Purchase Credit Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | chipAndPinText               |
      | payeeField                   |
      | googleMap                    |
      | cardEndingNumber             |
      | dateOfTransaction            |
      | unsureTransactionButton      |

  @secondary
  Scenario: TC_004_View Manual Or Paper Card Posted Transaction Details fields (Manual/Paper)
    When I select manual Or Paper Credit Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | manualPaperText              |
      | payeeField                   |
      | cardEndingNumber             |
      | dateOfTransaction            |

  #no test data (need to discuss with BA)
  @manual @secondary
  Scenario: TC_005_View Magnetic Stripe Card Posted Transaction Details fields (Magnetic Stripe)
    When I select magnetic Stripe Credit Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | MagneticStripeText           |
      | payeeField                   |
      | cardEndingNumber             |
      | dateOfTransaction            |

  #no test data
  @manual
  Scenario: TC_006_View Authorisation Method Not Available Card Posted Transaction Details fields (No Auth)
    When I select authorisation Method Not Available Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | creditCardText               |
      | payeeField                   |
      | cardEndingNumber             |
      | dateOfTransaction            |

  @primary
  Scenario: TC_007_View Direct Debit Card Posted Transaction Details fields (Direct Debit In)
    When I select direct Debit In Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | creditCardText               |
      | payeeField                   |
      | cardEndingNumber             |
      | dateOfTransaction            |

  #no test data
  @manual @primary
  Scenario: TC_009_View Chip and PIN Card Purchase Posted Transaction Details fields (Chip and PIN Purchase Cash Back)
    When I select chip And Pin Purchase Cash Back Credit Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | cashBack                     |
      | unsureTransactionButton      |
      | chipAndPinText               |
      | cashBackReferenceField       |
      | payeeField                   |
      | cardEndingNumber             |
      | dateOfTransaction            |

  #need to be discussed with BA to validate the scenario
  @manual @secondary
  Scenario: TC_010_View Foreign Currency Online Card Posted Transaction Details fields (Foreign Currency Online )
    When I select foreign Currency Online Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | foreignCurrency              |
      | unsureTransactionButton      |
      | onlinePhoneMailText          |
      | payeeField                   |
      | cardEndingNumber             |
      | dateOfTransaction            |

  #MOB3-6425,6426
  #no stub data
  @manual @secondary
  Scenario: TC_011_View Cash Withdrawal At Branch Posted Transaction Details fields (Cash Withdrawal Branch Credit Card)
    When I select cash Withdrawal Branch Credit Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | creditCardText               |
      | payeeField                   |
      | cardEndingNumber             |
      | dateOfTransaction            |

  @primary
  Scenario: TC_012_View Cash Withdrawal At ATM Posted Transaction Details fields (Credit Card ATM)
    When I select cash Withdrawal ATM Credit Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | chipAndPinText               |
      | payeeField                   |
      | cardEndingNumber             |
      | dateOfTransaction            |

  #MOB3-6430,6428
  @primary
  Scenario: TC_013_View Charges Or Fees Posted Transaction Details fields (Charges/Fees/AnnualFees)
    When I select charges Credit Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | creditCardText               |
      | payeeField                   |
      | cardEndingNumber             |
      | dateOfTransaction            |

  #no test data
  @manual @primary
  Scenario: TC_014_View Money Transfer Posted Transaction Details fields (Money Transfer)
    When I select money Transfer Credit Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | creditCardText               |
      | payeeField                   |
      | cardEndingNumber             |
      | dateOfTransaction            |

  @primary
  Scenario: TC_015_View Interest Posted Transaction Details fields (Interest On Outstanding Balance)
    When I select interest Credit Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | creditCardText               |
      | payeeField                   |
      | cardEndingNumber             |
      | dateOfTransaction            |

  #MOB3-6316,6375
  @secondary
  Scenario: TC_016_View Cash Payment At Branch Posted Transaction Details fields (Cash Payment)
    When I select cash Payment At Branch on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | creditCardText               |
      | payeeField                   |
      | cardEndingNumber             |

  @primary
  Scenario: TC_017_View Cheque Payment Posted Transaction Details fields (Cheque Payment)
    When I select cheque Payment on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | creditCardText               |
      | payeeField                   |
      | cardEndingNumber             |

  #no stub data
  @manual @secondary
  Scenario: TC_018_View Bank Giro Payment Posted Transaction Details fields (BankGiro Payment)
    When I select bank Giro Payment on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | creditCardText               |
      | payeeField                   |
      | cardEndingNumber             |

  #no stub data
  @manual @primary
  Scenario: TC_019_View Electronic Payment Posted Transaction Details fields (Electronic Payment)
    When I select electronic Payment on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | creditCardText               |
      | payeeField                   |
      | cardEndingNumber             |

  #no stub data
  @manual @primary
  Scenario: TC_020_View Balance Transfer Posted Transaction Details fields (Balance Transfer)
    When I select balance Transfer on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | creditCardText               |
      | payeeField                   |
      | cardEndingNumber             |

  #no stub data
  @manual @primary
  Scenario: TC_021_View Payment By Refunds Posted Transaction Details fields (Refunds)
    When I select refunds on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | creditCardText               |
      | payeeField                   |
      | cardEndingNumber             |

  #no stub data
  @manual @primary
  Scenario: TC_022_View Payment By Rewards Posted Transaction Details fields (Rewards)
    When I select rewards on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | creditCardText               |
      | payeeField                   |
      | cardEndingNumber             |

  #STAMOB-47
  @manual
  Scenario: TC_023_Exception Scenario: User is not displayed Google maps for chip and pin or contactless transaction for multiple location results
    When I select a debit transaction
    And the authorisation method is Chip & Pin or Contactless
    And there is multiple location responses returned
    Then I should not be displayed google maps for the transaction on the VTD
