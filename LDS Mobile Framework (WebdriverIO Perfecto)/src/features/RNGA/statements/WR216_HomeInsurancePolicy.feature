@team-statements @nga3 @rnga
Feature: Home Insurance Policy Details
  In order to be better informed about my home insurance policy
  As a RNGA Retail customer,
  I want to view my home insurance policy details in RNGA

  #MOB3-10221
  # remove this scenario once condensed account tile is done for iOS
  @primary @stub-enrolled-valid @ios
  Scenario: TC_001_User views their insurance policy on the homepage
    Given I am an enrolled "home_insurance" user logged into the app
    When I select homeInsurance account tile from home page
    Then I should see account details with the following fields
      |fieldsInAccountTile            |
      |homeInsuranceStaticProductTitle|
      |policyCoverType                |
      |insuredAddress                 |
      |periodOfCover                  |

  #MOB3-1686
  @primary @stub-enrolled-valid
  Scenario: TC_002_User views their insurance policy details
    Given I am an enrolled "home_insurance" user logged into the app
    When I select homeInsurance account tile from home page
    Then I should be displayed policy details with the following fields
      |fieldsInDetails              |
      |policyNumber                 |
      |enquiriesPhoneNumber         |
      |policyUpdatedDate            |
      |policyHolders                |
      |insuredPropertyAddress       |
      |whatsIncludedInPolicyLink    |
      |optionalCoverYouCouldAddLink |
      |costAndPaymentDetailsLink    |
      |policyScheduleNote           |
      |termsDisclaimer              |

  @environmentOnly
  Scenario: TC_003_User views insurance policy core coverage and sub-cover details
    Given I am an enrolled "home_insurance" user logged into the app
    And I select homeInsurance account tile from home page
    When I select to view whats Included In Policy details
    Then I should be displayed coverage details with the following fields
      |fieldsInDetails    |
      |coreCoverName      |
      |coreCoverLimit     |
      |coreCoverExcess    |
      |subCoverName       |
      |subCoverLimit      |
      |subCoverExcess     |
      |subCoverPremium    |

  @environmentOnly
  Scenario: TC_004_User views Optional cover
    Given I am an enrolled "home_insurance" user logged into the app
    And I select homeInsurance account tile from home page
    When I select to view optional Cover You Could Add details
    Then I should be displayed optionalCoverage details with the following fields
      |fieldsInDetails         |
      |optionalCoverName       |
      |optionalCoverDescription|

  @environmentOnly
  Scenario: TC_005_User views payment details for my insurance policy
    Given I am an enrolled "home_insurance" user logged into the app
    And I select homeInsurance account tile from home page
    When I select to view cost And Payment Details details
    Then I should be displayed payment details with the following fields
      |fieldsInDetails          |
      |homeInsuranceTitle       |
      |premiumText              |
      |totalAnnualCost          |
      |monthlyAmount            |
      |monthlyPaymentDay        |
      |paymentMethod            |
      |paymentTypeText          |
      |policyPaymentDisclaimer  |

  # test data not available to test the below scenarios
  @manual
  Scenario: TC_006_User views different Home Insurance policies on different account tiles on Homepage
    Given I have more than one and less than 11 active home insurance policies
    When I view the Homepage
    Then I should be displayed each policy separately
    And I should be displayed the home insurance policies in the order of oldest policy first and newest last.

  @manual
  Scenario: TC_007_Joint account holders should be able to view the same policy under their respective logon profiles
    Given I have an active home insurance policy
    And I have a joint account
    When I view the Homepage
    Then I should be able to see the Home Insurance policy on my Homepage under my login
    And the joint account holder must be able to view the same Home Insurance policy under his/her login.

  @manual
  Scenario Outline: TC_008_User should not be able to see policies that have expired, cancelled, or have been void
    Given I am an enrolled "valid" user logged into the app
    And I select <type> account tile from home page
    Then I should not be able to see the Home Insurance information
    Examples:
      |type|
      |expired|
      |cancelled |
      |void      |

  @manual
  Scenario: TC_009_User is not shown policy details & shown a message (HC-058) when there are more than 10 insurance policies (MG-2857 & MG-2852)
    Given I am an enrolled "home_insurance" user logged into the app
    When I view the Homepage
    Then I must be displayed only one home insurance account with a message (HC-058) informing that I have more than 10 policies
    When I view the home insurance account for more information
    Then I should be displayed messages on the policy details screen asking the customer to contact the bank for more information on their policies.
