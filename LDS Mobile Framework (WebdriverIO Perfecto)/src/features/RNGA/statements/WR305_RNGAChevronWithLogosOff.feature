@team-statements @nga3 @rnga @stub-enrolled-valid @sw-transaction-logo-android-off @secondary @android
Feature: Current and Savings Account Statements
  As a NGA Retail customer,
  I want to view my current & savings account statements,
  So that I am better informed about my transactions

 # VTD engagement - chevron with transaction logos off
  Scenario Outline: TC_001_View current month transactions & loading of transactions in the all tab & current month
    Given I am an enrolled "current" user logged into the app
    When I select <type> account tile from home page
    And I view <monthTab> transactions of current account
    Then I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
      | chevron                     |
    And I should not see the green or blue vertical bar
    When I tap on the pending transactions accordion
    Then I should not see a chevron on pending transaction lines
    And I should scroll down to the end of the transactions to view <output> message of that <monthTab> tab
    Examples:
      | type           | output        | monthTab     |
      | currentAccount | currentEndOf  | allTab       |
      | currentAccount | currentInOuts | currentMonth |
