@team-statements @nga3 @rnga @stub-enrolled-valid @sw-transaction-logo-android-on
Feature: Current and Savings Account Statements
  As a NGA Retail customer,
  I want to view my current & savings account statements,
  So that I am better informed about my transactions

  # VTD engagement - chevron with transaction logos on
  @primary
  Scenario Outline: TC_001_View current month transactions & loading of transactions in the all tab & current month
    Given I am an enrolled "current" user logged into the app
    When I select <type> account tile from home page
    And I view <monthTab> transactions of current account
    Then I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
      | chevron                     |
    And I should not see the green or blue vertical bar
    When I tap on the pending transactions accordion
    Then I should see a chevron on pending transaction lines of iOS and not on android
    And I should scroll down to the end of the transactions to view <output> message of that <monthTab> tab
    Examples:
      | type           | output        | monthTab     |
      | currentAccount | currentEndOf  | allTab       |
      | currentAccount | currentInOuts | currentMonth |

  Scenario: TC_002_View current month transactions & loading of transactions in the current month
    Given I am an enrolled "current and savings account" user logged into the app
    When I select saving account tile from home page
    And I should be on the currentMonth of statements page
    Then I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    And I should scroll down to the end of the transactions to view savingEndOf message of that currentMonth tab

  Scenario Outline: TC_003_Display of next calendar month tab on viewing the current month statements
    Given I am an enrolled "current and savings account" user logged into the app
    When I select <type> account tile from home page
    Then I should not see the next calendar month to the right of the current month tab
    Examples:
      | type    |
      | current |
      | saving  |

  @secondary
  Scenario Outline: TC_004_View loading of transactions for previous months
    Given I am an enrolled "current and savings account" user logged into the app
    When I select <type> account tile from home page
    And I swipe to see previous month transactions on the account statement page
    Then I should be on the same previous month statements page
    And  I should scroll down to see the next set of transactions loaded for <type> account for previous month
    Examples:
      | type    |
      | current |
      | saving  |

  @android
  #STAMOB-581 id changes for iOS
  Scenario Outline: TC_005_Display of months tab in the statements for the current year
    Given I am an enrolled "current and savings account" user logged into the app
    And I select <type> account tile from home page
    And I see previous month tab to the left of the current month tab in statements
    When I navigate to a previous month for current year
    Then I should see the month tab heading in the format <Month>
    And I should see the future months to right of the selected previous month
    Examples:
      | type    |
      | current |
      | saving  |

  # no test data
  @manual
  Scenario Outline: TC_006_Display of previous calendar month tab for an account opened in current month
    Given I am an enrolled "statement_valid" user logged into the app
    When I select <type> account tile from home page
    Then I should not see the previous calendar month to the left of the month tab
    Examples:
      | type    |
      | current |
      | saving  |

  @manual @stubOnly
  Scenario Outline: TC_007_Display of months tab in the statements for previous year
    Given I am an enrolled "statement_valid" user logged into the app
    When I select <type> account tile from home page
    And I navigate to a month for a previous year
    Then I should see the month tab heading in the format <Year><Month>
    And I should see the future months to right of the selected previous month
    Examples:
      | type    |
      | current |
      | saving  |

  # More execution time (84 swipes)
  @manual
  Scenario Outline: TC_008_Message displayed to view transactions older than 7 years
    Given I am an enrolled "statement_valid" user logged into the app
    When I navigate to <type> account on home page
    And I navigate to a month more than 7 years from the current month
    Then I should not see the previous calendar month to the left of the month tab
    And I should see online statements only go back seven years message in the month tab
    Examples:
      | type    |
      | current |
      | saving  |

  @stubOnly
  Scenario Outline: TC_009_No Transactions message in the current month
    Given I am an enrolled "statement_valid" user logged into the app
    When I select <type> account tile from home page
    Then I should see <output> no transactions message in <month> tab of that account
    Examples:
      | type                             | month   | output          |
      | currentAccountWithNoTransactions | allTab  | noCurrentInOuts |
      | youngSaver                       | current | saving          |

  @secondary @stubOnly
  Scenario: TC_010_No Transaction message for the previous month
    Given I am an enrolled "statement_valid" user logged into the app
    And I select youngSaver account tile from home page
    When I navigate to a previous month for current year
    Then I should be on the same previous month statements page
    And I should see saving no transactions message in the month tab

  @primary
  Scenario Outline: TC_011_Display previous month end of transactions
    Given I am an enrolled "current and savings account" user logged into the app
    When I select <type> account tile from home page
    And  I navigate to a previous month for current year
    Then I should be on the same previous month statements page
    And I should scroll down to the end of the transactions to view <output> message
    Examples:
      | type    | output        |
      | saving  | savingEndOf   |
      | current | currentInOuts |

  Scenario Outline: TC_012_Navigate to statements from view transactions menu option in contextual menu on home page
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to <type> account on home page
    And I select the "view transaction" option in the action menu of <type> account
    Then I should be on the <monthTab> of statements page
    Examples:
      | type    | monthTab     |
      | current | allTab       |
      | saving  | currentMonth |

  Scenario Outline: TC_013_No View transactions options in contextual menu in statements page
    Given I am an enrolled "current and savings account" user logged into the app
    When I select <type> account tile from home page
    Then I should be on the <monthTab> of statements page
    When I select the action menu from statements page
    Then I should not see view transaction option
    Examples:
      | type    | monthTab     |
      | current | allTab       |
      | saving  | currentMonth |

  #MOB3-5565,7325
  @secondary @stubOnly
  Scenario Outline: TC_014_View transactions for all account types
    Given I am an enrolled "statement_valid" user logged into the app
    When I select <type> account tile from home page
    Then I should be on the <typeAccount> of statements page
    Examples:
      | type                                  | typeAccount  |
      | current                               | currentMonth |
      | saving                                | currentMonth |
      | creditCardAccountWithViewTransactions | recent       |
      | personalLoan                          | currentYear  |

  @stubOnly
  Scenario Outline: TC_015_View transactions for all account types thru action menu
    Given I am an enrolled "statement_valid" user logged into the app
    And I navigate to <type> account on home page
    When I select the view transaction option from the action menu of <type> account
    Then I should be on the <typeAccount> of statements page
    Examples:
      | type                                       | typeAccount  |
      | current Account With View Transactions     | currentMonth |
      | saving                                     | currentMonth |
      | credit Card Account With View Transactions | recent       |
      | personal Loan                              | currentYear  |

  #MOB3-13435,13434
  @primary
  Scenario: TC_016_Fixed Bond account tile entry (HC024)
    Given I am an enrolled "fixed_Bond" user logged into the app
    When I select fixedTermDeposit account tile from home page
    Then I should be displayed a message on fixed bond account statement page

  Scenario: TC_017_Fixed Bond actions menu entry (HC024)
    Given I am an enrolled "fixed_Bond" user logged into the app
    When I navigate to fixedTermDeposit account on home page
    And I select the "view transaction" option in the action menu of fixedTermDeposit account
    Then I should be displayed a message on fixed bond account statement page

  @android
  Scenario: TC_018_Accounts should not be swipable in statements view
    Given I am an enrolled "current and savings account" user logged into the app
    When I select current account tile from home page
    Then I should not be able to swipe between accounts in statements view
    And bottom navigation bar is displayed
