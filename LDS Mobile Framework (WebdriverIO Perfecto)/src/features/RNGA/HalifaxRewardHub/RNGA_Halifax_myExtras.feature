@team-mco @rnga @nga3 @sw-reward-hub-entry-ios-on @sw-reward-hub-entry-android-on @sw-spending-reward-reward-hub-on
@sw-rewards-extra-reward-hub-on @sw-savers-prizedraw-reward-hub-on @sw-mortgage-prizedraw-reward-hub-on @android @hfx @pending
@primary
Feature: Halifax Reward_Hub Functionality
  In order to see all rewards in one place
  As a RNGA Halifax customer
  I want to access rewards hub functionality in RNGA app

  #TODO-COMOB-325

  Scenario Outline: TC_001 Main Success Scenario: User Enabled all rewards
    Given I am an enrolled "reward_hub" user logged into the app
    When I am shown the reward hub entry point tile on home page
    And I select Find out how button on reward hub entry point tile
    Then I should be on the halifax reward hub page
    And I should see <reward> tile with <state> status
    When I select call to action link from <reward> tile
    Then I should be on <reward> web journey
    When I select back button from cwa page
    Then I should be on the halifax reward hub page

    Examples:
      |reward            |state          |
      |cashbackExtra     |optIn          |
      |rewardsExtra      |optInOnTrack   |
      |mortgagePrizeDraw |registered     |
      |saversPrizeDraw   |registered     |

  Scenario Outline: TC_002 Main Success Scenario: User not registered/optedIn for cashback and mortgage prize draw reward
    Given I am an enrolled "reward_hub" user logged into the app
    When I am shown the reward hub entry point tile on home page
    And I select Find out how button on reward hub entry point tile
    Then I should be on the halifax reward hub page
    And I should see <reward> tile with <state> status
    When I select call to action link from <reward> tile
    Then I should be on <reward> web journey
    When I select back button from cwa page
    Then I should be on the halifax reward hub page

    Examples:
      |reward            |state          |
      |cashbackExtra     |notOptIn       |
      |rewardsExtra      |optInNotOnTrack|
      |mortgagePrizeDraw |notRegistered  |
      |saversPrizeDraw   |registered     |

  Scenario Outline: TC_003 Main Success Scenario: Verify user with NO PCA accounts
    Given I am an enrolled "reward_hub" user logged into the app
    When I am shown the reward hub entry point tile on home page
    And I select Find out how button on reward hub entry point tile
    Then I should be on the halifax reward hub page
    And I should see <reward> tile with <state> status
    When I select call to action link from <reward> tile
    Then I should be on <reward> web journey
    When I select back button from cwa page
    Then I should be on the halifax reward hub page

    Examples:
      |reward            |state          |
      |cashbackExtra     |notOptIn       |
      |rewardsExtra      |notOptIn       |
      |mortgagePrizeDraw |registered     |
      |saversPrizeDraw   |notRegistered  |

  Scenario Outline: TC_004 Main Success Scenario: Verify eligible user not optedIn/registered to any rewards
    Given I am an enrolled "reward_hub" user logged into the app
    When I am shown the reward hub entry point tile on home page
    And I select Find out how button on reward hub entry point tile
    Then I should be on the halifax reward hub page
    And I should see <reward> tile with <state> status
    When I select call to action link from <reward> tile
    Then I should be on <reward> web journey
    When I select back button from cwa page
    Then I should be on the halifax reward hub page

    Examples:
      |reward            |state          |
      |cashbackExtra     |notEligible    |
      |rewardsExtra      |notEligible    |
      |mortgagePrizeDraw |notRegistered  |
      |saversPrizeDraw   |notRegistered  |

  Scenario Outline: TC_005 Main Success Scenario: User optedIn today for rewards EXTRA
    Given I am an enrolled "reward_hub" user logged into the app
    When I am shown the reward hub entry point tile on home page
    And I select Find out how button on reward hub entry point tile
    Then I should be on the halifax reward hub page
    And I should see <reward> tile with <state> status
    When I select call to action link from <reward> tile
    Then I should be on <reward> web journey
    When I select back button from cwa page
    Then I should be on the halifax reward hub page

    Examples:
      |reward            |state          |
      |cashbackExtra     |notOptIn       |
      |rewardsExtra      |optInToday     |
      |mortgagePrizeDraw |notRegistered  |
      |saversPrizeDraw   |notRegistered  |
