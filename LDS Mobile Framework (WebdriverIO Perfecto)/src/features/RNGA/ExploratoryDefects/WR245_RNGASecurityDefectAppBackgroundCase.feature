@team-mpt @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @secondary @environmentOnly
Feature: E2E_verify RNGA Background and foregrounding the app on triggering an API call

  @genericAccount
  Scenario Outline: TC_01 verify Add Beneficiary page on backgrounding and foregrounding the app on triggering an API call
    Given I am a current user on the home page
    When I navigate to add new recipient option using payment hub
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    Then I select continue button on add payee page
    When I select continue on UK review details page
    Then I should be prompted to enter my password
    When I enter a correct password
    And I put the app in the background
    And I launch the app from the background
    Then I am on the Make Payment screen with newly added beneficiary selected

    Examples:
      | sortCode | accountNumber | recipientName |
      | 110300   | 12709366      | SO08          |

  @genericAccount
  Scenario: TC_02 verify payment page on backgrounding and foregrounding the app on triggering an API call
    Given I am a isa user on the home page
    And I am on the payment hub page
    And I submit the transfer with all the mandatory fields with topUpIsa from account and to account with amount value 1
    When I select ISA transfer agreement checkbox and confirm option in the review transfer page
    And I put the app in the background
    And I launch the app from the background
    Then I should see view transaction and make another transfer option in the transfer success page

  @otherAccount
  Scenario: TC_03 verify home page on background and foregrounding the app in opt for fingerprint and transactions page
    Given I have launched unenrolled application
    And I am a current user
    When I click on Login button
    And I login with my username and password
    And I enter correct MI
    And I choose call me now option
    And I capture and enter the 4 digit pin for eia call
    Then I am shown Enrolment success screen
    And I select continue button on the enrolment confirmation page
    And I select confirm button in auto logout settings page
    And I opt out for fingerprint from the interstitial
    And I put the app in the background
    And I launch the app from the background
    And I should be on the home page
    When I select current account tile from home page
    And I put the app in the background
    And I launch the app from the background
    Then I view allTab transactions of current account
