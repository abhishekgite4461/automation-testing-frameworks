@team-mco @rnga @nga3 @instrument @ff-webview-debug-on @environmentOnly @hfx @pending

Feature: Validate the overdraft journey for adding, amending and removing overdraft to an existing current account for Halifax brand

  Background:
    Given I am a current user on the home page

  Scenario Outline: TC_001_Verify successful Add Reward Extras for Halifax URCA account

    When I select the "Add your Reward Extras" option in the action menu of current account
    And I verify details on add reward extra landing page
    And I select proposition <proposition> on add reward extra landing page
    And I select choose how to earn for an offer on add reward extra landing page
    Then I should be on the Reward Category page
    When I select category <category> on add reward extra category page
    Then I should be on the lets get started page
    When I select the continue button on lets get started page
    Then I should be on the Your Email page
    And I select Yes button for email address confirmation on Your Email page
    When I select the continue button on Your Email page
    And I should be on the Legal Bits page
    And I select the I confirm checkbox in the legal bits add offer page

    Examples:
      | proposition          | category |
      | propRewardFilmRental | Save     |
      | CashBack             | Spend    |

  Scenario: TC_002_Verify successful Track Reward Extras for Halifax RCA account

    When I select the "Track your Reward Extras" option in the action menu of current account
    Then I verify details on track reward extra landing page
    And I verify Engage Qualifying Status message
    And I verify Engage Last Updated date
    When I select Show my Offer Progress on reward tracker page
    Then I verify Engage Qualification Description

  Scenario: TC_003_Verify Message shown for already Opted-In Halifax account

    When I select the "Add your Reward Extras" option in the action menu of current account
    Then I verify Already Have Reward Extras Title is displayed
    And I verify Already Have Reward Extras message is displayed
