@team-login @mbnaSuite @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @manual @optFeature @primary

Feature: As an RNGA User I should be able to login via fingerprint and
  I want to be informed of any fingerprint change in my device
  so that I can decide to login into APP using biometric prompt or not

#  Precondition:
#  Device is bio-metric compatible
#  NGA Retail/Business - Device Enrollment switch is ON
#  NGA Retail/Business - Light Logon switch is ON
#  Bio-metric switch is ON
#  ff isBiometricOptInRepromptEnabled is ON

  Scenario:TC_01 User clicks on No button on Before we confirm popup, during enrollment journey
    Given I am a current user
    When I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I choose call me now option
    And I capture and enter the 4 digit pin for eia call
    And I select continue button on the enrolment confirmation page
    And I choose my security auto logout settings
    And I click 'Turn ON Bio' button on biometric screen
    And I see before we confirm popup with yes and no buttons
    And I click No button
    Then I should see Bio not turned on popup
    And I should get re-prompted after n number of days
    When I click on Ask me later button
    Then I should be on the home page

  Scenario:TC_02 User clicks on No button on Before we confirm popup, upon logon
    Given I am an enrolled "current" user on the MI page
    When I enter correct MI
    And I see biometric screen
    And I click 'Turn ON Bio' button on bio-metric screen
    And I see 'Before we confirm' popup with yes and no buttons
    And I click No button
    Then I should see 'Bio not turned on' popup
    And I should get re-prompted after n number of days
    When I click on Ask me later button
    Then I should be on the home page

  Scenario:TC_03 User clicks on No button on Before we confirm popup, in Settings(Security) journey
    Given I am a current user on the home page
    When I navigate to settings page via more menu
    And I select security settings from settings menu
    And I toggle 'Sign in with bio' button to ON
    And I see 'Before we confirm' popup with Yes and No buttons
    And I click on No button
    Then I should see Bio not turned On popup
    And I should get re-prompt only if re-prompt state was stored while enrollment/logon
