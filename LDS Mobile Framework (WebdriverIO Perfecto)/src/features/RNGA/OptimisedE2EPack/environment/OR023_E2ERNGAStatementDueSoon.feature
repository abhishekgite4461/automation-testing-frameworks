@team-statements @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-enrolled-valid @sw-upcoming-payments-on @genericAccount
Feature: E2E Due Soon Payments
  As a NGA Retail customer,
  I want to view my pending payment transactions,
  So that I am better informed about my transactions

  @environmentOnly
  Scenario: TC_01 View upcoming payments option in contextual menu on statements page
    Given I am a current user on the home page
    When I select current account tile from home page
    Then I should not see payments due soon option on the action menu
    When I select the viewUpcomingPayment option from the action menu in statements page
    Then I should be displayed upcomingPayments web view page
    And I navigate to logoff via more menu
    And I should be on the logout page

  Scenario: TC_002_View payments due soon options in contextual menu on statements page and validate due soon fields
    Given I am a current user on the home page
    And I select saving account tile from home page
    When I select the viewPaymentsDueSoon option from the action menu in statements page
    Then I should see dueSoon tab in statements screen
    And I should validate fields in due soon screen
      | fieldsToValidate            |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    And I should not see payments due soon option on the action menu
    When I select one of the pending payment transaction
    Then I should see upcoming transaction details for that transaction with the following fields
      | fieldsInDetailScreen  |
      | dueSoonLozenge        |
      | datePendingPayment    |
      | vtdCloseButton        |
      | vtdAmount             |
      | outgoingPaymentText   |
      | payeeField            |
      | sortCodeAccountNumber |
      | referenceField        |
      | changeOrDeleteText    |
    When I close the pending transactions VTD
    Then I should see dueSoon tab in statements screen
    And I navigate to logoff via more menu
    And I should be on the logout page
