@team-login @mbnaSuite @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-enrolled-typical @genericAccount
Feature: Verify Settings menu entry points
  In order to edit my app settings
  As a RNGA authenticated customer
  I want to view my app settings page

  # Note : For MBNA a "current" user will return a "credit card" user

  Background:
    Given I am an enrolled "current" user logged into the app
    And I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page

  #MOB3-3080 #MOB3-2076
  Scenario Outline: TC_01 To verify if the user is getting logged off from the app due to inactivity as per the selected autolog off time settings
    When I select the desired "<autoLogOffTime>" from the auto log off page
    Then I should see a log off alert 15 seconds before the "<autoLogOffTime>" auto logoff time
    And I must be logged out of the app after the selected auto logoff time
    Examples:
      | autoLogOffTime |
      | 1 Minute       |

  #MOB3-3080 #MOB3-2076
  Scenario Outline: TC_02 To verify if the user is getting logged off from the app after selecting log off option from the log off alert pop up
    When I select the desired "<autoLogOffTime>" from the auto log off page
    Then I should see a log off alert 15 seconds before the "<autoLogOffTime>" auto logoff time
    When I select the logoff option from the logoff alert pop up
    Then I should be on the logout page
    Examples:
      | autoLogOffTime |
      | 2 Minute       |

  #MOB3-3080 #MOB3-2076
  Scenario Outline: TC_03 To verify if the user is not getting logged off from the app after selecting continue option from the log off alert pop up
    When I select the desired "<autoLogOffTime>" from the auto log off page
    Then I should see a log off alert 15 seconds before the "<autoLogOffTime>" auto logoff time
    When I select the continue option from the logoff alert pop up
    Then I should be on the security settings page after the log out time selected
    Examples:
      | autoLogOffTime |
      | 1 Minute       |

  #MOB3-3080 #MOB3-2076

  Scenario Outline: TC_04 To verify if the user is getting logged off from the app immediately during app exit after selecting immediate log out option
    When I select the desired "<autoLogOffTime>" from the auto log off page
    And I put the app in the background
    And I launch the app from the background
    Then I should be on the logout page
    Examples:
      | autoLogOffTime  |
      | logoffImmediate |
