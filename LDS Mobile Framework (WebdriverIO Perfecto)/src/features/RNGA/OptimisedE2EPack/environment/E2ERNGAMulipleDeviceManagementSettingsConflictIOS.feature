@team-navigation @nga3 @rnga @stub-enrolled-typical @optFeature @ff-mdm-push-notifications-setting-enabled-on
@ff-push-notifications-settings-off @sw-push-optin-b-v2-on @appiumOnly @genericAccount @ios

# MCC-598, MCC-354
Feature: Apple Push Notifications
  In order to receive push on the devices of my choosing
  As an RNGA customer
  I want to manage which of my devices are enabled for push

  Preconditions
  Customer is fully enrolled
  Opt-in Version A Control-iOS ver2.0 is ON

  Scenario: TC_001 iOS Customer’s current device is not push enabled- Happy path
    Given I am an enrolled "current" user logged into the app
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the notifications page
    When I select to enable notifications
    And I allow notifications for my device
    Then I should see the alert toggle in the correct position according to my current status
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I have turned off device push
    And I relaunch the app and login as a "current" user
    Then I navigate to settings page from home page
    When I select the real time alerts option from the Settings page
    Then I should be on the notifications page
    When I can see an option to enable notifications
    And I select Go to Device Settings Button
    Then I should be taken to the OS device settings
    When I turn on push at device settings level
    And I navigate back to the app
    Then I should be on the notifications page
    And I should now be able to interact with the alert toggles
    And The alert toggles should be shown in the correct position according to my current status
    When I navigate to logoff via more menu
    Then I should be on the logout page

  Scenario: TC_002 iOS Customer’s current device is not push enabled- Doesn’t correctly enable push
    When I have turned off device push
    And I relaunch the app and login as a "current" user
    Then I navigate to settings page from home page
    When I select the real time alerts option from the Settings page
    Then I should be on the notifications page
    When I can see an option to enable notifications
    And I select Go to Device Settings Button
    Then I should be taken to the OS device settings
    When I do not turn on push at device settings level
    And I navigate back to the app
    Then I should see overlay advising that I did not complete the correct actions
    And I should not be able to interact with the alert toggles
    When I should see instructions on how to enable notifications
    Then The alert toggles should be shown in the correct position according to my current status
