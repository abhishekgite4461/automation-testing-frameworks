@team-aov @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @mbna @stub-enrolled-valid
Feature: E2E_Verfiy Home page , account tiles for credit card user in MBNA and their action menu

  #MQE-605: Card management switch is disabled in PUT12 for MBNA to incorporate Production behaviour
  #TODO : Need to remove this scenario when switch is ON in Production and add @mbnasuite to TC_05
  @genericAccount
  Scenario: TC_01 Verify Home page , account tiles and their action menu
    Given I am a current user on the home page
    And bottom navigation bar is displayed
    Then I should see the below options in the bottom nav
      |optionsInBottomNav|
      | home             |
      | apply            |
      | payAndTransfer   |
      | support          |
      | more             |
    When I select home tab from bottom navigation bar
    And I navigate and validate the options in the creditCard account in below table in the home page
      | accountFields            |
      | accountTileLogo          |
      | creditCardAccountNumber  |
      | creditCardAccountBalance |
      | progressAvailableCredit  |
    And I select the action menu of creditCard account
    Then I should see the below options in the action menu of the creditCard account
      | optionsInActionMenu        |
      | viewTransactionsOption     |
      | payCreditCardOption        |
      | balanceMoneyTransferOption |
      | manageCreditLimitOption    |
      | cardManagementOption       |
      | pdfStatementsOption        |
    And I close the action menu in the home page
    When I navigate to settings page via more menu
    And I select the reset app option from the settings page
    Then I should be on the reset app Page
    When I successfully reset the app
    Then I should be on the logout page

  @stub-credit-card-multiple-accounts @ff-credit-card-tile-redesign-enabled-on @genericAccount
  Scenario: TC02_Verify the credit card account details on the account's tile displayed on homepage as per MBNA rapid repair project
    Given I am an enrolled "current" user logged into the app
    And I navigate and validate the options in the creditCard account in below table in the home page
      | accountFields            |
      | creditCardAccountNumber  |
      | creditCardAccountBalance |
      | progressBar              |
      | progressAvailableCredit  |
      | progressCreditLimit      |
      | minimumPayment           |
      | dueDate                  |
      | statementBalance         |

  @ff-offer-tile-and-pay-credit-card-enabled-on @stub-credit-card-single-valid-account @otherAccount
  Scenario: TC03_Verify the Balance or Money transfer tile displayed when a user has with a single credit card acount
    Given I am an enrolled "only_one_credit_card" user logged into the app
    And I navigate and validate that Balance or Money transfer tile is displayed
