@team-payments @nga3 @rnga @sw-direct-debit-nga-on @sw-amend-standing-order-on @stub-enrolled-valid @genericAccount
Feature: Direct Debit Native journey
  In order to view my direct debit
  As a RNGA authenticated customer
  I want to be able to view direct debit within my app

 #PAYMOB-1816,1979
  Scenario: TC_001 User views and delete direct debit by swipe
    Given I am a current user on the home page
    When I navigate to current account on home page
    And I select the "standing order & direct debit" option in the action menu of current account
    Then I am shown all direct debits set up against the account listed with following fields
      | ddFields       |
      | merchantName   |
      | amount         |
      | frequency      |
      | nextDueDateDD  |
    When I select a direct debit
    Then I am shown the VTD for the direct debit with following fields
      | vtdFields    |
      | merchantVTD  |
      | amountVTD    |
      | referenceVTD |
      | frequencyVTD |
      | lastPaid     |
      | nextPaid     |
    And I should see option to delete for a cancellable direct debit
    When I choose the delete option to delete the direct debit
    Then I am displayed with the Delete Direct Debit Info page
    And I am shown the CTA buttons according to the LBG and Non-LBG DD
    When I navigate back to the scheduled payment hub
