@team-navigation @nga3 @rnga @ff-view-pin-enabled-on @stub-viewpin-success @sw-viewpin-ios-on @sw-viewpin-android-on
@sw-viewpin-password-android-on @sw-viewpin-password-ios-on @otherAccount @optFeature @sit02Job @environmentOnly
@mbnaSuite

# can execute only in SIT 02 B env
Feature:View Pin of the linked debit or credit card
  In order to view pin
  As an RNGA customer
  I want to manage debit or credit card linked

  Background:
    Given I am an enrolled "creditCard" user logged into the app
    And I select support tab from bottom navigation bar

#MCC-2101, MCC- 2113
  Scenario: TC_001 Customer successfully views their PIN (password switch is off)
    Then I should see an option to View PIN for my cards

#MCC-2132, MCC-2137 , MCC-2112, MCC-2120
  Scenario: TC_002 Customer successfully views their PIN
    When I select the View PIN option from the Help or Support menu
    Then I choose the View PIN option
    And I successfully enter my password
    And I select to reveal the PIN
    When the PIN number should be displayed for also long as I am holding the reveal PIN button
