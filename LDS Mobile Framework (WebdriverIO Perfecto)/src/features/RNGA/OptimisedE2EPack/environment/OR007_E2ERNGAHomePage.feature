@team-aov @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @sw-direct-debit-nga-on @sw-amend-standing-order-on @optFeature
Feature: E2E_Verfiy Home page , account tiles (current ,savings ,ISA ,HTB ISA ,non-cbs loan ,cbs loan ,mortgage and mbna specific accounts) and their action menu

  # DPL-832, DPL-841, DPL-834, DPL-836, DPL-839, DPL-840, DPL-835, DPL-845, DPL-846, DPL-843, DPL-2220
  # DPL-2855, DPL-1709, DPL-1710, DPL-2050, DPL-1711, DPL-3277, DPL-3568, DPL-3119
  # DPL-945, DPL-947, DPL-1003, DPL-1004
  @environmentOnly @genericAccount
  Scenario: TC_01 Verify Home page , account tiles and their action menu
    Given I am an enrolled "current" user logged into the app
    And bottom navigation bar is displayed
    Then I should see the below options in the bottom nav
      |optionsInBottomNav|
      | home             |
      | apply            |
      | payAndTransfer   |
      | support          |
      | more             |
    And I navigate and validate the options in the current account in below table in the home page
      | accountFields      |
      | accountTileLogo    |
      | accountNumber      |
      | sortCode           |
      | accountBalance     |
    When I select the action menu of current account
    Then I should see the below options in the action menu of the current account
      | optionsInActionMenu        |
      | viewTransactionsOption     |
      | transferAndPaymentOption   |
      | viewUpcomingPayment        |
      | sendMoneyOutsideTheUKOption|
      | chequeDepositOption        |
      | standingOrderAndDirectDebitOption |
      | changeAccountTypeOption    |
      | applyForOverdraftOption    |
      | orderPaperStatementOption  |
      | cardManagementOption       |
      | sendBankDetailsOption      |
    And I close the action menu in the home page
    And I navigate and validate the options in the saving account in below table in the home page
      | accountFields            |
      | accountTileLogo          |
      | accountNumber            |
      | sortCode                 |
      | accountBalance           |
    When I select the action menu of saving account
    Then I should see the below options in the action menu of the saving account
      | optionsInActionMenu           |
      | viewTransactionsOption        |
      | transferAndPaymentOption      |
      | chequeDepositOption           |
      | orderPaperStatementOption     |
      | cardManagementOption          |
      | renewYourSavingsAccountOption |
      #| viewPendingPaymentOption     |
      | viewInterestRateOption        |
    When I select renewYourSavingsAccountOption from contextual menu
    Then I should be displayed the Renew your savings account webview
    When I select home tab from bottom navigation bar
    And I navigate and validate the options in the previousIsa account in below table in the home page
      | accountFields      |
      | accountTileLogo    |
      | accountNumber      |
      | sortCode           |
      | accountBalance     |
    And I select the action menu of previousIsa account
    Then I should see the below options in the action menu of the previousIsa account
      | optionsInActionMenu           |
      | viewTransactionsOption        |
      | transferAndPaymentOption      |
      | chequeDepositOption           |
      | orderPaperStatementOption     |
      | cardManagementOption          |
      | renewYourSavingsAccountOption |
      | viewInterestRateOption        |
    When I select renewYourSavingsAccountOption from contextual menu
    Then I should be displayed the Renew your savings account webview
    When I select home tab from bottom navigation bar
    And I navigate and validate the options in the htbIsa account in below table in the home page
      | accountFields         |
      | accountTileLogo       |
      | accountNumber         |
      | sortCode              |
      | accountBalance        |
    And I select the action menu of htbIsa account
    Then I should see the below options in the action menu of the htbIsa account
      | optionsInActionMenu       |
      | viewTransactionsOption    |
      | transferAndPaymentOption  |
      | orderPaperStatementOption |
      | cardManagementOption      |
      #| viewPendingPaymentOption  |
      | viewInterestRateOption    |
    And I close the action menu in the home page
    When I select home tab from bottom navigation bar
    And I navigate to settings page via more menu
    And I select the reset app option from the settings page
    Then I should be on the reset app Page
    When I successfully reset the app
    Then I should be on the logout page

  # DPL-1009, DPL-2855, DPL-1709, DPL-1710, DPL-2050, DPL-1711, DPL-838, DPL-842, DPL-839
  # DPL-1009
  @environmentOnly @otherAccount
  Scenario: TC_02 Verify Home page , account tiles and their action menu for other accounts
    Given I am an enrolled "non cbs loan and mortgage" user logged into the app
    And I navigate and validate the options in the nonCbsLoan account in below table in the home page
      | accountFields      |
      | accountTileLogo    |
      | loanAccountNumber  |
      | loanCurrentBalance |
    When I select the action menu of nonCbsLoan account
    Then I should see the below options in the action menu of the nonCbsLoan account
      | optionsInActionMenu         |
      | viewTransactionsOption      |
      | loanAdditionalPaymentOption |
      | loanRepaymentHolidayOption  |
      | loanClosureOption           |
      | loanBorrowMoreOption        |
      | loanAnnualStatementsOption  |
    And I close the action menu in the home page
    And I navigate and validate the options in the mortgage account in below table in the home page
      | accountFields          |
      | accountTileLogo        |
      | mortgageAccountNumber  |
      | mortgageAccountBalance |
    When I select the action menu of mortgage account
    Then I should see the below options in the action menu of the mortgage account
      | optionsInActionMenu    |
      | viewTransactionsOption |
    And I close the action menu in the home page

  # MOB3-1370, 2685, 2050
  # CBS loans are only available for LDS brand and not for other brands
  @lds @noExistingData @stub-enrolled-valid
  Scenario: TC_02 Verify the CBS loan account details on the account's tile displayed on homepage
    Given I am an enrolled "cbs personal loan" user logged into the app
    When I navigate and validate the options in the cbsLoan account in below table in the home page
      | accountFields        |
      | accountTileLogo      |
      | cbsLoanAccountNumber |
      | cbsLoanSortCode      |
      | cbsCurrentBalance    |
    Then I should not see action menu of cbsLoan account

  #DPL-1709, DPL-1710, DPL-2050, DPL-1711, DPL-2855
  @otherAccount
  Scenario Outline: TC_03 Verify the NIL/NA/negative current balance and available balance on account tile displayed on homepage
    Given I am a nil negative na user on the home page
    When I navigate to <account> account on home page
    Then I validate the <account> account balance as <accountField> in the home page
    Examples:
      |account   |accountField   |
      |saving    |negativeBalance|
      |fallowIsa |nilBalance     |
      |creditCard|NABalance      |

  # MOB3-1187, 2169, DPL-844
  @stub-enrolled-valid @otherAccount
  Scenario Outline: TC_04 Verify the nick name is displayed for the below accounts in the account tile of the homepage
    Given I am an enrolled "nickName" user logged into the app
    When I navigate to the "<accountTile>" account in the home page
    Then I should see the nick name on the "<accountTile>" account tile in the home page
    Examples:
      | accountTile                          |
      | Automation-Current-NickName          |
      | Automation-Savings-NickName          |
      | Automation-ISA-NickName              |
      | Automation-FixedTermDeposit-NickName |

  #TODO: Need to Remove @pending tag once credit card without overdue amount test data is available
  @stub-enrolled-valid @genericAccount
  Scenario: TC_05 Verify Home page , account tiles and their action menu
    Given I am an enrolled "credit Card Without Overdue Amount" user logged into the app
    Then I should see the below options in the bottom nav
      |optionsInBottomNav|
      | home             |
      | apply            |
      | payAndTransfer   |
      | support          |
      | more             |
    When I select home tab from bottom navigation bar
    And I navigate and validate the options in the creditCardWithoutOverdueAmount account in below table in the home page
      | accountFields            |
      | accountTileLogo          |
      | creditCardAccountNumber  |
      | creditCardAccountBalance |
      | availableCredit          |
    And I select the action menu of creditCardWithoutOverdueAmount account
    Then I should see the below options in the action menu of the creditCardWithoutOverdueAmount account
      | optionsInActionMenu        |
      | viewTransactionsOption     |
      | payCreditCardOption        |
      | balanceMoneyTransferOption |
      | manageCreditLimitOption    |
      | cardManagementOption       |
      | pdfStatementsOption        |
    And I close the action menu in the home page
    When I navigate to settings page via more menu
    And I select the reset app option from the settings page
    Then I should be on the reset app Page
    When I successfully reset the app
    Then I should be on the logout page

  #DPL-1709, DPL-1710, DPL-2050, DPL-1711, DPL-1709, DPL-1710
  @stub-enrolled-valid @genericAccount
  Scenario: TC_06 Verify Home page , account tiles and their action menu
    Given I am a creditCard user on the home page
    And bottom navigation bar is displayed
    Then I should see the below options in the bottom nav
      |optionsInBottomNav|
      | home             |
      | apply            |
      | payAndTransfer   |
      | support          |
      | more             |
    When I select home tab from bottom navigation bar
    And I navigate and validate the options in the overDueCreditCard account in below table in the home page
      | accountFields            |
      | accountTileLogo          |
      | creditCardAccountNumber  |
      | creditCardAccountBalance |
      | negativeAvailableCredit   |
      | overdueAmount             |
    And I select the action menu of overDueCreditCard account
    Then I should see the below options in the action menu of the overDueCreditCard account
      | optionsInActionMenu        |
      | viewTransactionsOption     |
      | payCreditCardOption        |
      | cardManagementOption       |
      | pdfStatementsOption        |
    And I close the action menu in the home page
    When I navigate to settings page via more menu
    And I select the reset app option from the settings page
    Then I should be on the reset app Page
    When I successfully reset the app
    Then I should be on the logout page

  # DPLDC-1135
  @stub-enrolled-valid @genericAccount @android
  Scenario: TC_08 Verify device rotation on Home page
    Given I am an enrolled "current" user logged into the app
    When I rotate the Tab to portrait orientation
    And I rotate the Tab to landscape orientation
    And I rotate the Tab to portrait orientation
    Then I should be on the home page
    When I put the app in the background
    And I launch the app from the background
    Then I should be on the home page

  #The Quicklinks feature sits behind a IBC switch which was implemented in 20.06 and only turned ON for BOS brand
  @stub-enrolled-valid @ff-quick-links-redesign-for-pca-tiles-enabled-on @genericAccount @bos
  Scenario: TC_09_Verify the current account displays quicklinks on homepage
    Given I am an enrolled "current" user logged into the app
    And I navigate and validate the quickLinks in the current account in below table in the home page
      | accountFields      |
      | upcomingPayments   |
      | payAndTransfer     |
      | manageCard         |
      | more               |
    When I select the More quicklink of current account
    Then I should see the below options in the action menu of the current account
      | optionsInActionMenu               |
      | viewTransactionsOption            |
      | transferAndPaymentOption          |
      | viewUpcomingPayment               |
      | sendMoneyOutsideTheUKOption       |
      | chequeDepositOption               |
      | standingOrderAndDirectDebitOption |
      | changeAccountTypeOption           |
      | applyForOverdraftOption           |
      | orderPaperStatementOption         |
      | cardManagementOption              |
      | sendBankDetailsOption             |

  #Following scenario is only iOS tagged because Perfecto is giving error when visitng a quicklink on Android devices.
  @stub-enrolled-valid @ff-quick-links-redesign-for-pca-tiles-enabled-on @genericAccount @bos @ios
  Scenario: TC_10_Verify the current account quicklinks are validated
    Given I am an enrolled "current" user logged into the app
    And I navigate and validate the quickLinks in the current account in below table in the home page
      | accountFields      |
      | upcomingPayments   |
      | payAndTransfer     |
      | manageCard         |
      | more               |
    When I select the Upcoming payments quicklink of current account
    Then I should be displayed upcoming payments page
    And I tap on back button
    When I select the Pay & Transfer quicklink of current account
    Then I should be displayed Pay & Transfer page
    And I tap on back button
    When I select the Manage Card quicklink of current account
    Then I should be on card management page

  #AVC-4383
  @stub-enrolled-valid @ff-rename-accounts-enabled-on @otherAccount
  Scenario Outline: TC_038_Rename a current account
    Given I am an enrolled "rename_savings_account" user logged into the app
    And I select the "rename account" option in the action menu of <accountType> account
    Then I should be on Rename account page
    And I should see <accountType> account name in text field
    And the Rename button is disabled
    When I enter new account name <newAccountName> in the text field
    And I select Rename button
    Then confirmation box is displayed
    When I select Back to your accounts button on confirmation box
    Then I should be on the home page
    And new account name <newAccountName> is displayed
    And I reset the account name from <newAccountName> to its original name of <accountType>
    Examples:
      | accountType | newAccountName    |
      | saving      | Savings Account 2 |

  #The reward hub entry point feature is only turned ON for HFX brand on Android platform.
  @stub-enrolled-valid @ff-enable-reward-hub-entry-point-for-halifax-on @genericAccount @hfx @android
  Scenario: TC_11_Verify the halifax rewards hub entry point tile is displays and clickable on homepage
    Given I am a current user on the home page
    When I am shown the reward hub entry point tile on home page
    And I select Find out how button on reward hub entry point tile
    Then I should be on the halifax reward hub page
