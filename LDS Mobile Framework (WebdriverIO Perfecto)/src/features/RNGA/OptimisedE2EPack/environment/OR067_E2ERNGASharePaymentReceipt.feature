@team-payments @nga3 @rnga @stub-enrolled-valid @ff-share-payment-receipt-on @optFeature
Feature: Share Payment Receipt
  As a RNGA authenticated user
  I want to share the payment success receipt using the compatible apps on my device

  @genericAccount
  Scenario: TC_01 Able to share the Payment Receipt using any of the app via native share panel (Ex:Mail)
    Given I am a current user on the home page
    And I am on the payment hub page
    And I choose the from account as current and to account as uKBank in the payment hub page
    And I enter 1.00 in the amount field in the payment hub page
    When I select Continue button on payment hub page
    And I select the confirm option in the review payment page
    Then I am shown Payment Successful page
    When I select the share option on the payment success page
    And I am on the Share Preview screen
    Then I should see the following details populated on the share preview screen
      | detail                    |
      | bankIcon                  |
      | payerName                 |
      | beneficiaryAccountNumber  |
      | paymentReceiptDateField   |
    And I should see the first 4 digits of the recipient account number is masked
    When I select Share on the Share Preview screen
    Then I should see the native OS share panel

  # Its manual, since we have to verify the Image and Text from the Other User's end
  @manual
  Scenario: TC_02 Verify the share Receipt from the end of other User
    Given I am a current user on the home page
    And I am on the payment hub page
    And I choose the from account as current and to account as uKBank in the payment hub page
    And I enter 1.00 in the amount field in the payment hub page
    When I select Continue button on payment hub page
    And I select the confirm option in the review payment page
    Then I am shown Payment Successful page
    When I select the share option on the payment success page
    And I select Share on the Share Preview screen
    Then I should see the native OS share panel
    When I select the gmail app on the share panel
    Then I should be able to share the Payment receipt to the concerned person along with the pre-defined text
    And the receiver should get both the text and image of the payment
