@team-navigation @nga3 @rnga @ff-inapp-search-on @sw-inapp-search-android-on @stub-enrolled-typical
@sw-inapp-search-ios-on @environmentOnly @ios @genericAccount @optFeature @mbnaSuite

Feature: In App Search Functionality
  In order to navigate to the different features of the app
  As an RNGA customer
  I want to see all valid eligible search results

  #AVC-4921
  Scenario Outline: TC_01 Main Success Scenario :Search for reset mobile banking app
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the reset mobile banking app link as search result
    When I tap on the reset mobile banking app link
    Then I should be on reset Mobile Banking App page as per the searched keyword
    Examples:
      | keyword   |
      | reset     |
      | logoff    |
      | register  |
      | mobile    |

  Scenario:TC_02 Search for GForms -  Loan repayment holiday
    Given I am an enrolled "current" user logged into the app
    When I tap on the In-App search box
    Then I should see below search results populated for loan repayment
      | fields                    |
      | Loan                      |
      | relief                    |
      | repayment                 |
      | covid-19                  |
      | salary                    |

  Scenario Outline: TC_03 Main Success Scenario :Search for change your address
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the change Your Address link as search result
    When I tap on the change Your Address link
    Then I should be on change Your Address page as per the searched keyword
    Examples:
      | keyword     |
      | post		|
      | code		|
      | letter		|
      | house		|
      | changing	|
      | details		|

  Scenario Outline: TC_04 Main Success Scenario :Search for making a payment
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the make Payment link as search result
    When I tap on the make Payment link
    Then I should be on pay And Transfer page as per the searched keyword
    Examples:
      | keyword       |
      | give          |
      | send          |
      | transfer      |
      | transfers     |
      | bill          |

  Scenario Outline: TC_05 Main Success Scenario :Search for managing your personal details
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the manage Your Personal Details link as search result
    When I tap on the manage Your Personal Details link
    Then I should be on manage Your Personal Details page as per the searched keyword
    Examples:
      | keyword         |
      | phone		    |
      | address		    |
      | adress		    |
      | info		    |
      | information		|
      | detail		    |
      | email		    |
      | contact		    |
      | name		    |

  #AVC-5040, AVC-5427
  Scenario Outline: TC_06 Main Success Scenario :No search result found page
    Given I am a CURRENT user on the home page
    When I tap on the In-App search box
    Then I should be able to see the pre defined suggested search results
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the No Search Result found page
    When I tap on the help and support button
    Then I should land into Support Hub Page
    Examples:
      | keyword     |
      | ASDAXYZ		|
