#Remove pending tag once DPL-3866 is resolved
@team-pi @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @sw-cma-nga-off @sw-card-management-on @sw-mobile-chat-ios-off @sw-mobile-chat-android-off @optFeature @mbna @stub-enrolled-typical @genericAccount @pending
Feature: Verify user successfully taken to the support hub page
  As a customer
  I want access to a support hub
  So that I can find the assistance I need And resolve any queries

  Background:
    Given I am a creditCard user on the home page
    When I select support tab from bottom navigation bar

  #MCC-989, #MCC-1014, #MCC-996, #MCC-1013, #MCC-1014, #MCC-994, #MCC-1002, #MCC-1025, #MCC-1003, #MCC-1026, #MCC-989
  Scenario: Verify customer views Support Hub
    Then I should be on the support hub page
    And I should not see message us option
    And I should see call us option
    When I select "lost and stolen card" option in the support hub page
    Then I should be on the lost and stolen page
    And I select support tab from bottom navigation bar
    When I select "reset password" option in the support hub page
    Then I should be on the password reset page
    And I select support tab from bottom navigation bar
    When I select "replacement card and pin" option in the support hub page
    Then I should be on the replacement card and pin page
    And I select support tab from bottom navigation bar
    When I select "change of address" option in the support hub page
    Then I should be on the your personal details page
    When I select support tab from bottom navigation bar
    Then I should not see CMA Tile in the support hub home page
    And I should see "atm and branch finder tile" in the support hub home page
    And I am on the atm and branch finder page
    And I should see find ATM tile option
    And I should not see search branch tile option
    And I am displayed a note message stating clicking search option links take you outside the app
    When I navigate back
    And I should see "app feedback tile" in the support hub home page
