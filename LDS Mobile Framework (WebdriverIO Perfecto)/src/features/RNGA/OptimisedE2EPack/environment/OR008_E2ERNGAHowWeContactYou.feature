@team-login @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @stub-enrolled-typical @optFeature
Feature: Screen to display ways by which bank can contact a customer
  In order to choose how the bank can contact me
  As a Retail App user
  I want to have options to manage how bank can contact me

  @environmentOnly @otherAccount
  Scenario: TC_01 Main Success Scenario: Current & Passbook account Customer selects How we contact you from Settings page
    Given I am an enrolled "nil balance coserviced passbook" user logged into the app
    When I navigate to settings page from home page
    And I select the how we contact you option from the settings page
    Then I should be shown the screen with following options
      | howWeContactYouOptions       |
      | onlinePaperPreference        |
      | statementFreqPreference      |
      | passbookTransactionsUpdates |
      | marketingPreference          |
#      | realTimeAlerts               |
    When I navigate to logoff via more menu
    Then I should be on the logout page

  #MBNA specific scenario
  @mbna @genericAccount
  Scenario: TC_02 Main Success Scenario: MBNA credit card Customer selects How we contact you from Settings page
    Given I am an enrolled "creditCard" user logged into the app
    When I navigate to settings page from home page
    And I select the how we contact you option from the settings page
    Then I should be shown the screen with following options
      | howWeContactYouOptions       |
      | onlinePaperPreference        |
      | marketingPreference          |
    When I navigate to logoff via more menu
    Then I should be on the logout page
