@team-statements @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @genericAccount @stub-enrolled-valid @android @sw-transaction-logo-android-on
Feature: E2E Transaction Logos
  As a NGA Customer
  I should be able to see merchant logos on my transactions
  So that I can recognise transactions more quickly

  Scenario Outline: TC_001_Display default logo or merchant logo
    Given I am a current user on the home page
    When I select current account tile from home page
    Then I should be displayed default logo or merchant logo next to the transaction
    When I select one of the posted transaction
    Then I should be displayed default logo or merchant logo of that transaction in VTD screen
    And I close the VTD page
    When I tap on search icon in all tab
    And I enter the <keyword> in search dialog box
    Then I should be displayed default logo or merchant logo next to the transaction
    When I select one of the posted transaction
    Then I should be displayed default logo or merchant logo of that transaction in VTD screen
    Examples:
      | keyword |
      | 1       |
