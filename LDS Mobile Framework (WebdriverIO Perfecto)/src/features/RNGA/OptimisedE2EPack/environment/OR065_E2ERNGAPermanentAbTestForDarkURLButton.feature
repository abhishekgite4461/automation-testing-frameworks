@team-pi @nga3 @rnga @sw-ab-testing-on @sw-data-consent-on @ff-dark-url-enabled-on @genericAccount @optFeature @environmentOnly
Feature: The dark URL button can be renamed using an ABtest.
  As a team wanting to run an ABTest
  I want to know that the dark URL an be relabelled
  So that I know that the app can be assigned an AB test in Adobe target.

  Background:
    Given I am an enrolled "current" user logged into the app

  Scenario: TC_001 - Dark URL button is renamed to Submit by A/B test
    When I navigate to profile page via more menu
    And I navigate to manage data consent page
    And I select the consentOptedIn button for the marketingPrefs section
    And I select the consentOptedIn button for the performancePrefs section
    And I confirm my selection on the data consent page
    And I navigate to logoff via more menu
    And I navigate back to the login screen
    And I am a current user on the MI page
    And I enter correct MI
    Then I should be on the home page
    And I navigate to settings page via more menu
    When I navigate to the dark URL page
    Then I should see a button labelled Submit to enter the URL
    When I navigate to profile page via more menu
    And I navigate to manage data consent page
    And I select the consentOptedOut button for the marketingPrefs section
    And I select the consentOptedOut button for the performancePrefs section
    And I confirm my selection on the data consent page
    And I navigate to logoff via more menu
    And I navigate back to the login screen
    And I am a current user on the MI page
    And I enter correct MI
    Then I should be on the home page
    And I navigate to settings page via more menu
    When I navigate to the dark URL page
    Then I should see a button labelled Open to enter the URL
