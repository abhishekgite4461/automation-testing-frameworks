@team-payments @rnga @nga3 @sw-enrolment-on @sw-light-logon-on @sw-sca-android-off @sw-sca-ios-off @optFeature @genericAccount @stub-enrolled-valid
#TODO Need to add for End to End Scenario
Feature: In order to move money to them, delete them or view pending payments
  As an RNGA user
  I want to be able to perform multiple actions on an existing recipient

  # MOB3-9153, 1632
  # PayM - unable to add the new beneficiary with uk mobile number
  @stubOnly
  Scenario: TC_01 Manage Recipient Options
    Given I am a current user on the home page
    And I navigate to the remitter account on the payment hub page
    And I select remitting AccountWithExternalBeneficiary Account from the account list
    And I select the recipient account on the payment hub page
    When I select manage on external beneficiary
    Then I can see the Recipients name
    And I have the option to pay the recipient
    And I have the option to delete the recipient
    And the option to cancel manage Recipient

   # MOB3-9546, 1620, 1612, 9620,MQE-57

  Scenario Outline: TC_02 User keeps recipient
    Given I am a current user on the home page
    And I add a new recipient with valid <sortCode>, <accountNumber>, <recipientName>, <reference>
    And I select the recipient account on the payment hub page
    And I am on manage actions for a <type>
    And I select to delete a recipient
    When I select to keep the recipient
    Then I am taken back to the recipient list
    And  the recipient is still available
    And I navigate back to home page from payment hub
    And I navigate to logoff via more menu
    And I should be on the logout page
    Examples:
      | sortCode | accountNumber | recipientName   | reference | type |
      |122253    | 10006969      | Kari Smallridge | test      | adam |
