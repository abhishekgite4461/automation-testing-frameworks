@team-pi @nga3 @rnga @environmentOnly @optFeature @sw-enrolment-on @sw-light-logon-on @genericAccount @sw-mobile-chat-android-on @sw-mobile-chat-ios-on
Feature: Verify user successfully taken to the support hub page
  As a customer
  I want access to a support hub
  So that I can find the assistance I need And resolve any queries

  Background:
    Given I am a current user on the home page
    And I select support tab from bottom navigation bar

  @manual
  # Third party intervention is required for communication(Desktop Chat) hence manual
  #MCC-279, #MCC-280, #MCC-406, #MCC-282, #MCC-427, #MCC-291, #MCC-427, #MCC-287, #MCC-405, #MCC-301, #MCC-342, #MCC-400
  Scenario: TC_01 Customer successfully taken to Chat Window and views below assets
    When I select message us option from support hub home page
    Then I should be on the message us home page
    When I am able to send message to the agent
    Then I should be able to view these chat options
      | chatOptions              |
      | greetMessage             |
      | closeButton              |
      | agentIcon                |
      | dateAtTop                |
      | font                     |
      | timeStamp                |
      | textBubble               |
      | agentIcon                |
      | sendButton               |
      | authenticationMessage    |
      | chatHistory              |
      | agentTypingIndicator     |
      | systemMessages           |
      | unreadMessage            |
      | closedConversation       |
    And the agent sends DeepLink URLs
    When I view the URL on the Message Us screen
    Then I am shown the link as a clickable option
    When I select the URL by tapping on it
    Then I am taken to the page designated within the URL

  @pending
  #TODO run this test when APIE-7334 is fixed
  # CHAT is not working in test environments so no point in running this test till it starts working again
  Scenario: TC_02 Customer is able to type and send message
    When I select message us option from support hub home page
    Then I should be on the message us home page
    When I send my question "ATM" to agent or bot to answer it
    Then I validate chat modal window and close it
    When I navigate to logoff via more menu
    Then I should be on the logout page

  @pending
  # new entry point for chat from c2c screen
  # PIDI-95, PIDI-112
  # FF and Switch should be on to see the new chat entry point in support hub --> call us --> Other banking query screen
  Scenario: TC_03 Customer Displayed Other Banking Queries Chat Entry Point (chat switched ON)
    When I select call us on support hub page
    Then I should be on the call us home page
    When I select the other banking query tile on call us home page
    Then I am displayed the call us otherBankingQuery for the selected option
    And I should see message us option
    When I select the option to Chat
    Then the chat window is launched

  @pending
  # new entry point for chat from c2c screen
  # PIDI-95, PIDI-112
  Scenario: TC_04 Customer Not Displayed Other Banking Queries Chat Entry Point (chat switched OFF)
    When I select call us on support hub page
    Then I should be on the call us home page
    When I select the other banking query tile on call us home page
    Then I am displayed the call us otherBankingQuery for the selected option
    And I should not see message us option
