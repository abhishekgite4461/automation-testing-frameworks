@rnga @nga3 @team-payments @sw-amend-standing-order-on @sw-delete-standing-order-on @sca @sw-biometric-payment-off
@sit02Job
Feature: Native Standing Order SCA - Amend - FeatureFlag ON, Switch ON - Delete - FeatureFlag ON, Switch ON
  In Order to amend standing order using NGA app
  As a Retail NGA user
  I want to be able to amend or delete native standing order using nga app

  @genericAccount @stub-standing-order
  #PAT-850, PAT-954, PAT-879, PAT-878, PAT-893, PAT-851, PAT-882, PAT-847, PAT-883, PAT-837, PAT-873, PAT-838, PAT-881, PAT-965
  Scenario Outline: TC001 - Action Menu: User should see Native Amend Standing order and delete Payee when Standing order already present
    Given I am a current user on the home page
    When I navigate to current account on home page
    And I select the "standing order & direct debit" option in the action menu of current account
    Then I should see the native scheduled payment hub landing screen
    When I select the set up standing order button
    Then I should see the from field pre-populated with current account
    When I select the recipient account on the payment hub page
    And I am on manage actions for a <recipient>
    And I select to delete a recipient
    And I am asked for confirmation if I want to delete the recipient
    And I select to confirm delete a recipient
    And I select view message
    Then I should see the native scheduled payment hub landing screen
    Examples:
      | recipient |
      | uKBank    |

  @genericAccount @stub-enrolled-valid
  #PAT-850, PAT-954, PAT-879, PAT-878, PAT-893, PAT-851, PAT-882, PAT-847, PAT-883, PAT-837, PAT-873, PAT-838, PAT-881, PAT-965
  Scenario: TC002 - Action Menu: User should be able to delete the Standing order by swipe
    Given I am a current user on the home page
    When I navigate to current account on home page
    And I select the "standing order & direct debit" option in the action menu of current account
    Then I should see the native scheduled payment hub landing screen
    When I swipe on the standing order to delete
    And I am displayed a warning with option to proceed or cancel
    And I choose to continue on the warning message
    Then I should see the native scheduled payment hub landing screen

  @genericAccount @stub-enrolled-valid
  #PAT-850, PAT-954, PAT-879, PAT-878, PAT-893, PAT-851, PAT-882, PAT-847, PAT-883, PAT-837, PAT-873, PAT-838, PAT-881, PAT-965
  Scenario: TC003 - Action Menu: User should be able to delete the Standing order from VTD
    Given I am a current user on the home page
    When I navigate to current account on home page
    And I select the "standing order & direct debit" option in the action menu of current account
    Then I should see the native scheduled payment hub landing screen
    And I select a standing order
    And I am shown the VTD for the standing order with following fields
      | vtdFields           |
      | MerchantVTD         |
      | AmountVTD           |
      | accountNoVTD        |
      | referenceVTD        |
      | FrequencyVTD        |
      | NextPaid            |
      | endDate             |
      | standingOrderAmend  |
      | standingOrderDelete |
    And I choose delete option to delete the standing order
    And I choose to continue on the warning message
    And I should see the native scheduled payment hub landing screen

  @stub-enrolled-valid
  #PAT-844, PAT-886
  Scenario: TC005 - Amend Standing Order - Success: User should be able to amend the Standing order with password validation
    Given I am a current user on the home page
    When I navigate to current account on home page
    And I select the "standing order & direct debit" option in the action menu of current account
    Then I should see the native scheduled payment hub landing screen
    And I select a standing order
    And I choose amend option to delete the standing order
    And I should be on the payment hub page
    And I select view payment limits link
    And I am shown following account daily limits for the account in view payments limit page
      | detail                |
      | onlineMobileBanking   |
      | ukPaymentsDailyLimit  |
      | ukPaymentResetMessage |
      | transferYourOwnAccount|
      | telephonyBanking      |
      | ukTelephonyBanking    |
    And I select back buttom from view payment limit page
    When I update the standing order amount field with value as 5
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see a Standing Order set up success message

  @stub-manage-payments-amend-so-no-step-up
  #PAT-844, PAT-886
  # PaymentEndDate iteration is not working properly. will verify and push accordingly
  Scenario Outline: TC006 - Amend Standing Order - Success: User should be able to amend the Standing order without password validation
    Given I am a current user on the home page
    When I navigate to current account on home page
    And I select the "standing order & direct debit" option in the action menu of current account
    Then I should see the native scheduled payment hub landing screen
    And I select a standing order
    And I choose amend option to delete the standing order
    And I should be on the payment hub page
    When I update the standing order <standingOrderField> field with value as <correspondingValue>
    And I review the Standing Order
    Then I should see a Standing Order set up success message

    Examples:
      | standingOrderField | correspondingValue |
      | frequency          | Weekly             |
      | frequency          | Monthly            |
