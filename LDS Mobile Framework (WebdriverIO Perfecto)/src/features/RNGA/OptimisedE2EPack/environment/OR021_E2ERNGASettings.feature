@team-login @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-enrolled-typical @genericAccount
Feature: Verify Settings menu entry points
  In order to edit my app settings
  As a RNGA authenticated customer
  I want to view my app settings page

  Background:
    Given I am an enrolled "current" user logged into the app

  #MOB3-3018,3028,3325,7419,7572 #MCC-31,26- AC01
# ToDo Test Data - MPT-3482, for EverydayOffer option not available, request is being placed
  Scenario: TC_01 To verify static menu on the settings page
    When I navigate to settings page from home page
    Then I should see the below options in the settings page
      | optionsInSettingsPage           |
      | personalDetailsButton           |
#      | everydayOfferButton             |
      | securitySettingsButton          |
      | payAContactButton               |
      | resetAppButton                  |
      | legalInfo                       |
      | howWeContactYou                 |
    When I select security settings from settings menu
    Then I should see the below options in the security settings page
      | securitySettingsOptions               |
      | securityDetailsUserIDLabel            |
      | securityDetailsDeviceTypeLabel        |
      | securityDetailsDeviceNameLabel        |
      | securityDetailsAppVersionLabel        |
      | securityDetailsAutoLogOffButton       |
      | securityDetailsForgottenPasswordButton|
    And I verify the app details in the security settings page
    When I select the autoLog off settings option from the security settings page
    Then I should be on the autoLog off settings page
    When I navigate to logoff via more menu
    Then I should be on the logout page

  # mbna specific scenario : everydayday offer and pay a contact functionality is suppressed in mbna
  @mbna
  Scenario: TC_02 To verify static menu on the settings page
    When I navigate to settings page from home page
    Then I should see the below options in the settings page
      | optionsInSettingsPage           |
      | personalDetailsButton           |
      | securitySettingsButton          |
      | resetAppButton                  |
      | legalInfo                       |
      | howWeContactYou                 |
    When I select security settings from settings menu
    Then I should see the below options in the security settings page
      | securitySettingsOptions               |
      | securityDetailsUserIDLabel            |
      | securityDetailsDeviceTypeLabel        |
      | securityDetailsDeviceNameLabel        |
      | securityDetailsAppVersionLabel        |
      | securityDetailsAutoLogOffButton       |
      | securityDetailsForgottenPasswordButton|
    And I verify the app details in the security settings page
    When I select the autoLog off settings option from the security settings page
    Then I should be on the autoLog off settings page
    When I navigate to logoff via more menu
    Then I should be on the logout page
