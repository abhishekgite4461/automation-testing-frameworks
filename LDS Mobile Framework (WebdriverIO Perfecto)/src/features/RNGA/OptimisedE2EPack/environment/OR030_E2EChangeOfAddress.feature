@team-navigation @nga3 @rnga @sw-coa-native-on @sw-enrolment-on @optFeature @mbnaSuite @sca @sit02Job @genericAccount
Feature: User is able to change their address using Native app

  # Note : For MBNA a "current" user will return a "credit card" user
  # CoA will point to either PUT12 or SIT 02 environment
  # Confirmation screen doesn't appear at other environments and script will fail

  @stub-enrolled-valid
  #PAS-15751, PAS-16150, MCOA-26, MCOA-42, MCOA-30, MCOA-100, MCOA-112, MCOA-101, MCOA-113
  Scenario: TC_01 Main Success Scenario: User is able to change their address successfully
    Given I am a CURRENT user on the home page
    When I am on the address update page
    Then I should see an option to search my new address using postcode
    When I proceed to call customer care
    Then I should be on the secure call page
    When I go back to previous page
    And I enter the correct post code of my new address
    And I perform a postcode search to see the matching addresses
    Then I should see list of addresses for entered postcode
    When I select my new address
    Then I should be asked to confirm my new address
    And I should see my new address on Map
    And I should see my new address selected for confirmation
    And I should see my new address is in title case
    When I confirm my new address
    Then I should see final instructions before updating my address
    When I confirm to update my new address
    Then I should see address change successful message
    And I can proceed to homepage after my address got updated
    When I proceed to homepage after my address got updated
    Then I should be on the home page

  #MCOA-36, MCOA-118
  @stub-coa-no-address-returned
  Scenario: TC_02 Exception Scenario: User should be shown error message when no address is returned for the postcode entered
    Given I am a CURRENT user on the home page
    And I am on the address update page
    And I enter the no address post code of my new address
    And I proceed to call customer care
    Then I should be on the secure call page
    When I go back to previous page
    Then I should see no address post code is same as entered
    When I perform a postcode search to see the matching addresses
    Then I should be displayed a change of address error message
    And I am provided Call Us option
    And I am provided Back To Your Personal Details option

  @stub-enrolled-valid
  Scenario: RNGA Change of Address from Support hub
    Given I am a CURRENT user on the home page
    When I select support tab from bottom navigation bar
    And I select "change of address" option in the support hub page
    Then I should be on the your personal details page
    And I should be shown the list of personal details
      | personalDetail   |
      | Full Name        |
      | User ID          |
      | Mobile Number    |
      | Home Number      |
      | Work Number      |
      | Address          |
      | Postcode         |
      | Email address    |
    When I choose to update address from the personal details
    Then I should be on the address update page
    And I should see an option to search my new address using postcode
    When I proceed to call customer care
    Then I should be on the secure call page
    When I go back to previous page
    And I replace existing with new post code of my new address
    And I perform a postcode search to see the matching addresses
    Then I should see list of addresses for entered postcode
    When I select my new address
    Then I should be asked to confirm my new address
    And I should see my new address on Map
    And I should see my new address selected for confirmation
    And I should see my new address is in title case
    When I confirm my new address
    Then I should see final instructions before updating my address
    When I confirm to update my new address
    Then I should see address change successful message
    And I can proceed to homepage after my address got updated
    When I proceed to homepage after my address got updated
    Then I should be on the home page
