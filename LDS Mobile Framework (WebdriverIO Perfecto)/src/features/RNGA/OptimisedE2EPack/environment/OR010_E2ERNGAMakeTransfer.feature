@team-payments @nga3 @rnga @environmentOnly @sw-enrolment-on @sw-light-logon-on @sw-hcc-android-on @sw-hcc-ios-on @optFeature @ff-pca-alltab-enabled-off

Feature: Verify amount field decimal validation, maximum available balance and perform success transfer journey

  @genericAccount
  Scenario Outline: TC_01 Verify amount field decimal validation, maximum available balance and perform success transfer journey and view transaction
    Given I am a current user on the home page
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    When I enter <amount> in the amount field in the payment hub page
    Then I should see only 2 decimal places in the amount field in the payment hub page
    And I clear the amount field in the payment hub page
    When I enter amount greater than available balance in the remitter account in the payment hub page
    Then I should be able to close the insufficient money in your account error message in the payment hub page
    When I clear the amount field in the payment hub page
    And I submit the transfer with all the mandatory fields with any from account and to account with amount value 10005.00
    And I select the confirm option in the review transfer page
    Then I should see internet transaction limit exceeded error message in payment hub page
    And I should see the values pre-populated in the payment hub page for transfer with amount value 10005.00
    When I clear the amount field in the payment hub page
    And I submit transfer with from account <fromAccount> and to account <toAccount> with amount 1.00
    And I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page
    When I select the view transaction option in the transfer success page
    And I should see the - £1.00 deducted from latest transaction
    And I navigate to home page
    Then I should see the corresponding 1.00 deducted in the remitter <fromAccount> and recipient accounts <toAccount>
    Examples:
      | fromAccount | toAccount | amount   |
      | current     | saving    | 0.019999 |

  # Validating the numeric keyboard is a part of ui testing , hence manual
  @genericAccount @stub-enrolled-valid
  Scenario Outline: TC_02 Displays the platform specific keyboard on Transfer & Payment screen
    Given I am a current user on the home page
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    When I select the amount field in the payment hub page
    And I enter q2.0 in the amount field in the payment hub page
    Then I should not see the values pre-populated in the payment hub page for transfer with amount value q
    Examples:
      |fromAccount|toAccount|
      |current    |saving   |

  #Once DPL-2349 is resolved, will modify the amount back to 250£
  @otherAccount
  Scenario: TC_03 Show an error message MG-1283 when monthly cap limit is set on beneficiary account and transfer limit exceed monthly limit
    Given I am a monthly cap transfer user on the home page
    And I am on the payment hub page
    And I select the to account as monthlyCap in the payment hub page
    And I submit with amount 5 in the payment hub page
    When I select the confirm option in the review transfer page
    Then I should see monthly cap limit exceeded error message in payment hub page
    And I should see the values pre-populated in the payment hub page for transfer with amount value 5
