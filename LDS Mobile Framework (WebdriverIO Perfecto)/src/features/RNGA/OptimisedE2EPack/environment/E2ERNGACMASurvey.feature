@team-pi @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @sw-cma-nga-on @optFeature @stub-enrolled-typical @genericAccount
Feature: Verify user is displayed CMA Survey from support hub page
  In order to compare the service quality across the banking industry
  As a Retail NGA User
  I want to be able to see the CMA Survey results from support hub page

  #PIDI-1267 & PIDI-996 SC02 Customer only sees survey results link on Support hub
  # TODO - PIDI-2184
  # The functionality is getting changed and we are in process of developing in-app browser
  Scenario: TC_001_Customer can view CMA Survey Page from support hub page
    Given I am an enrolled "current" user logged into the app
    When I select support tab from bottom navigation bar
    Then I should see "CMA Tile" in the support hub home page
    # And I scroll down to bottom of support hub page
    # When I select the CMA survey link
    # Then I am displayed the CMA survey page in my mobile browser

  #PIDI-1267 & PIDI-996 SC01 Customer does not see survey results link on call us
  Scenario: TC_002_Customer can not view CMA Survey Page from call us page
    Given I am an enrolled "current" user logged into the app
    When I select support tab from bottom navigation bar
    And I select call us on support hub page
    Then I should not see CMA Tile in the call us page
