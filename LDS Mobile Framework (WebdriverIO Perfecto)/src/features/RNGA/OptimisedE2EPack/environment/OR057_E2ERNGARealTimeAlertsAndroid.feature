@team-navigation @nga3 @rnga @optFeature @android @sw-push-notification-on
@stub-enrolled-valid @appiumOnly @genericAccount

Feature: Verify Real Time Alerts entry point
  In order to edit my app push notification settings
  As a RNGA authenticated customer
  I want to view Real Time Alerts page under Settings menu

  Preconditions for Real Time Alerts entry point
  Customer is fully enrolled
  Customer has never opted in for push notifications
  Data Consent screen is shown
  Enable Push Notification - Android switch is ON
  Opt-in Version A Control-Android is OFF

  #MCC-29,32- AC01
  Scenario: TC_01 To verify smart alert preferences landing page
    Given I am a current user
    When I enter correct MI
    And I navigate to settings page from home page
    Then I select the real time alerts option from the Settings page
    And I should be on the smart alerts page
    When I opt in for Account alerts
    Then I should see a success confirmation
    And I should be opted in for Account alerts
    When I opt out of the Account alerts
    Then I should see a opted out confirmation
    And I should be opted out of Account alerts
