@team-payments @stub-enrolled-valid @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @genericAccount @optFeature
Feature: E2E_Verify RNGA for Standing Order

  # MOB3-10016, 1541
  # Other frequencies are commented after discussion with BA, it may be required if there is change in SO feature
  Scenario Outline: TC_01 Main Success Scenario: Creation of a Successful Standing Order for internal accounts
    Given I am a current user on the home page
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I provide Reference for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    And I enter a correct password
    And I should see a Standing Order set up success message
    Examples:
      | fromAccount | Amount | toAccount | frequency        |
      | current     | 2      | saving    | Weekly           |
#      | current     | 2      | isa       | Every four weeks |
#      | current     | 2      | isa       | Monthly          |
#      | current     | 2      | saving    | Every two months |
#      | current     | 2      | saving    | Quarterly        |
#      | current     | 2      | saving    | Half yearly      |

  # MOB3-10013, 1642
  Scenario Outline: TC_02 Business Rule Validation - User can select payment date up to 31 days from today
    Given I am a current user on the home page
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order
    And I choose to select the Start date for Standing Order
    Then I should be allowed to select a payment date up to 31 days after the current date
    Examples:
      | fromAccount | Amount | toAccount |
      | current     | 2      | saving    |

  # PAYMOB-1173
  Scenario Outline: TC_03 Main Success Scenario: Creation of a Successful Standing Order for ISA accounts
    Given I am a current user on the home page
    And I select <Account> account tile from home page
    And I select the top up isa option from the action menu in statements page
    And I have chosen to make this a Standing Order option and dismiss popup for <Account>
    When I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I provide Reference for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    And I enter a correct password
    And I should see a Standing Order set up success message
    Examples:
      | Amount | Account   | frequency |
      | 2      | fallowIsa | Weekly    |
