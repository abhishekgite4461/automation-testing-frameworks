@team-navigation @rnga @nga3 @stub-enrolled-typical @sw-card-management-on @optFeature @genericAccount
Feature: Validate the more menu option for retail customers
  As a NGA authenticated customer
  I want to view the list of options available in the more menu
  So that I can access the entry points to specific journeys

  @manual
  Scenario: TC_01 Validate the appropriate account holder name in the more menu for joint account
    Given I am an enrolled "current" user logged into the app
    When I select more menu
    Then I should see the name of the account holder appropriately for joint account users

  Scenario: TC_02 Main Success: Customer accesses the more menu
    Given I am an enrolled "current" user logged into the app
    And I select more tab from bottom navigation bar
    And I should see the below options in the more menu
      | optionsInMorePage  |
      | yourProfile        |
      | inbox              |
      | settings           |
      | cardManagement     |
      | depositCheque      |
      | viewDepositHistory |
      | logout             |
    And I navigate to logoff via more menu
    And I should be on the logout page
    And I select the contact us option from the logoff page pre auth header
    Then I should be on pre auth call us home page overlay

  # mbna specific scenario : Cheque deposit functionality is suppressed in mbna
  @mbna
  Scenario: TC_03 Main Success: Customer accesses the more menu
    Given I am an enrolled "creditCard" user logged into the app
    And I select more tab from bottom navigation bar
    And I should see the below options in the more menu
      | optionsInMorePage      |
      | yourProfile            |
      | inbox                  |
      | settings               |
      | cardManagement         |
      | logout                 |
    And I navigate to logoff via more menu
    And I should be on the logout page
    And I select the contact us option from the logoff page pre auth header
    Then I should be on pre auth call us home page overlay
