@team-payments @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @sw-cop-nga-ios-on @sw-cop-nga-and-on @environmentOnly @optFeature @genericAccount @cop
@sw-biometric-payment-off @sit02Job
Feature: Enable Confirmation of Payee
  In order to to pay an external recipient
  As a RNGA authenticated customer
  I want to get the confirmation of payee details to whom I am paying through COP service

  Background:
    Given I am a current user on the home page
    And I navigate to add new recipient option using payment hub
    And I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
    And I choose UK account from account category page

  Scenario: TC_01 Verify the Payment Success journey with COP responses
    When I validate 800647, 18411768 , Hughie
    And I select continue button on add payee page
    Then I should see account number invalid message in a red banner on the payment hub page
    When I dismiss the error message on the payment hub
    And I go back and come to UK beneficiary details page again
    And I validate 800647, 10655465, XYZ Developments
    And I dont choose the Business selection entity
    And I select continue button on add payee page
    Then I should see Please Wait Spinner
    And I should see Checking Details Spinner with relevant bank name
    And I should see Payee matches business account message with the X Y Z DEVELOPMENTS LIMITED playback
    And I see the options to change Details or Continue
    When I select the back button in the COP response page
    And I go back and come to UK beneficiary details page again
    And I validate 800647, 10658869, Hughie
    And I dont choose the Business selection entity
    And I select continue button on add payee page
    Then I should see Please Wait Spinner
    And I should see Checking Details Spinner with relevant bank name
    And I should see Account name not match message
    And I should see the COP share details option
    And I see the options to change Details or Continue
    When I navigate back on New UK beneficiary details page
    And I select continue button on add payee page
    Then I should see Please Wait Spinner
    And I should see Checking Details Spinner with relevant bank name
    And I should see Account name still not match message
    And I should see the COP share details option
    When I continue to payment hub page from COP response page
    And I submit with amount 5.00 in the payment hub page
    And I select the confirm option in the review payment page
    And I enter a correct password
    Then I should see the payment successful message page

  Scenario Outline: TC_02_Payee Name Exact Match
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I choose the Business selection entity
    When I select continue button on add payee page
    Then I should see Please Wait Spinner
    And I should see Checking Details Spinner with relevant bank name
    And I am shown Payee matches message
    Examples: Valid entries
      | sortCode | accountNumber | recipientName      |
      | 800647   | 10655465      | X Y Z Developments |
