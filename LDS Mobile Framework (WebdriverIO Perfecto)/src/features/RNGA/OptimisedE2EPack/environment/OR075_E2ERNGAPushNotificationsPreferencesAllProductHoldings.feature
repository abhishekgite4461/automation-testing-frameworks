@team-navigation @nga3 @rnga @stub-enrolled-typical @optFeature @ff-mdm-push-notifications-setting-enabled-on
@sw-push-notification-on @appiumOnly @sw-push-optin-b-v2-on @exhaustiveData

  # Epic- 2317(2376,2384,2380,2391,2321,2318,2377,2319,2320,2390,2385)
Feature: Push Notifications for all product holdings
  In order to receive push on the devices of my choosing
  As an RNGA customer
  I want to manage which of my devices are enabled for push

  Scenario:TC1- Customer has only a credit card
    Given I am an enrolled "creditCard" user logged into the app
    And I navigate to settings page from home page
    When I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page

  Scenario:TC2 - Customer with accounts that don't have any applicable alerts navigates to the Push Notification settings page
    Given I am an enrolled "CASHISA" user logged into the app
    And I navigate to settings page from home page
    When I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
