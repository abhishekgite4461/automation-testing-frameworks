@team-login @mbnaSuite @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-enrolled-valid @genericAccount
Feature: RNGA user wants to update phone numbers
  In order to keep my phone number up to date
  As a retail RNGA user
  I want option to update my phone number

  # Note : For MBNA a "current" user will return a "credit card" user

  #MOB3-6512,7330
  Scenario: TC_01 Phone Number successful update
    Given I am an enrolled "current" user logged into the app
    And I have submitted request to change mobile, home and work
    When The request has been successfully processed
    Then I should be shown a success screen
    And I should be shown an option to go back to my personal details
    When I navigate to logoff via more menu
    Then I should be on the logout page
