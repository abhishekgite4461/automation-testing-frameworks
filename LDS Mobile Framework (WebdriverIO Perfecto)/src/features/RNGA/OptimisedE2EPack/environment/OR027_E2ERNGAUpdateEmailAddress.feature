@team-login @mbnaSuite @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-enrolled-typical @genericAccount
Feature: RNGA user wants to update email address
  In order to keep my email address up to date
  As a retail RNGA user
  I want an option to update my email address

  # Note : For MBNA a "current" user will return a "credit card" user

  #MOB3-6858,7750
  Scenario: TC_01 The email has been updated successfully
    Given I am an enrolled "current" user logged into the app
    And I have submitted a request to change my email address
    When The request has been successfully processed
    Then I should be shown a success message with prompt to close the message
    When I dismiss the prompt
    Then I should be shown updated personal details screen
    When I navigate to logoff via more menu
    Then I should be on the logout page
