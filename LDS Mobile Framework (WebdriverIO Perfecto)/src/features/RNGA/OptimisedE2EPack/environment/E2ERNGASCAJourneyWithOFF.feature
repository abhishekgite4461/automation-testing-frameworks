@team-payments @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @ff-strong-customer-auth-off @sw-sca-android-off @sw-sca-ios-off @genericAccount
Feature: E2E_Verify RNGA payment hub

  @stub-enrolled-valid
  Scenario Outline: TC_01 Adding New UK External beneficiary, Payment, Dynamic Search and deletion of the Recipient for a current user
    Given I am a current user on the home page
    When I add a new recipient with valid <sortCode>, <accountNumber>, <recipientName>, <reference>
    And I enter payment amount greater than available balance in the remitter account in the payment hub page
    Then I should be able to close the insufficient money in your account error message in the payment hub page
    When I clear the amount field in the payment hub page
    And I submit the payment from account <fromAccount> and to previously added account for amount <amount>
    And I select the confirm option in the review payment page
    Then I should see a payment confirmed screen
    When I navigate back to home page from payment success page
    And I choose the recipient account on the payment hub page
    And I search recipient account <recipientName> on payment hub page to delete
    And I delete the selected recipient
    Then I am asked for confirmation if I want to delete the recipient
    When I select to confirm delete a recipient
    And I navigate back to home page after deleting the recipient
    And I navigate to logoff via more menu
    Then I should be on the logout page
    Examples:
      | sortCode | accountNumber | recipientName | amount | reference | fromAccount    |
      | 400202   | 74374163      | Adam Anderson | 1      | test      | currentPayment |

  @stub-enrolled-typical
  Scenario Outline: TC_02 Dynamic Searching per character added
    Given I am a current user on the home page
    And I add a new recipient with valid <sortCode>, <accountNumber>, <recipientName>, <reference>
    And I select the recipient account on the payment hub page
    Then I verify search results for each search term
      | searchType | searchTerm | results         |
      | recipient  | ili        | no results      |
      | recipient  | Ada        | adam            |
      | recipient  | Ander      | adam            |
    Examples:
      | sortCode | accountNumber | recipientName | reference |
      | 400202   | 74374163      | Adam Anderson | test      |

  @stub-enrolled-valid
  Scenario Outline: TC_03 Adding New UK External beneficiary, Payment, Dynamic Search and deletion of the Recipient for a savings user
    Given I am a current user on the home page
    And I am on the payment hub page
    When I select the from account as saving in the payment hub page
    And I select the recipient account on the payment hub page
    And I choose to Add new payee on search recipient page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    And I select continue on UK review details page
    Then I should be prompted to enter my password
    When I enter a correct password
    And I am on the Make Payment screen with newly added beneficiary selected
    And I submit the payment with amount value <amount> and reference
    And I select the confirm option in the review payment page
    Then I should see a payment confirmed screen
    When I navigate back to home page from payment success page
    And I am on the payment hub page
    And I select the from account as saving in the payment hub page
    And I select the recipient account on the payment hub page
    And I search recipient account <recipientName> on payment hub page to delete
    And I delete the selected recipient
    Then I am asked for confirmation if I want to delete the recipient
    When I select to confirm delete a recipient
    And I navigate back to home page after deleting the recipient
    And I navigate to logoff via more menu
    Then I should be on the logout page
    Examples:
      | sortCode | accountNumber | recipientName | amount |
      | 400202   | 74374163      | TestAutomation| 1.00   |

  @environmentOnly
  Scenario: TC_04 Add New Company Beneficiary account and Make Payment
    Given I am a current user on the home page
    When I navigate to add new recipient option using payment hub
    Then I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
    Then I should see beneficiaryType on beneficiary type page
      | detail                    |
      | ukAccount                 |
      | internationalBankAccount  |
      | ukMobileNumber            |
      | payingUkMobileNumberLink  |
    When I choose UK account from account category page
    And I enter whitelisted company details
    And I select continue button on add payee page
    And list of matching companies with the notes
      | results                                    |
      | paymentHubAddUkAccountSelectionCompanyList |
      | businessBeneficiaryPayeeNotes              |
    And I select a result
    Then I should see UK Company Review Details page
    When the selected Recipient Name, Sort Code, Account Number should be displayed
    And I have the ability to add a reference 5434298992481650, 5434298992481650 and retype the reference
    And I select continue on UK review details page
    Then I should be displayed the password confirmation screen
    When I enter a correct password
    And I am on the Make Payment screen with newly added beneficiary selected
    And I enter 1.00 in the payment amount field in the payment hub page
    Then I should see only 2 decimal places in the payment amount field in the payment hub page
    When I select the continue option in the payment hub page
    And I verify the reference prepopulated in review page
    And I select the confirm option in the review payment page
    Then I am shown Payment Successful page
    And I should see share receipt option on the payment success page

  Scenario Outline: TC_03 Main Success Scenario: Creation of a Successful Standing Order for external accounts
    Given I am a current user on the home page
    When I navigate to add new recipient option using payment hub
    Then I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    Then I should see UK Review Details page
    When I select continue on UK review details page
    Then I should be prompted to enter my password
    When I enter a correct password
    And I am on the Make Payment screen with newly added beneficiary selected
    And I have chosen to make this a Standing Order option
    And I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    And I enter a correct password
    And I should see a Standing Order set up success message
    And I navigate back to home page from payment hub

    Examples:
      | sortCode | accountNumber | recipientName | Amount | frequency        |
      | 110300   | 12709366      | SO02          | 2      | Every four weeks |
#     | 110300   | 12709366      | SO01          | 2      | Weekly           |
