@team-statements @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @genericAccount @stub-enrolled-valid
@environmentOnly @android
# DPLDA-425 Disabling the switch @sw-transaction-logo-android-on due to WAS layer issue. will include once it is sorted
Feature: E2E_Current Account Search Transactions
  # in perfecto currently there is issue with iOS search. Manually working fine. Will remove android tag once this is fixed
  #STAMOB-17|App defect|iOS id issue - can't be resolved for few versions

  #Pre condition: User should have more than 60 transactions
  Scenario Outline: TC_001 User able to search transactions for current account
    Given I am a current user on the home page
    When I navigate to current account to validate accountNumber and select current account
    Then I should be on the allTab of statements page
    And I should see search icon in all tab
    When I tap on search icon in all tab
    Then I should be displayed current account name last four digits of account number in the header and no action menu for that current name
    And I should be displayed search page with below options
      | fieldsInSearchPage |
      | searchInstruction  |
      | searchDialogBox    |
      | backButton         |
    When I enter the <keyword> in search dialog box
    Then I should be displayed matching transactions with that <keyword> in the search list
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
      | chevron                     |
    And I should be displayed search page with below options
      | fieldsInSearchPage            |
      | clearSearch                   |
      | searchResultsCount            |
      | searchResultsTransactionsList |
      | searchFurtherBack             |
    When I tap on search more transactions
    Then I should be displayed further more transactions available with the same <keyword> in the results
    When I select one of the posted transaction
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdContainer            |
      | date                    |
      | vtdCloseButton          |
      | vtdAmount               |
      | transactionTypeField    |
      | transactionDescription  |
      | unsureTransactionButton |
    When I close the posted transactions
    Then I should be on the search page with transactions
    When I clear the search results
    Then The search word <keyword> entered should be cleared
    And The results list should be reset
    When I select to go back
    Then I should be on the allTab of statements page
    Examples:
      | keyword |
      | 1       |

  #STAMOB-1810
  Scenario Outline: TC_002 User able to search transactions for current account and validate app behaviour during device rotation Tablet
    Given I am a current user on the home page
    When I rotate the Tab to portrait orientation
    And I select current account tile from home page
    Then I should be on the allTab of statements page
    And I should see search icon in all tab
    When I tap on search icon in all tab
    Then I should be displayed search page with below options
      | fieldsInSearchPage |
      | searchInstruction  |
      | searchDialogBox    |
      | backButton         |
    When I enter the <keyword> in search dialog box
    And I rotate the Tab to landscape orientation
    And I rotate the Tab to portrait orientation
    And I select to go back
    Then I should be on the allTab of statements page
    Examples:
      | keyword |
      | 1       |
