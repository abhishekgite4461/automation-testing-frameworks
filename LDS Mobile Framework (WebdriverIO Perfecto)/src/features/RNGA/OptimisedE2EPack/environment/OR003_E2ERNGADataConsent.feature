@mbnaSuite @team-mco @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @sw-data-consent-on @optFeature @genericAccount

  # Note : For MBNA a "current" user will return a "credit card" user

Feature: E2E_Verify RNGA Manage Data Consent from my profile
  In order to manage my data consents on the device
  As a customer
  I must be provided with the options to make changes to my consent

  @environmentOnly
  Scenario: TC_01 Verify data consent interstitial and accept all button
    Given I reinstall the app
    And I am a current user on the home page
    When I navigate to logoff via more menu
    And I navigate back to the login screen
    And I am a current user on the MI page
    And I enter correct MI
    And I have not made a selection for my cookie usage already on that device
    And I see a native data consent interstitial page without a back button and global menu on the header
    And I see two options for selection
    And I select the option to accept all consents
    Then I should be on the home page

  @environmentOnly
  Scenario: TC_02 Verify data consent interstitial, manage consent button and customize data consents page
    Given I reinstall the app
    And I am a current user on the home page
    When I navigate to logoff via more menu
    And I navigate back to the login screen
    And I am a current user on the MI page
    And I enter correct MI
    And I have not made a selection for my cookie usage already on that device
    And I see a native data consent interstitial page without a back button and global menu on the header
    And I see two options for selection
    And I select the option to manage consent
    And I should be taken to the my customize consents page
    And I make a new selection for my consents
    And The confirm option is then enabled
    And I confirm my selection on the data consent page
    Then I should be on the home page

  @stub-enrolled-typical
  Scenario: TC_03 Set data consent from manage data consent page and winback after using native back button
    Given I am a current user on the home page
    When I navigate to profile page via more menu
    And The manage data consent link is displayed
    And I navigate to manage data consent page
    And I toggle on the marketingPrefs consent option
    And I navigate back
    And I see a Winback option on the data consent page
    And I opt to stay on the data consent page
    And I should remain on the manage data consent page
    And I navigate back
    And I see a Winback option on the data consent page
    And I opt to leave the data consent page
    And I should be on the your personal details page
    And I navigate to manage data consent page
    And I am taken to the manage data consent page with a back button visible
    And I make a new selection for my consents
    And The confirm option is then enabled
    And I confirm my selection on the data consent page
    Then I should be on the your personal details page
