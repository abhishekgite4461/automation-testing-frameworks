@team-statements @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-enrolled-valid @sw-pca-alltab-on
Feature: E2E_verify RNGA current and previous month transactions

  @genericAccount @ff-condensed-accounttile-on
  Scenario Outline: TC_01 View current and previous month transactions for current account
    Given I am a current user on the home page
    When I navigate to current account to validate accountNumber and select current account
    Then I should be displayed current account name action menu and last four digits of current number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | balanceAfterPending |
      | overDraftLimit      |
    When I view <month> transactions of current account
    Then I should be displayed <month> statements transactions
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
      | chevron                     |
    When I scroll down on the transactions
    Then I should be displayed current balance in the header alongside account Name
    And I should not be displayed account tile in statements page
    And I should be displayed month tabs in place of account tile
    When I scroll back to the top of my transactions
    Then The current balance will no longer display in the header and the account tile will reappear
    When I select one of the posted transactions to validate fields on the VTD
    Then The fields on VTD should match with the transaction line
    And I close the VTD page
    And I should scroll down to the end of the transactions to view <output> message of that <month> tab
    And bottom navigation bar is displayed
    When I navigate to logoff via more menu
    Then I should be on the logout page
    Examples:
      | month    | output          |
      | allTab   | noCurrentInOuts |
      | current  | currentInOuts   |
      | previous | currentInOuts   |

  #STAMOB-1911
  @genericAccount @ff-condensed-accounttile-on
  Scenario Outline: TC_02 View current and previous month transactions for savings account
    Given I am a current user on the home page
    When I navigate to saving account to validate accountNumber and select saving account
    Then I should be displayed saving account name action menu and last four digits of account number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
      | availableFunds      |
      | interestRateLabel   |
    When I view <month> transactions of saving account
    Then I should be displayed <month> statements transactions
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    When I scroll down on the transactions
    Then I should be displayed current balance in the header alongside account Name
    And I should not be displayed account tile in statements page
    And I should be displayed month tabs in place of account tile
    When I scroll back to the top of my transactions
    Then The current balance will no longer display in the header and the account tile will reappear
    And I should scroll down to the end of the transactions to view <output> message of that <month> tab
    And bottom navigation bar is displayed
    When I navigate to logoff via more menu
    Then I should be on the logout page
    Examples:
      | month    | output      |
      | current  | savingEndOf |
      | previous | savingEndOf |

  #STAMOB-1911
  @genericAccount @ff-condensed-accounttile-on
  Scenario Outline: TC_03 View current month transactions for ISA account
    Given I am a current user on the home page
    When I navigate to isa account to validate accountNumber and select isa account
    Then I should be displayed isa account name action menu and last four digits of isa number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
      | remainingAllowance  |
      | interestRateLabel   |
    When I view <month> transactions of isa account
    Then I should be displayed current statements transactions
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    When I scroll down on the transactions
    Then I should be displayed current balance in the header alongside account Name
    And I should not be displayed account tile in statements page
    And I should be displayed month tabs in place of account tile
    When I scroll back to the top of my transactions
    Then The current balance will no longer display in the header and the account tile will reappear
    And I should scroll down to the end of the transactions to view <output> message of that <month> tab
    And bottom navigation bar is displayed
    When I navigate to logoff via more menu
    Then I should be on the logout page
    Examples:
      | month   | output      |
      | current | savingEndOf |

  #STAMOB-1911
  @genericAccount @ff-condensed-accounttile-on
  Scenario Outline: TC_04 View current month transactions for HTB ISA account
    Given I am a current user on the home page
    When I navigate to htbIsa account to validate accountNumber and select htbIsa account
    Then I should be displayed htbIsa account name action menu and last four digits of htbIsa number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
      | interestRateLabel   |
    When I view <month> transactions of htbIsa account
    Then I should be displayed current statements transactions
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    When I scroll down on the transactions
    Then I should be displayed current balance in the header alongside account Name
    And I should not be displayed account tile in statements page
    And I should be displayed month tabs in place of account tile
    When I scroll back to the top of my transactions
    Then The current balance will no longer display in the header and the account tile will reappear
    And I should scroll down to the end of the transactions to view <output> message of that <month> tab
    And bottom navigation bar is displayed
    When I navigate to logoff via more menu
    Then I should be on the logout page
    Examples:
      | month   | output      |
      | current | savingEndOf |

  @genericAccount @environmentOnly @android
  Scenario: TC_05 Fixed Bond account tile entry (HC024)
    Given I am a current user on the home page
    When I select fixedTermDeposit account tile from home page
    Then I should be displayed a message on fixed bond account statement page
    When I scroll down on the transactions
    Then The account tile should not be hidden
    When I navigate to logoff via more menu
    Then I should be on the logout page

  @mbnaSuite @noExistingData @stubOnly
  Scenario: TC_06 View debit and credit transaction in credit card statements
    Given I am a current user on the home page
    When I navigate to creditCard account to validate accountNumber and select creditCard account
    And I should be on the recent of statements page
    Then I should be displayed creditCard account name action menu and last four digits of creditCard number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
      | availableCredit     |
      | creditLimit         |
      | overDueAmount       |
    And I should see transactions since message on recent tab
    And I should see debit transaction with below details in credit card statements
      | detail                      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    When I scroll down on the transactions
    Then I should be displayed current balance in the header alongside account Name
    And I should not be displayed account tile in statements page
    And I should be displayed month tabs in place of account tile
    When I scroll back to the top of my transactions
    Then The current balance will no longer display in the header and the account tile will reappear
    And I swipe to previous 3 months transactions on the account statement page
    And The previously billed info panel appears

  @noExistingData @stubOnly @android
  Scenario: TC_07 User views their insurance policy on the homepage
    Given I am a home_insurance user on the home page
    When I navigate to homeInsurance account on home page
    Then I should see account details with the following fields
      | fieldsInAccountTile             |
      | homeInsuranceStaticProductTitle |
      | policyCoverType                 |
      | insuredAddress                  |
      | periodOfCover                   |
    When I select homeInsurance account tile from home page
    Then I should be displayed homeInsurance account name last four digits of policy number in the header and no action menu for that homeInsurance name
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | policyCoverType     |
      | insuredAddress      |
      | periodOfCover       |
    And I should be displayed policy details with the following fields
      | fieldsInDetails              |
      | policyNumber                 |
      | enquiriesPhoneNumber         |
      | policyUpdatedDate            |
      | policyHolders                |
      | insuredPropertyAddress       |
      | whatsIncludedInPolicyLink    |
      | optionalCoverYouCouldAddLink |
      | costAndPaymentDetailsLink    |
      | policyScheduleNote           |
      | termsDisclaimer              |
    When I scroll down on the transactions
    Then I should not be displayed account tile in statements page
    When I scroll back to the top of my transactions
    Then The account tile will reappear
    When I navigate to logoff via more menu
    Then I should be on the logout page

  @noExistingData @stubOnly @android
  Scenario: TC_08 View loan details & current year transactions
    Given I am a non cbs loan user on the home page
    When I navigate to nonCbsLoan account to validate accountNumber and select nonCbsLoan account
    Then I should be on the currentYear of statements page
    And I should be displayed nonCbsLoan account name action menu and last four digits of loan number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
      | originalLoan        |
    And I tap on the plus sign to expand the accordion with following loan details
      | fieldsForLoanAccount   |
      | nextRepaymentDue       |
      | remainingTerm          |
      | minusAccordion         |
      | openingDate            |
      | monthlyRepaymentAmount |
      | originalLoanTerm       |
      | noteOnAnnualStatement  |
    And I tap on minus sign to close the accordion and should see the following loan details
      | fieldsForLoanAccount |
      | plusAccordion        |
      | nextRepaymentDue     |
      | remainingTerm        |
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    When I scroll down on the transactions
    Then I should be displayed current balance in the header alongside account Name
    And I should not be displayed account tile in statements page
    And I should be displayed month tabs in place of account tile
    When I scroll back to the top of my transactions
    Then The current balance will no longer display in the header and the account tile will reappear
    And I should scroll down to the end of the transactions to view loanEndOf message
    When I swipe to see previous year transactions on the account statement page
    Then I should be on the same previous year statements page
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    And I should not be displayed account tile in statements page
    And I should scroll down to the end of the transactions to view loanEndOf message
    When I navigate to logoff via more menu
    Then I should be on the logout page

  @android @lds @genericAccount
  Scenario: TC_09 View personal loan(CBS) account details on account tile
    Given I am a current user on the home page
    When I navigate to cbsLoan account on home page
    Then I should see account details with the following fields
      | fieldsInAccountTile |
      | accountName         |
      | accountNumber       |
      | sortCode            |
      | accountBalance      |
    When I navigate to cbsLoan account to validate accountNumber and select cbsLoan account
    Then I should be displayed cbsLoan account name last four digits of loan number in the header and no action menu for that cbsLoan name
    And I should see account details with the following fields
      | fieldsInAccountTile |
      | accountBalance      |
    And I should see the message displayed and should not see action menu on account tile
    When I scroll down on the transactions
    Then The account tile should not be hidden
    When I navigate to logoff via more menu
    Then I should be on the logout page

  #Remove @hfx once the data is identified for other brands
  @android @otherAccount @hfx
  Scenario Outline: TC_10 Repossessed Mortgage account tile on home screen
    Given I am a repossessed mortgage user on the home page
    When I navigate to <type> account on home page
    Then I should be displayed the following fields of <type> account
      | fieldsForMortgage                        |
      | offsetRepossessedMortgageName            |
      | offsetRepossessedMortgageNumber          |
      | offsetRepossessedMortgageBalance         |
      | offsetRepossessedMortgageMonthlyPayment  |
      | offsetRepossessedMortgageBalanceAsAtDate |
    When I navigate to <type> account to validate accountNumber and select <type> account
    Then I should be displayed <type> account name last four digits of mortgageAccount number in the header and no action menu for that <type> name
    And I should see account details with the following fields
      | fieldsInAccountTile    |
      | accountBalance         |
      | monthlyPayment         |
      | balanceAsOf            |
      | NotFinalSettlementText |
    And I should be displayed <field> for rePossessedMortgage account
    And I should not be displayed any summary and sub-account details tabs
    And I should see a <message> on the statement page
    When I scroll down on the transactions
    Then The account tile should not be hidden
    Examples:
      | type                | message            | field        |
      | rePossessedMortgage | rePossessedMessage | alertMessage |

  @android @otherAccount
  Scenario Outline: TC_11 Offset Mortgage account tile on home screen
    Given I am a offset mortgage user on the home page
    When I navigate to <type> account on home page
    Then I should be displayed the following fields of <type> account
      | fieldsForMortgage                        |
      | offsetRepossessedMortgageName            |
      | offsetRepossessedMortgageNumber          |
      | offsetRepossessedMortgageBalance         |
      | offsetRepossessedMortgageMonthlyPayment  |
      | offsetRepossessedMortgageBalanceAsAtDate |
    When I navigate to <type> account to validate accountNumber and select <type> account
    Then I should be displayed <type> account name last four digits of mortgageAccount number in the header and no action menu for that <type> name
    And I should see account details with the following fields
      | fieldsInAccountTile    |
      | accountBalance         |
      | monthlyPayment         |
      | balanceAsOf            |
      | NotFinalSettlementText |
    And I should be displayed <field> for rePossessedMortgage account
    And I should not be displayed any summary and sub-account details tabs
    And I should see a <message> on the statement page
    When I scroll down on the transactions
    Then The account tile should not be hidden
    Examples:
      | type           | message       | field          |
      | offSetMortgage | offSetMessage | noAlertMessage |

  @genericAccount @android
  Scenario: TC_12 View mortgage transactions, end of transaction message, view sub-accounts list and details
    Given I am a current user on the home page
    When I navigate to mortgage account to validate accountNumber and select mortgage account
    And I should be on the mortgageSummary of statements page
    Then I should be displayed mortgage account name action menu and last four digits of mortgageAccount number in the header
    And I should see account details with the following fields
      | fieldsInAccountTile    |
      | accountBalance         |
      | monthlyPayment         |
      | balanceAsOf            |
      | NotFinalSettlementText |
    When I view full mortgage details
    Then I should see following fields for expanded mortgage details
      | fieldsInMortgageDetails |
      | nextPaymentDue          |
      | remainingFullTerm       |
      | mortgageType            |
      | mortgageHolders         |
      | propertyAddress         |
      | mortgageOpenedDate      |
      | arrearsBalance          |
      | currentBalance          |
      | currentMonthlyPayment   |
      | minimumMonthlyPayment   |
      | interestRateAsOfDate    |
      | staticDisclaimerText    |
      | currentBalanceInfoLink  |
      | monthlyPaymentsInfoLink |
    When I select mortgageSummaryTab currentBalanceInfo link to view info
    Then I should be displayed the currentBalanceInfo as a layover
    When I select close button in info page
    And I select mortgageSummaryTab monthlyPaymentsInfo link to view info
    Then I should be displayed the monthlyPaymentInfo as a layover
    When I select close button in info page
    And I tap on minus sign to close the accordion
    Then I should see following fields in statements list
      | fieldsInStatementsList         |
      | firstTranscationDate           |
      | firstTranscationDescription    |
      | firstTransactionRunningBalance |
      | firstTranscationAmount         |
    And I scroll down on the transactions
    And I should be displayed current balance in the header alongside account Name
    And I should not be displayed account tile in statements page
    And I should be displayed month tabs in place of account tile
    When I scroll back to the top of my transactions
    Then The current balance will no longer display in the header and the account tile will reappear
    And I should scroll down to the end of the transactions to view mortgageEndOf message
    When I select sub account tab
    Then I should see summary of sub account with the following fields in the summary for each sub-account
      | fieldsInSubAccountsList         |
      | subAccountTypeNo                |
      | mortgageSubAccountBalance       |
      | mortgageSubAccountType          |
      | interestRateAsAtDate            |
      | subAccountCurrentMonthlyPayment |
      | nextPaymentDueOnDate            |
      | disclaimerText                  |
    And I tap on any of the sub account
    When I tap on sub account accordion in details tab
    Then I should see following fields for expanded mortgage details
      | fieldsInMortgageDetails        |
      | nextPaymentDue                 |
      | remainingFullTerm              |
      | mortgageType                   |
      | subAccountMortgageOpenedDate   |
      | paymentType                    |
      | originalAmount                 |
      | originalFullTerm               |
      | subAccountInterestRateAsAtDate |
      | currentMonthlyPaymentDetails   |
      | minimumMonthlyPayment          |
      | remainingAmount                |
      | mortgageRateHistoryTitle       |
      | mortgageRateHistoryDate        |
      | mortgageRateHistoryType        |
      | mortgageRateHistoryRate        |
      | staticDisclaimerText           |
      | currentBalanceInfoLink         |
      | monthlyPaymentsInfoLink        |
    When I select mortgageSubAccountTab currentBalanceInfo link to view info
    Then I should be displayed the currentBalanceInfo as a layover
    When I select close button in info page
    And I select mortgageSubAccountTab monthlyPaymentsInfo link to view info
    Then I should be displayed the monthlyPaymentInfo as a layover
    When I select close button in info page
    Then I should be displayed subAccountDetailsTab with full mortgage details
    When I navigate to logoff via more menu
    Then I should be on the logout page
