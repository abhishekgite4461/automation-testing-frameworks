@team-statements @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-enrolled-typical @genericAccount @environmentOnly @defect
Feature: E2E Saving Interest Rate Details
  As a NGA Customer
  I should be able to view interest rate for my saving account
  So that I am aware of the interest rate on my saving account

  #STAMOB-1308
  Scenario: TC_01 Interest Rate validation for Savings Account
    Given I am a current user on the home page
    And I navigate to saving account on home page
    When I tap and hold on the saving account tile to Savings Interest Rate Details screen
    Then I should see the following fields in the details screen
      | fieldsInDetailsScreen   |
      | accountName             |
      | accountBalance          |
      | interestRateLabel       |
      | interestRateDescription |
    When I close the interest rate details screen using the close button
    And I select View Interest Details option of saving account from action menu
    Then I should see the following fields in the details screen
      | fieldsInDetailsScreen   |
      | accountName             |
      | accountBalance          |
      | interestRateLabel       |
      | interestRateDescription |
    And I close the interest rate details screen using the close button
    When I select saving account tile from home page
    And I select the View Interest Details option from the action menu in statements page
    Then I should be on Savings Interest Rate Details screen
    And I should see the following fields in the details screen
      | fieldsInDetailsScreen   |
      | accountName             |
      | accountBalance          |
      | interestRateLabel       |
      | interestRateDescription |
    When I close the interest rate details screen using the close button
    And I navigate to logoff via more menu
    Then I should be on the logout page
