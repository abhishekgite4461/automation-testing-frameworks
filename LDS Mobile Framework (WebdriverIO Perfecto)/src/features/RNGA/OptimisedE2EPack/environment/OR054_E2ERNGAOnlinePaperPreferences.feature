@team-navigation @mbnaSuite @nga3 @rnga @environmentOnly @optFeature @exhaustiveData
@appiumOnly @ff-webview-debug-on

#MOB3-1301

# Note : For MBNA a "current" user will return a "credit card" user

# REASONS FOR APPIUM ONLY EXECUTION
# Cookie Policy crops up during the launch of the online paper references home page everytime in PUT 12 , SIT
# enev even after accepting data consent , the above issue persists
# manually rejects the cookie policy on the launch of the page which cannot be done in perfecto
# sometimes BY POST toggle does not work in perfecto and the test script fails

Feature: User wants to change his/her online & paper preferences
  In order to edit my online & paper preferences
  As a RNGA authenticated customer
  I want to view online and paper preferences option from How we contact page

  Background:
    Given I am an enrolled "current" user logged into the app
    When I navigate to settings page via more menu

  Scenario: TC_01 User updates paperless preferences
    And I navigate to online and paper preferences from the settings page
    Then I should be on the manage your online and paper preference webview page
    When I select update preference from manage your online and paper preference webview page
    Then I am on confirm your paperless preferences webview page
    When I accept the terms & condition and submit order paper preference
    Then I should see update preference confirmation page

  Scenario: TC_02 User selects OK button on manage paperless preferences deep link to native reset password screen
    When I navigate to online and paper preferences from the settings page
    Then I should be on the manage your online and paper preference webview page
    When I select update preference from manage your online and paper preference webview page
    Then I am on confirm your paperless preferences webview page
    When I select on the forget password link on confirm your paperless preferences webview page
    Then I should see forgotten password winback with options on top of the screen with Cancel and OK option
    And on selection of OK option I should be on native reset password screen
