@team-login @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @genericAccount @stub-coa-map-valid-address @sw-coa-native-on
Feature: User wants to view personal details
  In order to view or edit my personal details
  As a RNGA authenticated customer
  I want to view my personal details page

   # Note : For MBNA a "current" user will return a "credit card" user
  # Note :  @mbnaSuite removed due to MBNA team has not implemented new change of address screen
  #MOB3-5090 #MOB3-5097
  Scenario: TC_01 View personal details and change address page
    Given I am an enrolled "current" user logged into the app
    When I navigate to profile page via more menu
    Then I should be shown the list of personal details
      | personalDetail |
      | Full Name      |
      | User ID        |
      | Mobile Number  |
      | Home Number    |
      | Work Number    |
      | Address        |
      | Postcode       |
      | Email address  |
    When I choose to update address from the personal details
    And I should be shown an option to change my address using secure call for non UK address or child account
    And I navigate to logoff via more menu
    Then I should be on the logout page
