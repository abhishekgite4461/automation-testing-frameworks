@team-delex @mbnaSuite @nga3 @rnga @stub-enrolled-typical @sw-enrolment-on @sw-light-logon-on @optFeature
@genericAccount

Feature: E2E_Verify RNGA legal information display

  # Addded the Scenario to Enrolment required Feature file, Since it needs an unenrolled user.
  # Scenario Outline: TC_001_Unenrolled Customer chooses to view Third party acknowledgements
  # Note : For MBNA a "current" user will return a "credit card" user

  Scenario Outline: TC_01 Enrolled Customer chooses to view Third party acknowledgements
    Given I am a current user on the home page
    When I navigate to legal info from settings page via more menu
    And I select <legalOption> from the legal information page overlay
    Then I should be on the <legalOption> legal info item page overlay
    And I navigate back
    When I select <legalOption> from the legal information page overlay
    Then I should be on the <legalOption> legal info item page overlay
    When I select home tab from bottom navigation bar
    Then I am navigated to first account on home page
    Examples:
      | legalOption                  |
      | legal and privacy            |
      | cookie use and permissions   |
      | third party acknowledgements |
