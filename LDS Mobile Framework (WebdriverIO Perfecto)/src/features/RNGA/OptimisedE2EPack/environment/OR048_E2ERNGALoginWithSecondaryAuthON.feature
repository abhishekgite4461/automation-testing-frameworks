@team-login @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @unEnrolled
Feature: Traditional login using secondary auth when MI information already set with 'Device Enrolment' and 'Light Logon' switches ON

  Background:
    Given I am a current user on the MI page

#  @stub-valid
#  Scenario: TC_001_Validate the mi text fields header to be unique in RNGA app
#    Then I should see 3 text boxes to enter the MI and their headers are unique

  @stub-valid @mbnaSuite
  Scenario: TC_01 Validate the MI information is masked
    When I enter 2 characters of MI
    Then I should see the MI information is masked

  #Applicable only for environment clubbed all the above scenario in one scenario. we cannot clubbed in stub due to
  # each scenario pointing to different stub combination
  @environmentOnly
  Scenario: TC_02 Login to RNGA app with Wrong MI and enter the correct MI & wrong password second time
    When I enter incorrect MI
    Then I should see incorrect MI error message in re-enter MI page
    When I enter correct MI in re-enter MI page
    And I enter incorrect password in confirm password page
    Then I should see incorrect password error message in re-enter MI page
    And I should be on the re-enter MI page
    When I enter incorrect MI in re-enter MI page
    And I enter correct password in confirm password page
    Then I should see incorrect MI error message in re-enter MI page
    And I should be on the re-enter MI page
    When I enter correct MI in re-enter MI page
    Then I should see the fscs tile in the re-enter password page
    And I enter correct password in confirm password page
    And I should be on the EIA page

  #Applicable only for environment clubbed all the above scenario in one scenario. we cannot clubbed in stub due to
  # each scenario pointing to different stub combination
  @environmentOnly @mbna
  Scenario: TC_03 Login to RNGA app with Wrong MI and enter the correct MI & wrong password second time (MBNA)
    When I enter incorrect MI
    Then I should see incorrect MI error message in re-enter MI page
    When I enter correct MI in re-enter MI page
    And I enter incorrect password in confirm password page
    Then I should see incorrect password error message in re-enter MI page
    And I should be on the re-enter MI page
    When I enter incorrect MI in re-enter MI page
    And I enter correct password in confirm password page
    Then I should see incorrect MI error message in re-enter MI page
    And I should be on the re-enter MI page
    When I enter correct MI in re-enter MI page
    And I enter correct password in confirm password page
    Then I should be on the EIA page
