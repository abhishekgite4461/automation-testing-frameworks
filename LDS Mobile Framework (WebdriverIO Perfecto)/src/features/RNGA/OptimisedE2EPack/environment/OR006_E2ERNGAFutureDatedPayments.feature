@team-payments @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @genericAccount @sw-sca-android-off @sw-sca-ios-off
Feature: E2E_Verify RNGA user select date when the payment should go out on

  @stub-enrolled-typical
  Scenario Outline: TC_01 Internal Payment : make future dated payment
    Given I am a current user on the home page
    When I navigate to add new recipient option using payment hub
    Then I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    Then I select continue button on add payee page
    And I continue to payment hub page from COP response page
    And I should see UK Review Details page
    And I should see to add a reference <reference>
    When I select continue on UK review details page
    Then I should be prompted to enter my password
    When I enter a correct password
    And I am on the Make Payment screen with newly added beneficiary selected
    And I enter amount in the amount field as 1.00
    And I select calendar picker option for future date payment
    Then I should see a calendar view
    And I should be allowed to select a payment date up to 31 days from current day
    When I select previous month chevron from the top header of calendar view
    And I select the date as <days> from today's date from calendar view
    And I select Continue button on payment hub page
    Then I should see the review payment page
    And I am shown 'When' the payment will be made on payment review page
    And I am shown information of payment date if it falls on a holiday
    When I select the confirm option in the review payment page
    Then I should see the payment successful message page
    And I navigate back to home page from payment success page
    Examples:
      | sortCode | accountNumber | recipientName | reference | days |
      | 122253   | 10006969      | Kari Smallridge     | test0123      | 2    |

  # Added Pending payment scenarios here to avoid data maintenance
  @stub-enrolled-typical
  Scenario Outline: TC_02 Verify pending payment is not allowed to select as the UK beneficiary account in the payment hub page
    Given I am a current user on the home page
    When I select <account> account tile from home page
    And I choose the recipient account on the payment hub page
    Then I should not be able to select pending payment from view recipient page
    And I navigate back to home page from payment hub
    And I navigate to logoff via more menu
    And I should be on the logout page
    Examples:
      | account |
      | current |

  # MOB3-9153, 1632
  @stub-enrolled-valid
  Scenario: TC_03 Recipient with a Pending Payment
    Given I am a current user on the home page
    And I navigate to the remitter account on the payment hub page
    And I select remittingAccountWithExternalBeneficiary Account from the account list
    And I select the recipient account on the payment hub page
    When I select manage on an pending payment account
    Then I am given the option to view the pending payments for the account
    And the pay recipient option is disabled
    And the delete recipient is disabled
    And I close the pending payment popup in the payment hub
    And I navigate back to home page from payment hub
    And I navigate to logoff via more menu
    And I should be on the logout page
