@team-payments @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @ff-strong-customer-auth-off @sw-sca-android-off @sw-sca-ios-off @sw-save-the-change-native-off
Feature: E2E_Verify RNGA payment hub

  @stub-enrolled-typical @genericAccount
  Scenario Outline: TC_02 Dynamic Searching per character added
    Given I am a current user on the home page
    And I add a new recipient with valid <sortCode>, <accountNumber>, <recipientName>, <reference>
    And I select the recipient account on the payment hub page
    Then I verify search results for each search term
      | searchType | searchTerm | results         |
      | recipient  | ili        | no results      |
      | recipient  | Ada        | adam            |
      | recipient  | Ander      | adam            |
    Examples:
      | sortCode | accountNumber | recipientName | reference |
      | 122253   | 10006969      | Kari Smallridge | test    |

  # ALM-3109 is raised for payM not working on lower environments
  @stubOnly @stub-enrolled-valid @genericAccount
  Scenario Outline: TC_03 Successfully making a payment to PAYM
    Given I am a current user on the home page
    And I am on the payment hub page
    When I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    And I select the continue option in the payment hub page
    And I select the confirm option in the review payment page
    Then I should see a payment confirmed screen
    And I should see an option make another transfer
    And I should see an option to view transactions
    Examples:
      | fromAccount        | toAccount                  | amount | referenceText |
      | addPayMRemitter    | addExistingPayMBeneficiary | 10.00  | PAYM          |

  # ALM-3109 is raised for payM not working on lower environments
  @stubOnly @stub-enrolled-valid @genericAccount
  Scenario Outline: TC_04 Add new PayM beneficairy and make a payment
    Given I am a current user on the home page
    And I navigate to add new recipient option using payment hub
    And I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I choose UK mobile number from account category page
    When I am on enter basic detail page for pay a contact
    Then I should see the following details in Add PayM details page
      | detail       |
      | mobileNumber |
      | amount       |
      | reference    |
    And I have entered valid <mobileNumber> <amount> and <referenceText>
    And I select continue in add new UK mobile number page
    And I am on the Confirm Contact page
    And I am displayed the following details
      | detail                      |
      | nameAndPhoneNumberInConfirm |
    And I select Confirm
    And I am taken to the Confirm Payment page
    And I select Confirm on the Confirm Payment page
    And I should be prompted to enter my password
    When I enter a correct password
    Then I am shown Payment Successful page
    Examples:
      | mobileNumber | amount | referenceText |
      | 07575858520  | 20.00  | newPayM       |

  #MQE-59
  @stub-enrolled-typical @genericAccount
  Scenario: TC_05 Display Payment success screen for immediate(same day) payment when Payment request is successful
    Given I am a current and creditcard user on the home page
    And I am on the payment hub - pay a credit card page
    And I submit the pay a credit card with all the mandatory fields with corresponding amount value 1.00
    When I select the confirm option in the review payment page
    Then I should see a payment confirmed screen
    And I navigate back to home page from payment success page
    And I navigate to logoff via more menu
    And I should be on the logout page

  @stub-internet-transaction-limit @genericAccount
  Scenario Outline: TC_06 Amount field entered is greater than the Internet transaction limit(MG-454)
    Given I am an enrolled "current" user logged into the app
    And I fetch the payment limit for current account from IBC
    And I add a new recipient with details <fromAccount>, <sortCode>, <accountNumber>, <recipientName>, <reference>
    When I submit the payment with all the mandatory fields
    Then I should see internet transaction limit exceeded error message in payment hub page
    And I should see the values pre-populated in the payment hub page for payment
    When I select dismiss modal using close button
    And I choose the recipient account on the payment hub page
    And I search recipient account <recipientName> on payment hub page to delete
    And I delete the selected recipient
    Then I am asked for confirmation if I want to delete the recipient
    When I select to confirm delete a recipient
    Examples:
      | sortCode | accountNumber | recipientName   | reference | fromAccount |
      | 400202   | 74374163      | ATWELL MARTIN LTD| test      | current |

  @stub-cbs-prohibitive-transfers @otherAccount
  Scenario: TC_07 Show an error message MG-385  when CBS Prohibitive indicator is loaded on either the remitting or beneficiary account
    Given I am a cbs prohibitive indicator user on the home page
    And I am on the payment hub page
    And I select the to account as cbsProhibitive in the payment hub page
    And I submit with amount 1.00 in the payment hub page
    When I select the confirm option in the review transfer page
    Then I should see cbs prohibitive error message in payment unsuccessful page

  @stub-enrolled-valid @genericAccount @ff-pca-alltab-enabled-off
  Scenario Outline: TC_08 Verifying the Pre-population of the accounts on Payment Hub from the Statements page
    Given I am a current user on the home page
    And I obtain the default primary account
    And I select <selectAccount> account tile from home page
    And I am on the payment hub page
    Then I should see the from field pre-populated with <fromAccountDisplay> account
    And I should see the to field pre-populated with <toAccountDisplay> account
    Examples:
      | selectAccount | fromAccountDisplay | toAccountDisplay |
      | current       | current            | noAccount        |
      | saving        | saving             | noAccount        |
      | isa           | isa                | noAccount        |
      | creditCard    | defaultPrimary     | creditCard       |
      | mortgage      | defaultPrimary     | mortgage         |

  @stub-enrolled-valid @genericAccount @ff-pca-alltab-enabled-off
  Scenario Outline: TC_09 Verifying the Pre-population of the accounts on Payment Hub from the Call Us page
    Given I am a current user on the home page
    And I obtain the default primary account
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    When I choose <selectAccount> tile from your accounts page
    Then I should be on the secure call page
    When I am on the payment hub page
    Then I should see the from field pre-populated with <fromAccountDisplay> account
    And I should see the to field pre-populated with <toAccountDisplay> account
    Examples:
      | selectAccount | fromAccountDisplay | toAccountDisplay |
      | current       | current            | noAccount        |
      | saving        | saving             | noAccount        |
      | creditCard    | defaultPrimary     | creditCard       |
      | mortgage      | defaultPrimary     | mortgage         |

  @stub-enrolled-valid @genericAccount @ff-pca-alltab-enabled-off
  Scenario Outline: TC_10 Verifying the Pre-population of the accounts on Payment Hub from the Cheque Deposit page
    Given I am a current user on the home page
    And I obtain the default primary account
    And I navigate to cheque deposit page for more information via more menu
    And I select the account navigation icon in deposit cheque page
    When I choose <selectAccount> tile from your accounts page
    Then I should be on the cheque deposit screen
    When I am on the payment hub page
    Then I should see the from field pre-populated with <fromAccountDisplay> account
    And I should see the to field pre-populated with <toAccountDisplay> account
    Examples:
      | selectAccount | fromAccountDisplay | toAccountDisplay |
      | current       | current            | noAccount        |
      | saving        | saving             | noAccount        |
      | isa           | isa                | noAccount        |

  @stub-enrolled-typical @genericAccount
  Scenario: TC_11 List of eligible and inEligible accounts in the From Account
    Given I am a current user on the home page
    When I choose the remitter account on the payment hub page
    Then I should see the below accounts in the view remitter page
      | accounts |
      | current  |
      | saving   |
      | isa      |
    And I should not see the below accounts in the view remitter page
      | accounts   |
      | creditCard |
      | nonCbsLoan |
    And I navigate back to home page from payment hub
    And I navigate to logoff via more menu
    And I should be on the logout page

  @manual
  Scenario: TC_12 Unrecognised Reference Format
    And I have entered recipient name|sort code|account number of a whitelisted company
    And I choose to Continue
    And the Search results page is displayed
    And the UK Company Review Details page is displayed
    And I enter an invalid Reference
    When I choose to Continue
    Then I am displayed an error message

  @stub-internet-transaction-limit @genericAccount
  Scenario: TC_13 Verify fraudster warning pop-up appears when a customer is making a payment of £5,000+
    Given I am a current user on the home page
    And I navigate to saving account on home page
    And I select the "transfer and payment" option in the action menu of saving account
    When I submit transfer with from account saving and to account current with amount 1
    And I select dismiss modal using close button
    And I select the leave option in the payment hub page winback
    Then I should be on the home page
    When I am on the payment hub page
    And I submit the payment with all the mandatory fields with any from account and to account with amount value 5000.00
    Then I see the fraudster warning message pop up on the payment hub page
    When I choose to cancel on the warning message
    Then I should be on the payment hub page
    When I select Continue button on payment hub page
    Then I see the fraudster warning message pop up on the payment hub page

  @stub-credit-card-only @otherAccount
  Scenario: TC_14 User does not hold other eligible remitting accounts
    Given I am a credit card only user on the home page
    When I am on the payment hub page
    Then I should see the from field pre-populated with debitCard account
    And I should see the to field pre-populated with creditCard account
    And I am not shown payment date in the payment hub

  @environmentOnly @genericAccount
  # DPLDA-50
  Scenario Outline: TC_16 RNGA Saving Account Pay Credit Card Bill
    Given I am a current and savings user on the home page
    When I navigate to creditCard account on home page
    And I select the "pay a credit card" option in the action menu of creditCard account
    And I select dismiss modal using close button
    And I select payAndTransfer tab from bottom navigation bar
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter amount greater than the balance of the credit card account in the payment hub page
    Then I should see Outbound or Insufficient credit card payment validation error message
    When I am on the payment hub page with data prepopulated
    And I clear the amount field in the payment hub page
    And I submit the future dated payment for a credit card with all the mandatory fields with corresponding amount value <amount>
    And I select the confirm option in the review payment page
    Then I should see a payment confirmed screen
    Examples:
      | fromAccount   | toAccount  | amount |
      | saving        | creditCard | 0.01   |

  # this sceanrio is copied from home page feature file(aov team) because this scenario should ideally be a part of payments team
  @stub-enrolled-valid @genericAccount
  Scenario: TC_02 Verify the Send Bank Details functionality
    Given I am an enrolled "current" user logged into the app
    And I navigate and validate the options in the current account in below table in the home page
      | accountFields      |
      | accountTileLogo    |
      | accountNumber      |
      | sortCode           |
      | accountBalance     |
    When I select the action menu of current account
    Then I should see the below options in the action menu of the current account
      | optionsInActionMenu                     |
      | viewTransactionsOption                  |
      | transferAndPaymentOption                |
      | viewUpcomingPayment                     |
      | sendMoneyOutsideTheUKOption             |
      | chequeDepositOption                     |
      | standingOrderAndDirectDebitOption       |
      | changeAccountTypeOption                 |
      | applyForOverdraftOption                 |
      | orderPaperStatementOption               |
      | cardManagementOption                    |
      | sendBankDetailsOption                   |
    When I select sendBankDetailsOption option
    Then I should see the native OS share panel
    When I select the email app on the share panel
    Then I should see the relevant current account details on the email text

  # PAYMOB-397 #DPLDA-513
  @environmentOnly @genericAccount
  Scenario: TC-07 Main Success Scenario: User navigates to save the changes functionality (webview page) from account tile menu options
    Given I am a current user on the home page
    When I select the "save the change" option in the action menu of current account
    Then I should be on the save the changes webview page
    And I should be able to navigate back to home screen from save the changes webview page
