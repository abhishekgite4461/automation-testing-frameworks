@team-delex @mbnaSuite @nga3 @rnga @stub-enrolled-typical @sw-enrolment-on @sw-light-logon-on @deviceHealthCheck
Feature: Few sanity scripts to check DeviceHealth

  Scenario: TC_01 Log into RNGA successfully and enrol the device with a valid registered phone number
    Given I am a current user on the home page
    When I choose my security auto logout settings
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page
    When I navigate to settings page via more menu
    And I select the reset app option from the settings page
    Then I should see information about reset app and a warning message
    When I select reset app button from the reset app page
    And I select confirm button from the reset app confirmation pop up
    Then I should be on the logout page with reset app complete message

  Scenario: TC_03 Verify success transfer journey and view transaction
    Given I am a current user on the home page
    And I am on the payment hub page
    When I submit transfer with from account current and to account saving with amount 1.00
    And I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page
    When I select the view transaction option in the transfer success page
    And I should see the - £1.00 in the current month statement of the remitting account
    And I navigate to home page
    Then I should see the corresponding 1.00 deducted in the remitter current and recipient accounts saving

  Scenario: TC_01 Enrolled Customer chooses to view Third party acknowledgements
    Given I am a current user on the home page
    When I navigate to legal info from settings page via more menu
    And I select legal and privacy from the legal information page overlay
    Then I should be on the legal and privacy  legal info item page overlay
    And I navigate back
    When I select legal and privacy from the legal information page overlay
    Then I should be on the <legalOption> legal info item page overlay
    When I select home tab from bottom navigation bar
    Then I am navigated to first account on home page
