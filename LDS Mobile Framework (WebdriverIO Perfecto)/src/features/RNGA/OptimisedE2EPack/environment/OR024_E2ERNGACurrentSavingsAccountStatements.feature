@team-statements @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @genericAccount @sw-pca-alltab-on @environmentOnly @ios @stub-enrolled-valid
  # separate feature file is created for android for all the scenarios with condensed account tile changes.
  # Will be common scenarios for both once condensed account tile is implemented in iOS
Feature: E2E_verify RNGA current and previous month transactions

  Scenario: TC_01 Fixed Bond account tile entry (HC024)
    Given I am a current user on the home page
    When I select fixedTermDeposit account tile from home page
    Then I should be displayed a message on fixed bond account statement page
    When I navigate to logoff via more menu
    Then I should be on the logout page
