@team-statements @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @stub-rnga-hcc @otherAccount @optFeature
# Making it Stub Only for time being since separate data is required for these scenarios.
Feature: Display overdraft remaining on current account

  Scenario: TC_01 Display overdraft remaining and progress bar
    Given I am a applied overdraft current user on the home page
    When I select currentAccountWithUsedOverDraft account tile from home page
    Then I should see account details with the following fields
      | fieldsInAccountTile      |
      | balanceAfterPendingLabel |
      | balanceAfterPendingValue |
      | overDraftLimitLabel      |
      | overDraftLimitValue      |
      | overDraftRemainingLabel  |
      | overDraftRemainingValue  |
    And I should see the remaining overdraft limit progress bar

  Scenario Outline: TC_02 Do not display overdraft remaining (Overdraft not in use & no overdraft) and no progress bar
    Given I am a applied overdraft current user on the home page
    When I select <type> account tile from home page
    Then I should see account details with the following fields
      | fieldsInAccountTile      |
      | balanceAfterPendingLabel |
      | balanceAfterPendingValue |
      | overDraftLimitLabel      |
      | overDraftLimitValue      |
    And I should not see the remaining overdraft limit progress bar
    Examples:
      | type                              |
      | currentAccountWithUnusedOverDraft |
      | currentAccountWithNoOverDraft     |
