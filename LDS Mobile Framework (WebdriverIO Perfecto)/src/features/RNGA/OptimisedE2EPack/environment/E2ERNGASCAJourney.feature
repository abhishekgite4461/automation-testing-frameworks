@team-payments @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @sw-sca-android-on @sw-sca-ios-on @optFeature @genericAccount @ff-cop-off @sca
@sw-biometric-payment-off @sit02Job
Feature: E2E_Verify RNGA Add new Beneficiary journey with SCA

  @stub-enrolled-typical
  Scenario Outline: TC_01 Internal Payment : make future dated payment with SCA Journey
    Given I am a current user on the home page
    When I navigate to add new recipient option using payment hub
    Then I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    Then I select continue button on add payee page
    And I am on the Make Payment screen with newly added beneficiary selected
    And I enter amount in the amount field as 1.00
    And I enter reference as reference
    And I select view payment limits link
    And I am shown following account daily limits for the account in view payments limit page
      | detail                |
      | onlineMobileBanking   |
      | ukPaymentsDailyLimit  |
      | ukPaymentResetMessage |
      | transferYourOwnAccount|
      | telephonyBanking      |
      | ukTelephonyBanking    |
    And I select back buttom from view payment limit page
    And I select calendar picker option for future date payment
    And I should see a calendar view
    And I should be allowed to select a payment date up to 31 days from current day
    When I select previous month chevron from the top header of calendar view
    And I select the date as <days> from today's date from calendar view
    And I select Continue button on payment hub page
    Then I should see the review payment page
    And I am shown 'When' the payment will be made on payment review page
    When I select the confirm option in the review payment page
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see the payment successful message page
    And I navigate back to home page from payment success page
    Examples:
      | sortCode | accountNumber | recipientName | days |
      | 110300   | 12709366      | TEST01231     | 2    |

  @stub-enrolled-typical
  Scenario Outline: TC_02 Main Success Scenario: Creation of a Successful Standing Order for external accounts with SCA Journey
    Given I am a current user on the home page
    When I navigate to add new recipient option using payment hub
    And I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    Then I select continue button on add payee page
    And I am on the Make Payment screen with newly added beneficiary selected
    And I have chosen to make this a Standing Order option
    And I provide <Amount> for a standing order
    And I select view payment limits link
    And I am shown following account daily limits for the account in view payments limit page
      | detail                |
      | onlineMobileBanking   |
      | ukPaymentsDailyLimit  |
      | ukPaymentResetMessage |
      | transferYourOwnAccount|
      | telephonyBanking      |
      | ukTelephonyBanking    |
    And I select back buttom from view payment limit page
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I review the Standing Order
    And I should be prompted to enter my password
    And I enter a correct password
    And I should see a Standing Order set up success message
    And I navigate back to home page from payment hub
    Examples:
      | sortCode | accountNumber | recipientName | Amount | frequency        |
      | 110300   | 12709366      | SO02          | 2      | Every four weeks |

  @stub-enrolled-valid
  Scenario Outline: TC_03 Adding New UK External beneficiary, Payment, Dynamic Search and deletion of the Recipient for a savings user
    Given I am a current user on the home page
    And I am on the payment hub page
    When I select the from account as saving in the payment hub page
    And I select the recipient account on the payment hub page
    And I choose to Add new payee on search recipient page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    And I am on the Make Payment screen with newly added beneficiary selected
    And I select view payment limits link
    And I am shown following account daily limits for the account in view payments limit page
      | detail                |
      | onlineMobileBanking   |
      | ukPaymentsDailyLimit  |
      | ukPaymentResetMessage |
      | transferYourOwnAccount|
      | telephonyBanking      |
      | ukTelephonyBanking    |
    And I select back buttom from view payment limit page
    And I submit the payment with amount value <amount> and reference
    And I select the confirm option in the review payment page
    And I should be prompted to enter my password
    And I enter a correct password
    Then I am shown Payment Successful page
    When I navigate back to home page from payment success page
    And I am on the payment hub page
    And I select the from account as saving in the payment hub page
    And I select the recipient account on the payment hub page
    And I search recipient account <recipientName> on payment hub page to delete
    And I delete the selected recipient
    Then I am asked for confirmation if I want to delete the recipient
    When I select to confirm delete a recipient
    And I navigate back to home page after deleting the recipient
    And I navigate to logoff via more menu
    Then I should be on the logout page
    Examples:
      | sortCode | accountNumber | recipientName | amount |
      | 400202   | 74374163      | TestAutomation| 1.00   |

  @stub-enrolled-valid
  Scenario Outline: TC_04 Adding New UK External beneficiary, Payment, Dynamic Search and deletion of the Recipient for a current user
    Given I am a current user on the home page
    When I navigate to add new recipient option using payment hub
    And I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    And I am on the Make Payment screen with newly added beneficiary selected
    And I enter payment amount greater than available balance in the remitter account in the payment hub page
    And I should be able to close the insufficient money in your account error message in the payment hub page
    And I clear the amount field in the payment hub page
    And I am on the Make Payment screen with newly added beneficiary selected
    And I enter <amount> in the payment amount field in the payment hub page
    And I select Continue button on payment hub page
    And I select the confirm option in the review payment page
    And I should be prompted to enter my password
    And I enter a correct password
    Then I am shown Payment Successful page
    When I navigate back to home page from payment success page
    And I choose the recipient account on the payment hub page
    And I search recipient account <recipientName> on payment hub page to delete
    And I delete the selected recipient
    Then I am asked for confirmation if I want to delete the recipient
    When I select to confirm delete a recipient
    And I navigate back to home page after deleting the recipient
    And I navigate to logoff via more menu
    Then I should be on the logout page
    Examples:
      | sortCode | accountNumber | recipientName | amount |
      | 400202   | 74374163      | Adam Anderson | 1      |

  @stub-enrolled-valid
  Scenario Outline: TC_05 Dynamic Searching per character added
    Given I am a current user on the home page
    When I navigate to add new recipient option using payment hub
    Then I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    Then I am on the Make Payment screen with newly added beneficiary selected
    When I enter amount in the amount field as 1.00
    And I enter reference as <reference>
    And I select Continue button on payment hub page
    Then I should see the review payment page
    When I select the confirm option in the review payment page
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see the payment successful message page
    When I select the make another payment option in the payment success page
    Then I should be navigated to the payment hub page with from account as current in the payment hub page
    When I select the recipient account on the payment hub page
    Then I verify search results for each search term
      | searchType | searchTerm | results         |
      | recipient  | ili        | no results      |
      | recipient  | Ada        | adam            |
      | recipient  | Ander      | adam            |
    Examples:
      | sortCode | accountNumber | recipientName | reference |
      | 400202   | 74374163      | Adam Anderson | test      |

  @stub-enrolled-valid
  Scenario Outline: TC_06 User keeps recipient
    Given I am a current user on the home page
    When I navigate to add new recipient option using payment hub
    Then I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    Then I am on the Make Payment screen with newly added beneficiary selected
    When I enter amount in the amount field as 1.00
    And I enter reference as <reference>
    And I select Continue button on payment hub page
    Then I should see the review payment page
    When I select the confirm option in the review payment page
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see the payment successful message page
    When I select the make another payment option in the payment success page
    Then I should be navigated to the payment hub page with from account as current in the payment hub page
    When I select the recipient account on the payment hub page
    And I am on manage actions for a <type>
    And I select to delete a recipient
    And I select to keep the recipient
    Then I am taken back to the recipient list
    And  the recipient is still available
    When I navigate back to home page from payment hub
    And I navigate to logoff via more menu
    Then I should be on the logout page
    Examples:
      | sortCode | accountNumber | recipientName | reference | type |
      | 400202   | 74374163      | Adam Anderson | test      | adam |

  @stub-whitelisted-biller-success
  Scenario: TC_07 Add New Company Beneficiary account and Make Payment with SCA journey
    Given I am a current user on the home page
    When I navigate to add new recipient option using payment hub
    Then I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I enter whitelisted company details
    And I select continue button on add payee page
    And I select the biller AQUA MASTERCARD from the biller list
    And I validate reference field label
    And I enter 1.00 in the payment amount field in the payment hub page
    And I enter the wrong reference value for the biller
    Then I validate the error banner message for wrong reference value
    And I enter reference as <5434298992481650>
    And I select view payment limits link
    And I am shown following account daily limits for the account in view payments limit page
      | detail                |
      | onlineMobileBanking   |
      | ukPaymentsDailyLimit  |
      | ukPaymentResetMessage |
      | transferYourOwnAccount|
      | telephonyBanking      |
      | ukTelephonyBanking    |
    And I select back buttom from view payment limit page
    When I select the continue option in the payment hub page
    And I verify the reference prepopulated in review page
    And I select the confirm option in the review payment page
    Then I should be displayed the password confirmation screen
    When I enter a correct password
    Then I am shown Payment Successful page
    And I should see share receipt option on the payment success page
