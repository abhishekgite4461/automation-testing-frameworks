@team-navigation @nga3 @rnga @stub-enrolled-typical @ios @optFeature @ff-push-notification-on @ff-push-notifications-opt-in-on
@ff-push-notifications-opt-in-bscreen-on @ff-push-notifications-settings-on @ff-push-notifications-conflict-on
@sw-push-optin-a-on @appiumOnly @genericAccount

Feature: Verify Real Time Alerts entry point
  In order to edit my app push notification settings
  As a RNGA authenticated customer
  I want to view Real Time Alerts page under Settings menu

  #MCC-29,32- AC01
  Scenario: TC_01 To verify smart alert preferences landing page
    Given I am an enrolled "current" user logged into the app
    And I navigate to settings page from home page
    When I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    When I opt in for Account alerts
    And I allow notifications for my device
    Then I should see a success confirmation
    And I should be opted in for Account alerts
    When I opt out of the Account alerts
    Then I should see a opted out confirmation
    And I should be opted out of Account alerts
