@team-statements @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-enrolled-valid @stubOnly @ff-ios-help-enhancements2-on
Feature: E2E_Help With This Transaction

  @genericAccount
  Scenario Outline: TC_001 Main Success Scenario: User views help with this transaction for posted transactions
    Given I am a current user on the home page
    And I select <type> account tile from home page
    When I select one of the posted transaction
    And I select the help with this transaction link in VTD screen
    Then I must be displayed the Unsure About Transactions Help and Info screen
    And I should see a list of below cleared transaction help options
      | fieldsInHelpPage         |
      | infoHeading              |
      | dontRecogniseTransaction |
      | expectingRefund          |
      | multipleDates            |
      | covid19Support           |
      | checkTheGuidanceHeading  |
      | disputeMessage           |
      | contactUs                |
    And I select below options to view the related helpDetails page
      | helpPageOptions          |
      | dontRecogniseTransaction |
      | expectingRefund          |
      | multipleDates            |
      | covid19Support           |
    And I select below links under guidance to view related page
      | helpPageOptions          |
      | disputeMessage           |
      | contactUs                |
    Examples:
      | type           |
      | currentAccount |
      | creditCard     |

  Scenario Outline: TC_002 Main Success Scenario: User views help with this transaction screen for pending transactions
    Given I am an enrolled "current and savings account" user logged into the app
    And I select <type> account tile from home page
    When I tap on the pending transactions accordion
    And I select <transactionType> on statements page
    And I select the help with this transaction link in VTD screen
    Then I must be displayed the Unsure About Transactions Help and Info screen
    And I should see a list of below pending transaction help options
      | fieldsInHelpPage         |
      | infoHeading              |
      | whatIsPendingTransaction |
      | cancelPendingTransaction |
      | dontRecogniseTransaction |
      | covid19Support           |
      | checkTheGuidanceHeading  |
      | disputeMessage           |
      | contactUs                |
    And I select below options to view the related helpDetails page
      | helpPageOptions          |
      | whatIsPendingTransaction |
      | cancelPendingTransaction |
      | dontRecogniseTransaction |
      | covid19Support           |
    And I select below links under guidance to view related page
      | helpPageOptions          |
      | disputeMessage           |
      | contactUs                |
    Examples:
      | type                                   | transactionType      |
      | currentAccountWithPendingTransactions  | pending Cheque       |
      | currentAccountWithPendingTransactions  | pending Direct Debit |
      | creditCardWithPendingDebitTransactions | pending Credit Card  |

  #click on weblinks to be tested manually as tap on weblinks in help details page is not working through automation as they are implemented as html hyperlinks
  @manual
  Scenario Outline: TC_003 Main Success Scenario: User views web pages from help details page of posted transactions
    Given I am an enrolled "current and savings account" user logged into the app
    And I select <type> account tile from home page
    When I select one of the posted transaction
    And I select the help with this transaction link in VTD screen
    Then I select below links in the helpDetails page to view respective web pages
      | helpDetailsPageLinks     |
      | dontRecogniseTransaction |
      | expectingRefund          |
      | covid19Support           |
    Examples:
      | type           |
      | currentAccount |
      | creditCard     |

  #click on weblinks to be tested manually as tap on weblinks in help details page is not working through automation as they are implemented as html hyperlinks
  @manual
  Scenario Outline: TC_004 Main Success Scenario: User views web pages from help details page of pending transactions
    Given I am an enrolled "current and savings account" user logged into the app
    And I select <type> account tile from home page
    When I tap on the pending transactions accordion
    And I select <transactionType> on statements page
    And I select the help with this transaction link in VTD screen
    Then I select below links in the helpDetails page to view respective web pages
      | helpDetailsPageLinks     |
      | dontRecogniseTransaction |
      | covid19Support           |
    Examples:
      | type                                   | transactionType      |
      | currentAccountWithPendingTransactions  | pending Cheque       |
      | currentAccountWithPendingTransactions  | pending Direct Debit |
      | creditCardWithPendingDebitTransactions | pending Credit Card  |
