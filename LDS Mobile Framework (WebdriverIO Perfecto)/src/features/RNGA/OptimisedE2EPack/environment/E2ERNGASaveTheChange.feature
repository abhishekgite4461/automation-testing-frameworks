@team-payments @nga3 @rnga @sw-save-the-change-cwa-on @sw-save-the-change-native-on @genericAccount @environmentOnly
Feature: Save the Change Native journey
  If I hold eligible accounts for Save the Change
  As a RNGA authenticated customer
  I should be able to register for it natively

  #PAYMOB-2209, 2319
  Scenario: TC01_User Successfully Registers for Save the Change
    Given I am a current user on the home page
    When I navigate to current account on home page
    And I select the "save the change" option in the action menu of current account
    And I have not set up Save the Change before on the eligible account
    And I am on Save the Change Homepage with the ability to swipe through all 4 instructional pages
    And I select the option Set up Save the Change
    And I select an eligible recipient saving account
    And I accept terms and conditions and select Continue on the review screen
    And I am shown a Success screen
    And I have the option to go back to accounts
    And I select the option to go back to accounts
    And I navigate to current account on home page
    And I select the "save the change" option in the action menu of current account
    And I have set up Save the Change before on the eligible account
    And I select turn OFF save the change option
    And I choose to OK option on winback dialogue
    Then I should see the save the change turn OFF success screen
