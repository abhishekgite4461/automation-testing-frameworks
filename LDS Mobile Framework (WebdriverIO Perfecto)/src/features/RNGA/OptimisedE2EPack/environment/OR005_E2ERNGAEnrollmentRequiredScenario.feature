@nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @team-release

Feature: E2E_Call us Home Page
  In order to get a quick resolution for my Query
  As a RNGA Customer
  I want to see options to contact specific customer services team

  @team-pi @stub-enrolled-youth @otherAccount
  Scenario: TC_01 Main Success Scenario : Youth Account call us page options from Support Hub
    Given I am a youth user on the home page
    When I navigate to call us home page
    Then I should not see new product enquiries tile
    And I should be able to view these call us tiles
      | callUsTiles           |
      | yourAccountsTile      |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should see security and travel emergency label
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
      | medicalAssistAbroadTab |
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I select the contact us option from the login page pre auth header
    Then I should be on pre auth call us home page overlay
    And I should be able to view these call us tiles
      | callUsTiles           |
      | personalAccountsTile  |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should be able to view close button on call us page
    And I should see security and travel emergency label
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
      | medicalAssistAbroadTab |
    And I select close button on pre auth call us home page overlay

  # MOB3-4917, 5041
  @stub-enrolled-youth @team-delex @otherAccount
  Scenario: TC_02 Youth user shouldn't be able to apply for new products
    Given I am a Youth user on the home page
    When I select apply tab from bottom navigation bar
    Then I should see can't apply error page
    When I navigate to settings page via more menu
    And I select the reset app option from the settings page
    Then I should be on the reset app Page
    When I successfully reset the app
    Then I should be on the logout page

  @team-delex @genericAccount
  #Verify the EIA journey without bi-pass (using front end app handling phone call )
  Scenario: TC_01 Log into RNGA successfully and enrol the device with a valid registered phone number
    Given I am a eia_user_for_enrollment_without_byPass user
    When I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I choose call me now option
    And I capture and enter the 4 digit pin for eia call
    And I select continue button on the enrolment confirmation page
    And I choose my security auto logout settings
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page
    When I navigate to settings page via more menu
    And I select the reset app option from the settings page
    Then I should be on the reset app Page
    When I successfully reset the app
    Then I should be on the logout page

  #TODO MQE-56
  # Due to future dated payment, other TCs getting failed. Hence making this as exhaustive
  @team-payments @stub-enrolled-valid @exhaustiveData
  Scenario: TC_03 Future Dated Credit Card Payment journey
    Given I am a current and creditcard user on the home page
    And I am on the payment hub page
    And I choose the from account as current and to account as creditCard in the payment hub page
    And I enter amount in the amount field as 1.00
    And I select calendar picker option for future date payment
    And I submit future date payment with date as 2 days after today
    When I select the confirm option in the review payment page
    Then I should see the payment successful message page
    And I navigate back to home page from payment success page
    And I navigate to logoff via more menu
    And I should be on the logout page

  @team-pi @stub-enrolled-valid @exhaustiveData @genericAccount
  #Requested for new test data, Once received we will remove the exhaustive data tag.
  #CHAT is not working in test environments so commented out the below lines
  Scenario: TC_04 Verify customer views Support Hub
    Given I am a current and creditcard user on the home page
    When I select support tab from bottom navigation bar
    Then I should be on the support hub page
    And I should see call us option
    And I validate carousel options on support hub page
      | carouselOptions     |
      | resetPassword       |
      | lostOrStolenCard    |
      | replaceCardOrPin    |
      | changeOfAddress     |
    And I select support tab from bottom navigation bar
    And I should see "CMA Tile" in the support hub home page
    And I should see "atm and branch finder tile" in the support hub home page
    And I am on the atm and branch finder page
    And I am displayed the following search options
      | searchOption       |
      | searchBranchesTile |
      | findNearbyAtmTile  |
    And I am displayed a note message stating clicking search option links take you outside the app
    When I navigate back
    And I should see "app feedback tile" in the support hub home page
    Then I should see message us option
    #When I select message us option from support hub home page
    #And I send my question "ATM" to agent or bot to answer it
    #Then I validate chat modal window and close it
    And I navigate to logoff via more menu
    And I should be on the logout page

  @stub-enrolled-valid @team-delex @outOfScope @genericAccount
  Scenario: TC_05 Verify the current account details on the account's tile displayed on homepage with already applied overdraft
    Given I am a applied overdraft current user on the home page
    And I navigate and validate the options in the current account in below table in the home page
      | accountFields      |
      | accountNumber      |
      | sortCode           |
      | accountBalance     |
      | accountInfoBalance |
    When I select the action menu of current account
    Then I should see the below options in the action menu of the current account
      | optionsInActionMenu        |
      | viewTransactionsOption     |
      | transferAndPaymentOption   |
      | sendMoneyOutsideTheUKOption|
      | standingOrderOption        |
      | directDebitOption          |
      | manageOverdraftOption      |
      | orderPaperStatementOption  |
      | chequeDepositOption        |
      | cardManagementOption       |
      | viewPendingPaymentOption   |
    And I close the action menu in the home page
    When I navigate to settings page via more menu
    And I select the reset app option from the settings page
    Then I should be on the reset app Page
    When I successfully reset the app
    Then I should be on the logout page

  @team-payments @stub-enrolled-valid @genericAccount
  Scenario Outline: TC_07 ISA Remaining allowance should be reflected on Homepage when user is moving money from cash ISA account to credit card successfully
    Given I am a current user on the home page
    And I am on the payment hub page
    And I submit the transfer with all the mandatory fields with from account as <fromAccount> and to account as <toAccount> with amount value <amount>
    When I select ISA transfer agreement checkbox and confirm option in the review payment page
    Then I should see the payment successful message page
    And I should see view transaction and make another payment option in the payment success page
    And I navigate back to home page from payment success page
    And I navigate to logoff via more menu
    And I should be on the logout page
    Examples:
      |fromAccount     | toAccount  |amount |
      |isaWithPayment  | creditCard | 1     |

  @team-payments @stub-enrolled-valid @genericAccount
  Scenario: TC_08 Transfer success scenario from ISA to non ISA
    Given I am a isa user on the home page
    And I am on the payment hub page
    And I submit the transfer with all the mandatory fields with topUpIsa from account and to account with amount value 1
    When I select ISA transfer agreement checkbox and confirm option in the review transfer page
    Then I should see view transaction and make another transfer option in the transfer success page
    And I navigate back to home page from payment success page
    And I navigate to logoff via more menu
    And I should be on the logout page

  @team-statements @stub-enrolled-valid @genericAccount @ios
  # separate feature file is created for android for all the scenarios with condensed account tile changes.
  # Will be common scenarios for both once condensed account tile is implemented in iOS
  Scenario: TC_10 View mortgage transactions, end of transaction message, view sub-accounts list and details
    Given I am a current user on the home page
    When I select mortgage account tile from home page
    Then I should be on the mortgage account statement summary
    And I should see following fields in statements list
      | fieldsInStatementsList         |
      | firstTranscationDate           |
      | firstTranscationDescription    |
      | firstTransactionRunningBalance |
      | firstTranscationAmount         |
    When I view full mortgage details
    Then I should see following fields for expanded mortgage details
      | fieldsInMortgageDetails      |
      | nextPaymentDue               |
      | remainingFullTerm            |
      | mortgageType                 |
      | mortgageHolders              |
      | propertyAddress              |
      | mortgageOpenedDate           |
      | arrearsBalance               |
      | currentBalance               |
      | currentMonthlyPayment        |
      | minimumMonthlyPayment        |
      | interestRateAsOfDate         |
      | staticDisclaimerText         |
      | currentBalanceInfoLink       |
      | monthlyPaymentsInfoLink      |
    When I select mortgageSummaryTab currentBalanceInfo link to view info
    Then I should be displayed the currentBalanceInfo as a layover
    When I select close button in info page
    And I select mortgageSummaryTab monthlyPaymentsInfo link to view info
    Then I should be displayed the monthlyPaymentInfo as a layover
    When I select close button in info page
    And I should scroll down to the end of the transactions to view mortgageEndOf message
    And I select sub account tab
    Then I should see summary of sub account with the following fields in the summary for each sub-account
      | fieldsInSubAccountsList         |
      | subAccountTypeNo                |
      | mortgageSubAccountBalance       |
      | mortgageSubAccountType          |
      | interestRateAsAtDate            |
      | subAccountCurrentMonthlyPayment |
      | nextPaymentDueOnDate            |
      | disclaimerText                  |
    And I tap on any of the sub account
    When I tap on sub account accordion in details tab
    Then I should see following fields for expanded mortgage details
      | fieldsInMortgageDetails        |
      | nextPaymentDue                 |
      | remainingFullTerm              |
      | mortgageType                   |
      | subAccountMortgageOpenedDate   |
      | paymentType                    |
      | originalAmount                 |
      | originalFullTerm               |
      | subAccountInterestRateAsAtDate |
      | currentMonthlyPaymentDetails   |
      | minimumMonthlyPayment          |
      | remainingAmount                |
      | mortgageRateHistoryTitle       |
      | mortgageRateHistoryDate        |
      | mortgageRateHistoryType        |
      | mortgageRateHistoryRate        |
      | staticDisclaimerText           |
      | currentBalanceInfoLink         |
      | monthlyPaymentsInfoLink        |
    When I select mortgageSubAccountTab currentBalanceInfo link to view info
    Then I should be displayed the currentBalanceInfo as a layover
    When I select close button in info page
    And I select mortgageSubAccountTab monthlyPaymentsInfo link to view info
    Then I should be displayed the monthlyPaymentInfo as a layover
    When I select close button in info page
    Then I should be displayed subAccountDetailsTab with full mortgage details
    When I navigate to logoff via more menu
    Then I should be on the logout page

  @team-pi @stub-enrolled-valid @genericAccount
  Scenario Outline: TC_11 Customer lands on call us page after selecting arrangement
    Given I am a current and savings and isa user on the home page
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    When I select the <account_type> tile on your accounts page
    Then I should be on the secure call page
    And I am able to view the account number and sort code
    And I am able to view the call us button
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    And I am shown the below self service options
      | serviceOption          |
      | Reset your password    |
      | Lost or stolen cards   |
      | Replacement cards PINS |
    When I navigate to logoff via more menu
    Then I should be on the logout page
    Examples:
      | account_type    |
      | Current account |
      | Savings account |
      | ISA account     |

  #Feature: E2E_Self service options for Lost or Stolen Queries, personal account , Other Banking and Internet Banking queries
  @team-pi @stub-enrolled-valid @genericAccount
  Scenario Outline: TC_12 Customer lands on call us page after selecting arrangement
    Given I am a current and savings and isa user on the home page
    And I navigate to call us home page
    When I select the <option> tile on call us home page
    Then I should be on the secure call page
    And I am able to view the call us button
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    And I am shown the below self service options
      | serviceOption          |
      | Reset your password    |
      | Lost or stolen cards   |
      | Replacement cards PINS |
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I select the contact us option from the login page pre auth header
    And I select the <option> tile on call us home page overlay
    And I am able to view the call us button
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    And I am not shown the below self service options
      | serviceOption          |
      | Reset your password    |
      | Lost or stolen cards   |
      | Replacement cards PINS |
    And I select the back option from the unsecure call us page overlay
    Then I should be on pre auth call us home page overlay
    And I select close button on pre auth call us home page overlay
    Examples:
      | option                     |
      | Internet banking           |
      | Other banking query        |

  @team-pi @stub-enrolled-typical @genericAccount
  Scenario Outline: TC_13 Customer lands on call us page after selecting arrangement
    Given I am a current and savings and isa user on the home page
    And I navigate to call us home page
    When I select the <option> tile on call us home page
    Then I should be on the secure call page
    And I am able to view the call us button
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    And I am not shown the below self service options
      | serviceOption          |
      | Reset your password    |
      | Lost or stolen cards   |
      | Replacement cards PINS |
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I select the contact us option from the login page pre auth header
    And I select the <option> tile on call us home page overlay
    And I am able to view the call us button
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    And I am not shown the below self service options
      | serviceOption          |
      | Reset your password    |
      | Lost or stolen cards   |
      | Replacement cards PINS |
    And I select the back option from the unsecure call us page overlay
    Then I should be on pre auth call us home page overlay
    And I select close button on pre auth call us home page overlay
    Examples:
      | option                     |
      | Suspected fraud            |
      | Emergency cash abroad      |

     #MORE-29
#  Redesigned welcome screen
#  In order to have a better user experience
#  As a RNGA Customer
#  I want to see a re-designed welcome page
  @team-login @mbnaSuite @stub-tc-not-accepted @unEnrolled
  Scenario: TC_14 Main Success Scenario : User should be able to choose contact us from the header
    Given I am a current user
    When I select the contact us option from the welcome page pre auth header
    Then I should be on pre auth call us home page overlay
    When I select the close option from the unsecure call us page overlay
    And I select the legal info option from the welcome page
    Then I am on the pre auth legal information page overlay

  @team-delex @stub-valid @unEnrolled
  Scenario Outline: TC_15 Unenrolled Customer chooses to view Third party acknowledgements
    Given I am a current user
    And I select the legal info option from the welcome page
    When I select <legalOption> from the legal information page overlay
    Then I should be on the <legalOption> legal info item page overlay
    And I navigate back to legal info home page
    When I select <legalOption> from the legal information page overlay
    Then I should be on the <legalOption> legal info item page overlay
    When I select the dismiss button from the legal info item page overlay
    Then I should be on the welcome page
    Examples:
      | legalOption                  |
      | legal and privacy            |
      | cookie use and permissions   |
      | third party acknowledgements |

  @team-pi @stub-enrolled-typical @genericAccount
  Scenario: TC_16 Non-Youth Account call us page options from Support Hub
    Given I am an enrolled "current" user logged into the app
    When I navigate to call us home page
    And I should be able to view these call us tiles
      | callUsTiles           |
      | yourAccountsTile      |
      | newProductEnquireTile |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should be able to view native back button
    And I should see security and travel emergency label
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
      | medicalAssistAbroadTab |
    And I navigate to logoff via more menu
    Then I should be on the logout page
    When I select the contact us option from the login page pre auth header
    Then I should be on pre auth call us home page overlay
    And I should be able to view these call us tiles
      | callUsTiles           |
      | personalAccountsTile  |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should be able to view close button on call us page
    And I should see security and travel emergency label
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
      | medicalAssistAbroadTab |
    And I select close button on pre auth call us home page overlay

# TODO MOB3-6438 Need to modify the feature file to run against the envionment since STUB does not have relaunch after enrolment
  #MOB3-2216 #MQE-11
  @environmentOnly @team-login @mbnaSuite @stub-enrolled-typical @genericAccount
  Scenario: TC_17 Entering wrong MI on light logon page, validate that MI is masked and entering correct MI next time
    Given I am an enrolled "current" user logged into the app
    And I relaunch the app
    And I am a current user
    When I enter 2 characters of MI
    Then I should see the MI information is masked
    When I enter incorrect MI
    Then I should see incorrect MI error message in re-enter MI page
    When I enter correct MI in re-enter MI page
    And I accept interstitial during logon
    Then I should be on the home page

  @mbnaSuite @team-delex @stub-enrolled-typical @genericAccount
  Scenario: TC_01 To verify Reset app functionality
    Given I am a current user on the home page
    When I navigate to settings page via more menu
    And I select the reset app option from the settings page
    Then I should see information about reset app and a warning message
    When I navigate back
    And I select the reset app option from the settings page
    And I select reset app button from the reset app page
    Then I select cancel button from the reset app confirmation pop up
    When I select reset app button from the reset app page
    And I select confirm button from the reset app confirmation pop up
    Then I should be on the logout page with reset app complete message
    And I select the contact us option from the logoff page pre auth header
    And I should be on pre auth call us home page overlay
    And I select close button on pre auth call us home page overlay
    And I select back to logon button on the logout page
    And I should be on the environment selector page

  @team-aov @stub-enrolled-valid @genericAccount
  #This feature has been implemented on LDS, HFX and BOS brand only.
  Scenario: TC_18 Verify Coronavirus support Entry points for Help & Support page
    Given I am a current user on the home page
    When I am shown the Covid19 tile on home page
    And I select Find out how button on Covid19 tile
    Then I should be on the support hub page
    And I should see message us option
    And I should see call us option
    And I should see and validate please confirm your contact details button
    And I validate covid-19 and support links under the useful links section
      | usefulLinks                   |
      | mortgagePaymentHolidaysLink   |
      | creditcardPaymentHolidaysLink |
      | loanPaymentHolidaysLink       |
      | overdraftChargesLink          |
      | travelAndDisruptionCoverLink  |
