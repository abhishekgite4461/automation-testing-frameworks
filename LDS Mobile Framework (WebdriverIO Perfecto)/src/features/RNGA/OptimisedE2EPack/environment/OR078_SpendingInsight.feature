@team-statements @nga3 @rnga @stub-enrolled-valid @sw-spendinginsights-android-on @ff-spendinginsights-enabled-on
@sw-spendinginsights-ios-on @environmentOnly @primary @optFeature @genericAccount
Feature: Spending Insights
  As a NGA Retail customer,
  I want to be able to access spending insights
  So that I can understand my spending month on month

  #STAMOB-1420,1419, STAMOB-1913
  Scenario: TC_001_View Spending Insights option in contextual menu on home screen & statements screen
    Given I am a current user on the home page
    And I navigate to current account on home page
    When I select the action menu of current account
    Then I should see viewSpendingInsights option in the action menu
    And I close the action menu in the home page
    When I select current account tile from home page
    And I select the viewSpendingInsights option from the action menu in statements page
    Then I should be displayed spendingInsights web view page
    When I tap on back button in spending insights page
    Then I should be navigated back to statements page
