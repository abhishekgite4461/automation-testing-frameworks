@team-pi @nga3 @rnga @manual

  # PIDI-500 Android Support hub AB test (useful links)
  # PIDI-2384 & PIDI-2381 New variations in the ab test for call us button position
  # adobe activity - "Support Hub - Call US Button Location Covid-19" in QA Workspace
  # These scenarios are manual only since there is an interaction between 3rd party provider Adobe Target.

Feature: AB Test - call us button position
  As a product owner
  I want to test best position of call us button with live customers
  So that I can have the best entry points for call us in order to reduce reliance on calls from customers.

  Background:
    Given I am an enrolled "current" user logged into the app

  @primary
  Scenario:AC01 - Test Variance A (control)
    When I select support tab from bottom navigation bar
    Then I should see the call us button in the main panel

  @primary
  Scenario:AC02 - Test Variance B
    When I select support tab from bottom navigation bar
    Then I should see the chat entry point as a full width button and the call us entry point in the 'useful help' section below

  @primary
  Scenario:AC03 - Test Variance C
    When I select support tab from bottom navigation bar
    Then I should not see the call us button in the support hub.

  @secondary
  Scenario:AC04 - Default test variance
    When I select support tab from bottom navigation bar
    Then I should see the default view (call us button in the main panel with chat us button)
