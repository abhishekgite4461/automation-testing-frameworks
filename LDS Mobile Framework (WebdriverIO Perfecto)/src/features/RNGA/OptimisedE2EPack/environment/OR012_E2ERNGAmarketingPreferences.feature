@team-pi @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature

  # Note : For MBNA a "current" user will return a "credit card" user

Feature: E2E_selecting marketing consents from native entry page
  As a customer
  I want to be able to access marketing preferences
  So that I can review and update how I am contacted about Lloyds products

  @ios @stub-enrolled-typical @mbnaSuite @genericAccount
  Scenario: TC_01 Updating my marketing preferences and winback after using native back button
    Given I am a current user on the home page
    When I navigate to marketing preferences hub via more menu
    And I toggle the postPrefs marketing option
    And I navigate back
    And I'm presented with a Winback option
    And I opt to stay on the marketing preferences page
    And I'm on the marketing hub page
    And I navigate back
    And I'm presented with a Winback option
    And I opt to leave the marketing preferences page
    Then I should be on the your personal details page
    When I select marketing preferences link
    And I'm on the marketing hub page
    And I opt in to all the marketing prefernces
    And I submit my preferences
    Then I'm presented with a Success message

  @android @stub-enrolled-typical @mbnaSuite @genericAccount
  Scenario: TC_02 Updating my marketing preferences and winback after using native/device back but§ton
    Given I am a current user on the home page
    When I navigate to marketing preferences hub via more menu
    And I toggle the postPrefs marketing option
    And I navigate back
    And I'm presented with a Winback option
    And I opt to stay on the marketing preferences page
    And I'm on the marketing hub page
    And I select back key on the device
    And I'm presented with a Winback option
    And I opt to stay on the marketing preferences page
    And I'm on the marketing hub page
    And I navigate back
    And I'm presented with a Winback option
    And I opt to leave the marketing preferences page
    Then I should be on the your personal details page
    When I select marketing preferences link
    And I'm on the marketing hub page
    And I opt in to all the marketing prefernces
    And I submit my preferences
    Then I'm presented with a Success message

  # TODO: MQE-526 Remove @stubOnly tag once datafly is integrated
  @ios @stub-gdpr-coservicing-pref-notset @bos @hfx @otherAccount @stubOnly
  Scenario: TC_03 Updating my co-servicing marketing preferences and winback after using native back button
    Given I am a nil balance coserviced passbook user on the home page
    When I navigate to profile page via more menu
    And The marketing preference links for both primary and secondary brands should be visible
    And I select the marketing preferences link for the primary brand
    And I toggle on the emailPrefs marketing option
    And I navigate back
    And I'm presented with a Winback option
    And I opt to stay on the marketing preferences page
    Then I must remain on the primary brand marketing consent page
    When I navigate back
    And I'm presented with a Winback option
    And I opt to leave the marketing preferences page
    Then I should be on the your personal details page
    When I select the marketing preferences link for the primary brand
    And I opt in to all the marketing preferences for the primary brand
    And I select continue to navigate to the secondary marketing preferences screen
    And I opt in to all the marketing preferences for the secondary brand
    And I submit my preferences
    Then I'm presented with a Success message

  # TODO: MQE-526 Remove @stubOnly tag once datafly is integrated
  @android @stub-gdpr-coservicing-pref-notset @bos @hfx @otherAccount @stubOnly
  Scenario: TC_04 Updating my co-servicing marketing preferences and winback after using native back button
    Given I am a nil balance coserviced passbook user on the home page
    When I navigate to profile page via more menu
    And The marketing preference links for both primary and secondary brands should be visible
    And I select the marketing preferences link for the primary brand
    And I toggle on the internetPrefs marketing option
    And I navigate back
    And I'm presented with a Winback option
    And I opt to stay on the marketing preferences page
    Then I must remain on the primary brand marketing consent page
    And I select back key on the device
    And I'm presented with a Winback option
    And I opt to stay on the marketing preferences page
    And I must remain on the primary brand marketing consent page
    When I navigate back
    And I'm presented with a Winback option
    And I opt to leave the marketing preferences page
    Then I should be on the your personal details page
    When I select the marketing preferences link for the primary brand
    And I opt in to all the marketing preferences for the primary brand
    And I select continue to navigate to the secondary marketing preferences screen
    And I opt in to all the marketing preferences for the secondary brand
    And I submit my preferences
    Then I'm presented with a Success message
