@team-navigation @nga3 @rnga @stub-enrolled-typical @optFeature @ff-mdm-push-notifications-setting-enabled-on
@sw-push-notification-on @appiumOnly @genericAccount @android

# MCC-598, MCC-354
Feature: Multiple device notification from settings
  In order to receive push on the devices of my choosing
  As an RNGA customer
  I want to manage which of my devices are enabled for push

  Preconditions
  Customer is fully enrolled
  Opt-in Version A Control-Android is ON

  Background:
    Given I am an enrolled "current" user logged into the app
    And I navigate to logoff via more menu
    Then I should be on the logout page
    When I have turned off device push
    And I am a current user
    And I enter correct MI
    Then I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the notifications page
    When I can see an option to enable notifications
    And I select Go to Device Settings Button
    Then I should be taken to the OS device settings

  Scenario: TC_001 Android Customer’s current device is not push enabled- Happy path
    When I turn on push at device settings level
    And I navigate back to the app
    Then I should now be able to interact with the alert toggles
    And The alert toggles should be shown in the correct position according to my current status

  Scenario: TC_002 Android Customer’s current device is not push enabled- Doesn’t correctly enable push
    When I do not turn on push at device settings level
    And I navigate back to the app
    Then I should see overlay advising that I did not complete the correct actions
    When I select ok on the notifications not activated overlay
    Then I should not be able to interact with the alert toggles
    And I should see instructions on how to enable notifications
    And The alert toggles should be shown in the correct position according to my current status
