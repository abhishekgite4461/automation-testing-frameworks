@team-payments @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @sw-enable-mortgage-overpayment-on @optFeature @genericAccount
@environmentOnly
Feature: Mortgage Overpayments
  As an rnga customer with a eligible mortgage account
  I want to see "Make overpayment" entry point navigates to native payment hub
  So that I can carry out overpayment on my mortgage accounts

  Background:
    Given I am an enrolled "mortgage" user logged into the app

  Scenario Outline: TC_001 - Successfully making a mortgage overpayment
    When I navigate to mortgage account on home page
    And I select the "make an overpayment" option in the action menu of mortgage account
    Then I should see the to field pre-populated with mortgage account
    When I enter <amount> in the amount field in the payment hub page
    And I select the continue option in the payment hub page
    And I select the confirm option in the review payment page
    Then I should see a payment confirmed screen
    And I should see an option make another transfer
    And I should see an option to view transactions
    When I select dismiss modal using close button
    Then I navigate to home page
    And I select mortgage account tile from home page
    When I select the make an overpayment option from the action menu in statements page
    Then I should see the to field pre-populated with mortgage account
    When I enter <amount> in the amount field in the payment hub page
    And I select the continue option in the payment hub page
    And I select the confirm option in the review payment page
    Then I should see a payment confirmed screen
    And I should see an option make another transfer
    And I should see an option to view transactions
    Examples:
      | amount |
      | 5      |

  Scenario Outline: TC_002 - Successfully making a mortgage overpayment with sub-account
    When I navigate to mortgage account on home page
    And I select the "make an overpayment" option in the action menu of mortgage account
    Then I should see the to field pre-populated with mortgage account
    And I enter <amount> in the amount field in the payment hub page
    When I enable the toggle to pay a sub-account
    And I select on the toggle to pay a sub-account
    And I select the continue option in the payment hub page
    And I select the confirm option in the review payment page
    Then I should see a payment confirmed screen
    And I should see an option make another transfer
    And I should see an option to view transactions
    When I select dismiss modal using close button
    And I navigate to mortgage account on home page
    And I select mortgage account tile from home page
    And I select the make an overpayment option from the action menu in statements page
    Then I should see the to field pre-populated with mortgage account
    And I enter <amount> in the amount field in the payment hub page
    When I enable the toggle to pay a sub-account
    And I select on the toggle to pay a sub-account
    And I select the continue option in the payment hub page
    And I select the confirm option in the review payment page
    Then I should see a payment confirmed screen
    And I should see an option make another transfer
    And I should see an option to view transactions
    Examples:
      | amount |
      | 5      |
