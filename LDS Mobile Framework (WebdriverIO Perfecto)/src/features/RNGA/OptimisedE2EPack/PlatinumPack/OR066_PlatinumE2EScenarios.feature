@team-release @team-delex @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @environmentOnly @genericAccount @stub-enrolled-valid @platinum
Feature:Device Enrolment for retail customer with Device enrolment switch ON

  # Note : For MBNA a "current" user will return a "credit card" user

  Scenario: TC_01 Log into RNGA successfully and enrol the device with a valid registered phone number
    Given I am a current user
    When I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I choose call me now option
    And I capture and enter the 4 digit pin for eia call
    And I select continue button on the enrolment confirmation page
    And I choose my security auto logout settings
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page
    When I navigate to settings page via more menu
    And I select the reset app option from the settings page
    Then I should see information about reset app and a warning message
    When I select reset app button from the reset app page
    And I select confirm button from the reset app confirmation pop up
    Then I should be on the logout page with reset app complete message

  Scenario: TC_02 Verify Home page , account tiles and their action menu
    Given I am an enrolled "current" user logged into the app
    And bottom navigation bar is displayed
    Then I should see the below options in the bottom nav
      |optionsInBottomNav|
      | home             |
      | apply            |
      | payAndTransfer   |
      | support          |
      | more             |
    And I navigate and validate the options in the current account in below table in the home page
      | accountFields      |
      | accountTileLogo    |
      | accountNumber      |
      | sortCode           |
      | accountBalance     |
    When I select the action menu of current account
    Then I should see the below options in the action menu of the current account
      | optionsInActionMenu        |
      | viewTransactionsOption     |
      | transferAndPaymentOption   |
      | viewUpcomingPayment        |
      | sendMoneyOutsideTheUKOption|
      | chequeDepositOption        |
      | standingOrderAndDirectDebitOption |
      | changeAccountTypeOption    |
      | applyForOverdraftOption    |
      | orderPaperStatementOption  |
      | cardManagementOption       |
      | sendBankDetailsOption      |
    And I close the action menu in the home page
    And I navigate and validate the options in the saving account in below table in the home page
      | accountFields            |
      | accountTileLogo          |
      | accountNumber            |
      | sortCode                 |
      | accountBalance           |
    When I select the action menu of saving account
    Then I should see the below options in the action menu of the saving account
      | optionsInActionMenu           |
      | viewTransactionsOption        |
      | transferAndPaymentOption      |
      | chequeDepositOption           |
      | orderPaperStatementOption     |
      | cardManagementOption          |
      | renewYourSavingsAccountOption |
      #| viewPendingPaymentOption     |
      | viewInterestRateOption        |
    When I select renewYourSavingsAccountOption from contextual menu
    Then I should be displayed the Renew your savings account webview
    When I select home tab from bottom navigation bar
    And I navigate and validate the options in the previousIsa account in below table in the home page
      | accountFields      |
      | accountTileLogo    |
      | accountNumber      |
      | sortCode           |
      | accountBalance     |
    And I select the action menu of previousIsa account
    Then I should see the below options in the action menu of the previousIsa account
      | optionsInActionMenu           |
      | viewTransactionsOption        |
      | transferAndPaymentOption      |
      | chequeDepositOption           |
      | orderPaperStatementOption     |
      | cardManagementOption          |
      | renewYourSavingsAccountOption |
      | viewInterestRateOption        |
    When I select renewYourSavingsAccountOption from contextual menu
    Then I should be displayed the Renew your savings account webview
    When I select home tab from bottom navigation bar
    And I navigate and validate the options in the htbIsa account in below table in the home page
      | accountFields         |
      | accountTileLogo       |
      | accountNumber         |
      | sortCode              |
      | accountBalance        |
    And I select the action menu of htbIsa account
    Then I should see the below options in the action menu of the htbIsa account
      | optionsInActionMenu       |
      | viewTransactionsOption    |
      | transferAndPaymentOption  |
      | orderPaperStatementOption |
      | cardManagementOption      |
      #| viewPendingPaymentOption  |
      | viewInterestRateOption    |
    And I close the action menu in the home page
    When I select home tab from bottom navigation bar
    And I navigate to settings page via more menu
    And I select the reset app option from the settings page
    Then I should be on the reset app Page
    When I successfully reset the app
    Then I should be on the logout page

  Scenario: TC_03 Verify success transfer journey and view transaction
    Given I am a current user on the home page
    And I am on the payment hub page
    When I submit transfer with from account current and to account saving with amount 1.00
    And I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page
    When I select the view transaction option in the transfer success page
    And I should see the - £1.00 in the current month statement of the remitting account
    And I navigate to home page
    Then I should see the corresponding 1.00 deducted in the remitter current and recipient accounts saving

#  We can execute only in SIT02 environment since liberty service is not availabe in other environment
  Scenario Outline: TC_04 Adding New UK External beneficiary, Payment, Dynamic Search and deletion of the Recipient for a current user
    Given I am a current user on the home page
    When I navigate to add new recipient option using payment hub
    And I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    And I am on the Make Payment screen with newly added beneficiary selected
    And I enter payment amount greater than available balance in the remitter account in the payment hub page
    And I should be able to close the insufficient money in your account error message in the payment hub page
    And I clear the amount field in the payment hub page
    And I am on the Make Payment screen with newly added beneficiary selected
    And I enter <amount> in the payment amount field in the payment hub page
    And I select Continue button on payment hub page
    And I select the confirm option in the review payment page
    And I should be prompted to enter my password
    And I enter a correct password
    Then I am shown Payment Successful page
    When I navigate back to home page from payment success page
    And I choose the recipient account on the payment hub page
    And I search recipient account <recipientName> on payment hub page to delete
    And I delete the selected recipient
    Then I am asked for confirmation if I want to delete the recipient
    When I select to confirm delete a recipient
    And I navigate back to home page after deleting the recipient
    And I navigate to logoff via more menu
    Then I should be on the logout page
    Examples:
      | sortCode | accountNumber | recipientName | amount |
      | 400202   | 74374163      | Adam Anderson | 1      |

  @exhaustiveData
  Scenario: TC_05 Display 'When' field on payment confirmed page during Future Dated Credit Card Payment
    Given I am a current and creditcard user on the home page
    And I am on the payment hub page
    And I choose the from account as current and to account as creditCard in the payment hub page
    And I enter amount in the amount field as 1.00
    And I select calendar picker option for future date payment
    And I submit future date payment with date as 2 days after today
    When I select the confirm option in the review payment page
    Then I should see the payment successful message page
    And I am shown 'When' the payment will be made on payment success page
    And I navigate back to home page from payment success page

  Scenario: TC_06 Main Success: Customer accesses the more menu
    Given I am a current user on the home page
    And I select more tab from bottom navigation bar
    And I should see the below options in the more menu
      | optionsInMorePage  |
      | yourProfile        |
      | inbox              |
      | settings           |
      | cardManagement     |
      | depositCheque      |
      | viewDepositHistory |
      | logout             |
    And I navigate to logoff via more menu
    And I should be on the logout page
    When I select the contact us option from the logoff page pre auth header
    Then I should be on pre auth call us home page overlay

  Scenario: TC_07 Non-Youth Account call us page options from Support Hub
    Given I am a current user on the home page
    When I navigate to call us home page
    And I should be able to view these call us tiles
      | callUsTiles           |
      | yourAccountsTile      |
      | newProductEnquireTile |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should be able to view native back button
    And I should see security and travel emergency label
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
      | medicalAssistAbroadTab |
    And I navigate to logoff via more menu
    Then I should be on the logout page
    When I select the contact us option from the login page pre auth header
    Then I should be on pre auth call us home page overlay
    And I should be able to view these call us tiles
      | callUsTiles           |
      | personalAccountsTile  |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should be able to view close button on call us page
    And I should see security and travel emergency label
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
      | medicalAssistAbroadTab |
    And I select close button on pre auth call us home page overlay

#  Need to update the options along with the feature team.
  Scenario: TC_09 To verify static menu on the settings page
    Given I am a current user on the home page
    When I navigate to settings page from home page
    Then I should see the below options in the settings page
      | optionsInSettingsPage           |
      | personalDetailsButton           |
      | howWeContactYou                 |
      | securitySettingsButton          |
      | payAContactButton               |
      | resetAppButton                  |
      | legalInfo                       |
    When I select security settings from settings menu
    Then I should see the below options in the security settings page
      | securitySettingsOptions               |
      | securityDetailsUserIDLabel            |
      | securityDetailsDeviceTypeLabel        |
      | securityDetailsDeviceNameLabel        |
      | securityDetailsAppVersionLabel        |
      | securityDetailsAutoLogOffButton       |
      | securityDetailsForgottenPasswordButton|
    When I select the autoLog off settings option from the security settings page
    Then I should be on the autoLog off settings page

#  Need to update the scenario with change phone number
  Scenario: TC_10 The email has been updated successfully
    Given I am a current user on the home page
    And I have submitted a request to change my email address
    When The request has been successfully processed
    Then I should be shown a success message with prompt to close the message
    When I dismiss the prompt
    Then I should be shown updated personal details screen
    When I navigate to logoff via more menu
    Then I should be on the logout page

  Scenario: TC_11 Customer lands on Product Hub page
    Given I am a current user on the home page
    When I select apply tab from bottom navigation bar
    And I close the opened Featured option from product hub menu
    Then I should see the product hub home page with product categories
      | productCategoryLabels |
      | featured              |
      | currentAccounts       |
      | loans                 |
      | creditCards           |
      | savings               |
      | insurance             |
      | mortgages             |

  Scenario: TC_12 To verify Reset app functionality
    Given I am a current user on the home page
    When I navigate to settings page via more menu
    And I select the reset app option from the settings page
    Then I should see information about reset app and a warning message
    When I navigate back
    And I select the reset app option from the settings page
    And I select reset app button from the reset app page
    Then I select cancel button from the reset app confirmation pop up
    When I select reset app button from the reset app page
    And I select confirm button from the reset app confirmation pop up
    Then I should be on the logout page with reset app complete message
    And I select the contact us option from the logoff page pre auth header
    And I should be on pre auth call us home page heaoverlay
    And I select close button on pre auth call us home page overlay
    And I select back to logon button on the logout page
    And I should be on the environment selector page

  @manual
  Scenario: TC_13 Relaunch app immediately after opted in and authenticate with fingerprint
    Given I am a fingerprints registered RNGA-user
    And I have opted-in for fingerprint authentication
    When I relaunch the app
    And I start the Login journey
    Then I should be prompted to Login via fingerprint
    When I use a wrong fingerprint for authentication
    Then I should not be authenticated or given access to my account
    When I use correct fingerprint I should be able to login
    Then I should be on the home page
    When I submit the password reset request
    And I log out of the NGA app in the password reset journey
    Then I must be authenticated only by MI to login to NGA when i relaunch the app

  @manual
  Scenario: TC_14 Add international recipient
    Given I am a current user on the home page
    And I select current account tile from home page
    When I select the "international payment" option in the action menu of current account
    Then I should be on the international payment webview page
    When I give all inputs in adding new international recipient
    Then it should show international recipient success screen

  @manual
  Scenario: TC_15 Pay international recipient
    Given I am a current user on the home page
    And I select current account tile from home page
    When I select the "international payment" option in the action menu of current account
    Then I should be on the international payment webview page
    When I make payment for international recipient
    Then it should show international recipient payment success screen
