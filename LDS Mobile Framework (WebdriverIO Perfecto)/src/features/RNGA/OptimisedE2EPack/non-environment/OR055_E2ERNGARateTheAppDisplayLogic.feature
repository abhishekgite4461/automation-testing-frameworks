@team-login @nga3 @rnga @optFeature @primary @manual
#Requires several random login to rate, can't be automated
Feature: Display logic for rate the application pop up
  As a RNGA User
  I want the ability to rate the NGA App
  So that I can share my comments about the App

#----------------------------------------------iOS Logic-----------------------------------------------------------------------------#

  #To ensure there is even distribution of ratings, it has been agreed to randomly assign customer to 7 login baskets
  #which will be starting from 10th login and going up to 40th login

  @ios
  Scenario Outline: TC_01 Main Success TC_001_ User is displayed the rating prompt for the 1st time
    Given I am an enrolled "current" user logged into the app
    And this is the login attempt number <number>
    When I make a successful payment
    Then I am displayed the option to rate the app or defer it
    Examples:
      | number |
      | 10     |
      | 15     |
      | 20     |
      | 25     |
      | 30     |
      | 35     |
      | 40     |

#----------------------------------------------Android Logic--------------------------------------------------------------------------#

  #First time the rating prompt is shown on a successful Payment when Login counter hits 10th login OR Payments counter hits 5th payment
  #Thereafter, The rating prompt is shown on a successful Payment when Login counter hits 100th login OR Payments counter hits 20th payment

  @android
  Scenario Outline: TC_02 Main Success TC_001_User is displayed the rating prompt when login attempt counter requirements are met
    Given I am an enrolled "current" user logged into the app
    And this is the login attempt number <number>
    When I make a successful payment
    Then I am displayed the option to rate the app or defer it
    Examples:
      | number |
      | 10     |
      | 100    |

  @android
  Scenario Outline: TC_03 Main Success TC_002_ User is displayed the rating prompt when payment attempt counter requirements are met
    Given I am an enrolled "current" user logged into the app
    And this is the payment attempt number <number>
    When I make a successful payment
    Then I am displayed the option to rate the app or defer it
    Examples:
      | number |
      | 5      |
      | 20     |
