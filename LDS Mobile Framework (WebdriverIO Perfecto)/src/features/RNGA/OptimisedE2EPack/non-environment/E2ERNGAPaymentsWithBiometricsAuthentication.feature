@rnga @nga3 @team-payments @sw-enrolment-on @sw-light-logon-on @sca @sw-biometric-payment-on @stub-enrolled-valid @manual
Feature: Biometrics In RNGA Payments
  In order set up a new payee / amend a standing order
  As a RNGA customer
  I want to be able to authenticate using biometric

  Background:
    Given Biometric Payments switch is ON
    And user is opted into using biometric

  Scenario Outline: TC_001_ Main Success: User adds new payee using Bio successfully (Immediate payment)
    Given I am an enrolled user logged into the app
    And I am on payments hub
    And I have chose to add a new payee
    And I have entered my payment details
    And I select Continue on review screen
    When I authenticate with <Biometric>
    And I receive success message
    Then I am shown payment & payee have been added successfully
    Examples:
      | TouchID |
      | FaceID  |

  Scenario Outline: TC_002_ Main Success: User adds new payee using Bio successfully (standing order payment)
    Given I am an enrolled user logged into the app
    And I am on payments hub
    And I have chose to add a new payee
    And I have entered my payment details
    And I have selected standing orders
    And I select Continue on review screen
    When I authenticate with <Biometric>
    And I receive success message
    Then I am shown payment & payee have been added successfully
    Examples:
      | TouchID |
      | FaceID  |

  Scenario Outline: TC_003_ Main Success: User amends standing order using Bio successfully (standing order payment)
    Given I am an enrolled user logged into the app
    And I am on the scheduled payments hub
    And I have chosen to amend a standing order
    And I have edited my payment details
    And I select Continue on review screen
    When I authenticate with <Biometric>
    And I receive success message
    Then I am shown standing order amended successfully
    Examples:
      | TouchID |
      | FaceID  |
