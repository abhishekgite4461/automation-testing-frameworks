@team-statements @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-enrolled-valid @noExistingData @stubOnly @ios
  # separate feature file is created for android for all the scenarios with condensed account tile changes.
  # Will be common scenarios for both once condensed account tile is implemented in iOS
Feature: Home Insurance Policy Details
  In order to be better informed about my home insurance policy
  As a RNGA Retail customer,
  I want to view my home insurance policy details in RNGA

  #MPT-3026 Don't have data for HI - need to request data - takes long time to get this data. exhaustive data
  #DPL-1429
  Scenario: TC_01 User views their insurance policy on the homepage
    Given I am a home_insurance user on the home page
    When I select homeInsurance account tile from home page
    Then I should see account details with the following fields
      |fieldsInAccountTile            |
      |homeInsuranceStaticProductTitle|
      |policyCoverType                |
      |insuredAddress                 |
      |periodOfCover                  |
    And I should be displayed policy details with the following fields
      |fieldsInDetails              |
      |policyNumber                 |
      |enquiriesPhoneNumber         |
      |policyUpdatedDate            |
      |policyHolders                |
      |insuredPropertyAddress       |
      |whatsIncludedInPolicyLink    |
      |optionalCoverYouCouldAddLink |
      |costAndPaymentDetailsLink    |
      |policyScheduleNote           |
      |termsDisclaimer              |
    When I navigate to logoff via more menu
    Then I should be on the logout page
