@team-payments @rnga @nga3 @environmentOnly @sw-enrolment-on @sw-light-logon-on @optFeature

Feature: Make Transfer between non-ISA account
  As a Retail NGA user
  I want to transfer money between isa accounts
  So that I can optimise my financial position across my accounts

  # Can't make more than one deposit for HTB ISA
  @exhaustiveData @stub-enrolled-valid
  Scenario: TC_01 Top up Help to Buy ISA account using non-ISA account(HC-139) where remaining allowance is >0
    Given I am a htb isa user on the home page
    And I am on the payment hub page
    And I select the to account as htbIsa in the payment hub page
    When I submit with amount 1 in the payment hub page
    Then I should see the htbIsa warning message in the review transfer page
    When I select the confirm option in the review transfer page
    Then I should see view transaction and make another transfer option in the transfer success page

  @genericAccount @stub-isa-transfer-blocked
  # MPT-3588 Test Data challenge
  Scenario Outline: TC_02 Block users from transferring money from Non-ISA to previous year ISA account where remaining allowance is equals to 0
    Given I am a current user on the home page
    And I am on the payment hub page
    When I submit the transfer with all the mandatory fields with from account as <fromAccount> and to account as <toAccount> with amount value 1
    Then I should see already subscribed to isa error message in transfer failure page
    Examples:
      |fromAccount       |toAccount      |
      |current           |previousIsa    |

  @noExistingData @stub-isa-transfer-blocked
  # MPT-3588 Test Data challenge
  Scenario Outline: TC_03 Validate ISA to ISA transfer when user has already funded ISA in current tax year
    Given I am a current year funded cash isa user on the home page
    And I am on the payment hub page
    And I submit the isa transfer with all the mandatory fields with from account as <fromAccount> and to account as <toAccount> with amount value 1
    When I select ISA transfer agreement checkbox and confirm option in the review transfer page
    Then I should see view transaction and make another transfer option in the transfer success page
    Examples:
      |fromAccount       |toAccount         |
      |previousIsa       | currentIsa       |
      |previousIsa       | previousIsaFunded|

  @genericAccount @stub-isa-transfer-blocked
  # MPT-3588 Test Data challenge
  Scenario Outline: TC_04 BLOCK Customers from Funding Two Cash or Fixed ISAs when the current year isa is already funded
    Given I am a current user on the home page
    And I am on the payment hub page
    When I submit the transfer with all the mandatory fields with from account as <fromAccount> and to account as <toAccount> with amount value 1
    Then I should see already subscribed to isa error message in transfer failure page
    Examples:
      |fromAccount       |toAccount      |
      |currentIsaFunded  | currentIsa    |
      |currentIsaFunded  | previousIsa   |
      |previousIsaFunded | previousIsa   |
