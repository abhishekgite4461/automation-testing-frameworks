@team-statements @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-enrolled-valid @stubOnly @sw-hcc-android-on
@sw-hcc-ios-on @sw-totalpending-android-on @ff-exposed-pendingtransactions-on
Feature: E2E Pending credit and debit card transaction details
  In order to view the statements for the nominated credit card
  As a NGA authenticated customer with credit card
  I want to view my credit and debit card pending transactions

  # Note : For MBNA a "current" user will return a "credit card" user
  @genericAccount @android
  Scenario: TC_01 Statements Viewing the pending debit card and cheque transactions
    Given I am a current user on the home page
    When I select current Account With Pending Debit Transactions account tile from home page
    Then I should be on the allTab of statements page
    And I should not see the pending transactions accordion
    And I should see exposed pending transactions ordered below
      | detail                                     |
      | unclearedChequesHeader                     |
      | totalChequesPending                        |
      | chequeNoteText                             |
      | exposedPendingChequeTransactionDescription |
      | exposedPendingChequeTransactionAmount      |
      | chevronOnPendingChequeLine                 |
      | pendingHeader                              |
      | totalPending                               |
      | exposedPendingDebitTransactionDescription  |
      | exposedPendingDebitTransactionAmount       |
      | chevronOnPendingTransactionLine            |
      | accountBalanceHeader                       |
    When I select exposed Pending Cheque on statements page
    Then I should see pending transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | vtdAmount               |
      | notYetAvailable         |
      | chequesText             |
      | payeeField              |
      | depositedOn             |
      | availableBy             |
      | creditDisclaimerText    |
      | unsureTransactionButton |
    When I close the pending transactions VTD
    Then I should be on the allTab of statements page
    When I select exposed Pending Direct Debit on statements page
    Then I should see pending transaction details for that transaction with the following fields
      | fieldsInDetailScreen    |
      | vtdCloseButton          |
      | vtdAmount               |
      | debitCardText           |
      | payeeField              |
      | dateOfTransaction       |
      | debitDisclaimerText     |
      | unsureTransactionButton |
    When I select the help with this transaction link in VTD screen
    Then I must be displayed the Unsure About Transactions Help and Info screen
    When I select to go back
    Then I should be displayed VTD page of that transaction
    When I close the pending transactions VTD
    And I swipe to see previous month transactions on the account statement page
    Then I should not see the pending transactions accordion
    And I should see exposed pending transactions ordered below
      | detail                                     |
      | unclearedChequesHeader                     |
      | totalChequesPending                        |
      | chequeNoteText                             |
      | exposedPendingChequeTransactionDescription |
      | exposedPendingChequeTransactionAmount      |
      | chevronOnPendingChequeLine                 |
      | pendingHeader                              |
      | totalPending                               |
      | exposedPendingDebitTransactionDescription  |
      | exposedPendingDebitTransactionAmount       |
      | chevronOnPendingTransactionLine            |
      | accountBalanceHeader                       |
    And I navigate to logoff via more menu
    And I should be on the logout page

  #exposed pending is not yet implemented for iOS
  @genericAccount @ios
  Scenario: TC_02 Statements Viewing the pending debit card and cheque transactions
    Given I am a current user on the home page
    When I select current Account With Pending Debit Transactions account tile from home page
    Then I should be on the allTab of statements page
    When I tap on the pending transactions accordion
    Then I should see pending Debit transaction with below detail in statements
      | detail                        |
      | pendingLabel                  |
      | pendingDebitTransactionDescription |
      | pendingDebitTransactionAmount      |
    When I select pending Direct Debit on statements page
    Then I should see pending transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | vtdCloseButton            |
      | vtdAmount                 |
      | debitCardText             |
      | payeeField                |
      | dateOfTransaction         |
      | debitDisclaimerText       |
      | unsureTransactionButton   |
    When I close the pending transactions VTD
    And I select pending Cheque on statements page
    Then I should see pending transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | vtdCloseButton            |
      | vtdAmount                 |
      | notYetAvailable           |
      | chequesText               |
      | payeeField                |
      | depositedOn               |
      | availableBy               |
      | creditDisclaimerText      |
      | unsureTransactionButton   |
    When I select the help with this transaction link in VTD screen
    Then I must be displayed the Unsure About Transactions Help and Info screen
    When I select to go back
    Then I should be displayed VTD page of that transaction
    When I close the pending transactions VTD
    And I swipe to see previous month transactions on the account statement page
    Then I should see minimised pending transactions accordion
    And I navigate to logoff via more menu
    And I should be on the logout page

  @mbnaSuite
  Scenario: TC_03 Statements Viewing the pending credit card transactions
    Given I am a creditCard user on the home page
    When I select creditCard account tile from home page
    And I tap on the pending transactions accordion
    Then I should see pending Credit transaction with below detail in statements
      | detail                        |
      | pendingLabel                  |
      | pendingCreditTransactionDescription |
      | pendingCreditTransactionAmount      |
    When I select pending Credit Card on statements page
    Then I should see pending transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | vtdCloseButton            |
      | vtdAmount                 |
      | creditCardText            |
      | payeeField                |
      | dateOfTransaction         |
      | creditCardDisclaimerText  |
      | unsureTransactionButton   |
    And I close the pending transactions VTD
    And I navigate to logoff via more menu
    And I should be on the logout page
