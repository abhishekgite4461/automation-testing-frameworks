@team-statements @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-rnga-hcc @ff-hcc-enabled-on @sw-hcc-ios-on @sw-hcc-android-on
@manual
# automation will be completed along with total pending story which will be in kenya release
Feature: E2E High Cost Of Credit
  As a NGA Retail customer,
  I want my Statements page to display my balance after pending,
  So that I am always informed of the limit of my overdraft facility

  Scenario Outline: Display Balance After Pending Overdraft Limit and Account Balance
    Given I am an enrolled "current" user logged into the app
    When I navigate to <type> account to validate balanceAfterPending and select <type> account
    Then I should be on the allTab of statements page
    And I should see account details with the following fields
      | fieldsInAccountTile      |
      | balanceAfterPendingLabel |
      | overDraftLimitLabel      |
      | overDraftLimitValue      |
      | <message>                |
      | accountTrueBalance       |
    And The balanceAfterPending value should be same with the value in Account overview page
    And The balance after pending value should be <symbol> with difference between account balance and the total value of pending transactions
    Examples:
      | type                                            | symbol   | message            |
      | currentAccountWithPendingLessThanAccountBalance | positive | noOverDraftMessage |
      | currentAccountWithPendingMoreThanAccountBalance | negative | overDraftMessage   |
      | currentAccountWithAccountBalanceNegative        | negative | overDraftMessage   |
