@team-login @rnga @nga3 @stub-kyc-dismissed-once @ff-kyc-on @optFeature @primary @stubOnly
Feature: Display of KYC screen to ensure user details are up to date
  In order to keep my contact details up to date
  As a Retail Customer
  I want to be prompted to update my details on the App on regular basis

# Preconditions for KYC
# KYC Switch is ON
# User has not confirmed the KYC details on any other device
# User has not already ignored the KYC twice
# User is not performing any App sign journey
# User is not shown Whats new screen
# User is not shown Big prompt lead (Interstitial page)
# User is not shown Touch ID registration prompt

  Background:
    Given I submit "correct" MI as "current" user on the MI page
    And I should be on the review your contacts page

  Scenario: TC_01 User chooses to update later and ignore KYC for the first time
    Given I am able to view the existing details
      | existingDetails |
      | emailId         |
      | mobileNumber    |
    And I am able to view options
      | options        |
      | detailsCorrect |
      | updateLater    |
    When I choose to update my details later without edit
    Then I should be on the home page
    When I navigate to profile page via more menu
    Then I should be able to view the updated details on personal details page
      | details      |
      | email        |
      | mobileNumber |
   # Note: status of the database column NGASecProfileState should be set to -1
   # And the phone and email update timestamp should not change

  #User details not stored on stub. Cant run on env due to KYC condtitions
  @manual
  Scenario Outline: TC_02 User chooses to edit and update details on KYC screen
    Given I have updated my <details>
    And I am displayed KYC confirmation screen
    When I choose to edit the details
    Then I should be taken back to KYC screen
    When I choose to edit my <details>
    Then I should be displayed KYC confirmation screen with newly entered email and phone number
    And I am given option to edit the details or continue
    When I proceed on the confirmation screen
    Then I should not be prompted for password confirmation
    When I navigate to profile page via more menu
    Then I should be able to view the updated <details> on personal details page
    Examples:
      | details       |
      | Email id      |
      | Mobile number |
   # Note: status of the database column NGASecProfileState should be set to -1
   # And the phone and email update timestamp should not change
