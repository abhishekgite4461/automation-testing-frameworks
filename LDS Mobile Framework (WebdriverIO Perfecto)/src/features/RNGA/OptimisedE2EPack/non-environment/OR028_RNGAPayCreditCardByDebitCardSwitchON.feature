@team-payments @nga3 @rnga @ff-pay-credit-card-by-debit-card-on @sw-pccdc-on @mbnaSuite
Feature: Pay credit card by debit card
  In order to pay my credit card
  As a RNGA customer
  I want to be use a debit card from any bank to pay my LBG credit card

  # TODO:MPT-3471 Web journey and network restrictions for PCCDC. Hence feature file is Pending
  @genericAccount @pending
  Scenario: TC_01 Main Success Scenario: Pay Credit Card by Debit Card journey
    Given I am a Current and credit card user on the home page
    And I am on the payment hub page
    When I choose the from account as debitCard and to account as creditCard in the payment hub page
    Then I am not shown payment date in the payment hub
    When I enter amount greater than the balance of the credit card account in the payment hub page
    Then I should see credit card payment validation error message
    When I dismiss the error message
    And I submit the pccdc with all the mandatory fields with corresponding amount value 2.00
    Then I am displayed a warning message
    When I choose to continue on the warning message
    Then I am taken out of the app with the PCCDC journey continuing in the devices native web browser application
    When I switch back to the banking app
    Then the Payment Hub should be dismissed

  @stub-no-creditcard
  Scenario: TC_02 Exception: User does not hold a Credit Card
    Given I am a no credit card account user on the home page
    When I navigate to the remitter account on the payment hub page
    Then I am not shown the option to pay using debit card

  @stub-credit-card-only @otherAccount
  Scenario: TC_03 User does not hold other eligible remitting accounts
    Given I am a credit card only user on the home page
    When I am on the payment hub page
    Then I should see the from field pre-populated with debitCard account
    And I should see the to field pre-populated with creditCard account
    And I am not shown payment date in the payment hub
