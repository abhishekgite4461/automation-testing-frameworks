@team-statements @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-enrolled-valid @sw-spending-rewards-cwa-on @sw-sr-ur-optin-on @otherAccount @exhaustiveData @ff-webview-debug-on

#its exhaustive data as user needs to be opted out every time before executing these scenarios
Feature: E2E_verify Spending Rewards

  #DPL-1426
  Scenario: TC_01 User wants to optIn view spending rewards from home page
    Given I am an enrolled "opted out spending rewards" user logged into the app
    When I select everyday offers tile from home page
    Then I should see below options in the everyday offers page
      | options                          |
      | activateTheOffers                |
      | spendingRewardsShopTitle         |
      | spendingRewardsEnjoyDescription  |
      | spendingRewardsOptInButton       |
      | spendingRewardsNotNowOptInButton |
    When I opt in for spending rewards
    Then I should be opted-in for spending rewards
    When I tap on view your offers
    Then I should be on the spending rewards webview page
    And I should see below options in the everyday opt in page
      | options                   |
      | expiredOffers             |
      | settings                  |
      | findOutMore               |
    And I should be on New Tab selected by default

  #DPL-1426
  Scenario: TC_02 User wants to optIn and modify spending rewards from settings
    Given I am an enrolled "opted out spending rewards" user logged into the app
    When I navigate to settings page via more menu
    And I tap on register for everyday offers option
    Then I should see below options in the everyday offers page
      | options                          |
      | activateTheOffers                |
      | spendingRewardsShopTitle         |
      | spendingRewardsEnjoyDescription  |
      | spendingRewardsOptInButton       |
      | spendingRewardsNotNowOptInButton |
    When I opt in for spending rewards
    Then I should be on the settings page
    When I select modify spending rewards from the settings menu page
    Then I should be on the modify spending rewards webview page
    And I should see below options in the everyday opt in page
      | options                   |
      | expiredOffers             |
      | settings                  |
      | findOutMore               |
    And I should be on New Tab selected by default
