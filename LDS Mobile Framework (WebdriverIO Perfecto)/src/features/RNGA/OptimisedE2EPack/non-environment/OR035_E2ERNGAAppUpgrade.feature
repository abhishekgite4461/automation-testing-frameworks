@team-release @team-delex @rnga @nga3 @manual @optFeature
Feature: E2E_Verify RNGA upgrade path from the previous unbanned versions to the latest build version

  Background:
    Given I am an enrolled "unbanned previous app" user logged into the app
    And I navigate to logoff via more menu
    And I download the latest version of the app and install

  Scenario: TC_01 Upgrade from previous unbanned version to the latest build version and verify the Face ID, Touch ID or Finger print in latest app
    Then I should see the whats new screen
    When I swipe the whats new screens and select Log On button
    Then I should be on the touch id/face id/fingerprint interstitial page
    And I should be on the home page

  Scenario: TC_02 Update the customer personal information in previous app and upgrade to the latest app and verify everything got updated in the latest app
    Then I should see the whats new screen
    When I swipe the whats new screens and select Log On button
    And I enter correct MI
    Then I should be on the home page
    And the below options from the settings and your profile
      |your security settings|
      |Marketing preference  |
      |location pop up       |
      |Phone number          |
      |Email ID              | 
