@team-statements @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-enrolled-valid @ios
  # separate feature file is created for android for all the scenarios with condensed account tile changes.
  # Will be common scenarios for both once condensed account tile is implemented in iOS
Feature: E2E Verify RNGA Loan details
  As a NGA Retail customer,
  I want to view loans statements,
  So that I am better informed about my transactions

  @noExistingData
  Scenario: TC_01 View loan details & current year transactions
    Given I am a non cbs loan user on the home page
    When I select nonCbsLoan account tile from home page
    Then I should be on the current year loan statements tab
    And I tap on the plus sign to expand the accordion with following loan details
      | fieldsForLoanAccount   |
      | nextRepaymentDue       |
      | remainingTerm          |
      | minusAccordion         |
      | openingDate            |
      | monthlyRepaymentAmount |
      | originalLoanTerm       |
      | noteOnAnnualStatement  |
    And I tap on minus sign to close the accordion and should see the following loan details
      | fieldsForLoanAccount |
      | plusAccordion        |
      | nextRepaymentDue     |
      | remainingTerm        |
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    And I should scroll down to the end of the transactions to view loanEndOf message
    When I swipe to see previous year transactions on the account statement page
    Then I should be on the same previous year statements page
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    And I should scroll down to the end of the transactions to view loanEndOf message
    When I navigate to logoff via more menu
    Then I should be on the logout page

  #DPL-1008
  @genericAccount @lds
  Scenario: TC_02 View personal loan(CBS) account details on account tile
    Given I am a current user on the home page
    When I navigate and validate the options in the cbsLoan account in below table in the home page
      | accountFields        |
      | cbsLoanAccountNumber |
      | cbsLoanSortCode      |
      | cbsCurrentBalance    |
    And I select cbsLoan account tile from home page
    Then I should see account details with the following fields
      | fieldsInAccountTile |
      | accountName         |
      | accountNumber       |
      | sortCode            |
      | accountBalance      |
    And I should see the message displayed and should not see action menu on account tile
    When I navigate to logoff via more menu
    Then I should be on the logout page
