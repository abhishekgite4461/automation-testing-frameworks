@team-release @team-triage @nga3 @rnga @mbnaSuite @optFeature @manual
Feature: E2E_Resilience scenarios

  # Note : For MBNA a "current" user will return a "credit card" user
  Scenario: TC_01 Validate the device language and date change
    Given I am a current user on the home page
    And I am anywhere in the middle of any journey with in the app or while loading the journey
    And I background the app and get it back to foreground
    Then I should see the app landing in the same page
    And I change the device language other than english from device settings
    And I should not see the app changing language in payment details and statements page
    When I change the device date from mobile settings
    And I select the date from calender in payment hub
    Then I should see the current date in calender as the changed date

  Scenario Outline: TC_02 Validate transfer, payment, login and logout when there is no internet connectivity, lost or change to wifi from 3g or 4g
    Given I am a current user
    And I see the internet connection is lost
    And I initiate a <action> journey
    Then I should see the internet connection lost error in transfer page
    And I initiate a <action> journey
    And I switch the network from wifi to 3G or 3G to wifi
    And I should see the <action> completed successfully
    Examples:
      | action |
      | transfer |
      | payment  |
      | login    |
      | logout   |

  Scenario Outline: TC_03 Validate auto logout when navigating outside the app within and beyond time limit
    Given I am a current user on the home page
    And I am on the payment hub page
    When I am listening to music or attending a call or playing game while the app is in background
    And my log off timer setting is set to <mins>
    Then I should not see the auto log out message on getting back to the app within the timer limit
    And I am on the payment hub page
    When I am listening to music or attending a call or playing game while the app is in background
    And my log off timer setting is set to <mins>
    Then I should see the auto log out message on getting back to the pp after exceeding the timer limit
    Examples:
      |   mins         |
      | immediate      |
      | 1 Minute       |
      | 2 minute       |
      | 5 minute       |
      | 10 minute      |

  Scenario: TC_04 Validate Incomplete Standing order add new beneficiary done with mobile battery low
  is updated in browser
    Given I am a current user on the home page
    And I am on the payment hub page
    And I add a new beneficiary on app
    And I create a standing order on app
    When the device shuts down due to low battery after adding beneficiary successfully & creating standing order
    And I should see the beneficiary added in desktop site
    Then I should not see the standing order created in desktop site

  @android
  Scenario: TC_05 Validating the text size changes are reflected in the app
    Given I am a current user on the home page
    And I am changing the text size from mobile settings
    When I open the app after changing the text size
    Then I should see the text size changes reflected in the app

  @android
  Scenario: TC_06 Validating the text fields while toggling the auto correct option from keyboard settings
    Given I am a current user on the home page
    And I toggle the auto correct option ON or OFF from device settings
    When I open the app after changing the auto correct option
    Then I should see all the text fields in app behave the same as expected

  Scenario Outline: TC_07 Validating the permissions pop up given to the app from device
    Given I am a current user on the home page
    And I change the permissions <permission> to the app from device settings
    And I Launch the app for the first time
    Then I should see the permission pop up displayed for location contacts, phone and camera
    Examples:
      | permission |
      | location |
      | contact |
      | phone |
      | camera |

  @genericAccount
  Scenario: TC_08 Validate app behaviour during device rotation Tablet
    Given I am a current user on the home page
    When I rotate the Tab to landscape orientation
    Then I should see the app in landscape orientation
    When I have submitted a request to change my email address
    And The request has been successfully processed
    And I rotate the Tab to portrait orientation
    Then I should see the app in portrait orientation

  Scenario: TC_09 Validate app behaviour on deleting MI & turn off and turn on internet
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    Then I should be on enter MI page
    And I enter first character of MI and I can see the 2nd MI field is highlighted
    And I select first character of MI and edit the first character
    And I should see the 2nd MI field is highlighted
    And I enter valid MI on MI page
    And I turn off the Internet and turn on back while loading
    And I should be on the home page
