@team-payments @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @sensorInstrument @ios @optFeature @genericAccount @stub-enrolled-valid
Feature: Cheque Deposit (Retail).
  In order to have funds available in my account within the new 2 day clearing model.
  As a Retail NGA User
  I want to deposit a cheque

  Scenario Outline: TC_01 Main Success Scenario: ICS e2e
    Given I am a current user on the home page
    When I navigate to <type> account on home page
    And I select the action menu of <type> account
    And I select deposit cheque option in action menu
    Then I should be on the cheque deposit screen
    When I navigate to cheque deposit page via more menu
    And I select the account navigation icon in deposit cheque page
    Then I should be shown only the eligible accounts for depositing a cheque
    And I navigate back
    And I see below fields on cheque deposit page
      |chequeDepositOptions |
      |depositAccount |
      |chequeAmountField |
      |chequeReferenceField |
      |captureFrontOfChequeButton|
      |captureBackOfImageButton |
    When I enter an amount more than the ics mobile cheque deposit transaction limit <chequeMaxAmount>
    Then I should see transaction limit error message in cheque deposit page
    When I select the cheque reference field
    And I enter correct alphanumeric characters of length 18 as <reference>
    Then I should be able to view the 18 characters in reference
    When I deposit a valid cheque <image> of <amount> with <reference>
    Then I should see thumbnail images in deposit screen
    When I select review deposit button
    Then I should be able to see review deposit screen
    When I select thumbnail for front and back image
    Then I should be able to see the full screen image
    When I close the cheque image
    And I confirm the cheque deposit
    And I select confirm button if popup screen displayed for amount mismatch
    Then I should be able to see deposit success or failure screen
    And I navigate to deposit history page via more menu
    When I am on deposit history page
    Then I see the below fields on deposit history page
      |historyPageDetails|
      |depositDate|
      |depositReference|
      |depositStatus|
      |depositAmount|
    When I am on deposit history page
    Then I see the below account picker fields on deposit history page
      |accountPickerList |
      |accountType |
      |sortCode |
      |accountNumber |
      |ledgerBalance |
      |availableBalance |
    When I select an item in deposit history list
    Then I see the below fields in deposit review page
      |depositReviewFields|
      |depositStatus |
      |depositAccountType |
      |depositedDate |
      |depositAmount |
      |depositReference |
    And I navigate to deposit history page via more menu
    When I change the account which is displayed in the history page
    Then I should see history details for the newly selected account
    When I select the pending cheque details page
    Then I should be shown message mapped to status code related to pending received from Clearing Status field
    And I should see the hint to tap to view enlarged image
    When I select the cheque image
    Then I should be able to see the enlarged cheque image
    When I close the cheque image
    Then I should return to the history review of the selected cheque
    And I navigate back
    When I finish scrolling through all available transactions
    Then I should see a message that I have reached the end of available transactions
    Examples:
      |image                        |amount |reference          |type    |chequeMaxAmount|
      |DepositSuccess/FrontOfCheque |10.14  |testDepositJourney |current |501.00         |
