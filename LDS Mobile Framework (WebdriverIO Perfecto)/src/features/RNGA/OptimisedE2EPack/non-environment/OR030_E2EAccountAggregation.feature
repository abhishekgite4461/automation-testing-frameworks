@rnga @nga3 @team-aov @stubOnly @sw-enrolment-on @sw-account-aggregation-on @primary @optFeature
Feature: Open Banking Account Aggregation - Switch V1 ON - Switch V2 ON - Feature Flag ON
  In Order to access all my accounts including external bank current accounts using NGA app
  As a Retail NGA user
  I want to be able to add external bank accounts using my app

# Reason for Manual:
# Account aggregation journey is dependent on the OB journey and
# involves switching from App to Browser/App and then returning to App,
# which is being tested manually.

  @stub-open-banking-auto-load-external-accounts-success @failed
  #MOBS-24, MOBS-25, MOBS-428, MOBS-448, MOBS-450, MOBS-444, MOBS-473, MOBS-531, #MOBS-482, MOBS-472
  Scenario: TC001 – Main Success Scenario: User should see the OB Promo tile V2 on the homepage when Switch V1 and Switch V2 is ON & Feature flag is ON
    Given I submit "correct" MI as "current" user on the MI page
    Then I should see ghost tile for external accounts load
    When I select the ghost tile to validate it is not clickable
    And I wait for ghost tile to disappear
    Then I should see external accounts loaded
    And I am shown Open Banking promo tile
    And I am shown the option to add external accounts
    And I am not shown the option to view my external accounts
    And I should be shown the list of added external bank account tiles above the Open Banking Promo Tile

  @manual
  #MOBS-90, MOBS-19, MOBS-112, MOBS-257, MOBS-167, MOBS-322, MOBS-24, MOBS-25, MOBS-428, MOBS-448
  Scenario Outline: TC002 – Main Success Scenario: User should Add external accounts successfully
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the Redirecting screen for account aggregation
    And I should see the provider bank name extBankAIB on the redirect screen
    And I should be navigated to the mobile browser for the designated webpage of the selected provider bank
    When I aggregate "<countOfAccounts>" of my external account from the external provider website
    Then I see deeplink is triggered after account is successfully aggregated
    And I should be redirected back to the NGA app
    And I should be on the home page
    And I should see ghost tile for external accounts load
    And I wait for ghost tile to disappear
    And I should see external accounts loaded
    And I should receive an email on my email address with T&C as an attachment

    Examples:
      | countOfAccounts |
      | one             |
      | more than one   |

  @stub-arrangements-loan-only
  #MOBS-70, MOBS-144, MOBS-119, MOBS-145
  Scenario: TC003 – Main Success Scenario: User should be able to view Externally added accounts on the Account Overview page
    Given I am an enrolled "current" user logged into the app
    Then I should see external accounts loaded
    And I should see below fields in external provider bank extBankNTW with account type Credit
      | creditCardNumber |
      | availableCredit  |
    And I should see below fields in external provider bank extBankNTW with account type Savings
      | sortCode      |
      | accountNumber |
    And I should see below fields in external provider bank extBankNTW with account type Current Account
      | sortCode      |
      | accountNumber |

  @stub-arrangements-loan-only @failed
  #MOBS-919, MOBS-940
  Scenario: TC004 - Main Success Scenario: User should be able to remove external bank accounts from Consent Management screen
    Given I am an enrolled "current" user logged into the app
    Then I should see external accounts loaded
    When I select action menu button on external bank extBankNTW Credit account tile
    Then I should see external bank name extBankNTW on action menu header
    When I select Manage accounts option from the action menu
    Then I should be on Consent settings page
    And I should see Authorised account for extBankBarclays on consent settings page
    When I select Manage button for extBankBarclays account on consent settings page
    Then I should see Consent settings pop up for extBankBarclays
    When I select Remove option on Consent settings pop up
    Then I should see remove alert on Consent settings page
    And I should see winback with Close and Remove option
    When I select the Remove option in winback
    And I wait for spinner to disappear
    Then I should see Revoked account for extBankBarclays on consent settings page

  @stub-arrangements-loan-only
  #MOBS-161, MOBS-200, MOBS-162, MOBS-199
  Scenario: TC005 - Main Success Scenario: User should be able to see the transactions for the externally added account
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I select action menu button on external bank extBankBarclays Credit account tile
    And I should see external bank name extBankBarclays on action menu header
    And I should see below options on external bank account action menu pop up
      | View transactions |
      | Manage accounts   |
    When I select View transactions option from the action menu
    Then I should see the transactions for the externally added account
    When I navigate to home page
    Then I should be on the home page
    And I select action menu button on external bank extBankBarclays Savings account tile
    And I should see external bank name extBankBarclays on action menu header
    And I should see below options on external bank account action menu pop up
      | View transactions |
      | Manage accounts   |
    When I select View transactions option from the action menu
    Then I should see the transactions for the externally added account
    When I navigate to home page
    Then I should be on the home page
    And I select action menu button on external bank extBankHFX Current account tile
    And I should see external bank name extBankHFX on action menu header
    And I should see below options on external bank account action menu pop up
      | View transactions |
      | Manage accounts   |
    When I select View transactions option from the action menu
    Then I should see the transactions for the externally added account

  @manual
  #MOBS-557, MOBS-559, MOBS-859
  Scenario: TC006 – Aggregation App to App: User should Add external accounts successfully via Universal Links and should see Ghost tile on clicking Close button
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the Redirecting screen for account aggregation
    And I should see the provider bank name extBankAIB on the redirect screen
    And I should be navigated to the mobile browser/app for the selected provider bank
    When I aggregate "<countOfAccounts>" of my external account from the external provider website
    Then I see universal link is triggered after account is successfully aggregated
    And I should be redirected back to the NGA app
    And I should see loading indicator in the app
    And I should see aggregation successful native page
    When I select the close button at the bottom of the page
    Then I should be on the home page
    And I should see ghost tile for external accounts load
    When I wait for ghost tile to disappear
    Then I should see external accounts loaded

  @manual
  #MOBS-557, MOBS-559, MOBS-859
  Scenario: TC007 – Aggregation App to App: User should Add external accounts successfully via Universal Links and should see Ghost tile on clicking Cross button
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the Redirecting screen for account aggregation
    And I should see the provider bank name extBankAIB on the redirect screen
    And I should be navigated to the mobile browser/app for the selected provider bank
    When I aggregate "<countOfAccounts>" of my external account from the external provider website
    Then I see universal link is triggered after account is successfully aggregated
    And I should be redirected back to the NGA app
    And I should see loading indicator in the app
    And I should see aggregation successful native page
    When I close the account aggregation screen
    Then I should be on the home page
    And I should see ghost tile for external accounts load
    When I wait for ghost tile to disappear
    Then I should see external accounts loaded

  @manual
  #MOBS-557, MOBS-559, MOBS-859
  Scenario: TC008 – Aggregation App to App: User should see aggregation not successful screen via Universal Links and no action takes place on clicking close button
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the Redirecting screen for account aggregation
    And I should see the provider bank name extBankAIB on the redirect screen
    And I should be navigated to the mobile browser/app for the selected provider bank
    When I aggregate "<countOfAccounts>" of my external account from the external provider website
    And I see that the account aggregation is not successful
    Then I see universal link is triggered after account aggregation fails
    And I should be redirected back to the NGA app
    And I should see loading indicator in the app
    And I should see aggregation not successful error page
    When I select the close button at the bottom of the page
    Then I should be on the home page
    And I should not see ghost tile for external accounts load
    And I should see external accounts loaded

  @manual
  #MOBS-557, MOBS-559, MOBS-859
  Scenario: TC009 – Aggregation App to App: User should see aggregation not successful screen via Universal Links and no action takes place on clicking cross icon
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
    And I should see external accounts loaded
    And I am shown Open Banking promo tile
    When I proceed to add my external account
    Then I should see the Open Banking screen for account aggregation
    When I proceed to the secure and protected screen
    Then I should see the Secure and protected screen for account aggregation
    When I proceed to the set up in 3 steps screen
    Then I should see the Set up in 3 steps screen for account aggregation
    When I proceed to the account provider screen
    Then I should see the Account provider screen for account aggregation
    When I select an external provider bank extBankAIB from the list
    Then I should see the Important information screen for account aggregation
    When I agree to the T&C and Consent for open banking
    And I proceed to the external provider website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the Redirecting screen for account aggregation
    And I should see the provider bank name extBankAIB on the redirect screen
    And I should be navigated to the mobile browser/app for the selected provider bank
    When I aggregate "<countOfAccounts>" of my external account from the external provider website
    And I see that the account aggregation is not successful
    Then I see universal link is triggered after account aggregation fails
    And I should be redirected back to the NGA app
    And I should see loading indicator in the app
    And I should see aggregation not successful error page
    When I close the account aggregation screen
    Then I should be on the home page
    And I should not see ghost tile for external accounts load
    And I should see external accounts loaded
