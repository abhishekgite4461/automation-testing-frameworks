@team-navigation @nga3 @rnga @primary @instrument @mbnaSuite @optFeature @stubOnly
Feature: Retrieve Notifications in the app
  In order to view relevant notifications about my account
  As a customer
  I want to see and action the notifications as soon as I land on the home page

  #MOB3-10479/1218/10482/704
  Background:
    Given I am an enrolled "valid" user logged into the app

  #------------------------------------------ Main Success scenario---------------------------#

  @stub-three-notifications
  Scenario: TC_01 Main Success Scenario : Multiple notification
    Given I have multiple notification
    When I select the notification placement outlining the number of notifications
    Then The notification expands to show me a detail of all notifications
    When I click on one of the notifications
    Then I must be taken to the relevant journey based on the link in the notification
    When I navigate back to the home screen from the notification journey
    Then The multiple notification service and Arrangement service must be refreshed

  @stub-credit-card-notification
  Scenario: TC_02 Alternate Success Scenario : Single notification
    Given I have only 1 notification
    When I select the notification placement
    Then I must be taken to the relevant journey based on the link in the notification
    When I navigate back to the home screen from the notification journey
    Then The single notification service and Arrangement service must be refreshed

  #------------------------------------------ Variation scenario---------------------------#

  @stub-two-notifications
  Scenario: TC_03 Variation Scenario : Close notification placement
    Given I have multiple notification
    And I select the notification placement outlining the number of notifications
    When I collapse the notification placement outlining the number of notifications
    Then The multiple notification service and Arrangement service must be refreshed
