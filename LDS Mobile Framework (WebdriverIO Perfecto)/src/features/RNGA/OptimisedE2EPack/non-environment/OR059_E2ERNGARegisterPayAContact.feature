@team-payments @stub-enrolled-valid @nga3 @rnga @environmentOnly @sw-enrolment-on @sw-light-logon-on @manual @optFeature

Feature: User wants register for pay a contact
  In order to send or receive money via my phone number
  As a RNGA authenticated customer
  I want to be able to register my phone number through the mobile app

  #  ALM-3109 unable to add the P2P contact in environments
  # TODO: MPT-3417 Need to run this on Appium or head Spin since it is a web view
  Scenario: TC_01 User navigates to P2P settings from add new beneficiary winback
    Given I am a current user on the home page
    When I navigate to pay a contact settings page via more menu
    Then I should be on the register for pay a contact page
    When I navigate to homepage from register for pay a contact page
    And I navigate to add new recipient option using payment hub
    Then I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
    And I select the pay Uk number tooltip on beneficiary type page
    Then I should see a pop for pay a contact registration
    When I select register now button on the popup in the p2p page
    Then I should be on the register for pay a contact page
    When I choose to register for pay a contact
    Then I should see registration successful screen
