@team-mco @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @stub-scottish-widows-2-accounts @otherAccount @optFeature @sw-scottish-widows-tiles-on @ff-pension-transfer-on
@sw-enable-pension-transfer-on

  # created separate scenario for iOS because condensed account tile is not present for iOS platform
Feature: In order to view my SW Pensions
  As a LBG & SW customer
  I want to view and service my SW products in the LBG app.

  #DPL-1011
  @android
  Scenario: TC_01 Discovering more details about a Scottish Widows Account
    Given I am an enrolled "Scottish Widows" user logged into the app
    And I navigate to scottishWidows account and validate accountNumber
    When I select scottishWidows account tile from home page
    Then I should be displayed scottishWidows account name action menu and last four digits of scottishWidows number in the header
    And I should see my Pension balance
    And I should see a statement page with information about scottish widows pensions
    When I select the more information button on the statement view
    Then I should see the Scottish widows FAQ page
    And The Global Menu should not appear
    When I navigate back
    Then I should see a statement page with information about scottish widows pensions

  @ios
  Scenario: TC_02 Discovering more details about a Scottish Widows Account
    Given I am an enrolled "Scottish Widows" user logged into the app
    And I navigate to scottishWidows account and validate accountNumber
    When I select scottishWidows account tile from home page
    Then I should see my Pension Policy number on statement page
    And I should see my Pension balance
    And I should see a statement page with information about scottish widows pensions
    When I select the more information button on the statement view
    Then I should see the Scottish widows FAQ page
    And The Global Menu should not appear
    When I navigate back
    Then I should see a statement page with information about scottish widows pensions

  @android
  Scenario: TC_003 - Verify pension transfer option from action tile action menu and landing page
    Given I am an enrolled "Scottish Widows" user logged into the app
    When I navigate to scottishWidows account on home page
    And I select the action menu of scottishWidows account
    Then I should see the below options in the action menu of the scottishWidows account
      | optionsInActionMenu   |
      | pensionTransferOption |
    When I select the "pensionTransfer" option
    Then I should be displayed the Pension Transfer webview
    When I navigate to home page
    And I select scottishWidows account tile from home page
    And I select the action menu from statements page
    Then I should see the below options in the action menu of the scottishWidows account
      | optionsInActionMenu   |
      | pensionTransferOption |
    When I select the "pensionTransfer" option
    Then I should be displayed the Pension Transfer webview
