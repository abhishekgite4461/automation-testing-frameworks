@team-statements @nga3 @rnga @ff-travel-disputes-on @sw-travel-disputes-android-on @ff-ios-help-enhancements2-on @sw-travel-disputes-ios-on @android @optFeature @genericAccount
@pending
# Blocked by backend issues
# This will be moved to environment once CWA deployment is completed
Feature: Travel Disputes
  In order for me to take appropriate steps
  As a RNGA user with current posted debit card transactions
  I should be able to submit G-from request for the disputed transaction

  @stub-enrolled-valid
  Scenario Outline: TC_001 Main Success Scenario: Travel Disputes entry point and Personal details design (Mandatory fields all present)
    Given I am an enrolled "current" user logged into the app
    And I select <type> account tile from home page
    When I select <transaction> on statements page
    And I select the help with this transaction link in VTD screen
    Then I must be displayed the Unsure About Transactions Help and Info screen
    When I select Travel Dispute option on helpDetails page
    Then I should see following fields on TravelDisputesHelp page
      | fields                         |
      | travelDisputesNavigationHeader |
      | travelDisputesTitle            |
      | travelCopy                     |
      | getStartedButton               |
    When I select Get Started Button on travel disputes help page
    Then I should see following fields on BeforeYouBegin page
      | fields                         |
      | beforeYouBeginNavigationHeader |
      | pageHeader                     |
      | warningText                    |
      | detailsLabel                   |
      | addressLabel                   |
      | addressField                   |
      | mobileNumberLabel              |
      | mobileNumberField              |
      | emailLabel                     |
      | emailAddress                   |
      | updateDetailsSecondaryButton   |
      | continueButton                 |
    When I select continue button on BeforeYouBegin page
    Then I should be on GForm page
    And I navigate back
    When I select the updateDetailsSecondaryButton to update personal details
    Then I should be on the your personal details page
    Examples:
      | type                                   | transaction |
      | current Account With View Transactions | debitCard   |

  @stub-travel-dispute
  Scenario Outline: TC_002 Main Success Scenario: Travel Disputes entry point and Personal details design (Mandatory fields missing)
    Given I am an enrolled "current" user logged into the app
    And I select <type> account tile from home page
    When I select <transaction> on statements page
    And I select the help with this transaction link in VTD screen
    Then I must be displayed the Unsure About Transactions Help and Info screen
    When I select Travel Dispute option on helpDetails page
    Then I should see following fields on TravelDisputesHelp page
      | fields                         |
      | travelDisputesNavigationHeader |
      | travelDisputesTitle            |
      | travelCopy                     |
      | getStartedButton               |
    When I select Get Started Button on travel disputes help page
    Then I should see following fields on BeforeYouBegin page
      | fields                         |
      | beforeYouBeginNavigationHeader |
      | pageHeader                     |
      | updateWarningText              |
      | updateDetailsPrimaryButton     |
    And I should not see continue button
    When I select the updateDetailsPrimaryButton to update personal details
    Then I should be on the your personal details page
    When I navigate back
    Then I should be on the settings page
    Examples:
      | type                                   | transaction |
      | current Account With View Transactions | debitCard   |
