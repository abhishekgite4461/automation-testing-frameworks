@mbnaSuite @team-login @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @primary
Feature:Device Enrolment for retail customer with Device enrolment switch ON and Qivox bypass switch ON
  ###This feature file handles the sceanrios with Qivox bypass EIA call in ON status, hence this feature file is exclusively for automation purpose.

  # Note : For MBNA a "current" user will return a "credit card" user

  ## Removing TC_01 Log into RNGA successfully and enrol the device with a valid registered phone number as Enrollment Scenario happening
  ## for Generic Accounts in other parts as part of optimised pack.

  @environmentOnly @unEnrolled @uninstallAppAfterRun
  Scenario: TC_02 Log into RNGA without registered phone number i.e. display Request OTP screen. User wish to proceed with request OTP and choose the confirm option .  After requesting OTP try to enrol for partially registered user with wrong otp
    Given I am a no registered phone number user
    When I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I select the confirm option on the Request OTP page
    Then I should be on the request OTP confirmation page
    And I should see contact us button on the request OTP confirmation page
    When I relaunch the app
    And I am a no registered phone number user
    And I login with my username and password
    And I enter correct MI
    And I enter invalid OTP in the enter one time password page
    Then I should see incorrect OTP message in the enter one time password page

  @unEnrolled
  Scenario: TC_04 Log in RNGA and maximum number of enrolled device count reached
    Given I am a max device enrolled user
    When I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    Then I should be on the maximum device enrolled error page

  #MOB3-108
  @environmentOnly @unEnrolled @outOfScope
  Scenario: TC_05 Log in RNGA and validate the eia phone number is masked, then Log in RNGA with a phone number updated in the Desktop site
    Given I am a current user
    When I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I set the phone number config at backend to "2" days
    And I set a phone number in the desktop site
    Then I should not see the mobile number is greyed out
    When I relaunch the app
    And I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    Then I should see the mobile number is greyed out
    And I set the phone number config at backend to "0" days

  #Requires sim swap, can't automate
  @manual
  Scenario: TC_06 Log in RNGA with a sim swap with more than one registered number
    Given I am a sim swapped user
    When I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I choose call me now option
    Then I should be on the phone authentication failed error page
    When I press on the ok button in the phone authentication failed error page
    Then I should be on the eia page with the number greyed out for sim swapped user
