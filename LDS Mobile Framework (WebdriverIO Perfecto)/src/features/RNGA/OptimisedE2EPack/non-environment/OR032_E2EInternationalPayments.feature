@team-payments @nga3 @rnga @environmentOnly @sw-enrolment-on @sw-light-logon-on @genericAccount @optFeature @pending

Feature: User wants to access international payments journey
  In Order to add or pay a current international recipient
  As a RNGA Authenticated Customer
  I want to be able to access international payments journey

  # Remove pending tag once DPLDA-472 International paymnents issue resolved in SIT10-W
  Scenario: TC_01 User uses entry points for international payment
    Given I am a current user on the home page
    When I navigate to current account on home page
    And I select the "international payment" option in the action menu of current account
    Then I should be on the international payment webview page
    When I select dismiss modal using close button
    And I navigate to add new recipient option using payment hub
    Then I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
    And I select international bank account from account category page
    Then I should be on the international payment webview page
