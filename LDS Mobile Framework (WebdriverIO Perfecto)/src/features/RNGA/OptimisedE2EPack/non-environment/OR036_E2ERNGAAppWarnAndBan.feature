@team-delex @nga3 @rnga @optFeature @appWarnBan @sit02Job @mbnaSuite
Feature: E2E_Verfiy app warn and ban screens for non supported Retail app versions

  @android @genericAccount
  Scenario Outline: Verify app Warn Screens for enrolled user
    Given I am an enrolled "current" user logged into the app
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I relaunch the app
    And I change an app version to <appVersion> on environment selection page
    And I select the environment
    And I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay
    And I select close button on pre auth call us home page overlay
    When I choose to update the app from the warn screen
    And I navigate to app store screen
    And I navigate back to app from app store
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to continue without updating from the warn screen
    Then I should be on enter MI page
    When I enter correct MI
    And I accept interstitial during logon
    Then I should be on the home page
    When I navigate to logoff via more menu
    Then I should be on the logout page
    Examples:
      | appVersion         |
      | 55                 |

  @android @unEnrolled
  Scenario Outline: Verify app Warn Screens for unenrolled user
    Given I am a current user
    When I relaunch the app
    And I change an app version to <appVersion> on environment selection page
    And I select the environment
    And I choose an option to continue to the next screen
    And I login with my username and password
    And I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay
    And I select close button on pre auth call us home page overlay
    When I choose to update the app from the warn screen
    And I navigate to app store screen
    And I navigate back to app from app store
    And I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to continue without updating from the warn screen
    Then I should be on enter MI page
    When I enter correct MI
    And I finish enrolment
    And I accept interstitial during logon
    Then I should be on the home page
    When I reset the app
    Then I should be on the logout page
    Examples:
      | appVersion         |
      | 55                 |

  @ios @genericAccount
  Scenario Outline: Verify app Warn Screens for enrolled user for iOS
    Given I am an enrolled "current" user logged into the app
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I relaunch the app
    And I change an app version to <appVersion> on environment selection page
    And I select the environment
    And I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to update the app from the warn screen
    And I navigate to app store screen
    And I validate app name in app store
    And I validate the app version <expectedAppVersion> on app store page
    And I navigate back to app from app store
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to continue without updating from the warn screen
    Then I should be on enter MI page
    When I enter correct MI
    And I accept interstitial during logon
    Then I should be on the home page
    When I navigate to logoff via more menu
    Then I should be on the logout page
    Examples:
      | appVersion         | expectedAppVersion |
      | 55.0               | 58.03              |

  @ios @unEnrolled
  Scenario Outline: Verify app Warn Screens for unenrolled user for iOS
    Given I am a current user
    When I relaunch the app
    And I change an app version to <appVersion> on environment selection page
    And I select the environment
    And I choose an option to continue to the next screen
    And I login with my username and password
    And I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to update the app from the warn screen
    And I navigate to app store screen
    And I validate app name in app store
    And I validate the app version <expectedAppVersion> on app store page
    And I navigate back to app from app store
    And I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to continue without updating from the warn screen
    Then I should be on enter MI page
    When I enter correct MI
    And I finish enrolment
    And I accept interstitial during logon
    Then I should be on the home page
    When I reset the app
    Then I should be on the logout page
    Examples:
      | appVersion         | expectedAppVersion |
      | 55.0               | 58.03              |

  @android @genericAccount
  Scenario Outline: Verify app Ban Screens for enrolled user
    Given I am an enrolled "current" user logged into the app
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I relaunch the app
    And I change an app version to <appVersion> on environment selection page
    And I select the environment
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay
    When I select close button on pre auth call us home page overlay
    And I choose to update the app in the ban screen
    And I navigate to app store screen
    And I navigate back to app from app store
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I choose to use the Mobile Browser Banking
    And I navigate to mobile browser
    Then I validate url from mobile browser
    When I navigate back to app from mobile browser
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    Examples:
      | appVersion         |
      | 53                 |

  @android @unEnrolled
  Scenario Outline: Verify app Ban Screens for Unenrolled user
    Given I am a current user
    When I relaunch the app
    And I change an app version to <appVersion> on environment selection page
    And I select the environment
    And I choose an option to continue to the next screen
    And I login with my username and password
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay
    When I select close button on pre auth call us home page overlay
    And I choose to update the app in the ban screen
    And I navigate to app store screen
    And I navigate back to app from app store
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I choose to use the Mobile Browser Banking
    And I navigate to mobile browser
    Then I validate url from mobile browser
    When I navigate back to app from mobile browser
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    Examples:
      | appVersion         |
      | 53                 |

  @ios @genericAccount
  Scenario Outline: Verify app ban screens for enrolled user for iOS
    Given I am an enrolled "current" user logged into the app
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I relaunch the app
    And I change an app version to <appVersion> on environment selection page
    And I select the environment
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I choose to update the app in the ban screen
    And I navigate to app store screen
    And I validate app name in app store
    And I validate the app version <expectedAppVersion> on app store page
    And I navigate back to app from app store
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I choose to use the Mobile Browser Banking
    And I navigate to mobile browser
    Then I validate url from mobile browser
    When I navigate back to app from mobile browser
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    Examples:
      | appVersion         | expectedAppVersion |
      | 53.0               | 58.03              |

  @ios @unEnrolled
  Scenario Outline: Verify app Ban Screens for Unenrolled user
    Given I am a current user
    When I relaunch the app
    And I change an app version to <appVersion> on environment selection page
    And I select the environment
    And I choose an option to continue to the next screen
    And I login with my username and password
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I choose to update the app in the ban screen
    And I navigate to app store screen
    And I validate app name in app store
    And I validate the app version <expectedAppVersion> on app store page
    And I navigate back to app from app store
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I choose to use the Mobile Browser Banking
    And I navigate to mobile browser
    Then I validate url from mobile browser
    When I navigate back to app from mobile browser
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    Examples:
      | appVersion         | expectedAppVersion |
      | 53.0               | 58.03              |

  # All the below scenarios are marked as '@manual', as they are OS and platform specific
  #only for Android 4.4 users
  @manual
  Scenario: TC_07 Verify OS Version Warn Screens for Android 4.4 enrolled user
    Given I am enrolled NGA user on Android 4.4
    When I launch the app
    Then I should be shown Android OS version 4.4 warn screen
    When I choose to continue in the version warn screen
    Then I should be on enter MI page
    When I enter correct MI
    Then I should be on the home page

  #only for Android 4.4 users
  @manual
  Scenario: TC_08 Verify app Warn Screens for Android 4.4 enrolled user
    Given I am enrolled NGA user on Android 4.4
    And I am using NGA warned app version
    When I launch the app
    Then I should be shown NGA warn screen for Android 4.4
    When I choose to continue in the app warn screen
    Then I should be on enter MI page
    When I enter correct MI
    Then I should be on the home page

  #only for Android 4.4 users
  @manual
  Scenario: TC_09 Verify app Ban Screens for Android 4.4 enrolled user
    Given I am enrolled NGA user on Android 4.4
    And I am using NGA banned app version
    When I launch the app
    Then I should be shown NGA ban screen for Android 4.4
    When I choose to use the Mobile Browser Banking
    Then I should be shown mobile browser
    And I validate url/ui from mobile browser
    When I navigate back to app from mobile browser
    Then I should be shown NGA ban screen for Android 4.4

  # DPLB-420, CAN UPGRADE
  # Prerequisite - This OS warn screen is at last priority as an intersitial screen, So This screen will displayed if no other intersitial are desired to displaued
  @ios @manual
  Scenario: TC_10 Verify OS Warn Screens for customers enrolled on iOS 9/10 device which supports iOS 11/12
    Given I am an enrolled user in iOS 9/10 device that supports iOS 11/12
    When I launch the app
    And I provide my MI/Fingerprint
    Then I should be shown "Can upgrade screen"
    When I choose to continue from the "Can upgrade OS screen"
    Then I should be on the home page

  # DPLB-420, CAN UPGRADE
  @ios @manual
  Scenario: TC_11 Verify OS Warn Screens not displayed for customers enrolled on iOS 9/10 device which supports iOS 11/12 when he launches the apps within 2 days of seeing the OS warn screen
    Given I am an enrolled user in iOS 9/10 device that supports iOS 11/12
    And I had already seen the OS Warn message
    When I launch the app within 2 days of seeing the OS warn screen
    And I provide my MI/Fingerprint
    Then I should be on the home page

  # DPLB-420, CAN UPGRADE
  # Prerequisite - This OS warn screen is at last priority as an intersitial screen, So This screen will displayed if no other intersitial are desired to displaued
  @ios @manual
  Scenario: TC_12 Verify OS Warn Screens for customers enrolled on iOS 9/10 device which supports iOS 11/12 when he launches the app after 2 days of seeing the OS warn screen
    Given I am an enrolled user in iOS 9/10 device that supports iOS 11/12
    And I had already seen the OS Warn message
    When I launch the app after 2 days of seeing the OS warn screen
    And I provide my MI/Fingerprint
    Then I should be shown "Can upgrade OS screen"

  # DPLB-420, CAN'T UPGRADE
  # Prerequisite - This OS warn screen is at last priority as an intersitial screen, So This screen will displayed if no other intersitial are desired to displaued
  @ios @manual
  Scenario: TC_13 Verify OS Warn Screens for customers enrolled on iOS 9/10 device which does not supports iOS 11/12
    Given I am an enrolled user in iOS 9/10 device that does not supports iOS 11/12
    When I launch the app
    And I provide my MI/Fingerprint
    Then I should be shown "Can't upgrade screen"
    When I choose to continue from the "Can't upgrade OS screen"
    Then I should be on the home page

  # DPLB-420, CAN'T UPGRADE
  @ios @manual
  Scenario: TC_14 Verify OS Warn Screens not displayed for customers enrolled on iOS 9/10 device which does not support iOS 11/12 when he launches the apps within 4 days of seeing the OS warn screen
    Given I am an enrolled user in iOS 9/10 device that does not supports iOS 11/12
    And I had already seen the OS Warn message
    When I launch the app within 4 days of seeing the OS warn screen
    And I provide my MI/Fingerprint
    Then I should be on the home page

  # DPLB-420, CAN'T UPGRADE
  # Prerequisite - This OS warn screen is at last priority as an intersitial screen, So This screen will displayed if no other intersitial are desired to displaued
  @ios @manual
  Scenario: TC_15 Verify OS Warn Screens for customers enrolled on iOS 9/10 device which does not supports iOS 11/12 when he launches the app after 4 days of seeing the OS warn screen
    Given I am an enrolled user in iOS 9/10 device that does not supports iOS 11/12
    And I had already seen the OS Warn message
    When I launch the app after 4 days of seeing the OS warn screen
    And I provide my MI/Fingerprint
    Then I should be shown "Can't upgrade screen"

  # DPLB-420, CAN UPGRADE App Warn
  @ios @manual
  Scenario: TC_16 Verify App Warn Screens for customers enrolled on iOS 9/10 device which supports iOS 11/12
    Given I am an enrolled user in iOS 9/10 device that supports iOS 11/12
    And I am using NGA Warned App version
    When I launch the app
    Then I should be shown "Can upgrade WARN screen"
    When I choose to continue from the "Can upgrade WARN screen"
    And I provide my MI/Fingerprint
    Then I should be on the home page

  # DPLB-420, CAN't UPGRADE App Warn
  @ios @manual
  Scenario: TC_17 Verify App Warn Screens for customers enrolled on iOS 9/10 device which does not supports iOS 11/12
    Given I am an enrolled user in iOS 9/10 device that does not supports iOS 11/12
    And I am using NGA Warned App version
    When I launch the app
    Then I should be shown "Can't upgrade WARN screen"
    When I choose to continue from the "Can't upgrade WARN screen"
    And I provide my MI/Fingerprint
    Then I should be on the home page

  # DPLB-420, CAN UPGRADE App Ban
  @ios @manual
  Scenario: TC_18 Verify App Ban Screens for customers enrolled on iOS 9/10 device which supports iOS 11/12
    Given I am an enrolled user in iOS 9/10 device that supports iOS 11/12
    And I am using NGA Banned App version
    When I launch the app
    Then I should be shown "Can upgrade BAN screen"
    When I choose to continue "Go to our Mobile site"
    Then I should be shown mobile browser
    And I validate url/ui from mobile browser

  # DPLB-420, CAN't UPGRADE App Ban
  @ios @manual
  Scenario: TC_19 Verify App ban Screens for customers enrolled on iOS 9/10 device which does not supports iOS 11/12
    Given I am an enrolled user in iOS 9/10 device that does not supports iOS 11/12
    And I am using NGA Banned App version
    When I launch the app
    Then I should be shown "Can't upgrade BAN screen"
    When I choose to continue "Go to our Mobile site"
    Then I should be shown mobile browser
    And I validate url/ui from mobile browser
