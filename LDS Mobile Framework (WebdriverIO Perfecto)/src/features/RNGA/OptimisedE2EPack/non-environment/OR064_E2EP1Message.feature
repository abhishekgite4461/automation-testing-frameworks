@team-pi @nga3 @rnga @stubOnly @stub-enrolled-valid @optFeature

Feature: In order to show priority messages to customers in the mobile app
  As a product owner I want a priority placement to be created
  Note: This story is to create the placement with the prescribed behaviours. Any Test specific behaviours will be created as part of each AB test story

  #These scenarios are stubOnly since there is an interaction between 3rd party provider Adobe target.
  Background:
    Given I am an enrolled "current" user logged into the app

  #PI-569 and  #PI-474
  @ab-priority-message~normal
  Scenario:AC01 Clicking the dismiss button
    Then I should see normal placement in between the header and my first account tile
    When I navigate away from the home page and come back
    Then I should see normal placement in between the header and my first account tile
    When I select the priority dismiss button
    Then the placement should not be displayed in the top of the home page

  @ab-priority-message~callToAction
  Scenario:AC02 Group B dismissing the app upgrade message
    Then I should see Call to action placement in between the header and my first account tile
    When I select the priority dismiss button
    Then the app upgrade message should disappear for that session
