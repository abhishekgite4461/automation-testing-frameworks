@team-statements @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-enrolled-valid @otherAccount @ios
  # separate feature file is created for android for all the scenarios with condensed account tile changes.
  # Will be common scenarios for both once condensed account tile is implemented in iOS
Feature: E2E_verify RNGA Mortgage transactions

  #MPT-3026 Offset and Repossessed Mortgages are difficult to be sourced
  #Remove @hfx once the data is identified for other brands
  @hfx
  Scenario Outline: TC_01 Repossessed Mortgage account tile on home screen
    Given I am a repossessed mortgage user on the home page
    When I navigate and validate the options in the <type> account in below table in the home page
      | accountFields          |
      | mortgageAccountNumber  |
      | mortgageAccountBalance |
    And I select <type> account tile from home page
    Then I should be displayed the following fields of <type> account
      | fieldsForMortgage                        |
      | offsetRepossessedMortgageName            |
      | offsetRepossessedMortgageNumber          |
      | offsetRepossessedMortgageBalance         |
      | offsetRepossessedMortgageMonthlyPayment  |
      | offsetRepossessedMortgageBalanceAsAtDate |
    And I should be displayed <field> for rePossessedMortgage account
    And I should not be displayed any summary and sub-account details tabs
    And I should see a <message> on the statement page
    Examples:
      | type                | message            | field          |
      | rePossessedMortgage | rePossessedMessage | alertMessage   |

  Scenario Outline: TC_02 Offset Mortgage account tile on home screen
    Given I am a offset mortgage user on the home page
    When I navigate and validate the options in the <type> account in below table in the home page
      | accountFields          |
      | mortgageAccountNumber  |
      | mortgageAccountBalance |
    And I select <type> account tile from home page
    Then I should be displayed the following fields of <type> account
      | fieldsForMortgage                        |
      | offsetRepossessedMortgageName            |
      | offsetRepossessedMortgageNumber          |
      | offsetRepossessedMortgageBalance         |
      | offsetRepossessedMortgageMonthlyPayment  |
      | offsetRepossessedMortgageBalanceAsAtDate |
    And I should be displayed <field> for rePossessedMortgage account
    And I should not be displayed any summary and sub-account details tabs
    And I should see a <message> on the statement page
    Examples:
      | type                | message            | field          |
      | offSetMortgage      | offSetMessage      | noAlertMessage |
