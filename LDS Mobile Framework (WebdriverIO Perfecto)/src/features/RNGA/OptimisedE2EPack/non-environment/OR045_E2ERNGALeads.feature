@team-pi @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stubOnly @instrument

Feature: E2E_Validate the ASM, Banner leads

  #DPL-945, DPL-3511
  @stub-accountlink-valid
  Scenario: TC_01 Display account link on the current account tile
    Given I am an enrolled "current" user logged into the app
    When I navigate to islamicCurrent account on home page
    Then I see the Account Link lead placement in the account tile of islamicCurrent
    When I select islamicCurrent account tile from home page
    Then I see the islamicCurrent Account Link lead in the statement page

  #DPL-3014
  @stub-accountlink-valid @pending
  Scenario: TC_01 Display account link on the current account tile
    Given I am an enrolled "current" user logged into the app
    When I navigate to islamicCurrent account on home page
    Then I see the Account Link lead placement in the account tile of islamicCurrent
    And I should not see full width design for the account tile of islamicCurrent

  @stub-banner-payment-success
  Scenario: TC_02 Display banner on payment success page
    Given I am an enrolled "current" user logged into the app
    When there is a banner lead configured
    And I navigate to payment success page
    Then I should be able to see the banner lead placement on payment success page

  @stub-big-prompt
  Scenario: TC_03 Display Big prompt lead
    When I submit "correct" MI as "current" user on the MI page
    And an interstitial lead is displayed
    Then I must not see the Global Menu in the lead screen
    When I select the active native button at the bottom of the screen
    Then I should be on the home page

  @stub-bigprompt-takeover
  Scenario: TC_04 Display Big Prompt Take Over
    When I submit "correct" MI as "current" user on the MI page
    And an interstitial lead is displayed
    Then I must not see the Global Menu in the lead screen
    And I navigate out of the lead by selecting the options in the lead screen

  @stub-single-dpn
  Scenario: TC_05 Native Button gets enabled on the Leads page on scrolling
    When I submit "correct" MI as "current" user on the MI page
    And an interstitial lead is displayed
    And I scroll the dpn lead to the bottom
    Then the native button should be in enabled mode

  #DPL-947
  @stub-accountlink-valid @pending
  Scenario: TC_01 Display account link on the savings account tile
    Given I am an enrolled "savings account" user logged into the app
    When I navigate to saving account on home page
    Then I see the Account Link lead placement in the account tile of saving
    When I select saving account tile from home page
    Then I see the saving Account Link lead in the statement page

  #DPL-1006
  @stub-accountlink-valid @pending
  Scenario: TC_01 Display account link on the creditcard account tile
    Given I am an enrolled "creditcard" user logged into the app
    When I navigate to creditcard account on home page
    Then I see the Account Link lead placement in the account tile of creditcard
    When I select creditcard account tile from home page
    Then I see the creditcard Account Link lead in the statement page

  #DPL-1003
  @stub-accountlink-valid @pending
  Scenario: TC_01 Display account link on the isa account tile
    Given I am an enrolled "isaAccount" user logged into the app
    When I navigate to isaAccount account on home page
    Then I see the Account Link lead placement in the account tile of isaAccount
    When I select isaAccount account tile from home page
    Then I see the isaAccount Account Link lead in the statement page

  #DPL-1008
  @stub-accountlink-valid @pending
  Scenario: TC_01 Display account link on the cbs loan account tile
    Given I am an enrolled "cbsLoan" user logged into the app
    When I navigate to cbsLoan account on home page
    Then I see the Account Link lead placement in the account tile of cbsLoan
    When I select cbsLoan account tile from home page
    Then I see the cbsLoan Account Link lead in the statement page

  #DPL-1004
  @stub-accountlink-valid @pending
  Scenario: TC_01 Display account link on the htb isa account tile
    Given I am an enrolled "htbIsa" user logged into the app
    When I navigate to htbIsa account on home page
    Then I see the Account Link lead placement in the account tile of htbIsa
    When I select htbIsa account tile from home page
    Then I see the htbIsa Account Link lead in the statement page

  #DPL-1007
  @stub-accountlink-valid @pending
  Scenario: TC_01 Display account link on the non cbs loan account tile
    Given I am an enrolled "nonCBSLoan" user logged into the app
    When I navigate to nonCBSLoan account on home page
    Then I see the Account Link lead placement in the account tile of nonCBSLoan
    When I select nonCBSLoan account tile from home page
    Then I see the nonCBSLoan Account Link lead in the statement page

  #DPL-1009
  @stub-accountlink-valid @pending
  Scenario: TC_01 Display account link on the mortgage account tile
    Given I am an enrolled "mortgage" user logged into the app
    When I navigate to mortgage account on home page
    Then I see the Account Link lead placement in the account tile of mortgage
    When I select mortgage account tile from home page
    Then I see the mortgage Account Link lead in the statement page

  #DPL-945, 3511, 3014
  #ToDo: When the Lead flag is off
  @stub-accountlink-valid @pending
  Scenario: TC_01 Display account link on the current account tile
    Given I am an enrolled "current" user logged into the app
    When I navigate to islamicCurrent account on home page
    Then I should not see the Account Link lead placement in the account tile of islamicCurrent
    And I should see full width design for the account tile of islamicCurrent
