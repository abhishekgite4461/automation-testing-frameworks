@team-triage @mbnaSuite @nga3 @rnga @environmentOnly @uatOnly @optFeature @pending

#REASONS FOR MANUAL EXECUTION :
#non availability of test data in FT environment

#MOB3-734
Feature: User wants to access Inbox and view unread correspondence from the bank
  In order to access correspondence from the bank
  As a RNGA Authenticated Customer
  I want to view my Inbox

  # Note : For MBNA a "current" user will return a "credit card" user

  Background:
    Given I am a current user on the home page

  @android
  Scenario: TC_01 User Opens a message in Inbox Webview via More menu
    When I navigate to inbox page via more menu
    And I choose a correspondence option from the inbox page
    Then I should be taken to the message
    When I choose to download the correspondence as a PDF from the inbox page
    Then PDF is downloaded to my device
    And I should see the notification counter updated in more menu according to how many unread items I have

  @ios
  Scenario: TC_02 User Opens a message in Inbox Webview via More menu
    When I navigate to inbox page via more menu
    And I choose a correspondence option from the inbox page
    Then I should be taken to the message
    When I choose to view PDF from the inbox page
    Then PDF is opened on my device to view with close button
    And I close the PDF by clicking the close button
