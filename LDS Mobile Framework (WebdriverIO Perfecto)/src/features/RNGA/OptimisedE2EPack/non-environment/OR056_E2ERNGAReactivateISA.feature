@team-payments @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-valid-fallow-isa @exhaustiveData
Feature: Reactivate fallow ISA

  # PAYMOB-1218
  Scenario: TC_01 Selecting Reactive ISA option from the Alert of Account tile
    Given I am a htb isa user on the home page
    When I choose reactivate ISA option from fallowIsa account tile in home page
    Then I should be on the reactivate ISA declaration page
    When I choose to agree declaration and reactivate ISA from reactivate your ISA page
    And I enter a valid password on reactivate ISA password dialog panel
    Then I am on Reactivate ISA success screen
    When I select dismiss modal using close button
    And I navigate to logoff via more menu
    Then I should be on the logout page

  Scenario: TC_02 Display Reactivate ISA success page with the top up isa journey
    Given I am a htb isa user on the home page
    And I select fallowIsa account tile from home page
    And I select the top up isa option from the action menu in statements page
    And I enter 10 in the amount field in the payment hub page
    And I select Continue button on payment hub page
    And I select the confirm option in the review transfer page
    And I select the reactivate ISA option from the reactivate ISA interstitial page
    When I choose to agree declaration and reactivate ISA from reactivate your ISA page
    And I enter a valid password on reactivate ISA password dialog panel
    Then I should be on the review transfer page
    When I select dismiss modal using close button
    And I select leave option from winback
    And I navigate to logoff via more menu
    Then I should be on the logout page

  Scenario: TC_03 Success reactivating ISA
    Given I am a htb isa user on the home page
    When I select fallowIsa account tile from home page
    And I select the action menu from statements page
    And I see option to reactivate ISA journey
    Then I should see below details in declaration page
      | details                                |
      | title                                  |
      | reactivateIsaAccountNameText           |
      | reactivateIsaAccountSortCode           |
      | reactivateIsaAccountNumber             |
      | reactivateIsaAccountHolderName         |
      | reactivateIsaAccountHolderAddress      |
      | reactivateIsaAccountHolderDateOfBirth  |
      | reactivateIsaNationalInsuranceNo       |
      | reactivateIsaUpdateAddressLinkText     |
      | reactivateIsaUpdateNiNumberLinkText    |
    When I choose to agree declaration and reactivate ISA from reactivate your ISA page
    Then I should be prompted to enter my password
    When I enter a valid password on reactivate ISA password dialog panel
    Then I should see reactivation details in ISA Reactivation Success page
      | details                               |
      | reactivateIsaSupportingCopy           |
      | reactivateIsaAccountNameText          |
      | reactivateIsaAccountSortCode          |
      | reactivateIsaAccountNumber            |
      | reactivateIsaRemainingAllowanceText   |
      | reactivateIsaRemainingAllowanceAmount |
    And I should see instructional message
