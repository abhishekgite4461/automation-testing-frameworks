@team-statements @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-enrolled-valid
Feature: E2E_view RNGA current and credit card posted transaction details
  #reason for manual & stubOnly: Cannot get the test data for most of the transaction types. We can test only with live accounts/Stubs

  @stubOnly
  Scenario Outline: TC_01 User views Posted Transaction Details for current account payment transactions
    Given I am a current user on the home page
    When I select current Account With View Transactions account tile from home page
    And I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | vtdCloseButton            |
      | date                      |
      | vtdAmount                 |
      | <typeOfTransaction>       |
      | transactionDescription    |
      | transferPaymentsButton    |
      | unsureTransactionButton   |
    Examples:
      | transactionType            | typeOfTransaction        |
      | faster Payments Incoming   | fasterPaymentsInText     |
      | mobile Payments Inbound    | mobilePaymentInText      |
      | bank Giro Credit           | bankGiroCreditText       |
      | cash Deposited In IDM      | cashText                 |

  @stubOnly
  Scenario Outline: TC_02 User views Posted Transaction Details for current account payment transactions
    Given I am a current user on the home page
    When I select current Account With View Transactions account tile from home page
    And I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | vtdCloseButton            |
      | date                      |
      | vtdAmount                 |
      | <typeOfTransaction>       |
      | transferPaymentsButton    |
      | unsureTransactionButton   |
      | <transactionDetailField1> |
      | <transactionDetailField2> |
    Examples:
      | transactionType            | typeOfTransaction        |  transactionDetailField1 | transactionDetailField2   |
      | faster Payments Outgoing   | fasterPaymentsOutText     | dateOfTransaction       | timeOfTransaction         |
      | mobile Payments Outgoing    | mobilePaymentOutText      | dateOfTransaction       | timeOfTransaction         |
      | transfersOut           | transferText       | sortCode                | accountNumber              |

  @stubOnly
  Scenario Outline: TC_03 User views Posted Transaction Details for current account debit card transactions
    Given I am a current user on the home page
    When I select current Account With View Transactions account tile from home page
    And I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | <typeOfTransaction>          |
      | payeeField                   |
      | dateOfTransaction            |
      | cardEndingNumber             |
    Examples:
      | transactionType            | typeOfTransaction         |
      | chip And Pin Purchase      | chipAndPinText            |
      | online Debit Card          | onlinePhoneMailText       |

  @stubOnly
  Scenario: TC_04 View Standing Order Posted Transaction Details fields
    Given I am a current user on the home page
    When I select current Account With View Transactions account tile from home page
    And I select standing Order on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | date                      |
      | vtdCloseButton            |
      | vtdAmount                 |
      | unsureTransactionButton   |
      | manageStandingOrderButton |
      | standingOrderText         |
      | payeeField                |

  @otherAccount
  Scenario: TC_05 View Direct Debit Posted Transaction Details fields
    Given I am a statement valid user on the home page
    When I select current Account With View Transactions account tile from home page
    And I tap on search icon in all tab
    And I enter the EON Energy in search dialog box
    And I select direct Debit on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen      |
      | date                      |
      | vtdCloseButton            |
      | vtdAmount                 |
      | unsureTransactionButton   |
      | viewDirectDebit           |
      | directDebitText           |
      | payeeField                |
      | referenceDirectDebit      |

  @stubOnly @mbnaSuite
  Scenario Outline: TC_06 View Credit Card Posted Transaction Details
    Given I am a current user on the home page
    When I select credit Card Account With View Transactions account tile from home page
    And I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | <typeOfTransaction>          |
      | payeeField                   |
      | cardEndingNumber             |
      | dateOfTransaction            |
    Examples:
      | transactionType                             | typeOfTransaction         |
      | online Credit Card                          | onlinePhoneMailText       |
      | contact Less Purchase Credit Card           | contactLessPurchaseText   |
      | chip And Pin Purchase Credit Card           | chipAndPinText            |
      | direct Debit In Card                        | creditCardText            |
      | cash Withdrawal ATM Credit Card             | chipAndPinText            |
      | charges Credit Card                         | creditCardText            |
      | interest Credit Card                        | creditCardText            |

  @noExistingData @mbnaSuite @manual
  # cannot be tested as we cannot get test data for the below transactions
  Scenario: TC_07 View Chip and PIN Card Purchase Posted Transaction Details fields (Chip and PIN Purchase Cash Back)
    Given I am a current user on the home page
    And I select credit Card Account With View Transactions account tile from home page
    When I select chip And Pin Purchase Cash Back Credit Card on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | chipAndPinText               |
      | cashBack                     |
      | payeeField                   |
      | cardEndingNumber             |
      | dateOfTransaction            |

  @noExistingData @mbnaSuite @manual
  # cannot be tested as we cannot get test data for the below transactions
  Scenario Outline: TC_08 View Credit Card Posted Transaction Details
    Given I am a current user on the home page
    And I select credit Card Account With View Transactions account tile from home page
    When I select <transactionType> on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | creditCardText               |
      | payeeField                   |
      | cardEndingNumber             |
      | dateOfTransaction            |
    Examples:
      | transactionType                             |
      | money Transfer Credit Card                  |
      | cheque Payment                              |
      | electronic Payment                          |
      | balance Transfer                            |
      | refunds                                     |
      | rewards                                     |

  @noExistingData @mbnaSuite @stubOnly
  # cannot be tested as we cannot get test data for the below transactions
  Scenario: TC_09 View Credit Card Posted Transaction Details for cheque deposit
    Given I am a current user on the home page
    And I select credit Card Account With View Transactions account tile from home page
    When I select cheque Payment on statements page
    Then I should see posted transaction details for that transaction with the following fields
      | fieldsInDetailScreen         |
      | date                         |
      | vtdCloseButton               |
      | vtdAmount                    |
      | unsureTransactionButton      |
      | creditCardText               |
      | payeeField                   |
      | cardEndingNumber             |
      | dateOfTransaction            |
