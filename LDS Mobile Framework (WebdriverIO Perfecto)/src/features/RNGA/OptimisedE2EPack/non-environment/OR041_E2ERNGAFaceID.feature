@team-login @mbnaSuite @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @manual @optFeature @primary
#Requires regular faceID updates, hence manual
Feature: Login Face ID
  As an RNGA User being Opted into face id on an eligible device
  I want to see the face id authentication to login
  and also should be able to cancel the face id authentication

  # Note : For MBNA a "current" user will return a "credit card" user

  Scenario: TC_01 Opted in Customer who has deleted and added their face logs in and shown MI
    Given I am an RNGA customer on an eligible device/OS and opted in to Face ID
    And I have deleted and then added (changed) my Face ID off the device
    When I launch the app
    And I login using Face ID
    Then I must see a Face Changed pop up and asked to do MI
    When I enter correct MI
    Then I should see risk pop up
    When I select "Yes" on the risk pop up
    Then I should be on the home page
