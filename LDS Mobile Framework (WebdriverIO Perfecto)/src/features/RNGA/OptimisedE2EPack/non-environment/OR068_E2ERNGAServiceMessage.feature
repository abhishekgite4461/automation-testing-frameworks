@team-pi @nga3 @rnga @stubOnly @stub-enrolled-valid @manual @primary
Feature: In order to show priority service messages to customers in the mobile app
  As a product owner
  I want to add some pre-approved messages to the app
  So that I can notify customers when there is a service outage irrespective of the data consent of the users

  #These scenarios are stubOnly since there is an interaction between 3rd party provider Adobe target.
  Background:
    Given I am an enrolled "current" user logged into the app

  #PidI-564 and  #PIDI-565
  @ab-priority-message~normal
  Scenario:AC01 User can view and select pre-approved messages
    Then I should see a pre-approved message
    And I should be able to select pre-approved message

  @ab-priority-message~plannedServiceMessage
  Scenario:AC02 Planned outage message
    Then I should see a message in the P1 placement with a CTA saying
      |'We’re working on our app'|
    When I select on the stay option on the winback
    Then I should see a message in the P1 placement with a CTA saying

  @ab-priority-message~unplannedServiceMessage
  Scenario:AC03 Unplanned outage message
    Then I should see a message in the P1 placement with a CTA saying
      |'Some parts of our app aren’t working at the moment. Find out more'.|
    When I select on the stay option on the winback
    Then I should see a message in the P1 placement with a CTA saying

  @ab-priority-message~serviceRestored
  Scenario:AC04 Service restored message
    Then I should see a message in the P1 placement saying
      |'Our app is working as usual'|
