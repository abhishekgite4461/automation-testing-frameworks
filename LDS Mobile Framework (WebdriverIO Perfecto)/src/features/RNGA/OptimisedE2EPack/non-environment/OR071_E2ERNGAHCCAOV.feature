@team-aov @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @stub-rnga-hcc @sw-hcc-ios-on @sw-hcc-android-on @otherAccount @sw-overdraft-remaining-ios-on @sw-overdraft-remaining-android-on

Feature: E2E High Cost Of Credit
  As a NGA Retail customer
  I want my account to display correct labels and fields with regards to overdraft
  So that I am always informed of the limit of my overdraft facility

  Scenario: TC_01 Display Home page current account where customer has fully used their overdraft limit
    Given I am an enrolled "current_with_fully_used_overdraft_limit" user logged into the app
    And I navigate and validate the options in the completeUsedOverdraft account in below table in the home page
      | accountFields      |
      | accountTileLogo    |
      | accountNumber      |
      | sortCode           |
      | accountBalance     |
      | overDraftLimit     |
      | RemainingOverdraft |

  Scenario: TC_02 Display Home page current account where Customer has partially used their overdraft limit
    Given I am an enrolled "current_with_partially_used_overdraft_limit" user logged into the app
    And I navigate and validate the options in the partialUsedOverdraft account in below table in the home page
      | accountFields      |
      | accountTileLogo    |
      | accountNumber      |
      | sortCode           |
      | accountBalance     |
      | overDraftLimit     |
      | RemainingOverdraft |

  Scenario: TC_03 Display Home page current account where Customer has not used their overdraft limit
    Given I am an enrolled "current_with_not_used_overdraft_limit" user logged into the app
    And I navigate and validate the options in the noUsedOverdraft account in below table in the home page
      | accountFields      |
      | accountTileLogo    |
      | accountNumber      |
      | sortCode           |
      | accountBalance     |
      | overDraftLimit     |
    Then I should not see the Remaining Overdraft field in the account tile
