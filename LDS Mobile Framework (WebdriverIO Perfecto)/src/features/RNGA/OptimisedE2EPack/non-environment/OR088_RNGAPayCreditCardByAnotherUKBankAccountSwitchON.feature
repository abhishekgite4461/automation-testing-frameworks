@team-payments @nga3 @rnga @mbnaSuite @ff-pay-credit-card-by-master-card-enabled-on @sw-pay-credit-by-master-card-on @stub-enrolled-valid
Feature: Pay credit card by another UK bank account
  In order to pay my credit card
  As a RNGA customer
  I want to be use a another any UK bank account to pay my credit card

  Scenario Outline: TC_01 Main Success Scenario: Pay Credit Card by Another UK Bank Account journey
    Given I am a Current and credit card user on the home page
    And I am on the payment hub page
    When I choose the from account as anotherUKBankAccount and to account as masterCreditCard in the payment hub page
    Then I am not shown payment date in the payment hub
    When I enter amount greater than the balance of the credit card account in the payment hub page
    Then I should see credit card payment validation error message
    When I dismiss the error message
    And I submit the pccmc with all the mandatory fields with corresponding amount value 2.00
    Then I should be on select your bank account page
    When I select another UK <bankAccount> from the list displayed
    Then I should be on confirm payment page
    When I select check box option to confirm the payment on confirm payment page
    And I select the confirm button on confirm payment page
    And I select the dialog box continue button to finsh your payment
    Examples:
      | bankAccount|
      | Barclays   |
