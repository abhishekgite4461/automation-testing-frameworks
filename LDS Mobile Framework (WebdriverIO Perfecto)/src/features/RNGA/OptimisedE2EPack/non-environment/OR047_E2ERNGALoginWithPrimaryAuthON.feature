@team-login @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature
Feature: Traditional login using primary auth with 'Device Enrolment' and 'Light Logon' switches ON

  #MOB3-104 #MOB-1796
  @stub-incorrect-credentials @unEnrolled
  Scenario: TC_02 Login to RNGA app with an incorrect or incomplete username/password
    Given I am a current user
    When I navigate to the login screen
    And I login with an incorrect username "garbage"
    Then I should see an incorrect username or password error message
    When I login with an incorrect password "garbage"
    Then I should see an incorrect username or password error message
    When I enter the password "incor"
    Then I should see the continue button is disabled
    When I enter the username ""
    Then I should see the continue button is disabled
    When I clear the username and password fields
    And I login with my username and password
    And I select FSCS tile in MI page
    And I select leave app in MI page
    Then I should see mobile banking page

  @stub-incorrect-credentials @unEnrolled @mbna
  Scenario: TC_03 Login to RNGA app with an incorrect or incomplete username/password
    Given I am a current user
    When I navigate to the login screen
    And I login with an incorrect username "garbage"
    Then I should see an incorrect username or password error message
    When I login with an incorrect password "garbage"
    Then I should see an incorrect username or password error message
    When I enter the password "incor"
    Then I should see the continue button is disabled
    When I enter the username ""
    Then I should see the continue button is disabled

  @unEnrolled @environmentOnly
  Scenario: TC_04 Login to RNGA app with a incorrect password consecutively for 6 times so that the account is revoked
    Given I am a current_revoke user
    When I navigate to the login screen
    And I login with an incorrect password 5 times
    Then I should be on revoked error page

  @stub-revoked-password @unEnrolled @mbnaSuite
  Scenario: TC_05 Login to RNGA app with a revoked password
    Given I am a revoked user
    When I navigate to the login screen
    And I login with my username and password
    Then I should be on revoked error page
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  #Need to configure the jailbroken device for this scenario: Device limitation
  @manual
  Scenario Outline: TC_08 Login to RNGA app in jailbroken/rooted mobile device
    Given I am a "<userType>" user
    When I navigate to the login screen
    And I login with my username and password
    Then I should be on jailbroken/rooted error page
    Examples:
      | userType              |
      | Malware               |
      | Tampered              |
      | Jailbroken/Rooted     |
      | Data on App/OS Version|

  #MOB3-105
  #TODO: MQE-833 - Removing @mbnasuite until this gets fixed.
  @stub-suspended @unEnrolled
  Scenario: TC_09 Login to RNGA app with suspended user
    Given I am a suspended user
    When I navigate to the login screen
    And I login with my username and password
    Then I should be on suspended error page
    And I should see the telephone number displayed in the error page
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  #MOB3-105 #MOB3-2019
  @stub-inactive @unEnrolled
  Scenario: TC_11 Login User’s mandate is inactive
    Given I am a inactive user
    When I navigate to the login screen
    And I login with my username and password
    And I should be on inactive error page
    Then I select the contact us option from the error page
    And I should be on pre auth call us home page overlay
    And I select close button on pre auth call us home page overlay
    And I should be on inactive error page
    And I click on Logon to Mobile Banking button in the error page
    And I should be on the environment selector page

  #MOB3-105
  @stub-mandateless @unEnrolled
  Scenario: TC_12 Login to RNGA app without a mandate
    Given I am a mandateless user
    And I navigate to the login screen
    When I login with my username and password
    Then I should see a not registered to IB error message in login page

  #MOB-262
  @stub-compromised-password @unEnrolled
  Scenario: TC_13 Login to RNGA app with compromised password
    Given I am a compromised password user
    When I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    Then I should see a compromised password error page
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  @stub-valid @unEnrolled
  Scenario: TC_14 Allow the user to reset the password in RNGA
    Given I am a current user
    When I navigate to the login screen
    And I select the forget your login link
    Then I should see forget username page in the mobile browser

  #MOB3-1796
  @stub-valid @unEnrolled
  Scenario: TC_15 Validate the password field is masked
    Given I am a current user
    When I navigate to the login screen
    And I enter my username and password
    Then I should see the password field is masked

  @stub-valid @unEnrolled @mbnaSuite
  Scenario: TC_023_Should be able to Copy and paste contents in username field
    Given I am a current user
    When I navigate to the login screen
    And I enter the username "user"
    And I copy the content in the "userId" field
    And I should be able to paste option in the userId field

  @stub-valid @unEnrolled @mbnaSuite
  Scenario: TC_024_User cannot copy the password field content
    Given I am a current user
    When I navigate to the login screen
    And I enter the password "pass"
    Then I should not be able to see copy option after longpress in the password field

  # Following scenario is iOS only as password field doesn't retrieve any values to validate in android
  @stub-valid @ios @unEnrolled @mbnaSuite
  Scenario: TC_025_User should be able to only paste contents in password field
    Given I am a current user
    When I navigate to the login screen
    And I enter the username "user"
    And I copy the content in the "userId" field
    And I paste the content in the "password" field
    Then I should see the password field populated with 4 characters
