@team-login @mbnaSuite @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature
#MOB3-173
Feature: User wants to Re-set password
  In order to reset my password
  As a RNGA authenticated customer
  I want to view my reset password page

   # Note : For MBNA a "current" user will return a "credit card" user

  @stub-reset-password @genericAccount
  Scenario: TC_01 User successfully changes password
    Given I am an enrolled "current" user logged into the app
    And I navigate to the re-set password page
    When I enter my new valid password in the reset password page
    And I select the submit button in the reset password page
    Then I should see a password updated pop up in the reset password page
    And I should be on the logout page

  #it will corrupt and deplete test data, hence moving it to manual.
  @otherAccount
  Scenario: TC_02 User Enters non-identical password HC-010, previous password used in the last five instances MG-515
    Given I am an enrolled "past password" user logged into the app
    And I navigate to the re-set password page
    When I enter two non identical password in the reset password page
    And I select the submit button in the reset password page
    Then I should see a non-identical error message on the reset password page
    And I should not see a password updated pop up in the reset password page
    When I dismiss the server error message
    And I enter new password which I have used in last five instances
    And I select the submit button in the reset password page
    Then I should see a last 5 password error banner on the reset password page
    And I should not see a password updated pop up in the reset password page

  @otherAccount @environmentOnly
  Scenario: TC_003 User enters password which is their name MG-3418, birthday MG-3418, the same as Memorable information MG-265
    Given I am an enrolled "personal detail password" user logged into the app
    When I navigate to the re-set password page
    And I enter my valid personal name detail in the reset password page
    And I select the submit button in the reset password page
    Then I should see a no personal details error banner on the reset password page
    And I should not see a password updated pop up in the reset password page
    When I dismiss the server error message
    And  I enter my valid birthday detail in the reset password page
    And I select the submit button in the reset password page
    Then I should see a no personal details error banner on the reset password page
    And I should not see a password updated pop up in the reset password page
    And I dismiss the server error message

  @genericAccount
  Scenario: TC_004 User enters new password that is too simple e.g ‘password1’ or ‘abcd1234’ MG-3419
    Given I am an enrolled "current" user logged into the app
    And I navigate to the re-set password page
    When I enter a non secure password password1
    And I select the submit button in the reset password page
    Then I should see an easy-to-guess passwords error banner on the reset password page
    And I should not see a password updated pop up in the reset password page
