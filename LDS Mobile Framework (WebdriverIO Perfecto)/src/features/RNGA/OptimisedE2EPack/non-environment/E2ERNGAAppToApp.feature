@team-login @nga3 @rnga @manual
Feature: RNGA users will provide consent to view LBG accounts in TPP app(AISP)
  In order to view my Current account in TPP
  As a TPP and LBG App user
  I want ability to give consent via Retail NGA app to view my account in TPP

# Preconditions for App to App
# App to App Consent - IBC switch is ON & Feature flag is ON
# User has TPP app installed on same device and RNGA App is launched using TPP app
# User is not shown app sign for pending transaction
# User is not shown Touch ID registration prompt
# User is not shown What's new screens
#------------------------------------------- Main Success scenario-----------------------------------------------------------------#

  Scenario: TC_001_Main Success Scenario: Customer is displayed the MI screen in RNGA app after launching the app from TPP
    Given I am an enrolled RNGA customer
    And I have 3rd Party App (TPP) installed on same device as RNGA app
    And RNGA app universal links are configured
    And 'App to App consent' switch is ON
    When I choose to give consent to view my account from TPP
    Then my RNGA app should be launched
    And I should be displayed the MI/biometric screen

  Scenario: TC_002_Main Success Scenario: User gives consent in CWA screen in the App to view Current accounts in TPP
    Given I am a valid enrolled current account RNGA customer
    And I have successfully logged into RNGA app after launching from TPP
    And I am displayed the Account Consent CWA screen
    When I choose to grant the consent
    Then the CWA screen should be closed
    And I should be displayed a message on Native screen regarding progress (Finishing up spinner)
    And I should be redirected to the TPP app where this consent journey started
    And I should be logged out of my Lloyds app

  Scenario: TC_003_Variation Scenario: User decline consent to view Current accounts in TPP
    Given I am a valid enrolled current account RNGA customer
    And I have successfully logged into RNGA app after launching from TPP
    And I have been displayed Consent screen (CWA) with list of my current accounts
    When I choose to decline the consent to view the selected accounts
    Then the CWA screen will show Cancelling spinner
    And CWA should be closed
    And I should be displayed a Native interstitial screen briefly
    And I should be redirected to the TPP app where this consent journey started
    And I should be logged out of my Lloyds app

  Scenario: TC_004_Main Success Scenario: User approves payment in the App initiated via TPP
    Given I am a valid enrolled current account RNGA customer
    And I have successfully logged into RNGA app after launching from TPP in a payment journey
    And I am displayed the Payment Consent CWA screen
    When I choose to grant the consent
    Then the CWA screen should be closed
    And I should be displayed a message on Native screen regarding progress (Finishing up spinner)
    And I should be redirected to the TPP app where this consent journey started
    And I should be logged out of my Lloyds app

# Note – Payment can be single domestic, future domestic, Standing order.
#------------------------------------------- Exceptional Success scenario-----------------------------------------------------------------#

  Scenario: TC_005_Exception Scenario: Logout User from RNGA when consent is initiated from TPP, then LBG app should be first logged out and login should be initiated with App to App context
    Given I am an enrolled RNGA customer already logged into the app
    And I have 3rd Party App (TPP) installed on same device as RNGA app
    When I choose to give consent to view my account from TPP
    Then my RNGA app should be launched
    And I should be logged off from the current session
    And logout process should be seamless
    And I should be shown the MI/biometric screen

  Scenario:TC_006_Exception Scenario: Customer is displayed the Error Page Logged Out screen in RNGA app after launching the app from TPP
    Given I am an enrolled RNGA customer
    And I have 3rd Party App (TPP) installed on same device as RNGA app
    And RNGA app universal links are configured
    And 'App to App consent' switch is ON
    And  RNGA app is launched from TPP
    When the RNGA app launch fails due to error
    Then I should be displayed Error page Logged Out
    And I should be given an additional option to go back to TPP from the Error page

  Scenario: TC_007_Exception Scenario: Customer is displayed the App Warn/Ban screen in RNGA app after launching the app from TPP
    Given I am an enrolled RNGA customer
    And I have 3rd Party App (TPP) installed on same device as RNGA app
    And RNGA app universal links are configured
    And 'App to App consent' switch is ON
    And RNGA app is launched from TPP
    When the RNGA app launch fails due to app ban/warn
    Then I should be displayed App Warn/Ban error page
    And I should be given an additional option to go back to TPP from the Error page

  Scenario: TC_008_Exception Scenario: Customer is displayed the App Launch fail screen in RNGA app after launching the app from TPP
    Given I am an enrolled RNGA customer
    And I have 3rd Party App (TPP) installed on same device as RNGA app
    And RNGA app universal links are configured
    And 'App to App consent' switch is ON
    And RNGA app is launched from TPP
    When the RNGA app launch fails due to technical reason/loss of internet connectivity
    Then I should be displayed App Launch Fail screen
    And I should be given an additional option to go back to TPP from the Error page

  Scenario: TC_009_Exception Scenario: User select an option to go back to TPP from Error screens
    Given I am on Error screen
    When I select an option to go back to 3rd Party App (TPP)
    Then I should be navigated back to TPP app

  Scenario: TC_010_Exception Scenario: User should be shown an appropriate error message when there are no eligible accounts available with the logged in brand for consent
    Given I am a valid enrolled "Savings/Credit Card/Mortgage/Loan" account RNGA customer
    And I don't have any eligible current accounts for consent
    And I successfully login to RNGA app after launching from TPP for account consent journey
    And Error screen is displayed with Option to return to TPP in CWA screen
    When the Customer selects option to return to TPP
    Then the CWA screen should be closed
    And i am briefly shown NGA loading screen
    And I should be redirected to the TPP app where this consent journey started
    And I should be logged out of my NGA appropriate
