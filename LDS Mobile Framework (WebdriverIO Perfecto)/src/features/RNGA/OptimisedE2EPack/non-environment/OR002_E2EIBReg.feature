@team-login @sw-enrolment-on @sw-ibreg_mvp-on @rnga @nga3 @sw-card-management-on @optFeature
Feature: Native IB Registration end to end scenarios

  #MOB3-4290, MOB3-5730
  #TODO: Add Fingerprint Interstitial step when PI-326 implementation is completed
  @stub-ibreg-activation-success @mbnaSuite @stubOnly
  Scenario: TC_01 Customer sees Security log-off Setting page after Success screen (NGA Retail - Device Enrolment ON)
    Given I am a ibreg_ultralite user
    And I click on Login button
    And I login with my username and password
    And I should be on the IB Registration Activation Code page
    And I should see contact us option on the IB Registration Activation Code page
    When I enter valid Activation Code on IB Registration Activation Code page
    And I click on Confirm Code Button on IB Registration Activation Code page
    And I enter correct memorable information in IB Registration confirm Activation code page
    And I select continue on the IB Registration Success page
    Then I should see contact us option on the auto logout settings page
    When I select confirm button in auto logout settings page
    # And I opt out for fingerprint from the interstitial
    Then I should be on the home page

  #MOB3-3739, MOB3-3805, #MOB3-3427, MOB3-3428,#MOB3-3741, MOB3-3808
  @stub-ibreg-retail-mandate @mbnaSuite @stubOnly
  Scenario Outline: TC_02 Customer existing Retail mandate taken to log-in page
    Given I am a ibreg_retail mandate user
    Then I should be on the Welcome Benefits page
    When I navigate to the login screen
    And I click on Register for Internet Banking in login page
    And I enter valid personal details with "Test12" postcode on the IB Registration Personal Details page
    And I click Next Button on the IB Registration Personal Details page
    And I select Account Type "<accountType>" on the IB Registration Account Details page
    And I enter "<accountType>" on the IB Registration Account Details page
    And I click Next Button on the IB Registration Account Details page
    Then I should be on the Login page
    And I should see Error Message on the Login page
    And I should not see Register for Internet Banking button on the Login page

    Examples:
      |accountType     |
      |CREDIT_CARD     |

  @stub-ibreg-incorrect-postcode @unEnrolled @ios
  Scenario Outline: TC_003 Customer sees postcode error message
    Given I am a current user
    Then I should be on the Welcome Benefits page
    When I navigate to the login screen
    And I click on Register for Internet Banking in login page
    And I enter valid personal details with "Test12" postcode on the IB Registration Personal Details page
    And I enter DOB on the IB Registration Personal Details page for incorrect post code user
    And I click Next Button on the IB Registration Personal Details page
    And I select Account Type "<accountType>" on the IB Registration Account Details page
    And I enter "<accountType>" for incorrect postcode user on the IB Registration Account Details page
    And I click Next Button on the IB Registration Account Details page
    Then "Test12" PostCode appears on the IB Registration Postcode Error page
    Examples:
      |accountType     |
      #|CREDIT_CARD     |
      |CURRENT_SAVINGS |
      #|LOAN            |

  #MOB3-5899
  # Note : For MBNA a "current" user will return a "credit card" user
  @stub-enrolled-typical @genericAccount @mbnaSuite
  Scenario: TC_04 Validate old card control options in the More menu
    Given I am a current user on the home page
    When I select more menu
    Then I should see the below options in the more menu
      | optionsInMorePage |
      | cardManagement    |
    And I should not see the below options in the more menu
      | optionsInMorePage  |
      | lostStolenCard     |
      | replacementCardPin |
    When I select home tab from bottom navigation bar
    And I navigate to current account on home page
    And I select the action menu of current account
    Then I should see the below options in the action menu of the current account
      | optionsInActionMenu  |
      | cardManagementOption |
    And I should not see the below options in the action menu of the current account
      | optionsInActionMenu         |
      | lostOrStolenCardOption      |
      | replacementCardAndPinOption |
    And I close the action menu in the home page

  # MOB3-4164, MOB3-5435, #MOB3-3961, MOB3-5297
  # TODO:Add Fingerprint Interstitial step when PI-326 implementation is completed
  @stub-ibreg-eia-success @stubOnly
  Scenario Outline: TC_05 Customer Authentication via EIA will see Registration success screen - Retail Customer message - HC-201 + HC-203
    Given I am a ibreg_eia_success user
    And I complete EIA registration for Internet Banking
    And I choose call me now option
    Then I should see the success Message as "<HC-201>" on the IB Registration Success page
    And I should see the footer Message as "<HC-203>" on the IB Registration Success page
    When I select continue on the IB Registration Success page
    And I choose my security auto logout settings
    # And I opt out for fingerprint from the interstitial
    Then I should be on the home page

    Examples:
      | HC-201                                                        | HC-203                              |
      | Only you can access your account from the app on this device. | password and memorable information. |

  #MOB3-4164, MOB3-5435
  @stub-ibreg-youth-o2ofail @stubOnly
  Scenario: TC_06 Customer 11-15 - 020 o2o check Fails
    Given I am a ibreg_youth_o2ofail user
    When I complete EIA registration for Internet Banking
    Then I should be on the IB Registration HelpDesk page

  #MOB3-4164, MOB3-5435
  @stub-ibreg-youth-o2opass @stubOnly
  Scenario: TC_07 Customer 11-15 - 020 check passes
    Given I am a ibreg_youth_o2opass user
    When I complete EIA registration for Internet Banking
    Then I should be on the IB Registration LetterSent page

  #MOB3-2502, MOB3-1151
  @stub-ibreg-create-mi-within-grace-period @mbnaSuite @stubOnly
  Scenario: TC_08 MI Created with device enrolment on within grace period
    Given I am a ibreg_create_mi user
    When I click on Login button
    And I login with my username and password
    Then I should be on the IB Registration Create Memorable Info page
    When I enter Create Memorable Info Details on the IB Registration Create Memorable Info page
    And I select Next Button on IB Registration Create Memorable Info page
    And I select continue button on the no security call needed page
    And I select continue button on the enrolment confirmation page
    And I select confirm button in auto logout settings page
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page

  #MOB3-2502, MOB3-1151
  @stub-ibreg-create-mi-after-grace-period @mbnaSuite @stubOnly
  Scenario: TC_09 MI Created with device enrolment on - outside the grace period
    Given I am a ibreg_create_mi user
    And I click on Login button
    And I login with my username and password
    Then I should be on the IB Registration Create Memorable Info page
    When I enter Create Memorable Info Details on the IB Registration Create Memorable Info page
    And I select Next Button on IB Registration Create Memorable Info page
    And I choose call me now option
    And I select continue button on the enrolment confirmation page
    And I select confirm button in auto logout settings page
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page

  #MOB3-3961, MOB3-5297
  @stub-ibreg-adult-o2opass @stubOnly @failed
  Scenario: TC_10 Accept Terms and Conditions and navigate to Letter Sent page
    Given I am a ibreg_adult_o2opass user
    When I complete EIA registration for Internet Banking
    Then I should be on the IB Registration LetterSent page

  #MOB3-3961, MOB3-5297
  @stub-ibreg-adult-o2ofail @stubOnly @failed
  Scenario: TC_11 Accept Terms and Conditions and navigate to HelpDesk page
    Given I am a ibreg_adult_o2ofail user
    When I complete EIA registration for Internet Banking
    Then I should be on the IB Registration HelpDesk page

  @stub-ibreg-eia-ineligible-o2opass @stubOnly @failed
  #MOB3-4164, MOB3-5435
  Scenario: TC012 - EIA Non eligible - sim swap, does not answer and no retry, wrong code entered, Phone unreachable - 020 check Pass
    Given I am a ibreg_eia_ineligible_o2opass user
    When I complete EIA registration for Internet Banking
    And I choose call me now option
    Then I should be on the IB Registration LetterSent page
    And I should see contact us option on the IB Registration LetterSent page

  @stub-ibreg-eia-ineligible-o2ofail @stubOnly @failed
  #MOB3-4164, MOB3-5435
  Scenario: TC013 - EIA Non eligible - sim swap, blacklisted, does not answer and no retry, wrong code entered, Phone unreachable - 020 check Fail
    Given I am a ibreg_eia_ineligible_o2ofail user
    When I complete EIA registration for Internet Banking
    And I choose call me now option
    Then I should be on the IB Registration HelpDesk page

  #exploratory DPL-2256
  @unEnrolled @ios
  Scenario Outline: TC_14 Customer existing Retail mandate taken to log-in page
    Given I am a current user
    Then I should be on the Welcome Benefits page
    When I navigate to the login screen
    And I click on Register for Internet Banking in login page
    And I enter Personal Details on the IB Registration Personal Details page
    And I enter DOB on the IB Registration Personal Details page
    And I click Next Button on the IB Registration Personal Details page
    And I select Account Type "<accountType>" on the IB Registration Account Details page
    And I enter "<accountType>" on the IB Registration Account Details page
    And I edit Account Details "<fieldType>" on the IB Registration Account Details page
    And I tap out of the field on the IB Registration Account Details page
    Then I should be able to close the error message in Account Details page
    When I clear "<fieldType>" on the IB Registration Account Details page
    And I enter "<accountType>" on the IB Registration Account Details page
    And I click Next Button on the IB Registration Account Details page
    Then I should see Error Message on the Login page
    And I should be on the Login page
    And I should not see Register for Internet Banking button on the Login page

    Examples:
      |accountType     |fieldType       |
      |CURRENT_SAVINGS |sortCode        |
#       |LOAN            |loanNumber      |
      |CREDIT_CARD     |creditCardNumber|
#      |MORTGAGE        |mortgageNumber  |
