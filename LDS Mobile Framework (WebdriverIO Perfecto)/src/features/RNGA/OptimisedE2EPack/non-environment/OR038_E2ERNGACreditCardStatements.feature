@mbnaSuite @team-statements @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @optFeature @noExistingData @stubOnly @ios @stub-enrolled-valid
  # separate feature file is created for android for all the scenarios with condensed account tile changes.
  # Will be common scenarios for both once condensed account tile is implemented in iOS
Feature: E2E_Verify RNGA credit card statements
  In order to view the statements for the nominated credit card
  As a NGA authenticated customer with credit card
  I want to view my credit card transactions

  Scenario Outline: TC_01 View debit transaction in credit card statements
    Given I am a current user on the home page
    When I select creditCard account tile from home page
    Then I should see transactions since message on recent tab
    And I should see <type> transaction with below details in credit card statements
      | detail                      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
      | firstCreditIndicator        |
    And I swipe to previous 3 months transactions on the account statement page
    And The previously billed info panel appears
    Examples:
      | type          |
      | debit         |
      | credit        |
