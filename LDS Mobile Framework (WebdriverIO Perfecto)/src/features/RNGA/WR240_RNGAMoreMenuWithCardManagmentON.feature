@team-mpt @team-login @rnga @nga3 @stub-enrolled-typical @sw-card-management-on
Feature: Validate the more menu option for retail customers
  As a NGA authenticated customer
  I want to view the list of options available in the more menu
  So that I can access the entry points to specific journeys

  #MOB3-1169
  @manual @primary
  Scenario: TC_001_Validate the appropriate account holder name in the more menu for joint account
    Given I am an enrolled "current" user logged into the app
    When I select more menu
    Then I should see the name of the account holder appropriately for joint account users

  @primary
  Scenario: TC_002_Main Success: Customer accesses the more menu
    Given I am an enrolled "current" user logged into the app
    When I select more tab from bottom navigation bar
    Then I should see the below options in the more menu
      | optionsInMorePage  |
      | yourProfile        |
      | inbox              |
      | settings           |
      | cardManagement     |
      | depositCheque      |
      | viewDepositHistory |
      | logout             |

  # the below scenario is specific to ios only
  @ios
  Scenario: TC_003_Main Success: Validate the state when toggling between the tabs for ios
    Given I am an enrolled "current" user logged into the app
    When I navigate to settings page from home page
    And I select home tab from bottom navigation bar
    And I select more tab from bottom navigation bar
    Then I should be on the settings page

  # the below scenario is specific to android only
  @android
  Scenario: TC_003_Main Success: Validate the state when toggling between the tabs for android
    Given I am an enrolled "current" user logged into the app
    When I navigate to settings page from home page
    And I select home tab from bottom navigation bar
    And I select more tab from bottom navigation bar
    Then I should be on more menu
