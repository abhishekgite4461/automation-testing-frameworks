@team-mpt @nga3 @rnga @primary @stubOnly
#MOB3-4952/5739
Feature: Verify app Ban Screens
  In order to use Mobile Internet Banking for non supported Retail apps
  As a RNGA authenticated customer
  I want to be Banned from non supported app versions

#  AC01 TC_001_To verify the launch of app store / play store
#  AC01 TC_002_User wants to use Mobile Internet Banking
  @stub-app-ban-unenrolled
  Scenario: Verify app Ban Screens for unenrolled user
    Given I submit username and password on login screen as app ban user
    When I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I choose to update the app in the ban screen
    And I navigate to app store screen
    Then I validate app name in app store
    When I navigate back to app from app store
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I choose to use the Mobile Browser Banking
    And I navigate to mobile browser
    Then I validate url from mobile browser
    When I navigate back to app from mobile browser
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking

  @stub-app-ban-enrolled
  Scenario: Verify app Ban Screens for enrolled user
    Given I submit "correct" MI as "app ban" user on the MI page
    When I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I choose to update the app in the ban screen
    And I navigate to app store screen
    Then I validate app name in app store
    When I navigate back to app from app store
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
    When I choose to use the Mobile Browser Banking
    And I navigate to mobile browser
    Then I validate url from mobile browser
    When I navigate back to app from mobile browser
    And I navigate to the ban screen
    Then I should see an option to update the app in the ban screen
    And I should see an option to use the Mobile Browser Banking
