@team-mpt @nga3 @rnga @primary @stubOnly
#MOB3-4950/5740
Feature: Verify app Warn Screens
  In order to update non supported Retail apps
  As a RNGA authenticated customer
  I want to be Warned from non supported app versions

  #  AC01 TC_001_To verify the launch of app store / play store
  #  AC01 TC_002_User wants to carry on with the current version
  @stub-app-warn-unenrolled
  Scenario: Verify app Warn Screens for unenrolled user
    Given I submit username and password on login screen as app warn user
    When I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to update the app from the warn screen
    And I navigate to app store screen
    And I validate app name in app store
    And I navigate back to app from app store
    And I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to continue without updating from the warn screen
    Then I should be on enter MI page
    And I enter correct MI
    When I finish enrolment
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page

  @stub-app-warn-enrolled
  Scenario: Verify app Warn Screens for enrolled user
    Given I am a app warn user
    When I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to update the app from the warn screen
    And I navigate to app store screen
    And I validate app name in app store
    And I navigate back to app from app store
    And I navigate to the warn screen
    Then I should see an option to update the app in the warn screen
    And I should see an option to continue without updating in the warn screen
    When I choose to continue without updating from the warn screen
    Then I login into the app from memorable information page
