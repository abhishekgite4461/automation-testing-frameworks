@team-login @nga3 @rnga @manual
#  Due to the nature of the feature journey, we are unable to automate this feature
Feature: RNGA user wants to approve the transaction initiated on Desktop using Appsign
  In order to approve a transaction initiated on Desktop
  As a retail RNGA user
  I want an option to approve the transaction using the Retail App

  Background:
    Given I am an enrolled "valid" user
    And I have requested to approve a transaction on Desktop using Appsign functionality
    When I open the App
    Then I should be prompted to login to the App
    When I successfully login
    Then I should be shown the pending approval request

  @primary
  Scenario: TC_001 Main Success: Customer selects Appsign to approve pending payment on Desktop and approves the transaction
    When I approve the pending request
    Then I should be logged off
    And the transaction should be approved

  @primary
  Scenario: TC_002 Variation Success: Customer selects Appsign to approve pending payment on Desktop and rejects the transaction
    And I should be given options to Approve, Cancel or Mark transaction as not recognized
    When I cancel the pending request
    Then I should be logged off
    And the transaction should be declined

  @primary
  Scenario: TC_003 Variation Success: Customer selects Appsign to approve pending payment on Desktop and marks the transaction as Suspect
    When I select 'I don't recognize this' option
    Then I am displayed 'Marked the transaction as not recognized' screen
    And I should be displayed a 'Call Us' button to call Telephony team
    And I should be displayed the timings for the Telephony team
    And i should be logged out

  Scenario: TC_004 Exception: Customer selects to approve the transaction which has timed out (after 5 mins of raising the request)
    When I approve the request after it has timed out (5 mins)
    Then I should be displayed a screen with a message that authorization request has timed out
    And I should be logged out
