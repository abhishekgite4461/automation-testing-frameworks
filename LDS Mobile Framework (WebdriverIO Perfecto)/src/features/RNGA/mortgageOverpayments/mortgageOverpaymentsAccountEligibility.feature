@team-payments @nga3 @rnga @sw-enable-mortgage-overpayment-on @secondary
Feature: Mortgage Overpayments
  As an rnga customer with a eligible mortgage account
  I do not want to see "Make overpayment" entry point when the user navigates to native payment hub
  If i have selected a remitting account which cannot make payments to mortgages
  The 'TO' accounts list I should not see any mortgage accounts

  Background:
    Given I am an enrolled "mortgage" user logged into the app

  @stub-mortgage-overpayment-repossessed-account-eligibility
  Scenario Outline: TC_001 - Repossessed and offeset account not eligibility for mortgage overpayment
    When I navigate to <account> account on home page
    Then I should not see action menu of <account> account
    When I select <account> account tile from home page
    Then I should not see action menu of <account> account
    Examples:
      | account             |
      | rePossessedMortgage |
      | offSetMortgage      |

  @stub-mortgage-overpayment-account-eligibility
  Scenario: TC_002 - Credit card account not eligibility to receive payment
    When I navigate to mortgage account on home page
    And I select the "make an overpayment" option in the action menu of mortgage account
    And I choose the remitter account on the payment hub page
    Then I should see the below accounts in the view remitter page
      | accounts        |
      | islamicCurrent  |
    And I should not see the below accounts in the view remitter page
      | accounts   |
      | creditCard |

  @stub-mortgage-overpayment-loan-account-eligibility
  Scenario: TC_003 - Loan account not eligibility to receive payment
    When I navigate to mortgage account on home page
    And I select the "make an overpayment" option in the action menu of mortgage account
    And I choose the remitter account on the payment hub page
    Then I should see the below accounts in the view remitter page
      | accounts        |
      | islamicCurrent  |
    And I should not see the below accounts in the view remitter page
      | accounts   |
      | nonCbsLoan |

  @stub-mortgage-overpayment-multiplesubaccounts
  Scenario: TC_004 - ISA account eligibility to receive payment
    When I navigate to mortgage account on home page
    And I select the "make an overpayment" option in the action menu of mortgage account
    And I choose the remitter account on the payment hub page
    Then I should see the below accounts in the view remitter page
      | accounts |
      | saving   |
      | cashIsa |
