@team-payments @nga3 @rnga @stub-mortgage-overpayment-multiplesubaccounts @sw-enable-mortgage-overpayment-on @secondary
@pending

Feature: Mortgage Overpayments
  In order to make a lump sum overpayment to my mortgage account
  As a customer
  I want to be able to pay to a specific sub-account

  Background:
    Given I am an enrolled "mortgage" user logged into the app

  Scenario: TC_001 - Sub accounts fetch failure from action menu
    When I navigate to mortgage account on home page
    And I select the action menu of mortgage account
    And I select the "make an overpayment" option in the action menu of mortgage account
    Then my sub accounts are not fetched due to a technical issue
    And I should be taken to the payments hub where I see an error message in the form of a red banner

  Scenario: TC_002 - Sub accounts fetch failure pre selected mortgage from action menu
    When I navigate to mortgage account on home page
    And I select the action menu of mortgage account
    And I select the "make an overpayment" option in the action menu of mortgage account
    Then my sub accounts are not fetched due to a technical issue
    And I should be taken to the payments hub where my mortgage account us pre-selected as the beneficiary

  Scenario: TC_003 - Sub accounts fetch failure no sub-account toggle from action menu
    When I navigate to mortgage account on home page
    And I select the action menu of mortgage account
    And I select the "make an overpayment" option in the action menu of mortgage account
    Then my sub accounts are not fetched due to a technical issue
    And I should be taken to the payments hub where I do not see a sub-account toggle

  Scenario: TC_004 - Sub accounts fetch failure from beneficiary list
    When I navigate to pay and transfer from home page
    And I select my mortgage account as my beneficiary
    Then my sub accounts are not fetched due to a technical issue
    And I should see an error message in the form of a red banner

  Scenario: TC_005 - Sub accounts fetch failure pre selected mortgage from beneficiary list
    When I navigate to pay and transfer from home page
    And I select my mortgage account as my beneficiary
    Then my sub accounts are not fetched due to a technical issue
    And I should return to the payments hub screen where my mortgage account is selected as the beneficiary

  Scenario: TC_006 - Sub accounts fetch failure no sub-account toggle from beneficiary list
    When I navigate to pay and transfer from home page
    And I select my mortgage account as my beneficiary
    Then my sub accounts are not fetched due to a technical issue
    And I should return to the payments hub screen where I do not see a sub-account toggle
