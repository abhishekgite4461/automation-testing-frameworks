@team-payments @nga3 @rnga @stub-mortgage-overpayment-multiplesubaccounts @sw-enable-mortgage-overpayment-off @primary
Feature: Mortgage Overpayments
  As an rnga customer with a eligible mortgage account
  I do not want to see "Make overpayment" entry point when the user navigates to native payment hub

  Scenario: TC_001 - Switch off - Mortgage overpayment entry point from action menu in homepage and statement view
    Given I am an enrolled "mortgage" user logged into the app
    When I navigate to mortgage account on home page
    And I select the action menu of mortgage account
    Then I should not see the below options in the action menu of the mortgage account
      | optionsInActionMenu     |
      | makeAnOverpaymentOption |
    When I close the action menu in the home page
    And I navigate to mortgage account on home page
    And I select mortgage account tile from home page
    And I select the action menu from statements page
    Then I should not see the below options in the action menu of the mortgage account
      | optionsInActionMenu     |
      | makeAnOverpaymentOption |
