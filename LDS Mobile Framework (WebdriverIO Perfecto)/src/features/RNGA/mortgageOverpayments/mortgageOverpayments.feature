@team-payments @nga3 @rnga @stub-mortgage-overpayment-multiplesubaccounts @sw-enable-mortgage-overpayment-on
Feature: Mortgage Overpayments
  As an rnga customer with a eligible mortgage account
  I want to see "Make overpayment" entry point navigates to native payment hub
  So that I can carry out overpayment on my mortgage accounts

  #COE-101/75/91
  Background:
    Given I am an enrolled "mortgage" user logged into the app

  #------------------------------------------ Main Success scenario---------------------------#

  @primary
  Scenario: TC_001 - Verify make overpayment entry point from action menu in homepage
    When I navigate to mortgage account on home page
    And I select the action menu of mortgage account
    Then I should see the below options in the action menu of the mortgage account
      | optionsInActionMenu     |
      | viewTransactionsOption  |
      | makeAnOverpaymentOption |
    And I close the action menu in the home page

  @secondary
  Scenario: TC_002 - Verify make overpayment entry point from action menu in statements View
    When I navigate to mortgage account on home page
    And I select mortgage account tile from home page
    And I select the action menu from statements page
    Then I should see the below options in the action menu of the mortgage account
      | optionsInActionMenu     |
      | makeAnOverpaymentOption |
    And I close the action menu in the home page

  @secondary
  Scenario: TC03 - Pre select mortgage account as beneficiary
    When I obtain the default primary account
    And I navigate to mortgage account on home page
    And I select the "make an overpayment" option in the action menu of mortgage account
    Then I should see the from field pre-populated with defaultPrimary account
    And I should see the to field pre-populated with mortgage account

  @secondary
  Scenario: TC04 - Verify tool tip icon & learn about overpayments modal
    When I navigate to mortgage account on home page
    And I select the "make an overpayment" option in the action menu of mortgage account
    Then I should see the to field pre-populated with mortgage account
    When I select a tooltip icon to informing me about overpayments
    Then I should see more information alert pop up about mortgage overpayments

  @primary
  Scenario: TC05 - Verify sub-account toggle displayed for an eligible mortgage with more than one sub-account
    When I navigate to mortgage account on home page
    And I select the "make an overpayment" option in the action menu of mortgage account
    Then I should see the pay a sub-account toggle in payment hub page
    When I enable the toggle to pay a sub-account
    Then I should see a list of all my sub-accounts
    And None of the sub-account should be pre-selected

  @primary
  Scenario: TC06 - Verify sub-account information
    When I navigate to mortgage account on home page
    And I select the "make an overpayment" option in the action menu of mortgage account
    Then I should see the pay a sub-account toggle in payment hub page
    When I enable the toggle to pay a sub-account
    Then the balance and interest rate should be shown for each eligible sub account

  @secondary
  Scenario: TC07 - Verify tool tip icon & learn about sub-accounts modal
    When I navigate to mortgage account on home page
    And I select the "make an overpayment" option in the action menu of mortgage account
    And I enable the toggle to pay a sub-account
    And I select a tooltip icon to informing me about sub-accounts
    Then I should see more information alert pop up about sub-accounts

  @secondary
  Scenario Outline: TC_008 - Verify mortgage overpayment Review screen
    When I make a payment of <amount> to my mortgage sub-account
    Then I should see the corresponding details in the review payment page
    Examples:
      |amount|
      |5.00  |

  @primary
  Scenario Outline: TC_009 - Verify mortgage overpayment Success screen
    When I make a payment of <amount> to my mortgage sub-account
    And I select the confirm option in the review payment page
    Then I should see a payment confirmed screen
    Examples:
      |amount|
      |5.00  |
