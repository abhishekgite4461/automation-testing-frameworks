@team-login @nga3 @rnga
Feature: Traditional login using secondary auth when MI information already set with 'Device Enrolment' and 'Light Logon' switches ON

  Background:
    Given I am a current user on the MI page

  @stub-valid @mbnaSuite
  Scenario: TC_001_Validate the mi text fields header to be unique in RNGA app
    Then I should see 3 text boxes to enter the MI and their headers are unique

  @stub-valid @primary @mbnaSuite
  Scenario: TC_002_Validate the MI information is masked
    When I enter 2 characters of MI
    Then I should see the MI information is masked

  @stub-valid @instrument @mbnaSuite
  Scenario: TC_003_Validating forgotten your logon details link on enter MI page
    When I select on the forget your login link in MI page
    Then I should see forgotten your logon details mobile browser journey page

  @stub-valid
  Scenario: TC_004_Validating stay button on FSCS tile winback popup on enter MI page
    When I select FSCS tile in MI page
    And I choose the stay option on the fscs popup
    Then I should be on enter MI page

  # TODO follow up with perfecto team for Connection error in browser switching
  @stub-valid
  Scenario: TC_005_Validating leave the app button on FSCS tile winback popup on enter MI page
    When I select FSCS tile in MI page
    And I choose the leave option on the fscs popup
    Then I should see fscs page in the mobile browser

  @stub-valid @primary @mbnaSuite
  Scenario: TC_006_Login to RNGA app with correct MI information
    When I enter correct MI
    Then I should be on the EIA page

  @stub-valid @mbnaSuite
  Scenario: TC_007_Validate the pre-auth header menu in the login page in RNGA
    When I select the contact us option from the MI page pre auth header
    Then I should be on pre auth call us home page overlay

  @manual
  Scenario: TC_008_Login to RNGA app with incorrect MI information and password for 6 times cumulatively
    When I enter incorrect MI
    Then I should see incorrect MI error message in re-enter MI page
    When I enter incorrect MI in re-enter MI page
    Then I should be on the confirm password page
    When I enter incorrect password in confirm password page
    Then I should see incorrect password error message in re-enter MI page
    When I enter incorrect MI in re-enter MI page
    Then I should be on the confirm password page
    When I enter incorrect password in confirm password page
    Then I should see incorrect password error message in re-enter MI page
    When I enter incorrect MI in re-enter MI page
    Then I should be on revoked error page
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  @stub-incorrectmi-incorrect-password @primary @mbnaSuite
  Scenario: TC_009_Login to RNGA app with Wrong MI and enter the correct MI & wrong password second time
    When I enter incorrect MI
    Then I should see incorrect MI error message in re-enter MI page
    When I enter correct MI in re-enter MI page
    And I enter incorrect password in confirm password page
    Then I should see incorrect password error message in re-enter MI page
    And I should be on the re-enter MI page

  @stub-correctmi-correct-password @secondary @mbnaSuite
  Scenario: TC_010_Login to RNGA app with Wrong MI and enter the correct MI & correct password second time
    When I enter incorrect MI
    Then I should see incorrect MI error message in re-enter MI page
    When I enter correct MI in re-enter MI page
    And I enter correct password in confirm password page
    Then I should be on the EIA page

  @stub-incorrectmi-correct-password @primary @mbnaSuite
  Scenario: TC_011_Login to RNGA app with Wrong MI and enter the incorrect MI & correct password second time
    When I enter incorrect MI
    Then I should see incorrect MI error message in re-enter MI page
    When I enter incorrect MI in re-enter MI page
    And I enter correct password in confirm password page
    Then I should see incorrect MI error message in re-enter MI page
    And I should be on the re-enter MI page

  @stub-incorrectmi-incorrect-password
  Scenario: TC_012_Validating stay button on FSCS tile winback popup on enter Re enter MI page
    And I enter incorrect MI
    And I should see incorrect MI error message in re-enter MI page
    When I select the fscs tile on the re-enter MI page
    And I choose the stay option on the fscs popup
    Then I should be on the re-enter MI page

  # TODO MOB3-6846 follow up with perfecto team for Connection error in browser switching
  @stub-incorrectmi-incorrect-password
  Scenario: TC_013_Validating leave the app button on FSCS tile winback popup on Re enter MI page
    And I enter incorrect MI
    And I should see incorrect MI error message in re-enter MI page
    When I select the fscs tile on the re-enter MI page
    And I choose the leave option on the fscs popup
    Then I should see fscs page in the mobile browser

  @stub-incorrectmi-correct-password @primary
  Scenario: TC_015_Login to RNGA app with Wrong MI, incorrect MI & correct password second time validate fscs tile
    When I enter incorrect MI
    And I should see incorrect MI error message in re-enter MI page
    And I enter correct MI in re-enter MI page
    Then I should see the fscs tile in the re-enter password page

  @stub-valid @mbnaSuite
  Scenario: TC_014_Validate the tooltip on MI information
    When I select the tooltip link on the MI page
    Then I should be on the MI tooltip page
