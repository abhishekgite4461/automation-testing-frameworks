@team-mpt @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @stub-enrolled-typical @secondary @android @appShortcut
Feature: Verify AppShortcuts on the long press of the app
  Background:
    Given I am a current user on the home page
    And I select back key on the device
    And I click yes on the Alert Dialogue
    And I put the app in the background

 #DPL-1451 DPL-1452
  Scenario: TC_01_Verify four shortcut Options on long press of app launcher
    When I long press the app from shortcut
    Then I should see four shortcuts from the app
    When I select Branch Finder from shortcut
    Then I should be displayed with map screen opened
    When I long press the app from shortcut
    And I select contact us from shortcut
    And I enter correct MI
    Then I should be displayed with the Support page opened
    When I select Leave option
    And I long press the app from shortcut
    And I select Pay and Transfer from shortcut
    And I enter correct MI
    Then I should be displayed with the PayAndTransfer page opened
    When I select Leave option
    And I long press the app from shortcut
    And I select Manage cards from shortcut
    And I enter correct MI
    Then I should be displayed with the ManageCards page opened

  #DPL-192
  @makeWifiOn
  Scenario Outline: TC_02_Verify shortcut to Branch Finder and ourWebsite on long press of app launcher when internet is OFF
    Given my internet connection is OFF
    When I long press the app from shortcut
    Then I should see four shortcuts from the app
    When I select Branch Finder from shortcut
    Then I should see internet connection error being displayednpm
    When I select back key on the device <noOfTimes> times
    And I long press the app from shortcut

    Examples:
      | noOfTimes |
      |3          |

 #DPL-192
  @resetDeviceLocation
  Scenario Outline: TC_04_Verify shortcut to Branch Finder on long press of app launcher Outside UK
    When I set the location as "<OutsideUKAddress>" which is outsite UK in device
    And I long press the app from shortcut
    Then I should see four shortcuts from the app
    When I select Branch Finder from shortcut
    Then I should be displayed with map screen opened
    Examples:
      | OutsideUKAddress |
      | Chennai          |

  #DPL-192
  @manual
  Scenario Outline: TC_03_Verify shortcut to Branch Finder and ourWebsite on long press of app launcher When Maps app not installed
    When I launch settings in device
    And I search for the navigation app <AppManager>
    And I search for the navigation app <mapApp>
    And I disable the app
    And I long press the app from shortcut
    Then I should see four shortcuts from the app
    When I select Branch Finder from shortcut
    Then I should be displayed with map screen opened
    Examples:
      | mapApp | AppManager  |
      | Maps   | Apps        |

  #DPL-1451 DPL-1452
  @manual
  Scenario Outline: TC_05_Verify user is able to Create shortcut on homescreen
    When I have long pressed on the app launcher
    And I press and hold the "<ShortCutOption>" option
    Then I should be able to create a shortcut on homescreen with selected option
    Examples:
      | ShortCutOption |
      | Contact us     |
      | Manage cards   |
