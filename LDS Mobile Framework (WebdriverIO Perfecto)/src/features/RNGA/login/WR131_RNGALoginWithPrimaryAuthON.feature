@team-login @rnga @nga3
Feature: Traditional login using primary auth with 'Device Enrolment' and 'Light Logon' switches ON

  #MOB3-1796
  @stub-valid @primary @mbnaSuite
  Scenario: TC_001_Login to RNGA app with correct credentials
    Given I am a current user
    And I navigate to the login screen
    When I login with my username and password
    Then I should be on enter MI page

  #MOB3-104
  @stub-incorrect-credentials @primary @mbnaSuite
  Scenario Outline: TC_002_Login to RNGA app with an incorrect username
    Given I am a current user
    And I navigate to the login screen
    When I login with an incorrect username "<username>"
    Then I should see an incorrect username or password error message

    Examples:
      | username     |
      | garbage      |
      | garbage-/_+( |

  #MOB3-104
  @stub-incorrect-credentials @primary @mbnaSuite
  Scenario Outline: TC_003_Login to RNGA app with an incorrect password
    Given I am a current user
    And I navigate to the login screen
    When I login with an incorrect password "<password>"
    Then I should see an incorrect username or password error message

    Examples:
      | password               |
      | garbage                |
      | garbage-/_+(           |
      | garbagegarbagegarbagee |

  #MOB3-1821
  @manual @primary @mbnaSuite
  Scenario: TC_004_Login to RNGA app with a incorrect password consecutively for 6 times so that the account is revoked
    Given I am a current user
    And I navigate to the login screen
    When I login with an incorrect password 6 times
    Then I should be on revoked error page

  @stub-revoked-password @primary @mbnaSuite
  Scenario: TC_005_Login to RNGA app with a revoked password
    Given I am a revoked user
    And I navigate to the login screen
    When I login with my username and password
    Then I should be on revoked error page
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  #MOB3-1796
  @stub-valid @primary @mbnaSuite
  Scenario: TC_006_Login to RNGA app with less than minimum characters required for password field
    Given I am a current user
    And I navigate to the login screen
    When I enter valid username
    And I enter the password "Incor"
    Then I should see the continue button is disabled

  #MOB3-1796
  @stub-valid @primary @mbnaSuite
  Scenario: TC_007_Validate Login to RNGA app with less than minimum characters required for username field
    Given I am a current user
    And I navigate to the login screen
    When I enter the username ""
    And I enter valid password
    Then I should see the continue button is disabled

  #Need to configure the jailbroken device for this scenario
  @manual @primary @mbnaSuite
  Scenario Outline: TC_008_Login to RNGA app in jailbroken/rooted mobile device
    Given I am a "<userType>" user
    And I navigate to the login screen
    When I login with my username and password
    Then I should be on jailbroken/rooted error page

    Examples:
      | userType              |
      | Malware               |
      | Tampered              |
      | Jailbroken/Rooted     |
      | Data on App/OS Version|

  #MOB3-105
  @stub-suspended @primary @mbnaSuite
  Scenario: TC_009_Login to RNGA app with suspended user
    Given I am a suspended user
    And I navigate to the login screen
    When I login with my username and password
    Then I should be on suspended error page
    And I should see the telephone number displayed in the error page
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  #MOB3-2019
  @stub-inactive @primary @mbnaSuite
  Scenario: TC_010_Login to RNGA app with inactive user
    Given I am a inactive user
    And I navigate to the login screen
    When I login with my username and password
    Then I should be on inactive error page
    When I click on Logon to Mobile Banking button in the error page
    Then I should be on the environment selector page

  #MOB3-105
  @stub-inactive @primary @mbnaSuite
  Scenario: TC_011_Login User’s mandate is inactive
    Given I am a inactive user
    And I navigate to the login screen
    When I login with my username and password
    Then I should be on inactive error page
    And I should see the telephone number displayed in the error page
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  #MOB3-105
  @stub-mandateless @primary @mbnaSuite
  Scenario: TC_012_Login to RNGA app without a mandate
    Given I am a mandateless user
    And I navigate to the login screen
    When I login with my username and password
    Then I should see a not registered to IB error message in login page

  #MOB-262
  @stub-compromised-password @primary @mbnaSuite
  Scenario: TC_013_Login to RNGA app with compromised password
    Given I am a compromised password user
    And I navigate to the login screen
    When I login with my username and password
    And I enter correct MI
    Then I should see a compromised password error page
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  #MOB3-118
  #TODO remove android tag once MPT-502 is fixed.
  @stub-valid @primary @instrument @android @mbnaSuite
  Scenario: TC_014_Allow the user to reset the password in RNGA
    Given I am a current user
    And I navigate to the login screen
    When I select the forget your login link
    Then I should see forget username page in the mobile browser

  #MOB3-465
  # TODO MOB3-9480 remove outofscope tag once we start running on master or master and release are merged as the bug is fixed on master
  @stub-partially-registered @mbnaSuite
  Scenario: TC_015_Login to RNGA app with partially registered user
    Given I am a partially registered user
    And I navigate to the login screen
    When I login with my username and password
    Then I should be on partially registered error page
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  #MOB3-260
  @stub-valid @mbnaSuite
  Scenario: TC_016_Validate the pre-auth header menu in the login page in RNGA
    Given I am a current user
    And I navigate to the login screen
    When I select the contact us option from the login page pre auth header
    Then I should be on pre auth call us home page overlay

  @manual
  Scenario: TC_017_Login to RNGA app above the threshold
    Given The threshold level is set
    When I am a current user
    And I navigate to the login screen
    And I login with my username and password
    Then I should see maximum threshold error message in login page

  #MOB3-1796
  @stub-valid @primary @mbnaSuite
  Scenario: TC_018_Validate the password field is masked
    Given I am a current user
    And I navigate to the login screen
    When I enter my username and password
    Then I should see the password field is masked

  #MOB3-1975
  @stub-valid @mbnaSuite
  Scenario: TC_019_Login Username can be a maximum of 30 characters
    Given I am a current user
    And I navigate to the login screen
    When I enter the username "withmorethanthirtycharactersinusername"
    Then I should be restricted from entering more than 30 characters in username text box

  #MOB3-1975
  @manual @stub-valid @mbnaSuite
  Scenario: TC_020_Login Password can be a maximum of 20 characters
    Given I am a current user
    And I navigate to the login screen
    When I enter the password "withmorethantwentycharactersinpassword"
    Then I should be restricted from entering more than 20 characters in password text box

  #MOB3-259
  @stub-2factor-user @mbnaSuite
  Scenario: TC_021_Login User is registered for internet banking,User’s account requires 2 factor authentication
    Given I am a 2fa user
    And I navigate to the login screen
    When I login with my username and password
    Then I should be on the two factor auth error page
    When I click on Logon to Mobile Banking button in the error page
    Then I should be on the environment selector page

  @stub-valid @manual @primary @mbnaSuite
  Scenario: TC_023_Should be able to Copy and paste contents in username field
    Given I am a current user
    And I navigate to the login screen
    When I enter the username "user"
    And I copy the content in the username field
    And I paste the content in the username field
    Then I should see the username field filled with "useruser"

  @stub-valid @manual @primary @mbnaSuite
  Scenario: TC_024_User cannot copy the password field content
    Given I am a current user
    And I navigate to the login screen
    And I enter the password "pass"
    And I copy the content in the password field
    When I paste the content in the username field
    Then I should not see the username field filled with "pass"

  #since the password field is masked, should actually match with the no of characters
  @stub-valid @manual @primary @mbnaSuite
  Scenario: TC_025_User should be able to only paste contents in password field
    Given I am a current user
    And I navigate to the login screen
    And I enter the username "user"
    And I copy the content in the username field
    When I paste the content in the password field
    Then I should see the password field filled with "user"

  @stub-valid @mbna
  Scenario: TC_026_User should be able to see password special characters restriction message in the login page
    Given I am a current user
    When I navigate to the login screen
    Then I should see the password special characters restriction message in the login page

  @stub-valid
  Scenario: TC_027_User should not see password special characters restriction message in the login page
    Given I am a current user
    When I navigate to the login screen
    Then I should not see the password special characters restriction message in the login page
