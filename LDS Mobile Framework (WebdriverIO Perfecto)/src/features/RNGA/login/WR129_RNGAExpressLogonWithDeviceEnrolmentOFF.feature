@team-login @sw-enrolment-on @sw-light-logon-on @rnga @nga3 @manual @mbnaSuite
#Manual since execution involves turning OFF Device Enrolment switch after enrolling into the device
Feature: Express login with DeviceEnrolment switch OFF and Light logon switch ON

  Background:
    Given I am an enrolled "current" user logged into the app
    And I toggle the Device Enrolement switch OFF
    And I relaunch the app

  #MOB3-3445
  Scenario: TC_001_Verify an enrolled user is able to login to app with MI when enrolment switch is OFF
    When I enter correct MI
    Then I should be on the home page
