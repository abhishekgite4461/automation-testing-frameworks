@team-login @nga3 @rnga @stub-enrolled-typical
Feature: Express Logon with ‘Device Enrolment’ and ‘Light Logon’ switches ON
# TODO MOB3-6438 Need to modify the feature file to run against the envionment since STUB does not have relaunch after enrolment
  Background:
    Given I am a current user
    And I should be on enter MI page

  @primary @mbnaSuite
  Scenario: TC_001_Validating Express Logon successful
    When I enter correct MI
    Then I should be on the home page

  @mbnaSuite
  Scenario: TC_002_Validate the mi text fields header to be unique in RNGA app
    And I should see 3 text boxes to enter the MI and their headers are unique

  @primary @mbnaSuite
  Scenario: TC_003_Validate the MI information is masked
    When I enter 2 characters of MI
    Then I should see the MI information is masked

  #MOB3-1157AC-3 #MOB3-2136AC-3
  Scenario: TC_004_Validating stay button on FSCS tile winback popup on enter MI page
    And I select FSCS tile in MI page
    When I choose the stay option on the fscs popup
    Then I should be on enter MI page

  #MOB3-1157AC-3 #MOB3-2136AC-3
  Scenario: TC_005_Validating leave the app button on FSCS tile winback popup on enter MI page
    And I select FSCS tile in MI page
    When I choose the leave option on the fscs popup
    Then I should see fscs page in the mobile browser

  #MOB3-1157AC-3 #MOB3-2136AC-3 Yet to implement for Android
  @mbnaSuite
  Scenario: TC_006_Validating forgotten your logon details link on enter MI page
    When I select on the forget your login link in MI page
    Then I should see forgotten your logon details mobile browser journey page

  #MOB3-2216
  @manual
  Scenario: TC_009_Entering wrong MI in light logon page consecutively for 6 times so that account is revoked
    When I enter incorrect MI for 6 times consecutively
    Then I should be on revoked error page
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  #MOB3-2216
  @primary @environmentOnly @mbnaSuite
  Scenario: TC_010_Entering wrong MI in light logon page and entering correct MI next time
    And I enter incorrect MI
    Then I should see incorrect MI error message in re-enter MI page
    When I enter correct MI in re-enter MI page
    Then I should be on the home page

  #MOB3-2214
  @manual
  Scenario: TC_011_Validating the restrict access exception scenario
    When I enter correct MI
    Then I should be on jailbroken/rooted error page

  #MOB3-2214
  @manual
  Scenario: TC_012_Validating the suspended exception scenario
    When I enter correct MI
    Then I should be on suspended error page
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  #MOB3-2214
  @manual
  Scenario: TC_013_Validating the inactive mandate user exception scenario
    When I enter correct MI
    Then I should be on inactive error page
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay

  #MOB3-2214
  @manual
  Scenario: TC_014_Validating the revoked password exception scenario
    When I enter correct MI
    Then I should be on revoked error page

  #MOB3-2214
  @manual
  Scenario: TC_015_Validating the compromised password exception scenario
    When I enter correct MI
    Then I should see a compromised password error page
    When I select the contact us option from the error page
    Then I should be on pre auth call us home page overlay
