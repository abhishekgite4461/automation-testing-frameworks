@team-login @rnga @nga3 @mbnaSuite
Feature: Traditional login with Create Memorable Information with 'Device enrolment' switch ON
  In order to make the logon process easy
  As a retail user
  I want to create MI before enrolling

  Background:
    Given I am a current account without mi user
    When I navigate to the login screen
    And I login with my username and password

  #---------------------------------------Main Success scenario---------------------------------------#
  # Need step definitions
  @pending
  Scenario: TC_001 Main Success Scenario :create a valid memorable information in RNGA app
    And I enter the valid Memorable Information in the create MI page
    And I re-enter the same valid Memorable Information in upper case in the re-enter the MI
    Then I should be on the eia page

  @stubOnly @stub-ibreg-create-mi-after-grace-period
  Scenario: TC_002 Main Success Scenario :User should be able to contact us from the create memorable information page
    When I select the contact us option from the create mi page pre auth header
    Then I should be on pre auth call us home page overlay

  #---------------------------------------Variation scenario---------------------------------------#
  # Need step definitions
  @pending
  Scenario Outline: TC_003 Negative Sceanrio :create a invalid memorable information in RNGA app
    And I enter the "<invalidMemorableInformation>" in the create MI page
    Then I should see invalid mi error message and prompted to set MI again
    Examples:
      | invalidMemorableInformation |
      | garba                       |
      | garbagegarbagegarbage       |
      | 1234567890                  |
      | 123456789ga                 |
      | garbage garbage             |
      | garbage-garbage             |
      | garbage%�garbage            |

  # Need step definitions
  @pending
  Scenario: TC_004 Negative Sceanrio :create a memorable information same as the userid in RNGA app
    And I enter the username as the memorable information in the create MI page
    Then I should see invalid mi error message and prompted to set MI again

  # Need step definitions
  @pending
  Scenario: TC_005 Negative Sceanrio :create a memorable information same as the password in RNGA app
    And I enter the password as the memorable information in the create MI page
    Then I should see invalid mi error message and prompted to set MI again

  # Need step definitions
  @pending
  Scenario: TC_006 Negative Sceanrio :create a MI with unmatching MI in the re-enter MI in RNGA app
    And I enter  a the valid  memorable information in the create MI page
    And I enter unmatching MI in the re-enter page
    Then I should see invalid mi error message
    And I should be prompted to set MI again
