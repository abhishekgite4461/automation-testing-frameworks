@team-login @rnga @nga3 @mbnaSuite
Feature:Device Enrolment for retail customer with Device enrolment switch ON and Qivox bypass switch ON
  ###This feature file handles the sceanrios with Qivox bypass EIA call in ON status, hence this feature file is exclusively for automation purpose.

  @stub-valid @primary
  Scenario: TC_001_Log into RNGA successfully and enrol the device with a valid registered phone number
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose call me now option
    Then I select continue button on the enrolment confirmation page
    When I choose my security auto logout settings
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page

  @stub-valid
  Scenario: TC_002_Log into RNGA and choose the cancel option in the eia page and accept the pop up
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I select the cancel option on the EIA page
    And I accept the pop-up message on the EIA page
    Then I should be on the welcome page

  @stub-valid
  Scenario: TC_003_Log into RNGA and choose the cancel option in the eia page and choose cancel the pop up
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I select the cancel option on the EIA page
    And I cancel the pop-up message on the EIA page
    Then I should be on the EIA page

  @stub-no-phone-number
  Scenario: TC_004_Log into RNGA with no registered phone number i.e. display Request OTP screen and cancel the cancellation
    Given I am a no registered phone number user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I select the cancel option on the request OTP page
    And I cancel the pop-up on the request OTP page
    Then I should be on the request OTP page

  #MOB3-9564 pending tag to be removed once bug related to STUB is fixed
  @stub-no-phone-number
  Scenario: TC_005_Log into RNGA with no registered phone number i.e. display Request OTP screen and confirm the cancellation
    Given I am a no registered phone number user
    And I navigate to the login screen
    And I login with my username and password
    When I enter correct MI
    Then I should see contact us button on the request OTP page
    When I select the cancel option on the request OTP page
    And I accept the pop-up on the request OTP page
    Then I should be on the welcome page

  @stub-no-phone-number @primary
  Scenario: TC_006_Log into RNGA without registered phone number i.e. display Request OTP screen. User wish to proceed with request OTP and choose the confirm option
    Given I am a no registered phone number user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I select the confirm option on the Request OTP page
    Then I should be on the request OTP confirmation page
    And I should see contact us button on the request OTP confirmation page

  @noExistingData @primary
  Scenario: TC_007_Enrol for partially registered user with wrong otp
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I select the confirm option on the Request OTP page
    Then I should be on the request OTP confirmation page
    And I relaunch the app
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I enter the incorrect otp received on the enter postal address otp page
    And I should see validation loading panel
    And I click be on the error page

  @environmentOnly
  Scenario: TC_008_Select the tooltip option available in the enter postal address otp page and choose close option
    Given I am a no registered phone number user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I select the confirm option on the Request OTP page
    And I should be on the request OTP confirmation page
    And I relaunch the app
    And I login with my username and password
    And I enter correct MI
    And I select the tooltip on the enter postal address otp page
    When I select the close option on the not recieved tooltip in postal address otp page
    Then I should be on the enter postal address otp page

  @environmentOnly
  Scenario: TC_009_Select the tooltip option available in the enter postal address otp page and choose contact us option
    Given I am a no registered phone number user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I select the confirm option on the Request OTP page
    And I should be on the request OTP confirmation page
    And I relaunch the app
    And I login with my username and password
    And I enter correct MI
    And I select the tooltip on the enter postal address otp page
    When I select the contact us option on the not recieved tooltip in postal address otp page
    Then I should see the overlay with call option

  @environmentOnly
  Scenario: TC_010_Choose the header menu in the address otp page
    Given I am a no registered phone number user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I select the confirm option on the Request OTP page
    And I should be on the request OTP confirmation page
    And I relaunch the app
    And I login with my username and password
    And I enter correct MI
    When I select the contact us option from the enter postal address otp page
    Then I should be on pre auth call us home page overlay

  @manual @primary
  Scenario: TC_011_Log in RNGA and maximum number of enrolled device count reached
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I enrol for five different devices
    Then I should be on error page with message exceeded maximum devices enrolled count

  @stubOnly @stub-eia-bypass
  Scenario: TC_012_Enrol in RNGA within the grace period of registering to IB with registered MI – Device Enrolment without EIA
    Given I am a full mandate user
    And I navigate to the login screen
    And I login with my username and password
    When I enter correct MI
    Then I should see contact us option in the phone authentication not required page
    When I select continue button on the phone authentication not required page
    And I select continue button on the enrolment confirmation page
    And I choose my security auto logout settings
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page

  @manual
  Scenario: TC_013_Enrol in RNGA within the grace period of registering to IB without registered MI and Terms and conditions accepted already – Device Enrolment without EIA
    Given I am a full mandate user
    And I navigate to the login screen
    And I login with my username and password
    When I create a valid MI
    Then I click continue button on the phone authentication not required page
    And I select continue button on the enrolment confirmation page
    When I choose my security auto logout settings
    Then I should be on the home page

  @manual
  Scenario: TC_014_Enrol in RNGA within the grace period of registering to IB without registered MI and Terms and Conditions not accepted – Device Enrolment without EIA
    Given I am a full mandate user
    And I navigate to the login screen
    And I login with my username and password
    When I create a valid MI
    And I select the accept button on the terms and conditions page
    Then I click continue button on the phone authentication not required page
    And I select continue button on the enrolment confirmation page
    When I choose my security auto logout settings
    Then I should be on the home page

  #MOB3-2090
  @stub-tc-not-accepted @stubOnly
  Scenario: TC_015_Accepting the terms and conditions in RNGA where terms and conditions is not accepted either on paper or through IB
    Given I am a TC not accepted user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I select the accept button on the terms and conditions page
    Then I should be on the EIA page

  @stub-tc-not-accepted @stubOnly
  Scenario: TC_016_Declining the terms and conditions in RNGA and then choose Decline & logout option where terms and conditions is not accepted either on paper or through IB
    Given I am a TC not accepted user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I select the decline button on the terms and conditions page
    And I select the decline option on the decline terms and conditions page
    Then I should be on the terms and conditions declined error page

  @stub-tc-not-accepted @stubOnly
  Scenario: TC_017_Declining the terms and conditions in RNGA and then choose back to Terms & condition option where terms and conditions is not accepted either on paper or through IB
    Given I am a TC not accepted user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I select the decline button on the terms and conditions page
    And I select the back option on the decline terms and conditions page
    Then I should be on the terms and conditions page with accept and decline button

  #MOB3-108
  @manual @primary
  Scenario: TC_018_Log in RNGA and validate the eia phone number is masked
    Given I am a current user
    When I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    Then I should see all the phone number should be masked in the EIA page

  @manual
  Scenario: TC_019_Log in RNGA with a black listed phone number
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I should see the phone number should be greyed out on the eia page

  @manual
  Scenario: TC_020_Log in RNGA without registered phone number and the address of the customer is not changed within the configurable parameter
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    Then I should be on the Request otp page

  @stub-address-changed-otp
  Scenario: TC_021_Log in RNGA without registered phone number and the address of the customer is changed within the configurable parameter and the 'stop address OTP switch' is ON
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I select request password button in the security call failed page
    Then I should be on the address changed error page

  @manual
  Scenario: TC_022_Log in RNGA without registered phone number and the address of the customer is changed within the configurable parameter and the 'stop address OTP switch' is OFF
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    Then I should be on the Request otp page

  @manual @primary
  Scenario: TC_023_Log in RNGA with a phone number updated in the Desktop site
    Given I am a current user
    And I set a phone number in the desktop site
    And I navigate to the login screen
    And I login with username and password
    And I enter correct MI
    Then I should see the phone number greyed out on the eia page

  @manual @primary
  Scenario: TC_024_Log in RNGA with a sim swap with more than one registered number
    Given I am a sim swapped user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose call me now option
    Then I should be on the phone authentication failed error page
    When I press on the ok button in the phone authentication failed error page
    Then I should be on the eia page with the number greyed out for sim swapped user

  @manual
  Scenario: TC_025_Log in RNGA with a sim swap with only one registered number
    Given I am a sim swapped user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose call me now option
    And I should be on the Request otp page

  # Reinstalling the app as the default configurations might have been affected by previous test runs.
  @stub-valid
  Scenario: TC_026_Log into RNGA successfully and enrol the device with a valid registered phone number
    Given I reinstall the app
    And I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose call me now option
    Then I select continue button on the enrolment confirmation page
    When I verify the default time in the security auto logout settings page
    And I choose my security auto logout settings
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page

  #MOB3-260
  @stub-valid
  Scenario: TC_0027_Validate pre auth menu on Logoff Setting page
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I choose call me now option
    And I select continue button on the enrolment confirmation page
    When I select the contact us option from the enrolment security settings page pre auth header
    Then I should be on pre auth call us home page overlay

  #MOB3-260 #MORE-2277
  @stub-valid @defect
  Scenario: TC_0028_Validate pre auth menu on congratulations screen
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I choose call me now option
    When I select the contact us option from the enrolment confirmation page pre auth header
    Then I should be on pre auth call us home page overlay

  #MOB3-1170 #MOB3-2096
  @stub-eia-bypass @secondary
  Scenario: TC_029_Log into RNGA and enrol the device without EIA call within the grace period of registering to IB
    Given I am a full IB grace period user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I select continue button on the no security call needed page
    And I select continue button on the enrolment confirmation page
    When I choose my security auto logout settings
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page

  #MOB3-2093 #MOB3-113
  @stub-invalid-otp
  Scenario: TC_030_Verify user is shown invalid OTP error message on entering invalid OTP in the enter One time password screen
    Given I am a current user
    And I login with my username and password
    And I enter correct MI
    When I enter invalid OTP in the enter one time password page
    Then I should see incorrect OTP message in the enter one time password page

  #MOB3-2101
  @stub-expired-otp
  Scenario: TC_031_Verify user is shown expired OTP error page on entering expired OTP in the enter One time password screen
    Given I am a current user
    And I login with my username and password
    And I enter correct MI
    When I enter expired OTP in the enter one time password page
    Then I should be on the expired OTP error page

  @stub-valid @primary
  Scenario: TC_032_Validate pre auth menu on EIA call me screen
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I select the contact us option from the EIA page pre auth header
    Then I should be on pre auth call us home page overlay

  @stub-eia-pending @stubOnly
  Scenario: TC_033_Validate contact us pre auth menu is not present in EIA calling now screen
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose call me now option
    Then I should not see the contact us option on the EIA calling now page
