@team-login @manual @rnga @nga3 @mbnaSuite
Feature:Device Enrolment for retail customer with Device enrolment switch ON and Qivox bypass switch OFF
  ###This feature file handles the sceanrios with Qivox bypass EIA call in OFF status, hence this feature file is exclusively for functional point of view.

  Scenario: TC_001_Log into RNGA successfully and enrol the device with a valid registered phone number
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose call me now option
    Then I should see we’re calling you now screen with a four digit code
    And I should get an EIA call on selected phone number
    And I enter the correct four digit code in the mobile device
    And I select continue button on the enrolment confirmation page
    When I choose my security auto logout settings
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page

  Scenario: TC_002_Log into RNGA and choose the cancel option in the eia page and accept the pop up
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose the cancel option in the eia page
    And I accept the pop-up message in the eia page
    Then I should be on the welcome page

  Scenario: TC_003_Log into RNGA and choose the cancel option in the eia page and choose cancel the pop up
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose the cancel option in the eia page
    And I cancel the pop-up message in the eia page
    Then I should be on the EIA page

  Scenario: TC_004_Log into RNGA with no registered phone number i.e. display Request OTP screen and cancel the cancellation
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose the cancel option in the request OTP page
    And I cancel the pop-up message in the request OTP page
    Then I should be on the welcome page

  Scenario: TC_005_Log into RNGA with no registered phone number i.e. display Request OTP screen and confirm the cancellation
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose the cancel option in the request OTP page
    And I accept the pop-up message in the request OTP page
    Then I should be on the welcome page

  Scenario: TC_006_Log into RNGA without registered phone number i.e. display Request OTP screen. User wish to proceed with request OTP and choose the confirm option
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose the confirm option in the Request OTP page
    Then I should see a loader on the Request OTP page
    And I should be on the OTP request confirmation page

  Scenario: TC_007_Enrol for partially registered user successfully
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose the confirm option in the Request OTP page
    Then I should see a loader on the Request OTP page
    And I should be on the OTP request confirmation page
    And I relaunch the app
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I enter the correct otp recieved in the enter postal address otp page
    And I should see validation loading panel
    And I select continue button on the enrolment confirmation page

  Scenario: TC_008_Enrol for partially registered user with wrong otp
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose the confirm option in the Request OTP page
    Then I should see a loader on the Request OTP page
    And I should be on the OTP request confirmation page
    And I relaunch the app
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I enter the incorrect otp recieved in the enter postal address otp page
    And I should see validation loading panel
    And I click be on the error page

  Scenario: TC_009_Select the tooltip option available in the enter postal address otp page and choose close option
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose the confirm option in the Request OTP page
    Then I should see a loader on the Request OTP page
    And I should be on the OTP request confirmation page
    And I relaunch the app
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I select the tooltip in the enter postal address otp page
    And I choose the close option in the re-request OTP overlay
    And I should be on the enter postal address otp page

  Scenario: TC_010_Select the tooltip option available in the enter postal address otp page and choose contact us option
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose the confirm option in the Request OTP page
    Then I should see a loader on the Request OTP page
    And I should be on the OTP request confirmation page
    And I relaunch the app
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I select the tooltip in the enter postal address otp page
    And I choose the contact us option in the re-request OTP overlay
    And I should see the overlay with call option

  Scenario: TC_011_Choose the header menu in the address otp page
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose the confirm option in the Request OTP page
    Then I should see a loader on the Request OTP page
    And I should be on the OTP request confirmation page
    And I relaunch the app
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I click on the top header menu in the enter postal address otp page
    And I should see logoff option in the enter postal address otp page

  Scenario: TC_012_Log in RNGA and maximum number of enrolled device count reached
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I enrol for five different devices
    Then I should be on error page with message exceeded maximum devices enrolled count

  Scenario: TC_013_Enrol in RNGA within the grace period of registering to IB with registered MI – Device Enrolment without EIA
    Given I am a full mandate user
    And I navigate to the login screen
    And I login with my username and password
    When I enter correct MI
    Then I click continue button on the phone authentication not required page
    And I select continue button on the enrolment confirmation page
    When I choose my security auto logout settings
    Then I should be on the home page

  Scenario: TC_014_Enrol in RNGA within the grace period of registering to IB without registered MI and Terms and conditions accepted already – Device Enrolment without EIA
    Given I am a full mandate user
    And I navigate to the login screen
    And I login with my username and password
    When I create a valid MI
    Then I click continue button on the phone authentication not required page
    And I select continue button on the enrolment confirmation page
    When I choose my security auto logout settings
    Then I should be on the home page

  Scenario: TC_015_Enrol in RNGA within the grace period of registering to IB without registered MI and Terms and Conditions not accepted – Device Enrolment without EIA
    Given I am a full mandate user
    And I navigate to the login screen
    And I login with my username and password
    When I create a valid MI
    And I select the accept button in the terms and condition page
    Then I click continue button on the phone authentication not required page
    And I select continue button on the enrolment confirmation page
    When I choose my security auto logout settings
    Then I should be on the home page

  Scenario: TC_016_Login in RNGA with EIA call unanswered above the maximum threshold limit with only one registered phone number
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose call me now option
    Then I should see a four digit code in the device
    And I leave the phone unanswered above the maximum threshold limit
    And I should be on the Request OTP page

  Scenario: TC_017_Login into RNGA with EIA call unanswered above the maximum threshold limit with more than one registered phone number
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose call me now option
    Then I should see we’re calling you screen with four digit code
    And I should get an EIA call on selected phone number
    And I leave the phone unanswered above the maximum threshold limit
    And I should be on the phone authentication failed error page
    When I press on the ok button in the phone authentication failed error page
    Then I should be on the eia page to choose another number

  Scenario: TC_018_Login in RNGA with EIA call with wrong otp above the maximum allowable limit
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose call me now option
    Then I should see we’re calling you screen with four digit code
    And I should get an EIA call on selected phone number
    And I enter the incorrect four digit code in the mobile device above the maximum allowable limit
    And I should be on the error page

  Scenario: TC_019_Accepting the terms and condition in RNGA where terms and condition is not accepted either on paper or through IB
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    When I enter correct MI
    Then I select the accept button in the terms and condition page
    And I should be on the EIA page

  Scenario: TC_020_Declining the terms and condition in RNGA and then choose Decline & logout option where terms and condition is not accepted either on paper or through IB
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    When I enter correct MI
    Then I select the decline button in the terms and condition page
    And I choose the Decline and logout button in the terms and condition winback view
    And I should be on the logout page

  Scenario: TC_021_Declining the terms and condition in RNGA and then choose back to Terms & condition option where terms and condition is not accepted either on paper or through IB
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    When I enter correct MI
    Then I select the decline button in the terms and condition page
    And I choose the back to terms and condition button in the terms and condition winback view
    And I should be on the terms and condition page with accept and decline button

  Scenario: TC_022_Log in RNGA and validate the eia phone number is masked
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I should see the phone number should be masked in the eia page

  Scenario: TC_023_Log in RNGA with a black listed phone number
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I should see the phone number should be greyed out in the eia page

  Scenario: TC_024_Log in RNGA without registered phone number and the address of the customer is not changed within the configurable parameter
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    Then I should be on the Request otp page

  Scenario: TC_025_Log in RNGA without registered phone number and the address of the customer is changed within the configurable parameter and the 'stop address OTP switch' is ON
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    Then I should be on the address changed error page

  Scenario: TC_026_Log in RNGA without registered phone number and the address of the customer is changed within the configurable parameter and the 'stop address OTP switch' is OFF
    Given I am a current user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    Then I should be on the Request otp page

  Scenario: TC_027_Log in RNGA with a phone number updated in the Desktop site
    Given I am a current user
    And I set a phone number in the desktop site
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    Then I should see the phone number greyed out in the eia page

  Scenario: TC_028_Log in RNGA with a sim swap with more than one registered number
    Given I am a sim swapped user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose call me now option
    Then I should see we’re calling you screen with four digit code
    And I should get an EIA call on selected phone number
    And I enter the correct four digit code in the mobile device
    And I should be on the phone authentication failed error page
    When I press on the ok button in the phone authentication failed error page
    Then I should be on the eia page with the number greyed out for sim swapped user

  Scenario: TC_029_Log in RNGA with a sim swap with only one registered number
    Given I am a sim swapped user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    When I choose call me now option
    Then I should see we’re calling you screen with four digit code in the device
    And I should get an EIA call on selected phone number
    And I enter the correct four digit code in the mobile device
    And I should be on the Request otp page

  Scenario: TC_030_Log into RNGA and enrol the device without EIA call within the grace period of registering to IB
    Given I am a full IB grace period user
    And I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I select continue button on the no security call needed page
    And I select continue button on the enrolment confirmation page
    When I choose my security auto logout settings
    Then I should be on the home page
