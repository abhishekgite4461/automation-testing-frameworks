@team-login @nga3 @rnga @stub-valid @mbnaSuite
Feature: Redesigned welcome screen
  In order to have a better user experience
  As a RNGA Customer
  I want to see a re-designed welcome page

  Background:
    Given I am a current user

  #---------------------------------------Main Success scenario---------------------------------------#
  #MORE-29
  @primary
  Scenario: TC-001 Main Success Scenario : User should be able to choose contact us from the header
    When I select the contact us option from the welcome page pre auth header
    Then I should be on pre auth call us home page overlay

  #MORE-29
  @primary
  Scenario: TC-002 Main Success Scenario : User should be able select the legal information from the welcome page
    When I select the legal info option from the welcome page
    Then I am on the pre auth legal information page overlay

  #MORE-77
  Scenario: TC-003 Main Success Scenario : User should be able to dismiss the legal info main page
    And I select the legal info option from the welcome page
    When I select the dismiss button from the legal information page overlay
    Then I should be on the welcome page

  #MORE-77
  @environmentOnly
  Scenario Outline: TC-004A Main Success Scenario : User should be able to dismiss the legal info item page
    And I select the legal info option from the welcome page
    When I select <legalOption> from the legal information page overlay
    Then I should be on the <legalOption> legal info item page overlay
    When I select the dismiss button from the legal info item page overlay
    Then I should be on the welcome page
    Examples:
      |legalOption                 |
      |legal and privacy           |
      |cookie use and permissions  |
      |third party acknowledgements|

  #MORE-77
  @stubOnly @ios
  Scenario Outline: TC-004B Main Success Scenario : User should be able to dismiss the legal info item page
    And I select the legal info option from the welcome page
    When I select <legalOption> from the legal information page overlay
    Then I should be on the <legalOption> legal info item page overlay
    When I select the dismiss button from the legal info item page overlay
    Then I should be on the welcome page
    Examples:
      |legalOption                 |
      |legal and privacy           |
      |cookie use and permissions  |
      |third party acknowledgements|

  #MORE-77
  @environmentOnly
  Scenario Outline: TC-005A Main Success Scenario : User should be able to navigate back to legal info page form the legal info item page
    And I select the legal info option from the welcome page
    When I select <legalOption> from the legal information page overlay
    Then I should be on the <legalOption> legal info item page overlay
    When I select the back button from the legal info item page overlay
    Then I am on the pre auth legal information page overlay
    Examples:
      |legalOption                 |
      |legal and privacy           |
      |cookie use and permissions  |
      |third party acknowledgements|

  #MORE-77 #MQE-453 back button locator missing on ios
  @stubOnly
  Scenario Outline: TC-005B Main Success Scenario : User should be able to navigate back to legal info page form the legal info item page
    And I select the legal info option from the welcome page
    When I select <legalOption> from the legal information page overlay
    Then I should be on the <legalOption> legal info item page overlay
    When I select the back button from the legal info item page overlay
    Then I am on the pre auth legal information page overlay
    Examples:
      |legalOption                 |
      |legal and privacy           |
      |cookie use and permissions  |
      |third party acknowledgements|
