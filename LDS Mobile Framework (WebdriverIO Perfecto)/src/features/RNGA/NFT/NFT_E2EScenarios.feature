@team-release @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @stub-enrolled-valid @performance
Feature:Device Enrolment for retail customer with Device enrolment switch ON

  Scenario: TC_01 RNGA Login And Enrollment NFT
    Given I am a current user
    When I navigate to the login screen
    And I login with my username and password
    And I enter correct MI
    And I choose call me now option
    And I capture and enter the 4 digit pin for eia call
    And I select continue button on the enrolment confirmation page
    And I choose my security auto logout settings
    And I opt out for fingerprint from the interstitial
    Then I should be on the home page
    When I navigate to logoff via more menu
    Then I should be on the logout page

  Scenario: TC_02 Payment Journey Verify success transfer journey
    Given I am a current user
    And I enter correct MI
    Then I should be on the home page
    And I am on the payment hub page
    When I submit transfer with from account current and to account saving with amount 1.00
    And I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page

  Scenario Outline: TC_03 Payments Manage Standing Order
    Given I am a current user
    And I enter correct MI
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I provide Reference for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    And I enter a correct password
    And I should see a Standing Order set up success message
    Examples:
      | fromAccount | Amount | toAccount | frequency |
      | current     | 1      | saving    | Weekly    |

  Scenario: TC_04 Call us page options from Support Hub
    Given I am a current user
    And I enter correct MI
    Then I should be on the home page
    When I select support tab from bottom navigation bar
    And I select call us on support hub page
    Then I should be able to view these call us tiles
      | callUsTiles           |
      | yourAccountsTile      |
      | newProductEnquireTile |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
      | medicalAssistAbroadTab |
    When I select the Suspected fraud tile on call us home page overlay
    Then I should be on the secure call page
    When I navigate back
    And I select the Lost or stolen cards tile on call us home page overlay
    Then I should be on the secure call page
    When I navigate back to call us from secure call page
    And I select "change of address" option in the support hub page
    Then I should be on the your personal details page
    And I navigate to logoff via more menu
    And I should be on the logout page

  Scenario Outline: TC_05 Auto LogOff
    Given I am a current user
    And I enter correct MI
    When I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    And I select the desired "<autoLogOffTime>" from the auto log off page
    Then I should be on the security settings page
    When I navigate to logoff via more menu
    Then I should be on the logout page
    Examples:
      | autoLogOffTime |
      | 1 Minute       |
      | 2 Minute       |
      | 5 Minute       |
      | 10 Minute      |

  Scenario Outline: TC_06 View current and previous month transactions for current account
    Given I am a current user
    And I enter correct MI
    When I select current account tile from home page
    And I view <month> transactions of current account
    Then I should be displayed current statements transactions
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    And bottom navigation bar is displayed
    When I navigate to logoff via more menu
    Then I should be on the logout page
    Examples:
      | month    |
      | current  |
      | previous |

  Scenario: TC_07 View pending transactions for current account
    Given I am a current user
    And I enter correct MI
    When I select current account tile from home page
    And I view current transactions of current account
    Then I tap on the pending transactions accordion
    And I should see following fields in the pending transaction accordion
      | fieldsInPendingTransactionAccordion |
      | pendingLabel                        |
      | pendingTransactionDescription       |
      | pendingTransactionAmount            |
    And bottom navigation bar is displayed
    When I navigate to logoff via more menu
    Then I should be on the logout page

  Scenario: TC_08 Reset Password User successfully changes password
    Given I am a current user
    And I enter correct MI
    And I navigate to the re-set password page
    When I enter my new valid password in the reset password page
    And I select the submit button in the reset password page
    Then I should see a password updated pop up in the reset password page
    And I should be on the logout page
