@team-mco @nga3 @rnga @ff-webview-debug-on @appiumOnly @pending
Feature: Verify Product hub category links

  Background:
    Given I am an enrolled "valid" user logged into the app
    And I select apply tab from bottom navigation bar
    And I close the opened Featured option from product hub menu

  Scenario Outline: AC01 TC_002_Verify the Loans calculator happy journey
    When I select the loan calculator of the Loans option in the product hub page
    And I select the Home improvement option in loan calculator page
    Then I should be on the apply loan page
    When I enter <amount> and <term> for calculating my loan eligibility
    And I select future option as "NO"
    Then I should be on the "your loan application" page
    When I select the employment status as "Employed - Part time"
    And I enter <salary> detail and confirm "no" for regular monthly expenses
    And I select spending and expenses accordian
    And I enter my <mortgageShare> and <dependent> details
    And I select extra cost as "NO"
    And I select "your contact details" accordian
    Then I confirm my email address in your loan application page

    Examples:
      |amount|   |term|  |salary| |mortgageShare| |dependent|
      | 4000 |   | 20 |  |3000  | |    1000     | |      1  |
