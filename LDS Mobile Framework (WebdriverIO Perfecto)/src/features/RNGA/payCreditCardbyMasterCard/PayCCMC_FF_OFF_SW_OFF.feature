@team-payments @nga3 @rnga @sw-pay-credit-by-master-card-off @stub-enrolled-valid
Feature: Pay credit card using master card open banking platform
  As a RNGA Authenticated user
  I want to make payment to my credit card using another UK bank account

  #PJO-6092, PJO-6221
  Scenario: TC_01 User is not shown the option another uk bank account in from selection screen through bottom navigation menu
    Given I am a credit card and current account user on the home page
    When I navigate to the remitter account on the payment hub page
    Then I should not see the option to pay by another uk bank account

  #PJO-6092, PJO-6221
  Scenario: TC_02 User is not shown the option another uk bank account in from selection screen through action menu
    Given I am a credit card and current account user on the home page
    When I select the "pay a credit card" option in the action menu of creditCard account
    And I select the remitter account on the payment hub page
    Then I should not see the option to pay by another uk bank account
