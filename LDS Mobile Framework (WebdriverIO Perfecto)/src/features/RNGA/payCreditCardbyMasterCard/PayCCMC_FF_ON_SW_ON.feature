@team-payments @nga3 @rnga @ff-pay-credit-card-by-master-card-enabled-on @sw-pay-credit-by-master-card-on
Feature: Pay credit card using master card open banking platform
  As a RNGA Authenticated user
  I want to make payment to my credit card using another UK bank account

  #PJO-6092, PJO-6221
  @stub-pccmc-success
  Scenario: TC_01 User is shown the option another uk bank account in from selection screen
    Given I am a credit card and current account user on the home page
    When I navigate to the remitter account on the payment hub page
    Then I should see the option to pay by another uk bank account

  #PJO-6092, PJO-6221
  @stub-pccmc-success
  Scenario: TC_02 User is shown the option another uk bank account in from selection screen through action menu
    Given I am a credit card and current account user on the home page
    When I select the "pay a credit card" option in the action menu of masterCreditCard account
    And I select the remitter account on the payment hub page
    Then I should see the option to pay by another uk bank account

  #PJO-6109, PJO-5994
  @stub-pccmc-success
  Scenario: TC_03 Pay Credit Card by Another UK Bank Account journey
    Given I am a credit card and current account user on the home page
    And I am on the payment hub page
    When I choose the from account as anotherUKBankAccount and to account as masterCreditCard in the payment hub page
    Then I am shown payment date in the payment hub
    And I should see the select provider button disabled in the payment hub page
    When I enter amount greater than the balance of the credit card account in the payment hub page
    Then I should see credit card payment validation error message
    When I dismiss the error banner message
    And I enter 2.00 in the amount field in the payment hub page
    And I should see the select provider button enabled in the payment hub page
    And I click the select provider button in the payment hub page
    And I select an external provider bank extBankNTW from the list
    Then I should be on confirm payment page
    And I should see the selected provider bank extBankNTW as From account
    And I should see the selected credit card account masterCreditCard as To account
    And I should see the confirm button enabled in the confirm payment page
    And I should see the edit button enabled in the confirm payment page
    When I should not see the checkbox at confirm payment page
    And I select the confirm button on confirm payment page
    Then I should see the Important information screen
    When I agree to the T&C and Consent on information screen
    And I proceed to the external bank website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the provider bank name extBankNTW on the redirect screen

  @stub-pccmc-success
  Scenario: TC_04 User can choose pay by debit option at provider list screen
    Given I am a credit card and current account user on the home page
    And I am on the payment hub page
    When I choose the from account as anotherUKBankAccount and to account as masterCreditCard in the payment hub page
    Then I am shown payment date in the payment hub
    And I should see the select provider button disabled in the payment hub page
    When I enter amount greater than the balance of the credit card account in the payment hub page
    Then I should see credit card payment validation error message
    When I dismiss the error banner message
    And I enter 2.00 in the amount field in the payment hub page
    And I should see the select provider button enabled in the payment hub page
    And I click the select provider button in the payment hub page
    And I navigate to provider list screen
    Then I select pay by uk debit card option
    And I should see the from field pre-populated with debitCard account
    And I should see the to field pre-populated with masterCreditCard account
    And I am not shown payment date in the payment hub
    And I submit the pccdc with all the mandatory fields with corresponding amount value 2.00
    And I am displayed a warning message

  @stub-pccmc-success
  Scenario: TC_05 User can select future dated payments for Pay Credit Card by Another UK Bank Account journey
    Given I am a credit card and current account user on the home page
    And I am on the payment hub page
    When I choose the from account as anotherUKBankAccount and to account as masterCreditCard in the payment hub page
    Then I am shown payment date in the payment hub
    And I should see the select provider button disabled in the payment hub page
    When I enter 2.00 in the amount field in the payment hub page
    And I select calendar picker option for future date payment
    Then I should see a calendar view
    And I should be allowed to select a payment date up to 31 days from current day
    When I select previous month chevron from the top header of calendar view
    And I select the date as 2 from today's date from calendar view
    And I should see the select provider button enabled in the payment hub page
    And I click the select provider button in the payment hub page
    And I select an external provider bank extBankNTW from the list
    Then I should be on confirm payment page
    And I am shown 'When' the payment will be made on payment review page
    When I should not see the checkbox at confirm payment page
    And I select the confirm button on confirm payment page
    Then I should see the Important information screen
    When I agree to the T&C and Consent on information screen
    And I proceed to the external bank website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the provider bank name extBankNTW on the redirect screen

  @stub-pccmc-success
  Scenario: TC_06 User is shown the additional option statement and minimum balance for pay credit card
    Given I am a credit card and current account user on the home page
    And I am on the payment hub page
    When I choose the from account as anotherUKBankAccount and to account as masterCreditCard in the payment hub page
    Then I am shown payment date in the payment hub
    And I should see the select provider button disabled in the payment hub page
    When I select an additional option for pay a credit card in the payment hub page
    Then I should see statement balance and minimum amount with instalment plan option in the payment hub - pay a credit card screen

  @environmentOnly @manual
  Scenario: TC_07 Success-Pay Credit Card by Another UK Bank Account journey
    Given I am a credit card and current account user on the home page
    And I am on the payment hub page
    When I choose the from account as anotherUKBankAccount and to account as credit card in the payment hub page
    Then I am shown payment date in the payment hub
    And I should see the select provider button disabled in the payment hub page
    When I enter amount greater than the balance of the credit card account in the payment hub page
    Then I should see credit card payment validation error message
    When I dismiss the error banner message
    And I enter 2.00 in the amount field in the payment hub page
    And I should see the select provider button enabled in the payment hub page
    And I click the select provider button in the payment hub page
    And I select an external provider bank woodBank from the list
    Then I should be on confirm payment page
    And I should see the selected provider bank woodBank as From account
    And I should see the selected credit card account masterCreditCard as To account
    And I should see the confirm button enabled in the confirm payment page
    And I should see the edit button enabled in the confirm payment page
    When I should not see the checkbox at confirm payment page
    And I select the confirm button on confirm payment page
    Then I should see the Important information screen
    When I agree to the T&C and Consent on information screen
    And I proceed to the external bank website
    Then I should see winback with Cancel and Continue option
    When I select the Continue option in winback
    Then I should see the provider bank name woodBank on the redirect screen
    And I should be navigated to the mobile browser for the designated webpage of the selected provider bank
    When I submit the payments at provider bank
    Then I see universal link is triggered after payment is submitted successfully
    When I should be redirected back to the NGA app
    And I am shown the redirect screen
    Then I am showm the success screen
