@rnga @nga3 @stub-valid @team-platform-engineering @android
Feature: Sanity test

  Scenario Outline: TC_001 Sanity test
    Given I am an enrolled "current" user logged into the app
    When I navigate to pay and transfer from home page
    And I select the recipient account on the payment hub page
    And I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    Then I select continue button on add payee page
    And I should see UK Review Details page
    And I should see to add a reference <reference>
    When I select continue on UK review details page
    Then I should be prompted to enter my password
    When I enter a correct password
    And I am on the Make Payment screen with newly added beneficiary selected
    And I submit the payment with all the mandatory fields with corresponding from account as <fromAccount> and to account from beneficiary list with amount value <amount> and reference
    And I select the confirm option in the review payment page
    And I select the view transaction option in the payment success page
    And I should see following fields in statements list
      | fieldsInStatementsList      |
      | firstTranscationDate        |
      | firstTranscationDescription |
      | firstTranscationAmount      |
    Given I have submitted request to change mobile, home and work
    When The request has been successfully processed
    Then I should be shown a success screen
    And I navigate to security settings page from home page
    When I select the autoLog off settings option from the security settings page
    And I select the desired "logoffImmediate" from the auto log off page
    Then I should be on the logout page
    Examples:
      | sortCode | accountNumber | recipientName | amount | reference | fromAccount    |
      | 300087   | 74374163      | TEST0123      | 1      | test      | currentPayment |
