@team-performance @nga3 @rnga @environmentOnly @manual @mbnaSuite

#MOB3-8110 #MOB3-8112
Feature: User wants to view the credit card statement from the bank
  In Order view my credit card statement from the bank
  As a RNGA Authenticated Customer
  I want the ability to filter by year

  Scenario: TC_001_ User wants to view filter their statements
    Given I am an enrolled "valid" user logged into the app
    And I select PDF statements option from creditCard account in UI tile menu from home page
    When I select filter button from credit card PDF Statements page
    Then I should be able to see native filter to select year for statement download
