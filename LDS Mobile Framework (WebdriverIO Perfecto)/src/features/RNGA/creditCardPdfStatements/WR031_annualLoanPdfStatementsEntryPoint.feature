@team-performance @nga3 @rnga @environmentOnly @lds
#testdata are available for lloyds brand

#MOB3-8117 #MOB3-8118
Feature: User wants to view annual loan statement from the bank
  In order view my loan statements from the bank
  As a rnga authenticated customer
  I want to be able to download the PDF statements to my device

  Scenario: TC_001_User views loan PDF statements page
    Given I am an enrolled "annual Loans PDF eligible" user logged into the app
    When I navigate to nonCbsLoan account on home page
    And I select the "annual PDF statements" option in the action menu of nonCbsLoan account
    Then I should be on the annual loan PDF statements page
