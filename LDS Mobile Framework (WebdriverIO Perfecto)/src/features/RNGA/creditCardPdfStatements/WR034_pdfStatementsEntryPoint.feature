@team-performance @nga3 @rnga @environmentOnly @lds @mbnaSuite

#MOB3-8108 #MOB3-8109
Feature: User wants to view credit card statement from the bank
  In Order View my credit card statement from the bank
  As a RNGA Authenticated Customer
  I want to be able to download the PDF statement to my device

  Scenario: TC_001_User views PDF Statements webview
    Given I am an enrolled "credit card" user logged into the app
    And I select pdfCreditCard account tile from home page
    When I select the "PDF statements" option in the action menu of pdfCreditCard account
    Then I should be able to view Credit card PDF Statements page
