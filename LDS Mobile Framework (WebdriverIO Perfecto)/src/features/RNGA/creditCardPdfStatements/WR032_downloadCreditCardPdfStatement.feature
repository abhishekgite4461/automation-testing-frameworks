@team-performance @nga3 @rnga @environmentOnly @manual @mbnaSuite

#MOB3-8114 #MOB3-8115
Feature: User Wants to download PDF from CC statement
  In Order to read the correspondence
  As a RNGA Authenticated Customer
  I want to download it as a PDF

  Scenario: TC_001_User downloads PDF Statement
    Given I am an enrolled "valid" user logged into the app
    And I select PDF statements option from credit card account in UI tile  menu from home page
    When I choose to download statement as a PDF from the PDF statements page
    Then a PDF should be downloaded to my device
