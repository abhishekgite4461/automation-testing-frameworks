@team-statements @nga3 @rnga @sw-spending-rewards-cwa-on @sw-sr-ur-optin-on @stub-enrolled-typical @manual @environmentOnly

Feature: User wants to register for spending rewards
  In order to register for spending rewards
  As a RNGA authenticated customer
  I want to have the option to register for spending rewards

  # its manual as we have to opt out user every time before executing these scenarios

  @primary
  Scenario: TC_001_User wants to opt in for spending rewards from home page
    Given I am an enrolled "opted out spending rewards" user logged into the app
    When I select everyday offers tile from home page
    And I confirm my selection on the spending rewards page
    Then I should be opted-in for spending rewards

  @primary
  Scenario: TC_002_User wants to opt in for spending rewards from settings
    Given I am an enrolled "opted out spending rewards" user logged into the app
    When I navigate to settings page via more menu
    And I tap on register for everyday offers option
    And I tap on yes I want everyday offers button
    Then I should be displayed congratulations with OK button
    When I tap on Ok button
    Then I should be on more options page

  Scenario Outline: TC_003_Verify options in every day offers register page
    Given I am an enrolled "opted out spending rewards" user logged into the app
    When I select everyday offers tile from home page
    Then I should be displayed every day offers registration page with below options
      | options                               |
      | activate the offers youre interested in |
      | shop with your <brand> cards          |
      | enjoy cash back into your account     |
      | yes I Want Everyday Offers            |
      | Not now Thanks                        |
    Examples:
      | brand       |
      | Lloyds      |
      | Halifax     |
      | BOS         |

  Scenario: TC_004_User does not confirm to opt in for spending rewards
    When I select everyday offers tile from home page
    And I do not confirm my selection on spending rewards page
    Then I should not be opted-in for spending rewards
