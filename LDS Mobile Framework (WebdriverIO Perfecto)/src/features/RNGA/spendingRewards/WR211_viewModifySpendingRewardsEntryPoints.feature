@team-statements @nga3 @rnga @environmentOnly @sw-spending-rewards-cwa-on @sw-sr-ur-optin-on @manual @primary
#MOB3-2640/MOB3-3170
Feature: User wants to modify which account is linked to spending rewards
  In order to modify my spending rewards preferences
  As a RNGA authenticated customer
  I want to have the option to select an alternative account in my spending rewards preferences

  # its manual as we have to opt IN user before executing these scenarios
  Scenario: TC_001_User wants to access modify spending rewards from setting menu
    Given I am an enrolled "opted in spending rewards" user logged into the app
    When I navigate to settings page via more menu
    And I select modify spending rewards from the settings menu page
    Then I should be on the modify spending rewards webview page

  Scenario: TC_002_User wants to view spending rewards from home page
    Given I am an enrolled "opted in spending rewards" user logged into the app
    When I select the view all offers button from home page
    Then I should be on the spending rewards page
