@team-statements @nga3 @rnga @stub-valid @manual @environmentOnly @sw-spending-rewards-cwa-on @sw-sr-ur-optin-on
#MOB3-7713 #MOB3-3029
Feature: RNGA Spending rewards preferences
  In order to be warned while leaving the app
  As a RNGA Authenticated Customer
  I Want to have the option to cancel or go to the mobile browser

  #its manual as these are web page validations
  Scenario Outline: TC_001_Display winback popup when user chooses external links in FAQs section for spending rewards
    Given I am an enrolled "optedInSpendingRewards" user logged into the app
    When I am on spending rewards FAQs page
    And I select "<external links>" from answers to the FAQs listed in the FAQs page
    Then I should see a winback option on top of the screen with Cancel and OK option
    And on selection of cancel option I should stay on the same screen

    Examples:
      | external links                      |
      | Online Banking terms and conditions |
      | terms of our Privacy Policy         |
      | our privacy policy                  |
      | Privacy Statement                   |
      | phone number                        |

  Scenario Outline: TC_002_Display winback popup when user chooses external links in FAQs section for spending rewards
    Given I am an enrolled "optedInSpendingRewards" user logged into the app
    When I am on spending rewards FAQs page
    And I select "<external links>" from answers to the FAQs listed in the FAQs page
    Then I should see a winback option on top of the screen with Cancel and OK option
    And on selection of ok option I should navigate to the web browser page related to the external link

    Examples:
      | external links                      |
      | Online Banking terms and conditions |
      | terms of our Privacy Policy         |
      | our privacy policy                  |
      | Privacy Statement                   |
      | phone number                        |
