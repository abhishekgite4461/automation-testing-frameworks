@team-payments @nga3 @stub-current-and-savings-account @rnga
Feature: Deposit History (togglefunctionality)
  In order to see the deposited cheques and deposit history by toggling between the journey.
  As a Retail NGA User
  I want to toggle between Deposit history and Cheque deposit pages

  Background:
    Given I am an enrolled "current and savings account" user logged into the app
    And I navigate to deposit history page via more menu

  Scenario: TC_001 Verify that the user is able to toggle between Deposit history and Cheque deposit
    And I should be on deposit history page
    And I tap on deposit cheque button from deposit history page
    And I should be on deposit cheque page
    When I tap on deposit history from deposit cheque page
    Then I should be on deposit history page

  Scenario: TC_002 Verify that user not able to see the deposit cheque page
    When I tap on deposit cheque button from deposit history page
    Then I should be able to see appropriate error message on deposit cheque page
      |errorMsg          |
      |technicalErrorText|
      |accountErrorText  |
