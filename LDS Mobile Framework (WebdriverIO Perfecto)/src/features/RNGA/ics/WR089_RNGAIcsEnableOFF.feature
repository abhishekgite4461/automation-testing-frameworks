@team-payments @sw-cheque-imaging-enabled-off @nga3 @rnga @stub-current-and-savings-account
Feature: To verify ICS entry point when cheque imaging enabled switch is OFF
  In order to check ICS entry points is disabled ,
  I validate FCM entry point from global menu with "cheque imaging enabled switch - OFF"

  Scenario: TC_001_Customer unable to view ics entry points from global menu, when cheque imaging enabled switch is OFF
    Given I am an enrolled "current and savings account" user logged into the app
    When I select more menu
    Then I should not be able to see Deposit Cheque & History Cheque entry points
