@team-payments @nga3 @rnga @sw-rnga-industrial-off @stub-current-and-savings-account
Feature: Cheque Deposit (Retail).
  In order to have funds available in my account within the new 2 day clearing model.
  As a Retail NGA User
  I want to deposit a cheque

  #MOB3-5746 #MOB3-3934 #MOB3-3934 #MOB3-6573 #MOB3-10062
  @primary @sensorInstrument
  Scenario Outline: TC_001 Main Success Scenario: User enters the valid amount
  and capture the valid cheque then able to deposit the cheque successfully
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I deposit a valid cheque <image> of <amount> with <reference>
    Then I should see thumbnail images in deposit screen
    When I select review deposit button
    And I select thumbnail for front image
    Then I should be able to see the full screen image
    When I close the cheque image
    Then I should be able to see review deposit screen
    When I select thumbnail for back image
    Then I should be able to see the full screen image
    When I close the cheque image
    And I confirm the cheque deposit
    Then I should be able to see deposit success screen
    Examples:
      |image                       |amount  |reference       |
      |DepositSuccess/FrontOfCheque|12      | Success screen |

  #MOB3-10056 #MOB3-10057
  @environmentOnly @primary @imageInjection @sensorInstrument
  Scenario Outline: TC_002 Main Success Scenario: Verify updated deferred balance after successful cheque deposit
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I capture the main balance from account tile
    And I deposit a cheque <image> of <amount> with <reference>
    Then I confirm the cheque deposit
    When I navigate back to cheque deposit home screen
    Then I should see an updated deferred balance
    Examples:
      |image                        |amount     |reference       |
      |DeferredBalance/FrontOfCheque|143.17     | Success screen |

  #MOB3-8893 #MOB3-8894
  @secondary @imageInjection @environmentOnly @sensorInstrument
  Scenario Outline: TC_003 Success Scenario: Verify user is able to view reference text in transaction history
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I deposit a cheque <image> of <amount> with <reference>
    Then I confirm the cheque deposit
    When I select view deposit history button
    Then I am able to view the <reference> in the transaction history
    Examples:
      |image                       |amount  |reference       |
      |DepositSuccess/FrontOfCheque|50      |testreference   |

  #MOB3-8893 #MOB3-8894
  @secondary @imageInjection @environmentOnly @sensorInstrument
  Scenario Outline: TC_004 Success Scenario: Verify user is not able to view reference text in transaction history
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I deposit a cheque <image> of <amount> without <reference>
    Then I confirm the cheque deposit
    When I select view deposit history button
    Then I am not able to view the <reference> in the transaction history
    Examples:
      |image                  |amount  |reference      |
      |Reference/FrontOfCheque|12      |               |

  #MOB3-8308 #MOB3-8286
  # Statements test set up is not ready
  @manual @secondary @imageInjection @sensorInstrument
  Scenario: TC_005 Success Scenario: Verify user is able to view MOBILE CHEQUE narrative and reference in statements transaction page
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I deposit a cheque <image> of <amount> with <reference>
    Then I confirm the cheque deposit
    When I select view deposit history button
    And I select the back header option in the cheque deposit page
    Then I am able to view the mobile cheque narrative and reference in the statements transaction page

  #MOB3-8308 #MOB3-8286
  # Statements test set up is not ready
  @manual @secondary @imageInjection @sensorInstrument
  Scenario: TC_006 Success Scenario: Verify user is able to view MOBILE CHEQUE narrative in statements transaction page
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I deposit a cheque <image> of <amount> without <reference>
    Then I confirm the cheque deposit
    When I select view deposit history button
    And I select the back header option in the cheque deposit page
    Then I am able to view mobile cheque narrative in the statements transaction page

 #MOB3-5746 #MOB3-5754
  @manual @secondary @sensorInstrument
  Scenario: TC_007 Negative Scenario: Verify IQA SDK error messages on front camera
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I select on the front of camera
    And I capture the front cheque image
    Then I should see an appropriate iqa sdk error messages

  #MOB3-5743 #MOB3-5753
  @manual @secondary @sensorInstrument
  Scenario: TC_008 Negative Scenario: Verify hard-coded SDK error messages on front camera
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I select on the front of camera
    And I capture the front cheque image
    Then I should see an appropriate hard-coded sdk error messages

  #MOB3-11805 UI design has been changed iOS App development is in progress
  @primary @android @imageInjection @sensorInstrument
  Scenario Outline: TC_009 Negative Scenario: Verifying the error message for user has entered deposit amount greater than cheque deposit transaction limit
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I deposit a cheque <image> of <amount> with <reference>
    Then I should see transaction limit error message in cheque deposit page
    Examples:
      |image                          |amount  |reference           |
      |LimitErrorMessage/FrontOfCheque|555     |Limit error message |

  #MOB3-6576
  @imageInjection @primary @environmentOnly @sensorInstrument
  Scenario Outline: TC_010 Negative Scenario: Verify duplicate cheque error message
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I deposit a cheque <image> of <amount> with <reference>
    And I confirm the cheque deposit
    Then I should receive an duplicate cheque error message on the failure screen for duplicate cheque <chequeOrder>
    Examples:
      |image                          |amount  |reference         |chequeOrder|
      |DuplicateCheque/FrontOfCheque  |10      | Success screen   | 1         |
      |DuplicateCheque/FrontOfCheque  |10      | duplicate cheque | 2         |

  #MOB3-6576 #@manual
  @primary @environmentOnly @imageInjection @manual @sensorInstrument
  Scenario Outline: TC_011 Negative Scenario: Verify Daily cheque limit error message
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I deposit cheques <image> of <amount> with <reference> for daily limit validation
    And I confirm the cheque deposit
    Then I should receive an daily limit error message for last cheque <chequeOrder>
    Examples:
      |image                     |amount  |reference       |chequeOrder|
      |DailyLimit/DailyLimit1    |400     | First Cheque   | 1         |
      |DailyLimit/DailyLimit2    |400     | Second Cheque  | 2         |
      |DailyLimit/DailyLimit3    |400     | Third Cheque   | 3         |

  #MOB3-5756
  @environmentOnly @imageInjection @sensorInstrument
  Scenario Outline: TC_012 Negative Scenario: Verify invalid sort code error message
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I deposit a cheque <image> of <amount> with <reference>
    And I confirm the cheque deposit
    Then I should be able to see the invalid sort code error message
    Examples:
      |image                 |amount  |reference        |
      |SortCode/FrontImage   |25      |verify sort code |

  #MOB3-6576
  @manual @sensorInstrument
  Scenario: TC_013 Negative Scenario: Validate the transaction number
    Given I am an enrolled "current and savings account" user logged into the app
    When I deposit a cheque with transaction code not in scope
    Then the failure screen should display a reject message

  #MOB3-6576
  @manual @secondary @sensorInstrument
  Scenario: TC_014 Negative Scenario: Validate the sort code and account number length
    Given I am an enrolled "current and savings account" user logged into the app
    When I deposit a cheque with code and account number of incorrect length
    Then the failure screen should display a reject message

  #MOB3-11984
  @manual @sensorInstrument
  Scenario: TC_015 Negative Scenario: Account permission handling
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I select the account navigation icon in deposit cheque page
    Then I should not see ineligible accounts for depositing a cheque

  #MOB3-11982, #MOB3-11803
  @primary @sensorInstrument
  Scenario Outline: TC_016 Business Scenario: Verify cheque deposit option entry point for valid accounts in account action menu
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to <type> account on home page
    And I select the action menu of <type> account
    And I select deposit cheque option in action menu
    Then I should be on the cheque deposit screen
    Examples:
      |type           |
      |islamicCurrent |

  #MOB3-11982, #MOB3-11803
  @primary @sensorInstrument
  Scenario Outline: TC_017 Business Scenario: Verify cheque deposit entry point for invalid account in account action menu
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to <type> account on home page
    And I select the action menu of <type> account
    Then I should not see deposit cheque option in action menu
    Examples:
      |type          |
      |personalLoan  |
      |creditCard    |
      |mortgage      |

  #MOB3-3932
  @primary @sensorInstrument
  Scenario: TC_018 Business Scenario: Customer should be able to view only the eligible accounts while depositing a cheque
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I select the account navigation icon in deposit cheque page
    Then I should be shown only the eligible accounts for depositing a cheque

  #MOB3-3932
  @bos @hfx @manual @secondary @sensorInstrument
  Scenario: TC_019 Business Scenario: Halifax Customer holds Co-service account with BoS
  So the user should be able to view eligible accounts in secondary brand.
    Given I am an enrolled "coservice" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I select the account navigation icon in deposit cheque page
    Then I should be shown eligible accounts for primary and secondary brands

  #MOB3-5348
  @secondary @sensorInstrument
  Scenario: TC_020 Business Scenario: Verifying Information dialogue box is displayed on first use
    Given I reinstall the app
    And I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page for more information via more menu
    Then I should see Information dialogue box popped up

  @manual @primary @sensorInstrument
  Scenario: TC021 Business Scenario: To verify the daily limit of cheque deposits after the limit changed in IBC
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I login into ibc and changed the daily limit of cheque deposit
    Then I should be able to see the changed daily limit in deposit cheque page

  @sensorInstrument
  Scenario: TC_022 Technical Scenario: Verify cheque deposit page
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    Then I see below fields on cheque deposit page
      |chequeDepositOptions |
      |depositAccount       |
      |chequeAmountField    |
      |chequeReferenceField |
      |captureFrontOfChequeButton|
      |captureBackOfImageButton  |

  #MOB3-10062
  @imageInjection @manual @sensorInstrument
  Scenario Outline:TC_023 Technical Scenario: Verify fast cheque icon pages in cheque deposit success screen
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    Then I should be able to see the fast cheque icon in cheque deposit page
    When I deposit a cheque <image> of <amount> with <reference>
    And I confirm the cheque deposit
    Then I should be able to see the fast cheque icon in deposit success screen
    Examples:
      |image        |amount  |reference       |
      |FrontOfCheque|12      | Success screen |

  #MOB3-3928
  @sensorInstrument
  Scenario Outline: TC_024 Technical Scenario: Verify Reference field accepts appropriate alphanumeric value of length 18
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I select the cheque reference field
    And I enter correct alphanumeric characters of length 18 as <reference>
    Then I should be able to view the 18 characters in reference
    Examples:
      |reference         |
      |testDepositJourney|

  #MOB3-3928
  #TODO:Android tag should be removed once PAYMOB-632
  @android
  Scenario Outline: TC_025 Technical Scenario: Minimum amount validation in cheque amount field
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I enter cheque amount less than minimum <invalidAmount>
    Then I should not be displayed with entered <invalidAmount>
    Examples:
      |invalidAmount|
      |0.00         |

  #MOB3-5348
  @environmentOnly @sensorInstrument
  Scenario: TC_026 Technical Scenario: Verifying Information dialogue box is not shown once selected
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page for more information via more menu
    Then I should see the checkbox for information dialogue is selected
    When I navigate to logoff via more menu
    And I select back to logon button on the logout page
    Then I should be on the environment selector page
    When I select the environment
    And I login into the app from memorable information page
    And I navigate to cheque deposit page via more menu
    Then I should not be displayed information regarding all cheques being supported

  #MOB3-5348
  @environmentOnly @sensorInstrument
  Scenario: TC_027 Technical Scenario: Verifying Information dialogue box is shown when checkbox is not selected
    Given I am a current and savings account user on the home page
    When I navigate to cheque deposit page for more information via more menu
    Then I should see the checkbox for information dialogue is not selected
    When I navigate to logoff via more menu
    And I select back to logon button on the logout page
    Then I should be on the environment selector page
    When I select the environment
    And I login into the app from memorable information page
    And I navigate to cheque deposit page via more menu
    Then I see information regarding all cheques not being accepted

  #MOB3-6630 #MOB3-9504 #MOB3-5743
  @sensorInstrument
  Scenario: TC_028 Technical Scenario: Validate the camera to capture image from device camera
    Given I reinstall the app
    And I am an enrolled "current and savings account" user logged into the app
    And I navigate to cheque deposit page via more menu
    And I select on the front of camera button
    When I select allow in camera permission pop up
    And I close the ics view demo screen
    Then I should be allowed to capture an image

   #MOB3-9508 #MOB3-9509 #DPL-128
  @secondary @imageInjection @sensorInstrument
  Scenario Outline: TC_029 Technical Scenario: Users attempts to leave the cheque deposit journey with and without scanned cheques
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I capture front and back of cheque <image>
    And I select the back header option in the cheque deposit page
    Then I should see winback option in the middle of the screen with stay and leave option
    When I choose to Leave option on winback dialogue
    Then I should be on more menu
    When I navigate to cheque deposit page via more menu
    And I enter correct cheque transaction limit <validAmount>
    And I enter correct alphanumeric characters of length 18 as <reference>
    And I select the back header option in the cheque deposit page
    Then I should be on more menu
    Examples:
      | image         | validAmount | reference          |
      | FrontOfCheque | 1.00        | testDepositJourney |

  #MOB3-9508 #MOB3-9509 #DPL-128 #DPL-723
  #The tag @android was added as the scenario is android specific and clicking on bottom navigation will trigger winback in only android. Confirmed with BA this is a expected behaviour.
  @secondary @imageInjection @android @sensorInstrument
  Scenario Outline: TC_031 Technical Scenario: Users attempts to leave the cheque deposit journey but decided to continue the journey and then decides to leave the journey and comes back on Cheque-Deposit screen to check if a new session is started.
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I capture front and back of cheque <image>
    And I select the back header option in the cheque deposit page
    Then I should see winback option in the middle of the screen with stay and leave option
    And on selection of stay option I should stay on cheque deposit homepage
    And I select the account navigation icon in deposit cheque page
    And I select <bottomTabBarIcons> tab from bottom navigation bar
    And I should see winback option in the middle of the screen with stay and leave option
    And I choose to Leave option on winback dialogue
    And I navigate to cheque deposit page via more menu
    And I see below fields on cheque deposit page
      |chequeDepositOptions |
      |depositAccount       |
      |chequeAmountField    |
      |chequeReferenceField |
      |captureFrontOfChequeButton|
      |captureBackOfImageButton  |
    Examples:
      | image           | bottomTabBarIcons |
      | FrontOfCheque   | home |

  #DPL-723
  #The tag @android was added as the scenario is android specific and clicking on bottom navigation will trigger winback in only android. Confirmed with BA this is a expected behaviour.
  @secondary @imageInjection @android @sensorInstrument
  Scenario Outline: TC_041 Technical Scenario: Users attempts to go back from Review screen to Cheque-Deposit screen to confirm if all the details enetered are still there.
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I deposit a cheque <image> of <amount> with <reference>
    And I select <bottomTabBarIcons> tab from bottom navigation bar
    Then I should see winback option in the middle of the screen with stay and leave option
    And I choose to Stay option on winback dialogue
    And I should be able to see review deposit screen
    And I select the back header option in the cheque deposit page
    And I should see cheque thumbnail images in deposit screen
    Examples:
      | image         | amount  | reference       | bottomTabBarIcons |
      | FrontOfCheque | 111     | Test amount     | home              |

  #MOB3-9504
  @android @secondary @sensorInstrument
  Scenario: TC_031 Technical Scenario: Validate the app take to the settings alert in android after deny to camera permission
    Given I reinstall the app
    And I am an enrolled "current and savings account" user logged into the app
    And I navigate to cheque deposit page via more menu
    And I select on the front of camera button
    When I deny in camera permission pop up and select front of camera
    And I select the checkbox and click deny again
    Then dialog box should pop up with settings option to enable camera
    When I select settings and enable camera permission
    Then I should be able to use camera on cheque deposit page

  #MOB3-6630
  #TODO This scenario need to be validated after installation of app
  #TODO current framework design is not supporting the reinstall in STUB env, hence kept as manual
  @manual @ios @secondary @sensorInstrument
  Scenario: TC_032 Technical Scenario: Validate the settings button after deny to camera permission in ios
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I select on the front of camera button
    And I deny in camera permission pop up and select front of camera
    Then goto settings button should be enabled to give camera permission

 #MOB3-3939 #MOB3-3934
  @secondary @imageInjection @environmentOnly @sensorInstrument
  Scenario Outline: TC_033 Technical Scenario: Verify an amount mismatch popup and confirm when cheque amount is different than user amount
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I deposit a cheque <image> of <amount> with <reference>
    And I select confirm button in review deposit page
    Then I should see an amount mismatch popup
    And on selection of confirm button I should see deposit success screen
    Examples:
      |image                       |amount  |reference       |
      |AmountMismatch/FrontOfCheque|111     |amount mismatch |

 #MOB3-3939 #MOB3934
  @secondary @environmentOnly @imageInjection @sensorInstrument
  Scenario Outline: TC_034 Technical Scenario: Verify an amount mismatch popup and cancel to check navigation to deposit home screen
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I deposit a cheque <image> of <amount> with <reference>
    And I select confirm button in review deposit page
    Then I should see an amount mismatch popup
    And on selection of cancel button I should navigate to deposit home screen
    Examples:
      |image                          |amount   |reference     |
      |AmountMismatch/FrontOfCheque   |121      |Review screen |

  #MOB3-10059
  @ios @sensorInstrument
  Scenario Outline:TC_035 Technical Scenario: Verify auto-logoff alert popup in cheque scanning page
    Given I am an enrolled "current and savings account" user logged into the app
    And I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    And I select the desired "<autoLogOffTime>" from the auto log off page
    And I navigate to cheque deposit page via more menu
    And I select on the front of camera
    And I select the logoff button 15 seconds before the "<autoLogOffTime>" auto logoff time in sdk
    Then I should be on the logout page
    Examples:
      | autoLogOffTime |
      | 1 Minute       |

  #MOB3-6570
  @android @sensorInstrument
  Scenario Outline:TC_036 Technical Scenario: Verify auto-logoff alert popup in cheque scanning page
    Given I am an enrolled "current and savings account" user logged into the app
    And I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    And I select the desired "<autoLogOffTime>" from the auto log off page
    And I navigate to cheque deposit page via more menu
    And I select on the front of camera
    And I select close button after desired "<autoLogOffTime>" from camera sdk page
    Then I should be on the logout page
    Examples:
      | autoLogOffTime |
      | 5 Minute       |

  #MOB3-3945
  @manual @primary @sensorInstrument
  Scenario: TC_037 Technical Scenario: Verifying ics view demo screens are displayed when selected
    Given I am an enrolled "current and savings account" user logged into the app
    When  I navigate to cheque deposit page via more menu
    And I select view demo link in cheque deposit page
    Then I see ics demo on a scrollable page in landscape with help text

  #MOB3-3945
  @manual @sensorInstrument
  Scenario: TC_038 Technical Scenario: Verifying view demo screens are displayed on first use of cheque deposit
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I select front of cheque on cheque deposit page
    Then I see ics demo on a scrollable page in landscape with help text

  @sensorInstrument
  Scenario Outline: TC_039 Verify the win-back popup's and should be on the cheque deposit page when clicked on stay option
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I enter correct alphanumeric characters of length 18 as testDepositJourney
    And I select <bottomTabBarIcons> tab from bottom navigation bar
    Then I should be on the home page
    Examples:
      |bottomTabBarIcons|
      |home             |
      |apply            |
      |support          |
      |more             |

  @sensorInstrument
  Scenario Outline: TC_040 Verify the win-back popup's and should be on the more menu page when clicked on leave option
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I enter correct alphanumeric characters of length 18 as testDepositJourney
    And I select <bottomTabBarIcons> tab from bottom navigation bar
    Then I should be on the payment hub page
    Examples:
      |bottomTabBarIcons|
      |payAndTransfer   |
