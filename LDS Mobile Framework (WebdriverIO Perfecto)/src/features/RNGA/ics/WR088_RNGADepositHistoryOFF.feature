@team-payments @nga3 @rnga @stub-current-and-savings-account
Feature: Deposit History(RNGA)
  In order to see the deposited cheques and deposit history by toggling between the journey.
  As a Retail NGA User
  I want to toggle between Deposit history and Cheque deposit pages

  Scenario:TC_001 Verify that user not able to see the cheque history page
    Given I am an enrolled "current and savings account" user logged into the app
    And I navigate to cheque deposit page via more menu
    When I tap on deposit history from deposit cheque page
    Then I should be able to see appropriate error message on deposit history page
      |errorMsg          |
      |technicalErrorText|
      |accountErrorText  |
