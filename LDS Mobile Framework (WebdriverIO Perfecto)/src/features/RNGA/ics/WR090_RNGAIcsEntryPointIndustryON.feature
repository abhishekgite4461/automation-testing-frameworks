@team-payments @nga3 @rnga @stub-current-and-savings-account @sensorInstrument
Feature: To verify Stub response of ICS Journey with Industry switch ON.
  In order to know the stub response of Industry switch
  As RNGA User ,
  I want to perform cheque deposit journey with Industry switch ON .
  So that,the Inclusion table in Alogent will be active and it should accept all cheques.

  Scenario Outline: TC_001 Main Success Scenario: User deposits cheque with Industry ON
  and capture the valid cheque then able to deposit the cheque successfully
    Given I am an enrolled "current and savings account" user logged into the app
    When I navigate to cheque deposit page via more menu
    And I deposit a valid cheque <image> of <amount> with <reference>
    Then I should see thumbnail images in deposit screen
    When I select review deposit button
    And I select thumbnail for front image
    Then I should be able to see the full screen image
    When I close the cheque image
    Then I should be able to see review deposit screen
    When I select thumbnail for back image
    Then I should be able to see the full screen image
    When I close the cheque image
    And I confirm the cheque deposit
    Then I should be able to see deposit success screen
    Examples:
      |image                              |amount  |reference       |
      |DepositSuccess/NonHomeFrontOfCheque|15      | Success screen |
