@team-payments @nga3 @stub-current-and-savings-account @rnga
Feature: Deposit History
  In order to check the status of the cheques I have deposited via RNGA
  As a Retail NGA User
  I want to check my deposit history using deposit history journey

  Background:
    Given I am an enrolled "current and savings account" user logged into the app
    And I navigate to deposit history page via more menu

  #MOB3-5116
  @primary
  Scenario: TC_001 Verify deposit history page
    When I am on deposit history page
    Then I see the below fields on deposit history page
      |historyPageDetails|
      |depositDate|
      |depositReference|
      |depositStatus|
      |depositAmount|

  #MOB3-5116
  Scenario: TC_002 Verify account picker in deposit history page
    When I am on deposit history page
    Then I see the below account picker fields on deposit history page
      |accountPickerList |
      |accountType       |
      |sortCode          |
      |accountNumber     |
      |ledgerBalance     |
      |availableBalance  |

  #MOB3-5124
  Scenario: TC_003 Verify deposit history review page
    When I select an item in deposit history list
    Then I see the below fields in deposit review page
      |depositReviewFields|
      |depositStatus      |
      |depositAccountType |
      |depositedDate      |
      |depositAmount      |
      |depositReference   |

  #MOB3-5116
  @manual @secondary
  Scenario: TC_004_Verify retrieval of data
    When I am on deposit history page
    Then I should be able to see the last 50 transactions with in 99 days of cheque history information

 #MOB3-5116
  # defect on Android stubs , ios works fine
  @primary @defect
  Scenario: TC_005_Verify history transactions for account change
    When I change the account which is displayed in the history page
    Then I should see history details for the newly selected account

  #MOB3-5116
  @manual
  Scenario: TC_006_Verify display of cheque transaction technical failure message (MG-029)
    When the service to bring back cheque transaction fails due to technical issue
    Then I should see general technical error message on screen

  #MOB3-5116
  @environmentOnly
  Scenario: TC_007_Verify display of cheque transaction message for no transactions (HC-078)
    When I am on deposit history page
    Then I should see no transactions message in the deposit history page

  #MOB3-5120
  @primary
  Scenario: TC_008_Verify data items in pending cheque details on deposit history details screen
    When I select the pending cheque details page
    Then I should be shown message mapped to status code related to pending received from Clearing Status field

  #MOB3-5120
  @primary
  Scenario: TC_009_Verify data items in funds available cheque details deposit history details screen
    When I select the funds available cheque details page
    Then I should be shown message mapped to status code related to funds available received from Clearing Status field

 #MOB3-5120
  @secondary
  Scenario: TC_010_Verify data items in rejected cheque details deposit history details screen
    When I select the rejected cheque details page
    Then I should be shown message mapped to status code related to rejected received from Clearing Status field

  #MOB3-5755
  Scenario: TC_011_Verify hint to tap image in the front cheque image
    When I select an item in deposit history list
    Then I should see the hint to tap to view enlarged image

  #MOB3-5755
  @secondary
  Scenario: TC_012_Verify enlarged image after clicking the zoom tap image
    And I select an item in deposit history list
    When I select the cheque image
    Then I should be able to see the enlarged cheque image

  #MOB3-5124
  @manual
  Scenario: TC_013_Verify load indicator on image area while retrieving front cheque image
    When I select an item in deposit history list
    Then I view loading indicator on image area when front image is retrieving

  #MOB3-5124
  @manual
  Scenario: TC_014_Verify load indicator on image area while retrieving front cheque image
    And I select an item in deposit history list
    When the cheque image retrieval process fails
    Then I should see appropriate retrieval failure message

  #MOB3-5124
  Scenario: TC_015_Verify user is returning to deposit history review page after closing the full cheque image
    And I select an item in deposit history list
    And I select the cheque image
    When I close the cheque image
    Then I should return to the history review of the selected cheque

  #MOB3-5116
  @environmentOnly
  Scenario: TC_016_Verify message at end of display of cheque transaction (HC-077)
    When I finish scrolling through all available transactions
    Then I should see a message that I have reached the end of available transactions

  #MOB-11984
  @manual
  Scenario: TC_017_Account permission handling
    When I select an account ineligible to view history
    Then I should see an ineligible account error message in deposit history page
