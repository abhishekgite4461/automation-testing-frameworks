@team-performance @nga3 @rnga @environmentOnly @lds @ios
#registered P2P testdata is available only for lloyds

#MOB3-1301
Feature: User wants access their P2P settings
  In order to manage my P2P preferences
  As a RNGA Authenticated customer
  I want to be able to de-register my phone number

  Background:
    Given I am an enrolled "registered P2P" user logged into the app

  @primary
  Scenario: TC_001_User navigates to P2P settings from more menu
    When I navigate to pay a contact settings page via more menu
    Then I should be on the pay a contact setting webview page

  Scenario: TC_002_User navigates to P2P settings from settings menu
    When I navigate to settings page from home page
    And I select pay a contact settings from the settings menu
    Then I should be on the pay a contact setting webview page
