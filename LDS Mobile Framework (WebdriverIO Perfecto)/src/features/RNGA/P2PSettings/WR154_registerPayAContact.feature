@team-performance @nga3 @rnga @environmentOnly
#testdata is available for lloyds brand only

#MOB3-728
Feature: User wants register for pay a contact
  In order to send or receive money via my phone number
  As a RNGA authenticated customer
  I want to be able to register my phone number through the mobile app

  Background:
    Given I am an enrolled "not registered P2P" user logged into the app

  @primary
  Scenario: TC_001_User navigates to P2P settings from more menu
    When I navigate to pay a contact settings page via more menu
    Then I should be on the register for pay a contact page

  @primary
  Scenario: TC_002_User navigates to P2P settings from settings menu
    When I navigate to settings page from home page
    And I select pay a contact settings from the settings menu
    Then I should be on the register for pay a contact page

  Scenario: TC_003_User navigates to P2P settings from add new beneficiary winback
    And I navigate to add new recipient option using payment hub
    And I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    When I select the pay Uk number tooltip on beneficiary type page
    Then I should see a pop for pay a contact registration
    When I select register now button on the popup in the p2p page
    Then I should be on the register for pay a contact page
