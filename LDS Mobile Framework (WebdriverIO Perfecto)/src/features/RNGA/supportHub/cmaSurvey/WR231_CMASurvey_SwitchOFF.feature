@team-pi @stub-enrolled-typical @sw-cma-nga-off @nga3 @rnga @primary
Feature: Verify user is displayed CMA Survey
  In order to compare the service quality across the banking industry
  As a Retail NGA User
  I want to be able to see the CMA Survey results

  #MCC-508, #MCC-665
  Scenario: TC_001_Customer Cannot See CMA Survey Entry Point
    Given I am an enrolled "current" user logged into the app
    When I select support tab from bottom navigation bar
    Then I should not see CMA Tile in the support hub home page
