@team-pi @stub-enrolled-typical @sw-cma-nga-on @nga3 @rnga @ios
Feature: Verify user is displayed CMA Survey
  In order to compare the service quality across the banking industry
  As a Retail NGA User
  I want to be able to see the CMA Survey results

  #MCC-508, #MCC-509, #MCC-665, #MCC-666
  Scenario: TC_001_Customer Can View CMA Survey Page
    Given I am an enrolled "current" user logged into the app
    When I select support tab from bottom navigation bar
    And I select call us on support hub page
    And I select the CMA survey link
    Then I am displayed the CMA survey page in my mobile browser
