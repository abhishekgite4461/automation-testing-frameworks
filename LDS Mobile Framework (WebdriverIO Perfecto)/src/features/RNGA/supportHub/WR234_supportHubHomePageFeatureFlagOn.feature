@team-pi @nga3 @rnga @stub-enrolled-typical @sw-card-management-on @sw-mobile-chat-ios-on @sw-mobile-chat-android-on @primary
Feature: Verify user successfully taken to the support hub page
  As a customer
  I want access to a support hub
  So that I can find the assistance I need And resolve any queries

  Background:
    Given I am an enrolled "current" user logged into the app

  #MCC-989, #MCC-1014, #MCC-996, #MCC-1013, #MCC-1014, #MCC-994, #MCC-1002, #MCC-1025, #MCC-1003, #MCC-1026, #MCC-989  #MCC-1014, #MCC-999
  # MCC-992, #MCC-1012 #MCC-998, #MCC-1019 #MCC-997, #MCC-1011
  Scenario: Verify customer views Support Hub
    When I select support tab from bottom navigation bar
    Then I should be on the support hub page
    And I should see message us option
    And I should see call us option
    When I select "atm and branch" option in the support hub page
    Then I should be on the atm and branch finder page
    And I navigate back
    And I should see "CMA Tile" in the support hub home page
    And I should see "app feedback tile" in the support hub home page
    And I validate carousel options on support hub page
      | carouselOptions     |
      | resetPassword       |
      | lostOrStolenCard    |
      | replaceCardOrPin    |
      | changeOfAddress     |

  #MCC-508, #MCC-509, #MCC-665, #MCC-666
  @manual
  Scenario: Customer Can View CMA Survey Page
    When I select support tab from bottom navigation bar
    Then I select the CMA survey link
    And I am displayed the CMA survey page in my mobile browser

  # MCC-994, MCC-1026
  Scenario: Customer selects ATM and Branch Finder from the Support Hub Page
    When I select support tab from bottom navigation bar
    Then I am on the atm and branch finder page
    And bottom navigation bar is displayed
    And I am displayed the following search options
      | searchOption       |
      | searchBranchesTile |
      | findNearbyAtmTile  |
    And I am displayed a note message stating clicking search option links take you outside the app
