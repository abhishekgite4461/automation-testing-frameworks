@team-pi @nga3 @rnga @stub-enrolled-valid
Feature: User wants to search for nearby ATM or Branches
  In order to withdraw money or visit a bank branch
  As a RNGA customer
  I want to search for ATM or Bank Branch

  #PIDE-285
  Background:
    Given I am an enrolled "current" user logged into the app
    When I select support tab from bottom navigation bar
    And I am on the atm and branch finder page

  # MOB3-10076, 10054
  @secondary
  Scenario Outline: TC_003 Validate search results for Branches and ATM's
    When I select the <tile_type> option on the atm and branch finder page
    Then I am taken out of the application to the default map and displayed correct search keyword for <search_result>
    And I select map back button
    And I should be on the atm and branch finder page
    Examples:
      | tile_type       | search_result |
      | search branches | Branch        |
      | find nearby ATM | atm           |

  @secondary @mbnaSuite
  Scenario Outline: TC_003 Validate search results for Branches and ATM's
    When I select the <tile_type> option on the atm and branch finder page
    Then I am taken out of the application to the default map and displayed correct search keyword for <search_result>
    And I select map back button
    And I should be on the atm and branch finder page
    Examples:
      | tile_type       | search_result |
      | find nearby ATM | atm           |

   # MOB3-10170, 10172
  @primary
  Scenario Outline: TC_004 Customer tries to search branch based on the brands - Lloyds, halifax and BoS
    When I search based on <brand_type> brand
    Then I am displayed the results based on the <branch_type> brands
    Examples:
      | brand_type | branch_type     |
      | Lloyds     | Lloyds          |
      | Halifax    | Halifax and BoS |
      | BoS        | Halifax and BoS |

  @secondary
  Scenario Outline:TC_005 customer tries to find atm and branch with location services turned OFF
    When I select the <tile_type> option on the atm and branch finder page
    Then I should be displayed an option to turn ON the location services on the mobile device
    Examples:
      | tile_type       |
      | search branches |
      | find nearby ATM |
