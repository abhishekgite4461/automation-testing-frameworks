@team-pi @nga3 @rnga @stub-enrolled-typical
Feature: Verify user successfully taken to chat window page
  In order to resolve my query quickly
  As a Retail NGA User
  I want to allow customer to engage with LBG agents in Connect in their own time and anytime

  Background:
    Given I am an enrolled "current" user logged into the app
    And I select support tab from bottom navigation bar
    When I select message us option from support hub home page

  #MCC-280
  @primary
  Scenario: TC_001_Customer is able to type and send message
    Then I should be on the message us home page
    When I send my question "ATM" to agent or bot to answer it
    Then I validate chat modal window and close it

  #MCC-279, #MCC-280, #MCC-406, #MCC-282, #MCC-427, #MCC-291, #MCC-427, #MCC-287, #MCC-405, #MCC-301, #MCC-342, #MCC-400
  #3rd party intervention is required to automate this scenario
  @primary @manualOnly
  Scenario: TC_002_Customer successfully taken to Chat Window and views below assets
    Then I should be able to view these chat options
      | chatOptions                  |
      | greetMessage                 |
      | closeButton                  |
      | agentIcon                    |
      | dateAtTop                    |
      | font                         |
      | timeStamp                    |
      | textBubble                   |
      | agentIcon                    |
      | sendButton                   |
      | authenticationMessage        |
      | chatHistory                  |
      | agentTypingIndicator         |
      | systemMessages               |
      | unreadMessage                |
      | closedConversation           |

  #MCC-281, #MCC-402
  #3rd party intervention is required to automate this scenario
  @primary @manualOnly
  Scenario: TC_003_Customer is able to type and send message
    Then I am able to send message to the agent

  #MCC-280, #MCC-342
  #3rd party intervention is required to automate this scenario
  @manualOnly
  Scenario: TC_004_Customer is shown an exception message due to network error(WiFi - Off)
    Then network has been interrupted
    And I should be able to view these chat options
      | chatOptions                     |
      | authenticationErrorMessage      |
