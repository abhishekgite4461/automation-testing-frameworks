@team-pi @nga3 @rnga @stub-enrolled-typical @sw-mobile-chat-ios-off @sw-mobile-chat-android-off @primary
Feature: Verify user successfully taken to chat window page – switch off
  In order to resolve my query quickly
  As a Retail NGA User
  I want to allow customers to engage with LBG agents in Connect in their own time and anytime

  #MCC-486, #MCC-485
  Scenario: TC_001_Chat to Us Entry Point not displayed when Enable Mobile Chat switch is Off
    Given I am an enrolled "current" user logged into the app
    When I select support tab from bottom navigation bar
    Then I should not see message us option
