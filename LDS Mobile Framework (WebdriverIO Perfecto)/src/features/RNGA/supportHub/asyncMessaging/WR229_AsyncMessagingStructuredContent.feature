@team-pi @nga3 @rnga @stub-enrolled-typical @sw-mobile-chat-ios-on @sw-mobile-chat-android-on @manualOnly
#   Desktop Chat is required. hence it is not feasible for automation
Feature: Verify user is displayed structured content
  In order to communicate quickly and intuitively
  As a Retail NGA User
  I want to be able to receive structured content

  Background:
    Given I am an enrolled "current" user logged into the app
    And I am on the Message Us page

#MCC-426, #MCC-283
  Scenario: TC_001_Customer Can See Multiple Choice Options
    And the Chat Bot sends multiple selectable options
    When I view the options that have been sent
    Then the options are displayed as clickable buttons
    And I can select my option

#MCC-426, #MCC-283
  Scenario: TC_002_Customer Can Click Multiple Choice Answers
    And the Bot Agent sends multiple selectable options
    When I select my option
    Then the selected option is sent back to the Bot Agent
    And I can see my selected option in the chat history

#MCC-426, #MCC-283
  Scenario: TC_003_Customer Ignores Multiple Choice And Types Answer
    And the Bot Agent sends multiple selectable options
    And I do not select an option sent by the bot
    When I use the free text option
    Then I can type my response in the message edit box and send it to the bot agent

#MCC-428, #MCC-284
  @primary
  Scenario: TC_004_Customer Can See DeepLink URL
    And the agent sends DeepLink URLs
    When I view the URL on the Message Us screen
    Then I am shown the link as a clickable option

#MCC-428, #MCC-284
  Scenario: TC_005_Customer Can See Multiple DeepLink URLs
    And the agent sends multiple DeepLink URLs
    When I view the multiple URLs on the Message Us screen
    Then each link is shown as a clickable option

#MCC-428, #MCC-284
  @primary
  Scenario: TC_006_Customer Clicks DeepLink URL
    And the agent sends DeepLink URLs
    When I select the URL by tapping on it
    Then I am taken to the page designated within the URL
