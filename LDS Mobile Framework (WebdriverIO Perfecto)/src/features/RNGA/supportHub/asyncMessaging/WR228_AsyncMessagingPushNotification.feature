@team-pi @nga3 @rnga @stub-enrolled-typical @sw-mobile-chat-ios-on @sw-mobile-chat-android-on @manualOnly
#  Desktop Chat is required. Hence it is not feasible for automation
Feature: Verify Customer Receives Notifications
  In order to know I have received a response
  As a Retail NGA User
  I want to receive notifications when an agent responds to my message

#MCC-285, #MCC-544
  Scenario: TC_001_Customer Receives Notification App Closed
    Given I am an enrolled "current" user
    And not logged into the app
    When I am sent a chat message
    Then I receive a push notification advising me I have unread messages
    And when I select the notification the NGA is launched

#MCC-288, #MCC-543
  Scenario: TC_002_ Customer Receives Notification Not On Chat Window
    Given I am an enrolled "current" user logged into the app
    And I am not on the Chat Window
    When I am sent a chat message
    Then I receive an in-app notification advising me I have unread messages
    And when I select the notification it is dismissed

#MCC-288, #MCC543
  Scenario: TC_03_ Customer Receives Notification When NGA Minimised
    Given I am an enrolled "current" user logged into the app
    And I the NGA is minimised
    When I am sent a chat message
    Then I receive a notification advising me I have unread messages
    And when I select the notification I am taken back into the NGA

#MCC-286, #MCC-670
  Scenario: TC_04_Unread Message Count on Global menu
    Given I am an enrolled "current" user logged into the app
    And I have unread messages in my inbox and/or chat window
    When the Global menu icon is visible
    Then I am shown an indicator on the Global Menu
    And it displays the total number of unread messages

#MCC-286, #MCC-670
  Scenario: TC_05_Unread Message Count on Entry Point
    Given I am an enrolled "current" user logged into the app
    And I have unread chat messages
    When I view the Global Menu
    Then I am shown an indicator on the Chat entry point
    And it displays the number of unread messages

#MCC-289, #MCC-664
  Scenario: TC_06_App Icon Unread Message Flag
    Given I am an enrolled "current" user
    And not logged into the app
    And I have unread messages in the chat window
    When I view the NGA icon
    Then I am shown an unread message flag
    And it shows the total number of unread messages

#MCC-289, #MCC-664
  Scenario: TC_07_App Icon No Unread Messages
    Given I am an enrolled "current" user
    And not logged into the app
    And I have no unread messages in the chat window
    When I view the NGA icon
    Then I am not shown an unread message flag

#MCC-610
  @android
  Scenario: TC_08_Android Customer Has Push Enabled And Exits Chat
    Given I am an enrolled "current" user logged into the app
    And I am on the chat window
    And push notifications for NGA are enabled on my device
    When I exit the chat window
    Then I am returned to the previous screen

#MCC-610
  @android
  Scenario: TC_09_Android Customer Has Push Blocked And Exits Chat
    Given I am an enrolled "current" user logged into the app
    And I am on the chat window
    And push notifications for NGA are blocked on my device
    And I have not previously been shown the Chat Push Notification Opt-In Screen
    When I exit the chat window
    Then I am shown the Chat Push Notification Opt-In Screen
    And I have the option to enable push notifications
    And I have the option to select ‘not now’

#MCC-610
  @android
  Scenario: TC_10_Android Customer Viewing chat Push Notification Opt-In Screen Enables Notifications
    Given I am an enrolled "current" user logged into the app
    And I am viewing the Chat Push Notification Opt-In Screen
    When I choose to enable push notifications
    Then I am displayed instructions on how to enable push on my device for NGA
    And I am presented the option to view my device push settings for NGA
    And when I select that option, my device push settings for NGA are displayed (OFF)
    And I can toggle my device push settings for NGA ON
    And when toggled ON I am displayed a message confirming that I have turned device push notifications ON
    And if I select BACK, I am taken to Chat Push Notification Opt-In Screen

#MCC-610
  @android
  Scenario: TC_11_Android Customer Viewing chat Push Notification Opt-In Screen Choses Not Now
    Given I am an enrolled "current" user logged into the app
    And I am viewing the Chat Push Notification Opt-In Screen
    When I choose to ‘not now’
    Then the chat window is closed
    And I am taken back to the previous screen

#MCC-595
  @android
  Scenario: TC_12_NGA Force Closed Before Push Enabled
    Given I am an enrolled "current" user logged into the app
    And I am viewing the Chat Push Notification Opt-In Screen
    When the NGA is force closed
    Then no enable/’not now’ decision is recorded
    And I will be shown the Chat Push Notification Opt-In Screen next time I close the chat window

#MCC-595
  @android
  Scenario: TC_13_NGA Timed Out Before Push Enabled
    Given I am an enrolled "current" user logged into the app
    And I am viewing the Chat Push Notification Opt-In Screen
    When the NGA session times out
    Then no enable/’not now’ decision is recorded
    And I will be shown the Chat Push Notification Opt-In Screen next time I close the chat window

#MCC-595
  @android
  Scenario: TC_14_NGA Minimised Before Push Enabled
    Given I am an enrolled "current" user logged into the app
    And I am viewing the Chat Push Notification Opt-In Screen
    When the NGA is minimised
    And the NGA session ends
    Then no enable/’not now’ decision is recorded
    And I will be shown the Chat Push Notification Opt-In Screen next time I close the chat window
