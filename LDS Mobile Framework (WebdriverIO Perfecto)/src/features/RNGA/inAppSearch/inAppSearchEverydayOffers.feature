@team-outboundmessaging @nga3 @rnga @ff-inapp-search-on @sw-inapp-search-android-on @stub-enrolled-typical @sw-inapp-search-ios-on

# Reason for Stub execution: Dependant on specific environment to run which varies based on backend change

Feature:Search Functionality
  In order to search Everyday Offers
  As an RNGA customer
  I want to see all eligible search results

    #AVC-4970,AVC-4910
  Scenario: TC_01 Search for Everyday Offers for customers without everyday offers
    Given I am an enrolled "opted out spending rewards" user logged into the app
    And I have to select on the search icon on home page
    And I will be on Search landing page
    When I enter search everyday offers in the search dialog box
    Then I should be able to see the set up everyday offers link as search result
    When I tap on the set up everyday offers link
    Then I should be on set up everyday offers page as per the searched keyword
