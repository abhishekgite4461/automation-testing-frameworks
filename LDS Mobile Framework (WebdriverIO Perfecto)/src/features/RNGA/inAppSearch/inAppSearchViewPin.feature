@team-outboundmessaging @nga3 @rnga @ff-inapp-search-on @sw-inapp-search-android-on @stub-enrolled-typical @sw-inapp-search-ios-on
@sw-viewpin-android-on @sw-viewpin-password-android-on @sw-viewpin-ios-on @sw-viewpin-password-ios-on

Feature: In App Search Functionality
  In order to navigate to the different features of the app
  As an RNGA customer
  I want to see all valid eligible search results

   #AVC-5165
  Scenario Outline: TC_01 Main Success Scenario :Search for View Pin
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the view pin link as search result
    When I tap on the view pin link
    Then I should be on view pin page as per the searched keyword
    Examples:
      | keyword     |
      | card        |
      | security    |
      | personnal   |
      | identify    |
      | number      |
      | safety      |
      | ATM         |
      | validat     |
      | verify      |
      | view        |
      | pin         |
      | PIN         |
      | show        |

  @manual
  Scenario Outline: TC_02 Main Success Scenario :Search for Replace card and PIN
    Given I am a current user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the replace card and pin link as search result
    When I tap on the replace card and pin link
    Then I should be on replace card and pin  page as per the searched keyword
    Examples:
      | keyword        |
      | lost	       |
      | stolen         |
      | number         |
      | new            |
      | report         |
      | personal       |
      | identification |
      | number         |
      | code           |
      | passcode       |
      | pass           |
      | replace        |
      | card           |
      | PIN            |
      | pin            |
      | damaged        |
      | forgotten      |
      | forgot         |

  @manual
  Scenario Outline: TC_03 Main Success Scenario :Search for Lost or stolen cards
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the card has been lost or stolen link as search result
    When I tap on the card has been lost or stolen  link
    Then I should be on lost or stolen cards  page as per the searched keyword
    Examples:
      | keyword        |
      | taken	       |
      | swiped	       |
      | burgled	       |
      | know	       |
      | theft	       |
      | left	       |
      | behind	       |
      | lose	       |
      | lost	       |
      | stole	       |
      | thief	       |
      | mugged	       |
      | card	       |
      | stolen	       |
