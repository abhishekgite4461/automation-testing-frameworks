@team-outboundmessaging @nga3 @rnga @ff-inapp-search-on @sw-inapp-search-android-on @stub-enrolled-typical @sw-inapp-search-ios-on

# Reason for Stub execution: Can only be executed in STUB as functionality is dependent on switch which is in progress

Feature: In App Search Functionality
  In order to navigate to the different features of the app
  As an RNGA customer
  I want to see all valid eligible search results

  #AVC-4921
  Scenario Outline: TC_01 Main Success Scenario :Search for reset mobile banking app
    Given I am a CURRENT user on the home page
    When I select the search icon on home page
    Then I will be on Search landing page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the reset mobile banking app link as search result
    When I tap on the reset mobile banking app link
    Then I should be on reset Mobile Banking App page as per the searched keyword
    Examples:
      | keyword   |
      | reset     |
      | logoff    |
      | register  |
      | mobile    |
