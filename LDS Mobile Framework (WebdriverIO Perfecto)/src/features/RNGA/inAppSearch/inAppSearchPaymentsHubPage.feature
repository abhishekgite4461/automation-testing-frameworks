@team-outboundmessaging @nga3 @rnga @ff-inapp-search-on @sw-inapp-search-android-on @stub-enrolled-typical @sw-inapp-search-ios-on @ios

Feature: In App Search Functionality
  In order to navigate to the different features of the app
  As an RNGA customer
  I want to see all valid eligible search results

  #AVC-4921
  Scenario Outline: TC_01 Main Success Scenario :Search for making a payment
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the make Payment link as search result
    When I tap on the make Payment link
    Then I should be on pay And Transfer page as per the searched keyword
    Examples:
      | keyword       |
      | pay           |
      | paid          |
      | give          |
      | send          |
      | transfer      |
      | transfers     |
      | transferred   |
      | paid          |
      | bill          |
      | make          |
      | payment       |

  #AVC-4921
  Scenario Outline: TC_02 Main Success Scenario :Search for making a credit card payment
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the Pay Credit Card link as search result
    When I tap on the pay Credit Card link
    Then I should be on pay And Transfer page as per the searched keyword
    Examples:
      | keyword           |
      | bill              |
      | avios             |
      | duo               |
      | clarity           |
      | barclaycard       |
      | via               |
      | mastercard        |
      | master card       |
      | amex              |
      | american          |
      | express           |
      | transfer          |
      | premier           |
      | airmiles          |
      | american express  |

  #AVC-4968
  Scenario Outline: TC_03 Main Success Scenario :Search for viewing your cheque deposit history
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the cheque Deposit History link as search result
    When I tap on the cheque Deposit History link
    Then I should be on cheque Deposit History page as per the searched keyword
    Examples:
      | keyword           |
      | checks            |
      | pay               |
      | in                |
      | paid              |
      | into              |
      | put               |
      | past              |
      | passed            |
      | photo             |
      | scan              |
      | echk              |
      | check             |
      | cheques           |
      | picture           |
      | cheque            |
      | deposit           |
      | history           |
      | submit            |
      | view              |
      | scanning          |

  #AVC-4968
  Scenario Outline: TC_04 Main Success Scenario :Search for deposit a cheque
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the deposit A Cheque link as search result
    When I tap on the deposit A Cheque link
    Then I should be on deposit A Cheque page as per the searched keyword
    Examples:
      | keyword           |
      | deposit           |
      | submit            |
      | scan              |
      | photo             |
      | picture           |
      | check             |
      | pay               |
      | in                |
      | cheque            |
      | scanning          |
