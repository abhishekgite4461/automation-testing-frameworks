@team-outboundmessaging @nga3 @rnga @ff-inapp-search-on @sw-inapp-search-android-on @stub-enrolled-typical @sw-inapp-search-ios-on @ios @manual

Feature: In App Search Functionality
  In order to navigate to the different features of the app
  As an RNGA customer
  I want to see all valid eligible search results

  #AVC-4917
  Scenario Outline: TC_01 Main Success Scenario :Search for How we contact you
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the manage paper-free preference link as search result
    When I tap on the manage paper-free preference link
    Then I should be on manage paper-free preference page as per the searched keyword
    Examples:
      | keyword        |
      | statement      |
      | correspondance |
      | statements     |
      | letter         |
      | letters        |
      | pdf            |
      | pdfs           |
      | communication  |
      | comms          |
      | access         |
      | change         |
      | chaging        |
      | manage         |
      | paperless      |
      | preferences    |
      | paperfree      |
      | customise      |
      | fully          |
