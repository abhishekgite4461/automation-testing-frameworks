@team-outboundmessaging @nga3 @rnga @ff-inapp-search-on @sw-inapp-search-android-on @stub-enrolled-typical @sw-inapp-search-ios-on

Feature: In App Search Functionality
  In order to navigate to the different features of the app
  As an RNGA customer
  I want to see all valid eligible search results

  #AVC-5211 #AVC-4974
  Scenario Outline: TC_01 Main Success Scenario: Search for Fingerprint/face scan logon settings
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the fingerprint/face scan logon settings link as search result
    When I tap on the fingerprint/face scan logon settings link
    Then I should be on security settings page as per the searched keyword
    Examples:
      | keyword         |
      | log             |
      | login           |
      | log in          |
      | signin          |
      | signon          |
      | sign on         |
      | faceid          |
      | face id         |
      | biometric       |
      | recognition     |
      | bio metric      |
      | bio-metric      |
      | identifier      |
      | verification    |
      | identification  |
      | authentication  |
      | password        |
      | memorable       |
      | information     |
      | logon           |
      | settings        |
      | log on          |
      | fingerprint     |
      | touchid         |
      | touch ID        |
      | touch           |
      | finger          |
      | thumb           |
      | finger id       |
      | finger printer  |
      | finger scan     |
      | fingerprinting  |
      | fingertip       |

  Scenario Outline: TC_02 Main Success Scenario: Search for auto sign out setting
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the choose auto sign out setting link as search result
    When I tap on the choose auto sign out setting link
    Then I should be on auto logout page as per the searched keyword
    Examples:
      | keyword       |
      | time          |
      | out           |
      | time-out      |
      | logout        |
      | log           |
      | off           |
      | out           |
      | automatically |
      | automatic     |
      | logged        |
      | logs          |
      | logging       |
      | sign          |
      | signing       |
      | signout       |
      | signingout    |
      | signedout     |
      | choose        |
      | chose         |
      | auto          |
      | logoff        |
      | settings      |
      | logs          |

  #AVC-5212 #AVC-4975
  Scenario Outline: TC_03 Main Success Scenario: Search for Security settings, Profile
    Given I am a valid user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the <search> link as search result
    When I tap on the <search> link
    Then I should be on security settings page as per the searched keyword
    Examples:
      | keyword     | search                            |
      | user        | View your User ID                 |
      | id          | View your User ID                 |
      | view        | View your device type             |
      | device      | View your device type             |
      | type        | View your device type             |
      | name        | View your device name             |
      | current app | View your current app version     |
      | version     | View your current app version     |
      | security    | View/amend your security settings |
      | settings    | View/amend your security settings |
      | view        | View/amend your security settings |
      | amend       | View/amend your security settings |
      | user        | View/amend your security settings |
      | id          | View/amend your security settings |
      | app         | View/amend your security settings |
      | version     | View/amend your security settings |
      | device      | View/amend your security settings |
      | name        | View/amend your security settings |
      | type        | View/amend your security settings |
      | forgotten   | View/amend your security settings |
      | password    | View/amend your security settings |
      | reset       | View/amend your security settings |
      | auto        | View/amend your security settings |
      | logoff      | View/amend your security settings |
      | log         | View/amend your security settings |
      | off         | View/amend your security settings |
      | setting     | View/amend your security settings |
      | log on      | View/amend your security settings |
      | face        | View/amend your security settings |
      | touch       | View/amend your security settings |
