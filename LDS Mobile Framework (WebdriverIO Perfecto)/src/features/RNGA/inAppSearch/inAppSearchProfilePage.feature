@team-outboundmessaging @nga3 @rnga @ff-inapp-search-on @sw-inapp-search-android-on @stub-enrolled-typical @sw-inapp-search-ios-on @ios

Feature: In App Search Functionality
  In order to navigate to the different features of the app
  As an RNGA customer
  I want to see all valid eligible search results

  #AVC-4923
  Scenario Outline: TC_01 Main Success Scenario :Search for change your address
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the change Your Address link as search result
    When I tap on the change Your Address link
    Then I should be on change Your Address page as per the searched keyword
    Examples:
      | keyword     |
      | post		|
      | code		|
      | letter		|
      | statement	|
      | house		|
      | home		|
      | live		|
      | living		|
      | moving		|
      | changing	|
      | changed		|
      | moved		|
      | adress		|
      | adresse		|
      | different	|
      | personal	|
      | details		|
      | change		|
      | address		|

  Scenario Outline: TC_02 Main Success Scenario :Search for Manage and view your marketing choices
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the manage And View Your Marketing Choices link as search result
    When I tap on the manage And View Your Marketing Choices link
    Then I should be on manage And View Your Marketing Choices page as per the searched keyword
    Examples:
      | keyword         |
      | change		    |
      | edit		    |
      | preference		|
      | preferences		|
      | message		    |
      | advert		    |
      | advertising		|
      | advertise		|
      | email		    |
      | e-mail		    |
      | call		    |
      | phone		    |
      | letter		    |
      | market		    |
      | marketing		|
      | choices		    |
      | manage		    |
      | view		    |

  Scenario Outline: TC_03 Main Success Scenario :Search for managing your personal details
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the manage Your Personal Details link as search result
    When I tap on the manage Your Personal Details link
    Then I should be on manage Your Personal Details page as per the searched keyword
    Examples:
      | keyword         |
      | personal		|
      | phone		    |
      | address		    |
      | adress		    |
      | info		    |
      | information		|
      | detail		    |
      | email		    |
      | contact		    |
      | personnal		|
      | details		    |
      | name		    |
      | manage		    |
      | number		    |
      | mobile		    |
      | home		    |
      | work		    |
      | user		    |
      | id		        |
      | postcode		|
      | data		    |
      | marketing		|
      | choices		    |
      | consent		    |

  Scenario Outline: TC_04 Main Success Scenario :Search for view your data consents for this device
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the view Your Data Consents link as search result
    When I tap on the view Your Data Consents link
    Then I should be on view Your Data Consents page as per the searched keyword
    Examples:
      | keyword     |
      | agree		|
      | preference	|
      | settings	|
      | legal		|
      | private		|
      | privacy		|
      | cookie		|
      | cookies		|
      | data		|
      | consent		|
      | this		|
      | device		|
      | view		|
      | consents	|

  Scenario Outline: TC_05 Main Success Scenario :Search for change your phone number
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the change Your Phone Number link as search result
    When I tap on the change Your Phone Number link
    Then I should be on change Your Phone Number page as per the searched keyword
    Examples:
      | keyword     |
      | change		|
      | phone		|
      | number		|
      | mobile		|
      | home		|
      | work		|
      | call		|
      | ring		|
      | contact		|
      | details		|
      | profile		|
      | personal	|
      | update		|
      | call		|

  Scenario Outline: TC_05 Main Success Scenario :Search for change your email address
    Given I am a CURRENT user on the home page
    When I enter search <keyword> in the search dialog box
    Then I should be able to see the change Your Email Address link as search result
    When I tap on the change Your Email Address link
    Then I should be on change Your Email Address page as per the searched keyword
    Examples:
      | keyword     |
      | change		|
      | email		|
      | address		|
      | contact		|
      | details		|
      | profile		|
      | personal	|
      | update		|
