@team-outboundmessaging @nga3 @rnga @ff-inapp-search-on @sw-inapp-search-android-on @stub-enrolled-typical @sw-inapp-search-ios-on

# Reason for Stub execution: Can only be executed in STUB as functionality is dependent on switch which is in progress

Feature:Search Functionality
  In order to search
  As an RNGA customer
  I want to see all eligible search results

  Background:
    Given I am a CURRENT user on the home page

  Scenario: TC_01 Search for Apply
    Given I have to select on the search icon on home page
    And I will be on Search landing page
