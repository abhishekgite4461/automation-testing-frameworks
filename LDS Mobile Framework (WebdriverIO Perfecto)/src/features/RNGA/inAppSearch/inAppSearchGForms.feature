@team-outboundmessaging @nga3 @rnga @ff-inapp-search-on @sw-inapp-search-android-on @stub-enrolled-typical @sw-inapp-search-ios-on @ios

Feature:Search Functionality
  In order to search GForms
  As an RNGA customer
  I want to see all eligible search results

#  AVC-5341
  Scenario:TC_01 Search for GForms -  Loan repayment holiday
    Given I am an enrolled "current" user logged into the app
    And I have to select on the search icon on home page
    When I will be on Search landing page
    Then I should see below search results populated for loan repayment
      | fields                    |
      | Loan                      |
      | borrow                    |
      | repayment                 |
      | repay                     |
      | help                      |
      | overdraft                 |
      | pause                     |
      | suspend                   |
      | holiday                   |
      | stop                      |
      | coronavirus               |
      | corona                    |
      | virus                     |
      | covid                     |
      | covid-19                  |
      | covid19                   |
      | covid 19                  |
      | earning                   |
      | earn                      |
      | salary                    |
      | paid                      |
      | break                     |
      | reduce                    |
      | grant                     |
      | repayments                |
      | struggling                |
      | interrupt                 |
      | cash flow                 |
      | cash-flow                 |
      | cashflow                  |
      | income                    |
      | crisis                    |
      | relief                    |
      | hold                      |
      | pandemic                  |
      | urgent                    |
      | breathing                 |
      | space                     |

  Scenario: TC_02 Search for GForms -  Mortgage repayment holiday
    Given I am an enrolled "current" user logged into the app
    And I have to select on the search icon on home page
    When I will be on Search landing page
    Then I should see below search results populated for mortgage repayment
      | fields               |
      | mortgage             |
      | repayment            |
      | home                 |
      | house                |
      | flat                 |
      | repay                |
      | borrow               |
      | help                 |
      | overdraft            |
      | pause                |
      | suspend              |
      | holiday              |
      | stop                 |
      | coronavirus          |
      | corona               |
      | virus                |
      | covid                |
      | covid-19             |
      | covid19              |
      | covid 19             |
      | earning              |
      | earn                 |
      | salary               |
      | paid                 |
      | break                |
      | reduce               |
      | grant                |
      | repayments           |
      | struggling           |
      | interrupt            |
      | cash flow            |
      | cash-flow            |
      | cashflow             |
      | income               |
      | crisis               |
      | relief               |
      | hold                 |
      | pandemic             |
      | urgent               |
      | breathing            |
      | space                |
