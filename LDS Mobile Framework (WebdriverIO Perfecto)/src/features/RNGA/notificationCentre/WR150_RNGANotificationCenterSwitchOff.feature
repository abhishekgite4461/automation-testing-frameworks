@team-outboundmessaging @nga3 @rnga @primary @sw-notification-centre-off @stub-credit-card-notification @mbnaSuite
Feature: Retrieve Notifications in the app
  In order to view relevant notifications about my account
  As a customer
  I do not want to see and action the notifications as the switch is off

  #MOB3-10479/1218/10482/704
  Background:
    Given I am an enrolled "valid" user logged into the app

 #------------------------------------------ Variation scenario---------------------------#

  Scenario: TC001 Variation Scenario : Switch Off
    Then The notification placement should not be displayed
