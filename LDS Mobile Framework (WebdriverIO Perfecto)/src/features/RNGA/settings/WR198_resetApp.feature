@team-mpt @nga3 @rnga @stub-enrolled-typical @primary @mbnaSuite
#MOB3-694
Feature: Verify if user is able to de-enrol the device
  In order to de-register my device
  As a RNGA authenticated customer
  I want to reset my app

  Scenario: AC01 TC_001_To verify user is able to cancel reset/de-enrolment of the app
    Given I am an enrolled "current" user logged into the app
    And I navigate to settings page from home page
    And I select the reset app option from the settings page
    When I select reset app button from the reset app page
    And I select cancel button from the reset app confirmation pop up
    Then I should be on the reset app Page
    When I navigate back
    Then I should be on the settings page
    When I select the reset app option from the settings page
    And I select reset app button from the reset app page
    And I select confirm button from the reset app confirmation pop up
    Then I should be on the logout page

  @manual
  Scenario Outline: TC_002 Exception Scenario: User is logged off due to inactivity and displayed message (HC002)
    Given I am an enrolled valid RNGA logged into the app
    And my logoff setting is set to logout after <Time> minutes
    And I am on Reset Mobile Banking screen
    And I am displayed logoff winback option 15 sec before  logout <Time>
    When I am inactive for <InactiveTime>
    Then I must be  logged off
    And I must be displayed the Logoff complete page with message (HC002)
    Examples:
      | Time      | InactiveTime |
      | 1  Minute | 1.01         |
      | 2  Minute | 2.01         |
      | 5  Minute | 5.01         |
      | 10 Minute | 10.01        |

  @manual
  Scenario: TC_003 Exception Scenario: User is logged off due backgrounding the app and displayed message (HC002)
    Given I am an enrolled valid RNGA user logged into the app
    And my logoff setting is set to logout Immediately after exiting the app
    And I am on Reset Mobile Banking screen
    And I background the app
    When I bring the app to foreground
    Then I must be logged-off
    And I must be displayed the Logoff complete page with message (HC002)

  @manual
  Scenario: TC_004 Exception Scenario: User has no internet connection while resetting the app
    Given I am an enrolled valid RNGA user logged into the app
    And I am on Reset Mobile Banking screen
    And I have no internet connection
    When I reset the app
    Then I must be logged-off
    And I must be displayed the Logoff complete page with message (HC002)
    And my app must not be reset

  @manual
  Scenario: TC_005 Exception Scenario: Connection error from server while resetting the app
    Given I am an enrolled valid RNGA user logged into the app
    And I am on Reset Mobile Banking screen
    And I reset the app
    When there is a connection error from server
    Then I must be logged-off
    And I must be displayed the Logoff complete page with message (HC002)
    And my app must not be reset
