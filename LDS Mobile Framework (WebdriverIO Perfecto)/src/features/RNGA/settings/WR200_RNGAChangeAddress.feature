@team-login @nga3 @rnga @stub-coa-map-valid-address @sw-coa-native-on
Feature: RNGA user wants to update Home address
  In order to keep my address up to date
  As a Retail Customer
  I want an option to update my address

  # MOB3-11248, 11675
  Scenario: TC_001_Customer selects the secure call option to change address
    Given I am an enrolled "current and savings and creditcard" user logged into the app
    And I am on the address update page
    And I should see the instructions regarding address change
    When I proceed to call customer care
    Then I should be on the secure call page
    When I go back to previous page
    Then I should be on the change of address postcode search screen
