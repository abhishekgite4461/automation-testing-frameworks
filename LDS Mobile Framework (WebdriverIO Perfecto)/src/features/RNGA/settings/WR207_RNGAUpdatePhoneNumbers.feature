#TODO MOB3-8363 Refactor feature file to have consistency in steps
@team-login @nga3 @rnga
Feature: RNGA user wants to update phone numbers
  In order to keep my phone number up to date
  As a retail RNGA user
  I want option to update my phone number

  #MOB3-6598,6294
  @stub-not-registered-for-paym @secondary
  Scenario: AC01 TC_001_Customer is not registered for Pay a Contact and wants to update a phone number
    Given I am an enrolled "not registered for paym" user logged into the app
    And I am on my personal details page
    When I choose to update a phone number
    Then I should be shown the update phone number page
    And I should be shown my current mobile number, home number, work number and work extension
    And the existing numbers are masked when displayed
    When I navigate back
    Then I should be on the your personal details page
    When I navigate back
    Then I should be on more menu

  #MOB3-6598,6294
  @stub-enrolled-typical @secondary @mbnaSuite
  Scenario: AC02 TC_002_Customer is registered for Pay a Contact and wants to update a phone number
    Given I am an enrolled "valid" user logged into the app
    When I am on the update phone number page
    Then I should be shown the update phone number page
    And I should be shown a warning message that the mobile number is registered for Pay a Contact
    And I should be shown my current mobile number, home number, work number and work extension
    And the existing numbers are masked when displayed

  #MOB3-6508,6595
  @stub-enrolled-typical @mbnaSuite
  Scenario Outline: AC01 TC_003_Customer enters a number in at least one main phone number field
    Given I am an enrolled "valid" user logged into the app
    And I am on the update phone number page
    When I enter "<mobile number>", "<home number>", "<work number>", "<work extn>"
    Then I should be allowed to proceed with the number update
    Examples: Phone numbers entered
      | mobile number | home number | work number  | work extn |
      |               |             |  1234567890  | 12345     |
      | 7123456789    |             |  1234567890  | 12345     |
  #MOB3-6508,6595
  @stub-enrolled-typical @mbnaSuite
  Scenario Outline: AC01 TC_004_Customer leaves all fields blank except work extension
    Given I am an enrolled "valid" user logged into the app
    And I am on the update phone number page
    When I enter "<mobile number>", "<home number>", "<work number>", "<work extn>"
    Then I should not be allowed to proceed with the number update
    Examples: Phone numbers left blank
      | mobile number | home number | work number | work extn |
      |               |             |             |           |
      |               |             |             | 123       |

  #MOB3-6508,6595
  @stub-enrolled-typical @mbnaSuite
  Scenario: AC01 TC_005_Customer tries to exit the screen while editing the mobile number
    Given I am an enrolled "valid" user logged into the app
    And I am on the update phone number page
    And I have started editing the mobile number
    When I try to exit the update phone number page
    Then I am displayed a winback with continue and cancel options

  #MOB3-6508,6595
  @stub-enrolled-typical @mbnaSuite
  Scenario: AC01 TC_006_Customer tries to exit screen while editing home number
    Given I am an enrolled "valid" user logged into the app
    And I am on the update phone number page
    And I have started editing the home number
    When I try to exit the update phone number page
    Then I am displayed a winback with continue and cancel options

  #MOB3-6508,6595
  @stub-enrolled-typical @secondary @mbnaSuite
  Scenario: AC01 TC_007_Customer tries to exit screen while editing work number
    Given I am an enrolled "valid" user logged into the app
    And I am on the update phone number page
    And I have started editing the work number
    When I try to exit the update phone number page
    Then I am displayed a winback with continue and cancel options

  #MOB3-6508,6595
  @stub-enrolled-typical @mbnaSuite
  Scenario: AC01 TC_008_Customer tries to exit screen while editing work extension
    Given I am an enrolled "valid" user logged into the app
    And I am on the update phone number page
    And I have started editing the work extension
    When I try to exit the update phone number page
    Then I am displayed a winback with continue and cancel options

  #MOB3-1287,6596,8049
  @stub-enrolled-typical @mbnaSuite
  Scenario Outline: AC01 TC_010_Customer is displayed Confirm phone number update screen while changing their mobile, home or work number
    Given I am an enrolled "valid" user logged into the app
    When I choose to continue after updating my <phone_number> number
    Then I should be displayed the confirm phone number update screen
    And I should be shown mobile, home, work and work extension numbers
    And I should see non-edited numbers as masked except <phone_number>
    And The edited <phone_number> number is not masked
    And I should be shown options to continue or cancel the changes
    Examples:
      | phone_number |
      | mobile       |
      | home         |
      | work         |
      | work extn    |

  #MOB3-1287,6596
  @stub-enrolled-typical @mbnaSuite
  Scenario: AC02 TC_011_Customer cancels the update on Confirm phone number screen
    Given I am an enrolled "valid" user logged into the app
    And I am on the confirm phone number update screen
    When I choose to cancel the update phone number request
    Then I should be taken back to the update phone number screen

  #MOB3-1287,6596
  @stub-enrolled-typical @mbnaSuite
  Scenario: AC03 TC_012_Customer proceeds with the phone number update
    Given I am an enrolled "valid" user logged into the app
    And I am on the confirm phone number update screen
    When I choose to continue with the update phone number request
    Then I should be asked to confirm the action by entering my IB password

  #MOB3-6511,7396
  @stub-updated-phone-number-referred @secondary  @mbnaSuite
  Scenario: AC01 TC_013_Display "Contact number under review" screen when update phone request has been delayed for review by CCTM Fraud Monitoring after scoring event (MG-2289)
    Given I am an enrolled "Updated phone number referred" user logged into the app
    When I request phone number update from update phone number screen
    Then I should be shown contact number under review screen with error message
    And I should be shown an option to go back to my personal details

  #MOB3-6511,7396 #PSP-618
  @stub-updated-phone-number-declined @secondary  @mbnaSuite
  Scenario: AC02 TC_014_Display "Unsuccessful number update" screen when update phone request has been declined by CCTM Fraud Monitoring after scoring event (MG-2290)
    Given I am an enrolled "Updated phone number declined" user logged into the app
    When I request phone number update from update phone number screen
    Then I should be shown unsuccessful number update screen with error message

  #MOB3-6512,7330
  @stub-enrolled-typical @primary  @mbnaSuite
  Scenario: AC02 TC_015_Phone Number successful update
    Given I am an enrolled "valid" user logged into the app
    And I have submitted request to change mobile, home and work
    When The request has been successfully processed
    Then I should be shown a success screen
    And I should be shown an option to go back to my personal details
