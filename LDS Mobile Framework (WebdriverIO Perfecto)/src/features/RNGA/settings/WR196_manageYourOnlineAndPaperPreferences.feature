@team-outboundmessaging @nga3 @rnga @environmentOnly @android @appiumOnly @mbnaSuite
#MOB3-1301 #MCC-30,33- AC01 MCC-94 & MCC-101 AC01, AC02

# REASONS FOR APPIUM ONLY EXECUTION
# Cookie Policy crops up during the launch of the online paper references home page everytime in PUT 12 , SIT
# enev even after accepting data consent , the above issue persists
# manually rejects the cookie policy on the launch of the page which cannot be done in perfecto
# sometimes BY POST toggle does not work in perfecto and the test script fails

Feature: User wants to change his/her online & paper preferences
  In order to edit my online & paper preferences
  As a RNGA authenticated customer
  I want to view online and paper preferences option from how we contact page

  Background:
    Given I am a current user
    When I enter correct MI
    Then I navigate to settings page from home page
    When I navigate to online and paper preferences from the settings page
    Then I should be on the manage your online and paper preference webview page

  @primary
  Scenario: TC_001_User selects back button on manage paperless preferences in Android (HUBPAGE metadata)
    When I select back button from the online and paper preferences webview page
    Then I should be on the how we contact page

  @primary
  Scenario: TC_002_User initiates forgotten password from webview
    When I select update preference from manage your online and paper preference webview page
    Then I am on confirm your paperless preferences webview page
    When I select on the forget password link on confirm your paperless preferences webview page
    Then I should see forgotten password winback with options on top of the screen with Cancel and OK option
    And on selection of OK option I should be on native reset password screen

  Scenario: TC_003_User selects back/home button on Confirm paperless preferences (INPROGRESS metadata)
    When I select update preference from manage your online and paper preference webview page
    Then I am on confirm your paperless preferences webview page
    When I select back button from confirm your preferences webview page
    Then I should see winback option on top of the screen with Stay and Leave option
    When I select Leave option
    Then I should be on the home page

  Scenario: TC_004_Display winback popup when user chooses home/back option from confirm paperless preferences screen
    When I select update preference from manage your online and paper preference webview page
    Then I am on confirm your paperless preferences webview page
    When I select home button from confirm your preferences webview page
    Then I should see winback option on top of the screen with Stay and Leave option
    And on selection of Stay or Cancel option I should stay on the same screen

  Scenario: TC_005_Display winback popup when user chooses home/back option from confirm paperless preferences screen
    When I select update preference from manage your online and paper preference webview page
    Then I am on confirm your paperless preferences webview page
    When I select back button from confirm your preferences webview page
    Then I should see winback option on top of the screen with Stay and Leave option
    And on selection of Stay or Cancel option I should stay on the same screen

  Scenario: TC_006_Display winback popup when user chooses home/back option from confirm paperless preferences screen
    When I select update preference from manage your online and paper preference webview page
    Then I am on confirm your paperless preferences webview page
    When I select home button from confirm your preferences webview page
    Then I should see winback option on top of the screen with Stay and Leave option
    When I select Leave option
    Then I should be on the home page

  Scenario: TC_007_User initiates forgotten password from webview
    When I select update preference from manage your online and paper preference webview page
    Then I am on confirm your paperless preferences webview page
    When I select on the forget password link on confirm your paperless preferences webview page
    Then I should see forgotten password winback with options on top of the screen with Cancel and OK option
    And on selection of Stay or Cancel option I should stay on the same screen
