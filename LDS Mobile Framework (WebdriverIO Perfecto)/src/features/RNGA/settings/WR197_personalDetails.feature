@team-login @nga3 @rnga @stub-coa-map-valid-address @sw-coa-native-on
Feature: User wants to view personal details
  In order to view or edit my personal details
  As a RNGA authenticated customer
  I want to view my personal details page

  Background:
    Given I am a valid user on the home page
    And I navigate to settings page via more menu

  #MOB3-3322 #MOB3-3325
  @primary
  Scenario: TC_001_View personal details page from settings in more menu
    When I select your personal details option in settings page
    Then I should be on the your personal details page

 #MOB3-5090 #MOB3-5097
  @primary
  Scenario: TC_002_View personal details page
    When I select your personal details option in settings page
    Then I should be on the your personal details page
    And I should be shown the list of personal details
      | personalDetail   |
      | Full Name        |
      | User ID          |
      | Mobile Number    |
      | Home Number      |
      | Work Number      |
      | Address          |
      | Postcode         |
      | Email address    |

  #MOB3-1279 #MOB3-6609
  ## old change of address screen not used anymore so updated with new change of address screen steps
  Scenario: TC_003_View change address update page using postcode finder
    When I select your personal details option in settings page
    And I choose to update address from the personal details
    And I should see an option to search my new address using postcode
