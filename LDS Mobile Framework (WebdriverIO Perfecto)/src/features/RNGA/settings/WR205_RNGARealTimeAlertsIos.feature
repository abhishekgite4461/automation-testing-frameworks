@team-outboundmessaging @nga3 @rnga @stub-enrolled-typical @primary @ios @ff-push-notification-on @ff-push-notifications-opt-in-on
@ff-push-notifications-opt-in-bscreen-on @ff-push-notifications-settings-on @ff-push-notifications-conflict-on @sw-push-optin-a-on @stubOnly
Feature: Verify Real Time Alerts entry point
  In order to edit my app push notification settings
  As a RNGA authenticated customer
  I want to view Real Time Alerts page under Settings menu

  #MCC-29,32- AC01
  Scenario: TC_002_To verify smart alert preferences landing page
    Given I am an enrolled "current" user logged into the app
    And I navigate to settings page from home page
    When I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
