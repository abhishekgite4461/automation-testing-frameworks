#TODO MOB3-8664 Remove iOS tag once stub webview content is working on Android
@team-mpt @nga3 @rnga @stub-enrolled-typical @mbnaSuite
Feature: Display Legal Info

  Background:
    Given I am an enrolled "valid" user logged into the app
    And I navigate to the legal info option from the settings page
    And I am on the legal information page

  # MOB3-7909, 7422
  @primary
  Scenario: AC01 TC_002_Customer chooses to view Legal and privacy information
    Then the page should have links for legal and privacy, cookie use and third party acknowledgements
    When I navigate back
    Then I should be on the settings page
    When I select the legal info option from the settings page
    And I select the legal and privacy link
    Then I should be shown the legal and privacy information page
    And I navigate back
    When I select the cookie use and permissions link
    Then I should be shown the cookie use and permissions page
    And I navigate back
    When I select the third party acknowledgements link
    Then I should be shown the third party acknowledgements page

  # MOB3-7909, 7422
  @ios
  Scenario:AC01 TC_005_Customer selects external link when on a webview
    When I select an external link on the Legal and privacy page
    Then I should be displayed a native winback with options to proceed or stay on the page
    And I navigate back
    When I select an external link on the Cookie use and permissions page
    Then I should be displayed a native winback with options to proceed or stay on the page
