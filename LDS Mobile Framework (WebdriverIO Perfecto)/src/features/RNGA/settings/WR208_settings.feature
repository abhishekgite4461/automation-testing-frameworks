@team-mpt @team-login @nga3 @rnga @stub-enrolled-typical
Feature: Verify Settings menu entry points
  In order to edit my app settings
  As a RNGA authenticated customer
  I want to view my app settings page

  Background:
    Given I am an enrolled "current with spending rewards opted" user logged into the app

  #MOB3-3018,3028,3325,7419,7572 #MCC-31,26- AC01
  @primary
  Scenario: TC_001_To verify static menu on the settings page
    When I navigate to settings page from home page
    Then I should see the below options in the settings page
      | optionsInSettingsPage           |
      | personalDetailsButton           |
      | everydayOfferButton             |
      | securitySettingsButton          |
      | payAContactButton               |
      | resetAppButton                  |
      | legalInfo                       |
      | howWeContactYou                 |
    When I navigate back
    Then I should be on more menu

  #MOB3-3355/1295- AC01
  @primary @mbnaSuite
  Scenario: TC_002_To verify security settings landing page
    When I navigate to security settings page from home page
    Then I should see the below options in the security settings page
      | securitySettingsOptions               |
      | securityDetailsUserIDLabel            |
      | securityDetailsDeviceTypeLabel        |
      | securityDetailsDeviceNameLabel        |
      | securityDetailsAppVersionLabel        |
      | securityDetailsAutoLogOffButton       |
      | securityDetailsForgottenPasswordButton|
    When I navigate back
    Then I should be on the settings page
    When I navigate back
    Then I should be on more menu

  #MOB3-2670/3362- AC01
  @mbnaSuite
  Scenario: TC_003_To verify AutoLog off page
    When I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    Then I should be on the autoLog off settings page
    When I navigate back
    Then I should be on the security settings page

  #MOB3-3344/1291- AC01
  @mbnaSuite
  Scenario: TC_004_To verify the app details in the security settings page
    When I navigate to security settings page from home page
    Then I verify the app details in the security settings page

  Scenario: TC_005_To verify the app lands for everyday offer entry point from settings
    And I navigate to settings page from home page
    When I select modify spending rewards from the settings menu page
    And I navigate back
    Then I should be on the settings page
