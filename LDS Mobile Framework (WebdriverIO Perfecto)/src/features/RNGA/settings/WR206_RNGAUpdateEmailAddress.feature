@team-login @nga3 @rnga @mbnaSuite
Feature: RNGA user wants to update email address
  In order to keep my email address up to date
  As a retail RNGA user
  I want an option to update my email address

  #MOB3-6790,6929
  @stub-enrolled-typical @secondary
  Scenario: AC01 TC_001_ Customer selects to update email address
    Given I am an enrolled "valid" user logged into the app
    And I am on my personal details page
    When I choose to update my email address
    Then I am shown the update email address page
    And I am shown fields for email address, re-enter email address
    And I am shown my current email address in the email address field
    When I navigate back
    Then I should be on the your personal details page
    When I navigate back
    Then I should be on more menu

  #MOB3-6045,6927
  @stub-no-paperless-statements @secondary
  Scenario: AC01 TC_002_Customer leaves both email fields blank when NOT registered for credit card paperless statement
    Given I am an enrolled "no paperless statements" user logged into the app
    And I am on the update email address page
    And I have not provided any input in the email address field
    When I proceed to save the changes
    Then I should be presented with a password confirmation pop-up

  #MOB3-6045,6927
  @stub-enrolled-typical
  Scenario: AC02 TC_003_Customer leaves both email fields blank when registered for credit card paperless statement
    Given I am an enrolled "valid" user logged into the app
    And I am on the update email address page
    And I have not provided any input in the email address field
    When I proceed to save the changes
    Then I should be shown an error message
    And I should be on the update email page after dismissing the error message
   # Error message MG-1660 is displayed

  #MOB3-6045,6927
  @stub-enrolled-typical @secondary
  Scenario: AC03 TC_004_Customer enters email address in correct format
    Given I am an enrolled "valid" user logged into the app
    And I am on the update email address page
    And I enter an email address in the email address field
    And I re-enter the same email address in the re-enter email address field
    When I proceed to save the changes
    Then I should be presented with a password confirmation pop-up

  #MOB3-6045,6927
  @stub-enrolled-typical
  Scenario Outline: AC04 TC_005_Customer enters email address in incorrect format
    Given I am an enrolled "valid" user logged into the app
    And I am on the update email address page
    When I enter "<invalid_email>" in the email address field
    Then I should be shown an error message
    Examples:
      | invalid_email |
      | a.b_atc.com   |
      | ab@cdotcom    |
      | ab@cdot@com   |
      | ab@c.com.     |
      | ab.@.c.com    |
   # Error message HC-026 is displayed

  #MOB3-6045,6927
  @stub-enrolled-typical
  Scenario: AC05 TC_006_Email address entered by Customer in the two email fields don't match
    Given I am an enrolled "valid" user logged into the app
    And I am on the update email address page
    And I enter an email address in the email address field
    When I re-enter a different email address in the re-enter email address field
    Then I should be shown an error message
   # Error message HC-007 is displayed

  #MOB3-6045,6927, #Updated the scenario as per the comments from PSP-603
  @stub-enrolled-valid @secondary
  Scenario: AC06 TC_007_Customer tries to exit screen while editing email address
    Given I am an enrolled "valid" user logged into the app
    And I am on the update email address page
    And I enter an email address in the email address field
    When I try to exit screen using the back button
    Then I should be on the your personal details page

  #MOB3-6858,7750
  @stub-enrolled-typical @primary
  Scenario: AC01 TC_008_The email has been updated successfully
    Given I am an enrolled "valid" user logged into the app
    And I have submitted a request to change my email address
    When The request has been successfully processed
    Then I should be shown a success message with prompt to close the message
    When I dismiss the prompt
    Then I should be shown updated personal details screen
