@team-login @nga3 @rnga
Feature: User is prompted to confirm password for step-up authentication
  In order to allow RNGA Customer to proceed with restricted transaction
  As a Security Manager
  I want to the Customer to confirm their password

  #MOB3-6588, 6591, 6791, 7413, 7840, 1669, 8558, 8563, 10112, #MQE-495
  @stub-enrolled-valid
  Scenario Outline: TC_001_Customer is displayed password confirm screen while updating email/phone
    Given I am an enrolled "valid" user logged into the app
    When I choose to proceed with <action>
    Then I should be displayed the password confirmation screen
    And I should be displayed option to enter my IB password
    And I should be given options to proceed or cancel
    And I should be displayed option for forgotten password
    Examples:
      | action                  |
      | phone number update     |
      | email address update    |
      | Add UK Beneficiary      |
      | Add Company Beneficiary |
      | Add New PayM            |
      | Reactivate ISA          |

  #MOB3-6510,6594, 7413
  @stub-enrolled-valid @mbnaSuite
  Scenario: TC_002_SC_001_Customer enters correct password when updating phone number
    Given I am an enrolled "valid" user logged into the app
    And I choose to proceed with phone number update
    And I am displayed password confirmation screen
    When I choose to proceed having entered the right password
    Then I should be displayed successful update page

  #MOB3-6850,7738, 7413
  @stub-enrolled-typical @mbnaSuite
  Scenario: TC_003_SC_002_Customer enters correct password when updating email address
    Given I am an enrolled "valid" user logged into the app
    And I choose to proceed with email address update
    And I am displayed password confirmation screen
    When I choose to proceed having entered the right password
    Then I should be displayed successful update dialog

  #MOB3-6510, 6594, 6850, 7738, 7413, 7840, 1669, 8558, 8563, 10112
  @stub-incorrect-confirmation-password @primary
  Scenario Outline: TC_003_Customer enters incorrect password
    Given I am an enrolled "incorrect confirmation password" user logged into the app
    And I choose to proceed with <action>
    And I am displayed password confirmation screen
    When I have entered wrong password
    Then I should be displayed an error message for wrong password
    And I should be given an option to enter the password again
    Examples:
      | action                  |
      | phone number update     |
      | email address update    |
      | Add UK Beneficiary      |
      | Add Company Beneficiary |
      | Add New PayM            |

  #MOB3-6510, 6594, 6850, 7738, 7413, 7840, 1669, 8558, 8563, 10112
  @manual
  Scenario Outline: TC_004_Customer enters incorrect password consecutively 6 times
    Given I am an enrolled "valid" user logged into the app
    And I choose to proceed with <action>
    And I am displayed password confirmation screen
    And I have entered wrong password 5 times consecutively
    When I choose to proceed having entered the wrong password the 6th time
    Then I should be displayed revoked error page
    Examples:
      | action                  |
      | phone number update     |
      | email address update    |
      | Add UK Beneficiary      |
      | Add Company Beneficiary |
      | Add New PayM            |

  #MOB3-6588, 6591, 6791, 7413, 7413, 7840, 1669, 8558, 8563, 10112, #MQE-495
  @stub-enrolled-typical
  Scenario Outline: TC_005_Customer tries to enter more than 20 chars in the password field
    Given I am an enrolled "valid" user logged into the app
    And I choose to proceed with <action>
    And I am displayed password confirmation screen
    When I enter the password "withmorethantwentycharactersinpassword" into a dialog
    Then I should be restricted from entering more than 20 characters in password dialog text box
    Examples:
      | action                  |
      | phone number update     |
      | email address update    |
      | Add UK Beneficiary      |
      | Add Company Beneficiary |
      | Add New PayM            |

  #MOB3-6588, 6591, 6791, 7413, 7413, 7840, 1669, 8558, 8563, 10112
  @stub-enrolled-valid @secondary
  Scenario Outline: TC_006_Customer tries to enter less than 6 chars in the password field
    Given I am an enrolled "valid" user logged into the app
    And I choose to proceed with <action>
    And I am displayed password confirmation screen
    When I enter the password "12345" into a dialog
    Then the option to continue on enter password dialogue should not be enabled
    Examples:
      | action                  |
      | phone number update     |
      | email address update    |
      | Add UK Beneficiary      |
      | Add Company Beneficiary |
      | Add New PayM            |

  #MOB3-6510, 6594, 6850, 7738, 7413, 7840, 1669, 8558, 8563, 10112, #PSP-603
  @stub-enrolled-valid @secondary
  Scenario Outline: TC_007_Customer chooses Cancel option on password pop-up
    Given I am an enrolled "valid" user logged into the app
    And I am displayed password confirmation screen while <action>
    When I choose to cancel the process on enter password dialog
    Then I should be taken back to the <screen_name> screen
    Examples: Cancel enter password from different screens
      | action                  | screen_name                         |
      | phone number update     | Update phone number                 |
      | email address update    | Update email address                |
      | Add UK Beneficiary      | Review UK beneficiary details       |
      | Add Company Beneficiary | Review company beneficiary details  |
      | Add New PayM            | Add new payM details                |
      | Reactivate ISA          | Reactivate ISA confirmation details |

  #MOB3-6592, 6593, 6844, 7634, 7413, 7840, 1669, 8558, 8563, 10112
  @stub-enrolled-valid @secondary
  Scenario Outline: TC_008_Customer chooses to proceed after being shown the Warning message on Forgotten your password link
    Given I am an enrolled "valid" user logged into the app
    And I am displayed password confirmation screen while <action>
    And I choose forgotten your password option
    And I am displayed a warning message
    When I choose to continue on the warning message
    Then I am displayed reset password screen in the app
    Examples:
      | action                  |
      | phone number update     |
      | email address update    |
      | Add UK Beneficiary      |
      | Add Company Beneficiary |
      | Add New PayM            |

  #MOB3-6592, 6593, 6844, 7634, 7413, 7840, 1669, 8558, 8563, 10112
  @stub-enrolled-valid @primary
  Scenario Outline: TC_009_Customer chooses to cancel after being shown the Warning message on Forgotten your password link
    Given I am an enrolled "valid" user logged into the app
    And I am displayed password confirmation screen while <action>
    And I choose forgotten your password option
    And I am displayed a warning message
    When I choose to cancel on the warning message
    Then I am displayed password confirmation screen
    Examples:
      | action                  |
      | phone number update     |
      | email address update    |
      | Add UK Beneficiary      |
      | Add Company Beneficiary |
      | Add New PayM            |
      | Reactivate ISA          |
