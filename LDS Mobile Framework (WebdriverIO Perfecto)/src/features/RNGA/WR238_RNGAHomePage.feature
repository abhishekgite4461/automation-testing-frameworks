@team-mpt @rnga @nga3 @sw-card_management-on
Feature: Validate the options and the account tiles in the home page
#All the options in the action menu is purely data dependant, hence may need to refactor once run in real environment.
#Also this feature file covers only validating the generic option in the action menu, respective feature team will validate the entry points.

  @android @stub-enrolled-typical
  Scenario: TC_001_Verify scroll back to top behaviour when home tab is selected
    Given I am a valid user on the home page
    And I am navigated to first account on home page
    And I scroll down
    And I do not see first account on home page
    When I select home tab from bottom navigation bar
    Then I am navigated to first account on home page

  # MOB3-1187, 2169, 674
  @stub-enrolled-typical @primary
  Scenario: TC_002_Verify the current account details on the account's tile displayed on homepage
    Given I am an enrolled "current" user logged into the app
    And I navigate and validate the options in the current account in below table in the home page
      | accountFields      |
      | accountNumber      |
      | sortCode           |
      | accountBalance     |
      | accountInfoBalance |
    When I select the action menu of current account
    Then I should see the below options in the action menu of the current account
      | optionsInActionMenu         |
      | viewTransactionsOption      |
      | changeAccountTypeOption     |
      | transferAndPaymentOption    |
      | sendMoneyOutsideTheUKOption |
      | standingOrderOption         |
      | directDebitOption           |
      | applyForOverdraftOption     |
      | orderPaperStatementOption   |
      | cardManagementOption        |
    And I close the action menu in the home page

  @primary @environmentOnly
  Scenario: TC_003_User Opts-in and Confirms spending rewards is activated
    Given I am an enrolled "current" user logged into the app
    When I select register for everday offers tile from home page
    And I opt in for spending rewards
    Then I should be opted-in for spending rewards

  @stub-enrolled-typical
  Scenario: TC_004_User does not confirm to opt in for spending rewards
    Given I am an enrolled "current" user logged into the app
    When I select register for everday offers tile from home page
    And I do not confirm my selection on spending rewards page
    Then I should not be opted-in for spending rewards

  @manual
  Scenario: TC_005_User has opted out for spending rewards
    Given I am an enrolled "current" user logged into the app
    When I opt out from everyday offer
    And I am on the home page
    Then I should see register for everday offers tile in home page

  @stub-spending-rewards-technical-error @stubOnly
  Scenario: TC_006_User is shown with error banner in case of opting in for everyday offer
    Given I am an enrolled "current" user logged into the app
    When I select register for everday offers tile from home page
    And I navigate back
    Then I should be on the home page
    When I select register for everday offers tile from home page
    And I confirm my selection on the spending rewards page
    Then I should see everyday offer technical error message in home page

  # The below scenario is applicable only for ios
  @stub-enrolled-typical @ios
  Scenario: TC_007_User should be able to navigate back to the home page
    Given I am an enrolled "current" user logged into the app
    And I select current account tile from home page
    And I select the first transaction in the account statement page
    When I navigate back
    Then I should be on the current month statements tab
    When I navigate back
    Then I should be on the home page

  # The below scenario is applicable only for android
  @stub-enrolled-valid @android
  Scenario: TC_008_User should be able to navigate back to the home page
    Given I am an enrolled "current" user logged into the app
    And I select current account tile from home page
    And I should be on the statements page
    When I navigate back
    Then I should be on the home page

  @stub-enrolled-valid @failed
  Scenario: TC_009_Verify the landing page for the cheque deposit journey from home and more page
    Given I am an enrolled "current" user logged into the app
    When I navigate to current account on home page
    And I select the action menu of current account
    And I select deposit cheque option in action menu
    And I navigate back
    Then I should be on the home page
    When I navigate to cheque deposit page via more menu
    And I navigate back
    Then I should be on more menu

  # MOB3-1187, 2169
  # Note Data is not available for applied overdraft account
  @stub-valid @primary @environmentOnly @failed
  Scenario: TC010_Verify the current account details on the account's tile displayed on homepage with already applied overdraft
    Given I am an enrolled "applied overdraft current account" user logged into the app
    And I navigate and validate the options in the current account in below table in the home page
      | accountFields      |
      | accountNumber      |
      | sortCode           |
      | accountBalance     |
      | accountInfoBalance |
    When I select the action menu of current account
    Then I should see the below options in the action menu of the current account
      | optionsInActionMenu        |
      | viewTransactionsOption     |
      | transferAndPaymentOption   |
      | sendMoneyOutsideTheUKOption|
      | standingOrderOption        |
      | directDebitOption          |
      | manageOverdraftOption      |
      | orderPaperStatementOption  |
      | cardManagementOption       |
      | viewPendingPaymentOption   |
    And I close the action menu in the home page

  # MOB3-1361, 2170, 5864
  @stub-enrolled-typical @primary
  Scenario: TC011_Verify the saving account details on the account's tile displayed on homepage
    Given I am an enrolled "current and savings" user logged into the app
    And I navigate and validate the options in the saving account in below table in the home page
      | accountFields            |
      | accountNumber            |
      | sortCode                 |
      | accountBalance           |
    When I select the action menu of saving account
    Then I should see the below options in the action menu of the saving account
      | optionsInActionMenu           |
      | viewTransactionsOption        |
      | transferAndPaymentOption      |
      | orderPaperStatementOption     |
      | cardManagementOption          |
      | renewYourSavingsAccountOption |
      | viewInterestRateOption        |
    And I close the action menu in the home page

  # MOB3-2692, 2786, 737
  @stub-enrolled-typical @primary
  Scenario: TC012_Verify the credit card account details on the account's tile displayed on homepage
    Given I am an enrolled "credit card" user logged into the app
    And I navigate and validate the options in the creditCard account in below table in the home page
      | accountFields            |
      | creditCardAccountNumber  |
      | creditCardAccountBalance |
      | availableCredit          |
      | overdueAmount            |
    When I select the action menu of creditCard account
    Then I should see the below options in the action menu of the creditCard account
      | optionsInActionMenu        |
      | viewTransactionsOption     |
      | payCreditCardOption        |
      | balanceMoneyTransferOption |
      | manageCreditLimitOption    |
      | cardManagementOption       |
      | pdfStatementsOption        |
    And I close the action menu in the home page

  # MOB3-2692, 2786, 6222, 731
  @stub-enrolled-valid
  Scenario: TC013_Verify the credit card account details on the account's tile displayed on homepage for credit amount user
    Given I am an enrolled "additional credit amount" user logged into the app
    And I navigate and validate the options in the overDueCreditCard account in below table in the home page
      | accountFields                    |
      | creditCardAccountNumber          |
      | creditCardAccountBalance         |
      | availableCredit                  |
    When I select the action menu of overDueCreditCard account
    Then I should see the below options in the action menu of the creditCard account
      | optionsInActionMenu        |
      | viewTransactionsOption     |
      | balanceMoneyTransferOption |
      | manageCreditLimitOption    |
      | cardManagementOption       |
      | pdfStatementsOption        |
    And I close the action menu in the home page

  # MOB3-1187, 2169
  @stub-valid @manual @primary
  Scenario Outline: TC014_Verify the nick name is displayed for the below account in the account tile of the homepage
    Given I am an enrolled "current savings isa fixed Term Deposit" user logged into the app
    When I navigate to the current account in the home page
    Then I should see the nick name on the "<accountTile>" account tile in the home page
    Examples:
      | accountTile             |
      | currentAccount          |
      | savingAccount           |
      | isaAccount              |
      | fixedTermDepositAccount |

  # MOB3-1186, 2726
  @stub-enrolled-valid @primary @failed
  Scenario: TC015_Verify the ISA account details on the account's tile displayed on homepage
    Given I am an enrolled "fallow isa" user logged into the app
    And I navigate and validate the options in the isa account in below table in the home page
      | accountFields      |
      | accountNumber      |
      | sortCode           |
      | accountBalance     |
      | accountInfoBalance |
    When I select the action menu of isa account
    Then I should see the below options in the action menu of the isa account
      | optionsInActionMenu           |
      | viewTransactionsOption        |
      | transferAndPaymentOption      |
      | reactivateIsaOption           |
      | topupIsaOption                |
      | orderPaperStatementOption     |
      | cardManagementOption          |
      | renewYourSavingsAccountOption |

  # MOB3-1186, 2726
  @stub-enrolled-valid @primary @failed
  Scenario: TC016_Verify the ISA account details on the account's tile displayed on homepage
    Given I am an enrolled "isa with zero remaining allowance" user logged into the app
    And I navigate and validate the options in the isa account in below table in the home page
      | accountFields      |
      | accountNumber      |
      | sortCode           |
      | accountBalance     |
      | accountInfoBalance |
    When I select the action menu of isa account
    Then I should see the below options in the action menu of the isa account
      | optionsInActionMenu           |
      | viewTransactionsOption        |
      | transferAndPaymentOption      |
      | orderPaperStatementOption     |
      | cardManagementOption          |
      | renewYourSavingsAccountOption |

  # MOB3-2172, DPL-1010
  @stub-valid @secondary @failed
  Scenario: TC017_Verify the fixed term deposit account details on the account's tile displayed on homepage
    Given I am an enrolled "fixed Term Deposit" user logged into the app
    And I navigate and validate the options in the fixedTermDeposit account in below table in the home page
      | accountFields  |
      | accountNumber  |
      | sortCode       |
      | accountBalance |
    When I select the action menu of fixedTermDeposit account
    Then I should see the below options in the action menu of the fixedTermDeposit account
      | optionsInActionMenu       |
      | viewTransactionsOption    |
      | orderPaperStatementOption |
      | viewInterestRateOption    |

  # MOB3-1373, 2678, DPL-2171
  @stub-valid-special-accounts @primary
  Scenario: TC018_Verify the mortgage account details on the account's tile displayed on homepage
    Given I am an enrolled "mortgage" user logged into the app
    And I navigate and validate the options in the mortgage account in below table in the home page
      | accountFields          |
      | mortgageAccountNumber  |
      | mortgageAccountBalance |
    When I select the action menu of mortgage account
    Then I should see the below options in the action menu of the mortgage account
      | optionsInActionMenu    |
      | viewTransactionsOption |
    And I close the action menu in the home page

  # MOB3-2787, 2790, 708, 732, 5547, 6223, 756, DPL-1007
  @stub-valid-special-accounts @primary
  Scenario: TC019_Verify the non CBS loan account details on the account's tile displayed on homepage
    Given I am an enrolled "non cbs personal loan" user logged into the app
    And I navigate and validate the options in the nonCbsLoan account in below table in the home page
      | accountFields      |
      | loanAccountNumber  |
      | loanCurrentBalance |
    When I select the action menu of nonCbsLoan account
    Then I should see the below options in the action menu of the nonCbsLoan account
      | optionsInActionMenu         |
      | viewTransactionsOption      |
      | loanRepaymentHolidayOption  |
      | loanClosureOption           |
      | loanBorrowMoreOption        |
      | loanAnnualStatementsOption  |
      | loanAdditionalPaymentOption |
    And I close the action menu in the home page

  # MOB3-1370, 2685
  # Test data is not available in stub as well as in environment(HFX and BOS). Hence this scenario is pending long back.
  @stub-enrolled-typical @manual @primary
  Scenario: TC020_Verify the CBS loan account details on the account's tile displayed on homepage
    Given I am an enrolled "cbs personal loan" user logged into the app
    When I navigate and validate the options in the cbsLoan account in below table in the home page
      | accountFields        |
      | cbsLoanAccountNumber |
      | cbsLoanSortCode      |
      | cbsCurrentBalance    |
    Then I should not see action menu of cbsLoan account

  @stub-enrolled-typical @pending
  Scenario: TC_021_Verify the landing page for a single account user
    Given I am an enrolled "single eligible account" user logged into the app
    When I swipe the account tile in the home page
    Then I should be on the current month statements tab

  @primary @manual @stub-enrolled-typical
  Scenario Outline: TC022_Verify the NIL/NA/negatiev current balance and available balance on account tile displayed on homepage
    Given I am a nil na negative balance user
    When I navigate to the saving account in the home page
    Then I should see the current balance as <type> on the saving account tile in the home page
    And I should see the available balance as <type> on the saving account tile in the home page
    Examples:
      | type             |
      | NIL              |
      | N/A              |
      | negative balance |

  # TODO MPL-1017 This was test is missing a @stub- tag and hence cannot run in it's current form.
  # MOB3-737 Halifax Flexicard also known as Installment credit card
  @stub-enrolled-valid @hfx @failed
  Scenario: AC01 TC024_Verify Halifax Flexicard user is shown setup installment credit card option
    Given I am an enrolled "Halifax Flexi credit card with installment not setup" user logged into the app
    And I navigate and validate the options in the instalmentCreditCard account in below table in the home page
      | accountFields                    |
      | creditCardAccountNumber          |
      | creditCardAccountBalance         |
      | availableCredit                  |
      | overdueAmountForAdditionalCredit |
    When I select the action menu of creditCard account
    Then I should see the below options in the action menu of the instalmentCreditCard account
      | optionsInActionMenu        |
      | viewTransactionsOption     |
      | balanceMoneyTransferOption |
      | manageCreditLimitOption    |
      | cardManagementOption       |
      | pdfStatementsOption        |
      | setupInstallmentCreditCard |

  # MOB3-737 Halifax Flexicard also known as Installment credit card
  @stub-enrolled-valid @hfx @failed
  Scenario: AC02 TC025_Verify Halifax Flexicard user is shown maintain installment credit card option
    Given I am an enrolled "Halifax Flexi credit card with installment already setup" user logged into the app
    And I navigate and validate the options in the instalmentCreditCard account in below table in the home page
      | accountFields                    |
      | creditCardAccountNumber          |
      | creditCardAccountBalance         |
      | availableCredit                  |
      | overdueAmountForAdditionalCredit |
    When I select the action menu of creditCard account
    Then I should see the below options in the action menu of the instalmentCreditCard account
      | optionsInActionMenu           |
      | viewTransactionsOption        |
      | balanceMoneyTransferOption    |
      | manageCreditLimitOption       |
      | cardManagementOption          |
      | pdfStatementsOption           |
      | maintainInstallmentCreditCard |

    #TODO remove android tag once the defect 12560 is fixed.
  @stubOnly @stub-host-system-down
  Scenario: TC026_Display balance as NA and error message when host system to retrieve the balance info is down/unavailable
    Given I am an enrolled "host system down" user logged into the app
    When I dismiss the host system down error message in the home page
    Then I should see the balance to be N/A in the home page

  # MOB3-21422
  @stub-enrolled-youth
  Scenario: TC_027_Youth customer should NOT see option for spending rewards
    Given I am an enrolled "youth" user logged into the app
    Then I should not see spending rewards tile in the home page

  @stubOnly @stub-homepage-pending-payment
  Scenario: TC028_Display balance as NA and error message when host system to retrieve the balance info is down/unavailable
    Given I login to app as a "home page pending payment" user
    When I dismiss the pending payment error message in the home page
    Then I should not see pending payment error message in the home page

  #MOB3-7702
  @stubOnly @stub-enrolled-valid
  Scenario Outline: TC_029_Display account alerts on home page
    Given I am an enrolled "account alert" user logged into the app
    When I navigate to <type> account on home page
    Then I should see alert warning on the <type> account tile in home page
    Examples:
      | type             |
      | accountWithAlert |

  # MOB3-1193, 2781, DPL-1427
  @stub-invalid-accounts
  Scenario: TC_031_No valid accounts customer should be shown appropriate error message referencing [MSG-528] on the account tile
    Given I am a invalid accounts user on the home page
    Then I am displayed no valid accounts error message on the account tile

  # MOB3-1193, 2781, DPL-945. DPL-947
  @stub-some-dormant-accounts
  Scenario: TC_032_Alert message should be displayed for dormant account
    Given I am a dormant accounts user on the home page
    When I select the alert warning on the current account tile in home page
    Then I should see valid alert message for dormant account

  @stub-enrolled-valid @primary @manual
  Scenario: TC_035_User has non dormant accounts
    Given I am an enrolled "all accounts" user logged into the app
    When I am on the home page
    Then I should see all the eleigible accounts in the home page

  @environmentOnly
  Scenario: TC_036_User views the offer for spending rewards
    Given I am an enrolled "everyday offer opted in" user logged into the app
    When I select the view all offers button from home page
    Then I should be on the everyday view offers page

  @manual
  Scenario: TC_037_User is not displayed with counter for everyday offer if api has failed
    Given I am an enrolled "everyday offer opted in" user logged into the app
    When I view the everyday offer tile
    Then I display tile without the counter of the offers

  # PAYMOB-397
  @primary @manual
  Scenario: TC-08 Main Success Scenario: User navigates to save the changes functionality (webview page) from account tile menu options
    Given I am a current user on the home page
    When I select save the changes option from the current account action menu
    Then I should be on the save the changes webview page
    And I should have the option to navigate to home screen

  @team-aov @stub-enrolled-valid @secondary @android
  #This feature has been implemented on LDS, HFX and BOS brand only.
  #This scenario is for Android only as logs from Perfecto devices can only be obtained for Android
  Scenario: TC_18 Verify analytics are logged when Covid-19 tile is clicked on home page
    Given I am a current user on the home page
    When I am shown the Covid19 tile on home page
    And I select Find out how button on Covid19 tile
    Then analytics are logged for clicking the Covid19 tile

  #This feature has been implemented HFX brand only.
  #This scenario is for Android only as logs from Perfecto devices can only be obtained for Android
  #TODO: Remove the stubonly tag when the backend is updated to 20.09 as this feature will be implemented behind a IBC switch
  @team-aov @stub-enrolled-valid @ff-enable-reward-hub-entry-point-for-halifax-on @secondary @android @hfx @stubOnly
  Scenario: TC_18 Verify analytics are logged when Halifax Reward tile is clicked on home page
    Given I am a current user on the home page
    When I am shown the reward hub entry point tile on home page
    And I select Find out how button on reward hub entry point tile
    Then analytics are logged for clicking the Halifax Reward Hub tile
