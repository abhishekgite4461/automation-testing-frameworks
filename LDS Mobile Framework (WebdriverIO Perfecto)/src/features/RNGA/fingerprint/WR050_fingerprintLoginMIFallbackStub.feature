@team-login @nga3 @rnga @stub-biometric-login @pending @mbnaSuite
Feature: MI fallback on tapping cancel button in Fingerprint login  using Biometric Stub

  Background:
    Given I am an enrolled "fp authentication" user on the MI page

  #MOB3-2996
  Scenario: TC_001 Cancel Fingerprint Login prompt and landing on MI page
    When I cancel the fingerprint login
    Then I should be on enter MI page

  Scenario: TC_003 Verify the correct MI takes user to the home page from the MI fallback
    When I select on the cancel button from the fingerprint login prompt
    And I enter correct MI
    Then I should be on the home page
