@team-login @nga3 @rnga @stub-enrolled-typical @pending @mbnaSuite
Feature: No FP/Global Menu during Opted Out journey using Success Scenarios- Enrolled Stub

  Background:
    Given I have a fingerprint enabled phone
    And I have no fingerprints registered
    And I am an enrolled "valid" user logged into the app

  #MOB3-1210
  Scenario: TC_001 Validate the Opt out status in security settings
    When I navigate to the security settings screen
    Then the fingerprint status is opted out

  #MOB3-1211/2990
  Scenario: TC_002 Show No Fingerprints registered from Settings
    Given I navigate to the security settings screen
    When I opt in for fingerprint from settings
    Then I am notified that no fingerprints are registered in the phone

  #MOB3-1211/2990
  Scenario: TC_003 Show Security Settings screen after dismissing No Fingerprints message
    Given I navigate to the security settings screen
    And I opt in for fingerprint from settings
    When I dismiss the no fingerprints prompt
    Then the fingerprint status is opted out
    And I should be on the security settings page
