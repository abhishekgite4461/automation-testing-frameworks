@team-login @nga3 @rnga @stub-biometric-valid @fingerprintScannerAndFingerprint @mbnaSuite
Feature: OptIn/OptOut of Fingerprint using Biometric Stub

  Background:
    Given I am an enrolled "fingerprint" user logged into the app

  #MOB3-2954,2957
  Scenario: TC_001 User can select the opt in option from fingerprint interstitial
    When I opt in for fingerprint from the interstitial
    Then I will be presented with the fraud message

  #MOB3-1209,2402
  Scenario: TC_002 User accept the fraud message and opted in to fingerprint authentication
    Given I am displayed with opt in or opt out of fingerprint authentication
    And I opt in for fingerprint from the interstitial
    When I accept the fraud message
    Then I should be on the home page

  #MOB3-1209,2402
  Scenario: TC_003 User declines the fraud message and opted out from fingerprint authentication
    Given I am displayed with opt in or opt out of fingerprint authentication
    And I opt in for fingerprint from the interstitial
    When I decline the fraud message
    Then I should be on the home page

  #MOB3-1209,2402
  Scenario: TC_004 User can opt out from interstitial
    When I opt out for fingerprint from the interstitial
    Then I should be on the home page

  #MOB3-1206
  @primary
  Scenario: TC_005 Opt in from interstitial and Opt in status insecurity settings
    Given I have opted in to fingerprint authentication
    When I navigate to the security settings screen
    Then the fingerprint status is opted in

  #MOB3-1206
  Scenario: TC_006 Opt in from interstitial and Opt out status in security settings
    Given I have opted in to fingerprint authentication
    When I navigate to the security settings screen
    And I opt out for fingerprint from settings
    Then the fingerprint status is opted out

  #MOB3-1223
  Scenario: TC_007 Opt out from security settings
    Given I opt out for fingerprint from the interstitial
    When I navigate to the security settings screen
    Then the fingerprint status is opted out

  #MOB3-1223
  @primary
  Scenario: TC_008 Opt out from interstitial and Opt in status in security settings
    Given I opt out for fingerprint from the interstitial
    When I navigate to the security settings screen
    And I opt in for fingerprint from settings
    And I accept the fraud message
    Then the fingerprint status is opted in

  #MOB3-1223
  @primary
  Scenario: TC_009 Opt out status in security settings after declining fraud message
    Given I opt out for fingerprint from the interstitial
    When I navigate to the security settings screen
    And I opt in for fingerprint from settings
    And I decline the fraud message
    Then the fingerprint status is opted out
