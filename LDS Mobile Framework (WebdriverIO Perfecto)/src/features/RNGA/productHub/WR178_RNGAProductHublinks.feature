@team-mco @nga3 @rnga @stub-enrolled-typical
Feature: Verify Product hub category links

  Background:
    Given I am an enrolled "valid" user logged into the app
    When I select apply tab from bottom navigation bar
    And I close the opened Featured option from product hub menu

  #MOB3-672, 6234
  @primary
  Scenario: AC01 TC_001_Verify the Savings Product link
    When I select the Compare savings accounts of the Savings in the product hub page
    Then I should be on the Compare savings accounts page

  #MOB3-5996, 6240
  @secondary
  Scenario: AC01 TC_002_Verify the Loans Product link
    When I select the loan calculator of the Loans option in the product hub page
    Then I should be on the loan calculator page

  #MOB3-6006, 6241
  @secondary @mbnaSuite
  Scenario: AC01 TC_003_Verify the Credits Cards Product link
    When I select the Compare credit card of the Credit cards in the product hub page
    Then I should be on the Compare credit card page

  #MOB3-5940, 6160
  @secondary
  Scenario: AC01 TC_004_Verify the Current Product link
    And I select the overdraft of the current accounts in the product hub page
    Then I should be on the overdraft current accounts page

  #MOB3-6008, 6166
  @secondary
  Scenario: AC01 TC_005_Verify the Insurance Product link
    When I select the Home Insurance option in the product hub page
    Then I should be on the Home Insurance page

  #MOB3-6047, 6170
  @manual
  Scenario: AC01 TC_006_Verify the Mortgage Product link
    When I click on the book branch appointment in the product hub page
    Then I should be on the book branch appointment page
