@team-mco @nga3 @rnga @secondary
Feature: Display list of products and categories

  # MORE-21 #PI-684 Did changes as per the PI-684 in which we say we are removing Discover and apply title
  @stub-enrolled-valid
  Scenario: Verify scroll back to top behaviour when Apply tab is selected
    Given I am an enrolled "valid" user logged into the app
    When I select apply tab from bottom navigation bar
    Then I should not see Discover and apply title on product hub page
    And I see featured product on product hub page
    When I expand Savings on product hub page
    And I scroll down
    Then I do not see featured product on product hub page
    When I select apply tab from bottom navigation bar
    Then I see featured product on product hub page

  @stub-enrolled-typical
  Scenario: AC01 TC_001_Customer lands on Product Hub page
    Given I am an enrolled "valid" user logged into the app
    When I select apply tab from bottom navigation bar
    And I close the opened Featured option from product hub menu
    Then I should see the product hub home page with product categories
      | productCategoryLabels |
      | featured              |
      | currentAccounts       |
      | loans                 |
      | creditCards           |
      | savings               |
      | insurance             |
      | mortgages             |

  @stub-enrolled-typical
  Scenario: AC01 TC_002_Customer clicks featured option
    Given I am an enrolled "valid" user logged into the app
    When I select apply tab from bottom navigation bar
    Then "Featured" category should display the below products
      | productLabels  |
      | loanCalculator |
      | homeInsurance  |
      | carInsurance   |

  @stub-enrolled-typical
  Scenario: AC01 TC_003_Customer clicks Current Account option
    Given I am an enrolled "valid" user logged into the app
    When I select apply tab from bottom navigation bar
    And I close the opened Featured option from product hub menu
    Then "Current Account" category should display the below products
      | productLabels         |
      | compareAccounts       |
      | overdrafts            |
      | travelMoney           |
      | internationalPayments |

  @stub-enrolled-typical
  Scenario: AC01 TC_004_Customer clicks Loan option
    Given I am an enrolled "valid" user logged into the app
    When I select apply tab from bottom navigation bar
    And I close the opened Featured option from product hub menu
    Then "Loan" category should display the below products
      | productLabels |
      | calculator    |
      | borrowMore    |
      | personalLoan  |
      | carFinance    |

  @stub-enrolled-typical
  Scenario: AC01 TC_005_Customer clicks Credit cards option
    Given I am an enrolled "valid" user logged into the app
    When I select apply tab from bottom navigation bar
    And I close the opened Featured option from product hub menu
    Then "Credit Card" category should display the below products
      | productLabels      |
      | compareCreditCards |
      | balanceTransfer    |

  @stub-enrolled-typical
  Scenario: AC01 TC_006_Customer clicks Savings option
    Given I am an enrolled "valid" user logged into the app
    When I select apply tab from bottom navigation bar
    And I close the opened Featured option from product hub menu
    Then "Saving" category should display the below products
      | productLabels          |
      | compareSavingsAccounts |
      | instantAccess          |
      | topUpISA               |
      | cashIsa                |
      | fixedTerm              |

  @stub-enrolled-typical
  Scenario: AC01 TC_007_Customer clicks Insurance option
    Given I am an enrolled "valid" user logged into the app
    When I select apply tab from bottom navigation bar
    And I close the opened Featured option from product hub menu
    Then "Insurance" category should display the below products
      | productLabels   |
      | vanInsurance    |
      | travelInsurance |

  @stub-enrolled-typical
  Scenario: AC01 TC_008_Customer clicks Mortgages option
    Given I am an enrolled "valid" user logged into the app
    When I select apply tab from bottom navigation bar
    And I close the opened Featured option from product hub menu
    Then "Mortgage" category should display the below products
      | productLabels         |
      | agreementInPrinciple  |
      | bookBranchAppointment |
      | switchToNewDeal       |
      | otherMortgageOptions  |

  @stub-enrolled-typical
  Scenario: TC_009_User should be able to navigate back to the apply page
    Given I am an enrolled "valid" user logged into the app
    When I select apply tab from bottom navigation bar
    And I select the loan calculator of the Loans option in the product hub page
    And I navigate back
    Then I should be on the product hub page

  @stub-enrolled-youth
  Scenario:TC_010_Youth user shouldn't be able to apply for new products
    Given I am an enrolled "Youth" user logged into the app
    When I select apply tab from bottom navigation bar
    Then I should see can't apply error page

  @stub-enrolled-typical @mbna
  Scenario: AC01 TC_001_Customer lands on Product Hub page
    Given I am an enrolled "valid" user logged into the app
    When I select apply tab from bottom navigation bar
    And I close the opened Featured option from product hub menu
    Then I should see the product hub home page with product categories
      | productCategoryLabels |
      | featured              |
      | creditCards           |
