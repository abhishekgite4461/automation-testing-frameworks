@team-mco @nga3 @rnga @stub-enrolled-valid @secondary
Feature: Verify Product Sales & Servicing journey links from Account Tiles

  #MOB3-7911, 708
  Scenario: AC01 TC_001_Verify Customer is able to view make additional payment webview screen
    Given I am an enrolled "non cbs personal loan" user logged into the app
    When I navigate to nonCbsLoan account on home page
    And I select the action menu of nonCbsLoan account
    And I am able to view loanAdditionalPaymentOption in contextual menu
    And I select loanAdditionalPaymentOption from contextual menu
    Then I should be displayed the Additional payment webview

  #MOB3-7916, 731
  Scenario: AC01 TC_002_Verify Customer is able to view request balance transfer webview screen
    Given I am an enrolled "credit card" user logged into the app
    When I navigate to creditCard account on home page
    And I select the action menu of creditCard account
    And I am able to view balanceMoneyTransferOption in contextual menu
    And I select balanceMoneyTransferOption from contextual menu
    Then I should be displayed the Balance transfer webview

  #MOB3-731, 5870
  @manual
  Scenario: AC02 TC_003_Verify Customer is able to view request balance transfer webview screen with error message
    Given I am an enrolled "Credit card customer with balance transfer enabled and insufficient credit limit" user logged into the app
    When I navigate to creditCard account on home page
    And I select the action menu of creditCard account
    And I am able to view Balance transfer option in contextual menu
    And I select the 'Balance Transfer' option
    Then I should be displayed the Balance transfer webview with an error message

  #Note - The error message shown is to indicate that customer does not have sufficient credit balance to perform balance transfer on Halifax
  #TODO MOB3-737 MOB3-7922
  @hfx @stubOnly
  Scenario: AC01 TC_004_Verify Halifax Flexicard Customer is able to view setup installment credit card webview screen
    Given I am an enrolled "Halifax Flexi credit card with installment not setup" user logged into the app
    When I navigate to creditCard account on home page
    And I select the action menu of creditCard account
    Then I am able to view setupInstallmentOption in contextual menu
#    When I select the 'Setup installment credit card' option
#    Then I should be displayed the Setup Installment Credit card webview

  #TODO MOB3-737 MOB3-7922
  @hfx @stubOnly
  Scenario: AC02 TC_005_Verify Halifax Flexicard Customer is able to view Maintain setup installment credit card webview screen
    Given I am an enrolled "Halifax Flexi credit card with installment already setup" user logged into the app
    When I navigate to creditCard account on home page
    And I select the action menu of creditCard account
    Then I am able to view maintainInstallmentOption in contextual menu
#    When I select the 'Maintain installment credit card' option
#    Then I should be displayed the Maintain Installment Credit card webview

  #MOB3-7908, 6222
  Scenario: AC01 TC_006_Verify Customer is able to view manage credit limit webview screen
    Given I am an enrolled "credit card" user logged into the app
    When I navigate to creditCard account on home page
    And I select the action menu of creditCard account
    And I am able to view manageCreditLimitOption in contextual menu
    And I select manageCreditLimitOption from contextual menu
    Then I should be displayed the Change credit limit webview

  #MOB3-756, 7925
  Scenario: AC01 TC_007_Verify Customer is able to view early settlement webview screen
    Given I am an enrolled "valid" user logged into the app
    When I navigate to nonCbsLoan account on home page
    And I select the action menu of nonCbsLoan account
    And I am able to view loanClosureOption in contextual menu
    And I select loanClosureOption from contextual menu
    Then I should be displayed the Loan closure webview

  #MOB3-5547, 7928
  Scenario: AC01 TC_008_Verify Customer is able to view borrow more webview screen
    Given I am an enrolled "valid" user logged into the app
    When I navigate to nonCbsLoan account on home page
    And I select the action menu of nonCbsLoan account
    And I select loanBorrowMoreOption from contextual menu
    Then I should be displayed the Borrow more webview

  #MOB3-5864, 7931
  Scenario: AC01 TC_009_Verify Customer is able to view renew savings webview screen
    Given I am an enrolled "valid" user logged into the app
    When I navigate to saving account on home page
    And I select the action menu of saving account
    And I am able to view renewYourSavingsAccountOption in contextual menu
    And I select renewYourSavingsAccountOption from contextual menu
    Then I should be displayed the Renew your savings account webview

  #MOB3-7919, 732
  Scenario: AC01 TC_010_Verify Customer is able to view repayment holiday webview screen
    Given I am an enrolled "non cbs personal loan" user logged into the app
    When I navigate to nonCbsLoan account on home page
    And I select the action menu of nonCbsLoan account
    And I am able to view loanRepaymentHolidayOption in contextual menu
    And I select loanRepaymentHolidayOption from contextual menu
    Then I should be displayed the Repayment holiday webview

  @pending
  Scenario: AC01 TC_011_Verify Customer is able to view overdraft webview screen
    Given I am an enrolled "valid" user logged into the app
    When I navigate to current account on home page
    And I select the action menu of current account
    And I am able to view applyForOverdraftOption in contextual menu
    And I select applyForOverdraftOption from contextual menu
    Then I should be displayed the Overdraft webview

  @pending
  Scenario: AC01 TC_011_Verify Customer is able to view overdraft webview screen
    Given I am an enrolled "valid" user logged into the app
    When I navigate to current account on home page
    And I select the action menu of current account
    And I am able to view changeAccountTypeOption in contextual menu
    And I select changeAccountTypeOption from contextual menu
    Then I should be displayed the Change account type webview
