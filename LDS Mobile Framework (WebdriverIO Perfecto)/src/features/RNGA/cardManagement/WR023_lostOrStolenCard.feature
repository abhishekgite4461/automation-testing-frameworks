@team-performance @nga3 @rnga @environmentOnly

#MOB3-2534 #MOB3-8102
Feature: Verify lost and stolen card page
  In order to verify the lost or stolen card page
  As a RNGA authenticated customer
  I want to view the details of lost and stolen card

  Background:
    Given I am an enrolled "valid" user logged into the app

  @primary @mbnaSuite
  Scenario: AC01 TC_001_User enters cancel card (Lost or Stolen cards) webview via global menu
    When I select the lost and stolen card option from global menu
    Then I should be on the lost and stolen page

  Scenario: AC01 TC_002_User enters cancel card (Lost or Stolen cards) webview via account
    When I select easy Saver account tile from home page
    And I select the "lost or stolen cards" option in the action menu of easy Saver account
    Then I should be on the lost and stolen page

  @mbna
  Scenario: AC01 TC_002_User enters cancel card (Lost or Stolen cards) webview via account
    When I select creditCard account tile from home page
    And I select the "lost or stolen cards" option in the action menu of creditCard account
    Then I should be on the lost and stolen page
