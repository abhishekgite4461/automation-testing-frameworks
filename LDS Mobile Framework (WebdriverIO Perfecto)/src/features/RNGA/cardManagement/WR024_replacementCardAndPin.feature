@team-performance @nga3 @rnga @environmentOnly

#MOB3-2535 #MOB3-3593
Feature: Verify replacement card and pin page
  In order to verify the replacement card and pin page
  As a RNGA authenticated customer
  I want to view the details of replacement card and pin

  Background:
    Given I am an enrolled "valid" user logged into the app

  @primary @mbnaSuite
  Scenario: AC01 TC_001_User enters Re-Order Card/PIN webview via global menu
    When I select the replacement card and pin option from global menu
    Then I should be on the replacement card and pin page

  Scenario: AC01 TC_002_User enters Re-Order Card/PIN webview via account menu
    When I select easy Saver account tile from home page
    And I select the "replacement card and pin" option in the action menu of easy saver account
    Then I should be on the replacement card and pin page

  @mbna
  Scenario: AC01 TC_002_User enters Re-Order Card/PIN webview via account menu
    When I select creditCard account tile from home page
    And I select the "replacement card and pin" option in the action menu of creditCard account
    Then I should be on the replacement card and pin page
