@team-payments @nga3 @rnga @ff-strong-customer-auth-on @environmentOnly
Feature: Make Payment while adding new payee
  As a RNGA Authenticated user
  I want to make payment while adding Payee

  Background:
    Given I am a current user on the home page
    And I navigate to add new recipient option using payment hub
    And I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate 110300, 12709366, TESTING
    And I select continue button on add payee page
    And I am on the enter Payments details page
    And I am on the Make Payment screen with newly added beneficiary selected

    #PAYMOB-1509
  Scenario: TC_01_User Exceeds transaction limit
    When I enter 10005.00 in the payment amount field in the payment hub page
    And I enter reference as test
    And I select Continue button on payment hub page
    Then I should see internet transaction limit exceeded error message in payment hub page

   #PAYMOB-1510
  Scenario: TC_02_Users payment exceeds account daily limit
    When I enter 100.00 in the payment amount field in the payment hub page
    And I enter reference as test
    And I select Continue button on payment hub page
    Then I should see account daily limit exceeded error message in payment hub page

  @primary @stub-sca-payment-onhold
  Scenario: TC_03_User's Payment is referred
    When I enter 40.00 in the payment amount field in the payment hub page
    And I enter reference as test
    And I select Continue button on payment hub page
    And I select the confirm option in the review payment page
    And I enter a correct password
    Then I should see the payment hold error message in the payment on hold page
    And I should see transfer and payment button in the payment on hold page
    And my new payee TESTING is not created

  @primary @stub-sca-payment-declined
  Scenario: TC_04_User's payment is declined
    When I enter 80.00 in the payment amount field in the payment hub page
    And I enter reference as test
    And I select Continue button on payment hub page
    And I select the confirm option in the review payment page
    And I enter a correct password
    Then I should see the declined error message in the payment decline page
