@team-payments @nga3 @rnga @ff-pay-credit-card-by-debit-card-on @sw-pccdc-off @mbnaSuite @primary
Feature: Pay credit card by debit card - OFF
  In order to pay my credit card
  As a RNGA customer
  I want to be use a debit card from any bank to pay my LBG credit card

  # This feature will be removed after BR19.03 release
  @stub-enrolled-typical
  Scenario: TC_001_ Main Success: Do not display Debit Card when the PCCDC switch is OFF
    Given I am a credit card and current account user on the home page
    When I navigate to the remitter account on the payment hub page
    Then I am not shown the option to pay using debit card

  @stub-credit-card-only
  Scenario: TC_002_ User does not hold other eligible remitting accounts - PCCDC switch OFF
    Given I am a credit card only user on the home page
    When I select the Payment Hub option in the bottom navigation
    Then I should see the no remitter eligible error message
    When I select Home button on no eligible accounts page
    Then I should be on the home page
    When I select the action menu of creditCard account
    Then I am not shown the pay credit card option in the Action menu
