@team-payments @nga3 @rnga @stub-enrolled-valid
# MOB3-8094 TODO pending due to most of the functionality are not delivered
Feature: Add new UK beneficiary details
  In order to to pay an external recipient
  As a RNGA authenticated customer
  I want the ability to add the basic details to add a UK Account recipient

  Background:
    Given I am an enrolled "valid" user logged into the app
    And I navigate to add new recipient option using payment hub
    And I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page

  # MOB3-1665
  Scenario: TC_003 User navigates back on UK beneficiary detail page
    When I navigate back on New UK beneficiary details page
    Then I should see the account category page

  # MOB3-1665
  @secondary @ios
  Scenario: TC_004 Switch eligible remitting accounts from New UK beneficiary details page
    And I select the remitting account
    And I should see list of eligible accounts that can add a new recipient
    When I select eligible Remitting Account on payment hub page
    Then I should see the following details in New UK beneficiary details page
      | details                                  |
      | paymentHubAddUkAccountPayeeNameLabel     |
      | paymentHubAddUkAccountAccountNumberLabel |

  # MOB3-1666
  Scenario Outline: TC_005 Valid data entered
    And I validate <sortCode>, <accountNumber>, <recipientName>
    Then I select continue button on add payee page
    Examples: Valid entries
      | sortCode | accountNumber | recipientName      |
      | 779508   | 60074163      | abcd1234           |
      | 654321   | 876543221     | 123456789012345678 |

  # MOB3-1666
  Scenario Outline: TC_006 Invalid data entered
    And I validate <sortCode>, <accountNumber>, <recipientName>
    Then I see an error message for the invalid fields
    And I am not allowed to add a new beneficiary
    Examples: Invalid entries
      | sortCode | accountNumber | recipientName     |
      | 12345    | 123456        |                   |
      |          | 123456        | abcdefghijklmnopq |
      | 23433    | 7635342       |                   |

  # MOB3-1666
  @manual
  Scenario Outline: TC_007 Invalid data entered
    And I validate <sortCode>, <accountNumber>, <recipientName>
    Then I see an error message for the invalid fields
    And I am not allowed to add a new beneficiary
    Examples: Invalid entries
      | sortCode  | accountNumber | recipientName         |
      | 123451212 | 1234561212    | abcdefghijkl123456789 |

  # MOB3-1668, 7412, 1666
  @primary
  Scenario Outline: TC_008 Display UK review details page & verify security link
    Then I should see the following details in New UK beneficiary details page
      | details             |
      | beneficiaryTileIcon |
      | payeeNameUk         |
      | sortCodeField       |
      | accountNumberUk     |
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    And I should see UK Review Details page
    And I select the security link
    And I should see a warning overlay with the ability to close
    And the selected Recipient Name, Sort Code, Account Number should be displayed
    And I should see to add a reference <reference>
    When I go back in the journey
    Then I should see the following details in New UK beneficiary details page
      | details                                  |
      | paymentHubAddUkAccountPayeeNameLabel     |
      | paymentHubAddUkAccountAccountNumberLabel |

    Examples: Enter reference
      | reference | sortCode | accountNumber | recipientName |
      | test      | 779508   | 60074163      | abcd1234      |

  # MOB3-1668, 7412
  @primary
  Scenario Outline: TC_008 Display UK review details page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    Then I should see UK Company Review Details page
    And the selected Recipient Name, Sort Code, Account Number should be displayed
    And I should see to add a reference <reference>
    Examples: Enter reference
      | reference | sortCode | accountNumber | recipientName |
      | test      | 779508   | 60074163      | abcd1234      |

  # MOB3-7614, 1670, 1668
  @primary
  Scenario Outline: TC_009 Successfully Adds New Recipient UK Account
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    Then I should see UK Review Details page
    When I select continue on UK review details page
    Then I should be prompted to enter my password
    And I enter a correct password
    And I am on the Make Payment screen with newly added beneficiary selected
    Examples: Valid entries
      | sortCode | accountNumber | recipientName |
      | 779508   | 60074163      | abcd1234      |

  # MOB3-1668, 7412
  @secondary
  Scenario Outline: TC_010 Winback on UK Review Details page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    Then I should see UK Review Details page
    When I select Cancel on UK Review Details page
    Then I should see a winback option on top of the screen with Cancel and OK option
    And on selection of a Cancel option I should stay on the same screen
    And on selection of Leave option I should navigate to account category page
    Examples: Valid entries
      | sortCode | accountNumber | recipientName |
      | 779508   | 60074163      | abcd1234      |

  # MOB3-1667, 7523
  @environmentOnly
  Scenario Outline: TC_013 Invalid combinations of sort code and account number
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    Then I should see UK Review Details page
    When I select continue on UK review details page
    Then I see an error message for the invalid fields
    Examples: Valid entries
      | sortCode | accountNumber | recipientName |
      | 252525   | 12345678      | abcd1234      |
      | 309443   | 12345678      | abcd1234      |
      | 309443   | 74881560      | abcd1234      |

  # MOB3-7436, 7465
  @manual
  Scenario: TC_015 Prohibitive CBS indicator on Account - MG-124personal, MG-513 corporate
    And there is a prohibitive indicator on the remitting account
    When I enter the correct password on the password dialog screen
    Then I am shown an Error Logged In page with an error message

  # MOB3-7436, 7465
  @manual
  Scenario: TC_016 Max arrangements - MG-125 personal, MG-514 corporate
    And I have reached the maximum number of arrangements for the account
    When I enter the correct password on the password dialog screen
    Then I am shown an Error Logged In page with an error message

  # MOB3-7436, 7465
  @manual
  Scenario: TC_017 Arrangement Rejected - MG-577
    And the service fails to create an arrangement or no connectivity
    When I enter the correct password on the password dialog screen
    Then I am shown an Error Logged In page with an error message

  # MOB3-7436, 7465
  @manual
  Scenario: TC_018 Add remitting account as beneficiary - MG-577
    And that I entered the Sort Code and Account number of the remitting account in the enter basic details page
    When I enter the correct password on the password dialog screen
    Then I am shown an Error Logged In page with an error message

  # MOB3-7436, 7465
  @manual
  Scenario: TC_019 Access Payment Hub from error page
    When I am on the Error Logged In Page
    Then I should have to option to go to the Payment and Transfers page
