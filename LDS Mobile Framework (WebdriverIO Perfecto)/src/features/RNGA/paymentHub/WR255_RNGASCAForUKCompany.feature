@team-payments @nga3 @rnga @stub-enrolled-valid @ff-strong-customer-auth-on @stubOnly
Feature: Make Payment while adding new UK company
  As a RNGA Authenticated user
  I want to make payment while adding UK company

  #PAYMOB-1064, PAYMOB-1063
  Scenario: TC_07 Select Back on header for Company Account
    Given I am an enrolled "valid" user logged into the app
    When I navigate to add new recipient option using payment hub
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I enter whitelisted company details
    And I select continue button on add payee page
    Then I am on UK Company Search results page
    When I select a result
    And I have the ability to add a reference test, test and retype the reference
    And I select continue on UK review details page
    And I enter 1.00 in the payment amount field in the payment hub page
    And I select Continue button on payment hub page
    And I select the header back option in the review payment page
    Then I am on the enter Payments details page
    When I select the header back option in the enter payment details page
    And I should see UK Company Review Details page
