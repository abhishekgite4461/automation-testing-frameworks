@team-payments @nga3 @rnga
Feature: Payment hub for transfer and payment

  As a Retail NGA user
  I want to be able to access the payment hub
  So that I can initiate transfer or payment request

  #MOB3-4486,MOB3-1637
  @stub-enrolled-typical
  Scenario: TC_001: Pre-populate from and to field when user taps on pay a credit card option from action menu
    Given I am a current saving isa creditCard mortgage loan user on the home page
    And I obtain the default primary account
    When I navigate to creditCard account on home page
    And I select the "pay a credit card" option in the action menu of creditCard account
    Then I should see the from field pre-populated with defaultPrimary account
    And I should see the to field pre-populated with creditCard account

  # MORE-1345 MORE-1317
  @stub-enrolled-typical
  Scenario Outline: TC_002 verify no winback on payment hub
    Given I am a current user on the home page
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    When I select dismiss modal using close button
    Then I should be on the home page
    Examples:
      | fromAccount | toAccount | amount |
      | current     | saving    | 1      |

  # MORE-1345 MORE-1317
  @stub-enrolled-typical
  Scenario Outline: TC_002 verify winback on payment review
    Given I am a current user on the home page
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I select the continue option in the payment hub page
    When I select dismiss modal using close button
    Then I should see the cancel and OK option in the payment hub page winback
    Examples:
      | fromAccount | toAccount | amount |
      | current     | saving    | 1      |

  @stub-enrolled-typical @primary
  Scenario Outline: TC_004: User initiate Transfers & Payments from Actions menu then pre-populate the From field based on the last visible account tile from Homepage
    Given I am a current saving isa creditCard mortgage loan user on the home page
    When I navigate to <account> account on home page
    And I select the "transfer and payment" option in the action menu of <account> account
    Then I should see the from field pre-populated with <account> account
    Examples:
      | account |
      | current |

  @stub-enrolled-typical
  Scenario Outline: TC_005: User initiate Transfers & Payments from statements page then pre-populate the From field based on the last visible account tile from Homepage
    Given I am a current saving isa creditCard mortgage loan user on the home page
    When I select <account> account tile from home page
    And I am on the payment hub page
    Then I should see the from field pre-populated with defaultPrimary account
    Examples:
      | account |
      | current |
      | saving  |

  @stub-enrolled-typical
  Scenario: TC_006: User initiate Transfers & Payments from home page then pre-populate the From field with the default primary account
    Given I am a current saving isa creditCard mortgage loan user on the home page
    When I obtain the default primary account
    And I select payAndTransfer tab from bottom navigation bar
    Then I should see the from field pre-populated with defaultPrimary account

  # android defect PAYMOB-249
  @stub-enrolled-valid @defect
  Scenario Outline: TC_007: User initiate Transfers & Payments from statements view and the last visible account tile is not eligible for payments then next eligible account should be pre-populated in From field
    Given I am a current saving isa creditCard mortgage loan user on the home page
    When I obtain the default primary account
    And I select <account> account tile from home page
    And I select payAndTransfer tab from bottom navigation bar
    Then I should see the from field pre-populated with defaultPrimary account
    Examples:
      | account    |
      | mortgage   |
      | creditCard |

  @stub-enrolled-valid @primary
  Scenario: TC_008: User initiate Transfers & Payments from home page and the last visible account tile is not eligible for payments then next eligible account should be pre-populated in From field
    Given I am a current saving isa creditCard mortgage loan user on the home page
    And I obtain the default primary account
    When I select creditCard account tile from home page
    And I am on the payment hub page
    Then I should see the from field pre-populated with defaultPrimary account
    And I select dismiss modal using close button
    And I select home tab from bottom navigation bar
    When I select mortgage account tile from home page
    And I am on the payment hub page
    Then I should see the from field pre-populated with defaultPrimary account

  @stub-paymentHub-valid @primary
  Scenario: TC_009: Display list of all eligible remitting accounts in the from field
    Given I am a current saving isa creditCard mortgage loan user on the home page
    When I choose the remitter account on the payment hub page
    Then I should see the below accounts in the view remitter page
      | accounts |
      | current  |
      | saving   |
      | isa       |
    And I should not see the below accounts in the view remitter page
      | accounts   |
      | creditCard |
      | nonCbsLoan |

  @stub-paymentHub-valid @primary
  Scenario Outline: TC_011: Verify pending payment is not allowed to select as the UK beneficiary account in the payment hub page
    Given I am a current saving isa creditCard mortgage loan user on the home page
    When I select <account> account tile from home page
    And I choose the recipient account on the payment hub page
    Then I should not be able to select pending payment from view recipient page
    Examples:
      | account |
      | current |

  @stub-enrolled-typical
  Scenario Outline: TC_012: Display international recipient set using IBAN number When IBC switch Display International Recipient in Payment Hub is ON
    Given I am a current saving isa creditCard mortgage loan user on the home page
    When I select <account> account tile from home page
    And I choose the recipient account on the payment hub page
    Then I should see the international recipient with IBAN number from view recipient page
    Examples:
      | account |
      | current |

  @stub-enrolled-typical
  Scenario Outline: TC_013: Display international recipient set using Account number When IBC switch Display International Recipient in Payment Hub is ON
    Given I am a current saving isa creditCard mortgage loan user on the home page
    When I select <account> account tile from home page
    And I choose the recipient account on the payment hub page
    Then I should see the international recipient with account number from view recipient page
    Examples:
      | account |
      | current |

  #MOB3-4489
  @stub-no-remitter-eligible-accounts @secondary
  Scenario Outline: TC_014: Verify user should not see transfer and payments in action menu of account if the user doesn't hold any eligible remiiter accounts
    Given I am a no remitter eligible accounts user on the home page
    And I select <account> account tile from home page
    When I select the action menu of <account> account
    Then I should not see Transfer and payment option in the action menu
    Examples:
      | account |
      | current |

  #MOB3-4489
  @stub-no-remitter-eligible-accounts @secondary
  Scenario: TC_016: Verify user is navigated to home page on selecting close modal option in transfer and payment can't be made error page
    Given I am a no remitter eligible accounts user on the home page
    When I select the Payment Hub option in the bottom navigation
    Then I should see the no remitter eligible error message
    When I select dismiss modal using close button
    Then I should be on the home page

  #MOB3-4489
  @stub-no-remitter-eligible-accounts @secondary
  Scenario: TC_018: Verify user is navigated to product hub page on selecting Apply for other products button in transfer and payment can't be made error page
    Given I am a no remitter eligible accounts user on the home page
    And I select the Payment Hub option in the bottom navigation
    When I select Apply for other accounts button on no eligible accounts page
    Then I should be on the product hub page

  #MOB3-1630
  @stub-no-beneficiaries @secondary
  Scenario: TC_020: Verify user is able to see no recipients have set up message
    Given I am a no recipients available user on the home page
    And I choose the recipient account on the payment hub page
    Then I see a message that I have no recipients set up on the account

  @stub-enrolled-typical
  Scenario: TC_021: Display uk beneficiary in alphabetic order
    Given I am a current user on the home page
    When I am on the view recipient page
    Then I should see the recipients in alphanumeric order in view recipient page

  @manual
  Scenario: TC_022: Display list of own accounts in "Your accounts" list as per order defined by the arrangement service
    Given I am a current saving isa creditCard mortgage loan user on the home page
    When I am on the view recipient page
    Then I should see the internal accounts as per arrangement service in view recipient page

  @stub-enrolled-typical
  Scenario: TC_023: List of recipients should refresh after scrolled down and tapped back button
    Given I am a current user on the home page
    And I am on the view recipient page
    When I scroll down in view recipient page and select header back option
    And I select the recipient option in the payment hub page
    Then I should see the recipients should refresh with top of screen in view recipient page

  @stub-enrolled-typical
  Scenario: TC_024: User should not be allowed to transfer amount in the same account
    Given I am a current user on the home page
    When I am on the view recipient page with defaultPrimary remitter account
    Then I should not see the current account in view recipient page

  @manual
  Scenario: TC_025: Closed Savings account should not be displayed in Recipient list
    Given I am a current saving isa creditCard mortgage loan user on the home page
    When I am on the view recipient page with closed savings account
    Then I should not see the closed savings account in view recipient page

  # MORE-1395
  @stub-dormant-accounts
  Scenario: TC_026_Exception Scenario: Customer logging in with only dormant accounts should see information page and should NOT be able to view Payment hub
    Given I am a dormant accounts user on the home page
    When I navigate to pay and transfer from home page
    Then I must see payments hub unavailable error message page

  # MOB3-1193, 2781, 607, MORE-1395
  @stub-invalid-accounts
  Scenario: TC_027_Exception Scenario: No valid accounts customer should be shown an information page and should NOT be able to view Payment hub
    Given I am a invalid accounts user on the home page
    When I navigate to pay and transfer from home page
    Then I must see payments hub unavailable error message page

  # PAYMOB-788 TODO: DPL-3791
  @stub-enrolled-valid @android
  Scenario: TC_028_Exception Scenario: No valid accounts customer should be shown an information page and should NOT be able to view Payment hub
    Given I am a current user on the home page
    When I select isaRemainingZero account tile from home page
    And I verify the remaining allowance zero on account tile
    And I select the action menu from statements page
    Then I should not see top up ISA option in the action menu
