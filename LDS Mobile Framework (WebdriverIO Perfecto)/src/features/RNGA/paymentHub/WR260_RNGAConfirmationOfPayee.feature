@team-payments @nga3 @rnga @sw-cop-nga-ios-on @sw-cop-nga-and-on
Feature: Enable Confirmation of Payee
  In order to to pay an external recipient
  As a RNGA authenticated customer
  I want to get the confirmation of payee details to whom I am paying through COP service

  Background:
    Given I am a current user on the Home page
    And I navigate to add new recipient option using payment hub
    And I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
    And I choose UK account from account category page

  #PAYMOB-828,723
  @stub-business-account-maybe-match
  Scenario Outline: TC_001 Payee Name partially matches (BAMM)
    Then I should see the following details in New UK beneficiary details page
      | details         |
      | payeeNameUk     |
      | sortCodeField   |
      | accountNumberUk |
    When I validate <sortCode>, <accountNumber>, <recipientName>
    And I dont choose the Business selection entity
    And I select continue button on add payee page
    Then I should see Please Wait Spinner
    And I should see Checking Details Spinner with relevant bank name
    And I should see Payee matches business account message without the <recipientName> playback
    And I see the options to change Details or Continue
    Examples:
      | sortCode | accountNumber | recipientName    |
      | 800647   | 10655465      | XYZ Developments |

  #PAYMOB-838,678
  @environmentOnly
  Scenario Outline: TC_002 Payee Name partially matches (ANNM)
    When I validate <sortCode>, <accountNumber>, <recipientName>
    And I dont choose the Business selection entity
    And I select continue button on add payee page
    Then I should see Please Wait Spinner
    And I should see Checking Details Spinner with relevant bank name
    And I should see Account name not match message
    And I should see the COP share details option
    And I see the options to change Details or Continue
    When I select the COP Share details option
    Then I should see the native OS share panel
    And I select the email app on the share panel
    When I navigate back from the mail app
    And I navigate back on New UK beneficiary details page
    And I select continue button on add payee page
    Then I should see Please Wait Spinner
    And I should see Checking Details Spinner with relevant bank name
    And I should see Account name still not match message
    And I should see the COP share details option
    And I see the options to change Details or Continue
    Examples:
      | sortCode | accountNumber | recipientName |
      | 800647   | 10658869      | Hughie        |

  #PAYMOB-2000,2001
  @stub-incorrect-account-number
  Scenario Outline: TC_003 Payee Name partially matches (AC01)
    When I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    And I should see account number invalid message in a red banner on the payment hub page
    Examples:
      | sortCode | accountNumber | recipientName  |
      | 800647   | 18411768      | Hughie         |

  # PAYMOB-677, PAYMOB-816
  @primary @stub-cop-match
  Scenario Outline: TC_05_Payee Name matches (N/A)
    And I validate <sortCode>, <accountNumber>, <recipientName>
    When I select continue button on add payee page
    Then I should see Please Wait Spinner
    And I should see Checking Details Spinner with relevant bank name
    And I am shown Payee matches message
    Examples: Valid entries
      | sortCode | accountNumber | recipientName      |
      | 800647   | 10658869      | Hughie Omai        |

  #PAYMOB-721, PAYMOB-819
  @stub-banm-match
  Scenario Outline: TC_06 Payee Name matches Business account (BANM)
    And I validate <sortCode>, <accountNumber>, <recipientName>
    When I select continue button on add payee page
    Then I should see Please Wait Spinner
    And I should see Checking Details Spinner with relevant bank name
    And I am shown Payee matches business account with <recipientName>
    And I see the options to change Details or Continue
    Examples: Valid Business entries
      | sortCode | accountNumber | recipientName  |
      | 800647   | 10655764      | Boilerman Ltd  |

  #PAYMOB-722, PAYMOB-821
  @stub-panm-match
  Scenario Outline: TC_07 Payee Name matches personal account(PANM)
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I choose the Business selection entity
    When I select continue button on add payee page
    Then I should see Please Wait Spinner
    And I should see Checking Details Spinner with relevant bank name
    And I am shown Payee matches personal account with <recipientName>
    And I see the options to change Details or Continue
    Examples: Valid Business entries
      | sortCode | accountNumber | recipientName  |
      | 800647   | 10658869      | Hughie Omai    |

  #PAYMOB-930,946
  @environmentOnly
  Scenario: Sort Code look up match and dont match
    Then I should be on the COP UK beneficiary details page
    And I verify bank name tool tip accordingly for each sort code
      | searchedSortCode | bankName             |
      | 800647           | Bank of Scotland plc |
      | 116458           | HALIFAX              |
      | 010004           | NAT WEST BANK PLC    |
      | 040004           | MONZO BANK LIMITED   |
      | 458888           | no Tool Tip          |
      | 192891           | no Tool Tip          |

  #PAYMOB-685, PAYMOB-829
  @stub-mbam-match
  Scenario Outline: TC_08 Payee Name partially matches ( MBAM)
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I choose the Business selection entity
    When I select continue button on add payee page
    Then I should see Please Wait Spinner
    And I should see Checking Details Spinner with relevant bank name
    And I am shown Payee does not fully match
    And I see the options to change Details or Continue
    Examples: Valid Personal entries
      | sortCode | accountNumber | recipientName  |
      | 800647   | 10658869      | Hughie Omai    |

  #PAYMOB-678, PAYMOB-838
  @stub-ncop
  Scenario Outline: TC_09 Display no cop (NCOP)
    And I validate <sortCode>, <accountNumber>, <recipientName>
    When I select continue button on add payee page
    Then I should see Please Wait Spinner
    And I am on the enter Payments details page
    Examples: Valid Business entries
      | sortCode | accountNumber | recipientName  |
      | 800647   | 10658869      | Hughie Omai    |

  #PAYMOB-718, PAYMOB-1749
  @stub-server-error
  Scenario Outline: TC_10 Service Errors
    And I validate <sortCode>, <accountNumber>, <recipientName>
    When I select continue button on add payee page
    Then I should see Please Wait Spinner
    And I am shown a full screen error page
    And I see the options to change Details or Continue
    Examples: Valid Business entries
      | sortCode | accountNumber | recipientName  |
      | 800647   | 10658869      | Hughie Omai    |
