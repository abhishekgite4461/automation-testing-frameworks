@team-payments @nga3 @rnga @ff-strong-customer-auth-on
Feature: Make Payment while adding new payee
  As a RNGA Authenticated user
  I want to make payment while adding Payee

  Background:
    Given I am a current user on the home page
    And I navigate to add new recipient option using payment hub
    And I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate 110300, 12709366, TESTING
    And I select continue button on add payee page
    And I am on the enter Payments details page
    And I am on the Make Payment screen with newly added beneficiary selected
    And I enter 1.00 in the payment amount field in the payment hub page
    And I enter reference as test

  #PAYMOB-1291,#PAYMOB-1294
  @primary @stub-enrolled-valid
  Scenario: TC_01 User successfully adds a new payment and makes an immediate payment 
    When I select Continue button on payment hub page
    And I select the confirm option in the review payment page
    And I enter a correct password
    Then I am shown Payment Successful page

   #PAYMOB-1290,#PAYMOB-1292
  @primary @stub-enrolled-valid
  Scenario: TC_02 Internal Payment : make future dated payment
    When I select calendar picker option for future date payment
    Then I should see a calendar view
    And I should be allowed to select a payment date up to 31 days from current day
    When I select previous month chevron from the top header of calendar view
    And I select the date as 2 from today's date from calendar view
    And I select Continue button on payment hub page
    Then I should see the review payment page
    And I am shown 'When' the payment will be made on payment review page
    When I select the confirm option in the review payment page
    And I enter a correct password
    Then I should see the payment successful message page
    And I am shown 'When' the payment will be made on payment success page
    And I navigate back to home page from payment success page

   #PAYMOB-1037,#PAYMOB-1084
  @primary @stub-enrolled-valid
  Scenario Outline:TC_03 User successful adds a new payment and set up a standing order   
    When I have chosen to make this a Standing Order option
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I provide Reference for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then   I should see a Standing Order set up success message
    Examples:
      | frequency |
      | Weekly    |
   
  #PAYMOB-1064, PAYMOB-1063
  @stub-enrolled-valid
  Scenario: TC_04 Winback on Review payments details page
    When I select Continue button on payment hub page
    And I select dismiss modal using close button
    Then I should see the leave and Stay option in the payment hub page winback
    When I select stay option from winback
    And I select dismiss modal using close button
    And I select leave option from winback
    Then I should be on the home page
   
   #PAYMOB-1064, PAYMOB-1063
  @stub-enrolled-valid
  Scenario: TC_05 Winback on enter payments details page
    When I select dismiss modal using close button
    Then I should see the leave and Stay option in the payment hub page winback
    When I select stay option from winback
    And I select dismiss modal using close button
    And I select leave option from winback
    Then I should be on the home page

   #PAYMOB-1064, PAYMOB-1063
  @stub-enrolled-valid
  Scenario: TC_06 Select Back on header for UK Account
    When I select Continue button on payment hub page
    And I select the header back option in the review payment page
    Then I am on the enter Payments details page
    When I select the header back option in the enter payment details page
    Then I should be on the New UK beneficiary details page

  @environmentOnly
  #PAYMOB-1501, PAYMOB-1509
  Scenario: TC_07 Incorrect Password below 4 attempts
    When I select Continue button on payment hub page
    And I select the confirm option in the review payment page
    And I enter incorrect password for 3 times consecutively
    Then I should be shown that password entered is incorrect

  @environmentOnly
  #PAYMOB-1501, PAYMOB-1509
  Scenario: TC_08 Incorrect Password on Final attempt
    When I select Continue button on payment hub page
    And I select the confirm option in the review payment page
    And I enter incorrect password for 4 times consecutively
    Then I should be shown that password entered is incorrect and the next one is final attempt

  @environmentOnly
  #PAYMOB-1501, PAYMOB-1509
  Scenario: TC_09  Log Out page after 5 attempts of incorrect password
    When I select Continue button on payment hub page
    And I select the confirm option in the review payment page
    And I enter incorrect password for 5 times consecutively
    Then I should be on revoked error page of SCA journey
