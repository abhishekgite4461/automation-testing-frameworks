@team-payments @nga3 @rnga @stub-enrolled-valid @ff-share-payment-receipt-on
Feature: Share Payment Receipt
  As a RNGA authenticated user
  I want to share the payment success receipt using the compatible apps on my device

  # PAYMOB-938
  @primary @genericAccount
  Scenario: TC_01 Able to share Payment Receipt using native OS share panel
    Given I am a current user on the home page
    And I am on the payment hub page
    And I choose the from account as current and to account as uKBank in the payment hub page
    And I enter 1.00 in the amount field in the payment hub page
    And I select Continue button on payment hub page
    When I select the confirm option in the review payment page
    Then I am shown Payment Successful page
    When I select the share option on the payment success page
    And I select Share on the Share Preview screen
    Then I should see the native OS share panel

  # PAYMOB-934
  @primary @genericAccount
  Scenario Outline: TC_02 Able to see the details on share preview screen for the 'Immediate' and 'Future Dated' Payment success page
    Given I am a current user on the home page
    And I am on the payment hub page
    And I choose the from account as current and to account as uKBank in the payment hub page
    And I enter 1.00 in the amount field in the payment hub page
    And I select calendar picker option for future date payment
    And I submit future date payment with date as <days> days after today
    When I select the confirm option in the review payment page
    Then I am shown Payment Successful page
    When I select the share option on the payment success page
    And I am on the Share Preview screen
    Then I should see the Bank Icon at the top of the screen
    And I should see the first 4 digits of the recipient account number is masked
    And I should see the date value on the share preview screen
    And I should see the full name of the user who logged in against from field

    Examples:
      | days |
      |  0   |
      |  2   |

  # PAYMOB-934
  @primary @genericAccount @pending
  Scenario Outline: TC_03 Able to see the details on share preview screen of the Standing Order success page
    Given I am a current user on the home page
    And I am on the payment hub page
    And I choose the from account as current and to account as uKBank in the payment hub page
    And I enter 1.00 in the amount field in the payment hub page
    And I have chosen to make this a Standing Order option
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    And I enter a correct password
    And I should see a Standing Order set up success message
    When I select the share option on the standing order success page
    And I am on the Share Preview screen
    Then I should see the Bank Icon at the top of the screen
    And I should see the first 4 digits of the recipient account number is masked
    And I should see the Standing Order duration on the share preview screen
    And I should see the full name of the user who logged in against from field

    Examples:
      | frequency    |
      | Weekly       |
      | Quarterly    |
      | Monthly      |

  # PAYMOB-934
  @primary @stubOnly @genericAccount
  Scenario Outline: TC_04 Able to see the details on share preview screen of the PayM success page
    Given I am a current user on the home page
    And I navigate to add new recipient option using payment hub
    And I choose to Add new payee on search recipient page
    And I choose UK mobile number from account category page
    And I have selected or entered a registered <mobileNumber> to receive PayM
    And I enter <amount> in the Amount field
    And I select continue in add new UK mobile number page
    And I am on the Confirm Contact page
    And I select continue on Confirm Contact page
    And I am on the Confirm Payment Page
    When I select Cancel on the Confirm Payment Page
    Then I am taken to the Confirm Contact page
    And I select Confirm
    And I am on the Confirm Payment Page
    And I select Confirm on the Confirm Payment page
    And I should be prompted to enter my password
    When I enter a correct password
    Then I am shown Payment Successful page
    When I select the share option on the payment success page
    Then I am on the Share Preview screen
    And I should see the following details populated on the share preview screen
      | detail               |
      | bankIcon             |
      | payerName            |
      | nameAndNumberPreview |
    When I select Share on the Share Preview screen
    Then I should see the native OS share panel
    Examples:
      | mobileNumber | amount |
      | 07575858520  | 20.00  |

  # Manual, since we have to test with other different apps specific to the device
  # PAYMOB-943
  @manual
  Scenario: TC_05 Able to see the pre-defined text and image in the compatible apps
    Given I am a current user on the home page
    And I am on the payment hub page
    And I choose the from account as current and to account as uKBank in the payment hub page
    And I enter 1.00 in the amount field in the payment hub page
    And I select Continue button on payment hub page
    When I select the confirm option in the review payment page
    Then I am shown Payment Successful page
    When I select the share option on the payment success page
    And I select Share on the Share Preview screen
    Then I should see the native OS share panel
    When I select a compatible app that allows population of predefined text with the image to be sent
    Then I should see the copy associated to the imaged when shared with the selected app

  # Manual, since we have to test with other different apps specific to the device
  # PAYMOB-943
  @manual
  Scenario: TC_06 Able to see only the image with the incompatible apps
    Given I am a current user on the home page
    And I am on the payment hub page
    And I choose the from account as current and to account as uKBank in the payment hub page
    And I enter 1.00 in the amount field in the payment hub page
    And I select Continue button on payment hub page
    When I select the confirm option in the review payment page
    Then I am shown Payment Successful page
    When I select the share option on the payment success page
    And I select Share on the Share Preview screen
    Then I should see the native OS share panel
    When I select a incompatible app that doesn't allow the population of predefined text with the image to be sent
    Then I should see only the image of the payment receipt being shared with the selected app
