@team-payments @nga3 @rnga @stub-enrolled-valid
Feature: Add new UK account beneficiary
  In order to to pay an external recipient
  As a NGA authenticated customer
  I want to be able to set up a recipient for that specific account

  # MOB3-1661, 1664
  @primary
  Scenario: TC_001 Add a new recipient entry point available on the 'to' account
    Given I am a valid user on the home page
    And I navigate to the remitter account on the payment hub page
    And I select eligible Remitting Account on payment hub page
    When I select the recipient account on the payment hub page
    And I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I should see beneficiaryType on beneficiary type page
      | detail                    |
      | ukAccount                 |
      | internationalBankAccount  |
      | ukMobileNumber            |
      | payingUkMobileNumberLink  |

  # MOB3-1661
  @secondary
  Scenario: TC_002 Add a new recipient entry point available on the 'to' account when no recipients have not been set up
    Given I am an enrolled "valid" user logged into the app
    And I navigate to the remitter account on the payment hub page
    And I select eligible Remitting With No Payees Account on payment hub page
    When I select the recipient account on the payment hub page
    Then I see a message that I have no recipients set up on the account

  # MOB3-1661
  # TODO DEFECT ID - MORE-1371
  @primary
  Scenario: TC003 Add a new recipient entry point unavailable on the 'to' account when ineligible account is selected
    Given I am an enrolled "valid" user logged into the app
    And I navigate to the remitter account on the payment hub page
    And I select ineligible Remitting Account on payment hub page
    When I select the recipient account on the payment hub page
    Then I should not see an option to add a new recipient

  # MOB3-1664
  Scenario: TC_004 Entry point to setting up UK account / Company recipient
    Given I am an enrolled "valid" user logged into the app
    And I navigate to the remitter account on the payment hub page
    And I select eligible Remitting Account on payment hub page
    When I select the recipient account on the payment hub page
    Then I should see an option to add a new recipient
    And I should see the option to create a new recipient using UK bank details

  # MOB3-1664
  Scenario: TC_005 Entry point to setting up International Bank Account recipient
    Given I am an enrolled "valid" user logged into the app
    And I navigate to the remitter account on the payment hub page
    And I select eligible Remitting Account on payment hub page
    When I select the recipient account on the payment hub page
    Then I should see an option to add a new recipient
    And I should able to create a new international recipient

  # MOB3-1664
  @primary
  Scenario: TC_006 Entry point to setting up UK Mobile number recipient
    Given I am an enrolled "valid" user logged into the app
    And I navigate to the remitter account on the payment hub page
    And I select eligible Remitting Account on payment hub page
    When I select the recipient account on the payment hub page
    Then I should see an option to add a new recipient
    And I should able to create a payM contact recipient

  # MOB3-1664
  @environmentOnly @secondary
  Scenario: TC_007 Adding International Recipient not available to Youth Customers
    Given I am an enrolled "youth" user logged into the app
    When I am on the payment hub page
    Then I should see error logged in page
