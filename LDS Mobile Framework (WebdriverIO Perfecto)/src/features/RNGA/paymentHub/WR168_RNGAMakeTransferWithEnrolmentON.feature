@team-payments @nga3 @rnga
Feature: Make Transfer between non-ISA account

  As a Retail NGA user
  I want to transfer money to one of my own accounts
  So that I can optimise my financial position across my accounts

  #MOB3-2262,MOB3-6617
  @stub-enrolled-valid
  Scenario Outline: TC_001: Show amount field when User select "From" and "To" field on Transfer & Payment screen
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    When I choose the from account as current and to account as <toAccount> in the payment hub page
    Then I should see amount field in the payment hub page
    And I should see standing order toggle option in the payment hub page
    Examples:
      |toAccount|
      |current  |

  @stub-enrolled-typical
  Scenario: TC_002: Doesnt show amount field when the "To" field is not selected on Transfer & Payment screen
    Given I am an enrolled "saving" user logged into the app
    And I am on the payment hub page
    When I select the from account as saving in the payment hub page
    Then I should not see amount field in the payment hub page

  @stub-enrolled-typical
  Scenario Outline: TC_003: Doesnt enable the continue button if user enters invalid amount
    Given I am an enrolled "current and savings" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    When I enter <amount> in the amount field in the payment hub page
    Then I should see the continue button disabled in the payment hub page
    Examples:
      |fromAccount|toAccount|amount|
      |current    |saving   |0     |

  @stub-enrolled-typical
  Scenario Outline: TC_004: Show amount field when User select "From" and "To" field on Transfer & Payment screen
    Given I am an enrolled "current and savings" user logged into the app
    And I am on the payment hub page
    When I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    Then I should see the continue button disabled in the payment hub page
    Examples:
      |fromAccount|toAccount|
      |current    |saving   |

  @stub-enrolled-typical
  Scenario Outline: TC_005: Show help text when user enter amount on Transfer & Payment screen
    Given I am an enrolled "current and savings" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    When I select the amount field in the payment hub page
    Then I should see the help text for amount field in the payment hub page
    Examples:
      |fromAccount|toAccount|
      |current    |saving   |

  # Validating the numeric keyboard is a part of ui testing , hence manual
  @manual @primary
  Scenario Outline: TC_006: Displays the platform specific keyboard on Transfer & Payment screen
    Given I am an enrolled "valid" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    When I select the amount field in the payment hub page
    Then I should see the numeric keyboard in the payment hub page
    Examples:
      |fromAccount|toAccount|
      |current    |saving   |

  @stub-enrolled-typical @primary
  Scenario Outline: TC_007: Displays only 2 decimal digits for the amount field on Transfer & Payment screen
    Given I am an enrolled "current and savings" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    When I enter <amount> in the amount field in the payment hub page
    Then I should see only 2 decimal places in the amount field in the payment hub page
    And I should see the continue button enabled in the payment hub page
    Examples:
      |fromAccount|toAccount|amount         |
      |current    |saving   |0.019999       |
      |current    |saving   |9999999999.9999|

  @stub-enrolled-typical
  Scenario Outline: TC_008: Auto-populate decimal digits for integer amount on Transfer & Payment screen
    Given I am an enrolled "current and savings" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    When I enter <amount> in the amount field in the payment hub page
    Then I should see only 2 decimal places in the amount field in the payment hub page
    Examples:
      |fromAccount|toAccount|amount |
      |current    |saving   |1      |

  @stub-enrolled-typical @primary
  Scenario Outline: TC_009: Enter amount greater than available balance for Transfer
    Given I am an enrolled "current and savings" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    When I enter amount greater than available balance in the remitter account in the payment hub page
    Then I should be able to close the insufficient money in your account error message in the payment hub page
    Examples:
      |fromAccount|toAccount|
      |current    |saving   |

  #MOB3-1729,6618
  @stub-enrolled-typical
  Scenario Outline: TC_010: choose edit option in the Review transfer page
    Given I am an enrolled "current and savings" user logged into the app
    And I am on the payment hub page
    And I submit the transfer with all the mandatory fields with from account as <fromAccount> and to account as <toAccount> with amount value <amount>
    When I select the edit option in the review transfer page
    Then I should see the values pre-populated in the payment hub page for transfer with amount value <amount>
    Examples:
      | fromAccount | toAccount | amount |
      | current     | saving    | 1.00   |

  #MOB3-6618
  @stub-enrolled-typical
  Scenario Outline: TC_011: choose header back option in the Review transfer page
    Given I am an enrolled "current and savings" user logged into the app
    And I am on the payment hub page
    And I submit transfer with from account <fromAccount> and to account <toAccount> with amount <amount>
    Then I should see the corresponding details in the review transfer page
    When I select the header back option in the review transfer page
    Then I should see the values pre-populated in the payment hub page for transfer with amount value <amount>
    Examples:
      |fromAccount|toAccount|amount |
      |current    |saving   |1.00   |

  #MOB3-1728
  #MOB3-9601 marking outofscope for release candidate only
  @stub-enrolled-typical @primary
  Scenario Outline: TC_012: show transfer successful for successful transfer
    Given I am an enrolled "current and savings" user logged into the app
    And I am on the payment hub page
    And I submit transfer with from account <fromAccount> and to account <toAccount> with amount <amount>
    When I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page
    Examples:
      |fromAccount|toAccount|amount |
      |current    |saving   |1.00   |

  @stub-enrolled-typical @primary
  Scenario Outline: TC_013: choose view transaction in the transfer success page
    Given I am an enrolled "current and savings" user logged into the app
    And I am on the payment hub page
    And I submit the transfer with all the mandatory fields with from account as <fromAccount> and to account as <toAccount> with amount value <amount>
    And I select the confirm option in the review transfer page
    When I select the view transaction option in the transfer success page
    And I should see the - £<amount> in the current month statement of the remitting account
    Examples:
      |fromAccount |toAccount | amount|
      |current     |saving    |1.00   |

  @stub-enrolled-typical @primary
  Scenario Outline: TC_014: Amount should be updated after successful transfer
    Given I am an enrolled "current and savings" user logged into the app
    And I am on the payment hub page
    And I submit transfer with from account <fromAccount> and to account <toAccount> with amount <amount>
    And I select the confirm option in the review transfer page
    When I navigate to the home page from transfer success page
    Then I should see the corresponding <amount> deducted in the remitter <fromAccount> and recipient accounts <toAccount>
    Examples:
      | fromAccount | toAccount | amount |
      | current     | saving    | 1      |

  @stub-enrolled-typical
  Scenario Outline: TC_015: Ability to make another transfer after successful transfer
    Given I am an enrolled "current and savings" user logged into the app
    And I am on the payment hub page
    And I submit the transfer with all the mandatory fields with from account as <fromAccount> and to account as <toAccount> with amount value <amount>
    And I select the confirm option in the review transfer page
    When I select the make another transfer option in the transfer success page
    Then I should be navigated to the payment hub page with from account as <fromAccount> in the payment hub page
    Examples:
      |fromAccount|toAccount|amount |
      |current    |saving   |1      |

  @stub-enrolled-typical
  Scenario Outline: TC_016: choose hard core back button after successful transfer
    Given I am an enrolled "current and savings" user logged into the app
    And I am on the payment hub page
    And I submit the transfer with all the mandatory fields with from account as <fromAccount> and to account as <toAccount> with amount value <amount>
    And I select the confirm option in the review transfer page
    When I select the hard core back button in the transfer success page
    Then I should be on the success transfer page
    Examples:
      |fromAccount|toAccount|amount |
      |current    |saving   |1      |

  #MOB3-6094
  @manual
  Scenario: TC_017: Monitor CCTM fraud monitoring after successful transfer
    Given I am an enrolled "current and savings" user logged into the app
    And I am on the payment hub page
    When I make a successful transfer between my accounts
    Then I should be able to see the transaction monitored in the GIBUC127 monitor transaction

  #MOB3-1723
  @environmentOnly @primary
  Scenario: TC_018: Show an error message[MG-454] amount field entered is greater than the Internet transaction limit
    Given I am an enrolled "internet transaction limit" user logged into the app
    And I am on the payment hub page
    And I submit the transfer with all the mandatory fields with any from account and to account with amount value 560.00
    When I select the confirm option in the review transfer page
    Then I should see internet transaction limit exceeded error message in payment hub page
    And I should see the values pre-populated in the payment hub page for transfer with amount value 560.00

  # MOB3-10201, MOB3-8914 open defects
  @environmentOnly @primary
  Scenario: TC_019: Show an error message MG-385  when CBS Prohibitive indicator is loaded on either the remitting or beneficiary account
    Given I am an enrolled "cbs prohibitive transfer" user logged into the app
    And I am on the payment hub page
    And I select the to account as cbsProhibitive in the payment hub page
    And I submit with amount 1.00 in the payment hub page
    When I select the confirm option in the review transfer page
    Then I should see cbs prohibitive error message in payment hub page
    And I should see the values pre-populated in the payment hub page for transfer with amount value 1.00

  # MOB3-10201, MOB3-8914 open defects
  @environmentOnly @primary
  Scenario: TC_020: Show an error message MG-1283 when monthly cap limit is set on beneficiary account and transfer limit exceed monthly limit
    Given I am an enrolled "monthly cap transfer" user logged into the app
    And I am on the payment hub page
    And I select the to account as monthlyCap in the payment hub page
    And I submit with amount 260.00 in the payment hub page
    When I select the confirm option in the review transfer page
    Then I should see monthly cap limit exceeded error message in payment hub page
    And I should see the values pre-populated in the payment hub page for transfer with amount value 260.00

  @stubOnly @stub-transfer-confirm-lost-internet @ios
  Scenario: TC_021: Show an internet connectivity error message
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I submit the transfer with all the mandatory fields with any from account and to account with amount value 1.00
    When I select the confirm option in the review transfer page
    Then I should see the internet connection lost error in transfer failure page
    And I should see the transfer and payment option in the transfer failure page

  #MOB3-6639
  @stub-enrolled-typical
  Scenario Outline: TC_022: No Winback when dismiss modal on payment hub page for transfer
    Given I am an enrolled "current and savings" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    When I select dismiss modal using close button
    Then I should be on the home page
    Examples:
      |fromAccount|toAccount|amount|
      |current    |saving   |1.00  |

  #PJO-6723, PJO-9386
  @stub-enrolled-typical
  Scenario Outline: TC_023: Verify Payment limit hub and win back on closing the modal
    Given I am an enrolled "current and savings" user logged into the app
    When I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I select view payment limits link
    And I am shown following account daily limits for the account in view payments limit page
      | detail                |
      | onlineMobileBanking   |
      | ukPaymentsDailyLimit  |
      | ukPaymentResetMessage |
      | transferYourOwnAccount|
      | telephonyBanking      |
      | ukTelephonyBanking    |
    And I select dismiss modal using close button
    Then I should see the leave and Stay option in the payment hub page winback
    And I select stay option from winback
    And I select dismiss modal using close button
    And I select leave option from winback
    And I should be on the home page
    Examples:
      |fromAccount|toAccount|amount|
      |current    |saving   |1.00  |
