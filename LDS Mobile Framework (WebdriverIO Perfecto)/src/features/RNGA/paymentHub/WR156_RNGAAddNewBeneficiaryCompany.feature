@team-payments @nga3 @rnga @stub-enrolled-typical
# MOB3-1672,7404 TODO accessibility id's are not delivered for android and IOS
Feature: Add new Company account beneficiary
  In order to pay an external Company recipient
  As a NGA authenticated customer
  I want to be able to set up a recipient for that specific account

  Background:
    Given I am an enrolled "valid" user logged into the app
    And I navigate to add new recipient option using payment hub
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I enter whitelisted company details
    And I select continue button on add payee page

  # MOB3-1671,7405
  # TODO: iOS tag will be removed once the feature has been implemented in Android PAYMOB-1956
  @primary @ios
  Scenario: TC_001 Display and Select Whitelisted companies
    Then I should see below UK List fields populated
      | fields                                            |
      | paymentHubAddUkAccountSelectionPayeeNameValue     |
      | paymentHubAddUkAccountSelectionSortCodeValue      |
      | paymentHubAddUkAccountSelectionAccountNumberValue |
    And list of matching companies with the notes
      | results                                    |
      | paymentHubAddUkAccountSelectionCompanyList |
      | businessBeneficiaryPayeeNotes              |

  # MOB3-1671,7405
  @primary
  Scenario Outline: TC_002 View UK company review details page
    And I am on UK Company Search results page
    When I select a result
    Then I should see UK Company Review Details page
    And the selected Recipient Name, Sort Code, Account Number should be displayed
    And I have the ability to add a reference <reference>, <reEnterReference> and retype the reference
    Examples: Enter references
      | reference | reEnterReference |
      | test      | test             |

  # MOB3-1671,7405
  Scenario: TC_003 Go back on company Search
    And I am on UK Company Search results page
    When I go back in the journey
    Then I should be on the New UK beneficiary details page

  # MOB3-1672,7404
  @manual @secondary
  Scenario Outline: TC_004 Max characters for reference field
    And I have entered recipient name|sort code|account number of a whitelisted company
    And I choose to Continue
    And the Search results page is displayed
    And the UK Company Review Details page is displayed
    When I enter the 19th character in the <field>
    Then no character should be inputted
    Examples:
      | field              |
      | Reference          |
      | Re-enter Reference |

  # MOB3-1672,7404
  @primary
  Scenario Outline: TC_005 Mandatory Reference fields
    And I am on UK Company Search results page
    When I select a result
    Then I should see UK Company Review Details page
    And the selected Recipient Name, Sort Code, Account Number should be displayed
    And I have the ability to add a reference <reference>, <reEnterReference> and retype the reference
    And I should not be able to continue
    And I should see an error message
    Examples: Empty Fields
      | reference | reEnterReference |
      |           |                  |

  # MOB3-1672
  @secondary
  Scenario Outline: TC_006 Mismatch References
    And I am on UK Company Search results page
    When I select a result
    Then I should see UK Company Review Details page
    And the selected Recipient Name, Sort Code, Account Number should be displayed
    And I have the ability to add a reference <reference>, <reEnterReference> and retype the reference
    And I should not be able to continue
    And I should see an error message
    Examples: Invalid references
      | reference | reEnterReference |
      | test      | test1            |

  # MOB3-1672,7404
  @manual @primary
  Scenario: TC_007 Unrecognised Reference Format
    And I have entered recipient name|sort code|account number of a whitelisted company
    And I choose to Continue
    And the Search results page is displayed
    And the UK Company Review Details page is displayed
    And I enter an invalid Reference
    When I choose to Continue
    Then I am displayed an error message

  # MOB3-1672,7404
  @secondary
  Scenario: TC_008 Display Winback on Cancel - Company Review Details page
    And I am on UK Company Search results page
    When I select a result
    Then I should see UK Company Review Details page
    When I select Cancel on UK Review Details page
    Then I should see a winback option on top of the screen with Cancel and OK option
    And on selection of a Cancel option I should stay on the same screen

  # MOB3-1672,7404
  Scenario: TC_009 Go Back on Company review page
    And I am on UK Company Search results page
    And I select a result
    And I should see UK Company Review Details page
    When I go back in the journey
    Then I am on UK Company Search results page

  # MOB3-7614, 1670
  @primary
  Scenario Outline: TC_010 Successfully Adds New Recipient Company Account
    And I select a result
    And I should see UK Company Review Details page
    And the selected Recipient Name, Sort Code, Account Number should be displayed
    And I have the ability to add a reference <reference>, <reEnterReference> and retype the reference
    And I select continue on UK review details page
    And I am displayed password confirmation screen
    When I enter a correct password
    Then I am on the Make Payment screen with newly added beneficiary selected
    Examples: Enter references
      | reference | reEnterReference |
      | test      | test             |

  # MOB3-7436, 7465
  @manual
  Scenario: TC_011 Prohibitive CBS indicator on Account MG-513
    And there is a prohibitive indicator on the account I am adding for
    When I enter the correct password on the password dialog screen
    Then I am shown an Error Logged In page with an error message

  #MOB3-7436, 7465
  @manual
  Scenario: TC_012 Max arrangements MG-514
    And I have reached the maximum number of arrangements for the account
    When I enter the correct password on the password dialog screen
    Then I am shown an Error Logged In page with an error message

  #MOB3-7436, 7465
  @manual
  Scenario: TC_013 Arrangement Rejected MG-577
    And the service fails to create an arrangement / no connectivity
    When I enter the correct password on the password dialog screen
    Then I am shown an Error Logged In page with an error message
