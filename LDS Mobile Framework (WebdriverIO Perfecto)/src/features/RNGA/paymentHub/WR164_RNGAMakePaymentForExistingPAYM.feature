@team-payments @nga3 @rnga  @stub-enrolled-valid
Feature: Pay an existing PAYM contact
  In order to make payments to existing contacts
  As a NGA user,
  I want to complete my P2P Payment
  Pre conditions:
  User has eligible remitting account
  Existing Contact set up

  Background:
    Given I am an enrolled "existing paym beneficiary" user logged into the app
    And I am on the payment hub page

  # MOB3-7755, 1539
  @secondary @ios
  Scenario Outline: TC_001 Payment initiated successfully
    When I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    Then I should see payment hub main page
    And I should see the from field pre-populated with <fromAccount> account
    And I should see the to field pre-populated with <toAccount> account
    And I should see amount field in the payment hub page
    Examples:
      | fromAccount        | toAccount                  |
      | addPayMRemitter    | addExistingPayMBeneficiary |

  # MOB3-7755, 1539
  @secondary
  Scenario Outline: TC_002 Tooltip on Amount field
    When I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I select the amount field
    Then I should see my available balance
    And I should see text advising the minimum and maximum amount
    Examples:
      | fromAccount        | toAccount                  |
      | addPayMRemitter    | addExistingPayMBeneficiary |

  # MOB3-7757, 7637
  @secondary
  Scenario Outline: TC_003 Entering an valid amount
    When I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    Then I should be able to continue with the payment
    Examples:
      | fromAccount        | toAccount                  | amount |
      | addPayMRemitter    | addExistingPayMBeneficiary | 45     |

  # MOB3-7757, 7637
  @secondary
  Scenario Outline: TC_004 Entering an invalid amount
    When I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see an error advising that the amount is outside of the transaction limits
    Examples:
      | fromAccount        | toAccount                  | amount |
      | addPayMRemitter    | addExistingPayMBeneficiary | 0.99   |
      | addPayMRemitter    | addExistingPayMBeneficiary | 301    |

  # MOB3-7755, 1539
  Scenario Outline: TC_005 Reference field text
    When I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I select the reference field in payment screen
    Then I should see some text advising that the reference will appear on the recipient's statement
    Examples:
      | fromAccount        | toAccount                  |
      | addPayMRemitter    | addExistingPayMBeneficiary |

  # MOB3-7755, 1539
  Scenario Outline: TC_006 User doesn't enter a reference
    When I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have not entered any reference text
    And I enter <amount> in the amount field in the payment hub page
    Then I should be able to continue
    Examples:
      | fromAccount        | toAccount                  | amount |
      | addPayMRemitter    | addExistingPayMBeneficiary | 10.00  |

  # MOB3-8373, 1539
  Scenario Outline: TC_007 User enters a reference
    When I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see reference text visible on the review payment screen
    Examples:
      | fromAccount        | toAccount                  | amount | referenceText |
      | addPayMRemitter    | addExistingPayMBeneficiary | 40.00  | PAYM          |

  # MOB3-7761, 1542
  Scenario Outline: TC_008 Selecting cancel on the Review payment screen
    When I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    And I select the continue option in the payment hub page
    And I am on the review payment screen
    And I select the cancel option in the review transfer page
    And I should be taken to the payments page
    And I should see the from field pre-populated with <fromAccount> account
    And I should see the to field pre-populated with <toAccount> account for p2p
    Then I should see the values pre-populated in the payment hub page for transfer with amount value <amount>
    Examples:
      | fromAccount        | toAccount                  | amount | referenceText |
      | addPayMRemitter    | addExistingPayMBeneficiary | 40.00  | PAYM          |

  # MOB3-8373, 1539
  @primary
  Scenario Outline: TC_009 Review payment page
    When I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    And I select the continue option in the payment hub page
    Then I should see the review payment page
    And I should see the values pre-populated in the payment review page for transfer with amount value <amount>
    Examples:
      | fromAccount        | toAccount                  | amount | referenceText |
      | addPayMRemitter    | addExistingPayMBeneficiary | 40.00  | PAYM          |

  # MOB3-7762, 1706
  @primary
  Scenario Outline: TC_010 Successfully making a payment
    When I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    And I select the continue option in the payment hub page
    And I select the confirm option in the review payment page
    Then I should see a payment confirmed screen
    And I should see an option make another transfer
    And I should see an option to view transactions
    Examples:
      | fromAccount        | toAccount                  | amount | referenceText |
      | addPayMRemitter    | addExistingPayMBeneficiary | 40.00  | PAYM          |

  # MOB3-8373, 1539
  @manual
  Scenario: TC_011 Payment processing
    And I have entered a valid amount
    When I confirm the payment
    Then I should see a payment processing indicator

  # MOB3-7763, 7635
  @secondary
  Scenario Outline: TC_012 Selecting View transactions on confirmation page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    And I select the continue option in the payment hub page
    And I select the confirm option in the review payment page
    And I should see a payment confirmed screen
    When I select View Transaction in transfer and payment page
    Then I should be taken to the remitting accounts statement
    Examples:
      | fromAccount        | toAccount                  | amount | referenceText |
      | addPayMRemitter    | addExistingPayMBeneficiary | 40.00  | PAYM          |

  # MOB3-7763, 7635
  @secondary
  Scenario Outline: TC_013 Selecting Transfers and Payments on confirmation page
    When I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter <amount> in the amount field in the payment hub page
    And I enter <referenceText> in the reference field in the payment hub page
    And I select the continue option in the payment hub page
    And I select the confirm option in the review payment page
    And I should see a payment confirmed screen
    And I select Transfers and Payments
    Then I should be taken to Transfer and Payments page
    Examples:
      | fromAccount        | toAccount                  | amount | referenceText |
      | addPayMRemitter    | addExistingPayMBeneficiary | 40.00  | PAYM          |
