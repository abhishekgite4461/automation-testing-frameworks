@team-payments @nga3 @rnga @sw-direct-debit-nga-on @sw-amend-standing-order-on
Feature: Direct Debit Native journey
  In order to view my direct debit
  As a RNGA authenticated customer
  I want to be able to view direct debit within my app

  #PAYMOB-1816,1979
  # TODO: iOS tag will be removed once the feature implemented for Android
  @primary @ios @stub-enrolled-valid
  Scenario: TC_001 User views direct debits
    Given I am a current user on the home page
    When I navigate to current account on home page
    And I select the "standing order & direct debit" option in the action menu of current account
    Then I am shown all direct debits set up against the account listed with following fields
      | ddFields       |
      | MerchantName   |
      | Amount         |
      | Frequency      |
    And I select a direct debit
    And I should see option to delete for a cancellable direct debit

  # TODO: iOS tag will be removed once the feature implemented for Android
  @primary @ios @stub-enrolled-valid
  Scenario: TC_002 VTD for Direct Debit
    Given I am a current user on the home page
    When I navigate to current account on home page
    And I select the "standing order & direct debit" option in the action menu of current account
    And I select a direct debit
    Then I am shown the VTD for the direct debit with following fields
      | vtdFields      |
      | MerchantVTD    |
      | AmountVTD      |
      | FrequencyVTD   |
      | LastPaid       |
      | NextPaid       |
    And I should see option to delete for a cancellable direct debit

  @primary @stub-enrolled-valid @ios @stubOnly
  Scenario: TC_003 Delete Direct debit via swipe / long press for iOS
    Given I am a current user on the home page
    When I navigate to current account on home page
    And I select the "standing order & direct debit" option in the action menu of current account
    And I swipe on the direct debit to delete
    And I am shown the option to delete
    And I select delete option
    And I am displayed a warning with option to proceed or cancel
    And I choose to continue on the warning message
    Then I am shown all direct debits set up against the account listed with following fields
      | ddFields       |
      | MerchantName   |
      | Amount         |
      | Frequency      |

  @primary @stub-enrolled-valid @android @stubOnly
  Scenario: TC_004 Delete Direct debit via swipe / long press for Android
    Given I am a current user on the home page
    When I navigate to current account on home page
    And I select the "standing order & direct debit" option in the action menu of current account
    And I swipe on the direct debit to delete
    And I am displayed a warning with option to proceed or cancel
    And I choose to continue on the warning message
    Then I am shown all direct debits set up against the account listed with following fields
      | ddFields       |
      | MerchantName   |
      | Amount         |
      | Frequency      |

  @stub-no-direct-debits
  Scenario: TC_005 User does not have Direct Debit
    Given I am a current user on the home page
    When I navigate to current account on home page
    And I select the "standing order & direct debit" option in the action menu of current account
    Then I am shown a message that I have no direct debits set up under the relevant header

  @stub-unable-to-load-dd
  Scenario: TC_006 Unable to load direct debits
    Given I am a current user on the home page
    When I navigate to current account on home page
    And I select the "standing order & direct debit" option in the action menu of current account
    Then I am shown a message that unable to load direct debits under the relevant header
