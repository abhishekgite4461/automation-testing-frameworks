@team-payments @nga3 @rnga @stub-enrolled-valid
Feature: Add new PayM beneficiary
  In order to pay an external recipient by their mobile number
  As a RNGA authenticated customer
  I want the ability to add a mobile number to pay

  Background:
    Given I am a valid user on the home page
    And I navigate to add new recipient option using payment hub
    And I choose to Add new payee on search recipient page
    And I choose UK mobile number from account category page

  # MOB3-1675, 8410
  @primary
  Scenario: TC_001 Ability to enter PayM details
    When I am on enter basic detail page for pay a contact
    Then I should see the following details in Add PayM details page
      | detail       |
      | mobileNumber |
      | amount       |
      | reference    |

  # MOB3-1675, 8410
  @secondary
  Scenario: TC_002 View eligible remitting accounts to switch to
    And I am on enter basic detail page for pay a contact
    When I select the remitting account
    Then I should see list of eligible accounts that can add a new recipient
    When I select eligible Remitting Account from the account list
    Then I should see the following details in Add PayM details page
      | detail              |
      | beneficiaryTileIcon |

  # MOB3-1676, 411
  @manual @secondary
  Scenario Outline: TC_004 Max Character limit for PayM details fields
    And I am on enter basic detail page for pay a contact
    When I enter the <Number> characters for the <Field> Field
    Then I am unable to enter any more characters
    Examples:
      | Number | Field     |
      | 21     | To        |
      | 16     | Amount    |
      | 19     | Reference |

  # MOB3-1676, 8411
  Scenario Outline: TC_005 Min Character length for To field
    And I am on enter basic detail page for pay a contact
    When I have entered 7 or less digits <mobileNumber> in the To field
    Then I am shown an error message
    Examples:
      | mobileNumber |
      | 012345       |

  # MOB3-1676, 8411
  Scenario Outline: TC_006 Invalid Amount, error code is HC-037
    And I am on enter basic detail page for pay a contact
    When I enter <amount> in the Amount field
    Then I am shown an error message
    Examples:
      | amount |
      | 0.99   |
      | 300.01 |

  # MOB3-1677, 8415
  @manual @secondary
  Scenario: TC_008 Access to phone contacts
    And I have not previously given permission for the app to access my contacts
    When I select the address book
    Then I am shown a OS message asking if I allow access to contacts

  # MOB3-1677, 8415
  @manual @secondary
  Scenario: TC_009 Grant access to phone contacts
    And I have not previously given permission for the app to access my contacts
    And  I select the address book
    When I grant access to phone contacts
    Then I am taken to a view of the devices contact list

  # MOB3-1677, 8415
  @manual @secondary
  Scenario: TC_010 Deny access to phone contacts
    And I have not previously given permission for the app to access my contacts
    And  I select the address book
    When I deny access to phone contacts
    Then I am shown a warning message that app needs access to contacts

  # MOB3-1677, 8415
  @manual @secondary
  Scenario: TC_011 Contact access turned off
    And I previously had granted access to contacts
    And I have turned off access to contacts in the device's setting menu
    When I select the address book
    Then I am shown a warning message that app needs access to contacts

  # MOB3-1677, 8415
  @manual @secondary
  Scenario: TC_012 Select a Number
    And I have previously given permission for the app to access my contacts
    And I am in my phones contact screen
    When I select a number
    Then I am shown the add PayM details page with the number selected populated in the To field
    And Contact name is populated using the address book name

  # MOB3-1679, 8401
  @primary
  Scenario Outline: TC_013 User Shown Confirm Contact Page
    And I have selected or entered a registered <mobileNumber> to receive PayM
    And I enter <amount> in the Amount field
    When I select continue in add new UK mobile number page
    Then I am on the Confirm Contact page
    And I am displayed the following details
      | detail                      |
      | nameAndPhoneNumberInConfirm |
    When I select Confirm
    Then I am taken to the Confirm Payment page
    Examples:
      | mobileNumber | amount |
      | 07575858520  | 20.00  |

  # MOB3-1679 8401
  @android
  Scenario Outline: TC_015 User Cancels on Confirm Contact page
    And I have selected or entered a registered <mobileNumber> to receive PayM
    And I enter <amount> in the Amount field
    And I select continue in add new UK mobile number page
    And I am on the Confirm Contact page
    When I Click on Cancel Button
    Then I should see the leave and Stay option in the payment hub page winback
    Examples:
      | mobileNumber | amount |
      | 07575858520  | 20.00  |

  # MOB3-1681, 8591
  @primary
  Scenario Outline: TC_018 Payment Successful
    And I have selected or entered a registered <mobileNumber> to receive PayM
    And I enter <amount> in the Amount field
    And I select continue in add new UK mobile number page
    And I am on the Confirm Contact page
    And I select continue on Confirm Contact page
    And I am on the Confirm Payment Page
    When I select Cancel on the Confirm Payment Page
    Then I am taken to the Confirm Contact page
    And I select Confirm
    And I am on the Confirm Payment Page
    And I select Confirm on the Confirm Payment page
    And I should be prompted to enter my password
    When I enter a correct password
    Then I am shown Payment Successful page
    Examples:
      | mobileNumber | amount |
      | 07575858520  | 20.00  |

  # MOB3-8145, 8585
  @secondary
  Scenario Outline: TC_019 Adding unregistered number
    And I have selected or entered an unregistered <mobileNumber> to receive PayM
    And I enter <amount> in the Amount field
    When I select Continue
    Then I am Displayed with an error
    Examples:
      | mobileNumber | amount |
      | 07123456789  | 20.00  |

  # MOB3-8145, 8585
  Scenario Outline: TC_020 Enter invalid format Number
    And I have selected or entered a registered <mobileNumber> to receive PayM
    Then I am Displayed with an error
    Examples:
      | mobileNumber |
      | +44123       |

  # MOB3-8145, 8585
  @manual @secondary
  Scenario Outline: TC_021 Over daily limit
    And I have previously transferred <Amount> using pay a contact in the current day
    And I try to send <PaymAmount> amount while adding a new PayM contact
    When I enter the correct password
    Then I am displayed an error logged in page with an error message
    Examples:
      | PayMAmount | Amount |
      | 200        | 101    |
      | 299        | 1.01   |

  Scenario Outline: TC_022 verify winback on payment hub for UK mobile number in Confirm Payee
    And  I have selected or entered a registered <mobileNumber> to receive PayM
    And  I enter <amount> in the Amount field
    And  I select continue in add new UK mobile number page
    And  I am on the Confirm Contact page
    Then I select dismiss modal using close button
    When I should see the leave and Stay option in the payment hub page winback
    Then I select stay option from winback
    And  I should be on the home page

    Examples:
      | mobileNumber | amount |
      | 07466961020  | 2.00  |

  @primary
  Scenario Outline: TC_023 verify winback on payment hub for UK mobile number in Review
    And  I have selected or entered a registered <mobileNumber> to receive PayM
    And  I enter <amount> in the Amount field
    And  I select continue in add new UK mobile number page
    And  I am on the Confirm Contact page
    Then I select dismiss modal using close button
    When I should see the leave and Stay option in the payment hub page winback
    And  I select stay option from winback
    And  I select continue on Confirm Contact page
    And  I am taken to the Confirm Payment page
    Then I select dismiss modal using close button
    When I should see the leave and Stay option in the payment hub page winback
    And  I select stay option from winback
    Then I select dismiss modal using close button
    And  I select stay option from winback
    And  I should be on the home page

    Examples:
      | mobileNumber | amount |
      | 07466961020  | 2.00   |
