@team-payments @nga3 @rnga @ff-share-payment-receipt-on
Feature: Make Payment to existing personal or corporate beneficiary online

  In order to send the money conveniently
  As a Retail NGA user
  I want to be able to make a payment to existing personal or corporate beneficiary online

  #MOB3-2263 #MOB3-6980
  @stub-payment-declined
  Scenario: TC_001: User initiate payment from eligible account to one of the existing UK bank recipient(Sort code, Account number)
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    When I select the from account as current in the payment hub page
    And I select the to account from the beneficiary list
    Then I should see amount field in the payment hub page
    And I should see reference field in the payment hub page
    And I should see standing order toggle option in the payment hub page

  @stub-payment-declined
  Scenario: TC_002: Doesn't show amount field when the "To" field is not selected on Transfer & Payment screen
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    When I select the from account as current in the payment hub page
    Then I should not see amount field in the payment hub page

  @stub-payment-declined
  Scenario Outline: TC_003: Doesn't enable the continue button if user enters invalid amount
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select the from account as current in the payment hub page
    And I select the to account from the beneficiary list
    When I enter <amount> in the payment amount field in the payment hub page
    Then I should see the continue button disabled in the payment hub page
    Examples:
      |amount|
      |0     |

  @stub-payment-declined
  Scenario: TC_004: Show amount field when User select "From" and "To" field on Transfer & Payment screen
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select the from account as current in the payment hub page
    When I select the to account from the beneficiary list
    Then I should see the continue button disabled in the payment hub page

  @stub-payment-declined
  Scenario: TC_005: Show help text when user enter amount on Transfer & Payment screen
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select the from account as current in the payment hub page
    When I select the to account from the beneficiary list
    And I select the amount field in the payment hub page
    Then I should see the help text for amount field in the payment hub page

  # Validating the numeric keyboard is a part of ui testing , hence manual
  @manual
  Scenario: TC_006: Displays the platform specific keyboard on Transfer & Payment screen
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select the from account as current in the payment hub page
    When I select the to account from the beneficiary list
    And I select the amount field in the payment hub page
    Then I should see the numeric keyboard in the payment hub page

  #MOB3-2263
  @stub-payment-declined
  Scenario Outline:TC_007: Verify amount field level validations
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select the from account as current in the payment hub page
    When I select the to account from the beneficiary list
    And I enter <amount> in the payment amount field in the payment hub page
    Then I should see only 2 decimal places in the payment amount field in the payment hub page
    And I should see the continue button enabled in the payment hub page
    Examples:
      |amount         |
      |0.019999       |
      |9999999999.9999|

  @stub-payment-declined
  Scenario Outline: TC_008: Auto-populate decimal digits for integer amount on Transfer & Payment screen
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select the from account as current in the payment hub page
    And I select the to account from the beneficiary list
    When I enter <amount> in the payment amount field in the payment hub page
    Then I should see only 2 decimal places in the payment amount field in the payment hub page
    Examples:
      |amount |
      |1.00   |

  @stub-payment-declined @secondary
  Scenario: TC_009: Enter amount greater than available balance for Transfer
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select the from account as current in the payment hub page
    And I select the to account from the beneficiary list
    When I enter payment amount greater than available balance in the remitter account in the payment hub page
    Then I should be able to close the insufficient money in your account error message in the payment hub page

  #MOB3-1729
  @stub-payment-declined
  Scenario Outline: TC_010: choose edit option in the Review payment page
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I submit the payment with all the mandatory fields with any from account and to account with amount value <amount>
    When I select the edit option in the review payment page
    Then I should see the values pre-populated in the payment hub page for transfer with amount value <amount>
    Examples:
      |amount |
      | 1.00  |

  #MOB3-6618
  @stub-payment-declined
  Scenario Outline: TC_011: choose header back option in the Review payment page
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I submit the payment with all the mandatory fields with corresponding from account as <fromAccount> and to account from beneficiary list with amount value <amount> and reference
    Then I should see the corresponding details in the review payment page
    When I select the header back option in the review payment page
    Then I should see the values pre-populated in the payment hub page for transfer with amount value <amount>
    Examples:
      |fromAccount|amount |
      |current    |1.00   |

  #MOB3-1728
  @stub-enrolled-valid @primary
  Scenario Outline: TC_012: show payment successful for successful payment
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I submit the payment with all the mandatory fields with corresponding from account as <fromAccount> and to account from beneficiary list with amount value <amount> and reference
    And I verify the reference prepopulated in review page
    When I select the confirm option in the review payment page
    Then I should see a payment confirmed screen
    And I should see share receipt option on the payment success page
    Examples:
      |fromAccount        |amount |
      |currentPayment     | 1     |

  @stub-enrolled-valid
  Scenario Outline: TC_013: choose view transaction in the payment success page
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I submit the payment with all the mandatory fields with corresponding from account as <fromAccount> and to account from beneficiary list with amount value <amount> and reference
    And I select the confirm option in the review payment page
    When I select the view transaction option in the payment success page
    And I should see the <amount> in the current month statement of the remitting account
    Examples:
      |fromAccount     |amount |
      |currentPayment  |1      |

  @stub-enrolled-valid @primary @environmentOnly
  Scenario Outline: TC_014: Amount should be updated after successful payment
    Given I am an enrolled "valid payment" user logged into the app
    And I am on the payment hub page
    And I submit the payment with all the mandatory fields with corresponding from account as <fromAccount> and to account from beneficiary list with amount value <amount> and reference
    And I select the confirm option in the review payment page
    When I navigate to the home page from payment success page
    Then I should see corresponding (.*) deducted in the remitter (.*) account
    Examples:
      |fromAccount       |amount |
      |currentPayment    |1      |

  @stub-enrolled-valid
  Scenario Outline: TC_015: Ability to make another payment after successful payment
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I submit the payment with all the mandatory fields with corresponding from account as <fromAccount> and to account from beneficiary list with amount value <amount> and reference
    And I select the confirm option in the review payment page
    When I select the make another payment option in the payment success page
    Then I should be navigated to the payment hub page with from account as <fromAccount> in the payment hub page
    Examples:
      |fromAccount       |amount |
      |currentPayment    |1      |

  @stub-enrolled-valid
  Scenario Outline: TC_017: choose Header back button after successful payment
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I submit the payment with all the mandatory fields with corresponding from account as <fromAccount> and to account from beneficiary list with amount value <amount> and reference
    And I select the confirm option in the review payment page
    When I navigate to the home page from payment success page
    Then I should be on the home page
    Examples:
      |fromAccount       |amount |
      |currentPayment    |1      |

  #MOB3-6094
  @manual
  Scenario: TC_018: Monitor CCTM fraud monitoring after successful transfer
    Given I am an enrolled "current and savings" user logged into the app
    And I am on the payment hub page
    When I make a successful transfer between my accounts
    Then I should be able to see the transaction monitored in the GIBUC127 monitor transaction

  @primary @environmentOnly
  Scenario: TC_019: Amount field entered is greater than the Internet transaction limit(MG-454)
    Given I am an enrolled "internet transaction limit for payment" user logged into the app
    And I am on the payment hub page
    When I submit the payment with all the mandatory fields with any from account and to account with amount value 10005.00
    Then I should see internet transaction limit exceeded error message in payment hub page
    And I should see the values pre-populated in the payment hub page for transfer with amount value 10005.00

  #TODO: pending should be removed once MOB3-13347 app crash issue fixed
  #MOB3-6528
  @stub-cbs-prohibitive-payments @primary @stubOnly
  Scenario Outline: TC_020: Show an error message MG-385  when CBS Prohibitive indicator is loaded on either the remitting or beneficiary account
    Given I am an enrolled "cbs prohibitive payment" user logged into the app
    And I am on the payment hub page
    When I submit the payment with all the mandatory fields with corresponding from account as <fromAccount> and to account from beneficiary list with amount value <amount> and reference
    And I select the confirm option in the review payment page
    Then I should see cbs prohibitive error message in payment unsuccessful page
    Examples:
      |fromAccount |amount |
      |current     |1      |

  #TODO : Change step to verify error message on payment unsuccessful screen once MOB3-13001 is fixed and remove iOS
  @stub-monthly-cap-exceeded @ios
  Scenario Outline: TC_021: Show an error message MG-1283 when monthly cap limit is set on beneficiary account and transfer limit exceed monthly limit
    Given I am an enrolled "monthly cap payment" user logged into the app
    And I am on the payment hub page
    When I submit the payment with all the mandatory fields with from account as <fromAccount> and <toAccount> beneficiary with amount value <amount> and reference
    And I select the confirm option in the review payment page
    Then I should see monthly cap limit exceeded error message in payment hub page
    And I should see the values pre-populated in the payment hub page for transfer with amount value <amount>
    Examples:
      |fromAccount|toAccount            | amount |
      |current    |monthlyCapBeneficiary| 251    |

  #TODO : Remove @ios when android defect MOB3-13501 is fixed
  @stubOnly @stub-payment-confirm-lost-internet @ios @defect
  Scenario: TC_022: Show an internet connectivity error message
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I submit the payment with all the mandatory fields with any from account and to account with amount value 1.00
    When I select the confirm option in the review payment page
    Then I should see the internet connection lost error in payment failure page
    And I should see the transfer and payment option in the payment failure page

  #MOB3-6639
  @stub-enrolled-typical
  Scenario: TC_023: No Winback when dismiss modal on payment hub page for payment
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select the from account as current in the payment hub page
    And I select the to account from the beneficiary list
    And I enter 1.00 in the amount field in the payment hub page
    When I select dismiss modal using close button
    Then I should be on the home page

  @primary @stub-enrolled-valid
  Scenario Outline: TC_024: User should see tooltip when payment reference is non-editable
    Given I am an enrolled "whitelisted beneficiary" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    When  I select tooltip for information on non-editable reference in the payment hub page
    Then I should be able to close the non-editable reference tooltip in the payment hub page
    Examples:
      |fromAccount                      |toAccount             |
      |currentWithWhiteListedBeneficiary|whitelistedBeneficiary|

  @stub-payment-onhold
  Scenario Outline: TC_025: Display payment on hold error message to make payment
    Given I am an enrolled "payment on hold" user logged into the app
    And I am on the payment hub page
    And I submit the payment with all the mandatory fields with corresponding from account as <fromAccount> and to account from beneficiary list with amount value <amount> and reference
    When I select the confirm option in the review payment page
    Then I should see the payment hold error message in the payment on hold page
    And I should see transfer and payment button in the payment on hold page
    When I select call us option on payment on hold page
    Then I should be on the call us home page
    Examples:
      |fromAccount        |amount  |
      |current            | 50     |

  @stub-payment-declined-scoringevent
  Scenario: TC_026: Display payment declined error message for make payment
    Given I am an enrolled "declined payment" user logged into the app
    And I am on the payment hub page
    And I submit the payment with all the mandatory fields with any from account and to account with amount value 22
    When I select the confirm option in the review payment page
    Then I should see the declined error message in the payment decline page
    And I should see pre auth and back to logon in the payment decline page

  @manual
  Scenario Outline: TC_027: Transaction monitoring using CCTM fraud monitoring system for make payment
    Given I am an enrolled "<type>" user logged into the app
    And I am on the payment hub page
    And I submit the payment with all the mandatory fields with any from account and to account with amount value "<amount>"
    When I select the confirm option in the review payment page
    Then I should verify the FS logs for sub-channel, app alias name and auth code(900 for light login and 300 for touch ID)
    Examples:
      |type            |amount|
      |declined payment|22    |
      |on hold payment |12    |

  # the script will fail for ios alone until MOB3-8914 is fixed
  @environmentOnly
  Scenario: TC_028: Amount field entered is greater than the account daily limit
    Given I am an enrolled "account daily limit for payment" user logged into the app
    And I am on the payment hub page
    And I submit the payment with all the mandatory fields with any from account and to account with amount value 560.00
    When I select the confirm option in the review payment page
    Then I should see account daily limit exceeded error message in payment hub page

  @stub-enrolled-typical
  Scenario:TC_029: Verify reference field level validations
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select the from account as current in the payment hub page
    And I select the to account from the beneficiary list
    When I clear and enter iammorethan18characters in the reference field in the payment hub page
    Then I should see only 18 characters in the reference field in the payment hub page

  @stub-enrolled-typical
  Scenario:TC_030: Verify reference field level validations for special characters
    Given I am an enrolled "current" user logged into the app
    And I am on the payment hub page
    And I select the from account as current in the payment hub page
    And I select the to account from the beneficiary list
    When I clear and enter @,&.+-'/_£$* in the reference field in the payment hub page
    Then I should see reference field with @,&.+-'/_ in the payment hub page
