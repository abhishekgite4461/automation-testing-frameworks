@team-payments @nga3 @rnga @stubOnly
Feature: Reactivate ISA
  In order to continue adding money to my ISA
  As a NGA authenticated customer with a fallow ISA account
  I want to reactivate the fallow ISA account

  # MOB3-6328
  @stub-reactivate-isa-success @primary
  Scenario: TC_001 Reactivate ISA option is available in actions menu
    Given I am an enrolled "valid fallow ISA" user logged into the app
    When I select isa account tile from home page
    And I select the action menu of isa account
    Then I see option to reactivate ISA journey

  # MOB3-6291
  @stub-reactivate-isa-success @primary
  Scenario: TC_002 Declarations page for Help to Buy ISA
    Given I am an enrolled "valid" user logged into the app
    When I select htbIsa account tile from home page
    And I select reactivate ISA option in the action menu of htbIsa account
    Then I should see below details in declaration page
      | details                                |
      | title                                  |
      | reactivateIsaAccountNameText           |
      | reactivateIsaAccountSortCode           |
      | reactivateIsaAccountNumber             |
      | reactivateIsaAccountHolderName         |
      | reactivateIsaAccountHolderAddress      |
      | reactivateIsaAccountHolderDateOfBirth  |
      | reactivateIsaNationalInsuranceNo       |
      | reactivateIsaUpdateAddressLinkText     |
      | reactivateIsaUpdateNiNumberLinkText    |

  # MOB3-6291, MOB3-12092
  @stub-reactivate-isa-success
  Scenario: TC_003 Declarations page for Help to Buy ISA
    Given I am an enrolled "valid fallow ISA" user logged into the app
    And I select htbIsa account tile from home page
    And I select reactivate ISA option in the action menu of htbIsa account
    And I choose to update national insurance number in reactivate declaration page
    And I choose to agree declaration and reactivate ISA from reactivate your ISA page
    When I enter a valid password on reactivate ISA password dialog panel
    Then I should see reactivation details in ISA Reactivation Success page
      | details                           |
      | reactivateIsaInstructionalMessage |
      | reactivateIsaAccountNameText      |
      | reactivateIsaAccountSortCode      |
      | reactivateIsaAccountNumber        |

  # MOB3-6924, MOB3-12092, PAYMOB-1218
  @stub-reactivate-isa-success
  Scenario: TC_004 Reactivate ISA update my address
    Given I am an enrolled "valid" user logged into the app
    When I select isa account tile from home page
    And I select reactivate ISA option in the action menu of isa account
    And I choose to update my address in reactivate declaration page
    Then I should see the current address in update address page
      | address                                         |
      | settingsPersonalDetailsAddressTitle             |
      | settingsPersonalDetailsAddressLineOne           |
      | settingsPersonalDetailsPostcode                 |
      | settingsPersonalDetailsChangeAddressDescription |
      | settingsPersonalDetailsChangeAddressButton      |
    When I select call to change address button on change address page
    Then I should be on the update your address call us page

  # MOB3-6679
  @stub-reactivate-isa-success @primary
  Scenario: TC_005 Declarations page for all ISA except Help to Buy
    Given I am an enrolled "valid fallow ISA not Help to Buy ISA" user logged into the app
    When I select isa account tile from home page
    And I select reactivate ISA option in the action menu of isa account
    And I choose to agree declaration and reactivate ISA from reactivate your ISA page
    And I should be prompted to enter my password
    And I enter a valid password on reactivate ISA password dialog panel
    Then I should see reactivation details in ISA Reactivation Success page
      | details                               |
      | reactivateIsaSupportingCopy           |
      | reactivateIsaAccountNameText          |
      | reactivateIsaAccountSortCode          |
      | reactivateIsaAccountNumber            |
      | reactivateIsaRemainingAllowanceText   |
      | reactivateIsaRemainingAllowanceAmount |

  # MOB3-6365
  @stub-reactivate-isa-success @primary
  Scenario: TC_006 Password prompt appears when accepting the declarations
    Given I am an enrolled "valid" user logged into the app
    When I select isa account tile from home page
    And I select reactivate ISA option in the action menu of isa account
    And I choose to agree declaration and reactivate ISA from reactivate your ISA page
    Then I should be prompted to enter my password
    When I enter a valid password on reactivate ISA password dialog panel
    Then I should see reactivation details in ISA Reactivation Success page
      | details                             |
      | reactivateIsaSuccessHeader          |
      | reactivateIsaSupportingCopy         |
      | reactivateIsaAccountNameText        |
      | reactivateIsaAccountSortCode        |
      | reactivateIsaAccountNumber          |
      | reactivateIsaRemainingAllowanceText |

  @stub-reactivate-isa-wrong-password
  Scenario: TC_007 Password entered incorrectly
    Given I am an enrolled "valid fallow ISA" user logged into the app
    And I select isa account tile from home page
    And I select reactivate ISA option in the action menu of isa account
    And I choose to agree declaration and reactivate ISA from reactivate your ISA page
    And I should be prompted to enter my password
    When I have entered wrong password
    Then I should be displayed an error message for wrong password
    And I should be given an option to enter the password again

  # MOB3-6677
  @stub-valid-fallow-isa-with-remaining-allowance-above-0 @primary
  Scenario: TC_008 Success screen- ISA has allowance left and user has a remitting account
    Given I am an enrolled "valid fallow ISA with remaining allowance above 0" user logged into the app
    When I select isa account tile from home page
    And I select reactivate ISA option in the action menu of isa account
    And I choose to agree declaration and reactivate ISA from reactivate your ISA page
    Then I should be prompted to enter my password
    When I enter a valid password on reactivate ISA password dialog panel
    Then I should see instructional message

  # MOB3-6677
  @stub-valid-fallow-isa-with-no-remaining-allowance
  Scenario: TC_009 Success screen- ISA has NO remaining allowance and user has a remitting account
    Given I am an enrolled "valid fallow ISA with No remaining allowance" user logged into the app
    And I select cashIsa account tile from home page
    And I select reactivate ISA option in the action menu of cashIsa account
    And I choose to agree declaration and reactivate ISA from reactivate your ISA page
    And I should be prompted to enter my password
    When I enter a valid password on reactivate ISA password dialog panel
    Then I am on Reactivate ISA success screen
    And I should not see an option to add money to the ISA

  # MOB3-6677
  @stub-valid-fallow-isa-with-remaining-allowance-above-0-and-No-Remitting-account
  Scenario: TC_010 Success screen- ISA has remaining allowance and user has NO remitting account
    Given I am an enrolled "valid fallow ISA with remaining allowance above 0 and No Remitting account" user logged into the app
    And I select isa account tile from home page
    And I select reactivate ISA option in the action menu of isa account
    And I choose to agree declaration and reactivate ISA from reactivate your ISA page
    And I should be prompted to enter my password
    When I enter a valid password on reactivate ISA password dialog panel
    Then I am on Reactivate ISA success screen
    And I should see a message how to make an external transfer to my ISA
    And I should not see an option to add money to the ISA

  # MOB3-6720
  @stub-valid-fallow-isa
  Scenario: TC_011 Success screen- ISA isn't enabled to receive a transfer
    Given I am an enrolled "valid fallow ISA" user logged into the app
    And I select isa account tile from home page
    And I select reactivate ISA option in the action menu of isa account
    And I choose to agree declaration and reactivate ISA from reactivate your ISA page
    And I should be prompted to enter my password
    When I enter a valid password on reactivate ISA password dialog panel
    Then I am on Reactivate ISA success screen
    And I should not see an option to add money to the ISA

  @manual
  Scenario: TC_012 CBS call fails after accepting the declarations
    Given I am an enrolled "valid fallow ISA" user logged into the app
    And I have confirmed the declarations
    And I am prompted to enter my password
    When I enter a valid password
    And the CBS call fails
    Then I should be shown with a native Error Page Logged In message

  #MOB3-10808, MOB3-10803, MOB3-10812, MOB3-10809
  @stub-reactivate-isa-success
  Scenario: TC_013 Display Reactivate ISA success page with the top up isa journey
    Given I am an enrolled "valid fallow ISA" user logged into the app
    And I select isa account tile from home page
    And I select top up ISA option in the action menu of isa account
    And I enter amount greater than remaining allowance in the recipient account in the payment hub page
    And I select the confirm option in the review transfer page
    And I select the reactivate ISA option from the reactivate ISA interstitial page
    When I choose to agree declaration and reactivate ISA from reactivate your ISA page
    And I enter a valid password on reactivate ISA password dialog panel
    Then I am on Reactivate ISA success screen

  #MOB3-11301
  @stub-enrolled-valid
  Scenario: TC_014 Selecting Reactive ISA option from Account tile
    Given I am an enrolled "valid fallow ISA" user logged into the app
    And I select isa account tile from home page
    When I choose reactivate ISA option from isa account tile in home page
    Then I should be on the reactivate ISA declaration page

  #MOB3-11301
  @stub-enrolled-valid
  Scenario: TC_015 Not selecting Reactive ISA option from Account tile
    Given I am an enrolled "valid fallow ISA" user logged into the app
    When I navigate to isa account and choose not now option from alert notification in the home page
    Then I should be on the home page

  #MOB3-10809 AC-3
  @manual
  Scenario: TC_016 Top up fallow ISA less than its remaining allowance
    Given I am an enrolled "valid fallow ISA" user logged into the app
    And I transfer money from any account to isa account
    And I navigate back to home page
    And I select isa account tile from home page
    And I select top up isa option from the action menu in the home page
    And I enter amount less than amount transferred to isa account
    When I select the confirm option in the review transfer page
    Then I should be on the success transfer page

  #MOB3-11301
  @manual
  Scenario: TC_007 Verify reactivate ISA option is not visible once its reactivated
    Given I am an enrolled "valid fallow ISA" user logged into the app
    And I navigate to isa account tile
    And I select reactivate ISA option in the action menu
    And I choose to agree declaration and reactivate ISA from reactivate your ISA page
    And I should be prompted to enter my password
    And I enter a valid password on reactivate ISA password dialog panel
    And I am on Reactivate ISA success screen
    When I logoff
    And I login with "valid fallow ISA" user into the app
    Then I should not be shown reactivate isa option in actions menu
