@team-payments @nga3 @rnga @sw-international-payments-off @environmentOnly
Feature: Payment hub for transfer and payment

  As a Retail NGA user
  I want to be able to access the payment hub
  So that I can initiate transfer or payment request

  Scenario Outline: TC_001: Don't Display international recipient in the list if IBC switch Display International Recipient in Payment Hub is OFF
    Given I am an enrolled "current saving isa creditCard mortgage loan" user logged into the app
    When I select <account> account tile from home page
    And I choose the recipient account on the payment hub page
    Then I should not see the international recipient from view recipient page
    Examples:
      |account  |
      |current  |
