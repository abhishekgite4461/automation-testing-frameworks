@team-payments @sw-enrolment-off @sw-light-logon-off @rnga @nga3 @stub-valid @stubOnly
Feature: Make Transfer between non-ISA account with device not already enrolled and enrolment switch is OFF

  As a Retail NGA user
  I want to transfer money to one of my own accounts
  So that I can optimise my financial position across my accounts

  Background:
    Given I am a current user
    And I login with my username and password
    And I enter correct MI
    And I am on the payment hub page

  Scenario: TC_001: User should not see standing order option in payment hub page if device is not enrolled for payment
    When I select the from account as current in the payment hub page
    And I select the to account from the beneficiary list
    Then I should see amount field in the payment hub page
    And I should not see standing order toggle option in the payment hub page
