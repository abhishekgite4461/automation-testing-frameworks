@team-payments @nga3 @rnga @ff-strong-customer-auth-on @sw-faster-payment-error-on @stub-enrolled-valid
Feature: Verify Faster Payment error message when the payment services is down
  As a RNGA Authenticated user
  I want to make payment while adding Payee

  # PAYMOB-951
  Scenario: TC_01_User shown warning message in the payment hub
    Given I am a current user on the home page
    And I am on the payment hub page
    And I should not see amount field in the payment hub page
    Then I am displayed a faster payment warning message

  # PAYMOB-1331
  Scenario: TC_02_User showm warning message on immediate payment
    Given I am a current user on the home page
    And I am on the payment hub page
    When I submit the payment with all the mandatory fields with any from account and to account with amount value 1.00
    And I am displayed faster payment warning pop up message
    Then I have link to view online banking status

  # PAYMOB-1331
  Scenario: TC_03_User linked to service status page
    Given I am a current user on the home page
    And I am on the payment hub page
    When I submit the payment with all the mandatory fields with any from account and to account with amount value 1.00
    And I am displayed faster payment warning pop up message
    And I have link to view online banking status
    And I select link to view online banking status
    Then I am displayed a warning message
    And I select leave option from winback
#    Then I am on the status page in the native browser

  # PAYMOB-1332
  Scenario Outline: TC_04_User shown warning message when the user adding new payee
    Given I am a current user on the home page
    When I navigate to add new recipient option using payment hub
    Then I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
    And I can see paym is disabled
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    Then I select continue button on add payee page
    And I am on the Make Payment screen with newly added beneficiary selected
    And I enter amount in the amount field as 1.00
    And I enter reference as reference
    And I select Continue button on payment hub page
    And I am displayed faster payment warning pop up message
    Examples:
      | sortCode | accountNumber | recipientName |
      | 110300   | 12709366      | TEST01231     |
