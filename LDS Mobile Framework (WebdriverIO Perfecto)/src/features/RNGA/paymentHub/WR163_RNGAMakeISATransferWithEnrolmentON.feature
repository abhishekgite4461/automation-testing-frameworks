@team-payments @nga3 @rnga
Feature: Make Transfer between non-ISA account

  As a Retail NGA user
  I want to transfer money between isa accounts
  So that I can optimise my financial position across my accounts

  @stub-enrolled-valid
  Scenario: TC_001:Validating the entry point for Top up ISA from home page action menu
    Given I am an enrolled "top up isa" user logged into the app
    And I select topUpIsa account tile from home page
    When I select the action menu of topUpIsa account
    Then I should see the top up isa option in the action menu

  @stub-enrolled-valid
  Scenario: TC_002:Validating the entry point for Top up ISA from statements view action menu
    Given I am an enrolled "top up isa" user logged into the app
    And I select topUpIsa account tile from home page
    When I select the action menu of topUpIsa account
    Then I should see the top up isa option in the action menu

  # MOB3-10566 account tile swiping step can replaced once the bug is closed
  @stub-enrolled-valid @primary
  Scenario: TC_003: Top up Help to Buy ISA account using non-ISA account(HC-139) where remaining allowance is >0
    Given I am an enrolled "htb with top up isa" user logged into the app
    And I am on the payment hub page
    And I select the to account as htbIsa in the payment hub page
    When I submit with amount 1 in the payment hub page
    Then I should see the htbIsa warning message in the review transfer page
    When I select the confirm option in the review transfer page
    Then I should see view transaction and make another transfer option in the transfer success page

  @stub-enrolled-valid @primary
  Scenario: TC_004: Top up isa account excluding Help to Buy ISA account using non-ISA account(HC-139) where remaining allowance is >0
    Given I am an enrolled "top up isa" user logged into the app
    And I am on the payment hub page
    And I select the to account as topUpIsa in the payment hub page
    When I submit with amount 1 in the payment hub page
    Then I should see the topUpIsa warning message in the review transfer page
    When I select the confirm option in the review transfer page
    Then I should see view transaction and make another transfer option in the transfer success page

  #MOB3-8507, #MOB3-8506
  @stub-enrolled-valid @primary
  Scenario Outline: TC_005: Display ISA Warning when payment is initiated from variable Rate ISA to non-LBG account
    Given I am an enrolled "variable rate isa" user logged into the app
    And I am on the payment hub page
    When I submit the payment with all the mandatory fields with from account as <fromAccount> and any beneficiary with amount value <amount> and reference
    Then I should see withdraw from ISA to non lbg account warning message on review payment screen
    Examples:
      |fromAccount     |amount|
      |isaPayment      | 1    |

  #MOB3-8507, #MOB3-8506
  #Payments from fixed rate isa is allowed only on LDS.
  @environmentOnly @lds @primary
  Scenario Outline: TC_006: Display ISA Warning when payment is initiated from fixedrate ISA to non-LBG account
    Given I am an enrolled "fixed rate isa" user logged into the app
    And I am on the payment hub page
    When I submit the payment with all the mandatory fields with from account as <fromAccount> and any beneficiary with amount value <amount> and reference
    Then I should see withdraw from ISA to non lbg account warning message on review payment screen
    Examples:
      |fromAccount     |amount|
      |fixedRateISA    | 1    |

  #MOB3-8507, #MOB3-8506
  #Payments from HTB is allowed only on BOS and HFX.
  @environmentOnly @bos @hfx @primary
  Scenario Outline: TC_007: Display ISA Warning when payment is initiated from HTB ISA to non-LBG account
    Given I am an enrolled "htb isa" user logged into the app
    And I am on the payment hub page
    When I submit the payment with all the mandatory fields with from account as <fromAccount> and any beneficiary with amount value <amount> and reference
    Then I should see withdraw from ISA to non lbg account warning message on review payment screen
    Examples:
      |fromAccount     |amount|
      |htbIsa          | 1    |

  #MOB3-8507, #MOB3-8506
  @environmentOnly
  Scenario Outline: TC_008: Validate ISA warning message when payment is initiated and  Remaining allowance should be reflected on Homepage when user is moving money from variable rate ISA account to credit card successfully
    Given I am an enrolled "variable rate isa" user logged into the app
    And I am on the payment hub page
    And I submit the isa transfer with all the mandatory fields with corresponding from account as <fromAccount> and to account as <toAccount> with amount value 1
    Then I should see withdraw from ISA to non lbg account warning message on review payment screen
    When I select ISA transfer agreement checkbox and confirm option in the review payment page
    Then I should see view transaction and make another payment option in the payment success page
    When I navigate to the home page from payment success page
    And I should see the corresponding remaining allowance 1 deducted in the remitter variable rate isa account
    Examples:
      |fromAccount     |toAccount                |
      |isaWithPayment  |creditCardForIsaTransfer |

  #MOB3-8507, #MOB3-8506
  #payments from fixed rate isa is only allowed on Lloyds
  @environmentOnly @lds @primary
  Scenario Outline: TC_009: Validate ISA warning message when payment is initiated and  Remaining allowance should be reflected on Homepage when user is moving money from variable rate ISA account to credit card successfully
    Given I am an enrolled "fixed rate isa" user logged into the app
    And I am on the payment hub page
    And I submit the isa transfer with all the mandatory fields with corresponding from account as <fromAccount> and to account as <toAccount> with amount value 1
    Then I should see withdraw from ISA to non lbg account warning message on review payment screen
    When I select ISA transfer agreement checkbox and confirm option in the review payment page
    Then I should see view transaction and make another payment option in the payment success page
    When I navigate to the home page from payment success page
    And I should see the corresponding remaining allowance 1 deducted in the remitter fixed rate isa account
    Examples:
      |fromAccount     |toAccount                |
      |fixedRateISA    |creditCardForIsaTransfer |

  #MOB3-8507, #MOB3-8506
  #payments from HTB is only allowed on BOS and HFX
  @environmentOnly @bos @hfx @primary
  Scenario Outline: TC_010: Validate ISA warning message when payment is initiated and  Remaining allowance should be reflected on Homepage when user is moving money from variable rate ISA account to credit card successfully
    Given I am an enrolled "htb isa" user logged into the app
    And I am on the payment hub page
    And I submit the isa transfer with all the mandatory fields with corresponding from account as <fromAccount> and to account as <toAccount> with amount value 1
    Then I should see withdraw from ISA to non lbg account warning message on review payment screen
    When I select ISA transfer agreement checkbox and confirm option in the review payment page
    Then I should see view transaction and make another payment option in the payment success page
    When I navigate to the home page from payment success page
    And I should see the corresponding remaining allowance 1 deducted in the remitter help to buy isa account
    Examples:
      |fromAccount     |toAccount                |
      |htbIsa          |creditCardForIsaTransfer |

  # MOB3-1537
  @stub-isa-max-amount
  Scenario: TC_011: Display error message when User has entered transfer amount value greater than remaining ISA allowance
    Given I am an enrolled "isa with default main balance more than isa limit" user logged into the app
    And I am on the payment hub page
    And I select the to account as isa in the payment hub page
    When I enter amount greater than remaining allowance in the recipient account in the payment hub page
    And I select the confirm option in the review transfer page
    Then I should see the max ISA transfer error message on payment hub page

  #MOB3-1537
  @stub-isa-min-amount
  Scenario: TC_012: Display error message when user has entered transfer amount less than minimum ISA transfer limit
    Given I am an enrolled "isa with default main balance more than isa limit" user logged into the app
    And I am on the payment hub page
    When I submit the transfer with all the mandatory fields with from account as current and to account as isa with amount value 0.01
    Then I should see the min ISA error message on payment hub page

  # MOB3-10566 account tile swiping step can replaced once the bug is closed
  @stub-enrolled-valid @primary
  Scenario: TC_013: ISA Remaining allowance should be reflected on Homepage when user is moving money from non ISA to ISA(excluding HTB) account successfully
    Given I am an enrolled "top up isa" user logged into the app
    And I am on the payment hub page
    And I submit the isa transfer with all the mandatory fields with from account as current and corresponding to account as topUpIsa with amount value 1
    When I select the confirm option in the review transfer page
    And I navigate to the home page from transfer success page
    Then I should see the corresponding remaining allowance 1 deducted in the recipient top up isa account

  @environmentOnly
  Scenario Outline: TC_014: ISA Remaining allowance should be reflected on Homepage when user is moving money from ISA to non lbg account successfully
    Given I am an enrolled "variable rate isa" user logged into the app
    And I am on the payment hub page
    And I submit the isa transfer with all the mandatory fields with corresponding from account as <fromAccount> and to account as <toAccount> with amount value 1
    When I select ISA transfer agreement checkbox and confirm option in the review payment page
    And I navigate to the home page from payment success page
    Then I should see the corresponding remaining allowance 1 deducted in the remitter variable rate isa account
    Examples:
      |fromAccount     |toAccount  |
      |isaWithPayment  |nonLBG     |

  #MOB3-1658,MOB3-8335
  @stub-enrolled-valid
  Scenario: TC_015: ISA Remaining allowance should be reflected on Homepage when user is moving money from  ISA to non ISA account successfully
    Given I am an enrolled "top up isa" user logged into the app
    And I am on the payment hub page
    And I submit the isa transfer with all the mandatory fields with topUpIsa from account and to account with amount value 1
    When I select ISA transfer agreement checkbox and confirm option in the review transfer page
    And I navigate to the home page from transfer success page
    Then I should see the corresponding remaining allowance 1 deducted in the remitter top up isa account

  #MOB3-1658,MOB3-8335
  @stub-enrolled-valid @primary
  Scenario: TC_016: Transfer success scenario from ISA to non ISA
    Given I am an enrolled "top up isa" user logged into the app
    And I am on the payment hub page
    And I submit the transfer with all the mandatory fields with topUpIsa from account and to account with amount value 1
    When I select ISA transfer agreement checkbox and confirm option in the review transfer page
    Then I should see view transaction and make another transfer option in the transfer success page

  #MOB3-1658,MOB3-8335
  @stub-isa-transfer-warning @primary
  Scenario: TC_017: Display ISA Warning when transfer from ISA to non ISA
    Given I am an enrolled "htb isa" user logged into the app
    And I am on the payment hub page
    When I submit the transfer with all the mandatory fields with htbIsa from account and to account with amount value 1
    Then I should see withdraw from ISA to non ISA warning message on review transfer screen

  #MOB3-8350,MOB3-7992
  @stub-to-htbisa-above-fund-limit @primary
  Scenario: TC_018: Non-ISA to Help to Buy ISA account transfer above funding limit
    Given I am an enrolled "htb isa" user logged into the app
    And I am on the payment hub page
    And I select the to account as htbIsa in the payment hub page
    And I submit with amount 220 in the payment hub page
    When I select the confirm option in the review transfer page
    Then I should see the funding limit more than limit error message on the error page
    When I select the home option in the error page
    Then I should be on the home page

  #MOB3-8352,MOB3-2375
  @stub-isa-transfer-blocked @primary
  Scenario: TC_019: Block users from transferring money from Non-ISA to previous year ISA account where remaining allowance is equals to 0
    Given I am a previous year isa with remaining allowance zero user on the home page
    And I am on the payment hub page
    When I select the previousIsa with zero remaining allowance as recipient in payment hub page
    And I submit with amount 1 in the payment hub page
    Then I should see already subscribed to isa error message in transfer failure page

  #MOB3-2376,MOB3-8358
  @stub-isa-transfer-blocked @primary
  Scenario Outline: TC_020: BLOCK Customers from Funding Two Cash or Fixed ISAs when the current year isa is already funded
    Given I am an enrolled "current and previous year isa with funded" user logged into the app
    And I am on the payment hub page
    When I submit the transfer with all the mandatory fields with from account as <fromAccount> and to account as <toAccount> with amount value 1
    Then I should see already subscribed to isa error message in transfer failure page
    Examples:
      |fromAccount       |toAccount      |
      |currentIsaFunded  | currentIsa    |
      |currentIsaFunded  | previousIsa   |
      |previousIsaFunded | previousIsa   |

  @stub-isa-transfer-warning @primary
  Scenario Outline: TC_021: Display ISA warning overlay message when user transfer amount from previous year ISA account to current year ISA and User has already funded ISA in current tax year
    Given I am an enrolled "current and previous year isa with funded" user logged into the app
    And I am on the payment hub page
    When I submit the isa transfer with all the mandatory fields with from account as <fromAccount> and to account as <toAccount> with amount value 1
    Then I should see withdraw from ISA to ISA warning message on review transfer screen
    Examples:
      |fromAccount       |toAccount         |
      |previousIsa       | currentIsa       |
      |previousIsa       | previousIsaFunded|

  @stub-isa-transfer-warning @primary
  Scenario Outline: TC_022: Validate ISA to ISA transfer when user has already funded ISA in current tax year
    Given I am an enrolled "current and previous year isa with funded" user logged into the app
    And I am on the payment hub page
    And I submit the isa transfer with all the mandatory fields with from account as <fromAccount> and to account as <toAccount> with amount value 1
    When I select ISA transfer agreement checkbox and confirm option in the review transfer page
    Then I should see view transaction and make another transfer option in the transfer success page
    Examples:
      |fromAccount       |toAccount         |
      |previousIsa       | currentIsa       |
      |previousIsa       | previousIsaFunded|

  @stub-isa-transfer-warning @primary
  Scenario: TC_023: Should see the confirm button disabled when transfer agreement checkbox is not checked
    Given I am an enrolled "current and previous year isa with funded" user logged into the app
    And I am on the payment hub page
    When I submit the isa transfer with all the mandatory fields with from account as previousIsa and to account as currentIsa with amount value 1
    Then I should see confirm button disabled in the review transfer screen
