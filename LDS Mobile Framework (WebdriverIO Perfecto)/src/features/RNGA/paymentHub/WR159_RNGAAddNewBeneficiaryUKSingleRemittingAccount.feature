@team-payments @nga3 @rnga @stub-enrolled-typical @secondary @lds @ios
#MOB3-1665
Feature: Add new UK beneficiary details
  In order to to pay an external recipient
  As a RNGA authenticated customer
  I want the ability to add the basic details to add a UK Account recipient

  Scenario: TC_001 No eligible account to switch to from New UK mobile beneficiary details page in ios
    Given I am an enrolled "single account" user logged into the app
    When I navigate to add new recipient option using payment hub
    Then I should see an option to add a new recipient
    When I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I select the remitting account
    Then I should see list of eligible accounts that can add a new recipient
