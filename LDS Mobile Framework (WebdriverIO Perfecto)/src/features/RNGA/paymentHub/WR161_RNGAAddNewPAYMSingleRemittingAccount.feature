@team-payments @nga3 @rnga @stub-enrolled-typical @secondary @lds
# MOB3-1675, 8410
Feature: Add new PayM beneficiary
  In order to pay an external recipient by their mobile number
  As a RNGA authenticated customer
  I want the ability to add a mobile number to pay

  Scenario: TC_001 No eligible account to switch to from New UK beneficiary details page in android
    Given I am an enrolled "single account" user logged into the app
    When I navigate to add new recipient option using payment hub
    And I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    And I choose UK mobile number from account category page
    And I select the remitting account
    Then I should see list of eligible accounts that can add a new recipient
