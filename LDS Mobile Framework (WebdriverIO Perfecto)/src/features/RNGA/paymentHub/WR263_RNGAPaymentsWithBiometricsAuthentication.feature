@rnga @nga3 @team-payments @sw-enrolment-on @sw-light-logon-on @sca @sw-biometric-payment-on @stub-enrolled-valid @manual
Feature: Biometrics In RNGA Payments
  In order set up a new payee / amend a standing order
  As a RNGA customer
  I want to be able to authenticate using biometric

  Background:
    Given Biometric Payments switch is ON
    And user is opted into using biometric

  #PJO-6646,6649
  @primary
  Scenario Outline: TC_001_ Main Success: User adds new payee using Bio successfully (Immediate payment)
    Given I am an enrolled user logged into the app
    And I am on payments hub
    And I have chose to add a new payee
    And I have entered my payment details
    And I select Continue on review screen
    When I authenticate with <Biometric>
    And I receive success message
    Then I am shown payment & payee have been added successfully
    Examples:
      | TouchID |
      | FaceID  |

  #PJO-6603,6663
  @primary
  Scenario Outline: TC_002_ Main Success: User adds new payee using Bio successfully (standing order payment)
    Given I am an enrolled user logged into the app
    And I am on payments hub
    And I have chose to add a new payee
    And I have entered my payment details
    And I have selected standing orders
    And I select Continue on review screen
    When I authenticate with <Biometric>
    And I receive success message
    Then I am shown payment & payee have been added successfully
    Examples:
      | TouchID |
      | FaceID  |

  #PJO-6668,6575
  @primary
  Scenario Outline: TC_003_ Main Success: User amends standing order using Bio successfully (standing order payment)
    Given I am an enrolled user logged into the app
    And I am on the scheduled payments hub
    And I have chosen to amend a standing order
    And I have edited my payment details
    And I select Continue on review screen
    When I authenticate with <Biometric>
    And I receive success message
    Then I am shown standing order amended successfully
    Examples:
      | TouchID |
      | FaceID  |

  #PJO-6730,6731
  Scenario Outline: TC_004_ Exception Success: User falls back to password when adding new payee successfully (Immediate payment, FDP)
    Given I am an enrolled user logged into the app
    And I am on payments hub
    And I have chose to add a new payee
    And I have entered my payment details
    And I select Continue on review screen
    When I fail to authenticate with <Biometric> the max number of times
    Then I am shown password dialog box
    When I successfully enter the correct password
    Then I am shown payment & payee have been added successfully
    Examples:
      | TouchID |
      | FaceID  |

  #PJO-6734,6735
  Scenario Outline: TC_005_ Exception Success: User falls back to password amending standing order successfully (standing order)
    Given I am an enrolled user logged into the app
    And I am on payments hub
    And I have chose to add a new payee
    And I have entered my payment details
    And I have selected standing orders
    And I select Continue on review screen
    When I fail to authenticate with <Biometric> the max number of times
    Then I am shown password dialog box
    When I successfully enter the correct password
    Then I am shown payment & payee have been added successfully
    Examples:
      | TouchID |
      | FaceID  |

  #PJO-6732,6733
  Scenario Outline: TC_006_ Exception Success: User falls back to password when amending standing order successfully (standing order payment)
    Given I am an enrolled user logged into the app
    And I am on the scheduled payments hub
    And I have chosen to amend a standing order
    And I have edited my payment details
    And I select Continue on review screen
    When I fail to authenticate with <Biometric> the max number of times
    Then I am shown password dialog box
    When I successfully enter the correct password
    Then I am shown standing order amended successfully
    Examples:
      | TouchID |
      | FaceID  |
