@team-payments @nga3 @rnga @sw-save-the-change-cwa-on @sw-save-the-change-native-on @genericAccount
Feature: Save the Change Native journey
  If I hold eligible accounts for Save the Change
  As a RNGA authenticated customer
  I should be able to register for it natively

  #TODO: PAYMOB-2381 is raised for updating the Ids.
  #PAYMOB-2209, 2319
  @primary @stub-stc-no-registered
  Scenario: TC01_User Successfully Registers for Save the Change
    Given I am a current user on the home page
    When I navigate to current account on home page
    And I select the "save the change" option in the action menu of current account
    And I have not set up Save the Change before on the eligible account
    And I am on Save the Change Homepage with the ability to swipe through all 4 instructional pages
    And I select the option Set up Save the Change
    And I select an eligible recipient saving account
    And I accept terms and conditions and select Continue on the review screen
    Then I am shown a Success screen
    And I have the option to go back to accounts

  @primary @stub-enrolled-valid
  Scenario: TC02_User already registered for Save the Change
    Given I am a current user on the home page
    When I navigate to islamicCurrent account on home page
    And I select the "save the change" option in the action menu of islamicCurrent account
    And I have set up Save the Change before on the eligible account
    And I select turn OFF save the change option
    And I choose to OK option on winback dialogue
    Then I should see the save the change turn OFF success screen
    And I have the option to go back to accounts

  #TODO: iOS tag will be removed once feature implemented in Android
  @stub-stc-no-savings @ios
  Scenario: TC03_User has no available recipient accounts
    Given I am a stcCurrentOnly user on the home page
    When I navigate to stcCurrentOnly account on home page
    And I select the "save the change" option in the action menu of stcCurrentOnly account
    And I am on Save the Change Homepage with the ability to swipe through all 4 instructional pages
    And I select the option Set up Save the Change
    Then I am shown the option to Open a new Saving account
    When I select Open New Saving account
    Then I should be on the product hub page

  Scenario: TC04_User Unable to set up Save the change
    Given I hold an eligible account for Save the Change
    And I have not set up Save the Change before on the eligible account
    And I select Save the Change in the action menu for the eligible account
    And I am shown Save the Change Homepage with the ability to swipe through all 4 instructional pages
    And Select the option Set up Save the Change
    And I select an eligible recipient saving account
    When I select Continue on the review screen
    Then I am shown a red banner error that Save the Change cannot be set up
