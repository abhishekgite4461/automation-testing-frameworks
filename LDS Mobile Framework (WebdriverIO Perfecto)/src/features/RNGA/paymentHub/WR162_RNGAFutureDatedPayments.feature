@team-payments @nga3 @rnga @stub-enrolled-valid @ff-share-payment-receipt-on
Feature: In order to pay a recipient on future date
  As a RNGA authenticated user
  I want to be able to select date when the payment should go out on

  Background:
    Given I am a valid payment user on the home page
    And I am on the payment hub page

  # MOB3-1511, 9894
  @primary
  Scenario Outline: TC_001 Ability to select date for future dated payment calendar view
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter amount in the amount field as 1.00
    When I select calendar picker option for future date payment
    Then I should see a calendar view
    And I should be allowed to select a payment date up to 31 days from current day
    And other dates must not be able to be selected
    Examples:
      | fromAccount    | toAccount                        |
      | islamicCurrent | uKBank                           |
      | islamicCurrent | creditCardWithCreditTransactions |

  # MOB3-1511, 9894
  Scenario Outline: TC_002 Ability to navigate to next  month
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter amount in the amount field as 1.00
    When I select calendar picker option for future date payment
    Then I should see a calendar view
    When I select next month chevron from the top header of calendar view
    Then I should be able to navigate to next month calendar
    Examples:
      | fromAccount    | toAccount |
      | islamicCurrent | uKBank    |

  # MOB3-1511, 9894
  Scenario Outline: TC_003 Ability to navigate to previous month
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter amount in the amount field as 1.00
    And I select calendar picker option for future date payment
    And I should see a calendar view
    And I have the dates where I can pay in the previous month
    When I select previous month chevron from the top header of calendar view
    Then I should be able to navigate to previous month calendar
    Examples:
      | fromAccount    | toAccount |
      | islamicCurrent | uKBank    |

  # MOB3-1511, 9894
  Scenario Outline: TC_004 User select date from calendar picker option
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter amount in the amount field as 1.00
    When I select calendar picker option for future date payment
    Then I should see a calendar view
    When I select the date as <days> from today's date from calendar view
    Then the date field should be populated with the selected date on the payment hub
    Examples:
      | fromAccount    | toAccount | days |
      | islamicCurrent | uKBank    | 2    |

  # MOB3-1511, 9894
  @secondary
  Scenario Outline: TC_005 User select tomorrow date from calendar picker option
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter amount in the amount field as 1.00
    And I select calendar picker option for future date payment
    When I select the date as <days> from today's date from calendar view
    Then the future dated payment should display Tomorrow in text
    Examples:
      | fromAccount    | toAccount                        | days |
      | islamicCurrent | creditCardWithCreditTransactions | 1    |

  # MOB3-7232, 9897
  @primary
  Scenario Outline: TC_006 Display 'When' field on review details page
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter amount in the amount field as 1.00
    And I select calendar picker option for future date payment
    And I select the date as <days> from today's date from calendar view
    When I select Continue button on payment hub page
    Then I should see the review payment page
    And I am shown 'When' the payment will be made on payment review page
    And I am shown information of payment date if it falls on a holiday
    Examples:
      | fromAccount    | toAccount                        | days |
      | islamicCurrent | uKBank                           | 2    |
      | islamicCurrent | creditCardWithCreditTransactions | 2    |

  # MOB3-9617, 9898
  @primary
  Scenario Outline: TC_007 Display 'When' field on payment confirmed page
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I enter amount in the amount field as 1.00
    And I select calendar picker option for future date payment
    And I submit future date payment with date as 2 days after today
    When I select the confirm option in the review payment page
    Then I should see the payment successful message page
    And I am shown 'When' the payment will be made on payment success page
    And I should see share receipt option on the payment success page
    Examples:
      | fromAccount    | toAccount                        |
      | islamicCurrent | uKBank                           |
      | islamicCurrent | creditCardWithCreditTransactions |
