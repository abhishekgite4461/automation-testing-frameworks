@team-payments @nga3 @rnga
Feature: User want to make a payment to their Credit Card

  In order to pay my LBG credit card
  As a Retail NGA user
  I want to be able to pay my credit

  #MOB3-1708
  @stub-enrolled-typical
  Scenario: TC_001: Verify the screen details when User initiate Pay a Credit Card journey from the Actions menu
    Given I am an enrolled "credit card" user logged into the app
    And I obtain the default primary account
    When I navigate to creditCard account on home page
    And I select the "pay a credit card" option in the action menu of creditCard account
    Then I should see the from field pre-populated with defaultPrimary account
    And I should see the to field pre-populated with creditCard account

  #MOB3-1708
  @stub-enrolled-typical
  Scenario: TC_002: Verify the screen details when User initiate Pay a Credit Card journey
    Given I am an enrolled "credit card" user logged into the app
    When I am on the payment hub - pay a credit card page
    Then I should see amount and payment date in the payment hub - pay a credit card screen
    #And I should see bill due date field in the payment hub - pay a credit card screen

  #MOB3-1708
  @stub-enrolled-typical
  Scenario: TC_003: Ability to select pre-populated amount options to pay a Credit Card
    Given I am an enrolled "credit card" user logged into the app
    And I am on the payment hub - pay a credit card page
    When I select an additional option for pay a credit card in the payment hub page
    Then I should see statement balance and minimum amount option in the payment hub - pay a credit card screen

  #MOB3-1708
  @stub-enrolled-typical
  Scenario: TC_004: Show help text when user enter amount on Transfer & Payment screen
    Given I am an enrolled "credit card" user logged into the app
    And I am on the payment hub - pay a credit card page
    When I select the payment amount field in the payment hub page
    Then I should see the help text for amount field in the payment hub page

  #MOB3-1708
  @stub-enrolled-typical
  Scenario: TC_005: Disable continue button when all the mandatory fields are completed on the Payment hub - pay a credit card screen
    Given I am an enrolled "credit card" user logged into the app
    When I am on the payment hub - pay a credit card page
    Then I should see the continue button disabled in the payment hub page

  #MOB3-1708
  @stub-enrolled-typical
  Scenario: TC_006: No winback when user choose home/back option from Native app header after entering all the mandatory fields on the screen
    Given I am an enrolled "credit card" user logged into the app
    And I am on the payment hub - pay a credit card page
    And I enter 1.00 in the amount field in the payment hub page
    And I select dismiss modal using close button
    Then I should be on the home page

  #MOB3-1708
  @environmentOnly
  Scenario: TC_007: Amount field entered is greater than the Internet transaction limit(MG-454)
    Given I am an enrolled "internet transaction limit for pay a credit card" user logged into the app
    And I am on the payment hub page
    And I select the to account as internetTransactionLimitCreditCard in the payment hub page
    When I submit the pay a credit card with all the mandatory fields with amount value 10005.00
    Then I should see internet transaction limit exceeded error message in payment hub page
    And I should see the values pre-populated in the payment hub page for transfer with amount value 10005.00

  #MOB3-1708
  @stub-enrolled-typical
  Scenario: TC_008: Check Amount field entered is greater than the available balance of the remitting account if Payment request is set to today's date i.e. immediate payment (HC-035)
    Given I am an enrolled "credit card" user logged into the app
    And I am on the payment hub - pay a credit card page
    When I enter amount greater than available balance in the remitter account in the payment hub page
    Then I should be able to close the insufficient money in your account error message in the payment hub page

  #MOB3-1719
  @stub-enrolled-typical
  Scenario: TC_009: Display "Confirm Payment Details" screen when all the Pay a credit card details entered are correct and within transaction limits
    Given I am an enrolled "credit card" user logged into the app
    And I am on the payment hub - pay a credit card page
    When I submit the pay a credit card with all the mandatory fields with corresponding amount value 1.00
    Then I should see the corresponding details in the review payment page

  #MOB3-1719
  @stub-enrolled-typical
  Scenario: TC_010: User select edit option from Confirm Payment screen
    Given I am an enrolled "credit card" user logged into the app
    And I am on the payment hub - pay a credit card page
    And I submit the pay a credit card with all the mandatory fields with corresponding amount value 1.00
    When I select the edit option in the review payment page
    Then I should see the values pre-populated in the payment hub page for payment with amount value 1.00

  #MOB3-6884
  @manual
  Scenario: TC_011: System should step down the authentication mechanism as user device is enrolled and use transaction signing to authenticate the transaction
    Given I am an enrolled "credit card" user logged into the app
    And I am on the payment hub - pay a credit card page
    And I am on Confirm Payment Details screen for pay a credit card
    When I select an option to confirm the payment request
    Then I should be authenticated using transaction signing process
    And I should not be asked to enter my password

  #MOB3-1720
  @stub-enrolled-typical @primary
  Scenario: TC_012: Display Payment success screen for immediate(same day) payment when Payment request is successful
    Given I am an enrolled "credit card" user logged into the app
    And I am on the payment hub - pay a credit card page
    And I submit the pay a credit card with all the mandatory fields with corresponding amount value 1.00
    When I select the confirm option in the review payment page
    Then I should see a payment confirmed screen

  #MOB3-1720
  @stub-enrolled-typical @primary
  Scenario: TC_013: Ability to view transaction post payment request
    Given I am an enrolled "credit card" user logged into the app
    And I am on the payment hub - pay a credit card page
    And I submit the pay a credit card with all the mandatory fields with corresponding amount value 1.00
    And I select the confirm option in the review payment page
    When I select the view transaction option in the payment success page
    Then I should see the - £1.00 in the current month statement of the remitting account

  #MOB3-1720
  @stub-enrolled-typical
  Scenario: TC_015: Ability to Make another Payment request from Payment success screen
    Given I am an enrolled "credit card" user logged into the app
    And I obtain the default primary account
    And I am on the payment hub - pay a credit card page
    And I submit the pay a credit card with all the mandatory fields with corresponding amount value 1.00
    And I select the confirm option in the review payment page
    When I select the make another payment option in the payment success page
    Then I should be navigated to the payment hub page with from account as defaultPrimary in the payment hub page

  # TODO: Android tag should be removed once MQE-836 is fixed
  @stubOnly @stub-payment-confirm-lost-internet @android
  Scenario: TC_016: Show an internet connectivity error message
    Given I am an enrolled "credit card" user logged into the app
    And I am on the payment hub - pay a internet lost connectivity credit card page
    And I submit the pay a credit card with all the mandatory fields with amount value 1
    When I select the confirm option in the review payment page
    Then I should see the internet connection lost error in payment failure page
    And I should see the transfer and payment option in the payment failure page

  #MOB3-1720
  @stub-enrolled-typical
  Scenario: TC_017: choose header home option in the Success Payment page
    Given I am an enrolled "credit card" user logged into the app
    And I am on the payment hub - pay a credit card page
    And I submit the pay a credit card with all the mandatory fields with corresponding amount value 1.00
    And I select the confirm option in the review payment page
    When I navigate to the home page from payment success page
    Then I should be on the home page

  # MOB3-1720
  @android @stub-enrolled-typical
  Scenario: TC_018: choose android hard code back button in the Success Payment page
    Given I am an enrolled "credit card" user logged into the app
    And I am on the payment hub - pay a credit card page
    And I submit the pay a credit card with all the mandatory fields with corresponding amount value 1.00
    And I select the confirm option in the review payment page
    When I select the hard core back button in the payment success page
    Then I should be on the success payment page

  @stub-payment-declined-scoringevent
  Scenario: TC_019: Display payment declined error message for pay a credit card
    Given I am an enrolled "declined payment" user logged into the app
    And I am on the payment hub - pay a declined credit card page
    And I submit the pay a credit card with all the mandatory fields with corresponding amount value 22
    When I select the confirm option in the review payment page
    Then I should see the declined error message in the payment decline page
    And I should see pre auth and back to logon in the payment decline page

  @stub-payment-onhold
  Scenario: TC_020: Display payment on hold error message for pay a credit card
    Given I am an enrolled "payment on hold" user logged into the app
    And I am on the payment hub - pay a payment on hold card page
    And I submit the pay a credit card with all the mandatory fields with corresponding amount value 12
    When I select the confirm option in the review payment page
    Then I should see the payment hold error message in the payment on hold page
    And I should see transfer and payment button in the payment on hold page

  @manual
  Scenario Outline: TC_021: Transaction monitoring using CCTM fraud monitoring system for pay a credit card
    Given I am an enrolled "credit card" user logged into the app
    And I am on the payment hub - pay a "<type>" card page
    And I submit the pay a credit card with all the mandatory fields with corresponding amount value "<amount>"
    When I select the confirm option in the review payment page
    Then I should verify the FS logs for sub-channel, app alias name and auth code(900 for light login and 300 for touch ID)
    Examples:
      |type            |amount|
      |declined payment|22    |
      |on hold payment |12    |

  @stub-payment-pending-creditcard
  Scenario: TC_022: User is trying to pay a credit but there is already a future dated payment set against that card
    Given I am an enrolled "credit card" user logged into the app
    And I am on the payment hub - pay a pending payment card page
    And I submit the pay a credit card with all the mandatory fields with amount value 1
    When I select the confirm option in the review payment page
    Then I should see the pending payment error in payment failure page
    And I should see the transfer and payment option in the payment failure page

  # the script will fail for ios alone until MOB3-8914 is fixed
  @environmentOnly
  Scenario: TC_023: Amount field entered is greater than the account daily limit
    Given I am an enrolled "account daily limit for pay a credit card" user logged into the app
    And I am on the payment hub page
    And I select the to account as accountDailyLimitCreditCard in the payment hub page
    And I submit the pay a credit card with all the mandatory fields with amount value 560.00
    When I select the confirm option in the review payment page
    Then I should see account daily limit exceeded error message in payment hub page

  #Instalment credit cards are present only for HFX brand currently in the backend
  @stub-enrolled-typical @hfx
  Scenario: TC_024: Display pre-populated amount with instalment payment plan options to pay a instalment Credit Card
    Given I am an enrolled "instalment credit card" user logged into the app
    And I am on the payment hub - pay a instalment credit card page
    When I select an additional option for pay a credit card in the payment hub page
    Then I should see statement balance and minimum amount with instalment plan option in the payment hub - pay a credit card screen
