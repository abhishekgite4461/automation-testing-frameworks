@team-payments @nga3 @rnga @ff-pay-credit-card-by-debit-card-on @mbnaSuite @sw-pccdc-on
Feature: Pay credit card by debit card
  In order to pay my credit card
  As a RNGA customer
  I want to be use a debit card from any bank to pay my LBG credit card

  # TODO:3471 Webview
  @stub-enrolled-typical @manual @primary
  Scenario: TC_001_ Main Success: Prompt user to enter their Debit Card details
    Given I am a credit card and current account user on the home page
    And I am on the payment hub page
    And I choose the from account as debitCard and to account as creditCard in the payment hub page
    And I am not shown payment date in the payment hub
    And I submit the pccdc with all the mandatory fields with corresponding amount value 2.00
    And I am displayed a warning message
    When I choose to continue on the warning message
    Then I am taken out of the app with the PCCDC journey continuing in the devices native web browser application

  @stub-enrolled-typical
  Scenario: TC_002_ Main Success: Display message that user is leaving the app to complete journey
    Given I am a credit card and current account user on the home page
    And I am on the payment hub page
    And I choose the from account as debitCard and to account as creditCard in the payment hub page
    And I am not shown payment date in the payment hub
    When I submit the pccdc with all the mandatory fields with corresponding amount value 2.00
    Then I am displayed a warning message
    When I choose to cancel on the warning message
    Then I am on the payment hub page with data prepopulated

  # TODO:COSE-769 Requested the specific stub
  @stub-enrolled-typical @primary @pending
  Scenario: TC_003_Exception: User does not hold a Credit Card
    Given I am a no credit card account user on the home page
    And I am on the payment hub page
    When I navigate to the remitter account on the payment hub page
    Then I am not shown the option to pay using debit card

  @stub-credit-card-only
  Scenario: TC_004_ User does not hold other eligible remitting accounts
    Given I am a credit card only user on the home page
    When I am on the payment hub page
    Then I should see the from field pre-populated with debitCard account
    And I should see the to field pre-populated with creditCard account
    And I am not shown payment date in the payment hub

  @manual
  Scenario: TC_005_ Display Homepage on navigation back to app
    Given I am a credit card user on the home page
    And I am on the payment hub page
    And I choose the from account as debitCard and to account as creditCard in the payment hub page
    And I submit the pccdc with all the mandatory fields with corresponding amount value 2.00
    And I am displayed a warning message
    When I select leave from payment hub page
    Then I am taken out of the app with the PCCDC journey continuing in the devices native web browser application
    When I switch back to the banking app
    Then I should be on the home page

  @manual
  Scenario: TC_006_ Display Homepage on navigation back to app
    Given I am a credit card user on the home page
    And I am on the payment hub page
    And I choose the from account as debitCard and to account as creditCard in the payment hub page
    And I submit the pccdc with all the mandatory fields with corresponding amount value 2.00
    And I am displayed a warning message
    When I select leave from payment hub page
    #web journey
    Then I am taken out of the app with the PCCDC journey continuing in the devices native web browser application
    And I have met my auto logoff time limit
    When I switch back to the banking app
    Then I should be logged off

  @stub-pccdc-error @manual
  Scenario Outline: TC_007_ Exception: Fails to retrieve URL (MG- )
    Given I am an enrolled "credit card" user logged into the app
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    When I submit the pccdc with all the mandatory fields with corresponding amount value <Amount>
    Then I should be shown an error message because of timeout or other unexpected technical issue
    And I am not shown the Warning message about leaving the app
    Examples:
      | fromAccount | Amount | toAccount     |
      | pccdc       | 2      | creditCard    |

  @stub-enrolled-valid @primary
  Scenario Outline: TC_008_ Exception: Unable to pay more than balance
    Given I am a credit card and current account user on the home page
    And I am on the payment hub page
    And I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    When I enter amount greater than the balance of the credit card account in the payment hub page
    Then I should see credit card payment validation error message
    Examples:
      | fromAccount | toAccount     |
      | debitCard   | creditCard    |
      | isaSaver    | creditCard    |
