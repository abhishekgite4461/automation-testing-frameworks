@team-payments @nga3 @rnga @stub-enrolled-valid
Feature: In order to move money to them, delete them or view pending payments
  As an RNGA user
  I want to be able to perform multiple actions on an existing recipient

  Background:
    Given I am an enrolled "valid" user logged into the app
    And I navigate to the remitter account on the payment hub page
    And I select remitting AccountWithExternalBeneficiary Account from the account list
    And I select the recipient account on the payment hub page

  @primary
  # MOB3-9153, 1632
  Scenario:TC_001:Manage Recipient Options
    When I select manage on external beneficiary
    Then I can see the Recipients name
    And I have the option to pay the recipient
    And I have the option to delete the recipient
    And the option to cancel manage Recipient

  # MOB3-9153, 1632
  Scenario:TC_002:Selecting Cancel
    When  I select manage on external beneficiary
    And  I cancel Manage on an external beneficiary
    Then I am taken back to the recipient list

  @primary
   # MOB3-9153, 1632
  Scenario:TC_003: Recipient with a Pending Payment
    When I select manage on an pending payment account
    Then I am given the option to view the pending payments for the account
    And the pay recipient option is disabled
    And the delete recipient is disabled

  @secondary
   # MOB3-9546, 1620, 1612
  Scenario Outline:TC_004: User deletes recipient
    Given I am on manage actions for a <type>
    When I select to delete a recipient
    Then I am asked for confirmation if I want to delete the recipient
    Examples:
      | type            |
      | ukMobile        |
      | ukBankRecipient |

  @primary
   # MOB3-9546, 1620, 1612, 9620
  Scenario Outline:TC_005: User keeps recipient
    Given I am on manage actions for a <type>
    And I select to delete a recipient
    When I select to keep the recipient
    Then I am taken back to the recipient list
    And  the recipient is still available
    Examples:
      | type            |
      | ukMobile        |
      | ukBankRecipient |

  @primary @environmentOnly
   # MOB3-9546, 1620, 1612, 9620
  Scenario Outline:TC_006:User confirms deletion of recipient
    Given that I want to delete recipient <sortCode> <accountNumber> <recipientName>
    When I select remittingAccountWithExternalBeneficiary Account from the account list
    And I select the recipient account on the payment hub page
    And I select to delete added recipient
    Then I am asked for confirmation if I want to delete the recipient

    When I select to confirm delete a recipient
    Then the recipient is removed from the recipient list
    Examples: Valid entries
      | sortCode | accountNumber | recipientName |
      | 402527   | 52561158      | test          |

  @secondary @pending @environmentOnly
   # MOB3-1612, 9620
  Scenario Outline:TC_007:Deleting user with a standing order
    Given I am on manage actions for a <type>
    When I select to delete a recipient
    And I am asked for confirmation if I want to delete the recipient
    And I select to confirm delete a recipient
    Then I am should be shown a message saying recipient could not be deleted due to standing order
    And the option to View Standing Orders
    And the option to Close
    Examples:
      | type            |
      | ukBankRecipient |

  @pending @environmentOnly
  # MOB3-1612, 9620
  Scenario Outline:TC_008 Dismiss deleting user with a standing order set up message
    Given I am on manage actions for a <type>
    When I select to delete a recipient
    And I am asked for confirmation if I want to delete the recipient
    And I select to confirm delete a recipient
    Then I am should be shown a message saying recipient could not be deleted due to standing order
    When I close the message
    Then I am taken back to the recipient list
    Examples:
      | type            |
      | ukBankRecipient |

  @pending @environmentOnly
   # MOB3-1612, 9620
  Scenario Outline:TC_009 View Standing order
    Given I am on manage actions for a <type>
    When I select to delete a recipient
    And I am asked for confirmation if I want to delete the recipient
    And I select to confirm delete a recipient
    Then I am should be shown a message saying recipient could not be deleted due to standing order
    When I select view message
    Then I am taken to the standing order page
    Examples:
      | type            |
      | ukBankRecipient |
