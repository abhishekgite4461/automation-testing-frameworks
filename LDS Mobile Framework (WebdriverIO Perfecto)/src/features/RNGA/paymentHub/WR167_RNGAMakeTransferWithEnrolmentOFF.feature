@team-payments @sw-enrolment-off @sw-light-logon-off @rnga @nga3 @stub-valid @stubOnly
Feature: Make Transfer between non-ISA account with device not already enrolled and enrolment switch is OFF

  As a Retail NGA user
  I want to transfer money to one of my own accounts
  So that I can optimise my financial position across my accounts
  Background:
    Given I am a current user
    And I login with my username and password
    And I enter correct MI
    And I am on the payment hub page

  Scenario Outline: TC_001: User should not see standing order option in payment hub page if device is not enrolled for transfer
    When I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    Then I should see amount field in the payment hub page
    And I should not see standing order toggle option in the payment hub page
    Examples:
      |fromAccount|toAccount|
      |saving     |current  |
