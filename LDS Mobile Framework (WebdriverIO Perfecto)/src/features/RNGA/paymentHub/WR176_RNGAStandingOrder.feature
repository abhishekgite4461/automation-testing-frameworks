@team-payments @nga3 @rnga @ff-share-payment-receipt-on
Feature: In order to move money to a recipient
  As a RNGA authenticated user
  I want to be able to set up a standing order

  Background:
    Given I am a valid payment user on the home page
    And I am on the payment hub page

  # MOB3-10016, 1541, PAYMOB-1174
  # TODO MOB3-11756 Yearly frequency selection needs to be added in SO
  @primary
  Scenario Outline: TC_001 Main Success Scenario: Creation of a Successful Standing Order for internal accounts
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order for <toAccount>
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I provide Reference for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    And I enter a correct password
    And I should see a Standing Order set up success message
    When I select standing order option on standing order success page
    Then I should be on the standing orders webview page

    Examples:
      | fromAccount | Amount | toAccount | frequency        |
      | current     | 2      | saving    | Weekly           |
      | current     | 2      | saving    | Every four weeks |
      | current     | 2      | isa       | Monthly          |
      | current     | 2      | saving    | Every two months |
      | current     | 2      | saving    | Quarterly        |
      | current     | 2      | saving    | Half yearly      |
      | current     | 2      | saving    | Weekly           |

  # MOB3- 10016, 1541
  @primary @stub-enrolled-valid
  Scenario Outline: TC_002 Main Success Scenario: Creation of a Successful Standing Order for external accounts
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see a Standing Order set up success message
    And I should see share receipt option on the standing order success page

    Examples:
      | fromAccount | Amount | toAccount | frequency        |
      | current     | 2      | uKBank    | Every two months |
      | current     | 2      | uKBank    | Half yearly      |

  # MOB3-10441, 9227
  @secondary @stub-enrolled-valid
  Scenario Outline: TC_003 ISA warning message creating an Standing Order
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    When Customer wants to make the payment a standing order
    Then I should be shown a warning message stating my remaining allowance for <toAccount>

    Examples:
      | fromAccount | toAccount      |
      | current     | isa            |
      | current     | htbIsa         |
      | current     | instantCashIsa |

  # MOB3-10015, 1540
  @stub-enrolled-valid
  Scenario Outline: TC_004 User selects Edit option from Review Standing Order screen
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I provide Reference for a standing order
    And I select Edit option on standing order from review
    Then I should be navigated back to previous screen with the data pre-populated

    Examples:
      | fromAccount | Amount | toAccount | frequency |
      | current     | 2      | saving    | Weekly    |

  # MOB3-10015, 1540
  @stub-enrolled-valid
  Scenario Outline: TC_005 NGA header back button navigation
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I provide Reference for a standing order
    And I select NGA header back button
    Then I should be navigated back to previous screen with the data pre-populated

    Examples:
      | fromAccount | Amount | toAccount | frequency |
      | current     | 2      | saving    | Weekly    |

  # MOB3-10013, 1642
  @primary @stub-enrolled-valid
  Scenario Outline: TC_006 Business Rule Validation - User can select payment date up to 31 days from today
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order
    And I choose to select the Start date for Standing Order
    Then I should be allowed to select a payment date up to 31 days after the current date

    Examples:
      | fromAccount | Amount | toAccount |
      | current     | 2      | saving    |

  # MOB3-10013, 1643
  # Unable to set year in date picker
  @manual
  Scenario Outline: TC_007 New Functionality Validation - Month picker disabled from Calendar module when selecting end date
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I choose to select the End date for Standing Order
    And I select the year drop down from the Calendar module
    Then I should be able to select year up to 2063 for the selected <frequency>

    Examples:
      | fromAccount | Amount | toAccount | frequency |
      | current     | 2      | saving    | Yearly    |

  # MOB3-10014, 1643
  @stub-enrolled-valid
  Scenario Outline: TC_008 New Functionality Validation - Month and year picker enabled from Calendar module when selecting end date
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I choose to select the End date for Standing Order
    And I select the year drop down from the Calendar module
    Then the Year picker should be enabled
    And the Month picker should be enabled
    And I should be able to select month and year up to 2063 for the selected <frequency>

    Examples:
      | fromAccount | Amount | toAccount | frequency |
      | current     | 2      | saving    | Weekly    |

  @stub-so-declined @secondary
  Scenario Outline: Standing Order Declined for internal accounts
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I provide Reference for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see the standing order decline page
    And I should see the option to exit Internet Banking
    Examples:
      | fromAccount | Amount | toAccount | frequency        |
      | current     | 25     | saving    | Weekly           |

  @stub-so-onhold @secondary
  Scenario Outline: Standing Order Under Review for internal accounts
    Given I choose the from account as <fromAccount> and to account as <toAccount> in the payment hub page
    And I have chosen to make this a Standing Order option
    When I provide <Amount> for a standing order for <toAccount>
    And I choose standing order <frequency>
    And I select the Start Date for standing order
    And I select the End Date for selected <frequency> for a standing order
    And I provide Reference for a standing order
    And I review the Standing Order
    Then I should be prompted to enter my password
    When I enter a correct password
    Then I should see the standing order under review page
    And I should see standing order option on standing order under review page
    Examples:
      | fromAccount | Amount | toAccount | frequency        |
      | current     | 15    | saving    | Weekly           |
