@team-navigation @nga3 @rnga @sw-push-notification-on @ff-Push-Notifications-DeepLink-Enabled-on @manual

# REASONS FOR MANUAL EXECUTION:
# Need an integration with external tools like postman which will trigger the push notification

#MCC-2502, MCC-2499
Feature: Deep Linked Push notifications
  In order to enable push notifications with deep linking to payments hub
  As an RNGA customer opted for push notification's
  I want to be redirected to payments hub while interacting with deep linked push notification

  @primary
  Scenario: TC_001 App is logged out customer interacts with deep link push
    Given the app is logged out
    And a credit card push notification containing a deep link has been received by my device
    When I interact with this push notification
    And I should be taken to the login page
    And I successfully log in
    Then I should be taken to the payments hub page

  @secondary
  Scenario: TC_002 App is timed out (either in background or foreground)- customer interacts with deep link push
    Given the app has timed out
    And a credit card push notification containing a deep link has been received by my device
    When I interact with this push notification
    And I am taken to the login page
    And I successfully log in
    Then I should be taken to the payments hub

  #MCC-2506, MCC-2497
  @primary
  Scenario: TC_003 customer interacts with deep link push and payment made successfully
    Given I am on payments hub
    When I successfully pay my credit card
    Then I should be returned to either the homepage or another page within the app, according to Payments hub journey rules

  @secondary
  Scenario: TC_004 customer interacts with deep link push and payment is not successful
    Given I am on payments hub
    When my credit card payment fails
    Then I should be returned to either the homepage or another page within the app, according to Payments hub journey rules

  #MCC-2494, MCC-2501
  @secondary
  Scenario: TC_005 App is active and in foreground- customer interacts with deep link push
    Given the app is logged in
    And a credit card push notification containing a deep link has been received by my device
    When I interact with this push notification
    Then I should be taken to the payments hub

  #MCC-2503, MCC-2500
  @secondary
  Scenario: TC_006 App is in the background but still logged in and customer interacts with deep link push
    Given the app is in the background
    And I am logged in
    And a credit card push notification containing a deep link has been received by my device
    When I interact with this push notification
    Then I should be taken to the payments hub

  @secondary
  Scenario: TC_007 App is in the background and logged out and customer interacts with deep link push
    Given the app is in the background
    And I am logged out
    And a credit card push notification containing a deep link has been received by my device
    When I interact with this push notification
    And I should be taken to the login page
    And I successfully log in
    Then I should be taken to the payments hub

  #MCC-2540
  @secondary
  Scenario: TC_008 customer interacts with deep link push while in login interstitial page
    Given the app is in the foreground
    And I have successfully logged in
    And I am looking at an login interstitial
    And a credit card push notification containing a deep link has been received by my device
    When I interact with this push notification
    And I should be taken to the payments hub
    And I finish my payments journey
    Then I should see the interstitial again

  @secondary
  Scenario: TC_009 customer interacts with deep link push while customer is expected to see an interstitial page
    Given I am not logged in to the app
    And I am expecting to see an interstitial next time I log in
    And a credit card push notification containing a deep link has been received by my device
    When I interact with this push notification
    And I successfully log in
    And I should be taken to the payments hub
    Then not see the interstitial this time around
