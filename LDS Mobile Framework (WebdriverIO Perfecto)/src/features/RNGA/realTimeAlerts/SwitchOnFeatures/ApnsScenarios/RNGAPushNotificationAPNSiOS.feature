@team-navigation @nga3 @rnga @stub-enrolled-typical @optFeature @ff-mdm-push-notifications-setting-enabled-on  @secondary
@sw-push-notification-on @appiumOnly @exhaustiveData

  #  MCC- 1546 MCC- 1545
Feature: Apple Push Notification Service(APNS)
  In order to receive push on the devices of my choosing
  As an RNGA customer
  I want to manage which of my devices are enabled for push

  Scenario: T_001 Customer’s current device is not push enabled (has never displayed the ‘Don’t allow’ on native overlay)- successfully enabled
    Given I am an enrolled "current" user logged into the app
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the notifications page
    When I select to enable notifications
    And I allow notifications for my device
    And I should now be able to interact with the alert toggles
    And The alert toggles should be shown in the correct position according to my current status
    And I navigate to logoff via more menu
    Then I should be on the logout page

  Scenario: T_002 Customer’s current device is not push enabled (never select ‘Don’t allow’ on native overlay) chooses to enable notifications, customer denies push permission
    Given I am an enrolled "current" user logged into the app
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the notifications page
    And I don’t allow notifications for my device
    And I should not be able to interact with the alert toggles
    When I should see instructions on how to enable notifications
    Then The alert toggles should be shown in the correct position according to my current status

  Scenario: T_003 Customer’s current device is not push enabled (has previous selected ‘Don’t allow’ on native overlay or has previously select accept but has since turned push off at device level) chooses to enable notifications
    Given I am an enrolled "current" user logged into the app
    When I have turned off device push
    And I relaunch the app and login as a "current" user
    Then I navigate to settings page from home page
    When I select the real time alerts option from the Settings page
    Then I should be on the notifications page
    When I can see an option to enable notifications
    And I select Go to Device Settings Button
    Then I should be taken to the OS device settings
    When I do not turn on push at device settings level
    And I navigate back to the app
    Then I should see overlay advising that I did not complete the correct actions
    And I should not be able to interact with the alert toggles
    When I should see instructions on how to enable notifications
    Then The alert toggles should be shown in the correct position according to my current status
