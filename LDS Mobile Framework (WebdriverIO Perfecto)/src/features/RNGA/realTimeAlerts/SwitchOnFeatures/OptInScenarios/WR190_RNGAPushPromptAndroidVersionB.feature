@team-navigation @nga3 @rnga @environmentOnly @currentAccount @sw-push-notification-on
@appiumOnly

# REASONS FOR APPIUM EXECUTION :
# need to change the device date for 3 times (each ranging 14 , 21 and 21 days respectively)
# most of the times app launch fails as the date range for testing exceeds the app certificate expiry date
# so need an afterscript to put the date back to default in perfecto devices
# if the afterscript fails , then the entire consecutive test cases will fail

# MCC-598, MCC-354
Feature: Push prompt at login (0-14 days)
  In order to indicate my preference for receiving push notifications
  As an RNGA customer
  I want to be prompted to opt in/out of receiving push notifications after login

  Preconditions for Push Prompt Version B
  Customer is fully enrolled
  Customer has never opted in for push notifications
  Touch ID / Face ID is shown
  Data Consent screen is shown
  Enable Push Notification - Android switch is OFF
  Opt-in Version A Control-Android is ON
  Customer logs into the app 14 days after the Data Consent Screen is shown (Customer can get the push prompt
  screen on any random day in between 0-14 days post GDPR screen)

  Background:
    Given I am a current user
    When I enter correct MI
    And I navigate to logoff via more menu
    Then I should be on the logout page

  @primary
  Scenario: TC_001 Happy path Customer is shown the Push prompt screen for first or second time, chooses to opt in
    When I login again after 14 days from today's date
    Given I am a current user
    When I enter correct MI
    Then I see the push prompt B screen
    When I choose to navigate through three push prompt screens
    And I chose Not now
    Then  I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And I should be opted out of Account alerts
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I login again after 35 days from today's date
    Given I am a current user
    When I enter correct MI
    Then I see the push prompt B screen
    When I choose to navigate through three push prompt screens
    And  I chose to opt in
    Then I should see a success confirmation
    And I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And I should be opted in for Account alerts

  Scenario: TC_002 Variation Customer is shown Push prompt screen for first or second time, customer chooses not now
    When I login again after 14 days from today's date
    Given I am a current user
    When I enter correct MI
    Then I see the push prompt B screen
    When I choose to navigate through three push prompt screens
    And I chose Not now
    Then  I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And I should be opted out of Account alerts
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I login again after 35 days from today's date
    Given I am a current user
    When I enter correct MI
    Then I see the push prompt B screen
    When I choose to navigate through three push prompt screens
    And  I chose Not now
    Then I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And I should be opted out of Account alerts

  Scenario: TC_003 Variation Customer is shown the Push prompt for final time
    When I login again after 14 days from today's date
    Given I am a current user
    When I enter correct MI
    Then I see the push prompt B screen
    When I choose to navigate through three push prompt screens
    And I chose Not now
    Then  I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And I should be opted out of Account alerts
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I login again after 35 days from today's date
    Given I am a current user
    When I enter correct MI
    Then I see the push prompt B screen
    When I choose to navigate through three push prompt screens
    And  I chose Not now
    Then I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And  I should be opted out of Account alerts
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I login again after 56 days from today's date
    Given I am a current user
    When I enter correct MI
    Then I see the push prompt B screen
    And I choose to navigate through three push prompt screens

  Scenario: TC_004 Variation Customer is shown the Push prompt for final time, customer opts in
    Given I am a current user
    When I enter correct MI
    And I navigate to logoff via more menu
    Then I should be on the logout page
    When I login again after 14 days from today's date
    Given I am a current user
    When I enter correct MI
    Then I see the push prompt B screen
    When I choose to navigate through three push prompt screens
    And I chose Not now
    Then  I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And I should be opted out of Account alerts
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I login again after 35 days from today's date
    Given I am a current user
    When I enter correct MI
    Then I see the push prompt B screen
    When I choose to navigate through three push prompt screens
    And  I chose Not now
    Then I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And  I should be opted out of Account alerts
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I login again after 56 days from today's date
    Given I am a current user
    When I enter correct MI
    Then I see the push prompt B screen
    When I choose to navigate through three push prompt screens
    And  I chose to opt in
    Then I should see a success confirmation
    And I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And I should be opted in for Account alerts

  Scenario: TC_005 Variation Customer is shown the Push prompt for final time, customer chooses no thanks
    When I login again after 14 days from today's date
    Given I am a current user
    When I enter correct MI
    Then I see the push prompt B screen
    When I choose to navigate through three push prompt screens
    And I chose Not now
    Then  I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And I should be opted out of Account alerts
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I login again after 35 days from today's date
    Given I am a current user
    When I enter correct MI
    Then I see the push prompt B screen
    When I choose to navigate through three push prompt screens
    And  I chose Not now
    Then I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And  I should be opted out of Account alerts
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I login again after 56 days from today's date
    Given I am a current user
    When I enter correct MI
    Then I see the push prompt B screen
    And I choose to navigate through three push prompt screens
    When I chose No Thanks option
    Then I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And I should be opted out of Account alerts

  Scenario: TC_006 Exception Customer is not shown the Push prompt as customer is already opted in
    When I login again after 14 days from today's date
    Given I am a current user
    When I enter correct MI
    Then I see the push prompt B screen
    When I choose to navigate through three push prompt screens
    And  I chose to opt in
    Then I should see a success confirmation
    And I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And I should be opted in for Account alerts
    When I login again after 15 days from today's date
    Then I should not see the push prompt screen B
    And I should be on the home page

  Scenario: TC_007 Exception Customer is not shown the Push prompt as customer has previously opted out
    When I login again after 14 days from today's date
    Given I am a current user
    When I enter correct MI
    Then I see the push prompt B screen
    When I choose to navigate through three push prompt screens
    And  I chose to opt in
    Then I should see a success confirmation
    And I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    When I opt out of the Account alerts
    Then I should see a opted out confirmation
    And I should be opted out of Account alerts
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I login again after 35 days from today's date
    Given I am a current user
    When I enter correct MI
    Then I should not see the push prompt screen A
    And I should be on the home page
