@team-navigation @nga3 @rnga @stub-enrolled-typical @android @sw-push-notification-on @stubOnly
# MCC-103, 95
Feature: Usage of real time alerts for preference amendment
  In order to control the notifications I can receive
  As a RNGA authenticated customer
  I want to access and use the Notifications page

  Pre condition:Customer is not currently opted in for Push Notiifcations
  Customer is fully authenticated

  Background:
    Given I am an enrolled "current and savings" user logged into the app
    And I navigate to settings page from home page
    When I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page

  @primary
  Scenario: TC_003 Main Success Scenario for opting in on Notifications page
    When I opt in for Account alerts
    Then I should see a success confirmation
    And I should be opted in for Account alerts
    When I opt out of the Account alerts
    Then I should see a opted out confirmation
    And I should be opted out of Account alerts

  #MCC-420, MCC-419, MCC-166, MCC-165
  @pending
  Scenario: TC_008 No internet connectivity when customer attempts to update Account alerts status on Notifications setting page (HC-028)
    Given I am on the real time notifications page
    And I choose to change my status for Account alerts
    When there is no internet connectivity
    Then I should see a failure banner (HC-028)
    When I dismiss the banner
    Then I should see that the Account alerts status has not changed

  #MCC-420, MCC-419, MCC-166, MCC-165
  @pending
  Scenario: TC_009 Unsuccessful response when customer attempts to update Account alerts status on Notifications setting page (HC-028)
    Given I am on the real time notifications page
    And I choose to update my status for Account alerts
    When my attempt to update my status is unsuccessful
    Then I should see a failure banner (HC-028)
    When I dismiss the banner
    Then I should see that the Account alerts status has not changed
