@team-navigation @nga3 @rnga @ios @sw-push-optin-a-on @appiumOnly @environmentOnly
# MCC-103, 95

Feature: Usage of real time alerts for preference amendment
  In order to control the notifications I can receive
  As a RNGA authenticated customer
  I want to access and use the Notifications page

  Pre condition:Customer is not currently opted in for Push Notiifcations
  Customer is fully authenticated

  @primary
  Scenario: TC_002 Main Success Scenario for opting in on Notifications page
    Given I am an enrolled "current and savings" user logged into the app
    And I navigate to settings page from home page
    When I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    When I opt in for Account alerts
    And I allow notifications for my device
    Then I should see a success confirmation
    And I should be opted in for Account alerts
    When I opt out of the Account alerts
    Then I should see a opted out confirmation
    And I should be opted out of Account alerts

  #Alternative
  Scenario: TC_004 iOS Opting in on Notifications page, user doesn’t confirm
    Given I reinstall the app
    And I am an enrolled "current and savings" user logged into the app
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    When I opt in for Account alerts
    And I don’t allow notifications for my device
    Then I will not be opted in for Account alerts
