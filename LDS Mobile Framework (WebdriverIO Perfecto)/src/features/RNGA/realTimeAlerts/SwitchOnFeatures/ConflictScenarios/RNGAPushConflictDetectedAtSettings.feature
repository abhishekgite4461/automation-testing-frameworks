@team-navigation @nga3 @rnga @stub-enrolled-valid @sw-push-notification-on @android @appiumOnly
# MCC-62 epic

# REASONS FOR APPIUM ONLY EXECUTION:
# Need an afterScript to reset the device settings
# if the afterscript fails , it might affect the execution of all the other consecutive scripts

# REASONS FOR PENDING SCRIPTS :
# Appium gets hung and terminated if we pause the session for 5 mins(for the app to get timed out)
# Error thrown : ERROR: A session is either terminated or not started

Feature: Conflict detection at settings

  In order to indicate my preference for receiving push notifications
  As an RNGA customer
  I want to be informed about a conflict at settings

  Preconditions for Push Prompt Conflict Screen
  Customer is fully enrolled
  Customer has opted in for push notifications
  Touch ID / Face ID is shown
  Data Consent screen is shown
  Enable Push Notification - Android switch is ON
  Opt-in Version A Control-Android is OFF

  Background:
    Given I am a current user
    When I enter correct MI
    Then I navigate to settings page from home page
    When I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    When I opt in for Account alerts
    Then I should see a success confirmation
    When I navigate to logoff via more menu
    Then I should be on the logout page
    Given I have turned off device push
    And I am a current user
    When I enter correct MI
    Then I should be on the login conflict prompt screen
    When I choose to not turn on push in push conflict screen
    Then I should see a confirmation that I have chosen not to activate notifications
    When I select ok on this confirmation that I have chosen not to activate notifications
    Then I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    When I opt in for Account alerts

  Scenario: TC-001 Customer is presented with Conflict pop up chooses Go to device notification and changes to on
    Then I should see the conflict screen pop up
    When I select Go to Device Settings Button
    Then I should be taken to the OS device settings
    When I turn on push at device settings level
    And I navigate back to the app
    And my session is still active
    Then I should be opted in for Account alerts

  Scenario: TC-002 Customer is presented with Conflict popup chooses not now
    Then I should see the conflict screen pop up
    And I select Not Now from Conflict pop up

  Scenario: TC-003 Customer is presented with Conflict pop up chooses Go to device notification and fail to change the app settings
    Then I should see the conflict screen pop up
    When I select Go to Device Settings Button
    Then I do not turn on push at device settings level
    When I navigate back to the app
    And my session is still active
    Then I should see overlay advising that I did not complete the correct actions
    When I select ok on the notifications not activated overlay
    Then I should be opted out of Account alerts

  @pending
  Scenario: TC-004 Session no longer active when customer navigates back into the app
    Then I should see the conflict screen pop up
    When I select Go to Device Settings Button
    Then I should be taken to the OS device settings
    When I navigate back to the app
    And my session is no longer active
    Then I should be on the logout page
