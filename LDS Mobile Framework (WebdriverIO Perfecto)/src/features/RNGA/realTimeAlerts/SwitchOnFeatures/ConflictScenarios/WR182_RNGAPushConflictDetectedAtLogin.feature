@team-navigation @nga3 @rnga @stub-enrolled-valid @sw-push-notification-on @android @appiumOnly
# MCC-62 epic

# REASONS FOR APPIUM ONLY EXECUTION:
# Need an afterScript to reset the device settings
# if the afterscript fails , it might affect the execution of all the other consecutive scripts

# REASONS FOR PENDING SCRIPTS :
# Appium gets hung and terminated if we pause the session for 5 mins(for the app to get timed out)
# Error thrown : ERROR: A session is either terminated or not started

Feature: Conflict detection at login

  In order to indicate my preference for receiving push notifications
  As an RNGA customer
  I want to be informed about a conflict at login

  Preconditions for Push Prompt Conflict Screen
  Customer is fully enrolled
  Customer has opted in for push notifications
  Touch ID / Face ID is shown
  Data Consent screen is shown
  Enable Push Notification - Android switch is ON
  Opt-in Version A Control-Android is OFF

  Background:
    Given I am a current user
    When I enter correct MI
    Then I navigate to settings page from home page
    When I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    When I opt in for Account alerts
    Then I should see a success confirmation
    And I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page
    And I select the desired "1 Minute" from the auto log off page
    And I navigate to logoff via more menu
    And I should be on the logout page
    Given I have turned off device push
    And I am a current user
    When I enter correct MI
    Then I should be on the login conflict prompt screen

  #Main scenarios
  Scenario:TC-001 Main success Push conflict detected, User turns on OS level Push, session active
    When I choose to turn on push in push conflict screen
    Then I should be taken to the OS device settings
    And I turn on push at device settings level
    And my session is still active
    When I navigate back to the app
    Then I should see a success confirmation
    And I should be on the home page

  #Alternative scenarios
  Scenario: TC-002 Alternative Push conflict detected, User chooses to not turn on Push from prompt
    When I choose to not turn on push in push conflict screen
    And I should see a confirmation that I have chosen not to activate notifications
    And I select ok on this confirmation that I have chosen not to activate notifications
    And I should be on the home page

  #Exceptions
  @pending
  Scenario: TC-003 Exceptions Push conflict detected, User turns on OS level push, session not active
    When I choose to turn on push in push conflict screen
    Then I should be taken to the OS device settings
    And I turn on push at device settings level
    And my session is no longer active
    When I navigate back to the app
    And I navigate back to the login screen
    Then I should be on the environment selector page

  Scenario: TC-004 Exceptions Conflict detected, user selects to turn on OS level Push, doesn't complete action, session still active, user continues to Homepage
    When I choose to turn on push in push conflict screen
    And I should be taken to the OS device settings
    And I do not turn on push at device settings level
    And my session is still active
    And I navigate back to the app
    Then I should see unsuccessful message with two options
    When I select Not now
    And I should be on the home page

  Scenario: TC-005 Exceptions Conflict detected, user selects to turn on OS level Push, doesn't complete action, session still active, user chooses to Opt out
    And I choose to turn on push in push conflict screen
    And I should be taken to the OS device settings
    And I do not turn on push at device settings level
    And my session is still active
    And I navigate back to the app
    Then I should see unsuccessful message with two options
    When I select to opt out
    Then I should see a confirmation that I have chosen not to activate notifications
    And I select ok on this confirmation that I have chosen not to activate notifications
    And I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And I should be opted out of Account alerts

  @pending
  Scenario: TC-006 Exceptions Conflict detected, user selects to turn on OS level Push, doesn't complete action, session not active
    When I choose to turn on push in push conflict screen
    Then I should be taken to the OS device settings
    When I do not turn on push at device settings level
    And my session is no longer active
    And I navigate back to the app
    Then I navigate back to the login screen
