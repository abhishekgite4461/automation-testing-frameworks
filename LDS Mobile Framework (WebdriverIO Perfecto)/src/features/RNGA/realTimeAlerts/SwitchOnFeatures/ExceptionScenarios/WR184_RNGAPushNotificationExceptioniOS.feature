@team-navigation @nga3 @rnga @ios @sw-push-optin-a-on @stub-credit-card-only @stubOnly

# MCC-103, 95
Feature: Usage of real time alerts for preference amendment
  In order to control the notifications I can receive
  As a RNGA authenticated customer
  I want to access and use the Notifications page

  Pre condition:Customer is not currently opted in for Push Notiifcations
  Customer is fully authenticated

  Scenario: TC_001 Customer does not have a valid product, therefore Notifications menu option should not display
    Given I am an enrolled "creditCard" user logged into the app
    When I navigate to settings page from home page
    Then I should not see the Notifications option
