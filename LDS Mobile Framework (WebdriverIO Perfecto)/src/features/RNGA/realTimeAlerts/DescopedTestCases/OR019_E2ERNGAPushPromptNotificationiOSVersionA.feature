@team-navigation @nga3 @rnga @pending @genericAccount @appiumOnly @sw-push-optin-a-off @outOfScope

# REASONS FOR APPIUM EXECUTION :
# need to change the device date for 3 times (each ranging 14 , 21 and 21 days respectively)
# most of the times app launch fails as the date range for testing exceeds the app certificate expiry date
# so need an afterscript to put the date back to default in perfecto devices
# if the afterscript fails , then the entire consecutive test cases will fail

# MCC-598, MCC-354
Feature: Push prompt at login (0-14 days)
  In order to indicate my preference for receiving push notifications
  As an RNGA customer
  I want to be prompted to opt in/out of receiving push notifications after login

  Preconditions for Push Prompt Version B
  Customer is fully enrolled
  Customer has never opted in for push notifications
  Data Consent screen is shown
  Touch ID / Face ID is shown
  Enable Push Notification - iOS switch is ON
  Opt-in Version A Control-iOS is OFF
  Customer logs into the app 14 days after the Data Consent Screen is shown (Customer can get the push prompt
  screen on any random day in between 0-14 days post GDPR screen)

  Scenario: TC_01 Happy path Customer is shown the Push prompt version A screen for first , second and third time, chooses to opt in
    Given I am a current user
    And I enter correct MI
    And I navigate to logoff via more menu
    And I should be on the logout page
    When I login again after 14 days from today's date
    Given I am a current user
    And I enter correct MI
    And I see the push prompt A screen
    And I chose Not now
    Then I should be on the home page
    And I navigate to settings page from home page
    When I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And I should be opted out of Account alerts
    And I navigate to logoff via more menu
    And I should be on the logout page
    When I login again after 35 days from today's date
    Given I am a current user
    And I enter correct MI
    And I see the push prompt A screen
    When I chose Not now
    Then I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And I should be opted out of Account alerts
    And I navigate to logoff via more menu
    And I should be on the logout page
    When I login again after 56 days from today's date
    Given I am a current user
    And I enter correct MI
    And I see the push prompt A screen
    When I chose to opt in
    And I allow notifications for my device
    Then I should see a success confirmation
    And I should be on the home page
    When I navigate to settings page from home page
    And I select the real time alerts option from the Settings page
    Then I should be on the smart alerts page
    And I should be opted in for Account alerts
