@team-navigation @nga3 @rnga @stub-enrolled-typical @optFeature @ff-mdm-push-notifications-setting-enabled-on
@sw-push-notification-on @appiumOnly @genericAccount @ios @sw-push-optin-b-v2-on @outOfScope

# Feature for iOS only
# MCC- 1546 MCC- 1545
Feature: Apple Push Notification Service(APNS)
  In order to receive push on the devices of my choosing
  As an RNGA customer
  I want to manage which of my devices are enabled for push

  Background:
    Given I am an enrolled "current or savings" user logged into the device
    And I have navigated to the Notifications page

  Scenario: T_001 Customer’s current device is not push enabled (has never displayed the ‘Don’t allow’ on native overlay)- successfully enabled
    When the current device has push disabled at device level
    And this device has never asked for push notifications to be enabled before
    And I see an option to enable notifications
    Then I should not be able to interact with the alert toggles
    When the alert toggles should be shown in the correct position according to my current status
    And I select to enable notifications
    Then I should see the native notifications overlay with options to allow or deny permission
    When I choose to allow
    Then I should be able to interact with the alert toggles
    And the alert toggles should be shown in the correct position according to my current status

  Scenario: T_002 Customer’s current device is not push enabled (never select ‘Don’t allow’ on native overlay) chooses to enable notifications, customer denies push permission
    When the current device has push disabled at device level
    And I select to enable notifications
    Then I should see the native notifications overlay with options to allow or deny permission
    When I select to allow push permissions
    Then I should see an option to enable notifications
    And I should see instructions on how to enable push notifications on my device (conflict overlay)
    And I should not be able to interact with the alert toggles
    And the alert toggles should be shown in the correct position according to my current status
