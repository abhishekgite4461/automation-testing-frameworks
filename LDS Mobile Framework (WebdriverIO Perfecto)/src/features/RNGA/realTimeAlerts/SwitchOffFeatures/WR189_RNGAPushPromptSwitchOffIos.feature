@team-navigation @nga3 @rnga @ios @sw-push-optin-a-off @sw-push-optin-b-v2-off @stub-enrolled-typical @stubOnly

# MCC-368, MCC-361, MCC-376, MCC-375, MCC-374

Feature: Push Notifications Switch OFF

  As an RNGA customer
  I want to be prompted to opt in/out of receiving push notifications after login

#  Pre-conditions:
#  Customer is fully enrolled
#  Fingerprint and GDPR Screen is already shown

  Scenario: TC_001 Navigating to the Notifications page
    Given I am an enrolled "current" user logged into the app
    When I navigate to settings page from home page
    Then I should not see the Notifications option

  Scenario: TC_002 Customer is not shown the Push prompt screen after light login
    Given I am an enrolled "current" user logged into the app
    And I navigate to logoff via more menu
    Then I should be on the logout page
    Given I am a current user
    When I enter correct MI
    Then I should be on the home page
