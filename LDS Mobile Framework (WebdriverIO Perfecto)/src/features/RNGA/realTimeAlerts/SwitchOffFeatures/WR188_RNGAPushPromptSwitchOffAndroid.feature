@team-navigation @nga3 @rnga @stub-enrolled-typical @android @sw-push-notification-off @stubOnly

# MCC-368, MCC-361, MCC-376, MCC-375, MCC-374

Feature: Push Notifications Switch OFF

  As an RNGA customer
  I want to be prompted to opt in/out of receiving push notifications after login

#  Pre-conditions:
#  Customer is fully enrolled
#  Fingerprint and GDPR Screen is already shown

  Scenario: TC_001 Navigating to the Notifications page
    Given I am an enrolled "current" user logged into the app
    When I navigate to settings page via more menu
    Then I should not see the Notifications option

  Scenario: TC_002 Customer is not shown the Push prompt screen after light login
    Given I am an enrolled "current" user logged into the app
    Then I should be on the home page
