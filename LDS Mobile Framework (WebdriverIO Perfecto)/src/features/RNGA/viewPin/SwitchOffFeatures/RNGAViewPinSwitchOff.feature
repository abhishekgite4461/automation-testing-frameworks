@team-navigation @nga3 @rnga @ff-view-pin-enabled-on @environmentOnly @stub-viewpin-success @sw-viewpin-ios-on
@sw-viewpin-android-on @sit02Job

#MCC-2132, MCC-2137 , MCC-2112, MCC-2120
Feature:View Pin of the linked debit or credit card
  In order to view pin
  As an RNGA customer
  I want to manage debit or credit card linked

  Scenario: TC_001 Customer successfully views their PIN (password switch is off)
    Given I am an enrolled "credit card only" user logged into the app
    And I select support tab from bottom navigation bar
    When I select the View PIN option from the Help or Support menu
    Then I choose the View PIN option
    And I select to reveal the PIN
    When the PIN number should be displayed for also long as I am holding the reveal PIN button
