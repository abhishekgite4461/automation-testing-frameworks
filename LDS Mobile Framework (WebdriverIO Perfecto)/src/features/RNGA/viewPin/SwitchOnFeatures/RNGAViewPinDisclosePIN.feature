@team-navigation @nga3 @rnga @ff-view-pin-enabled-on @stub-viewpin-success @sw-viewpin-ios-on @sw-viewpin-password-ios-on
@sw-viewpin-android-on @sw-viewpin-password-android-on @manual @sit02Job

# REASONS FOR APPIUM EXECUTION :
# can execute only in SIT 02 B env
Feature:View Pin of the linked debit or credit card
  In order to view pin
  As an RNGA customer
  I want to manage debit or credit card linked

#MCC-2348 MCC-2331 MCC-2176 MCC-2137 MCC-2139
  Background:
    Given I am an enrolled "credit card only" user logged into the app
    And I select support tab from bottom navigation bar
    When I select the option to View PIN from Card and PIN settings page
    Then I should see the selected card displayed as per the designs
    And I should see an area hiding my PIN
    And I should see a 'Reveal PIN' button
    And I should see some advisory text

  Scenario: TC-01 Discard PIN if customer closes modal window
    When the customer closes the modal window
    Then the system will discard the PIN data from the session
    And the system will display the card carousel
    When the customer closes the App window
    Then the system will discard the PIN data from the session
    And there will be NO error banner

  Scenario: TC-02 Reopening App after window after closing it (within auto log off limit)
    Given I am on the View PIN modal
    And I close the app window
    And my auto log off limit hasn't been reached
    When I re-open the app window
    Then I will be on the card carousel page

  Scenario:TC-03 Reopening App after window after closing it (after auto log off limit reached)
    Given I am on the View PIN modal
    And I close the app window
    And my auto log off limit has been reached
    When I re-open the app window
    Then I will be on the app time out page
