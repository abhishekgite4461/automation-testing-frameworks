@team-shareiban @nga3 @rnga @ff-inapp-search-on @sw-inapp-search-android-on @stub-enrolled-valid @sw-inapp-search-ios-on
@ff-share-international-account-details-enabled-off @sw-share-international-account-details-off

Feature:Search IBAN Functionality
  In order to search IBAN
  As an valid RNGA customer
  I search for a keyword and get links to view my BIC, IBAN

  #ENG-1590 #ENG-1594
  Scenario Outline: Verify user is able to view IBAN\BiC Search results when
  FF 'Enable in app search' ON and FF 'Share International Account Details Enabled' ON
    Given I am an enrolled "current" user logged into the app
    When I enter search <keyword> in the search dialog box
    Then I should not see Search Results for BIC and IBAN
    Examples:
      |  keyword               |
      |  International         |
      |  IBAN                  |
      |  BIC                   |
