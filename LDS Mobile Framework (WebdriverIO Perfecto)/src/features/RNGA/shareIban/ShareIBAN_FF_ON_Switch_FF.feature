@environmentOnly @rnga @nga3 @team-shareiban @ff-share-international-account-details-enabled-on @sw-share-international-account-details-off
Feature: Native isShareInternationalAccDetailsEnabled-FeatureFlag-OFF-Switch-Share international account details-ON
  In Order to share my bnk details
  As a Retail NGA user
  I want to be able to share my UK Bank details or International Bank details

  #ENG-1294 #ENG-1292
  Scenario: Action Menu: User should not see Send Bank Details Page when isShareInternationalAccDetailsEnabled-FeatureFlag-OFF-Switch-Share international account details-ON
    Given I am an enrolled "current" user logged into the app
    When I select the "send bank details" option in the action menu of current account
    Then I should not see the Send Bank Details Page
