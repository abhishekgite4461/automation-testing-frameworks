@rnga @nga3 @team-shareiban @ff-share-international-account-details-enabled-on @sw-share-international-account-details-on
Feature: Native isShareInternationalAccDetailsEnabled-FeatureFlag-ON-Switch-Share international account details-ON
  In Order to share my bnk details
  As a Retail NGA user
  I want to be able to share my UK Bank details or International Bank details

  #ENG-1294 #ENG-1292
  @stub-enrolled-valid
  Scenario: Action Menu: User should see Send Bank Details Page And is able to view UK Account Details and IBAN Details When
  isShareInternationalAccDetailsEnabled-FeatureFlag-ON-Switch-Share international account details-ON
    Given I am a credit card and current account user on the home page
    When I select the "send bank details" option in the action menu of islamicCurrent account
    Then I should be able to see Send Bank Details Page
    And there is a warning “Keep your details safe. Only send to people you know”
    And under International Bank details we can see Account Name, IBAN, BIC
    And there is an option to share these International Bank Details
    And under UK Bank Details we can see Name on the account, Sort Code and Account Number
    And there is an option to share these UK Bank details
    And I click Send UK Details Button

  #ENG-1294 #ENG-1292
  @stub-enrolled-valid
  Scenario: Action Menu: User should see Send Bank Details Page And is able to share International details via copy When
  isShareInternationalAccDetailsEnabled-FeatureFlag-ON-Switch-Share international account details-ON
    Given I am a credit card and current account user on the home page
    When I select the "send bank details" option in the action menu of islamicCurrent account
    Then I should be able to see Send Bank Details Page
    And there is a warning “Keep your details safe. Only send to people you know”
    And under International Bank details we can see Account Name, IBAN, BIC
    And there is an option to share these International Bank Details
    And under UK Bank Details we can see Name on the account, Sort Code and Account Number
    And there is an option to share these UK Bank details
    And I click Send International Details button

  #ENG-1294 #ENG-1292
  @stub-enrolled-valid
  Scenario: Action Menu: User clicks close icon it is redirected to HomePage
    Given I am a credit card and current account user on the home page
    When I select the "send bank details" option in the action menu of islamicCurrent account
    And I should be able to see Send Bank Details Page
    And I click close icon
    Then I should be on the home page

  #ENG-1294 #ENG-1292
  @stub-share-international-account-details-failure
  Scenario: Verify when API is unavailable user can see an error in IBAN section
    Given I am a credit card and current account user on the home page
    When I select the "send bank details" option in the action menu of islamicCurrent account
    Then I should be able to see Send Bank Details Page
    And I can see only UK Bank Details
    And I see an error message "Sorry, we can't show international bank details right now. Please try again later"

  #ENG-1294 #ENG-1292
  @stub-pccmc-success
  Scenario: Verify only current account user can see "Send Bank Details Option"
    Given I am a credit card and current account user on the home page
    When I select the action menu of masterCreditCard account
    Then I am unable to see Send bank details option in the action menu

  @manual
  Scenario: Action Menu: User should see Send Bank Details Page and is able to share International details via message when Feature Flag and Switch is ON
    Given I am a credit card and current account user on the home page
    When I select the "send bank details" option in the action menu of islamicCurrent account
    Then I should be able to see Send Bank Details Page
    And there is a warning “Keep your details safe. Only send to people you know”
    And under International Bank details we can see Account Name, IBAN, BIC
    And there is an option to share these International Bank Details
    When I click Send International Details button
    Then I see an option to copy and send this as a message
    And I click message messaging app opens

  @manual
  Scenario: Action Menu: User should see Send Bank Details Page And is able to share International details via message when Feature Flag and Switch is ON
    Given I am a credit card and current account user on the home page
    When I select the "send bank details" option in the action menu of islamicCurrent account
    Then I should be able to see Send Bank Details Page
    And there is a warning “Keep your details safe. Only send to people you know”
    And under International Bank details we can see Account Name, IBAN, BIC
    And there is an option to share these International Bank Details
    When I click Send International Details button
    Then I see an option to copy and send this as a message
    And I click Copy I am able to copy this message

  @manual
  Scenario: Action Menu: User should see Send Bank Details Page And is able to share UK Bank details via copy when Feature Flag and Switch is ON
    Given I am a credit card and current account user on the home page
    When I select the "send bank details" option in the action menu of islamicCurrent account
    Then I should be able to see Send Bank Details Page
    And there is a warning “Keep your details safe. Only send to people you know”
    And under International Bank details we can see Account Name, IBAN, BIC
    And there is an option to share these International Bank Details
    When I click Send UK Details Button
    Then I see an option to copy and send this as a message
    And I click Copy I am able to copy this message

  @manual
  Scenario: Action Menu: User should see Send Bank Details Page And is able to share UK Bank details via message when Feature Flag and Switch is ON
    Given I am a credit card and current account user on the home page
    When I select the "send bank details" option in the action menu of islamicCurrent account
    Then I should be able to see Send Bank Details Page
    And there is a warning “Keep your details safe. Only send to people you know”
    And under UK Bank Details we can see Name on the account, Sort Code and Account Number
    And there is an option to share these UK Bank details
    When I click Send UK Details Button
    Then I see an option to copy and send this as a message
    When I click message messaging app opens
