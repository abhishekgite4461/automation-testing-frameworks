@team-shareiban @nga3 @rnga @ff-inapp-search-on @sw-inapp-search-android-on @sw-inapp-search-ios-on @ff-share-international-account-details-enabled-on@sw-share-international-account-details-on
Feature:Search IBAN Functionality
  In order to search IBAN
  As an valid RNGA customer
  I search for a keyword and get links to view my BIC, IBAN

  #ENG-1590 #ENG-1594
  @stub-enrolled-valid
  Scenario Outline: Verify user is able to view IBAN\BiC Search results when FF 'Enable in app search' ON and FF 'Share International Account Details Enabled' ON
    Given I am an enrolled "current" user logged into the app
    When I enter search <keyword> in the search dialog box
    Then I should be able to see Search results that say "View your International Bank Account Number (IBAN)"
    And another result that says "View your Bank Identifier Code (BIC)"
    Examples:
      |  keyword                          |
      | International                     |
      |IBAN                               |
      |BIC                                |
      |SWIFT                              |
      | International Bank Account Number |
      |Account details                    |
      |Foreign                            |
      |Overseas                           |
      |Abroad                             |
      |Receive money internationally      |
      |View IBAN                          |

  #ENG-1593 #ENG-1591
  @stub-enrolled-valid
  Scenario Outline: Verify user is able to view IBAN\BiC Search results and when user clicks on IBAN search result shown Account Selection Page if user has multiple current accounts
    Given I am an enrolled "current" user logged into the app
    And I enter search <keyword> in the search dialog box
    And I should be able to see Search results that say "View your International Bank Account Number (IBAN)"
    And another result that says "View your Bank Identifier Code (BIC)"
    When I select the IBAN Search Result
    Then I should land on Account Selection Page
    And I select the first Current Account being displayed
    And I should be able to see Send Bank Details Page
    Examples:
      | keyword                           |
      | International                     |
      | IBAN                              |
      | SWIFT                             |

  #ENG-1593 #ENG-1591 #ENG-1595 #ENG-1592
  @stub-enrolled-valid
  Scenario Outline: Verify user is able to view IBAN\BiC Search results and when shown Account Selection Page if user has multiple current accounts
    Given I am an enrolled "current" user logged into the app
    And I enter search <keyword> in the search dialog box
    And I should be able to see Search results that say "View your International Bank Account Number (IBAN)"
    And another result that says "View your Bank Identifier Code (BIC)"
    When I select the Bic Search Result
    Then I should land on Account Selection Page
    And I select the first Current Account being displayed
    And I should be able to see Send Bank Details Page
    Examples:
      | keyword                           |
      | International                     |
      | BIC                               |

  #ENG-1595 #ENG-1592
  @stub-no-creditcard
  Scenario Outline: Verify user with single current account is directly taken to Send Bank Details screen on searching for a keyword
    Given I am an enrolled "current" user logged into the app
    And I enter search <keyword> in the search dialog box
    And I should be able to see Search results that say "View your International Bank Account Number (IBAN)"
    And another result that says "View your Bank Identifier Code (BIC)"
    When I select the IBAN Search Result
    Then I should be able to see Send Bank Details Page
    Examples:
      | keyword                           |
      | International                     |
      | IBAN                              |

  #ENG-1595 #ENG-1592
  @stub-no-creditcard
  Scenario Outline: Verify user with single current account is directly taken to Send Bank Details screen on searching for a keyword
    Given I am an enrolled "current" user logged into the app
    And I enter search <keyword> in the search dialog box
    Then I should be able to see Search results that say "View your International Bank Account Number (IBAN)"
    And another result that says "View your Bank Identifier Code (BIC)"
    When I select the Bic Search Result
    Then I should be able to see Send Bank Details Page
    Examples:
      | keyword                           |
      | International                     |
      | BIC                               |

  #ENG-1851 #ENG-1849
  @stub-arrangements-loan-only
  Scenario Outline: Verify user with single current account is directly taken to Send Bank Details screen on searching for a keyword
    Given I am an enrolled "creditcard" user logged into the app
    And I enter search <keyword> in the search dialog box
    Then I should be able to see Search results that say "View your International Bank Account Number (IBAN)"
    And another result that says "View your Bank Identifier Code (BIC)"
    When I select the IBAN Search Result
    Then Account Selection Page displays a warning
    Examples:
      | keyword                           |
      | International                     |
      | BIC                               |
