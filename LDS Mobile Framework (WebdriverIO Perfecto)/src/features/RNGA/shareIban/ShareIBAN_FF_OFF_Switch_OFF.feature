@rnga @nga3 @team-shareiban @stub-enrolled-valid @ff-share-international-account-details-enabled-off @sw-share-international-account-details-off
Feature: Native isShareInternationalAccDetailsEnabled-FeatureFlag-OFF-Switch-Share international account details-OFF
  In Order to share my bank details
  As a Retail NGA user
  I want to be able to share my UK Bank details or International Bank details

  #ENG-1294 #ENG-1292
  Scenario: Action Menu: User should not see Send Bank Details Page when isShareInternationalAccDetailsEnabled-FeatureFlag-OFF-Switch-Share international account details-OFF
    Given I am a credit card and current account user on the home page
    When I select the "send bank details" option in the action menu of current account
    Then I should not see the Send Bank Details Page
