@team-pi @nga3 @rnga @stub-accountlink-valid @stubOnly @primary
Feature: Account Link Lead Placement
  In order interact with an Account link
  Customer have to see a account link placement right below the account details in the Acc Tile
  pre condition:Account link lead should be configured

  Background:
    Given I am a current user on the home page

  #------------------------------------------ Main Success scenario---------------------------#

  #MOB3-4932
  Scenario: TC_001 Main Success Scenario : Display account link on the account tile
    Given I navigate to islamicCurrent account on home page
    Then I see the Account Link lead placement in the account tile of islamicCurrent

  Scenario: TC_002 Alternate Success Scenario : Display account link leads below the account tile
    Given I select islamic Current account tile from home page
    Then I see the Account Link lead placement in the account tile of islamicCurrent
