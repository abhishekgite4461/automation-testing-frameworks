@team-pi @nga3 @rnga @stubOnly @primary
Feature: Banner Lead Placement

  In order interact with a banner
  As a customer configured with banner
  I want to be shown a banner on Transfer/Payment success and unsuccessful page

  Background:
    Given I am a valid user on the home page
    And there is a banner lead configured

  #MOB3-9654
  @stub-banner-payment-success
  Scenario: TC_001 Display banner on payment success page
    Given I navigate to payment success page
    Then I should be able to see the banner lead placement on payment success page

  #MOB3-9654
  @stub-banner-payment-failure @ios
  Scenario: TC_002 Display banner on payment unsuccessful page
    Given I navigate to payment unsuccessful page
    Then I should be able to see the banner lead placement on payment unsuccessful page

  #MOB3-9654
  @stub-banner-payment-failure-lost-connection @android
  Scenario: TC_003 Display banner on payment unsuccessful page
    Given I navigate to payment unsuccessful page
    Then I should be able to see the banner lead placement on payment unsuccessful page

  #MOB3-9654
  @stub-banner-transfer-success
  Scenario: TC_004 Display banner on transfer success page
    Given I navigate to transfer success page
    Then I should be able to see the banner lead placement on transfer success page

  #MOB3-9654
  @stub-banner-transfer-failure @ios
  Scenario: TC_005 Display banner on transfer unsuccessful page
    Given I navigate to transfer unsuccessful page
    Then I should be able to see the banner lead placement on transfer unsuccessful page

  #MOB3-9654
  @stub-banner-transfer-failure-lost-connection @android
  Scenario: TC_006 Display banner on transfer unsuccessful page
    Given I navigate to transfer unsuccessful page
    Then I should be able to see the banner lead placement on transfer unsuccessful page
