@team-pi @nga3 @rnga @manual
Feature: Outcome Code tracking for DPN

  In order to track a customers interaction with the DPN lead
  As a Customer Insights rep
  I want to be able to track the outcome codes for the lead

  #MOB3-4525,MOB3-4526
  #Session expiry is 20 minutes after killing the app as Galaxy keeps the session open for 20 mins after which the code gets sent to SLR or log off.
  Background:
    Given I am a valid user
    And I have a DPN lead configured

  Scenario: Reserved lead
    When I check the lead status in SLR
    Then the lead should be marked as "Not Actioned" with an outcome code 640

  Scenario: View the lead
    And the customer logs in to the app
    When they  see the DPN Interstitial
    And I check the lead status in SLR
    Then The lead should be marked as "Reserved" with an outcome code 641

  Scenario: Clicked on the native button
    And the customer logs in to the app
    And they  see the DPN Interstitial
    When they choose to skip the lead
    And the session expires
    And I check the lead status in SLR
    Then The lead should be marked as "No Thanks" with an outcome code 644

  Scenario: Clicked inside the lead
    And the customer logs in to the app
    And they  see the DPN Interstitial
    When thew customer actions the copy inside the lead
    And the session expires
    And I check the lead status in SLR
    Then The lead should be marked as "Applied" with an outcome code 642
