@team-pi @nga3 @rnga @manual
Feature: Leads styling for Big Prompt

  As a customer
  I want the leads style to be in line with the app
  So that the experience of using the app is consistent

  Background:
    Given I am an RNGA user logged in to the app
    And I have a Big Prompt lead configured

  #MOB3-4867,4913
  Scenario: TC_001
    When I am shown the Big Prompt Interstitial
    Then the Big Prompt lead must have some styles and designs that are relevant to the brand
