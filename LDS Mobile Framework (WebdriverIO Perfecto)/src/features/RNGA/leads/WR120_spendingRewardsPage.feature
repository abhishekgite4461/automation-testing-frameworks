@team-performance @nga3 @rnga @stub-native-leads-valid @stubOnly @primary

#MOB3-8357
Feature: DPN Leads
  As a customer
  I want the relevant leads to be kept ready
  So that as soon as I log in, I can see Spending Rewards Registration Leads link from the DPN lead page

  Background:
    Given I submit "correct" MI as "native leads valid" user on the MI page
    And an interstitial lead is displayed

  Scenario: TC_001 Navigate to Spending Rewards Registration page
    When I select the Spending Rewards link from the DPN lead page
    Then I should be on the spending rewards registration page
