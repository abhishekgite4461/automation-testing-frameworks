@team-pi @nga3 @rnga @stubOnly
Feature: Multiple interstitial leads returned

  In order to promote the most important leads
  as a promoter of leads
  I want to be able to select which lead will be seen first by a user

  @stub-multiple-leads @primary
  #MOB3-4865/#MOB3-4912
  Scenario: TC_001 Display multiple leads
    Given I submit "correct" MI as "multiple leads" user on the MI page
    When an interstitial lead is displayed
    Then I am shown the interstitial lead with the highest priority

  @stub-multiple-interstitial-leads @primary
  #MOB3-4865/#MOB3-4912
  Scenario: TC_002 Display multiple interstitial leads
    Given I submit "correct" MI as "multiple interstitial leads" user on the MI page
    When an interstitial lead is displayed
    Then I am shown the interstitial lead with the highest priority

  @stub-multiple-bp-priority-duplicated @primary
  #MOB3-4865/#MOB3-4912
  Scenario: TC_003 Display multiple big prompt priority leads
    Given I submit "correct" MI as "multiple bp priority duplicated" user on the MI page
    When an interstitial lead is displayed
    Then I am shown the interstitial lead with the highest priority

  @stub-multiple-dpn @secondary
  #MOB3-4865/#MOB3-4912
  Scenario: TC_004 Display multiple dpn leads
    Given I submit "correct" MI as "multiple dpn" user on the MI page
    When an interstitial lead is displayed
    Then I am shown the interstitial lead with the highest priority

  @stub-mdpn-priority-duplicated @primary
  #MOB3-4912
  Scenario: TC_005 Display multiple dpn priority duplicate leads
    Given I submit "correct" MI as "multiple dpn priority duplicated" user on the MI page
    When an interstitial lead is displayed
    Then I am shown the interstitial lead with the highest priority
