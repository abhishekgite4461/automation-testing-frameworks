@team-pi @nga3 @rnga @manual
Feature: Leads journey

  #MOB3-4536/4537
  Scenario: TC_001 Suppressing the lead in Fingerprint Opt In Journey
    Given I am an enrolled "valid" user logged into the app
    When I have configured an interstitial lead
    And I Opt In for the fingerprint from the interstitial
    Then I must not see the lead after the Opt In journey
    And I must be shown the home page directly

  #MOB3-4536/4537
  Scenario: TC_002 Suppressing the lead in Enrolment journey
    Given I am an IB registered User
    And I have configured an interstitial lead
    And I start the enrolment journey
    When I finish the enrolment
    Then I must not see the lead after the enrolment journey
    And I must be shown the home page directly

  #MOB3-4536/4537
  Scenario: TC_003 Suppressing the lead in Fingerprint Change journey
    Given I am an enrolled "valid" user logged into the app
    And I have configured an interstitial lead
    When I change my fingerprint in my device
    Then I must not see the lead after the Fingerprint change journey
    And I must be shown the home page directly

  #MOB3-4536/4537
  Scenario: TC_004 Suppressing the lead in App Signin journey
    Given I am an enrolled "valid" user logged into the app
    And I have performed App Signin journey in the desktop
    And I have configured an interstitial lead
    When I launch the app in the mobile
    And I have entered MI
    Then I must not see any lead after launching the app

  #MOB3-4532/4533
  Scenario: TC_005 Slow retrieval of Lead service
    Given I am an enrolled "valid" user logged into the app
    When there is no update from the leads service to the app
    And all the other services for the home page are completed
    Then I must be on the home page after logging in without waiting for the leads
