@team-login @nga3 @rnga @stub-native-leads-valid @stubOnly
Feature: DPN Leads

  As a customer
  I want the relevant leads to be kept ready
  So that as soon as I log in, I can see Settings:Product Details Page, Product Hub and Call Us Leads link from the DPN lead page

  Background:
    Given I submit "correct" MI as "native leads valid" user on the MI page
    And an interstitial lead is displayed

  #MQE-66
  Scenario: TC_001 Navigate to Product Hub page
    When I select the Product Hub link from the DPN lead page
    Then I should be on the product hub page

  Scenario: TC_002 Navigate to Settings:Personal Details page
    When I select the Personal Detail link from the DPN lead page
    Then I should be on the your personal details page

  Scenario: TC_003 Navigate to Settings:Change Phone Numbers page
    When I select the Change Phone Number link from the DPN lead page
    Then I should be shown the update phone number page

  #MQE-66
  Scenario: TC_004 Navigate to Settings:Change Address page
    When I select the Change Address link from the DPN lead page
    Then I should be on the address update page
