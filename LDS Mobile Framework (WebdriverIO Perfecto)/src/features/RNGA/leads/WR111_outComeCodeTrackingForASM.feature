@team-pi @nga3 @rnga @manual
Feature: Outcome Code tracking for Banner

  In order to track a customers interaction with the Banner
  As a Customer Insights rep
  I want to be able to track the outcome codes for the Banner

  #MOB3-4938, MOB3-10668
  #Session expiry is 20 minutes after killing the app as Galaxy keeps the session open for 20 mins after which the code gets sent to SLR or log off.
  Background:
    Given I am an RNGA user logged in to the app
    And I have a ASM lead configured

  Scenario: Reserved lead
    When I check the lead status in SLR
    Then the lead should be marked as "Not Actioned" with an outcome code 640

  Scenario: View the lead
    When I am shown the ASM
    Then the lead is marked as "Reserved" in SLR with an outcome code 641

  Scenario: Clicked on the lead
    Given I am shown the ASM
    When I click on the the lead
    And then end the session
    Then the lead is marked as "Applied" in SLR with an outcome code 642
