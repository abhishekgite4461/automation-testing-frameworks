@team-pi @nga3 @rnga @manual
Feature: Leads styling for DPN

  As a customer
  I want the leads style to be in line with the app
  So that the experience of using the app is consistent

  Background:
    Given I submit "correct" MI as "valid" user on the MI page
    And  there is an interstitial lead configured

  #MOB3-4529,4530
  Scenario: TC_001
    When I am shown the DPN Interstitial
    Then the DPN lead must have some styles and designs that are relevant to the brand
