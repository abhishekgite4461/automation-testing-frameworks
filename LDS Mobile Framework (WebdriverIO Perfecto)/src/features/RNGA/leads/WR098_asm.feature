@team-panm @nga3 @rnga @stubOnly @primary @stub-asm-lead-all-pages

Feature: ASM

  In order interact with an ASM
  As a customer configured with ASM
  I want to be shown ASMs interleaved into the displayed account tiles

#DPL-2701, DPL-2644, DPL-3012, DPL-3510, LDS-1002
  Scenario: Asm Lead links to native page
    Given I am an enrolled "native leads valid" user logged into the app
    And I scroll through my accounts tiles to an ASM lead
    When I select the asm tile
    Then I should be on the payment hub page
