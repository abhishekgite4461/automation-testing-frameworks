@team-performance @nga3 @rnga @stub-native-leads-valid @stubOnly @primary

#MOB3-8354
Feature: DPN Leads
  As a customer
  I want the relevant leads to be kept ready
  So that as soon as I log in, I can see Security Settings Leads link from the DPN lead page

  Scenario: TC_001 Navigate to Security Settings page
    Given I submit "correct" MI as "native leads valid" user on the MI page
    And an interstitial lead is displayed
    When I select the Security settings link from the DPN lead page
    Then I should be on the security settings page
