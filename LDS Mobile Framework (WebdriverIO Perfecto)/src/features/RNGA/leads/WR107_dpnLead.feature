@team-pi @nga3 @rnga @stubOnly @instrument
Feature: DPN Leads

  As a customer
  I want the relevant leads to be kept ready
  So that as soon as I log in, I can see any relevant marketing placements

  @stub-leads-valid
  #MOB3-4532/4533
  Scenario: TC_001 Fetching Leads
    Given I submit "correct" MI as "valid" user on the MI page
    When there is an interstitial lead configured
    Then an interstitial lead is displayed

  @stub-leads-valid @secondary
  #MOB3-4496/4528
  Scenario: TC_002 Removing Global Menu
    Given I submit "correct" MI as "valid" user on the MI page
    And there is an interstitial lead configured
    When an interstitial lead is displayed
    Then I must not see the Global Menu in the lead screen
    And  I see native button at the bottom of the lead

  @stub-single-dpn @android @primary
  Scenario: TC_003 Button Action
    Given I submit "correct" MI as "valid" user on the MI page
    And there is an interstitial lead configured
    And an interstitial lead is displayed
    When I scroll the dpn lead to the bottom
    And the native button should be in enabled mode
    Then I click on the native button at the bottom of the lead
    And I should be on the home page

  @stub-enrolled-typical
  #MOB3-4532/4533
  Scenario: TC_004 No leads configured
    Given I am an enrolled "valid" user logged into the app
    When there is no an interstitial lead configured
    Then I should be on the home page

  @stub-leads-valid @android @primary
  #MOB3-4532/4533
  Scenario: TC_005 Pop-up for Scrolling of Leads
    Given I submit "correct" MI as "valid" user on the MI page
    And there is an interstitial lead configured
    And an interstitial lead is displayed
    When I click on the native button at the bottom of the lead
    Then an alert pop-up saying to scroll to the bottom should be displayed

  @stub-leads-valid @ios @primary
  #MOB3-4509/4510
  Scenario: TC_006 Native Button disabled on the Leads page without scrolling
    Given I submit "correct" MI as "valid" user on the MI page
    And there is an interstitial lead configured
    When an interstitial lead is displayed
    Then the native button should be in disabled mode

  @stub-leads-valid @ios @primary
  #MOB3-4509/4510
  Scenario: TC_007 Native Button gets enabled on the Leads page on scrolling
    Given I submit "correct" MI as "valid" user on the MI page
    And there is an interstitial lead configured
    And an interstitial lead is displayed
    When I scroll the dpn lead to the bottom
    Then the native button should be in enabled mode
