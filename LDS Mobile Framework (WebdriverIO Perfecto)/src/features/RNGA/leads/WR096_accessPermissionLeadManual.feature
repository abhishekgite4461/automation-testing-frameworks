@team-pi @nga3 @rnga @manual
Feature: Marketing Leads journey

  Background:
    Given I am an enrolled "valid" user logged into the app

 #MOB3-6755/MOB3-6754
  Scenario: TC_001 User should be displayed with a Big Prompt lead
    When I am a customer eligible to update my marketing consent via a interstitial lead
    Then I should see a Big Prompt interstitial after I log in and before reaching the home page

  Scenario: TC_002 Global menu and back buttons won’t be displayed in the marketing landing page
    Given I am a customer eligible to update my marketing consent via a lead
    When I have seen a Big Prompt interstitial lead while logging in
    Then I should be not displayed with Global menu or back button on the marketing page

  Scenario: TC_003 User should be only displayed with one button for the big prompt lead
    Given I am a customer eligible to update my marketing consent via a lead
    When I have seen a Big Prompt interstitial lead while logging in
    Then I should only be displayed with one button on the screen

  Scenario: TC_004 User should be taken to the marketing consents page after clicking on the lead
    Given I am a customer eligible to update my marketing consent via a lead
    When I have seen a Big Prompt interstitial lead while logging in
    And I clicked on the only button on the screen
    Then I must be taken to the new marketing consents page

  Scenario: TC_005 Outcome Codes for lead taken 643
    Given I am a customer eligible to update my marketing consent via a lead
    When I am taken to the new marketing consents page from Big Prompt interstitial lead
    And I Submit my preferences 
    And I have seen a response back from OCIS that the preferences have been updated
    Then the lead outcome code is updated in SLR with 643 as "Taken Up"

  Scenario: TC_006 Outcome Codes for lead reserved 641
    Given I am a customer eligible to update my marketing consent via a lead
    And I am taken to the new marketing consents page
    When I Submit my preferences 
    And I have seen a response back from OCIS that the preferences have not been updated
    Then the lead outcome code sent will be 641 as "Reserved"

    #MOB3-4934/10529
  Scenario: TC_007 Multiple Account Link
    Given I am an RNGA user with Account Links configured
    When I have displayed with account link in account tile
    Then I am shown with the highest priority for that particular placement
