@team-pi @nga3 @rnga @pending
Feature: Leads journey

  Background:
    Given I am an enrolled "valid" user logged into the app

  #Marketing Leads - Interstitial Big Prompt & Big Prompt Take over
  #MOB3-7287
  Scenario: TC_001 links from interstitials direct to target screen
    Given I am a customer looking at an interstitial lead
    And I am displayed with deep link
    When I click on the deep link
    Then I should be redirected to the target screen

  Scenario: TC_002 links from banners direct to target screen
    Given I am a customer looking an banner lead
    And I am displayed with deep link
    When I click on the deep link
    Then I should be redirected to the target screen

  Scenario: TC_003 links from sticky footer direct to target screen
    Given I am a customer looking an sticky footer lead
    And I am displayed with deep link
    When I click on the deep link
    Then I should be redirected to the target screen

  Scenario: TC_004 links from account link direct to target screen
    Given I am a customer looking an account link lead
    And I am displayed with deep link
    When I click on the deep link
    Then I should be redirected to the target screen

  Scenario: TC_005 invalid links
    Given I am a customer looking at an interstitial lead with a invalid link
    And I am displayed with deep link
    When I click on the deep link
    Then I should be redirected to the Home screen

  Scenario: TC_006 disabled links - feature switch OFF
    Given I am a customer looking at an interstitial lead
    And the GDPR client side feature switch is OFF
    And the Marketing Preferences environment switch for Halifax is ON
    And I am displayed with deep link
    When I click on the deep link
    Then I should be redirected to the Home screen

  Scenario: TC_007 disabled links - environment switch OFF
    Given I am a customer looking an interstitial lead
    And the GDPR client side feature switch is ON
    And the Marketing Preferences environment switch for Halifax is OFF
    And I am displayed with deep link
    When I click on the deep link
    Then I should be redirected to the Home screen
