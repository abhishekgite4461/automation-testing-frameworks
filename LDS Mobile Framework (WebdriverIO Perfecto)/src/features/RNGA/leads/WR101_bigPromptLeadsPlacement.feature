@team-pi @nga3 @rnga @stubOnly
Feature: Big Prompt Leads Placement

  In order interact with a Big Prompt lead
  As a customer
  I want to be shown a Big Prompt lead between successful login and home page

  Background:
    Given I submit "correct" MI as "big prompt" user on the MI page

  @stub-big-prompt
  #MOB3-4853, MOB3-4905
  Scenario: TC_001 Fetching Big Prompt lead
    Then an interstitial lead is displayed

  @stub-big-prompt @primary
  #MOB3-4853, MOB3-4905
  Scenario: TC_002 Display Big prompt lead
    Given an interstitial lead is displayed
    Then I must not see the Global Menu in the lead screen
    When I select the active native button at the bottom of the screen
    Then I should be on the home page

  @stub-big-prompt @secondary
  Scenario: TC_003 Removing Global Menu
    Given an interstitial lead is displayed
    Then I must not see the Global Menu in the lead screen

  @android @stub-big-prompt @secondary
  Scenario: TC_004 Verify user shouldn't leave leads page on tapping device back button
    Given an interstitial lead is displayed
    When I select back key on the device
    Then I should be on the interstitial page

  @stub-bigprompt-scrollable @primary
  Scenario: TC_005 Scroll the lead
    Given an interstitial lead is displayed
    When I scroll the big prompt lead to the bottom
    And I select the active native button at the bottom of the screen
    Then I should be on the home page
