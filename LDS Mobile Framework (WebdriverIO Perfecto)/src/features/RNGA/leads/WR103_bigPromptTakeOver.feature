@team-pi @nga3 @rnga @stubOnly
Feature: Big Prompt Take Over

  In order interact with a Big Prompt lead
  As a customer
  I want to be shown a Big Prompt lead between successful login and home page

  Background:
    Given I submit "correct" MI as "bigprompt takeover" user on the MI page

  @stub-bigprompt-takeover
  #MOB3-4853
  Scenario: TC_001 Fetching Leads
    Then an interstitial lead is displayed

  @stub-bigprompt-takeover @primary
  #MOB3-4854,4855
  Scenario: TC_002 Display Big Prompt Take Over
    Given an interstitial lead is displayed
    Then I must not see the Global Menu in the lead screen
    And I navigate out of the lead by selecting the options in the lead screen

  @stub-bigprompt-takeover @secondary
  Scenario: TC_003 Validate bottom nav is not displayed
    Given an interstitial lead is displayed
    Then bottom navigation bar is not displayed

  @android @stub-bigprompt-takeover @secondary
  Scenario: TC_004 Verify user shouldn't leave leads page on tapping device back button
    Given an interstitial lead is displayed
    When I select back key on the device
    Then I should be on the interstitial page

  @stub-bp-takeover-scrollable @primary
  Scenario: TC_005 Scroll the lead
    Given an interstitial lead is displayed
    Then I scroll the bigprompt takeover lead to the bottom
