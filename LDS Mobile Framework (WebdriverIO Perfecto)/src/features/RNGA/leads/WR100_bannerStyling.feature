@team-pi @nga3 @rnga @manual
Feature: Leads styling for Banner

  As a customer
  I want the leads style to be in line with the app
  So that the experience of using the app is consistent

  Background:
    Given I am an RNGA user logged in to the app
    And I have a Banner lead configured

  #MOB3-9655, 9656
  Scenario: TC_001
    When I am shown the Banner
    Then the Sticky Footer lead must have the styles and designs that are relevant to the brand
