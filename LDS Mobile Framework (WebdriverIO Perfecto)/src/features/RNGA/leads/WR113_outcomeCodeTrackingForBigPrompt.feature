@team-pi @nga3 @rnga @manual
Feature: Outcome code tracking for Big Prompt

  In order to track a customers interaction with the Big prompt
  As a Customer Insights rep
  I want to be able to track the outcome codes for the lead

  #MOB3-4857,MOB3-4911
  #Session expiry is 20 minutes after killing the app as Galaxy keeps the session open for 20 mins after which the code gets sent to SLR or log off.
  Background:
    Given I am a valid user
    And I have a Big Prompt lead configured

  Scenario: Reserved lead
    When I check the lead status in SLR
    Then the lead should be marked as "Not Actioned" with an outcome code 640

  Scenario: View the lead
    And the customer logs in to the app
    When they see the Big Prompt
    And I check the lead status in SLR
    Then the lead should be marked as "Reserved" with an outcome code 641

  Scenario: Clicked on the native button
    And the customer logs in to the app
    And they see the Big Prompt
    When they choose to skip the lead
    And the session expires
    And I check the lead status in SLR
    Then The lead should be marked as "No Thanks" with an outcome code 644

  Scenario: Clicked on lead
    And the customer logs in to the app
    And they see the Big Prompt
    When the customer actions the lead
    And the session expires
    And I check the lead status in SLR
    Then The lead should be marked as "Applied" with an outcome code 642
