@team-pi @nga3 @rnga @manual
Feature: Leads styling for Sticky Footer

  As a customer
  I want the leads style to be in line with the app
  So that the experience of using the app is consistent

  Background:
    Given I am an RNGA user logged in to the app
    And I have a Sticky Footer lead configured

  #MOB3-4924,6483
  Scenario: TC_001
    When I am shown the Sticky Footer
    Then the Sticky Footer lead must have the styles and designs that are relevant to the brand
