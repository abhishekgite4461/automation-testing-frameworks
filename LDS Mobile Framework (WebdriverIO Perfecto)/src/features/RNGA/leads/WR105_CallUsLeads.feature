@team-pi @nga3 @rnga @stub-native-leads-valid @stubOnly
Feature: DPN Leads

  As a customer
  I want the relevant leads to be kept ready
  So that as soon as I log in, I can see Call Us home page and specific Call us pages Leads link from the DPN lead page

  Background:
    Given I submit "correct" MI as "native leads valid" user on the MI page
    And an interstitial lead is displayed

  Scenario: TC_001 Navigate to Call Us home page
    When I select the Call Us link from the DPN lead page
    Then I should be on the call us home page

  #MOB3-10192
  Scenario Outline: TC_002 Navigate to Call Us pages
    When I select the <call_us_page> link from the DPN lead page
    Then I should be on the secure call page
    Examples:
      | call_us_page                |
      | Call Us Internet Banking    |
      | Call Us Other Banking       |
      | Call Us Medical Assistance  |
      | Call Us Suspected Fraud     |
      | Call Us Lost Stolen         |
      | Call Us Emergency Cash      |
      | Call Us New Current Account |
      | Call Us New Credit Card     |
      | Call Us New Loan            |
      | Call Us New Isa Account     |
      | Call Us New Mortgage        |
      | Call Us New Savings         |
