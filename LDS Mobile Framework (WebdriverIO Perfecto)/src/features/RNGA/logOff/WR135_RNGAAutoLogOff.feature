@team-login @nga3 @rnga @stub-enrolled-typical @mbnaSuite
Feature: Verify Settings menu entry points
  In order to edit my app settings
  As a RNGA authenticated customer
  I want to view my app settings page

  Background:
    Given I am an enrolled "current" user logged into the app
    And I navigate to security settings page from home page
    And I select the autoLog off settings option from the security settings page

  #MOB3-3080 #MOB3-2076
  @primary
  Scenario Outline: AC01 TC_001_To verify if the user is getting logged off from the app due to inactivity as per the selected autolog off time settings
    And I select the desired "<autoLogOffTime>" from the auto log off page
    Then I should see a log off alert 15 seconds before the "<autoLogOffTime>" auto logoff time
    And I must be logged out of the app after the selected auto logoff time

    Examples:
      | autoLogOffTime |
      | 1 Minute       |

  #MOB3-3080 #MOB3-2076
  @primary
  Scenario Outline: AC01 TC_002_To verify if the user is getting logged off from the app after selecting log off option from the log off alert pop up
    And I select the desired "<autoLogOffTime>" from the auto log off page
    Then I should see a log off alert 15 seconds before the "<autoLogOffTime>" auto logoff time
    And I select the logoff option from the logoff alert pop up
    And I should be on the logout page

    Examples:
      | autoLogOffTime |
      | 2 Minute       |

  #MOB3-3080 #MOB3-2076
  @primary
  Scenario Outline: AC01 TC_003_To verify if the user is not getting logged off from the app after selecting continue option from the log off alert pop up
    And I select the desired "<autoLogOffTime>" from the auto log off page
    And I should see a log off alert 15 seconds before the "<autoLogOffTime>" auto logoff time
    And I select the continue option from the logoff alert pop up
    Then I should be on the security settings page after the log out time selected

    Examples:
      | autoLogOffTime |
      | 1 Minute       |

  #MOB3-3080 #MOB3-2076
  @primary
  Scenario Outline: AC01 TC_004_To verify if the user is getting logged off from the app immediately during app exit after selecting immediate log out option
    And I select the desired "<autoLogOffTime>" from the auto log off page
    And I put the app in the background
    And I launch the app from the background
    Then I should be on the logout page

    Examples:
      | autoLogOffTime  |
      | logoffImmediate |

  #MOB3-7394
  @manual
  Scenario: AC01 TC_005_To verify if the user is able to view successful auto log off toast message post updating the auto log off timer settings
    And I select save option from the security settings page
    Then I should be able to see an auto log off settings successfully updated toast message for 3 seconds
