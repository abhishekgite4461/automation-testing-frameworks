@team-login @nga3 @rnga @mbnaSuite
Feature: Verify app log off initiated by the user
  In order to log off or sign out from the app
  As a RNGA authenticated customer
  I want to view the log off option from the global menu

  Background:
    Given I am an enrolled "current" user logged into the app

  #MOB3-3948 #MOB3-3076 #MORE-58
  @stub-enrolled-typical @primary
  Scenario: User successfully logs off from the app
    When I navigate to logoff via more menu
    Then I should be on the logout page
    When I select the contact us option from the logoff page pre auth header
    Then I should be on pre auth call us home page overlay

  @manual
  #When logOff request returns response{ "surveyRequired": true } from the server for the user NPS survey is displayed
  Scenario: User wish to provide NPS app survey from Logoff screen
    When I navigate to logoff via more menu
    And I should be on the logout page
    And I select the NPS Start survey button on the logout page
    And I should be navigated to mobile browser with 3rd party NPS URLs
    Then I verify client app has passed correct parameters with NPS survey URL as mentioned in below table
      | Brand          | NPS URL                                                                            | cper id |
      | LloydsBank     | http://survey.euro.confirmit.com/wix/p1844748822.aspx?cper=%@&brand=LloydsBank     | ocsID   |
      | BankOfScotland | http://survey.euro.confirmit.com/wix/p1844748822.aspx?cper=%@&brand=BankOfScotland | ocsID   |
      | Halifax        | http://survey.euro.confirmit.com/wix/p1844748822.aspx?cper=%@&brand=Halifax        | ocsID   |
