@team-pi @nga3 @rnga @stubOnly @primary @instrument @mbnaSuite
Feature: Error Scenarios retrieving marketing preferences from a lead
  As a customer
  I want to be informed if there are issues with my marketing consents retrieval or submission
  So that I can do it at a later time.

  Background: Given I have navigated to the Your profile Page
    Given I submit "correct" MI as "valid" user on the MI page

  @stub-gdpr-bp-ocis-read-error @android
  Scenario: TC_001 Error retrieving Marketing Consents from OCIS Android
    And an interstitial lead is displayed
    And I select the marketing hub link in the lead
    When the following error message appears Sorry, your marketing choices are not available at this time. Please try again later.
    And I dismiss the preferences error message
    Then I should be on the home page

  @stub-gdpr-bp-ocis-read-error @ios
  Scenario: TC_002 Error retrieving Marketing Consents from OCIS IOS
    Given an interstitial lead is displayed
    And I select the marketing hub link in the lead
    When An error pop up appears saying marketing choices cannot be retrieved
    And I confirm I have read the error message
    Then I should be on the home page

  @stub-gdpr-bp-ocis-write-error
  Scenario: TC_003 Error writing to Marketing Consents from OCIS
    And an interstitial lead is displayed
    And I navigate to the marketing hub via the lead
    And I toggle the internetPrefs marketing option
    When I submit my preferences
    Then the following error message appears Sorry, your marketing choices could not be updated. Please try again later.
