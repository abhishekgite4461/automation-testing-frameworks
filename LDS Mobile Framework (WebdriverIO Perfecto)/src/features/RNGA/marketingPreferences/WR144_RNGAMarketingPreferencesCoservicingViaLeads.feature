@team-pi @nga3 @rnga @instrument @bos @hfx
#TODO MOB3 -7544
Feature: Co-Servicing via Lead Entry
  As a co servicing customer on RNGA
  I want to be able to set my preferences for both BoS and Halifax from the same app via Lead
  So that I can set different preferences for the different brands from the same app.

  Background:
    Given I submit "correct" MI as "coservicing account" user on the MI page
    And an interstitial lead is displayed
    And I select the marketing preferences link in the lead interstitial

  #------------------------------------------ Main Success scenario---------------------------#

  #MCOM - 7543/9587
  @primary @stub-gdpr-bp-coservicing-pref-notset
  Scenario: Main Success Scenario : TC001 Entry points via Lead
    Given I will be taken to the primary brand consents page
    And I see a progress indicator showing 50% as complete
    And I opt in to all the marketing preferences for the primary brand
    When I select continue to navigate to the secondary marketing preferences screen
    And I see a progress indicator showing 100% as complete
    And I opt in to all the marketing preferences for the secondary brand
    And I submit my preferences
    Then I'm presented with a Success message

  @primary @ios @stub-gdpr-bp-coservicing-pref-set
  Scenario: Main Success Scenario : TC002 Entry points via Lead
    Given I will be taken to the primary brand consents page
    When I opt in to all the marketing preferences for the primary brand
    And I submit my preferences
    Then I'm presented with a Success message

  #------------------------------------------ Exeception scenario---------------------------#

  @secondary @stub-gdpr-bp-coservicing-pref-notset
  Scenario: TC003 Incomplete preferences primary and secondary brand
    Given I will be taken to the primary brand consents page
    When I toggle the internetPrefs marketing option for the primary brand
    And I select continue to navigate to the secondary marketing preferences screen
    Then An error message will be shown informing to complete all consents
    When I opt in to all the marketing preferences for the primary brand
    And I select continue to navigate to the secondary marketing preferences screen
    And I toggle the internetPrefs marketing option for the secondary brand
    And I submit my preferences
    Then An error message will be shown informing to complete all consents

  @android @secondary @stub-gdpr-bp-coservicing-pref-notset
  Scenario: TC004 Navigating from the Secondary to Primary brand through device back button
    Given I opt in to all the marketing preferences for the primary brand
    And I select continue to navigate to the secondary marketing preferences screen
    When I toggle the internetPrefs marketing option for the secondary brand
    And I select back key on the device
    Then I will be taken to the primary brand consents page

  @secondary @stub-gdpr-bp-coservicing-pref-notset
  Scenario: TC005 Navigating from the Secondary to Primary brand through header back button
    Given I opt in to all the marketing preferences for the primary brand
    And I select continue to navigate to the secondary marketing preferences screen
    When I toggle the internetPrefs marketing option for the secondary brand
    And I navigate back
    Then I will be taken to the primary brand consents page
