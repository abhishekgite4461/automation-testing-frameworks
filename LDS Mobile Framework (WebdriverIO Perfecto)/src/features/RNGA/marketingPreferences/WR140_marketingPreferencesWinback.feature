@team-pi @nga3 @rnga @stub-gdpr-ocis-prefs-not-set @mbnaSuite
Feature: winback customers to marketing consents page from native entry
  As a customer
  I want to be warned if i'm leaving marketing prefernces page before my changes have been saved
  So that my updated preferences arent lost

  Background:
    Given I am a marketing preferences user on the home page
    And I navigate to marketing preferences hub via more menu

  @secondary
  Scenario: TC001 - back button triggers a Winback prompt
    Given I toggle the internetPrefs marketing option
    When I navigate back
    Then I'm presented with a Winback option

  @primary
  Scenario: TC002 Win back to preferences hub after back button
    Given I toggle the emailPrefs marketing option
    When I navigate back
    And I'm presented with a Winback option
    And I opt to stay on the marketing preferences page
    Then I'm on the marketing hub page

  @secondary @pending
  Scenario: TC003 decline win back after selecting call us on bottom navigation bar
    Given I toggle the emailPrefs marketing option
    When I select support tab from bottom navigation bar
    And I'm presented with a Winback option
    And I opt to leave the marketing preferences page
    Then I should be on the call us home page
  #Note: Winback from bottom navigation bar is yet to be resolved by the redesign team

  @secondary
  Scenario: TC004 No win back via back button if no changes are made
    Given I'm on the marketing hub page
    When I navigate back
    Then I should be on the your personal details page

  @secondary
  Scenario: TC005 No win back via bottom navigation bar if no changes are made
    Given I'm on the marketing hub page
    When I select support tab from bottom navigation bar
    Then I should be on the support hub page

  @secondary @android
  Scenario: TC006 Win back to preferences hub after device back button
    Given I toggle the postPrefs marketing option
    When I select back key on the device
    And I'm presented with a Winback option
    And I opt to stay on the marketing preferences page
    Then I'm on the marketing hub page

  @primary @android
  Scenario: TC007 decline win back after using device back button
    Given I toggle the postPrefs marketing option
    When I select back key on the device
    And I'm presented with a Winback option
    And I opt to leave the marketing preferences page
    Then I should be on the your personal details page

  @secondary @android
  Scenario: TC008 No win back via device back button if no changes are made
    Given I select back key on the device
    Then I should be on the your personal details page

  @secondary @android
  Scenario: TC009 - device back button triggers a Winback prompt
    Given I toggle the internetPrefs marketing option
    When I select back key on the device
    Then I'm presented with a Winback option
