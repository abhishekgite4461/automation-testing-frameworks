@team-pi @nga3 @rnga @mbnaSuite
Feature: selecting marketing consents from native entry page
As a customer
I want to be able to access marketing preferences
So that I can review and update how I am contacted about Lloyds products

  Background: Given I have navigated to the Your profile Page
    Given I am an enrolled "marketing preferences" user logged into the app
    And I navigate to profile page via more menu
    And I select marketing preferences link

  #MOB3-6746, MOB3-6659
  @stub-enrolled-typical @primary
  Scenario: TC001 Accessing the Preferences hub by the native entry point
    Then I'm on the marketing hub page

  @stub-enrolled-typical @secondary
  Scenario: TC002 Updating Exisitng Preferences
    Given I'm on the marketing hub page
    When I toggle the internetPrefs marketing option
    And I submit my preferences
    Then I'm presented with a Success message

  @stub-gdpr-ocis-prefs-not-set @stubOnly @primary
  Scenario: TC003 Creating Marketing Preferences
    Given I'm on the marketing hub page
    And my marketing prefences are in an unselected state
    When I opt in to all the marketing prefernces
    And I submit my preferences
    Then I'm presented with a Success message

  @stub-enrolled-typical @primary
  Scenario: TC004 return to acounts after updating my preferences
    Given I have succesfully updated my preferences
    When I select the option to return to accounts
    Then I should be on the home page

  @stub-enrolled-typical @secondary
  Scenario: TC005 selecting my contact details after updating my preferences
    Given I have succesfully updated my preferences
    When I select the option to return to my profile
    And I am on my personal details page
