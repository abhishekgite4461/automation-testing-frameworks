@team-pi @nga3 @rnga @stubOnly @secondary @mbnaSuite
Feature: Error Scenarios for marketing preferences on native entry
  As a customer
  I want to be informed if there are issues with my marketing consents retrieval or submission
  So that I can do it at a later time.

  Background: Given I have navigated to the Your profile Page
    Given I am an enrolled "marketing preferences" user logged into the app

  #MOB3-7981/MOB3-6743
  @stub-gdpr-ocis-read-error @android
  Scenario: TC_001 Error retrieving Marketing Consents from OCIS Android
    Given I navigate to marketing preferences hub via more menu
    When the following error message appears Sorry, your marketing choices are not available at this time. Please try again later.
    And I dismiss the preferences error message
    Then I should be on the your personal details page

  @stub-gdpr-ocis-read-error @ios
  Scenario: TC_002 Error retrieving Marketing Consents from OCIS IOS
    Given I navigate to profile page via more menu
    When I select marketing preferences link
    And the following error message appears Error message.Sorry, your marketing choices are not available at this time. Please try again later.
    And I dismiss the preferences error message
    Then I should be on the your personal details page

  @stub-gdpr-ocis-write-error
  Scenario: TC_003 Error writing to Marketing Consents from OCIS
    Given I navigate to marketing preferences hub via more menu
    When I toggle the internetPrefs marketing option
    And I submit my preferences
    Then the following error message appears Sorry, your marketing choices could not be updated. Please try again later.

  @stub-gdpr-ocis-prefs-not-set
  Scenario: TC004 Display an error message when saving preferences in an unselected state
    Given I navigate to marketing preferences hub via more menu
    When I toggle the internetPrefs marketing option
    And I submit my preferences
    And the following error message appears You need to update all of your marketing choices before you can proceed.
    Then I dismiss the preferences error message
