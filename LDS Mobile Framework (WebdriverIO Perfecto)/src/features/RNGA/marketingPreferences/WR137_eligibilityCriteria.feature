@team-pi @nga3 @rnga @stub-enrolled-youth @mbnaSuite @secondary
Feature: removing marketing prefrences link for inelligable users
  As a product owner
  I want to suppress marketing preferences for ineligible accounts
  So that they are not able to set preferences

  #MOB3-6739
  Scenario: TC_001 Youth customer doesn't have marketing preferences link
    Given I am an enrolled "youth customer" user logged into the app
    When I navigate to profile page via more menu
    Then the link to the marketing hub isn't displayed
