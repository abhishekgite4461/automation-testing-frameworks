@team-pi @nga3 @rnga @primary @instrument
@sw-marketing-consents-lloyds-off @sw-marketing-consents-halifax-off @sw-marketing-consents-bos-off
@sw-marketing-consents-mbna-off
Feature: Switching Off marketing preferences hub
  In Order to prevent customers from seeing the new marketing consents journey
  As an NGA Product Owner
  I want to be able to switch off the new Marketing consents journey

  #MOB3-6739
  @stub-enrolled-typical @mbnaSuite
  Scenario: TC_001 Switch off - Your Profile page entry point
    Given I am a marketing preferences user on the home page
    When I navigate to profile page via more menu
    Then the link to the marketing hub isn't displayed

  @stub-gdpr-bp-prefs-not-set @mbnaSuite
  Scenario: TC002 Switches Off - Lead
    Given I submit "correct" MI as "marketing preferences" user on the MI page
    When an interstitial lead is displayed
    And I select the marketing hub link in the lead
    Then I should be on the home page

    #MOB3-6744, MOB3-9569
  @bos @hfx @stub-gdpr-coservicing-pref-notset
  Scenario: TC003 Switches Off co-servicing account - Your Profile page entry point
    Given I am a coservicing account user on the home page
    When I navigate to profile page via more menu
    Then No marketing preferences links are displayed

  @bos @hfx @stub-gdpr-bp-coservicing-pref-notset
  Scenario: TC004 Switches Off co-servicing account - Lead
    Given I submit "correct" MI as "coservicing account" user on the MI page
    When an interstitial lead is displayed
    And I select the marketing hub link in the lead
    Then I should be on the home page
