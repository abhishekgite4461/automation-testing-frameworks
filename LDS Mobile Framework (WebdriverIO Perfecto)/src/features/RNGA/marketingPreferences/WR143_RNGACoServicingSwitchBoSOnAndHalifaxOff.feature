@team-pi @nga3 @rnga @stubOnly @sw-marketing-consents-halifax-off @primary @stub-gdpr-coservicing-pref-notset
Feature: OCIS Integration for Co-Servicing
  As a co servicing customer on RNGA
  I want to be able to set my preferences for both BoS and Halifax from the same app
  So that I can set different preferences for the different brands from the same app.

  #MOB3-6744, MOB3-9569
  Background:
    Given I am an enrolled "coservicing account" user logged into the app
    And I navigate to profile page via more menu

  # TC001 Verify marketing preference link when only secondary brand is switched on
  @hfx
  Scenario: TC001 Create marketing preferences when only secondary brand switch is turned on
    And Marketing preference links for only the secondary brand is visible
    And I select the marketing preferences link for the secondary brand
    When I opt in to all the marketing preferences for the secondary brand
    And I submit my preferences
    Then I'm presented with a Success message

  # TC003 Verify marketing preference links when logged on with the primary brand and secondary brand is switched on
  @bos
  Scenario: TC002 Create marketing preferences when logged on with the primary brand
    And Marketing preference links for only the primary brand is visible
    And I select the marketing preferences link for the primary brand
    When I opt in to all the marketing preferences for the primary brand
    And I submit my preferences
    Then I'm presented with a Success message
