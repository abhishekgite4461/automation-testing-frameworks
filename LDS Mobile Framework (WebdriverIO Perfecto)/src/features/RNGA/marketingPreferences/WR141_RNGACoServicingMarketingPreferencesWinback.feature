@team-pi @nga3 @rnga @bos @hfx @stub-gdpr-coservicing-pref-set
Feature: Winback for Co-servicing customers to marketing consents page
  As a customer
  I want to be warned if i'm leaving marketing preferences page before my changes have been saved
  So that my updated preferences aren't lost

  Background:
    Given I am a coservicing account user on the home page
    And I navigate to profile page via more menu

  @primary
  Scenario: TC001 Win back to primary preferences hub after back button
    Given I am on the primary marketing consents page for a co-serviced account
    And I toggle on the emailPrefs marketing option
    When I navigate back
    And I'm presented with a Winback option
    And I opt to stay on the marketing preferences page
    Then I must remain on the primary brand marketing consent page

  @android @secondary
  Scenario: TC002 Win back to secondary preferences hub after device back button
    Given I am on the secondary marketing consents page for a co-serviced account
    And I toggle on the emailPrefs marketing option
    When I select back key on the device
    And I'm presented with a Winback option
    And I opt to stay on the marketing preferences page
    Then I must remain on the secondary brand marketing consent page

  @primary
  Scenario: TC003 Win back to secondary preferences hub after back button
    Given I am on the secondary marketing consents page for a co-serviced account
    And I toggle on the emailPrefs marketing option
    When I navigate back
    And I'm presented with a Winback option
    And I opt to stay on the marketing preferences page
    Then I must remain on the secondary brand marketing consent page

  @secondary
  Scenario: TC004 Decline winback after using back button for secondary brand
    Given I am on the secondary marketing consents page for a co-serviced account
    And I toggle on the emailPrefs marketing option
    When I navigate back
    And I'm presented with a Winback option
    And I opt to leave the marketing preferences page
    Then I should be on the your personal details page

  @android @secondary
  Scenario: TC005 Decline winback after using device back button for Secondary brand
    Given I am on the secondary marketing consents page for a co-serviced account
    And I toggle the emailPrefs marketing option
    When I select back key on the device
    And I'm presented with a Winback option
    And I opt to leave the marketing preferences page
    Then I should be on the your personal details page

  @secondary @pending
  Scenario: TC006 Decline winback after selecting call us on bottom navigation bar for secondary brand
    Given I am on the secondary marketing consents page for a co-serviced account
    And I toggle on the emailPrefs marketing option
    When I select support tab from bottom navigation bar
    Then I'm presented with a Winback option
    When I opt to leave the marketing preferences page
    Then I should be on the call us home page
  #Note: This is yet to be resolved by the redesign team

  @primary @pending
  Scenario: TC007 Win back to preferences after selecting call us on bottom navigation bar for secondary brand
    Given I am on the secondary marketing consents page for a co-serviced account
    And I toggle on the emailPrefs marketing option
    When I select support tab from bottom navigation bar
    Then I'm presented with a Winback option
    When I opt to stay on the marketing preferences page
    Then I must remain on the secondary brand marketing consent page
  #Note: Winback from bottom navigation bar is yet to be resolved by the redesign team

  @primary @instrument @android
  Scenario: TC008 Winback on accordion section from primary brand
    Given I am on the primary marketing consents page for a co-serviced account
    When I select the link on the accordion section
    And I'm presented with a Winback option
    And I opt to stay on the marketing preferences page
    Then I must remain on the primary brand marketing consent page

  @secondary @instrument @android
  Scenario: TC009 Leave journey using the link on accordion section from secondary brand
    Given I am on the secondary marketing consents page for a co-serviced account
    When I select the link on the accordion section
    And I opt to leave the marketing preferences page
    Then I must be taken out of my journey to the external page

  #TODO: MQE-63
  @secondary
  Scenario: TC0010 No winback via bottom navigation bar if no changes are made from secondary brand
    Given I am on the secondary marketing consents page for a co-serviced account
    When I select support tab from bottom navigation bar
    Then I should be on the support hub page

  #TODO: MQE-63
  @secondary
  Scenario: TC0011 No winback via bottom navigation bar if no changes are made from secondary brand
    Given I am on the secondary marketing consents page for a co-serviced account
    When I select support tab from bottom navigation bar
    Then I should be on the support hub page
