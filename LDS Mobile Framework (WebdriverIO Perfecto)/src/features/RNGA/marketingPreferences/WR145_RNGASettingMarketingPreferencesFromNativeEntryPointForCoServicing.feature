@team-pi @nga3 @rnga @bos @hfx @primary
Feature: Selecting marketing consents from native entry page for a Co-Servicing account
  As a co servicing customer on RNGA
  I want to be able to set my preferences for both BoS and Halifax from the same app
  So that I can set different preferences for the different brands from the same app.

  #MOB3-6744, MOB3-9569
  Background:
    Given I am an enrolled "coservicing account" user logged into the app
    And I navigate to profile page via more menu

  #  TC001 Verify marketing preference links for both primary and secondary brand
  @stub-gdpr-coservicing-pref-notset @stubOnly
  Scenario: TC001 Create marketing preferences through the primary brand link
    Then The marketing preference links for both primary and secondary brands should be visible
    Given I select the marketing preferences link for the primary brand
    When I opt in to all the marketing preferences for the primary brand
    And I select continue to navigate to the secondary marketing preferences screen
    And I opt in to all the marketing preferences for the secondary brand
    And I submit my preferences
    Then I'm presented with a Success message

  @stub-gdpr-coservicing-pref-notset @stubOnly
  Scenario: TC002 Create marketing preferences through the secondary brand link
    Given I select the marketing preferences link for the secondary brand
    When I opt in to all the marketing preferences for the primary brand
    And I select continue to navigate to the secondary marketing preferences screen
    And I opt in to all the marketing preferences for the secondary brand
    And I submit my preferences
    Then I'm presented with a Success message

  @stub-gdpr-coservicing-pref-set
  Scenario: TC003 Update all marketing preferences for only primary brand
    Given I select the marketing preferences link for the primary brand
    And I'm on the marketing preferences hub
    When I opt in to all the marketing preferences for the primary brand
    And I submit my preferences
    Then I'm presented with a Success message

  @stub-gdpr-coservicing-pref-set
  Scenario: TC004 Update all marketing preferences for only secondary brand
    Given I select the marketing preferences link for the secondary brand
    And I'm on the marketing preferences hub
    When I opt in to all the marketing preferences for the secondary brand
    And I submit my preferences
    Then I'm presented with a Success message
