@team-pi @nga3 @rnga @instrument @mbnaSuite

Feature: setting marketing preferences via a lead
  As a business
  I want to be able to direct cutomers to the new marketing preferences hub
  So that I can encourage them to opt in ot out of the new marketing preferences.

  Background: Given I have been displayed an interstitial lead for Marketing Preferences
    Given I submit "correct" MI as "valid" user on the MI page

  #MOB3-6746, MOB3-6659
  @stub-gdpr-bp-prefs-not-set @primary
  Scenario: TC_001 Global menu button should be removed from marketing Preferences Hub
    Given an interstitial lead is displayed
    When I navigate to the marketing hub via the lead
    Then I must not see the Global Menu in the lead screen

  @stub-gdpr-bp-prefs-not-set @primary @android
  Scenario: TC_002 Device back button should be disabled after following a lead to the marketing Preferecens Hub
    And an interstitial lead is displayed
    And I navigate to the marketing hub via the lead
    When I select back key on the device
    Then I'm on the marketing hub page

  @stub-gdpr-bp-prefs-not-set @stubOnly @primary
  Scenario: TC_003 Creating Marketing Preferences from a lead
    Given an interstitial lead is displayed
    And I navigate to the marketing hub via the lead
    And my marketing prefences are in an unselected state
    When I opt in to all the marketing prefernces
    And I submit my preferences
    Then I'm presented with a Success message

  @stub-gdpr-bp-prefs-set @primary
  Scenario: TC_004 Return to accounts after updating my preferences
    Given an interstitial lead is displayed
    And I navigate to the marketing hub via the lead
    When I have succesfully updated my preferences
    And I select the option to return to accounts
    Then I should be on the home page

  @stub-gdpr-bp-prefs-set @secondary
  Scenario: TC_005 Navigating to my contact details section after updating my preferences
    Given an interstitial lead is displayed
    And I navigate to the marketing hub via the lead
    When I have succesfully updated my preferences
    And I select the option to return to my profile
    Then I should be on the your personal details page
