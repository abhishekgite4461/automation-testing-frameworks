@team-redesign @rnga @nga3 @primary @sw-card-management-off @stub-enrolled-typical @android
Feature: User accesses the bottom navigation bar
  In order to access the entry points to specific journeys
  As an NGA authenticated customer
  I want to view the options available on the bottom navigation bar

  @mbna
  Scenario: TC_001_Main Success: Customer accesses the more menu in mbna
    Given I am an enrolled "creditCard" user logged into the app
    When I select more tab from bottom navigation bar
    Then I should see the below options in the more menu
      | optionsInMorePage  |
      | yourProfile        |
      | inbox              |
      | settings           |
      | lostStolenCard     |
      | replacementCardPin |
      | logout             |

  Scenario: TC_002_Main Success: Customer accesses the more menu
    Given I am an enrolled "current" user logged into the app
    When I select more tab from bottom navigation bar
    Then I should see the below options in the more menu
      | optionsInMorePage  |
      | yourProfile        |
      | inbox              |
      | settings           |
      | lostStolenCard     |
      | replacementCardPin |
      | depositCheque      |
      | viewDepositHistory |
      | logout             |
