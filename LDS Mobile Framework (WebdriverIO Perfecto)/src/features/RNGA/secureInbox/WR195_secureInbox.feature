@team-outboundmessaging @nga3 @rnga @environmentOnly @mbnaSuite @noExistingData @uatOnly @pending
#MOB3-734
Feature: User wants to access Inbox and view unread correspondence from the bank
  In order to access correspondence from the bank
  As a RNGA Authenticated Customer
  I want to view my Inbox

  Background:
    Given I am an enrolled "valid non youth inbox" user logged into the app

  @primary
  Scenario: User Opens Inbox Webview from more menu
    When I navigate to inbox page via more menu
    Then I should be on the inbox webview page

  Scenario: Handling INBOXHUBPAGE metadata
    And I am on inbox home page
    When I select home or back button
    Then I should be on the home page

  Scenario: User Opens message
    And I am on inbox home page
    When I choose a correspondence option from the inbox home page
    Then I should be taken to the message
    And I should be presented with an option to download as a PDF

  Scenario: Handling INBOXLANDINGPAGE metadata
    And I am on message inbox page
    When I select the home or back button from the message inbox page
    Then I am on inbox home page

  Scenario: User has a notification of unread mail on the more menu
    When I read an unread items in my inbox
    Then I should see the notification counter updated in more menu according to how many unread items I have
    And the counter should not surpass the number 999

  Scenario: User has a notification of unread mail on inbox option in more menu
    When I have unread items in my inbox
    Then I should see the notification counter updated in inbox option according to how many unread items I have
    And the counter should not surpass the number 999

  Scenario: User has no notifications
    When all item in my inbox are read
    Then I should not see a notification in the more menu
    And I should not see a notification on inbox option in more menu

  Scenario: User downloads a PDF
    And I have opened a message in my Inbox
    When I choose to download the correspondence as a PDF from the inbox page
    Then PDF is downloaded to my device
    And I should see the notification counter updated in more menu according to how many unread items I have
