@team-performance @nga3 @rnga @environmentOnly @lds @hfx @primary

#MOB3-745
Feature: User wants to amend and delete standing order & direct debits
  In order to amend and delete my standing order & direct debits
  As a RNGA authenticated customer
  I want to be able to view standing order & direct debits within my app

  Background:
    Given I am an enrolled "standing order and direct debits eligible" user logged into the app

  Scenario: TC_001_User enters direct debit webview page from account tile menu options
    When I select the "direct debit" option in the action menu of current account
    Then I should be on the direct Debit webview page

  Scenario: TC_002_User enters standing orders webview page from account tile menu options
    When I select the "standing order" option in the action menu of current account
    Then I should be on the standing Order webview page
