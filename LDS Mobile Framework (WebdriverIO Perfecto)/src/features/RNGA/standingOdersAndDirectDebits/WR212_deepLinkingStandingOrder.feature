@team-performance @nga3 @rnga @environmentOnly @primary

#MOB3-2443
Feature: User wants to deep link standing order
  In Order to set up a new Standing order
  As a RNGA Authenticated Customer
  I want to be taken to the native payments hub page

  Background:
    Given I am an enrolled "standing order" user logged into the app

  Scenario: TC_001_User wants to deep link standing order
    When I select the "standing order" option in the action menu of current account
    And I select to set up a new standing order
    Then I should be on the payment hub page
