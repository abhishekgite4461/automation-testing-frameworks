@team-pi @sw-light-logon-off @nga3 @rnga @stub-enrolled-valid @mbnaSuite @outOfScope
#These Scenarios are never going to be implemented in LIVE, Checked manually as part of regression.
Feature: Customer Makes Unsecure Call with Light Logon Off

  Scenario Outline: AC01 TC_001_Customer calls Telephony agent in Unauth mode with Enable Secure call switch OFF
    Given I am an enrolled "current saving isa creditCard mortgage loan" user logged into the app
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    And I select the <account_type> tile on your accounts page
    When I select call us button in call us page
    Then I validate the call made for <account_type> is unsecure call
    Examples:
      | account_type         |
      | Credit account       |
      | Current account      |
      | Saving account       |
      | ISA account          |
      | Loan account         |
      | Term deposit account |

  Scenario Outline: AC02 TC_002_Customer with Current account is not able to make a secure call from Sales query page
    Given I am an enrolled "current saving isa creditCard mortgage loan" user logged into the app
    And I select support tab from bottom navigation bar
    And I select the new product enquire tile on call us home page
    When I select individual <sales_option> on new product page
    Then I validate the call made for <sales_option> is unsecure call
    Examples:
      | sales_option         |
      | New Current account  |
      | New Savings account  |
      | New ISA account      |
      | New credit account   |
      | New loan account     |
      | New Mortgage account |

  #MOB3-3434
  Scenario Outline: AC01 TC_003_Customer tries to call from a tablet or iPad with Light Logon OFF
    Given I am an enrolled "current" user logged into the app
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    And I select the <account_type> tile on your accounts page
    When I select call us button in call us page
    Then I should be informed of an alternative way of reaching customer service
    Examples:
      | account_type    |
      | Credit account  |
      | Savings account |
      | ISA account     |
      | Loan account    |
      | Current account |
