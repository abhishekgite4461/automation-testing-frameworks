@team-pi @nga3 @rnga @stub-no-mobile-number
Feature: Customer requested to register mobile number when they do not have one.
  In order to enquire about a LBG product
  As a RNGA Customer
  I should be advised to register my phone number when I don't have one

  @mbnaSuite @mobileOnly
  Scenario Outline: AC01 TC_001_Customer calls Telephony agent in un-auth mode with Light Logon & Enable Secure call switch ON when no mobile number registered
    Given I am an enrolled "no mobile number" user logged into the app
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    And I select the <account_type> tile on your accounts page
    When I select call us button in call us page
    Then I should be advised that I need to register a mobile phone number
    And I should be given an option to register a mobile phone
    And I should be given an option to continue with a non-authenticated call to Customer Services
    When I select the option to register a mobile phone number
    Then I should be shown the update phone number page
    Examples:
      | account_type         |
      | Credit account       |

  @tabletOnly @primary
  Scenario Outline: AC01 TC_002_Customer is displayed with alternate unsecure phone number for contacting Telephony agent
    Given I am an enrolled "no mobile number" user logged into the app
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    And I select the <account_type> tile on your accounts page
    When I select call us button in call us page
    Then I should see an alert pop up with alternate unsecure phone number for contacting
    Examples:
      | account_type         |
      | Savings account      |
      | ISA account          |
      | Loan account         |
      | Current account      |

  @mobileOnly @primary
  Scenario Outline: AC01 TC_003_Customer calls Telephony agent in un-auth mode with Light Logon & Enable Secure call switch ON when no mobile number registered
    Given I am an enrolled "no mobile number" user logged into the app
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    And I select the <account_type> tile on your accounts page
    When I select call us button in call us page
    Then I should be advised that I need to register a mobile phone number
    And I should be given an option to register a mobile phone
    And I should be given an option to continue with a non-authenticated call to Customer Services
    When I select the option to register a mobile phone number
    Then I should be shown the update phone number page
    Examples:
      | account_type         |
      | Savings account      |
      | ISA account          |
      | Loan account         |
      | Current account      |

  @secondary @mobileOnly @mbnaSuite
  Scenario Outline: AC01 TC_004_Customer calls Telephony agent in un-auth mode with Light Logon & Enable Secure call switch ON when no mobile number registered
    Given I am an enrolled "no mobile number" user logged into the app
    And I navigate to call us home page
    And I select the <option> tile on call us home page
    When I select call us button in call us page
    Then I should be advised that I need to register a mobile phone number
    And I should be given an option to register a mobile phone
    And I should be given an option to continue with a non-authenticated call to Customer Services
    When I select the option to register a mobile phone number
    Then I should be shown the update phone number page
    Examples:
      | option                |
      | Internet banking      |
      | Other banking query   |
      | Suspected fraud       |
      | Lost or stolen cards  |
      | Emergency cash abroad |

  @secondary @tabletOnly @mbnaSuite
  Scenario Outline: AC01 TC_005_Scenario Outline: AC01 TC_002_Customer is displayed with alternate unsecure phone number for contacting Telephony agent
    Given I am an enrolled "no mobile number" user logged into the app
    And I navigate to call us home page
    And I select the <option> tile on call us home page
    When I select call us button in call us page
    Then I should see an alert pop up with alternate unsecure phone number for contacting
    Examples:
      | option                |
      | Internet banking      |
      | Other banking query   |
      | Suspected fraud       |
      | Lost or stolen cards  |
      | Emergency cash abroad |
