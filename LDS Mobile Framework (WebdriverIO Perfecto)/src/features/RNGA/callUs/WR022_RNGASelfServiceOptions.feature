@team-pi @nga3 @rnga
Feature: Self service options for Lost or Stolen Queries, personal account , Other Banking and Internet Banking queries

  Background:
    Given I am an enrolled "current saving isa creditcard mortgage loan" user logged into the app
    And I navigate to call us home page

  #MOB3-3560, 3114
  @stub-enrolled-typical @mbnaSuite
  Scenario: AC01 TC_001_Customer accesses Call us page for Lost or Stolen queries
    When I select the Lost or stolen cards tile on call us home page
    Then I should be on the secure call page
    And I am shown a link for lost or stolen cards

  #MOB3-3110, 3559
  @stub-enrolled-typical @primary @mbnaSuite
  Scenario: AC01 TC_002_Customer accesses Call us page for Other Banking queries
    When I select the other banking query tile on call us home page
    Then I should be on the secure call page
    And I am shown the below self service options
      | serviceOption          |
      | Reset your password    |
      | Lost or stolen Cards   |
      | Replacement cards PINS |

  #MOB3-3110, 3559
  @stub-enrolled-typical @mbnaSuite
  Scenario: AC01 TC_003_Customer accesses Call us page for Internet Banking
    When I select the internet banking tile on call us home page
    Then I should be on the secure call page
    And I am shown the below self service options
      | serviceOption          |
      | Reset your password    |
      | Lost or stolen cards   |
      | Replacement cards PINS |

  #MOB3-3110, 3559
  @stub-enrolled-valid @mbnaSuite
  Scenario Outline: AC01 TC_004_Verify the in app link for Lost or stolen cards
    When I select the <tile_type> tile on call us home page
    And I select lost or stolen cards self service link
    Then I should be on the lost and stolen page
    Examples:
      | tile_type           |
      | Other banking query |
      | Internet banking    |

  #MOB3-3110, 3559
  @stub-enrolled-valid
  Scenario Outline: AC01 TC_005_Verify the in app link for Replacement cards and PINs
    When I select the <tile_type> tile on call us home page
    And I select replacement cards pins self service link
    Then I should be on the replacement card and pin page
    Examples:
      | tile_type           |
      | Other banking query |
      | Internet banking    |

  #MOB3-3110, 3559
  @stub-enrolled-typical @mbnaSuite
  Scenario Outline: AC01 TC_006_Verify the in app link for Reset your password
    When I select the <tile_type> tile on call us home page
    And I select reset your password self service link
    Then I am taken to reset your password page in the app
    Examples:
      | tile_type           |
      | Other banking query |
      | Internet banking    |

  #MOB3-3110, MOB3-3559
  @stub-enrolled-valid
  Scenario Outline: AC01 TC_007_Verify the in app link for Lost or stolen cards from individual accounts
    When I select your accounts tile on call us home page
    And I select the <account_type> tile on your accounts page
    Then I should be on the secure call page
    When I select lost or stolen cards self service link
    Then I should be on the lost and stolen page
    Examples:
      | account_type          |
      | Current account       |
      | Savings account       |
      | ISA account           |
      | Credit account        |
      | Mortgage UFSS account |
      | Loan account          |

  #MOB3-3110, MOB3-3559
  @stub-enrolled-valid
  Scenario Outline: AC01 TC_008_Verify the in app link for Replacement cards and PINs from individual accounts
    When I select your accounts tile on call us home page
    And I select the <account_type> tile on your accounts page
    Then I should be on the secure call page
    When I select replacement cards pins self service link
    Then I should be on the replacement card and pin page
    Examples:
      | account_type          |
      | Current account       |
      | Savings account       |
      | ISA account           |
      | Credit account        |
      | Mortgage UFSS account |
      | Loan account         |

  #MOB3-3110, MOB3-3559
  @stub-enrolled-typical @secondary @mbnaSuite
  Scenario Outline: AC01 TC_009_Verify the in app link for Reset your password from individual accounts
    When I select your accounts tile on call us home page
    And I select the <account_type> tile on your accounts page
    Then I should be on the secure call page
    When I select reset your password self service link
    Then I am taken to reset your password page in the app
    Examples:
      | account_type          |
      | Credit account        |

  @stub-enrolled-valid @secondary
  Scenario Outline: AC01 TC_009_Verify the in app link for Reset your password from individual accounts
    When I select your accounts tile on call us home page
    And I select the <account_type> tile on your accounts page
    Then I should be on the secure call page
    When I select reset your password self service link
    Then I am taken to reset your password page in the app
    Examples:
      | account_type          |
      | Current account       |
      | Savings account       |
      | ISA account           |
      | Mortgage UFSS account |
      | Loan account          |

  #MOB3-3572, MOB3-3071
  @stub-enrolled-typical
  Scenario: AC01 TC_010_Customer accesses Call us page for Medical Assistance Abroad
    When I select the medical assistance abroad tile on call us home page
    Then I should be on the secure call page
    And I am not shown the below self service options
      | serviceOption          |
      | Reset your password    |
      | Lost or stolen cards   |
      | Replacement cards PINS |
