# JIRA story id MOB3-3565
# mobile number is registered and has not been modified in last 48 hrs (commented as this cannot be automated)
# To make secure call, customer must hold one of the following accounts - Current, Savings, ISA or Credit Card
# Scenario from ticket MOB3-3565. Assumes customer does NOT hold any current, savings or credit card account

@team-pi @nga3 @rnga @mobileOnly @stubOnly
Feature: Customer Makes unsecure Call
  In order to resolve my query quickly
  As a Retail NGA User
  I want to make a call to Customer service

  @stub-arrangements-loan-only
  Scenario Outline: AC03 TC_001_Customer with only loans account can only make un-auth calls even with Light Logon & Enable Secure call switch ON
    Given I am an enrolled "loan only" user logged into the app
    And I navigate to call us home page
    And I select the <option> tile on call us home page
    When I select call us button in call us page
    Then I validate the call made for <option> is unsecure call
    Examples:
      | option                |
      | Internet banking      |
      | Other banking query   |
      | Suspected fraud       |
      | Lost or stolen cards  |
      | Emergency cash abroad |

  @stub-arrangements-mortgage-only
  Scenario Outline: AC03 TC_002_Customer with only mortgage account can only make un-auth calls even with Light Logon & Enable Secure call switch ON
    Given I am an enrolled "mortgage only" user logged into the app
    And I navigate to call us home page
    And I select the <option> tile on call us home page
    When I select call us button in call us page
    Then I validate the call made for <option> is unsecure call
    Examples:
      | option                |
      | Internet banking      |
      | Other banking query   |
      | Suspected fraud       |
      | Lost or stolen cards  |
      | Emergency cash abroad |

  #MOB3-3572
  @stub-enrolled-valid
  Scenario: AC03 TC_003_Medical assistance related calls are always un-auth even with Light Logon and Enable Secure call switch ON & mobile number unchanged for the last 48 hours
    Given I am an enrolled "current" user logged into the app
    And I navigate to call us home page
    And I select the medical assistance abroad tile on call us home page
    Then I should be on the secure call page
    When I select call us button in call us page
    Then I validate the call made for medical assistance abroad is unsecure call

  #MOB3-5862 #MOB3-4584
  @primary @stub-enrolled-valid
  Scenario Outline: TC_004_Customer makes a unsecure call in non-logged mode (Non-auth call)
    Given I am an enrolled "current" user on the MI page
    And I select the contact us option from the MI page pre auth header
    And I select the <option> tile on call us home page
    And I should be on the unsecure call us page
    When I select the phone number on unsecure call us page
    Then I validate the call made for <option> is unsecure call
    Examples: Call us page options
      | option                    |
      | Personal accounts         |
      | Internet banking          |
      | Other banking query       |
      | Suspected fraud           |
      | Lost or stolen cards      |
      | Emergency cash abroad     |
      | Medical assistance abroad |

  @primary @stub-enrolled-valid @mbnaSuite @mbna
  Scenario Outline: TC_004_Customer makes a unsecure call in non-logged mode (Non-auth call)
    Given I am an enrolled "current" user on the MI page
    And I select the contact us option from the MI page pre auth header
    And I select the <option> tile on call us home page
    And I should be on the unsecure call us page
    When I select the phone number on unsecure call us page
    Then I validate the call made for <option> is unsecure call
    Examples: Call us page options
      | option                    |
      | Personal accounts         |
      | Internet banking          |
      | Other banking query       |
      | Suspected fraud           |
      | Lost or stolen cards      |
      | Emergency cash abroad     |

  @stub-arrangements-mortgage-only
  Scenario Outline: AC01 TC_005_ Customer holds only mortgage account and tries to make a call regarding change of address with Secure call and  Light Logon switch ON
    Given I am an enrolled "mortgage only" user logged into the app
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    And I select the <account_type> tile on your accounts page
    When I select call us button in call us page
    Then I validate the call made for mortgage account is unsecure call
    Examples:
      | account_type          |
      | Mortgage Ufss Account |

  @stub-arrangements-loan-only
  Scenario Outline: AC01 TC_006_ Customer holds only Loan account and tries to make a call regarding change of address with Secure call and  Light Logon switch ON
    Given I am an enrolled "loan only" user logged into the app
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    And I select the <account_type> tile on your accounts page
    When I select call us button in call us page
    Then I validate the call made for loan account is unsecure call
    Examples:
      | account_type     |
      | Loan account     |
