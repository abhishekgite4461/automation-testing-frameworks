# JIRA stories: MOB3-2647, MOB3-3565
# Mobile number is registered and has not been modified in the last 48 hrs (this cannot be automated)
# To make a secure call, the customer must hold one of the following accounts - Current, Savings, ISA or Credit Card

@team-pi @nga3 @rnga @sw-coa-native-on
Feature: Customer Makes Secure Call
  In order to resolve my query quickly
  As a Retail NGA User
  I want to make an authenticated call to Customer service

  @primary @stub-enrolled-valid
  Scenario Outline: AC01 TC_001_Customer calls Telephony agent in Auth mode with Light Logon & Enable Secure call switch ON
    Given I am an enrolled "current and savings and creditcard " user logged into the app
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    And I select the <account_type> tile on your accounts page
    When I select call us button in call us page
    Then I validate the call made for <account_type> is secure call
    Examples:
      | account_type         |
      | Savings account      |
      | ISA account          |
      | Loan account         |
      | Current account      |
      | Term deposit account |

  @primary @mbna @stub-enrolled-valid
  Scenario Outline: AC01 TC_001_Customer calls Telephony agent in Auth mode with Light Logon & Enable Secure call switch ON
    Given I am an enrolled "current and savings and creditcard " user logged into the app
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    And I select the <account_type> tile on your accounts page
    When I select call us button in call us page
    Then I validate the call made for <account_type> is secure call
    Examples:
      | account_type         |
      | Credit account       |

  #MOB3-3434
  @tabletOnly @mbnaSuite @stub-enrolled-valid
  Scenario Outline: AC01 TC_002_Customer tries to call from a tablet or iPad with Light Logon & Enable Secure call switch ON
    Given I am an enrolled "current" user logged into the app
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    And I select the <account_type> tile on your accounts page
    When I select call us button in call us page
    Then I should see an alert pop up with alternate unsecure phone number for contacting
    Examples:
      | account_type         |
      | Credit account       |
      | Savings account      |
      | ISA account          |
      | Loan account         |
      | Current account      |
      | Term deposit account |

  @manualOnly @stub-enrolled-valid
  # MI reporting - not for QE scope. PO will test this statistics
  Scenario Outline: AC02 TC_003_Check if correct details are logged in OTP System when secure call is made
    Given I am an enrolled "current" user logged into the app
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    And I select the <account_type> tile on your accounts page
    When I click on call us button in secure call page
    Then device overlay screen should be displayed with key lock icon and message on secure call
    And my primary account details are logged against the call in LBG system
    And the click on <account_type> is captured for MI reporting
    Examples:
      | account_type         |
      | Credit account       |
      | Savings account      |
      | Current account      |
      | ISA account          |
      | Loan account         |
      | Term deposit account |

  @primary @mbnaSuite @stub-mobile-number-changed-in-grace-period
  Scenario Outline: AC04 TC_004_Customer makes unsecure call for an account when number changed in last 48 hrs and Light Logon & Enable Secure call switch is ON
    Given I am an enrolled "current" user logged into the app
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    And I select the <account_type> tile on your accounts page
    When I select call us button in call us page
    Then I validate the call made for <account_type> is unsecure call
    Examples:
      | account_type         |
      | Credit account       |
      | Savings account      |
      | Current account      |

  #Scenario from ticket MOB3-3565. Assumes customer holds current, savings or credit card account
  @mbnaSuite @stub-enrolled-valid
  Scenario Outline: AC02 TC_005_Customer calls Telephony agent in Auth mode with Light Logon & Enable Secure call switch ON for non-account queries
    Given I am an enrolled "current and savings and creditcard" user logged into the app
    And I navigate to call us home page
    And I select the <option> tile on call us home page
    When I select call us button in call us page
    Then I validate the call made for <option> is secure call
    Examples:
      | option                |
      | Internet banking      |
      | Other banking query   |
      | Suspected fraud       |
      | Lost or stolen cards  |
      | Emergency cash abroad |

  @stub-enrolled-valid
  Scenario: AC01 TC_005_ Customer holds only current account and tries to make a call regarding change of address with Secure call and  Light Logon switch ON
    Given I am an enrolled "current" user logged into the app
    And I select support tab from bottom navigation bar
    When I select "change of address" option in the support hub page
    Then I should be on the your personal details page
    And I choose to update address from the personal details
    And I should see an option to search my new address using postcode
    When I proceed to call customer care
    Then I should be on the secure call page
    And I select call us button in the update your address page
    And I validate the call made for COA is secure call
