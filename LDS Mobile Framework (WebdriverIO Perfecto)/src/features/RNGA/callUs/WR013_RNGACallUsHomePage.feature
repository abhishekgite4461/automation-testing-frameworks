@team-pi @nga3 @rnga
Feature: Call us Home Page
  In order to get a quick resolution for my Query
  As a RNGA Customer
  I want to see options to contact specific customer services team

  @mbnaSuite @stub-enrolled-typical
  Scenario: Verify scroll back to top behaviour when support tab bar is selected
    Given I am a current user on the home page
    When I select support tab from bottom navigation bar
    Then I should be on the support hub page
    And I scroll down to bottom of support hub page
    When I select support tab from bottom navigation bar
    Then I am navigated to support hub page title

  #MOB3-2655
  @stub-enrolled-typical @primary
  Scenario: AC01 TC_001 Main Success Scenario : Non youth Account call us page options from Support Hub
    Given I am a current user on the home page
    When I navigate to call us home page
    Then I should be able to view these call us tiles
      | callUsTiles           |
      | yourAccountsTile      |
      | newProductEnquireTile |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should see security and travel emergency label
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
      | medicalAssistAbroadTab |

  @stub-enrolled-youth @primary
  Scenario: AC01 TC_002 Main Success Scenario : Youth Account call us page options from Support Hub
    Given I am a youth user on the home page
    When I navigate to call us home page
    Then I should not see new product enquiries tile
    And I should be able to view these call us tiles
      | callUsTiles           |
      | yourAccountsTile      |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should see security and travel emergency label
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
      | medicalAssistAbroadTab |

  #MOB3-3786 #MOB3-3500 #MOB3-5862
  @stub-enrolled-typical @primary
  Scenario: TC_003 Main Success Scenario : Call us page options when not logged into the App in the MI page
    Given I am a valid user on the MI page
    When I select the contact us option from the MI page pre auth header
    Then I should be on pre auth call us home page overlay
    And I should be able to view these call us tiles
      | callUsTiles           |
      | personalAccountsTile  |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should be able to view close button on call us page
    And I should see security and travel emergency label
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
      | medicalAssistAbroadTab |

  @stub-valid
  Scenario: TC_004 Main Success Scenario : Call us page options when not logged into the App in the login page
    Given I am a valid user
    And I navigate to the login screen
    When I select the contact us option from the login page pre auth header
    Then I should be on pre auth call us home page overlay
    And I should be able to view these call us tiles
      | callUsTiles           |
      | personalAccountsTile  |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should be able to view close button on call us page
    And I should see security and travel emergency label
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
      | medicalAssistAbroadTab |

  @stub-valid
  Scenario: TC_005 Main Success Scenario : Call us page options when not logged into the App in the welcome page
    Given I am a valid user
    When I select the contact us option from the welcome page pre auth header
    Then I should be on pre auth call us home page overlay
    And I should be able to view these call us tiles
      | callUsTiles           |
      | personalAccountsTile  |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should be able to view close button on call us page
    And I should see security and travel emergency label
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |
      | medicalAssistAbroadTab |

    #MOB3-3786 #MOB3-3500 #MOB3-5862
  @stub-enrolled-typical @primary @mbna @mbnaSuite
  Scenario: TC_003 Main Success Scenario : Call us page options when not logged into the App in the MI page
    Given I am a valid user on the MI page
    When I select the contact us option from the MI page pre auth header
    Then I should be on pre auth call us home page overlay
    And I should be able to view these call us tiles
      | callUsTiles           |
      | personalAccountsTile  |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should be able to view close button on call us page
    And I should see security and travel emergency label
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |

  @stub-valid @mbna @mbnaSuite
  Scenario: TC_004 Main Success Scenario : Call us page options when not logged into the App in the login page
    Given I am a valid user
    And I navigate to the login screen
    When I select the contact us option from the login page pre auth header
    Then I should be on pre auth call us home page overlay
    And I should be able to view these call us tiles
      | callUsTiles           |
      | personalAccountsTile  |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should be able to view close button on call us page
    And I should see security and travel emergency label
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |

  @stub-valid @mbna @mbnaSuite
  Scenario: TC_005 Main Success Scenario : Call us page options when not logged into the App in the welcome page
    Given I am a valid user
    When I select the contact us option from the welcome page pre auth header
    Then I should be on pre auth call us home page overlay
    And I should be able to view these call us tiles
      | callUsTiles           |
      | personalAccountsTile  |
      | internetBankingTile   |
      | otherBankingQueryTile |
    And I should be able to view close button on call us page
    And I should see security and travel emergency label
    And I should see the reasons to call options
      | reasonsToCall          |
      | suspectedFraudTab      |
      | lostOrStolenCardTab    |
      | emergencyCashAbroadTab |

  @stub-invalid-accounts @ios
  #iOS Tag can be removed once Android dev is done
  Scenario: TC_005 Main Success Scenario : Call us page options when not logged into the App in the welcome page
    Given I am a invalid accounts user on the home page
    When I navigate to call us home page
    Then I should not see your accounts tile in the call us page
