@team-pi @nga3 @rnga
Feature: Display list of LBG customer relationship accounts for making a call

  Background:
    Given I am an enrolled "current saving isa creditcard mortgage loan" user logged into the app
    And I navigate to call us home page
    When I select your accounts tile on call us home page

  @stub-enrolled-valid @stubOnly
  Scenario: AC01 TC_001_Customer with all account types
    Then I should be shown below accounts I hold
      | accountTypes          |
      | Current account       |
      | Savings account       |
      | ISA account           |
      | Loan account          |
      | Mortgage UFSS account |

  @stub-enrolled-typical @stubOnly @mbnaSuite @mbna
  Scenario: AC01 TC_001_Customer with all account types
    Then I should be shown below accounts I hold
      | accountTypes          |
      | Credit account        |

  # This scenario is pending availability of co-service account test data
  # @pending - once the whole feature is ready, change back to original state as pending
  @manual
  Scenario: AC08 TC_008_Halifax Customer holds Co-service account with BoS
    Then I should be shown my current account with Halifax
    And I should be also shown the BoS account

  @manualOnly
  # MI reporting is required
  Scenario: AC09: TC_009_Update MI reporting in view of personal account
    And the click on account is captured for MI reporting
