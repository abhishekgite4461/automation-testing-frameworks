@team-pi @nga3 @rnga @mobileOnly
Feature: Call Customer Service for Sales queries
  In order to enquire about a LBG product
  As a RNGA Customer
  I want the option to speak to Sales Team

  @secondary @stub-enrolled-typical
  Scenario: AC01 TC_001_Customer selects one of the options on New product enquiries page & validates the callus page
    Given I am an enrolled "current and savings and isa" user logged into the app
    And I navigate to call us home page
    When I select new product enquiry tile on call us home page
    Then I am shown the list of sales option
      | salesOptions          |
      | newCurrentAccount     |
      | newSavingsAccount     |
      | newIsaAccount         |
      | newCreditAccount      |
      | newLoanAccount        |
      | newMortgagesAccount   |
    And I select individual sales option on new product page and validate callus page
      | salesOptions          |
      | newCurrentAccount     |
      | newSavingsAccount     |
      | newIsaAccount         |
      | newCreditAccount      |
      | newLoanAccount        |
      | newMortgagesAccount   |

  #doing changes as per PI-868, the screens in ios have changed after select new product enquiry tile
  @secondary @mbnaSuite @stub-enrolled-typical @android
  Scenario: AC02 TC_002_Customer selects one of the options on New product enquiries page & validates the callus page
    Given I am an enrolled "creditCard" user logged into the app
    And I navigate to call us home page
    When I select new product enquiry tile on call us home page
    Then I am shown the list of sales option
      | salesOptions          |
      | newCreditAccount      |
    And I select individual sales option on new product page and validate callus page
      | salesOptions          |
      | newCreditAccount      |

  #MCC-1487 For optimising call scenarios
  @stub-enrolled-valid
  Scenario Outline: AC03 TC_003_Customer with Current Savings and Credit account is able to make a secure call from New product enquire page
    Given I am an enrolled "current and savings and creditcard" user logged into the app
    And I navigate to call us home page
    And I select new product enquiry tile on call us home page
    And I select individual <sales_option> on new product page
    When I select call us button in call us page
    Then I validate the call made for <sales_option> is secure call
    Examples:
      | sales_option        |
      | newCurrent account  |
      | newSavings account  |
      | newISA account      |
      | newCredit account   |
      | newLoan account     |

  #MCC-1487 For optimising call scenarios
  @stub-arrangements-loan-only
  Scenario Outline: AC04 TC_004_Customer with only Loan account is not able to make a secure call from New product enquire page
    Given I am an enrolled "loan" user logged into the app
    And I navigate to call us home page
    And I select new product enquiry tile on call us home page
    And I select individual <sales_option> on new product page
    When I select call us button in call us page
    Then I validate the call made for <sales_option> is unsecure call
    Examples:
      | sales_option     |
      | newCurrent account  |
      | newSavings account  |
      | newISA account      |
      | newCredit account   |
      | newLoan account     |
      | newMortgages account |

  #MCC-1487 For optimising call scenarios
  @stub-arrangements-mortgage-only
  Scenario Outline: AC05 TC_005_Customer with only Mortgage account is not able to make a secure call from  product enquire page
    Given I am an enrolled "Mortgage" user logged into the app
    And I navigate to call us home page
    And I select new product enquiry tile on call us home page
    And I select individual <sales_option> on new product page
    When I select call us button in call us page
    Then I validate the call made for <sales_option> is unsecure call
    Examples:
      | sales_option     |
      | newCurrent account  |
      | newSavings account  |
      | newISA account      |
      | newCredit account   |
      | newLoan account     |
      | newMortgages account |

  #doing changes as per PI-868, the screens in ios have changed after select new product enquiry tile
  @mbna @stub-enrolled-typical @android
  Scenario Outline: AC05 TC_005_MBNA customer with credit card account is not able to make a secure call from New product enquire page
    Given I am an enrolled "creditCard" user logged into the app
    And I navigate to call us home page
    And I select new product enquiry tile on call us home page
    And I select individual <sales_option> on new product page
    When I select call us button in call us page
    Then I validate the call made for <sales_option> is secure call
    Examples:
      | sales_option     |
      | newCredit account   |
