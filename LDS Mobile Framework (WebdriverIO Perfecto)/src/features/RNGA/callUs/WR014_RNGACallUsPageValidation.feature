@team-pi @nga3 @rnga
Feature: Customer lands on Call Us Secure page after selecting Individual Account
  In order to get a quick resolution for my Query
  As a RNGA Customer
  I want to see options to contact specific customer services team

  #---------------------------------------Main Success scenario---------------------------------------#
  #MOB3-2579, 3559
  @primary @stub-enrolled-valid
  Scenario Outline: AC01 TC_001 Main Success Scenario : Customer lands on call us page after selecting arrangement
    Given I am an enrolled "current and savings and isa" user logged into the app
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    When I select the <account_type> tile on your accounts page
    Then I should be on the secure call page
    And I am able to view the account number and sort code
    And I am able to view the call us button
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    And I am shown the below self service options
      | serviceOption          |
      | Reset your password    |
      | Lost or stolen cards   |
      | Replacement cards PINS |
    Examples:
      | account_type    |
      | Current account |
      | Savings account |
      | ISA account     |

  #MOB3-2579, 3559
  @stub-enrolled-valid
  Scenario Outline: AC01 TC_002 Main Success Scenario : Customer lands on call us page after selecting arrangement
    Given I am an enrolled "current saving isa creditcard mortgage loan" user logged into the app
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    When I select the <account_type> tile on your accounts page
    Then I should be on the secure call page
    And I am able to view the reference number
    And I am able to view the call us button
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    And I am shown the below self service options
      | serviceOption          |
      | Reset your password    |
      | Lost or stolen cards   |
      | Replacement cards PINS |
    And I am able to view the global menu
    Examples:
      | account_type          |
      | Credit account        |
      | Mortgage UFSS account |
      | Loan account          |

  #MOB3-3565, MOB3-3572
  @secondary @stub-enrolled-typical
  Scenario Outline: AC01 TC_003 Main Success Scenario : Customer selects servicing query not related to an account
    Given I am an enrolled "current" user logged into the app
    And I navigate to call us home page
    When I select the <option> tile on call us home page
    Then I should be on the secure call page
    And I am able to view the call us button
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    Examples:
      | option                    |
      | Internet banking          |
      | Other banking query       |
      | Suspected fraud           |
      | Lost or stolen cards      |
      | Emergency cash abroad     |

  #MOB3-3786 #MOB3-3500 #MOB3-5862
  @stub-enrolled-typical @mbnaSuite
  Scenario Outline: TC_004 Main Success Scenario : Customer selects servicing query related to an account or service option when not logged into the App in MI page
    Given I am an enrolled "valid" user on the MI page
    When I select the contact us option from the MI page pre auth header
    And I select the <option> tile on call us home page overlay
    Then I should be on the unsecure call us page overlay
    And I am able to view the phone number button to call
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    Examples:
      | option                     |
      | Personal accounts          |
      | Internet banking           |
      | Other banking query        |
      | Suspected fraud            |
      | Lost or stolen cards       |
      | Emergency cash abroad      |

  #MORE-30
  @stub-valid @mbnaSuite
  Scenario Outline: TC_005 Main Success Scenario : Customer selects servicing query related to an account or service option when not logged into the App in Login page
    Given I am a valid user
    And I navigate to the login screen
    When I select the contact us option from the login page pre auth header
    And I select the <option> tile on call us home page overlay
    Then I should be on the unsecure call us page overlay
    And I am able to view the phone number button to call
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    Examples:
      | option                     |
      | Personal accounts          |
      | Internet banking           |
      | Other banking query        |
      | Suspected fraud            |
      | Lost or stolen cards       |
      | Emergency cash abroad      |

  #MORE-30
  @stub-valid
  Scenario: TC_006 Main Success Scenario : Customer selects servicing query related to an account or service option when not logged into the App in Login page
    Given I am a valid user
    And I navigate to the login screen
    When I select the contact us option from the login page pre auth header
    And I select the Medical assistance abroad tile on call us home page overlay
    Then I should be on the unsecure call us page overlay
    And I am able to view the phone number button to call

  @stub-valid @mbnaSuite
  Scenario Outline: TC_007 Main Success Scenario : Customer selects servicing query related to an account or service option when not logged into the App in Welcome page
    Given I am a valid user
    When I select the contact us option from the welcome page pre auth header
    And I select the <option> tile on call us home page overlay
    Then I should be on the unsecure call us page overlay
    And I am able to view the phone number button to call
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    Examples:
      | option                     |
      | Personal accounts          |
      | Internet banking           |
      | Other banking query        |
      | Suspected fraud            |
      | Lost or stolen cards       |
      | Emergency cash abroad      |

  #MORE-30
  @stub-valid
  Scenario Outline: TC_008 Main Success Scenario : Customer should be able to go back to the call us home page from the call us number page
    Given I am a valid user
    And I navigate to the login screen
    And I select the contact us option from the login page pre auth header
    And I select the <option> tile on call us home page overlay
    When I select the back option from the unsecure call us page overlay
    Then I should be on pre auth call us home page overlay
    Examples:
      | option                     |
      | Personal accounts          |
      | Internet banking           |
      | Other banking query        |
      | Suspected fraud            |
      | Lost or stolen cards       |
      | Emergency cash abroad      |
      | Medical assistance abroad  |

  #MORE-30
  @stub-enrolled-typical
  Scenario Outline: TC_009 Main Success Scenario : Customer should be able to close from the call us number page
    Given I am an enrolled "valid" user on the MI page
    And I select the contact us option from the MI page pre auth header
    And I select the <option> tile on call us home page overlay
    When I select the close option from the unsecure call us page overlay
    Then I should be on enter MI page
    Examples:
      | option                     |
      | Personal accounts          |
      | Internet banking           |
      | Other banking query        |
      | Suspected fraud            |
      | Lost or stolen cards       |
      | Emergency cash abroad      |
      | Medical assistance abroad  |

  #MORE-30
  @stub-enrolled-typical @mbna @mbnaSuite
  Scenario Outline: TC_009 Main Success Scenario : Customer should be able to close from the call us number page
    Given I am an enrolled "valid" user on the MI page
    And I select the contact us option from the MI page pre auth header
    And I select the <option> tile on call us home page overlay
    When I select the close option from the unsecure call us page overlay
    Then I should be on enter MI page
    Examples:
      | option                     |
      | Personal accounts          |
      | Internet banking           |
      | Other banking query        |
      | Suspected fraud            |
      | Lost or stolen cards       |
      | Emergency cash abroad      |

  @stub-valid @mbna @mbnaSuite
  Scenario Outline: TC_008 Main Success Scenario : Customer should be able to go back to the call us home page from the call us number page
    Given I am a valid user
    And I navigate to the login screen
    And I select the contact us option from the login page pre auth header
    And I select the <option> tile on call us home page overlay
    When I select the back option from the unsecure call us page overlay
    Then I should be on pre auth call us home page overlay
    Examples:
      | option                     |
      | Personal accounts          |
      | Internet banking           |
      | Other banking query        |
      | Suspected fraud            |
      | Lost or stolen cards       |
      | Emergency cash abroad      |

  @stub-enrolled-valid @mbna @mbnaSuite
  Scenario Outline: AC01 TC_002 Main Success Scenario : Customer lands on call us page after selecting arrangement
    Given I am an enrolled "current saving isa creditcard mortgage loan" user logged into the app
    And I navigate to call us home page
    And I select your accounts tile on call us home page
    When I select the <account_type> tile on your accounts page
    Then I should be on the secure call page
    And I am able to view the reference number
    And I am able to view the call us button
    And I am able to view the below information labels
      | labelName      |
      | callTimeLabel  |
      | textPhoneLabel |
    And I am shown the below self service options
      | serviceOption          |
      | Reset your password    |
      | Lost or stolen cards   |
      | Replacement cards PINS |
    And I am able to view the global menu
    Examples:
      | account_type          |
      | Credit account        |
