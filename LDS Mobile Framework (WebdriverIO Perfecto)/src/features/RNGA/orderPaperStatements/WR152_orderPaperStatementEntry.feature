@team-performance @nga3 @rnga @environmentOnly @lds

#MOB3-8868 #MOB3-8869
Feature: User wants to view a physical copy of card statement
  In order to view a physical copy of my card statement from the bank
  As a RNGA authenticated customer
  I want to be able to order a paper statement from my app

  Scenario: TC_001_User uses entry point from current account action Menu
    Given I am an enrolled "order paper statements eligible" user logged into the app
    And I select current account tile from home page
    When I select the "order paper statements" option in the action menu of current account
    Then I should be able to view order paper statements webview page
