@team-performance @nga3 @rnga @environmentOnly @manual

#MOB3-8873 #MOB3-8874
Feature: User wants to view a physical copy of card statement
  In Order to view a physical copy of my card statement from the bank
  As a RNGA Authenticated Customer
  I want to be able to order a paper statement from my app

  Background:
    Given I am an enrolled "valid" user logged into the app
    And I choose order paper statements option from the current account action menu
    And I select and confirm date from the native OS date filter

  Scenario: TC_001_Display winback popup when user chooses back option from confirm order statements page
    When I choose home or back option from confirm order paper statements page
    Then I should see a winback option on top of the screen with Cancel and OK options
    And By selecting the Cancel option, I should stay on the same screen

  Scenario: TC_002_Display winback popup when user chooses home option from confirm order statements page
    When I choose home or back option from confirm order paper statements page
    Then I should see a winback option on top of the screen with Cancel and OK options
    And By selecting the OK option, I should navigate to homepage

  Scenario: TC_003_User previews order paper statement and successfully orders statement
    When I select Confirm button from confirm order paper statement page
    Then I should be on the success order paper statement page
