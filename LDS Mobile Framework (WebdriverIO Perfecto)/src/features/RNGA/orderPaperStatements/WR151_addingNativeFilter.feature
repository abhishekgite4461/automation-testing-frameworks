@team-performance @nga3 @rnga @environmentOnly @manual

#MOB3-8871 #MOB3-8873
Feature: User wants to view a physical copy of card statement
  In Order to view a physical copy of my card statement from the bank
  As a RNGA Authenticated Customer
  I want to filter on specific dates so I can order the statement I need

  Scenario: TC_001_User wants to filter on specific dates to order the statement
    Given I am an enrolled "valid" user logged into the app
    When I choose order paper statements option from the current account action menu
    Then I should be able to choose date
    And I should be able to click on confirm button
