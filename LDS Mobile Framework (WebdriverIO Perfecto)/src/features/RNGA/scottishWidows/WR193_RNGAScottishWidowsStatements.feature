@nga3 @rnga @team-mco @stub-scottish-widows-2-accounts
Feature: In order to view more details on my SW pension
As a customer
I want to see some basic information in the statements view

  Background:
    Given I am an enrolled "Scottish Widows" user logged into the app

  @primary
  Scenario: TC01 - Scottish Widows Statement View
    When I select scottishWidows account tile from home page
    Then I should see a statement page with information about scottish widows pensions

  @secondary
  Scenario: TC03 - Navigating to FAQ
    And I select scottishWidows account tile from home page
    When I select the more information button on the statement view
    Then I should see the Scottish widows FAQ page

  @secondary
  Scenario: TC04 - Returning to statement view via global back button
    And I select scottishWidows account tile from home page
    And I navigate to the scottish widows FAQ Page
    When I navigate back
    Then I should see a statement page with information about scottish widows pensions

  @android @secondary
  Scenario: TC05 - Returning to statement view via native back button
    And I select scottishWidows account tile from home page
    And I navigate to the scottish widows FAQ Page
    When I select back key on the device
    Then I should see a statement page with information about scottish widows pensions

  @secondary
  Scenario: TC06 - Suppress Global Menu on FAQ page
    Given I select scottishWidows account tile from home page
    When I navigate to the scottish widows FAQ Page
    Then The Global Menu should not appear

  #TODO : defect found in Android - JIRA NO :- PANM-1190
  @ios
  Scenario: TC07 - verify scottish widows help desk number is visible in FAQ page
    And I select scottishWidows account tile from home page
    When I navigate to the scottish widows FAQ Page
    Then I should see the scottish widows help desk number under What can I do next option
