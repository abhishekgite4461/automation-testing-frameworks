@team-mco @nga3 @rnga @stub-scottish-widows-2-accounts @ff-pension-transfer-off @primary
Feature: SW Pensions Transfer Entry Point
  As an rnga customer with a eligible SW Pensions account
  I do not want to see "Pension Transfer" entry point from SW Pensions account action menu
  So that I am not able to carry out SW Pensions Transfer application from account tile action menu

  Scenario: TC_001_Switch_off Scottish Widows pensions transfer entry point tile switched Off in homepage
    Given I am an enrolled "Scottish Widows" user logged into the app
    When I navigate to scottishWidows account on home page
    Then I should not see action menu of scottishWidows account
    When I navigate to scottishWidows account on home page
    And I select scottishWidows account tile from home page
    Then I should not see action menu of scottishWidows account
