@team-mco @nga3 @rnga @secondary @stub-scottish-widows-2-accounts @sw-scottish-widows-tiles-off
Feature: E2E_selecting marketing consents from native entry page
  in order to control the ability to show/hide the SW pension tile
  As an implementation manager
  I want to be able to switch on or switch off this feature in the app from the server

  Scenario: TC_001_Switch_off Scottish Widows pensions tile switched Off in homepage
    Given I am an enrolled "Scottish Widows" user logged into the app
    Then my scottishWidows pension tile should not appear in my account list
