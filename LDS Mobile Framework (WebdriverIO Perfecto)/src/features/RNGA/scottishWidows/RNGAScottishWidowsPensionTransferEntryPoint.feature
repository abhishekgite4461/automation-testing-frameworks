@team-mco @nga3 @rnga @stub-scottish-widows-2-accounts @ff-pension-transfer-on @primary
Feature: SW Pensions Transfer Entry Point
  As an rnga customer with a eligible SW Pensions account
  I want to see "Pension Transfer" entry point from SW Pensions account action menu
  So that I can carry out SW Pensions Transfer application from account tile action menu

  Background:
    Given I am an enrolled "Scottish Widows" user logged into the app
    When I navigate to scottishWidows account on home page

  #COE-101/75/91
  Scenario: TC_001 - Verify pension transfer option from action tile action menu and landing page
    And I select the action menu of scottishWidows account
    Then I should see the below options in the action menu of the scottishWidows account
      | optionsInActionMenu   |
      | pensionTransferOption |
    When I select the "pensionTransfer" option
    Then I should be displayed the Pension Transfer webview

  Scenario: TC_002 - Verify pension transfer option from statements page
    And I select scottishWidows account tile from home page
    And I select the action menu from statements page
    Then I should see the below options in the action menu of the scottishWidows account
      | optionsInActionMenu   |
      | pensionTransferOption |
    When I select the "pensionTransfer" option
    Then I should be displayed the Pension Transfer webview
