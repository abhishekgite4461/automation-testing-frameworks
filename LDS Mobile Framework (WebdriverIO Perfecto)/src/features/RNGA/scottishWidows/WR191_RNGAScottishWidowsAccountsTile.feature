@team-mco @nga3 @rnga @stub-scottish-widows-2-accounts
Feature: In order to view my SW Pensions
  As a LBG & SW customer
  I want to view and service my SW products in the LBG app.

  Background:
    Given I am an enrolled "Scottish Widows" user logged into the app

  @primary
  Scenario: TC_001 - Scottish Widows pension tile appears in account tiles
    When I navigate to scottishWidows account on home page
    Then I should see my Pension Policy number
    And I should see my Pension balance

  Scenario: TC_003 - Verify scottish Widows pension tile appears in the homepage and statements view
    When I select scottishWidows account tile from home page
    Then I should see my Pension Policy number
    And I should see my Pension balance

  @manual @stubOnly
  Scenario: TC_004 - Scottish Widows pension tile appears only when all the details of the pension are available
    When there is a delay in the in getting my policy details when I land on the homepage
    Then I should see not see the tile initially
    And when the details have been retrieved then it should appear
