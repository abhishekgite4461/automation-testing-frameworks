@rnga @nga3 @team-coa @ff-native-change-of-address-off @sw-coa-native-on @sw-coa-cwa-on @sw-enrolment-on @stub-enrolled-typical @stubOnly
Feature: Native Change Of Address - Entry Point - Feature Flag OFF - Native Switch ON - CWA Switch ON
  In order to let bank know about my new residential address
  As a RNGA customer
  I want to see respective page based on my eligibility to change my address

  #MCOA-30, MCOA-101, MCOA-113
  Scenario: TC001 - Technical Validation Screnario: Native CoA - Feature Flag OFF - Switch ON - CWA Switch ON - Navigate to CWA Change Address Webview page
    Given I am an enrolled "current" user logged into the app
    When I am on the address update page
    Then I should see the Webview Change of Address page
