@rnga @nga3 @team-coa @sw-coa-native-on @sw-coa-cwa-on @sw-enrolment-on @stub-enrolled-typical @stubOnly @secondary
Feature: Native Change Of Address - Entry Point - Feature Flag ON - Native Switch ON - CWA Switch ON
  In order to let bank know about my new residential address
  As a RNGA customer
  I want to see respective page based on my eligibility to change my address

  #MCOA-30, MCOA-101, MCOA-113
  Scenario: TC001 - Technical Validation Scenario: Native CoA - Feature Flag ON - Switch ON - CWA Switch ON - Navigate to Native Post Code entry page
    Given I am an enrolled "current" user logged into the app
    When I am on the address update page
    Then I should see an option to search my new address using postcode
