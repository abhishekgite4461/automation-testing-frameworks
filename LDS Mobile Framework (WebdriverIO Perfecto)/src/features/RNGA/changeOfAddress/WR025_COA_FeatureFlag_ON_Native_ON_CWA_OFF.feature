@rnga @nga3 @team-coa @sw-coa-native-on @sw-coa-cwa-off @sw-enrolment-on @stubOnly
Feature: User is able to change their address using Native app
  In order to let bank know about my new residential address
  As an RNGA customer
  I want to change my residential address

  @stub-enrolled-typical @secondary
  #PAS-15751, PAS-16150, MCOA-26, MCOA-42, MCOA-30, MCOA-100, MCOA-112, MCOA-101, MCOA-113
  Scenario: TC001 – Main Success Scenario: User is able to change their address successfully
    Given I am an enrolled "current" user logged into the app
    When I am on the address update page
    Then I should see an option to search my new address using postcode
    When I proceed to call customer care
    Then I should be on the secure call page
    When I go back to previous page
    And I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    Then I should see list of addresses for entered postcode
    When I select my new address
    Then I should be asked to confirm my new address
    And I should see my new address on Map
    And I should see my new address selected for confirmation
    And I should see my new address is in title case
    When I confirm my new address
    Then I should see final instructions before updating my address
    When I confirm to update my new address
    Then I should see address change successful message
    And I can proceed to homepage after my address got updated
    When I proceed to homepage after my address got updated
    Then I should be on the home page

  @stub-coa-eligibility-filtering-failed @secondary
  #MCOA-26, MCOA-42, MCOA-30, MCOA-40, MCOA-43
  Scenario: TC002 - Exception Scenario: User should be shown error message when they are not eligible to change their address
    Given I am an enrolled "ineligible_with_restrictive_indicator" user logged into the app
    When I am on the address update page
    Then I should be displayed a change of address error message
    And I am provided Call Us option
    And I am not provided Back To Your Personal Details option
    When I proceed to Call us from error page
    Then I should be on the secure call page
    When I go back to previous page
    Then I should be displayed a change of address error message

  @stub-coa-no-address-returned @secondary
  #MCOA-36, MCOA-118
  Scenario: TC003 – Exception Scenario: User should be shown error message when no address is returned for the postcode entered
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter the post code of my new address
    And I proceed to call customer care
    Then I should be on the secure call page
    When I go back to previous page
    Then I should see post code is same as entered
    When I perform a postcode search to see the matching addresses
    Then I should be displayed a change of address error message
    And I am provided Call Us option
    And I am provided Back To Your Personal Details option

  @stub-coa-cctm-refer-branch @secondary
  #PAS-15749, PAS-16149
  Scenario: TC004 - Exception Scenario: User should be shown Refer to Branch error message when they cannot update their address due to their CCTM score
    Given I am an enrolled "ineligible_cctm_refer_to_branch" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    And I should see final instructions before updating my address
    When I confirm to update my new address
    Then I should be displayed a change of address error message
    And I am provided Back To Your Personal Details option
    And I am not provided Call Us option
    When I navigate to home page
    Then I should be on the home page

  @stub-coa-cctm-refer-fraud-ops @secondary
  #PAS-15749, PAS-16149
  Scenario: TC005 - Exception Scenario: User should be shown Fraud error message when they cannot update their address due to their CCTM score
    Given I am an enrolled "ineligible_cctm_fraud_ops" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    And I should see final instructions before updating my address
    When I confirm to update my new address
    Then I should be displayed a change of address error message
    And I am provided Back To Your Personal Details option
    And I am not provided Call Us option
    When I navigate to home page
    Then I should be on the home page

  @stub-coa-ocis-fail @secondary
  #PAS-16401, PAS-16456
  Scenario: TC006 - Exception Scenario: Change of Address Error while updating the address in address database
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    And I should see final instructions before updating my address
    When I confirm to update my new address
    Then I should be displayed a change of address error message
    And I am provided Back To Your Personal Details option
    And I am provided Call Us option

  @stub-coa-cctm-decline @secondary
  #PAS-16400, PAS-16491
  Scenario: TC007 - Exception Scenario: Change of Address Decline Error appearance on confirmation
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    And I should see final instructions before updating my address
    When I confirm to update my new address
    Then I should be displayed a change of address error message
    And I am not provided Call Us option
    And I am not provided Back To Your Personal Details option

  @stub-enrolled-typical @secondary
  #MCOA-24, MCOA-114
  Scenario: TC008 - Exception Scenario: Click my address is not on the list button from Address List page - Navigate to handoff error page
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    When I perform a postcode search to see the matching addresses
    Then I should see the Address List in title case having Post Code as entered
    When I choose My Address Not On List
    Then I should be displayed a change of address error message
    And I am provided Call Us option
    And I am provided Back To Your Personal Details option
    When I go back to previous page
    Then I should see the Address List in title case having Post Code as entered

  @stub-coa-retry-password-max-attempts @secondary
  #PAS-15758, PAS-17424
  Scenario: TC009 - Exception Scenario: User should be logged out on entering password incorrect multiple times
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    When I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    Then I should see final instructions before updating my address
    When I select that I understand the instructions
    And I enter my password to authenticate
    Then I should be logged off after entering password incorrectly multiple times

  #_____________________________________________________________________________________________________________________

  #  POST CODE ENTRY PAGE SCENARIOS
  #_____________________________________________________________________________________________________________________

  @stub-coa-eligibility-filtering-failed
  #MCOA-40, MCOA-43, MCOA-22, MCOA-41
  Scenario: TC010 - Technical Validation Scenario: Customer not eligible to change address - Click call us button
    Given I am an enrolled "ineligible_with_correspondence_address" user logged into the app
    When I am on the address update page
    Then I should be displayed a change of address error message
    When I proceed to Call us from error page
    Then I should be on the secure call page
    When I go back to previous page
    Then I should be displayed a change of address error message

  @stub-coa-eligibility-filtering-failed
  #MCOA-40, MCOA-43, MCOA-22, MCOA-41
  Scenario: TC011 - Technical Validation Scenario: Customer not eligible to change address - Click back arrow on top
    Given I am an enrolled "ineligible_with_correspondence_address" user logged into the app
    When I am on the address update page
    Then I should be displayed a change of address error message
    When I go back to previous page
    Then I should be on the your personal details page

  @stub-coa-eligibility-filtering-failed @android
  #MCOA-40, MCOA-43, MCOA-22, MCOA-41
  Scenario: TC012 - Technical Validation Scenario: Customer not eligible to change address - Click Device back button
    Given I am an enrolled "ineligible_with_correspondence_address" user logged into the app
    When I am on the address update page
    Then I should be displayed a change of address error message
    When I select back key on the device
    Then I should be on the your personal details page

  @stub-enrolled-typical
  #MCOA-1, MCOA-44
  Scenario: TC013 - Technical Validation Scenario: Allow Entry of Post Code - Enter Post Code of new Address
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter the post code of my new address
    Then I should see post code is same as entered
    And I should see the instructions regarding address change
    And I can proceed to find my new address
    When I go back to previous page
    And I choose to update address from the personal details
    And I focus on post code
    Then I should see post code help text
    When I tap out of the post code field
    Then I should not see post code help text

  @stub-enrolled-valid @manual
  #MCOA-17, MCOA-39
  Scenario Outline: TC014 - Technical Validation Scenario: Post Code Validation - Invalid values - Next button enabled
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter post code as "<invalidPostCode>"
    Then I should see post code as "<postCodeValue>"
    And I can proceed to find my new address

    Examples:
      | invalidPostCode | postCodeValue |
      | !E@1£2#B$L%     | E12BL         |
      | EC1Y 4 XX       | EC1Y 4XX      |
      | " NW11 8LE"     | NW11 8LE      |

  @stub-enrolled-typical
  #MCOA-17, MCOA-39
  Scenario Outline: TC015 - Technical Validation Scenario: Post Code Validation - Maximum Length
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter post code as "<longPostCodeValue>"
    Then I am not able to enter long post code
    And I can proceed to find my new address

    Examples:
      | longPostCodeValue |
      | NW118LE1          |

  @stub-enrolled-typical
  #MCOA-17, MCOA-39
  Scenario Outline: TC016 - Technical Validation Scenario: Post Code Validation - Maximum Length with single Space
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter post code as "<longPostCodeValue>"
    Then I am not able to enter long post code with space
    And I can proceed to find my new address

    Examples:
      | longPostCodeValue |
      | NW11 8LE1         |

  @stub-enrolled-typical
  #MCOA-17, MCOA-39
  Scenario Outline: TC017 - Technical Validation Scenario: Post Code Validation - Capitalization
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter post code as "<postCode>"
    Then I should see "<postCode>" post code is capitalized

    Examples:
      | postCode |
      | nw11 8le |

  @stub-enrolled-typical
  #MCOA-17, MCOA-39
  Scenario: TC018 - Technical Validation Scenario: Post Code Validation - Deleting single character
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter the post code of my new address
    And I can proceed to find my new address
    And I delete a character from the post code
    Then I can not proceed to find my new address

  @stub-enrolled-typical
  #MCOA-17, MCOA-39
  Scenario: TC019 - Technical Validation Scenario: Post Code Validation - Deleting entirely
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter the post code of my new address
    And I can proceed to find my new address
    And I clear the post code
    Then I can not proceed to find my new address
    When I go back to previous page
    Then I should be on the your personal details page

  @stub-enrolled-typical
  #MCOA-17, MCOA-39
  Scenario: TC020 – Technical Validation Scenario: Post Code should persist when navigated back from Other Banking Queries page
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter the post code of my new address
    And I proceed to call customer care
    And I go back to previous page
    Then I should see post code is same as entered
    When I go back to previous page
    And I select address details
    Then I see that post code entered earlier is not displayed
    When I focus on post code
    And I tap out of the post code field
    Then I can not proceed to find my new address

  @stub-enrolled-typical @android
  #MCOA-17, MCOA-39
  Scenario: TC021 – Technical Validation Scenario: Post Code should persist when navigated back from Other Banking Queries page using Device back button
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter the post code of my new address
    And I proceed to call customer care
    And I should be on the secure call page
    And I select back key on the device
    Then I should see post code is same as entered

  @stub-coa-no-address-returned
  #MCOA-36, MCOA-118
  Scenario: TC022 – Technical Validation Scenario: No Address returned - Navigate back to Post Code entry page from Handoff Error page
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I should be displayed a change of address error message
    And I go back to previous page
    Then I should see an option to search my new address using postcode
    And I should see post code is same as entered

  @stub-enrolled-valid @android
  #MCOA-98, MCOA-122
  Scenario Outline: TC023 – Technical Validation Scenario: Post Code Validation - Valid values - Next button enabled - Error icon not visible
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter post code as "<validPostCode>"
    Then I should not see an error icon
    And I can proceed to find my new address
    Examples:
      | validPostCode |
      | A1 2BC        |

  @stub-enrolled-typical @android
  #MCOA-98, MCOA-122
  Scenario Outline: TC024 – Technical Validation Scenario: Post Code Validation - Partial Valid values - Next button disabled - Error icon not visible
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter post code as "<partialPostCode>"
    Then I should not see an error icon
    And I can not proceed to find my new address

    Examples:
      | partialPostCode |
      | A1 2            |
      | DE34F           |

  @stub-enrolled-typical @android @manual
  #MCOA-98, MCOA-122
  Scenario Outline: TC025 – Technical Validation Scenario: Post Code Validation - Invalid values - Next button disabled - Error Icon visible
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter post code as "<invalidPostCode>"
    Then I should see an error icon
    And I can not proceed to find my new address

    Examples:
      | invalidPostCode |
      | 5               |
      | "A "            |
      | AAA             |
      | "AA "           |
      | A5AA            |
      | A5 A            |
      | AA5AA           |
      | AA5 A           |
      | A5A A           |
      | A55 A           |
      | A5 55           |
      | AA5A A          |
      | AA55 A          |
      | AA5 55          |
      | A5A 55          |
      | A55 55          |
      | A5 5A5          |
      | AA5A 55         |
      | AA55 55         |
      | AA5 5A5         |
      | A5A 5A5         |
      | A55 5A5         |
      | AA5A 5A5        |
      | AA55 5A5        |
      | A5A55           |
      | A55A5           |
      | A5555           |
      | AA5A55          |
      | AA55A5          |
      | AA5555          |
      | A5A5A5          |
      | A555A5          |
      | AA5A5A5         |
      | AA555A5         |
      | A55 5AAA        |
      | A55 5AA5        |
      | A555AAA         |
      | A555AA5         |
      | AA5 5AAA        |
      | AA5 5AA5        |
      | AA55AAA         |
      | AA55AA5         |
      | A5A 5AAA        |
      | A5A 5AA5        |
      | A5A5AAA         |
      | A5A5AA5         |
      | A55AAA          |
      | A55AA5          |
      | A5 5AAA         |
      | A5 5AA5         |

  @stub-enrolled-typical
  #MCOA-98, MCOA-122
  Scenario: TC026 – Technical Validation Scenario: Post Code Validation - Valid values - Next button enabled
    Given I am an enrolled "current" user logged into the app
    When I am on the address update page
    Then I verify find my new address is enabled for entered address
      | validPostCode |
      | A1 2BC        |
      | DE3 4FG       |
      | H5I 6JK       |
      | L78 9MN       |
      | OP1Q 2RS      |
      | TU34 5VW      |
      | A12BC         |
      | DE34FG        |
      | H5I6JK        |
      | L789MN        |
      | OP1Q2RS       |
      | TU345VW       |

  @stub-enrolled-typical @ios
  #MCOA-98, MCOA-122
  Scenario Outline: TC027 – Technical Validation Scenario: Post Code Validation - Partial Valid values - Next button disabled
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter post code as "<partialPostCode>"
    Then I can not proceed to find my new address

    Examples:
      | partialPostCode |
      | A1 2            |
      | DE34F           |

  @stub-enrolled-typical @ios @manual
  #MCOA-98, MCOA-122
  #Includes examples for both complete and partial post codes both of which are valid
  Scenario Outline: TC028 – Technical Validation Scenario: Post Code Validation - Valid values - Error icon not visible
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter post code as "<validPostCode>"
    Then I should not see an error icon

    Examples:
      | validPostCode |
      | A1 2BC        |
      | DE3 4FG       |
      | H5I 6JK       |
      | L78 9MN       |
      | OP1Q 2RS      |
      | TU34 5VW      |
      | A12BC         |
      | DE34FG        |
      | H5I6JK        |
      | L789MN        |
      | OP1Q2RS       |
      | TU345VW       |
      | A1 2          |
      | DE34F         |

  @stub-enrolled-typical @ios @manual
  #MCOA-98, MCOA-122
  Scenario Outline: TC029 – Technical Validation Scenario: Post Code Validation - Invalid values - Next button disabled
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter post code as "<invalidPostCode>"
    Then I can not proceed to find my new address

    Examples:
      | invalidPostCode |
      | 5               |
      | "A "            |
      | AAA             |
      | "AA "           |
      | A5AA            |
      | A5 A            |
      | AA5AA           |
      | AA5 A           |
      | A5A A           |
      | A55 A           |
      | A5 55           |
      | AA5A A          |
      | AA55 A          |
      | AA5 55          |
      | A5A 55          |
      | A55 55          |
      | A5 5A5          |
      | AA5A 55         |
      | AA55 55         |
      | AA5 5A5         |
      | A5A 5A5         |
      | A55 5A5         |
      | AA5A 5A5        |
      | AA55 5A5        |
      | A5A55           |
      | A55A5           |
      | A5555           |
      | AA5A55          |
      | AA55A5          |
      | AA5555          |
      | A5A5A5          |
      | A555A5          |
      | AA5A5A5         |
      | AA555A5         |
      | A55 5AAA        |
      | A55 5AA5        |
      | A555AAA         |
      | A555AA5         |
      | AA5 5AAA        |
      | AA5 5AA5        |
      | AA55AAA         |
      | AA55AA5         |
      | A5A 5AAA        |
      | A5A 5AA5        |
      | A5A5AAA         |
      | A5A5AA5         |
      | A55AAA          |
      | A55AA5          |
      | A5 5AAA         |
      | A5 5AA5         |

  @stub-enrolled-typical @ios @manual
  #MCOA-98, MCOA-122
  Scenario Outline: TC030 – Technical Validation Scenario: Post Code Validation - Invalid values - Error Icon visible
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter post code as "<invalidPostCode>"
    Then I should see an error icon

    Examples:
      | invalidPostCode |
      | 5               |
      | "A "            |
      | AAA             |
      | "AA "           |
      | A5AA            |
      | A5 A            |
      | AA5AA           |
      | AA5 A           |
      | A5A A           |
      | A55 A           |
      | A5 55           |
      | AA5A A          |
      | AA55 A          |
      | AA5 55          |
      | A5A 55          |
      | A55 55          |
      | A5 5A5          |
      | AA5A 55         |
      | AA55 55         |
      | AA5 5A5         |
      | A5A 5A5         |
      | A55 5A5         |
      | AA5A 5A5        |
      | AA55 5A5        |
      | A5A55           |
      | A55A5           |
      | A5555           |
      | AA5A55          |
      | AA55A5          |
      | AA5555          |
      | A5A5A5          |
      | A555A5          |
      | AA5A5A5         |
      | AA555A5         |
      | A55 5AAA        |
      | A55 5AA5        |
      | A555AAA         |
      | A555AA5         |
      | AA5 5AAA        |
      | AA5 5AA5        |
      | AA55AAA         |
      | AA55AA5         |
      | A5A 5AAA        |
      | A5A 5AA5        |
      | A5A5AAA         |
      | A5A5AA5         |
      | A55AAA          |
      | A55AA5          |
      | A5 5AAA         |
      | A5 5AA5         |

  #_____________________________________________________________________________________________________________________

  #  ADDRESS LIST PAGE SCENARIOS
  #_____________________________________________________________________________________________________________________

  @stub-enrolled-typical
  #MCOA-23, MCOA-103
  Scenario: TC031 – Technical Validation Scenario: Navigate to Address List page
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    When I perform a postcode search to see the matching addresses
    Then I should see Address List instructions
    When I go back to previous page
    Then I should see an option to search my new address using postcode
    And I should see post code is same as entered
    When I perform a postcode search to see the matching addresses
    Then I should see Address List instructions
    And I should see the Address List in title case having Post Code as entered
    And I should see My Address Not On List

  @stub-enrolled-typical @android
  #MCOA-23, MCOA-103
  Scenario: TC032 - Technical Validation Scenario: Navigate to Address List - Click device back button
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    When I perform a postcode search to see the matching addresses
    Then I should see the Address List
    When I select back key on the device
    Then I should see an option to search my new address using postcode
    And I should see post code is same as entered

  @stub-enrolled-typical @manual @secondary
  #MCOA-23, MCOA-103
  Scenario: TC033 - Technical Validation Scenario: Navigate to Address List page and the addresses are in ascending order if having house numbers
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    When I perform a postcode search to see the matching addresses
    Then I should see the address list in ascending order if having house numbers

  @stub-enrolled-typical
  #MCOA-24, MCOA-114
  Scenario: TC034 - Technical Validation Scenario: Navigate back to Address List page from Hand Off error page
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    When I perform a postcode search to see the matching addresses
    And I choose My Address Not On List
    And I go back to previous page
    Then I should see the Address List in title case having Post Code as entered

  @stub-enrolled-typical @android
  #MCOA-24, MCOA-114
  Scenario: TC035 - Technical Validation Scenario: Navigate back to Address List page from Hand Off error page using device back button
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    When I perform a postcode search to see the matching addresses
    And I choose My Address Not On List
    And I select back key on the device
    Then I should see the Address List in title case having Post Code as entered

  #_____________________________________________________________________________________________________________________

  #  CONFIRM ADDRESS PAGE SCENARIOS
  #_____________________________________________________________________________________________________________________

  @stub-enrolled-typical
  #MCOA-25, MCOA-182
  Scenario: TC036 – Technical Validation Scenario: Confirm Address - Should see previously selected address from the List
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    Then I should be asked to confirm my new address
    And I should see my new address selected for confirmation
    And I should see my new address on Map
    And I should see my new address is in title case
    And I can proceed to confirm new address
    When I go back to previous page
    Then I should see the Address List
    And I should see the Address List in title case having Post Code as entered

  @stub-coa-map-invalid-address @ios
  #MCOA-25, MCOA-182
  Scenario: TC037 – Technical Validation Scenario: Confirm Address - Map Placeholder Image should be visible
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    Then I should be asked to confirm my new address
    And I should see Map placeholder image

  @stub-enrolled-typical @android
  #MCOA-25, MCOA-182
  Scenario: TC038 – Technical Validation Scenario: Confirm Address - Click Android device back key
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    Then I should be asked to confirm my new address
    And I select back key on the device
    And I should see the Address List
    And I should see the Address List in title case having Post Code as entered

  #_____________________________________________________________________________________________________________________

  #  ONE LAST THING PAGE SCENARIOS
  #_____________________________________________________________________________________________________________________

  @stub-enrolled-typical
  #PAS-15750, PAS-15818
  Scenario: TC039 – Technical Validation Scenario: One Last Thing Page - Should see Confirm address button enabled
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    Then I should see final instructions before updating my address
    And I should see final instructions before agreeing to update my address
    And I can proceed to select that I understand the instructions
    When I select that I understand the instructions
    Then I should see enter password dialog
    When I cancel the enter password dialog
    Then I should not see enter password dialog
    When I go back to previous page
    Then I should be asked to confirm my new address

  @stub-enrolled-typical @android
  #PAS-15750, PAS-15818
  Scenario: TC040 – Technical Validation Scenario: One Last Thing Page - Click device back key
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    Then I should see final instructions before updating my address
    When I select back key on the device
    Then I should be asked to confirm my new address

  @stub-coa-cctm-refer-branch
  #PAS-15749, PAS-16149
  Scenario: TC041 - Technical Validation Scenario: Navigation to Home via Home button post confirmation from Refer to Branch error
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    And I should see final instructions before updating my address
    And I confirm to update my new address
    And I should be displayed a change of address error message
    When I navigate to home page
    Then I should be on the home page

  @stub-coa-cctm-refer-branch @android
  #PAS-15749, PAS-16149
  Scenario: TC042 – Technical Validation Scenario: Navigation to Home via device back button post confirmation from Refer to Branch error
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    And I should see final instructions before updating my address
    And I confirm to update my new address
    And I should be displayed a change of address error message
    When I select back key on the device
    Then I should be on the home page

  @stub-coa-cctm-refer-fraud-ops
  #PAS-15749, PAS-16149
  Scenario: TC043 – Technical Validation Scenario: Navigation to Home via Home button post confirmation from FraudOps error
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    And I should see final instructions before updating my address
    And I confirm to update my new address
    And I should be displayed a change of address error message
    When I navigate to home page
    Then I should be on the home page

  @stub-coa-cctm-refer-fraud-ops @android
  #PAS-15749, PAS-16149
  Scenario: TC044 – Technical Validation Scenario: Navigation to Home via device back button post confirmation from FraudOps error
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    And I should see final instructions before updating my address
    And I confirm to update my new address
    And I should be displayed a change of address error message
    When I select back key on the device
    Then I should be on the home page

  @stub-coa-ocis-fail
  #PAS-16401, PAS-16456
  Scenario: TC045 - Technical Validation Scenario: Navigation to Call us and Personal Details post confirmation from error to update address database
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    And I should see final instructions before updating my address
    When I confirm to update my new address
    Then I should be displayed a change of address error message
    And I am provided Back To Your Personal Details option
    And I am provided Call Us option
    When I proceed to Call us from error page
    Then I should be on the change of address call us screen
    And  I go back to previous page
    And I should be displayed a change of address error message
    When I proceed to Personal Details from error page
    Then I should be on the your personal details page

  @stub-coa-ocis-fail
  #PAS-16401, PAS-16456
  Scenario: TC046 - Technical Validation Scenario: Navigation to Home via Home button post confirmation from error to update address database
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    And I should see final instructions before updating my address
    And I confirm to update my new address
    And I should be displayed a change of address error message
    When I navigate to home page
    Then I should be on the home page

  @stub-coa-ocis-fail @android
  #PAS-15749, PAS-16149
  Scenario: TC047 – Technical Validation Scenario: Navigation to Home via device back button post confirmation from error to update address database
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    And I should see final instructions before updating my address
    And I confirm to update my new address
    And I should be displayed a change of address error message
    When I select back key on the device
    Then I should be on the home page

  @stub-coa-cctm-decline
  #PAS-16400, PAS-16491
  Scenario: TC048 - Technical Validation Scenario: User is not able to navigate to Home via Home button post confirmation from Decline Error
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    And I should see final instructions before updating my address
    And I confirm to update my new address
    And I should be displayed a change of address error message
    And I am not provided Back To Your Personal Details option
    And I am not provided Call Us option

  @stub-coa-retry-password
  #PAS-15758, PAS-17424
  Scenario: TC049 - Exception Scenario: User should be logged out on entering password incorrect multiple times
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    And I enter the post code of my new address
    When I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    Then I should see final instructions before updating my address
    When I confirm to update my new address
    Then I should see enter password dialog

  #_____________________________________________________________________________________________________________________

  #  ADDRESS CHANGED SCENARIOS
  #_____________________________________________________________________________________________________________________

  @stub-enrolled-typical
  #PAS-15751, PAS-16150
  Scenario: TC050 – Technical Validation Scenario: Address Change Success - Click back to personal details button
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    And I confirm to update my new address
    Then I should see address change successful message
    When I proceed to homepage after my address got updated
    Then I should be on the home page

  @stub-enrolled-typical
  #PAS-15751, PAS-16150
  Scenario: TC051 – Technical Validation Scenario: Address Change Success - Click home icon
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    And I confirm to update my new address
    Then I should see address change successful message
    When I navigate to home page
    Then I should be on the home page

  @stub-enrolled-typical @android
  #PAS-15751, PAS-16150
  Scenario: TC052 – Technical Validation Scenario: Address Change Success - Click device back button
    Given I am an enrolled "current" user logged into the app
    And I am on the address update page
    When I enter the post code of my new address
    And I perform a postcode search to see the matching addresses
    And I select my new address
    And I confirm my new address
    And I confirm to update my new address
    Then I should see address change successful message
    When I select back key on the device
    Then I should be on the home page
