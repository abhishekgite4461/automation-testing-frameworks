@rnga @nga3 @team-coa @ff-native-change-of-address-off @sw-coa-native-on @sw-coa-cwa-off @sw-enrolment-on @stub-enrolled-typical @stubOnly
Feature: Native Change Of Address - Entry Point - Feature Flag OFF - Native Switch ON - CWA Switch OFF
  In order to let bank know about my new residential address
  As an RNGA customer
  I want to update my address using call

  #MCOA-30, MCOA-101, MCOA-113
  Scenario: TC001 - Technical Validation Scenario: Native CoA - Feature Flag OFF - Switch ON - CWA Switch OFF - Navigate to old Change address page
    Given I am an enrolled "current" user logged into the app
    When I am on the address update page
    Then I should be shown an option to change my address using secure call
