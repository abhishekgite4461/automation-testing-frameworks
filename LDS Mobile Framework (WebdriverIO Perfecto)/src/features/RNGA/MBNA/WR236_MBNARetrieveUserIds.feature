@team-mpt @mbnaSuite @rnga @nga3 @environmentOnly @manual

Feature: Retrieve MBNA user ids from old app to the new app
        In order that I can easily login to the new app
        As a migrated MBNA customer
        I want my user ids saved in the old app to be displayed during app upgrade

  Scenario: TC001 - Main Success Scenario - Retrieve saved user ids from old app
    Given I am a migrated MBNA customer
    And I have downloaded the new MBNA app
    And I have old MBNA app installed on my device
    And I have more than one user id saved in my old MBNA app
    When I launch the new app
    Then all the user ids saved in my old app must be retrieved

  Scenario: TC002 – Negative Scenario - User has no user ids saved
    Given I am a migrated MBNA customer
    And I have downloaded the new MBNA app
    And I have old MBNA app installed on my device
    And I have no user id saved in my old MBNA app
    When I launch the new app
    Then no user id must be retrieved

  Scenario: TC003 – Negative Scenario - User is a new customer to MBNA
    Given I am a new MBNA customer
    And I have downloaded the new MBNA app
    And I have old MBNA app installed on my device
    When I launch the new app
    Then no user id must be retrieved

  Scenario: TC004 – Negative Scenario - User has no old MBNA app installed
    Given I am a migrated MBNA customer
    And I have downloaded the new MBNA app
    And I do not have old MBNA app installed on my device
    When I launch the new app
    Then no user id must be retrieved
#   Note: When there is no user id saved, then user should not see the option to invoke the actions menu from the "Change" button/icon
