@team-mpt @mbnaSuite @rnga @nga3 @environmentOnly @manual @sw-remember-me-on

Feature: In order that I can protect the bank in case of failure
  As an implementation manager
  I want to be able to Switch on the remember me functionality for MBNA brand

  Scenario: TC01 Main Success Scenario: User is displayed remember me functionality
    Given the Remember Me MBNA switch is ON
    When the user downloads the new MBNA app
    Then the user should have the remember me functionality
