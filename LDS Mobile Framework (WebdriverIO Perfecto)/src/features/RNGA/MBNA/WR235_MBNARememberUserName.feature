@team-mpt @mbnaSuite @rnga @nga3 @environmentOnly @manual

Feature: Display retrieved MBNA user ids from old app to the new app
        In order that I can easily login to the new app
        As a migrated MBNA customer
        I want my user ids saved in the old app to be displayed during app upgrade

  Scenario: TC001 - Main Success Scenario - User has more than one user ids saved in the old MBNA app
    Given I am a migrated MBNA customer
    And I have not enrolled my device
    And I have downloaded the new MBNA app
    And I have more than one user id saved in my old MBNA app
    When I launch the new app
    Then the most recent user id must be pre-populated in the user id field
    And I must have the option to clear the user id field
    And I must have the option to view all the user ids that I have saved in my old app
    And I must have the option to select one user id from the saved ones to register my device
    And I must have the option to enter a user id manually
    And I must have the option to enter a password

  Scenario: TC-002 – Main Success Scenario - User has only one user ids saved in the old MBNA app
    Given I am a migrated MBNA customer
    And I have not enrolled my device
    And I have downloaded the new MBNA app
    And I have one user id saved in my old MBNA app
    When I launch the new app
    Then I should be displayed the registration page
    And the user ids that I have saved in my old app must be pre-populated in the user id field
    And I must have the option to clear the user id field
    And I must have the option to select the saved user id again
    And I must have the option to enter another user id
    And I must have the option to enter a password

  Scenario: TC003 – Main Success Scenario - User has no user ids saved
    Given I am a migrated MBNA customer
    And I have not enrolled my device
    And I have old MBNA app installed on my device
    And I have downloaded the new MBNA app
    And I have no user id saved in my old MBNA app
    When I launch the new app
    Then no user id must be retrieved
    And I should be displayed the registration page
    And no user id must be pre-populated in the user id field
    And I must not have the option to select any user id
    And the user id field must be blank
    And I must have the option to enter a user id
    And I must have the option to enter a password

  Scenario: TC004 – Main Success Scenario – Customer enters correct user id and password
    Given I am a migrated MBNA customer
    And I have not enrolled my device
    And I have downloaded the new MBNA app
    And I launch the new app
    And I am on the registration page
    And I enter correct user id to register my device
    And I enter correct password
    When I continue to register
    Then I must be displayed the Memorable Information

  Scenario: TC005 – Exceptional Scenario – Customer user id is not migrated
    Given I am a migrated MBNA customer
    And I have not enrolled my device
    And I have downloaded the new MBNA app
    And I have more than one user id saved in my old MBNA app
    And I launch the new app
    And I am on the registration page
    And I must be displayed all the user ids that I have saved in my old app
    And I select a user id to register my device that is not migrated
    And I have entered a password
    When I continue to register
    Then I must be displayed a message informing that user id/password is entered invalid (MG-358)
    And the user id and password fields must be cleared
#  Note: MG-358 is displayed when user enters incorrect user id and or password. The same message must be mapped when the system does not find a user id in the MBNA database.
#  MG-358 = 8000067: You’ve entered an incorrect user ID or password. Please try again.

  Scenario: TC006 – Exceptional Scenario – Customer enters incorrect user id
    Given I am a MBNA customer
    And I have not enrolled my device
    And I have downloaded the new MBNA app
    And I launch the new app
    And I am on the registration page
    And I enter an incorrect user id to register my device
    And I have entered a password
    When I continue to register
    Then I must be displayed a message informing that user id/password is entered invalid (MG-358)
    And the user id and password fields must be cleared

  Scenario: TC007 – Exceptional Scenario – Customer selects valid user id but enters incorrect password
    Given I am a migrated MBNA customer
    And I have not enrolled my device
    And I have downloaded the new MBNA app
    And I have more than one user id saved in my old MBNA app
    And I launch the new app
    And I am on the registration page
    And I am displayed all the user ids that I have saved in my old app
    And I select a valid user id to register my device
    And I enter incorrect password
    When I continue to register
    Then I must be displayed a message informing that user id/password is entered invalid (MG-358)
    And the user id and password fields must be cleared

  Scenario: TC008 – Main Success Scenario – Customer enters correct user id but enters incorrect password
    Given I am a MBNA customer
    And I have not enrolled my device
    And I have downloaded the new MBNA app
    And I launch the new app
    And I am on the registration page
    And I enter correct user id to register my device
    And I enter incorrect password
    When I continue to register
    Then I must be displayed a message informing that user id/password is entered invalid (MG-358)
    And the user id and password fields must be cleared

  Scenario: TC009 – Negative Scenario - User has no old MBNA app installed
    Given I am a migrated MBNA customer
    And I have downloaded the new MBNA app
    And I do not have old MBNA app installed on my device
    When I launch the new app
    Then no user id must be retrieved
    And I should be displayed the registration page
    And I must have the option to enter a user id
    And I must have the option to enter a password

  Scenario: TC010– Main Success Scenario – Migrated MBNA Customer enters correct user id, password and MI and login successfully
    Given I am a migrated MBNA customer
    And I have not enrolled my device
    And I have downloaded the new MBNA app
    And I launch the new app within X time after the new app is released
    And I am on the registration page
    And I enter correct user id to register my device
    And I enter correct password
    And I enter correct MI
    When I continue to register
    Then I must not be asked to do EIA
    And my device must be enrolled
    And I must be logged onto the app
    And I must be displayed the no security call needed page
#   Note: X is a configurable time at FS end. It is considered to be in hours and will be 999 hours.

  Scenario: TC011– Main Success Scenario – New MBNA Customer is asked EIA
    Given I am a new MBNA customer
    And I have not enrolled my device
    And I have downloaded the new MBNA app
    And I launch the new app within X time after the new app is released
    And I am on the registration page
    And I enter correct user id to register my device
    And I enter correct password
    And I enter correct MI
    When I continue to register
    Then I must not be asked to do EIA
#    Note: X is a configurable time at FS end. It is considered to be in hours and will be 999 hours.

  Scenario: TC012 – Main Success Scenario – Customer enters correct user id, password and MI and creates MI
    Given I am a migrated MBNA customer
    And I have not enrolled my device
    And I have not created my MI
    And I have downloaded the new MBNA app
    And I launch the new app
    And I am on the registration page
    And I enter correct user id to register my device
    And I enter correct password
    When I continue to register
    Then I must asked to do create MI

  Scenario: TC013 – Main Success Scenario – Migrated MBNA Customer skips EIA after creating MI
    Given I am a migrated MBNA customer
    And I have not enrolled my device
    And I have not created my MI
    And I have downloaded the new MBNA app
    And I launch the new app
    And I am on the registration page
    And I enter correct user id to register my device
    And I enter correct password
    And I have created MI
    When I continue to register
    Then I must not be asked to do EIA
    And my device must be enrolled
    And I must be logged onto the app
    And I must be displayed the no security call needed page

  Scenario: TC014 – Main Success Scenario – New MBNA Customer is asked to do EIA after creating MI
    Given I am a new MBNA customer
    And I have not enrolled my device
    And I have created my MI
    And I have downloaded the new MBNA app
    And I launch the new app within X time after the new app is released
    And I am on the registration page
    And I enter correct user id to register my device
    And I enter correct password
    And I have created MI
    When I continue to register
    Then I must not be asked to do EIA
    And my device must be enrolled
    And I must be logged onto the app
    And I must be displayed the no security call needed page

  Scenario: TC015 – Main Success Scenario – Customer successfully registers for mobile banking
    Given I am a migrated MBNA customer
    And I have not enrolled my device
    And I have downloaded the new MBNA app
    And I launch the new app
    And I am on the registration page
    And I enter correct user id to register my device
    And I enter correct password
    And I enter correct MI
    When I have successfully logged in
    Then the user ids saved in the old MBNA must be deleted

  Scenario: TC01– Main Success Scenario – Customer successfully registers for mobile banking
    Given I am a migrated MBNA customer
    And I have not enrolled my device
    And I have downloaded the new MBNA app
    When I launch the new app
    And I am on the registration page
    And I enter correct user id to register my device
    And I enter correct password
    And I enter correct MI
    Then I have successfully logged in
    And the user ids saved in the old MBNA must be deleted

  Scenario: TC002– Exception Scenario – Customer does not successfully registers for mobile banking
    Given I am a migrated MBNA customer
    And I have not enrolled my device
    And I have downloaded the new MBNA app
    When I launch the new app
    And I am on the registration page
    And I enter correct user id to register my device
    And I enter correct password
    And I enter correct MI
    And there is some error
    Then I have not successfully logged in
    And the user ids saved in the old MBNA must not be deleted

  Scenario: TC001 – Main Success Scenario – Customer enters correct user id and password
    Given I am a migrated MBNA customer
    And I have not enrolled my device
    And I have downloaded the new MBNA app
    And I have user id with special characters saved in my old MBNA app
    When I launch the new app
    And I am on the registration page
    And I select the user id with special characters to register my device
    And I enter correct password
    Then I continue to register
    And I must be displayed the Memorable Information
#  Note: User should only be able to select user id with special characters from old MBNA app.
#  User should not be able to allowed to enter user id with special characters.

  Scenario: TC02 – Exceptional Scenario – Customer user id with special characters is not migrated
    Given I am a migrated MBNA customer
    And I have not enrolled my device
    And I have downloaded the new MBNA app
    And I have user id with special characters saved in my old MBNA app
    When I launch the new app
    And I am on the registration page
    Then I must be displayed all the user ids that I have saved in my old app
    When I select user id with special characters to register my device that is not migrated
    And I have entered a password
    And I continue to register
    Then I must be displayed a message informing that user id/password is entered invalid (MG-358)
    And the user id and password fields must be cleared
