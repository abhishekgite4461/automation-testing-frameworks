@mbna @nga3 @rnga @sw-enrolment-on @sw-light-logon-on @pending @primary @team-pi
Feature: MBNA single product view
  As an MBNA customer with a single credit card
  I want to see max 4 options on the account overview screen to interact with my product
  So that I can carry out servicing on my credit card

 #Note : In IOS small screen devices only 3 options will be displayed
 #Note : Automation in progress

  Scenario: TC_001 Single product view with some or all core options been ineligible
    Given I am an enrolled "MBNASingleProduct" user logged into the app
    When I am on the home page and have only a single product holding
    Then I should be see only 4 options based on eligibility from the list below
      | productLabels     |
      | balanceTransfer   |
      | moneyTransfer     |
      | manageCreditLimit |
      | viewTransactions  |
      | cardControls      |
      | pdfStatements     |
      | payCreditCard     |
      | inbox             |
