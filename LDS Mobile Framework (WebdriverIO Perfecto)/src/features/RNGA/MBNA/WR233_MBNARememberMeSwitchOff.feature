@team-mpt @mbnaSuite @rnga @nga3 @environmentOnly @manual @sw-remember-me-off

Feature: In order that I can protect the bank in case of failure
  As an implementation manager
  I want to be able to Switch on the remember me functionality for MBNA brand

  Scenario: TC02 Main Success Scenario: User is not displayed remember me functionality
    Given the Remember Me MBNA switch is ON
    When the user downloads the new MBNA app
    Then the user should not have the remember me functionality
    And the user should be displayed the normal user registration functionality
