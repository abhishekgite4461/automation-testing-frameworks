@team-mco @rnga @nga3 @instrument @bos @ff-webview-debug-on @pending @environmentOnly
Feature: Validate the overdraft journey for adding, amending and removing overdraft to an existing current account for BOS brand

  Background:
    Given I am an enrolled "current" user logged into the app
    And I navigate to current account on home page
    When I select the action menu of current account
    Then I am able to view applyForOverdraftOption in contextual menu
    When I select applyForOverdraftOption from contextual menu
    And I select the account to apply the overdraft
    Then I should close the overdraft info module

  Scenario Outline: TC_001_Verify successful application for New Overdraft Scenario for Solo account
    When I input the overdraft amount <overdraft> in the overdraft input box
    And I select continue button on the overdraft screen
    Then I land on the Income and Expenditure screen
    And I select No for something about to change question on the Income and Expenditure screen
    When I select full time employment status on the Income and Expenditure screen
    And I enter income <income> on the Income and Expenditure screen
    And I enter mortgage <mortgage> on the Income and Expenditure screen
    And I enter maintenance <maintenance> on the Income and Expenditure screen
    And I select continue on the Income and Expenditure screen
    And I confirm my email and select continue
    And I confirm the fee on the PCCI page and continue
    Then I verify the overdraft success page with <overdraft>
    Examples:
      |overdraft|income|mortgage|maintenance|
      |  300    | 1334  |0       |0          |

  Scenario Outline: TC_001_Verify Overdraft Amend Scenario
    Then I verify the existing overdraft should be greater than 0
    When I input the overdraft amount <overdraft> in the overdraft input box
    And I select continue button on the overdraft screen
    Then I land on the Income and Expenditure screen
    And I select No for something about to change question on the Income and Expenditure screen
    When I select full time employment status on the Income and Expenditure screen
    And I enter income <income> on the Income and Expenditure screen
    And I enter mortgage <mortgage> on the Income and Expenditure screen
    And I enter maintenance <maintenance> on the Income and Expenditure screen
    And I select continue on the Income and Expenditure screen
    And I confirm my email and select continue
    And I confirm the fee on the PCCI page and continue
    Then I verify the overdraft success page with <overdraft>
    Examples:
      |overdraft|income|mortgage|maintenance|
      |  400    | 1334  |0       |0          |

  Scenario: TC_001_Verify Overdraft Removal Scenario
    And I verify the existing overdraft should be greater than 0
    And I input the overdraft amount 0 in the overdraft input box
    When I select remove button on the overdraft screen
    Then I should see overdraft success page for removing overdraft
