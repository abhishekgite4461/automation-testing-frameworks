@team-payments @nga3 @rnga @ff-whitelisted-biller-on @sw-whitelisted-biller-off @stub-whitelisted-biller-success
Feature: Whitelisted Biller from 3.0 Api
  As a RNGA Authenticated user
  I want to make payment to whitelisted biller

  #PAT-1339 PAT-1320
  Scenario: TC_01 User is shown the whitelisted biller from 3.0 api
    Given I am an enrolled "valid" user logged into the app
    When I navigate to add new recipient option using payment hub
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I enter whitelisted company details
    And I select continue button on add payee page
    Then I am on UK Company Search results page
    And I should not see the biller LLOYDS BANK CHILD TRUST FUND
    When I select a result
    Then I have the ability to add a reference test, test and retype the reference
