@team-payments @nga3 @rnga @ff-whitelisted-biller-on @sw-whitelisted-biller-on
Feature: Whitelisted Biller from 3.0 Api
  As a RNGA Authenticated user
  I want to make payment to whitelisted biller

  @stub-whitelisted-biller-success
  #PAT-1339 PAT-1342 PAT-1320 PAT-1341
  Scenario: TC_01 User is shown the whitelisted biller from 3.0 api
    Given I am an enrolled "valid" user logged into the app
    When I navigate to add new recipient option using payment hub
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I enter whitelisted company details
    And I select continue button on add payee page
    Then I am on UK Company Search results page
    And I should see the biller LLOYDS BANK CHILD TRUST FUND

  @stub-whitelisted-biller-success
    #PAT-1339 PAT-1342 PAT-1320 PAT-1321 PAT-1336 PAT-1334 PAT-1333
  Scenario Outline: TC_02 User navigates to whitelisted biller journey from 3.0 api
    Given I am an enrolled "valid" user logged into the app
    When I navigate to add new recipient option using payment hub
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    Then I am on UK Company Search results page
    When I select the biller <whitelistedBiller> from the biller list
    And I validate reference field label
    And I enter 1.00 in the payment amount field in the payment hub page
    And I enter the wrong reference value for the biller
    Then I validate the error banner message for wrong reference value
    When I enter the <correctReferenceFormat> reference value for the biller
    And I select Continue button on payment hub page
    Then I select the confirm option in the review payment page
    When I enter a correct password
    Then I am shown Payment Successful page
    Examples:
      | sortCode | accountNumber | recipientName | whitelistedBiller              | correctReferenceFormat |
      | 802045   | 00213765      | abdcf         | BRITISH TELECOM HOUSEHOLD BILL | AA12345678             |

  @stub-whitelisted-biller-success
    #PAT-1339 PAT-1342 PAT-1320 PAT-1321 PAT-1336 PAT-1334 PAT-1333
  Scenario Outline: TC_03 User navigates to 3.0 whitelisted biller journey for future dated payment
    Given I am an enrolled "valid" user logged into the app
    When I navigate to add new recipient option using payment hub
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I validate <sortCode>, <accountNumber>, <recipientName>
    And I select continue button on add payee page
    Then I am on UK Company Search results page
    When I select the biller <whitelistedBiller> from the biller list
    And I validate reference field label
    And I enter 1.00 in the payment amount field in the payment hub page
    And I enter the <correctReferenceFormat> reference value for the biller
    And I select calendar picker option for future date payment
    Then I should see a calendar view
    And I should be allowed to select a payment date up to 31 days from current day
    When I select previous month chevron from the top header of calendar view
    And I select the date as 2 from today's date from calendar view
    And I select Continue button on payment hub page
    Then I should see the review payment page
    And I am shown 'When' the payment will be made on payment review page
    When I select the confirm option in the review payment page
    And I enter a correct password
    Then I am shown Payment Successful page
    Examples:
      | sortCode | accountNumber | recipientName | whitelistedBiller              | correctReferenceFormat |
      | 802045   | 00213765      | abdcf         | BRITISH TELECOM HOUSEHOLD BILL | AA12345678             |
      | 802045   | 00213765      | abdcf12       | BRITISH TELECOM VISA           | 1234567890123456       |

  @stub-whitelisted-biller-api-fail
    #PAT-1319 PAT-1374
  Scenario: TC_04 User is shown error banner when the 3.0 whitelisted biller api fails
    Given I am an enrolled "valid" user logged into the app
    When I navigate to add new recipient option using payment hub
    And I choose to Add new payee on search recipient page
    And I choose UK account from account category page
    And I enter whitelisted company details
    And I select continue button on add payee page
    Then I validate the error banner message for api failure
