@team-mpt @rnga @nga3 @manual
Feature: Display What's New screens to NGA User on first login after major app upgrade
  In order to be aware and make use of new functionality in the app
  As a Retail App user
  I want to be informed about new functionality on app upgrade

  Background:
    Given I am an enrolled "valid" user

  Scenario: TC_001 Main Success Scenario: Customer opens the app after major app upgrade for the first time
    Given I have upgraded the app to a major release version
    When I open the app for first time after the upgrade
    Then I am shown the What's New screens
    And I am displayed the following fields
      | Screen Title               |
      | Sub title                  |
      | Image of new functionality |
      | Navigation options         |
  #  Navigation options -
  #  First screen to have Skip and Next
  #  Last Screen to have Log on and Previous
  #  Middle screens to have Previous, Skip and Next

  Scenario: TC_002 Main Success Scenario: Customer selects to skip or logon from What's New screens
    Given I have upgraded the app to a major release version
    And I am shown the What's New screens
    When I choose to skip/login from What's New screen
    Then I am shown the MI screen for logging into the app
  # Touch id/face id to be performed if User has opted in

  Scenario: TC_003 Variation Scenario: Customer opens the app after minor app upgrade (such as 2.1, 2.2, 3.1 etc) for the first time .He had NOT upgraded to the latest major version
    Given I have upgraded the app to a minor release version
    And I had not upgraded to last major version
    When I open the app for first time after the upgrade
    Then I am shown the What's New screens
  #  Example - Customer upgrades to version 2.1 from version 1 having missed upgrade to version 2

  Scenario: TC_004 Exception Scenario: Customer opens the app after minor app upgrade (such as 2.1, 2.2, 3.1 etc) for the first time .He had upgraded to latest major version
    Given I have upgraded the app to a minor release version
    And I had upgraded to last major version
    When I open the app for first time after the upgrade
    Then I am not shown the What's New screens
  #  Example - Customer upgrades to version 2.1 from version 2

  Scenario: TC_005 Exception Scenario: Customer has already seen What's New screen and opens app again after major app upgrade
    Given I have upgraded the app to a major release version
    And I have already seen What's New screen on the first login
    When I open the app for next time
    Then I am not shown the What's New screens

  Scenario: TC_006 Exception Scenario: Customer opens the app after major app upgrade for the first time, however Light Logon is OFF
    Given I have upgraded the app to a major release version
    And light logon is off
    When I open the app for the first time after the upgrade
    Then I am not shown the What's New screens

  Scenario: TC_007 Variation Scenario: Customer has already opened the app after a major app upgrade with Light Logon OFF and opens the app again with Light Logon ON
    Given I have upgraded the app to a major release version
    And I have already opened the app for the first time when light logon was off
    When light logon is on
    And I open the app for the next time
    Then I am shown the What's New screens
