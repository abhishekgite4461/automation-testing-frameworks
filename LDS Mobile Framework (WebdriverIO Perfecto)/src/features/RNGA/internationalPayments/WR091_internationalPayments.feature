@team-payments @nga3 @rnga @environmentOnly

#MOB3-709
Feature: User wants to access international payments journey
  In Order to add or pay a current international recipient
  As a RNGA Authenticated Customer
  I want to be able to access international payments journey

  Background:
    Given I am an enrolled "valid" user logged into the app

  @primary
  Scenario: AC01 TC_001_User uses action menu for international payment
    And I select current account tile from home page
    When I select the "international payment" option in the action menu of current account
    Then I should be on the international payment webview page

  @primary
  Scenario: AC01 TC_002_User uses entry point from new recipient
    And I navigate to add new recipient option using payment hub
    And I should see an option to add a new recipient
    And I choose to Add new payee on search recipient page
    When I select international bank account from account category page
    Then I should be on the international payment webview page

  @manual
  Scenario: AC01 TC_003_User initiates forgotten password from webview
    And I am on confirm details for International payment page
    When I select forgotten your password option
    Then I should see forgotten password winback with options on top of the screen with cancel and OK option
    And on selection of cancel option I should stay on the same screen

  @manual
  Scenario: AC01 TC_004_User initiates forgotten password from webview
    And I am on confirm details for International payment page
    When I select forgotten your password option
    Then I should see forgotten password winback with options on top of the screen with cancel and OK option
    And on selection of OK option I should navigate to native reset password screen

  @manual @primary
  Scenario Outline: AC01 TC_005_User Triggers EIA call to add new international beneficiary with METADATA EIAPAGE
    And I am on initiate EIA page for confirming international payment page
    When I select to continue with the EIA call
    And my log off timer setting is set to |mins|
    Then the log of timer is bumped up to |action|

    Examples:
      |   mins         |      action        |
      | immediate      |increased to 5 mins |
      | 1 Minute       |increased to 5 mins |
      | 2 minute       |increased to 5 mins |
      | 5 minute       |not changed         |
      | 10 minute      |not changed         |
