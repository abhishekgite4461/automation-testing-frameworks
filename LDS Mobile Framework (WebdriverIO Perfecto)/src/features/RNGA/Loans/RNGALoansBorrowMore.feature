@team-mco @nga3 @rnga @stub-enrolled-valid @primary
Feature: Loans - Borrow More Entry Point
  As an rnga customer with a eligible loan application
  I want to see "Borrow More" entry point from loan account action menu
  So that I can carry out loan application from account tile action menu

  #COE-101/75/91
  Scenario: TC_001 - Verify borrow more option from action tile action menu and landing page
    Given I am an enrolled "valid" user logged into the app
    When I navigate to personalLoan account on home page
    And I select the action menu of personalLoan account
    Then I should see the below options in the action menu of the personalLoan account
      | optionsInActionMenu  |
      | loanBorrowMoreOption |
    When I select the "borrowMore" option
    Then I should be displayed the Borrow more webview
