@smokeNga3 @rnga @nga3 @team-platform-engineering
Feature: Sanity test

  @stub-valid
  Scenario: Sanity test enrolment journey
    Given I am a current user on the home page

  @stub-enrolled-typical
  Scenario Outline: Sanity test light logon journey
    Given I am a current user on the home page
    When I select <fromAccount> account tile from home page
    And I am on the payment hub page
    And I submit transfer with from account <fromAccount> and to account <toAccount> with amount <amount>
    And I select the confirm option in the review transfer page
    Then I should see the corresponding details in the transfer success page
    Examples:
      | fromAccount | toAccount | amount |
      | current     | saving    | 1.00   |
