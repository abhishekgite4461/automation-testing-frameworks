class AppInfo {
    static get name() {
        return 'CBOS';
    }

    static get fileName() {
        return 'BNGA_BOS.3.ipa';
    }

    static get appPackage() {
        return 'com.lbg.mobile.cbos';
    }
}

module.exports = AppInfo;
