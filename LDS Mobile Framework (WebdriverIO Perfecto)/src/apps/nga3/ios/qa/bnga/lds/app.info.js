class AppInfo {
    static get name() {
        return 'CLloyds';
    }

    static get fileName() {
        return 'BNGA_LDS.3.ipa';
    }

    static get appPackage() {
        return 'com.lbg.mobile.clloyds';
    }
}

module.exports = AppInfo;
