class AppInfo {
    static get name() {
        return 'Halifax';
    }

    static get fileName() {
        return 'RNGA_HFX.3.ipa';
    }

    static get appPackage() {
        return 'com.lbg.mobile.halifax';
    }
}

module.exports = AppInfo;
