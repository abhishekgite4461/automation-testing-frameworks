class AppInfo {
    static get name() {
        return 'Lloyds Bank';
    }

    static get fileName() {
        return 'RNGA_LDS.3.ipa';
    }

    static get appPackage() {
        return 'com.lbg.mobile.rlloyds';
    }
}

module.exports = AppInfo;
