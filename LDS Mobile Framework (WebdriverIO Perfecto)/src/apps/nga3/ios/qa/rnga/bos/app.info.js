class AppInfo {
    static get name() {
        return 'Bank of Scot';
    }

    static get fileName() {
        return 'RNGA_BOS.3.ipa';
    }

    static get appPackage() {
        return 'com.lbg.mobile.rbos';
    }
}

module.exports = AppInfo;
