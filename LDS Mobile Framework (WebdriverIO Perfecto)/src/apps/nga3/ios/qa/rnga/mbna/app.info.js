class AppInfo {
    static get name() {
        return 'MBNA';
    }

    static get fileName() {
        return 'RNGA_MBNA.3.ipa';
    }

    static get appPackage() {
        return 'com.lbg.mobile.mbna';
    }
}

module.exports = AppInfo;
