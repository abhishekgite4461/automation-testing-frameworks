class AppInfo {
    static get name() {
        return 'MBNA';
    }

    static get fileName() {
        return 'RNGA3_MBNA_UAT.apk';
    }

    static get appPackage() {
        return 'com.lbg.mobile.mbna';
    }
}

module.exports = AppInfo;
