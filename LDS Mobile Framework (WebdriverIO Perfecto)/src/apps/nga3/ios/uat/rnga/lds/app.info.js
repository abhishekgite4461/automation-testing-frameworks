class AppInfo {
    static get name() {
        return 'Lloyds Bank';
    }

    static get fileName() {
        return 'RNGA3_LDS_UAT.ipa';
    }

    static get appPackage() {
        return 'com.lbg.mobile.rlloyds';
    }
}

module.exports = AppInfo;
