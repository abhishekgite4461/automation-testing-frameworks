class AppInfo {
    static get name() {
        return 'Halifax';
    }

    static get fileName() {
        return 'RNGA3_HFX_UAT.ipa';
    }

    static get appPackage() {
        return 'com.lbg.mobile.halifax';
    }
}

module.exports = AppInfo;
