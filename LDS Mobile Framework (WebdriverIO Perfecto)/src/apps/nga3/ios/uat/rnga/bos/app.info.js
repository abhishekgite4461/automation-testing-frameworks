class AppInfo {
    static get name() {
        return 'Bank of Scot';
    }

    static get fileName() {
        return 'RNGA3_BOS_UAT.ipa';
    }

    static get appPackage() {
        return 'com.lbg.mobile.rbos';
    }
}

module.exports = AppInfo;
