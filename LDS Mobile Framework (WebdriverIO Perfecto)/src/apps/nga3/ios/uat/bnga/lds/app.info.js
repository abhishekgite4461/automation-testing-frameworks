class AppInfo {
    static get name() {
        return 'BNGA_LDS_Tester_Obf';
    }

    static get appPackage() {
        return 'com.lbg.mobile.clloyds';
    }
}

module.exports = AppInfo;
