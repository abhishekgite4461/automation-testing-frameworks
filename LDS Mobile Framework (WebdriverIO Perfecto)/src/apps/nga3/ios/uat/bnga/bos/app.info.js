class AppInfo {
    static get name() {
        return 'BNGA_CBOS_Tester_Obf';
    }

    static get appPackage() {
        return 'com.lbg.mobile.cbos';
    }
}

module.exports = AppInfo;
