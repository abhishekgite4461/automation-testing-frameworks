const BaseAppInfo = require('../../../app.info');

class AppInfo extends BaseAppInfo {
    static get name() {
        return 'Lloyds Commercial';
    }

    static get appPackage() {
        return 'com.lloydsbank.businessmobile.qaBuildType';
    }

    static get appActivity() {
        return 'com.mobile.ui.environment.activity.EnvironmentSelectorActivity';
    }
}

module.exports = AppInfo;
