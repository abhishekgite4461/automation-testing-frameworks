const BaseAppInfo = require('../../../app.info');

class AppInfo extends BaseAppInfo {
    static get name() {
        return 'Bank of Scotland Commercial';
    }

    static get appPackage() {
        return 'uk.co.bankofscotland.businessbank.qaBuildType';
    }

    static get appActivity() {
        return 'com.mobile.ui.environment.activity.EnvironmentSelectorActivity';
    }
}

module.exports = AppInfo;
