class AppInfo {
    static get name() {
        return 'Lloyds Bank';
    }

    static get fileName() {
        return 'RNGA3_LDS_UAT.apk';
    }

    static get appPackage() {
        return 'com.grppl.android.shell.CMBlloydsTSB73.qaBuildType';
    }

    static get appActivity() {
        return 'com.mobile.ui.environment.activity.EnvironmentSelectorActivity';
    }
}

module.exports = AppInfo;
