const BaseAppInfo = require('../../../app.info');

class AppInfo extends BaseAppInfo {
    static get name() {
        return 'Bank of Scot';
    }

    static get fileName() {
        return 'RNGA3_BOS_UAT.apk';
    }

    static get appPackage() {
        return 'com.grppl.android.shell.BOS.qaBuildType';
    }

    static get appActivity() {
        return 'com.mobile.ui.environment.activity.EnvironmentSelectorActivity';
    }
}

module.exports = AppInfo;
