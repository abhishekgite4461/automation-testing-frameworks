const BaseAppInfo = require('../../../app.info');

class AppInfo extends BaseAppInfo {
    static get name() {
        return 'Halifax';
    }

    static get fileName() {
        return 'RNGA3_HFX_UAT.apk';
    }

    static get appPackage() {
        return 'com.grppl.android.shell.halifax.qaBuildType';
    }

    static get appActivity() {
        return 'com.mobile.ui.environment.activity.EnvironmentSelectorActivity';
    }
}

module.exports = AppInfo;
