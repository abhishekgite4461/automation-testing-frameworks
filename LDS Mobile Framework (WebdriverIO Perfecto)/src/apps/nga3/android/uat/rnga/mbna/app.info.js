class AppInfo {
    static get name() {
        return 'Beta Mbna';
    }

    static get fileName() {
        return 'RNGA3_MBNA_UAT.apk';
    }

    static get appPackage() {
        return 'uk.co.mbna.cardservices.android.qaBuildType';
    }

    static get appActivity() {
        return 'com.mobile.ui.environment.activity.EnvironmentSelectorActivity';
    }
}

module.exports = AppInfo;
