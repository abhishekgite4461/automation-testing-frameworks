class AppInfo {
    static get name() {
        return 'Halifax';
    }

    static get fileName() {
        return 'RNGA_HFX.3.apk';
    }

    static get appPackage() {
        return 'com.grppl.android.shell.halifax.qaBuildType';
    }

    static get appActivity() {
        return 'com.mobile.ui.environment.activity.EnvironmentSelectorActivity';
    }
}

module.exports = AppInfo;
