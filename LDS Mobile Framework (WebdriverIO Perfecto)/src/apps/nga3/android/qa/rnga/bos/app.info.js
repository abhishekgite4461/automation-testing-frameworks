class AppInfo {
    static get name() {
        return 'Bank of Scot';
    }

    static get fileName() {
        return 'RNGA_BOS.3.apk';
    }

    static get appPackage() {
        return 'com.grppl.android.shell.BOS.qaBuildType';
    }

    static get appActivity() {
        return 'com.mobile.ui.environment.activity.EnvironmentSelectorActivity';
    }
}

module.exports = AppInfo;
