class AppInfo {
    static get name() {
        return 'MBNA';
    }

    static get fileName() {
        return 'RNGA_MBNA.3.apk';
    }

    static get appPackage() {
        return 'uk.co.mbna.cardservices.android.qaBuildType';
    }

    static get appActivity() {
        return 'com.mobile.ui.environment.activity.EnvironmentSelectorActivity';
    }
}

module.exports = AppInfo;
