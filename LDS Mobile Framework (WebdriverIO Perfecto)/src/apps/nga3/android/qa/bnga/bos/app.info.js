class AppInfo {
    static get name() {
        return 'Bank of Scotland Commercial';
    }

    static get fileName() {
        return 'BNGA_BOS.3.apk';
    }

    static get appPackage() {
        return 'uk.co.bankofscotland.businessbank.qaBuildType';
    }

    static get appActivity() {
        return 'com.mobile.ui.environment.activity.EnvironmentSelectorActivity';
    }
}

module.exports = AppInfo;
