class AppInfo {
    static get name() {
        return 'Lloyds Commercial';
    }

    static get fileName() {
        return 'BNGA_LDS.3.apk';
    }

    static get appPackage() {
        return 'com.lloydsbank.businessmobile.qaBuildType';
    }

    static get appActivity() {
        return 'com.mobile.ui.environment.activity.EnvironmentSelectorActivity';
    }
}

module.exports = AppInfo;
