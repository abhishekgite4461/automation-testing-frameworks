class AppInfo {
    static get appActivity() {
        return 'co.uk.apptivation.nga.library.application.GrappleActivity';
    }
}

module.exports = AppInfo;
