module.exports = {
    appPackage: ['com.google.android.dialer', 'com.samsung.android.incallui'],
    appActivity: ['com.android.incallui.InCallActivity', 'com.android.incallui.SecInCallActivity',
        'com.android.incallui.call.InCallActivity']
};
