/* eslint import/no-dynamic-require: "off" */

class AppInfoFactory {
    static getAppInfo(caps) {
        const appVersion = caps.appVersion.toLowerCase();
        const platform = caps.platformName.toLowerCase();
        const appType = caps.appType.toLowerCase();
        const brand = caps.brand.toLowerCase();
        const phase = caps.isUAT ? 'uat' : 'qa';
        return require(['.', appVersion, platform, phase, appType, brand, 'app.info'].join('/'));
    }
}

module.exports = AppInfoFactory;
