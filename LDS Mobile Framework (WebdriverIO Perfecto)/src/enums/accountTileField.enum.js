const Enum = require('./enum');

module.exports = new Enum({
    LOAN_ACCOUNT_NUMBER: {value: '-?£?[-+]?[0-9]+'},
    CURRENT_BALANCE: {value: '-?£?[-+]?[0-9.,]+\\.[0-9]+'},
    ACCOUNT_ACCOUNT_INFO_BALANCE: {value: /-?£[-+]?[0-9.,]+\.[0-9]+/g},
    AVAILABLE_CREDIT_CARD_LIMIT: {value: /£[0-9.,]*\d.\d{2}/g},
    BANK_ACCOUNT_NUMBER: {value: /[\d\s]{16}|\d{8}/g},
    SORT_CODE_NUMBER: {value: /[0-9].{10}/},
    NIL_BALANCE: {value: /NIL/g},
    NA_BALANCE: {value: /N\/A|Not available/i},
    DUE_DATE: {value: /([0-9]{2}\s[A-Za-z]{3}\s[0-9]{4})/g},
    GREETING_MESSAGE: {value: /Good\s\w+|Welcome/},
    MORTGAGE_INFO_BALANCE: {value: /-?£[-+]?[0-9.,]+\.[0-9]+|NIL/g},
    BANK_LOGO: {value: /Bank\s\w+|logo/},
    OVERDRAFT_LIMIT_LABEL: {value: /Overdraft\s\w+|limit/},
    OVERDRAFT_LIMIT_VALUE: {value: / £[0-9.,]*\d{3}.\d{2}/},
    REMAINING_OVERDRAFT_LABEL: {value: /Remaining\s\w+|overdraft/},
    REMAINING_OVERDRAFT_VALUE: {value: /Remaining overdraft: £[0-9.,]*\d.\d{2}/}
});
