const Enum = require('./enum');
const DeviceHelper = require('../utils/device.helper');

const environmentScalingFactor = () => (DeviceHelper.isStub() ? 1 : 2);

module.exports = new Enum({
    TIMEOUTS: {
        IMPLICIT: () => 2000 * environmentScalingFactor(),
        WAIT_FOR: () => 10000 * environmentScalingFactor(),
        WAIT_FOR_SERVER_RESPONSE: () => 20000 * environmentScalingFactor(),
        WAIT_FOR_NOT_VISIBLE: () => 1000 * environmentScalingFactor(),
        EIA_CALL: () => 60000,
        LAUNCH_TIMEOUT: () => 45000 // time to wait for element on App launch
    }
});
