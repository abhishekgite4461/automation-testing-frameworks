const Enum = require('./enum');

module.exports = new Enum({
    SPAN: {element: 'span'},
    H1: {element: 'h1'},
    H2: {element: 'h2'},
    H3: {element: 'h3'},
    H4: {element: 'h4'},
    A: {element: 'a'},
    P: {element: 'p'},
    BUTTON: {element: 'button'},
    INPUT: {element: 'input'},
    LABEL: {element: 'label'},
    STRONG: {element: 'strong'},
    DIV: {element: 'div'},
    OPTION: {element: 'option'},
    SELECT: {element: 'select'},
    HEADER: {element: 'header'}
});
