const _ = require('lodash');

const Enum = require('./enum');

const NoSlackChannelAvailable = Symbol.for('NoSlackChannelAvailable');

class Teams extends Enum {
    constructor(enumLiterals) {
        super(_.fromPairs(_.map(_.toPairs(enumLiterals), ([name, props]) => [name, Object.assign(props, {tag: `@team-${name.replace('_', '-').toLowerCase()}`})])));
    }

    fromTag(tag) {
        if (!tag.startsWith('@team-')) {
            throw new Error(`Call to Team.fromTag with illegal tag: ${tag}`);
        }
        return this.fromString(tag.replace('@team-', '').replace('-', '_').toUpperCase());
    }
}

module.exports = new Teams({
    ICS: {slackChannel: '#t-ics'},
    MBNA: {slackChannel: '#t-mbna'},
    LOGIN: {slackChannel: '#t-login'},
    PLATFORM_ENGINEERING: {slackChannel: '#t-platform-eng'},
    PAYMENTS: {slackChannel: '#t-payments'},
    UAT: {slackChannel: NoSlackChannelAvailable},
    CORE: {slackChannel: '#t-core'},
    MPT: {slackChannel: '#t-mpt'},
    PANM: {slackChannel: '#t-persoandneedsmet'},
    STATEMENTS: {slackChannel: '#t-statements'},
    PERFORMANCE: {slackChannel: '#t-performance'},
    PAS: {slackChannel: '#t-pas-mobile-qa'},
    RELEASE: {slackChannel: '#t-release'},
    BR: {slackChannel: '#t-br'},
    COA: {slackChannel: '#t-pas-coa'},
    GDPR: {slackChannel: '#t-gdpr'},
    OUTBOUNDMESSAGING: {slackChannel: '#t-outboundmessaging'},
    REDESIGN: {slackChannel: '#redesign'},
    SERVICING: {slackChannel: '#t-accagg'},
    PI: {slackChannel: '#t-pi'},
    PAYMENT_AGGREGATOR: {slackChannel: '#t-pisp'},
    CROWBAR: {slackChannel: '#t-crowbar'},
    AOV: {slackChannel: '#t-aov'},
    MCO: {slackChannel: NoSlackChannelAvailable},
    DELEX: {slackChannel: NoSlackChannelAvailable},
    NAVIGATION: {slackChannel: NoSlackChannelAvailable},
    TRIAGE: {slackChannel: NoSlackChannelAvailable},
    SHAREIBAN: {slackChannel: NoSlackChannelAvailable}
});
