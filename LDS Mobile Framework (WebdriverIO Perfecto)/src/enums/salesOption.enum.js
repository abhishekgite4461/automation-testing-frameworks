const Enum = require('./enum');

module.exports = new Enum({
    NEW_CURRENT_ACCOUNT: {},
    NEW_SAVINGS_ACCOUNT: {},
    NEW_ISA_ACCOUNT: {},
    NEW_CREDIT_ACCOUNT: {},
    NEW_LOAN_ACCOUNT: {},
    NEW_MORTGAGE_ACCOUNT: {}
});
