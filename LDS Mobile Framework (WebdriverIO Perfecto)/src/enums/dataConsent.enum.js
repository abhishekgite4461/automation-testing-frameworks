const Enum = require('./enum');

module.exports = new Enum({
    PERFORMANCE: {identifier: 'performance'},
    MARKETING: {identifier: 'marketing'}
});
