const Enum = require('./enum');

module.exports = new Enum({
    UPDATE_PHONE_NUMBER: {},
    UPDATE_EMAIL_ADDRESS: {},
    REVIEW_UK_BENEFICIARY_DETAILS: {},
    REVIEW_COMPANY_BENEFICIARY_DETAILS: {},
    ADD_NEW_PAYM_DETAILS: {},
    REACTIVATE_ISA_CONFIRMATION_DETAILS: {}
});
