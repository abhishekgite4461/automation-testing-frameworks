const Enum = require('../enum');

module.exports = new Enum({
    HFX: {
        ukNumber: '0345 720 3040', outsideUKNumber: '+44 1132 422 229'
    },
    BOS: {
        ukNumber: '0345 721 3141', outsideUKNumber: '+44 1313 392 573'
    },
    LDS: {
        ukNumber: '0345 300 0000', outsideUKNumber: '+44 1733 347 007'
    }
});
