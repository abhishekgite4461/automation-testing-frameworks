const Enum = require('../enum');

module.exports = new Enum({
    ENROLMENT: {
        appName: 'Device enrollment', defaultStateOn: true, restApiName: 'SW_NGA_ENROL_SWITCH', ibcName: 'SW_NGA_DvcEnrol', isGlobal: false
    },
    LIGHT_LOGON: {
        appName: 'Light logon', defaultStateOn: true, restApiName: 'SW_NGA_LIGHT_LOGON', ibcName: 'SW_NGA_Logon', isGlobal: false
    },
    TOP_UP: {
        appName: 'Top up', defaultStateOn: true, restApiName: 'SW_ENISATOPUPNGA', ibcName: 'SW_EnISATopUpNGA', isGlobal: false
    },
    IBREG_MVP: {
        appName: 'Native Registration', defaultStateOn: true, restApiName: 'SW_EnRNGANtvIBReg', ibcName: 'SW_EnRNGANtvReg', isGlobal: false
    },
    ENABLE_YOUTH: {
        appName: 'Youth Account Registration', defaultStateOn: true, restApiName: 'SW_EnblYouthIB', ibcName: 'SW_EnblYouthIB', isGlobal: false
    },
    SECURE_CALL: {
        appName: 'Secure call', defaultStateOn: true, restApiName: 'SW_EnAthClRNGA', ibcName: 'SW_EnAthClRNGA', isGlobal: false
    },
    TELL_US_NEW_DESIGN: {
        appName: 'Tell us new design', defaultStateOn: true, restApiName: 'SW_EnTellUsRNGA', ibcName: 'SW_EnTellUsRNGA', isGlobal: true
    },
    APPLE_PAY: {
        appName: 'Apple Pay', defaultStateOn: true, restApiName: 'SW_EnRgsLkAppPay', ibcName: 'SW_EnRgsLkAppPay', isGlobal: true
    },
    OSR: {
        appName: 'Osr', defaultStateOn: true, restApiName: 'SW_AlwOlSvRedGA', ibcName: 'SW_AlwOlSvRedGA', isGlobal: false
    },
    REPLACEMENT_CARDS: {
        appName: 'Replacement cards', defaultStateOn: true, restApiName: 'SW_EnRpCrdsPsNGA', ibcName: 'SW_EnRpCrdsPsNGA', isGlobal: false
    },
    SMR: {
        appName: 'Smr', defaultStateOn: true, restApiName: 'SW_EnBIRSMRRNGA', ibcName: 'SW_EnBIRSMRRNGA', isGlobal: false
    },
    STRATEGIC_SWITCH_FIX_MIGRATION: {
        appName: 'Strategic Switch Fix Migration', defaultStateOn: true, restApiName: 'SW_EnStrMigrnNGA', ibcName: 'SW_EnStrMigrnNGA', isGlobal: true
    },
    PAYMENT_HUB_RNGA: {
        appName: 'Payment Hub RNGA', defaultStateOn: true, restApiName: 'SW_PymntHbRNGA', ibcName: 'SW_PymntHbRNGA', isGlobal: false
    },
    AUTH_CALL_TO_TELEPHONY_RNGA: {
        appName: 'Enable Authenticated Call to Telephony RNGA', defaultStateOn: true, restApiName: 'SW_EnAthClRNGA', ibcName: 'SW_EnAthClRNGA', isGlobal: false
    },
    NEW_CALL_FEATURES: {
        appName: 'Display New Call Features RNGA', defaultStateOn: true, restApiName: 'SW_DispNwClRNGA', ibcName: 'SW_DispNwClRNGA', isGlobal: false
    },
    RNGA_DEPOSIT_CHEQUE: {
        appName: 'Cheque Deposit', defaultStateOn: true, restApiName: 'SW_EnFCMDeptRNGA', ibcName: 'SW_EnFCMDeptRNGA', isGlobal: false
    },
    RNGA_HISTORY_CHEQUE: {
        appName: 'Cheque History', defaultStateOn: true, restApiName: 'SW_EnFCMHstRNGA', ibcName: 'SW_EnFCMHstRNGA', isGlobal: false
    },
    CHEQUE_IMAGING_ENABLED: {
        appName: 'Cheque Imaging Enabled', defaultStateOn: true, restApiName: 'SW_EnICSFCMMbl30', ibcName: 'SW_EnICSFCMMbl30', isGlobal: true
    },
    RNGA_INDUSTRIAL: {
        appName: 'Cheque Industrial', defaultStateOn: true, restApiName: 'SW_EnFCMIndRNGA', ibcName: 'SW_EnFCMIndRNGA', isGlobal: false
    },
    INTERNATIONAL_PAYMENTS: {
        appName: 'Display International Payee in Payment Hub', defaultStateOn: true, restApiName: 'SW_INTERNATIONAL_PAYMENT', ibcName: 'SW_Allow_SIP', isBranded: true
    },
    CARD_MANAGEMENT: {
        appName: 'Card Management', defaultStateOn: true, restApiName: 'SW_RNGACardControls', ibcName: 'SW_EnDCC_RNGA', isGlobal: false
    },
    IBREG_EIA: {
        appName: 'Enable Registration EIA', defaultStateOn: true, restApiName: 'SW_DisableEIA', ibcName: 'SW_DisableEIA', isGlobal: false
    },
    IBREG_MANDATELESS: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'No Mandate for Logon', isEnrolled: true
    },
    MARKETING_CONSENTS_LLOYDS: {
        appName: 'Marketing Preferences LDS', defaultStateOn: true, restApiName: 'SW_EnNwMrktCnLyd', ibcName: 'SW_EnNwMrktCnLyd', isGlobal: true
    },
    MARKETING_CONSENTS_BOS: {
        appName: 'Marketing Preferences BOS', defaultStateOn: true, restApiName: 'SW_EnNwMrktCnBos', ibcName: 'SW_EnNwMrktCnBos', isGlobal: true
    },
    MARKETING_CONSENTS_HALIFAX: {
        appName: 'Marketing Preferences HFX', defaultStateOn: true, restApiName: 'SW_EnNwMrktCnHal', ibcName: 'SW_EnNwMrktCnHal', isGlobal: true
    },
    MARKETING_CONSENTS_MBNA: {
        appName: 'Marketing Preferences MBNA', defaultStateOn: true, restApiName: 'SW_EnNwMrktCnMbna', ibcName: 'SW_EnNwMrktCnMbna', isGlobal: true
    },
    SCOTTISH_WIDOWS_TILES: {
        appName: 'Scottish Widows Pension tiles', defaultStateOn: true, restApiName: 'SW_ShScWidPenNGA', ibcName: 'SW_ShScWidPenNGA', isGlobal: false
    },
    STANDING_ORDERS_AND_DIRECT_DEBIT: {
        appName: 'Standing Orders and Direct Debit', defaultStateOn: true, restApiName: 'SW_RtlNGASODD', ibcName: 'SW_RtlNGASODD', isBranded: true
    },
    STANDING_ORDER: {
        appName: 'Standing Order', defaultStateOn: true, restApiName: 'SW_EnStgOrdrRNGA', ibcName: 'SW_EnStgOrdrRNGA', isBranded: true
    },
    RATE_APP_IOS: {
        appName: 'Enable App Rating', defaultStateOn: true, restApiName: 'SW_AppRtngRtlIOS', ibcName: 'SW_AppRtngRtlIOS', isBranded: true
    },
    RATE_APP_ANDROID: {
        appName: 'Enable App Rating', defaultStateOn: true, restApiName: 'SW_AppRtngRtlAnd', ibcName: 'SW_AppRtngRtlAnd', isBranded: true
    },
    STANDING_ORDER_V2: {
        appName: 'Standing Order V2', defaultStateOn: true, restApiName: 'SW_EnStOrdRNGAv2', ibcName: 'SW_EnStOrdRNGAv2', isBranded: true
    },
    CREATE_STANDING_ORDER: {
        appName: 'Create Standing Order', defaultStateOn: true, restApiName: 'SW_RtlNGACrSO', ibcName: 'SW_RtlNGACrSO', isBranded: true
    },
    COA_NATIVE: {
        appName: 'CoA Native Enabled', defaultStateOn: false, restApiName: 'SW_ChngAdr30', ibcName: 'SW_ChngAdr30', isGlobal: false
    },
    COA_CWA: {
        appName: 'CoA Enabled (CWA)', defaultStateOn: false, restApiName: 'SW_ChngAddr', ibcName: 'SW_ChngAddr', isGlobal: false
    },
    DISPLAY_KYC: {
        appName: 'Display KYC', defaultStateOn: false, restApiName: 'SW_DispKYCPopup', ibcName: 'SW_DispKYCPopup', isGlobal: false
    },
    MOBILE_CHAT_ANDROID: {
        appName: 'Mobile Chat RNGA Android', defaultStateOn: true, restApiName: 'SW_EnChatNGA_AND', ibcName: 'SW_EnChatNGA_AND', isGlobal: false
    },
    MOBILE_CHAT_IOS: {
        appName: 'Mobile Chat RNGA iOS', defaultStateOn: true, restApiName: 'SW_EnChatNGA_IOS', ibcName: 'SW_EnChatNGA_IOS', isGlobal: false
    },
    FEEDBACK_FORM: {
        appName: 'Feedback Form', defaultStateOn: true, restApiName: 'SW_DispFdBkForm', ibcName: 'SW_DispFdBkForm', isBranded: true
    },
    CMA_NGA: {
        appName: 'Enable CMA Survey Feature', defaultStateOn: true, restApiName: 'SW_EnCMANGA', ibcName: 'SW_EnCMANGA', isGlobal: false
    },
    DATA_CONSENT: {
        appName: 'Analytics Consents Enabled', defaultStateOn: true, restApiName: 'SW_EnGDPRDtCnst', ibcName: 'SW_EnGDPRDtCnst', isGlobal: false
    },
    NOTIFICATION_CENTRE: {
        appName: 'Notification Centre', defaultStateOn: true, restApiName: 'SW_EN_NTN_CTR_NGA', ibcName: 'SW_EN_NTN_CTR_NGA', isGlobal: true
    },
    STATEMENT_FREQ: {
        appName: 'Enable Statement Frequency', defaultStateOn: true, restApiName: 'SW_EnStFrCuSvPro', ibcName: 'SW_EnStFrCuSvPro', isGlobal: true
    },
    PASSBOOK_ACCOUNT: {
        appName: 'Passbook account notifications is enabled', defaultStateOn: true, restApiName: 'SW_DspPbkNtfNGA', ibcName: 'SW_DspPbkNtfNGA', isGlobal: true
    },
    ACCOUNT_AGGREGATION: {
        appName: 'Account Aggregation', defaultStateOn: true, restApiName: 'SW_EnOBAARNGA', ibcName: 'SW_EnOBAARNGA', isGlobal: false
    },
    PAYMENT_AGGREGATION: {
        appName: 'Payment from agg to agg or own account', defaultStateOn: false, restApiName: 'SW_EnPymtAgg2Acc', ibcName: 'SW_EnPymtAgg2Acc', isGlobal: false
    },
    REMEMBER_ME: {
        appName: 'Enable Remember Me', defaultStateOn: false, restApiName: 'SW_EnRememberMe', ibcName: 'SW_EnRememberMe', isGlobal: false
    },
    PCCDC: {
        appName: 'Pay credit card by debit card', defaultStateOn: false, restApiName: 'SW_EnPccDcNGA', ibcName: 'SW_EnPccDcNGA', isGlobal: false
    },
    PUSH_OPTIN_A: {
        appName: 'Enable Push Opt-In versionA', defaultStateOn: false, restApiName: 'SW_EnPsNtfctnIOS', ibcName: 'SW_EnPsNtfctnIOS', isGlobal: false
    },
    PUSH_OPTIN_B: {
        appName: 'Enable Push Opt-In versionB', defaultStateOn: false, restApiName: 'SW_OptVrsnCtlIOS', ibcName: 'SW_OptVrsnCtlIOS', isGlobal: false
    },
    PUSH_OPTIN_B_V2: {
        appName: 'Enable Push Opt-In versionB', defaultStateOn: false, restApiName: 'SW_OptVrCtlIOSV2', ibcName: 'SW_OptVrCtlIOSV2', isGlobal: false
    },
    PUSH_NOTIFICATION: {
        appName: 'Push notifications is enabled', defaultStateOn: false, restApiName: 'SW_EnPsNtfctnAND', ibcName: 'SW_EnPsNtfctnAND', isGlobal: false
    },
    UPCOMING_PAYMENTS: {
        appName: 'Upcoming Payments', defaultStateOn: true, restApiName: 'SW_UpmgPymntCWA', ibcName: 'SW_UpmgPymntCWA', isGlobal: false
    },
    SPENDING_REWARDS_CWA: {
        appName: 'Spending Rewards CWA', defaultStateOn: true, restApiName: 'SW_SdRwdCWANGA', ibcName: 'SW_SdRwdCWANGA', isGlobal: false
    },
    SR_UR_OPTIN: {
        appName: 'Spending Rewards Unregistered user Opt in', defaultStateOn: true, restApiName: 'SW_SWUROpt_in2.5', ibcName: 'SW_SWUROpt_in2.5', isGlobal: false
    },
    NGA_RESTRICT_ACCESS: {
        appName: 'NGA Retail - Device ID - Restrict Access', defaultStateOn: true, restApiName: 'SW_NGA_RESTRICT_ACCESS', ibcName: 'SW_NGA_DvcRest', isGlobal: false
    },
    RNGA_LT: {
        appName: 'RNGA - Loan Detail and Transactions', defaultStateOn: true, restApiName: 'SW_LT', ibcName: 'SW_RNGALnDtlTr', isGlobal: false
    },
    ENABLE_RNGA_VEF_CREDIT_CARD: {
        appName: 'Enable RNGA VEF Credit Card', defaultStateOn: false, restApiName: 'SW_EnRNGAVEFCdt', ibcName: 'SW_EnRNGAVEFCdt', isGlobal: false
    },
    ENABLE_MORTGAGE_OVERPAYMENT: {
        appName: 'Mortgage Overpayment', defaultStateOn: true, restApiName: 'SW_EnOvPmtMorNGA', ibcName: 'SW_EnOvPmtMorNGA', isGlobal: false
    },
    AB_TESTING: {
        appName: 'Enable AB testing on NGA', defaultStateOn: true, restApiName: 'SW_EnABTsngOnNGA', ibcName: 'SW_EnABTsngOnNGA', isGlobal: false
    },
    PERSONALISED_EXPERIENCES_ANDROID: {
        appName: 'Enable personalised experiences on Android devices for NGA', defaultStateOn: true, restApiName: 'SW_EnPerExpAnd', ibcName: 'SW_EnPerExpAnd', isGlobal: false
    },
    PERSONALISED_EXPERIENCES_IOS: {
        appName: 'Enable personalised experiences on iOS devices for NGA', defaultStateOn: true, restApiName: 'SW_EnPerExpIOS', ibcName: 'SW_EnPerExpIOS', isGlobal: false
    },
    CALL_US_LOST_AND_STOLEN_CARDS: {
        appName: 'Enable Lost and Stolen cards', defaultStateOn: true, restApiName: 'SW_EnLnSlnRtNGA', ibcName: 'SW_EnLnSlnRtNGA', isGlobal: false
    },
    PCA_ALLTAB: {
        appName: 'NGA Enable PCA All Tab', defaultStateOn: true, restApiName: 'SW_NGA_EnAllTab', ibcName: 'SW_NGA_EnAllTab', isGlobal: false
    },
    SEARCH_TRANSACTIONS: {
        appName: 'Search Transactions', defaultStateOn: true, restApiName: 'SW_NGA_TxnSearch', ibcName: 'SW_NGA_TxnSearch', isGlobal: false
    },
    FASTER_PAYMENT_ERROR: {
        appName: 'Faster payments error', defaultStateOn: false, restApiName: 'SW_EnFPayError', ibcName: 'SW_EnFPayError', isGlobal: false
    },
    AMEND_STANDING_ORDER: {
        appName: 'Amend Standing Order', defaultStateOn: true, restApiName: 'SW_EnAmendSoNGA', ibcName: 'SW_EnAmendSoNGA', isGlobal: false
    },
    DELETE_STANDING_ORDER: {
        appName: 'Delete Standing Order', defaultStateOn: true, restApiName: 'SW_EnDeleteSoNGA', ibcName: 'SW_EnDeleteSoNGA', isGlobal: false
    },
    ENABLE_IOS_WKWEBVIEW: {
        appName: 'Enable WKwebview iOS', defaultStateOn: true, restApiName: 'SW_EnWKWebiOS', ibcName: 'SW_EnWKWebiOS', isGlobal: false
    },
    HCC_ANDROID: {
        appName: 'High cost of credit programme', defaultStateOn: false, restApiName: 'SW_NGAAndHCC', ibcName: 'SW_NGAAndHCC', isGlobal: false
    },
    HCC_IOS: {
        appName: 'High cost of credit programme', defaultStateOn: false, restApiName: 'SW_NGAiOSHCC', ibcName: 'SW_NGAiOSHCC', isGlobal: false
    },
    SPENDINGINSIGHTS_IOS: {
        appName: 'Spending Insights', defaultStateOn: false, restApiName: 'SW_NGAiOSSIPCA', ibcName: 'SW_NGAiOSSIPCA', isGlobal: false
    },
    SPENDINGINSIGHTS_ANDROID: {
        appName: 'Spending Insights', defaultStateOn: false, restApiName: 'SW_NGAAndSIPCA', ibcName: 'SW_NGAAndSIPCA', isGlobal: false
    },
    COP_NGA_IOS: {
        appName: 'Enable Confirm Payee Feature', defaultStateOn: false, restApiName: 'SW_EnConfPayNGA', ibcName: 'SW_EnConfPayNGA', isGlobal: false
    },
    COP_NGA_AND: {
        appName: 'Enable CoP journey', defaultStateOn: false, restApiName: 'SW_EnConfPayNGA', ibcName: 'SW_EnConfPayNGA', isGlobal: false
    },
    SCA_ANDROID: {
        appName: 'Enable SCA journey', defaultStateOn: true, restApiName: 'SW_EnPmtNwPayAnd', ibcName: 'SW_EnPmtNwPayAnd', isGlobal: false
    },
    SCA_IOS: {
        appName: 'SCA payment journey', defaultStateOn: true, restApiName: 'SW_EnPmtNwPayIOS', ibcName: 'SW_EnPmtNwPayIOS', isGlobal: false
    },
    WHITELISTED_BILLER: {
        appName: 'New whitelisted biller journey', defaultStateOn: false, restApiName: 'SW_EnNewBillPay', ibcName: 'SW_EnNewBillPay', isGlobal: false
    },
    DIRECT_DEBIT_NGA: {
        appName: 'Direct Debit NGA', defaultStateOn: true, restApiName: 'SW_EnDrDbtNGA', ibcName: 'SW_EnDrDbtNGA', isGlobal: false
    },
    ENABLE_SCOTTISH_WIDOWS_ANDROID: {
        appName: 'Enable Scottish Widows Pension tile in Android devices for NGA', defaultStateOn: false, restApiName: 'SW_EnScWdPeTiAnd', ibcName: 'SW_EnScWdPeTiAnd', isGlobal: false
    },
    ENABLE_SCOTTISH_WIDOWS_IOS: {
        appName: 'Enable Scottish Widows Pension tile in iOS devices for NGA', defaultStateOn: false, restApiName: 'SW_EnScWdPeTiIOS', ibcName: 'SW_EnScWdPeTiIOS', isGlobal: false
    },
    ENABLE_PUSH_OPTIN_ANDROID: {
        appName: 'Enable push opt in for chat on Android devices', defaultStateOn: false, restApiName: 'SW_EnPhOpInChAnd', ibcName: 'SW_EnPhOpInChAnd', isGlobal: false
    },
    ENABLE_PUSH_OPTIN_IOS: {
        appName: 'Enable push opt in for chat on IOS devices', defaultStateOn: false, restApiName: 'SW_EnPhOpInChIOS', ibcName: 'SW_EnPhOpInChIOS', isGlobal: false
    },
    ENABLE_PLACEMENT_RNGA_IOS: {
        appName: 'Enable placement before login for RNGA on IOS devices', defaultStateOn: false, restApiName: 'SW_EnPlBLRNGAIos', ibcName: 'SW_EnPlBLRNGAIos', isGlobal: false
    },
    ENABLE_PLACEMENT_RNGA_ANDROID: {
        appName: ' Enable placement before login for RNGA on Android devices', defaultStateOn: false, restApiName: 'SW_EnPlBLRNGAAnd', ibcName: 'SW_EnPlBLRNGAAnd', isGlobal: false
    },
    ENABLE_RTIM_RNGA_IOS: {
        appName: 'Enable RTIM Service for RNGA on IOS', defaultStateOn: false, restApiName: 'SW_EnRTIMRNGAIos', ibcName: 'SW_EnRTIMRNGAIos', isGlobal: false
    },
    ENABLE_RTIM_RNGA_ANDROID: {
        appName: 'Enable RTIM Service for RNGA on Android', defaultStateOn: false, restApiName: 'SW_EnRTIMRNGAAnd', ibcName: 'SW_EnRTIMRNGAAnd', isGlobal: false
    },
    ENABLE_TARGET_AND_CELEBRUS_CONNECTIVITY: {
        appName: 'Enable Target and Celebrus connectivity', defaultStateOn: false, restApiName: 'SW_EnTarCelCtvty', ibcName: 'SW_EnTarCelCtvty', isGlobal: false
    },
    SAVE_THE_CHANGE_NATIVE: {
        appName: 'Enable Native Save the Change', defaultStateOn: true, restApiName: 'SW_EnSTCNative', ibcName: 'SW_EnSTCNative', isGlobal: false
    },
    SAVE_THE_CHANGE_CWA: {
        appName: 'Enable CWA Save the Change', defaultStateOn: true, restApiName: 'SW_EnableSTCNGA', ibcName: 'SW_EnableSTCNGA', isGlobal: false
    },
    ENABLE_PENSION_TRANSFER: {
        appName: 'NGA Retail - Enable Pension Transfer', defaultStateOn: true, restApiName: 'SW_EnPensTranNGA', ibcName: 'SW_EnPensTranNGA', isGlobal: false
    },
    TOTALPENDING_ANDROID: {
        appName: 'Exposed pending transaction feature', defaultStateOn: false, restApiName: 'SW_NGAAndTTLPND', ibcName: 'SW_NGAAndTTLPND', isGlobal: false
    },
    INAPP_SEARCH_ANDROID: {
        appName: 'Enable In app search', defaultStateOn: false, restApiName: 'SW_AppSearch_And', ibcName: 'SW_AppSearch_And', isGlobal: false
    },
    INAPP_SEARCH_IOS: {
        appName: 'Enable in app search', defaultStateOn: false, restApiName: 'SW_AppSearch_IOS', ibcName: 'SW_AppSearch_IOS', isGlobal: false
    },
    TRANSACTION_LOGO_ANDROID: {
        appName: 'Enable transaction logo', defaultStateOn: false, restApiName: 'SW_NGAAndLogoPCA', ibcName: 'SW_NGAAndLogoPCA', isGlobal: false
    },
    TRANSACTION_LOGO_IOS: {
        appName: 'Enable transaction logo', defaultStateOn: false, restApiName: 'SW_NGAiOSLogoPCA', ibcName: 'SW_NGAiOSLogoPCA', isGlobal: false
    },
    PAY_CREDIT_BY_MASTER_CARD: {
        appName: 'Pay credit card by master card', defaultStateOn: false, restApiName: 'SW_EnMCCPayCC', ibcName: 'SW_EnMCCPayCC', isGlobal: false
    },
    BIOMETRIC_PAYMENT: {
        appName: 'Biometric Payment', defaultStateOn: true, restApiName: 'SW_EnBioPymtsNGA', ibcName: 'SW_EnBioPymtsNGA', isGlobal: false
    },
    VIEWPIN_ANDROID: {
        appName: 'View PIN - Enable Entry Point', defaultStateOn: false, restApiName: 'SW_Enbl_View_PIN_Service_NGA_Android', ibcName: 'SW_Enbl_View_PIN_Service_NGA_Android', isGlobal: false
    },
    VIEWPIN_PASSWORD_ANDROID: {
        appName: 'View PIN - Require Password', defaultStateOn: false, restApiName: 'SW_Enbl_View_PIN_Pwd_Chck_NGA_Android', ibcName: 'SW_Enbl_View_PIN_Pwd_Chck_NGA_Android', isGlobal: false
    },
    VIEWPIN_IOS: {
        appName: 'View Pin Service', defaultStateOn: false, restApiName: 'SW_Enbl_View_PIN_Service_NGA_IOS', ibcName: 'SW_Enbl_View_PIN_Service_NGA_IOS', isGlobal: false
    },
    VIEWPIN_PASSWORD_IOS: {
        appName: 'View Pin Password Check', defaultStateOn: false, restApiName: 'SW_Enbl_View_PIN_Pwd_Chck_NGA_IOS', ibcName: 'SW_Enbl_View_PIN_Pwd_Chck_NGA_IOS', isGlobal: false
    },
    OVERDRAFT_REMAINING_IOS: {
        appName: 'Enable NGA iOS Overdraft remaining field', defaultStateOn: false, restApiName: 'SW_NGAIOSODRem', ibcName: 'SW_NGAIOSODRem', isGlobal: false
    },
    OVERDRAFT_REMAINING_ANDROID: {
        appName: 'Enable NGA Android Overdraft remaining field', defaultStateOn: false, restApiName: 'SW_NGAANDODRem', ibcName: 'SW_NGAANDODRem', isGlobal: false
    },
    SHARE_INTERNATIONAL_ACCOUNT_DETAILS: {
        appName: 'Share international account details', defaultStateOn: false, restApiName: 'SW_EnShareAccHub', ibcName: 'SW_EnShareAccHub', isGlobal: false
    },
    TQT_ANDROID: {
        appName: 'Enable NGA Android Triage Tool PCA', defaultStateOn: false, restApiName: 'SW_NGAAndTRTOOL', ibcName: 'SW_NGAAndTRTOOL', isGlobal: false
    },
    TRAVEL_DISPUTES_ANDROID: {
        appName: 'Enable Travel dispute feature', defaultStateOn: false, restApiName: 'SW_DisRNGA_And', ibcName: 'SW_DisRNGA_And', isGlobal: false
    },
    TRAVEL_DISPUTES_IOS: {
        appName: 'Enable Travel Disputes CTA', defaultStateOn: false, restApiName: 'SW_DisRNGA_IOS', ibcName: 'SW_DisRNGA_IOS', isGlobal: false
    },
    MONTHLY_PDF_ANDROID: {
        appName: 'Enable Monthly PDFs entry point', defaultStateOn: false, restApiName: 'SW_NGAAndPDFCA', ibcName: 'SW_NGAAndPDFCA', isGlobal: false
    },
    REWARD_HUB_ENTRY_IOS: {
        appName: 'Enable Reward Hub entry point on iOS devices for NGA', defaultStateOn: false, restApiName: 'SW_EnRwdHubiOS', ibcName: 'SW_EnRwdHubiOS', isGlobal: false
    },
    REWARD_HUB_ENTRY_ANDROID: {
        appName: 'Enable Reward Hub entry point on android devices for NGA', defaultStateOn: false, restApiName: 'SW_EnRwdHubAnd', ibcName: 'SW_EnRwdHubAnd', isGlobal: false
    },
    SPENDING_REWARD_REWARD_HUB: {
        appName: 'Show Spending Reward on Reward Hub home page', defaultStateOn: false, restApiName: 'SW_SpRwdonRwdHub', ibcName: 'SW_SpRwdonRwdHub', isGlobal: false
    },
    REWARDS_EXTRA_REWARD_HUB: {
        appName: 'Show Reward Extras on Reward Hub home page', defaultStateOn: false, restApiName: 'SW_RwdExonRwdHub', ibcName: 'SW_RwdExonRwdHub', isGlobal: false
    },
    SAVERS_PRIZEDRAW_REWARD_HUB: {
        appName: 'Show Savers Prize Draw on Reward Hub home page', defaultStateOn: false, restApiName: 'SW_SavPDonRwdHub', ibcName: 'SW_SavPDonRwdHub', isGlobal: false
    },
    MORTGAGE_PRIZEDRAW_REWARD_HUB: {
        appName: 'Show Mortgage Prize Draw on Reward Hub home page', defaultStateOn: false, restApiName: 'SW_MorPDonRwdHub', ibcName: 'SW_MorPDonRwdHub', isGlobal: false
    }
});
