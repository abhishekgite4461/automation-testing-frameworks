const Enum = require('./enum');

module.exports = new Enum({
    PORTRAIT: {value: '0'},
    LANDSCAPE: {value: '1'},
    PORTRAIT_REVERSE: {value: '2'},
    LANDSCAPE_REVERSE: {value: '3'}
});
