const Enum = require('./enum');

module.exports = new Enum({
    BOTTOM_NAV_PRODUCT_HUB: {
        activityName: 'bottomNavProductHub', options: ['apply', 'products', 'offers']
    },
    BOTTOM_NAV_SUPPORT_HUB: {
        activityName: 'bottomNavSupportHub', options: ['support', 'help']
    },
    DARK_URL_BUTTON: {
        activityName: 'moreTabDarkURL', options: ['Open', 'Submit']
    },
    PRIORITY_MESSAGE: {
        activityName: 'accountOverviewPriorityMessage', options: ['normal', 'callToAction']
    },
    MESSAGE_US_BUTTON: {
        activityName: 'supportHubMessagingTitle', options: ['messagingTitleMessage', 'messagingTitleChat']
    }
});
