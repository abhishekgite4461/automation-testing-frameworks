const Enum = require('./enum');

module.exports = new Enum({
    CHANGE_CREDIT_LIMIT: {},
    REPAYMENT_HOLIDAY: {},
    ADDITIONAL_PAYMENT: {},
    BALANCE_TRANSFER: {},
    BORROW_MORE: {},
    LOAN_CLOSURE: {},
    RENEW_YOUR_SAVINGS_ACCOUNT: {},
    OVERDRAFT: {},
    CHANGE_ACCOUNT_TYPE: {},
    PENSION_TRANSFER: {}
});
