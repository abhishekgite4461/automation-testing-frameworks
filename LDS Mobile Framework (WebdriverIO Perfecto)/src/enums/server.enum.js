const Enum = require('./enum');

module.exports = new Enum({
    STUB: {group: 'STUB', legacyName: 'STUB', testPhase: 'STUB'},
    'SIT2.1': {
        group: 'SIT2-GLZ', legacyName: 'SIT2.1-GLZ', testPhase: 'MFIT', platform: 'Galaxy', appName: 'SIT2.1'
    },
    'SIT2.2': {group: 'SIT2-GLZ', legacyName: 'SIT2.2-GLZ'},
    'SIT2.3': {
        group: 'SIT2-GLZ', legacyName: 'SIT2.3-GLZ', testPhase: 'IFIT', platform: 'Galaxy', appName: 'SIT2.3'
    },
    'PUT2.1': {group: 'PUT2-GLZ', legacyName: 'PUT2.1-GLZ'},
    'PUT2.2': {group: 'PUT2-GLZ', legacyName: 'PUT2.2-GLZ'},
    'PUT2.3': {group: 'PUT2-GLZ', legacyName: 'PUT2.3-GLZ'},
    'SIT-3': {group: 'SIT-3 GLZ', legacyName: 'SIT-3 GLZ'},
    'SIT-4': {
        group: 'SIT-4 GLZ', legacyName: 'SIT-4 GLZ', testPhase: 'BR', platform: 'Galaxy', appName: 'SIT4', isEiaBypassed: false
    },
    'SIT02-W': {
        group: 'SIT-DI2', legacyName: 'SIT02-White-DI2', testPhase: 'MFIT', platform: 'Di2', appName: 'SIT02-White', isEiaBypassed: true
    },
    'SIT01-W': {
        group: 'SIT-DI2', legacyName: 'SIT01-White-DI2', testPhase: 'BR', platform: 'Di2', appName: 'SIT01-White', isEiaBypassed: false
    },
    'SIT02-B': {
        group: 'SIT-DI2', legacyName: 'SIT02-Black-DI2', testPhase: 'MFIT', platform: 'Di2', appName: 'SIT02-Black', isEiaBypassed: true
    },
    'SIT01-B': {
        group: 'SIT-DI2', legacyName: 'SIT01-Black-DI2', testPhase: 'BR', platform: 'Di2', appName: 'SIT01-Black', isEiaBypassed: false
    },
    'SIT03-B': {
        group: 'SIT-DI2', legacyName: 'SIT03-Black-DI2', testPhase: 'BR', platform: 'Di2', appName: 'SIT03-Black', isEiaBypassed: false
    },
    'SIT03-W': {
        group: 'SIT-DI2', legacyName: 'SIT03-White-DI2', testPhase: 'BR', platform: 'Di2', appName: 'SIT03-White', isEiaBypassed: false
    },
    'SIT09-W': {
        group: 'SIT-DI2', legacyName: 'SIT09-White-DI2', testPhase: 'MFIT', platform: 'Di2', appName: 'SIT09-White', isEiaBypassed: false
    },
    'SIT10-B': {
        group: 'SIT-DI2', legacyName: 'SIT10-Black-DI2', testPhase: 'MFIT', platform: 'Di2', appName: 'SIT10-Black', isEiaBypassed: false
    },
    'SIT10-W': {
        group: 'SIT-DI2', legacyName: 'SIT10-White-DI2', testPhase: 'MFIT', platform: 'Di2', appName: 'SIT10-White', isEiaBypassed: false
    },
    PUT12: {
        group: 'PUT-DI2', legacyName: 'PUT12-DI2', testPhase: 'BR', platform: 'Di2', appName: 'PUT12', isEiaBypassed: false
    },
    PUT14: {
        group: 'PUT-DI2', legacyName: 'PUT14-DI2', testPhase: 'MFIT', platform: 'Di2', appName: 'PUT14', isEiaBypassed: false
    },
    PUT02: {
        group: 'PUT-DI2', legacyName: 'PUT02-DI2', testPhase: 'MFIT', platform: 'Di2', appName: 'PUT02', isEiaBypassed: false
    },
    PUT03: {
        group: 'PUT-DI2', legacyName: 'PUT03-DI2', testPhase: 'IFIT', platform: 'Di2', appName: 'PUT03', isEiaBypassed: false
    },
    PUT05: {
        group: 'PUT-DI2', legacyName: 'PUT05-DI2', testPhase: 'IFIT', platform: 'Di2', appName: 'PUT05', isEiaBypassed: false
    },
    PUT06: {
        group: 'PUT-DI2', legacyName: 'PUT06-DI2', testPhase: 'BR', platform: 'Di2', appName: 'PUT06', isEiaBypassed: false
    },
    PUT07: {
        group: 'PUT-DI2', legacyName: 'PUT07-DI2', testPhase: 'MFIT', platform: 'Di2', appName: 'PUT07', isEiaBypassed: true
    },
    PUT13: {
        group: 'PUT-DI2', legacyName: 'PUT13-DI2', testPhase: 'MFIT', platform: 'Di2', appName: 'PUT13', isEiaBypassed: false
    },
    'UAT-LUAT1W': {
        group: 'UAT', legacyName: 'LUAT1 WHITE-DI2', testPhase: 'UAT', platform: 'Di2', appName: 'LUAT1 WHITE-DI2', isEiaBypassed: false
    },
    'UAT-LUAT1B': {
        group: 'UAT-DI2', legacyName: 'LUAT1 BLACK-DI2', testPhase: 'UAT', platform: 'Di2', appName: 'LUAT1 BLACK-DI2', isEiaBypassed: false
    },
    'UAT-LUAT2W': {
        group: 'UAT', legacyName: 'LUAT2 WHITE-DI2', testPhase: 'UAT', platform: 'Di2', appName: 'LUAT2 WHITE-DI2', isEiaBypassed: false
    },
    'UAT-LUAT2B': {
        group: 'UAT-DI2', legacyName: 'LUAT2 BLACK-DI2', testPhase: 'UAT', platform: 'Di2', appName: 'LUAT2 BLACK-DI2', isEiaBypassed: false
    },
    'UAT-PROD': {
        group: 'PROD', legacyName: 'PROD', testPhase: 'IFIT', platform: 'Di2', appName: 'PUT05', isEiaBypassed: false
    },
    'UAT-LUAT3B': {
        group: 'UAT-DI2', legacyName: 'LUAT3 BLACK-DI2', testPhase: 'UAT', platform: 'Di2', appName: 'LUAT3 BLACK-DI2', isEiaBypassed: false
    },
    'UAT-LUAT3W': {
        group: 'UAT-DI2', legacyName: 'LUAT3 WHITE-DI2', testPhase: 'UAT', platform: 'Di2', appName: 'LUAT3 WHITE-DI2', isEiaBypassed: false
    },
    'UAT-LUAT4B': {
        group: 'UAT-DI2', legacyName: 'LUAT4 BLACK-DI2', testPhase: 'UAT', platform: 'Di2', appName: 'LUAT4 BLACK-DI2', isEiaBypassed: false
    },
    'UAT-LUAT4W': {
        group: 'UAT-DI2', legacyName: 'LUAT4 WHITE-DI2', testPhase: 'UAT', platform: 'Di2', appName: 'LUAT4 WHITE-DI2', isEiaBypassed: false
    },
    'FOV-P2L1B': {
        group: 'FOV-DI2', legacyName: 'FOV P2 L1 B', testPhase: 'FOV', platform: 'Di2', appName: 'FOV P2 L1 B', isEiaBypassed: false
    },
    'FOV-P2L1W': {
        group: 'FOV-DI2', legacyName: 'FOV P2 L1 W', testPhase: 'FOV', platform: 'Di2', appName: 'FOV P2 L1 W', isEiaBypassed: false
    },
    'FOV-P2L2B': {
        group: 'FOV-DI2', legacyName: 'FOV P2 L2 B', testPhase: 'FOV', platform: 'Di2', appName: 'FOV P2 L2 B', isEiaBypassed: false
    },
    'FOV-P2L2W': {
        group: 'FOV-DI2', legacyName: 'FOV P2 L2 W', testPhase: 'FOV', platform: 'Di2', appName: 'FOV P2 L2 W', isEiaBypassed: false
    },
    'FOV-RKL1B': {
        group: 'FOV-DI2', legacyName: 'FOV RK L1 B', testPhase: 'FOV', platform: 'Di2', appName: 'FOV RK L1 B', isEiaBypassed: false
    },
    'FOV-RKL1W': {
        group: 'FOV-DI2', legacyName: 'FOV RK L1 W', testPhase: 'FOV', platform: 'Di2', appName: 'FOV RK L1 W', isEiaBypassed: false
    },
    'FOV-RKL2B': {
        group: 'FOV-DI2', legacyName: 'FOV RK L2 B', testPhase: 'FOV', platform: 'Di2', appName: 'FOV RK L2 B', isEiaBypassed: false
    },
    'FOV-RKL2W': {
        group: 'FOV-DI2', legacyName: 'FOV RK L2 W', testPhase: 'FOV', platform: 'Di2', appName: 'FOV RK L2 W', isEiaBypassed: false
    },
    'DI2-PROD': {group: 'PROD', legacyName: 'PROD'},
    'DI2-LUAT1W': {group: 'UAT-DI2', legacyName: 'LUAT1 WHITE-DI2'},
    'DI2-LUAT1B': {group: 'UAT-DI2', legacyName: 'LUAT1 BLACK-DI2'},
    'DI2-LUAT2W': {group: 'UAT-DI2', legacyName: 'LUAT2 WHITE-DI2'},
    'DI2-LUAT2B': {group: 'UAT-DI2', legacyName: 'LUAT2 BLACK-DI2'},
    'DI2-LUAT3W': {group: 'UAT-DI2', legacyName: 'LUAT3 WHITE-DI2'},
    'DI2-LUAT3B': {group: 'UAT-DI2', legacyName: 'LUAT3 BLACK-DI2'},
    'DI2-LUAT4W': {group: 'UAT-DI2', legacyName: 'LUAT4 WHITE-DI2'},
    'DI2-LUAT4B': {group: 'UAT-DI2', legacyName: 'LUAT4 BLACK-DI2'}
});
