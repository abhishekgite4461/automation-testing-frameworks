const Enum = require('./enum');

module.exports = new Enum({
    FULL_NAME: {},
    USER_ID: {},
    MOBILE_NUMBER: {},
    HOME_NUMBER: {},
    WORK_NUMBER: {},
    WORK_EXTENSION_NUMBER: {},
    ADDRESS: {},
    POSTCODE: {},
    EMAIL_ADDRESS: {}
});
