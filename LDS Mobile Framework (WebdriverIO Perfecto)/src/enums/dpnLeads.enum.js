const Enum = require('./enum');

module.exports = new Enum({
    SECURITY_SETTINGS: {locator: 'securitySettingsLead'},
    PRODUCT_HUB: {locator: 'productHubLead'},
    CHANGE_PHONE_NUMBER: {locator: 'updatePhoneLead'},
    CALL_US_INTERNET_BANKING: {locator: 'callUsInternetBankingLead'},
    CALL_US_OTHER_BANKING: {locator: 'callUsOtherBankingQueryLead'},
    CALL_US_MEDICAL_ASSISTANCE: {locator: 'callUsMedicalAssistanceLead'},
    CALL_US_SUSPECTED_FRAUD: {locator: 'callUsSuspectedFraudLead'},
    CALL_US_LOST_STOLEN: {locator: 'callUsLostOrStolenLead'},
    CALL_US_EMERGENCY_CASH: {locator: 'callUsEmergencyCashLead'},
    CALL_US_NEW_CURRENT_ACCOUNT: {locator: 'callUsNewCurrentAccountLead'},
    CALL_US_NEW_CREDIT_CARD: {locator: 'callUsNewCurrentAccountLead'},
    CALL_US_NEW_LOAN: {locator: 'callUsNewLoanLead'},
    CALL_US_NEW_ISA_ACCOUNT: {locator: 'callUsNewIsaAccountLead'},
    CALL_US_NEW_MORTGAGE: {locator: 'callUsNewMortgageLead'},
    CALL_US_NEW_SAVINGS: {locator: 'callUsNewSavingsLead'},
    SPENDING_REWARDS: {locator: 'callUsNewSavingsLead'},
    CALL_US: {locator: 'callUsLead'},
    PERSONAL_DETAIL: {locator: 'personalDetailsLead'},
    CHANGE_ADDRESS: {locator: 'updateAddressLead'}
});
