const Enum = require('./enum');

module.exports = new Enum({
    FEATURED: {},
    CURRENT: {},
    LOAN: {},
    CREDIT_CARD: {},
    SAVING: {},
    INSURANCE: {},
    MORTGAGE: {},
    SHARE_DEALING: {}
});
