const Enum = require('../enum');

module.exports = new Enum({
    ENROLMENT: {
        appName: 'Device enrollment', defaultStateOn: true, restApiName: 'SW_CNGA_ENROL_SWITCH', ibcName: 'SW_NGAC_Enl', isGlobal: false
    },
    LIGHT_LOGON: {
        appName: 'Light logon', defaultStateOn: true, restApiName: 'SW_CNGA_LIGHT_LOGON', ibcName: 'SW_NGAC_Lgn', isGlobal: false
    },
    PENDING_PAYMENTS: {
        appName: 'Pending Payments BNGA', defaultStateOn: true, restApiName: 'SW_BNGAPenPymnt', ibcName: 'SW_BNGAPenPymnt', isGlobal: false
    },
    PAYMENT_HUB: {
        appName: 'Payment Hub BNGA', defaultStateOn: true, restApiName: 'SW_EnPymtHbBNGA', ibcName: 'SW_EnPymtHbBNGA', isGlobal: false
    },
    ADD_BENEFICIARY: {
        appName: 'Add Beneficiary', defaultStateOn: true, restApiName: 'SW_EnAdNwBenBNGA', ibcName: 'SW_EnAdNwBenBNGA', isGlobal: false
    },
    DEPOSIT_CHEQUE: {
        appName: 'Cheque Deposit', defaultStateOn: true, restApiName: 'SW_EnFCMDeptBNGA', ibcName: 'SW_EnFCMDeptBNGA', isGlobal: false
    },
    HISTORY_CHEQUE: {
        appName: 'Cheque History', defaultStateOn: true, restApiName: 'SW_EnFCMHstBNGA', ibcName: 'SW_EnFCMHstBNGA', isGlobal: false
    },
    CHEQUE_IMAGING_ENABLED: {
        appName: 'Cheque Imaging Enabled', defaultStateOn: true, restApiName: 'SW_EnICSFCMMbl30', ibcName: 'SW_EnICSFCMMbl30', isGlobal: true
    },
    TOUCH_ID: {
        appName: 'TouchID Enabled', defaultStateOn: false, restApiName: 'SW_EnTchIDBNGA', ibcName: 'SW_EnTchIDBNGA', isGlobal: false
    },
    FINGERPRINT_ANDROID: {
        appName: 'Fingerprint Android', defaultStateOn: false, restApiName: 'SW_EnAndrdFPBNGA', ibcName: 'SW_EnAndrdFPBNGA ', isGlobal: false
    },
    DATA_CONSENT: {
        appName: 'Analytics Consents Enabled', defaultStateOn: true, restApiName: 'SW_EnGDPRDtCnst', ibcName: 'SW_EnGDPRDtCnst', isGlobal: false
    },
    MOBILE_CHAT_ANDROID: {
        appName: 'Mobile Chat BNGA Android', defaultStateOn: true, restApiName: 'SW_EnChatBNGAand', ibcName: 'SW_EnChatBNGAand', isGlobal: false
    },
    MOBILE_CHAT_IOS: {
        appName: 'Mobile Chat BNGA iOS', defaultStateOn: true, restApiName: 'SW_BNGAchat_iOS', ibcName: 'SW_BNGAchat_iOS', isGlobal: false
    },
    PCA_ALLTAB: {
        appName: 'NGA Enable PCA All Tab', defaultStateOn: true, restApiName: 'SW_NGA_EnAllTab', ibcName: 'SW_NGA_EnAllTab', isGlobal: false
    },
    ENABLE_IOS_WKWEBVIEW: {
        appName: 'Enable WKwebview BNGA iOS', defaultStateOn: true, restApiName: 'SW_EnWKBNGAiOS', ibcName: 'SW_EnWKBNGAiOS', isGlobal: false
    },
    ENABLE_PLACEMENT_BNGA_IOS: {
        appName: 'Enable placement before login for BNGA on IOS devices', defaultStateOn: false, restApiName: 'SW_EnPlBLBNGAIos', ibcName: 'SW_EnPlBLBNGAIos', isGlobal: false
    },
    ENABLE_PLACEMENT_BNGA_ANDROID: {
        appName: 'Enable placement before login for BNGA on Android devices', defaultStateOn: false, restApiName: 'SW_EnPlBLBNGAAnd', ibcName: 'SW_EnPlBLBNGAAnd', isGlobal: false
    },
    ENABLE_AB_TESTING_IOS: {
        appName: 'Enable AB testing for BNGA on IOS devices', defaultStateOn: false, restApiName: 'SW_EnABTgBNGAIOS', ibcName: 'SW_EnABTgBNGAIOS', isGlobal: false
    },
    ENABLE_AB_TESTING_ANDROID: {
        appName: 'Enable AB testing for BNGA on Android devices', defaultStateOn: false, restApiName: 'SW_EnABTgBNGAAnd', ibcName: 'SW_EnABTgBNGAAnd', isGlobal: false
    },
    ENABLE_PERSONALISATION_BNGA_ANDROID: {
        appName: 'Enable Personalisation for BNGA on Android devices', defaultStateOn: false, restApiName: 'SW_EnPerBNGAAnd', ibcName: 'SW_EnPerBNGAAnd', isGlobal: false
    },
    ENABLE_PERSONALISATION_BNGA_IOS: {
        appName: 'Enable Personalisation for BNGA on IOS devices', defaultStateOn: false, restApiName: 'SW_EnPerBNGAIos', ibcName: 'SW_EnPerBNGAIos', isGlobal: false
    },
    ENABLE_RTIM_BNGA_IOS: {
        appName: 'Enable RTIM Service for BNGA on IOS', defaultStateOn: false, restApiName: 'SW_EnRTIMBNGAIos', ibcName: 'SW_EnRTIMBNGAIos', isGlobal: false
    },
    ENABLE_RTIM_BNGA_ANDROID: {
        appName: 'Enable RTIM Service for BNGA on Android', defaultStateOn: false, restApiName: 'SW_EnRTIMBNGAAnd', ibcName: 'SW_EnRTIMBNGAAnd', isGlobal: false
    },
    ENABLE_RTIM_BNGA_WEB: {
        appName: 'Enable RTIM Service on Web', defaultStateOn: false, restApiName: 'SW_EnRTIMSerWeb', ibcName: 'SW_EnRTIMSerWeb', isGlobal: false
    }
});
