const Enum = require('../enum');

module.exports = new Enum({
    INTERNET_BANKING: {
        phoneNumber: '441733232030'
    },
    OTHER_BANKING_QUERY: {
        phoneNumber: '441733347338'
    },
    SUSPECTED_FRAUD: {
        phoneNumber: '441733232030'
    },
    LOST_OR_STOLEN_CARDS: {
        phoneNumber: '441702278270'
    },
    EMERGENCY_CASH_ABROAD: {
        phoneNumber: '441702278274'
    },
    MEDICAL_ASSISTANCE_ABROAD: {
        phoneNumber: '441633815819'
    },
    BUSINESS_ACCOUNTS: {
        phoneNumber: '441733347338'
    }
});
