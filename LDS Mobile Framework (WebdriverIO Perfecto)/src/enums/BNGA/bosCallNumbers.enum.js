const Enum = require('../enum');

module.exports = new Enum({
    INTERNET_BANKING: {
        phoneNumber: '441313398620'
    },
    OTHER_BANKING_QUERY: {
        phoneNumber: '441315498724'
    },
    SUSPECTED_FRAUD: {
        phoneNumber: '441313398620'
    },
    LOST_OR_STOLEN_CARDS: {
        phoneNumber: '441314541605'
    },
    EMERGENCY_CASH_ABROAD: {
        phoneNumber: '441132428196'
    },
    MEDICAL_ASSISTANCE_ABROAD: {
        phoneNumber: '441633439013'
    },
    BUSINESS_ACCOUNTS: {
        phoneNumber: '441315498724'
    }
});
