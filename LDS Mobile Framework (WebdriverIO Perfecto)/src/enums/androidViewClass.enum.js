const Enum = require('./enum');
const DeviceHelper = require('../utils/device.helper');

// Android View classes are different based on version
const oldAndroidVersion = () => String(parseFloat(DeviceHelper.osVersion())).split('.')[0] < 6;

module.exports = new Enum({
    BUTTON: {class: 'Button', namespace: 'widget'},
    COMPOUND_BUTTON: {class: 'CompoundButton', namespace: 'widget'},
    FRAME_LAYOUT: {class: 'FrameLayout', namespace: 'widget'},
    GALLERY: {class: 'Gallery', namespace: 'widget'},
    IMAGE_VIEW: {class: 'ImageView', namespace: 'widget'},
    LINEAR_LAYOUT: {class: 'LinearLayout', namespace: 'widget'},
    LIST_VIEW: {class: 'ListView', namespace: 'widget'},
    PROGRESS_BAR: {class: 'ProgressBar', namespace: 'widget'},
    RELATIVE_LAYOUT: {class: 'RelativeLayout', namespace: 'widget'},
    TEXT_VIEW: {class: 'TextView', namespace: 'widget'},
    VIEW: {class: 'View', namespace: 'view'},
    EDIT_TEXT: {class: 'EditText', namespace: 'widget'},
    EXPANDABLE_LIST_VIEW: {class: 'ExpandableListView', namespace: 'widget'},
    IMAGE_BUTTON: {class: 'ImageButton', namespace: 'widget'},
    CHECK_BOX: {class: 'CheckBox', namespace: 'widget'},
    CHECKED_TEXT_VIEW: {class: 'CheckedTextView', namespace: 'widget'},
    HORIZONTAL_SCROLL_VIEW: {class: 'HorizontalScrollView', namespace: 'widget'},
    VIEW_PAGER: {class: 'ViewPager', namespace: 'support.v4.view'},
    SWITCH: {class: 'Switch', namespace: 'widget'},
    VIEW_GROUP: {class: oldAndroidVersion() ? 'View' : 'ViewGroup', namespace: 'view'},
    RECYCLER_VIEW: {class: 'RecyclerView', namespace: 'recyclerview.widget'},
    SPINNER: {class: 'Spinner', namespace: 'widget'},
    WEB_VIEW: {class: 'WebView', namespace: 'webkit'},
    SCROLL_VIEW: {class: 'ScrollView', namespace: 'widget'},
    TABLE_LAYOUT: {class: 'TableLayout', namespace: 'widget'},
    TABLE_ROW: {class: 'TableRow', namespace: 'widget'},
    RADIO_BUTTON: {class: 'RadioButton', namespace: 'widget'},
    TOGGLE_BUTTON: {class: 'ToggleButton', namespace: 'widget'}
});
