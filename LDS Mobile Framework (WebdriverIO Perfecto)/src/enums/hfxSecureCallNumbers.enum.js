const Enum = require('./enum');

module.exports = new Enum({
    INTERNET_BANKING: {
        secure: '1733462241', unsecure: '1132798302'
    },
    OTHER_BANKING_QUERY: {
        secure: '1733462241', unsecure: '1132421984'
    },
    CURRENT_ACCOUNT: {
        secure: '1733462241', unsecure: '1132421984'
    },
    SAVINGS_ACCOUNT: {
        secure: '1733462241', unsecure: '1132422005'
    },
    CREDIT_ACCOUNT: {
        secure: '1733462241', unsecure: '8000283258'
    },
    ISA_ACCOUNT: {
        secure: '1733462241', unsecure: '1132421984'
    },
    LOAN_ACCOUNT: {
        secure: '1733462241', unsecure: '34572434'
    },
    TERM_DEPOSIT_ACCOUNT: {
        secure: '1733462241', unsecure: '1132421984'
    },
    SUSPECTED_FRAUD: {
        secure: '1733462241', unsecure: '1132798302'
    },
    LOST_OR_STOLEN_CARDS: {
        secure: '1733462241', unsecure: '1132428196'
    },
    EMERGENCY_CASH_ABROAD: {
        secure: '1733462241', unsecure: '1132428196'
    },
    MEDICAL_ASSISTANCE_ABROAD: {
        unsecure: '1633439015'
    },
    MORTGAGE_ACCOUNT: {
        unsecure: '3458503705'
    },
    COA: {
        secure: '1733462241', unsecure: '1132421984'
    },
    NEWCURRENT_ACCOUNT: {
        secure: '1733462241', unsecure: '1132421984'
    },
    NEWSAVINGS_ACCOUNT: {
        secure: '1733462241', unsecure: '1132422005'
    },
    NEWISA_ACCOUNT: {
        secure: '1733462241', unsecure: '1132421984'
    },
    NEWLOAN_ACCOUNT: {
        secure: '1733462241', unsecure: '34572434'
    },
    NEWCREDIT_ACCOUNT: {
        secure: '1733462241', unsecure: '8000283258'
    },
    PERSONAL_ACCOUNTS: {
        unsecure: '1132421984'
    },
    NEWMORTGAGES_ACCOUNT: {
        secure: '1733462241', unsecure: '3458503705'
    },
    TABLET_IPAD: {
        unsecure: '1132421984'
    },
    VOICE_ID: {
        secure: '1733263163', unsecure: '1733263163'
    }
});
