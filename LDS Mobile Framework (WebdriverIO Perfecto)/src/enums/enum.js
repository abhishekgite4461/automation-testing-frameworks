class EnumSymbol {
    constructor(name, params) {
        this.symbol = Symbol.for(name);
        this.keys = [];

        for (const key in params) {
            if (Object.prototype.hasOwnProperty.call(params, key)) {
                this[key] = params[key];
                this.keys.push(key);
            }
        }

        Object.freeze(this);
    }

    get name() {
        return this.toString();
    }

    toString() {
        return Symbol.keyFor(this.symbol);
    }

    valueOf() {
        return this.name;
    }
}

class Enum {
    constructor(enumLiterals) {
        for (const key in enumLiterals) {
            if (Object.prototype.hasOwnProperty.call(enumLiterals, key)) {
                this[key] = new EnumSymbol(key, enumLiterals[key]);
            }
        }
        Object.freeze(this);
    }

    symbols() {
        return Object.keys(this).map((key) => this[key]);
    }

    keys() {
        return Object.keys(this);
    }

    contains(sym) {
        if (!(sym instanceof EnumSymbol)) return false;
        return this[Symbol.keyFor(sym.symbol)] === sym;
    }

    fromString(name) {
        for (const key of Object.keys(this)) {
            if (key === name) {
                return this[key];
            }
        }
        throw new Error(`Enum ${name} not found!`);
    }

    fromStringByEscapingSpace(name) {
        const escapedName = name.trim().replace(/ /g, '_').toUpperCase();
        return this.fromString(escapedName);
    }

    asArray() {
        const arr = [];
        for (const key of Object.keys(this)) {
            arr.push(this[key].value);
        }
        return arr;
    }
}

module.exports = Enum;
