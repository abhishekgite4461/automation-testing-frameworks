const Enum = require('./enum');

module.exports = new Enum({
    EMAIL_ADDRESS_UPDATE: {},
    PHONE_NUMBER_UPDATE: {},
    ADD_UK_BENEFICIARY: {},
    ADD_COMPANY_BENEFICIARY: {},
    ADD_NEW_PAYM: {},
    REACTIVATE_ISA: {}
});
