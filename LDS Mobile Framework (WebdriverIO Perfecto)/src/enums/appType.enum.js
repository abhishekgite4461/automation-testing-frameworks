const Enum = require('./enum');

module.exports = new Enum({
    RNGA: {internalType: 'RNGA', isUAT: false},
    BNGA: {internalType: 'BNGA', isUAT: false},
    RNGA_UAT: {internalType: 'RNGA', isUAT: true},
    BNGA_UAT: {internalType: 'BNGA', isUAT: true}
});
