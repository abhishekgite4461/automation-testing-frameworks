const Enum = require('./enum');

module.exports = new Enum({
    PLEASE_SELECT: {value: 'Please select'},
    WEEKLY: {value: 'Weekly'},
    EVERY_FOUR_WEEKS: {value: 'Every four weeks'},
    MONTHLY: {value: 'Monthly'},
    EVERY_TWO_MONTHS: {value: 'Every two months'},
    QUARTERLY: {value: 'Quarterly'},
    HALF_YEARLY: {value: 'Half yearly'},
    YEARLY: {value: 'Yearly'}
});
