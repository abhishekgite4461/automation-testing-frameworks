const Enum = require('./enum');

module.exports = new Enum({
    VALID: {
        testPhase: 'STUB', platform: 'Success Scenarios', appName: 'Unenrolled', isEnrolled: false
    },
    ENROLLED_TYPICAL: {
        testPhase: 'STUB', platform: 'Success Scenarios', appName: 'Typical', isEnrolled: true
    },
    ENROLLED_VALID: {
        testPhase: 'STUB', platform: 'Success Scenarios', appName: 'Enrolled', isEnrolled: true
    },
    VALID_SPECIAL_ACCOUNTS: {
        testPhase: 'STUB', platform: 'Success Scenarios - Special Accounts', appName: 'Enrolled', isEnrolled: true
    },
    YOUTH: {
        testPhase: 'STUB', platform: 'Youth Customer', appName: 'Unenrolled', isEnrolled: false
    },
    STC_REGISTERED: {
        testPhase: 'STUB', platform: 'Save the Change', appName: 'Already registered', isEnrolled: true
    },
    STC_NO_REGISTERED: {
        testPhase: 'STUB', platform: 'Save the Change', appName: 'No registered accounts', isEnrolled: true
    },
    STC_NO_SAVINGS: {
        testPhase: 'STUB', platform: 'Save the Change', appName: 'No saving accounts', isEnrolled: true
    },
    ENROLLED_YOUTH: {
        testPhase: 'STUB', platform: 'Youth Customer', appName: 'Enrolled', isEnrolled: true
    },
    BIOMETRIC_VALID: {
        testPhase: 'STUB', platform: 'Biometric', appName: 'Biometric Not Set', isEnrolled: true
    },
    BIOMETRIC_LOGIN: {
        testPhase: 'STUB', platform: 'Biometric', appName: 'Biometric Opted In', isEnrolled: true
    },
    CBS_PROHIBITIVE_PAYMENTS: {
        testPhase: 'STUB', platform: 'Payments Hub - Payments', appName: 'Hub - CBS Prohibitive indicator', isEnrolled: true
    },
    CBS_PROHIBITIVE_TRANSFERS: {
        testPhase: 'STUB', platform: 'Payments Hub - Transfers', appName: 'Confirmation - CBS Prohibitive indicator', isEnrolled: true
    },
    MONTHLY_CAP_EXCEEDED: {
        testPhase: 'STUB', platform: 'Payments Hub - Payments', appName: 'Hub - Monthly cap limit exceeded', isEnrolled: true
    },
    LEADS_VALID: {
        testPhase: 'STUB', platform: 'Leads - DPN', appName: 'Single DPN', isEnrolled: true
    },
    SINGLE_ASM_LEAD: {
        testPhase: 'STUB', platform: 'Leads - ASM', appName: 'Single ASM', isEnrolled: true
    },
    MULTIPLE_ASM_LEAD: {
        testPhase: 'STUB', platform: 'Leads - ASM', appName: 'Multiple ASM', isEnrolled: true
    },
    SINGLE_ASM_LEAD_POSITION_2: {
        testPhase: 'STUB', platform: 'Leads - ASM', appName: 'ASM - ASM2ALLPAGES', isEnrolled: true
    },
    ASM_LEAD_ALL_PAGES: {
        testPhase: 'STUB', platform: 'Leads - ASM', appName: 'ASM - ASM123ALLPAGES', isEnrolled: true
    },
    BIG_PROMPT: {
        testPhase: 'STUB', platform: 'Leads - Big Prompt', appName: 'Single BP', isEnrolled: true
    },
    BIGPROMPT_SCROLLABLE: {
        testPhase: 'STUB', platform: 'Leads - Big Prompt', appName: 'Single BP - Scrollable content', isEnrolled: true
    },
    BIGPROMPT_TAKEOVER: {
        testPhase: 'STUB', platform: 'Leads - Big Prompt Takeover', appName: 'Single Big Prompt Takeover', isEnrolled: true
    },
    BP_TAKEOVER_SCROLLABLE: {
        testPhase: 'STUB', platform: 'Leads - Big Prompt Takeover', appName: 'Single BP(T) - Scrollable content', isEnrolled: true
    },
    MULTIPLE_LEADS: {
        testPhase: 'STUB', platform: 'Leads - Big Prompt', appName: 'Multiple BP', isEnrolled: true
    },
    MULTIPLE_INTERSTITIAL_LEADS: {
        testPhase: 'STUB', platform: 'Leads - Other', appName: 'Multiple Interstitial Leads', isEnrolled: true
    },
    MULTIPLE_BP_PRIORITY_DUPLICATED: {
        testPhase: 'STUB', platform: 'Leads - Big Prompt', appName: 'Multiple BP - priority duplicated', isEnrolled: true
    },
    SINGLE_DPN: {
        testPhase: 'STUB', platform: 'Leads - DPN', appName: 'Single DPN', isEnrolled: true
    },
    MULTIPLE_DPN: {
        testPhase: 'STUB', platform: 'Leads - DPN', appName: 'Multiple DPN', isEnrolled: true
    },
    MDPN_PRIORITY_DUPLICATED: {
        testPhase: 'STUB', platform: 'Leads - DPN', appName: 'Multiple DPN - priority duplicated', isEnrolled: true
    },
    STICKY_FOOTER: {
        testPhase: 'STUB', platform: 'Leads - Sticky Footer', appName: 'Sticky Footer', isEnrolled: true
    },
    MULTIPLE_STICKY_FOOTER: {
        testPhase: 'STUB', platform: 'Leads - Sticky Footer', appName: 'Multiple Sticky Footer', isEnrolled: true
    },
    '2FACTOR_USER': {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: '2FA User', isEnrolled: false
    },
    GDPR_BP_COSERVICING_PREF_NOTSET: {
        testPhase: 'STUB', platform: 'GDPR', appName: 'Big Prompt - Prefs not set co-servicing', isEnrolled: true
    },
    GDPR_BP_PREFS_NOT_SET: {
        testPhase: 'STUB', platform: 'GDPR', appName: 'Big Prompt - Prefs not set', isEnrolled: true
    },
    GDPR_BP_PREFS_SET: {
        testPhase: 'STUB', platform: 'GDPR', appName: 'Big Prompt - Prefs set', isEnrolled: true
    },
    GDPR_BP_COSERVICING_PREF_SET: {
        testPhase: 'STUB', platform: 'GDPR', appName: 'Big Prompt - Prefs set co-servicing', isEnrolled: true
    },
    GDPR_COSERVICING_PREF_NOTSET: {
        testPhase: 'STUB', platform: 'GDPR', appName: 'Both not set co-servicing', isEnrolled: true
    },
    GDPR_COSERVICING_PREF_SET: {
        testPhase: 'STUB', platform: 'GDPR', appName: 'Both are set co-servicing', isEnrolled: true
    },
    GDPR_OCIS_READ_ERROR: {
        testPhase: 'STUB', platform: 'GDPR', appName: 'Error scenario - Cannot retrieve', isEnrolled: true
    },
    GDPR_OCIS_WRITE_ERROR: {
        testPhase: 'STUB', platform: 'GDPR', appName: 'Error scenario - Cannot update', isEnrolled: true
    },
    GDPR_BP_OCIS_READ_ERROR: {
        testPhase: 'STUB', platform: 'GDPR', appName: 'Error scenario - BP Cannot retrieve', isEnrolled: true
    },
    GDPR_BP_OCIS_WRITE_ERROR: {
        testPhase: 'STUB', platform: 'GDPR', appName: 'Error scenario - BP Cannot update', isEnrolled: true
    },
    GDPR_OCIS_PREFS_NOT_SET: {
        testPhase: 'STUB', platform: 'GDPR', appName: 'prefs not set', isEnrolled: true
    },
    NO_PAPERLESS_STATEMENTS: {
        testPhase: 'STUB', platform: 'Personal Details', appName: 'Not Registered For Credit Card Paperless', isEnrolled: true
    },
    INCORRECT_CONFIRMATION_PASSWORD: {
        testPhase: 'STUB', platform: 'Personal Details', appName: 'Wrong Password', isEnrolled: true
    },
    COMPROMISED_PASSWORD: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'Compromised Password', isEnrolled: false
    },
    INCORRECT_CREDENTIALS: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'Incorrect Credentials', isEnrolled: false
    },
    ENROLLED_INCORRECT_MI: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'Enrolled: Incorrect MI', isEnrolled: true
    },
    INCORRECT_MI_MAX_ATTEMPT: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'Incorrect MI - Max Attempts Reached', isEnrolled: false
    },
    INCORRECTMI_INCORRECT_PASSWORD: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'Unenrolled: Incorrect MI or Incorrect password', isEnrolled: false
    },
    INCORRECTMI_CORRECT_PASSWORD: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'Unenrolled: Incorrect MI Correct password', isEnrolled: false
    },
    CORRECTMI_CORRECT_PASSWORD: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'Unenrolled: Incorrect MI - Correct MI - Correct password', isEnrolled: false
    },
    SUSPENDED: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'Mandate Suspended', isEnrolled: false
    },
    INACTIVE: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'Mandate is inactive', isEnrolled: false
    },
    MANDATELESS: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'No Mandate for Logon', isEnrolled: false
    },
    PARTIALLY_REGISTERED: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'Partially Registered', isEnrolled: false
    },
    REVOKED_PASSWORD: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'Password Revoked', isEnrolled: false
    },
    NO_PHONE_NUMBER: {
        testPhase: 'STUB', platform: 'Enrolment Error Scenarios', appName: 'No Phone Numbers (OTP)', isEnrolled: false
    },
    NO_MOBILE_NUMBER: {
        testPhase: 'STUB', platform: 'Click To Call Scenarios', appName: 'No Registered Mobile Number', isEnrolled: true
    },
    MOBILE_NUMBER_CHANGED_IN_GRACE_PERIOD: {
        testPhase: 'STUB', platform: 'Click To Call Scenarios', appName: 'Mobile Number Changed In Grace Period', isEnrolled: true
    },
    RESET_PASSWORD: {
        testPhase: 'STUB', platform: 'Reset Password Success Scenario', appName: 'Success', isEnrolled: false
    },
    RESET_PASSWORD_EASY_GUESS: {
        testPhase: 'STUB', platform: 'Reset Password Error Scenarios', appName: 'Easy Guess Password', isEnrolled: false
    },
    RESET_PASSWORD_INSECURE_PWD: {
        testPhase: 'STUB', platform: 'Reset Password Error Scenarios', appName: 'Not Secure Enough', isEnrolled: false
    },
    RESET_PASSWORD_PERSONAL_INFO: {
        testPhase: 'STUB', platform: 'Reset Password Error Scenarios', appName: 'Personal Details Used', isEnrolled: false
    },
    RESET_PASSWORD_PREV_PWD: {
        testPhase: 'STUB', platform: 'Reset Password Error Scenarios', appName: 'Previous Password Used', isEnrolled: false
    },
    INVALID_OTP: {
        testPhase: 'STUB', platform: 'Enrolment Error Scenarios', appName: 'Invalid OTP', isEnrolled: false
    },
    PAYMENTHUB_VALID: {
        testPhase: 'STUB', platform: 'Payments Hub - General', appName: 'Success', isEnrolled: true
    },
    NO_ELIGIBLE_ACCOUNTS: {
        testPhase: 'STUB', platform: 'Payments Hub - General', appName: 'No Eligible Accounts', isEnrolled: true
    },
    TRANSFER_EXCEEDS_BALANCE: {
        testPhase: 'STUB', platform: 'Payments Hub - Transfers', appName: 'Amount exceeds balance', isEnrolled: true
    },
    TRANSFER_CONFIRM_LOST_INTERNET: {
        testPhase: 'STUB', platform: 'Payments Hub - Transfers', appName: 'Confirmation - Connectivity Lost', isEnrolled: true
    },
    PAYMENT_CONFIRM_LOST_INTERNET: {
        testPhase: 'STUB', platform: 'Payments Hub - Payments', appName: 'Confirmation - Connectivity Lost', isEnrolled: true
    },
    TO_HTBISA_ABOVE_FUND_LIMIT: {
        testPhase: 'STUB', platform: 'Payments Hub - Transfers', appName: 'Confirmation - Non-ISA to HTB-ISA transfer over limit', isEnrolled: true
    },
    ISA_TRANSFER_WARNING: {
        testPhase: 'STUB', platform: 'Payment Hub - ISA', appName: '200 - ISA Verify Transfer WARN', isEnrolled: true
    },
    ISA_TRANSFER_BLOCKED: {
        testPhase: 'STUB', platform: 'Payment Hub - ISA', appName: '200 - ISA Verify Transfer BLOCKED', isEnrolled: true
    },
    ISA_MAX_AMOUNT: {
        testPhase: 'STUB', platform: 'Payment Hub - ISA', appName: '402 - ISA Amount exceed remaining allowance', isEnrolled: true
    },
    ISA_MIN_AMOUNT: {
        testPhase: 'STUB', platform: 'Payment Hub - ISA', appName: '402 - ISA Amount below minimum limit', isEnrolled: true
    },
    IBREG_MANDATELESS: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'No Mandate for Logon', isEnrolled: true
    },
    APP_BAN_ENROLLED: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'App Ban - User is Banned for app version', isEnrolled: true
    },
    APP_BAN_UNENROLLED: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'App Ban - Unenrolled user is Banned for app version', isEnrolled: false
    },
    APP_WARN_UNENROLLED: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'Unenrolled: App Warn - User is Warned for app version', isEnrolled: false
    },
    APP_WARN_ENROLLED: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'Enrolled: App Warn - User is Warned for app version', isEnrolled: true
    },
    NOT_REGISTERED_FOR_PAYM: {
        testPhase: 'STUB', platform: 'Paym', appName: 'Eligible', isEnrolled: true
    },
    EIA_BYPASS: {
        testPhase: 'STUB', platform: 'Enrolment Error Scenarios', appName: 'Enrolment Bypass', isEnrolled: false
    },
    EIA_PENDING: {
        testPhase: 'STUB', platform: 'Enrolment Error Scenarios', appName: 'EIA Pending', isEnrolled: false
    },
    NO_REMITTER_ELIGIBLE_ACCOUNTS: {
        testPhase: 'STUB', platform: 'Payments Hub - General', appName: 'No Eligible Accounts', isEnrolled: true
    },
    NO_BENEFICIARIES: {
        testPhase: 'STUB', platform: 'Payments Hub - General', appName: 'No beneficiaries', isEnrolled: true
    },
    NO_RECIPIENT: {
        testPhase: 'STUB', platform: 'Payments Hub - General', appName: 'No Recipient', isEnrolled: true
    },
    INTERNET_TRANSACTION_LIMIT: {
        testPhase: 'STUB', platform: 'Payments Hub - Payments', appName: 'Hub - Internet Transaction Limit', isEnrolled: true
    },
    PAYMENT_DECLINED: {
        testPhase: 'STUB', platform: 'Payments Hub - Payments', appName: 'Confirmation - Payment declined No Funds', isEnrolled: true
    },
    STANDING_ORDER: {
        testPhase: 'STUB', platform: 'Payments Hub - General', appName: 'Recipient with standing order', isEnrolled: true
    },
    PAYMENT_DECLINED_SCORINGEVENT: {
        testPhase: 'STUB', platform: 'Payments Hub - Payments', appName: 'Confirmation - Payment declined Scoring Event', isEnrolled: true
    },
    PAYMENT_ONHOLD: {
        testPhase: 'STUB', platform: 'Payments Hub - Payments', appName: 'Confirmation - Payment held for review', isEnrolled: true
    },
    SCA_PAYMENT_ONHOLD: {
        testPhase: 'STUB', platform: 'Payments Hub - SCA payment', appName: 'Fulfill - On hold', isEnrolled: true
    },
    SCA_PAYMENT_DECLINED: {
        testPhase: 'STUB', platform: 'Payments Hub - SCA payment', appName: 'Fulfill - Forced logout', isEnrolled: true
    },
    PAYMENT_PENDING_CREDITCARD: {
        testPhase: 'STUB', platform: 'Payments Hub - Payments', appName: 'Confirmation - Pending Credit Card Payment', isEnrolled: true
    },
    PAYMENT_LIMIT_EXCEEDED: {
        testPhase: 'STUB', platform: 'Payments Hub - Payments', appName: 'Review - Payment limit exceeded', isEnrolled: true
    },
    IBREG_USERID_NOT_UNIQUE: {
        testPhase: 'STUB', platform: 'Registration Scenarios', appName: 'Username Not Unique'
    },
    IBREG_SUSPENDED: {
        testPhase: 'STUB', platform: 'Registration Scenarios', appName: 'Mandate Suspended'
    },
    IBREG_INELIGIBLE_PRODUCT: {
        testPhase: 'STUB', platform: 'Registration Scenarios', appName: 'Not Eligible'
    },
    IBREG_APPLICATION_IN_PROGRESS: {
        testPhase: 'STUB', platform: 'Registration Scenarios', appName: 'Duplicate Application'
    },
    IBREG_COMMERCIAL_WITH_VALID_PASSWORD: {
        testPhase: 'STUB', platform: 'Registration Scenarios', appName: 'Mandate Exists'
    },
    IBREG_COMMERCIAL_WITH_REVOKED_PASSWORD: {
        testPhase: 'STUB', platform: 'Registration Scenarios', appName: 'Mandate Exists Invalid Password'
    },
    IBREG_COMMERCIAL_BOS: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'No Mandate for Logon'
    },
    IBREG_RETAIL_MANDATE: {
        testPhase: 'STUB', platform: 'Registration Scenarios', appName: 'Mandate Exists No Username'
    },
    IBREG_FAILING_UNIQUENESS_CHECK: {
        testPhase: 'STUB', platform: 'Registration Scenarios', appName: 'Uniqueness Check Failed'
    },
    IBREG_YOUTH_FAILING_UNIQUENESS_CHECK: {
        testPhase: 'STUB', platform: 'Registration Scenarios', appName: 'Uniqueness Check Failed Youth User'
    },
    IBREG_INCORRECT_POSTCODE: {
        testPhase: 'STUB', platform: 'Registration Scenarios', appName: 'Incorrect Postcode'
    },
    IBREG_EIA_FRAUD: {
        testPhase: 'STUB', platform: 'Registration - EIA Scenarios', appName: 'EIA Fail - Fraud - Letter Sent'
    },
    IBREG_EIA_INELIGIBLE_O2OPASS: {
        testPhase: 'STUB', platform: 'Registration - EIA Scenarios', appName: 'EIA Fail - Sim Swap'
    },
    IBREG_EIA_INELIGIBLE_O2OFAIL: {
        testPhase: 'STUB', platform: 'Registration - EIA Scenarios', appName: 'EIA Fail - Application Reference'
    },
    IBREG_EIA_SUCCESS: {
        testPhase: 'STUB', platform: 'Registration - EIA Scenarios', appName: 'EIA Success', isEnrolled: false
    },
    IBREG_YOUTH_O2OPASS: {
        testPhase: 'STUB', platform: 'Registration - Letter Sent Scenarios', appName: 'New User (Youth)', isEnrolled: false
    },
    IBREG_YOUTH_O2OFAIL: {
        testPhase: 'STUB', platform: 'Registration - Letter Sent Scenarios', appName: 'Failed (For Youth)', isEnrolled: false
    },
    IBREG_EIA_COMMERCIAL_MANDATE: {testPhase: 'STUB', platform: 'Registration - EIA Scenarios', appName: 'EIA Success Commercial Mandate'},
    IBREG_ADULT_O2OPASS: {
        testPhase: 'STUB', platform: 'Registration - Letter Sent Scenarios', appName: 'New User (Adult)', isEnrolled: false
    },
    IBREG_ADULT_O2OFAIL: {
        testPhase: 'STUB', platform: 'Registration - Letter Sent Scenarios', appName: 'Failed', isEnrolled: false
    },
    IBREG_PASSWORD_BLACKLISTED: {testPhase: 'STUB', platform: 'Registration Scenarios', appName: 'Password Blacklisted'},
    IBREG_ACTIVATION_SUCCESS: {
        testPhase: 'STUB', platform: 'Registration - Activation Code Scenarios', appName: 'Activation Code Success', isEnrolled: false
    },
    IBREG_REQUEST_NEW_CODE_LESS_THAN_3_DAYS: {
        testPhase: 'STUB', platform: 'Registration - Activation Code Scenarios', appName: 'Request New Code less than 3 days', isEnrolled: false
    },
    IBREG_REQUEST_NEW_CODE_020_FAILED: {
        testPhase: 'STUB', platform: 'Registration - Activation Code Scenarios', appName: 'Request New Code 020 failed', isEnrolled: false
    },
    IBREG_REQUEST_NEW_CODE_LIMIT_EXCEEDED: {
        testPhase: 'STUB', platform: 'Registration - Activation Code Scenarios', appName: 'Request New Code Limit Exceeded', isEnrolled: false
    },
    IBREG_ACTIVATION_CODE_ATTEMPTS_EXCEEDED: {
        testPhase: 'STUB', platform: 'Registration - Activation Code Scenarios', appName: 'Attempts Exceeded Can not Request more online', isEnrolled: false
    },
    IBREG_INVALID_CODE_NEW_SENT_020_PASS: {
        testPhase: 'STUB', platform: 'Registration - Activation Code Scenarios', appName: 'Invalid Code New Sent 020 Pass', isEnrolled: false
    },
    IBREG_ACTIVATION_CODE_EXPIRED_020_PASS: {
        testPhase: 'STUB', platform: 'Registration - Activation Code Scenarios', appName: 'Activation Code Expired 020 Pass', isEnrolled: false
    },
    IBREG_ACTIVATION_CODE_EXPIRED_020_FAILED: {
        testPhase: 'STUB', platform: 'Registration - Activation Code Scenarios', appName: 'Activation Code Expired 020 failed', isEnrolled: false
    },
    IBREG_ACTIVATION_CODE_CREATE_MI: {
        testPhase: 'STUB', platform: 'Registration - Activation Code Scenarios', appName: 'Activation Code Create MI', isEnrolled: false
    },
    IBREG_LETTER_SENT_COMMERCIAL_NEW_PASSWORD: {
        testPhase: 'STUB', platform: 'Registration - Letter Sent Scenarios', appName: 'Commercial Mandate (New Password)', isEnrolled: false
    },
    IBREG_LETTER_SENT_COMMERCIAL_EXISTING_PASSWORD: {
        testPhase: 'STUB', platform: 'Registration - Letter Sent Scenarios', appName: 'Commercial Mandate (Existing Password)', isEnrolled: false
    },
    IBREG_ACTIVATION_CODE_NOT_VALID: {
        testPhase: 'STUB', platform: 'Registration - Activation Code Scenarios', appName: 'Activation Code Not valid', isEnrolled: false
    },
    UPDATED_PHONE_NUMBER_REFERRED: {
        testPhase: 'STUB', platform: 'Personal Details', appName: 'Request Refer', isEnrolled: true
    },
    UPDATED_PHONE_NUMBER_DECLINED: {
        testPhase: 'STUB', platform: 'Personal Details', appName: 'Request Suspended', isEnrolled: true
    },
    NATIVE_LEADS_VALID: {
        testPhase: 'STUB', platform: 'Leads - DPN', appName: 'Single DPN with a native link', isEnrolled: true
    },
    HOST_SYSTEM_DOWN: {
        testPhase: 'STUB', platform: 'Home Screen Error Scenarios', appName: 'Balance is Null', isEnrolled: false
    },
    HOMEPAGE_PENDING_PAYMENT: {
        testPhase: 'STUB', platform: 'Home Screen Error Scenarios', appName: 'Pending Payment', isEnrolled: false
    },
    EXPIRED_OTP: {
        testPhase: 'STUB', platform: 'Enrolment Error Scenarios', appName: 'Expired OTP', isEnrolled: false
    },
    ADDRESS_CHANGED_OTP: {
        testPhase: 'STUB', platform: 'Enrolment Error Scenarios', appName: 'No Phone Numbers (OTP) Address changed recently', isEnrolled: false
    },
    TC_NOT_ACCEPTED: {
        testPhase: 'STUB', platform: 'Enrolment Scenarios', appName: 'Show terms and conditions', isEnrolled: false
    },
    IBREG_CREATE_MI_WITHIN_GRACE_PERIOD: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'Create MI - Within grace period', isEnrolled: false
    },
    IBREG_CREATE_MI_AFTER_GRACE_PERIOD: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'Create MI - After grace period', isEnrolled: false
    },
    STATEMENT_VALID: {
        testPhase: 'STUB', platform: 'Success Scenarios', appName: 'Enrolled', isEnrolled: true
    },
    BANNER_PAYMENT_SUCCESS: {
        testPhase: 'STUB', platform: 'Leads - Banner', appName: 'Payment success', isEnrolled: true
    },
    BANNER_PAYMENT_FAILURE: {
        testPhase: 'STUB', platform: 'Leads - Banner', appName: 'Payment failure', isEnrolled: true
    },
    BANNER_PAYMENT_FAILURE_LOST_CONNECTION: {
        testPhase: 'STUB', platform: 'Leads - Banner', appName: 'payment failure - lost connection', isEnrolled: true
    },
    BANNER_TRANSFER_SUCCESS: {
        testPhase: 'STUB', platform: 'Leads - Banner', appName: 'Transfer success', isEnrolled: true
    },
    BANNER_TRANSFER_FAILURE: {
        testPhase: 'STUB', platform: 'Leads - Banner', appName: 'Transfer failure', isEnrolled: true
    },
    BANNER_TRANSFER_FAILURE_LOST_CONNECTION: {
        testPhase: 'STUB', platform: 'Leads - Banner', appName: 'Transfer failure - lost connection', isEnrolled: true
    },
    SAVINGS_ACCOUNT_STATEMENT: {
        testPhase: 'STUB', platform: 'Success Scenarios', appName: 'Enrolled', isEnrolled: true
    },
    CURRENT_AND_SAVINGS_ACCOUNT: {
        testPhase: 'STUB', platform: 'Success Scenarios', appName: 'Enrolled', isEnrolled: true
    },
    COA_ELIGIBILITY_FILTERING_FAILED: {
        testPhase: 'STUB', platform: 'Change Of Address', appName: 'Eligibility Filtering Failed', isEnrolled: true
    },
    MORTGAGE_ACCOUNT: {
        testPhase: 'STUB', platform: 'Success Scenarios', appName: 'Enrolled', isEnrolled: true
    },
    COA_NO_ADDRESS_RETURNED: {
        testPhase: 'STUB', platform: 'Change Of Address', appName: 'Postcode - No Address Found', isEnrolled: true
    },
    DORMANT_ACCOUNTS: {
        testPhase: 'STUB', platform: 'Arrangement Error Scenarios', appName: 'All Accounts Dormant', isEnrolled: true
    },
    SOME_DORMANT_ACCOUNTS: {
        testPhase: 'STUB', platform: 'Arrangement Error Scenarios', appName: 'Some Accounts Dormant', isEnrolled: true
    },
    INVALID_ACCOUNTS: {
        testPhase: 'STUB', platform: 'Arrangement Error Scenarios', appName: 'No Valid Accounts', isEnrolled: true
    },
    COA_MAP_INVALID_ADDRESS: {
        testPhase: 'STUB', platform: 'Change Of Address', appName: 'Postcode - Invalid Address', isEnrolled: true
    },
    COA_MAP_VALID_ADDRESS: {
        testPhase: 'STUB', platform: 'Change Of Address', appName: 'Postcode - Valid Address', isEnrolled: true
    },
    HOME_INSURANCE: {
        testPhase: 'STUB', platform: 'Arrangement Error Scenarios', appName: 'No Valid Accounts', isEnrolled: true
    },
    ACCOUNTLINK_VALID: {
        testPhase: 'STUB', platform: 'Leads - Account Link', appName: 'Account Link', isEnrolled: true
    },
    COA_CCTM_REFER_BRANCH: {
        testPhase: 'STUB', platform: 'Change Of Address', appName: 'CCTM - Refer Branch', isEnrolled: true
    },
    COA_CCTM_REFER_FRAUD_OPS: {
        testPhase: 'STUB', platform: 'Change Of Address', appName: 'CCTM - Refer Fraud Ops', isEnrolled: true
    },
    REACTIVATE_ISA_SUCCESS: {
        testPhase: 'STUB', platform: 'Reactivate ISA', appName: 'AAA - Happy path - Reactivate ISA', isEnrolled: true
    },
    VALID_FALLOW_ISA_WITH_REMAINING_ALLOWANCE_ABOVE_0: {
        testPhase: 'STUB', platform: 'Reactivate ISA', appName: 'AAA - Happy path - Reactivate ISA', isEnrolled: true
    },
    VALID_FALLOW_ISA_WITH_NO_REMAINING_ALLOWANCE: {
        testPhase: 'STUB', platform: 'Reactivate ISA', appName: 'Success page - No allowance', isEnrolled: true
    },
    VALID_FALLOW_ISA_WITH_REMAINING_ALLOWANCE_ABOVE_0_AND_NO_REMITTING_ACCOUNT: {
        testPhase: 'STUB', platform: 'Reactivate ISA', appName: 'Success page - No allowance no remitting accounts', isEnrolled: true
    },
    VALID_FALLOW_ISA: {
        testPhase: 'STUB', platform: 'Reactivate ISA', appName: 'Success page - ISA not able to receive transfer', isEnrolled: true
    },
    REACTIVATE_ISA_WRONG_PASSWORD: {
        testPhase: 'STUB', platform: 'Reactivate ISA', appName: 'Activation page -  Wrong password', isEnrolled: true
    },
    COA_OCIS_FAIL: {
        testPhase: 'STUB', platform: 'Change Of Address', appName: 'OCIS - Fail', isEnrolled: true
    },
    COA_CCTM_DECLINE: {
        testPhase: 'STUB', platform: 'Change Of Address', appName: 'CCTM - Decline', isEnrolled: true
    },
    FIXED_BOND: {
        testPhase: 'STUB', platform: 'Success Scenarios', appName: 'Enrolled', isEnrolled: true
    },
    CARD_READER_RESPOND: {
        testPhase: 'STUB', platform: 'Success Scenarios', appName: 'Unenrolled - Card Reader Respond', isEnrolled: false
    },
    CARD_READER_IDENTIFY: {
        testPhase: 'STUB', platform: 'Success Scenarios', appName: 'Unenrolled - Card Reader Identify', isEnrolled: false
    },
    DORMANT: {
        testPhase: 'STUB', platform: 'Success Scenarios', appName: 'Enrolled', isEnrolled: true
    },
    SPENDING_REWARDS: {
        testPhase: 'STUB', platform: 'Everyday Offers', appName: 'Spending Rewards', isEnrolled: true
    },
    SPENDING_REWARDS_TECHNICAL_ERROR: {
        testPhase: 'STUB', platform: 'Everyday Offers', appName: 'Failed Registration', isEnrolled: false
    },
    CARD_READER_RESPOND_CORRECT_PASSCODE: {
        testPhase: 'STUB', platform: 'Card Reader Respond', appName: 'Card Number Correct Passcode', isEnrolled: false
    },
    CARD_READER_RESPOND_INCORRECT_PASSCODE: {
        testPhase: 'STUB', platform: 'Card Reader Respond', appName: 'Card Number Incorrect Passcode', isEnrolled: false
    },
    CARD_READER_RESPOND_CARDNUMBER_INVALID: {
        testPhase: 'STUB', platform: 'Card Reader Respond', appName: 'Card Number is Invalid', isEnrolled: false
    },
    CARD_READER_RESPOND_CARD_INVALID: {
        testPhase: 'STUB', platform: 'Card Reader Respond', appName: 'Card is Invalid', isEnrolled: false
    },
    CARD_READER_RESPOND_CARD_EXPIRED: {
        testPhase: 'STUB', platform: 'Card Reader Respond', appName: 'Card Expired', isEnrolled: false
    },
    CARD_READER_RESPOND_CARD_SUSPENDED: {
        testPhase: 'STUB', platform: 'Card Reader Respond', appName: 'Card is Suspended', isEnrolled: false
    },
    CARD_READER_RESPOND_NEW_CARD: {
        testPhase: 'STUB', platform: 'Card Reader Respond', appName: 'Card Already Exist', isEnrolled: false
    },
    CARD_READER_IDENTIFY_CORRECT_PASSCODE: {
        testPhase: 'STUB', platform: 'Card Reader Identify', appName: 'Card Number Correct Passcode', isEnrolled: false
    },
    CARD_READER_IDENTIFY_MULTIPLE_BUSINESSES: {
        testPhase: 'STUB', platform: 'Card Reader Identify', appName: 'Multiple Businesses', isEnrolled: false
    },
    CARD_READER_IDENTIFY_INCORRECT_PASSCODE: {
        testPhase: 'STUB', platform: 'Card Reader Identify', appName: 'Card Number Incorrect Passcode', isEnrolled: false
    },
    CARD_READER_IDENTIFY_INVALID_CARD: {
        testPhase: 'STUB', platform: 'Card Reader Identify', appName: 'Card Number is Invalid', isEnrolled: false
    },
    CARD_READER_IDENTIFY_EXPIRED_CARD: {
        testPhase: 'STUB', platform: 'Card Reader Identify', appName: 'Card Expired', isEnrolled: false
    },
    CARD_READER_IDENTIFY_SUSPENDED_CARD: {
        testPhase: 'STUB', platform: 'Card Reader Identify', appName: 'Card is Suspended', isEnrolled: false
    },
    CARD_READER_IDENTIFY_NEW_CARD: {
        testPhase: 'STUB', platform: 'Card Reader Identify', appName: 'Card Already Exist', isEnrolled: false
    },
    SINGLE_BUSINESS_FULL_SIGNATORY_ENROLLED: {
        testPhase: 'STUB', platform: 'Business Special Scenarios - Enrolled', appName: 'Single Business - Full Signatory', isEnrolled: true
    },
    SINGLE_BUSINESS_FULL_DELEGATE_ENROLLED: {
        testPhase: 'STUB', platform: 'Business Special Scenarios - Enrolled', appName: 'Single Business - Full Access Delegate', isEnrolled: true
    },
    SINGLE_BUSINESS_VIEW_ONLY_ENROLLED: {
        testPhase: 'STUB', platform: 'Business Special Scenarios - Enrolled', appName: 'Single Business - View Only Delegate', isEnrolled: true
    },
    SINGLE_BUSINESS_FULL_SIGNATORY_UNENROLLED: {
        testPhase: 'STUB', platform: 'Business Special Scenarios - Unenrolled', appName: 'Single Business - Full Signatory', isEnrolled: false
    },
    SINGLE_BUSINESS_FULL_DELEGATE_UNENROLLED: {
        testPhase: 'STUB', platform: 'Business Special Scenarios - Unenrolled', appName: 'Single Business - Full Access Delegate', isEnrolled: false
    },
    SINGLE_BUSINESS_VIEW_ONLY_UNENROLLED: {
        testPhase: 'STUB', platform: 'Business Special Scenarios - Unenrolled', appName: 'Single Business - View Only Delegate', isEnrolled: false
    },
    MULTIPLE_BUSINESS_FULL_SIGNATORY: {
        testPhase: 'STUB', platform: 'Business Special Scenarios - Enrolled', appName: 'Full Signatory Businesses', isEnrolled: true
    },
    MULTIPLE_BUSINESS_FULL_DELEGATE: {
        testPhase: 'STUB', platform: 'Business Special Scenarios - Enrolled', appName: 'Full Access Delegate Businesses', isEnrolled: true
    },
    MULTIPLE_BUSINESS_VIEW_ONLY: {
        testPhase: 'STUB', platform: 'Business Special Scenarios - Enrolled', appName: 'View Only Delegate Businesses', isEnrolled: true
    },
    ANALYTICS_CONSENT_UNCHANGED: {
        testPhase: 'STUB', platform: 'Analytics Consent', appName: 'Unchanged', isEnrolled: true
    },
    ANALYTICS_CONSENT_UNSET: {
        testPhase: 'STUB', platform: 'Analytics Consent', appName: 'Unset', isEnrolled: true
    },
    ANALYTICS_CONSENT_SINGLE_BUSINESS: {
        testPhase: 'STUB', platform: 'Analytics Consent', appName: 'Unset - single business', isEnrolled: true
    },
    ANALYTICS_CONSENT_UNSET_BIGPROMPT: {
        testPhase: 'STUB', platform: 'Analytics Consent', appName: 'Unset - BigPrompt', isEnrolled: true
    },
    ANALYTICS_CONSENT_UNSET_BIOMETRIC_OPTEDIN: {
        testPhase: 'STUB', platform: 'Analytics Consent', appName: 'Unset - Biometric OptedIn', isEnrolled: true
    },
    ANALYTICS_CONSENT_UNSET_BIOMETRIC_SUSPENDED: {
        testPhase: 'STUB', platform: 'Analytics Consent', appName: 'Unset - Biometric Suspended', isEnrolled: true
    },
    ANALYTICS_CONSENT_UNSET_BIOMETRIC_UNSET: {
        testPhase: 'STUB', platform: 'Analytics Consent', appName: 'Unset - Biometric Unset', isEnrolled: true
    },
    ANALYTICS_CONSENT_UNSET_BIOMETRICS_CHANGED: {
        testPhase: 'STUB', platform: 'Analytics Consent', appName: 'Unset - Biometrics Changed', isEnrolled: true
    },
    ANALYTICS_CONSENT_UNSET_UNENROLLED: {
        testPhase: 'STUB', platform: 'Analytics Consent', appName: 'Unset - Unenrolled', isEnrolled: false
    },
    COA_RETRY_PASSWORD: {
        testPhase: 'STUB', platform: 'Change Of Address', appName: 'Retry - Password Validation', isEnrolled: true
    },
    COA_RETRY_PASSWORD_MAX_ATTEMPTS: {
        testPhase: 'STUB', platform: 'Change Of Address', appName: 'Retry Password Max Attempts', isEnrolled: true
    },
    APP_LAUNCH_FAILED: {
        testPhase: 'STUB', platform: 'App Launch Error Scenarios', appName: 'Technical Error', isEnrolled: false
    },
    HARD_TOKEN: {
        testPhase: 'STUB', platform: 'Login Error Scenarios', appName: 'Hard token users not allowed', isEnrolled: false
    },
    BENEFICIARY_PENDING_PAYMENT: {
        testPhase: 'STUB', platform: 'Payments Hub - General', appName: 'Beneficiary - Pending Payment', isEnrolled: true
    },
    MULTIPLE_BUSINESS_DELEGATE_VIEW_ONLY: {
        testPhase: 'STUB', platform: 'Business Special Scenarios - Enrolled', appName: 'View Only Delegate Businesses', isEnrolled: true
    },
    MULTIPLE_BUSINESS_FULL_SIGNATORY_ONLY: {
        testPhase: 'STUB', platform: 'Business Special Scenarios - Enrolled', appName: 'Full Signatory Businesses', isEnrolled: true
    },
    TREASURY_32_DAY_NOTICE: {
        testPhase: 'STUB', platform: 'Success Scenarios', appName: 'Enrolled', isEnrolled: true
    },
    FIXED_TERM_DEPOSIT: {
        testPhase: 'STUB', platform: 'Success Scenarios', appName: 'Enrolled', isEnrolled: true
    },
    SOFT_TOKEN_MI_NOT_SETUP: {
        testPhase: 'STUB', platform: 'Soft Token - MI is not set up', appName: 'Soft Token Correct Passcode', isEnrolled: false
    },
    SOFT_TOKEN_MI_NOT_SETUP_SINGLE_BUSINESS: {
        testPhase: 'STUB', platform: 'Soft Token - MI is not set up', appName: 'Soft Token Correct Passcode - Single business', isEnrolled: false
    },
    SOFT_TOKEN_MI_NOT_SETUP_INCORRECT_PASSCODE: {
        testPhase: 'STUB', platform: 'Soft Token - MI is not set up', appName: 'Soft Token Incorrect Passcode', isEnrolled: false
    },
    SOFT_TOKEN_MI_NOT_SETUP_INCORRECT_PASSCODE_MAX_ATTEMPTS_REACHED: {
        testPhase: 'STUB', platform: 'Soft Token - MI is not set up', appName: 'Soft Token Incorrect Passcode - Max Limit Reached', isEnrolled: false
    },
    SOFT_TOKEN_MI_ALREADY_SETUP: {
        testPhase: 'STUB', platform: 'Soft Token - MI already setup', appName: 'Soft Token Correct Passcode', isEnrolled: false
    },
    THREE_NOTIFICATIONS: {
        testPhase: 'STUB', platform: 'Notification scenarios', appName: '3 notifications', isEnrolled: true
    },
    TWO_NOTIFICATIONS: {
        testPhase: 'STUB', platform: 'Notification scenarios', appName: '2 notifications', isEnrolled: true
    },
    CREDIT_CARD_NOTIFICATION: {
        testPhase: 'STUB', platform: 'Notification scenarios', appName: 'CreditCard notification', isEnrolled: true
    },
    BUSINESS_BANKING: {
        testPhase: 'STUB', platform: 'Enrolment Error Scenarios', appName: 'Max Devices Reached', isEnrolled: false
    },
    CARD_READER_RESPOND_CARD_NO_NOT_EDITABLE: {
        testPhase: 'STUB', platform: 'Card reader respond', appName: 'Card number not editable', isEnrolled: false
    },
    PASSBOOK_VALID: {
        testPhase: 'STUB', platform: 'Statement frequency', appName: 'Passbook account notifications', isEnrolled: true
    },
    COSERVICING_SET: {
        testPhase: 'STUB', platform: 'GDPR', appName: 'Both are set co-servicing', isEnrolled: true
    },
    KYC_DISMISSED_ONCE: {
        testPhase: 'STUB', platform: 'KYC', appName: 'KYC Dismissed once', isEnrolled: true
    },
    MPA_TRANSFER: {
        testPhase: 'STUB', platform: 'P&T - Transfer - Success', appName: 'Transfer MPA', isEnrolled: true
    },
    MPA_PAYMENT: {
        testPhase: 'STUB', platform: 'P&T - Payment - Success', appName: 'Payment MPA', isEnrolled: true
    },
    PCCDC_ERROR: {
        testPhase: 'STUB', platform: 'Payments Hub - PCCDC', appName: '400 - Try Again', isEnrolled: true
    },
    CREDIT_CARD_ONLY: {
        testPhase: 'STUB', platform: 'Payments Hub - General', appName: 'Credit card only user', isEnrolled: true
    },
    OPEN_BANKING_NO_REWARDS: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'No rewards', isEnrolled: true
    },
    OPEN_BANKING_NO_EXTERNAL_ACCOUNTS: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'No external Accounts', isEnrolled: true
    },
    OPEN_BANKING_REDIRECT_SCREEN_DELAYED_RESPONSE: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Redirect Screen - Delayed Response', isEnrolled: true
    },
    OPEN_BANKING_OB_CONSENT_PROVIDED: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'OB Consent Provided', isEnrolled: true
    },
    OPEN_BANKING_GET_ACCOUNTS_ERROR: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Get Accounts Error', isEnrolled: true
    },
    OPEN_BANKING_GET_PROVIDERS_ERROR: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Get Providers Error', isEnrolled: true
    },
    OPEN_BANKING_REDIRECT_SCREEN_FAILURE: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Redirect Screen - Failure', isEnrolled: true
    },
    OPEN_BANKING_REMOVE_PROVIDER_ERROR: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Remove Provider Error', isEnrolled: true
    },
    OPEN_BANKING_AUTO_LOAD_EXTERNAL_ACCOUNTS_SUCCESS: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Auto Load External Accounts - Success', isEnrolled: true
    },
    OPEN_BANKING_AUTO_LOAD_EXTERNAL_ACCOUNTS_FAILURE: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Auto Load External Accounts - Failure', isEnrolled: true
    },
    OPEN_BANKING_CONSENT_NO_CONSENT: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Consent - No Consent', isEnrolled: true
    },
    OPEN_BANKING_CONSENT_API_FAILED: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Consent - API Failed', isEnrolled: true
    },
    OPEN_BANKING_AUTO_SCROLL_TO_LOADING_TILE: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Auto Scroll To Loading Tile', isEnrolled: true
    },
    OPEN_BANKING_OFFSHORE_CUSTOMER: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Offshore Customer', isEnrolled: true
    },
    OPEN_BANKING_ONSHORE_CUSTOMER: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Onshore Customer', isEnrolled: true
    },
    OPEN_BANKING_RENEWED_FOR_90_DAYS: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Renewed for 90 Days', isEnrolled: true
    },
    OPEN_BANKING_ERROR_ACCOUNT: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Error Account', isEnrolled: true
    },
    OPEN_BANKING_EXPIRED_ACCOUNT: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Expired account', isEnrolled: true
    },
    OPEN_BANKING_BALANCE_NOT_AVAILABLE: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Balance not available', isEnrolled: true
    },
    OPEN_BANKING_VIEW_TRANSACTIONS_ERROR: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'View Transactions Error', isEnrolled: true
    },
    OPEN_BANKING_ADD_EXTERNAL_ACCOUNT_LEAD: {
        testPhase: 'STUB', platform: 'Open banking scenarios', appName: 'Add External Account Lead', isEnrolled: true
    },
    ARRANGEMENTS_LOAN_ONLY: {
        testPhase: 'STUB', platform: 'Arrangements', appName: 'Loan Only', isEnrolled: true
    },
    ARRANGEMENTS_MORTGAGE_ONLY: {
        testPhase: 'STUB', platform: 'Arrangements', appName: 'Mortgage Only', isEnrolled: true
    },
    NO_CREDITCARD: {
        testPhase: 'STUB', platform: 'Arrangements', appName: 'Low Balance Classic Current Account Only', isEnrolled: true
    },
    SCOTTISH_WIDOWS_2_ACCOUNTS: {
        testPhase: 'STUB', platform: 'Scottish Widows', appName: 'Two accounts', isEnrolled: true
    },
    SURVEY_LINK: {
        testPhase: 'STUB', platform: 'Log Off Scenarios', appName: 'Survey link', isEnrolled: false
    },
    BNGA_CBS_TRANSFER: {
        testPhase: 'STUB', platform: 'P&T - Transfer - Errors', appName: 'CBS Prohibitive Transfer', isEnrolled: true
    },
    BNGA_CBS_PAYMENT: {
        testPhase: 'STUB', platform: 'P&T - Payment - Errors', appName: 'CBS Prohibitive Payment', isEnrolled: true
    },
    BNGA_TRANSFER_LIMIT_EXCEEDED: {
        testPhase: 'STUB', platform: 'P&T - Transfer - Errors', appName: 'Internet Transfer Limit', isEnrolled: true
    },
    BNGA_PAYMENT_LIMIT_EXCEEDED: {
        testPhase: 'STUB', platform: 'P&T - Payment - Errors', appName: 'Internet Transaction Limit', isEnrolled: true
    },
    BNGA_NO_BENEFICIARY: {
        testPhase: 'STUB', platform: 'P&T - Payment - Errors', appName: 'No Beneficiary', isEnrolled: true
    },
    BNGA_DUE_SOON: {
        testPhase: 'STUB', platform: 'P&T - Payment - Success', appName: 'Payment Due Soon', isEnrolled: true
    },
    BNGA_NO_ELIGIBLE_ACCOUNTS: {
        testPhase: 'STUB', platform: 'P&T - Transfer - Errors', appName: 'No Eligible Accounts', isEnrolled: true
    },
    BNGA_TRANSFER_INTERNET_LOST: {
        testPhase: 'STUB', platform: 'P&T - Transfer - Errors', appName: 'Internet Connection Lost', isEnrolled: true
    },
    BNGA_PAYMENT_INTERNET_LOST: {
        testPhase: 'STUB', platform: 'P&T - Payment - Errors', appName: 'Internet Connection Lost', isEnrolled: true
    },
    BNGA_PAYMENT_SUCCESS_PASSWORD: {
        testPhase: 'STUB', platform: 'P&T - Payment - Success', appName: 'Password Authentication', isEnrolled: true
    },
    BNGA_PAYMENT_CARD_READER_SUCCESS: {
        testPhase: 'STUB', platform: 'P&T - Payment - Card Reader Success', appName: 'Success Scenario', isEnrolled: true
    },
    BNGA_PAYMENT_CARD_READER_INVALID_PASSCODE: {
        testPhase: 'STUB', platform: 'P&T - Payment - Card Reader Errors', appName: 'Invalid Pass code', isEnrolled: true
    },
    BNGA_ACCOUNT_DAILY_LIMIT: {
        testPhase: 'STUB', platform: 'P&T - Payment - Errors', appName: 'Account Daily Limit', isEnrolled: true
    },
    BNGA_PAYMENT_DECLINED: {
        testPhase: 'STUB', platform: 'P&T - Payment - Errors', appName: 'Declined', isEnrolled: true
    },
    BNGA_PAYMENT_REVIEW: {
        testPhase: 'STUB', platform: 'P&T - Payment - Errors', appName: 'Payment On Hold', isEnrolled: true
    },
    BNGA_TRANSFER_SUCCESS: {
        testPhase: 'STUB', platform: 'P&T - Transfer - Success', appName: 'Success Scenario', isEnrolled: true
    },
    BNGA_BENEFICIARY_WITH_STANDING_ORDER: {
        testPhase: 'STUB', platform: 'P&T - Payment - Errors', appName: 'Beneficiary with standing order', isEnrolled: true
    },
    PENDING_TRANSACTIONS_NOT_RETRIEVED: {
        testPhase: 'STUB', platform: 'Statements', appName: 'Pending Transactions - Error Variations', isEnrolled: true
    },
    SO_DECLINED: {
        testPhase: 'STUB', platform: 'Payments Hub - Standing orders', appName: 'Confirmation - Standing order declined', isEnrolled: true
    },
    SO_ONHOLD: {
        testPhase: 'STUB', platform: 'Payments Hub - Standing orders', appName: 'Confirmation - Standing order held for review', isEnrolled: true
    },
    MORTGAGE_OVERPAYMENT_MULTIPLESUBACCOUNTS: {
        testPhase: 'STUB', platform: 'Mortgage Overpayment', appName: 'Multiple Subaccounts', isEnrolled: true
    },
    MORTGAGE_OVERPAYMENT_ACCOUNT_ELIGIBILITY: {
        testPhase: 'STUB', platform: 'Mortgage Overpayment - Errors', appName: 'Remitting account can not make overpayment', isEnrolled: true
    },
    MORTGAGE_OVERPAYMENT_REPOSSESSED_ACCOUNT_ELIGIBILITY: {
        testPhase: 'STUB', platform: 'Mortgage Overpayment - Errors', appName: 'Repossessed mortgage account error scenario', isEnrolled: true
    },
    MORTGAGE_OVERPAYMENT_LOAN_ACCOUNT_ELIGIBILITY: {
        testPhase: 'STUB', platform: 'Mortgage Overpayment - Errors', appName: 'Loan accounts are not eligible for payment', isEnrolled: true
    },
    PAYMENT_AGGREGATION_INITIATE_PAYMENT_FAILURE: {
        testPhase: 'STUB', platform: 'Payment Hub - Payment Aggregation', appName: 'Initiate Payment Failure', isEnrolled: true
    },
    PAYMENT_AGGREGATION_SSO_FAILURE: {
        testPhase: 'STUB', platform: 'Payment Hub - Payment Aggregation', appName: 'SSO failure', isEnrolled: true
    },
    PAYMENT_AGGREGATION_REDIRECT_SCREEN_DELAY: {
        testPhase: 'STUB', platform: 'Payment Hub - Payment Aggregation', appName: 'Redirect Screen Delay', isEnrolled: true
    },
    PAYMENT_AGGREGATION_ELIGIBLE_PCA_AND_EXT_PCA: {
        testPhase: 'STUB', platform: 'Payment Hub - Payment Aggregation', appName: 'Eligible PCA and Ext PCA only', isEnrolled: true
    },
    PAYMENT_AGGREGATION_INELIGIBLE_PAYMENT_HOLDINGS: {
        testPhase: 'STUB', platform: 'Payment Hub - Payment Aggregation', appName: 'Ineligible Payment Holdings', isEnrolled: true
    },
    PAYMENT_AGGREGATION_PROVIDERS_NOT_SUPPORTED: {
        testPhase: 'STUB', platform: 'Payment Hub - Payment Aggregation', appName: 'Providers Not Supported', isEnrolled: true
    },
    RNGA_SEARCH_ERROR: {
        testPhase: 'STUB', platform: 'Statements', appName: 'All Transactions - Error Scenarios', isEnrolled: true
    },
    VOICE_ID: {
        testPhase: 'STUB', platform: 'Voice ID Scenarios', appName: 'ASM - Invite for Voice ID', isEnrolled: true
    },
    VOICE_ID_NO_MOBILE_NUMBER: {
        testPhase: 'STUB', platform: 'Voice ID Scenarios', appName: 'ASM - Invite for Voice ID, no phone', isEnrolled: true
    },
    CHEQUE_DEPOSIT: {
        testPhase: 'STUB', platform: 'Cheque Imaging Scenarios', appName: 'Promotion ASM', isEnrolled: true
    },
    MANAGE_PAYMENTS_NO_STANDING_ORDER: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'No Standing Order', isEnrolled: true
    },
    MANAGE_PAYMENTS_FETCH_SO_FAILED_OUTCOME: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'Fetch SO - Failed Outcome', isEnrolled: true
    },
    MANAGE_PAYMENTS_FETCH_SO_FORCED_LOGOUT: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'Fetch SO - Forced Logout', isEnrolled: true
    },
    MANAGE_PAYMENTS_AMEND_SO_NO_STEP_UP: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'Amend SO - No Step Up', isEnrolled: true
    },
    MANAGE_PAYMENTS_AMEND_SO_VERIFY_FAILED_OUTCOME: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'Amend SO Verify - Failed Outcome', isEnrolled: true
    },
    MANAGE_PAYMENTS_AMEND_SO_VERIFY_FORCED_LOGOUT: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'Amend SO Verify - Forced Logout', isEnrolled: true
    },
    MANAGE_PAYMENTS_AMEND_SO_VERIFY_TRY_AGAIN: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'Amend SO Verify - Try Again', isEnrolled: true
    },
    MANAGE_PAYMENTS_FETCH_SO_DETAILS_FAILED_OUTCOME: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'Fetch SO Details - Failed Outcome', isEnrolled: true
    },
    MANAGE_PAYMENTS_FETCH_SO_DETAILS_FORCED_LOGOUT: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'Fetch SO Details - Forced Logout', isEnrolled: true
    },
    MANAGE_PAYMENTS_DELETE_SO_FAILED_OUTCOME: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'Delete SO - Failed Outcome', isEnrolled: true
    },
    MANAGE_PAYMENTS_DELETE_SO_FORCED_LOGOUT: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'Delete SO - Forced Logout', isEnrolled: true
    },
    MANAGE_PAYMENTS_FULFILL_AMEND_SO_TRY_AGAIN: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'FulFill Amend SO - Try Again', isEnrolled: true
    },
    MANAGE_PAYMENTS_FULFILL_AMEND_SO_WRONG_PASSWORD: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'FulFill Amend SO - Wrong Password', isEnrolled: true
    },
    MANAGE_PAYMENTS_FULFILL_AMEND_SO_FAILED_OUTCOME: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'FulFill Amend SO - Failed Outcome', isEnrolled: true
    },
    MANAGE_PAYMENTS_FULFILL_AMEND_SO_FORCED_LOGOUT: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'FulFill Amend SO - Forced Logout', isEnrolled: true
    },
    RNGA_HCC: {
        testPhase: 'STUB', platform: 'HCC', appName: 'HCC - Success Scenarios', isEnrolled: true
    },
    CREDIT_CARD_MULTIPLE_ACCOUNTS: {
        testPhase: 'STUB', platform: 'Credit Card Enhancements', appName: 'Multiple Accounts', isEnrolled: true
    },
    CREDIT_CARD_SINGLE_VALID_ACCOUNT: {
        testPhase: 'STUB', platform: 'Credit Card Enhancements', appName: 'Single Account - Valid Data', isEnrolled: true
    },
    BUSINESS_ACCOUNT_MAYBE_MATCH: {
        testPhase: 'STUB', platform: 'Payments Hub - COP', appName: 'Business Account Maybe Match - BAMM', isEnrolled: true
    },
    BANM_MATCH: {
        testPhase: 'STUB', platform: 'Payments Hub - COP', appName: 'Business Account Name Match - BANM', isEnrolled: true
    },
    PANM_MATCH: {
        testPhase: 'STUB', platform: 'Payments Hub - COP', appName: 'Personal Account Name Match - PANM', isEnrolled: true
    },
    MBAM_MATCH: {
        testPhase: 'STUB', platform: 'Payments Hub - COP', appName: 'May Be A Match - MBAM', isEnrolled: true
    },
    COP_MATCH: {
        testPhase: 'STUB', platform: 'Payments Hub - COP', appName: 'Match', isEnrolled: true
    },
    NO_DIRECT_DEBITS: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'No Direct Debits', isEnrolled: true
    },
    UNABLE_TO_LOAD_DD: {
        testPhase: 'STUB', platform: 'Manage Payments', appName: 'Fetch DD - Failed Outcome', isEnrolled: true
    },
    SERVER_ERROR: {
        testPhase: 'STUB', platform: 'Payments Hub - COP', appName: 'Confirmation of Payee - 500 server error', isEnrolled: true
    },
    NCOP: {
        testPhase: 'STUB', platform: 'Payments Hub - COP', appName: 'Cop not available - NCOP', isEnrolled: true
    },
    INCORRECT_ACCOUNT_NUMBER: {
        testPhase: 'STUB', platform: 'Payments Hub - COP', appName: 'Incorrect Account Number - AC01', isEnrolled: true
    },
    WHITELISTED_BILLER_SUCCESS: {
        testPhase: 'STUB', platform: 'Payments Hub - SCA whitelisted biller', appName: 'Success', isEnrolled: true
    },
    PCCMC_SUCCESS: {
        testPhase: 'STUB', platform: 'Payments Hub - Master card', appName: 'Success', isEnrolled: true
    },
    WHITELISTED_BILLER_API_FAIL: {
        testPhase: 'STUB', platform: 'Payments Hub - SCA whitelisted biller', appName: 'Fetch - Try again', isEnrolled: true
    },
    RENAME_ACCOUNTS: {
        testPhase: 'STUB', platform: 'Rename Account', appName: 'Rename Account - success', isEnrolled: true
    },
    PENDING_CHEQUEONLY_DEBITONLY: {
        testPhase: 'STUB', platform: 'Statements', appName: 'Pending Transactions - Null Variations', isEnrolled: true
    },
    VIEWPIN_SUCCESS: {
        testPhase: 'STUB', platform: 'View Pin', appName: 'View PIN - Success - 12 Digit PIN', isEnrolled: false
    },
    SHARE_INTERNATIONAL_ACCOUNT_DETAILS_FAILURE: {
        testPhase: 'STUB', platform: 'Share International Account Details', appName: 'International Account Details API Failure', isEnrolled: true
    },
    TRAVEL_DISPUTE: {
        testPhase: 'STUB', platform: 'Travel Dispute', appName: 'Address postcode missing', isEnrolled: true
    }
});
