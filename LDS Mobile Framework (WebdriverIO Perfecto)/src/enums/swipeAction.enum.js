const Enum = require('./enum');
const DeviceHelper = require('../utils/device.helper');

const swipeAction = (direction) => ({
    perform: (size, offset, swipeDuration = 1) => {
        // We've seen swiping too far on some Android devices, so scale less.
        const scalingFactor = (() => {
            if (DeviceHelper.isAndroid()) {
                return {
                    horizontal: 0.2,
                    vertical: 0.2
                };
            }
            return {
                horizontal: 0.5,
                vertical: 0.2
            };
        })();

        // Directions on unit square.
        const scaledStartX = direction.startX * scalingFactor.horizontal + 0.5;
        const scaledStartY = direction.startY * scalingFactor.vertical + 0.5;
        const scaleEndX = direction.endX * scalingFactor.horizontal + 0.5;
        const scaledEndY = direction.endY * scalingFactor.vertical + 0.5;

        // Scaled onto the window
        const startX = size.width * scaledStartX + offset.x;
        const endX = size.width * scaleEndX + offset.x;
        const startY = size.height * scaledStartY + offset.y;
        const endY = size.height * scaledEndY + offset.y;

        // Swipe on XCUITest supported iOS devices is done using dragFromToForDuration,
        // for other iOS devices and all Android devices swipe is done using touchAction
        if (DeviceHelper.isIOS()) {
            browser.execute('mobile: dragFromToForDuration', {
                duration: '.1',
                fromX: startX,
                fromY: startY,
                toX: endX,
                toY: endY
            });
        } else {
            // TODO: MPL-1513 Currently the value for Swipe Duration is hardcoded for appium, find alternate.
            browser.touchAction([
                {action: 'press', x: startX, y: startY},
                {action: 'wait', ms: !DeviceHelper.isPerfecto() ? 800 : swipeDuration},
                {action: 'moveTo', x: endX, y: endY},
                'release'
            ]);
        }
    }
});

module.exports = new Enum({
    LEFT: swipeAction({
        startX: 1, endX: -1, startY: 0, endY: 0
    }),
    RIGHT: swipeAction({
        startX: -1, endX: 1, startY: 0, endY: 0
    }),
    DOWN: swipeAction({
        startX: 0, endX: 0, startY: -1, endY: 1
    }),
    SHORTUP: swipeAction({
        startX: 0.5, endX: 0.5, startY: 0.75, endY: 0.25
    }),
    UP: swipeAction({
        startX: 0, endX: 0, startY: 1, endY: -1
    })
});
