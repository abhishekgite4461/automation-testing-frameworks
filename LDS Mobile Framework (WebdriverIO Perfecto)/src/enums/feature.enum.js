const Enum = require('./enum');

module.exports = new Enum({
    GDPR: {
        appName: 'GDPR Enabled', defaultStateOn: true, identifier: 'isGDPREnabled'
    },
    ICS: {
        appName: 'ICS Enabled', defaultStateOn: true, identifier: 'isICSEnabled'
    },
    BOTTOM_NAV_BAR: {
        appName: 'Bottom Nav Bar Enabled', defaultStateOn: false, identifier: 'isRedesignTabBarEnabled'
    },
    KYC: {
        appName: 'KYC Enabled', defaultStateOn: false, identifier: 'isKYCEnabled'
    },
    FUTURE_DATED_PAYMENT: {
        appName: 'Future Dated Payment Enabled', defaultStateOn: true, identifier: 'isFutureDatedPaymentEnabled'
    },
    CREATE_STANDING_ORDER: {
        appName: 'Create Standing Order Enabled', defaultStateOn: true, identifier: 'isCreateStandingOrderEnabled'
    },
    ARRANGEMENT_TYPE_BUSINESS: {
        appName: 'Arrangement Type Business Enabled', defaultStateOn: false, identifier: 'isArrangementTypeBusinessEnabled'
    },
    ARRANGEMENT_TYPE_ISA: {
        appName: 'Arrangement Type ISA Enabled', defaultStateOn: true, identifier: 'isArrangementTypeISAEnabled'
    },
    ARRANGEMENT_TYPE_GIA: {
        appName: 'Arrangement Type GIA Enabled', defaultStateOn: false, identifier: 'isArrangementTypeGIAEnabled'
    },
    ARRANGEMENT_TYPE_MORTGAGE: {
        appName: 'Arrangement Type Mortgage Enabled', defaultStateOn: false, identifier: 'isArrangementTypeMortgageEnabled'
    },
    ARRANGEMENT_TYPE_MORTGAGE_UFSS: {
        appName: 'Arrangement Type Mortgage UFSS Enabled', defaultStateOn: true, identifier: 'isArrangementTypeMortgageUFSSEnabled'
    },
    ARRANGEMENT_TYPE_INVESTMENT: {
        appName: 'Arrangement Type Investment Enabled', defaultStateOn: false, identifier: 'isArrangementTypeInvestmentEnabled'
    },
    ARRANGEMENT_TYPE_SAVED_LOAN: {
        appName: 'Arrangement Type Saved Loan Enabled', defaultStateOn: false, identifier: 'isArrangementTypeSavedLoanEnabled'
    },
    ARRANGEMENT_TYPE_SHARE_DEALING: {
        appName: 'Arrangement Type Share Dealing Enabled', defaultStateOn: false, identifier: 'isArrangementTypeShareDealingEnabled'
    },
    ARRANGEMENT_TYPE_HOME_INSURANCE: {
        appName: 'Arrangement Type Home Insurance Enabled', defaultStateOn: true, identifier: 'isArrangementTypeHomeInsuranceEnabled'
    },
    ARRANGEMENT_TYPE_HELP_TO_BUY: {
        appName: 'Arrangement Type Help To Buy Enabled', defaultStateOn: true, identifier: 'isArrangementTypeHelpToBuyEnabled'
    },
    ARRANGEMENT_TYPE_TREASURY_FIXED_TERM_DEPOSIT: {
        appName: 'Arrangement Type Treasury Fixed Term Deposit Enabled', defaultStateOn: false, identifier: 'isArrangementTypeTreasuryFixedTermDepositEnabled'
    },
    ARRANGEMENT_TYPE_TREASURY_32_DAYS_NOTICE: {
        appName: 'Arrangement Type Treasury 32 Days Notice Enabled', defaultStateOn: false, identifier: 'isArrangementTypeTreasury32DaysNoticeEnabled'
    },
    CREATED_MI_ON_FIRST_LOGIN: {
        appName: 'Created MI On First Login Enabled', defaultStateOn: true, identifier: 'isCreatedMIOnFirstLoginEnabled'
    },
    IS_DARK_URL_ENTRY: {
        appName: 'Dark Url Entry Enabled', defaultStateOn: false, identifier: 'isDarkUrlEntryEnabled'
    },
    CWA_JAVASCRIPT_BRIDGE: {
        appName: 'CWA Javascript Bridge Enabled', defaultStateOn: true, identifier: 'isCWAJavascriptBridgeEnabled'
    },
    ASM_LEADS_ENABLED: {
        appName: 'ASM Leads Enabled', defaultStateOn: true, identifier: 'isASMLeadsEnabled'
    },
    STATEMENT_BANNER_LEADS: {
        appName: 'Statement Banner Leads Enabled', defaultStateOn: false, identifier: 'isStatementBannerLeadsEnabled'
    },
    NATIVE_CHANGE_OF_ADDRESS: {
        appName: 'Native Change Of Address Enabled', defaultStateOn: true, identifier: 'isNativeChangeOfAddressEnabled'
    },
    TELL_US: {
        appName: 'Tell Us Enabled', defaultStateOn: true, identifier: 'isTellUsEnabled'
    },
    CLICKABLE_PENDING_TRANSACTIONS: {
        appName: 'Clickable Pending Transactions Enabled', defaultStateOn: true, identifier: 'isClickablePendingTransactionsEnabled'
    },
    PUSH_NOTIFICATION: {
        appName: 'Push Notification Enabled', defaultStateOn: false, identifier: 'isPushNotificationsEnabled'
    },
    BETA_FEEDBACK: {
        appName: 'Beta Feedback Enabled', defaultStateOn: false, identifier: 'isBetaFeedbackEnabled'
    },
    HOME_INSURANCE_VIEW_POLICY_DETAILS: {
        appName: 'Home Insurance View Policy Details Enabled', defaultStateOn: true, identifier: 'isHomeInsuranceViewPolicyDetailsEnabled'
    },
    NOTIFICATION_CENTER: {
        appName: 'Notification Center Enabled', defaultStateOn: true, identifier: 'isNotificationCenterEnabled'
    },
    ANALYTICS_OPT_IN: {
        appName: 'Analytics Opt In Enabled', defaultStateOn: true, identifier: 'isAnalytisOptInEnabled'
    },
    PAY_CREDIT_CARD_BY_DEBIT_CARD: {
        appName: 'Pay Credit Card By Debit Card Enabled', defaultStateOn: false, identifier: 'isPayCreditCardByDebitCardEnabled'
    },
    MOBILE_CHAT: {
        appName: 'Mobile Chat Enabled', defaultStateOn: false, identifier: 'isMobileChatEnabled'
    },
    RATE_THE_APP_DIALOG: {
        appName: 'Rate The App Dialog Enabled', defaultStateOn: true, identifier: 'isRateTheAppDialogEnabled'
    },
    PUSH_NOTIFICATIONS_OPT_IN: {
        appName: 'Push Notifications Opt In Enabled', defaultStateOn: false, identifier: 'isPushNotificationsOptInEnabled'
    },
    PUSH_NOTIFICATIONS_OPT_IN_BSCREEN: {
        appName: 'Push Notifications Opt In B Screen Enabled', defaultStateOn: false, identifier: 'isPushNotificationsOptInBScreenEnabled'
    },
    PUSH_NOTIFICATIONS_SETTINGS: {
        appName: ' Push Notifications Settings Enabled', defaultStateOn: true, identifier: 'isPushNotificationsSettingsEnabled'
    },
    PUSH_NOTIFICATIONS_CONFLICT: {
        appName: 'Push Notifications Conflict Enabled', defaultStateOn: false, identifier: 'isPushNotificationsConflictEnabled'
    },
    EXTENDED_VTD_INFORMATION: {
        appName: 'Extended VTD Information Enabled', defaultStateOn: true, identifier: 'isExtendedVTDInformationEnabled'
    },
    VTD_SPEDING_REWARDS: {
        appName: 'VTD Spending Rewards Enabled', defaultStateOn: false, identifier: 'isVTDSpendingRewardsEnabled'
    },
    SUPPORT_HUB: {
        appName: 'Support Hub Enabled', defaultStateOn: true, identifier: 'isSupportHubEnabled'
    },
    BNGA_SUPPORT_HUB: {
        appName: 'Support Hub Enabled', defaultStateOn: false, identifier: 'isSupportHubEnabled'
    },
    PAYMENT_AGGREGATION: {
        appName: 'Payment Aggregation', defaultStateOn: false, identifier: 'isPaymentAggregationEnabled'
    },
    PAYMENTS_ENABLED: {
        appName: 'Payments Enabled', defaultStateOn: false, identifier: 'isPaymentsEnabled'
    },
    ADD_BENEFICIARY_ENABLED: {
        appName: 'Add Beneficiary Enabled', defaultStateOn: true, identifier: 'isAddBeneficiaryEnabled'
    },
    BIOMETRIC_OPT_IN_ENABLED: {
        appName: 'Biometric Opt In After Enrolment Enabled', defaultStateOn: true, identifier: 'isBiometricOptInAfterEnrolmentEnabled'
    },
    PCA_ALLTAB_ENABLED: {
        appName: 'Current Account All Tab Enabled', defaultStateOn: true, identifier: 'isCurrentAccountAllTransactionsTabEnabled'
    },
    MORTGAGE_OVERPAYMENT: {
        appName: 'Mortgage Overpayment', defaultStateOn: false, identifier: 'isMortgageOverpayEnabled'
    },
    AB_TESTING: {
        appName: 'AB Testing Enabled', defaultStateOn: true, identifier: 'isABTestingEnabled'
    },
    WELCOME_MESSAGE: {
        appName: 'Welcome Message Enabled', defaultStateOn: true, identifier: 'isWelcomeMessageEnabled'
    },
    PRIORITY_MESSAGE: {
        appName: 'Priority Message Enabled', defaultStateOn: false, identifier: 'isPriorityMessageEnabled'
    },
    MDM_PUSH_NOTIFICATIONS_OPT_IN_ENABLED: {
        appName: 'MDM Push Notifications Opt In Enabled', defaultStateOn: false, identifier: 'isMDMPushNotificationsOptInEnabled'
    },
    MDM_PUSH_NOTIFICATIONS_SETTING_ENABLED: {
        appName: 'MDM Push Notifications Setting Enabled', defaultStateOn: false, identifier: 'isMDMPushNotificationsSettingEnabled'
    },
    VIEW_PIN_ENABLED: {
        appName: 'View Pin Enabled', defaultStateOn: false, identifier: 'isViewPinEnabled'
    },
    DARK_URL_ENABLED: {
        appName: 'Dark Url Enabled', defaultStateOn: false, identifier: 'isDarkUrlEntryEnabled'
    },
    SEARCH_TRANSACTIONS_ENABLED: {
        appName: 'Search Transactions Enabled', defaultStateOn: true, identifier: 'isCurrentAccountAllTransactionsSearchEnabled'
    },
    VOICE_ID: {
        appName: 'Voice ID Enabled', defaultStateOn: false, identifier: 'isVoiceIDOptInEnabled'
    },
    SHARE_PAYMENT_RECEIPT: {
        appName: 'Share Payment Receipt', defaultStateOn: false, identifier: 'isShareReceiptEnabled'
    },
    STRONG_CUSTOMER_AUTH: {
        appName: 'Strong Customer Authentication', defaultStateOn: true, identifier: 'isSCAEnabled'
    },
    CHEQUE_DEPOSIT: {
        appName: 'Cheque Deposit Enabled', defaultStateOn: false, identifier: 'isChequeImagingPromotionEnabled'
    },
    WEBVIEW_DEBUG: {
        appName: 'Webview Debuggable', defaultStateOn: false, identifier: 'isWebViewDebuggable'
    },
    WKWEBVIEW_FLAG: {
        appName: 'Wkwebview Flag', defaultStateOn: false, identifier: 'isWKWebViewEnabled'
    },
    PENSION_TRANSFER: {
        appName: 'Pension Transfer Enabled', defaultStateOn: false, identifier: 'isPensionTransferEnabled'
    },
    AMEND_STANDING_ORDER: {
        appName: 'Amend Standing Order', defaultStateOn: true, identifier: 'isNativeAmendStandingOrderEnabled'
    },
    DELETE_STANDING_ORDER: {
        appName: 'Delete Standing Order', defaultStateOn: true, identifier: 'isNativeDeleteStandingOrderEnabled'
    },
    HCC_ENABLED: {
        appName: 'Hcc Enbaled', defaultStateOn: true, identifier: 'isHCCEnabled'
    },
    MOBILE_CHAT_OTHER_BANKING_QUERIES: {
        appName: 'Mobile Chat Enabled For Other Banking Queries', defaultStateOn: false, identifier: 'isMobileChatEnabledForOtherBankingQueries'
    },
    CREDIT_CARD_TILE_REDESIGN_ENABLED: {
        appName: 'Enable credit card tile redesign enhancements', defaultStateOn: false, identifier: 'isCreditCardTileRedesignEnabled'
    },
    OFFER_TILE_AND_PAY_CREDIT_CARD_ENABLED: {
        appName: 'Enable offer tile showing balance and Money transfer links and pay credit card in quick links', defaultStateOn: false, identifier: 'isOfferTileAndPayCreditCardEnabled'
    },
    SPENDINGINSIGHTS_ENABLED: {
        appName: 'Spendinginsights Enabled', defaultStateOn: false, identifier: 'isSpendingInsightsEnabled'
    },
    WHITELISTED_BILLER: {
        appName: 'Whitelisted Biller', defaultStateOn: false, identifier: 'isNewBillPayEnabled'
    },
    PUSH_NOTIFICATIONS_DEEPLINK_ENABLED: {
        appName: 'Push Notifications Deep Link Enabled', defaultStateOn: true, identifier: 'isPushNotificationsDeepLinkEnabled'
    },
    COP: {
        appName: 'Confirmation of Payee', defaultStateOn: true, identifier: 'isCOPEnabled'
    },
    CONDENSED_ACCOUNTTILE: {
        appName: 'Condensed account tile', defaultStateOn: false, identifier: 'isStatementsCondensedAccountTileEnabled'
    },
    REMAINING_OVERDRAFT: {
        appName: 'Remaining Overdraft', defaultStateOn: false, identifier: 'isRemainingOverdraftEnabled'
    },
    DIRECT_DEBIT: {
        appName: 'Direct Debit', defaultStateOn: true, identifier: 'isNativeDirectDebitEnabled'
    },
    RENAME_ACCOUNTS_ENABLED: {
        appName: 'Enable the rename accounts journey', defaultStateOn: false, identifier: 'isRenameAccountsEnabled'
    },
    MAINTENANCE_SCREEN_ENABLED: {
        appName: 'Enable the maintenance screen', defaultStateOn: false, identifier: 'isMaintenanceScreenEnabled'
    },
    EXPOSED_PENDINGTRANSACTIONS: {
        appName: 'Exposed Pending Transactions Enabled', defaultStateOn: false, identifier: 'isExposedPendingEnabled'
    },
    INAPP_SEARCH: {
        appName: 'In App Search', defaultStateOn: false, identifier: 'isInAppSearchEnabled'
    },
    QUICK_LINKS_REDESIGN_FOR_PCA_TILES_ENABLED: {
        appName: 'Enable the quick links for current accounts instead of 3-dot action menu', defaultStateOn: false, identifier: 'isQuickLinksRedesignForPCATilesEnabled'
    },
    ENABLE_REWARD_HUB_ENTRY_POINT_FOR_HALIFAX: {
        appName: 'Enable the Reward Hub entry point for halifax brand', defaultStateOn: false, identifier: 'isRewardHubFeatureEnabled'
    },
    PAY_CREDIT_CARD_BY_MASTER_CARD_ENABLED: {
        appName: 'Pay Credit Card By Master Card Enabled', defaultStateOn: false, identifier: 'isPayCreditCardByMasterCardEnabled'
    },
    SHARE_INTERNATIONAL_ACCOUNT_DETAILS_ENABLED: {
        appName: 'Share IBAN Details', defaultStateOn: false, identifier: 'isShareInternationalAccDetailsEnabled'
    },
    TQT: {
        appName: 'Transaction Query Tool Enabled', defaultStateOn: false, identifier: 'isTransactionQueryToolEnabled'
    },
    TRAVEL_DISPUTES: {
        appName: 'Travel Disputes CTA Enabled', defaultStateOn: false, identifier: 'isTravelDisputesCTAEnabled'
    },
    IOS_HELP_ENHANCEMENTS2: {
        appName: 'iOS Transaction Help Enhancements 2', defaultStateOn: false, identifier: 'isTransactionHelpEnhancements2Enabled'
    },
    MONTHLY_PDF: {
        appName: 'Download Transactions', defaultStateOn: false, identifier: 'isDownloadTransactionsPCAEnabled'
    }
});
