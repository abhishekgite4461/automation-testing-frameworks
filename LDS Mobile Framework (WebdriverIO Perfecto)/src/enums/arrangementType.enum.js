const Enum = require('./enum');

module.exports = new Enum({
    CURRENT: {},
    CREDIT_ACCOUNT: {},
    SAVINGS: {},
    CBS_LOAN_ACCOUNT: {},
    LOAN_ACCOUNT: {},
    ISA_ACCOUNT: {},
    HTB_ISA_ACCOUNT: {},
    TERM_DEPOSIT_ACCOUNT: {},
    MORTGAGE_ACCOUNT: {},
    MORTGAGE_UFSS_ACCOUNT: {},
    INVESTMENT_ACCOUNT: {},
    HOME_INSURANCE: {},
    TREASURY_FTD: {},
    TREASURY_32DCN: {}
});
