const Enum = require('./enum');

module.exports = new Enum({
    INTERNET_BANKING: {
        secure: '1244757126', unsecure: '1244757126'
    },
    OTHER_BANKING_QUERY: {
        secure: '1244757126', unsecure: '1244757126'
    },
    CREDIT_ACCOUNT: {
        secure: '1244757126', unsecure: '1244757126'
    },
    SUSPECTED_FRAUD: {
        secure: '1244757126', unsecure: '1244757126'
    },
    LOST_OR_STOLEN_CARDS: {
        secure: '1244757126', unsecure: '1244757126'
    },
    EMERGENCY_CASH_ABROAD: {
        secure: '1244757126', unsecure: '1244757126'
    },
    PERSONAL_ACCOUNTS: {
        unsecure: '1244757126'
    }
});
