const Enum = require('./enum');

module.exports = new Enum({
    INTERNET_BANKING: {
        secure: '1733578240', unsecure: '1132798302'
    },
    OTHER_BANKING_QUERY: {
        secure: '1733578240', unsecure: '1313374218'
    },
    CURRENT_ACCOUNT: {
        secure: '1733578240', unsecure: '1313374218'
    },
    SAVINGS_ACCOUNT: {
        secure: '1733578240', unsecure: '1313377686'
    },
    CREDIT_ACCOUNT: {
        secure: '1733578240', unsecure: '1733574121'
    },
    ISA_ACCOUNT: {
        secure: '1733578240', unsecure: '1313377686'
    },
    LOAN_ACCOUNT: {
        secure: '1733578240', unsecure: '3456047291'
    },
    TERM_DEPOSIT_ACCOUNT: {
        secure: '1733578240', unsecure: '1313374218'
    },
    SUSPECTED_FRAUD: {
        secure: '1733578240', unsecure: '1132798302'
    },
    LOST_OR_STOLEN_CARDS: {
        secure: '1733578240', unsecure: '1314541605'
    },
    EMERGENCY_CASH_ABROAD: {
        secure: '1733578240', unsecure: '1314541605'
    },
    MEDICAL_ASSISTANCE_ABROAD: {
        unsecure: '1633439013'
    },
    MORTGAGE_ACCOUNT: {
        unsecure: '3458500842'
    },
    COA: {
        secure: '1733578240', unsecure: '1313374218'
    },
    NEWCURRENT_ACCOUNT: {
        secure: '1733578240', unsecure: '1313374218'
    },
    NEWSAVINGS_ACCOUNT: {
        secure: '1733578240', unsecure: '1313377686'
    },
    NEWISA_ACCOUNT: {
        secure: '1733578240', unsecure: '1313377686'
    },
    NEWLOAN_ACCOUNT: {
        secure: '1733578240', unsecure: '3456004611'
    },
    NEWCREDIT_ACCOUNT: {
        secure: '1733578240', unsecure: '1733574121'
    },
    PERSONAL_ACCOUNTS: {
        unsecure: '1313374218'
    },
    NEWMORTGAGES_ACCOUNT: {
        secure: '1733578240', unsecure: '3456047291'
    },
    TABLET_IPAD: {
        unsecure: '1313374218'
    },
    VOICE_ID: {
        secure: '1733624838', unsecure: '1733624838'
    }
});
