const Enum = require('./enum');

module.exports = new Enum({
    CURRENT_SAVINGS: {value: 'Current / Savings', account: 'accountNumber'},
    CREDIT_CARD: {value: 'Credit card', account: 'creditCardNumber'},
    LOAN: {value: 'Loan', account: 'loanNumber'},
    MORTGAGE: {value: 'Mortgage', account: 'mortgageNumber'}
});
