const Enum = require('./enum');

module.exports = new Enum({
    Android: {os: 'Android', osVersionRegex: '(4\\.[4-9]|[5-9]).*'},
    iOS: {os: 'iOS', osVersionRegex: '(1\\d).*'}
});
