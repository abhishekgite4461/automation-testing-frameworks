const date = new Date();
const teamCombinations = [
    ['@primary'], // sundays run primary
    ['@team-login', '@team-pi'],
    ['@team-payments', '@team-outboundmessaging', '@team-mco'],
    ['@team-statements', '@team-panm'],
    ['@team-mpt', '@team-servicing']
];

module.exports = {
    WEEKNIGHT: teamCombinations[date.getDay() % 4]
};
