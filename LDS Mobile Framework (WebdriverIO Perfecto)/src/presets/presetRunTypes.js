const perfectoDeviceList = require('../wdio_config/perfectoDeviceList');
const Enum = require('../enums/enum');

const uatDeviceList = perfectoDeviceList.weeknightUATRegression;
const defaults = {
    htmlReport: true,
    app: ['NGA3'],
    perfectoScreenshotOnErrorOnly: true
};

const basePresetTags = 'not @outOfScope and not @rebootDevice and not @appiumOnly and not @manual and not @manualOnly and not @team-uat and not @wkWebview';
const baseStubTags = `not @environmentOnly and ${basePresetTags}`;
const baseEnvironmentTags = 'not @stubOnly and not @noExistingData '
    + `and not @instrument and not @sensorInstrument and ${basePresetTags} `;
const stubTags = (brand) => {
    const baseTags = `${baseStubTags} and @optFeature and not @platinum and not @exhaustiveData 
    and @stubOnly`;
    if (brand === 'LDS' || brand === 'BOS' || brand === 'HFX') {
        return baseTags;
    } if (brand === 'MBNA') {
        return `(${baseTags} and (@mbna or @mbnaSuite))`;
    }
    throw new Error(`Incorrect brand name ${brand}`);
};

const envTagsGeneric = (brand) => {
    const baseTags = `${baseEnvironmentTags} and @optFeature and not @platinum and not @exhaustiveData and not @sca 
    and not @inAppSearch and not @sit02Job and not @coaLiberty and not @cop and not @appWarnBan and @genericAccount`;
    if (brand === 'LDS' || brand === 'BOS' || brand === 'HFX') {
        return baseTags;
    } if (brand === 'MBNA') {
        return `(${baseTags} and (@mbna or @mbnaSuite))`;
    }
    throw new Error(`Incorrect brand name ${brand}`);
};

const envTags = (brand) => {
    const baseTags = `${baseEnvironmentTags} and @optFeature and not @platinum and not @exhaustiveData and not @sca 
    and not @inAppSearch and not @coaLiberty and not @cop and not @appWarnBan`;
    if (brand === 'LDS' || brand === 'BOS' || brand === 'HFX') {
        return baseTags;
    } if (brand === 'MBNA') {
        return `(${baseTags} and (@mbna or @mbnaSuite))`;
    }
    throw new Error(`Incorrect brand name ${brand}`);
};

const envTagsNonGeneric = (brand) => {
    const baseTags = `${baseEnvironmentTags} and @optFeature and not @platinum and not @exhaustiveData and not @sca 
    and  not @inAppSearch and not @sit02Job and not @coaLiberty and not @cop and not @appWarnBan and 
    (@otherAccount or  @unEnrolled)`;
    if (brand === 'LDS' || brand === 'BOS' || brand === 'HFX') {
        return baseTags;
    } if (brand === 'MBNA') {
        return `(${baseTags} and (@mbna or @mbnaSuite))`;
    }
    throw new Error(`Incorrect brand name ${brand}`);
};

const DeviceHealthCheckTags = (brand) => {
    const baseTags = `${baseEnvironmentTags} and not @platinum and not 
    @exhaustiveData and not @sit02Job and @deviceHealthCheck
    and not @coaLiberty and not @cop and not @appWarnBan`;
    if (brand === 'LDS' || brand === 'BOS' || brand === 'HFX') {
        return baseTags;
    } if (brand === 'MBNA') {
        return `(${baseTags} and (@mbna or @mbnaSuite))`;
    }
    throw new Error(`Incorrect brand name ${brand}`);
};

const exhaustiveDataEnvTags = (brand) => {
    const baseTags = `${baseEnvironmentTags} and @optFeature and not @platinum and @exhaustiveData and not 
    @sca and not @coaLiberty and not @cop and not @appWarnBan and not @inAppSearch`;
    if (brand === 'LDS' || brand === 'BOS' || brand === 'HFX') {
        return baseTags;
    } if (brand === 'MBNA') {
        return `(${baseTags} and (@mbna or @mbnaSuite))`;
    }
    throw new Error(`Incorrect brand name ${brand}`);
};

const environmentSpecificDataEnvTags = (brand) => {
    const baseTags = `${baseEnvironmentTags} and @optFeature and not @platinum and @sit02Job`;
    if (brand === 'LDS' || brand === 'BOS' || brand === 'HFX') {
        return baseTags;
    } if (brand === 'MBNA') {
        return `(${baseTags} and (@mbna or @mbnaSuite))`;
    }
    throw new Error(`Incorrect brand name ${brand}`);
};

const brandNameForWeekday = ((new Date().getDay() % 2) ? ['LDS', 'HFX'] : ['BOS', 'MBNA']);

const presetConfig = {
    rebootDevice: {
        deviceIds: (platform) => perfectoDeviceList.ALL[platform],
        server: ['STUB'],
        type: ['RNGA'],
        brand: ['LDS'],
        platform: ['Android', 'iOS'],
        tags: '@rebootDevice',
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-preCommitSmokeTest']
    },
    parallelExecution: {
        server: ['STUB'],
        type: ['RNGA'],
        brand: ['LDS'],
        platform: ['Android', 'iOS'],
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        perfectoExtraTags: ['ci-mob-3'],
        perfectoReportingJobName: 'paralleljob',
        maxInstances: 2
    },
    weekdaySanity: {
        provider: 'mock',
        server: ['STUB'],
        platform: ['Android', 'iOS'],
        type: ['RNGA'],
        brand: ['LDS'],
        tags: '@smokeNga3',
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-preCommitSmokeTest']
    },
    // Weeknight Functional Stubs
    weeknightFunctionalStubsRnga: {
        deviceIds: (platform, brand) => perfectoDeviceList.weeknightFunctionalStubs.RNGA[platform][brand],
        server: ['STUB'],
        platform: ['Android', 'iOS'],
        type: ['RNGA'],
        brand: ['LDS', 'BOS', 'HFX', 'MBNA'],
        tags: stubTags,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true
    },
    weeknightFunctionalStubsBnga: {
        deviceIds: (platform, brand) => perfectoDeviceList.weeknightFunctionalStubs.BNGA[platform][brand],
        server: ['STUB'],
        platform: ['Android', 'iOS'],
        type: ['BNGA'],
        brand: ['LDS', 'BOS'],
        tags: `${baseStubTags} and @optFeature and not @sensorInstrument and not @instrument and @stubOnly`,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true
    },
    // Weekday device health check Functional Environment master for NonGeneric
    weekdayDeviceHealthCheckFunctionalEnvironmentMasterGeneric: {
        deviceIds: (platform,
            brand) => perfectoDeviceList.weeknightFunctionalEnvironmentMaster.Generic[platform][brand],
        platform: ['Android', 'iOS'],
        server: ['SIT10-W'],
        type: ['RNGA'],
        brand: ['LDS', 'HFX', 'BOS', 'MBNA'],
        // Disabled ibReg temporarily as we don't have any test data here.
        tags: DeviceHealthCheckTags,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weeknightFunctionalEnvironment'],
        dataRepository: 'remote',
        toggleSwitches: true,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        ensureMobileNumber: true
    },
    // Weekday device health check Functional Environment master for NonGeneric
    weekdayDeviceHealthCheckFunctionalEnvironmentMasterNonGeneric: {
        deviceIds: (platform,
            brand) => perfectoDeviceList.weeknightFunctionalEnvironmentMaster.NonGeneric[platform][brand],
        platform: ['Android', 'iOS'],
        server: ['SIT10-W'],
        type: ['RNGA'],
        brand: ['LDS', 'HFX', 'BOS', 'MBNA'],
        // Disabled ibReg temporarily as we don't have any test data here.
        tags: DeviceHealthCheckTags,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weeknightFunctionalEnvironment'],
        dataRepository: 'remote',
        toggleSwitches: true,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        ensureMobileNumber: true
    },

    // Weeknight Functional Environment for Generic scenario
    weeknightMasterAndroidGeneric: {
        deviceIds: (platform,
            brand) => perfectoDeviceList.weeknightFunctionalEnvironmentMaster.Generic[platform][brand],
        platform: ['Android'],
        server: ['SIT10-W'],
        type: ['RNGA'],
        brand: brandNameForWeekday,
        // Disabled ibReg temporarily as we don't have any test data here.
        tags: envTagsGeneric,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weeknightFunctionalEnvironmentMaster'],
        dataRepository: 'remote',
        toggleSwitches: true,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        ensureMobileNumber: true,
        reportPortal: true
    },
    // Weeknight Functional Environment for Non Generic scenario
    weeknightMasterAndroidNonGeneric: {
        deviceIds: (platform,
            brand) => perfectoDeviceList.weeknightFunctionalEnvironmentMaster.NonGeneric[platform][brand],
        platform: ['Android'],
        server: ['SIT10-W'],
        type: ['RNGA'],
        brand: brandNameForWeekday,
        // Disabled ibReg temporarily as we don't have any test data here.
        tags: envTagsNonGeneric,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weeknightFunctionalEnvironmentMaster'],
        dataRepository: 'remote',
        toggleSwitches: true,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        ensureMobileNumber: true,
        reportPortal: true
    },
    // Weeknight Functional Environment for Feature team
    weeknightMasterIosGeneric: {
        deviceIds: (platform,
            brand) => perfectoDeviceList.weeknightFunctionalEnvironmentMaster.Generic[platform][brand],
        platform: ['iOS'],
        server: ['SIT10-W'],
        type: ['RNGA'],
        brand: brandNameForWeekday,
        tags: envTagsGeneric,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weeknightFunctionalEnvironmentMaster'],
        dataRepository: 'remote',
        toggleSwitches: true,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        ensureMobileNumber: true,
        reportPortal: true
    },
    weeknightMasterIosNonGeneric: {
        deviceIds: (platform,
            brand) => perfectoDeviceList.weeknightFunctionalEnvironmentMaster.NonGeneric[platform][brand],
        platform: ['iOS'],
        server: ['SIT10-W'],
        type: ['RNGA'],
        brand: brandNameForWeekday,
        // Disabled ibReg temporarily as we don't have any test data here.
        tags: envTagsNonGeneric,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weeknightFunctionalEnvironmentMaster'],
        dataRepository: 'remote',
        toggleSwitches: true,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        ensureMobileNumber: true,
        reportPortal: true
    },
    // Weeknight Functional Environment of business app in Master Branch
    weeknightFunctionalEnvironmentBnga: {
        deviceIds: (platform, brand) => perfectoDeviceList.weeknightFunctionalEnvironmentBnga[platform][brand],
        platform: ['iOS', 'Android'],
        server: ['SIT10-W'],
        type: ['BNGA'],
        brand: ['LDS', 'BOS'],
        tags: envTags,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weeknightFunctionalEnvironmentBnga'],
        dataRepository: 'remote',
        toggleSwitches: true,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true
    },
    // Weeknight Functional Environment of business app in Iguana Branch
    weeknightFunctionalEnvironmentBNGARelease: {
        deviceIds: (platform, brand) => perfectoDeviceList.weeknightFunctionalEnvironmentBnga[platform][brand],
        platform: ['iOS', 'Android'],
        server: ['SIT10-W'],
        type: ['BNGA'],
        brand: ['LDS', 'BOS'],
        tags: envTags,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weeknightFunctionalEnvironmentBNGARelease'],
        dataRepository: 'remote',
        toggleSwitches: true,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true
    },
    localExecution: {
        deviceIds: (platform, brand) => perfectoDeviceList.localExecution[platform][brand],
        server: ['PUT12'],
        platform: ['Android'],
        type: ['RNGA'],
        brand: ['LDS', 'BOS', 'HFX'],
        tags: `${baseEnvironmentTags} and @optFeature and @platinum`
    },
    // Weeknight UAT Regression
    weeknightUATRegressionRnga: {
        deviceIds: (platform, brand) => uatDeviceList.RNGA[platform][brand],
        server: ['UAT-LUAT1W'],
        platform: ['iOS', 'Android'],
        type: ['RNGA_UAT'],
        brand: ['LDS', 'BOS', 'HFX', 'MBNA'],
        tags: '@uatDaily and not @mbna and not @pending and not @webview',
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weeknightUATRegression'],
        dataRepository: 'remote',
        toggleSwitches: false,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_UAT',
        perfectoEnableVideo: true
    },
    weeknightUATRegressionBnga: {
        deviceIds: (platform, brand) => uatDeviceList.BNGA[platform][brand],
        server: ['UAT-LUAT2W'],
        platform: ['iOS', 'Android'],
        type: ['BNGA_UAT'],
        brand: ['LDS', 'BOS'],
        tags: '@team-uat and not @mbna and not @pending and not @webview',
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weeknightUATRegression'],
        dataRepository: 'remote',
        toggleSwitches: false,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_UAT',
        perfectoEnableVideo: true
    },

    // Weekend Compatibility Stub
    weekendCompatibilityAndroid: {
        deviceIds: () => perfectoDeviceList.weekendCompatibilityStubs.Android,
        server: ['STUB'],
        platform: ['Android'],
        type: ['RNGA'],
        tags: stubTags,
        perfectoEnableVideo: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weekend', 'ci-mob-3-weeknightAndroid']
    },
    weekendCompatibilityIos: {
        deviceIds: () => perfectoDeviceList.weekendCompatibilityStubs.iOS,
        server: ['STUB'],
        platform: ['iOS'],
        type: ['RNGA'],
        tags: stubTags,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weekend', 'ci-mob-3-weeknightIos']
    },
    // Weeknight Functional Environment - Platinum Journeys
    weeknightFunctionalEnvironmentPlatinum: {
        deviceIds: (platform,
            brand) => perfectoDeviceList.weeknightFunctionalEnvironmentPlatinumMaster[platform][brand],
        platform: ['Android', 'iOS'],
        server: ['PUT12'],
        brand: ['LDS'],
        type: ['RNGA'],
        tags: `${baseEnvironmentTags} and @optFeature and @platinum`,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weeknightFunctionalEnvironmentPlatinum'],
        dataRepository: 'remote',
        toggleSwitches: true,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        ensureMobileNumber: true
    },
    // Weeknight Functional Environment - Platinum Journeys
    weeknightFunctionalEnvironmentPlatinumSIT10: {
        deviceIds: (platform, brand) => perfectoDeviceList.weeknightFunctionalEnvironmentMaster[platform][brand],
        platform: ['Android', 'iOS'],
        server: ['SIT10-W'],
        brand: ['LDS', 'HFX', 'BOS', 'MBNA'],
        type: ['RNGA'],
        tags: `${baseEnvironmentTags} and @optFeature and @platinum`,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weeknightFunctionalEnvironmentPlatinumSIT10'],
        dataRepository: 'remote',
        toggleSwitches: true,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        ensureMobileNumber: true
    },
    // Weeknight Functional Environment for Feature team and using the stub job devices
    weekendFunctionalEnvironmentMaster: {
        deviceIds: (platform, brand) => perfectoDeviceList.weeknightFunctionalMasterSIT02[platform][brand],
        platform: ['Android', 'iOS'],
        server: ['SIT10-W'],
        type: ['RNGA'],
        brand: ['LDS', 'BOS', 'HFX', 'MBNA'],
        // Disabled ibReg temporarily as we don't have any test data here.
        tags: envTags,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weekendFunctionalEnvironmentMaster'],
        dataRepository: 'remote',
        toggleSwitches: true,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        ensureMobileNumber: true
    },
    // Weeknight Functional Environment for Feature team and using SIT02-B
    weekdayFunctionalEnvironmentSpecificMaster: {
        deviceIds: (platform, brand) => perfectoDeviceList.weeknightFunctionalMasterSIT02[platform][brand],
        platform: ['Android', 'iOS'],
        server: ['SIT02-B'],
        type: ['RNGA'],
        brand: ['LDS', 'BOS', 'HFX', 'MBNA'],
        tags: environmentSpecificDataEnvTags,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weekdayFunctionalEnvironmentSpecificMaster'],
        dataRepository: 'remote',
        toggleSwitches: true,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        ensureMobileNumber: true,
        reportPortal: true
    },
    // Weeknight Functional Environment for Feature team for exhaustive and data maintanance scripts
    exhaustiveDataFunctionalEnvironmentMaster: {
        deviceIds: (platform, brand) => perfectoDeviceList.weeknightFunctionalEnvironmentMaster[platform][brand],
        platform: ['Android', 'iOS'],
        server: ['SIT10-W'],
        type: ['RNGA'],
        brand: ['LDS', 'BOS', 'HFX', 'MBNA'],
        tags: exhaustiveDataEnvTags,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-exhaustiveDataFunctionalEnvironmentMaster'],
        dataRepository: 'remote',
        toggleSwitches: true,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        ensureMobileNumber: true
    },
    // Weeknight Functional Environment for Release team
    weeknightFunctionalEnvironmentAndroid: {
        deviceIds: (platform, brand) => perfectoDeviceList.weeknightFunctionalEnvironment[platform][brand],
        platform: ['Android'],
        server: ['PUT12'],
        type: ['RNGA'],
        brand: ['LDS', 'HFX', 'BOS', 'MBNA'],
        // Disabled ibReg temporarily as we don't have any test data here.
        tags: envTags,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weeknightFunctionalEnvironment'],
        dataRepository: 'remote',
        toggleSwitches: true,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        ensureMobileNumber: true
    },
    // Weeknight Functional Environment for Release team
    weeknightFunctionalEnvironmentIos: {
        deviceIds: (platform, brand) => perfectoDeviceList.weeknightFunctionalEnvironment[platform][brand],
        platform: ['iOS'],
        server: ['PUT12'],
        type: ['RNGA'],
        brand: ['LDS', 'HFX', 'BOS', 'MBNA'],
        // Disabled ibReg temporarily as we don't have any test data here.
        tags: envTags,
        perfectoExtraTags: ['ci-mob-3', 'ci-mob-3-weeknightFunctionalEnvironment'],
        dataRepository: 'remote',
        toggleSwitches: true,
        perfectoScreenshotOnErrorOnly: true,
        perfectoDescription: 'Lloyds_FT',
        perfectoEnableVideo: true,
        ensureMobileNumber: true
    },

    // Because this config isn't injectable, I need a test configuration for the unit test to depend on.
    testPresetWithTags: {
        tags: '{$tags}not @environmentOnly and not @outOfScope'
    },
    testPresetWithoutTags: {
        tags: 'not @stubOnly and not @ibRegistration and @primary and @perfectoStability'
    }
};

Object.keys(presetConfig).forEach((key) => Object.assign(presetConfig[key], defaults));

module.exports = new Enum(presetConfig);
