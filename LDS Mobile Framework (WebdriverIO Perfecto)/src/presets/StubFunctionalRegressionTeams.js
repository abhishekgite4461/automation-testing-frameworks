const date = new Date();
const teamCombinations = [
    ['@primary'], // sundays run primary
    ['@team-login', '@team-pi', '@team-payments', '@team-outboundmessaging'],
    ['@team-mpt', '@team-servicing', '@team-statements', '@team-panm', '@team-mco'],
    ['@team-login', '@team-pi', '@team-payments', '@team-outboundmessaging'],
    ['@team-mpt', '@team-servicing', '@team-statements', '@team-panm', '@team-mco']
];

module.exports = {
    WEEKNIGHT: teamCombinations[date.getDay() % 4]
};
