const {When, Then} = require('cucumber');

const CallUsPage = require('../pages/callUs/callUs.page');
const CallUsMissingMobilePage = require('../pages/callUs/callUsMissingMobilePopUp.page');
const ResetYourPasswordPage = require('../pages/settings/resetYourPassword.page');
const RequestPermissionPage = require('../pages/requestPermissionPopUp.page');
const AlertPopUpPage = require('../pages/alertPopUp.page');
const DeviceHelper = require('../utils/device.helper');
const AuthPage = require('../pages/common/auth.page');

const authPage = new AuthPage();
const callUsPage = new CallUsPage();
const missingMobilePage = new CallUsMissingMobilePage();
const requestPermissionPage = new RequestPermissionPage();
const resetPasswordPage = new ResetYourPasswordPage();
const alertPopUpPage = new AlertPopUpPage();

Then(/^I should be on the secure call page$/, () => callUsPage.waitForSecureCallPageLoad());

Then(/^I am able to view the call us button$/, () => {
    callUsPage.shouldSeeCallUsButton();
});

Then(/^I am able to view the account number and sort code$/, () => {
    callUsPage.shouldSeeAccountNumber();
    callUsPage.shouldSeeSortCode();
});

Then(/^I am able to view the reference number$/, () => {
    callUsPage.shouldSeeReferenceNumber();
});

Then(/^I am able to view the below information labels$/, (table) => {
    const labels = table.hashes();
    callUsPage.validateCallUsPageLabels(labels);
});

Then(/^I am able to view the global menu$/, () => {
    authPage.selectTab('more');
});

When(/^I select call us button in call us page$/, () => {
    callUsPage.selectCallUsButton();
    if (DeviceHelper.isAndroid()) {
        requestPermissionPage.allowNativeAppCall();
    }
});

Then(/^I am able to view the phone number button to call$/, () => {
    callUsPage.shouldSeePhoneNumber();
});

Then(/^I should be on the unsecure call us page$/, () => {
    callUsPage.waitForUnSecureCallPageLoad();
});

Then(/^I should be on the unsecure call us page overlay$/, () => {
    callUsPage.waitForUnSecureCallPageLoad();
});

Then(/^I select the back option from the unsecure call us page overlay$/, () => {
    callUsPage.selectBackButton();
});

Then(/^I select the close option from the unsecure call us page overlay$/, () => {
    callUsPage.selectCloseButton();
});

Then(/^I select the phone number on unsecure call us page$/, () => {
    callUsPage.selectPhoneNumber();
    if (DeviceHelper.isAndroid()) {
        requestPermissionPage.allowNativeAppCall();
    }
});

Then(/^I should be advised that I need to register a mobile phone number$/, () => {
    if (DeviceHelper.isAndroid()) {
        requestPermissionPage.allowNativeAppCall();
    }
    missingMobilePage.shouldSeeMissingNumberAlert();
});

Then(/^I validate the call made for (.*) is (secure|unsecure) call$/, (option, callType) => {
    const type = (callType === 'secure') ? 'secure' : 'unsecure';
    callUsPage.validatePhoneNumber(option, type);
});

Then(/^I should be given an option to register a mobile phone$/, () => {
    missingMobilePage.shouldSeeOptionToAddNumber();
});

Then(/^I should be given an option to continue with a non-authenticated call to Customer Services$/, () => {
    missingMobilePage.shouldSeeOptionToContinueCall();
});

Then(/^I am shown a link for lost or stolen cards$/, () => {
    callUsPage.shouldSeeLostOrStolenSelfServiceButton();
});

Then(/^I am shown the below self service options$/, (table) => {
    const options = table.hashes()
        .map((entry) => entry.serviceOption);
    callUsPage.shouldSeeSelfServiceOptions(options);
});

Then(/^I am not shown the below self service options$/, (table) => {
    const options = table.hashes()
        .map((entry) => entry.serviceOption);
    callUsPage.shouldNotSeeSelfServiceOptions(options);
});

When(/^I select (.*) self service link$/, (option) => {
    callUsPage.selectSelfServiceOption(option);
});

Then(/^I am taken to reset your password page in the app$/, () => {
    resetPasswordPage.waitForPageLoad();
});

Then(/^I select the option to register a mobile phone number$/, () => {
    missingMobilePage.selectAddPhoneNumberOption();
});

Then(/^I select the option to continue with call$/, () => {
    missingMobilePage.selectContinueWithCallOption();
});

Then(/^I am displayed the call us (.*) for the selected option$/, (page) => {
    callUsPage.validateCallUsPages(page.replace(/ /g, ''));
});

Then(/^I should see an alert pop up with alternate unsecure phone number for contacting$/, () => {
    alertPopUpPage.shouldSeeAlert();
    alertPopUpPage.shouldSeeAlertContentText();
    callUsPage.validateTabletPhoneNumber();
});

Then(/^I should be on the change of address call us screen$/, () => {
    callUsPage.waitForSecureCallPageLoad();
});

Then(/^I am on the change of address call us screen$/, () => {
    callUsPage.waitForSecureCallPageLoad();
});
