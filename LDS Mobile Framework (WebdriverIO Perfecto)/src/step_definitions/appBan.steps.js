const {When} = require('cucumber');
const AppBanPage = require('../pages/appBan.page');

const appBanPage = new AppBanPage();

When(/^I navigate to the ban screen$/, () => {
    appBanPage.waitForAppBanPageLoad();
});

When(/^I should see an option to update the app in the ban screen$/, () => {
    appBanPage.verifyUpdateAppButton();
});

When(/^I should see an option to use the Mobile Browser Banking$/, () => {
    appBanPage.verifyGoToOurMobileSiteButton();
});

When(/^I choose to update the app in the ban screen$/, () => {
    appBanPage.clickUpdateAppButton();
});

When(/^I choose to use the Mobile Browser Banking$/, () => {
    appBanPage.clickGoToOurMobileSiteButton();
});
