const {When, Then} = require('cucumber');
const AccountMatcher = require('../../data/utils/accountMatcher');

const NativeAmendStandingOrderPage = require('../../pages/amendOrDeleteStandingOrder/nativeAmendStandingOrder.page');
const DirectDebitPage = require('../../pages/amendOrDeleteStandingOrder/directDebitVTD.page');
const CommonPage = require('../../pages/common.page');

const commonPage = new CommonPage();
const nativeAmendStandingOrderPage = new NativeAmendStandingOrderPage();
const directDebitVTD = new DirectDebitPage();

Then(/^I should (see|not see) the (.*) screen for native amend standing order$/, (isVisible, screenName) => {
    nativeAmendStandingOrderPage.waitForPageToLoad(isVisible === 'see', screenName);
});

Then(/^I should see the native scheduled payment hub landing screen$/, () => {
    nativeAmendStandingOrderPage.shouldSeeStandingOrdersHeading();
});

Then(/^I should see the (.*) standing orders account name$/, (accountType) => {
    const accountName = accountType.replace(/ /g, '');
    nativeAmendStandingOrderPage.shouldSeeSelectedAccount(AccountMatcher.getData(accountName));
});

Then(/^I should (see|not see) standing orders set up for (.*) account (with|without) amend and (with|without) delete button$/, (isVisibleSO, accountType, isAmendButtonVisible, isDeleteButtonVisible) => {
    nativeAmendStandingOrderPage.shouldSeeStandingOrdersForAccount(isVisibleSO === 'see', accountType,
        isAmendButtonVisible === 'with', isDeleteButtonVisible === 'with');
});

Then(/^I select the set up standing order button$/, () => {
    nativeAmendStandingOrderPage.selectSetUpStandingOrderButton();
});

Then(/^I should see no standing orders in the list$/, () => {
    nativeAmendStandingOrderPage.shouldSeeNoStandingOrderInList();
});

Then(/^I am shown a message that I have no direct debits set up under the relevant header$/, () => {
    nativeAmendStandingOrderPage.shouldSeeNoDirectDebitsInList();
});

Then(/^I am shown a message that unable to load direct debits under the relevant header$/, () => {
    nativeAmendStandingOrderPage.shouldSeeUnabletoLoadDirectDebitMessage();
});

Then(/^I select a direct debit$/, () => {
    nativeAmendStandingOrderPage.selectFirstDirectDebit();
});

Then(/^I select a standing order$/, () => {
    nativeAmendStandingOrderPage.selectFirstStandingOrder();
});

Then(/^I swipe on the direct debit to delete$/, () => {
    nativeAmendStandingOrderPage.swipeFirstDirectDebitToDelete();
});

Then(/^I swipe on the standing order to delete$/, () => {
    nativeAmendStandingOrderPage.swipeFirstStandingOrderToDelete();
});

Then(/^I am shown the option to delete$/, () => {
    nativeAmendStandingOrderPage.shouldSeeDeleteOptionForTheDirectDebit();
});

Then(/^I select delete option$/, () => {
    nativeAmendStandingOrderPage.selectDeleteOption();
});

Then(/^I should see option to delete for a cancellable direct debit$/, () => {
    directDebitVTD.shouldSeeDeleteDDButton();
});

Then(/^I choose the delete option to delete the direct debit$/, () => {
    directDebitVTD.selectDeleteDDButton();
});

Then(/^I am displayed with the Delete Direct Debit Info page$/, () => {
    directDebitVTD.shouldSeeDeleteDDInfoPage();
});

Then(/^I am shown the CTA buttons according to the LBG and Non-LBG DD$/, () => {
    directDebitVTD.verifyCTAButtons();
});

When(/^I navigate back to the scheduled payment hub$/, () => {
    commonPage.clickBackButton();
    commonPage.clickBackButton();
});

When(/^I choose the Manage button on the schedueled payment hub page$/, () => {
    commonPage.clickBackButton();
    commonPage.clickBackButton();
});

Then(/^I choose (.*) option to delete the standing order$/, (type) => {
    directDebitVTD.selectSOorDdDeleteOption(type);
});

Then(/^I select (amend|delete) button for the (.*) standing order$/, (buttonName, standingOrderName) => {
    nativeAmendStandingOrderPage.selectButtonForStandingOrder(buttonName,
        AccountMatcher.getData(standingOrderName));
});

Then(/^I am shown all direct debits set up against the account listed with following fields$/,
    (ddFields) => {
        const FieldsToValidate = ddFields.hashes();
        nativeAmendStandingOrderPage.shouldSeeDDOptions(FieldsToValidate);
    });

Then(/^I am shown the VTD for the (standing order|direct debit) with following fields$/,
    (type, vtdFields) => {
        const FieldsToValidate = vtdFields.hashes();
        directDebitVTD.shouldSeeOptionsInVTD(type, FieldsToValidate);
    });

Then(/^I should see standing order name (.*) in the winback$/, (standingOrderName) => {
    nativeAmendStandingOrderPage.shouldSeeSONameInWinBack(AccountMatcher.getData(standingOrderName));
});
