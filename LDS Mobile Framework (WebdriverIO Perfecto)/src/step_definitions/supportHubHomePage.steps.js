const {When, Then} = require('cucumber');
const SupportHubHomePage = require('../pages/supportHubHomePage.page');

const supportHubHomePage = new SupportHubHomePage();

Then(/^I should be on the support hub page$/, () => {
    supportHubHomePage.waitForPageLoad();
});

Then(/^I select call us on support hub page$/, () => {
    supportHubHomePage.selectCallUs();
});

When(/^I select the chat to us button$/, () => {
    supportHubHomePage.selectMobileChat();
});

Then(/^I should not see CMA Tile in the support hub home page$/, () => {
    supportHubHomePage.shouldNotDisplayCMASurveyLink();
});

Then(/^I am navigated to support hub page title$/, () => {
    supportHubHomePage.waitForPageLoad();
});

Then(/^I select the CMA survey link$/, () => {
    supportHubHomePage.selectCMASurveyLink();
});

When(/^I should see "(.*)" in the support hub home page$/, (tile) => {
    if (tile === 'CMA Tile') {
        supportHubHomePage.shouldDisplayCMASurveytile();
    } else if (tile === 'atm and branch finder tile') {
        supportHubHomePage.shouldDisplayATMandBranchFindertile();
    } else if (tile === 'app feedback tile') {
        supportHubHomePage.shouldDisplayAppFeedbacktile();
    } else {
        throw new Error(`Unknown tiles: ${tile}`);
    }
});

When(/^I select message us option from support hub home page$/, () => {
    supportHubHomePage.selectMobileChat();
});

When(/^I should (not see|see) message us option$/, (type) => {
    if (type === 'not see') {
        supportHubHomePage.verifyMobileChatVisibility(false);
    } else {
        supportHubHomePage.verifyMobileChatVisibility(true);
    }
});

Then(/^the label is (.*) for the message button$/, (label) => {
    supportHubHomePage.verifyMobileChatButtonName(label);
});

When(/^I should see call us option$/, () => {
    supportHubHomePage.shouldDisplayCallUsButton();
});

When(/^I select "(.*)" option in the support hub page$/, (options) => {
    if (options === 'replacement card and pin') {
        supportHubHomePage.selectReplacementCardAndPin();
    } else if (options === 'reset password') {
        supportHubHomePage.selectResetPassword();
    } else if (options === 'change of address') {
        supportHubHomePage.selectChangeOfAddress();
    } else if (options === 'lost and stolen card') {
        supportHubHomePage.selectLostAndStolenCard();
    } else if (options === 'atm and branch') {
        supportHubHomePage.selectATMBranchFinderTile();
    } else if (options === 'provide app feedback') {
        supportHubHomePage.selectProvideAppFeedback();
    } else if (options === 'Suspected fraud') {
        supportHubHomePage.selectSuspectFraud();
    } else {
        throw new Error(`Unknown options: ${options}`);
    }
});

When(/^I validate carousel options on support hub page$/, (options) => {
    const tableData = options.hashes();
    supportHubHomePage.validateCarousels(tableData);
});

When(/^I select the atm and branch finder option from support hub page$/, () => {
    supportHubHomePage.selectATMBranchFinderTile();
});

When(/^I should be displayed an error message in an overlay informing that I do not have an email account set up$/, () => {
    supportHubHomePage.waitForOverlay();
});

When(/^I select to dismiss the overlay$/, () => {
    supportHubHomePage.selectDismissOption();
});

When(/^I should be displayed with email account selection overlay$/, () => {
    supportHubHomePage.waitForEmailAccountSelectionOverlay();
});

When(/^I scroll down to bottom of support hub page$/, () => {
    supportHubHomePage.shouldDisplayCMASurveytile();
});

Then(/^I am navigated to support hub page title$/, () => {
    supportHubHomePage.waitForPageLoad();
});

When(/^I should see and validate please confirm your contact details button$/, () => {
    supportHubHomePage.shouldDisplayUpdateDetailsButton();
    supportHubHomePage.validateUpdateDetailsButton();
});

Then(/^I validate covid-19 and support links under the useful links section$/, (options) => {
    const tableData = options.hashes();
    supportHubHomePage.validateCovid19AndSupportLinks(tableData);
});

When(/^I select Corona virus help & support Hub link$/, () => {
    supportHubHomePage.selectCovidHubLink();
});

Then(/^I should be on covid hub native page$/, () => {
    supportHubHomePage.shouldSeeCovidHubNativePage();
});
