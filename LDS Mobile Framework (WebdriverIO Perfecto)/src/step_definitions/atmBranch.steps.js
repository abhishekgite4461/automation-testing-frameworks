const {When, Then} = require('cucumber');
const AtmBranchPage = require('../pages/atmBranchFinder/atmBranch.page');
const AtmBranchListViewPage = require('../pages/atmBranchFinder/atmBranchListView.page');
const AtmBranchDetailsPage = require('../pages/atmBranchFinder/atmBranchDetailedView.page');
const AtmBranchFilterPage = require('../pages/atmBranchFinder/atmBranchFilter.page');
const DetailedMapViewPage = require('../pages/atmBranchFinder/detailedMapView.page');
const GoogleMapPage = require('../pages/atmBranchFinder/googleMap.page');
const LeaveMobileBankingPopUpPage = require('../pages/atmBranchFinder/leaveMobileBankingPopUp.page');
const AtmBranchMapViewPage = require('../pages/atmBranchFinder/atmBranchMapView.page');
const RequestPermissionPopUpPage = require('../pages/requestPermissionPopUp.page');

const postCode = 'se19eq';

const googleMapPage = new GoogleMapPage();
const atmBranchListViewPage = new AtmBranchListViewPage();
const detailedMapViewPage = new DetailedMapViewPage();
const atmBranchMapViewPage = new AtmBranchMapViewPage();
const atmBranchPage = new AtmBranchPage();
const atmBranchFilterPage = new AtmBranchFilterPage();
const atmBranchDetailsPage = new AtmBranchDetailsPage();
const leaveMobileBankingPage = new LeaveMobileBankingPopUpPage();
const requestPermissionPopUpPage = new RequestPermissionPopUpPage();

When(/^I accept the gps allow pop up message$/, () => {
    requestPermissionPopUpPage.waitForPageLoad();
    requestPermissionPopUpPage.acceptGpsPopup();
});

When(/^I search for a specific location$/, () => {
    atmBranchPage.waitForPageLoad();
    atmBranchPage.searchSpecificLocation();
});

When(/^I select the list view$/, () => {
    atmBranchPage.waitForPageLoad();
    atmBranchPage.selectListViewButton();
});

When(/^I navigate to branch tab and validate in list view$/, () => {
    atmBranchListViewPage.waitForPageLoad();
    atmBranchListViewPage.navigateAndValidateBranchListView();
});

When(/^I navigate to atm tab and validate in list view$/, () => {
    atmBranchListViewPage.waitForPageLoad();
    atmBranchListViewPage.navigateAndValidateAtmListView();
});

When(/^I navigate to both tab and validate in list view$/, () => {
    atmBranchListViewPage.waitForPageLoad();
    atmBranchListViewPage.navigateAndValidateBothListView();
});

When(/^I select the filter button in map$/, () => {
    atmBranchPage.waitForPageLoad();
    atmBranchPage.selectFilterButton();
});

When(/^I make filter in the both tab$/, () => {
    atmBranchFilterPage.waitForPageLoad();
    atmBranchFilterPage.makeFilterInBothTab();
});

When(/^I choose a branch in the list view$/, () => {
    atmBranchListViewPage.waitForPageLoad();
    atmBranchListViewPage.selectFirstBranchInListView();
});

When(/^I validate the options available in the detailed description$/, () => {
    atmBranchDetailsPage.waitForPageLoad();
    atmBranchDetailsPage.validateDetailedOptions();
});

When(/^I enter a postcode in atm branch finder page$/, () => {
    atmBranchPage.waitForPageLoad();
    atmBranchPage.enterPostCode(postCode);
});

When(/^I see the current location in Map$/, () => {
    atmBranchPage.waitForPageLoad();
    atmBranchPage.validateCurrentLocation();
});

When(/^I select the Map view$/, () => {
    atmBranchPage.waitForPageLoad();
    atmBranchPage.clickMapViewButton();
});

When(/^I select the any ATM Branch from the Map$/, () => {
    atmBranchPage.waitForPageLoad();
    atmBranchPage.clickATMBranchInMap();
});

When(/^I select Map view in Detail view page$/, () => {
    atmBranchDetailsPage.waitForPageLoad();
    atmBranchDetailsPage.clickMapViewIcon();
});

When(/^I select Get Direction button in Map view page$/, () => {
    detailedMapViewPage.waitForPageLoad();
    detailedMapViewPage.clickGetDirection();
});

When(/^I select ok button on pop up box in Map view page$/, () => {
    leaveMobileBankingPage.waitForPageLoad();
    leaveMobileBankingPage.clickOkButton();
});

When(/^I select the back button in the Map view page$/, () => {
    detailedMapViewPage.waitForPageLoad();
    detailedMapViewPage.clickBackButton();
});

Then(/^I should be on the Google Map Page$/, () => {
    googleMapPage.waitForPageLoad();
});

When(/^I select the back button in the Google Map Page$/, () => {
    googleMapPage.waitForPageLoad();
    googleMapPage.clickBackButton();
});

When(/^I select the back button in the Detail view page$/, () => {
    atmBranchDetailsPage.waitForPageLoad();
    atmBranchDetailsPage.clickBackButton();
});

When(/^I navigate to branch tab and validate in map view$/, () => {
    atmBranchMapViewPage.waitForPageLoad();
    atmBranchMapViewPage.navigateAndValidateBranchMapView();
});

When(/^I navigate to atm tab and validate in map view$/, () => {
    atmBranchMapViewPage.waitForPageLoad();
    atmBranchMapViewPage.navigateAndValidateAtmMapView();
});

When(/^I navigate to both tab and validate in map view$/, () => {
    atmBranchMapViewPage.waitForPageLoad();
    atmBranchMapViewPage.navigateAndValidateBothMapView();
});
