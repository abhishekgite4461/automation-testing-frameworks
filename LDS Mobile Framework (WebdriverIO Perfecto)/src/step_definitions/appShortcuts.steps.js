const {When, Then} = require('cucumber');
const AppShortcutsPage = require('../pages/appShortcuts.page');

const appShortcutsPage = new AppShortcutsPage();
const Actions = require('../pages/common/actions');

When(/^I long press the app from shortcut/, () => {
    appShortcutsPage.longPressTheApp();
});

Then(/^I should see four shortcuts from the app$/, () => {
    appShortcutsPage.shouldSeeAppShortcuts();
});

Then(/^I should see three shortcuts from the app$/, () => {
    appShortcutsPage.shouldSeethreeAppShortcuts();
});

When(/^I select Branch Finder from shortcut$/, () => {
    appShortcutsPage.selectBranchFinder();
});

When(/^I should be displayed with map screen opened$/, () => {
    appShortcutsPage.shouldSeeBranchFinder();
    Actions.pressAndroidBackKey();
});

When(/^I select our website from preauth shortcut$/, () => {
    appShortcutsPage.selectOurWebsite();
});

When(/^I should be displayed with the website opened$/, () => {
    appShortcutsPage.shouldSeeOurWebsite();
});

When(/^I select contact us from shortcut$/, () => {
    appShortcutsPage.selectContactUs();
});

When(/^I select Pay and Transfer from shortcut$/, () => {
    appShortcutsPage.selectPayAndTransfer();
});

When(/^I select Manage cards from shortcut$/, () => {
    appShortcutsPage.selectManageCards();
});

When(/^I should be displayed with the Support page opened$/, () => {
    appShortcutsPage.shouldSeeContactUs();
    Actions.pressAndroidBackKey();
});

When(/^I should be displayed with the PayAndTransfer page opened$/, () => {
    appShortcutsPage.shouldSeePayAndTransfer();
    Actions.pressAndroidBackKey();
    Actions.pressAndroidBackKey();
});

When(/^I should be displayed with the ManageCards page opened$/, () => {
    appShortcutsPage.shouldSeeCardManagement();
    Actions.pressAndroidBackKey();
    Actions.pressAndroidBackKey();
});

When(/^I set the location as (.*) which is outsite UK in device$/, (address) => {
    Actions.setLocation(address);
});

When(/^I reset back the original location of the device$/, () => {
    Actions.resetDeviceLocation();
});
