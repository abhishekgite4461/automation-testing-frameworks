const {Then, When} = require('cucumber');
require('chai').should();

const ConfirmationOfPayeePage = require('../pages/transferAndPayments/confirmationOfPayee.page');
const CommonPage = require('../pages/common.page');
const UkBeneficiaryDetailPage = require('../pages/transferAndPayments/ukBeneficiaryDetail.page.js');
const SharePreviewPage = require('../pages/paymentHub/sharePreview.page');

const ukBeneficiaryDetailPage = new UkBeneficiaryDetailPage();
const commonPage = new CommonPage();
const confirmationOfPayeePage = new ConfirmationOfPayeePage();
const sharePreviewPage = new SharePreviewPage();

Then(/^I should see Payee matches business account message (without|with) the (.*) playback$/, (type, recipientName) => {
    confirmationOfPayeePage.waitForCOPResponsePage();
    confirmationOfPayeePage.verifyBAMMNamePlayBack(type, recipientName);
});

Then(/^I see the options to change Details or Continue$/, () => {
    confirmationOfPayeePage.shouldSeeCOPDetailsEditOption();
    confirmationOfPayeePage.shouldSeeCOPDetailsContinueOption();
});

Then(/^I should see Account name (still not|not) match message$/, (type) => {
    confirmationOfPayeePage.verifyAccountNameNotMatchMessage(type);
});

Then(/^I should see account number invalid message in a red banner on the payment hub page$/, () => {
    ukBeneficiaryDetailPage.shouldSeeInvalidAccountNumberErrorMessage();
});

When(/^I dismiss the error message on the payment hub$/, () => {
    ukBeneficiaryDetailPage.closeErrorMessage();
});

When(/^I continue to payment hub page from COP response page$/, () => {
    confirmationOfPayeePage.selectCOPDetailsContinueOption();
});

Then(/^I verify bank name tool tip accordingly for each sort code$/, (sortCodeValidationTable) => {
    const bankDetails = sortCodeValidationTable.hashes();
    confirmationOfPayeePage.verifyToolTipWithSortCode(bankDetails);
});

When(/^I should see Please Wait Spinner$/, () => {
    confirmationOfPayeePage.shouldSeePleaseWaitSpinner();
});

When(/^I should see Checking Details Spinner with relevant bank name$/, () => {
    confirmationOfPayeePage.shouldSeeCheckingDetailsSpinner();
});

Then(/^I am shown Payee matches message$/, () => {
    confirmationOfPayeePage.shouldSeeAccountNameMatch();
});

Then(/^I am shown Payee matches (personal|business) account with (.*)$/, (type, recipientName) => {
    if (type === 'personal') {
        confirmationOfPayeePage.shouldSeePersonalDetailsMatch(recipientName);
    } else {
        confirmationOfPayeePage.shouldSeeBusinessDetailsMatch(recipientName);
    }
});

Then(/^I am shown Payee does not fully match$/, () => {
    confirmationOfPayeePage.shouldSeePartialMatch();
});

Then(/^I am shown a full screen error page$/, () => {
    confirmationOfPayeePage.shouldSeeFullError();
});

Then(/^I select the back button in the COP response page$/, () => {
    commonPage.selectHeaderBackButton();
});

Then(/^I should see the COP share details option$/, () => {
    confirmationOfPayeePage.shouldSeeCOPShareDetails();
});

When(/^I select the COP Share details option$/, () => {
    confirmationOfPayeePage.selectCOPShareDetailsOption();
});

Then(/^I validate share details options on COP response page and navigate back$/, () => {
    confirmationOfPayeePage.selectCOPShareDetailsOption();
    sharePreviewPage.shouldSeeNativeSharePanel();
    sharePreviewPage.shouldSeeEmailAppIcon();
    sharePreviewPage.selectEmailApp();
    sharePreviewPage.selectCancelButton();
});
