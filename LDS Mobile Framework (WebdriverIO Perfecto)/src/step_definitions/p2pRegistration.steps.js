const {When} = require('cucumber');
const PayUkNumberPopupPage = require('../pages/p2pRegistration/payUkNumberPopup.page');
const PayContactRegistrationPage = require('../pages/p2pRegistration/payContactRegistration.page');
const ConfirmPayAContactPage = require('../pages/p2pRegistration/confirmPayAContact.page');

const payUkNumberPopupPage = new PayUkNumberPopupPage();
const payContactRegistrationPage = new PayContactRegistrationPage();
const confirmPayAContactPage = new ConfirmPayAContactPage();

When(/^I should see a pop for pay a contact registration$/, () => {
    payUkNumberPopupPage.waitForPageLoad();
});

When(/^I select register now button on the popup in the p2p page$/, () => {
    payUkNumberPopupPage.clickRegisterNow();
});

When(/^I should be on pay a contact registration page$/, () => {
    payContactRegistrationPage.waitForPageLoad();
});

When(/^I select on register from pay a contact page$/, () => {
    payContactRegistrationPage.selectRegisterPayAContact();
});

When(/^I should see confirm mobile number page$/, () => {
    confirmPayAContactPage.waitForPageLoad();
});

When(/^I select De-register from Pay a Contact settings page$/, () => {
    payContactRegistrationPage.selectDeRegisterPayAContact();
});

When(/^I should be on Successful De-register Mobile number Page$/, () => {
    payContactRegistrationPage.verifyDeRegisterSuccessMessage();
});
