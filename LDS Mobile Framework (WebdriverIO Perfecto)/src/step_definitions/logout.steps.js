const {When, Then} = require('cucumber');

const LogoutPage = require('../pages/logout.page');

const logoutPage = new LogoutPage();

When(/^I navigate back to the login screen$/, () => {
    logoutPage.waitForPageLoad();
    logoutPage.navigateToLogin();
});

Then(/^I should be on the logout page$/, () => {
    logoutPage.waitForPageLoad();
});

Then(/^I should see the NPS survey button on the logout page$/, () => {
    logoutPage.waitForPageLoad();
    logoutPage.shouldSeeSurveyButton();
});

Then(/^I select the contact us option from the logoff page pre auth header$/, () => {
    logoutPage.selectContactUs();
});

When(/^I should see the cms tile in the logout page$/, () => {
    logoutPage.waitForPageLoad();
    logoutPage.shouldSeeCmsTile();
});

When(/^I should see the back to logon button on the logout page$/, () => {
    logoutPage.waitForPageLoad();
    logoutPage.shouldSeeLoginButton();
});

When(/^I should (not see|see) the FSCS tile in the logout page$/, (type) => {
    logoutPage.waitForPageLoad();
    if (type === 'not see') {
        logoutPage.verifyFscsTile(false);
    } else {
        logoutPage.verifyFscsTile(true);
    }
});

When(/^I select back to logon button on the logout page$/, () => {
    logoutPage.waitForPageLoad();
    logoutPage.clickBackToLogonButton();
});

When(/^I should see an App Banning message$/, () => {
    logoutPage.waitForPageLoad();
    logoutPage.verifyAppBanningMessage();
});

When(/^I should be displayed a log-off complete message - HC030$/, () => {
    logoutPage.waitForPageLoad();
    logoutPage.verifyLogoutCompleteMessage();
});

Then(/^I select see contact us option from the logoff page pre auth header$/, () => {
    logoutPage.selectContactUs();
});

Then(/^I should be on the logout page with reset app complete message$/, () => {
    logoutPage.waitForPageLoad();
    logoutPage.verifyAppResetCompleteMessage();
});
