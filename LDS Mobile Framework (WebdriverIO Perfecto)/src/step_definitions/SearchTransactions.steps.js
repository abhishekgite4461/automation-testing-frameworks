const {When, Then} = require('cucumber');
const SearchTransactionsPage = require('../pages/statements/searchTransactions.page.js');
const AccountStatementsPage = require('../pages/statements/accountStatement.page.js');

const searchTransactionsPage = new SearchTransactionsPage();
const accountStatementPage = new AccountStatementsPage();

Then(/^I should be displayed search page with below options$/, (searchFields) => {
    const FieldsToValidate = searchFields.hashes();
    searchTransactionsPage.validateSearchPageFields(FieldsToValidate);
});

When(/^I enter the (.*) in search dialog box$/, (keyword) => {
    searchTransactionsPage.enterKeywordInSearchBox(keyword);
});

Then(/^I should be displayed matching transactions with that (.*) in the search list$/, (keyword) => {
    searchTransactionsPage.validateSearchResults(keyword);
});

Then(/^I should be displayed clear option$/, () => {
    searchTransactionsPage.validateClearOption();
});

When(/^I select search$/, () => {
    searchTransactionsPage.tapOnSearchBox();
});

When(/^I search with (.*) in search dialog box$/, (keyword) => {
    accountStatementPage.clickSearchIcon();
    searchTransactionsPage.enterKeywordInSearchBox(keyword);
});

When(/^I clear the search results$/, () => {
    searchTransactionsPage.clearSearchResults();
});

Then(/^I should be displayed search more transactions option$/, () => {
    searchTransactionsPage.validateSearchFurtherBack();
});

When(/^I tap on search more transactions$/, () => {
    searchTransactionsPage.clickSearchFurtherBack();
});

Then(/^I should be displayed further more transactions available with the same (.*) in the results$/, (keyword) => {
    searchTransactionsPage.validateSearchResults(keyword);
});

Then(/^The results list should be reset$/, () => {
    searchTransactionsPage.validateSearchResultsCleared();
});

Then(/^The search word (.*) entered should be cleared$/, (keyword) => {
    searchTransactionsPage.validateSearchWordIsCleared(keyword);
});

Then(/^I am displayed a message that there are no more transactions$/, () => {
    searchTransactionsPage.validateNoMoreTransactionsMessage();
});

Then(/^I should not be displayed search more transactions option$/, () => {
    searchTransactionsPage.validateNoSearchFurtherBack();
});

When(/^I tap on search more transactions till all the transactions are loaded$/, () => {
    searchTransactionsPage.searchAllTransactions();
});

Then(/^I should be displayed no transaction message$/, () => {
    searchTransactionsPage.validateNoTransactionsToSearch();
});

Then(/^I should not be displayed any search results$/, () => {
    searchTransactionsPage.validateNoSearchResults();
});

When(/^I scroll to the bottom of the page$/, () => {
    searchTransactionsPage.swipeDownInSearchPage();
});

Then(/^I should be displayed all the transactions available with the same word (.*) in the results$/, (keyword) => {
    searchTransactionsPage.validateSearchResults(keyword);
});

Then(/^I should be displayed with a message no results have been found in this date range$/, () => {
    searchTransactionsPage.validateZeroResultsInDateRangeMessage();
});

Then(/^I should be on the search page with transactions$/, () => {
    searchTransactionsPage.validateSearchPageDisplayed();
});

Then(/^I am displayed an error message with retry option and red banner$/, () => {
    accountStatementPage.validateErrorMessage();
});
