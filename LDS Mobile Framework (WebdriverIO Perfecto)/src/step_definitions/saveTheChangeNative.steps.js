const {Given, When} = require('cucumber');
const SaveTheChangeNativePage = require('../pages/saveTheChangeNative.page');

const saveTheChangeNativePage = new SaveTheChangeNativePage();

When(/^I (have|have not) set up Save the Change before on the eligible account$/, (type) => {
    saveTheChangeNativePage.verifySaveTheChangeSetUp(type);
});

When(/^I am on Save the Change Homepage with the ability to swipe through all 4 instructional pages$/, () => {
    saveTheChangeNativePage.shouldSwipeThroughSTCInfoPages(3);
});

Given(/^I select the option Set up Save the Change$/, () => {
    saveTheChangeNativePage.selectSetUpSTCOption();
});

Given(/^I select turn OFF save the change option$/, () => {
    saveTheChangeNativePage.selectTurnOFFSTC();
});

Given(/^I accept terms and conditions and select Continue on the review screen$/, () => {
    saveTheChangeNativePage.selectAcceptTCandTurnONSTC();//
});

Given(/^I am shown a Success screen$/, () => {
    saveTheChangeNativePage.shouldSeeSTCSuccessScreen();//
});

Given(/^I have the option to go back to accounts$/, () => {
    saveTheChangeNativePage.shouldSeeBackToAccounts();//
});

Given(/^I select the option to go back to accounts$/, () => {
    saveTheChangeNativePage.selectBackToAccounts();//
});

Given(/^I should see the save the change turn OFF success screen$/, () => {
    saveTheChangeNativePage.shouldSeeSTCOFF();//
});

Given(/^I select an eligible recipient saving account$/, () => {
    saveTheChangeNativePage.selectSavingAccount();
});

Given(/^I am shown Manage STC and option to go back to AOV$/, () => {
    saveTheChangeNativePage.shouldSeeAlreadyRegistered();
});

Given(/^I am shown the option to Open a new Saving account$/, () => {
    saveTheChangeNativePage.shouldSeeOpenNewSaving();
});

Given(/^I select Open New Saving account$/, () => {
    saveTheChangeNativePage.selectOpenNewSaving();
});
