/* eslint import/no-dynamic-require: "off" */
const {Given, When} = require('cucumber');
const EnrolmentTask = require('../tasks/enrolment.task');
const BusinessSelectionPage = require('../pages/businessSelection.page');
const HomePage = require('../pages/home.page');
const CallUsHomePage = require('../pages/helpAndInfo/callUsHomePage.page');

const homePage = new HomePage();
const businessSelectionPage = new BusinessSelectionPage();
const callUsHomePage = new CallUsHomePage();
const DeviceHelper = require('../utils/device.helper');
const DeviceState = require('../utils/deviceState');

const tasks = {
    enrolment: (world) => (
        new EnrolmentTask(
            world.dataRepository,
            world.appConfigurationRepository,
            world.stubRepository,
            world.switchRepository,
            world.featureRepository,
            world.abTestRepository,
            world.dataConsentRepository,
            new DeviceState(DeviceHelper.deviceId())
        )
    )
};

// TODO: This should be same as "Given I am logged in as a (.*) user" above
Given(/^I am an enrolled "(.*)" user logged into the app$/, function (type) {
    tasks.enrolment(this).loginAsUserWithAccountTypes(type);
});

// TODO: This should be the only step definition available for logging in.
Given(/^I am a (.*) user on the home page$/, function (type) {
    tasks.enrolment(this).loginAsUserWithAccountTypes(type);
});

Given(/^I am on MI screen as (.*) business user with (.*) account$/, function (businessType, accountType) {
    tasks.enrolment(this)
        .proceedToMIWithAccountTypes(accountType, null, businessType);
});

Given(/^I am on MI screen as (.*) business user with (.*) account and (.*) access$/, function (businessType, accountType, access) {
    tasks.enrolment(this)
        .proceedToMIWithAccountTypes(accountType, access, businessType);
});

Given(/^I am on MI screen as (.*) access user with (.*) account$/, function (access, accountType) {
    tasks.enrolment(this)
        .proceedToMIWithAccountTypes(accountType, access);
});

Given(/^I am on home screen as (.*) business user with (.*) account and (.*) access$/, function (businessType, accountType, access) {
    tasks.enrolment(this)
        .loginAsUserWithAccountTypes(accountType, access, businessType);
    if (businessType === 'MULTIPLE_BUSINESS') {
        businessSelectionPage.selectBusinessWithinAccessLevel(access);
    }
    homePage.validateHomePage();
});

Given(/^I am on home screen as (.*?) access user with (.*?) account$/, function (access, accountType) {
    tasks.enrolment(this)
        .loginAsUserWithAccountTypes(accountType, access);
    businessSelectionPage.selectBusinessWithinAccessLevel(access);
    homePage.validateHomePage();
});

Given(/^I am on home screen as (.*?) access user with (.*?) account in (.*?) business$/, function (access, accountType, business) {
    tasks.enrolment(this)
        .loginAsUserWithAccountTypes(accountType, access);
    businessSelectionPage.selectBusinessWithinAccessLevel(access, business);
    homePage.validateHomePage();
});

// Used for multiple business
Given(/^I am on home screen as (.*?) access user with (.*?) account in (.*?) business$/, function (access, accountType, business) {
    tasks.enrolment(this)
        .loginAsUserWithAccountTypes(accountType, access);
    businessSelectionPage.selectBusinessWithinAccessLevel(access, business);
});

Given(/^I login to app as a "(.*)" user$/, function (accountType) {
    tasks.enrolment(this)
        .loginAsUserWithAccountTypes(accountType);
});

Given(/^I am an unenrolled (.*) user on the home page$/, function (type) {
    tasks.enrolment(this)
        .loginAsUserWithAccountTypes(type);
});

Given(/^I am a (.*) user on the MI page$/, function (type) {
    tasks.enrolment(this)
        .proceedToMIWithAccountTypes(type);
});

Given(/^I am an enrolled "(.*)" user on the MI page$/, function (type) {
    tasks.enrolment(this)
        .proceedToMIWithAccountTypes(type);
});

Given(/^I submit "(.*)" MI as "(.*)" user on the MI page$/, function (miType, type) {
    tasks.enrolment(this)
        .proceedToMIWithAccountTypes(type);
    tasks.enrolment(this).enterMI(miType);
});

Given(/^I submit username and password on login screen as (.*) user$/, function (type) {
    tasks.enrolment(this)
        .proceedToLoginWithAccountTypes(type);
});

When(/^I enter (correct|incorrect|2 characters of) MI$/, tasks.enrolment(this).enterMI);

When(/^I login into the app from memorable information page/, () => {
    tasks.enrolment(this).enterMI('correct');
    homePage.validateHomePage();
});

When(/^I relaunch the app and login as a "(.*)" user$/, (type) => {
    DeviceHelper.relaunchApp(true);
    tasks.enrolment(this).loginAsUserWithAccountTypes(type);
});

Given(/^I am an enrolled (.*) user on the call us home page$/, (type) => {
    tasks.enrolment(this).loginAsUserWithAccountTypes(type);
    homePage.selectTab('support');
    callUsHomePage.waitForPageLoad();
});
