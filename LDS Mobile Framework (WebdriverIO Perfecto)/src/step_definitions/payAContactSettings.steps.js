const {Then} = require('cucumber');
const PayAContactSettingsPage = require('../pages/payAContactSettings.page');

const payAContactSettingsPage = new PayAContactSettingsPage();

Then(/^I should be on the pay a contact setting webview page$/, () => {
    payAContactSettingsPage.waitForPageLoad();
});
