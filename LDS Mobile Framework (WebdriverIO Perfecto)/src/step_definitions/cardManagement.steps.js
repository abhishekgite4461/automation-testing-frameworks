const {When} = require('cucumber');

const CardManagementPage = require('../pages/cardManagement.page');

const cardManagementPage = new CardManagementPage();

When(/^I should be on card management page$/, () => {
    cardManagementPage.waitForPageLoad();
});

When(/^I should see below options on card management page$/, (table) => {
    const tableData = table.hashes();
    cardManagementPage.validateCardManagementOptions(tableData);
});

When(/^I select "(.*)" option in card management page$/, (cardManagementOption) => {
    if (cardManagementOption === 'Lost and stolen cards') {
        cardManagementPage.selectLostAndStolenCards();
    } else if (cardManagementOption === 'replacement card and pin') {
        cardManagementPage.selectReplacementCardAndPin();
    } else if (cardManagementOption === 'Freeze card transactions') {
        cardManagementPage.selectFreezCardTransactions();
    } else {
        throw new Error(`Unknown options: ${cardManagementOption}`);
    }
});
