const {Given, When, Then} = require('cucumber');
const SettingsPage = require('../pages/settings/settings.page');
const ResetPage = require('../pages/settings/reset.page');
const PersonalDetailsPage = require('../pages/settings/personalDetails/personalDetails.page');
const ChangePhoneNumbersPage = require('../pages/settings/personalDetails/changePhoneNumbers.page.js');
const ChangeEmailPage = require('../pages/settings/personalDetails/changeEmail.page');
const ConfirmAuthenticationPasswordPage = require('../pages/confirmAuthenticationPassword.page');
const ConfirmDetailsPage = require('../pages/settings/personalDetails/confirmDetails.page');
const ConfirmNumberPage = require('../pages/settings/personalDetails/confirmNumber.page');
const EditMobilePage = require('../pages/settings/personalDetails/editMobile.page');
const BusinessDetailsPage = require('../pages/settings/businessDetails.page');
const AutoLogOffSettingsPage = require('../pages/settings/autoLogOffSettings.page');
const SecuritySettingsPage = require('../pages/settings/securitySettings.page');
const ResetYourPasswordPage = require('../pages/settings/resetYourPassword.page');
const ResetConfirmationPopUpPage = require('../pages/settings/resetConfirmationPopUp.page');
const ResetPasswordConfirmationPopUpPage = require('../pages/settings/resetPasswordConfirmationPopUp.page');
const LogOffAlertPopUpPage = require('../pages/settings/logOffAlertPopUp.page');
const LogoutPage = require('../pages/logout.page');
const LoginPage = require('../pages/login.page');
const MarketingPreferencesHubPage = require('../pages/settings/personalDetails/marketingPreferencesHub.page');
const NavigationTask = require('../tasks/navigation.task');
const MorePage = require('../pages/more.page');

const newPassword = `Pass${Math.floor(Math.random() * 1000)}word`;
const validEmailAddress = `${Math.random()
    .toString(36)
    .substring(22)}@g.com`;
const personalMobileNumber = `${Math.floor(100000000 + Math.random() * 900000000)}`;
const homeMobileNumber = `${Math.floor(100000000 + Math.random() * 900000000)}`;
const workMobileNumber = `${Math.floor(100000000 + Math.random() * 900000000)}`;
const incorrectPassword = 'garbage2345';

const resetYourPasswordPage = new ResetYourPasswordPage();
const settingsPage = new SettingsPage();
const confirmDetailsPage = new ConfirmDetailsPage();
const resetPasswordPopUpConfirmationPage = new ResetPasswordConfirmationPopUpPage();
const personalDetailsPage = new PersonalDetailsPage();
const changeEmailPage = new ChangeEmailPage();
const confirmNumberPage = new ConfirmNumberPage();
const editMobilePage = new EditMobilePage();
const confirmAuthenticationPasswordPage = new ConfirmAuthenticationPasswordPage();
const morePage = new MorePage();

const changePhoneNumbersPage = new ChangePhoneNumbersPage();
const securitySettingsPage = new SecuritySettingsPage();
const logOffAlertPopUpPage = new LogOffAlertPopUpPage();
const autoLogOffSettingsPage = new AutoLogOffSettingsPage();
const resetPage = new ResetPage();
const logoutPage = new LogoutPage();
const loginPage = new LoginPage();
const businessDetailsPage = new BusinessDetailsPage();
const resetConfirmationPopUpPage = new ResetConfirmationPopUpPage();
const marketingPreferencesHubPage = new MarketingPreferencesHubPage();

When(/^I choose marketing preferences hub via more menu$/, () => {
    personalDetailsPage.selectPrimaryMarketingPreferencesLink();
    marketingPreferencesHubPage.waitForMarketingPreferenceHubPageLoad();
});

Then(/^I am on the (primary|secondary) marketing consents page for a co-serviced account$/, (link) => {
    personalDetailsPage.selectPrimaryOrSecondaryMarketingPreferencesLink(link);
    marketingPreferencesHubPage.isCoServicingPageVisible();
});

When(/^I select the security settings option from the settings page$/, () => {
    settingsPage.waitForPageLoad();
    settingsPage.selectSecuritySettings();
});

When(/^I select the reset app option from the settings page$/, () => {
    settingsPage.waitForPageLoad();
    settingsPage.navigateToReset();
});

When(/^I successfully reset the app$/, () => {
    resetPage.waitForPageLoad();
    resetPage.selectResetAppButton();
    resetConfirmationPopUpPage.waitForPageLoad();
    resetConfirmationPopUpPage.selectContinueButton();
});

const personalDetails = () => {
    settingsPage.waitForPageLoad();
    settingsPage.selectPersonalDetails();
};

// Personal Details section
When(/^I select your personal details option in settings page$/, personalDetails);

When(/^I enter my new email address in personal details page$/, () => {
    NavigationTask.openYourProfileFromMoreMenu();
    personalDetailsPage.waitForPageLoad();
    personalDetailsPage.isPageVisible();
    personalDetailsPage.waitForPageLoad();
    personalDetailsPage.selectEmailAddressDetails();
    changeEmailPage.waitForPageLoad();
    changeEmailPage.enterEmailAddress(validEmailAddress);
});

When(/^I confirm my new email address in personal details page$/, () => {
    changeEmailPage.waitForPageLoad();
    changeEmailPage.confirmEmailAddress(validEmailAddress);
});

When(/^I should see the updated email address$/, () => {
    personalDetailsPage.waitForPageLoad();
    personalDetailsPage.confirmUpdatedEmailAddress(validEmailAddress);
});

When(/^I verify Personal details link on settings page$/, () => {
    settingsPage.waitForPageLoad();
    settingsPage.verifyPersonalDetailsIsVisible();
});

When(/^I select mobile number module$/, () => {
    personalDetailsPage.waitForPageLoad();
    personalDetailsPage.selectMobileDetails();
});

When(/^I enter my new work mobile number in personal details page$/, () => {
    changePhoneNumbersPage.waitForPageLoad();
    changePhoneNumbersPage.changeMobileNumber(personalMobileNumber);
});
When(/^I enter my new home mobile number in personal details page$/, () => {
    changePhoneNumbersPage.waitForPageLoad();
    changePhoneNumbersPage.changeHomeNumber(homeMobileNumber);
});
When(/^I enter my new personal mobile number in personal details page$/, () => {
    changePhoneNumbersPage.waitForPageLoad();
    changePhoneNumbersPage.changeMobileNumber(personalMobileNumber);
});
When(/^I enter my new home mobile number in personal details page$/, () => {
    changePhoneNumbersPage.waitForPageLoad();
    changePhoneNumbersPage.changeHomeNumber(homeMobileNumber);
});
When(/^I enter my new personal mobile number in personal details page$/, () => {
    changePhoneNumbersPage.waitForPageLoad();
    changePhoneNumbersPage.changeWorkNumber(workMobileNumber);
});

When(/^I select confirm button in the personal Detail Page$/, () => {
    changePhoneNumbersPage.waitForPageLoad();
    changePhoneNumbersPage.clickContinueButton();
});

When(/^I select ok button in the personal Detail Page$/, () => {
    confirmNumberPage.waitForPageLoad();
    confirmNumberPage.selectOkButton();
});

When(/^I select back button in the your personal details page$/, () => {
    personalDetailsPage.waitForPageLoad();
    personalDetailsPage.clickBackButton();
});

When(/^I should see the updated mobile number in the personal details page$/, () => {
    editMobilePage.waitForPageLoad();
    editMobilePage.validateOption(personalMobileNumber, homeMobileNumber, workMobileNumber);
});

Then(/^I should be shown my name, user id, phone numbers, address details, email address$/, () => {
    personalDetailsPage.waitForPageLoad();
    personalDetailsPage.shouldSeePersonalDetails();
});
// End Personal Details sec

When(/^I confirm the correct authentication password$/, function () {
    const data = this.dataRepository.get();
    confirmAuthenticationPasswordPage.waitForPageLoad();
    confirmAuthenticationPasswordPage.enterPasswordAndConfirm(data.password);
});

When(/^I verify Security settings link on settings page$/, () => {
    settingsPage.waitForPageLoad();
    settingsPage.verifySecuritySettingsIsVisible();
});

When(/^I select on Security settings link on settings page$/, () => {
    settingsPage.selectSecuritySettings();
});

When(/^I verify Pay a Contact link on settings page$/, () => {
    settingsPage.waitForPageLoad();
    settingsPage.verifyPayAContactIsVisible();
});

When(/^I verify Going abroad link on settings page$/, () => {
    settingsPage.waitForPageLoad();
    settingsPage.verifyGoingAbroadIsVisible();
});

When(/^I select the Going Abroad option from settings menu$/, () => {
    settingsPage.waitForPageLoad();
    settingsPage.selectGoingAbroad();
});

When(/^I click on the back button in the settings menu$/, () => {
    settingsPage.waitForPageLoad();
    settingsPage.clickBackButton();
});

Then(/^I should see the landing page$/, () => {
    confirmDetailsPage.waitForPageLoad();
});

When(/^I select back to contact details in the landing page$/, () => {
    confirmDetailsPage.selectConfirmContact();
});

Then(/^I should see the below options in the settings page$/, (table) => {
    const tableData = table.hashes();
    settingsPage.waitForPageLoad();
    settingsPage.validateOptionsInTheSettingsPage(tableData);
});

When(/^I select the your Business Details option in settings page$/, () => {
    settingsPage.waitForPageLoad();
    settingsPage.selectBusinessDetails();
});

When(/^I should be on the your Business Details page$/, () => {
    businessDetailsPage.waitForPageLoad();
});

When(/^I click on the back button in the your Business details page$/, () => {
    businessDetailsPage.waitForPageLoad();
    businessDetailsPage.clickBackButton();
});

When(/^I select the Your Security Details option in settings page$/, () => {
    settingsPage.waitForPageLoad();
    settingsPage.selectSecuritySettings();
});

When(/^I select Auto logoff option in the your Security Details page$/, () => {
    securitySettingsPage.waitForPageLoad();
    securitySettingsPage.selectAutoLogOffOption();
});

When(/^I choose my inactivity time in auto logout settings$/, () => {
    autoLogOffSettingsPage.waitForPageLoad();
    autoLogOffSettingsPage.selectInactivityTimeTenMinutes();
});

Then(/^I should be on the settings page$/, () => {
    settingsPage.waitForPageLoad();
});

When(/^I select confirm button in auto logout settings page$/, () => {
    autoLogOffSettingsPage.waitForPageLoad();
    autoLogOffSettingsPage.clickConfirmButton();
});

Then(/^I should be on the autoLog off settings page$/, () => {
    autoLogOffSettingsPage.waitForPageLoad();
});

Then(/^I must see the confirm button disabled in auto logout settings page$/, () => {
    autoLogOffSettingsPage.verifyConfirmButtonIsNotEnabled();
});

Then(/^I should see the below options in the security settings page$/, (options) => {
    const securitySettingsOptions = options.hashes();
    securitySettingsPage.waitForPageLoad();
    securitySettingsPage.validateOptionsInTheSecuritySettingsPage(securitySettingsOptions);
});

Then(/^I select the autoLog off settings option from the security settings page$/, () => {
    securitySettingsPage.waitForPageLoad();
    securitySettingsPage.selectAutoLogOffOption();
});

When(/^I should see the security settings page/, () => {
    securitySettingsPage.waitForPageLoad();
});

Then(/^I should see the following options in security details page$/, (details) => {
    const securityDetailOptions = details.hashes();
    securitySettingsPage.validateOptionsInTheSecurityDetailsPage(securityDetailOptions);
});

When(/^I click on the reset your password option$/, () => {
    securitySettingsPage.waitForPageLoad();
    securitySettingsPage.resetYourPassword();
});

When(/^I should see the reset your password page$/, () => {
    resetYourPasswordPage.waitForPageLoad();
});

const enterPasswordDetails = (password) => {
    resetYourPasswordPage.waitForPageLoad();
    resetYourPasswordPage.enterNewPassword(password);
    resetYourPasswordPage.confirmNewPassword(password);
};

When(/^I enter my new valid password in the reset password page$/, () => {
    enterPasswordDetails(newPassword);
});

When(/^I enter my valid (personal name|birthday) detail in the reset password page$/, function (type) {
    const obtainedData = this.dataRepository.get();
    const password = (type === 'personal name') ? `${obtainedData.data.customerName}123` : `${obtainedData.data.dob}test`;
    enterPasswordDetails(password);
});

When(/^I enter new password which I have used in last five instances$/, function () {
    const data = this.dataRepository.get();
    resetYourPasswordPage.enterNewPassword(data.password);
    resetYourPasswordPage.confirmNewPassword(data.password);
});

When(/^I select the submit button in the reset password page$/, () => {
    resetYourPasswordPage.clickSubmitButton();
});

When(/^I should see an non-identical error message in banner on the reset password page$/, () => {
    resetYourPasswordPage.verifyNonIdenticalErrorMessage();
});

When(/^I should see an personal Information error message in banner on the reset password page$/, () => {
    resetYourPasswordPage.verifyNoPersonalDetailsErrorMessage();
});

When(/^I should see an error message MG-3419 in banner on the reset password page$/, () => {
    resetYourPasswordPage.verifyEasyToGuessPasswordErrorMessage();
});

When(/^I should see an error message MG-515 in banner on the reset password page$/, () => {
    resetYourPasswordPage.verifyLastFivePasswordErrorMessage();
});

When(/^I should see not secure enough error message in banner on the reset password page$/, () => {
    resetYourPasswordPage.verifyNonSecureErrorMessage();
});

When(/^I should see your password has been reset popup message$/, () => {
    resetYourPasswordPage.waitForPageLoad();
    resetYourPasswordPage.resetPasswordSuccess();
});

When(/^I click on password log off button$/, () => {
    resetYourPasswordPage.waitForPageLoad();
    resetYourPasswordPage.clickPasswordLogOff();
});

Then(/^I set my new password as (.*)$/, (personalInfo) => {
    resetYourPasswordPage.waitForPageLoad();
    resetYourPasswordPage.enterNewPassword(personalInfo);
    resetYourPasswordPage.confirmNewPassword(personalInfo);
});

Then(/^I verify the app details in the security settings page$/, function () {
    securitySettingsPage.waitForPageLoad();
    securitySettingsPage.verifyAppDetailsInTheSecuritySettingsPage(this.dataRepository.get().username);
});

const selectForgotPassword = () => {
    securitySettingsPage.waitForPageLoad();
    securitySettingsPage.clickForgottenPasswordOption();
};

Then(/^I select the forgotten password option from the security settings page$/, selectForgotPassword);

const waitForResetPassword = () => {
    resetYourPasswordPage.waitForPageLoad();
};

Then(/^I should be on the password reset page$/, waitForResetPassword);

Then(/^I select reset app button from the reset app page$/, () => {
    resetPage.waitForPageLoad();
    resetPage.selectResetAppButton();
});

Then(/^I select (confirm|cancel) button from the reset app confirmation pop up$/, (type) => {
    if (type === 'confirm') {
        resetConfirmationPopUpPage.waitForPageLoad();
        resetConfirmationPopUpPage.selectContinueButton();
    } else {
        resetConfirmationPopUpPage.waitForPageLoad();
        resetConfirmationPopUpPage.selectCancelButton();
    }
});

Then(/^I should be on the reset app Page$/, () => {
    resetPage.waitForPageLoad();
});

Then(/^I enter two non identical password in the reset password page$/, () => {
    resetYourPasswordPage.waitForPageLoad();
    resetYourPasswordPage.enterNewPassword('banking123');
    resetYourPasswordPage.confirmNewPassword('bank12345');
});

Then(/^I should see a non-identical error message on the reset password page$/, () => {
    resetYourPasswordPage.verifyNonIdenticalErrorMessage();
});

Then(/^I select the desired "(.*)" from the auto log off page$/, (autoLogOffTime) => {
    autoLogOffSettingsPage.waitForPageLoad();
    autoLogOffSettingsPage.clickAutoLogOffTimer(autoLogOffTime);
});

Then(/^I enter a non secure password (.*)$/, (password) => {
    resetYourPasswordPage.waitForPageLoad();
    resetYourPasswordPage.enterNewPassword(password);
    resetYourPasswordPage.confirmNewPassword(password);
});

Then(/^I should see a password updated pop up in the reset password page$/, () => {
    resetPasswordPopUpConfirmationPage.waitForResetPasswordConfirmationPopUpPageLoad();
    resetPasswordPopUpConfirmationPage.clickOkAndLogOffButton();
});

Then(/^I should not see a password updated pop up in the reset password page$/, () => {
    resetPasswordPopUpConfirmationPage.resetPasswordConfirmationPopUpIsNotVisible();
});

Then(/^I should see a no personal details error banner on the reset password page$/, () => {
    resetYourPasswordPage.verifyNoPersonalDetailsErrorMessage();
});

Then(/^I should see a non-secure error banner on the reset password page$/, () => {
    resetYourPasswordPage.verifyNonSecureErrorMessage();
});

Then(/^I should see a last 5 password error banner on the reset password page$/, () => {
    resetYourPasswordPage.verifyLastFivePasswordErrorMessage();
});

Then(/^I should see an easy-to-guess passwords error banner on the reset password page$/, () => {
    resetYourPasswordPage.verifyEasyToGuessPasswordErrorMessage();
});

Then(/^I should (not see|see) the password security tips$/, (type) => {
    if (type === 'not see') {
        resetYourPasswordPage.verifyPasswordSecurityTips(false);
    } else {
        resetYourPasswordPage.verifyPasswordSecurityTips(true);
    }
});

Then(/^I (select|close) the tips for a good password accordion from the reset password page$/, (type) => {
    if (type === 'select') {
        resetYourPasswordPage.selectTipsForPasswordAccordion();
    } else {
        resetYourPasswordPage.selectTipsForPasswordAccordion();
        resetYourPasswordPage.selectCloseButton();
    }
});

Then(/^I am displayed reset password screen in the app$/, () => {
    resetYourPasswordPage.waitForPageLoad();
});

Then(/^I should see a log off alert 15 seconds before the "(.*)" auto logoff time$/, (autoLogOffTime) => {
    SecuritySettingsPage.autoLogOffPauseTimer(autoLogOffTime);
    logOffAlertPopUpPage.shouldSeeLogoffAlertPopup();
});

Then(/^I must be logged out of the app after the selected auto logoff time$/, () => {
    LogOffAlertPopUpPage.waitLogoutPageLoad();
    logoutPage.shouldSeeSessionTimedOutMessage();
});

Then(/^I select the (logoff|continue) option from the logoff alert pop up$/, (type) => {
    if (type === 'logoff') {
        logOffAlertPopUpPage.shouldSeeLogoffAlertPopup();
        logOffAlertPopUpPage.clickLogOffOption();
    } else {
        logOffAlertPopUpPage.shouldSeeLogoffAlertPopup();
        logOffAlertPopUpPage.clickContinueOption();
        LogOffAlertPopUpPage.waitLogoutPageLoad();
    }
});

Then(/^I should be on the security settings page after the log out time selected$/, () => {
    securitySettingsPage.waitForPageLoad();
});

Then(/^I put the app in the background$/, () => {
    SecuritySettingsPage.sendAppToBackground();
});

Then(/^I launch the app from the background$/, () => {
    SecuritySettingsPage.bringAppFromBackground();
});

When(/^I dismiss the server error message$/, () => {
    loginPage.closeErrorMessage();
});

When(/^I clear work mobile number in personal details page$/, () => {
    changePhoneNumbersPage.waitForPageLoad();
    changePhoneNumbersPage.clearMobileNumber();
});

When(/^I clear home mobile number in personal details page$/, () => {
    changePhoneNumbersPage.waitForPageLoad();
    changePhoneNumbersPage.clearHomeNumber();
});

When(/^I clear personal mobile number in personal details page$/, () => {
    changePhoneNumbersPage.waitForPageLoad();
    changePhoneNumbersPage.clearMobileNumber();
});

Then(/^I should see error in the personal Detail page$/, () => {
    changePhoneNumbersPage.waitForPageLoad();
    changePhoneNumbersPage.isPopupErrorVisible();
});

Then(/^I select modify spending rewards from the settings menu page$/, () => {
    settingsPage.waitForPageLoad();
    settingsPage.selectSpendingRewardsButton();
});

Then(/^I tap on register for everyday offers option$/, () => {
    settingsPage.selectSpendingRewardsButton();
});

Then(/^I select pay a contact settings from the settings menu$/, () => {
    settingsPage.waitForPageLoad();
    settingsPage.selectPayAContactSettings();
});

Then(/^I enter incorrect password for (.*) times consecutively$/, (num) => {
    for (let i = 0; i < num; i++) {
        confirmAuthenticationPasswordPage.enterPasswordAndConfirm(incorrectPassword, num);
    }
});

Then(/^I select the real time alerts option from the Settings page$/, () => {
    settingsPage.waitForPageLoad();
    settingsPage.selectRealTimeAlertsTile();
});

Then(/^I should be on the smart alerts page$/, () => {
    settingsPage.waitForRealTimeAlertsPageLoad();
});

Then(/^I should see the personal Detail page$/, () => {
    changePhoneNumbersPage.waitForPageLoad();
});

Then(/^I am shown winback with Cancel and Ok option$/, () => {
    resetConfirmationPopUpPage.waitForPageLoad();
    resetConfirmationPopUpPage.verifyResetConfirmationPopup();
});

Then(/^I should see information about reset app and a warning message$/, () => {
    resetPage.waitForPageLoad();
    resetPage.verifyResetAppInfo();
    resetPage.verifyResetAppWarningMessage();
});

Then(/^I am (shown|not shown) the option to Remove Provider$/, (isShown) => {
    settingsPage.shouldSeeOpenBankingTile(isShown === 'shown');
});

Then(/^I should see remove provider options at the end of the page$/, () => {
    settingsPage.validateOpenBankingTilePostition();
});

When(/^I select Remove Provider tile$/, () => {
    settingsPage.selectOpenBankingTile();
});

Then(/^I should see error banner on settings screen$/, () => {
    settingsPage.shouldSeeOpenBankingErrorBanner();
});

When(/^I should (see|not see) the Notifications option$/, (isVisible) => {
    if (isVisible === 'see') {
        settingsPage.verifyNotificationsOptionIsNotVisible(true);
    } else {
        settingsPage.verifyNotificationsOptionIsNotVisible(false);
    }
});

Given(/^I navigate to the re-set password page/, () => {
    NavigationTask.navigateToSecuritySettings();
    selectForgotPassword();
    waitForResetPassword();
});

When(/^I have navigated to Personal details from settings/, () => {
    NavigationTask.openMoreMenu();
    morePage.selectSettings();
    personalDetails();
});
