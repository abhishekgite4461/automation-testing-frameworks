const {When, Then} = require('cucumber');
const CreateMemorableInfoPage = require('../../pages/ibRegistration/createMemorableInfo.page');
const Actions = require('../../pages/common/actions');

const createMemorableInfo = new CreateMemorableInfoPage();

Then(/^I should be on the IB Registration Create Memorable Info page$/, () => {
    createMemorableInfo.verifyCreateMemorableInfoTitle();
});

When(/^I enter Create Memorable Info Details on the IB Registration Create Memorable Info page$/, function () {
    const data = this.dataRepository.get();
    createMemorableInfo.enterCreateMemorableInformation(
        data.memorableInfo,
        data.confirmMemorableInfo
    );
});

Then(/^I should see contact us option on the IB Registration Create Memorable Info page$/, () => {
    createMemorableInfo.waitForPageLoad();
    createMemorableInfo.shouldSeeContactUsButton();
});

When(/^I enter valid Memorable Info on the IB Registration Create Memorable Info page$/, function () {
    const data = this.dataRepository.get();
    createMemorableInfo.enterMemorableInfo(data.memorableInfo);
});

When(/^I enter UserID in Memorable Info Field on the IB Registration Create Memorable Info page$/, function () {
    const data = this.dataRepository.get();
    createMemorableInfo.enterMemorableInfo(data.userId);
});

When(/^I enter password in Memorable Info Field on the IB Registration Create Memorable Info page$/, function () {
    const data = this.dataRepository.get();
    createMemorableInfo.enterMemorableInfo(data.userPassword);
});

When(/^I enter valid Confirm Memorable Info on the Create Memorable Info page$/, function () {
    const data = this.dataRepository.get();
    createMemorableInfo.enterConfirmMemorableInfo(data.memorableInformation);
});

When(/^I enter "(.*)" in Memorable Info Field on the IB Registration Create Memorable Info page$/, (memorableinfo) => {
    createMemorableInfo.enterMemorableInfo(memorableinfo);
});

When(/^I enter "(.*)" in Confirm Memorable Info Field on the Create Memorable Info page$/, (confirmmemorableinfo) => {
    createMemorableInfo.enterConfirmMemorableInfo(confirmmemorableinfo);
});

When(/^I select Memorable Info on the Create Memorable Info page$/, () => {
    createMemorableInfo.selectMemorableInfo();
});

When(/^I select Confirm Memorable Info on the Create Memorable Info page$/, () => {
    createMemorableInfo.selectConfirmMemorableInfo();
});

Then(/^I select Next Button on IB Registration Create Memorable Info page$/, () => {
    createMemorableInfo.selectNext();
});

Then(/^I select HideShow Button on Memorable Info Field on the Create Memorable Info page$/, () => {
    createMemorableInfo.selectHideShowMemorableInfo();
});

Then(/^I select HideShow Button on Confirm Memorable Info Field on the Create Memorable Info page$/, () => {
    createMemorableInfo.selectHideShowConfirmMemorableInfo();
});

Then(/^I should see Confirm Memorable Info Tip on the Create Memorable Info page$/, () => {
    createMemorableInfo.validateConfirmMemorableInfoTip();
});

When(/^I validate Confirm Memorable Info Field is enabled on the IB Registration Create Memorable Info page$/, () => {
    createMemorableInfo.isEnabledConfirmMemorableInfo(true);
});

When(/^I validate Confirm Memorable Info Field is not enabled on the IB Registration Create Memorable Info page$/, () => {
    createMemorableInfo.isEnabledConfirmMemorableInfo(false);
});

Then(/^I select FindOutMore Link in Memorable Info hint text on the IB Registration Create Memorable Info page$/, () => {
    createMemorableInfo.selectFindOutMore();
});

Then(/^I select FindOutMore Link in Memorable Info hint text on the Create Memorable Info page$/, () => {
    createMemorableInfo.selectFindOutMore();
});

Then(/^I validate Memorable Info tips dialog on the Create Memorable Info page$/, () => {
    createMemorableInfo.validateMemorableInfoTipDialog();
});

Then(/^I should be able to close Memorable Info tips dialog on the Create Memorable Info page$/, () => {
    createMemorableInfo.closeFindOutMoreTip();
});

When(/^I validate the Memorable Info Field is masked on the Create Memorable Info page$/, () => {
    createMemorableInfo.validateMaskedMemorableInfo(true);
});

When(/^I validate the Memorable Info Field is not masked on the Create Memorable Info page$/, () => {
    createMemorableInfo.validateMaskedMemorableInfo(false);
});

When(/^I validate the Confirm Memorable Info Field is masked on the Create Memorable Info page$/, () => {
    createMemorableInfo.validateMaskedConfirmMemorableInfo(true);
});

When(/^I validate the Confirm Memorable Info Field is not masked on the Create Memorable Info page$/, () => {
    createMemorableInfo.validateMaskedConfirmMemorableInfo(false);
});

When(/^I tap out of the Field on the IB Registration Create Memorable Info page$/, () => {
    Actions.tapOutOfField();
});

When(/^I clear the Memorable Info Field on the IB Registration Create Memorable Info page$/, () => {
    createMemorableInfo.clearMemorableInfo();
});

Then(/^I validate Memorable Info on the IB Registration Create Memorable Info page$/, function () {
    const data = this.dataRepository.get();
    createMemorableInfo.validateMemorableInfo(data.memorableInfo);
});

When(/^I clear the Confirm Memorable Info Field on the IB Registration Create Memorable Info page$/, () => {
    createMemorableInfo.clearConfirmMemorableInfo();
});

Then(/^I validate maximum characters allowed for Memorable Info Field should be "(.*)" on the IB Registration Create Memorable Info page$/, (maxAllowed) => {
    createMemorableInfo.validateMaxCharactersMemorableInfo(maxAllowed);
});

When(/^I select the contact us option from the create mi page pre auth header$/, () => {
    createMemorableInfo.waitForPageLoad();
    createMemorableInfo.selectContactUs();
});

Then(/^I am on Create Memorable Info page$/, () => {
    createMemorableInfo.verifyCreateMemorableInfoTitle();
});

Then(/^I enter (.*) in Memorable Info Field on the Create Memorable Info page$/, (memorableinfo) => {
    createMemorableInfo.enterMemorableInfo(memorableinfo);
});

Then(/^I should see Memorable Info Tip on the Create Memorable Info page$/, () => {
    createMemorableInfo.validateMemorableInfoTip();
});

When(/^I tap out of the Field on the Create Memorable Info page$/, () => {
    Actions.tapOutOfField();
});

When(/^I validate Confirm Memorable Info Field is not enabled on the Create Memorable Info page$/, () => {
    createMemorableInfo.isEnabledConfirmMemorableInfo(false);
});

Then(/^I validate maximum characters allowed for Memorable Info Field should be "(.*)" on the Create Memorable Info page$/, (maxAllowed) => {
    createMemorableInfo.validateMaxCharactersMemorableInfo(maxAllowed);
});

When(/^I enter valid Memorable Info on the Create Memorable Info page$/, function () {
    const data = this.dataRepository.get();
    createMemorableInfo.enterMemorableInfo(data.memorableInformation);
});

When(/^I validate Confirm Memorable Info Field is enabled on the Create Memorable Info page$/, () => {
    createMemorableInfo.isEnabledConfirmMemorableInfo(true);
});
