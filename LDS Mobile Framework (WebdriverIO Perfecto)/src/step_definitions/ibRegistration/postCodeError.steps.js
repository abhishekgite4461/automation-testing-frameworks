const {When, Then} = require('cucumber');
const PostCodeErrorPage = require('../../pages/ibRegistration/postCodeErrorPage.page.js');

const postCodeError = new PostCodeErrorPage();

Then(/^I am on the IB Registration Postcode Error page$/, () => {
    postCodeError.verifyPostcodeTitle();
});

Then(/^"(.*)" PostCode appears on the IB Registration Postcode Error page$/, (postCode) => {
    postCodeError.validatePostCode(postCode);
});

When(/^Next Button is not enabled on the IB Registration Postcode Error page$/, () => {
    postCodeError.isEnabledNextOnErrorPage(false);
});

When(/^I clear and enter "(.*)" in the Postcode field on the IB Registration Postcode Error page$/, (postCode) => {
    postCodeError.clearPostCode();
    postCodeError.setPostCode(postCode);
});

When(/^Next Button is enabled on the IB Registration Postcode Error page$/, () => {
    postCodeError.isEnabledNextOnErrorPage(true);
});

When(/^I validate "(.*)" is in Upper Case on the IB Registration Postcode Error page$/, (expectedPostCode) => {
    postCodeError.validatePostCode(expectedPostCode);
});

When(/^I enter valid PostCode on the IB Registration Postcode Error page$/, function () {
    postCodeError.setPostCode(this.dataRepository.get().postCode);
});

When(/^I click next button on the IB Registration Postcode Error page$/, () => {
    postCodeError.clickNext();
});

When(/^I clear the Postcode field on the IB Registration Postcode Error page$/, () => {
    postCodeError.clearPostCode();
});

Then(/^I validate maximum characters of PostCode Field on the IB Registration Postcode Error page$/, () => {
    postCodeError.verifyMaxCharactersPostCode();
});
