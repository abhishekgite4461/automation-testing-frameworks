const {When, Then} = require('cucumber');
const CreateSecureLogonDetailsPage = require('../../pages/ibRegistration/createSecureLogonDetails.page');
const Actions = require('../../pages/common/actions');

const createSecureLogonDetails = new CreateSecureLogonDetailsPage();

Then(/^I should be on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.verifyCreateSecureLogonDetailsTitle();
});

When(/^I enter Secure Logon Details on the IB Registration Create Secure Logon Details page$/, function () {
    const data = this.dataRepository.get();
    createSecureLogonDetails.enterSecureLogonDetails(data.userId, data.userPassword);
});

Then(/^I should see contact us option on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.verifyCreateSecureLogonDetailsTitle();
    createSecureLogonDetails.shouldSeeContactUsButton();
});

When(/^I enter blacklisted Secure Logon Details on the IB Registration Create Secure Logon Details page$/, function () {
    const data = this.dataRepository.get();
    createSecureLogonDetails.enterSecureLogonDetails(data.userId, data.blacklistedPassword);
});

When(/^I click UserId Field on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.clickUserId();
});

When(/^I enter valid UserId on the IB Registration Create Secure Logon Details page$/, function () {
    const data = this.dataRepository.get();
    createSecureLogonDetails.enterUserId(data.userId);
});

When(/^I enter "(.*)" in UserId Field on the IB Registration Create Secure Logon Details page$/, (userId) => {
    createSecureLogonDetails.enterUserId(userId);
});

When(/^I make the UserID field blank on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.clearUserId();
});

When(/^I delete characters from UserId Field on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.clickUserId();
    createSecureLogonDetails.clickKeyboardDelete();
});

Then(/^I should see UserId Field with "(.*)" value on the IB Registration Create Secure Logon Details page$/, (expectedUserId) => {
    createSecureLogonDetails.validateUserId(expectedUserId);
});

Then(/^I should see UserId Tip on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.validateTipMessage('UserId');
});

When(/^I click check button on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.clickUserIdCheckButton();
});

When(/^I should see UserID options are visible on IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.validateUserIdOptionsVisible();
});

When(/^I select User Id from UserID options on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.selectUsername();
});

When(/^I click Back Button in UserID options on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.clickBackUserIdOptions();
});

When(/^I should see a check button in User ID Field on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.validateCheckButtonVisible(true);
});

When(/^I should see a loading indicator on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.validateLoadingIndicator(true);
});

When(/^I should not see a check button in User ID Field on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.validateCheckButtonVisible(false);
});

When(/^I click Password Field on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.clickPassword();
});

When(/^I enter valid Password on the IB Registration Create Secure Logon Details page$/, function () {
    const data = this.dataRepository.get();
    createSecureLogonDetails.enterPassword(data.userPassword);
});

When(/^I enter "(.*)" in Password Field on the IB Registration Create Secure Logon Details page$/, (password) => {
    createSecureLogonDetails.enterPassword(password);
});

When(/^I enter UserID in Password Field on the IB Registration Create Secure Logon Details page$/, function () {
    const data = this.dataRepository.get();
    createSecureLogonDetails.enterPassword(data.userId);
});

When(/^I enter first name in Password Field on the IB Registration Create Secure Logon Details page$/, function () {
    const data = this.dataRepository.get();
    createSecureLogonDetails.enterPassword(`ab${data.firstName}12`);
});

When(/^I enter last name in Password Field on the IB Registration Create Secure Logon Details page$/, function () {
    const data = this.dataRepository.get();
    createSecureLogonDetails.enterPassword(`cd${data.lastName}34`);
});

When(/^I enter DOB with date format as "(.*)" in Password Field on the IB Registration Create Secure Logon Details page$/, (dateFormat) => {
    createSecureLogonDetails.enterDefaultDateAsPassword(dateFormat);
});

Then(/^I click HideShow Button on Password Field on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.clickHideShowPassword();
});

When(/^I should see the HideShow Label as "(.*)" on Password Field on the IB Registration Create Secure Logon Details page$/, (expectedLabel) => {
    createSecureLogonDetails.validateHideShowLabelPassword(expectedLabel);
});

When(/^I should see the Password Field is masked on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.validateMaskedPassword(true);
});

When(/^I should see the Password Field is not masked on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.validateMaskedPassword(false);
});

When(/^I make the Password Field blank on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.clearPassword();
});

When(/^I delete characters from Password Field on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.clickPassword();
    createSecureLogonDetails.clickKeyboardDelete();
});

Then(/^I should see Password Tip on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.validateTipMessage('Password');
});

Then(/^I should see FindOutMore Link beneath Password on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.validateFindOutMore();
});

When(/^I click Confirm Password Field on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.clickConfirmPassword();
});

When(/^I enter valid ConfirmPassword on the IB Registration Create Secure Logon Details page$/, function () {
    const data = this.dataRepository.get();
    createSecureLogonDetails.enterConfirmPassword(data.confirmUserPassword);
});

When(/^I enter "(.*)" in Confirm Password Field on the IB Registration Create Secure Logon Details page$/, (password) => {
    createSecureLogonDetails.enterConfirmPassword(password);
});

When(/^I should see the Confirm Password Field is masked on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.validateMaskedConfirmPassword(true);
});

When(/^I should see the Confirm Password Field is not masked on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.validateMaskedConfirmPassword(false);
});

Then(/^I click HideShow Button on Confirm Password Field on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.clickHideShowConfirmPassword();
});

When(/^I should see the HideShow Label as "(.*)" on Confirm Password Field on the IB Registration Create Secure Logon Details page$/, (expectedlabel) => {
    createSecureLogonDetails.validateHideShowLabelConfirmPassword(expectedlabel);
});

When(/^I make the Confirm Password field blank on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.clearConfirmPassword();
});

When(/^I should see Confirm Password Field is enabled on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.validateConfirmPasswordEnabled(true);
});

When(/^I should see Confirm Password Field is not enabled on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.validateConfirmPasswordEnabled(false);
});

When(/^I should see Confirm Password Tip on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.validateTipMessage('ConfirmPassword');
});

Then(/^I should see Account Validation Message as "(.*)" on the IB Registration Create Secure Logon Details page$/, (expectedMessage) => {
    createSecureLogonDetails.validateCommercialAccountMessage(expectedMessage);
});

Then(/^I should see Commercial User Id pre-populated on the IB Registration Create Secure Logon Details page$/, function () {
    const data = this.dataRepository.get();
    createSecureLogonDetails.validateCommercialUserId(data.userId);
});

When(/^I check max length of Password Field on the IB Registration Create Secure Logon Details page/, () => {
    createSecureLogonDetails.checkPasswordMaxLength();
});

When(/^I tap out of the field on the IB Registration Create Secure Logon Details page$/, () => {
    Actions.tapOutOfField();
});

When(/^I click Next Button on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.clickNext();
});

When(/^I should see Next Button is enabled on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.validateNextButtonEnabled(true);
});

When(/^I should see Next Button is not enabled on the IB Registration Create Secure Logon Details page$/, () => {
    createSecureLogonDetails.validateNextButtonEnabled(false);
});

When(/^I check the max length of UserId Field on the IB Registration Create Secure Logon Details page/, () => {
    createSecureLogonDetails.checkUserIdMaxLength();
});

When(/^I tap out and should see next button is not enabled on the IB Registration Create Secure Logon Details page/, () => {
    Actions.tapOutOfField();
    Actions.hideDeviceKeyboard();
    createSecureLogonDetails.validateNextButtonEnabled(false);
});

When(/^I should see an error icon on the IB Registration Create Secure Logon Details page/, () => {
    createSecureLogonDetails.validateErrorIconVisible(true);
});

When(/^I should not see an error icon on the IB Registration Create Secure Logon Details page/, () => {
    createSecureLogonDetails.validateErrorIconVisible(false);
});

When(/^I enter valid password and confirm password details on the IB Registration Create Secure Logon Details page/, function () {
    const data = this.dataRepository.get();
    Actions.hideDeviceKeyboard();
    createSecureLogonDetails.enterPassword(data.userPassword);
    Actions.tapOutOfField();
    createSecureLogonDetails.enterConfirmPassword(data.confirmUserPassword);
    Actions.tapOutOfField();
});
