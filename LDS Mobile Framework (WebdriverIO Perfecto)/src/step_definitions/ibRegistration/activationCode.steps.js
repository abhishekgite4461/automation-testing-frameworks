const {When, Then} = require('cucumber');
const ActivationCodePage = require('../../pages/ibRegistration/activationCode.page');

const activationCode = new ActivationCodePage();

Then(/^I should be on the IB Registration Activation Code page$/, () => {
    activationCode.verifyEnterActivationCodeTitle();
});

Then(/^I should see contact us option on the IB Registration Activation Code page$/, () => {
    activationCode.verifyEnterActivationCodeTitle();
    activationCode.shouldSeeContactUsButton();
});

When(/^I enter valid Activation Code on IB Registration Activation Code page$/, function () {
    const data = this.dataRepository.get();
    activationCode.enterActivationCode(data.activationCode);
});

When(/^I enter "(.*)" in Activation Code Field on IB Registration Activation Code page$/, (activationCodeData) => {
    activationCode.enterActivationCode(activationCodeData);
});

When(/^I validate Activation Code on IB Registration Activation Code page$/, function () {
    const data = this.dataRepository.get();
    activationCode.validateActivationCode(data.activationCode.toString()
        .toUpperCase());
});

Then(/^I click Request New Activation Code button on the IB Registration Activation Code page$/, () => {
    activationCode.clickRequestNewCode();
});

When(/^I should see Confirm Code Button is not enabled on the IB Registration Activation Code page$/, () => {
    activationCode.isEnabledConfirmCode(false);
});

When(/^I should see Confirm Code Button is enabled on the IB Registration Activation Code page$/, () => {
    activationCode.isEnabledConfirmCode(true);
});

When(/^I click on Confirm Code Button on IB Registration Activation Code page$/, () => {
    activationCode.clickConfirmCode();
});

Then(/^I should see ErrorMessage as "(.*)" on the IB Registration Activation Code page$/, (expectedErrorMessage) => {
    activationCode.validateErrorMessage(expectedErrorMessage);
});
