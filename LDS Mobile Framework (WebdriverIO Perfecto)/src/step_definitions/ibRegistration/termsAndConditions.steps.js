const {When, Then} = require('cucumber');
const NewWelcomeBenefitsPage = require('../../pages/ibRegistration/newWelcomeBenefits.page');
const PersonalDetailsPage = require('../../pages/ibRegistration/personalDetails.page.js');
const AccountDetailsPage = require('../../pages/ibRegistration/accountDetails.page.js');
const CreateSecureLogonDetailsPage = require('../../pages/ibRegistration/createSecureLogonDetails.page');
const CreateMemorableInfoPage = require('../../pages/ibRegistration/createMemorableInfo.page');
const TermsAndConditionsPage = require('../../pages/ibRegistration/termsAndConditions.page');
const AccountType = require('../../enums/accountType.enum');
const DeviceHelper = require('../../utils/device.helper');

const newWelcomeBenefits = new NewWelcomeBenefitsPage();
const personalDetails = new PersonalDetailsPage();
const accountDetails = new AccountDetailsPage();
const createSecureLogonDetails = new CreateSecureLogonDetailsPage();
const createMemorableInfo = new CreateMemorableInfoPage();
const termsAndConditions = new TermsAndConditionsPage();

function getAccountTypeFromString(type) {
    return AccountType.fromString(type.trim()
        .replace(/ /g, '_')
        .toUpperCase());
}

Then(/^I should be on the IB Registration TermsAndConditions page$/, () => {
    new TermsAndConditionsPage();
});

When(/^I click Agree Button on IB Registration TermsAndConditions page/, () => {
    termsAndConditions.agreeTermsAndConditions();
});

Then(/^I click Back Button on the IB Registration TermsAndConditions page/, () => {
    termsAndConditions.clickBack();
});

Then(/^I should see footer section on the IB Registration TermsAndConditions page/, () => {
    termsAndConditions.verifyFooterSection();
});

Then(/^I should see contact us option on the IB Registration TermsAndConditions page/, () => {
    termsAndConditions.waitForPageLoad();
    termsAndConditions.shouldSeeContactUsButton();
});

Then(/^I complete EIA registration for Internet Banking$/, function () {
    const data = this.dataRepository.get();
    if (DeviceHelper.isMBNA()) {
        newWelcomeBenefits.startIBRegistration();
    } else {
        newWelcomeBenefits.startRegistration();
    }
    personalDetails.setPersonalDetails(
        data.firstName, data.lastName, data.emailId,
        data.dobDate, data.dobMonth, data.dobYear, data.postCode
    );
    personalDetails.clickNext();

    if (DeviceHelper.isMBNA()) {
        accountDetails.enterCreditCardNumber(data.creditCardNumber);
    } else {
        const selectAccountType = getAccountTypeFromString(data.accountType1);
        accountDetails.enterAccountInformation(
            selectAccountType,
            data.accountNumber, data.sortCode
        );
    }
    accountDetails.clickNext();
    createSecureLogonDetails.enterSecureLogonDetails(data.userId, data.userPassword);
    createSecureLogonDetails.clickNext();
    createMemorableInfo.enterCreateMemorableInformation(
        data.memorableInfo,
        data.confirmMemorableInfo
    );
    createMemorableInfo.selectNext();
    termsAndConditions.agreeTermsAndConditions();
});
