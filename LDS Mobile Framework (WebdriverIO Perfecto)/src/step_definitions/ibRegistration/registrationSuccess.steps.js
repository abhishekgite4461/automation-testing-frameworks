const {Then} = require('cucumber');
const RegistrationSuccessPage = require('../../pages/ibRegistration/registrationSuccess.page');

const registrationSuccessPage = new RegistrationSuccessPage();

Then(/^I should be on the IB Registration Success page$/, () => {
    registrationSuccessPage.verifyRegistrationSuccessTitle();
});

Then(/^I should see the success Message as "(.*)" on the IB Registration Success page$/, (expectedSuccessMessage) => {
    registrationSuccessPage.validateSuccessMessage(expectedSuccessMessage);
});

Then(/^I should see the footer Message as "(.*)" on the IB Registration Success page$/, (expectedFooterMessage) => {
    registrationSuccessPage.validateFooterMessage(expectedFooterMessage);
});

Then(/^I select continue on the IB Registration Success page$/, () => {
    registrationSuccessPage.clickContinue();
});

Then(/^I should see contact us option on the IB Registration Success page$/, () => {
    registrationSuccessPage.verifyRegistrationSuccessTitle();
    registrationSuccessPage.shouldSeeContactUsButton();
});
