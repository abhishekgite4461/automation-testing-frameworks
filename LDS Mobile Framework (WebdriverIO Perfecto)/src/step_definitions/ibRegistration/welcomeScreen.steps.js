const {When, Then} = require('cucumber');
const NewWelcomeBenefitsPage = require('../../pages/ibRegistration/newWelcomeBenefits.page');
const RegistrationWebViewPage = require('../../pages/ibRegistration/registrationWebview.page');

const newWelcomeBenefits = new NewWelcomeBenefitsPage();
const registrationWebView = new RegistrationWebViewPage();

Then(/^I should be on the Welcome Benefits page$/, () => {
    newWelcomeBenefits.waitForPageLoad();
});

When(/^I click on Register button$/, () => {
    newWelcomeBenefits.startRegistration();
});

When(/^I click on Login button$/, () => {
    newWelcomeBenefits.goToLogin();
});

When(/^I should see Webview Journey Personal Details Page$/, () => {
    registrationWebView.verifyRegistrationWebViewTitle();
});

When(/^I navigate to fraud guarantee$/, () => {
    registrationWebView.navigateToFraudGuarantee();
});

Then(/^I navigate to app demo/, () => {
    registrationWebView.navigateToAppDemo();
});

Then(/^I navigate to new customer/, () => {
    registrationWebView.navigateToNewCustomer();
});

When(/^I should see fraud guarantee page in the mobile browser$/, () => {
    registrationWebView.canSeeFraudGuaranteeWebPage();
});

When(/^I should see app demo page in the mobile browser$/, () => {
    registrationWebView.canSeeAppDemoWebPage();
});

When(/^I should see new customer page in the mobile browser$/, () => {
    registrationWebView.canSeeNewCustomerWebPage();
});

When(/^I click yes on the Alert Dialogue$/, () => {
    registrationWebView.leaveTheApp(true);
});

Then(/^I select the contact us option from the welcome page pre auth header$/, () => {
    newWelcomeBenefits.waitForPageLoad();
    newWelcomeBenefits.selectContactUs();
});

Then(/^I select the legal info option from the welcome page$/, () => {
    newWelcomeBenefits.waitForPageLoad();
    newWelcomeBenefits.selectLegalInfo();
});
