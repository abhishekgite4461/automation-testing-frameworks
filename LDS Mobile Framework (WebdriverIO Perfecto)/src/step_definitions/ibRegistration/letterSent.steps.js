const {When, Then} = require('cucumber');
const LetterSentPage = require('../../pages/ibRegistration/letterSent.page');

const letterSent = new LetterSentPage();

Then(/^I should be on the IB Registration LetterSent page$/, () => {
    letterSent.verifyLetterSentTitle();
});

Then(/^I should see contact us option on the IB Registration LetterSent page$/, () => {
    letterSent.shouldSeeContactUsButton();
});

When(/^I verify registered username on IB Registration Letter Sent page$/, function () {
    const data = this.dataRepository.get();
    letterSent.validateRegisteredUsername(data.userId);
});

When(/^I verify the ErrorMessage "(.*) on the IB Registration Letter Sent page$/, (expectedErrorMEssage) => {
    letterSent.validateErrorMessage(expectedErrorMEssage);
});
