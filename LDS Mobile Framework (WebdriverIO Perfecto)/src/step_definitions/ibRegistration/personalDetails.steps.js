const {When, Then} = require('cucumber');
const PersonalDetailsPage = require('../../pages/ibRegistration/personalDetails.page');

const personalDetails = new PersonalDetailsPage();
const Actions = require('../../pages/common/actions');

Then(/^I should be on the IB Registration Personal Details page$/, () => {
    personalDetails.verifyPersonalDetailsTitle();
});

When(/^I click Back Button on the IB Registration Personal Details page/, () => {
    personalDetails.clickBack();
});

Then(/^I validate Next Button is enabled on the IB Registration Personal Details page$/, () => {
    personalDetails.isEnabledNextButton(true);
});

Then(/^I validate Next Button is not enabled on the IB Registration Personal Details page$/, () => {
    personalDetails.isEnabledNextButton(false);
});

Then(/^I validate FirstName on the IB Registration Personal Details page$/, function () {
    personalDetails.verifyFirstName(this.dataRepository.get().firstName);
});

Then(/^I validate LastName on the IB Registration Personal Details page$/, function () {
    personalDetails.verifyLastName(this.dataRepository.get().lastName);
});

Then(/^I validate EmailId on the IB Registration Personal Details page$/, function () {
    personalDetails.verifyEmail(this.dataRepository.get().emailId);
});

Then(/^I validate PostCode on the IB Registration Personal Details page$/, function () {
    personalDetails.verifyPostCode(this.dataRepository.get().postCode);
});

Then(/^I should see ErrorMessage on the IB Registration Personal Details page$/, () => {
    personalDetails.verifyErrorMessageVisible(true);
});

Then(/^I should see youth customer DOB error on the IB Registration Personal Details page$/, () => {
    personalDetails.verifyYouthErrorMessageVisible(true);
});

Then(/^I should not see youth customer DOB error on the IB Registration Personal Details page$/, () => {
    personalDetails.verifyYouthErrorMessageVisible(false);
});

Then(/^I should see ErrorMessage as "(.*)" on the IB Registration Personal Details page$/, (expectedErrorMessage) => {
    personalDetails.verifyErrorMessage(expectedErrorMessage);
});

Then(/^I should not see ErrorMessage on the IB Registration Personal Details page$/, () => {
    personalDetails.verifyErrorMessageVisible(false);
});

When(/^I validate "(.*)" is in Upper Case on the IB Registration Personal Details page$/, (expectedPostCode) => {
    personalDetails.verifyPostCode(expectedPostCode);
});

Then(/^I validate the first character is capitalized of First Name Field on the IB Registration Personal Details page$/, function () {
    personalDetails.verifyFirstCharacterFirstName(this.dataRepository.get().firstName);
});

Then(/^I validate the first character is capitalized of Last Name Field on the IB Registration Personal Details page$/, function () {
    personalDetails.verifyFirstCharacterLastName(this.dataRepository.get().lastName);
});

When(/^I clear the Last Name Field on the IB Registration Personal Details page$/, () => {
    personalDetails.clearLastName();
});

When(/^I should see contact us option on the IB Registration Personal Details page$/, () => {
    personalDetails.waitForPageLoad();
    personalDetails.shouldSeeContactUsButton();
});

When(/^I enter Personal Details on the IB Registration Personal Details page$/, function () {
    const data = this.dataRepository.get();
    personalDetails.setPersonalDetails(
        data.firstName, data.lastName, data.emailId,
        data.dobDate, data.dobMonth, data.dobYear, data.ibregPostCode
    );
});

When(/^I enter valid FirstName on the IB Registration Personal Details page$/, function () {
    const data = this.dataRepository.get();
    personalDetails.setFirstName(data.firstName);
});

When(/^I enter valid LastName on the IB Registration Personal Details page$/, function () {
    personalDetails.setLastName(this.dataRepository.get().lastName);
});

When(/^I enter "(.*)" in Email Field on the IB Registration Personal Details page$/, (emailId) => {
    personalDetails.setEmailId(emailId);
});

When(/^I enter DOB on the IB Registration Personal Details page$/, function () {
    const data = this.dataRepository.get();
    personalDetails.setDateOfBirth(data.date, data.month, data.year);
});

When(/^I enter valid EmailId on the IB Registration Personal Details page$/, function () {
    personalDetails.setEmailId(this.dataRepository.get().emailId);
});

When(/^I enter valid DOB on the IB Registration Personal Details page$/, function () {
    const data = this.dataRepository.get();
    personalDetails.setDefaultDateOfBirth(data.dobDate, data.dobMonth, data.dobYear);
});

When(/^I enter "(.*)" in PostCode Field on the IB Registration Personal Details page$/, (postCode) => {
    personalDetails.setPostCode(postCode);
});

When(/^I enter valid personal details with "(.*)" postcode on the IB Registration Personal Details page$/, function (postCode) {
    const data = this.dataRepository.get();
    personalDetails.setPersonalDetails(
        data.ibregIncorrectPostcode.firstName, data.ibregIncorrectPostcode.lastName,
        data.ibregIncorrectPostcode.emailId,
        data.ibregIncorrectPostcode.date, data.ibregIncorrectPostcode.month,
        data.ibregIncorrectPostcode.year, postCode
    );
});

When(/^I enter valid personal details with postcode on the IB Registration Personal Details page$/, function () {
    const data = this.dataRepository.get();
    personalDetails.setPersonalDetails(
        data.firstName, data.lastName, data.emailId,
        data.date, data.month, data.year, data.postCode
    );
});

When(/^I enter "(.*)" in First Name Field on the IB Registration Personal Details page$/, (firstName) => {
    personalDetails.setFirstName(firstName);
});

When(/^I enter "(.*)" in Last Name Field on the IB Registration Personal Details page$/, (lastName) => {
    personalDetails.setLastName(lastName);
});

When(/^I enter DOB on the IB Registration Personal Details page for incorrect post code user$/, function () {
    const data = this.dataRepository.get();
    personalDetails.setDateOfBirth(data.ibregIncorrectPostcode.date,
        data.ibregIncorrectPostcode.month, data.ibregIncorrectPostcode.year);
});

When(/^I enter valid PostCode on the IB Registration Personal Details page$/, function () {
    personalDetails.setPostCode(this.dataRepository.get().postCode);
});

When(/^I click Next Button on the IB Registration Personal Details page$/, () => {
    personalDetails.clickNext();
});

When(/^I click Back Button on DOB error message popup on the IB Registration Personal Details page$/, () => {
    personalDetails.clickBackOnDOBError();
});

When(/^I click First Name Field on the IB Registration Personal Details page$/, () => {
    personalDetails.clickFirstName();
});

When(/^I click Last Name Field on the IB Registration Personal Details page$/, () => {
    personalDetails.clickLastName();
});

When(/^I click EmailId Field on the IB Registration Personal Details page$/, () => {
    personalDetails.clickEmailId();
});

When(/^I click PostCode Field on the IB Registration Personal Details page$/, () => {
    personalDetails.clickPostCode();
});

When(/^I leave First Name Field blank on the IB Registration Personal Details page$/, () => {
    personalDetails.clickFirstName();
    Actions.tapOutOfField();
});

When(/^I leave Last Name Field blank on the IB Registration Personal Details page$/, () => {
    personalDetails.clickLastName();
    Actions.tapOutOfField();
});

When(/^I leave EmailId Field blank on the IB Registration Personal Details page$/, () => {
    personalDetails.clickEmailId();
    Actions.tapOutOfField();
});

When(/^I tap out of the field on the IB Registration Personal Details page$/, () => {
    Actions.tapOutOfField();
});

When(/^I clear the First Name Field on the IB Registration Personal Details page$/, () => {
    personalDetails.clearFirstName();
});

When(/^I open Date Picker on the IB Registration Personal Details page$/, () => {
    personalDetails.openDatePicker();
});

When(/^I click on cancel button of Date Picker on the IB Registration Personal Details page$/, () => {
    personalDetails.clickCancelOnDatePicker();
});

When(/^I click on ok button of Date Picker on the IB Registration Personal Details page$/, () => {
    personalDetails.clickOkOnDatePicker();
});

Then(/^I close the Error Message on the IB Registration Personal Details page$/, () => {
    personalDetails.closeErrorMessage();
});

Then(/^I validate maximum characters of First Name Field on the IB Registration Personal Details page$/, () => {
    personalDetails.verifyMaxCharactersFirstName();
});

Then(/^I validate maximum characters of Last Name Field on the IB Registration Personal Details page$/, () => {
    personalDetails.verifyMaxCharactersLastName();
});

Then(/^I validate maximum characters of PostCode Field on the IB Registration Personal Details page$/, () => {
    personalDetails.verifyMaxCharactersPostCode();
});

Then(/^I validate maximum characters of Email Field on the IB Registration Personal Details page$/, () => {
    personalDetails.verifyMaxCharactersEmail();
});

When(/^I clear the EmailId Field on the IB Registration Personal Details page$/, () => {
    personalDetails.clearEmailId();
});

When(/^I clear the Post Code field on the IB Registration Personal Details page$/, () => {
    personalDetails.clearPostCode();
});

Then(/^I validate the default date with "(.*)" on DOB Field on the IB Registration Personal Details page$/, (yearDelta) => {
    personalDetails.verifyDefaultDateOfBirth(yearDelta);
});

Then(/^I must be displayed the personal details settings menu$/, () => {
    personalDetails.verifyPersonalDetailsTitle();
});

Then(/^I should be on the personal details page$/, () => {
    personalDetails.verifyPersonalDetailsTitle();
});
