const {Then} = require('cucumber');
const HelpDeskPage = require('../../pages/ibRegistration/helpDesk.page.js');

const helpDesk = new HelpDeskPage();

Then(/^I should see ErrorMessage as "(.*)" on the IB Registration HelpDesk page$/, (expectedErrorMessage) => {
    helpDesk.waitForPageLoad();
    helpDesk.validateHelpDeskError(expectedErrorMessage);
});

Then(/^I should see CMS ErrorMessage as "(.*)" on the IB Registration HelpDesk page$/, (expectedErrorMessage) => {
    helpDesk.validateHelpDeskCMSError(expectedErrorMessage);
});

Then(/^I should be on the IB Registration HelpDesk page$/, () => {
    helpDesk.verifyHelpDeskTitle();
});
