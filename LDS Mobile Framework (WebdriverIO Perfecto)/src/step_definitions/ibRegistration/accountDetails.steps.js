const {When, Then} = require('cucumber');
const NewWelcomeBenefitsPage = require('../../pages/ibRegistration/newWelcomeBenefits.page');
const PersonalDetailsPage = require('../../pages/ibRegistration/personalDetails.page.js');
const AccountDetailsPage = require('../../pages/ibRegistration/accountDetails.page.js');
const Actions = require('../../pages/common/actions');
const AccountType = require('../../enums/accountType.enum');

const newWelcomeBenefits = new NewWelcomeBenefitsPage();
const personalDetails = new PersonalDetailsPage();
const accountDetails = new AccountDetailsPage();
const blankValue = '';

Then(/^I should be on the IB Registration Account Details page$/, () => {
    accountDetails.verifyAccountDetailsTitle();
});

When(/^I enter Account Details on the IB Registration Account Details page$/, function () {
    const data = this.dataRepository.get();
    const selectAccountType = getAccountTypeFromString(data.accountType1);
    accountDetails.enterAccountInformation(
        selectAccountType,
        data.accountNumber, data.sortCode
    );
});

When(/^I enter "(.*)" on the IB Registration Account Details page$/, function (accountType) {
    const data = this.dataRepository.get();
    const selectAccountType = getAccountTypeFromString(accountType);
    accountDetails.enterDetails(
        selectAccountType,
        data[selectAccountType.account], data.sortCode
    );
});

When(/^I edit Account Details "(.*)" on the IB Registration Account Details page$/, (fieldType) => {
    accountDetails.deleteCharacterFromField(fieldType);
});

When(/^I clear "(.*)" on the IB Registration Account Details page$/, (fieldType) => {
    accountDetails.clearCharacterFromField(fieldType);
});

Then(/^I should see contact us option on the IB Registration Account Details page$/, () => {
    accountDetails.waitForPageLoad();
    accountDetails.shouldSeeContactUsButton();
});

When(/^I select Account Type "(.*)" on the IB Registration Account Details page$/, (accountType) => {
    const selectAccountType = getAccountTypeFromString(accountType);
    accountDetails.selectAccountType(selectAccountType);
});

function getAccountTypeFromString(type) {
    return AccountType.fromStringByEscapingSpace(type.replace(/ \//g, ''));
}

When(/^I enter valid AccountNumber on the IB Registration Account Details page$/, function () {
    const data = this.dataRepository.get();
    accountDetails.enterAccountNumber(data.accountNumber);
});

When(/^I enter valid MortgageNumber on the IB Registration Account Details page$/, function () {
    const data = this.dataRepository.get();
    accountDetails.enterMortgageNumber(data.mortgageNumber);
});

When(/^I enter valid LoanNumber on the IB Registration Account Details page$/, function () {
    const data = this.dataRepository.get();
    accountDetails.enterLoanReferenceNumber(data.loanNumber);
});

When(/^I enter "(.*)" in "(.*)" Field on the IB Registration Account Details page$/, (fieldValue, fieldName) => {
    accountDetails.enterGenericAccountDetails(fieldValue, fieldName);
});

When(/^I enter valid SortCode on the IB Registration Account Details page$/, function () {
    const data = this.dataRepository.get();
    accountDetails.enterSortCode(data.sortCode);
});

When(/^I tap out of the field on the IB Registration Account Details page$/, () => {
    Actions.tapOutOfField();
});

When(/^I enter "(.*)" for incorrect postcode user on the IB Registration Account Details page$/, function (accountType) {
    const data = this.dataRepository.get();
    const selectAccountType = getAccountTypeFromString(accountType);
    accountDetails.enterDetails(
        selectAccountType,
        data.ibregIncorrectPostcode[selectAccountType.account], data.ibregIncorrectPostcode.sortCode
    );
});

Then(/^I should be able to close the error message in Account Details page$/, () => {
    accountDetails.closeErrorMessage();
});

When(/^I enter valid CreditCard on the IB Registration Account Details page$/, function () {
    const data = this.dataRepository.get();
    accountDetails.enterCreditCardNumber(data.creditCardNumber);
});

When(/^I click Mortgage Number Field on the IB Registration Account Details page$/, () => {
    accountDetails.clickMortgageNumber();
    Actions.hideDeviceKeyboard();
});

Then(/^I should see Mortgage Tip with "(.*)" value on the IB Registration Account Details page$/, (expectedMortgageTip) => {
    accountDetails.validateMortgageTip(expectedMortgageTip);
});

When(/^I click Loan Number Field on the IB Registration Account Details page$/, () => {
    accountDetails.clickLoanNumber();
    Actions.hideDeviceKeyboard();
});

When(/^I click CreditCard Number Field on the IB Registration Account Details page$/, () => {
    accountDetails.clickCreditCard();
    Actions.hideDeviceKeyboard();
});

Then(/^I should see Loan Tip with "(.*)" value on the IB Registration Account Details page$/, (expectedLoanTip) => {
    accountDetails.validateLoanTip(expectedLoanTip);
});

Then(/^I should see CreditCard Tip with "(.*)" value on the IB Registration Account Details page$/, (expectedCreditCardTip) => {
    accountDetails.validateCreditCardTip(expectedCreditCardTip);
});

When(/^I click Next Button on the IB Registration Account Details page$/, () => {
    accountDetails.clickNext();
});

When(/^I navigate back to IB Registration Account Details page from Personal Details page$/, () => {
    accountDetails.clickBack();
    personalDetails.clickNext();
});

When(/^I validate Next Button is enabled on the IB Registration Account Details page$/, () => {
    accountDetails.isEnabledNextButton(true);
});

When(/^I validate Next Button is not enabled on the IB Registration Account Details page$/, () => {
    accountDetails.isEnabledNextButton(false);
});

When(/^I delete characters from AccountNumber Field on the IB Registration Account Details page$/, () => {
    accountDetails.deleteCharacterFromField('accountNumber');
});

When(/^I delete characters from SortCode Field on the IB Registration Account Details page$/, () => {
    accountDetails.deleteCharacterFromField('sortCode');
});

When(/^I delete characters from MortgageNumber Field on the IB Registration Account Details page$/, () => {
    accountDetails.deleteCharacterFromField('mortgageNumber');
});

When(/^I delete characters from LoanNumber Field on the IB Registration Account Details page$/, () => {
    accountDetails.deleteCharacterFromField('loanNumber');
});

When(/^I delete characters from CreditCard Field on the IB Registration Account Details page$/, () => {
    accountDetails.deleteCharacterFromField('creditCardNumber');
});

Then(/^I validate LoanNumber on the IB Registration Account Details page$/, function () {
    const data = this.dataRepository.get();
    accountDetails.validateLoanNumber(data.loanNumber);
});

Then(/^I validate MortgageNumber on the IB Registration Account Details page$/, function () {
    const data = this.dataRepository.get();
    accountDetails.validateMortgageNumber(data.mortgageNumber);
});

Then(/^I validate CreditCard Number on the IB Registration Account Details page$/, function () {
    const data = this.dataRepository.get();
    accountDetails.validateCreditCardNumber(data.creditCardNumber);
});

Then(/^I should not see ErrorMessage on the IB Registration Account Details page$/, () => {
    accountDetails.isVisibleErrorMessage(false);
});

When(/^I clear the MortgageNumber Field on the IB Registration Account Details page$/, () => {
    accountDetails.clearMortgageNumber();
});

When(/^I clear the LoanNumber Field on the IB Registration Account Details page$/, () => {
    accountDetails.clearLoanNumber();
});

When(/^I delete characters from Third SortCode Field on the IB Registration Account Details page$/, () => {
    accountDetails.deleteCharacterFromField('sortCode');
});

When(/^I delete characters from Second SortCode Field on the IB Registration Account Details page$/, () => {
    accountDetails.deleteCharacterFromField('midSortCode');
});

When(/^I delete characters from First SortCode Field on the IB Registration Account Details page$/, () => {
    accountDetails.deleteCharacterFromField('firstSortCode');
});

When(/^I clear the CreditCard Field on the IB Registration Account Details page$/, () => {
    accountDetails.clearCreditCard();
});

When(/^I clear the SortCode Field on the IB Registration Account Details page$/, () => {
    accountDetails.clearSortCode();
});

When(/^I clear the Account Number Field on the IB Registration Account Details page$/, () => {
    accountDetails.clearAccountNumber();
});

Then(/^I verify the ErrorMessage "(.*)" on the IB Registration Account Detail page$/, (expectedErrorMessage) => {
    accountDetails.validateErrorMessage(expectedErrorMessage);
    accountDetails.closeErrorMessage();
});

When(/^I validate Account details on the IB Registration Account Details page$/, function () {
    const data = this.dataRepository.get();
    accountDetails.validateAccountDetails(data.sortCode, data.accountNumber);
});

When(/^I validate Account details should be blank on the IB Registration Account Details page$/, () => {
    accountDetails.validateAccountDetails(blankValue, blankValue);
});

When(/^I validate only sort code should be blank on the IB Registration Account Details page$/, function () {
    const data = this.dataRepository.get();
    accountDetails.validateAccountDetails(blankValue, data.accountNumber);
});

When(/^I validate only account number should be blank on the IB Registration Account Details page$/, function () {
    const data = this.dataRepository.get();
    accountDetails.validateAccountDetails(data.sortCode, blankValue);
});

When(/^I validate CreditCard should be blank on the IB Registration Account Details page$/, () => {
    accountDetails.validateCreditCardNumber(blankValue, blankValue);
});

When(/^I validate LoanNumber should be blank on the IB Registration Account Details page$/, () => {
    accountDetails.validateLoanNumber(blankValue, blankValue);
});

When(/^I validate MortgageNumber should be blank on the IB Registration Account Details page$/, () => {
    accountDetails.validateMortgageNumber(blankValue, blankValue);
});

When(/^I click Sort Code Field on the IB Registration Account Details page$/, () => {
    accountDetails.clickSortCode();
});

When(/^I click Account Number Field on the IB Registration Account Details page$/, () => {
    accountDetails.clickAccountNumber();
});

Then(/^I validate maximum characters of CreditCard Field on the IB Registration Account Details page$/, () => {
    accountDetails.verifyMaxCharactersCreditCard();
});

Then(/^I validate maximum characters of LoanNumber Field on the IB Registration Account Details page$/, () => {
    accountDetails.verifyMaxCharactersLoanNumber();
});

Then(/^I validate maximum characters of MortgageNumber Field on the IB Registration Account Details page$/, () => {
    accountDetails.verifyMaxCharactersMortgageNumber();
});

Then(/^I enter Personal and Account Details for IB Registration$/, function () {
    const data = this.dataRepository.get();
    newWelcomeBenefits.startRegistration();
    personalDetails.setPersonalDetails(
        data.firstName, data.lastName, data.emailId,
        data.dobDate, data.dobMonth, data.dobYear, data.ibregPostCode
    );
    personalDetails.clickNext();
    const selectAccountType = getAccountTypeFromString(data.accountType1);
    accountDetails.enterAccountInformation(
        selectAccountType,
        data.accountNumber, data.sortCode
    );
    accountDetails.clickNext();
});
