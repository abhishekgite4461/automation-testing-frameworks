const {Then} = require('cucumber');
require('chai');

const HomePage = require('../pages/home.page');
const AccountStatementPage = require('../pages/statements/accountStatement.page.js');
const ScottishWidowsFAQPage = require('../pages/scottishWidowsFAQ.page.js');

const homePage = new HomePage();
const scottishWidowsFAQPage = new ScottishWidowsFAQPage();
const accountStatementPage = new AccountStatementPage();

Then(/I should see my Pension Policy number$/, () => {
    homePage.verifyPensionPolicyNumberIsVisible();
});

Then(/I should see my Pension Policy number on statement page$/, () => {
    accountStatementPage.shouldSeePensionPolicyNumber();
});

Then(/I should see my Pension balance$/, () => {
    homePage.verifyPensionBalanceIsVisible();
});

Then(/I should see a statement page with information about scottish widows pensions$/, () => {
    accountStatementPage.verifyScottishWidowsStatementTextIsPresent();
});

Then(/I select the more information button on the statement view$/, () => {
    accountStatementPage.selectScottishWidowsFindOutMoreButton();
});

Then(/I should see the Scottish widows FAQ page$/, () => {
    accountStatementPage.verifyScottishWidowsFirstFAQIsPresent();
});

Then(/I navigate to the scottish widows FAQ Page$/, () => {
    accountStatementPage.verifyScottishWidowsStatementTextIsPresent();
    accountStatementPage.selectScottishWidowsFindOutMoreButton();
    accountStatementPage.verifyScottishWidowsFirstFAQIsPresent();
});

Then(/The Global Menu should not appear$/, () => {
    homePage.verifyNoTabMenu();
});

Then(/^I should see the scottish widows help desk number under What can I do next option$/, () => {
    scottishWidowsFAQPage.waitForPageLoad();
    scottishWidowsFAQPage.shouldSeeScottishWidowsHelpdeskNumber();
});
