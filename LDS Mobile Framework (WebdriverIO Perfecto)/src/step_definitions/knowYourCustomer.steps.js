const {When, Then} = require('cucumber');

const KycReviewYourContactPage = require('../pages/knowYourCustomer/kycReviewYourContact.page');
const PersonalDetailsPage = require('../pages/settings/personalDetails/personalDetails.page');

const kycReviewYourContactPage = new KycReviewYourContactPage();
const personalDetails = new PersonalDetailsPage();

When(/^I should be on the review your contacts page$/, () => {
    kycReviewYourContactPage.waitForPageLoad();
});

When(/^I enter mobile number "(.*)"$/, (mobileNumber) => {
    kycReviewYourContactPage.enterMobileNumber(mobileNumber);
});

When(/^I click submit button$/, () => {
    kycReviewYourContactPage.clickConfirmButton();
});

When(/^I am able to view the existing details$/, (table) => {
    const tableData = table.hashes();
    kycReviewYourContactPage.validateDetails(tableData);
});

When(/^I am able to view options$/, (table) => {
    const tableData = table.hashes();
    kycReviewYourContactPage.validateOptions(tableData);
});

When(/^I choose to update my details later without edit$/, () => {
    kycReviewYourContactPage.clickUpdateUpdateLaterButton();
    kycReviewYourContactPage.clickNotNowButton();
});

Then(/^I should be able to view the updated details on personal details page$/, (table) => {
    const tableData = table.hashes();
    personalDetails.validateKycDetails(tableData);
});

When(/^I should be on kyc confirmation page$/, () => {
    kycReviewYourContactPage.waitForPageLoad();
});
