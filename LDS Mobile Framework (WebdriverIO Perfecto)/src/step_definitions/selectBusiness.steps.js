const {When} = require('cucumber');
const SelectBusinessPage = require('../pages/selectBusiness.page');

When(/^I select my first business$/, () => {
    const selectBusinessPage = new SelectBusinessPage();
    selectBusinessPage.selectFirstBusiness();
});
