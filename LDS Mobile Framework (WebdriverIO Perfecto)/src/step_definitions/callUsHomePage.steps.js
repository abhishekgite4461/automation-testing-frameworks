const {Given, When, Then} = require('cucumber');

const CallUsHomePage = require('../pages/helpAndInfo/callUsHomePage.page');
const HomePage = require('../pages/home.page');
const CommonPage = require('../pages/common.page');
const CallUsReason = require('../enums/callUsReason.enum');
const SupportHubHomePage = require('../pages/supportHubHomePage.page');

const callUsHomePage = new CallUsHomePage();
const homePage = new HomePage();
const commonPage = new CommonPage();

const supportHubHomePage = new SupportHubHomePage();

Then(/^I should be on the call us home page$/, () => {
    callUsHomePage.waitForPageLoad();
});

When(/^I navigate to call us home page$/, () => {
    homePage.isMoreThanOneAccount();
    homePage.selectTab('support');
    supportHubHomePage.selectCallUs();
    callUsHomePage.waitForPageLoad();
});

Then(/^I should be on pre auth call us home page overlay$/, () => {
    callUsHomePage.waitForPageLoad();
});

Then(/^I should not see new product enquiries tile$/, () => {
    callUsHomePage.shouldNotDisplayNewProductEnquireTile();
});

Then(/^I should see security and travel emergency label$/, () => {
    callUsHomePage.shouldDisplaySecurityAndTravelLabel();
});

Given(/^I select your accounts tile on call us home page$/, () => {
    callUsHomePage.clickOnYourAccountsTile();
});

Given(/^I select new product enquiry tile on call us home page$/, () => {
    callUsHomePage.clickOnNewEnquiriesTile();
});

Then(/^I should be able to view close button on call us page/, () => {
    commonPage.verifyCloseModal();
});

Then(/^I should be able to view native back button$/, () => {
    callUsHomePage.shouldDisplayBackButton();
});

Then(/^I should be able to view these call us tiles$/, (table) => {
    const tableData = table.hashes();
    callUsHomePage.validateTiles(tableData);
});

Then(/^I should see the reasons to call options$/, (table) => {
    const tableData = table.hashes();
    callUsHomePage.validateReasonToCall(tableData);
});

Then(/^I should not see CMA Tile in the call us page$/, () => {
    callUsHomePage.shouldNotDisplayCMASurveyLink();
});

When(/^I should see CMA Tile in the call us page$/, () => {
    callUsHomePage.shouldDisplayCMASurveyLink();
});

const selectTilesInCallUsHomePage = function (type) {
    const reason = CallUsReason.fromStringByEscapingSpace(type);
    callUsHomePage.selectReasonForCall(reason, type);
};

When(/^I select the (.*) tile on call us home page$/, selectTilesInCallUsHomePage); // call us page for auth

When(/^I select the (.*) tile on call us home page overlay$/, selectTilesInCallUsHomePage); // call us page for unauth appears as overlay

Then(/^I select close button on pre auth call us home page overlay/, () => {
    commonPage.closeModal();
});

Then(/^I should not see the reasons to call options$/, (table) => {
    const tableData = table.hashes();
    callUsHomePage.validateReasonToCall(tableData);
});

Then(/^I should not see security and travel emergency label$/, () => {
    callUsHomePage.shouldDisplaySecurityAndTravelLabel();
});

Then(/^I am shown the list of sales option$/, (table) => {
    const labels = table.hashes();
    callUsHomePage.validateNewProductOptions(labels);
});

Then(/^I select individual sales option on new product page and validate callus page$/, (table) => {
    const labels = table.hashes();
    callUsHomePage.validateNewProductCallusPage(labels);
});

Then(/^I am displayed the CMA survey page in my mobile browser$/, () => {
    callUsHomePage.shouldSeeCMASurveyResult();
});

Then(/^I should not see your accounts tile in the call us page$/, () => {
    callUsHomePage.shouldNotSeeYourAccountsTile();
});
