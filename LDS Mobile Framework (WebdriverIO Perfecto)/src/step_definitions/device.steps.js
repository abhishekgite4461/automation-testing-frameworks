const {When} = require('cucumber');

const EnvironmentPage = require('../pages/environment.page');
const Server = require('../enums/server.enum');
const Actions = require('../pages/common/actions');
const DeviceHelper = require('../utils/device.helper');

When(/^I reinstall the app and select environment$/, function () {
    DeviceHelper.ensureCleanAppInstall(this.appConfigurationRepository.get());
    const server = Server.fromString(browser.capabilities.server);
    const environmentPage = new EnvironmentPage();
    environmentPage.selectEnvironment(server);
});

When(/^I reinstall baseline app$/, () => {
    browser.appURI = (() => 'PUBLIC:baseline/baseline.ipa')();
    DeviceHelper.perfectoAppInstall(false, false);
    browser.execute('mobile:application:open', {identifier: 'com.lbg.mobile.baseline'});
});

When(/^I reinstall the app$/, function () {
    DeviceHelper.ensureCleanAppInstall(this.appConfigurationRepository.get(), false, true);
});

When(/^I relaunch the app$/, () => {
    DeviceHelper.relaunchApp(true);
});

When(/^I reboot my device$/, () => {
    DeviceHelper.rebootDevice();
});

const selectServerStub = function (platform, appName) {
    DeviceHelper.relaunchApp(true);
    const server = {
        testPhase: 'STUB',
        platform: platform.toString()
            .replace(/_/g, ' '),
        appName: appName.toString()
            .replace(/_/g, ' ')
    };
    const environmentPage = new EnvironmentPage();
    environmentPage.selectEnvironment(server);
};

When(/^I relaunch the app and select platform (.*) and app (.*)$/, selectServerStub);

When(/^I select back key on the device$/, () => {
    Actions.pressAndroidBackKey();
});

When(/^I select back key on the device (.*) times$/, (noOfTimes) => {
    for (let i = 0; i < noOfTimes; i++) {
        Actions.pressAndroidBackKey();
    }
});

When(/^I have launched unenrolled application$/, function () {
    DeviceHelper.ensureCleanAppInstall(this.appConfigurationRepository.get(), false, false);
});
