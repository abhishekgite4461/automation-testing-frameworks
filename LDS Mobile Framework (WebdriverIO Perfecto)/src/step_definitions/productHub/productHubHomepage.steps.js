const {When, Then} = require('cucumber');
const ProductHubHomepage = require('../../pages/productHub/productHubHomepage.page');
const BusinessLoanPage = require('../../pages/productHub/businessLoan.page.js');
const ProductHubLandingSavingsApplyPage = require('../../pages/productHub/productHubLandingSavingsApply.page');

const productHubHomepage = new ProductHubHomepage();
const businessLoanPage = new BusinessLoanPage();
const productHubLandingSavingsApplyPage = new ProductHubLandingSavingsApplyPage();

Then(/^I should see the product hub home page with product categories$/, (table) => {
    const products = table.hashes();
    for (const product of products) {
        productHubHomepage.shouldShowLabel(product.productCategoryLabels);
    }
});

Then(/^"(.*)" category should display the below products$/, (productType, options) => {
    const productCategory = ProductHubHomepage.getProductByString(productType);
    productHubHomepage.selectProductCategory(productCategory);
    const products = options.hashes();
    for (const product of products) {
        productHubHomepage.shouldShowLabel(product.productLabels);
    }
});

Then(/^the Featured option is shown as expanded$/, () => {
    productHubHomepage.featureExpanded();
});

Then(/^the other options are shown as closed accordion$/, () => {
    productHubHomepage.businessCurrentAccountCollapsed();
});

Then(/^I select overdrafts option in the product hub page$/, () => {
    productHubHomepage.selectOverdraftsOptions();
});

Then(/^I select business loan from the product categories$/, () => {
    productHubHomepage.selectBusinessLoanOption();
});

Then(/^I should be navigated to business loan mobile browser$/, () => {
    businessLoanPage.waitForPageLoad();
});

When(/^I select Accept All cookies in webview$/, () => {
    productHubLandingSavingsApplyPage.selectAcceptAll();
});
