const {When} = require('cucumber');
const ProductHubOverdraftsApplyPage = require('../../pages/productHub/productHubOverdraftsApply.page');

const productHubOverdraftsApplyPage = new ProductHubOverdraftsApplyPage();

When(/^I navigate to product hub overdraft page$/, () => {
    productHubOverdraftsApplyPage.verifyOverdraftsPageTitle();
});

When(/^I select apply on product hub overdraft page$/, () => {
    productHubOverdraftsApplyPage.selectApplyNowButton();
});

When(/^I navigate to Overdrafts details page$/, () => {
    productHubOverdraftsApplyPage.verifyOverdraftsPageDetails();
});

When(/^I enter overdraft amount (.*) on the product hub overdraft page$/, (amount) => {
    productHubOverdraftsApplyPage.enterOverdraftAmount(amount);
});

When(/^I enter monthly after tax income (.*) on overdraft page$/, (aftertax) => {
    productHubOverdraftsApplyPage.enterMonthlyIncomeAfterTax(aftertax);
});

When(/^I select employment status (.*) on overdraft page$/, (status) => {
    productHubOverdraftsApplyPage.selectEmploymentStatus(status);
});

When(/^I enter monthly outgoings (.*) on overdraft page$/, (expenses) => {
    productHubOverdraftsApplyPage.enterMonthlyOutgoings(expenses);
});

When(/^I enter childcare cost (.*) on overdraft page$/, (childcare) => {
    productHubOverdraftsApplyPage.enterChildCareCost(childcare);
});

When(/^I select life changing event$/, () => {
    productHubOverdraftsApplyPage.selectLifeChangingEvent();
});

When(/^I submit the application for overdraft$/, () => {
    productHubOverdraftsApplyPage.clickSubmitButton();
});

When(/^I should see overdraft decline page$/, () => {
    productHubOverdraftsApplyPage.verifyOverdraftDeclinePage();
});

When(/^I should see help section on apply overdraft page$/, () => {
    productHubOverdraftsApplyPage.verifyHelpAndInfoButton();
});

When(/^I select need help button on apply overdraft page$/, () => {
    productHubOverdraftsApplyPage.selectHelpandInfoButton();
});

When(/^I should see the below details in help and support section$/, (table) => {
    const tableData = table.hashes();
    productHubOverdraftsApplyPage.validateHelpAndSupportSection(tableData);
});

When(/^I select find your nearest branch on apply overdraft page$/, () => {
    productHubOverdraftsApplyPage.selectFindYourNearestBranch();
});

When(/^I select leave app on apply overdraft page$/, () => {
    productHubOverdraftsApplyPage.selectLeaveApp();
});

When(/^I select let me just type link on the overdraft page$/, () => {
    productHubOverdraftsApplyPage.selectLetMeJustTypeLink();
});

When(/^I enter overdraft days (.*) on the product hub overdraft page$/, (days) => {
    productHubOverdraftsApplyPage.enterOverdraftDays(days);
});

When(/^I select pay back button on overdraft page$/, () => {
    productHubOverdraftsApplyPage.selectPayBackNoButton();
});

When(/^I navigate to Overdrafts partner details page$/, () => {
    productHubOverdraftsApplyPage.verifyOverdraftsPartnerPageDetails();
});

When(/^I enter overdraft amount for joint account (.*) on product hub overdraft page$/, (aftertax) => {
    productHubOverdraftsApplyPage.enterMonthlyIncomeAfterTaxJoint(aftertax);
});

When(/^I enter monthly outgoings for joint account (.*) on product hub overdraft page$/, (expenses) => {
    productHubOverdraftsApplyPage.enterMonthlyOutgoingsJoint(expenses);
});

When(/^I enter childcare cost for joint account (.*) on product hub overdraft page$/, (childcare) => {
    productHubOverdraftsApplyPage.enterChildCareCostJoint(childcare);
});
