const {When} = require('cucumber');
const ProductHubLandingSavingsApplyPage = require('../../pages/productHub/productHubLandingSavingsApply.page');
const ProductHubSelectProductPage = require('../../pages/productHub/productHubSelectProduct.page');
const DeviceHelper = require('../../utils/device.helper');
const HomePage = require('../../pages/home.page');

const productHubSelectProductPage = new ProductHubSelectProductPage();
const productHubLandingSavingsApplyPage = new ProductHubLandingSavingsApplyPage();
const homePage = new HomePage();

When(/^I select (.*) account on compare savings account page$/, function (accountName) {
    const data = this.dataRepository.get();
    productHubSelectProductPage.waitForPageLoad();
    productHubSelectProductPage.selectSavingsAccount(data[accountName.replace(/ /g, '')]);
    productHubSelectProductPage.startApplyingAccount();
});

When(/^I select required Other account on compare savings account page$/, () => {
    productHubSelectProductPage.selectOtherSavingsAccount();
});

When(/^I select apply now to open savings account$/, () => {
    productHubLandingSavingsApplyPage.selectApplyNow();
});

When(/^I select terms and conditions of savings account openings$/, () => {
    productHubLandingSavingsApplyPage.selectTermsandConditions();
});

When(/^I select Open Account on Savings Account Openings Page$/, () => {
    productHubLandingSavingsApplyPage.selectOpenAccount();
});

When(/^I should see that (.*) account is open$/, function (accountName) {
    const data = this.dataRepository.get();
    productHubLandingSavingsApplyPage.verifyAccountOpening(data[accountName.replace(/ /g, '')]);
});

When(/^I check account number and sort code are displayed on success page and home page$/, () => {
    productHubLandingSavingsApplyPage.verifyAccountAndSortCode();
    let strAccNum;
    if (DeviceHelper.isIOS()) {
        strAccNum = productHubLandingSavingsApplyPage.getAccountNumber();
    } else {
        const strValue = productHubLandingSavingsApplyPage.getSortCodeandNumberNo();
        strAccNum = strValue.match(/[0-9]{8}/g)[0];
    }
    homePage.navigateToHomePage();
    productHubLandingSavingsApplyPage.verifyAccountDetails(strAccNum);
});
