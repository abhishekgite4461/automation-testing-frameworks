const {When, Then} = require('cucumber');
const ProductHubLoanApplyPage = require('../../pages/productHub/productHubLoanApply.page');

const productHubLoanApplyPage = new ProductHubLoanApplyPage();

When(/^I should be on product hub loan page$/, () => {
    productHubLoanApplyPage.waitForPageLoad();
});

When(/^I enter loan amount (.*) and term (.*) on the product hub loan page$/, (amount, term) => {
    productHubLoanApplyPage.enterLoanAmountField(amount);
    productHubLoanApplyPage.enterLoanTermField(term);
});

When(/^I select Loan purpose drop down on the product hub loan page$/, () => {
    productHubLoanApplyPage.selectLoanPurpose();
});

When(/^I select flexible loan option on the product hub loan page$/, () => {
    productHubLoanApplyPage.selectFlexibleLoan();
});

When(/^I navigate to flexible loan repayment option page$/, () => {
    productHubLoanApplyPage.verifyRepaymentOptionPage();
});

When(/^I select closest match option$/, () => {
    productHubLoanApplyPage.selectClosestMatchOption();
});

When(/^I navigate to your application page$/, () => {
    productHubLoanApplyPage.verifyYourApplicationPage();
});

When(/^I select employment status drop down on your application page$/, () => {
    productHubLoanApplyPage.selectEmploymentStatus();
});

When(/^I select employment status value from the drop down$/, () => {
    productHubLoanApplyPage.selectEmploymentLoanPurposeDropDownOption();
});

When(/^I enter monthly income (.*) on your application page$/, (income) => {
    productHubLoanApplyPage.enterMonthlyIncome(income);
});

When(/^I enter monthly outgoings (.*) on your application page$/, (outgoing) => {
    productHubLoanApplyPage.enterMonthlyOutgoings(outgoing);
});

When(/^I enter dependent (.*) on your application page$/, (dependents) => {
    productHubLoanApplyPage.enterDependents(dependents);
});

When(/^I enter childcare cost (.*) on your application page$/, (childecost) => {
    productHubLoanApplyPage.enterChildCareCosts(childecost);
});

When(/^I select terms and conditions and submit the loan application to check eligibility$/, () => {
    productHubLoanApplyPage.selectAcknowledgeCheckBox();
    productHubLoanApplyPage.selectSubmitButton();
});

When(/^I should be on loan decline page$/, () => {
    productHubLoanApplyPage.verifyLoanDeclinePage();
});

When(/^I select Book an Appointment option on product hub loan page$/, () => {
    productHubLoanApplyPage.selectBookAppointment();
});

When(/^I should be on find a branch page$/, () => {
    productHubLoanApplyPage.verifyFindBranchPage();
});

When(/^I enter (.*) and select find a branch$/, function (postCode) {
    const data = this.dataRepository.get();
    productHubLoanApplyPage.enterPostCodeAndFindBranch(data[postCode]);
});

When(/^I should be shown branch (.*) results$/, function (postCode) {
    const data = this.dataRepository.get();
    productHubLoanApplyPage.verifyBranchResults(data[postCode]);
});

When(/^I select and continue on find a branch page$/, () => {
    productHubLoanApplyPage.selectContinueFindBranch();
});

When(/^I should be on choose an appointment time page$/, () => {
    productHubLoanApplyPage.verifyAppointmentTimePage();
});

Then(/^I should be on the apply loan page$/, () => {
    productHubLoanApplyPage.shouldVerifyApplyLoanPage();
});

When(/^I select continue button to apply loan$/, () => {
    productHubLoanApplyPage.selectApplyLoanContinueButton();
});

When(/^I should be on the loan purpose page$/, () => {
    productHubLoanApplyPage.shouldVerifyLoanPurposePage();
});

When(/^I select Loan purpose option on the loan purpose page$/, () => {
    productHubLoanApplyPage.selectLoanPurposeOption();
});

When(/^I select continue button to apply loan on loan purpose page$/, () => {
    productHubLoanApplyPage.selectLoanPurposeContinueButton();
});

When(/^I should be on the loan calculator page$/, () => {
    productHubLoanApplyPage.shouldVerifyLoanCalculatorPage();
});

When(/^I enter loan (.*) and (.*)for calculating my loan eligibility on loan calculator page$/, (amount, term) => {
    productHubLoanApplyPage.enterLoanAmountField(amount);
    productHubLoanApplyPage.enterLoanTermField(term);
});

When(/^I select calculate button on loan calculator page$/, () => {
    productHubLoanApplyPage.selectApplyLoanCalculateButton();
});

When(/^I should see the future change option on loan calculator page$/, () => {
    productHubLoanApplyPage.shouldVerifyFutureChangeOption();
});

When(/^I select future change option on loan calculator page$/, () => {
    productHubLoanApplyPage.selectFutureChangeOption();
});

When(/^I select continue application on loan calculator page$/, () => {
    productHubLoanApplyPage.selectContinueApplicationButton();
});

When(/^I should be on your application page$/, () => {
    productHubLoanApplyPage.shouldVerifyYourApplicationPage();
});

When(/^I select regular monthly income option on your application page$/, () => {
    productHubLoanApplyPage.selectRegularMonthlyIncome();
});

When(/^I select spending and expenses closest match option$/, () => {
    productHubLoanApplyPage.selectClosestMatchOption();
});

When(/^I select childcare cost option on your application page$/, () => {
    productHubLoanApplyPage.selectChildCareCosts();
});

When(/^I select contact details closest match option$/, () => {
    productHubLoanApplyPage.selectContactDetailsClosestMatchOption();
});
