const {When, Then} = require('cucumber');
const DepositHistoryPage = require('../../pages/ics/depositHistory.page');

const depositHistoryPage = new DepositHistoryPage();

When(/^I should be able to see appropriate error message on deposit history page$/, (table) => {
    const tableData = table.hashes();
    depositHistoryPage.verifyHistoryDisabledErrors(tableData);
});

When(/^I change the account which is displayed in the history page$/, () => {
    depositHistoryPage.clickAccountNavigation();
    depositHistoryPage.checkAndSelectAccount();
});

Then(/^I should see history details for the newly selected account$/, () => {
    depositHistoryPage.verifyChangedAccount();
});

When(/^I should be on deposit history page$/, () => {
    depositHistoryPage.verifyDepositHistoryPage();
});

When(/^I am on deposit history page$/, () => {
    depositHistoryPage.verifyDepositHistoryPage();
});

When(/^I tap on deposit cheque button from deposit history page$/, () => {
    depositHistoryPage.selectDepositCheque();
});

When(/^I select an item in deposit history list$/, () => {
    depositHistoryPage.selectDepositItem();
});

When(/^I see the below fields on deposit history page$/, (table) => {
    const tableData = table.hashes();
    depositHistoryPage.verifyDepositHistoryField(tableData);
});

When(/^I see the below account picker fields on deposit history page$/, (table) => {
    const tableData = table.hashes();
    depositHistoryPage.verifyDepositHistoryAccountPicker(tableData);
});

When(/^I finish scrolling through all available transactions$/, () => {
    depositHistoryPage.scrollToLastTransactionDetail();
});

Then(/^I should see a message that I have reached the end of available transactions$/, () => {
    depositHistoryPage.verifyEndOfTransactionMessage();
});

Then(/^I should see no transactions message in the deposit history page$/, () => {
    depositHistoryPage.verifyNoTransactionMessage();
});

Then(/^I am able to view the (.*) in the transaction history$/, (reference) => {
    depositHistoryPage.verifyReferenceText(reference);
});

Then(/^I am not able to view the (.*) in the transaction history$/, (reference) => {
    depositHistoryPage.verifyReferenceTextIsNotVisible(reference);
});

When(/^I select the pending cheque details page$/, () => {
    depositHistoryPage.selectPendingTransaction();
});

When(/^I select the funds available cheque details page$/, () => {
    depositHistoryPage.selectFundAvailableTransaction();
});

When(/^I select the rejected cheque details page$/, () => {
    depositHistoryPage.selectRejectedTransaction();
});
