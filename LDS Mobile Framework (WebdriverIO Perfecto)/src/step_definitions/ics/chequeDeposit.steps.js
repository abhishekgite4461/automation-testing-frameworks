const {When, Then} = require('cucumber');
const ChequeDepositPage = require('../../pages/ics/chequeDeposit.page');
const IcsMoreInformationPage = require('../../pages/ics/icsMoreInformation.page');
const ReviewDepositPage = require('../../pages/ics/reviewDeposit.page.js');
const ChequeDepositStatusPage = require('../../pages/ics/chequeDepositStatus.page');
const DepositChequeImagePage = require('../../pages/ics/depositChequeImage.page');
const LogOffAlertPopUpPage = require('../../pages/settings/logOffAlertPopUp.page');
const DepositHistoryPage = require('../../pages/ics/depositHistory.page');

const depositHistoryPage = new DepositHistoryPage();
const logOffAlertPopUpPage = new LogOffAlertPopUpPage();
const reviewDepositPage = new ReviewDepositPage();
const chequeDepositStatusPage = new ChequeDepositStatusPage();
const depositChequeImagePage = new DepositChequeImagePage();
const icsMoreInformationPage = new IcsMoreInformationPage();
const chequeDepositPage = new ChequeDepositPage();

const depositCheque = function (chequeImage, chequeAmount, referenceText) {
    chequeDepositPage.enterChequeAmount(chequeAmount);
    chequeDepositPage.enterChequeReference(referenceText);
    chequeDepositPage.selectFrontOfCheque();
    chequeDepositPage.startInjectImage(chequeImage);
    chequeDepositPage.selectReviewDepositButton();
};

const confirmChequeDeposit = function () {
    const data = this.dataRepository.get();
    reviewDepositPage.selectReviewConfirmButton();
    reviewDepositPage.checkForPasswordConfirmation(data.password);
    reviewDepositPage.checkForAmountMismatchConfirmation();
    reviewDepositPage.checkForPasswordConfirmation(data.password);
};

const captureCheque = function (chequeImage) {
    chequeDepositPage.selectFrontOfCheque();
    chequeDepositPage.startInjectImage(chequeImage);
};

When(/^I see below fields on cheque deposit page$/, (table) => {
    const tableData = table.hashes();
    chequeDepositPage.verifyChequeDepositField(tableData);
});

When(/^I see below fields on review deposit page$/, (table) => {
    const tableData = table.hashes();
    reviewDepositPage.verifyReviewDepositField(tableData);
});

When(/^I see below fields on deposit success screen$/, (table) => {
    const tableData = table.hashes();
    chequeDepositStatusPage.verifyChequeDepositSuccessField(tableData);
});

When(/^I should be on the cheque deposit screen$/, () => {
    chequeDepositPage.checkMoreInformationAlert();
    chequeDepositPage.verifyChequeDepositPage(true);
});

When(/^I should be able to view the cheque transaction and daily limits from cheque deposit page$/, () => {
    chequeDepositPage.verifyChequeLimitService();
});

When(/^I enter correct cheque transaction limit (.*)$/, (validAmount) => {
    chequeDepositPage.enterChequeAmount(validAmount);
});

When(/^I enter cheque amount less than minimum (.*)$/, (invalidAmount) => {
    chequeDepositPage.enterChequeAmount(invalidAmount);
    chequeDepositPage.selectReferenceField();
});

When(/^I should not be displayed with entered (.*)$/, (invalidAmount) => {
    chequeDepositPage.invalidChequeAmountValidation(invalidAmount);
});

When(/^I enter an amount more than the ics mobile cheque deposit transaction limit (.*)$/, (chequeMaxAmount) => {
    chequeDepositPage.enterChequeAmount(chequeMaxAmount);
});

When(/^I should see transaction limit error message in cheque deposit page$/, () => {
    chequeDepositPage.limitErrorMessageValidation(true);
});

When(/^I select the cheque amount field$/, () => {
    chequeDepositPage.selectChequeAmountFeild();
});

When(/^I select the cheque reference field$/, () => {
    chequeDepositPage.selectReferenceField();
});

When(/^I should see help text with Cheque Max and Daily limit$/, () => {
    chequeDepositPage.verifyChequeAmountFeildHelpText();
});

When(/^I should see reference help text as per copy deck$/, () => {
    chequeDepositPage.verifyReferenceFeildHelpText();
});
When(/^I enter correct alphanumeric characters of length 18 as (.*)$/, (reference) => {
    chequeDepositPage.enterChequeReference(reference);
});

When(/^I select the account navigation icon in deposit cheque page$/, () => {
    chequeDepositPage.waitForChequeDepositTabEnabled();
    chequeDepositPage.selectAccountNavigationIcon();
});

When(/^I should be on deposit cheque page$/, () => {
    chequeDepositPage.verifyDepositChequeDisabledPage();
});

When(/^I tap on deposit history from deposit cheque page$/, () => {
    chequeDepositPage.selectDepositHistory();
});

When(/^I should be able to see appropriate error message on deposit cheque page$/, (table) => {
    const tableData = table.hashes();
    chequeDepositPage.verifyDepositDisabledErrors(tableData);
});

When(/^I should see the checkbox for information dialogue is not selected$/, () => {
    icsMoreInformationPage.verifyChequeBoxSelectivity(false);
    icsMoreInformationPage.selectOkButton();
});

When(/^I should see Information dialogue box popped up$/, () => {
    icsMoreInformationPage.shouldSeeInformationDialogBox(true);
});

When(/^I select ok button from more information dialog$/, () => {
    icsMoreInformationPage.selectOkButton();
});

When(/^I see information regarding all cheques not being accepted$/, () => {
    icsMoreInformationPage.openMoreInformationDialogBox();
    icsMoreInformationPage.selectOkButton();
});

When(/^I should see the checkbox for information dialogue is selected$/, () => {
    icsMoreInformationPage.selectCheckBox();
    icsMoreInformationPage.selectOkButton();
});

When(/^I should not be displayed information regarding all cheques being supported$/, () => {
    icsMoreInformationPage.viewMoreInformationText(true);
});

When(/^I select More information text in cheque deposit page$/, () => {
    icsMoreInformationPage.selectMoreInformationText();
});

When(/^I select on the front of camera$/, () => {
    chequeDepositPage.selectFrontOfCheque();
});

When(/^I capture both front and back valid cheque image$/, () => {
    chequeDepositPage.startInjectImage();
});

When(/^I see the deposit cheque screen is displayed with thumbnail images$/, () => {
    chequeDepositPage.verifyThumbnailImages();
});

When(/^I select 'review deposit' button$/, () => {
    chequeDepositPage.selectReviewDepositButton();
});

When(/^I should be able to see review deposit screen$/, () => {
    reviewDepositPage.verifyReviewDepositPage();
});

When(/^I select thumbnail for front image$/, () => {
    reviewDepositPage.selectThumbnailImageFront();
});

When(/^I select thumbnail for back image$/, () => {
    reviewDepositPage.selectThumbnailImageBack();
});

When(/^I should be able to see the full screen image$/, () => {
    depositChequeImagePage.verifyEnlargedImage();
});

When(/^I select confirm button in review deposit page$/, function () {
    const data = this.dataRepository.get();
    reviewDepositPage.selectReviewConfirmButton();
    reviewDepositPage.checkForPasswordConfirmation(data.password);
});

When(/^I should be able to see deposit success screen$/, () => {
    chequeDepositStatusPage.verifyDepositSuccessPage();
});

When(/^I select confirm button if popup screen displayed for amount mismatch$/, () => {
    reviewDepositPage.confirmAmountMismatch();
});

When(/^I should be able to see deposit success or failure screen$/, () => {
    chequeDepositStatusPage.verifyDepositSuccessOrFailurePage();
});

When(/^I select on the front of camera button$/, () => {
    chequeDepositPage.selectFrontCamera();
});

When(/^I select allow in camera permission pop up$/, () => {
    chequeDepositPage.allowCameraPermission();
});

When(/^I close the ics view demo screen$/, () => {
    chequeDepositPage.closeDemo();
});

When(/^I should be allowed to capture an image$/, () => {
    chequeDepositPage.verifyCameraEnabled();
});

When(/^I deny in camera permission pop up and select front of camera$/, () => {
    chequeDepositPage.denyCameraPermission();
});

When(/^I select the checkbox and click deny again$/, () => {
    chequeDepositPage.selectCheckBoxCameraPermission();
});

When(/^dialog box should pop up with settings option to enable camera$/, () => {
    chequeDepositPage.verifySettingCameraPermission();
});

When(/^goto settings button should be enabled to give camera permission$/, () => {
    chequeDepositPage.selectSettings();
});

When(/^I select the back header option in the cheque deposit page$/, () => {
    chequeDepositPage.selectHomeBackButton();
});

Then(/^I should see winback option in the middle of the screen with stay and leave option$/, () => {
    chequeDepositPage.shouldSeeOptionsInWinback();
});

When(/^on selection of stay option I should stay on cheque deposit homepage$/, () => {
    chequeDepositPage.verifyWinBackOnNegativeAction();
});

When(/^on selection of leave option I should navigate to homepage$/, () => {
    chequeDepositPage.verifyWinBackOnPositiveAction();
});

When(/^I should see an amount mismatch popup$/, () => {
    reviewDepositPage.verifyAmountMismatchPopUp();
});

When(/^I deposit a cheque (.*) of (.*) with (.*)$/, depositCheque);

When(/^I capture front and back of cheque (.*)$/, captureCheque);

When(/^I deposit a cheque (.*) of (.*) without (.*)$/, depositCheque);

When(/^I confirm the cheque deposit$/, confirmChequeDeposit);

When(/^I deposit a valid cheque (.*) of (.*) with (.*)$/, (chequeImage, chequeAmount, referenceText) => {
    chequeDepositPage.enterChequeAmount(chequeAmount);
    chequeDepositPage.enterChequeReference(referenceText);
    chequeDepositPage.selectFrontOfCheque();
    chequeDepositPage.startInjectImage(chequeImage);
});

When(/^I click on the deposit another cheque button$/, () => {
    chequeDepositStatusPage.selectDepositAnotherCheque();
});

When(/^I click on the view deposit history button$/, () => {
    chequeDepositStatusPage.selectDepositHistoryButton();
});

When(/^I should see thumbnail images in deposit screen$/, () => {
    chequeDepositPage.verifyThumbnailImages();
});

When(/^I select review deposit button$/, () => {
    chequeDepositPage.selectReviewDepositButton();
});

When(/^I click on the cancel button on the review deposit page$/, () => {
    reviewDepositPage.selectReviewCancelButton();
});

When(/^on selection of confirm button I should see deposit success screen$/, function () {
    const data = this.dataRepository.get();
    reviewDepositPage.confirmAmountMismatch();
    reviewDepositPage.checkForPasswordConfirmation(data.password);
    chequeDepositStatusPage.verifyDepositSuccessPage();
});

When(/^on selection of cancel button I should navigate to deposit home screen$/, function () {
    const data = this.dataRepository.get();
    reviewDepositPage.cancelAmountMismatch();
    reviewDepositPage.checkForPasswordConfirmation(data.password);
    chequeDepositPage.verifyChequeDepositPage(true);
});

Then(/^I select the logoff button 15 seconds before the "(.*)" auto logoff time in sdk$/, (autoLogOffTime) => {
    ChequeDepositPage.autoLogOffPauseTimer(autoLogOffTime);
    logOffAlertPopUpPage.waitForPageLoadOnChequeScanningPage();
});

When(/^I should be able to see the fast cheque icon in deposit success screen$/, () => {
    chequeDepositStatusPage.verifyDepositSuccessPage();
    chequeDepositStatusPage.selectDepositHistoryButton();
    depositHistoryPage.verifyHelpandInfottext();
});

When(/^I should be able to see the fast cheque icon in cheque deposit page$/, () => {
    chequeDepositPage.verifyFastChequeLogo();
});

When(/^I select close button after desired "(.*)" from camera sdk page$/, (autoLogOffTime) => {
    chequeDepositPage.verifyCameraEnabled();
    ChequeDepositPage.autoLogOffPauseTimer(autoLogOffTime);
    chequeDepositPage.closeCameraButton();
});

When(/^I should be able to see the invalid sort code error message$/, () => {
    chequeDepositStatusPage.verifySortCodeErrorMessage();
});

When(/^I should receive an duplicate cheque error message on the failure screen for duplicate cheque (.*)$/, (chequeOrder) => {
    chequeDepositStatusPage.verifyDuplicateErrors(chequeOrder);
});

When(/^I deposit cheques (.*) of (.*) with (.*) for daily limit validation$/, (chequeImage, chequeAmount, referenceText) => {
    depositCheque(chequeImage, chequeAmount, referenceText);
    confirmChequeDeposit();
});

When(/^I should receive an daily limit error message for last cheque (.*)$/, (chequeOrder) => {
    chequeDepositStatusPage.verifyDailyLimitError(chequeOrder);
});

When(/^I should be able to view the 18 characters in reference$/, () => {
    chequeDepositPage.verifyReferenceMaxLength();
});

When(/^I capture the main balance from account tile$/, () => {
    chequeDepositPage.getMainBalance();
});

When(/^I navigate back to cheque deposit home screen$/, () => {
    chequeDepositStatusPage.setDepositedAmount();
    chequeDepositStatusPage.selectDepositAnotherCheque();
});

When(/^I should see an updated deferred balance$/, () => {
    chequeDepositPage.verifyUpdatedDeferredBalance();
});

When(/^I select settings and enable camera permission$/, () => {
    chequeDepositPage.selectSettingsOnCameraDialog();
    chequeDepositPage.enableCameraPermissionOnSettings();
    chequeDepositPage.navigateChequeScreenFromSettings();
});

When(/^I should be able to use camera on cheque deposit page$/, () => {
    chequeDepositPage.selectFrontCamera();
    chequeDepositPage.closeDemo();
    chequeDepositPage.closeCameraButton();
});

When(/^I close the sdk camera$/, () => {
    chequeDepositPage.closeCameraButton();
});

When(/^I should not see camera permission pop up$/, () => {
    chequeDepositPage.shouldNotDisplayCameraPermissionDialog();
});

When(/^I should not be shown enabled review deposit button$/, () => {
    chequeDepositPage.verifyReviewDepositButtonisNotEnabled();
});

When(/^I should be shown enabled review deposit button$/, () => {
    chequeDepositPage.verifyReviewDepositButtonisEnabled();
});

When(/^I should see cheque thumbnail images in deposit screen$/, () => {
    chequeDepositPage.verifyChequeThumbnailImages();
});
