const {Then} = require('cucumber');
const ChequeDepositAccountTilePage = require('../../pages/ics/chequeDepositAccountTile.page');

const chequeDepositAccountTilePage = new ChequeDepositAccountTilePage();

Then(/^I should be shown only the eligible accounts for depositing a cheque$/, () => {
    chequeDepositAccountTilePage.verifyEligibleAccountsInAccountTile();
});
