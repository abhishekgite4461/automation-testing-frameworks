const {When, Then} = require('cucumber');
const DepositChequeImagePage = require('../../pages/ics/depositChequeImage.page');

const depositChequeImagePage = new DepositChequeImagePage();

Then(/^I should be able to see the enlarged cheque image$/, () => {
    depositChequeImagePage.verifyEnlargedImage();
});

When(/^I close the cheque image$/, () => {
    depositChequeImagePage.closeChequeScreen();
});
