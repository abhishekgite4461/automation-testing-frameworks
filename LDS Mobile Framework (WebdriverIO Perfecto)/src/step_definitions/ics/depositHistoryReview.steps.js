const {When, Then} = require('cucumber');
const DepositHistoryReviewPage = require('../../pages/ics/depositHistoryReview.page');

const depositHistoryReviewPage = new DepositHistoryReviewPage();

Then(/^I should see the hint to tap to view enlarged image$/, () => {
    depositHistoryReviewPage.verifyHintTapImage();
});

When(/^I select the cheque image$/, () => {
    depositHistoryReviewPage.selectFrontImage();
});

Then(/^I see the below fields in deposit review page$/, (table) => {
    const tableData = table.hashes();
    depositHistoryReviewPage.verifyDepositReviewFields(tableData);
});

When(/^I should return to the history review of the selected cheque$/, () => {
    depositHistoryReviewPage.verifyHintTapImage();
});

When(/^I should be shown message mapped to status code related to pending received from Clearing Status field$/, () => {
    depositHistoryReviewPage.transactionReasonForStatusPendingCheque();
});

When(/^I should be shown message mapped to status code related to funds available received from Clearing Status field$/, () => {
    depositHistoryReviewPage.transactionReasonForStatusFundsAvailableCheque();
});

When(/^I should be shown message mapped to status code related to rejected received from Clearing Status field$/, () => {
    depositHistoryReviewPage.transactionReasonForStatusRejectedCheque();
});

When(/^I select view deposit history button$/, () => {
    depositHistoryReviewPage.clickDepositHistoryButton();
});
