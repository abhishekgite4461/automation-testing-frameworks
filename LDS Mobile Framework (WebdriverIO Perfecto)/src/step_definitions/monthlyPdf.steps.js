const {When, Then} = require('cucumber');
const MonthlyPdfPage = require('../pages/statements/monthlyPdf.page.js');

const monthlyPdfPage = new MonthlyPdfPage();

Then(/^I must be displayed the month selection screen$/, () => {
    monthlyPdfPage.waitForPageLoad();
});

When(/^I select close button in month selection screen$/, () => {
    monthlyPdfPage.selectCloseButton();
});

When(/^I select view button on the month selection screen$/, () => {
    monthlyPdfPage.selectViewButton();
    monthlyPdfPage.allowPopupForPdf();
});

Then(/^I should be on selected month PDF Preview screen$/, () => {
    monthlyPdfPage.waitForPdfPreview();
});

Then(/^I must be displayed the digital inbox screen$/, () => {
    monthlyPdfPage.waitForDigitalInbox();
});

When(/^I select NGA header back button in digital inbox screen$/, () => {
    monthlyPdfPage.selectInboxBackButton();
});

When(/^I enter any previous month and year on the month selection screen$/, () => {
    monthlyPdfPage.enterMonthAndYear();
});

Then(/^I must be displayed the previously selected month and year on the month selection screen$/, () => {
    monthlyPdfPage.validateMonthAndYear();
});

Then(/^I must be displayed the month selection screen with default values$/, () => {
    monthlyPdfPage.validateDefaultMonthAndYear();
});
