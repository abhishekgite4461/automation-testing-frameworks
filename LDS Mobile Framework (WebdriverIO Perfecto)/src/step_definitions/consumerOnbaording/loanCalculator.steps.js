const {When, Then} = require('cucumber');
const LoanCalculatorpage = require('../../pages/consumerOnboarding/loanCalculator.page');

const loanCalculatorpage = new LoanCalculatorpage();

When(/^I select the Home improvement option in loan calculator page$/, () => {
    loanCalculatorpage.waitForPageLoad();
    loanCalculatorpage.selectHomeImprovementoption();
    loanCalculatorpage.selectContinueButton();
});

Then(/^I enter (.*) and (.*) for calculating my loan eligibility$/, (amount, term) => {
    loanCalculatorpage.enterLoanAmount(amount);
    loanCalculatorpage.enterLoanTerm(term);
    loanCalculatorpage.selectCalculateButton();
});

Then(/^I select future option as "NO"$/, () => {
    loanCalculatorpage.selectFutureOption();
});

Then(/^I select the continue application$/, () => {
    loanCalculatorpage.selectContinueApplicationButton();
});

Then(/^I should be on the "your loan application" page$/, () => {
    loanCalculatorpage.shouldSeeApplyLoanPage();
});

Then(/^I select the employment status as "Employed - Part time"$/, () => {
    loanCalculatorpage.selectEmploymentStatus();
});

Then(/^I select spending and expenses accordian$/, () => {
    loanCalculatorpage.selectSpendingAccordian();
});

Then(/^I enter (.*) detail and confirm "no" for regular monthly expenses$/, (salary) => {
    loanCalculatorpage.enterSalaryAmount(salary);
    loanCalculatorpage.selectNoForOtherRegularMonthlyIncome();
});

Then(/^I enter my (.*) and (.*) details$/, (mortgageShare, dependent) => {
    loanCalculatorpage.enterMortgageShare(mortgageShare);
    loanCalculatorpage.enterDependentDetails(dependent);
});

Then(/^I select extra cost as "NO"$/, () => {
    loanCalculatorpage.selectNoForExtraCost();
});

Then(/^I select "your contact details" accordian$/, () => {
    loanCalculatorpage.selectContactDetailsAccordian();
});

Then(/^I confirm my email address in your loan application page$/, () => {
    loanCalculatorpage.selectConfirmEmailCheckBox();
});
