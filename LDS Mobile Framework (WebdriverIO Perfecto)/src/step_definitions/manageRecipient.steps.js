const {Given, When, Then} = require('cucumber');
require('chai')
    .should();
const ManageRecipientPage = require('../pages/transferAndPayments/manageRecipient.page');
const ViewRecipientPage = require('../pages/paymentHub/viewRecipient.page');
const SearchAccountAndRecipientPage = require('../pages/searchAccountAndRecipient.page');
const CommonPage = require('../pages/common.page');
const SavedAppDataRepository = require('../data/savedAppData.repository');
const SavedData = require('../enums/savedAppData.enum');

let title;
const viewRecipientPage = new ViewRecipientPage();
const manageRecipientPage = new ManageRecipientPage();
const searchAccountAndRecipientPage = new SearchAccountAndRecipientPage();
const commonPage = new CommonPage();

When(/^I cancel Manage on an external beneficiary$/, () => {
    manageRecipientPage.clickCancelButton();
});

Then(/^I have the option to delete the recipient$/, () => {
    manageRecipientPage.verifyDeleteButton();
});

Then(/^I have the option to pay the recipient$/, () => {
    manageRecipientPage.verifyMakePaymentButton();
});

Then(/^the option to cancel manage Recipient$/, () => {
    manageRecipientPage.verifyCancelButton();
});

Then(/^I can see the Recipients name$/, () => {
    manageRecipientPage.verifyHeader();
});

Then(/^I am taken back to the recipient list$/, () => {
    viewRecipientPage.verifyRecipientList();
});

Then(/^the recipient is still available$/, () => {
    viewRecipientPage.verifyRecipientIsPresentInRecipientList(title);
});

When(/^I click on manage action menu$/, () => {
    viewRecipientPage.selectManageRecipient();
});

When(/^I select manage on an pending payment account$/, () => {
    viewRecipientPage.selectManageOnPendingPayment();
});

Then(/^I am taken to the standing order page$/, () => {
    manageRecipientPage.verifyStandingOrderPageIsSeen();
});

When(/^I select manage on external beneficiary$/, () => {
    viewRecipientPage.selectManageOnUkMobileNumber();
});

When(/^I select to delete a recipient$/, () => {
    title = manageRecipientPage.getAccountTitle();
    SavedAppDataRepository.setData(SavedData.SELECTED_PAYEE, title);
    manageRecipientPage.selectDeleteOnAccount();
});

When(/^I select to delete added recipient$/, () => {
    viewRecipientPage.selectManageOnRecipientName();
    manageRecipientPage.selectDeleteOnAccount();
});

When(/^I delete the selected recipient$/, () => {
    viewRecipientPage.selectManageOnAccount();
    manageRecipientPage.selectDeleteOnAccount();
});

Given(/^I am on manage actions for a (.*)$/, (type) => {
    if (type === 'ukMobile') {
        viewRecipientPage.selectManageOnUkMobileNumber();
    } else {
        viewRecipientPage.selectManageOnUkAccountNumber();
    }
});

When(/^I select to keep the recipient$/, () => {
    manageRecipientPage.selectKeepAccount();
});

When(/^I select to confirm delete a recipient$/, () => {
    manageRecipientPage.selectConfirmDeleteOnAccount();
});

When(/^I click on cancel button on search recipient$/, () => {
    manageRecipientPage.clickCancelSearch();
});

Given(/^I am asked for confirmation if I want to delete the recipient$/, () => {
    manageRecipientPage.verifyConfirmationDiagueBoxIsVisible();
});

Then(/^I am should be shown a message saying recipient could not be deleted due to standing order$/, () => {
    manageRecipientPage.verifyRecipientCouldNotBeDeletedDueToStandingOrder();
});

Then(/^the option to View Standing Orders$/, () => {
    manageRecipientPage.verifyOptionToViewStandingOrderIsSeen();
});

Then(/^the option to Close$/, () => {
    manageRecipientPage.verifyOptionCloseOrderIsSeen();
});

When(/^I close the message$/, () => {
    manageRecipientPage.clickCloseButton();
});

Then(/^the recipient should not be displayed in the list$/, () => {
    searchAccountAndRecipientPage.verifyNoResultsMessage();
});

Then(/^the recipient is removed from the recipient list$/, () => {
    viewRecipientPage.verifyRecipientIsNotPresentInRecipientList(title);
});

Then(/^I select view message$/, () => {
    manageRecipientPage.clickViewOption();
});

Then(/^the pay recipient option is disabled$/, () => {
    manageRecipientPage.verifyPayRecipientIsNotVisible();
});

Then(/^the delete recipient is disabled$/, () => {
    manageRecipientPage.verifyDeleteRecipientIsNotVisible();
});

When(/^I close the pending payment popup in the payment hub$/, () => {
    manageRecipientPage.closePendingPaymentPopup();
});

When(/^I navigate back to home page from payment hub$/, () => {
    commonPage.closeModalAndLeave();
});

When(/^I navigate back to home page after deleting the recipient$/, () => {
    manageRecipientPage.cancelModalAndLeave();
});

Then(/^I am given the option to view the pending payments for the account$/, () => {
    manageRecipientPage.verifyViewPendingPaymentsIsVisible();
});

When(/^I select to view pending payment option$/, () => {
    manageRecipientPage.clickViewPendingPaymentsOption();
});
