const {Then} = require('cucumber');
const PayCreditCardbyMasterCard = require('../../pages/payCreditCardbyMasterCard/payCreditCardbyMasterCard.page');

const payCCbyMC = new PayCreditCardbyMasterCard();

Then(/^I should (see|not see) the option to pay by another uk bank account$/, (isShown) => {
    payCCbyMC.verifyAnotherUKAccountVisibility(isShown);
});

Then(/^I dismiss the error banner message$/, () => {
    payCCbyMC.selectDismissErrorBanner();
});

Then(/^I should be on confirm payment page$/, () => {
    payCCbyMC.shouldSeeReviewPayment();
});

Then(/^I should see the select provider button (disabled|enabled) in the payment hub page$/, (type) => {
    if (type === 'enabled') {
        payCCbyMC.selectProviderButtonEnability(true);
    } else {
        payCCbyMC.selectProviderButtonEnability(false);
    }
});

Then(/^I click the select provider button in the payment hub page$/, () => {
    payCCbyMC.selectProviderButton();
});

Then(/^I should see the selected provider bank (.*) as From account$/, function (bankName) {
    const data = this.dataRepository.get();
    payCCbyMC.shouldSeeSelectedBank(data[bankName]);
});

Then(/^I should see the selected credit card account (.*) as To account$/, function (accountName) {
    const data = this.dataRepository.get();
    payCCbyMC.verifySelectedCCAccount(data[accountName]);
});

Then(/^I should see the confirm button (.*) in the confirm payment page$/, (type) => {
    if (type === 'enabled') {
        payCCbyMC.confirmButtonEnability(true);
    } else {
        payCCbyMC.confirmButtonEnability(false);
    }
});

Then(/^I should see the edit button (.*) in the confirm payment page$/, (type) => {
    if (type === 'enabled') {
        payCCbyMC.editButtonEnability(true);
    } else {
        payCCbyMC.editButtonEnability(false);
    }
});

Then(/^I should not see the checkbox at confirm payment page$/, () => {
    payCCbyMC.shouldNotSeeCheckbox();
});

Then(/^I should (.*) the Important information screen$/, (isVisible) => {
    payCCbyMC.impScreenVisibility(isVisible);
});

Then(/^I agree to the T&C and Consent on information screen$/, () => {
    payCCbyMC.selectTCCheckBox();
});

Then(/^I proceed to the external bank website$/, () => {
    payCCbyMC.selectContinueBtn();
});

Then(/^I navigate to provider list screen$/, () => {
    payCCbyMC.shouldSeeProviderListScreen();
});

Then(/^I select pay by uk debit card option$/, () => {
    payCCbyMC.selectPaybyUKDebitCard();
});
