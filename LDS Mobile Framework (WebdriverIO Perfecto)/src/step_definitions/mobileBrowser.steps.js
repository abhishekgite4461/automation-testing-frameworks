const {When} = require('cucumber');
const DeviceHelper = require('../utils/device.helper');

const MobileBrowserPage = require('../pages/mobileBrowser.page');
const Actions = require('../pages/common/actions');

const mobileBrowserPage = new MobileBrowserPage();

When(/^I navigate to mobile browser$/, () => {
    mobileBrowserPage.waitForMobileBroswerPageLoad();
});

When(/^I validate url from mobile browser$/, () => {
    mobileBrowserPage.verifyBrowserUrl();
});

When(/^I navigate back to app from mobile browser$/, () => {
    if (DeviceHelper.isIOS()) {
        mobileBrowserPage.selectReturnToApp();
    } else {
        Actions.pressAndroidBackKey();
    }
});
