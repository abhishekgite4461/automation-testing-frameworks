const {When} = require('cucumber');
const PaymentTypePage = require('../pages/paymentType.page');

const paymentTypePage = new PaymentTypePage();

When(/^I select pay a contact tab in payment type page$/, () => {
    paymentTypePage.selectPayContact();
});

When(/^I should see error message for pay a contact$/, () => {
    paymentTypePage.verifyPayContactErrorMessage();
});
