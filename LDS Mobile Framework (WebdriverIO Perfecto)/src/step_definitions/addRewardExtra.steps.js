const {When, Then} = require('cucumber');

const AddRewardExtraWebview = require('../pages/addRewardExtraWebView.page');

const addRewardExtraWebviewPage = new AddRewardExtraWebview();

Then(/^I verify details on add reward extra landing page$/, () => {
    addRewardExtraWebviewPage.waitForPageLoad();
    addRewardExtraWebviewPage.shouldSeeAddRewardExtraLandingTitle();
});

Then(/^I verify details on track reward extra landing page$/, () => {
    addRewardExtraWebviewPage.waitForPageLoad();
    addRewardExtraWebviewPage.shouldSeeTrackRewardExtraLandingTitle();
});

Then(/^I verify Already Have Reward Extras Title is displayed$/, () => {
    addRewardExtraWebviewPage.waitForPageLoad();
    addRewardExtraWebviewPage.shouldSeeAlreadyHaveRewardTitle();
});

Then(/^I verify Already Have Reward Extras message is displayed$/, () => {
    addRewardExtraWebviewPage.shouldSeeAlreadyHaveRewardMessage();
});

Then(/^I should be on already have reward extras message page for URCA$/, () => {
    addRewardExtraWebviewPage.shouldSeeAlreadyHaveRewardExtras();
});

Then(/^I select back to account overview in add reward extra landing page$/, () => {
    addRewardExtraWebviewPage.selectBackToAccountOverviewTrackURCA();
});

Then(/^I select back to account overview in track reward extra landing page$/, () => {
    addRewardExtraWebviewPage.selectBackToAccountOverviewTrackRCA();
});

Then(/^I select proposition (.*) on add reward extra landing page$/, (proposition) => {
    addRewardExtraWebviewPage.selectProposition(proposition);
});

Then(/^I select choose how to earn for an offer on add reward extra landing page$/, () => {
    addRewardExtraWebviewPage.selectChooseHowToEarn();
});

Then(/^I should be on the Reward Category page$/, () => {
    addRewardExtraWebviewPage.shouldSeeRewardCategoryTitle();
});

Then(/^I select category (.*) on add reward extra category page$/, (category) => {
    addRewardExtraWebviewPage.selectCategory(category);
});

Then(/^I select choose this option for an offer on add reward extra landing page$/, () => {
    addRewardExtraWebviewPage.selectChooseThisOption();
});

Then(/^I should be on the lets get started page$/, () => {
    addRewardExtraWebviewPage.shouldSeeLetsGetStartedTitle();
});

When(/^I select the continue button on lets get started page$/, () => {
    addRewardExtraWebviewPage.selectContinueOnLetsGetStarted();
});

Then(/^I should be on the Your Email page$/, () => {
    addRewardExtraWebviewPage.shouldSeeYourEmailTitle();
});

Then(/^I select Yes button for email address confirmation on Your Email page$/, () => {
    addRewardExtraWebviewPage.selectYesButton();
});

When(/^I select the continue button on Your Email page$/, () => {
    addRewardExtraWebviewPage.selectContinueOnYourEmail();
});

Then(/^I should be on the Legal Bits page$/, () => {
    addRewardExtraWebviewPage.shouldSeeTheLegalBitsTitle();
});

Then(/^I select the I confirm checkbox in the legal bits add offer page$/, () => {
    addRewardExtraWebviewPage.selectIConfirmCheckboxLegalBits();
});

Then(/^I expand offer accordion on add reward extra landing page$/, () => {
    addRewardExtraWebviewPage.selectOfferAccordion();
});

Then(/^I verify Engage Qualifying Status message$/, () => {
    addRewardExtraWebviewPage.shouldSeeQualifyingStatus();
});

Then(/^I verify Engage Last Updated date$/, () => {
    addRewardExtraWebviewPage.shouldSeeLastUpdatedDate();
});

Then(/^I select Show my Offer Progress on reward tracker page/, () => {
    addRewardExtraWebviewPage.selectOfferProgress();
});

Then(/^I verify Engage Qualification Description/, () => {
    addRewardExtraWebviewPage.shouldSeeQualificationDescription();
});

Then(/^I verify the offer unavailability message for RCA$/, () => {
    addRewardExtraWebviewPage.shouldSeeRCAIneligibility();
});
