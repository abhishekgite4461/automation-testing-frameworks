const {Given, When, Then} = require('cucumber');

const PersonalDetailsPage = require('../pages/settings/personalDetails/personalDetails.page');
const ChangePhoneNumbersPage = require('../pages/settings/personalDetails/changePhoneNumbers.page');
const ConfirmPhoneNumbersPage = require('../pages/settings/personalDetails/confirmPhoneNumbers.page');
const ChangeEmailPage = require('../pages/settings/personalDetails/changeEmail.page');
const ConfirmPasswordPage = require('../pages/confirmAuthenticationPassword.page');
const UkCompanySearchResultsPage = require('../pages/transferAndPayments/ukCompanySearchResults.page.js');
const ReviewPaymentPage = require('../pages/paymentHub/reviewPayment.page');
const UserDetailsHelper = require('../utils/userDetails.helper');
const ReactivateIsaPage = require('../pages/paymentHub/reactivateIsa.page');
const DeviceHelper = require('../utils/device.helper');
const NavigationTask = require('../tasks/navigation.task');
const PaymentTask = require('../tasks/payment.task');
const YourAccountsTask = require('../tasks/yourAccounts.task');
const HomePage = require('../pages/home.page');
const BeneficiaryTypePage = require('../pages/transferAndPayments/beneficiaryType.page');
const UkBeneficiaryDetailPage = require('../pages/transferAndPayments/ukBeneficiaryDetail.page.js');

const ConfirmPasswordAction = require('../enums/confirmPasswordAction.enum');
const ConfirmPasswordLaunchScreen = require('../enums/confirmPasswordLaunchScreen.enum');

const personalDetailsPage = new PersonalDetailsPage();
const changeEmailPage = new ChangeEmailPage();
const changePhoneNumbersPage = new ChangePhoneNumbersPage();
const confirmPhoneNumbersPage = new ConfirmPhoneNumbersPage();
const confirmPasswordPage = new ConfirmPasswordPage();
const ukCompanySearchResultsPage = new UkCompanySearchResultsPage();
const reviewPaymentPage = new ReviewPaymentPage();
const reactivateIsaPage = new ReactivateIsaPage();
const homePage = new HomePage();
const beneficiaryTypePage = new BeneficiaryTypePage();
const ukBeneficiaryDetailPage = new UkBeneficiaryDetailPage();

const validEmailAddress = UserDetailsHelper.generateRandomEmailAddress();

const navigateToUkCompanyBeneficiaryReview = () => {
    const reference = 'test';
    PaymentTask.navigateToAddNewRecipient();
    PaymentTask.addNewPayee();
    beneficiaryTypePage.chooseUkAccountBeneficiary();
    PaymentTask.enterWhiteListedDetails();
    ukBeneficiaryDetailPage.continueToReviewDetailsPage();
    ukCompanySearchResultsPage.selectResult();
    PaymentTask.addReference(reference, reference);
    ukCompanySearchResultsPage.confirmReviewDetailsPage();
    ukCompanySearchResultsPage.confirmReviewDetailsPage();
};

const performUpdateAction = function (world, action) {
    const actionToPerform = ConfirmPasswordAction.fromStringByEscapingSpace(action);
    const data = world.dataRepository.get();

    switch (actionToPerform) {
    // eslint-disable-next-line no-case-declarations
    case ConfirmPasswordAction.PHONE_NUMBER_UPDATE:
        NavigationTask.openYourProfileFromMoreMenu();
        personalDetailsPage.waitForPageLoad();
        personalDetailsPage.isPageVisible();
        personalDetailsPage.selectMobileDetails();
        const mobNumber = DeviceHelper.isStub() ? data.phoneNumber : DeviceHelper.mobileNumber()
            .replace('+44', '');
        changePhoneNumbersPage.changeMobileNumber(mobNumber);
        changePhoneNumbersPage.clickContinueButton();
        confirmPhoneNumbersPage.clickConfirmButton();
        break;
    case ConfirmPasswordAction.EMAIL_ADDRESS_UPDATE:
        NavigationTask.openYourProfileFromMoreMenu();
        personalDetailsPage.waitForPageLoad();
        personalDetailsPage.isPageVisible();
        personalDetailsPage.selectEmailAddressDetails();
        changeEmailPage.enterEmailAddress(validEmailAddress);
        changeEmailPage.confirmEmailAddress(validEmailAddress);
        break;
    case ConfirmPasswordAction.ADD_UK_BENEFICIARY:
        PaymentTask.navigateToUkBeneficiaryReview();
        break;
    case ConfirmPasswordAction.ADD_COMPANY_BENEFICIARY:
        navigateToUkCompanyBeneficiaryReview();
        break;
    case ConfirmPasswordAction.ADD_NEW_PAYM:
        PaymentTask.navigateToPayMBeneficiaryReview();
        break;
    case ConfirmPasswordAction.REACTIVATE_ISA:
        homePage.chooseAccount('isa');
        YourAccountsTask.selectReactivateIsaOfAccount('isa');
        reactivateIsaPage.selectAgreeConfirmation();
        break;
    default:
        throw new Error(`${actionToPerform} - action is not defined`);
    }
};

Given(/^I am displayed password confirmation screen while (.*)$/, function (action) {
    performUpdateAction(this, action);
});

When(/^I choose to proceed with (.*)$/, function (action) {
    performUpdateAction(this, action);
});

Then(/^I am displayed password confirmation screen$/, confirmPasswordPage.isPageVisible);

Then(/^I should be displayed the password confirmation screen$/, () => {
    confirmPasswordPage.isPageVisible();
});

Then(/^I should be presented with a password confirmation pop-up$/, () => {
    confirmPasswordPage.isPageVisible();
});

Then(/^I should be displayed option to enter my IB password$/, () => {
    confirmPasswordPage.isPasswordFieldVisible();
});

Then(/^I should be given options to proceed or cancel$/, () => {
    confirmPasswordPage.isCancelButtonVisible();
    confirmPasswordPage.isConfirmButtonVisible();
});

Then(/^I should be displayed option for forgotten password$/, () => {
    confirmPasswordPage.isForgottenPasswordLinkVisible();
});

When(/^I enter the password "(.*)" into a dialog$/, (password) => {
    confirmPasswordPage.enterPassword(password);
});

Then(/^I should be restricted from entering more than (.*) characters in password dialog text box$/, (noOfCharacters) => {
    confirmPasswordPage.checkNumberOfPasswordCharacters(parseInt(noOfCharacters, 10));
});

Then(/^the option to continue on enter password dialogue is enabled$/, () => {
    confirmPasswordPage.confirmButtonIsEnabled();
});

When(/^I see password is masked on enter password dialog$/, () => {
    confirmPasswordPage.validateMaskedConfirmPassword(true);
});

Then(/^the option to continue on enter password dialogue should not be enabled$/, () => {
    confirmPasswordPage.confirmButtonIsDisabled();
});

When(/^I choose to proceed having entered the right password$/, function () {
    const data = this.dataRepository.get();
    confirmPasswordPage.enterPassword(data.password);
    confirmPasswordPage.clickConfirmButton();
});

When(/^I have entered wrong password$/, () => {
    confirmPasswordPage.enterWrongPassword();
});

When(/^I submit password on enter password dialog$/, () => {
    confirmPasswordPage.clickConfirmButton();
});

Then(/^I should be displayed an error message for wrong password$/, () => {
    confirmPasswordPage.isErrorMessageDisplayed();
});

When(/^I choose forgotten your password option$/, () => {
    confirmPasswordPage.clickForgottenPasswordLink();
});

Then(/^I should be shown that password entered is incorrect$/, () => {
    confirmPasswordPage.isSCAIncorrectPasswordDisplayed();
});

Then(/^I should be shown that password entered is incorrect and the next one is final attempt$/, () => {
    confirmPasswordPage.isSCAFinalAttemptWarningDisplayed();
});

Then(/^I should be given an option to enter the password again$/, () => {
    confirmPasswordPage.isPageVisible();
});

When(/^I choose to cancel the process on enter password dialog/, () => {
    confirmPasswordPage.clickCancelButton();
});

Then(/^I should be taken back to the (.*) screen/, (screenName) => {
    const screen = ConfirmPasswordLaunchScreen.fromStringByEscapingSpace(screenName);
    switch (screen) {
    case ConfirmPasswordLaunchScreen.UPDATE_PHONE_NUMBER:
        changePhoneNumbersPage.isPageVisible();
        break;
    case ConfirmPasswordLaunchScreen.UPDATE_EMAIL_ADDRESS:
        changeEmailPage.isPageVisible();
        break;
    case ConfirmPasswordLaunchScreen.REVIEW_UK_BENEFICIARY_DETAILS:
        ukCompanySearchResultsPage.isUKReviewPageDetailVisible();
        break;
    case ConfirmPasswordLaunchScreen.REVIEW_COMPANY_BENEFICIARY_DETAILS:
        ukCompanySearchResultsPage.isUKCompanyReviewDetailPageVisible();
        break;
    case ConfirmPasswordLaunchScreen.ADD_NEW_PAYM_DETAILS:
        reviewPaymentPage.verifyReviewPaymentPage();
        break;
    case ConfirmPasswordLaunchScreen.REACTIVATE_ISA_CONFIRMATION_DETAILS:
        reactivateIsaPage.verifyReactivateIsaPage();
        break;
    default:
        throw new Error(`${screenName} - screen name is not defined`);
    }
});
