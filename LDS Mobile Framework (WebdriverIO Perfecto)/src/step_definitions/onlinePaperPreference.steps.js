const {When, Then} = require('cucumber');
const OnlinePaperPreferenceWebViewpage = require('../pages/onlinePaperPreferenceWebViewpage.page');
const CommonPage = require('../pages/common.page');
const ResetYourPasswordPage = require('../pages/settings/resetYourPassword.page');
const HomePage = require('../pages/home.page');

const onlinePaperPreferenceWebViewPage = new OnlinePaperPreferenceWebViewpage();
const commonPage = new CommonPage();
const resetYourPasswordPage = new ResetYourPasswordPage();
const homePage = new HomePage();

Then(/^I should be on the manage your online and paper preference webview page$/, () => {
    onlinePaperPreferenceWebViewPage.waitForPageLoad();
    browser.pause(5000);
    onlinePaperPreferenceWebViewPage.dismissCookieConsentIfPresent();
});

When(/^I select back button from the online and paper preferences webview page$/, () => {
    onlinePaperPreferenceWebViewPage.selectGlobalBackButton();
});

When(/^I select update preference from manage your online and paper preference webview page$/, () => {
    onlinePaperPreferenceWebViewPage.waitForPageLoad();
    onlinePaperPreferenceWebViewPage.modifyPreferences();
    onlinePaperPreferenceWebViewPage.updatePreference();
});

When(/^I select on the forget password link on confirm your paperless preferences webview page$/, () => {
    onlinePaperPreferenceWebViewPage.selectForgetPassword();
});

When(/^I should see forgotten password winback with options on top of the screen with Cancel and OK option$/, () => {
    onlinePaperPreferenceWebViewPage.verifyForgottenPasswordWinbackOption();
});

When(/^on selection of OK option I should be on native reset password screen$/, () => {
    commonPage.selectWinBackLeaveButton();
    resetYourPasswordPage.waitForPageLoad();
});

Then(/^I am on confirm your paperless preferences webview page$/, () => {
    onlinePaperPreferenceWebViewPage.waitForConfirmPreferencePageLoad();
});

Then(/^I accept the terms & condition and submit order paper preference$/, function () {
    const data = this.dataRepository.get();
    onlinePaperPreferenceWebViewPage.acceptTermsAndCondition();
    onlinePaperPreferenceWebViewPage.enterPassword(data.password);
    onlinePaperPreferenceWebViewPage.submitPreference();
});

Then(/^I should see update preference confirmation page$/, () => {
    onlinePaperPreferenceWebViewPage.waitForPreferenceSuccessPageLoad();
});

Then(/^I select (back|home) button from confirm your preferences webview page$/, (option) => {
    if (option === 'back') {
        onlinePaperPreferenceWebViewPage.selectGlobalBackButton();
    } else {
        homePage.navigateToHomePage();
    }
});

Then(/^I should see winback option on top of the screen with Stay and Leave option$/, () => {
    onlinePaperPreferenceWebViewPage.verifyWinbackOption();
});

Then(/^on selection of Stay or Cancel option I should stay on the same screen$/, () => {
    onlinePaperPreferenceWebViewPage.selectStayButton();
    onlinePaperPreferenceWebViewPage.verifyConfirmPreferencePage();
});

When(/^I select Leave option$/, () => {
    onlinePaperPreferenceWebViewPage.selectLeaveOption();
});
