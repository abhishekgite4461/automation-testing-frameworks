const {Then} = require('cucumber');
const Actions = require('../pages/common/actions');

const AppLaunchErrorPage = require('../pages/appLaunchError.page');

const appLaunchErrorPage = new AppLaunchErrorPage();

Then(/^my internet connection is OFF$/, () => {
    Actions.turnWifiOff();
});

Then(/^I should see App launch screen being displayed$/, () => {
    appLaunchErrorPage.waitForPageLoad();
});

Then(/^I should see internet connection error being displayed$/, () => {
    appLaunchErrorPage.shouldSeeInternetConnectionError();
});

Then(/^I should see technical error being displayed$/, () => {
    appLaunchErrorPage.shouldSeeTechnicalError();
});

Then(/^I should turn wifi on$/, () => {
    Actions.turnWifiOn();
});

Then(/^I should see internet connection error being displayed in Browser$/, () => {
    appLaunchErrorPage.shouldSeeInternetConnectionInBrowser();
});
