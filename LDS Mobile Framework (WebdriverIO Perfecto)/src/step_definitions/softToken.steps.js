const {When, Then} = require('cucumber');
const SoftTokenPage = require('../pages/softToken.page');

const incorrectPasscode = '123456';
const passcode = '000000';

const softTokenPage = new SoftTokenPage();

When(/^I provide (valid|incorrect) 6 digit passcode on Soft Token screen$/, (type) => {
    softTokenPage.waitForPageLoad();
    if (type === 'valid') {
        softTokenPage.enterPasscode(passcode);
    } else {
        softTokenPage.enterPasscode(incorrectPasscode);
    }
    softTokenPage.selectContinueButton();
});

When(/^I should see contact us option in Soft Token screen$/, () => {
    softTokenPage.waitForPageLoad();
    softTokenPage.shouldSeeContactUsButton();
});

When(/^I choose an option to view more info on Software token$/, () => {
    softTokenPage.waitForPageLoad();
    softTokenPage.selectSoftTokenLink();
});

Then(/^I should be shown an overlay with Close option$/, () => {
    softTokenPage.isSoftTokenOverlayVisible();
});

Then(/^I should be on Soft token screen$/, () => {
    softTokenPage.shouldSeeSoftTokenScreen();
});

When(/^I select close option$/, () => {
    softTokenPage.selectCloseButton();
});

When(/^I should be shown an incorrect passcode error banner in soft token screen$/, () => {
    softTokenPage.verifyIncorrectPasscodeErrorBanner();
});
