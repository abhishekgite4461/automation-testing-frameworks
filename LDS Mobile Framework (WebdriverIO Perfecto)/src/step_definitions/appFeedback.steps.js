const {Then} = require('cucumber');
const AppFeedbackPage = require('../pages/appFeedback.page');

const appFeedbackPage = new AppFeedbackPage();

Then(/^I should be displayed a feedback form$/, () => {
    appFeedbackPage.waitForAppFeedbackPageLoad();
    appFeedbackPage.verifyFeedbackForm();
});
