const {When} = require('cucumber');
const SearchLandingPage = require('../../pages/inAppSearch/searchLanding.page');

const searchLandingPage = new SearchLandingPage();

When(/^I will be on Search landing page$/, () => {
    searchLandingPage.shouldSeeSearchBar();
});

When(/^I enter search (.*) in the search dialog box$/, (keyword) => {
    searchLandingPage.enterKeywordInAppSearchBox(keyword);
});

When(/^I should be able to see the (.*) link as search result$/, (searchResult) => {
    searchLandingPage.shouldSeeSearchResult(searchResult);
});

When(/^I tap on the (.*) link$/, (searchResult) => {
    searchLandingPage.selectSearchResult(searchResult);
});

When(/^I should be on (.*) page as per the searched keyword$/, (searchedPage) => {
    searchLandingPage.shouldSeeSearchResultLandingPage(searchedPage);
});

When(/^I enter search (.*) in the search dialog box and validate (.*) link as search result$/, (keyword, searchResult) => {
    searchLandingPage.enterKeywordInAppSearchBox(keyword);
    searchLandingPage.shouldSeeSearchResultLandingPage(searchResult);
});

When(/^I should see below search results populated for loan repayment$/, (searchList) => {
    const tableData = searchList.hashes();
    searchLandingPage.validateSearchResultsForLoan(tableData);
});

When(/^I should see below search results populated for mortgage repayment$/, (searchList) => {
    const tableData = searchList.hashes();
    searchLandingPage.validateSearchResultsForMortgage(tableData);
});

When(/^I should see below search (.*) populated for (.*) page$/, (searchList, searchResult) => {
    const tableData = searchList.hashes();
    searchLandingPage.validateSearchResults(tableData, searchResult);
});

When(/^I tap on the In-App search box$/, () => {
    searchLandingPage.selectInAppSearchBar();
});

When(/^I should be able to see the No Search Result found page$/, () => {
    searchLandingPage.shouldSeeNoResultFoundPage();
});

When(/^I tap on the help and support button$/, () => {
    searchLandingPage.selectHelpAndSupportButton();
});

When(/^I should land into Support Hub Page$/, () => {
    searchLandingPage.shouldSeeSupportHubPage();
});

When(/^I should be able to see the pre defined suggested search results$/, () => {
    searchLandingPage.shouldSeeSuggestedResultPage();
});
