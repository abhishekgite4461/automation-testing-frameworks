const {When, Then} = require('cucumber');

const RegisterPayAContactPage = require('../pages/registerPayAContact.page');

const registerPayAContactPage = new RegisterPayAContactPage();

When(/^I should be on the register for pay a contact page$/, () => {
    registerPayAContactPage.waitForPageLoad();
});

Then(/^I choose to register for pay a contact$/, () => {
    registerPayAContactPage.selectRegisterPayAContact();
    registerPayAContactPage.selectContinue();
    registerPayAContactPage.selectAgreementAndContinue();
    registerPayAContactPage.selectContinueToP2PAuthentication();
});

Then(/^I should see registration successful screen$/, () => {
    registerPayAContactPage.verifySuccessMessage();
});

Then(/^I navigate to homepage from register for pay a contact page$/, () => {
    registerPayAContactPage.selectHomeButton();
});

Then(/^I deregister the mobile number from P2P$/, () => {
    registerPayAContactPage.deregisterP2P();
});
