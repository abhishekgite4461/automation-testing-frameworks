const {When, Then} = require('cucumber');
const ActionMenuPage = require('../pages/actionMenu.page');
const ReactivateIsaPage = require('../pages/paymentHub/reactivateIsa.page.js');
const ReactivateIsaSuccessPage = require('../pages/paymentHub/reactivateIsaSuccess.page.js');
const ChangeAddressModalPage = require('../pages/paymentHub/changeAddressModal.page.js');
const YourAccountsTask = require('../tasks/yourAccounts.task');

const reactivateIsaPage = new ReactivateIsaPage();
const reactivateIsaSuccessPage = new ReactivateIsaSuccessPage();
const changeAddressModalPage = new ChangeAddressModalPage();
const actionMenuPage = new ActionMenuPage();

Then(/^I should see below details in declaration page$/, (table) => {
    const tableData = table.hashes();
    reactivateIsaPage.waitForPageLoad();
    reactivateIsaPage.verifyDeclarationDetails(tableData);
});

When(/^I choose to agree declaration and reactivate ISA from reactivate your ISA page$/, () => {
    reactivateIsaPage.waitForPageLoad();
    reactivateIsaPage.selectAgreeConfirmation();
});

When(/^I choose to update my address in reactivate declaration page$/, () => {
    reactivateIsaPage.waitForPageLoad();
    reactivateIsaPage.selectReactivateIsaUpdateMyAddress();
});

Then(/^I should (be|not be) prompted to enter my password$/, YourAccountsTask.verifyEnterPasswordDialogBox);

When(/^I enter a valid password on reactivate ISA password dialog panel$/, function () {
    const data = this.dataRepository.get();
    reactivateIsaPage.enterPasswordOnDialogBox(data.password);
});

When(/^I click on update my address in reactivate declaration page$/, () => {
    reactivateIsaPage.selectReactivateIsaUpdateMyAddress();
});

When(/^I should see the current address in update address page$/, (table) => {
    const tableData = table.hashes();
    reactivateIsaPage.verifyReactivateIsaUpdateMyAddress(tableData);
});

When(/^I select call to change address button on change address page$/, () => {
    reactivateIsaPage.selectCalltoChangeAddress();
});

When(/^I should be on the update your address call us page$/, () => {
    changeAddressModalPage.waitForPageLoad();
});

Then(/^I should see reactivation details in ISA Reactivation Success page$/, (table) => {
    const tableData = table.hashes();
    reactivateIsaSuccessPage.waitForPageLoad();
    reactivateIsaSuccessPage.verifyReactivateIsaSuccessPage(tableData);
});

When(/^I choose to update national insurance number in reactivate declaration page$/, () => {
    reactivateIsaPage.waitForPageLoad();
    reactivateIsaPage.verifyReactivateIsaUpdateNiNumberTextView();
});

When(/^I click on the view pending payments option in the action menu$/, () => {
    actionMenuPage.waitForPageLoad();
    actionMenuPage.selectPendingPaymentOption();
});

When(/^I select the direct debit option in the action menu$/, () => {
    actionMenuPage.waitForPageLoad();
    actionMenuPage.selectDirectDebitOption();
});

When(/^I see the Reactivate ISA success screen$/, () => {
    reactivateIsaSuccessPage.waitForPageLoad();
    reactivateIsaSuccessPage.verifyReactivateIsaSuccessPage();
});

Then(/^I (should|should not) see an option to add money to the ISA$/, (visible) => {
    if (visible === 'should') {
        reactivateIsaSuccessPage.verifyAddMoreMoneyButton();
    } else {
        reactivateIsaSuccessPage.verifyAddMoreMoneyButtonInvisible();
    }
});

Then(/^I should see a message how to make an external transfer to my ISA$/, () => {
    reactivateIsaSuccessPage.verifyMessageForExternalTransferIsa();
});

Then(/^I am on Reactivate ISA success screen$/, () => {
    reactivateIsaSuccessPage.verifyReactivateIsaSuccessScreen();
});

Then(/^I should see instructional message$/, () => {
    reactivateIsaSuccessPage.verifyInstructionalMessage();
});
