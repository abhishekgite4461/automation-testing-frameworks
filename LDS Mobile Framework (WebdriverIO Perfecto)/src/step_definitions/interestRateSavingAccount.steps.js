const {When} = require('cucumber');
const InterestRateSavingAccountPage = require('../pages/interestRateSavingAccount.page');

const interestRateSavingAccountPage = new InterestRateSavingAccountPage();

When(/^I should see interest rate in saving account$/, () => {
    interestRateSavingAccountPage.shouldSeeInterestRate();
});
