const {When, Then} = require('cucumber');
const ProductHubPage = require('../pages/productHub/productHub.page');
const ProductHubLandingPage = require('../pages/productHub/productHubLanding.page');
const ProductHubLandingCurrentPage = require('../pages/productHub/productHubLandingCurrent.page');
const ProductHubLandingLoanPage = require('../pages/productHub/productHubLandingLoan.page');
const ProductHubLandingSavingsPage = require('../pages/productHub/productHubLandingSavings.page');
const ProductHubLandingCreditCardPage = require('../pages/productHub/productHubLandingCreditCard.page');
const ProductHubLandingHomeInsurancePage = require('../pages/productHub/productHubLandingHomeInsurance.page');
const ChargeCardPage = require('../pages/chargeCard.page');

const numberOfMonth = '1';

const productHubPage = new ProductHubPage();
const productHubLandingPage = new ProductHubLandingPage();
const productHubLandingLoanPage = new ProductHubLandingLoanPage();
const productHubLandingCurrentPage = new ProductHubLandingCurrentPage();
const productHubLandingSavingsPage = new ProductHubLandingSavingsPage();
const productHubLandingCreditCardPage = new ProductHubLandingCreditCardPage();
const productHubLandingHomeInsurancePage = new ProductHubLandingHomeInsurancePage();
const chargeCardPage = new ChargeCardPage();

When(/^I should be on the product hub page$/, () => {
    productHubPage.waitForPageLoad();
});

When(/^I select the back button in the product hub landing page$/, () => {
    productHubLandingPage.waitForPageLoad();
    productHubLandingPage.goBack();
});

When(/^I select the loan calculator of the Loans option in the product hub page$/, () => {
    productHubPage.waitForPageLoad();
    productHubPage.selectLoanCalculator();
});

When(/^I select the overdraft of the current accounts in the product hub page$/, () => {
    productHubPage.waitForPageLoad();
    productHubPage.selectCurrentAccountsOverdraft();
});

When(/^I should be on the loan calculator page$/, () => {
    productHubLandingLoanPage.waitForPageLoad();
    productHubLandingLoanPage.shouldBeOnLoanCalculator();
});

When(/^I should be on the overdraft current accounts page$/, () => {
    productHubLandingCurrentPage.waitForPageLoad();
    productHubLandingCurrentPage.shouldBeOnCurrentAccountOverdraft();
});

When(/^I select the Compare credit card of the Credit cards in the product hub page$/, () => {
    productHubPage.waitForPageLoad();
    productHubPage.selectCompareCreditCard();
});

When(/^I select the Compare savings accounts of the Savings in the product hub page$/, () => {
    productHubPage.waitForPageLoad();
    productHubPage.selectCompareSavingsAccounts();
});

When(/^I select the Home Insurance option in the product hub page$/, () => {
    productHubPage.waitForPageLoad();
    productHubPage.selectHomeInsurance();
});

When(/^I select Other mortgages option in the product hub page$/, () => {
    productHubPage.waitForPageLoad();
    productHubPage.selectOtherMortgageOptions();
});

When(/^I should be on the Other mortgages options page$/, () => {
    productHubLandingPage.waitForPageLoad();
    productHubLandingPage.shouldBeOnOtherMortgage();
});

When(/^I should be on the Home Insurance page$/, () => {
    productHubLandingHomeInsurancePage.waitForPageLoad();
    productHubLandingHomeInsurancePage.shouldBeOnHomeInsurance();
});

When(/^I should be on the Compare savings accounts page$/, () => {
    productHubLandingSavingsPage.waitForPageLoad();
    productHubLandingSavingsPage.shouldBeOnCompareSavings();
});

When(/^I should be on the Compare credit card page$/, () => {
    productHubLandingCreditCardPage.waitForPageLoad();
    productHubLandingCreditCardPage.shouldBeOnCompareCreditCard();
});

When(/^I close the opened Featured option from product hub menu$/, () => {
    productHubPage.waitForPageLoad();
    productHubPage.selectFeatured();
});

When(/^I should see Loan Term Field on Loan Calculator page$/, () => {
    productHubLandingPage.waitForPageLoad();
    productHubLandingPage.enterLoanTermField(numberOfMonth);
});

When(/^I select apply for a charge card of the Cards in the product hub page$/, () => {
    productHubPage.waitForPageLoad();
    productHubPage.applyForChargeCard();
});

Then(/^I should see Charge Card page in the mobile browser$/, () => {
    chargeCardPage.waitForPageLoad();
    chargeCardPage.chargeCardIsVisible();
});

Then(/^I do not see (.*) product on product hub page$/, (productName) => {
    productHubPage.verifyProductisNotVisible(productName);
});

Then(/^I see (.*) product on product hub page$/, (productName) => {
    productHubPage.verifyProductisVisible(productName);
});

Then(/^I expand (.*) on product hub page$/, (productName) => {
    productHubPage.expandProduct(productName);
});

Then(/^I should not see Discover and apply title on product hub page$/, () => {
    productHubPage.verifyDiscoverAndApplyTitleIsNotVisible();
});
