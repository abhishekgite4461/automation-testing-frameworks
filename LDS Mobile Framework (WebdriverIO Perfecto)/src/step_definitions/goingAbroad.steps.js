const {When} = require('cucumber');

const GoingAbroadPage = require('../pages/settings/goingAbroads.page');

const goingAbroadPage = new GoingAbroadPage();

When(/^I should be on the Going Abroad page$/, () => {
    goingAbroadPage.waitForPageLoad();
});

When(/^I click on the back button in the Going Abroad page$/, () => {
    goingAbroadPage.waitForPageLoad();
    goingAbroadPage.clickBackButton();
});
