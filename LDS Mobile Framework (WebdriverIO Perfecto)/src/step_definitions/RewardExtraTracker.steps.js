const {Then} = require('cucumber');

const RewardExtraTrackerWebviewPage = require('../pages/rewardExtraTracker.page.js');

const rewardExtraTrackerWebviewPage = new RewardExtraTrackerWebviewPage();

Then(/^I verify ineligibility the Reward Extra Tracker Page$/, () => {
    rewardExtraTrackerWebviewPage.waitForPageLoad();
    rewardExtraTrackerWebviewPage.shouldSeeIneligibilityForRCA();
});

Then(/^I should be on the Reward Extra Tracker Page$/, () => {
    rewardExtraTrackerWebviewPage.waitForPageLoadURCA();
    rewardExtraTrackerWebviewPage.shouldSeeRewardExtraTrackerForURCA();
});

Then(/^I select the show my offer progress in reward extra page$/, () => {
    rewardExtraTrackerWebviewPage.selectShowMyOfferProgress();
});
