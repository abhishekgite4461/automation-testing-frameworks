const {When, Then} = require('cucumber');

const AtmBranchFinderPage = require('../pages/atmBranchFinder/atmBranchFinder.page');
const DeviceHelper = require('../utils/device.helper');
const SupportHubHomePage = require('../pages/supportHubHomePage.page');

const supportHubHomePage = new SupportHubHomePage();
const atmBranchFinderPage = new AtmBranchFinderPage();

When(/^I am on the atm and branch finder page$/, () => {
    supportHubHomePage.selectATMBranchFinderTile();
    atmBranchFinderPage.isPageVisible();
});

When(/^I should be on the atm and branch finder page$/, () => {
    atmBranchFinderPage.isPageVisible();
});

Then(/^I am displayed the following search options$/, (table) => {
    const options = table.hashes()
        .map((entry) => entry.searchOption);
    atmBranchFinderPage.validateAtmBranchFinderPageOptions(options);
});

Then(/^I should see find ATM tile option$/, () => {
    atmBranchFinderPage.shouldSeeATMTile();
});

Then(/^I should not see search branch tile option$/, () => {
    atmBranchFinderPage.shouldNotSeeBranchTile();
});

When(/^I select the (search branches|find nearby ATM) option on the atm and branch finder page$/, (option) => {
    if (option === 'search branches') {
        atmBranchFinderPage.selectSearchBranches();
    } else {
        atmBranchFinderPage.selectFindNearbyATM();
    }
});

Then(/^I am displayed a note message stating clicking search option links take you outside the app$/, () => {
    atmBranchFinderPage.verifyNoteMessage();
});

Then(/^I select map back button$/, () => {
    atmBranchFinderPage.selectMapBackButton();
});

Then(/^I am taken out of the application to the default map and displayed correct search keyword for (.*)$/, function (atmOrBranch) {
    const branchSearch = (atmOrBranch === 'Branch') ? DeviceHelper.getBrand()
        .toLowerCase()
        .concat(atmOrBranch) : atmOrBranch;
    const data = this.dataRepository.get();
    atmBranchFinderPage.validateMapATMBranch(data[branchSearch]);
});
