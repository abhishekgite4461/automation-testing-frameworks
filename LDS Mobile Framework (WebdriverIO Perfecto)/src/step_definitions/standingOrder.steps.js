const {When, Then} = require('cucumber');
const StandingOrderPage = require('../pages/transferAndPayments/standingOrder.page.js');
const SetupStandingOrderPage = require('../pages/transferAndPayments/setupStandingOrder.page.js');
const ConfirmNewStandingOrderPage = require('../pages/transferAndPayments/confirmNewStandingOrder.page');
const AmendStandingOrderPage = require('../pages/transferAndPayments/amendStandingOrder.page');
const ConfirmStandingOrderAmendmentsPage = require('../pages/transferAndPayments/confirmStandingOrderAmendments.page');
const AmendStandingOrderSuccessPage = require('../pages/transferAndPayments/amendStandingOrderSuccess.page');
const DeleteStandingOrderPage = require('../pages/transferAndPayments/deleteStandingOrder.page');
const AccountOptionsPage = require('../pages/transferAndPayments/accountOptions.page');
const StandingOrderReviewPage = require('../pages/transferAndPayments/standingOrderReview.page');
const StandingOrderSuccessfulPage = require('../pages/transferAndPayments/standingOrderSuccessful.page');
const StandingOrderAmendPage = require('../pages/paymentHub/standingOrderAmend.page');
const StandingOrderDeletePage = require('../pages/paymentHub/standingOrderDelete.page');

const CommonPage = require('../pages/common.page');

const reference = `StandingOrderAuto ${Math.floor(Math.random() * 10)}`;
const accountOptionsPage = new AccountOptionsPage();
const standingOrderPage = new StandingOrderPage();
const setupStandingOrderPage = new SetupStandingOrderPage();
const confirmStandingOrderAmendmentsPage = new ConfirmStandingOrderAmendmentsPage();
const deleteStandingOrderPage = new DeleteStandingOrderPage();
const confirmNewStandingOrderPage = new ConfirmNewStandingOrderPage();
const amendStandingOrderPage = new AmendStandingOrderPage();
const amendStandingOrderSuccessPage = new AmendStandingOrderSuccessPage();
const standingOrderReviewPage = new StandingOrderReviewPage();
const standingOrderSuccessfulPage = new StandingOrderSuccessfulPage();
const standingOrderAmendPage = new StandingOrderAmendPage();
const standingOrderDeletePage = new StandingOrderDeletePage();
const commonPage = new CommonPage();

const year = 2063;
const standingOrderReference = 'SO';

When(/^I select the setup standing order from standing order page$/, () => {
    standingOrderPage.selectSetupStandingOrder();
});

When(/^I should see standing order page$/, () => {
    accountOptionsPage.selectOneOfYourAccountsTab();
});

When(/^I validate standing orders tab is displayed in the standing order and direct debit page$/, () => {
    standingOrderPage.waitForPageLoad();
});

When(/^I select the standing order tab in the standing order and direct debit page$/, () => {
    standingOrderPage.selectStandingOrder();
});

When(/^I validate delete standing order button is not visible in the standing order and direct debit page$/, () => {
    standingOrderPage.verifyDeleteStandingOrderNotVisible();
});

When(/^I click on setup a standing order in the standing orders tab$/, () => {
    standingOrderPage.selectSetupStandingOrder();
});

When(/^I should see one of your accounts tab in the standing order page$/, () => {
    accountOptionsPage.verifyOneOfYourAccountsTab();
});

When(/^I click on one of your accounts tab in the standing order page$/, () => {
    accountOptionsPage.selectOneOfYourAccountsTab();
});

When(/^I select the account from the account list$/, () => {
    setupStandingOrderPage.selectStandingOrderAccount();
});

When(/^I enter the reference in the standing order page$/, () => {
    setupStandingOrderPage.standingOrderReference(reference);
});

When(/^I enter amount in the standing order page$/, () => {
    setupStandingOrderPage.enterStandingOrderAmount('1');
});

When(/^I select frequency in the standing order page$/, () => {
    setupStandingOrderPage.selectFrequency();
});

When(/^I click on continue setup in the standing order page$/, () => {
    setupStandingOrderPage.selectContinueStandingOrder();
});

When(/^I see the transaction limit exceeded message in the standing order page$/, () => {
    setupStandingOrderPage.transactionLimitExceededIsVisible();
});

When(/^I validate confirm new standing order page is displayed$/, () => {
    confirmNewStandingOrderPage.waitForPageLoad();
});

When(/^I confirm the correct authentication password in the confirm standing order page$/, function () {
    const data = this.dataRepository.get();
    confirmNewStandingOrderPage.waitForPageLoad();
    confirmNewStandingOrderPage.enterPassword(data.password);
});

When(/^I click on confirm setup in the confirm standing order page$/, () => {
    confirmNewStandingOrderPage.waitForPageLoad();
    confirmNewStandingOrderPage.confirmStandingOrder();
});

When(/^I select view standing orders in the standing order setup successful page$/, () => {
    standingOrderSuccessfulPage.waitForPageLoad();
    standingOrderSuccessfulPage.clickViewStandingOrder();
});

When(/^I should see the newly created standing order$/, () => {
    standingOrderPage.verifyNewStandingOrder(reference);
});

When(/^I click on amend in the standing order and direct direct page$/, () => {
    standingOrderPage.selectAmendStandingOrder();
});

When(/^I validate amend standing order page is displayed$/, () => {
    amendStandingOrderPage.waitForPageLoad();
});

When(/^I enter amount in the amend a standing order page$/, () => {
    amendStandingOrderPage.waitForPageLoad();
    amendStandingOrderPage.amendStandingOrderAmount('5');
});

When(/^I confirm the correct authentication password in the amend a standing order page$/, function () {
    const data = this.dataRepository.get();
    confirmStandingOrderAmendmentsPage.enterAmendPassword(data.password);
});

When(/^I click on confirm amendments button in the confirm amend standing order page$/, () => {
    confirmStandingOrderAmendmentsPage.confirmAmendStandingOrder();
});

When(/^I validate standing order amendment success page is displayed$/, () => {
    amendStandingOrderSuccessPage.waitForPageLoad();
    amendStandingOrderPage.waitForPageLoad();
});

When(/^I select view standing orders in the standing order amendment success page$/, () => {
    amendStandingOrderSuccessPage.clickViewStandingOrders();
});

When(/^I select delete option in the standing order page$/, () => {
    standingOrderPage.selectDeleteStandingOrder();
});

When(/^I confirm the correct authentication password in the delete standing order page$/, function () {
    const data = this.dataRepository.get();
    deleteStandingOrderPage.enterPassword(data.password);
});

When(/^I click delete standing order in the confirm delete standing order page$/, () => {
    deleteStandingOrderPage.confirmDeleteStandingOrder();
});

When(/^I should see the standing order deleted successful message in the standing order page$/, () => {
    standingOrderPage.verifyDeleteStandingOrderSuccessMsg();
});

When(/^I should see standing order cannot be cancelled message$/, () => {
    standingOrderPage.verifyStandingOrderNotCancelledMsg();
});

Then(/^I have chosen to make this a Standing Order option and dismiss popup for (.*)$/, (toAccount) => {
    standingOrderPage.toggleSODismissAlertISA(toAccount);
});

Then(/^I have chosen to make this a Standing Order option$/, () => {
    standingOrderPage.toggleStandingOrder();
});

Then(/^I select standing order frequency (.*) in standing order page$/, (frequency) => {
    standingOrderPage.selectStandingOrderFrequencyDropDown();
    standingOrderPage.selectStandingOrderFrequency(frequency);
});

Then(/^I select the Start Date for standing order$/, () => {
    standingOrderPage.selectStandingOrderStartDate();
});

Then(/^I enter the (.*) reference for the standing order$/, (value) => {
    standingOrderPage.enterReference(value);
});

Then(/^I select the End Date for selected (.*) for a standing order$/, (frequency) => {
    standingOrderPage.selectStandingOrderEndDate(frequency);
});

When(/^I choose standing order (.*)$/, (frequencyType) => {
    standingOrderPage.selectFrequency(frequencyType);
});

When(/^I provide (\d+) for a standing order$/, (amount) => {
    standingOrderPage.enterAmount(amount);
});

When(/^I provide Reference for a standing order$/, () => {
    standingOrderPage.enterReference(standingOrderReference);
});

When(/^I should be on the review Standing order page$/, () => {
    standingOrderReviewPage.waitForPageLoad();
});

When(/^I review the Standing Order$/, () => {
    standingOrderPage.reviewStandingOrder();
    standingOrderReviewPage.selectSOReviewConfirmButton();
});

Then(/^I should see a Standing Order set up success message$/, () => {
    standingOrderSuccessfulPage.waitForPageLoad();
});

Then(/^I select standing order option on standing order success page$/, () => {
    standingOrderSuccessfulPage.clickViewStandingOrder();
});

Then(/^Customer wants to make the payment a standing order$/, () => {
    standingOrderPage.selectStandingOrderSwitch();
});

Then(/^I should be shown a warning message stating my remaining allowance for (.*)$/, (toAccount) => {
    standingOrderPage.warningMessageISA();
    standingOrderPage.validateISAWarningMessageForSO(toAccount);
});

When(/^I select Edit option on standing order from review$/, () => {
    standingOrderPage.reviewStandingOrder();
    standingOrderPage.selectEditStandingOrder();
});

When(/^I select Edit option on standing order review page$/, () => {
    standingOrderPage.selectEditStandingOrder();
});

Then(/^I should be navigated back to previous screen with the data pre-populated$/, () => {
    standingOrderPage.prePopulatedSO();
});

When(/^I select NGA header back button$/, () => {
    standingOrderPage.reviewStandingOrder();
    standingOrderPage.selectBackFromReviewSO();
});

When(/^I choose to select the Start date for Standing Order$/, () => {
    standingOrderPage.chooseStandingOrderStartDate();
});

Then(/^I should be allowed to select a payment date up to 31 days after the current date$/, () => {
    standingOrderPage.selectDateAfter31Days();
});

When(/^I navigate back to home page from standing order$/, () => {
    commonPage.navigateHomeFromStandingOrder();
});

When(/^I choose to select the End date for Standing Order$/, () => {
    standingOrderPage.chooseStandingOrderEndDate();
});

When(/^I select the year drop down from the Calendar module$/, () => {
    standingOrderPage.selectMonthYearPicker();
});

Then(/^the Year picker should be enabled$/, () => {
    standingOrderPage.yearPickerEnabled();
});

Then(/^the Month picker should be enabled$/, () => {
    standingOrderPage.monthPickerEnabled();
});

Then(/^I should be able to select year up to 2063 for the selected (.*)$/, (frequencyType) => {
    standingOrderPage.selectYear(year, frequencyType);
});

Then(/^I should be able to select month and year up to 2063 for the selected (.*)$/, (frequencyType) => {
    standingOrderPage.selectMonthAndYear(year, frequencyType);
});

When(/^I should see the standing order decline page$/, () => {
    standingOrderPage.verifyStandingOrderDeclineMsg();
});

When(/^I should see the standing order under review page$/, () => {
    standingOrderPage.verifyStandingOrderUnderReviewMsg();
});

When(/^I should see standing order option on standing order under review page$/, () => {
    standingOrderPage.shouldSeeStandingOrderOption();
});

When(/^I select standing order option on standing order under review page$/, () => {
    standingOrderPage.selectStandingOrderOption();
});

Then(/^I should have the option to navigate to home screen$/, () => {
    standingOrderPage.viewBackButton();
});

Then(/^I change the reference (.*) of existing standing order$/, (ref) => {
    standingOrderAmendPage.changeReference(ref);
});

Then(/^I change the amount (.*) of existing standing order$/, (amount) => {
    standingOrderAmendPage.changeAmount(amount);
});

Then(/^I select amend in amend standing order page$/, () => {
    standingOrderAmendPage.selectAmend();
});

Then(/^I enter password in amend standing order page$/, function () {
    const data = this.dataRepository.get();
    standingOrderAmendPage.enterSOAmendPassword(data.password);
});

Then(/^I select confirm amendments button in the amend standing order confirmation page$/, () => {
    standingOrderAmendPage.selectConfirmAmendments();
});

Then(/^I should be on confirm standing order amendments page$/, () => {
    standingOrderAmendPage.waitForPageLoad();
});

Then(/^I should be on standing order changed page$/, () => {
    standingOrderAmendPage.verifyStandingOrderAmendment();
});

Then(/^I select view standing orders in the standing order amend success page$/, () => {
    standingOrderAmendPage.selectViewStandingOrder();
});

Then(/^I should be on delete standing order page$/, () => {
    standingOrderDeletePage.waitForPageLoad();
});

Then(/^I enter password in delete standing order page$/, function () {
    const data = this.dataRepository.get();
    standingOrderDeletePage.enterPassword(data.password);
});

Then(/^I select delete standing order in the delete standing order confirmation page$/, () => {
    standingOrderDeletePage.confirmDeleteStandingOrder();
});

Then(/^I should see standing order deleted successfully message in view standing orders page$/, () => {
    standingOrderDeletePage.verifySODeleteSuccessMessage();
});

Then(/^I should see the following fields on standing orders page$/, (info) => {
    const fieldsToValidate = info.hashes();
    standingOrderPage.viewFieldsOnStandingOrderPage(fieldsToValidate);
});

Then(/^I should see the option to exit Internet Banking$/, () => {
    standingOrderPage.shouldSeeExitInternetBanking();
});

Then(/^I should see the below fields for standing order are (editable|not editable)$/, (isEditable, table) => {
    const tableData = table.raw();
    standingOrderPage.isStandingOrderFieldEditable(isEditable === 'editable', tableData);
});

Then(/^I update the standing order (.*) field with value as (.*)$/, (field, fieldValue) => {
    standingOrderPage.updateFieldValue(field, fieldValue);
});

Then(/^I get the field values for the Standing order set up$/, () => {
    standingOrderPage.getStandingOrderValues();
});

Then(/^I should see the below fields on the Standing order review page$/, (table) => {
    const tableData = table.raw();
    standingOrderReviewPage.validateFieldValuesOnSOReviewPage(tableData);
});

Then(/^I should see standing order not set up error message$/, () => {
    standingOrderSuccessfulPage.viewErrorSOMessage();
});
