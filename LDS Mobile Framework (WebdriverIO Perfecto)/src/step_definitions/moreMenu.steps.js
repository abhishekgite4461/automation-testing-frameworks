const {When, Then} = require('cucumber');
const MorePage = require('../pages/more.page');
const ChequeDepositPage = require('../pages/ics/chequeDeposit.page');
const MarketingPreferencesHubPage = require('../pages/settings/personalDetails/marketingPreferencesHub.page');
const SettingsPage = require('../pages/settings/settings.page');
const DarkUrlPage = require('../pages/settings/darkUrl.page');
const HomePage = require('../pages/home.page');
const NavigationTask = require('../tasks/navigation.task');
const PersonalDetailsPage = require('../pages/settings/personalDetails/personalDetails.page');

const morePage = new MorePage();
const chequeDepositPage = new ChequeDepositPage();
const personalDetailsPage = new PersonalDetailsPage();
const marketingPreferencesHubPage = new MarketingPreferencesHubPage();
const settingsPage = new SettingsPage();
const darkURLPage = new DarkUrlPage();
const homePage = new HomePage();

Then(/^I should be on more menu$/, () => {
    morePage.waitForPageLoad();
});

When(/^I should see the below options in the more menu$/, (table) => {
    morePage.waitForPageLoad();
    morePage.validateOptionsAvailable(table.hashes());
});

When(/^I should not see the below options in the more menu$/, (table) => {
    morePage.validateOptionsAvailable(table.hashes(), false);
});

When(/^I select settings from more menu$/, () => {
    morePage.selectSettings();
});

When(/^I select more menu$/, NavigationTask.openMoreMenu);

Then(/^I should see switch business icon on More screen$/, () => {
    morePage.verifySwitchBusinessIconIsPresent();
});

Then(/^I select switch business icon on more menu/, () => {
    morePage.selectSwitchBusinessIcon();
});

Then(/^I should not see switch business icon on More screen$/, () => {
    morePage.verifySwitchBusinessIconIsNotPresent();
});

Then(/^I verify name of the (.*) on the more menu$/, (business) => {
    morePage.verifyBusinessName(business);
});

When(/^I navigate to settings page via more menu$/, NavigationTask.openSettingsFromMoreMenu);

When(/^I navigate to logoff via more menu$/, () => {
    NavigationTask.openMoreMenu();
    morePage.selectLogoff();
});

When(/^I should not be able to see Deposit Cheque & History Cheque entry points$/, () => {
    morePage.verifyChequeSubOptionsAreNotVisible();
});

When(/^I navigate to cheque deposit page via more menu$/, () => {
    NavigationTask.openMoreMenu();
    morePage.selectChequeDeposit();
    chequeDepositPage.checkMoreInformationAlert();
});

When(/^I should be shown Cheque Deposit option$/, () => {
    morePage.verifyChequeDepositEntryPointOn();
});

When(/^I should not be shown Cheque Deposit option$/, () => {
    morePage.verifyChequeDepositEntryPointOff();
});

When(/^I navigate to cheque deposit page for more information via more menu$/, () => {
    NavigationTask.openMoreMenu();
    morePage.selectChequeDeposit();
});

When(/^I navigate to deposit history page via more menu$/, () => {
    NavigationTask.openMoreMenu();
    morePage.selectDepositHistory();
    chequeDepositPage.checkMoreInformationAlert();
});

When(/^I navigate to inbox page via more menu$/, () => {
    NavigationTask.openMoreMenu();
    morePage.selectInbox();
});

When(/^I navigate to profile page via more menu$/, NavigationTask.navigateToProfile);

When(/^I navigate to marketing preferences hub via more menu$/, () => {
    NavigationTask.openYourProfileFromMoreMenu();
    personalDetailsPage.isPageVisible();
    personalDetailsPage.selectPrimaryMarketingPreferencesLink();
    marketingPreferencesHubPage.waitForMarketingPreferenceHubPageLoad();
});

When(/^I navigate to pay a contact settings page via more menu$/, () => {
    NavigationTask.openMoreMenu();
    morePage.selectSettings();
    settingsPage.waitForPageLoad();
    settingsPage.selectPayAContactSettings();
});

When(/^I navigate to legal info from settings page via more menu$/, () => {
    NavigationTask.openMoreMenu();
    morePage.selectSettings();
    settingsPage.waitForPageLoad();
    settingsPage.selectLegalInfo();
});
When(/^I select the your profile option on more menu$/, () => {
    morePage.selectYourProfile();
});

When(/^I should not be shown Cheque History option$/, () => {
    morePage.shouldNotShowChequeHistoryEntryPoint();
});

When(/^I should be shown Cheque History option$/, () => {
    morePage.shouldShowChequeHistoryEntryPoint();
});

When(/^I navigate to card management page via more menu$/, () => {
    NavigationTask.openMoreMenu();
    morePage.selectCardManagement();
});

When(/^I navigate to the dark URL page$/, () => {
    morePage.selectDarkURL();
});

When(/^I should (see|not see) the dark url link$/, (isVisible) => {
    if (isVisible === 'see') {
        morePage.verifyVisibilityOfDarkUrl(true);
    } else {
        morePage.verifyVisibilityOfDarkUrl(false);
    }
});

Then(/^I should see a button labelled (.*) to enter the URL$/, (label) => {
    darkURLPage.waitForPageLoad();
    darkURLPage.confirmOpenButtonText(label);
});

When(/^I verify users first name$/, () => {
    homePage.getFirstName();
    NavigationTask.openMoreMenu();
    morePage.validateUserFirstName();
});
