const {When, Then} = require('cucumber');
const ViewSpendingInsightsPage = require('../pages/viewSpendingInsights.page');

const viewSpendingInsightsPage = new ViewSpendingInsightsPage();

Then(/^I should be able to view spending insights webview page$/, () => {
    viewSpendingInsightsPage.waitForPageLoad();
});

When(/^I select Debit card in view spending insights webview page$/, () => {
    viewSpendingInsightsPage.selectDebitCardOption();
});

Then(/^I should be on Debit Card Insight webview page$/, () => {
    viewSpendingInsightsPage.shouldSeeDebitCardInsightPage();
});
