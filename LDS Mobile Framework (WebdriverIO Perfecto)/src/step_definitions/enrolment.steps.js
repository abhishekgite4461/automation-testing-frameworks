const {When, Then} = require('cucumber');

const DeviceHelper = require('../utils/device.helper');
const Server = require('../enums/server.enum');

const EnrolmentPage = require('../pages/enrolment.page');
const EnrolmentSuccessPage = require('../pages/enrolmentSuccess.page');
const EnrolmentSecurityPage = require('../pages/enrolmentSecurity.page');
const RequestOtpPage = require('../pages/requestOtp.page');
const RequestOtpConfirmationPage = require('../pages/requestOtpConfirmation.page');
const RequestOtpCancelPopUpPage = require('../pages/requestOtpCancelPopUp.page');
const EnterPostalOtpPage = require('../pages/enterPostalOtp.page');
const EnterPostalOtpTooltipPage = require('../pages/enterPostalOtpTooltip.page');
const NativeCallOverlayPage = require('../pages/nativeCallOverlay.page');
const EnrolmentEIABypassPage = require('../pages/enrolmentEIABypass.page');
const ExpiredOtpErrorPage = require('../pages/expiredOtpError.page');
const AddressChangedOtpErrorPage = require('../pages/addressChangedOtpError.page');
const EnrolmentTermsAndConditionPage = require('../pages/EnrolmentTermsAndConditions.page');
const EnrolmentDeclineTermsAndConditions = require('../pages/EnrolmentDeclineTermsAndConditions.page');
const EnrolmentTermsAndConditionDeclinedErrorPage = require('../pages/EnrolmentTermsAndConditionsDeclinedError.page');
const CardReaderRespondPage = require('../pages/enrolmentCardReaderRespond.page');
const EnvironmentApiClient = require('../utils/environmentApiClient');

const enrolmentPage = new EnrolmentPage();
const enterPostalOtpPage = new EnterPostalOtpPage();
const nativeCallOverlayPage = new NativeCallOverlayPage();
const expiredOtpErrorPage = new ExpiredOtpErrorPage();
const enrolmentDeclineTermsAndConditions = new EnrolmentDeclineTermsAndConditions();
const enrolmentTermsAndConditionPage = new EnrolmentTermsAndConditionPage();
const enrolmentTermsAndConditionDeclinedErrorPage = new EnrolmentTermsAndConditionDeclinedErrorPage();
const addressChangedOtpErrorPage = new AddressChangedOtpErrorPage();
const enrolmentEIABypassPage = new EnrolmentEIABypassPage();
const requestOtpPage = new RequestOtpPage();
const requestOtpCancelPopUpPage = new RequestOtpCancelPopUpPage();
const enterPostalOtpTooltipPage = new EnterPostalOtpTooltipPage();
const cardReaderRespondPage = new CardReaderRespondPage();

const incorrectOTP = 'incorrectOTP123';
const expiredOTP = 'expiredOTP123';
const passcode = '00000000';

const enrolmentSecurityPage = new EnrolmentSecurityPage();
const enrolmentSuccessPage = new EnrolmentSuccessPage();
const requestOtpConfirmationPage = new RequestOtpConfirmationPage();

When(/^I finish enrolment$/, () => {
    if (DeviceHelper.isBNGA()) {
        cardReaderRespondPage.waitForPageLoad();
        cardReaderRespondPage.enterPasscode(passcode);
        cardReaderRespondPage.selectContinueButton();
    } else {
        enrolmentPage.clickCallMeNowOption();
        const server = Server.fromString(browser.capabilities.server);
        if (!server.isEiaBypassed) {
            enrolmentPage.attendEiaCallAndEnterFourDigitNumber();
        }
    }
    enrolmentSuccessPage.waitForPageLoad();
    enrolmentSuccessPage.clickContinueOnEnrolmentPage();
    enrolmentSecurityPage.waitForPageLoad();
    enrolmentSecurityPage.clickContinueOnSecurityEnrolmentPage();
});

Then(/^I should be on the EIA page$/, () => {
    enrolmentPage.waitForEiaCallMeNowPageLoad();
});

When(/^I choose call me now option$/, () => {
    enrolmentPage.clickCallMeNowOption();
});

When(/^I should see all the phone number should be masked in the EIA page$/, () => {
    enrolmentPage.validateMaskedPhoneNumbers();
});

When(/^I should see contact us option on the EIA page$/, () => {
    enrolmentPage.waitForEiaCallMeNowPageLoad();
    enrolmentPage.shouldSeeContactUsButton();
});

When(/^I choose to receive EIA call on (.*)$/, (number) => {
    enrolmentPage.selectNumber(number);
});

When(/^I select the cancel option on the EIA page$/, () => {
    enrolmentPage.clickEIACancelOption();
});

When(/^I cancel the pop-up message on the EIA page$/, () => {
    enrolmentPage.clickExitCancelOption();
});

When(/^I accept the pop-up message on the EIA page$/, () => {
    enrolmentPage.clickExitYesOption();
});
Then(/^I select continue button on the enrolment confirmation page$/, () => {
    enrolmentSuccessPage.waitForPageLoad();
    enrolmentSuccessPage.clickContinueOnEnrolmentPage();
});

Then(/^I should see contact us option in enrolment confirmation page$/, () => {
    enrolmentSuccessPage.waitForPageLoad();
    enrolmentSuccessPage.shouldSeeContactUsButton();
});

Then(/^I am shown Enrolment success screen$/, () => {
    enrolmentSuccessPage.waitForPageLoad();
});

Then(/^I choose to continue from Enrolment success screen$/, () => {
    enrolmentSuccessPage.clickContinueOnEnrolmentPage();
});

When(/^I choose my security auto logout settings$/, () => {
    enrolmentSecurityPage.waitForPageLoad();
    enrolmentSecurityPage.clickContinueOnSecurityEnrolmentPage();
});

Then(/^I proceed from Logoff timeout settings screen$/, () => {
    enrolmentSecurityPage.clickContinueOnSecurityEnrolmentPage();
});

When(/^I should see contact us option on the auto logout settings page$/, () => {
    enrolmentSecurityPage.waitForPageLoad();
    enrolmentSecurityPage.shouldSeeContactUsButton();
});

When(/^I verify the default time in the security auto logout settings page$/, () => {
    enrolmentSecurityPage.waitForPageLoad();
    enrolmentSecurityPage.verifyDefaultTimeSettings();
});

When(/^I should see contact us option in the security auto logout settings page$/, () => {
    enrolmentSecurityPage.waitForPageLoad();
    enrolmentSecurityPage.shouldSeeContactUsButton();
});

Then(/^I should see Logoff settings screen with selected default timeout settings$/, () => {
    enrolmentSecurityPage.waitForPageLoad();
    enrolmentSecurityPage.verifyDefaultTimeSettings();
});

When(/^I should see contact us button on the request OTP page$/, () => {
    requestOtpPage.waitForPageLoad();
    requestOtpPage.shouldSeeContactUsButton();
});

When(/^I select the cancel option on the request OTP page$/, () => {
    requestOtpPage.waitForPageLoad();
    requestOtpPage.selectCancelButton();
});

When(/^I select the confirm option on the Request OTP page$/, () => {
    requestOtpPage.waitForPageLoad();
    requestOtpPage.selectConfirmButton();
});

When(/^I should be on the request OTP page$/, () => {
    requestOtpPage.verifyRequestOtpScreenIsVisible();
});

When(/^I should be on the request OTP confirmation page$/, () => {
    requestOtpConfirmationPage.waitForPageLoad();
});

When(/^I should see contact us button on the request OTP confirmation page$/, () => {
    requestOtpConfirmationPage.shouldSeeContactUsButton();
});

When(/^I cancel the pop-up on the request OTP page$/, () => {
    requestOtpCancelPopUpPage.waitForPageLoad();
    requestOtpCancelPopUpPage.selectCancelButton();
});

When(/^I accept the pop-up on the request OTP page$/, () => {
    requestOtpCancelPopUpPage.waitForPageLoad();
    requestOtpCancelPopUpPage.selectConfirmButton();
});

When(/^I select the tooltip on the enter postal address otp page$/, () => {
    enterPostalOtpPage.waitForPageLoad();
    enterPostalOtpPage.selectNotReceivedTooltip();
});

When(/^I select the close option on the not recieved tooltip in postal address otp page$/, () => {
    enterPostalOtpTooltipPage.waitForPageLoad();
    enterPostalOtpTooltipPage.selectCloseButton();
});

When(/^I select the contact us option on the not recieved tooltip in postal address otp page$/, () => {
    enterPostalOtpTooltipPage.waitForPageLoad();
    enterPostalOtpTooltipPage.selectContactUsButton();
});

When(/^I should be on the enter postal address otp page$/, () => {
    enterPostalOtpPage.waitForPageLoad();
});

When(/^I should see the overlay with call option$/, () => {
    nativeCallOverlayPage.shouldSeeNativeCallOverlay();
});

Then(/^I select the contact us option from the enrolment confirmation page pre auth header$/, () => {
    enrolmentSuccessPage.waitForPageLoad();
    enrolmentSuccessPage.selectContactUs();
});

When(/^I select the contact us option from the enrolment security settings page pre auth header$/, () => {
    enrolmentSecurityPage.waitForPageLoad();
    enrolmentSecurityPage.selectContactUs();
});

Then(/^I select continue button on the no security call needed page$/, () => {
    enrolmentEIABypassPage.waitForPageLoad();
    enrolmentEIABypassPage.clickContinueOnEIABypassPage();
});

Then(/^I select the contact us option from the enter postal address otp page$/, () => {
    enterPostalOtpPage.waitForPageLoad();
    enterPostalOtpPage.selectContactUs();
});

Then(/^I should see logoff option on the enter postal address otp page$/, () => {
    enterPostalOtpPage.shouldSeeLogoffOption();
});

Then(/^I enter (invalid|expired) OTP in the enter one time password page$/, (type) => {
    enterPostalOtpPage.waitForPageLoad();
    if (type === 'invalid') {
        enterPostalOtpPage.enterOTP(incorrectOTP);
    } else {
        enterPostalOtpPage.enterOTP(expiredOTP);
    }
});

Then(/^I should see incorrect OTP message in the enter one time password page$/, () => {
    enterPostalOtpPage.verifyIncorrectOTPMessage();
});

Then(/^I should be on the expired OTP error page$/, () => {
    expiredOtpErrorPage.waitForPageLoad();
    expiredOtpErrorPage.verifyExpiredOtpErrorMessage();
});

Then(/^I select request password button in the security call failed page$/, () => {
    requestOtpPage.waitForPageLoad();
    requestOtpPage.selectConfirmButton();
});

Then(/^I should be on the address changed error page$/, () => {
    addressChangedOtpErrorPage.waitForPageLoad();
    addressChangedOtpErrorPage.verifyAddressChangedErrorMessage();
});

Then(/^I select the (.*) button on the terms and conditions page$/, (type) => {
    enrolmentTermsAndConditionPage.waitForPageLoad();
    if (type === 'accept') {
        enrolmentTermsAndConditionPage.acceptTermsAndConditions();
    } else {
        enrolmentTermsAndConditionPage.declineTermsAndConditions();
    }
});

Then(/^I select the (.*) option on the decline terms and conditions page$/, (type) => {
    enrolmentDeclineTermsAndConditions.waitForPageLoad();
    if (type === 'decline') {
        enrolmentDeclineTermsAndConditions.confirmDecliningTermsAndConditions();
    } else {
        enrolmentDeclineTermsAndConditions.backToTermsAndConditionsPage();
    }
});

Then(/^I should be on the terms and conditions declined error page$/, () => {
    enrolmentTermsAndConditionDeclinedErrorPage.waitForPageLoad();
    enrolmentTermsAndConditionDeclinedErrorPage.verifyDeclinedErrorMessage();
});

Then(/^I should be on the terms and conditions page with accept and decline button$/, () => {
    enrolmentTermsAndConditionPage.waitForPageLoad();
    enrolmentTermsAndConditionPage.verifyAcceptButton();
    enrolmentTermsAndConditionPage.verifyDeclineButton();
});

When(/^I capture and enter the 4 digit pin for eia call$/, () => {
    enrolmentPage.attendEiaCallAndEnterFourDigitNumber();
});

When(/^I capture and enter the 4 digit pin after (.*) for eia call in webview$/, (miliseconds) => {
    enrolmentPage.webViewAttendEiaCallAndEnterFourDigitNumber(miliseconds);
});

When(/^I select the contact us option from the EIA page pre auth header$/, () => {
    enrolmentPage.waitForEiaCallMeNowPageLoad();
    enrolmentPage.selectContactUs();
});

When(/^I should not see the contact us option on the EIA calling now page$/, () => {
    enrolmentPage.waitForEiaCallingPageLoad();
    enrolmentPage.shouldNotSeeContactUsButton();
});

When(/^I should see contact us option in the phone authentication not required page$/, () => {
    enrolmentPage.waitForEiaCallNotRequiredPageLoad();
    enrolmentPage.shouldSeeContactUsButton();
});

When(/^I select continue button on the phone authentication not required page$/, () => {
    enrolmentPage.selectEnrolmentBypassContinueButton();
});

When(/^I set a phone number in the desktop site$/, function () {
    const data = this.dataRepository.get();
    EnvironmentApiClient.modifyMobileNumber(
        browser.capabilities.server,
        browser.capabilities.appType,
        browser.capabilities.brand,
        data.username,
        DeviceHelper.mobileNumber()
    );
});

When(/^I set the phone number config at backend to "(.*)" days$/, (days) => {
    EnvironmentApiClient.phoneNumberConfig(
        browser.capabilities.server,
        browser.capabilities.appType,
        browser.capabilities.brand,
        days
    );
});

Then(/^I (should|should not) see the mobile number is greyed out$/, (isVisible) => {
    if (isVisible === 'should') {
        enrolmentPage.checkGreyedOutEnability(false);
    } else {
        enrolmentPage.checkGreyedOutEnability(true);
    }
});
