const {When} = require('cucumber');
const DeviceHelper = require('../utils/device.helper');

const AppStorePage = require('../pages/appStore.page');
const Actions = require('../pages/common/actions');

const appStorePage = new AppStorePage();

When(/^I navigate to app store screen$/, () => {
    appStorePage.waitForAppStorePageLoad();
});

When(/^I validate app name in app store$/, () => {
    appStorePage.verifyAppTextInAppStore();
});

When(/^I navigate back to app from app store$/, () => {
    if (DeviceHelper.isIOS()) {
        appStorePage.selectDoneOrCancelButton();
    } else {
        Actions.pressAndroidBackKey();
        Actions.pause(10000);
    }
});

When(/^I validate the app version (.*) on app store page$/, (expectedAppVersion) => {
    appStorePage.compareAppVersions(expectedAppVersion);
});
