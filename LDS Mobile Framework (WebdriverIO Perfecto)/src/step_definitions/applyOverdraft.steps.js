const {When} = require('cucumber');
const ApplyOverdraftPage = require('../pages/applyOverdraft.page');

const applyOverdraftPage = new ApplyOverdraftPage();

When(/^I should see the apply overdraft details page$/, () => {
    applyOverdraftPage.waitForPageLoad();
});

When(/^I should close the overdraft info module$/, () => {
    applyOverdraftPage.closeOverdraftInfoModule();
});

When(/^I select the account to apply the overdraft$/, () => {
    applyOverdraftPage.selectAccount();
});

When(/^I input the overdraft amount (.*) in the overdraft input box$/, (overdraft) => {
    applyOverdraftPage.enterOverdraftAmount(overdraft);
});

When(/^I should see the calculation for the overdraft$/, () => {
    applyOverdraftPage.waitForFeeCalculator();
});

When(/^I land on the Income and Expenditure screen$/, () => {
    applyOverdraftPage.waitForINEPage();
});

When(/^I select continue button on the overdraft screen$/, () => {
    applyOverdraftPage.selectOverdraftContinueButton();
});

When(/^I select full time employment status on the Income and Expenditure screen$/, () => {
    applyOverdraftPage.selectEmploymentStatusFullTime();
});

When(/^I enter income (.*) on the Income and Expenditure screen$/, (income) => {
    applyOverdraftPage.enterIncomeAmount(income);
});

When(/^I enter mortgage (.*) on the Income and Expenditure screen$/, (mortgage) => {
    applyOverdraftPage.enterMortgageFee(mortgage);
});

When(/^I enter maintenance (.*) on the Income and Expenditure screen$/, (maintenance) => {
    applyOverdraftPage.enterMaintenanceFee(maintenance);
});

When(/^I select No for something about to change question on the Income and Expenditure screen$/, () => {
    applyOverdraftPage.selectNoChangeOption();
});

When(/^I select continue on the Income and Expenditure screen$/, () => {
    applyOverdraftPage.selectINEContinueButton();
});

When(/^I confirm my email and select continue$/, () => {
    applyOverdraftPage.selectConfirmEmail();
});

When(/^I confirm the fee on the PCCI page and continue$/, () => {
    applyOverdraftPage.selectConfirmPCCI();
});

When(/^I verify the overdraft success page with (.*)$/, (overdraft) => {
    applyOverdraftPage.verifyOverdraftSuccessPage(overdraft);
});

When(/^I should see overdraft success page for removing overdraft$/, () => {
    applyOverdraftPage.shouldSeeRemoveOverdraftSuccessPage();
});

When(/^I select remove button on the overdraft screen$/, () => {
    applyOverdraftPage.selectOverdraftRemoveButton();
});

When(/^I verify the existing overdraft should be greater than 0$/, () => {
    applyOverdraftPage.verifyExistingOverdraft();
});
