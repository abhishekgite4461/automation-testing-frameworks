const {When} = require('cucumber');

const ViewPin = require('../../pages/viewPin/viewPin.Page');

const viewPin = new ViewPin();

When(/^I select to reveal the PIN$/, () => {
    viewPin.selectViewPinOption();
});

When(/^the PIN number should be displayed for also long as I am holding the reveal PIN button$/, () => {
    viewPin.validatePINDisplay();
});
