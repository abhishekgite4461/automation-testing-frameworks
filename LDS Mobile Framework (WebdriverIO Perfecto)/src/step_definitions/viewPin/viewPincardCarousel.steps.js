const {Given, When} = require('cucumber');

const ViewPinCardCarousel = require('../../pages/viewPin/viewPinCardCarousel.Page.js');

const viewPinCardCarousel = new ViewPinCardCarousel();

Given(/^I choose the View PIN option$/, () => {
    viewPinCardCarousel.selectViewPinButton();
});

When(/^I successfully enter my password$/, function () {
    const data = this.dataRepository.get();
    viewPinCardCarousel.enterPassword(data.password);
});
