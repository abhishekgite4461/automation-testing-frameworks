const {When, Then} = require('cucumber');

const SupportHubHomePage = require('../../pages/supportHubHomePage.page');

const supportHubHomePage = new SupportHubHomePage();

Then(/^I should see an option to View PIN for my cards$/, () => {
    supportHubHomePage.shouldSeeViewPinEntry();
});

When(/^I select the View PIN option from the Help or Support menu$/, () => {
    supportHubHomePage.selectViewPinOrReplaceCard();
    supportHubHomePage.selectViewPinOption();
});
