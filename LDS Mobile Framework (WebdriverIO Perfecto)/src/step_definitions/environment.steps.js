const {Then} = require('cucumber');

const EnvironmentPage = require('../pages/environment.page');
const Server = require('../enums/server.enum');

Then(/^I should be on the environment selector page$/, () => {
    new EnvironmentPage();
});

Then(/^I select the environment$/, () => {
    const server = Server.fromString(browser.capabilities.server);
    const environmentPage = new EnvironmentPage();
    environmentPage.selectEnvironment(server);
});

Then(/^I change an app version to (.*) on environment selection page$/, (appVersion) => {
    const environmentPage = new EnvironmentPage();
    environmentPage.enterAppVersion(appVersion);
});
