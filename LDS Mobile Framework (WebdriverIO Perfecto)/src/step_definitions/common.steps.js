const {When, Then} = require('cucumber');

const CommonPage = require('../pages/common.page');
const Actions = require('../pages/common/actions');

const commonPage = new CommonPage();

When(/^I wait for (.*) seconds$/, (waitTime) => {
    Actions.pause(waitTime * 1000);
});

When(/^I navigate back$/, () => {
    commonPage.clickBackButton();
});

When(/^I navigate back to call us from secure call page$/, () => {
    commonPage.clickBackButton();
    commonPage.clickBackButton();
});

When(/^I navigate back to home page$/, () => {
    commonPage.clickHomeButton();
});

When(/^I choose to (.*) option on winback dialogue$/, (option) => {
    commonPage.selectWinbackOptions(option);
});

When(/^I should not see header back button in modal$/, () => {
    commonPage.shouldNotSeeBackButton();
});

Then(/^I rotate the Tab to (portrait|landscape) orientation$/, (orientation) => {
    CommonPage.setDeviceOrientation(orientation);
});

Then(/^I should see the app in (portrait|landscape) orientation$/, (orientation) => {
    CommonPage.getDeviceOrientation(orientation);
});
