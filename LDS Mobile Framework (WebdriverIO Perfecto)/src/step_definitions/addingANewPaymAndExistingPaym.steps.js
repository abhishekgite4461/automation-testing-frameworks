const {When, Then} = require('cucumber');
const UkMobileNumberDetailPage = require('../pages/transferAndPayments/ukMobileNumberDetail.page');
const PaymentHubMainPage = require('../pages/paymentHub/paymentHubMain.page');
const ReviewPaymentPage = require('../pages/paymentHub/reviewPayment.page');
const SuccessPaymentPage = require('../pages/paymentHub/successPayment.page');
const CommonPage = require('../pages/common.page');
const AccountMatcher = require('../data/utils/accountMatcher');
const PaymentTask = require('../tasks/payment.task');
const ConfirmContactPage = require('../pages/transferAndPayments/confirmContact.page');

const ukMobileNumberDetailPage = new UkMobileNumberDetailPage();
const paymentHubMainPage = new PaymentHubMainPage();
const reviewPaymentPage = new ReviewPaymentPage();
const successPaymentPage = new SuccessPaymentPage();
const commonPage = new CommonPage();
const confirmContactPage = new ConfirmContactPage();

When(/^I am on enter basic detail page for pay a contact$/, () => {
    ukMobileNumberDetailPage.waitForUKMobileDetailsPageLoad();
    ukMobileNumberDetailPage.verifyUKMobileDetailPage();
});

Then(/^I should see the following details in Add PayM details page$/, (table) => {
    const tableData = table.hashes();
    ukMobileNumberDetailPage.verifyUkMobileNumberDetails(tableData);
});

When(/^I have entered 7 or less digits (.*) in the To field$/, (phoneNumber) => {
    ukMobileNumberDetailPage.enterMobileNumber(phoneNumber);
});

When(/^I enter (.*) in the Amount field$/, (amount) => {
    ukMobileNumberDetailPage.enterAmount(amount);
});

Then(/^I am shown an error message$/, () => {
    ukMobileNumberDetailPage.verifyErrorMessage();
});

When(/^I select (.*) Account from the account list$/, (accountType) => {
    const accountName = accountType.replace(/ /g, '');
    paymentHubMainPage.chooseRemitterAccount(AccountMatcher.getData(accountName));
});

When(/^I have entered valid (.*) (.*) and (.*)$/, PaymentTask.enterUkMobileNumberDetails);

When(/^I select continue in add new UK mobile number page$/, () => {
    ukMobileNumberDetailPage.clickOnContinueButtonInPaymentHubPage();
});

When(/^I select Continue$/, () => {
    ukMobileNumberDetailPage.clickOnContinueButtonInPaymentHubPage();
});

When(/^I have selected or entered a registered (.*) to receive PayM$/, (phoneNumber) => {
    ukMobileNumberDetailPage.enterMobileNumber(phoneNumber);
});

When(/^I have selected or entered an unregistered (.*) to receive PayM$/, (phoneNumber) => {
    ukMobileNumberDetailPage.enterMobileNumber(phoneNumber);
});

Then(/^I am on the Confirm Contact page$/, confirmContactPage.verifyConfirmContactMessage);

Then(/^I am displayed the following details$/, (table) => {
    const tableData = table.hashes();
    confirmContactPage.verifyingDetailsInConfirmContactPage(tableData);
});

Then(/^I select Confirm$/, confirmContactPage.clickOnContinueButtonInConfirmContactPage);

Then(/^I am taken to the Confirm Payment page$/, () => {
    reviewPaymentPage.verifyReviewPaymentPage();
});

Then(/^I Click on Cancel Button$/, () => {
    confirmContactPage.clickOnCancelButton();
});

Then(/^I am shown the add PayM details page$/, () => {
    ukMobileNumberDetailPage.verifyAddPayMPage();
});

When(/^I select continue on Confirm Contact page$/, () => {
    confirmContactPage.waitForPageLoad();
    confirmContactPage.clickOnContinueButtonInConfirmContactPage();
});

When(/^I am on the Confirm Payment Page$/, reviewPaymentPage.verifyReviewPaymentPage);

When(/^I select Cancel on the Confirm Payment Page$/, () => {
    reviewPaymentPage.selectEditButton();
});

Then(/^I am taken to the Confirm Contact page$/, () => {
    confirmContactPage.verifyConfirmContactMessage();
});

Then(/^I am Displayed with an error$/, () => {
    ukMobileNumberDetailPage.verifyErrorMessage();
});

Then(/^I select Confirm on the Confirm Payment page$/, PaymentTask.confirmPayment);

Then(/^I am shown Payment Successful page$/, () => {
    successPaymentPage.waitForPageLoad();
    successPaymentPage.verifySuccessPaymentPage();
});

When(/^I navigate back to home page from payment success page$/, () => {
    commonPage.navigateHomeFromPaymentSuccess();
});

When(/^I navigate back to home page from transfer success page$/, () => {
    commonPage.navigateHomeFromPaymentSuccess();
});

When(/^I navigate back to home page from MPA approval submitted page$/, () => {
    commonPage.closeModalAndLeave();
});

Then(/^I should see share receipt option on the (.*) success page$/, (type) => {
    successPaymentPage.verifyShareReceiptOption(type);
});

Then(/^I should not see share receipt option on the (.*) success page$/, (type) => {
    successPaymentPage.shouldNotSeeShareRecipient(type);
});

Then(/^I select the share option on the (.*) success page$/, (type) => {
    successPaymentPage.selectShareReceiptOption(type);
});
