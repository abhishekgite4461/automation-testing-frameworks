const {When, Then, And} = require('cucumber');

const ChangeAccountTypeWebViewPage = require('../pages/ChangeAccountTypeWebView.page');

const changeAccountTypeWebViewPage = new ChangeAccountTypeWebViewPage();

module.exports = function changeAccountTypeSteps() {
    Then(/^I should be on the change account type page$/, () => {
        changeAccountTypeWebViewPage.waitForPageLoad();
    });

    When(/^I select change this account button on account type page$/, () => {
        changeAccountTypeWebViewPage.selectChangeThisAccountButton();
    });

    Then(/^I should be on the update your email address page$/, () => {
        changeAccountTypeWebViewPage.waitForUpdateYourEmail();
    });

    When(/^I select email correction option on update your email address page$/, () => {
        changeAccountTypeWebViewPage.selectEmailOptionButton();
    });

    And(/^I select continue button on update your email address page$/, () => {
        changeAccountTypeWebViewPage.selectContinueButton();
    });

    And(/^I select continue button on before you continue page$/, () => {
        changeAccountTypeWebViewPage.selectContinueButton();
    });

    Then(/^I should be on select account you want page$/, () => {
        changeAccountTypeWebViewPage.waitForSelectAccountYouWant();
    });

    When(/^I select find out more button on select account you want page$/, () => {
        changeAccountTypeWebViewPage.selectFindOutMoreButton();
    });

    Then(/^I should be on the account you want page$/, () => {
        changeAccountTypeWebViewPage.waitForAccountYouWantPage();
    });

    When(/^I select continue button on the account you want page$/, () => {
        changeAccountTypeWebViewPage.selectContinueButton();
    });

    Then(/^I should be on the your account offer page$/, () => {
        changeAccountTypeWebViewPage.waitForYourAccountOfferPage();
    });

    When(/^I select the option to complete application on your account offer page$/, () => {
        changeAccountTypeWebViewPage.selectCompleteApplicationTickBox();
    });

    And(/^I select complete application button on your account offer page$/, () => {
        changeAccountTypeWebViewPage.selectCompleteApplicationButton();
    });

    Then(/^I verify current account open successful message$/, () => {
        changeAccountTypeWebViewPage.waitOpenCurrentAccountSuccessMessage();
    });

    When(/^I select continue button on before you start page$/, () => {
        changeAccountTypeWebViewPage.selectBeforeYouStartContinueButton();
    });

    Then(/^I should be on your details page$/, () => {
        changeAccountTypeWebViewPage.waitYourDetailsPage();
    });

    Then(/^I select (.*) to apply account on your details page$/, (selectTitle) => {
        changeAccountTypeWebViewPage.selectSelectTitle(selectTitle);
    });

    When(/^I enter your first name (.*) to apply account on your details page$/, (firstName) => {
        changeAccountTypeWebViewPage.enterFirstName(firstName);
    });

    When(/^I enter your middle name (.*) to apply account on your details page$/, (middleName) => {
        changeAccountTypeWebViewPage.enterMiddleName(middleName);
    });

    When(/^I enter your last name (.*) to apply account on your details page$/, (lastName) => {
        changeAccountTypeWebViewPage.enterLastName(lastName);
    });

    When(/^I enter your dob date (.*) to apply account on your details page$/, (dd) => {
        changeAccountTypeWebViewPage.enterDOBDate(dd);
    });

    When(/^I enter your dob months (.*) to apply account on your details page$/, (mm) => {
        changeAccountTypeWebViewPage.enterDOBMonths(mm);
    });

    When(/^I enter your dob years (.*) to apply account on your details page$/, (yyyy) => {
        changeAccountTypeWebViewPage.enterDOBYears(yyyy);
    });

    When(/^I select your gender type to apply account on your details page$/, () => {
        changeAccountTypeWebViewPage.enterAboutYouGenders();
    });

    When(/^I select your marital status to apply account on your details page$/, () => {
        changeAccountTypeWebViewPage.selectYourMartialStatus();
    });

    When(/^I select your nationality to apply account on your details page$/, () => {
        changeAccountTypeWebViewPage.selectYourNationality();
    });

    When(/^I select your country of birth to apply account on your details page$/, () => {
        changeAccountTypeWebViewPage.selectYourCountryOfBirth();
    });

    When(/^I select where you live to enter the fields details to apply account on your details page$/, () => {
        changeAccountTypeWebViewPage.selectWhereToLive();
    });

    Then(/^I enter where you live house number (.*) field details to apply account on your details page$/, (housenumber) => {
        changeAccountTypeWebViewPage.enterHouseNumber(housenumber);
    });

    Then(/^I enter where you live house name (.*) field details to apply account on your details page$/, (housename) => {
        changeAccountTypeWebViewPage.enterHouseName(housename);
    });

    Then(/^I enter where you live Street (.*) field details to apply account on your details page$/, (street) => {
        changeAccountTypeWebViewPage.enterStreetName(street);
    });

    Then(/^I enter where you live City (.*) field details to apply account on your details page$/, (city) => {
        changeAccountTypeWebViewPage.enterCityName(city);
    });

    Then(/^I enter where you live County (.*) field details to apply account on your details page$/, (County) => {
        changeAccountTypeWebViewPage.enterCountyName(County);
    });

    Then(/^I enter where you live post code (.*) field details to apply account on your details page$/, (postcode) => {
        changeAccountTypeWebViewPage.enterWhereYouLivepostCode(postcode);
    });

    Then(/^I select when did you move to your current address details to apply account on your details page$/, () => {
        changeAccountTypeWebViewPage.selectMoveYourCurrentAddress();
    });

    When(/^I select your title to apply account on your details page$/, () => {
        changeAccountTypeWebViewPage.selectSelectTitle();
    });

    Then(/^I enter phone number (.*) details to apply account on your details page$/, (mobile) => {
        changeAccountTypeWebViewPage.enterMobileNumber(mobile);
    });

    Then(/^I enter email address (.*) details to apply account on your details page$/, (email) => {
        changeAccountTypeWebViewPage.enterEmailAddress(email);
    });

    Then(/^I select your marketing choices details$/, () => {
        changeAccountTypeWebViewPage.selectMarketingChoices();
    });

    Then(/^I enter income and expense (.*),(.*) and (.*) details to apply account on your details page$/, (monthly, mortgages, childCareCost) => {
        changeAccountTypeWebViewPage.enterIncomeExpenses(monthly, mortgages, childCareCost);
    });

    Then(/^I select continue button on your details page$/, () => {
        changeAccountTypeWebViewPage.selectYourDetailsContinueButton();
    });

    Then(/^I should be on review your details page$/, () => {
        changeAccountTypeWebViewPage.shouldSeeReviewYourDetailsPage();
    });

    Then(/^I select continue button on review your details page$/, () => {
        changeAccountTypeWebViewPage.selectBeforeYouStartContinueButton();
    });

    Then(/^I should be on your account offer page$/, () => {
        changeAccountTypeWebViewPage.shouldSeeYourAccountOfferPage();
    });

    Then(/^I select confirm checkbox to open this account$/, () => {
        changeAccountTypeWebViewPage.confirmCheckboxOpenAccount();
    });

    Then(/^I select continue button on your account offer page$/, () => {
        changeAccountTypeWebViewPage.selectBeforeYouStartContinueButton();
    });

    Then(/^I should verify refer decision message on page$/, () => {
        changeAccountTypeWebViewPage.waitForReferDecisionMessagePage();
    });

    Then(/^I select to open another current account$/, () => {
        changeAccountTypeWebViewPage.selectAnotherCurrentButton();
    });

    When(/^I should be on before your continue page for RCA Account upgrade$/, () => {
        changeAccountTypeWebViewPage.waitForBeforeYouContinue();
    });

    When(/^I select age and residence confirmation options on before you continue page for RCA Account upgrade$/, () => {
        changeAccountTypeWebViewPage.selectAgeAndResident();
    });

    When(/^I select continue button on before you continue page for RCA Account upgrade$/, () => {
        changeAccountTypeWebViewPage.selectBeforeYouContinueButton();
    });

    When(/^I should be on your needs page$/, () => {
        changeAccountTypeWebViewPage.waitForYourNeedsTitle();
    });

    When(/^I verify family travel insurance header$/, () => {
        changeAccountTypeWebViewPage.waitForFamilyTravelInsuranceHeader();
    });

    When(/^I select options for family travel insurance on your needs page$/, () => {
        changeAccountTypeWebViewPage.selectFamilyTravelOptions();
    });

    When(/^I select on continue button on your needs page$/, () => {
        changeAccountTypeWebViewPage.selectYourNeedsContinue();
    });

    When(/^I verify vehicle breakdown cover header$/, () => {
        changeAccountTypeWebViewPage.waitForVehicleBreakdownHeader();
    });

    When(/^I select options for vehicle breakdown cover on your needs page$/, () => {
        changeAccountTypeWebViewPage.selectVehicleBreakdownOptions();
    });

    When(/^I verify mobile phone insurance header$/, () => {
        changeAccountTypeWebViewPage.waitForMobilePhoneHeader();
    });

    When(/^I select options for mobile phone insurance on your needs page$/, () => {
        changeAccountTypeWebViewPage.selectMobilePhoneOptions();
    });

    When(/^I verify home emergency cover header$/, () => {
        changeAccountTypeWebViewPage.waitForHomeEmergencyHeader();
    });

    When(/^I select options for home emergency cover on your needs page$/, () => {
        changeAccountTypeWebViewPage.selectHomeEmergencyOptions();
    });

    When(/^I validate summary header on your needs page$/, () => {
        changeAccountTypeWebViewPage.waitForYourNeedsSummaryHeader();
    });

    When(/^I should be on your eligibility page$/, () => {
        changeAccountTypeWebViewPage.waitForYourEligibilityTitle();
    });

    When(/^I select continue button on your eligibility page for RCA Account upgrade$/, () => {
        changeAccountTypeWebViewPage.selectYourEligibilityContinue();
    });

    When(/^I select date of birth (.*) (.*) (.*) and resident for family member/, (day, month, year) => {
        changeAccountTypeWebViewPage.selectDobAndResident(day, month, year);
    });

    When(/^I select medical condition buttons for self and partner$/, () => {
        changeAccountTypeWebViewPage.selectMedicalConditionButtons();
    });

    When(/^I select vehicle breakdown final check button on your eligibility page$/, () => {
        changeAccountTypeWebViewPage.selectVehicleBreakdownFinalCheckButton();
    });

    Then(/^I select mobile phone insurance final check button on your eligibility page$/, () => {
        changeAccountTypeWebViewPage.selectMobileInsuranceFinalCheckButton();
    });

    When(/^I select home emergency cover one last check button on your eligibility page$/, () => {
        changeAccountTypeWebViewPage.selectHomeEmergencyFinalCheckButton();
    });

    When(/^I validate summary header for your eligibility page$/, () => {
        changeAccountTypeWebViewPage.waitForYourEligibilitySummaryHeader();
    });

    Then(/^I should be on the summary page for RCA Account upgrade$/, () => {
        changeAccountTypeWebViewPage.waitForSummaryPageTitleHead();
    });

    When(/^I select continue button on summary page for RCA Account upgrade$/, () => {
        changeAccountTypeWebViewPage.selectSummaryPageContinueButton();
    });

    When(/^I select the add offer to receive reward extras on thank you page$/, () => {
        changeAccountTypeWebViewPage.selectAddOfferButton();
    });
};
