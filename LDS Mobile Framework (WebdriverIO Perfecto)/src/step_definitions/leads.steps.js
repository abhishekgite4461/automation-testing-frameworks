const {Given, When, Then} = require('cucumber');

const LeadsPage = require('../pages/leads.page');
const HomePage = require('../pages/home.page');

const logger = require('../utils/logger');

const leadsPage = new LeadsPage();
const homePage = new HomePage();

Given(/^there (is|are)( no)? (an interstitial lead|multiple sticky footer leads have|a banner lead|a sticky footer lead) configured$/, (verb, negate, lead) => {
    // TODO: check if leads are configured for the user, however these are for stubs
    logger.info(`${lead} configured`);
});

When(/^an interstitial lead is displayed$/, () => {
    leadsPage.verifyInterstitialLead();
});

When(/^I click on the native button at the bottom of the lead$/, () => {
    leadsPage.clickConfirm();
});

Then(/^I must not see the Global Menu in the lead screen$/, () => {
    leadsPage.verifyNoGlobalMenu();
});

Then(/^I see native button at the bottom of the lead$/, () => {
    leadsPage.verifyNativeButton();
});

When(/^I scroll the (.*) lead to the bottom$/, (type) => {
    leadsPage.scrollLead(type);
});

Then(/^the native button should be in (enabled|disabled) mode$/, (state) => {
    leadsPage.verifyButton(state);
});

Then(/^an alert pop-up saying to scroll to the bottom should be displayed$/, () => {
    leadsPage.verifyAlertPopUp();
});

Then(/^I navigate out of the lead by selecting the options in the lead screen$/, () => {
    leadsPage.clickHere();
    homePage.waitForPageLoad();
});

When(/^I select the asm tile$/, () => {
    leadsPage.selectASMLeadTile();
});

Then(/^I select the marketing hub link in the lead$/, () => {
    leadsPage.clickMarketingHubLink();
});

Then(/^I am shown the interstitial lead with the highest priority$/, () => {
    leadsPage.viewHighestPriorityLead();
});

When(/^I select the active native button at the bottom of the screen$/, () => {
    leadsPage.verifyButton('enabled');
    leadsPage.clickConfirm();
});

When(/^I should be on the interstitial page$/, () => {
    leadsPage.verifyInterstitialLead();
});

When(/^I select the (.*) link from the DPN lead page$/, (leadType) => {
    leadsPage.selectNativeLead(leadType);
});

When(/I scroll through my accounts tiles to an ASM lead$/, () => {
    leadsPage.swipeToLead();
});

When(/^I select cheque promotion account tile$/, () => {
    leadsPage.selectChequeDepositTile();
});

When(/^I select Deposit Cheque for the Cheque promotion page$/, () => {
    leadsPage.selectDepositCheque();
});

When(/^I can see view demo button in Promotion page$/, () => {
    leadsPage.shouldSeeChequeHeader();
    leadsPage.shouldSeeViewdemo();
});

When(/^I select the View Demo button$/, () => {
    leadsPage.selectViewdemo();
});

When(/^I validate the Details in Cheque Promotion landing page$/, () => {
    leadsPage.validateChequePromotionPage();
});

When(/^I should see cheque imaging demo$/, () => {
    leadsPage.shouldSeeChequeDemoPage();
});

When(/^I close the demo should return to the cheque promotion screen$/, () => {
    leadsPage.selectCloseDemo();
    leadsPage.shouldSeeChequeHeader();
});

Then(/^I select the add external account link in the lead$/, () => {
    leadsPage.selectAddExternalAccountLink();
});
