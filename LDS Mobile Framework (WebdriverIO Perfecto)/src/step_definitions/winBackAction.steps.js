const {Given, When, Then} = require('cucumber');
const CommonPage = require('../pages/common.page');

const commonPage = new CommonPage();

Then(/^I am displayed a warning with option to proceed or cancel$/, () => {
    commonPage.shouldSeeOptionsInWinback();
});

When(/I choose to cancel on the warning message$/, () => {
    commonPage.selectWinBackStayButton();
});

When(/I choose to continue on the warning message/, () => {
    commonPage.selectWinBackLeaveButton();
});

Given(/^I am displayed a warning message$/, () => {
    commonPage.shouldSeeOptionsInWinback();
});

Then(/I choose to stay on the app$/, () => {
    commonPage.selectWinBackStayButton();
});
