const {Then} = require('cucumber');

const CardReaderRespondPage = require('../pages/enrolmentCardReaderRespond.page');
const CardReaderIdentifyPage = require('../pages/enrolmentCardReaderIdentify.page');
const CreateMemorableInformationPage = require('../pages/createMemorableInformation.page');
const OnlineBusinessBankingPage = require('../pages/onlineBusinessBanking.page');
const ErrorMessagePage = require('../pages/error.page');

const incorrectPasscode = '12345678';
const passcode = '00000000';
const mi = 't35ting';

const cardReaderRespondPage = new CardReaderRespondPage();
const cardReaderIdentifyPage = new CardReaderIdentifyPage();
const createMemorableInformationPage = new CreateMemorableInformationPage();
const onlineBusinessBankingPage = new OnlineBusinessBankingPage();
const errorPage = new ErrorMessagePage();

Then(/^I should be shown card reader respond screen$/, () => {
    cardReaderRespondPage.waitForPageLoad();
    cardReaderRespondPage.shouldSeeCardReaderRespondPage();
});

Then(/^I should see the continue button is disabled in card reader respond screen$/, () => {
    cardReaderRespondPage.shouldSeeContinueButtonDisabled();
});

Then(/^I should see 6 digit challenge code on the card reader respond screen$/, () => {
    cardReaderRespondPage.validateChallengeCodeLength();
});

Then(/^I should see the last 5 digit of my authentication or debit card pre-populated and (.*)/, function (type) {
    const data = this.dataRepository.get();
    cardReaderRespondPage.validatePrePopulatedCardLength();
    cardReaderRespondPage.compareCardDetails(data.cardReader.pan);
    cardReaderRespondPage.verifyCardDetailsEditableVisiblity(type);
});

Then(/^I should be prompted to enter the 8 digit passcode in card reader respond screen$/, () => {
    cardReaderRespondPage.shouldSeeEnterPasscodeField();
});
Then(/^I select Having trouble with your credit reader link from card reader respond screen$/, () => {
    cardReaderRespondPage.shouldSeeProblemWithCardReaderLink();
    cardReaderRespondPage.openHavingTroubleWithCardReaderLink();
});

Then(/^I should see problem with your card reader pop-up with close and continue option$/, () => {
    cardReaderRespondPage.verifyProblemWithCardReaderPopUp();
});

Then(/^I should stay on the card reader screen on selection of close option in problem with your card reader pop-up$/, () => {
    cardReaderRespondPage.verifyClosingProblemWithCardReaderPopUp();
});

Then(/^I should navigate to browser on selection of continue option in problem with your card reader pop-up$/, () => {
    cardReaderRespondPage.openHavingTroubleWithCardReaderLink();
    cardReaderRespondPage.selectContinueOnProblemWithCardReaderPopUp();
    onlineBusinessBankingPage.waitForPageLoad();
});

Then(/^I verify card number and enter passcode in the card reader respond screen$/, function () {
    const data = this.dataRepository.get();
    cardReaderRespondPage.waitForPageLoad();
    cardReaderRespondPage.compareCardDetails(data.cardReader.pan);
    cardReaderRespondPage.enterPasscode(passcode);
    cardReaderRespondPage.selectContinueButton();
});

Then(/^I choose an option Having trouble with your credit reader from card reader identify screen$/, () => {
    cardReaderIdentifyPage.selectTroubleCardReader();
});

Then(/^I should see pop-up with Close and Continue option$/, () => {
    cardReaderIdentifyPage.waitForDialog();
    cardReaderIdentifyPage.shouldSeeHelpPopUpContinueAndCloseButton();
});

Then(/^on selection of close option I should stay on the card reader screen$/, () => {
    cardReaderIdentifyPage.selectHelpPopCloseButton();
});

Then(/^on selection of continue option I should navigate to browser$/, () => {
    cardReaderIdentifyPage.waitForPageLoad();
    cardReaderIdentifyPage.selectTroubleCardReader();
    cardReaderIdentifyPage.selectHelpPopContinueButton();
});

Then(/^I provide valid card number and passcode in the the card reader identify screen$/, function () {
    const data = this.dataRepository.get();
    cardReaderIdentifyPage.waitForPageLoad();
    cardReaderIdentifyPage.enterLastFiveCardDigits(data.cardReader.pan);
    cardReaderIdentifyPage.enterPasscode(passcode);
    cardReaderIdentifyPage.selectContinueButton();
});

Then(/^I create my memorable information$/, () => {
    createMemorableInformationPage.waitForPageLoad();
    createMemorableInformationPage.enterMemorableInformation(mi);
    createMemorableInformationPage.reEnterMemorableInformation(mi);
    createMemorableInformationPage.selectNextButton();
});

Then(/^I verify last five pre-populated card digits is non-editable in the card reader respond screen$/, () => {
    cardReaderRespondPage.waitForPageLoad();
    cardReaderRespondPage.verifyCardDetailsEditableVisiblity('non-editable');
});

Then(/^I provide valid passcode in the card reader (.*) screen$/, cardReaderRespondPage.waitForCardReaderRespondPage);

Then(/^I have entered the last 5 digits of (.*) card number in the card reader identify page$/, function (type) {
    const data = this.dataRepository.get();
    cardReaderIdentifyPage.waitForPageLoad();
    let num = data.cardReader.pan;
    if (type === 'invalid') {
        num = '00000';
    }
    cardReaderIdentifyPage.enterLastFiveCardDigits(num);
});

Then(/^I enter the (.*) 8 digit passcode from the card reader in the card reader (.*) page$/, (type, field) => {
    if (field === 'identify') {
        cardReaderIdentifyPage.waitForPageLoad();
    } else if (field === 'respond') {
        cardReaderRespondPage.waitForPageLoad();
    } else {
        cardReaderIdentifyPage.waitForCardReaderAuthenticationPageLoad();
    }
    if (type === 'incorrect') {
        cardReaderIdentifyPage.enterPasscode(incorrectPasscode);
    } else {
        cardReaderIdentifyPage.enterPasscode(passcode);
    }
});

Then(/^I should be shown (.*) error banner in the card reader identify page$/, (type) => {
    cardReaderIdentifyPage.shouldSeeErrorBanner(type);
});

Then(/^I close the incorrect passcode error banner$/, () => {
    cardReaderIdentifyPage.closeInvalidCardBanner();
});

Then(/^I should see the continue button (.*) in the card reader identify page$/, (type) => {
    cardReaderIdentifyPage.verifyContinueButtonEnableVisibility(type);
});

Then(/^I have entered the last 5 digits of (.*) card number in the card reader respond page$/, function (type) {
    const data = this.dataRepository.get();
    cardReaderRespondPage.waitForPageLoad();
    let num = data.cardReader.pan;
    if (type === 'invalid') {
        num = '00000';
    }
    cardReaderRespondPage.enterLastFiveCardDigits(num);
});

const enterRespondPasscode = function (type) {
    if (type === 'incorrect') {
        cardReaderRespondPage.enterPasscode(incorrectPasscode);
    } else {
        cardReaderRespondPage.enterPasscode(passcode);
    }
};

Then(/^I submit the (.*) 8 digit passcode from the card reader in the card reader respond page$/, (type) => {
    enterRespondPasscode(type);
    cardReaderRespondPage.selectContinueButton();
});

Then(/^I should be shown (.*) error banner in the card reader respond page$/, (type) => {
    cardReaderRespondPage.waitForPageLoad();
    cardReaderRespondPage.shouldSeeErrorBanner(type);
});

Then(/^I should be shown card expired and new card issued error in the error page$/, () => {
    errorPage.waitForPageLoad();
    errorPage.shouldSeeCardExpiredAndNewCardIssuedMessage();
});

Then(/^I should be shown card suspended error in the error page$/, () => {
    errorPage.waitForPageLoad();
    errorPage.shouldSeeCardSuspendedMessage();
});

Then(/^I should see the continue button (.*) in the card reader respond page$/, (type) => {
    cardReaderRespondPage.verifyContinueButtonEnableVisibility(type);
});

Then(/^I leave the passcode field empty in the card reader respond page$/, () => {
    cardReaderRespondPage.clearPasscodeField();
});

Then(/^I leave the last five card digits field empty in the card reader respond page$/, () => {
    cardReaderRespondPage.clearCardDigitsField();
});

Then(/^I enter 7 digit passcode and 4 digit card number in the card reader respond page$/, function () {
    const data = this.dataRepository.get();
    cardReaderRespondPage.enterInsufficientDetails(passcode, data.cardReader.pan);
});

Then(/^I select continue in the card reader identify page$/, () => {
    cardReaderIdentifyPage.waitForPageLoad();
    cardReaderIdentifyPage.selectContinueButton();
});

Then(/^I select the continue button on the card reader Identify or respond page$/, () => {
    cardReaderRespondPage.selectContinueButton();
});
