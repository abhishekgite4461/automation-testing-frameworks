const {When, Then} = require('cucumber');
const SharePreviewPage = require('../pages/paymentHub/sharePreview.page');
const CommonPage = require('../pages/common.page');
const AuthPage = require('../pages/common/auth.page');
const MorePage = require('../pages/more.page');
const PaymentHubMainPage = require('../pages/paymentHub/paymentHubMain.page');
const SuccessPaymentPage = require('../pages/paymentHub/successPayment.page');

const successPaymentPage = new SuccessPaymentPage();
const morePage = new MorePage();
const authPage = new AuthPage();
const sharePreviewPage = new SharePreviewPage();
const commonPage = new CommonPage();

When(/^I am on the Share Preview screen$/, () => {
    sharePreviewPage.waitForPageLoad();
});

Then(/^I should see the full name of the user who logged in against from field$/, () => {
    sharePreviewPage.getFromAccountName();
    commonPage.closeModal();
    authPage.selectTab('more');
    morePage.validateUserName();
});

Then(/^I select share receipt icon on payment success screen$/, () => {
    successPaymentPage.selectShareReceiptOption();
    sharePreviewPage.waitForPageLoad();
});

Then(/^I should see the Bank Icon at the top of the screen$/, () => {
    sharePreviewPage.shouldSeeBankIcon();
});

Then(/^I should see the first 4 digits of the recipient account number is masked$/, () => {
    sharePreviewPage.shouldSeePayeeAccountNumberMasked();
});

Then(/^I should see the date value on the share preview screen$/, () => {
    const previewDate = PaymentHubMainPage.getFuturePaymentDate();
    sharePreviewPage.shouldSeeDateValue(previewDate);
});

When(/^I select Share on the Share Preview screen$/, () => {
    sharePreviewPage.waitForPageLoad();
    sharePreviewPage.selectShareOption();
});

When(/^I should see the native OS share panel$/, () => {
    sharePreviewPage.shouldSeeNativeSharePanel();
});

When(/^I select the email app on the share panel$/, () => {
    sharePreviewPage.shouldSeeEmailAppIcon();
    sharePreviewPage.selectEmailApp();
});

Then(/^I should see the relevant current account details on the email text$/, () => {
    sharePreviewPage.shouldSeeAccountDetails();
});

When(/^I should see the share option on the Preview screen$/, () => {
    sharePreviewPage.verifyShareOption();
});

Then(/^I should see the following details populated on the share preview screen$/, (sharePreviewDetails) => {
    const tableData = sharePreviewDetails.hashes();
    sharePreviewPage.verifySharePreviewScreenDetails(tableData);
});

When(/^I validate native OS panel and navigate back$/, () => {
    sharePreviewPage.waitForPageLoad();
    sharePreviewPage.selectShareOption();
    sharePreviewPage.shouldSeeNativeSharePanel();
    sharePreviewPage.shouldSeeEmailAppIcon();
    sharePreviewPage.selectEmailApp();
    sharePreviewPage.selectCancelButton();
});

When(/^I navigate back from the mail app$/, () => {
    sharePreviewPage.selectCancelButton();
});
