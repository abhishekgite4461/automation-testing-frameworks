const {When, Then} = require('cucumber');
const SecureInboxPage = require('../pages/secureInbox.page');

const secureInboxPage = new SecureInboxPage();

Then(/^I should be on the inbox webview page$/, () => {
    secureInboxPage.waitForPageLoad();
});

When(/^I select the message to view$/, () => {
    secureInboxPage.clickOnFirstMessage();
});

When(/^I choose View PDF to see the message$/, () => {
    secureInboxPage.clickViewPDF();
});

When(/^I choose to archive message in Inbox$/, () => {
    secureInboxPage.clickArchive();
});

When(/^I undo archive message in Inbox$/, () => {
    secureInboxPage.clickUndoArchive();
});

When(/^I verify the Undo Archive Message$/, () => {
    secureInboxPage.verifyUndoArchive();
});

When(/^I select inbox folders on inbox webview page$/, () => {
    secureInboxPage.selectInboxFolders();
});

When(/^I select (.*) account from inbox folders$/, function (accountName) {
    const data = this.dataRepository.get();
    secureInboxPage.selectFolders(data[accountName].toUpperCase());
});

When(/^I see that mails for selected (.*) account are filtered on inbox webview page$/, function (accountName) {
    const data = this.dataRepository.get();
    secureInboxPage.verifyFilteredMessages(data[accountName].toUpperCase());
});
