const {When, Then} = require('cucumber');
const MessageUsHomePage = require('../pages/messageUsHomePage.page');

const messageUsHomePage = new MessageUsHomePage();

Then(/^I should be on the message us home page$/, () => {
    messageUsHomePage.waitForPageLoad();
});

Then(/^I close chat window$/, () => {
    messageUsHomePage.closeChatWindow();
});

When(/^I send my question (.*) to agent or bot to answer it$/, (question) => {
    messageUsHomePage.sendChatMessage(question);
});

Then(/^I validate chat modal window and close it$/, () => {
    messageUsHomePage.waitForPageLoad();
    messageUsHomePage.validateChatWindow();
    messageUsHomePage.closeChatWindow();
});
