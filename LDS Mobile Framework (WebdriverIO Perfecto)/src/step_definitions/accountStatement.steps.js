const {When, Then} = require('cucumber');
const DeviceHelper = require('../utils/device.helper');
const AccountStatementPage = require('../pages/statements/accountStatement.page.js');
const ViewTransactionDetailPage = require('../pages/viewTransactionDetail.page');
const ActionMenuPage = require('../pages/actionMenu.page');
const AboutPendingTransactionsOverlayPage = require('../pages/statements/aboutPendingTransactionsOverlay.page');
const HomePage = require('../pages/home.page');
const CallUsPage = require('../pages/callUs/callUs.page');
const CommonPage = require('../pages/common.page');
const SpendingRewardsPage = require('../pages/spendingRewards.page');
const SaveInterestDetailPage = require('../pages/savingsinterestRateDetails.page');
const AccountMatcher = require('../data/utils/accountMatcher');

const viewTransactionDetailPage = new ViewTransactionDetailPage();
const accountStatementPage = new AccountStatementPage();
const actionMenuPage = new ActionMenuPage();
const aboutPendingTransactionsOverlayPage = new AboutPendingTransactionsOverlayPage();
const homePage = new HomePage();
const callUsPage = new CallUsPage();
const commonPage = new CommonPage();
const spendingRewardsPage = new SpendingRewardsPage();
const sirDetailPage = new SaveInterestDetailPage();

When(/^I select the first transaction in the account statement page$/, () => {
    accountStatementPage.selectFirstTransaction();
});

When(/^I validate the options in the VTD page/, () => {
    viewTransactionDetailPage.validateVTDPage();
});

When(/^I validate the pending transaction objects in the VTD page/, () => {
    viewTransactionDetailPage.validatePendingTransactionVTDPage();
});

When(/^I close the VTD page/, () => {
    viewTransactionDetailPage.closeVTDPage();
});

When(/^I should see first transaction in the account statement page$/, () => {
    accountStatementPage.verifyFirstTransactionVisible();
});

When(/^I select pending transactions tab$/, () => {
    accountStatementPage.selectPendingTransactionTab();
});

When(/^I should be able to see cheque transaction$/, () => {
    accountStatementPage.verifyChequeTransactions();
});

When(/^I should be able to select pending transaction$/, () => {
    accountStatementPage.selectPendingTransaction();
});

When(/^I should see credit card first transaction in the account statement page$/, () => {
    accountStatementPage.verifyCreditCardFirstTransactionVisible();
});

When(/^I should see transaction error text$/, () => {
    viewTransactionDetailPage.verifyTransactionErrorTextIsVisible();
});

Then(/^I should see transactions since message on recent tab$/, () => {
    accountStatementPage.viewRecentsTab();
    accountStatementPage.verifyTransactionSinceMessage();
});

When(/^I swipe to see previous month transactions for in and outs on the account statement page/, () => {
    accountStatementPage.swipeToPreviousMonthTransaction();
});

When(/^I swipe to see previous month transactions on the account statement page/, () => {
    accountStatementPage.swipeToPreviousMonth();
});

When(/^I swipe to see previous month for maximum transaction/, () => {
    accountStatementPage.swipeToPreviousMonthTransaction();
});

Then(/^I should be on the same previous month statements page/, () => {
    accountStatementPage.viewPreviousMonthTab();
});

Then(/^I swipe to see currentMonth transaction/, () => {
    accountStatementPage.swipeToCurrentMonth();
});

When(/^I select on the back button in the account statement page$/, () => {
    accountStatementPage.clickBackButton();
});

Then(/^I select action menu in the pending payments page$/, () => {
    accountStatementPage.selectActionMenu();
});

Then(/^I should be on the current month statements tab$/, () => {
    accountStatementPage.validateCurrentMonthName();
});

Then(/^I should not be able to swipe between accounts in statements view$/, () => {
    accountStatementPage.shouldNotSwipeBetweenAccounts();
});

Then(/^I should see transaction date in statements list$/, () => {
    accountStatementPage.validateStatementDate();
});

Then(/^I should see transaction description in statements list$/, () => {
    accountStatementPage.validateStatementDescription();
});

Then(/^I should see running balance in statements list$/, () => {
    accountStatementPage.validateStatementRunningBalance();
});

Then(/^I should see transacted amount in statements list$/, () => {
    accountStatementPage.validateStatementAmount();
});

Then(/^I should scroll down to see the next set of transactions loaded for (.*) account$/, (type) => {
    accountStatementPage.swipeToSeeNextSetOfTransaction(type);
});

Then(/^I should scroll down to see end of transaction message$/, () => {
    accountStatementPage.verifyEndOfTransactionMessage();
});

Then(/^I see recent statements tab credit card$/, () => {
    accountStatementPage.viewRecentsTab();
});

Then(/^The previously billed info panel appears$/, () => {
    accountStatementPage.verifyBilledInfoPanel();
});

Then(/^I should see (debit|credit) transaction with below details in credit card statements$/, (type, table) => {
    const tableData = table.hashes();
    accountStatementPage.validateTransactionDetails(tableData);
});

Then(/^I should see pending (.*) transaction with below detail in statements$/, (type, table) => {
    const tableData = table.hashes();
    accountStatementPage.validatePendingTransactionDetails(type, tableData);
});

Then(/^I should see no transactions message$/, () => {
    accountStatementPage.shouldSeeNoTransactionsMessage();
});

Then(/^I should not see transactions header$/, () => {
    accountStatementPage.verifyTransactionsHeaderIsNotPresent();
});

When(/^I tap to view the action menu$/, () => {
    accountStatementPage.selectActionMenuOnAccountStatementScreen();
});

Then(/^I should see the extensibility menu without view transaction entry$/, () => {
    actionMenuPage.verifyNoViewTransactionsOption();
});

Then(/^I select one of the (posted|pending|upcoming) transaction$/, (type) => {
    viewTransactionDetailPage.selectPostedTransaction(type);
});

Then(/^I should see (posted|pending|upcoming|) transaction details for that transaction with the following fields$/, (type, vtdFields) => {
    const FieldsToValidate = vtdFields.hashes();
    viewTransactionDetailPage.checkVtdFieldsVisibility(type, FieldsToValidate);
});

Then(/^I should see the next set of transactions loaded$/, () => {
    accountStatementPage.verifyNextSetOfTransactions();
});

When(/^I see previous month tab to the left of the current month tab in statements$/, () => {
    accountStatementPage.viewPreviousMonthTab();
});

When(/^I navigate to a previous month for current year$/, () => {
    accountStatementPage.swipeToPreviousMonth();
});

When(/^I view the next set of transactions$/, () => {
    accountStatementPage.verifyNextSetOfTransactions();
});

Then(/^I should see the month tab heading in the format <Month>$/, () => {
    accountStatementPage.viewMonthTab();
});

Then(/^I should see the future months to right of the selected previous month$/, () => {
    accountStatementPage.validateCurrentMonthName();
});

Then(/^I should not see the previous calendar month to the left of the month tab$/, () => {
    accountStatementPage.verifyNoPreviousCalanderMonth();// Stub is not implemented
});

When(/^I select the action menu from statements page$/, () => {
    homePage.dismissServerErrorIfPresent();
    accountStatementPage.selectActionMenuOnAccountStatementScreen();
});

When(/^I should not see view transaction option$/, () => {
    actionMenuPage.verifyNoViewTransactionsOption();
});

Then(/^I should see current month transactions for (.*) account$/, (type) => {
    if (type === 'current') {
        accountStatementPage.validateCurrentMonthName();
    }
});

When(/^I select (.*) on statements page$/, (transactionType) => {
    accountStatementPage.selectPostedTransaction(transactionType);
});

When(/^I select to view the information link$/, () => {
    viewTransactionDetailPage.selectViewInformationLink();
});

Then(/^I must be displayed the win-back option$/, () => {
    viewTransactionDetailPage.viewWinBack();
});

When(/^I cancel to go to the information link$/, () => {
    viewTransactionDetailPage.selectStayButtonOnWinBack();
});

When(/^I tap on the pending transactions accordion$/, () => {
    accountStatementPage.clickPendingTransactions();
});

Then(/^I should see pending transactions$/, () => {
    accountStatementPage.verifyPendingTransactionsArePresent();
});

Then(/^I should see a pending transactions header$/, () => {
    accountStatementPage.verifyPendingTransactionsHeaderIsPresent();
});

Then(/^I should see (minimised|expanded) pending transactions accordion/, (state) => {
    accountStatementPage.verifyPendingTransactionsAccordion(state);
});

When(/^I scroll back to the recent tab$/, () => {
    accountStatementPage.swipeTransactionList('future', 1);
});

Then(/^I should not see the pending transactions accordion$/, () => {
    accountStatementPage.verifyPendingTransactionsAccordionIsNotVisible();
});

Then(/^I should see the cleared transactions$/, () => {
    accountStatementPage.verifyTransactionsArePresent();
});

When(/^I swipe to (previous|future) (\d) months transactions on the account statement page$/, (dir, input) => {
    accountStatementPage.swipeTransactionList(dir, input);
});

Then(/^I should see (.*) no transactions message in the month tab$/, (output) => {
    accountStatementPage.validateNoTransactionMessage(output);
});

Then(/^I should scroll down to the end of the transactions to view (.*) message$/, (output) => {
    accountStatementPage.validateEndOfTransactionMessage(output);
});

Then(/^I should see following fields in statements list$/, (pageFields) => {
    const FieldsToValidate = pageFields.hashes();
    accountStatementPage.validateStatementsListFields(FieldsToValidate);
});

Then(/^I should validate fields in due soon screen$/, (dueSoonFields) => {
    const fields = dueSoonFields.hashes();
    viewTransactionDetailPage.checkForDueSoonAndFieldsVisibility(fields);
});

When(/^I select the (.*) option from the action menu in statements page$/, (option) => {
    homePage.dismissServerErrorIfPresent();
    accountStatementPage.selectActionMenuOnAccountStatementScreen();
    switch (option) {
    case 'make an overpayment':
        actionMenuPage.selectMakeAnOverpaymentOption();
        break;
    case 'viewUpcomingPayment':
        actionMenuPage.verifyPendingPaymentOption(option);
        actionMenuPage.selectPendingPaymentOption(option);
        break;
    case 'viewPaymentsDueSoon':
        actionMenuPage.verifyPendingPaymentOption(option);
        actionMenuPage.selectPendingPaymentOption(option);
        break;
    case 'View Interest Details':
        sirDetailPage.selectViewInterestDetailsOption();
        break;
    case 'top up isa':
        actionMenuPage.selectActionMenuTopUpIsa();
        break;
    case 'viewSpendingInsights':
        actionMenuPage.verifyPendingPaymentOption(option);
        actionMenuPage.selectPendingPaymentOption(option);
        break;
    case 'downloadTransactionsOption':
        actionMenuPage.shouldSeeDownloadTransactionsOption();
        actionMenuPage.selectDownloadTransactionsOption();
        break;
    default:
        throw new Error('unrecognised action menu option');
    }
});

Then(/^I should not see payments due soon option on the action menu$/, () => {
    accountStatementPage.selectActionMenuOnAccountStatementScreen();
    actionMenuPage.verifyNoPendingPaymentOption();
    actionMenuPage.closeActionMenu();
});

Then(/^I should see dueSoon tab in statements screen$/, () => {
    viewTransactionDetailPage.checkForDueSoonVisibility();
});

Then(/^I should not see the next calendar month to the right of the current month tab$/, () => {
    accountStatementPage.validateNoCalendarMonth();
});

Then(/^I navigate to a month for a previous year$/, () => {
    accountStatementPage.swipeToPreviousMonthOfPreviousYear();
});

Then(/^I should scroll down to see the next set of transactions loaded for (.*) account for previous month$/, (type) => {
    accountStatementPage.swipeToSeeNextSetOfTransactionForPreviousMonth(type);
});

Then(/^I select one of the pending payment transaction$/, () => {
    viewTransactionDetailPage.selectPendingPaymentTransaction();
});

Then(/^I should see the month tab heading in the format <Year><Month>$/, () => {
    viewTransactionDetailPage.validateMonthNameAndYear();
});

Then(/^I should see pending payment transaction details for that transaction with the following fields$/, (vtdFields) => {
    const FieldsToValidate = vtdFields.hashes();
    viewTransactionDetailPage.validatePendingVtdFields(FieldsToValidate);
});

Then(/^I see a message advising that online statements are available only for 6 months$/, () => {
    accountStatementPage.verifyStatementLimitMessage();
});

When(/^I should see the about pending transactions accordion header$/, () => {
    accountStatementPage.verifyAboutPendingTransactionsHeaderIsPresent();
});

When(/^I tap on about pending transactions header$/, () => {
    accountStatementPage.clickOnAboutPendingTransactionsHeader();
});

When(/^I should see about pending transactions overlay$/, () => {
    aboutPendingTransactionsOverlayPage.verifyAboutPendingTransactionsOverlay();
});

When(/^I tap on close button of about pending transactions overlay$/, () => {
    aboutPendingTransactionsOverlayPage.clickOnCloseButton();
});

Then(/^I should see no pending payments message in statements screen$/, () => {
    accountStatementPage.validateNoPendingPaymentsMessage();
});

Then(/^I should see below options in contextual menu$/, (loanFields) => {
    const FieldsToValidate = loanFields.hashes();
    accountStatementPage.verifyLoanFieldsInActionMenu(FieldsToValidate);
});

Then(/^I should be on the current year loan statements tab$/, () => {
    accountStatementPage.validateCurrentYear();
});

Then(/^I should not see the next calendar year to the right of the current year tab$/, () => {
    accountStatementPage.validateNextYearNotDisplayed(1, false);
});

When(/^I navigate to a previous year$/, () => {
    accountStatementPage.swipeToPreviousYear();
});

When(/^I should see the future year to right of the selected previous year$/, () => {
    accountStatementPage.validateTheFutureYearToRight(0, true);
});

When(/^I should see the previous years until the year the loan started$/, () => {
    accountStatementPage.validateThePreviousYearsloanStarted();
});

When(/^I should see the year tab heading in the format <YYYY>$/, () => {
    accountStatementPage.validateTheYearTab(0, true);
});

Then(/^I should see previous year tab to the left of the current year tab in statements$/, () => {
    accountStatementPage.validateThePreviousYearTab(-1, true);
});

Then(/^I tap on the plus sign to expand the accordion with following loan details$/, (loanFields) => {
    accountStatementPage.clickPlusAccordionLoans();
    const FieldsToValidate = loanFields.hashes();
    accountStatementPage.verifyLoanFieldsStatementsPage(FieldsToValidate);
});

Then(/^I tap on minus sign to close the accordion and should see the following loan details$/, (loanFields) => {
    accountStatementPage.clickMinusAccordionLoans();
    const FieldsToValidate = loanFields.hashes();
    accountStatementPage.verifyLoanFieldsStatementsPage(FieldsToValidate);
});

Then(/^I should see the message displayed and should not see action menu on account tile$/, () => {
    accountStatementPage.noActionMenuCbsLoan();
    accountStatementPage.validateCbsLoanMessage();
});

Then(/^I should see following fields in loan details closed accordion$/, (loanFields) => {
    const FieldsToValidate = loanFields.hashes();
    accountStatementPage.verifyLoanFieldsStatementsPage(FieldsToValidate);
});

When(/^I swipe to see previous year transactions on the account statement page/, () => {
    accountStatementPage.swipeToPreviousYear();
});

Then(/^I should be on the same previous year statements page/, () => {
    accountStatementPage.viewPreviousYearTab();
});

Then(/^I should scroll down to see the next set of transactions loaded for (.*) account for previous year$/, (type) => {
    accountStatementPage.swipeToSeeNextSetOfTransactionForPreviousMonth(type);
});

Then(/^I should see (.*) no transactions message in current year tab$/, (output) => {
    accountStatementPage.validateNoTransactionMessage(output);
});

Then(/^I should be on the (.*) of statements page$/, (typeAccount) => {
    accountStatementPage.validateStatementsPage(typeAccount);
});

Then(/^I see posted transaction detail screen and select payments and transfer$/, () => {
    viewTransactionDetailPage.verifyVtdPage();
    viewTransactionDetailPage.clickPaymentsAndTransferButton();
});

Then(/^I should be on the payment hub page of that account$/, () => {
    viewTransactionDetailPage.verifyPaymentHubPage();
});

Then(/^I see payment hub page and tap on close icon$/, () => {
    viewTransactionDetailPage.verifyPaymentHubPage();
    commonPage.closeModal();
});

When(/^I should see the (.*) deducted from latest transaction/, (amount) => {
    homePage.dismissServerErrorIfPresent();
    accountStatementPage.selectFirstTransaction();
    if (!DeviceHelper.isStub()) {
        accountStatementPage.validateAmountDeducted(amount);
    }
    viewTransactionDetailPage.closeVTDPage();
});

When(/^I should see the (.*) in the current month statement of the remitting account/, (amount) => {
    homePage.dismissServerErrorIfPresent();
    if (!DeviceHelper.isStub()) {
        accountStatementPage.validateAmountDeducted(amount);
    }
});

Then(/^I swipe to see current month transactions on the account statement page/, () => {
    accountStatementPage.swipeToCurrentMonth();
});

When(/^I navigate to (.*) account tile from statements page$/, function (accountName) {
    const data = this.dataRepository.get();
    const accName = accountName.replace(/ /g, '');
    homePage.swipeToAccountTile(data[accName]);
});

When(/^I navigate back to previous (.*) account and I am on the current month statements tab$/, function (accountName) {
    const data = this.dataRepository.get();
    const accName = accountName.replace(/ /g, '');
    accountStatementPage.navigateBackToPreviousAccount(data[accName]);
});

Then(/^I should see month tabs without year$/, () => {
    accountStatementPage.verifyMonthTabWithoutYear();
});

When(/^I should be on the mortgage account statement summary$/, () => {
    accountStatementPage.validateMortgageSummaryTab();
});

Then(/^I should see summary of sub account with the following fields in the summary for each sub-account$/, (pageFields) => {
    const FieldsToValidate = pageFields.hashes();
    accountStatementPage.validateSubAccountListFields(FieldsToValidate);
});

When(/^I select sub account tab$/, () => {
    accountStatementPage.selectSubAccountTab();
});

When(/^I should see following fields for (minimised|expanded) mortgage details$/, (option, pageFields) => {
    const FieldsToValidate = pageFields.hashes();
    accountStatementPage.validateMortgageDetailsFields(option, FieldsToValidate);
});

When(/^I view full mortgage details$/, () => {
    accountStatementPage.clickMortgageAccordion();
});

Then(/^I should see (.*) no transactions message in the summary tab$/, (output) => {
    accountStatementPage.validateNoTransactionMessage(output);
});

When(/^I tap on any of the sub account$/, () => {
    accountStatementPage.tapOnSubAccountFromList();
});

Then(/^I should be displayed with details tab$/, () => {
    accountStatementPage.validateDetailsTab();
});

When(/^I tap on sub account accordion in details tab$/, () => {
    accountStatementPage.clickMortgageAccordion();
});

Then(/^I should be displayed details tab with following fields in the sub-account transactions$/, (pageFields) => {
    accountStatementPage.validateDetailsTab();
    const FieldsToValidate = pageFields.hashes();
    accountStatementPage.validateStatementsListFields(FieldsToValidate);
});

When(/^I view full mortgage details for (.*) and select the (.*)$/, (type, infoLink) => {
    accountStatementPage.clickMortgageAccordionForSummaryAndSubAccount(type);
    accountStatementPage.chooseInfoLink(type, infoLink);
});

Then(/^I should be displayed the (.*) as a layover$/, (infoLink) => {
    accountStatementPage.validateInfoLayover(infoLink);
});

Then(/^I should be displayed (.*) with full mortgage details$/, (mortgageTab) => {
    accountStatementPage.validateMortgageTab(mortgageTab);
    accountStatementPage.validateMortgageAccordionState();
});

Then(/^I should be displayed a message to advise product can not be viewed online$/, () => {
    accountStatementPage.validateOffSetRepossessedAccountMessage();
});

When(/^I select close button in info page$/, () => {
    accountStatementPage.clickCloseButtonInInfoPage();
});

When(/^I navigate to click to call journey$/, () => {
    accountStatementPage.clickCallUs();
});

Then(/^I should be displayed the click to call journey$/, () => {
    callUsPage.waitForSecureCallPageLoad();
});

When(/^I select back button on click to call journey$/, () => {
    callUsPage.goBackToHome();
});

When(/^I tap on (.*) in vtd screen$/, (buttonType) => {
    viewTransactionDetailPage.clickStandingOrDirectDebitButton(buttonType);
});

Then(/^I should be displayed VTD of that transaction$/, () => {
    viewTransactionDetailPage.validateVTDPageLoaded();
});

Then(/^I should see (.*) no transactions message in the details tab$/, (output) => {
    accountStatementPage.validateNoTransactionMessage(output);
});

When(/^I select to view (.*) details$/, (policyLinkText) => {
    accountStatementPage.selectPolicyLink(policyLinkText);
});

Then(/^I should be displayed (policy|coverage|payment|optionalCoverage) details with the following fields$/, (type, policyFields) => {
    const FieldsToValidate = policyFields.hashes();
    accountStatementPage.checkPolicyCoverageFieldsVisibility(type, FieldsToValidate);
});

Then(/^I should be displayed insurance policy details$/, () => {
    accountStatementPage.validatePolicyDetailsPageLoaded();
});

Then(/^I should be displayed pending transactions for that (.*)$/, (type) => {
    accountStatementPage.verifyPendingTransactionsWhenLayoverClosed(type);
});

Then(/^I validate (.*) and (.*) details in VTD page/, (beneficiary, amount) => {
    viewTransactionDetailPage.validateVTDValues(beneficiary, amount);
});

Then(/^I should not be displayed any summary and sub-account details tabs$/, () => {
    accountStatementPage.verifyNoSummaryAndSubaccountTabs();
});

Then(/^I should see a (.*) on the statement page$/, (message) => {
    accountStatementPage.validateOffsetRepossessed(message);
});

Then(/^I should see the warning alert on the account tile on statement page$/, () => {
    accountStatementPage.verifyWarningAlert();
});

Then(/^I tap on the warning alert on the account tile$/, () => {
    accountStatementPage.clickWarningAlert();
});

Then(/^I must be displayed an alert overlay with a (.*)$/, (message) => {
    accountStatementPage.validateWarningMessage(message);
});

Then(/^I tap on the close alert overlay$/, () => {
    accountStatementPage.clickCloseButtonInAlertPage();
});

When(/^I should see in and out total without bar in the account statements page$/, () => {
    accountStatementPage.shouldSeeNoTransactionsMessage();
    accountStatementPage.verifyInAndOutTotalWithoutBar();
});

When(/^I view (.*) transactions of (.*) account$/, (month, type) => {
    accountStatementPage.navigateToCurrentAndPreviousMonth(month, type);
});

Then(/^I should be displayed (.*) statements transactions$/, (month) => {
    accountStatementPage.validateCurrentAndPreviousMonthTransactions(month);
});

When(/^I select the unsure transaction link in the Vtd page/, () => {
    viewTransactionDetailPage.selectUnSureTransaction();
});

When(/^I select the (Outgoing|Incoming) transactions option in the unsure transaction page/, (type) => {
    if (type === 'Outgoing') {
        viewTransactionDetailPage.selectOutgoingTransactions();
    } else {
        viewTransactionDetailPage.selectIncomingTransactions();
    }
});

When(/^I select the speak to us button in the (Outgoing|Incoming) transactions page/, () => {
    viewTransactionDetailPage.selectSpeakToUsButton();
});

Then(/^I should be displayed recent transactions with following fields$/, (pageFields) => {
    const FieldsToValidate = pageFields.hashes();
    accountStatementPage.validateRecentTransactionFields(FieldsToValidate);
});

Then(/^I should be displayed last month statement with the following fields in statements summary$/, (pageFields) => {
    const FieldsToValidate = pageFields.hashes();
    accountStatementPage.validateCreditCardStatementSummary(FieldsToValidate);
});

When(/^I navigate to (previous|future) (\d) months transactions on the account statement page$/, (dir, input) => {
    accountStatementPage.swipeTransactionList(dir, input);
});

When(/^I filter the transactions to view (.*) transaction for (.*)$/, (typeFilter, month) => {
    accountStatementPage.selectFilterTransactions(typeFilter, month);
});

Then(/^I should be displayed the (.*) transactions for the month selected$/, (typeFilter) => {
    accountStatementPage.validateFilterTransactions(typeFilter);
});

Then(/^I should see following fields for (.*) in statements summary$/, (type, pageFields) => {
    const FieldsToValidate = pageFields.hashes();
    accountStatementPage.validateStatementSummaryFieldsInFilterTransactions(type, FieldsToValidate);
});

Then(/^I should not be able to navigate to other months and accounts$/, () => {
    accountStatementPage.validateNotAbleToNavigateBetweenMonthsAndAccounts();
});

When(/^I clear the filter$/, () => {
    accountStatementPage.clickClearFilter();
});

Then(/^I should be displayed a message on fixed bond account statement page$/, () => {
    accountStatementPage.validateFixedBondMessage();
});

Then(/^I should be displayed a message$/, () => {
    accountStatementPage.validateDormantAccountMessage();
});

Then(/^I should not see any transactions$/, () => {
    accountStatementPage.validateNoTransactions();
});

Then(/^I should be displayed recent transactions$/, () => {
    accountStatementPage.verifyTransactionSinceMessage();
});

Then(/^I should be displayed that online statements only go back 6 months message$/, () => {
    accountStatementPage.verifyStatementLimitMessage();
});

Then(/^I view the transactions for the (.*)$/, (month) => {
    if (month === 'recent') {
        accountStatementPage.verifyTransactionSinceMessage();
    } else if (month === 'previous') {
        this.swipeToPreviousMonth();
    }
});

Then(/^I should not see any transaction$/, () => {
    accountStatementPage.validateNoTransactionsForCreditCard();
});

Then(/^I should not see the filter button$/, () => {
    accountStatementPage.validateNoFilterButton();
});

When(/^I view the filter$/, () => {
    accountStatementPage.clickFilterButton();
});

Then(/^I should be displayed the filter overlay$/, () => {
    accountStatementPage.validateFilterOverlayDisplayed();
});

Then(/^I should be displayed the following options$/, (pageFields) => {
    const FieldsToValidate = pageFields.hashes();
    accountStatementPage.validateFilterOverlayFields(FieldsToValidate);
});

When(/^I close the filter$/, () => {
    accountStatementPage.closeFilterOverlay();
});

Then(/^The filer must be closed and all transactions should be displayed for the month$/, () => {
    accountStatementPage.validateFilterOverlayClosed();
});

Then(/^I should see a message stating that there are no card holder transactions to filter$/, () => {
    accountStatementPage.validateFilterErrorAccountLevelTransactions();
});

When(/^I view my pending transactions for the selected account$/, () => {
    accountStatementPage.clickPendingTransactions();
});

Then(/^I should be displayed all transactions for the selected month$/, () => {
    accountStatementPage.validateClearFilter();
});

Then(/^I should see following fields in the pending transaction accordion$/, (pendingTransactionFields) => {
    const fieldsToValidate = pendingTransactionFields.hashes();
    accountStatementPage.verifyFieldsInPendingTransactionAccordion(fieldsToValidate);
});

Then(/^I should be displayed the statement for (recent|current) without any pending transaction accordion$/, (monthTab) => {
    if (monthTab === 'recent') {
        accountStatementPage.viewRecentsTab();
    } else if (monthTab === 'current') {
        accountStatementPage.validateCurrentMonthName();
    }
});

Then(/^I should not be displayed posted transaction header before the beginning of the posted transactions$/, () => {
    accountStatementPage.validateNoTransactionsHeader();
});

Then(/^I should be displayed posted transaction header after the end of pending transactions$/, () => {
    accountStatementPage.viewTransactionsHeader();
});

Then(/^I should not see payment and transfer option$/, () => {
    viewTransactionDetailPage.validateNoPaymentOption();
});

Then(/^I should see a message stating that the filter cannot be used as there are too many transactions to display on the page$/, () => {
    accountStatementPage.validateFilterErrorTooManyTransactions();
});

When(/^I close the error message dialog box$/, () => {
    aboutPendingTransactionsOverlayPage.clickOnCloseButton();
});

Then(/^I should see following fields in due soon page$/, (dueSoonFields) => {
    const fields = dueSoonFields.hashes();
    viewTransactionDetailPage.checkForDueSoonAndFieldsVisibility(fields);
});

Then(/^I should be displayed the statement page with all the outgoing payments due in the next 30 days$/, () => {
    viewTransactionDetailPage.checkForDueSoonVisibility();
    accountStatementPage.verifyTransactionsArePresent();
});

When(/^I select the help with this transaction link in VTD screen$/, () => {
    viewTransactionDetailPage.clickUnsureAboutTransaction();
});

Then(/^I must be displayed the Unsure About Transactions Help and Info screen$/, () => {
    viewTransactionDetailPage.validateUnsureAboutTransactionPageIsDisplayed();
});

When(/^I select to contact customer service$/, () => {
    accountStatementPage.clickCallUs();
});

When(/^I select to go back$/, () => {
    commonPage.clickBackButton();
});

When(/^I select to go back from Click To Call journey$/, () => {
    commonPage.clickBackButton();
});

When(/^I view the (.*) details$/, (link) => {
    accountStatementPage.selectAERLink(link);
});

Then(/^I must be displayed (.*) overlay giving me information about gross interest rate$/, (type) => {
    accountStatementPage.validateTreasuryInfoOverlay(type);
});

Then(/^I should be navigated back to treasury account page$/, () => {
    accountStatementPage.treasuryAccountPageDisplayed();
});

Then(/^I must be displayed a message informing that the product is not currently available$/, () => {
    accountStatementPage.productNotAvailableMessage();
});

Then(/^I should be displayed the fixed term deposit account page with the following details$/, (treasuryAccountFields) => {
    const fields = treasuryAccountFields.hashes();
    accountStatementPage.treasuryAccountFieldsValidation(fields);
});

Then(/^I should be displayed the 32 day notice account page with the following details$/, (treasuryAccountFields) => {
    const fields = treasuryAccountFields.hashes();
    accountStatementPage.treasuryAccountFieldsValidation(fields);
});

Then(/^I should be displayed VTD page of that transaction$/, () => {
    viewTransactionDetailPage.verifyVtdPage();
});

When(/^I select (.*) (.*) link to view info$/, (type, infoLink) => {
    accountStatementPage.chooseInfoLink(type, infoLink);
});

Then(/^I should be displayed the (.*) as a layover for the particular (.*)/, (infoLink, tab) => {
    accountStatementPage.validateInfoLayover(infoLink, tab);
});

Then(/^I should be displayed upcoming payments web view page$/, () => {
    accountStatementPage.displayUpcomingPaymentWebViewPage();
});

Then(/^I should be on the statements page$/, () => {
    accountStatementPage.displayStatementsPage();
});

Then(/^I must be navigated back to the previous screen$/, () => {
    viewTransactionDetailPage.validateUserNavigatedBackToUnsureScreen();
});

When(/^I tap on the (minimised|expanded) pending transactions accordion$/, (state) => {
    accountStatementPage.clickPendingTransactionToOpenorClose(state);
});

Then(/^I should be displayed with (.*) for (.*) in statements summary$/, (field, type) => {
    accountStatementPage.validateStatementFields(field, type);
});

Then(/^I should be displayed with (nominatedAccount|multipleSettlementAccountTextMsg) on statements page$/, (field) => {
    accountStatementPage.validateStatementFields(field);
});

Then(/^I should not see (additional information|exchange rate) on VTD page$/, (field) => {
    viewTransactionDetailPage.shouldNotSeeAdditionalInfromationOrExchangeRate(field);
});

Then(/^I should not see exchange rate or additional information on VTD page$/, () => {
    viewTransactionDetailPage.shouldNotSeeAdditionalInfromationAndExchangeRate();
});

Then(/^I should see an (.*) error message in place of the transaction$/, (message) => {
    accountStatementPage.validatePendingTransactionErrorMessages(message);
});

Then(/^I should be displayed view your every day offers button$/, () => {
    viewTransactionDetailPage.validateViewYourOffersButton();
});

Then(/^I should be opted in for spending rewards$/, () => {
    accountStatementPage.validateOptInForSpendingRewardsVTD();
});

Then(/^I should see (.*) transactions only/, (type) => {
    accountStatementPage.validateDebitOnlyChequeOnlyPendingTransactions(type);
});

Then(/^I should see account details with the following fields$/, (accountFields) => {
    const FieldsToValidate = accountFields.hashes();
    accountStatementPage.verifyAccountTileFields(FieldsToValidate);
});

Then(/^I should see the statements page expanded for (.*) on the account tile$/, (type) => {
    accountStatementPage.verifyCondensedAccountTile(type);
});

Then(/^I verify the remaining allowance zero on account tile$/, () => {
    accountStatementPage.verifyZeroRemaining();
});

When(/^I swipe down in the statements page$/, () => {
    accountStatementPage.swipeDownInStatementsPage();
});

Then(/^I should see the statements page collapsed for that (.*) account from the account tile$/, () => {
    accountStatementPage.verifyCollapsedStatementsAccountTile();
});

Then(/^The amount value should be equal to the cash back amount displayed in the transaction$/, () => {
    viewTransactionDetailPage.compareCashBackAmount();
});

When(/^I register for spending rewards$/, () => {
    viewTransactionDetailPage.registerForSpendingRewards();
    spendingRewardsPage.clickYesForEverydayOfferButton();
    spendingRewardsPage.clickRegisterPopUpConfirmButton();
});

When(/^I tap on view your offers$/, () => {
    viewTransactionDetailPage.clickViewYourOffers();
});

When(/^I tap on back button in upcoming payments page$/, () => {
    commonPage.clickBackButton();
});

Then(/^I should be navigated back to statements page$/, () => {
    accountStatementPage.validateStatementDate();
});

Then(/^I should scroll down to the end of the transactions to view (.*) message of that (.*) tab$/, (output, month) => {
    accountStatementPage.validateEndOfTransactionMessage(output, month);
});

Then(/^I should be displayed spending rewards hub$/, () => {
    accountStatementPage.validateSpendingRewardsHubSTUBPage();
});

Then(/^I should be navigated back to (.*) with full mortgage details$/, (mortgageTab) => {
    if (DeviceHelper.isAndroid()) {
        accountStatementPage.isSelectAccountPageDisplayed();
    } else {
        accountStatementPage.validateMortgageTab(mortgageTab);
        accountStatementPage.validateMortgageAccordionState();
    }
});

Then(/^I should see no other date of account debited or credited$/, () => {
    viewTransactionDetailPage.noAccountCreditedOrDebitedDate();
});

Then(/^I should see search icon in all tab$/, () => {
    accountStatementPage.validateSearchIcon();
});

When(/^I tap on search icon in all tab$/, () => {
    accountStatementPage.clickSearchIcon();
});

Then(/^I should not see the option to search$/, () => {
    accountStatementPage.validateNoSearchIcon();
});

When(/^I scroll down to load all the transactions in all tab$/, () => {
    accountStatementPage.scrollDownToLoadAllTheTransactionsInAllTab();
});

Then(/^I should be displayed search icon above pending transactions accordion$/, () => {
    accountStatementPage.validateSearchIconPosition();
});

Then(/^I should see error message saying please try again later$/, () => {
    accountStatementPage.validateErrorMessage();
});

Then(/^I should see (.*) no transactions message in (.*) tab of that account$/, (output, month) => {
    accountStatementPage.validateNoTransactionMessage(output, month);
});

Then(/^I should be displayed (.*) for (.*) transactions$/, (field, type) => {
    accountStatementPage.validateTotalFieldForFilters(field, type);
});

Then(/^I should be displayed with (.*) in statements summary$/, (field) => {
    accountStatementPage.validateTreasuryFields(field);
});

When(/^I should not see (.*) option in the action menu of statements page$/, (option) => {
    accountStatementPage.selectActionMenuOnAccountStatementScreen();
    switch (option) {
    case 'viewPaymentsDueSoon':
        actionMenuPage.verifyNoPendingPaymentOption();
        actionMenuPage.closeActionMenu();
        break;
    case 'View Interest Details':
        actionMenuPage.viewInterestDetailsOptionIsNotVisible();
        break;
    default:
        throw new Error('unrecognised action menu option');
    }
});

Then(/^I should be displayed (.*) account name action menu and last four digits of (.*) number in the header$/, (accountName, accountNum) => {
    accountStatementPage.validateStatementsPageHeader(AccountMatcher.getAccount(accountName), accountNum);
});

When(/^I scroll down on the transactions$/, () => {
    accountStatementPage.scrollDownInTheTransactions();
});

Then(/^I should be displayed current balance in the header alongside account Name$/, () => {
    accountStatementPage.validateAccountBalanceInHeader();
});

Then(/^I should not be displayed account tile in statements page$/, () => {
    accountStatementPage.validateNoAccountTile();
});

Then(/^I should be displayed month tabs in place of account tile$/, () => {
    accountStatementPage.validateMonthsTabSection();
});

When(/^I scroll back to the top of my transactions$/, () => {
    accountStatementPage.scrollUpOnTheTransactions();
});

Then(/^The current balance will no longer display in the header and the account tile will reappear$/, () => {
    accountStatementPage.validateCurrentBalanceAndAccountTileBackToPlace();
});

Then(/^The account tile should not be hidden$/, () => {
    accountStatementPage.validateAccountTileNotHidden();
});

Then(/^I should be displayed (.*) account name last four digits of (.*) number in the header and no action menu for that (.*) name$/, (accountName, accountNum, type) => {
    accountStatementPage
        .validateStatementsPageNoActionMenu(AccountMatcher.getAccount(accountName), accountNum, type);
});

When(/^I navigate to (.*) account to validate (.*) and select (.*) account$/, (accountHeaderName, value, type) => {
    const accountName = AccountMatcher.getAccount(accountHeaderName.replace(/ /g, ''));
    accountStatementPage.navigateAndGetAccountNumber(accountName, value, type);
    homePage.selectAccount(accountName);
});

When(/^I navigate to (.*) account and validate (.*)$/, (accountHeaderName, value) => {
    accountStatementPage.navigateAndGetAccountNumber(AccountMatcher.getAccount(accountHeaderName.replace(/ /g, '')), value, accountHeaderName);
});

Then(/^The account tile will reappear$/, () => {
    accountStatementPage.validateAccountTileNotHidden();
});

Then(/^I should not be displayed business name on the account tile$/, () => {
    accountStatementPage.validateNoBusinessName();
});

Then(/^I see the (.*) Account Link lead in the statement page$/, function (accountName) {
    const data = this.dataRepository.get();
    accountStatementPage.shouldSeeAccountLink(data[accountName.replace(/ /g, '')]);
});

Then(/^I should see the View rates link next to Interest rate label on account tile$/, () => {
    accountStatementPage.shouldSeeViewRatesLink();
});

When(/^I tap on View rates link$/, () => {
    accountStatementPage.selectViewRatesLink();
});

Then(/^I should not see the View rates link next to Interest rate label$/, () => {
    accountStatementPage.shouldNotSeeViewRatesLink();
});

When(/^I should see the Interest rate next to Interest rate label on account tile$/, () => {
    accountStatementPage.shouldSeeInterestRate();
});

Then(/^I should not see view interest rate details option in the action menu$/, () => {
    actionMenuPage.viewInterestDetailsOptionIsNotVisible();
});

When(/^I tap and hold on the account tile to Savings Interest Rate Details screen$/, () => {
    accountStatementPage.tapAndHoldOnAccountTile();
});

Then(/^I tap on minus sign to close the accordion$/, () => {
    accountStatementPage.minimiseMortgageAccordion();
});

Then(/^The (.*) value should be same with the value in Account overview page$/, (value) => {
    accountStatementPage.compareStatementsAccountTileValueWithAOVPage(value);
});

Then(/^I should be displayed (upcomingPayments|spendingInsights) web view page$/, (option) => {
    accountStatementPage.displayUpcomingPaymentWebViewPage(option);
});

When(/^I tap on back button in spending insights page$/, () => {
    commonPage.clickBackButton();
});

When(/^I should be displayed action menu with list of options$/, () => {
    accountStatementPage.viewActionMenuOptions();
});

Then(/^I (should|should not) see the remaining overdraft limit progress bar$/, (type) => {
    accountStatementPage.verifyOverdraftLimitProgressBar(type);
});

Then(/^I should see exposed pending transactions ordered below$/, (table) => {
    const tableData = table.hashes();
    accountStatementPage.validateTransactionDetails(tableData);
});

Then(/^The balance after pending value should be (.*) with difference between account balance and the total value of pending transactions$/, (symbol) => {
    accountStatementPage.calculateBalanceAfterPending(symbol);
});

Then(/^I should see (.*) error message under (.*) respectively$/, (errorMessage, header) => {
    accountStatementPage.validateExposedPendingErrorMessage(errorMessage, header);
});

Then(/^I should see (.*) pending transactions$/, (type) => {
    accountStatementPage.validateExposedPendingDebitOnlyChequeOnlyTransactions(type);
});

Then(/^I should not see the green or blue vertical bar$/, () => {
    accountStatementPage.validateNoCreditIndicator();
});

Then(/^I should see a chevron on pending transaction lines of iOS and not on android$/, () => {
    accountStatementPage.validateChevron();
});

Then(/^I should see a list of below (cleared|pending) transaction help options$/, (type, options) => {
    const FieldsToValidate = options.hashes();
    viewTransactionDetailPage.validateHelpPageOptions(type, FieldsToValidate);
});

Then(/^I select below options to view the related helpDetails page$/, (helpOptions) => {
    const tableData = helpOptions.hashes();
    viewTransactionDetailPage.selectAndValidateHelpOptionsDetailsPage(tableData);
});

When(/^I should not see travelDisputes option$/, () => {
    viewTransactionDetailPage.validateNoTravelDisputesField();
});

Then(/^I select below links under guidance to view related page$/, (helpOptions) => {
    const tableData = helpOptions.hashes();
    viewTransactionDetailPage.selectAndValidateHelpPageGuidanceLinks(tableData);
});

Then(/^I select below links in the helpDetails page to view respective web pages$/, (helpDetailsWebLinks) => {
    const tableData = helpDetailsWebLinks.hashes();
    viewTransactionDetailPage.selectAndValidateHelpDetailsPageWebLinks(tableData);
});

When(/^I select Travel Dispute option on helpDetails page$/, () => {
    viewTransactionDetailPage.selectTravelDisputes();
});

Then(/^I should see following fields on (.*) page$/, (page, options) => {
    const tableData = options.hashes();
    viewTransactionDetailPage.valdiateOptions(tableData);
});

When(/^I select Get Started Button on travel disputes help page$/, () => {
    viewTransactionDetailPage.selectTravelDisputeGetStartedButton();
});

When(/^I select the (.*) to update personal details$/, (button) => {
    viewTransactionDetailPage.selectUpDetailsButton(button);
});

Then(/^I should not see continue button$/, () => {
    viewTransactionDetailPage.shouldNotSeeContinueButton();
});

When(/^I select continue button on BeforeYouBegin page$/, () => {
    viewTransactionDetailPage.selectContinueButton();
});

Then(/^I should be on GForm page$/, () => {
    viewTransactionDetailPage.shouldSeeGFormPage();
});

When(/^I should be on the BeforeYouBegin page$/, () => {
    viewTransactionDetailPage.shouldBeOnBeforeYouBeginPage();
});

Then(/^I select one of the posted transactions to validate fields on the VTD$/, () => {
    viewTransactionDetailPage.selectPostedTransactionToValidateVTDfields();
});

Then(/^The fields on VTD should match with the transaction line$/, () => {
    viewTransactionDetailPage.validateFieldsOnVTD();
});

Then(/^I should be displayed default logo or merchant logo next to the transaction$/, () => {
    accountStatementPage.shouldSeeTransactionLogo();
});

Then(/^I should be displayed default logo or merchant logo of that transaction in VTD screen$/, () => {
    viewTransactionDetailPage.shouldSeeTransactionLogo();
});
