const {When, Then} = require('cucumber');
const SaveInterestDetailPage = require('../pages/savingsinterestRateDetails.page');
const ViewTransactionDetailPage = require('../pages/viewTransactionDetail.page');
const HomePage = require('../pages/home.page');
const AccountMatcher = require('../data/utils/accountMatcher');

const sirDetailPage = new SaveInterestDetailPage();
const viewTransactionDetailPage = new ViewTransactionDetailPage();
const homePage = new HomePage();

Then(/^I should be on Savings Interest Rate Details screen/, () => {
    sirDetailPage.checkForPageVisibility();
});

When(/^I select View Interest Details option of (.*) account from action menu$/, (accountName) => {
    homePage.selectActionMenuOfSpecificAccount(AccountMatcher.getAccount(accountName));
    sirDetailPage.selectViewInterestDetailsOption();
});

Then(/^I close the interest rate details screen using the close button$/, () => {
    sirDetailPage.closeViewInterestDetailsOption();
});

Then(/^I should see the following fields in the details screen$/, (pageFields) => {
    const FieldsToValidate = pageFields.hashes();
    sirDetailPage.checkForFieldsVisibility(FieldsToValidate);
});

When(/^I tap and hold on the (.*) account tile to Savings Interest Rate Details screen$/, (accountName) => {
    sirDetailPage.holdToOpenSirDetailsScreen(AccountMatcher.getAccount(accountName));
});

When(/^I close the interest rate details screen using swipe action$/, () => {
    sirDetailPage.closeInterestDetailSwipeAction();
});

When(/^I should not see the view interest rate details option from the action menu in the statement page$/, () => {
    sirDetailPage.openAndViewInterestRateDetailIsNotVisible();
});

When(/^I close the posted transactions$/, () => {
    viewTransactionDetailPage.closePostedTransactionDetails();
});

When(/^I close the posted transactions using swipe action$/, () => {
    viewTransactionDetailPage.closePostedTransactionDetailsSwipeAction();
});

When(/^I close the pending transactions VTD$/, () => {
    viewTransactionDetailPage.closePendingTransactionDetails();
});

When(/^I close the pending transactions using swipe action$/, () => {
    viewTransactionDetailPage.closePendingTransactionDetailsSwipeAction();
});

When(/^I select the view interest rate details option from the action menu of (.*) from your accounts$/, (type) => {
    homePage.selectActionMenuOfThatAccount(type);
    sirDetailPage.SelectViewInterestDetailsOption();
});

Then(/^I should not be on Savings Interest Rate Details screen/, () => {
    sirDetailPage.savingInterestRateScreenNotVisible();
});

Then(/^The savings interest rate details screen should be closed$/, () => {
    sirDetailPage.verifySMRIsClosed();
});

Then(/^I should be on the selected (.*) account tile on the home page$/, (accountName) => {
    sirDetailPage.validateTheScreenAfterSMRIsClosed(AccountMatcher.getAccount(accountName));
});

Then(/^I should be on the statements page of the selected (.*) account$/, (accountName) => {
    sirDetailPage.validateTheScreenAfterSMRIsClosed(AccountMatcher.getAccount(accountName));
});

Then(/^I check the interest rate displayed for saving account$/, () => {
    sirDetailPage.validateInterestRate();
});
