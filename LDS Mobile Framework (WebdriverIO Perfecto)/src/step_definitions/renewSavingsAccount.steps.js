const {When} = require('cucumber');
const RenewSavingsAccountPage = require('../pages/renewSavingsAccount.page');

const renewSavingsAccountPage = new RenewSavingsAccountPage();

When(/^I should be on renew savings account page$/, () => {
    renewSavingsAccountPage.waitForPageLoad();
});

When(/^I choose the renew saving account and proceed to renew it$/, () => {
    renewSavingsAccountPage.selectSavingAccount();
    renewSavingsAccountPage.selectRenewSaving();
});

When(/^I select apply now to renew savings account$/, () => {
    renewSavingsAccountPage.selectApply();
});

When(/^I should be on confirm your selection page$/, () => {
    renewSavingsAccountPage.verifyConfirmPage();
});

When(/^I select terms and conditions to renew savings account$/, () => {
    renewSavingsAccountPage.selectTermsAndConditions();
});

When(/^I confirm to renew saving account$/, () => {
    renewSavingsAccountPage.selectConfirm();
});

When(/^I should be on renewed saving account success page$/, () => {
    renewSavingsAccountPage.verifySuccessMessage();
});
