const {Then, When} = require('cucumber');
const WhitelistedBiller = require('../../pages/whitelistedBiller/whitelistedBiller.page');
const PaymentHubMainPage = require('../../pages/paymentHub/paymentHubMain.page');
const DeviceHelper = require('../../utils/device.helper');

const wrongBillerReference = '1234567';
const whitelistedBiller = new WhitelistedBiller();
const paymentHubMainPage = new PaymentHubMainPage();

Then(/^I should (see|not see) the biller (.*)$/, (isShown, billerName) => {
    whitelistedBiller.verifyBillerVisibility(isShown, billerName);
});

When(/^I select the biller (.*) from the biller list$/, (billerName) => {
    whitelistedBiller.selectBiller(billerName);
});

Then(/^I validate reference field label$/, () => {
    whitelistedBiller.shouldSeeReferenceHint();
});

Then(/^I validate the error banner message for wrong reference value$/, () => {
    whitelistedBiller.shouldSeeRefErrorMessage();
});

Then(/^I enter the (.*) reference value for the biller$/, (reference) => {
    if (reference !== 'wrong') {
        paymentHubMainPage.closeErrorMessage();
        paymentHubMainPage.enterReference(reference);
    } else if (DeviceHelper.isAndroid()) {
        paymentHubMainPage.enterReference(wrongBillerReference);
        paymentHubMainPage.selectAmountField();
    } else {
        paymentHubMainPage.selectAmountField();
        paymentHubMainPage.enterReference(wrongBillerReference);
    }
});

Then(/^I validate the error banner message for api failure$/, () => {
    whitelistedBiller.shouldSeeApiFailErrorMessage();
});
