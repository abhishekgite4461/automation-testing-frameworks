const {Then} = require('cucumber');
const NewProductEnquiriesPage = require('../pages/callUs/newProductEnquiries.page');

const newProductEnquiriesPage = new NewProductEnquiriesPage();

Then(/^I select individual (.*) on new product page$/, (type) => {
    newProductEnquiriesPage.waitForPageLoad();
    newProductEnquiriesPage.selectArrangement(type);
});
