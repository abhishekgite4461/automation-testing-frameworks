const {When, Then} = require('cucumber');
const OrderPaperStatementsPage = require('../pages/orderPaperStatements.page');

const orderPaperStatementsPage = new OrderPaperStatementsPage();

Then(/^I should be able to view order paper statements webview page$/, () => {
    orderPaperStatementsPage.waitForPageLoad();
});

Then(/^I select statement and proceed to order paper statements$/, () => {
    orderPaperStatementsPage.selectStatements();
    orderPaperStatementsPage.SelectContinue();
});

Then(/^I should be on confirm your order page$/, () => {
    orderPaperStatementsPage.verifyConfirmYourOrderPage();
});

When(/^should see below option on confirm your order page$/, (table) => {
    const tableData = table.hashes();
    orderPaperStatementsPage.validateConfirmOrderOptions(tableData);
});

Then(/^I select choose an account from make payment from panel$/, () => {
    orderPaperStatementsPage.selectChooseAccount();
});

When(/^I should see details of (.*) account in make payment from panel$/, function (accountName) {
    const data = this.dataRepository.get();
    orderPaperStatementsPage.verifyAccountDetails(data[accountName.replace(/ /g, '')]);
});
