const {When, Then} = require('cucumber');
const YourProfilePage = require('../pages/marketingPreferences/yourProfile.page');

const yourProfilePage = new YourProfilePage();

Then(/^I should be on the your profile page$/, () => {
    yourProfilePage.waitForPageLoad();
});

When(/^I update market preferences in your profile page$/, () => {
    yourProfilePage.waitForPageLoad();
    yourProfilePage.toggleSmsUpdate();
    yourProfilePage.toggleEmailUpdate();
    yourProfilePage.toggleSalesMessaging();
});
