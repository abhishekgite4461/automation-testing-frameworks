const {Given} = require('cucumber');
const DeviceState = require('../utils/deviceState');
const DeviceHelper = require('../utils/device.helper');
const EnrolmentTask = require('../tasks/enrolment.task');

Given(/^I am a (.*) user$/, function (accTypes) {
    const enrolmentTask = new EnrolmentTask(
        this.dataRepository,
        this.appConfigurationRepository,
        this.stubRepository,
        this.switchRepository,
        this.featureRepository,
        this.abTestRepository,
        this.dataConsentRepository,
        new DeviceState(DeviceHelper.deviceId())
    );
    enrolmentTask.assignUser(accTypes);
    enrolmentTask.prepareAppForAccountTypes(accTypes);
});

Given(/^I navigate to the account aggregated (lds|bos|hfx|mbna) app for a (.*) user$/, function (appName, userType) {
    browser.pause(5000);
    const enrolmentTask = new EnrolmentTask(
        this.dataRepository,
        this.appConfigurationRepository,
        this.stubRepository,
        this.switchRepository,
        this.featureRepository,
        this.abTestRepository,
        this.dataConsentRepository,
        new DeviceState(DeviceHelper.deviceId())
    );
    enrolmentTask.navigateToAccountAggregatedApp(appName, userType);
});
