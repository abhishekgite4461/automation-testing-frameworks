const {When} = require('cucumber');
const PostCodeEntryPage = require('../../pages/changeOfAddress/postCodeEntry.page');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');
const Actions = require('../../pages/common/actions');

const postCodeEntry = new PostCodeEntryPage();
const BLANK_VALUE = '';

When(/^.*see an option to search my new address using postcode.*$/, () => {
    postCodeEntry.verifyPostCodeEntryTitle();
});

When(/^I focus on post code$/, () => {
    postCodeEntry.verifyPostCodeEntryTitle();
    postCodeEntry.clickPostCode();
});

When(/^I enter the (correct|no address) post code of my new address$/, function (type) {
    postCodeEntry.verifyPostCodeEntryTitle();
    const data = this.dataRepository.get();
    const postCode = (type === 'correct') ? data.postCode : data.postCodeNoAddress;
    postCodeEntry.enterPostCode(postCode);
});

When(/^I replace existing with new post code of my new address$/, () => {
    postCodeEntry.verifyPostCodeEntryTitle();
    let postCode = SavedAppDataRepository.getData(SavedData.CURRENT_POST_CODE);
    if (postCode === 'EC1Y 4XX') {
        postCode = 'SE19EQ';
    } else {
        postCode = 'EC1Y4XX';
    }
    postCodeEntry.enterPostCode(postCode.replace(/"/g, ''));
});

When(/^I enter post code as "(.*)"$/, (postCode) => {
    postCodeEntry.verifyPostCodeEntryTitle();
    postCodeEntry.enterPostCode(postCode.replace(/"/g, ''));
});

When(/^I should (see|not see) post code help text$/, (isVisible) => {
    if (isVisible === 'see') {
        postCodeEntry.isVisiblePostCodeTip(true);
    } else {
        postCodeEntry.isVisiblePostCodeTip(false);
    }
});

When(/^I should see (correct|no address )post code is same as entered$/, function (type) {
    const data = this.dataRepository.get();
    const postCode = (type === 'correct') ? data.postCode : data.postCodeNoAddress;
    postCodeEntry.validatePostCode(postCode.toUpperCase());
});

When(/^I should see post code as "(.*)"$/, (expectedPostCode) => {
    postCodeEntry.verifyPostCodeEntryTitle();
    postCodeEntry.validatePostCode(expectedPostCode.toUpperCase());
});

When(/^I delete a character from the post code$/, () => {
    postCodeEntry.deletePostCodeCharacter();
});

When(/^I clear the post code$/, () => {
    postCodeEntry.clickPostCode();
    postCodeEntry.clearPostCode();
});

When(/^I am not able to enter long post code$/, () => {
    postCodeEntry.verifyPostCodeEntryTitle();
    postCodeEntry.validateMaxLength();
});

When(/^I am not able to enter long post code with space$/, () => {
    postCodeEntry.verifyPostCodeEntryTitle();
    postCodeEntry.validateMaxLengthWithSpace();
});

When(/^I should see "(.*)" post code is capitalized$/, (expectedPostCode) => {
    postCodeEntry.validatePostCodeCapitalization(expectedPostCode.toUpperCase());
});

When(/^I tap out of the post code field$/, () => {
    Actions.tapOutOfField();
    Actions.hideDeviceKeyboard();
});

When(/^I should see the instructions regarding address change$/, () => {
    postCodeEntry.verifyPostCodeEntryTitle();
    postCodeEntry.isVisibleEnterPostcodeHelpText();
    postCodeEntry.isVisibleEnterPostcodeCallUsText();
});

When(/^I proceed to call customer care$/, () => {
    postCodeEntry.verifyPostCodeEntryTitle();
    postCodeEntry.clickCallUsLink();
});

When(/^I go back to previous page$/, () => {
    postCodeEntry.clickBackArrow();
});

When(/^I verify find my new address is (enabled|disabled) for entered address/, (isEnabled, table) => {
    for (const option of table.hashes()) {
        postCodeEntry.enterPostCode(option.validPostCode);
    }
    if (isEnabled === 'enabled') {
        postCodeEntry.isEnabledFindAddress(true);
    } else {
        postCodeEntry.isEnabledFindAddress(false);
    }
});

When(/^I (can|can not) proceed to find my new address$/, (isEnabled) => {
    if (isEnabled === 'can') {
        postCodeEntry.isEnabledFindAddress(true);
    } else {
        postCodeEntry.isEnabledFindAddress(false);
    }
});

When(/^I perform a postcode search to see the matching addresses$/, () => {
    postCodeEntry.clickFindAddress();
});

When(/^I see that post code entered earlier is not displayed$/, () => {
    postCodeEntry.verifyPostCodeEntryTitle();
    postCodeEntry.validatePostCode(BLANK_VALUE);
});

When(/^I should (see|not see) an error icon$/, (isExisting) => {
    if (isExisting === 'see') {
        postCodeEntry.isExistingErrorIcon(true);
    } else {
        postCodeEntry.isExistingErrorIcon(false);
    }
});

When(/^I should see the Webview Change of Address page$/, () => {
    postCodeEntry.isVisibleWebViewPage();
});
