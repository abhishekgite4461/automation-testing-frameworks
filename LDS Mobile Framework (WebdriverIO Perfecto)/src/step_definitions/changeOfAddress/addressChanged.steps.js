const {When} = require('cucumber');
const AddressChangedPage = require('../../pages/changeOfAddress/addressChanged.page');

const addressChanged = new AddressChangedPage();

When(/^I should see address change successful message$/, () => {
    addressChanged.verifyAddressChangedTitle();
});

When(/^I can proceed to homepage after my address got updated$/, () => {
    addressChanged.verifyAddressChangedTitle();
    addressChanged.verifyBackToHomeButton();
});

When(/^I proceed to homepage after my address got updated$/, () => {
    addressChanged.verifyAddressChangedTitle();
    addressChanged.selectHomeButton();
});
