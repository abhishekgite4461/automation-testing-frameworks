const {When} = require('cucumber');
const ConfirmAddressPage = require('../../pages/changeOfAddress/confirmAddress.page');

const confirmAddress = new ConfirmAddressPage();

When(/^I should be asked to confirm my new address$/, () => {
    confirmAddress.verifyConfirmAddressTitle();
});

When(/^I should see Map placeholder image$/, () => {
    confirmAddress.isVisibleMapPlaceholderImage();
});

When(/^.*see my new address on Map$/, () => {
    confirmAddress.isVisibleMap();
});

When(/^.*see my new address selected for confirmation$/, () => {
    confirmAddress.validateNewAddress();
});

When(/^.*see my new address is in title case$/, () => {
    confirmAddress.validateNewAddressTitleCase();
});

When(/^I can proceed to confirm new address$/, () => {
    confirmAddress.isEnabledConfirmAddressButton();
});

When(/^I confirm my new address$/, () => {
    confirmAddress.clickConfirmAddressButton();
});
