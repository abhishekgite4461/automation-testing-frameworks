const {When} = require('cucumber');
const OneLastThingPage = require('../../pages/changeOfAddress/oneLastThing.page');

const oneLastThing = new OneLastThingPage();

When(/^.*final instructions before updating my address$/, () => {
    oneLastThing.verifyOneLastThingTitle();
});

When(/^I should see final instructions before agreeing to update my address$/, () => {
    oneLastThing.isVisiblePageText();
});

When(/^I can proceed to select that I understand the instructions$/, () => {
    oneLastThing.isEnabledIUnderstandButton();
});

When(/^I select that I understand the instructions$/, () => {
    oneLastThing.selectIUnderstrandButton();
});

When(/^I confirm to update my new address$/, function () {
    const data = this.dataRepository.get();
    oneLastThing.selectIUnderstrandButton();
    oneLastThing.enterPassword(data.password);
    oneLastThing.selectDialogOkButton();
});

When(/^I should (see|not see) enter password dialog$/, (isVisible) => {
    if (isVisible === 'see') {
        oneLastThing.isVisibleEnterPasswordDialog(true);
    } else {
        oneLastThing.isVisibleEnterPasswordDialog(false);
    }
});

When(/^I enter my password to authenticate$/, function () {
    const data = this.dataRepository.get();
    oneLastThing.enterPassword(data.password);
    oneLastThing.selectDialogOkButton();
});

When(/^I enter password as "(.*)" in the enter password dialog$/, (password) => {
    oneLastThing.enterPassword(password);
});

When(/^I click forgot password link in the enter password dialog$/, () => {
    oneLastThing.selectForgotPassword();
});

When(/^I should see cancel button is enabled in the enter password dialog$/, () => {
    oneLastThing.isDialogCancelButtonEnabled();
});

When(/^I cancel the enter password dialog$/, () => {
    oneLastThing.selectDialogCancelButton();
});

When(/^I should see ok button is (enabled|disabled) in the enter password dialog$/, (isEnabled) => {
    if (isEnabled === 'enabled') {
        oneLastThing.isDialogOkButtonEnabled(true);
    } else {
        oneLastThing.isDialogOkButtonEnabled(false);
    }
});

When(/^I click ok button in the enter password dialog$/, () => {
    oneLastThing.selectDialogOkButton();
});
