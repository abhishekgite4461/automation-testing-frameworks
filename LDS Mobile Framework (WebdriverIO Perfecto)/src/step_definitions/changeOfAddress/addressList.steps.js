const {When} = require('cucumber');
const AddressListPage = require('../../pages/changeOfAddress/addressList.page');

const addressList = new AddressListPage();

When(/^I should see list of addresses for entered postcode$/, () => {
    addressList.verifyAddressListTitle();
});

When(/^I see list of addresses for entered postcode$/, () => {
    addressList.verifyAddressListTitle();
});

When(/^I should see Address List instructions$/, () => {
    addressList.verifyAddressListTitle();
    addressList.isVisibleAddressListText();
});

When(/^I should see the Address List$/, () => {
    addressList.isVisibleAddressList();
});

When(/^I select my new address$/, () => {
    addressList.verifyAddressListTitle();
    addressList.selectAddress();
});

When(/^I should see My Address Not On List$/, () => {
    addressList.isVisibleAddressNotOnListButton();
});

When(/^I choose My Address Not On List$/, () => {
    addressList.clickAddressNotOnListButton();
});

When(/^I should see the Address List in title case having Post Code as entered$/, () => {
    addressList.verifyAddressListTitle();
    addressList.validateAddressListTitleCase();
});
