const {When} = require('cucumber');
const HandOffErrorPage = require('../../pages/changeOfAddress/handOffError.page');

const handOffError = new HandOffErrorPage();

When(/^I should be displayed a change of address error message$/, () => {
    handOffError.verifyHandOffErrorTitle();
    handOffError.isVisibleHandOffErrorText();
});

When(/^I (am|am not) provided Call Us option$/, (ableToProceed) => {
    if (ableToProceed === 'am') {
        handOffError.isVisibleCallUsButton(true);
    } else {
        handOffError.isVisibleCallUsButton(false);
    }
});

When(/^I proceed to Call us from error page$/, () => {
    handOffError.clickCallUsButton();
});

When(/^I (am|am not) provided Back To Your Personal Details option$/, (ableToProceed) => {
    if (ableToProceed === 'am') {
        handOffError.isVisibleBackToPersonalDetailsButton(true);
    } else {
        handOffError.isVisibleBackToPersonalDetailsButton(false);
    }
});

When(/^I proceed to Personal Details from error page$/, () => {
    handOffError.clickBackToPersonalDetailsButton();
});

When(/^I should be logged off after entering password incorrectly multiple times$/, () => {
    handOffError.isVisibleForcedLogoutPage();
});
