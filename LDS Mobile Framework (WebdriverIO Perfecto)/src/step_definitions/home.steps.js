const {Given, When, Then} = require('cucumber');

const HomePage = require('../pages/home.page');
const ActionMenuPage = require('../pages/actionMenu.page');
const LostAndStolenCardPage = require('../pages/lostAndStolen.page');
const FreezeCardTransactions = require('../pages/freezeCardTransactions.page');
const NoEligibleAccountsErrorPage = require('../pages/transferAndPayments/noEligibleAccountsError.page');
const PaymentHubMainPage = require('../pages/paymentHub/paymentHubMain.page');
const ReplacementCardAndPinPage = require('../pages/replacementCardAndPinPage.page');
const LoanAnnualStatement = require('../pages/loanAnnualStatement.page');
const ViewRecipientPage = require('../pages/paymentHub/viewRecipient.page');
const AlertPopUpPage = require('../pages/alertPopUp.page');
const DeviceHelper = require('../utils/device.helper.js');
const SettingsPage = require('../pages/settings/settings.page');
const CommonPage = require('../pages/common.page');
const AccountMatcher = require('../data/utils/accountMatcher');
const RequestPermissionPage = require('../pages/requestPermissionPopUp.page');
const YourAccountsTask = require('../tasks/yourAccounts.task');
const NavigationTask = require('../tasks/navigation.task');
const SearchLandingPage = require('../pages/inAppSearch/searchLanding.page');
const AccountStatementPage = require('../pages/statements/accountStatement.page.js');

const SavedAppDataRepository = require('../data/savedAppData.repository');
const SavedData = require('../enums/savedAppData.enum');
const PdfStatementsPage = require('../pages/pdfStatement.page');
const RenameAccountPage = require('../pages/renameAccount.page');

const paymentHubMainPage = new PaymentHubMainPage();
const viewRecipientPage = new ViewRecipientPage();
const actionMenuPage = new ActionMenuPage();
const lostAndStolenCardPage = new LostAndStolenCardPage();
const freezeCardTransactions = new FreezeCardTransactions();
const noEligibleAccountsErrorPage = new NoEligibleAccountsErrorPage();
const loanAnnualStatement = new LoanAnnualStatement();
const replacementCardAndPinPage = new ReplacementCardAndPinPage();
const alertPopUpPage = new AlertPopUpPage();
const settingsPage = new SettingsPage();
const homePage = new HomePage();
const commonPage = new CommonPage();
const requestPermissionPage = new RequestPermissionPage();
const pdfStatementsPage = new PdfStatementsPage();
const searchLandingPage = new SearchLandingPage();
const renameAccountPage = new RenameAccountPage();
const accountStatementPage = new AccountStatementPage();

When(/^I should be on the home page$/, () => homePage.validateHomePage());

When(/^I select the Payment Hub option in the bottom navigation$/, () => homePage.selectPayAndTransfer());

When(/^I select reactivate ISA option in the action menu of (.*) account$/, () => YourAccountsTask.selectReactivateIsaOfAccount);

When(/^I select top up ISA option in the action menu of (.*) account$/, function (accountName) {
    const data = this.dataRepository.get();
    homePage.waitForPageLoad();
    homePage.selectActionMenuOfSpecificAccount(data[accountName.replace(/ /g, '')]);
    actionMenuPage.selectActionMenuTopUpIsa();
});

When(/^I should see the below correct options in the home page$/, (table) => {
    const tableData = table.hashes();
    homePage.verifyTitle();
    homePage.validateHomePageOptions(tableData);
});

When(/^I navigate and validate the options in the (.*) account in below table in the home page$/,
    (accountName, table) => {
        const tableData = table.hashes();
        homePage.navigateAndValidateOptionInAccountTile(AccountMatcher.getAccount(accountName.replace(/ /g, '')), tableData, accountName);
    });

When(/^I validate the (.*) account balance as (.*) in the home page$/, (accountName, balanceType) => {
    homePage.validateBalanceTypeInAccountTile(AccountMatcher.getAccount(accountName.replace(/ /g, '')), balanceType);
});

When(/^I navigate and validate the options in the (.*) account in below table in the home page recursively$/,
    function (accountName, table) {
        const data = this.dataRepository.get();
        const tableData = table.hashes();
        homePage.navigateAndValidateOptionInAccountTileRecursively(
            data[accountName.replace(/ /g, '')], tableData, SavedAppDataRepository.getData(SavedData.ACCOUNT_ITERATOR_E2E)
        );
    });

When(/^I am able to view (.*?) in contextual menu$/, (optionName) => {
    actionMenuPage.validateActionMenuOption(optionName);
});

When(/^I select the "(.*)" option$/, (optionsInActionMenu) => {
    actionMenuPage.selectActionMenuOptions(optionsInActionMenu);
});

When(/^I select (.*) from contextual menu$/, (optionName) => {
    actionMenuPage.clickOnActionMenuOption(optionName);
});

When(/^I should see the below options in the action menu of the (.*?) account/, (accountName, table) => {
    const tableData = table.hashes();
    actionMenuPage.validateActionMenuOptions(tableData);
});

When(/^I should not see the below options in the action menu of the (.*) account/, (accountName, table) => {
    const tableData = table.hashes();
    actionMenuPage.validateActionMenuOptions(tableData, false);
});

Then(/^I should see report issue button$/, () => {
    homePage.verifyReportIssueIsVisible();
});

Then(/^I should not see top up ISA option in the action menu$/, () => {
    actionMenuPage.verifyTopUpISANotPresent();
});

When(/^I close the action menu in the home page$/, () => {
    actionMenuPage.closeActionMenu();
});

When(/^I select the call us tile on home page$/, () => {
    homePage.selectCallUs();
});

When(/^I should be on the annual loan PDF statements page$/, () => {
    loanAnnualStatement.waitForPageLoad();
});

When(/^I should be on the lost and stolen page$/, () => {
    lostAndStolenCardPage.waitForPageLoad();
});

When(/^I should be on the Freeze card transactions page$/, () => {
    freezeCardTransactions.waitForPageLoad();
});

Then(/^I select all options available on Freeze card transactions page$/, () => {
    freezeCardTransactions.selectAllOptions();
});

When(/^I select report a card lost or stolen now option on Freeze card transactions page$/, () => {
    freezeCardTransactions.selectReportLostOrStolen();
});

When(/^I should be on the replacement card and pin page$/, () => {
    replacementCardAndPinPage.waitForPageLoad();
});

When(/^I select deposit cheque option in action menu$/, () => {
    actionMenuPage.selectChequeDepositOption();
});

When(/^I select the back button in the lost and stolen page$/, () => {
    lostAndStolenCardPage.waitForPageLoad();
    lostAndStolenCardPage.selectBackButton();
});

When(/^I navigate via (.*) to see the credit card transaction details of (.*) account$/, function (type, accountName) {
    const data = this.dataRepository.get();
    if (type === 'actions menu') {
        homePage.selectActionMenuOfSpecificAccount(data[accountName.replace(/ /g, '')]);
        actionMenuPage.selectViewTransactionsOption();
    } else if (type === 'account') {
        homePage.selectAccount(data[accountName.replace(/ /g, '')]);
    } else {
        throw new Error('Type does not match');
    }
});

When(/^I select the "(.*)" option in the action menu of (.*) account$/, (option, accountName) => {
    homePage.selectActionMenuOfSpecificAccount(AccountMatcher.getAccount(accountName.replace(/ /g, '')));
    switch (option) {
    case 'make an overpayment':
        actionMenuPage.selectMakeAnOverpaymentOption();
        break;
    case 'pay a credit card':
        actionMenuPage.selectPayCreditCard();
        break;
    case 'transfer and payment':
        actionMenuPage.selectTransferAndPaymentOption();
        break;
    case 'view interest rate details':
        actionMenuPage.viewInterestDetailsOptionIsNotVisible();
        break;
    case 'PDF statements':
        actionMenuPage.selectPdfStatements();
        break;
    case 'order paper statements':
        actionMenuPage.selectOrderPaperStatements();
        break;
    case 'lost or stolen cards':
        actionMenuPage.selectLostAndStolenOption();
        break;
    case 'annual PDF statements':
        actionMenuPage.selectLoanAnnualStatementOption();
        break;
    case 'international payment':
        actionMenuPage.selectInternationalPayment();
        break;
    case 'replacement card and pin':
        actionMenuPage.selectReplacementCardAndPinOption();
        break;
    case 'view transaction':
        actionMenuPage.selectViewTransactionsOption();
        break;
    case 'direct debit':
        actionMenuPage.selectDirectDebitOption();
        break;
    case 'standing order':
        actionMenuPage.selectStandingOrderOption();
        break;
    case 'borrow more':
        actionMenuPage.selectBorrowMoreOption();
        break;
    case 'pension transfer':
        actionMenuPage.selectPensionTransferOption();
        break;
    case 'send bank details':
        actionMenuPage.selectSendBankDetailsOption();
        break;
    case 'standing order & direct debit':
        actionMenuPage.selectStandingOrderAndDirectDebitOption();
        break;
    case 'change account type':
        actionMenuPage.selectChangeAccountTypeOption();
        break;
    case 'rename account':
        actionMenuPage.selectRenameAccountOption();
        break;
    case 'save the change':
        actionMenuPage.selectSaveTheChangeOption();
        break;
    case 'Add your Reward Extras':
        actionMenuPage.selectAddRewardExtrasOption();
        break;
    case 'Track your Reward Extras':
        actionMenuPage.selectTrackRewardExtrasOption();
        break;
    case 'view spending insights':
        actionMenuPage.selectViewSpendingInsights();
        break;
    case 'Download transactions':
        actionMenuPage.selectDownloadTransactionsOption();
        break;
    default:
        throw new Error('unrecognised action menu option');
    }
});

Then(/^I am not shown the pay credit card option in the Action menu$/, () => {
    actionMenuPage.verifyNoPayCreditCard();
});

When(/^I navigate to (.*) account on home page$/, (accountName) => {
    homePage.swipeToAccount(AccountMatcher.getAccount(accountName));
});

const chooseAccount = (accountName) => {
    homePage.dismissServerErrorIfPresent();
    homePage.chooseAccount(AccountMatcher.getAccount(accountName), accountName);
};

When(/^I select (.*) account tile from home page$/, chooseAccount);

When(/^I should see (viewUpcomingPayment|viewPaymentsDueSoon|viewSpendingInsights) option in the action menu$/, (option) => {
    actionMenuPage.verifyPendingPaymentOption(option);
});

When(/^I should not see view interest rate details option in the action menu of (.*) account$/, function (accountName) {
    const data = this.dataRepository.get();
    homePage.selectActionMenuOfSpecificAccount(data[accountName.replace(/ /g, '')]);
    actionMenuPage.viewInterestDetailsOptionIsNotVisible();
});

When(/^I select (viewUpcomingPayment|viewPaymentsDueSoon) option in the action menu of (.*) account$/, (option, accountName) => {
    homePage.selectActionMenuOfSpecificAccount(AccountMatcher.getAccount(accountName));
    actionMenuPage.selectPendingPaymentOption(option);
});

When(/^I select the action menu of (.*) account$/, (accountName) => {
    homePage.selectActionMenuOfSpecificAccount(AccountMatcher.getAccount(accountName.replace(/ /g, '')));
});

When(/^I select (action menu|renew account sharing|settings) button on external bank (.*) (Current|Credit|Savings) account tile$/, function (buttonName, bankName, accountType) {
    const data = this.dataRepository.get();
    homePage.selectButtonOnExternalBankAccountTile(buttonName, data[bankName], accountType);
});

When(/^I should see (.*) interest rate value on account tile$/, (output) => {
    homePage.verifyInterestRateValues(output);
});

When(/^I select everyday offers tile from home page$/, () => {
    homePage.selectRegisterForEverydayOffersTile();
});

When(/^I should not see everyday offers tile on home page$/, () => {
    homePage.verifyEverydayOffersTileIsNotVisible();
});

When(/^I select the view all offers button from home page$/, () => {
    homePage.selectViewEverydayOffersTile();
});

When(/^I select register for everday offers tile from home page$/, () => {
    homePage.selectRegisterForEverydayOffersTile();
});

When(/^I should see everyday offer technical error message in home page$/, () => {
    homePage.shouldSeeTechnicalErrorForEverydayOffer();
});

When(/^I should be opted-in for spending rewards$/, () => {
    homePage.verifyEverydayOfferOptedIn();
});

When(/^I should not be opted-in for spending rewards$/, () => {
    homePage.verifyEverydayOfferNotOptedIn();
});

When(/^I should not see Transfer and payment option in the action menu$/, () => {
    actionMenuPage.verifyTransferAndPaymentOptionIsNotVisible();
});

When(/^I should not see deposit cheque option in action menu$/, () => {
    actionMenuPage.verifyChequeDepositOptionIsNotVisible();
});

When(/^I should see Apply for other accounts button on no eligible accounts page$/, () => {
    noEligibleAccountsErrorPage.waitForPageLoad();
    noEligibleAccountsErrorPage.verifyApplyForAccountsButton();
});

When(/^I select Home button on no eligible accounts page$/, () => {
    noEligibleAccountsErrorPage.clickOnHomeButton();
});

When(/^I select Apply for other accounts button on no eligible accounts page$/, () => {
    noEligibleAccountsErrorPage.waitForPageLoad();
    noEligibleAccountsErrorPage.clickOnApplyForAccountsButton();
});

Then(/^I should see the no remitter eligible error message$/, () => {
    noEligibleAccountsErrorPage.waitForPageLoad();
});

Then(/^I should not see action menu of (.*) account$/, function (accountName) {
    const data = this.dataRepository.get();
    homePage.noActionMenuCbsLoan(data[accountName.replace(/ /g, '')]);
});

When(/^I select the view transaction option from the action menu of (.*) account$/, (type) => {
    homePage.selectActionMenuOfThatAccount(type);
    actionMenuPage.selectViewTransactionsOption();
});

When(/^I select the transfer and payment option from the action menu of (.*) account$/, (type) => {
    homePage.selectActionMenuOfThatAccount(type);
    actionMenuPage.selectTransferAndPaymentOption();
});

When(/^I select the view transaction option from the action menu of (.*) account from your accounts$/, (type) => {
    homePage.selectActionMenuOfThatAccount(type);
    actionMenuPage.selectViewTransactionsOption();
});

Then(/^I select the view payments due soon option from the action menu of (.*) on your accounts$/, (type) => {
    homePage.selectActionMenuOfThatAccount(type);
    actionMenuPage.verifyPendingPaymentOption();
    actionMenuPage.selectPendingPaymentOption();
});

When(/^I dismiss the host system down error message in the home page$/, () => {
    homePage.clickErrorMessage();
});

When(/^I dismiss the pending payment error message in the home page$/, () => {
    homePage.dismissPendingPaymentErrorMessage();
});

When(/^I should not see pending payment error message in the home page$/, () => {
    homePage.shouldNotSeePendingPaymentErrorMessage();
});

When(/^I should see the balance to be N\/A in the home page$/, () => {
    homePage.shouldSeeNABalance();
});

const getLocatorForCreditCard = function (type) {
    switch (type) {
    case 'credit':
        return 'creditCard';
    case 'internet lost connectivity credit':
        return 'internetConnectivityLostCreditCard';
    case 'declined credit':
        return 'declinedCreditCard';
    case 'payment on hold':
        return 'holdCreditCard';
    case 'pending payment':
        return 'pendingPaymentCreditCard';
    case 'instalment credit':
        return 'instalmentCreditCard';
    default:
        throw new Error(`Unknown credit card type: ${type}`);
    }
};

const amOnPayCreditCardPage = function (type) {
    homePage.selectPayAndTransfer();
    paymentHubMainPage.waitForPaymentHubPageLoad();
    paymentHubMainPage.selectPayAccount('recipient');
    const data = this.dataRepository.get();
    viewRecipientPage.selectAccount(data[getLocatorForCreditCard(type)]);
};

Then(/^I am on the payment hub - pay a (.*) card page$/, amOnPayCreditCardPage);

Then(/^I obtain the default primary account$/, () => {
    homePage.getPrimaryAccountName();
});

const amOnViewRecipientPage = function () {
    homePage.selectPayAndTransfer();
    paymentHubMainPage.waitForPaymentHubPageLoad();
    paymentHubMainPage.selectPayAccount('recipient');
    viewRecipientPage.waitForViewRecipientPageLoad();
};

When(/^I am on the view recipient page$/, () => {
    amOnViewRecipientPage();
});

When(/^I scroll down in view recipient page and select header back option$/, () => {
    ViewRecipientPage.scrollToEndOfPage();
    commonPage.clickBackButton();
});

When(/^I am on the view recipient page with defaultPrimary remitter account$/, () => {
    homePage.getPrimaryAccountName();
    amOnViewRecipientPage();
});

When(/^I should not see the defaultPrimary account for (.*) view recipient page$/, function (businessName) {
    const data = this.dataRepository.get();
    viewRecipientPage.shouldNotSeeSameRemitterAccount(data[businessName]);
});

When(/^I should not see the (.*) account in view recipient page$/, function (accountName) {
    const data = this.dataRepository.get();
    viewRecipientPage.shouldNotSeeSameRemitterAccount(data[accountName]);
});

When(/^I should not see spending rewards tile in the home page$/, () => {
    homePage.verifyEverydayOffersTileIsNotVisible();
});

When(/^I should see the top up isa option in the action menu$/, () => {
    actionMenuPage.shouldSeeTopUpIsa();
});

When(/^I select save the changes option from the (.*) account action menu$/, (accountName) => {
    homePage.selectCurrentAccount(AccountMatcher.getAccount(accountName));
    actionMenuPage.selectSaveTheChangeOption();
});

When(/^I select overdraft option from the (.*) account action menu$/, function (accountName) {
    const data = this.dataRepository.get();
    homePage.selectCurrentAccount(data[accountName]);
    actionMenuPage.selectOverdraftOption();
});

When(/^I select the view policy details option from the action menu of (.*) account from your accounts$/, (type) => {
    homePage.selectActionMenuOfThatAccount(type);
    actionMenuPage.selectViewPolicyDetailsOption();
});

Then(/^I should see alert warning on the (.*) account tile in home page$/, function (accountName) {
    const data = this.dataRepository.get();
    homePage.shouldSeeAlertOfSpecificAccount(data[accountName.replace(/ /g, '')]);
});

Then(/^I select the alert warning on the (.*) account tile in home page$/, function (account) {
    const data = this.dataRepository.get();
    homePage.selectAlertNotification(data[account]);
});

Then(/^I should see valid alert message for dormant account$/, () => {
    homePage.verifyAlertMessageForDormantAccount();
    alertPopUpPage.selectConfirmButton();
});

When(/^I choose reactivate ISA option from (.*) account tile in home page$/, function (account) {
    const data = this.dataRepository.get();
    homePage.selectAlertNotification(data[account]);
    alertPopUpPage.selectConfirmButton();
});

When(/^I navigate to (.*) account and choose not now option from alert notification in the home page$/, function (accountName) {
    const data = this.dataRepository.get();
    homePage.selectAlertNotification(data[accountName.replace(/ /g, '')]);
    alertPopUpPage.selectCancelButton();
});

Then(/^I am displayed no valid accounts error message on the account tile$/, () => {
    homePage.verifyInvalidAccountError();
});

Then(/^I am displayed no valid postcode error message on the account tile$/, () => {
    homePage.verifyInvalidPostcode();
});

Then(/^I should see non accessible by nga error message in the home page$/, () => {
    homePage.shouldSeeNonAccessibleByNgaErrorMessage();
});

Then(/^I see the Account Link lead placement in the account tile of (.*)$/, function (accountName) {
    const data = this.dataRepository.get();
    homePage.shouldSeeAccountLink(data[accountName.replace(/ /g, '')]);
});

Then(/^I should be displayed the following fields of (.*) account$/, function (accountName, mortgageFields) {
    const data = this.dataRepository.get();
    const FieldsToValidate = mortgageFields.hashes();
    homePage.verifyMortgageFields(data[accountName.replace(/ /g, '')], FieldsToValidate);
});

Then(/^I should not have access to Payments and Transfers$/, () => {
    actionMenuPage.verifyTransferAndPaymentOptionIsNotVisible();
});

Then(/^I should see switch business icon on home page$/, () => {
    homePage.verifySwitchBusinessIconIsPresent();
});

Then(/^I should not see switch business icon on home page$/, () => {
    homePage.verifySwitchBusinessIconIsNotPresent();
});

When(/^I select switch business option on home page$/, () => {
    homePage.selectSwitchBusinessIcon();
});

When(/^I verify (.*) name on the account tiles in the homepage$/, (business) => {
    homePage.verifyBusinessNameIsCorrectForAllAccounts(business);
});

When(/^I have (multiple|only 1) notification$/, (notification) => {
    homePage.waitForPageLoad();
    homePage.shouldSeeNotificationsOnTheNotificationTile(notification);
});

When(/^I select the notification placement outlining the number of notifications$/, () => {
    homePage.selectTheMultipleNotificationCentrePlacement();
});

When(/^The notification expands to show me a detail of all notifications$/, () => {
    homePage.shouldSeeAllAvailableNotification();
});

When(/^I click on one of the notifications$/, () => {
    homePage.selectOneOfTheAvailableNotifications();
});

When(/^I select the notification placement$/, () => {
    homePage.selectTheSingleNotificationCentrePlacement();
});

When(/^I must be taken to the relevant journey based on the link in the notification$/, () => {
    homePage.shouldBeOnTheNotificationJourney();
});

When(/^I navigate back to the home screen from the notification journey$/, () => {
    homePage.navigateToHomeFromNotification();
});

When(/^The (single|multiple) notification service and Arrangement service must be refreshed$/, (notification) => {
    homePage.shouldSeeNotificationsOnTheNotificationTile(notification);
});

When(/^I collapse the notification placement outlining the number of notifications$/, () => {
    homePage.selectCloseButton();
});

When(/^The notification placement should not be displayed$/, () => {
    homePage.shouldNotSeeTheNotificationCentrePlacement();
});

When(/^I navigate to security settings page from home page$/, NavigationTask.navigateToSecuritySettings);

When(/^I select security settings from settings menu/, () => {
    settingsPage.selectSecuritySettings();
});

When(/^I navigate to settings page from home page$/, () => {
    NavigationTask.navigateToSettings();
});

When(/^bottom navigation bar is displayed$/, () => {
    homePage.verifyBottomNavBar();
});

When(/^bottom navigation bar is not displayed$/, () => {
    homePage.shouldNotSeeBottomNavBar();
});

When(/^I do not see first account on home page$/, () => {
    homePage.verifyAccountTileByIndexIsNotVisible(0);
});

When(/^I am navigated to first account on home page$/, () => {
    homePage.verifyAccountTileByIndexIsVisible(0);
});

When(/^I scroll down to account tile (\d+)$/, (index) => {
    homePage.scrollToAccountTileByIndex(index);
});

Given(/^I select (.*) tab from bottom navigation bar$/, (tabName) => {
    homePage.selectTab(tabName);
});

Given(/^I navigate to pay and transfer from home page$/, () => {
    homePage.selectPayAndTransfer();
});

When(/^I should see the below options in the bottom nav/, (table) => {
    homePage.shouldSeeOptionsInBottomNavBar(table.hashes());
});

When(/^I verify selected tab on bottom navigation bar is highlighted/, (table) => {
    homePage.verifySelectedTabIsHighlighted(table.hashes());
});

Then(/^I verify (.*) tab is highlighted on bottom navigation bar/, (tabName) => {
    homePage.shouldSeeTabHighlighted(tabName);
});

Then(/^I should see can't make payment error page$/, () => {
    homePage.shouldSeeCantMakePaymentErrorMessage();
});

Then(/^I should see can't apply error page$/, () => {
    homePage.shouldSeeCantApplyErrorMessage();
});

Then(/^I should see not authorised to apply for products error page$/, () => {
    homePage.shouldSeeFeatureNotAvailable();
});

Then(/^I scroll down$/, () => {
    HomePage.swipeOnScreen();
});

Then(/^I should not see payments due soon option on the action menu of (.*) account$/, function (accountName) {
    const data = this.dataRepository.get();
    homePage.selectActionMenuOfSpecificAccount(data[accountName.replace(/ /g, '')]);
    actionMenuPage.verifyNoPendingPaymentOption();
    actionMenuPage.closeActionMenu();
});

Then(/^I should be displayed (.*) for (.*) account$/, (field, type) => {
    homePage.validateAlertRepossessedMortgage(field, type);
});

When(/^I verify stay on app functionality for device back button on each tab$/, (table) => {
    for (const option of table.hashes()) {
        homePage.selectTab(option.optionsInBottomNav);
        browser.pause(1000); // wait for tab to load
        HomePage.pressAndroidBackKey();
        commonPage.selectWinBackStayButton();
    }
});

When(/^I press device back button$/, () => {
    HomePage.pressAndroidBackKey();
});

When(/^I navigate to home page$/, () => {
    homePage.navigateToHomePage();
});

Then(/^I should see the error banner on home page$/, () => {
    homePage.verifyHomeScreenErrorBanner();
});

Then(/my (.*) pension tile should not appear in my account list$/, function (accountName) {
    const data = this.dataRepository.get();
    homePage.verifyNoScottishWidowsAccountTiles(data[accountName.replace(/ /g, '')]);
});

Given(/^I select (.*) option from the action menu$/, (option) => {
    actionMenuPage.selectGivenActionMenuOption(option);
});

Then(/^I should see external bank name (.*) on action menu header$/, function (bankName) {
    const data = this.dataRepository.get();
    actionMenuPage.verifyExternalBankNameOnActionMenuHeader(data[bankName]);
});

Then(/^I should not see external bank (.*) with type (Current|Credit|Savings|Error|Expired|.*) on home page$/, function (bankName, accountType) {
    const data = this.dataRepository.get();
    homePage.externalProviderBankRemoved(data[bankName], accountType);
});

Then(/^I should see below options on external bank account action menu pop up$/, (table) => {
    const tableData = table.raw();
    actionMenuPage.verifyExternalBankAccountActionMenuPopupOptions(tableData);
});

Then(/^I should see close option in the action menu$/, () => {
    actionMenuPage.verifyCloseButtonPresent();
});

When(/^I select the (.*) (Current|Credit|Savings) external account tile$/, function (bankName, accountType) {
    const data = this.dataRepository.get();
    homePage.selectExternalAccountTile(data[bankName], accountType);
});

Then(/^I navigate to external bank (.*) error tile$/, function (bankName) {
    const data = this.dataRepository.get();
    homePage.navigateToExternalBankAccountErrorTile(data[bankName]);
});

When(/^I navigate to external bank (.*) (Current|Credit|Savings) account tile$/, function (bankName, accountType) {
    const data = this.dataRepository.get();
    homePage.navigateToExternalBankAccountTile(data[bankName], accountType);
});

Then(/^I am (shown|not shown) (action menu|renew account sharing|settings) button on external account tile for (.*)$/, function (isShown, buttonName, bankName) {
    const data = this.dataRepository.get();
    switch (buttonName.toLowerCase()) {
    case 'renew account sharing':
        homePage.shouldSeeRenewConsentButtonOnExternalTile(data[bankName], isShown === 'shown');
        break;
    default:
        throw new Error(`Given button ${buttonName} not present`);
    }
});

When(/^I select lost option on the lost and stolen page$/, () => {
    lostAndStolenCardPage.selectLostOption();
});

When(/^I select cards option on the lost and stolen page$/, () => {
    lostAndStolenCardPage.selectCardsOption();
});

When(/^I select replace cards and pin options on the replacement cards and pin page$/, () => {
    replacementCardAndPinPage.selectCardsAndPinOption();
});

When(/^I select to continue on the lost and stolen page$/, () => {
    lostAndStolenCardPage.selectContinue();
});

When(/^I should be on the card replace and pin details confirmation page$/, () => {
    lostAndStolenCardPage.verifyTitle();
});

When(/^I should be shown below fields on the card replace and pin details confirmation page$/, (table) => {
    const tableData = table.hashes();
    lostAndStolenCardPage.validateConfirmationOptions(tableData);
});

When(/^I select "(.*)" option in the action menu of creditCard account$/, (creditCardOption) => {
    if (creditCardOption === 'Card Management') {
        actionMenuPage.selectCardManagement();
    }
});

Then(/^I have added my external accounts more than 24 hours ago$/, () => {
    homePage.verifyTitle();
});

When(/^I should see normal placement in between the header and my first account tile$/, () => {
    homePage.verifyPriorityMessageVisibility(true);
});

Then(/^I should see a dismiss button in the top right corner of the placement$/, () => {
    homePage.shouldSeeDismissPriority();
});

Then(/^I select the priority dismiss button$/, () => {
    homePage.selectDismissPriority();
});

Then(/^the placement should not be displayed in the top of the home page$/, () => {
    homePage.verifyPriorityMessageVisibility(false);
});

When(/^I navigate away from the home page and come back$/, () => {
    homePage.selectTab('more');
    homePage.navigateToHomePage();
});

Then(/^I should see an upgrade now header for group B call to action$/, () => {
    homePage.shouldSeeUpdateNow();
});

When(/^I should see Call to action placement in between the header and my first account tile$/, () => {
    homePage.shouldSeeGroupBPriorityMessage();
});

When(/^the app upgrade message should disappear for that session$/, () => {
    homePage.shouldNotSeeCallActionPriority();
});

Then(/^I should be greeted with time of day with users first name/, () => {
    homePage.verifyGreetingMessages();
});

Then(/^Home text in the header should be replaced with a welcome message/, () => {
    homePage.shouldSeeWelcomeMessage();
});

Then(/I should not see welcome message again/, () => {
    homePage.shouldSeeHomeMessageInSecondLogin();
});

When(/I navigate to (.*) and return back to (.*) screen$/, (support, home) => {
    homePage.selectTab(support);
    homePage.selectTab(home);
    homePage.verifyHomeMessage();
});

When(/^I should be on the native register VoiceID page$/, () => {
    homePage.shouldSeeVoiceIdScreen();
});

When(/^I validate VoiceID page$/, () => {
    homePage.validateVoiceIdScreen();
});

When(/^I select Voice ID account tile$/, () => {
    homePage.selectAsmVoiceIdTile();
});

When(/^I select Voice ID register button on the page$/, () => {
    homePage.selectRegisterVoiceID();
    if (DeviceHelper.isAndroid()) {
        requestPermissionPage.allowNativeAppCall();
    }
});

When(/^I select the bottom nav or not now button$/, () => {
    homePage.selectBottomNavOrNotNow();
});

When(/^The priority message should be invisible when scrolling down and will display back while scroll up the page$/, () => {
    homePage.shouldNotSeePriorityAfterScrolldown();
    homePage.shouldSeePriorityAfterScrollUp();
});

When(/^I should not see the placement again within the same session$/, () => {
    homePage.selectTab('more');
    homePage.navigateToHomePage();
    homePage.verifyPriorityMessageVisibility(false);
});

When(/^there should not be any back button on the page$/, () => {
    commonPage.shouldNotSeeHeaderBackButton();
});

When(/^I navigate and validate that Balance or Money transfer tile is displayed$/,
    () => {
        homePage.verifyBalanceOrMoneyTransferTile();
    });

When(/I select (viewSpendingInsights|viewUpcomingPayment|viewPaymentsDueSoon) option$/, (option) => {
    actionMenuPage.selectPendingPaymentOption(option);
});

When(/^I select the Help or Support option from the menu$/, () => {
    actionMenuPage.selectReplacementCardAndPinOption();
});

Then(/^I should be on the PDF statements webview page for creditCard account$/, () => {
    pdfStatementsPage.waitForPageLoad();
});

Then(/^I should be able to view the PDF Statements for current year$/, () => {
    pdfStatementsPage.selectViewPdfButton();
});

When(/^I select freeze gambling option in Freeze card transactions page$/, () => {
    freezeCardTransactions.selectFreezeGambling();
});

When(/^I should be on Ready to Freeze Gambling page$/, () => {
    freezeCardTransactions.waitForReadyToFreezePage();
});

Then(/^I select Freeze button in Ready to Freeze Gambling page$/, () => {
    freezeCardTransactions.selectFreezeButtonGambling();
});

Then(/^I select Cancel button in Almost there popup in Freeze Gambling page$/, () => {
    freezeCardTransactions.selectCancelFreezeButton();
});

Then(/^I navigate to the "([^"]*)" account in the home page$/, (accountName) => {
    homePage.navigateToAccount(accountName);
});

Then(/^I should see the nick name on the "([^"]*)" account tile in the home page$/, (accountName) => {
    homePage.shouldSeeAccountName(accountName);
});

Then(/^I should not see the Remaining Overdraft field in the account tile$/, () => {
    homePage.shouldNotSeeRemainingOverdraft();
});

Then(/^new account name (.*) is displayed$/, (newAccountName) => {
    homePage.checkAccountNameVisibility(newAccountName);
});

Then(/^I reset the account name from (.*) to its original name of (.*)$/, (newAccountName, originalAccountName) => {
    homePage.selectActionMenuOfSpecificAccount(newAccountName);
    actionMenuPage.selectRenameAccountOption();
    renameAccountPage.enterAccountName(AccountMatcher.getAccount(originalAccountName.replace(/ /g, '')));
    renameAccountPage.selectRenameButton();
    renameAccountPage.shouldSeeConfirmationBoxEnabled();
    renameAccountPage.selectBackToYourAccountsButton();
});

Then(/^I have to select on the search icon on home page$/, () => {
    searchLandingPage.selectSearchIcon();
});

Then(/^I select the search icon on home page$/, () => {
    searchLandingPage.selectSearchIcon();
});

Then(/^I should see search icon on the home page$/, () => {
    searchLandingPage.shouldSeeSearchBar();
});

When(/^I navigate and validate the quickLinks in the (.*) account in below table in the home page$/,
    (accountName, table) => {
        const tableData = table.hashes();
        homePage.navigateAndValidateOptionInAccountTile(AccountMatcher.getAccount(accountName.replace(/ /g, '')), tableData);
    });

When(/^I select sendBankDetailsOption option$/, () => {
    actionMenuPage.swipeAndSelectSendBankDetailsOption();
});

When(/^I should see the below options in the account overview screen/, (table) => {
    const tableData = table.hashes();
    homePage.shouldSeeOptionsInAccountOverviewScreen(tableData);
});

When(/^I select the (.*) quicklink of (.*) account$/, (quickLink, accountName) => {
    homePage.selectQuicklinkOfSpecificAccount(quickLink, AccountMatcher.getAccount(accountName.replace(/ /g, '')));
});

Then(/^I should be displayed upcoming payments page$/, () => {
    accountStatementPage.displayUpcomingPaymentWebViewPage('upcomingPayments');
});

When(/^I tap on back button$/, () => {
    commonPage.clickBackButton();
});

Then(/^I am unable to see Send bank details option in the action menu$/, () => {
    actionMenuPage.shouldNotSeeSendBankDetailsOption();
});

Then(/^I should be displayed Pay & Transfer page$/, () => {
    paymentHubMainPage.waitForPaymentHubPageLoad();
});
