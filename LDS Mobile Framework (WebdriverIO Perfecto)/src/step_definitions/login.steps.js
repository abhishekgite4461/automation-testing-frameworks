const {When, Then} = require('cucumber');

const WelcomePage = require('../pages/welcome.page');
const LoginPage = require('../pages/login.page');
const MemorableInformationPage = require('../pages/memorableInformation.page');
const ErrorMessagePage = require('../pages/error.page');
const ForgottenLoginDetailsPage = require('../pages/forgottenLoginDetails.page');
const DmcDetailsPage = require('../pages/dmcDetails.page');
const FscsPage = require('../pages/fscs.page');
const FscsMobileBankingPage = require('../pages/fscsMobileBanking.page');
const ReEnterPasswordPage = require('../pages/reEnterPassword.page');
const ReEnterMemorableInformationPage = require('../pages/reEnterMemorableInformation.page');
const CwaPage = require('../pages/cwa.page');
const FingerprintLoginPage = require('../pages/fingerprint/fingerprintLogin.page');
const CardReaderRespondPage = require('../pages/enrolmentCardReaderRespond.page');
const CardReaderIdentifyPage = require('../pages/enrolmentCardReaderIdentify.page');
const DataPrivacyPage = require('../pages/dataPrivacy.page');
const HomePage = require('../pages/home.page');

const incorrectMI = 'incorre';

const forgottenLoginDetailsPage = new ForgottenLoginDetailsPage();
const reEnterPasswordPage = new ReEnterPasswordPage();
const loginPage = new LoginPage();
const fscsPage = new FscsPage();
const errorPage = new ErrorMessagePage();
const fscsMobileBankingPage = new FscsMobileBankingPage();
const memorableInformationPage = new MemorableInformationPage();
const welcomePage = new WelcomePage();
const reEnterMemorableInformationPage = new ReEnterMemorableInformationPage();
const cwaPage = new CwaPage();
const dmcDetailsPage = new DmcDetailsPage();
const fingerprintLoginPage = new FingerprintLoginPage();
const cardReaderRespondPage = new CardReaderRespondPage();
const cardReaderIdentifyPage = new CardReaderIdentifyPage();
const dataPrivacyPage = new DataPrivacyPage();

Then(/^I should be on the Login page$/, () => {
    loginPage.isPageVisible();
});

Then(/^I should be on the welcome page$/, () => {
    welcomePage.waitForPageLoad();
});

const canSeeThreeTextBox = function () {
    memorableInformationPage.validatePageLoad();
    memorableInformationPage
        .canSeeThreeTextboxForMemorableInformation();
};

const verifyUniqueMIHeaders = function () {
    memorableInformationPage.validatePageLoad();
    memorableInformationPage
        .verifyUniqueMIHeaders();
};

const selectFSCSTile = function () {
    memorableInformationPage.validatePageLoad();
    fscsMobileBankingPage.closeAllTabs();
    memorableInformationPage.selectFscsTitle();
};

const enterUsernamePassword = function () {
    const data = this.dataRepository.get();
    welcomePage.navigateToLoginScreen();
    loginPage.waitForPageLoad();
    loginPage.login(data.username, data.password);
};

When(/^I login with my username and password$/, function () {
    const data = this.dataRepository.get();
    loginPage.waitForPageLoad();
    loginPage.login(data.username, data.password);
});

When(/^I login with an incorrect username "(.*)"$/, function (username) {
    const data = this.dataRepository.get();
    loginPage.waitForPageLoad();
    loginPage.login(username, data.password);
});

When(/^I login with an incorrect password "(.*)"$/, function (password) {
    const data = this.dataRepository.get();
    loginPage.waitForPageLoad();
    loginPage.login(data.username, password);
});

When(/^I enter (correct|incorrect|2 characters of) memorable information in IB Registration confirm Activation code page$/, function (type) {
    memorableInformationPage.validateIbRegMiPageLoad();
    const data = this.dataRepository.get();
    if (type === 'correct') {
        memorableInformationPage.enterMemorableInformation(data.memorableInformation, 3);
    } else if (type === 'incorrect') {
        memorableInformationPage.enterMemorableInformation(incorrectMI, 3);
    } else if (type === '2 characters of') {
        memorableInformationPage.enterMemorableInformation(data.memorableInformation, 2);
    }
});

When(/^I accept interstitial during logon$/, () => {
    memorableInformationPage.proceedToPage(new HomePage());
});

When(/^I should see contact us option on the IB Registration Activation Code enter mi page$/, () => {
    memorableInformationPage.validateIbRegMiPageLoad();
    memorableInformationPage.shouldSeeContactUsButton();
});

Then(/^I should see an incorrect username or password error message$/, () => {
    loginPage.waitForPageLoad();
    loginPage.canSeeErrorMessage();
});

Then(/^I should be on enter MI page$/, () => {
    memorableInformationPage.validatePageLoad();
});

Then(/^I should be on the FP Authentication page$/, () => {
    fingerprintLoginPage.waitForPageLoad();
});

Then(/^I select the contact us option from the error page$/, () => {
    errorPage.selectContactUs();
});

Then(/^I should be on revoked error page$/, () => {
    errorPage.waitForPageLoad();
    errorPage.canSeeRevokedUserErrorMessage();
});

Then(/^I should be on revoked error page of SCA journey$/, () => {
    errorPage.waitForPageLoad();
    errorPage.isSCARevokedErrorMessageDisplayed();
});

Then(/^I should be shown an error page logged out with error message$/, () => {
    errorPage.waitForPageLoad();
    errorPage.shouldSeeHardTokenUserErrorMessage();
});

Then(/^I should be shown an error page logged out with error message <HC-322>$/, () => {
    errorPage.waitForPageLoad();
    errorPage.shouldSeeSoftTokenUserErrorMessage();
});

Then(/^I should be on suspended error page$/, () => {
    errorPage.waitForPageLoad();
    errorPage.canSeeSuspendedUserErrorMessage();
});

When(/^I login with an incorrect password 5 times$/, function () {
    const data = this.dataRepository.get();
    loginPage.waitForPageLoad();
    for (let i = 0; i < 5; i++) {
        loginPage.login(data.username, 'Incorrect');
    }
});

When(/^I enter the (username|password) "(.*)"$/, (type, inputData) => {
    loginPage.waitForPageLoad();
    if (type === 'username') {
        loginPage.enterUsername(inputData);
    } else {
        loginPage.enterPassword(inputData);
    }
});

When(/^I (copy|paste) the content in the "(.*)" field$/, (type, inputField) => {
    loginPage.waitForPageLoad();
    if (type === 'copy') {
        loginPage.copyContentFromTextField(inputField);
    } else {
        loginPage.pasteContentInTextField(inputField);
    }
});

When(/^I should see the username field filled with "(.*)"$/, (text) => {
    loginPage.checkIfTextIsPresent(text, 'username');
});

When(/^I should not be able to see copy option after longpress in the password field$/, () => {
    loginPage.copyOptionNotAvailableInPasswordField();
});

When(/^I should be able to paste option in the userId field$/, () => {
    loginPage.pasteOptionAvailableInUserIdField();
});

When(/^I should see the password field populated with (.*) characters$/, (noOfCharacters) => {
    loginPage.checkNumberOfPasswordCharacters(noOfCharacters);
});

When(/^I enter valid (username|password)$/, function (type) {
    const data = this.dataRepository.get();
    loginPage.waitForPageLoad();
    if (type === 'username') {
        loginPage.enterUsername(data.username);
    } else {
        loginPage.enterPassword(data.password);
    }
});

Then(/^I should see the continue button is disabled$/, () => {
    loginPage.waitForPageLoad();
    loginPage.shouldSeeContinueButtonDisabled();
});

When(/^I select the pre-auth header menu in the login page$/, () => {
    loginPage.waitForPageLoad();
    loginPage.openPreAuthMenu();
});

When(/^I select the forget your login link$/, () => {
    loginPage.waitForPageLoad();
    fscsMobileBankingPage.closeAllTabs();
    loginPage.openForgotUserNameWebPage();
});

When(/^I enter my username and password$/, function () {
    const data = this.dataRepository.get();
    loginPage.waitForPageLoad();
    loginPage.enterUsernameAndPassword(data.username, data.password);
});

Then(/^I should see the password field is masked$/, () => {
    loginPage.waitForPageLoad();
    loginPage.passwordFieldShouldBeMasked();
});

Then(/^I select continue button on login screen$/, () => {
    loginPage.selectContinueButton();
});

Then(/^I should be on inactive error page$/, () => {
    errorPage.waitForPageLoad();
    errorPage.canSeeInactiveUserErrorMessage();
});

Then(/^I should see forget username page in the mobile browser$/, () => {
    forgottenLoginDetailsPage.waitForPageLoad();
});

Then(/^I select close button in the mobile browser$/, () => {
    dmcDetailsPage.waitForDmcPageLoad();
    dmcDetailsPage.selectWebCloseButton();
});

Then(/^I select FSCS tile in MI page$/, selectFSCSTile);

Then(/^I select leave app in MI page$/, () => {
    fscsPage.waitForPageLoad();
    fscsPage.selectLeaveApp();
});

Then(/^I should see mobile banking page$/, () => {
    fscsMobileBankingPage.waitForPageLoad();
});

Then(/^I click on Logon to Mobile Banking button in the error page$/, () => {
    errorPage.waitForPageLoad();
    errorPage.clickLogonMobileBanking();
});

Then(/^I should see a not registered to IB error message in login page$/, () => {
    loginPage.waitForPageLoad();
    loginPage.canSeeMandateLessErrorMessage();
});

Then(/^I should see a compromised password error page$/, () => {
    errorPage.waitForPageLoad();
    errorPage.canSeeCompromisedPasswordErrorMessage();
});

Then(/^I should be on partially registered error page$/, () => {
    errorPage.waitForPageLoad();
    errorPage.canSeePartiallyRegisteredErrorMessage();
});

Then(/^I should be restricted from entering more than (.*) characters in (username|password) text box$/, (noOfCharacters, type) => {
    loginPage.waitForPageLoad();
    if (type === 'username') {
        loginPage.checkNumberOfUsernameCharacters(noOfCharacters);
    } else {
        loginPage.checkNumberOfPasswordCharacters(noOfCharacters);
    }
});

Then(/^I should be on the two factor auth error page$/, () => {
    errorPage.waitForPageLoad();
    errorPage.canSeeTwoFactorAuthErrorMessage();
});

Then(/^I should see the telephone number displayed in the error page$/, () => {
    errorPage.waitForPageLoad();
    errorPage.canSeeTelephoneNumber();
});

Then(/^I click on Register for Internet Banking in login page$/, () => {
    loginPage.waitForPageLoad();
    loginPage.selectRegisterForIB();
});

Then(/^I long press the username field$/, () => {
    loginPage.waitForPageLoad();
    loginPage.longPressUsername();
});

When(/^I navigate to the login screen$/, () => {
    welcomePage.navigateToLoginScreen();
    loginPage.waitForPageLoad();
});

Then(/^I should see 3 text boxes to enter the MI and their headers are unique$/, () => {
    canSeeThreeTextBox();
    verifyUniqueMIHeaders();
});

Then(/^I should see incorrect (MI|password) error message in re-enter MI page$/, (type) => {
    reEnterMemorableInformationPage.waitForPageLoad();
    reEnterMemorableInformationPage.verifyAndCloseErrorMsg(type);
});

When(/^I enter incorrect MI in re-enter MI page$/, () => {
    reEnterMemorableInformationPage.waitForPageLoad();
    reEnterMemorableInformationPage.reEnterMemorableInformation(incorrectMI);
});

Then(/^I enter correct MI in re-enter MI page$/, function () {
    reEnterMemorableInformationPage.waitForPageLoad();
    const data = this.dataRepository.get();
    reEnterMemorableInformationPage.reEnterMemorableInformation(data.memorableInformation);
});

Then(/^I enter incorrect password in confirm password page$/, () => {
    reEnterPasswordPage.waitForPageLoad();
    reEnterPasswordPage.reEnterPassword('Incorrect');
});

Then(/^I should be on the confirm password page$/, () => {
    reEnterPasswordPage.waitForPageLoad();
});
When(/^I enter correct password in confirm password page$/, function () {
    reEnterPasswordPage.waitForPageLoad();
    const data = this.dataRepository.get();
    reEnterPasswordPage.reEnterPassword(data.password);
});

Then(/^I should be on the re-enter MI page$/, () => {
    reEnterMemorableInformationPage.shouldSeeReenterMIPageTitle();
});

Then(/^I select back button in cwa page$/, () => {
    cwaPage.waitForPageLoad();
    cwaPage.backToApplication();
});

When(/^I select on the forget your login link in MI page$/, () => {
    memorableInformationPage.validatePageLoad();
    memorableInformationPage.clickForgotLogonDetails();
});

When(/^I navigate to mi page$/, () => {
    memorableInformationPage.validatePageLoad();
});

Then(/^I should see the user inaccessible error for token user$/, () => {
    errorPage.waitForPageLoad();
    errorPage.canSeeInaccessibleErrorMessage();
});

When(/^I enter the "(.*)" in the create MI page$/, (invalidMemorableInformation) => {
    memorableInformationPage.validatePageLoad();
    memorableInformationPage.enterMemorableInformation(invalidMemorableInformation);
});

When(/^I select on the forget your login link in reenter MI page$/, () => {
    reEnterMemorableInformationPage.waitForPageLoad();
    reEnterMemorableInformationPage.clickForgotLogonDetails();
});

Then(/^I should (not see|see) the cms tile in the MI page$/, (type) => {
    memorableInformationPage.validatePageLoad();
    if (type === 'not see') {
        memorableInformationPage.verifyCmsTile(false);
    } else {
        memorableInformationPage.verifyCmsTile(true);
    }
});

Then(/^I should (not see|see) the cms tile in the re-enter password page$/, (type) => {
    reEnterPasswordPage.waitForPageLoad();
    if (type === 'not see') {
        reEnterPasswordPage.verifyCmsTile(false);
    } else {
        reEnterPasswordPage.verifyCmsTile(true);
    }
});

Then(/^I should (not see|see) the fscs tile in the re-enter password page$/, (type) => {
    reEnterPasswordPage.waitForPageLoad();
    if (type === 'not see') {
        reEnterPasswordPage.verifyFscsTile(false);
    } else {
        reEnterPasswordPage.verifyFscsTile(true);
    }
});

Then(/^I re-enter the password$/, function () {
    const data = this.dataRepository.get();
    reEnterPasswordPage.waitForPageLoad();
    reEnterPasswordPage.reEnterPassword(data.password);
});

Then(/^I choose the stay option on the fscs popup$/, () => {
    fscsPage.waitForPageLoad();
    fscsPage.selectStayButton();
});

Then(/^I choose the leave option on the fscs popup$/, () => {
    fscsPage.waitForPageLoad();
    fscsPage.selectLeaveApp();
});

Then(/^I should see forgotten your logon details mobile browser journey page$/, () => {
    forgottenLoginDetailsPage.waitForPageLoad();
});

Then(/^I should verify that the MI headers for the 3 text boxes are unique$/, verifyUniqueMIHeaders);

Then(/^I should see the MI information is masked$/, () => {
    memorableInformationPage.validatePageLoad();
    memorableInformationPage
        .verifyMIFieldIsMasked();
});

Then(/^I should see fscs page in the mobile browser$/, () => {
    fscsMobileBankingPage.waitForPageLoad();
});

Then(/^I enter incorrect MI for (.*) times consecutively$/, (num) => {
    memorableInformationPage.validatePageLoad();
    memorableInformationPage.enterMemorableInformation(incorrectMI, 3);
    for (let i = 0; i < num; i++) {
        reEnterMemorableInformationPage.waitForPageLoad();
        reEnterMemorableInformationPage.verifyAndCloseErrorMsg('MI');
    }
});

Then(/^I select the fscs tile on the re-enter MI page$/, () => {
    reEnterMemorableInformationPage.waitForPageLoad();
    reEnterMemorableInformationPage.selectFscsTitle();
});

Then(/^I should see Error Message on the Login page$/, () => {
    loginPage.waitForPageLoad();
    loginPage.validateErrorMessage();
});

Then(/^I should not see Register for Internet Banking button on the Login page$/, () => {
    loginPage.waitForPageLoad();
    loginPage.canSeeRegisterForIB(false);
});

Then(/^I clear the username and password fields$/, () => {
    loginPage.waitForPageLoad();
    loginPage.clearUsernamePassword();
});

Then(/^I dismiss the error message$/, () => {
    loginPage.closeErrorMessage();
});

When(/^I provide my correct username, password and MI$/, function () {
    const data = this.dataRepository.get();
    welcomePage.navigateToLoginScreen();
    loginPage.waitForPageLoad();
    loginPage.login(data.username, data.password);
    memorableInformationPage.validatePageLoad();
    memorableInformationPage.enterMemorableInformation(data.memorableInformation, 3);
});

When(/^I provide my correct username and password$/, enterUsernamePassword);

Then(/^I select the tooltip link on the MI page$/, () => {
    memorableInformationPage.validatePageLoad();
    memorableInformationPage.selectTooltiplink();
});

Then(/^I should be on the MI tooltip page$/, () => {
    memorableInformationPage.shouldSeeTooltipPopup();
});

Then(/^I select the close option on tool tip page$/, () => {
    memorableInformationPage.closeToolTip();
});

Then(/^I select Ok button in express logon unavailable warning message$/, () => {
    memorableInformationPage.shouldSeeExpressLogonUnavailablePopUp();
    memorableInformationPage.selectOkButton();
});

Then(/^I should not see confirm password page$/, () => {
    reEnterPasswordPage.shouldNotSeeReEnterPasswordScreen();
});
Then(/^I should see an error message for hard token user$/, () => {
    loginPage.waitForPageLoad();
    loginPage.hardTokenErrorMessage();
});
Then(/^I should be shown an error page logged out with error message for maximum number of devices$/, () => {
    errorPage.waitForPageLoad();
    errorPage.shouldSeeMaxDeviceEnrolErrorMessage();
});
When(/^I navigated back on card reader respond screen$/, () => {
    cardReaderRespondPage.waitForPageLoad();
});

// ******************************* Add Business specific step defs below this ***********************************//
When(/^I am on card reader respond screen$/, function () {
    const data = this.dataRepository.get();
    welcomePage.navigateToLoginScreen();
    loginPage.waitForPageLoad();
    loginPage.login(data.username, data.password);
    memorableInformationPage.validatePageLoad();
    memorableInformationPage.enterMemorableInformation(data.memorableInformation, 3);
    cardReaderRespondPage.waitForPageLoad();
});
Then(/^I am on card reader identify screen$/, function () {
    const data = this.dataRepository.get();
    welcomePage.navigateToLoginScreen();
    loginPage.waitForPageLoad();
    loginPage.login(data.username, data.password);
    cardReaderIdentifyPage.waitForPageLoad();
});

When(/^I am at first welcome screen$/, () => {
    welcomePage.waitForPageLoad();
});

When(/^I choose an option to continue to the next screen$/, () => {
    welcomePage.moveToRegisterScreen();
});

When(/^I choose an option to view data privacy screen$/, () => {
    welcomePage.selectDataPrivacy();
});

Then(/^Data privacy screen should be displayed$/, () => {
    welcomePage.waitForDataPrivacy();
});

When(/^I choose to view data privacy notice$/, () => {
    welcomePage.selectDataPrivacyNotice();
});

When(/^I choose to view the cookies policy$/, () => {
    welcomePage.selectCookiesPolicy();
});

Then(/^I should be shown the screen with following welcome screen options$/, (table) => {
    const tableData = table.hashes();
    welcomePage.validateWelcomeScreenOptions(tableData);
});

Then(/^I should be shown the screen with following welcome benefits screen options$/, (table) => {
    const tableData = table.hashes();
    welcomePage.validateWelcomeBenefitsScreenOptions(tableData);
});

When(/^I choose register for online banking option$/, () => {
    welcomePage.selectRegisterForOnlineBanking();
});

When(/^I choose to view more information about the card reader$/, () => {
    welcomePage.selectMoreInformation();
});

When(/^I choose why you need card option$/, () => {
    welcomePage.selectWhyYouNeedCard();
});

When(/^I should be displayed an overlay on welcome screen$/, () => {
    welcomePage.waitForOverlay();
});

Then(/^I should be displayed data privacy site$/, () => {
    dataPrivacyPage.waitForPageLoad();
});

Then(/^I should (not see|see) the password special characters restriction message in the login page$/, (type) => {
    if (type === 'not see') {
        loginPage.specialCharactersRestrictionMessage(false);
        loginPage.updateYourPasswordLink(false);
    } else {
        loginPage.specialCharactersRestrictionMessage(true);
        loginPage.updateYourPasswordLink(true);
    }
});

Then(/^I should be on the maximum device enrolled error page$/, () => {
    errorPage.waitForPageLoad();
    errorPage.shouldSeeMaxDeviceEnrolErrorMessage();
});
