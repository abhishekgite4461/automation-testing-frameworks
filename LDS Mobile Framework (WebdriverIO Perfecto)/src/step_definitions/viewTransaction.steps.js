const {When, Then} = require('cucumber');
const ViewTransactionPage = require('../pages/viewTransaction.page');
const ActionMenuPage = require('../pages/actionMenu.page');

const viewTransactionPage = new ViewTransactionPage();
const actionMenuPage = new ActionMenuPage();

When(/^I should see transactions for recent transfer$/, () => {
    viewTransactionPage.waitForPageLoad();
    viewTransactionPage.verifyTransactionExist();
});

Then(/^I should see view transaction page$/, () => {
    viewTransactionPage.waitForPageLoad();
});

When(/^I select pay a credit card option in the action menu$/, () => {
    actionMenuPage.waitForPageLoad();
    actionMenuPage.selectPayCreditCard();
});

When(/^I click on the action menu of the credit card on your account page$/, () => {
    viewTransactionPage.waitForPageLoad();
    viewTransactionPage.selectActionMenu();
});

When(/^I click on pending payment from action menu$/, () => {
    actionMenuPage.waitForPageLoad();
    actionMenuPage.clickViewPendingPayment();
});
