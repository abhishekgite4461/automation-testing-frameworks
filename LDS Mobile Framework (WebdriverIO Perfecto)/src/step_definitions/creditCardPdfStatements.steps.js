const {When} = require('cucumber');

const CreditCardPdfStatementWebViewPage = require('../pages/creditCardPdfStatementsWebViewPage.page');

const creditCardPdfStatementWebViewPage = new CreditCardPdfStatementWebViewPage();

When(/^I should be able to view credit card PDF statements page$/, () => {
    creditCardPdfStatementWebViewPage.waitForPageLoad();
});
