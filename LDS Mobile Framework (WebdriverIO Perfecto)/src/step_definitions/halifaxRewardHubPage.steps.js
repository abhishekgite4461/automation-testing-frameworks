const {Then} = require('cucumber');
const HalifaxRewardHubPage = require('../pages/halifaxRewardHub.page');
const CommonPage = require('../pages/common.page');

const halifaxRewardHubPage = new HalifaxRewardHubPage();
const commonPage = new CommonPage();

Then(/^I should be on the halifax reward hub page$/, () => {
    halifaxRewardHubPage.waitForPageLoad();
});

Then(/^I should see (.*) tile with (.*) status$/, (reward, state) => {
    switch (reward) {
    case 'cashbackExtra':
        halifaxRewardHubPage.validateCashBackExtra(state);
        break;
    case 'rewardsExtra':
        halifaxRewardHubPage.validateRewardsExtra(state);
        break;
    case 'mortgagePrizeDraw':
        halifaxRewardHubPage.validateMortgagePrizeDraw(state);
        break;
    case 'saversPrizeDraw':
        halifaxRewardHubPage.validateSaversPrizeDraw(state);
        break;
    default:
        throw new Error('unrecognised reward menu option');
    }
});

Then(/^I select call to action link from (.*) tile$/, (reward) => {
    halifaxRewardHubPage.selectCallToAction(reward);
});

Then(/^I should be on (.*) web journey$/, (reward) => {
    halifaxRewardHubPage.validateRewardsWebJourney(reward);
});

Then(/^I select back button from cwa page$/, () => {
    commonPage.selectHeaderBackButton();
});
