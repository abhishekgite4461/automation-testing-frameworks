const {When, Then} = require('cucumber');
const SecureAndProtectedPage = require('../../pages/accountAggregation/accAggSecureAndProtected.page');

const secureAndProtected = new SecureAndProtectedPage();

Then(/^I should see details for secure and protected$/, () => {
    secureAndProtected.shouldSeeSecureAndProtectedInformation();
});

Then(/^I can proceed to the set up in 3 steps screen$/, () => {
    secureAndProtected.shouldSeeNextButton();
});

When(/^I proceed to the set up in 3 steps screen$/, () => {
    secureAndProtected.selectNextButton();
});
