const {Then} = require('cucumber');
const TransactionsPage = require('../../pages/accountAggregation/accAggTransactions.page');

const transactionsPage = new TransactionsPage();

const validateTransactionsPage = function () {
    transactionsPage.waitForPageLoad();
};

Then(/^I should see the transactions for the externally added account$/, () => {
    validateTransactionsPage();
});

Then(/^I select the (success|failure) deep link for renewing consent$/, (isSuccess) => {
    transactionsPage.selectDeepLink(isSuccess === 'success');
});

Then(/^I should see the logged off error screen$/, () => {
    transactionsPage.shouldSeeLoggedOffError();
});
