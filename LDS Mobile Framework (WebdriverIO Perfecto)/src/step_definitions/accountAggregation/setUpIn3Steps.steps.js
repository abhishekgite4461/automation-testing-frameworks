const {Then} = require('cucumber');
const SetUpIn3StepsPage = require('../../pages/accountAggregation/accAggSetUpIn3Steps.page');

const setUpIn3Steps = new SetUpIn3StepsPage();

Then(/^I should see details for set up in 3 steps$/, () => {
    setUpIn3Steps.shouldSeeSetUpIn3StepsInformation();
});

Then(/^I can proceed to the account provider screen$/, () => {
    setUpIn3Steps.shouldSeeNextButton();
});

Then(/^I proceed to the account provider screen$/, () => {
    setUpIn3Steps.selectNextButton();
});
