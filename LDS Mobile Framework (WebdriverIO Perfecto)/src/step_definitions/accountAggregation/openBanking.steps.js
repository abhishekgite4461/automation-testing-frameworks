const {Then} = require('cucumber');
const OpenBankingPage = require('../../pages/accountAggregation/accAggOpenBanking.page');

const openBanking = new OpenBankingPage();

Then(/^I should (see|not see) the (.*) screen for account aggregation$/, (isVisible, screenName) => {
    openBanking.shouldSeeScreen(isVisible === 'see', screenName);
});

Then(/^I (can|cannot) go back to the previous page$/, (backButtonVisible) => {
    openBanking.shouldSeeBackButton(backButtonVisible === 'can');
});

Then(/^I close the account aggregation screen$/, () => {
    openBanking.selectCrossButton();
});

Then(/^I should see details for open banking$/, () => {
    openBanking.shouldSeeOpenBankingInformation();
});

Then(/^I can proceed to the secure and protected screen$/, () => {
    openBanking.shouldSeeNextButton();
});

Then(/^I proceed to the secure and protected screen$/, () => {
    openBanking.selectNextButton();
});
