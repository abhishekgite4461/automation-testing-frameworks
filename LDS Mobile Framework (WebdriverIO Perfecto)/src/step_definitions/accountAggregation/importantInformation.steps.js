const {When, Then} = require('cucumber');
const ImportantInformationPage = require('../../pages/accountAggregation/accAggImportantInformation');

const importantInformation = new ImportantInformationPage();

Then(/^I should see the details for important information$/, () => {
    importantInformation.shouldSeeImportantInformation();
});

Then(/^I should see the selected provider bank name (.*)$/, function (bankName) {
    const data = this.dataRepository.get();
    importantInformation.shouldSeeAccountProviderName(data[bankName]);
});

Then(/^I have not agreed to the T&C for open banking$/, () => {
    importantInformation.shouldSeeTnCCheckBoxChecked();
});

Then(/^I have not agreed to the consent for open banking$/, () => {
    importantInformation.shouldSeeConsentCheckBoxChecked();
});

Then(/^I should (see|not see) checkbox for T&C for open banking$/, (isVisible) => {
    importantInformation.shouldSeeTnCCheckbox(isVisible === 'see');
});

Then(/^I (agree|disagree) to the T&C for open banking$/, (isChecked) => {
    importantInformation.agreeTnC(isChecked === 'agree');
});

Then(/^I (agree|disagree) to the consent for open banking$/, (isChecked) => {
    importantInformation.agreeConsent(isChecked === 'agree');
});

Then(/^I agree to the T&C and Consent for open banking$/, () => {
    importantInformation.agreeTnC(true);
    importantInformation.agreeConsent(true);
});

Then(/^I should see the details for renew consent important information screen for (.*)$/, function (bankName) {
    const data = this.dataRepository.get();
    importantInformation.shouldSeeRenewConsentInformation(data[bankName]);
    importantInformation.shouldSeeTnCCheckbox(false);
});

Then(/^I have (agreed|not agreed) to renew my consent on important information screen$/, (isChecked) => {
    importantInformation.shouldSeeRenewConsentCheckBoxChecked(isChecked === 'agreed');
});

When(/^I agree to renew my consent for open banking$/, () => {
    importantInformation.selectRenewConsentCheckBox();
});

Then(/^I (can|cannot) proceed to the external provider website$/, (isEnabled) => {
    importantInformation.shouldSeeConfirmButtonEnabled(isEnabled === 'can');
});

When(/^I proceed to the external provider website$/, () => {
    importantInformation.selectConfirmButton();
});
