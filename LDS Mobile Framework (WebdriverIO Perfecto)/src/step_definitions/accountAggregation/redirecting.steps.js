const {Then} = require('cucumber');
const RedirectingPage = require('../../pages/accountAggregation/accAggRedirecting.page');
const MonthlyPdfPage = require('../../pages/statements/monthlyPdf.page');

const redirectingPage = new RedirectingPage();
const monthlyPdfPage = new MonthlyPdfPage();

Then(/^I should see the redirect error screen for (.*)$/, (reason) => {
    redirectingPage.shouldSeeRedirectErrorScreen(reason);
});

Then(/^I should see the provider bank name (.*) on the redirect screen$/, function (bankName) {
    const data = this.dataRepository.get();
    redirectingPage.shouldSeeProviderNameOnRedirectingScreen(data[bankName]);
});

Then(/^I should see winback with (.*) and (.*) option$/, (option1, option2) => {
    if (option1 === 'Cancel' || option2 === 'Cancel' || option1 === 'Keep' || option1 === 'Go back') {
        redirectingPage.shouldSeeWinBackCancelButton();
    } else if (option1 === 'Continue' || option2 === 'Continue' || option2 === 'Delete' || option2 === 'Proceed') {
        redirectingPage.shouldSeeWinBackContinueButton();
    }
});

Then(/^I select the (.*) option in winback$/, (winBackOption) => {
    switch (winBackOption) {
    case 'Cancel':
    case 'Close':
    case 'Keep':
        redirectingPage.selectWinBackCancelButton();
        break;
    case 'Go back':
        redirectingPage.selectWinBackCancelButton();
        break;
    case 'Continue':
    case 'Remove':
    case 'Delete':
        redirectingPage.selectWinBackContinueButton();
        break;
    case 'Proceed':
        redirectingPage.selectWinBackContinueButton();
        monthlyPdfPage.selectPdfViewer();
        break;
    case 'Digital Inbox':
        redirectingPage.selectWinBackDigitalInboxButton();
        break;
    default:
        throw new Error(`Win-back option ${winBackOption} not present`);
    }
});

Then(/^I should see user logged out error screen$/, () => {
    redirectingPage.shouldSeeLogOffErrorPage();
});

Then(/^I trigger the deeplink url$/, () => {
    redirectingPage.triggerDeeplink();
});

Then(/^I return back to the nga app from mobile browser without completing the journey$/, () => {
    redirectingPage.navigateBackToApp();
});

Then(/^I manually switch back to the nga app from mobile browser without completing the journey$/, () => {
    redirectingPage.switchToApp();
});

Then(/^I see the incomplete renew consent journey message on redirect screen$/, () => {
    redirectingPage.shouldSeeIncompleteRenewError();
});

Then(/^I should see home button on redirect screen$/, () => {
    redirectingPage.shouldSeeRedirectHomeButton();
});

Then(/^I select home on redirect screen$/, () => {
    redirectingPage.selectRedirectHomeButton();
});
