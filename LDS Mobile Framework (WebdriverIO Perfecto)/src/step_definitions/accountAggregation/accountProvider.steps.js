const {When, Then} = require('cucumber');
const AccountProviderPage = require('../../pages/accountAggregation/accAggAccountProvider.page');

const accountProvider = new AccountProviderPage();

Then(/^I should see all the external account provider names and logos$/, () => {
    accountProvider.shouldSeeProviderNameAndLogo();
});

Then(/^I should see footer information text$/, () => {
    accountProvider.shouldSeeFooterInformation();
});

When(/^I select an external provider bank (.*) from the list$/, function (bankName) {
    const data = this.dataRepository.get();
    accountProvider.selectAccountProvider(data[bankName]);
});
