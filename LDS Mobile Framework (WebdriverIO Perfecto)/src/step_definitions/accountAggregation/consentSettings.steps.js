const {When, Then} = require('cucumber');
const ConsentSettingsPage = require('../../pages/accountAggregation/accAggConsentSettings.page');
const CommonPage = require('../../pages/common.page');

const consentSettingsPage = new ConsentSettingsPage();
const commonPage = new CommonPage();

const validateConsentSettingsPage = function () {
    consentSettingsPage.waitForPageLoad();
};

Then(/^I should be on Consent settings page$/, validateConsentSettingsPage);

Then(/^I should see the details for consent settings$/, () => {
    consentSettingsPage.shouldSeeConsentSettingsInformation();
});

Then(/^I should see (Authorised|Revoked|Expired|Failed) account for (.*) on consent settings page$/, function (consentStatus, bankName) {
    const data = this.dataRepository.get();
    consentSettingsPage.shouldSeeConsentStatus(consentStatus, data[bankName]);
});

Then(/^I should see (Expires|Expired|Any) message for (.*) account on consent settings page$/, function (expiryStatus, bankName) {
    const data = this.dataRepository.get();
    consentSettingsPage.shouldSeeConsentExpiryMessage(expiryStatus, data[bankName]);
});

Then(/^I should (see|not see) Manage button for (.*) account on consent settings page$/, function (isShown, bankName) {
    const data = this.dataRepository.get();
    consentSettingsPage.shouldSeeManageButton(isShown === 'see', data[bankName]);
});

When(/^I select Manage button for (.*) account on consent settings page$/, function (bankName) {
    const data = this.dataRepository.get();
    consentSettingsPage.selectManageButton(data[bankName]);
});

Then(/^I should see Consent settings pop up for (.*)$/, function (bankName) {
    const data = this.dataRepository.get();
    consentSettingsPage.shouldSeeManageDrawer(data[bankName]);
});

Then(/^I should see below options on Consent settings pop up$/, (table) => {
    const tableData = table.raw();
    consentSettingsPage.shouldSeeGivenOptionsOnManageDrawer(tableData);
});

Then(/^I should not see any external accounts on the consent settings page$/, () => {
    consentSettingsPage.shouldNotSeeExternalAccounts();
});

When(/^I select (Renew|Remove) option on Consent settings pop up$/, (option) => {
    consentSettingsPage.selectOptionOnManageDrawer(option);
});

Then(/^I should see remove alert on Consent settings page$/, () => {
    consentSettingsPage.waitForAnAlertOnConsentSettingsPage();
});

Then(/^I wait for spinner to disappear$/, () => {
    commonPage.waitForSpinnerToComplete();
});

Then(/^I should (see|not see) red error banner on the page$/, (isShown) => {
    consentSettingsPage.shouldSeeErrorBanner(isShown === 'see');
});
