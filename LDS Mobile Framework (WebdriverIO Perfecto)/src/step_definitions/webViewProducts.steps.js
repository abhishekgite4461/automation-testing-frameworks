const {Then} = require('cucumber');
const WebViewProductsPage = require('../pages/webViewProducts.page');
const WebViewProducts = require('../enums/webViewProducts.enum');

const webViewProducts = new WebViewProductsPage();

Then(/^I should be displayed the (.*) webview$/, (webViewProduct) => {
    const product = WebViewProducts.fromStringByEscapingSpace(webViewProduct);
    switch (product) {
    case WebViewProducts.CHANGE_CREDIT_LIMIT:
        webViewProducts.isManageCreditLimitPageVisible();
        break;
    case WebViewProducts.REPAYMENT_HOLIDAY:
        webViewProducts.isRepaymentHolidayPageVisible();
        break;
    case WebViewProducts.ADDITIONAL_PAYMENT:
        webViewProducts.isAdditionalPaymentPageVisible();
        break;
    case WebViewProducts.BALANCE_TRANSFER:
        webViewProducts.isBalanceTransferVisible();
        break;
    case WebViewProducts.LOAN_CLOSURE:
        webViewProducts.isLoanClosureVisible();
        break;
    case WebViewProducts.BORROW_MORE:
        webViewProducts.isBorrowMoreVisible();
        break;
    case WebViewProducts.RENEW_YOUR_SAVINGS_ACCOUNT:
        webViewProducts.isRenewYourSavingsAccountVisible();
        break;
    case WebViewProducts.PENSION_TRANSFER:
        webViewProducts.waitForPensionTransferTitle();
        break;
    case WebViewProducts.OVERDRAFT:
        webViewProducts.shouldSeeOverdraftVisible();
        break;
    case WebViewProducts.CHANGE_ACCOUNT_TYPE:
        webViewProducts.shouldSeeChangeAccountTypeVisible();
        break;
    default:
        throw new Error(`${product} - web journey page is not defined`);
    }
});
