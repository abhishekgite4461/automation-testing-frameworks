const {When} = require('cucumber');
const SplashScreenPage = require('../pages/splashScreen.page');

const splashScreenPage = new SplashScreenPage();

When(/^I verify the splash screen$/, () => {
    splashScreenPage.verifySplashScreen();
});
