const {When, Then} = require('cucumber');
const PaymentHubPage = require('../../pages/paymentAggregation/paymentAggRedirect.page');

const paymentHubPage = new PaymentHubPage();

When(/^I should see error screen while redirecting user to external browser screen$/, () => {
    paymentHubPage.waitForErrorScreenRedirectExternalBrowser();
});

When(/^I should see the redirect screen with authentication not completed message$/, () => {
    paymentHubPage.verifyAuthenticationNotCompletedMessage();
});

Then(/^I should see the remitter bank name (.*) on the redirect screen$/, function (bankName) {
    const data = this.dataRepository.get();
    paymentHubPage.verifyRedirectScreenMessage(data[bankName]);
});

Then(/^I should see error message for (.*) on redirect screen$/, function (bankName) {
    const data = this.dataRepository.get();
    paymentHubPage.waitForNotSupportedPISPErrorScreen(data[bankName]);
});
