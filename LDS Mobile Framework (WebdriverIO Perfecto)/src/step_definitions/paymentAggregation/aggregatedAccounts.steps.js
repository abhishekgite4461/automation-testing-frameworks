const {Given, Then} = require('cucumber');
const PaymentHubPage = require('../../pages/paymentAggregation/aggAccount.page');

const paymentHubPage = new PaymentHubPage();

Then(/^I should (see|not see) the aggregated accounts in from list on payment hub page$/, (isShown) => {
    paymentHubPage.fromAccountListValidate(isShown);
});

Given(/^I get the list of all aggregated external accounts on home page$/, () => {
    paymentHubPage.getListOfExternalAccounts();
});

Then(/^I select the aggregated account as a remitting account$/, () => {
    paymentHubPage.selectFromExternalAccount();
});

Then(/^I select aggregated account as (.*) with (.*) in the payment hub page$/, function (aggBank, aggAccountNumber) {
    const data = this.dataRepository.get();
    paymentHubPage.selectFromExternalAccount(data[aggBank], data[aggAccountNumber]);
});

Then(/^I should see the selected aggregated account (.*) with (.*) as a remitting account in payment hub$/, function (accountSortCode, aggAccountNumber) {
    const data = this.dataRepository.get();
    paymentHubPage.shouldSeeFromExternalAccount(data[accountSortCode], data[aggAccountNumber]);
});

Then(/^I select To on payment hub page$/, () => {
    paymentHubPage.selectToAccountList();
});

Then(/^I should not see the selected from account in To list$/, () => {
    paymentHubPage.shouldNotSeeFromExternalAccount();
});

Then(/^I should see the (.*) in To list on recipient page$/, (accountType) => {
    paymentHubPage.shouldSeeCurrentAccount(accountType);
});

Then(/^I select to account as (.*) with (.*) in the payment hub page$/, function (aggBank, aggAccountNumber) {
    const data = this.dataRepository.get();
    paymentHubPage.toExternalAccountSelect(data[aggBank], data[aggAccountNumber]);
});

Then(/^I should see the selected aggregated account (.*) with (.*) in To account in payment hub page$/, function (accountSortCode, aggAccountNumber) {
    const data = this.dataRepository.get();
    paymentHubPage.selectedToAccount(data[accountSortCode], data[aggAccountNumber]);
});

Then(/^I should not see the help text for amount field in the payment hub page$/, () => {
    paymentHubPage.shouldNotSeeHelpTextForAmount();
});

Then(/^I select creditor account as (.*) in the payment hub page$/, (eligibleOwnAccount) => {
    paymentHubPage.selectEligibleOwnAccount(eligibleOwnAccount);
});

Then(/^I select debitor account with sort code (.*) and account number (.*) in the payment hub page$/, function (accountSortCode, accountNumber) {
    const data = this.dataRepository.get();
    paymentHubPage.selectDebitorAccountBySortCodeAndAccountNumber(data[accountSortCode], data[accountNumber]);
});

Then(/^I select creditor account with sort code (.*) and account number (.*) in the payment hub page$/, function (accountSortCode, accountNumber) {
    const data = this.dataRepository.get();
    paymentHubPage.selectCreditorAccountBySortCodeAndAccountNumber(data[accountSortCode], data[accountNumber]);
});
