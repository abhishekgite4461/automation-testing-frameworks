const {Given, When, Then} = require('cucumber');

require('chai').should();

const TransferReviewPage = require('../pages/transferAndPayments/transferReview.page');
const TransferSuccessPage = require('../pages/transferAndPayments/transferSuccess.page');
const RateAppPaymentHub = require('../pages/transferAndPayments/rateAppPaymentHub.page');
const PaymentReviewPage = require('../pages/transferAndPayments/paymentReview.page');
const BeneficiaryTypePage = require('../pages/transferAndPayments/beneficiaryType.page');
const PaymentSuccessPage = require('../pages/transferAndPayments/paymentSuccess.page');
const PaymentUnsuccessfulPage = require('../pages/transferAndPayments/paymentUnsuccessful.page');
const ViewTransactionPage = require('../pages/transferAndPayments/viewTransaction.page');
const ActionMenuPage = require('../pages/actionMenu.page');
const StandingOrderSuccessfulPage = require('../pages/transferAndPayments/standingOrderSuccessful.page');
const PaymentHubMainPage = require('../pages/paymentHub/paymentHubMain.page');
const ViewRemitterPage = require('../pages/paymentHub/viewRemitter.page');
const ViewRecipientPage = require('../pages/paymentHub/viewRecipient.page');
const HomePage = require('../pages/home.page');
const ReviewTransferPage = require('../pages/paymentHub/reviewTransfer.page');
const SuccessTransferPage = require('../pages/paymentHub/successTransfer.page');
const FailureTransferPage = require('../pages/paymentHub/failureTransfer.page');
const FailurePaymentPage = require('../pages/paymentHub/failurePayment.page');
const ReviewPaymentPage = require('../pages/paymentHub/reviewPayment.page');
const SuccessPaymentPage = require('../pages/paymentHub/successPayment.page');
const SearchRecipientPage = require('../pages/searchAccountAndRecipient.page');
const DeclinedPaymentPage = require('../pages/paymentHub/declinePayment.page');
const HoldPaymentPage = require('../pages/paymentHub/holdPayment.page');
const DirectDebit = require('../pages/paymentHub/directDebit.page.js');
const StandingOrder = require('../pages/paymentHub/standingOrder.page.js');
const SaveTheChanges = require('../pages/paymentHub/saveTheChanges.page.js');
const TransferUnsuccessfulPage = require('../pages/transferAndPayments/transferUnsuccessful.page');
const ErrorMessagePage = require('../pages/error.page');
const PaymentBlockedPage = require('../pages/transferAndPayments/paymentBlocked.page');
const IsaWarningPage = require('../pages/transferAndPayments/isaWarning.page');
const PaymentHubCalendarPage = require('../pages/paymentHub/calendar.page');
const ReactivateIsaInterstitialPage = require('../pages/paymentHub/reactivateIsaInterstitial.page');
const ReactivateIsaPage = require('../pages/paymentHub/reactivateIsa.page.js');
const AccountStatementPage = require('../pages/statements/accountStatement.page.js');
const AccountMatcher = require('../data/utils/accountMatcher');
const CommonPage = require('../pages/common.page');
const UpComingPaymentPage = require('../pages/paymentHub/upComingPayment.page');
const SavedAppDataRepository = require('../data/savedAppData.repository');
const SavedData = require('../enums/savedAppData.enum');
const PaymentTask = require('../tasks/payment.task');
const YourAccountsTask = require('../tasks/yourAccounts.task');
const UkCompanySearchResultsPage = require('../pages/transferAndPayments/ukCompanySearchResults.page.js');
const UkBeneficiaryDetailPage = require('../pages/transferAndPayments/ukBeneficiaryDetail.page.js');
const ConfirmPasswordPage = require('../pages/confirmAuthenticationPassword.page');
const ViewPaymentLimitsPage = require('../pages/paymentHub/viewPaymentLimits.page');
const ConfirmationOfPayeePage = require('../pages/transferAndPayments/confirmationOfPayee.page');
const Actions = require('../pages/common/actions');
const DeviceHelper = require('../utils/device.helper');
const EnvironmentApiClient = require('../utils/environmentApiClient');

const transferAmount = '1';
const reference = 'Test';
let amountBeforeTransaction;
let amountAfterTransaction;

const homePage = new HomePage();
const paymentHubMainPage = new PaymentHubMainPage();
const viewRemitterPage = new ViewRemitterPage();
const viewRecipientPage = new ViewRecipientPage();
const reviewTransferPage = new ReviewTransferPage();
const successTransferPage = new SuccessTransferPage();
const failureTransferPage = new FailureTransferPage();
const failurePaymentPage = new FailurePaymentPage();
const reviewPaymentPage = new ReviewPaymentPage();
const successPaymentPage = new SuccessPaymentPage();
const searchRecipientPage = new SearchRecipientPage();
const beneficiaryTypePage = new BeneficiaryTypePage();
const declinedPaymentPage = new DeclinedPaymentPage();
const holdPaymentPage = new HoldPaymentPage();
const actionMenuPage = new ActionMenuPage();
const directDebit = new DirectDebit();
const standingOrder = new StandingOrder();
const saveTheChanges = new SaveTheChanges();
const rateAppPaymentHub = new RateAppPaymentHub();
const paymentUnsuccessfulPage = new PaymentUnsuccessfulPage();
const transferUnsuccessfulPage = new TransferUnsuccessfulPage();
const paymentBlockedPage = new PaymentBlockedPage();
const transferReviewPage = new TransferReviewPage();
const isaWarningPage = new IsaWarningPage();
const standingOrderSuccessfulPage = new StandingOrderSuccessfulPage();
const transferSuccessPage = new TransferSuccessPage();
const paymentReviewPage = new PaymentReviewPage();
const paymentSuccessPage = new PaymentSuccessPage();
const viewTransaction = new ViewTransactionPage();
const errorPage = new ErrorMessagePage();
const paymentHubCalendarPage = new PaymentHubCalendarPage();
const reactivateIsaInterstitialPage = new ReactivateIsaInterstitialPage();
const reactivateIsaPage = new ReactivateIsaPage();
const accountStatementPage = new AccountStatementPage();
const commonPage = new CommonPage();
const confirmationOfPayeePage = new ConfirmationOfPayeePage();
const upComingPaymentPage = new UpComingPaymentPage();
const ukCompanySearchResultsPage = new UkCompanySearchResultsPage();
const ukBeneficiaryDetailPage = new UkBeneficiaryDetailPage();
const confirmPasswordPage = new ConfirmPasswordPage();
const viewPaymentLimitsPage = new ViewPaymentLimitsPage();

When(/^I should see the transfer successful message$/, () => {
    rateAppPaymentHub.closeRateAppPopUp();
    transferSuccessPage.verifyButtonsAvailable();
});

When(/^I verify the remaining allowance from ISA account after Transaction$/, () => {
    transferSuccessPage.verifyButtonsAvailable();
    transferSuccessPage.selectViewTransaction();

    viewTransaction.waitForPageLoad();
    amountAfterTransaction = viewTransaction.verifyRemainingAllowanceInTransactionPage();
    amountBeforeTransaction.should.be.at.most(amountAfterTransaction);
});

Then(/^I select the transfer and payment option in the action menu$/, () => {
    actionMenuPage.selectTransferAndPaymentOption();
});

Then(/^I should be on standing order created successful page$/, () => {
    standingOrderSuccessfulPage.waitForPageLoad();
});

When(/^I select the pay Uk number tooltip on beneficiary type page$/, () => {
    beneficiaryTypePage.clickPayUkNumberLink();
});

Then(/^I should see the (from|to) field pre-populated with (.*) account$/, (recipientType, accountName) => {
    paymentHubMainPage.waitForPaymentHubPageLoad();
    let accountNameData = accountName;
    if (accountName !== 'defaultPrimary') {
        accountNameData = AccountMatcher.getAccount(accountName);
    }
    paymentHubMainPage.checkRecipientPrePopulation(recipientType, accountNameData);
});

Then(/^I should see the (from|to) field pre-populated with (.*) account for p2p$/, function (recipientType, accountName) {
    paymentHubMainPage.waitForPaymentHubPageLoad();
    const data = this.dataRepository.get();
    const accName = data[accountName];
    paymentHubMainPage.checkRecipientPrePopulationForP2p(recipientType, accName);
});

Then(/^I select the (remitter|recipient) account on the payment hub page$/, PaymentTask.selectFromOrToAccount);

Then(/^I choose the (remitter|recipient) account on the payment hub page$/, PaymentTask.chooseFromOrToAccount);

Then(/^I search recipient account (.*) on payment hub page$/, (accountName) => {
    viewRecipientPage.searchAccount(accountName);
});

Then(/^I search recipient account (.*) on payment hub page to delete$/, (accountName) => {
    viewRecipientPage.searchAccountToDelete(accountName);
});

Then(/^I should (see|not see) the below accounts in the view remitter page$/, (type, table) => {
    const tableData = table.hashes();
    const ListOfAllAccounts = tableData.map((account) => AccountMatcher.getAccount(account.accounts));
    viewRemitterPage.waitForViewRemitterPageLoad();
    viewRemitterPage.viewListOfAccounts(type, ListOfAllAccounts);
});

Then(/^I see option to reactivate ISA journey$/, () => {
    actionMenuPage.selectActionMenuReactivateISA();
});

When(/^I select (.*) Account on payment hub page$/, (accountName) => {
    paymentHubMainPage.chooseRemitterAccount(AccountMatcher.getAccount(accountName));
});

Then(/^I should see beneficiaryType on beneficiary type page$/, (table) => {
    const tableData = table.hashes();
    beneficiaryTypePage.verifyBeneficiaryCategory(tableData);
});

When(/^I choose UK account from account category page$/, () => {
    beneficiaryTypePage.chooseUkAccountBeneficiary();
});

When(/^I can see paym is disabled$/, () => {
    beneficiaryTypePage.verifyPayMisDisabled();
});

Given(/^I add a new recipient with valid (.*), (.*), (.*), (.*)$/, function (sortCode, accountNumber, recipientName, ref) {
    const data = this.dataRepository.get();
    homePage.selectPayAndTransfer();
    paymentHubMainPage.waitForPaymentHubPageLoad();
    PaymentTask.selectFromOrToAccount('recipient');
    searchRecipientPage.verifyAddNewPayee('should');
    PaymentTask.addNewPayee();
    beneficiaryTypePage.chooseUkAccountBeneficiary();
    PaymentTask.enterUkBeneficiaryDetails(sortCode, accountNumber, recipientName);
    ukBeneficiaryDetailPage.continueToReviewDetailsPage();
    confirmationOfPayeePage.waitForCOPResponsePage();
    confirmationOfPayeePage.selectCOPDetailsContinueOption();
    ukCompanySearchResultsPage.enterReference(ref);
    ukCompanySearchResultsPage.confirmReviewDetailsPage();
    YourAccountsTask.verifyEnterPasswordDialogBox('be');
    confirmPasswordPage.enterPasswordAndConfirm(data.password);
    paymentHubMainPage.verifyAddBeneficiary();
});

Given(/^I add a new recipient with details (.*), (.*), (.*), (.*), (.*)$/, function (fromAccount, sortCode, accountNumber, recipientName, ref) {
    const data = this.dataRepository.get();
    homePage.selectPayAndTransfer();
    PaymentTask.selectFromOrToAccount('remitter');
    paymentHubMainPage.chooseRemitterAccount(AccountMatcher.getAccount(fromAccount));
    paymentHubMainPage.waitForPaymentHubPageLoad();
    PaymentTask.selectFromOrToAccount('recipient');
    searchRecipientPage.verifyAddNewPayee('should');
    PaymentTask.addNewPayee();
    beneficiaryTypePage.chooseUkAccountBeneficiary();
    PaymentTask.enterUkBeneficiaryDetails(sortCode, accountNumber, recipientName);
    ukBeneficiaryDetailPage.continueToReviewDetailsPage();
    confirmationOfPayeePage.waitForCOPResponsePage();
    confirmationOfPayeePage.selectCOPDetailsContinueOption();
    ukCompanySearchResultsPage.enterReference(ref);
    ukCompanySearchResultsPage.confirmReviewDetailsPage();
    YourAccountsTask.verifyEnterPasswordDialogBox('be');
    confirmPasswordPage.enterPasswordAndConfirm(data.password);
    paymentHubMainPage.verifyAddBeneficiary();
});

Then(/^I should see the option to create a new recipient using UK bank details$/, () => {
    searchRecipientPage.selectAddNewPayee();
    beneficiaryTypePage.viewUkAccountVisible();
});

Then(/^I should able to create a new international recipient$/, () => {
    searchRecipientPage.selectAddNewPayee();
    beneficiaryTypePage.selectInternationalBankAccount();
});

Then(/^I should able to create a payM contact recipient$/, () => {
    searchRecipientPage.selectAddNewPayee();
    beneficiaryTypePage.viewPayUkNumberLinkVisible();
    beneficiaryTypePage.verifyUkMobileNumber();
});

When(/^I select international bank account from account category page$/, () => {
    beneficiaryTypePage.selectInternationalBankAccount();
});

Then(/^I should not be able to select pending payment from view recipient page$/, () => {
    viewRecipientPage.selectPendingPayment();
    viewRecipientPage.waitForViewRecipientPageLoad();
});

Then(/^I should see the international recipient with IBAN number from view recipient page$/, () => {
    viewRecipientPage.verifyInternationalPaymentWithIBANVisible();
});

Then(/^I should see the international recipient with account number from view recipient page$/, () => {
    viewRecipientPage.verifyInternationalPaymentWithAccountNumberVisible();
});

Then(/^I should not see the international recipient from view recipient page$/, () => {
    viewRecipientPage.shouldNotSeeInternationalPayment();
});

When(/^I select the To account in the payment hub page$/, () => {
    paymentHubMainPage.selectToAccountPane();
});

Then(/^I select the continue option in the payment hub page$/, () => {
    paymentHubMainPage.selectContinueButton();
});

Then(/^I validate the available balance of saving account$/, () => {
    paymentHubMainPage.checkAvailableBalance();
});

Then(/^I should see the corresponding details in the (.*) success page$/, (type) => {
    if (type === 'transfer') {
        successTransferPage.waitForPageLoad();
        if (DeviceHelper.isRNGA()) {
            successTransferPage.validateAccountDetails();
        }
    } else if (type === 'payment' || type.includes('creditCard')) {
        paymentSuccessPage.waitForPageLoad();
        if (DeviceHelper.isRNGA()) {
            paymentSuccessPage.validateAccountDetails(type);
        }
    }
});

Then(/^I am on the payment hub page$/, () => {
    homePage.selectPayAndTransfer();
    paymentHubMainPage.waitForPaymentHubPageLoad();
});

Then(/^I select payment hub from home page$/, () => {
    homePage.selectPayAndTransfer();
});

const selectedAggregatedFromAccount = function (fromExtProvider, fromAggAccount) {
    viewRemitterPage.selectedAggAccount(fromExtProvider, fromAggAccount);
};

Then(/^I should see from account as (.*) with (.*) selected on the payment hub page$/, function (fromExtProvider, fromAggAccount) {
    const data = this.dataRepository.get();
    paymentHubMainPage.waitForPaymentHubPageLoad();
    selectedAggregatedFromAccount(data[fromExtProvider], data[fromAggAccount]);
});

Then(/^I must see payments hub unavailable error message page$/, () => {
    paymentHubMainPage.verifyPaymentHubUnavailable();
});

const selectFromAndToAccounts = function (fromAccount, toAccount) {
    paymentHubMainPage.selectPayAccount('remitter');
    viewRemitterPage.selectAccount(fromAccount);
    paymentHubMainPage.waitForPaymentHubPageLoad();
    paymentHubMainPage.selectPayAccount('recipient');
    viewRecipientPage.selectAccount(toAccount);
};

const validateMortgagePaymentDetails = function () {
    reviewPaymentPage.validateAccountDetails('mortgage');
    reviewPaymentPage.shouldSeeSubaccountInfo();
    reviewPaymentPage.validateMortgageReferenceNumber();
};

const chooseBusinessAccount = function (fromAccount, businessName) {
    viewRemitterPage.selectBusinessAccount(fromAccount, businessName);
};

const selectFromBusinessAccount = function (fromAccount, businessName) {
    paymentHubMainPage.selectPayAccount('remitter');
    chooseBusinessAccount(fromAccount, businessName);
};

const selectToBusinessAccount = function (toAccount, businessName) {
    paymentHubMainPage.selectPayAccount('recipient');
    chooseBusinessAccount(toAccount, businessName);
};

const selectFromAndToAccountsOfDifferentBusiness = function (fromAccount, fromBusiness, toAccount, toBusiness) {
    selectFromBusinessAccount(fromAccount, fromBusiness);
    paymentHubMainPage.waitForPaymentHubPageLoad();
    selectToBusinessAccount(toAccount, toBusiness);
};

const selectRecipient = (account) => {
    paymentHubMainPage.selectPayAccount('recipient');
    viewRecipientPage.selectPaymentRecipient(account);
};

const verifyReferenceNonEditable = function () {
    paymentHubMainPage.referenceFieldNotEnabled();
};

const selectReferenceTooltip = function () {
    paymentHubMainPage.selectReferenceToolTip();
};

const closeReferenceToolTip = function () {
    paymentHubMainPage.closeReferenceToolTip();
};

const selectFromAndToForPayment = function (fromAccount, toAccount) {
    paymentHubMainPage.selectPayAccount('remitter');
    viewRemitterPage.selectAccount(fromAccount);
    paymentHubMainPage.selectPayAccount('recipient');
    viewRecipientPage.selectPaymentRecipient(toAccount);
};

const enterAmount = function (amountValue) {
    paymentHubMainPage.enterAmount(amountValue);
};

const enterPaymentReference = function () {
    paymentHubMainPage.enterReference(reference);
};

const selectContinueButton = function () {
    paymentHubMainPage.selectContinueButton();
};

const selectAggregatedFromAndToAccounts = function (fromSortCode, fromAggAccount, toSortCode, toAggAccount) {
    paymentHubMainPage.selectPayAccount('remitter');
    viewRemitterPage.selectAggAccount(fromSortCode, fromAggAccount);
    paymentHubMainPage.waitForPaymentHubPageLoad();
    paymentHubMainPage.selectPayAccount('recipient');
    viewRecipientPage.selectAggAccount(toSortCode, toAggAccount);
};

const submitTransfer = function (fromAccount, toAccount, amountValue) {
    selectFromAndToAccounts(AccountMatcher.getAccount(fromAccount), AccountMatcher.getAccount(toAccount));
    enterAmount(amountValue);
    selectContinueButton();
};

const submitIsaTransfer = function (fromAccountType, fromAccount, toAccountType, toAccount, amountValue) {
    selectFromAndToAccounts(AccountMatcher.getAccount(fromAccount), AccountMatcher.getData(toAccount));
    if (fromAccountType === 'corresponding from') {
        paymentHubMainPage.setRemainingAllowanceForFromIsa();
    } else if (toAccountType === 'corresponding to') {
        paymentHubMainPage.setRemainingAllowanceForToIsa();
    }
    enterAmount(amountValue);
    selectContinueButton();
};

const submitPayment = function (fromAccount, toAccount, amountValue) {
    const toAccountData = (toAccount === 'any') ? 'any' : AccountMatcher.getData(toAccount);
    selectFromAndToForPayment(AccountMatcher.getAccount(fromAccount), toAccountData);
    enterAmount(amountValue);
    enterPaymentReference();
    selectContinueButton();
    paymentHubMainPage.continueFraudsterWarningIfPresent();
};

const submitFutureDatePayment = function (days) {
    paymentHubCalendarPage.selectFuturePaymentDate(days);
    paymentHubMainPage.selectContinueButton();
};

const submitPayCreditCard = function (amountValue) {
    enterAmount(amountValue);
    selectContinueButton();
};

const saveAndSubmitPayCreditCard = function (amountValue) {
    paymentHubMainPage.waitForPaymentHubPageLoad();
    enterAmount(amountValue);
    paymentHubMainPage.setAccountDetailsForPayment('creditCard');
    selectContinueButton();
};

const saveAndSubmitFutureDatedPayCreditCard = function (amountValue) {
    paymentHubMainPage.waitForPaymentHubPageLoad();
    enterAmount(amountValue);
    paymentHubMainPage.setAccountDetailsForPayment('creditCard');
    paymentHubMainPage.selectCalendarPicker();
    paymentHubCalendarPage.waitForPageLoad();
    paymentHubCalendarPage.selectFuturePaymentDate(4);
    selectContinueButton();
};

const saveFromAndToAccountDetails = function (fromAccount, toAccount, amountValue) {
    selectFromAndToForPayment(fromAccount, toAccount);
    enterAmount(amountValue);
    enterPaymentReference();
    paymentHubMainPage.setAccountDetailsForPayment();
};

const saveAccountDetailsAndSubmitPayment = function (fromAccount, amountValue) {
    saveFromAndToAccountDetails(AccountMatcher.getAccount(fromAccount), AccountMatcher.getData('firstBeneficiary'), amountValue);
    selectContinueButton();
    paymentHubMainPage.continueFraudsterWarningIfPresent();
};

const submitTransferDifferentBusiness = (toAccount, toBusiness, amount) => {
    selectToBusinessAccount(AccountMatcher.getData(toAccount), AccountMatcher.getData(toBusiness));
    enterAmount(amount);
    paymentHubMainPage.setAccountDetailsForTransfer();
    selectContinueButton();
};

const submitPaymentForNewBeneficiary = (fromAccount, amountValue) => {
    SavedAppDataRepository.getData(SavedData.NEWLY_ADDED_BENEFICIARY);
    saveFromAndToAccountDetails(AccountMatcher.getAccount(fromAccount),
        SavedAppDataRepository.getData(SavedData.NEWLY_ADDED_BENEFICIARY)
            .toUpperCase(), amountValue);
    selectContinueButton();
};

const saveFromAndToAccountDetailsAndSubmitPayment = (fromAccount, toAccount, amountValue) => {
    saveFromAndToAccountDetails(AccountMatcher.getAccount(fromAccount),
        AccountMatcher.getData(toAccount), amountValue);
    selectContinueButton();
};

const submitTransferWithFirstAccount = function (transferType, type, amountValue) {
    // chooses the account which is passed as type on the remitter field,
    // for value 'any' proceeds with the default remitter account
    if (type !== 'any') {
        paymentHubMainPage.selectPayAccount('remitter');
        const data = this.dataRepository.get();
        viewRemitterPage.selectAccount(data[type]);
    }
    paymentHubMainPage.selectPayAccount('recipient');
    viewRecipientPage.selectFirstAccount();
    if (transferType === 'isa transfer') {
        paymentHubMainPage.setRemainingAllowanceForFromIsa();
    }
    enterAmount(amountValue);
    selectContinueButton();
};

const submitPaymentWithFirstAccount = function (amountValue) {
    paymentHubMainPage.selectPayAccount('recipient');
    viewRecipientPage.selectPaymentRecipient(this.dataRepository.get().uKBank);
    enterAmount(amountValue);
    selectContinueButton();
};

const saveAccountDetailsAndSubmitTransfer = function (fromAccount, toAccount, amountValue) {
    selectFromAndToAccounts(AccountMatcher.getAccount(fromAccount), AccountMatcher.getAccount(toAccount));
    enterAmount(amountValue);
    paymentHubMainPage.setAccountDetailsForTransfer();
    selectContinueButton();
};

const saveAccountDetailsAndSubmitMPAPayment = function
(typeOfPayment, fromAccount, fromBusiness, accountType, toAccount, amountValue) {
    selectFromBusinessAccount(AccountMatcher.getAccount(fromAccount), AccountMatcher.getData(fromBusiness));
    if (accountType === 'account') {
        selectRecipient(AccountMatcher.getAccount(toAccount));
    } else if (accountType === 'beneficiary') {
        selectRecipient(AccountMatcher.getData(toAccount));
    }
    enterAmount(amountValue);
    paymentHubMainPage.setRemitterDetails();
    paymentHubMainPage.setTransferAmount();
    selectContinueButton();
};

const selectDifferentBusinessAccount = function
(fromAccount, fromBusiness) {
    const data = this.dataRepository.get();
    chooseBusinessAccount(data[fromAccount], data[fromBusiness]);
    paymentHubMainPage.setRemitterDetails();
};

const saveAccountDetailsAndSubmitMPATransfer = (fromAccount, fromBusiness, toAccount, toBusiness, amountValue) => {
    selectFromAndToAccountsOfDifferentBusiness(AccountMatcher.getData(fromAccount),
        AccountMatcher.getData(fromBusiness),
        AccountMatcher.getData(toAccount),
        AccountMatcher.getData(toBusiness));
    enterAmount(amountValue);
    paymentHubMainPage.setAccountDetailsForTransfer();
    selectContinueButton();
};

const enterDefaultAmount = function (amountValue) {
    enterAmount(amountValue);
};

const savePaymentDetails = function (amountValue) {
    enterPaymentReference();
    enterAmount(amountValue);
    paymentHubMainPage.setAccountDetailsForPayment();
};

const saveAndSubmitPaymentDetails = function (amountValue) {
    savePaymentDetails(amountValue);
    selectContinueButton();
};

const saveAndSubmitP2PPaymentDetails = function (amountValue) {
    enterAmount(amountValue);
    enterPaymentReference();
    paymentHubMainPage.setAccountDetailsForPayment('paymobile');
    selectContinueButton();
};

Then(/^I choose the from account as (.*) and to (account|beneficiary) as (.*) in the payment hub page$/, (fromAccount, accountType, toAccount) => {
    if (accountType === 'account') {
        selectFromAndToAccounts(AccountMatcher.getAccount(fromAccount), AccountMatcher.getAccount(toAccount));
    } else if (accountType === 'beneficiary') {
        selectFromAndToAccounts(AccountMatcher.getAccount(fromAccount), AccountMatcher.getData(toAccount));
    }
});

// Payment Aggregation steps
Then(/^I select from account as (.*) with (.*) and to account as (.*) with (.*) in the payment hub page$/, function (fromSortCode, fromAggAccount, toSortCode, toAggAccount) {
    const data = this.dataRepository.get();
    selectAggregatedFromAndToAccounts(data[fromSortCode], data[fromAggAccount], data[toSortCode],
        data[toAggAccount]);
});

Then(/^I select tooltip for information on non-editable reference in the payment hub page$/, () => {
    verifyReferenceNonEditable();
    selectReferenceTooltip();
});

Then(/^I should be able to close the non-editable reference tooltip in the payment hub page$/, () => {
    closeReferenceToolTip();
});

Then(/^I should (see|not see) amount field in the payment hub page$/, (type) => {
    if (type === 'see') {
        paymentHubMainPage.amountFieldVisibility(true);
    } else {
        paymentHubMainPage.amountFieldVisibility(false);
    }
});

Then(/^I am displayed a faster payment warning message$/, () => {
    paymentHubMainPage.verifyFasterPaymentWarning();
});

Then(/^I am displayed faster payment warning pop up message$/, () => {
    paymentHubMainPage.verifyFasterPaymentWarningPopUp();
});

Then(/^I have link to view online banking status$/, () => {
    paymentHubMainPage.verifyFasterPaymentWarningStatusLink();
});

Then(/^I select link to view online banking status$/, () => {
    paymentHubMainPage.selectFasterPaymentWarningStatusLink();
});

Then(/^I select the (.*) (account|beneficiary) as (.*) in the payment hub page$/, (type, accountType, accountName) => {
    if (type === 'from') {
        paymentHubMainPage.selectPayAccount('remitter');
        viewRemitterPage.selectAccount(AccountMatcher.getAccount(accountName));
    } else if (type === 'to') {
        paymentHubMainPage.selectPayAccount('recipient');
        if (accountType === 'beneficiary') {
            viewRecipientPage.selectPaymentRecipient(AccountMatcher.getData(accountName));
        } else {
            viewRecipientPage.selectAccount(AccountMatcher.getAccount(accountName));
        }
    }
});

Then(/^I submit with amount (.*) in the payment hub page$/, (amount) => {
    paymentHubMainPage.enterAmount(amount);
    paymentHubMainPage.selectContinueButton();
});

Then(/^I select the to account from the beneficiary list$/, function () {
    paymentHubMainPage.selectPayAccount('recipient');
    viewRecipientPage.selectPaymentRecipient(this.dataRepository.get().uKBank);
});

Then(/^I should (see|not see) standing order toggle option in the payment hub page$/, (type) => {
    if (type === 'see') {
        paymentHubMainPage.standingOrderSwitchVisibility(true);
    } else {
        paymentHubMainPage.standingOrderSwitchVisibility(false);
    }
});

Then(/^I should see the continue button (disabled|enabled) in the payment hub page$/, (type) => {
    if (type === 'enabled') {
        paymentHubMainPage.continueButtonEnability(true);
    } else {
        paymentHubMainPage.continueButtonEnability(false);
    }
});

Then(/^I enter (.*) in the amount field in the payment hub page$/, enterAmount);

Then(/^I enter (.*) in the payment amount field in the payment hub page$/, enterAmount);

Then(/^I select the (payment amount|amount) field in the payment hub page$/, (type) => {
    if (type === 'amount') {
        paymentHubMainPage.selectAmountField();
    } else {
        paymentHubMainPage.selectPaymentAmountField();
    }
});

Then(/^I select an additional option for pay a credit card in the payment hub page$/, () => {
    paymentHubMainPage.additionalOptionCreditCard();
});

Then(/^I should see the help text for amount field in the payment hub page$/, () => {
    paymentHubMainPage.helpTextForAmountField();
});

Then(/^I should see only 2 decimal places in the amount field in the payment hub page$/, () => {
    paymentHubMainPage.shouldSeeOnlyTwoDecimalPlacesForAmount();
});

Then(/^I should see only 2 decimal places in the payment amount field in the payment hub page$/, () => {
    paymentHubMainPage.shouldSeeOnlyTwoDecimalPlacesForPaymentAmount();
});

Then(/^I should (not see|see) the values pre-populated in the payment hub page for transfer with amount value (.*)$/, (type, amount) => {
    paymentHubMainPage.verifyAmountValue(amount, type);
});

Then(/^I should see the values pre-populated in the payment hub page for payment$/, () => {
    paymentHubMainPage.verifyAmountValueFromIBC();
});

Then(/^I should see the values pre-populated in the payment hub page for payment with amount value (.*)$/, (amount) => {
    paymentHubMainPage.shouldContainAmountValueForPayments(amount);
});

When(/^I should see reference field in the payment hub page$/, () => {
    paymentHubMainPage.referenceField();
});

Then(/^I select the home header option in the payment hub page$/, () => {
    commonPage.selectHeaderBackButton();
});

Then(/^I select dismiss modal using close button$/, () => {
    commonPage.closeModal();
});

Then(/^I should see the cancel and OK option in the payment hub page winback$/, () => {
    commonPage.shouldSeeOptionsInWinback();
});

When(/^I should see the leave and Stay option in the payment hub page winback$/, () => {
    commonPage.shouldSeeOptionsInWinback();
});

Then(/^I select leave option from winback$/, () => {
    commonPage.shouldClickLeaveOptionsInWinback();
});

Then(/^I select stay option from winback$/, () => {
    commonPage.selectWinBackStayButton();
});

Then(/^I submit the transfer with all the mandatory fields with from account as (.*) and to account as (.*) with amount value (.*)$/, submitTransfer);

Then(/^I submit the isa transfer with all the mandatory fields with (from|corresponding from) account as (.*) and (to|corresponding to) account as (.*) with amount value (.*)$/, submitIsaTransfer);

Then(/^I submit the payment with all the mandatory fields with from account as (.*) and (.*) beneficiary with amount value (.*) and reference$/, submitPayment);

Then(/^I enter reference as (.*)$/, (ref) => {
    paymentHubMainPage.enterReference(ref);
});

Then(/^I enter amount greater than available balance in the remitter account in the payment hub page$/, () => {
    const amountValue = parseFloat(paymentHubMainPage.getRemitterAmount())
        + parseFloat(paymentHubMainPage.getOverDraftAmountIfPresent()) + 1;
    paymentHubMainPage.enterAmount(String(amountValue));
    paymentHubMainPage.selectContinueButton();
});

Then(/^I enter amount greater than remaining allowance in the recipient account in the payment hub page$/, () => {
    paymentHubMainPage.waitForPaymentHubPageLoad();
    const amountValue = parseFloat(paymentHubMainPage.getRemainingAllowanceForToIsa()) + 1;
    paymentHubMainPage.enterAmount(String(amountValue));
    paymentHubMainPage.selectContinueButton();
});

Then(/^I enter payment amount greater than available balance in the remitter account in the payment hub page$/, () => {
    const amountValue = parseFloat(paymentHubMainPage.getRemitterAmount()) + 1;
    paymentHubMainPage.enterAmount(String(amountValue));
    paymentHubMainPage.selectContinueButton();
});

Then(/^I enter amount greater than available balance in the aggregated remitter account in the payment hub page$/, () => {
    const amountValue = parseFloat(paymentHubMainPage.getAggRemitterAmount()) + 1;
    paymentHubMainPage.enterAmount(String(amountValue));
});

Then(/^I enter amount greater than the balance of the credit card account in the payment hub page$/, () => {
    paymentHubMainPage.waitForPaymentHubPageLoad();
    const amountValue = parseFloat(paymentHubMainPage.getCreditCardAmount()) + 1;
    paymentHubMainPage.enterAmount(String(amountValue));
    paymentHubMainPage.selectContinueButton();
});

Then(/^I should see credit card payment validation error message$/, () => {
    paymentHubMainPage.validateCreditCardPaymentErrorMessage();
});

Then(/^I should on Confirm payee page$/, () => {
    paymentHubMainPage.verifySuccessConfirmPage();
});

Then(/^I submit the (isa transfer|transfer) with all the mandatory fields with (.*) from account and to account with amount value (.*)$/, submitTransferWithFirstAccount);

Then(/^I submit the payment with all the mandatory fields with any from account and to account with amount value (.*)$/, submitPaymentWithFirstAccount);

Then(/^I submit the pay a credit card with all the mandatory fields with amount value (.*)$/, submitPayCreditCard);

Then(/^I submit the pay a credit card with all the mandatory fields with corresponding amount value (.*)$/, saveAndSubmitPayCreditCard);

Then(/^I submit the future dated payment for a credit card with all the mandatory fields with corresponding amount value (.*)$/, saveAndSubmitFutureDatedPayCreditCard);

Then(/^I should be able to close the insufficient money in your account error message in the payment hub page$/, () => {
    paymentHubMainPage.continueFraudsterWarningIfPresent();
    paymentHubMainPage.shouldSeeInsufficientMoneyErrorMessage();
    paymentHubMainPage.closeErrorMessage();
});

Then(/^I submit transfer with from account (.*) and to account (.*) with amount (.*)$/, (fromAccount, toAccount, amountValue) => {
    saveAccountDetailsAndSubmitTransfer(fromAccount, toAccount, amountValue);
});

Then(/^I select (.*) account of (.*) business with (.*) amount$/, submitTransferDifferentBusiness);

Then(/^I submit transfer with from account as (.*) of (.*) and differing business account as (.*) of (.*) with value (.*)$/, saveAccountDetailsAndSubmitMPATransfer);

Then(/^I submit the (payment|transfer) with from account as (.*) of (.*) and to (account|beneficiary) (.*) with amount value (.*)$/, saveAccountDetailsAndSubmitMPAPayment);

Then(/^I submit the payment with all the mandatory fields with corresponding from account as (.*) and to account from beneficiary list with amount value (.*) and reference$/, saveAccountDetailsAndSubmitPayment);

Then(/^I submit the payment from account (.*) and to previously added account for amount (.*)$/, submitPaymentForNewBeneficiary);

Then(/^I submit the payment with from account (.*) and to beneficiary (.*) from beneficiary list with amount (.*) and reference$/, saveFromAndToAccountDetailsAndSubmitPayment);

When(/^I enter all mandatoy fields with corresponding from account as (.*) and the to account from the beneficiary list with amount value (.*) and reference$/, function (fromAccount, amountValue) {
    const data = this.dataRepository.get();
    saveFromAndToAccountDetails(data[fromAccount], data.uKBank, amountValue);
});

Then(/^I enter amount in the amount field as (.*)$/, enterDefaultAmount);

Then(/^I select view payment limits link$/, () => {
    paymentHubMainPage.selectViewPaymentLimitsLink();
});

Then(/^I select account as (.*) of different (.*) business$/, selectDifferentBusinessAccount);

Then(/^I select the edit option in the review transfer page$/, () => {
    reviewTransferPage.selectEditButton();
});

Then(/^I select the cancel option in the review transfer page$/, () => {
    reviewPaymentPage.selectEditButton();
});

Then(/^I select the edit option in the review payment page$/, () => {
    reviewPaymentPage.selectEditButton();
});

Then(/^I select the confirm option in the review transfer page$/, () => {
    reviewTransferPage.waitForPageLoad();
    reviewTransferPage.selectConfirmButton();
});

Then(/^I select the (edit|confirm) option in the MPA review transfer page$/, (type) => {
    reviewTransferPage.waitForPageLoad();
    reviewTransferPage.verifyMPATransferMessage();
    if (type === 'confirm') {
        reviewTransferPage.selectConfirmButton();
    } else {
        reviewTransferPage.selectEditButton();
    }
});

Then(/^I select the (edit|confirm) option in the MPA review payment page$/, (type) => {
    reviewPaymentPage.waitForPageLoad();
    reviewPaymentPage.verifyMPAPaymentMessage();
    if (type === 'confirm') {
        reviewPaymentPage.selectConfirmButton();
    } else {
        reviewPaymentPage.selectEditButton();
    }
});

Then(/^I should (see|not see) MPA approval message in the review payment page$/, (type) => {
    if (type === 'not see') {
        reviewPaymentPage.verifyNoMPAPaymentMessage();
    } else {
        reviewPaymentPage.verifyMPAPaymentMessage();
    }
});

Then(/^I select the header back option in the review transfer page$/, () => {
    commonPage.selectModalHeaderBackButton();
});

Then(/^I select the header back option in the review payment page$/, () => {
    commonPage.selectModalHeaderBackButton();
});

Then(/^I select the header back option in the enter payment details page$/, () => {
    commonPage.selectModalHeaderBackButton();
});

Then(/^I should see the corresponding details in the review transfer page$/, () => {
    reviewTransferPage.validateAccountDetails();
});

Then(/^I should see the corresponding details in the review payment (page|page for credit card|page for P2P|page for mortgage overpayment)$/, (type) => {
    reviewPaymentPage.waitForPageLoad();

    // review and success payment page has different objects TODO: PAYMOB-2078
    if (type === 'page for credit card') {
        reviewPaymentPage.validateAccountDetails('creditCard');
    } else if (type === 'page for P2P') {
        reviewPaymentPage.validateAccountDetails('paymobile');
    } else if (type === 'page for mortgage overpayment') {
        validateMortgagePaymentDetails();
    } else {
        reviewPaymentPage.validateAccountDetails('payment');
    }
});

Then(/^I select the make another (transfer|payment) option in the (transfer|payment) success page$/, (buttonType, type) => {
    if (buttonType === 'transfer' && type === 'transfer') {
        successTransferPage.waitForPageLoad();
        successTransferPage.selectMakeAnotherTransferButton();
    } else {
        successPaymentPage.waitForPageLoad();
        successPaymentPage.selectMakeAnotherPaymentButton();
    }
});

Then(/^I should see make another (transfer|payment) option in the MPA (transfer|payment) submitted for approval page$/, (buttonType, type) => {
    if (buttonType === 'transfer' && type === 'transfer') {
        successTransferPage.waitForPageLoad();
        successTransferPage.verifyMPATransferApprovalSuccessMessage();
        successTransferPage.verifyMakeAnotherTransferButton();
    } else {
        successPaymentPage.waitForPageLoad();
        successPaymentPage.verifyMPAPaymentApprovalSuccessMessage();
        successPaymentPage.selectMakeAnotherPaymentButton();
    }
});

Then(/^I select the home header option in the (transfer|payment) success page$/, () => {
    commonPage.selectHeaderBackButton();
});

Then(/^I navigate to the home page from transfer success page$/, () => {
    successTransferPage.waitForPageLoad();
    commonPage.navigateHomeFromPaymentSuccess();
});

Then(/^I navigate to the home page from payment success page$/, () => {
    successPaymentPage.waitForPageLoad();
    commonPage.navigateHomeFromPaymentSuccess();
});

Then(/^I select the back header option in the payment success page$/, () => {
    commonPage.selectHeaderBackButton();
});

Then(/^I select the hard core back button in the payment success page$/, () => {
    successPaymentPage.waitForPageLoad();
    SuccessPaymentPage.selectHardCoreBackButton();
});

Then(/^I select the view transaction option in the (transfer|payment) success page$/, (type) => {
    if (type === 'transfer') {
        successTransferPage.waitForPageLoad();
        successTransferPage.selectViewTransaction();
    } else {
        successPaymentPage.waitForPageLoad();
        successPaymentPage.selectViewTransaction();
    }
});

const getLocatorForIsa = function (type) {
    switch (type) {
    case 'help to buy isa':
        return 'htbIsa';
    case 'top up isa':
        return 'topUpIsa';
    case 'instant cash isa':
        return 'isaWithPayment';
    case 'variable rate isa':
        return 'isaWithPayment';
    case 'fixed rate isa':
        return 'fixedRateISA';
    default:
        throw new Error(`Unknown isa type: ${type}`);
    }
};

Then(/^I should see corresponding (.*) deducted in the remitter (.*) account$/, function (amount, fromAccount) {
    const data = this.dataRepository.get();
    homePage.navigateAndValidateAmountAfterTransaction('from', data[fromAccount], amount);
});

Then(/^I should see the corresponding (.*) deducted in the remitter (.*) and recipient accounts (.*)$/, (amount, fromAccount, toAccount) => {
    homePage.waitForPageLoad();
    homePage.navigateAndValidateAmountAfterTransaction('from', AccountMatcher.getAccount(fromAccount), amount);
    homePage.navigateAndValidateAmountAfterTransaction('to', AccountMatcher.getAccount(toAccount), amount);
});

Then(/^I should see the corresponding (.*) (not|being) deducted in the remitter (.*)$/, (amount, type, fromAccount) => {
    homePage.waitForPageLoad();
    if (type === 'not') {
        homePage.navigateAndValidateNoAmountDeductionAfterTransaction('from', AccountMatcher.getAccount(fromAccount));
    } else {
        homePage.navigateAndValidateAmountAfterTransaction('from', AccountMatcher.getAccount(fromAccount), amount);
    }
});

Then(/^I should see the corresponding remaining allowance (.*) deducted in the (remitter|recipient) (.*) account$/, (amount, type, toAccount) => {
    homePage.waitForPageLoad();
    if (type === 'remitter') {
        homePage.navigateAndValidateRemainingAllowanceAfterTransaction('from', getLocatorForIsa(toAccount), amount);
    } else if (type === 'recipient') {
        homePage.navigateAndValidateRemainingAllowanceAfterTransaction('to', getLocatorForIsa(toAccount), amount);
    }
});

Then(/^I should be navigated to the payment hub page with from account as (.*) in the payment hub page$/, function (accountName) {
    let accountNameData = accountName;
    if (accountName !== 'defaultPrimary') {
        const data = this.dataRepository.get();
        accountNameData = data[accountName];
    }
    paymentHubMainPage.waitForPaymentHubPageLoad();
    paymentHubMainPage.checkRecipientPrePopulation('from', accountNameData);
});

Then(/^I select the hard core back button in the transfer success page$/, () => {
    successTransferPage.waitForPageLoad();
    SuccessTransferPage.selectHardCoreBackButton();
});

Then(/^I should see internet transaction limit exceeded error message in payment hub page$/, () => {
    paymentHubMainPage.shouldSeeInternetTransactionLimitMessage();
});

Then(/^I should see account daily limit exceeded error message in payment hub page$/, () => {
    paymentHubMainPage.shouldSeeAccountDailyLimitMessage();
});

Then(/^I should see cbs prohibitive error message in payment hub page$/, () => {
    paymentHubMainPage.shouldSeeCbsProhibitiveMessage();
});

Then(/^I should see cbs prohibitive error message in payment unsuccessful page$/, () => {
    paymentUnsuccessfulPage.shouldSeeCbsProhibitiveMessage();
});

Then(/^I should see monthly cap limit exceeded error message in payment hub page$/, () => {
    paymentHubMainPage.shouldSeeMonthlyCapLimitMessage();
});

Then(/^I should see amount and payment date in the payment hub - pay a credit card screen$/, () => {
    paymentHubMainPage.paymentAmountFieldVisibility(true);
    paymentHubMainPage.paymentDateVisibility(true);
});

Given(/^I am (shown|not shown) payment date in the payment hub$/, (condition) => {
    paymentHubMainPage.paymentDateVisibility(condition === 'shown');
});

Then(/^I should see statement balance and minimum amount option in the payment hub - pay a credit card screen$/, () => {
    paymentHubMainPage.statementBalanceCreditCardVisibility(true);
    paymentHubMainPage.minimumAmountCreditCardVisibility(true);
});

Then(/^I select the confirm option in the review payment page$/, () => {
    reviewPaymentPage.waitForPageLoad();
    reviewPaymentPage.selectConfirmButton();
});

Then(/^I verify the reference prepopulated in review page$/, () => {
    reviewPaymentPage.setReference();
    commonPage.selectHeaderBackButton();
    paymentHubMainPage.validateReference();
    paymentHubMainPage.selectContinueButton();
});

Then(/^I should see the declined error message in the payment decline page$/, () => {
    declinedPaymentPage.waitForDeclinedPageLoad();
    declinedPaymentPage.shouldSeeDeclinedErrorMessage();
});

Then(/^I should see pre auth and back to logon in the payment decline page$/, () => {
    declinedPaymentPage.shouldSeeContactUsButton();
    declinedPaymentPage.shouldSeeBackToLoginButton();
});

Then(/^I should see the payment hold error message in the payment on hold page$/, () => {
    holdPaymentPage.waitForPaymentHoldPageLoad();
    holdPaymentPage.shouldSeeHoldPaymentErrorMessage();
});

Then(/^I select call us option on payment on hold page$/, () => {
    holdPaymentPage.waitForPaymentHoldPageLoad();
    holdPaymentPage.selectCallUsOnPaymentPage();
});
Then(/^I should see transfer and payment button in the payment on hold page$/, () => {
    holdPaymentPage.shouldSeeTransferPaymentButton();
});

Then(/^my new payee(.*) is not created$/, (recipientName) => {
    holdPaymentPage.selectTransferPaymentButton();
    paymentHubMainPage.selectToAccountPane();
    viewRecipientPage.searchAccount(recipientName);
    searchRecipientPage.verifyNoResultsMessage();
});

Then(/^I select the header home option in the payment hub page$/, () => {
    paymentHubMainPage.selectHeaderHomeButton();
});

Then(/^I should be on the payment hub page$/, () => {
    paymentHubMainPage.waitForPaymentHubPageLoad();
});

Then(/^I select the stay option in the payment hub page winback$/, () => {
    paymentHubMainPage.selectStayInWinback();
});

Then(/^I select the leave option in the payment hub page winback$/, () => {
    paymentHubMainPage.selectLeaveInWinback();
});

Then(/^I should see the internet connection lost error in transfer failure page$/, () => {
    failureTransferPage.waitForPageLoad();
    failureTransferPage.shouldSeeInternetLostErrorMessage();
});

Then(/^I should see the transfer and payment option in the transfer failure page$/, () => {
    failureTransferPage.shouldSeeTransferPaymentsButton();
});

Then(/^I should see the internet connection lost error in payment failure page$/, () => {
    failurePaymentPage.waitForPageLoad();
    failurePaymentPage.shouldSeeInternetLostErrorMessage();
});

Then(/^I should see the transfer and payment option in the payment failure page$/, () => {
    failurePaymentPage.shouldSeeTransferPaymentsButton();
});

const verifyAddBeneficiary = function () {
    paymentHubMainPage.verifyAddBeneficiary();
};
Then(/^I am on the Make Payment screen with newly added beneficiary selected$/, verifyAddBeneficiary);

Then(/^I should see error logged in page$/, () => {
    paymentHubMainPage.verifyErrorMessage();
});

When(/^I choose UK mobile number from account category page$/, () => {
    beneficiaryTypePage.chooseUkMobileNumber();
});

Then(/^I should see payment hub main page$/, () => {
    paymentHubMainPage.verifyPaymentHubMainPage();
});

Then(/^I am on the enter Payments details page$/, () => {
    paymentHubMainPage.shouldSeePaymentHubMainPageWithSCA();
});

When(/^I select the amount field$/, () => {
    paymentHubMainPage.selectAmountField();
});

Then(/^I should see my available balance$/, () => {
    paymentHubMainPage.helpTextForAmountField();
});

Then(/^I should see text advising the minimum and maximum amount$/, () => {
    paymentHubMainPage.verifyMinimumAndMaximumLimtText();
});

Then(/^I should see an error advising that the amount is outside of the transaction limits$/, () => {
    paymentHubMainPage.verifyErrorMessageForMinimumAndMaxmimumDailyLimit();
});

Then(/^I should be taken to the payments page$/, () => {
    paymentHubMainPage.waitForPaymentHubPageLoad();
});

When(/^I select the reference field in payment screen$/, () => {
    paymentHubMainPage.selectReferenceField();
});

Then(/^I should see some text advising that the reference will appear on the recipient's statement$/, () => {
    paymentHubMainPage.verifyTextAdvisingReferenceWillAppearOnStatements();
});

Then(/^I should be able to continue with the payment$/, () => {
    paymentHubMainPage.selectContinueButton();
});

When(/^I have not entered any reference text$/, () => {
    paymentHubMainPage.selectReferenceField();
});

Then(/^I should be able to continue$/, () => {
    paymentHubMainPage.selectContinueButton();
    reviewPaymentPage.waitForPageLoad();
    reviewPaymentPage.verifyReviewPaymentPage();
});

Then(/^I enter (.*) in the reference field in the payment hub page$/, (referenceText) => {
    paymentHubMainPage.enterReference(referenceText);
});

Then(/^I should see the review payment page$/, () => {
    reviewPaymentPage.waitForPageLoad();
    reviewPaymentPage.verifyReviewPaymentPage();
});

Then(/^I should see reference text visible on the review payment screen$/, () => {
    reviewTransferPage.verifyReferenceFieldInReviewTransferPage();
});

Then(/^I should see a payment confirmed screen$/, () => {
    successPaymentPage.waitForPageLoad();
    rateAppPaymentHub.closeRateAppPopUp();
    successPaymentPage.verifySuccessPaymentPage();
});

Then(/^I should see an option to view transactions$/, () => {
    successPaymentPage.verifyViewTransactionButtonInPaymentSuccessPage();
});

Then(/^I should see an option make another transfer$/, () => {
    successPaymentPage.verifyMakeAnotherTransferButtonInPaymentSuccessPage();
});

Then(/^I am on the review payment screen$/, () => {
    reviewPaymentPage.waitForPageLoad();
    reviewPaymentPage.verifyReviewPaymentPage();
});

When(/^I select Transfers and Payments$/, () => {
    successPaymentPage.selectMakeAnotherPaymentButton();
});

Then(/^I should be taken to Transfer and Payments page$/, () => {
    paymentHubMainPage.verifyPaymentHubMainPage();
});

Then(/^I enter (.*) in the amount field in the payment hub page for the selected account$/, (amountValue) => {
    paymentHubMainPage.enterAmount(amountValue);
});

Then(/^I am on the payment confirmed page$/, () => {
    successTransferPage.waitForPageLoad();
    successTransferPage.verifySuccessTransferPage();
});

Then(/^I should be taken to the remitting accounts statement$/, () => {
    accountStatementPage.verifyFirstTransactionVisible();
});

Then(/^I select View Transaction in transfer and payment page$/, () => {
    successPaymentPage.selectViewTransaction();
    paymentHubMainPage.closeErrorMessage();
});

Then(/^I should see the pending payment error in payment failure page$/, () => {
    failurePaymentPage.waitForPageLoad();
    failurePaymentPage.shouldSeePendingPaymentErrorMessage();
});

Then(/^I should see the recipients in alphanumeric order in view recipient page$/, () => {
    viewRecipientPage.shouldSeePayeeInAlphabeticOrder();
});

Then(/^I select the recipient option in the payment hub page$/, () => {
    paymentHubMainPage.waitForPaymentHubPageLoad();
    paymentHubMainPage.selectPayAccount('recipient');
});

Then(/^I should see the recipients should refresh with top of screen in view recipient page$/, () => {
    viewRecipientPage.waitForViewRecipientPageLoad();
    viewRecipientPage.canSeeTopOfPage();
});

Then(/^I should see the values pre-populated in the payment review page for transfer with amount value (.*)$/, (amount) => {
    reviewPaymentPage.shouldContainAmountValue(amount);
});

Then(/^I clear the amount field in the payment hub page$/, () => {
    paymentHubMainPage.clearAmountField();
    Actions.tapOutOfField();
});

Then(/^I tap out of the field in the payment hub page$/, () => {
    Actions.tapOutOfField();
});

Then(/^I should be on the success transfer page$/, () => {
    successTransferPage.waitForPageLoad();
});

Then(/^I should be on the success payment page$/, () => {
    successPaymentPage.waitForPageLoad();
});

Then(/^I clear and enter (.*) in the reference field in the payment hub page$/, (referenceText) => {
    paymentHubMainPage.clearReference();
    paymentHubMainPage.enterReference(referenceText);
});

Then(/^I should see only 18 characters in the reference field in the payment hub page$/, () => {
    paymentHubMainPage.shouldSeeOnlyEighteenCharForReference();
});

Then(/^I should see reference field with (.*) in the payment hub page$/, (ref) => {
    paymentHubMainPage.shouldSeeReferenceFieldWithText(ref);
});

const amOnIsaPaymentHubPage = function (account) {
    homePage.swipeToAccount(getLocatorForIsa(account));

    homePage.selectActionMenu();
    actionMenuPage.selectActionMenuTopUpIsa();

    paymentHubMainPage.waitForPaymentHubPageLoad();
};

const saveAndAmOnIsaPaymentHubPage = function (account) {
    amOnIsaPaymentHubPage(account);
    paymentHubMainPage.setRemainingAllowanceForToIsa();
};

Then(/^I am on the payment hub page for (.*) account choosing top up isa from action menu$/, amOnIsaPaymentHubPage);

Then(/^I am on the payment hub page for (.*) account choosing top up isa from action menu from the corresponding account$/, saveAndAmOnIsaPaymentHubPage);

Then(/^I should see the (.*) warning message in the review transfer page$/, (type) => {
    if (type === 'htbIsa') {
        reviewTransferPage.waitForPageLoad();
        reviewTransferPage.shouldSeeHtbIsaWarningMessage();
    } else {
        reviewTransferPage.waitForPageLoad();
        reviewTransferPage.shouldSeeTopupIsaWarningMessage();
    }
});

Then(/^I should see view transaction and make another transfer option in the transfer success page$/, () => {
    successTransferPage.waitForPageLoad();
    successTransferPage.verifyTransactionButton();
    successTransferPage.verifyMakeAnotherTransferButton();
});

Then(/^I should see view transaction and make another payment option in the payment success page$/, () => {
    successPaymentPage.waitForPageLoad();
    successPaymentPage.verifyViewTransactionButtonInPaymentSuccessPage();
    successPaymentPage.verifyMakeAnotherTransferButtonInPaymentSuccessPage();
});

When(/^I should be on the direct Debit webview page$/, () => {
    directDebit.waitForPageLoad();
});

When(/^I should be on the standing Order webview page$/, () => {
    standingOrder.waitForPageLoad();
});

When(/^I should be on the save the changes webview page$/, () => {
    saveTheChanges.waitForPageLoad();
});

When(/^I should be able to navigate back to home screen from save the changes webview page$/, () => {
    saveTheChanges.Clickhomebutton();
});

When(/^I select to set up a new standing order$/, () => {
    standingOrder.selectSetUpNewStandingOrder();
});

When(/^I navigate to (payment success|payment unsuccessful|transfer success|transfer unsuccessful) page$/, (type) => {
    homePage.selectPayAndTransfer();
    paymentHubMainPage.selectToAccountPane();
    if (type === 'payment success' || type === 'payment unsuccessful') {
        searchRecipientPage.enterSearchText('ATWELL');
        searchRecipientPage.selectBannerRecipientForPayment();
    } else if (type === 'transfer success' || type === 'transfer unsuccessful') {
        searchRecipientPage.enterSearchText('Easy');
        searchRecipientPage.selectBannerAccountToTransfer();
    } else {
        throw new Error('It is an unrecognized state of payment/transfer');
    }
    paymentHubMainPage.enterAmount(transferAmount);
    paymentHubMainPage.selectContinueButton();
    paymentReviewPage.confirmPayment();
});

Then(/^I should be able to see the banner lead placement on (payment|transfer) (success|unsuccessful) page$/, (page, result) => {
    if (page === 'payment') {
        if (result === 'success') {
            successPaymentPage.waitForPageLoad();
            paymentSuccessPage.viewBannerPlacement();
        } else if (result === 'unsuccessful') {
            paymentUnsuccessfulPage.viewBannerPlacement();
        }
    } else if (page === 'transfer') {
        if (result === 'success') {
            successTransferPage.waitForPageLoad();
            transferSuccessPage.viewBannerPlacement();
        } else if (result === 'unsuccessful') {
            transferUnsuccessfulPage.viewBannerPlacement();
        }
    } else {
        throw new Error(`${page} is an unrecognized page`);
    }
});

Then(/^I should see withdraw from ISA to non lbg account warning message on review payment screen$/, () => {
    reviewPaymentPage.waitForPageLoad();
    reviewPaymentPage.shouldSeeWithdrawFromIsaToNonLbgAccountWarningMessage();
});

Then(/^I should see withdraw from ISA to non ISA warning message on review transfer screen$/, () => {
    reviewTransferPage.waitForPageLoad();
    reviewTransferPage.shouldSeeWithdrawFromIsaToNonIsaAccountWarningMessage();
});

Then(/^I should see withdraw from ISA to ISA warning message on review transfer screen$/, () => {
    reviewTransferPage.waitForPageLoad();
    reviewTransferPage.shouldSeeWithdrawFromIsaToIsaAccountWarningMessage();
});

Then(/^I should see confirm button disabled in the review transfer screen$/, () => {
    reviewTransferPage.waitForPageLoad();
    reviewTransferPage.shouldSeeConfirmButtonDisabled();
});

Then(/^I should see blocked ISA payment warning message on transfer unsuccessful page$/, () => {
    failurePaymentPage.waitForPageLoad();
    failurePaymentPage.shouldSeeBlockISAPaymentErrorMessage();
});

Then(/^I select ISA transfer agreement checkbox and confirm option in the review (transfer|payment) page$/, (type) => {
    if (type === 'transfer') {
        reviewTransferPage.waitForPageLoad();
    } else {
        reviewPaymentPage.waitForPageLoad();
    }
    reviewPaymentPage.scrollToISATransferCheckBox();
    reviewPaymentPage.selectISATransferAgreementCheckBox();
    if (type === 'transfer') {
        reviewTransferPage.selectConfirmButton();
    } else {
        reviewPaymentPage.selectConfirmButton();
    }
});

When(/^I should see the (transfer|payment) successful message page$/, (type) => {
    if (type === 'transfer') {
        rateAppPaymentHub.closeRateAppPopUp();
        successTransferPage.waitForPageLoad();
    } else {
        rateAppPaymentHub.closeRateAppPopUp();
        successPaymentPage.waitForPageLoad();
    }
});

Then(/^I should see the min ISA error message on payment hub page$/, () => {
    paymentHubMainPage.verifyMinISATransferErrorMessage();
});

Then(/^I should see the max ISA transfer error message on payment hub page$/, () => {
    paymentHubMainPage.verifyMaxISATransferErrorMessage();
});

Then(/^I should see the funding limit more than limit error message on the error page$/, () => {
    failureTransferPage.waitForMoreThanFundingLimitHTBPageLoad();
    failureTransferPage.canSeeHtbIsaAboveFundLimitErrorMessage();
});

Then(/^I select the home option in the error page$/, () => {
    errorPage.selectHomeButton();
});

Then(/^I select the (.*) with zero remaining allowance as recipient in payment hub page$/, function (accountName) {
    const data = this.dataRepository.get();
    paymentHubMainPage.selectPayAccount('recipient');
    viewRecipientPage.selectIsaWithRemainingAllowanceZero(data[accountName]);
});

Then(/^I should see already subscribed to isa error message in transfer failure page$/, () => {
    failureTransferPage.waitForIsaFailurePageLoad();
    failureTransferPage.shouldSeeIsaAlreadySubscribedErrorMessage();
});

Then(/^I should be on the payment Blocked page$/, () => {
    paymentBlockedPage.waitForPageLoad();
});

When(/^I review the transfer to ISA account in review transfer page$/, () => {
    transferReviewPage.reviewTransfer();
});

When(/^I should see renew warning page$/, () => {
    isaWarningPage.verifyRenewWarning();
});

When(/^I navigate back from standing order and direct debit page$/, () => {
    standingOrder.waitForPageLoad();
    standingOrder.clickBackButton();
});

Then(/^I should be on the (.*) web view page$/, (orderType) => {
    if (orderType === 'standingOrder') {
        standingOrder.waitForPageLoad();
    } else if (orderType === 'directDebit') {
        directDebit.waitForPageLoad();
    } else {
        throw new Error('Type does not match');
    }
});

When(/^I select calendar picker option for future date payment$/, () => {
    paymentHubMainPage.selectCalendarPicker();
});

Then(/^I should see a calendar view$/, () => {
    paymentHubCalendarPage.waitForPageLoad();
});

Then(/^I should be allowed to select a payment date up to 31 days from current day$/, () => {
    paymentHubCalendarPage.canSelectPaymentUptoMonth();
});

Then(/^I select cancel in the calendar pop up$/, () => {
    paymentHubCalendarPage.cancelCalendarPopUP();
});

Then(/^other dates must not be able to be selected$/, () => {
    paymentHubCalendarPage.cantSelectPaymentMoreThanMonth();
});

When(/^I select next month chevron from the top header of calendar view$/, () => {
    paymentHubCalendarPage.selectNextMonth();
});

Then(/^I should be able to navigate to next month calendar$/, () => {
    paymentHubCalendarPage.shouldSeeNextMonth();
});

When(/^I have the dates where I can pay in the previous month$/, () => {
    paymentHubCalendarPage.selectNextMonth();
});

When(/^I select previous month chevron from the top header of calendar view$/, () => {
    paymentHubCalendarPage.selectPreviousMonth();
});

Then(/^I should be able to navigate to previous month calendar$/, () => {
    paymentHubCalendarPage.shouldSeePrevMonth();
});

When(/^I select the date as (\d+) from today's date from calendar view$/, (days) => {
    paymentHubCalendarPage.waitForPageLoad();
    paymentHubCalendarPage.selectFuturePaymentDate(days);
});

When(/^I submit future date payment with date as (\d+) days after today$/, submitFutureDatePayment);

Then(/^the date field should be populated with the selected date on the payment hub$/, () => {
    paymentHubMainPage.waitForPaymentHubPageLoad();
    paymentHubMainPage.shouldSeeDatePopulated();
});

Then(/^the future dated payment should display (.*) in text$/, (date) => {
    paymentHubMainPage.waitForPaymentHubPageLoad();
    paymentHubMainPage.shouldSeeTomorrowPopulated(date);
});

When(/^I select Continue button on payment hub page$/, () => {
    paymentHubMainPage.selectContinueButton();
});

Then(/^I am shown notice warning message in the payment hub page$/, () => {
    paymentHubMainPage.shouldSeeNoticeWarningMessage();
});

Then(/^I am shown 'When' the payment will be made on payment (success|review) page$/, (type) => {
    const futureDate = PaymentHubMainPage.getFuturePaymentDate();
    if (type === 'review') {
        reviewPaymentPage.shouldSeeDateValue(futureDate);
    } else if (type === 'success') {
        successPaymentPage.shouldSeeDateValue(futureDate);
    }
});

Then(/^I am shown information of payment date if it falls on a holiday$/, () => {
    reviewPaymentPage.shouldSeeFuturePaymentMessage();
});

const navigateBackToHomePage = function () {
    paymentHubMainPage.navigateBackToHomePage();
};

Then(/^I go back to home page$/, navigateBackToHomePage);

When(/^I select the reactivate ISA option from the reactivate ISA interstitial page$/, () => {
    reactivateIsaInterstitialPage.waitForPageLoad();
    reactivateIsaInterstitialPage.selectReactivateIsaButton();
});

Then(/^I should be on the reactivate ISA declaration page$/, () => {
    reactivateIsaPage.waitForPageLoad();
});

When(/^I should be on the review transfer page$/, () => {
    reviewTransferPage.waitForPageLoad();
});

Then(/^I submit the payment with amount value (.*) and reference$/,
    saveAndSubmitPaymentDetails);

Then(/^I submit the P2P payment with amount value (.*) and reference$/,
    saveAndSubmitP2PPaymentDetails);

Then(/^I should see statement balance and minimum amount with instalment plan option in the payment hub - pay a credit card screen$/, () => {
    paymentHubMainPage.statementBalanceWithInstalmentPlanVisibility(true);
    paymentHubMainPage.minimumAmountWithInstalmentPlanVisibility(true);
});

Then(/^I select view standing orders in standing order setup successful page$/, () => {
    successPaymentPage.selectViewStandingOrders();
});

Then(/^I verify the remaining allowance (to|from) ISA account after Transaction for the amount value (.*)$/, (type, amount) => {
    paymentHubMainPage.verifyISARemainingAllowance(type, amount);
});

When(/^I delete beneficiary (.*)$/, (recipient) => {
    successPaymentPage.waitForPageLoad();
    successPaymentPage.selectMakeAnotherPaymentButton();
    paymentHubMainPage.selectToAccountPane();
    searchRecipientPage.selectManageRecipient(recipient);
    searchRecipientPage.deleteRecipient();
});

Given(/^I choose a recepient account on the payment hub page$/, () => {
    paymentHubMainPage.selectPayAccount('recipient');
});

When(/^I am shown a list of eligible accounts I can transfer to$/, () => {
    paymentHubMainPage.verifyListOfEligibleRemitterAccount();
});

Given(/^I am shown a list of all eligible accounts to make payment from$/, () => {
    paymentHubMainPage.verifyListOfEligibleRemitanceAccount();
});

Given(/^I am shown a list of eligible accounts and recipients I can pay$/, () => {
    paymentHubMainPage.verifyListOfEligibleRemitterAccount();
});

Given(/^I select an (.*) to transfer from$/, function (account) {
    const data = this.dataRepository.get();
    viewRemitterPage.selectAccount(data[account]);
});

When(/^I select the (.*) I want to transfer to$/, function (account) {
    const data = this.dataRepository.get();
    paymentHubMainPage.chooseRemitterAccount(data[account]);
});

Given(/^recipients listed in alphabetical order with available accounts at the top$/, () => {
    viewRecipientPage.shouldSeePayeeInAlphabeticOrder();
});

Then(/^I should be shown accounts eligible to make a transfer to, from the business currently selected$/, () => {
    paymentHubMainPage.verifyListOfEligibleRemitterAccount();
});

When(/^I delete (.*), (.*) beneficiary from the list$/, (number, recipient) => {
    paymentHubMainPage.selectToAccountPane();
    searchRecipientPage.deleteRecipient(number, recipient);
});

Then(/^I should see the internet connection lost error in payment hub page$/, () => {
    paymentHubMainPage.verifyInternetLostMessage();
});

Then(/^I am unable to edit the Reference field and displays the stored reference set$/, verifyReferenceNonEditable);

When(/^I should be on the standing orders page$/, () => {
    standingOrder.waitForPageLoad();
});

Then(/^I should be on upcoming payments page$/, () => {
    upComingPaymentPage.waitForPageLoad();
});

Then(/^I should see the (.*) transaction for (.*) and (.*)$/, function (transactionType, account, amount) {
    if (transactionType === 'Standing order') {
        const data = this.dataRepository.get();
        upComingPaymentPage.verifyUpcomingPayment(transactionType, data[account], amount);
    } else {
        upComingPaymentPage.verifyUpcomingPayment(transactionType, account, amount);
    }
});

Then(/^I should see Outbound or Insufficient credit card payment validation error message$/, () => {
    const amountValue = parseFloat(paymentHubMainPage.getCreditCardAmount()) + 1;
    const accountBalance = parseFloat(paymentHubMainPage.getRemitterAmount());
    if (accountBalance < amountValue) {
        paymentHubMainPage.continueFraudsterWarningIfPresent();
        paymentHubMainPage.shouldSeeInsufficientMoneyErrorMessage();
        paymentHubMainPage.closeErrorMessage();
    } else {
        paymentHubMainPage.validateCreditCardPaymentErrorMessage();
        paymentHubMainPage.closeErrorMessage();
    }
});

Given(/^I navigate to the remitter account on the payment hub page$/, () => {
    homePage.selectPayAndTransfer();
    PaymentTask.selectFromOrToAccount('remitter');
});

Given(/^I navigate to add new recipient option using payment hub$/, PaymentTask.navigateToAddNewRecipient);

When(/^I select a tooltip icon to informing me about overpayments$/, () => {
    paymentHubMainPage.selectLearnAboutOverpaymentToolTip();
});

When(/^I select a tooltip icon to informing me about sub-accounts$/, () => {
    paymentHubMainPage.selectSubAccountLearnAboutOverpaymentToolTip();
});

Then(/^I should see more information alert pop up about (mortgage overpayments|sub-accounts)$/, (type) => {
    paymentHubMainPage.shouldSeeInformationAboutOverpayment(type);
    paymentHubMainPage.selectLeaveInWinback();
});

Then(/^I should see the pay a sub-account toggle in payment hub page$/, () => {
    paymentHubMainPage.shouldSeeSubAccountToggle();
});

Then(/^I enable the toggle to pay a sub-account$/, () => {
    paymentHubMainPage.selectSubAccountToggle();
});

Then(/^I select on the toggle to pay a sub-account$/, () => {
    paymentHubMainPage.selectASubAccountFromSubAccountList();
});

Then(/^I should see a list of all my sub-accounts$/, () => {
    paymentHubMainPage.shouldSeeSubAccountsListed();
});

Then(/^None of the sub-account should be pre-selected$/, () => {
    paymentHubMainPage.shouldSeeSubAccountsUnSelected();
});

Then(/^the balance and interest rate should be shown for each eligible sub account$/, () => {
    paymentHubMainPage.shouldSeeSubAccountsInformation();
});

Then(/^I see the fraudster warning message pop up on the payment hub page$/, () => {
    paymentHubMainPage.waitForFraudsterWarningMessage();
});

Then(/^I should see 60 minutes lapse error message on payment hub$/, () => {
    paymentHubMainPage.validate60MinutesLapseErrorMessage();
});

Then(/^I should see 60 minutes lapse error message with over payment warning on payment hub$/, () => {
    paymentHubMainPage.validate60MinutesLapseWithOverPaymentErrorMessage();
});

Then(/^I make a payment of (.*) to my (.*) (account|sub-account)$/, (amountValue, accountType, account) => {
    homePage.swipeToAccount(AccountMatcher.getAccount(accountType));
    homePage.selectActionMenuOfSpecificAccount(AccountMatcher.getAccount(accountType.replace(/ /g, '')));
    actionMenuPage.selectMakeAnOverpaymentOption();
    enterAmount(amountValue);
    paymentHubMainPage.setAccountDetailsForPayment(accountType);
    if (account === 'sub-account') {
        paymentHubMainPage.selectSubAccountToggle();
        paymentHubMainPage.selectASubAccountFromSubAccountList();
    }
    paymentHubMainPage.setMortgageReferenceNumber();
    selectContinueButton();
});

Then(/^I should see the mortgage account along with the mortgage reference number in the to section$/, () => {
    paymentHubMainPage.shouldSeeMortgageAccount();
    successPaymentPage.shouldSeeReferenceFieldInSuccessPage();
});

When(/^I go back and come to UK beneficiary details page again$/, () => {
    commonPage.selectHeaderBackButton();
    beneficiaryTypePage.chooseUkAccountBeneficiary();
});

Then(/^I should see Payment Scheduled success page$/, () => {
    paymentSuccessPage.waitForPageLoad();
    successPaymentPage.shouldSeeScheduledSuccessPaymentPage();
});

Then(/^I submit the payment with all the mandatory fields$/, () => {
    const ibcPaymentLimit = SavedAppDataRepository.getData(SavedData.TRANSACTION_LIMIT_EXCEEDED_AMOUNT);
    enterAmount(ibcPaymentLimit);
    selectContinueButton();
});

Then(/^I fetch the payment limit for (.*) account from IBC$/, (accName) => {
    const account = browser.capabilities.brand === 'LDS' ? AccountMatcher.getAccount(accName) : 'Default';
    let ibcPaymentLimit = EnvironmentApiClient.fetchPaymentLimit(
        browser.capabilities.server,
        browser.capabilities.appType,
        browser.capabilities.brand,
        account
    );
    ibcPaymentLimit = parseInt(ibcPaymentLimit.paymentLimit, 10) + 1;
    ibcPaymentLimit = ibcPaymentLimit.toFixed(2);
    SavedAppDataRepository.setData(SavedData.TRANSACTION_LIMIT_EXCEEDED_AMOUNT,
        ibcPaymentLimit);
});

Then(/^I am shown following account daily limits for the account in view payments limit page$/, (table) => {
    const accountDailyLimits = table.hashes();
    viewPaymentLimitsPage.validateAccountDailyLimits(accountDailyLimits);
});

Then(/^I select back buttom from view payment limit page$/, () => {
    commonPage.selectModalHeaderBackButton();
});

Then(/^I validate (.*) is deducted from daily payment limit after transaction$/, (amount) => {
    viewPaymentLimitsPage.validateDailyPaymentLimitAfterTransaction(amount);
});
