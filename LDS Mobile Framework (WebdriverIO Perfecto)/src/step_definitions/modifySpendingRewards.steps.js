const {Then} = require('cucumber');
const ModifySpendingRewardsWebViewpage = require('../pages/modifySpendingRewardswebview.page');

const modifySpendingRewardsWebViewPage = new ModifySpendingRewardsWebViewpage();

Then(/^I should be on the modify spending rewards webview page$/, () => {
    modifySpendingRewardsWebViewPage.waitForPageLoad();
});
