const {When, Then} = require('cucumber');
require('chai')
    .should();
const PaymentHubMainPage = require('../pages/paymentHub/paymentHubMain.page');
const UkBeneficiaryDetailPage = require('../pages/transferAndPayments/ukBeneficiaryDetail.page');

const paymentHubMainPage = new PaymentHubMainPage();
const ukBeneficiaryDetailPage = new UkBeneficiaryDetailPage();

Then(/^I am not shown the option to pay using debit card$/, () => {
    ukBeneficiaryDetailPage.verifyNoDebitCardOption();
});

Then(/^I am on the payment hub page with data prepopulated$/, () => {
    paymentHubMainPage.waitForPaymentHubPageLoad();
});

When(/^I submit the pccdc with all the mandatory fields with corresponding amount value (.*)$/, (amount) => {
    paymentHubMainPage.enterAmount(amount);
    paymentHubMainPage.selectContinueButton();
});

When(/^I submit the pccmc with all the mandatory fields with corresponding amount value (.*)$/, (amount) => {
    paymentHubMainPage.enterAmount(amount);
});

Then(/^I should be on select your bank account page and verify fallowing headers and footer details populated on the page$/, (yourBankAccountDetails) => {
    const tableData = yourBankAccountDetails.hashes();
    ukBeneficiaryDetailPage.shouldSeeYourBankAccountDetails(tableData);
});

When(/^I select another UK (.*) from the list displayed$/, (bankAccount) => {
    ukBeneficiaryDetailPage.selectAnotherUKBankAccount(bankAccount);
});

Then(/^I should see the following details populated on the confirm payment screen$/, (confirmPreviewDetails) => {
    const tableData = confirmPreviewDetails.hashes();
    ukBeneficiaryDetailPage.shouldSeeConfirmPaymentDetails(tableData);
});

When(/^I select the confirm button on confirm payment page$/, () => {
    ukBeneficiaryDetailPage.selectConfirmPaymentButton();
});

Then(/^I should see the fallowing details on approve your payment page$/, (approveYourPayment) => {
    const tableData = approveYourPayment.hashes();
    ukBeneficiaryDetailPage.shouldSeeDetailsOnApproveYourPaymentPage(tableData);
});

When(/^I select check box option to continue the payment on approve your payment page$/, () => {
    ukBeneficiaryDetailPage.selectConfirmPaymentCheckBox();
});

When(/^I select continue button on approve your payment page$/, () => {
    ukBeneficiaryDetailPage.selectContinueButtonApproveYourPayment();
});

When(/^I should see the go to your bank dialog box on approve your payment page$/, () => {
    ukBeneficiaryDetailPage.shouldSeeGoToYourBankDialogBox();
});

When(/^I select the dialog box continue button to finish your payment$/, () => {
    ukBeneficiaryDetailPage.selectDialogBoxContinueButton();
});

When(/^I should be on unfinished payment page$/, () => {
    ukBeneficiaryDetailPage.shouldSeeUnfinishedPaymentPage();
});

Then(/^I select the select provider button in the payment hub page$/, () => {
    paymentHubMainPage.selectContinueButton();
});

Then(/^I select amount input field on right$/, () => {
    ukBeneficiaryDetailPage.selectAmountInputField();
});

Then(/^I should see below options in the action menu of select amount to pay$/, (selectAmountOptions) => {
    const tableData = selectAmountOptions.hashes();
    ukBeneficiaryDetailPage.shouldSeeAmountToPayOptions(tableData);
});

Then(/^I select cancel button on select amount to pay$/, () => {
    ukBeneficiaryDetailPage.selectCancelButton();
});
