/* eslint-disable no-undef */
const {When, Then} = require('cucumber');
const ShareIBANPage = require('../pages/shareIBAN.page');
const AccountSelectionPage = require('../pages/accountSelection.page');

const shareIbanPage = new ShareIBANPage();
const accountSelectionPage = new AccountSelectionPage();

Then(/^I should be able to see Send Bank Details Page$/, () => {
    shareIbanPage.waitForPageLoad();
});

Then(/^there is a warning “Keep your details safe\. Only send to people you know”$/, () => {
    shareIbanPage.shouldSeeWarningMessage();
});

Then(/^under International Bank details we can see Account Name, IBAN, BIC$/, () => {
    shareIbanPage.shouldSeeInternationalAccountDetails();
});

Then(/^there is an option to share these International Bank Details$/, () => {
    shareIbanPage.shouldSeeShareIBANDetailsButton();
});

Then(/^under UK Bank Details we can see Name on the account, Sort Code and Account Number$/, () => {
    shareIbanPage.shouldSeeUKDetailsPresent();
});

Then(/^there is an option to share these UK Bank details$/, () => {
    shareIbanPage.shouldSeeShareUKBankDetailsButton();
});

When(/^I click Send International Details button$/, () => {
    shareIbanPage.selectIntBankDetailsButton();
});

When(/^I click close icon$/, () => {
    shareIbanPage.selectCloseIcon();
});

Then(/^I can see only UK Bank Details$/, () => {
    shareIbanPage.shouldSeeUKDetailsPresent();
});

Then(/^I see an error message "([^"]*)"$/, (errorMessage) => {
    shareIbanPage.shouldSeeErrorMessage(errorMessage);
});

Then(/^I should not see the Send Bank Details Page$/, () => {
    shareIbanPage.shouldNotSeeTitle();
});

When(/^I click Send UK Details Button$/, () => {
    shareIbanPage.selectUKBankDetailsButton();
});

Then(/^I should be able to see Search results that say "([^"]*)"$/, (ibanMessage) => {
    shareIbanPage.shouldSeeIBANSearchResult(ibanMessage);
});

Then(/^another result that says "([^"]*)"$/, (bicMessage) => {
    shareIbanPage.shouldSeeBicSearchResult(bicMessage);
});

Then(/^I should not see Search Results for Bic and IBAN$/, () => {
    shareIbanPage.shouldNotSeeBicandIbanSearchResult();
});

Then(/^I select the IBAN Search Result$/, () => {
    shareIbanPage.selectIBANSearchResult();
});

Then(/^I should land on Account Selection Page$/, () => {
    accountSelectionPage.waitForPageLoad();
    accountSelectionPage.shouldSeeAccountDetails();
});

When(/^I select the first Current Account being displayed$/, () => {
    accountSelectionPage.shouldSeeAccountDetails();
    accountSelectionPage.selectOnFirstSearchResult();
});

When(/^I select the Bic Search Result$/, () => {
    shareIbanPage.selectBicSearchResult();
});

Then(/^Account Selection Page displays a warning$/, () => {
    accountSelectionPage.shouldSeeWarningMessage();
});
