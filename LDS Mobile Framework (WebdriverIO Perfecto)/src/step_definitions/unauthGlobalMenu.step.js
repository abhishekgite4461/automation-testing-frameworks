const {When} = require('cucumber');
const LoginPage = require('../pages/login.page');
const MemorableInformationPage = require('../pages/memorableInformation.page');
const CreateMemorableInformationPage = require('../pages/createMemorableInformation.page');
const CardReaderRespondPage = require('../pages/enrolmentCardReaderRespond.page');
const CardReaderIdentifyPage = require('../pages/enrolmentCardReaderIdentify.page');

const loginPage = new LoginPage();
const memorableInformationPage = new MemorableInformationPage();
const cardReaderRespondPage = new CardReaderRespondPage();
const cardReaderIdentifyPage = new CardReaderIdentifyPage();
const createMemorableInformationPage = new CreateMemorableInformationPage();

When(/^I select the contact us option from the MI page pre auth header$/, () => {
    memorableInformationPage.selectContactUs();
});

When(/^I select the contact us option from the login page pre auth header$/, () => {
    loginPage.selectContactUs();
});

When(/^I select the contact us option from the card reader identify page pre auth header$/, () => {
    cardReaderIdentifyPage.waitForPageLoad();
    cardReaderIdentifyPage.selectContactUs();
});

When(/^I select the contact us option from the card reader respond page pre auth header$/, () => {
    cardReaderRespondPage.waitForPageLoad();
    cardReaderRespondPage.selectContactUs();
});

When(/^I select the contact us option from the create memorable information page pre auth header$/, () => {
    createMemorableInformationPage.waitForPageLoad();
    createMemorableInformationPage.selectContactUs();
});
