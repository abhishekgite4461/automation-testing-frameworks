const {When, Then} = require('cucumber');

const RenameAccountPage = require('../pages/renameAccount.page');

const renameAccountPage = new RenameAccountPage();

Then(/^I should be on Rename account page$/, () => {
    renameAccountPage.waitForPageLoad();
});

Then(/^the Rename button is disabled$/, () => {
    renameAccountPage.shouldSeeRenameButtonDisabled();
});

When(/^I enter new account name (.*) in the text field$/, (newAccountName) => {
    renameAccountPage.enterAccountName(newAccountName);
});

When(/^I select Rename button$/, () => {
    renameAccountPage.selectRenameButton();
});

Then(/^confirmation box is displayed$/, () => {
    renameAccountPage.shouldSeeConfirmationBoxEnabled();
});

When(/^I select Back to your accounts button on confirmation box$/, () => {
    renameAccountPage.selectBackToYourAccountsButton();
});

Then(/^I should see (.*) account name in text field$/, (accountName) => {
    renameAccountPage.verifyExistingAccountName(accountName);
});
