const {When} = require('cucumber');
const AppWarnPage = require('../pages/appWarn.page');

const appWarnPage = new AppWarnPage();

When(/^I navigate to the warn screen$/, () => {
    appWarnPage.waitForAppWarnPageLoad();
});

When(/^I should see an option to update the app in the warn screen$/, () => {
    appWarnPage.verifyUpdateAppButton();
});

When(/^I should see an option to continue without updating in the warn screen$/, () => {
    appWarnPage.verifyContinueWithoutUpdatingButton();
});

When(/^I choose to continue without updating from the warn screen$/, () => {
    appWarnPage.clickContinueWithoutUpdatingButton();
});

When(/^I choose to update the app from the warn screen$/, () => {
    appWarnPage.clickUpdateAppButton();
});
