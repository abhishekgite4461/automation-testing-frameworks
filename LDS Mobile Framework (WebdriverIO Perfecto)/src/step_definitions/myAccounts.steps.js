const {When, Then} = require('cucumber');
const MyAccountsPage = require('../pages/callUs/myAccounts.page');

const myAccountsPage = new MyAccountsPage();

Then(/^I should be shown below accounts I hold$/, (table) => {
    myAccountsPage.waitForPageLoad();
    const arrangementTypes = table.hashes()
        .map((entry) => entry.accountTypes);

    myAccountsPage.viewArrangements(arrangementTypes);
});

When(/^I select the (.*) tile on your accounts page$/, (type) => {
    myAccountsPage.verifyArrangement(type);
});

When(/^I choose (.*) tile from your accounts page$/, function (accountName) {
    const data = this.dataRepository.get();
    myAccountsPage.chooseAccount(data[accountName]);
});
