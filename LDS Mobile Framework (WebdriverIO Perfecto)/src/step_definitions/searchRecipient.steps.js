const {Given, When, Then} = require('cucumber');
const SearchRecipientPage = require('../pages/searchAccountAndRecipient.page');
const UkBeneficiaryDetailPage = require('../pages/transferAndPayments/ukBeneficiaryDetail.page.js');
const UkCompanySearchResultsPage = require('../pages/transferAndPayments/ukCompanySearchResults.page.js');
const ConfirmPasswordPage = require('../pages/confirmAuthenticationPassword.page');
const PaymentHubMainPage = require('../pages/paymentHub/paymentHubMain.page');
const PaymentTask = require('../tasks/payment.task');
const YourAccountsTask = require('../tasks/yourAccounts.task');
const BeneficiaryTypePage = require('../pages/transferAndPayments/beneficiaryType.page');

const paymentHubMainPage = new PaymentHubMainPage();
const ukBeneficiaryDetailPage = new UkBeneficiaryDetailPage();
const ukCompanySearchResultsPage = new UkCompanySearchResultsPage();
const confirmPasswordPage = new ConfirmPasswordPage();
const beneficiaryTypePage = new BeneficiaryTypePage();
const searchRecipientPage = new SearchRecipientPage();

When(/^I see option to search payees$/, () => {
    searchRecipientPage.verifySearchBarPresent();
});

When(/^I choose the option to search payees$/, () => {
    searchRecipientPage.selectSearchBar();
});

Then(/^I (should|should not) see an option to add a new recipient$/, (type) => {
    searchRecipientPage.verifyAddNewPayee(type);
});

Then(/^I choose to Add new payee on search recipient page$/, () => {
    searchRecipientPage.selectAddNewPayee();
});

Then(/^I see list of Accounts in search page$/, () => {
    searchRecipientPage.verifyRecipientCell();
});

Then(/^I am on search recipient page$/, () => {
    searchRecipientPage.verifySearchRecipientPage();
});

Then(/^I see a message that I have no recipients set up on the account$/, () => {
    searchRecipientPage.verifyAddNewPayeeFooter();
});

When(/^I select the search resultant account$/, function (searchTable) {
    for (const searchItem of searchTable.hashes()) {
        const data = this.dataRepository.get();
        searchRecipientPage.selectResultantAccount(data[searchItem.results]);
    }
});

When(/^I select Cancel$/, () => {
    ukBeneficiaryDetailPage.cancelWinBack();
});

Then(/^I should see a winback option on top of the screen with Cancel and OK option$/, () => {
    ukBeneficiaryDetailPage.viewWinBackModal();
});

Then(/^on selection of Cancel option I should stay on the same screen$/, () => {
    ukBeneficiaryDetailPage.verifyWinBackOnNegativeAction();
});

Then(/^on selection of OK option I should navigate to account category page$/, () => {
    ukBeneficiaryDetailPage.verifyWinBackOnPositiveAction();
});

Then(/^I should see the following details in New UK beneficiary details page$/, (table) => {
    const tableData = table.hashes();
    ukBeneficiaryDetailPage.verifyUkCompanyDetails(tableData);
});

Then(/^I should see winBack scenarios on uk beneficiary details page$/, () => {
    ukBeneficiaryDetailPage.verifyWinBackOnNegativeAction();
    ukBeneficiaryDetailPage.verifyWinBackOnPositiveAction();
});

When(/^I navigate back on New UK beneficiary details page$/, () => {
    ukBeneficiaryDetailPage.verifyBackJourney();
});

Then(/^I should see the account category page$/, () => {
    ukBeneficiaryDetailPage.verifyBeneficiaryType();
});

When(/^I select the remitting account$/, () => {
    ukBeneficiaryDetailPage.selectRemittingAccount();
});

Then(/^I should see list of eligible accounts that can add a new recipient$/, () => {
    ukBeneficiaryDetailPage.verifyEligibleRemittingAccounts();
});

When(/^I verify search results for each search term$/, function (searchTable) {
    for (const searchItem of searchTable.hashes()) {
        const data = this.dataRepository.get();
        searchRecipientPage.clearSearch();
        searchRecipientPage.enterSearchText(searchItem.searchTerm);
        searchRecipientPage.verifySearchResults(
            searchItem.searchTerm,
            searchItem.searchType, data[searchItem.results]
        );
    }
});

When(/^I validate (.*), (.*), (.*)$/, PaymentTask.enterUkBeneficiaryDetails);

When(/^I enter following (.*)$/, (sortCode) => {
    ukBeneficiaryDetailPage.enterUkBeneficiarySortCode(sortCode);
});

Then(/^I see an error message for the invalid fields$/, () => {
    ukBeneficiaryDetailPage.verifyErrorMessage();
});

Then(/^I select continue button on add payee page$/, () => {
    ukBeneficiaryDetailPage.continueToReviewDetailsPage();
});

Then(/^I am not allowed to add a new beneficiary$/, () => {
    ukBeneficiaryDetailPage.unableToContinueToReviewDetailsPage();
});

When(/^I click on cancel searching$/, () => {
    searchRecipientPage.cancelSearch();
});

When(/^I click on clear searching$/, () => {
    searchRecipientPage.clearSearch();
});

When(/^I should see below UK List fields populated$/, (companyList) => {
    const tableData = companyList.hashes();
    ukCompanySearchResultsPage.validateCompanyBasicDetails(tableData);
});

When(/^list of matching companies with the notes$/, (matchingList) => {
    const tableData = matchingList.hashes();
    ukCompanySearchResultsPage.verifyMatchingList(tableData);
});

Then(/^I am on UK Company Search results page$/, () => {
    ukCompanySearchResultsPage.verifyUkCompanySearchResultsPage();
});

When(/^list of matching companies with the same sort code and account number$/, (matchingList) => {
    const tableData = matchingList.hashes();
    ukCompanySearchResultsPage.verifyMatchingList(tableData);
});

When(/^I select a result$/, () => {
    ukCompanySearchResultsPage.selectResult();
});

Then(/^I should see UK Company Review Details page/, () => {
    ukCompanySearchResultsPage.verifyUkCompanyReviewDetailPage();
});

Then(/^the selected Recipient Name, Sort Code, Account Number should be displayed/, () => {
    ukCompanySearchResultsPage.verifyBasicDetailsOnReviewDetailsPage();
});

When(/^I have the ability to add a reference (.*),(.*) and retype the reference$/, PaymentTask.addReference);

Then(/^I should be on the New UK beneficiary details page$/, () => {
    ukBeneficiaryDetailPage.shouldSeeNewUkBeneficiaryDetailPage();
});

Then(/^I should be on the COP UK beneficiary details page$/, () => {
    ukBeneficiaryDetailPage.shouldSeeCOPUkBeneficiaryDetailPage();
});

Then(/^I should not be able to continue$/, () => {
    ukCompanySearchResultsPage.unableToContinueToAddRecipient();
});

When(/^I go back in the journey$/, () => {
    ukCompanySearchResultsPage.navigateBack();
});

Then(/^I should see an error message$/, () => {
    ukBeneficiaryDetailPage.verifyErrorMessage();
});

Then(/^on selection of a Cancel option I should stay on the same screen$/, () => {
    ukCompanySearchResultsPage.verifyWinBackOnNegativeAction();
});

When(/^I should see to add a reference (.*)$/, (reference) => {
    ukCompanySearchResultsPage.enterReference(reference);
});

When(/^I select the security link$/, () => {
    ukCompanySearchResultsPage.selectSecurityLink();
});

Then(/^I should see a warning overlay with the ability to close$/, () => {
    ukCompanySearchResultsPage.waitForWarningOverlay();
    ukCompanySearchResultsPage.closeWarningOverlay();
});

const enterPassword = (world) => {
    const data = world.dataRepository.get();
    confirmPasswordPage.enterPasswordAndConfirm(data.password);
};

Then(/^I enter a correct password$/, function () {
    enterPassword(this);
});

When(/^I select continue on UK review details page$/, () => {
    ukCompanySearchResultsPage.confirmReviewDetailsPage();
});

When(/^I select Cancel on UK Review Details page$/, () => {
    ukCompanySearchResultsPage.cancelWinBack();
});

Then(/^on selection of Leave option I should navigate to account category page$/, () => {
    ukCompanySearchResultsPage.verifyWinBackOnPositiveAction();
});

Then(/^I should see UK Review Details page$/, () => {
    ukCompanySearchResultsPage.verifyUkCompanyReviewDetailPage();
});

When(/^I enter whitelisted company details$/, PaymentTask.enterWhiteListedDetails);

Then(/^I (should|should not) be given the option to add a new recipient at the end of the list$/, (visible) => {
    searchRecipientPage.verifyAddNewPayeeLink(visible);
});

Then(/^I clear the reference fields$/, () => {
    ukCompanySearchResultsPage.ClearReferenceFields();
});

When(/^I select recipient (.*) to make (payment|transfer) to from searched list of recipients$/, (recipient, type) => {
    paymentHubMainPage.selectToAccountPane();
    if (type === 'payment') {
        searchRecipientPage.selectRecipient(recipient);
    } else {
        searchRecipientPage.selectTransferAccount(recipient);
    }
});

When(/^I (choose|dont choose) the Business selection entity$/, (check) => {
    ukBeneficiaryDetailPage.verifyBusinessConfirmationCheckBox(check);
});

const deleteRecipient = (world, sortCode, accountNumber, recipientName) => {
    PaymentTask.addNewPayee();
    beneficiaryTypePage.chooseUkAccountBeneficiary();
    PaymentTask.enterUkBeneficiaryDetails(sortCode, accountNumber, recipientName);
    ukBeneficiaryDetailPage.continueToReviewDetailsPage();
    ukCompanySearchResultsPage.confirmReviewDetailsPage();
    YourAccountsTask.verifyEnterPasswordDialogBox('be');
    enterPassword(world);
    paymentHubMainPage.verifyAddBeneficiary();
    paymentHubMainPage.navigateBackToHomePage();
    PaymentTask.chooseFromOrToAccount('remitter');
};

Given(/^that I want to delete recipient (.*) (.*) (.*)$/, function (sortCode, accountNumber, recipientName) {
    deleteRecipient(this, sortCode, accountNumber, recipientName);
});
