const {When} = require('cucumber');

const DirectDebitPage = require('../pages/transferAndPayments/directDebit.page');
const DirectDebitNonCancellablePage = require('../pages/transferAndPayments/directDebitNonCancellable.page.js');
const ConfirmDirectDebitCancelPage = require('../pages/transferAndPayments/confirmDirectDebitCancel.page');

const directDebitPage = new DirectDebitPage();
const directDebitNonCancellablePage = new DirectDebitNonCancellablePage();
const confirmDirectDebitCancelPage = new ConfirmDirectDebitCancelPage();

When(/^I validate the direct debit tab is displayed in the standing order and direct debit page$/, () => {
    directDebitPage.waitForPageLoad();
});

When(/^I click on the direct debit tab is displayed in the standing order and direct debit page$/, () => {
    directDebitPage.waitForPageLoad();
    directDebitPage.selectDirectDebit();
});

When(/^I should not see the cancel direct debit button in the standing order and direct debit page$/, () => {
    directDebitPage.waitForPageLoad();
    directDebitPage.verifyCancelDirectDebitNotVisible();
});

When(/^I select cancel direct debit button in the standing order and direct debit page$/, () => {
    directDebitPage.waitForPageLoad();
    directDebitPage.clickCancelDirectDebit();
});

When(/^I should see direct debit cannot be cancelled message$/, () => {
    directDebitNonCancellablePage.waitForPageLoad();
    directDebitNonCancellablePage.verifyDirectDebitNotCancelledMsg();
});

When(/^I should see direct debit cancelled message$/, () => {
    directDebitPage.waitForPageLoad();
    directDebitPage.verifyDirectDebitCancelSuccessMsg();
});

When(/^I confirm cancel in the cancel direct debit page$/, () => {
    confirmDirectDebitCancelPage.waitForPageLoad();
    confirmDirectDebitCancelPage.confirmDirectDebitCancel('password1234');
});
