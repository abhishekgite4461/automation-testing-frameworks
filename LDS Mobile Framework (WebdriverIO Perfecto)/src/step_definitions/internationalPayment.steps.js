const {When, Then} = require('cucumber');

const InternationalPaymentPage = require('../pages/internationalPayment.page');

const internationalPaymentPage = new InternationalPaymentPage();

When(/^I should be on the international payment webview page$/, () => {
    internationalPaymentPage.waitForPageLoad();
});

When(/^I select International Payment Recipients$/, () => {
    internationalPaymentPage.selectPaymentRecipients();
});

When(/^I select Make a Payment to a first International Payment Recipient$/, () => {
    internationalPaymentPage.selectMakeIPPayment();
});

When(/^I enter the IP amount (.*) to be sent$/, (amount) => {
    internationalPaymentPage.enterIPAmount(amount);
});

When(/^I select continue on International Payment Page$/, () => {
    internationalPaymentPage.selectIPContinue();
});

When(/^I should see confirm International Payment Page$/, () => {
    internationalPaymentPage.verifyIPPaymentConfirmation();
});

When(/^I select terms and conditions on confirm international payment page$/, () => {
    internationalPaymentPage.selectTermsAndConditions();
});

When(/^I select confirm button on confirm international payment page$/, () => {
    internationalPaymentPage.confirmIPPayment();
});

When(/^I verify successful International Payment$/, () => {
    internationalPaymentPage.checkIPPayment();
});

When(/^I enter correct password in IP payment confirmation page$/, function () {
    const data = this.dataRepository.get();
    internationalPaymentPage.enterIPPassword(data.password);
});

When(/^I enter recipient BIC code (.*)$/, (BICcode) => {
    internationalPaymentPage.enterBICCode(BICcode);
});

When(/^I continue to add International recipient$/, () => {
    internationalPaymentPage.selectContinueButton();
});

When(/^I enter recipient bank address(.*)$/, (recipientBankAddress) => {
    internationalPaymentPage.enterRecipientBankAddress(recipientBankAddress);
});

When(/^I enter recipient IBAN details (.*)$/, (IBAN) => {
    internationalPaymentPage.enterIBAN(IBAN);
});

When(/^I enter recipient name (.*)$/, (recipientName) => {
    internationalPaymentPage.enterRecipientName(recipientName);
});

When(/^I enter recipient address (.*)$/, (recipientAddress) => {
    internationalPaymentPage.enterRecipientAddress(recipientAddress);
});

When(/^I continue further to add International recipient$/, () => {
    internationalPaymentPage.continueRecipientDetailsButton();
});

When(/^I choose to receive EIA call to add International recipient$/, () => {
    internationalPaymentPage.selectNumber();
});

When(/^I confirm to receive EIA call$/, () => {
    internationalPaymentPage.selectConfirmButton();
});

Then(/^I am shown International beneficiary Successful page$/, () => {
    internationalPaymentPage.verifySuccessMessage();
});
