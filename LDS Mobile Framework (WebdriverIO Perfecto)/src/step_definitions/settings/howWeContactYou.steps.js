const {When, Then} = require('cucumber');
const SettingsPage = require('../../pages/settings/settings.page');
const HowWeContactYouPage = require('../../pages/settings/howWeContactYou.page');
const MarketingPage = require('../../pages/settings/marketingPref.page');

const settingsPage = new SettingsPage();
const howWeContactYouPage = new HowWeContactYouPage();
const marketingPage = new MarketingPage();

When(/^I select the how we contact you option from the settings page$/, () => {
    settingsPage.selectHowWeContactYou();
});

Then(/^I should be shown the screen with following options$/, (table) => {
    const tableData = table.hashes();
    howWeContactYouPage.validateHowWeContactYouPageOptions(tableData);
});

When(/^I select Marketing preferences$/, () => {
    howWeContactYouPage.selectMarketingPreference();
});

Then(/^I should be displayed options for Halifax$/, () => {
    marketingPage.isHalifaxMarketingPrefVisible();
});

Then(/^I should be displayed options for Bank of Scotland$/, () => {
    marketingPage.isBosMarketingPrefVisible();
});

When(/^I navigate to online and paper preferences from the settings page$/, () => {
    settingsPage.selectHowWeContactYou();
    howWeContactYouPage.waitForPageLoad();
    howWeContactYouPage.selectOnlinePaperPreferenceTile();
});

Then(/^I should be on the how we contact page$/, () => {
    howWeContactYouPage.waitForPageLoad();
});
