const {Then} = require('cucumber');
const PhoneOutcomePage = require('../../../pages/settings/personalDetails/phoneOutcome.page');
const ForcedLogoutPage = require('../../../pages/forcedLogout.page');

const phoneOutcomePage = new PhoneOutcomePage();
const forcedLogoutPage = new ForcedLogoutPage();

Then(/^I should be displayed successful update page$/, () => {
    phoneOutcomePage.isPageVisible();
});

Then(/^I should be shown a success screen$/, () => {
    phoneOutcomePage.waitForPageLoad();
});

Then(/^I should be shown an option to go back to my personal details$/, () => {
    phoneOutcomePage.isBackToContactDetailsButtonVisible();
});

Then(/^I navigate back to contact details$/, () => {
    phoneOutcomePage.clickBackToContactDetailsButton();
});

Then(/^I should be shown contact number under review screen with error message$/, () => {
    phoneOutcomePage.isPageVisible();
    phoneOutcomePage.isMessageVisible();
});

Then(/^I should be shown unsuccessful number update screen with error message$/, () => {
    forcedLogoutPage.isPageVisible();
    forcedLogoutPage.isErrorIconVisible();
    forcedLogoutPage.isErrorMessageVisible();
    forcedLogoutPage.isGoToLogonButtonVisible();
});
