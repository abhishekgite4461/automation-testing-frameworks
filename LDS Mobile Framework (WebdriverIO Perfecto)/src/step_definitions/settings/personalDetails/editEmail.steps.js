const {When, Then} = require('cucumber');
const PersonalDetailsPage = require('../../../pages/settings/personalDetails/personalDetails.page');
const ChangeEmailPage = require('../../../pages/settings/personalDetails/changeEmail.page');
const AlertPopUpPage = require('../../../pages/alertPopUp.page');
const UserDetailsHelper = require('../../../utils/userDetails.helper');
const NavigationTask = require('../../../tasks/navigation.task');

const personalDetailsPage = new PersonalDetailsPage();
const changeEmailPage = new ChangeEmailPage();
const successPopUpPage = new AlertPopUpPage('settings/personalDetails/updateEmailSuccessPopUp.screen');

const validEmailAddress = UserDetailsHelper.generateRandomEmailAddress();
const differentEmailAddress = UserDetailsHelper.generateRandomEmailAddress();

// When(/^I am on the update email address page$/, function () {
//     MoreSteps.openYourProfileFromMoreMenu();
//     personalDetailsPage.waitForPageLoad();
//     personalDetailsPage.isPageVisible();
//     personalDetailsPage.isPageVisible();
//     personalDetailsPage.selectEmailAddressDetails();
//     changeEmailPage.isPageVisible();
// });

When(/^I have not provided any input in the email address field$/, () => {
    changeEmailPage.enterEmailAddress('');
    changeEmailPage.reenterEmailAddress('');
});

When(/^I enter an email address in the email address field$/, () => {
    changeEmailPage.enterEmailAddress(validEmailAddress);
});

When(/^I enter "(.*)" in the email address field$/, (emailAddress) => {
    changeEmailPage.enterEmailAddress(emailAddress);
});

When(/^I re-enter "(.*)" in the re-enter email address field$/, (emailAddress) => {
    changeEmailPage.reenterEmailAddress(emailAddress);
});

When(/^I re-enter (a different|the same) email address in the re-enter email address field$/, (emailType) => {
    const emailAddress = (emailType === 'the same') ? validEmailAddress : differentEmailAddress;
    changeEmailPage.reenterEmailAddress(emailAddress);
});

When(/^I proceed to save the changes$/, () => {
    changeEmailPage.clickConfirmButton();
});

When(/^I enter an email address in the correct format$/, () => {
    changeEmailPage.enterEmailAddress(validEmailAddress);
    changeEmailPage.reenterEmailAddress(validEmailAddress);
});

When(/^I have submitted a request to change my email address$/, () => {
    NavigationTask.navigateToProfile();
    personalDetailsPage.selectEmailAddressDetails();
    changeEmailPage.enterEmailAddress(validEmailAddress);
    changeEmailPage.reenterEmailAddress(validEmailAddress);
    changeEmailPage.clickConfirmButton();
});

When(/^I confirm my new email address in personal details page$/, () => {
    changeEmailPage.confirmEmailAddress(validEmailAddress);
});

When(/^I try to exit screen using the back button$/, () => {
    changeEmailPage.goBack();
});

Then(/^I am shown the update email address page$/, () => {
    changeEmailPage.isPageVisible();
});

Then(/^I am shown fields for email address, re-enter email address$/, () => {
    changeEmailPage.isEmailFieldVisible();
    changeEmailPage.isReenterEmailFieldVisible();
});

Then(/^I am shown my current email address in the email address field$/, () => {
    changeEmailPage.isEmailAddressFieldPopulatedWithEmail();
});

Then(/^I should be shown an error message$/, () => {
    changeEmailPage.isValidationErrorVisible();
});

Then(/^I should be on the update email page after dismissing the error message$/, () => {
    changeEmailPage.dismissValidationErrorMessage();
    changeEmailPage.isPageVisible();
});

Then(/^I should be shown a success message with prompt to close the message$/, () => {
    successPopUpPage.waitForPageLoad();
    successPopUpPage.isConfirmButtonVisible();
});

Then(/^I should be displayed successful update dialog$/, () => {
    successPopUpPage.waitForPageLoad();
});

Then(/^I dismiss the prompt$/, () => {
    successPopUpPage.selectConfirmButton();
});
