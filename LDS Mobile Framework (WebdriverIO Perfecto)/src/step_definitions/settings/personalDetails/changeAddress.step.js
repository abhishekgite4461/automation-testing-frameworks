const {When, Then} = require('cucumber');
const ChangeAddressCallUsPage = require('../../../pages/callUs/changeAddressCallUs.page');
const RequestPermissionPage = require('../../../pages/requestPermissionPopUp.page');

const requestPermissionPage = new RequestPermissionPage();
const DeviceHelper = require('../../../utils/device.helper');

const changeAddressCallUsPage = new ChangeAddressCallUsPage();

Then(/^I am able to view the legal information on the screen$/, () => {
    changeAddressCallUsPage.viewLegalInformation();
});

When(/^I select call us button in the update your address page$/, () => {
    changeAddressCallUsPage.selectCallUsButton();
    if (DeviceHelper.isAndroid()) {
        requestPermissionPage.allowNativeAppCall();
    }
});
