const {When, Then} = require('cucumber');
const ChangePhoneNumbersPage = require('../../../pages/settings/personalDetails/changePhoneNumbers.page');
const AlertPopUpPage = require('../../../pages/alertPopUp.page');
const UserDetailsHelper = require('../../../utils/userDetails.helper');
const ConfirmPhoneNumbersPage = require('../../../pages/settings/personalDetails/confirmPhoneNumbers.page');

const confirmPhoneNumbersPage = new ConfirmPhoneNumbersPage();
const changePhoneNumbersPage = new ChangePhoneNumbersPage();
const abandonChangesWinbackPage = new AlertPopUpPage('settings/personalDetails/abandonChangesWinback.screen');

const extensionNumber = UserDetailsHelper.generatePhoneExtensionNumber();

Then(/^I should be shown the update phone number page$/, () => {
    changePhoneNumbersPage.waitForPageLoad();
});

Then(/^the existing numbers are masked when displayed$/, () => {
    changePhoneNumbersPage.arePhoneNumbersMaskedWhenVisible();
});

Then(/^I should be shown my current mobile number, home number, work number and work extension$/, () => {
    changePhoneNumbersPage.arePhoneNumbersVisible();
});

Then(/^I should be shown a warning message that the mobile number is registered for Pay a Contact$/, () => {
    changePhoneNumbersPage.isPayAContactMessageVisible();
});

When(/^I enter "(.*)", "(.*)", "(.*)", "(.*)"$/, (mobileNumber, homeNumber, workNumber, workExtension) => {
    changePhoneNumbersPage.changeMobileNumber(mobileNumber);
    changePhoneNumbersPage.changeHomeNumber(homeNumber);
    changePhoneNumbersPage.changeWorkNumber(workNumber);
    changePhoneNumbersPage.changeWorkExtension(workExtension);
});

Then(/^I should be allowed to proceed with the number update$/, () => {
    changePhoneNumbersPage.isContinueButtonEnabled();
});

Then(/^I should not be allowed to proceed with the number update$/, () => {
    changePhoneNumbersPage.isContinueButtonDisabled();
});

When(/^I have started editing the mobile number$/, function () {
    const data = this.dataRepository.get();
    changePhoneNumbersPage.changeMobileNumber(data.phoneNumber);
});

When(/^I have started editing the home number$/, function () {
    const data = this.dataRepository.get();
    changePhoneNumbersPage.changeHomeNumber(data.phoneNumber);
});

When(/^I have started editing the work number$/, function () {
    const data = this.dataRepository.get();
    changePhoneNumbersPage.changeWorkNumber(data.phoneNumber);
});

When(/^I have started editing the work extension$/, () => {
    changePhoneNumbersPage.changeWorkExtension(extensionNumber);
});

When(/^I try to exit the update phone number page$/, () => {
    changePhoneNumbersPage.goBack();
});

Then(/^I am displayed a winback with continue and cancel options$/, () => {
    abandonChangesWinbackPage.waitForPageLoad();
    abandonChangesWinbackPage.isConfirmButtonVisible();
    abandonChangesWinbackPage.isCancelButtonVisible();
});

Then(/^I select cancel button option from alert popup$/, () => {
    abandonChangesWinbackPage.selectCancelButton();
});

Then(/^I select confirm button option from alert popup$/, () => {
    abandonChangesWinbackPage.selectConfirmButton();
});

Then(/^I view the change telephone page$/, () => {
    changePhoneNumbersPage.waitForPageLoad();
});

Then(/^I should see advisory text underneath the telephone area$/, () => {
    ChangePhoneNumbersPage.scrollToEndOfPage();
    changePhoneNumbersPage.shouldSeeHCCText();
});

Then(/^I have changed a (.*) telephone number$/, (homeNumber) => {
    changePhoneNumbersPage.waitForPageLoad();
    changePhoneNumbersPage.changeHomeNumber(homeNumber);
    changePhoneNumbersPage.clickContinueButton();
    confirmPhoneNumbersPage.waitForPageLoad();
});

Then(/^I should see advisory text underneath the telephone area on confirmation screen$/, () => {
    ChangePhoneNumbersPage.scrollToEndOfPage();
    changePhoneNumbersPage.shouldSeeHCCConfirmationText();
});
