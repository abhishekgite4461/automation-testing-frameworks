const {When, Then} = require('cucumber');
require('chai');

const DataConsentPage = require('../../../pages/settings/personalDetails/dataConsent.page');
const PersonalDetailsPage = require('../../../pages/settings/personalDetails/personalDetails.page');
const LeadsPage = require('../../../pages/leads.page');
const HomePage = require('../../../pages/home.page');
const MoreMenuPage = require('../../../pages/more.page');
const SettingsPage = require('../../../pages/settings/settings.page');
const ResetPage = require('../../../pages/settings/reset.page');
const ResetConfirmationPopUpPage = require('../../../pages/settings/resetConfirmationPopUp.page');
const AuthPage = require('../../../pages/common/auth.page');
const YourProfilePage = require('../../../pages/yourProfile.page');

const dataConsentPage = new DataConsentPage();
const leadsPage = new LeadsPage();
const personalDetailsPage = new PersonalDetailsPage();
const settingsPage = new SettingsPage();
const resetPage = new ResetPage();
const resetConfirmationPopUpPage = new ResetConfirmationPopUpPage();
const homePage = new HomePage();
const moreMenuPage = new MoreMenuPage();
const authPage = new AuthPage();
const yourProfilePage = new YourProfilePage();

Then(/I have not made a selection for my cookie usage already on that device$/, () => {
    dataConsentPage.waitForDataConsentPageLoad();
});

Then(/Only the data consent interstitial is displayed$/, () => {
    dataConsentPage.shouldSeePageTitle();
    leadsPage.verifyNoGlobalMenu();
    dataConsentPage.shouldNotSeeGlobalBackButton();
});

Then(/I see a native data consent (interstitial) page without a back button and global menu on the header$/, (pageType) => {
    dataConsentPage.shouldSeePageTitle(pageType);
    leadsPage.verifyNoGlobalMenu();
    dataConsentPage.shouldNotSeeGlobalBackButton();
});

Then(/I see two options for selection$/, () => {
    dataConsentPage.shouldSeeSelectionOptions();
});

Then(/I select the option to accept all consents$/, () => {
    dataConsentPage.selectAcceptAllButton();
});

Then(/I should not see the data consent interstitial$/, () => {
    dataConsentPage.shouldNotSeeTheDataConsentInterstitial();
});

Then(/The link to the data consent isn't displayed$/, () => {
    personalDetailsPage.shouldNotSeeTheDataConsentLink();
});

Then(/^I toggle on the (marketingPrefs|performancePrefs) consent option$/, (dataConsentSection) => {
    dataConsentPage.toggleDataConsentMarketingPreferences(dataConsentSection);
});

When(/I select the (consentOptedIn|consentOptedOut) button for the (marketingPrefs|performancePrefs) section$/, (button, dataConsentSection) => {
    dataConsentPage.selectButtonInSection(button, dataConsentSection);
});

Then(/the button for (marketingPrefs|performancePrefs) should be (consentOptedIn|consentOptedOut|unselected)$/, (dataConsentSection, buttonState) => {
    dataConsentPage.assertDataConsentButtonState(buttonState, dataConsentSection);
});

Then(/^I see a Winback option on the data consent page$/, () => {
    dataConsentPage.assertDataConsentWinbackModalVisible();
});

Then(/^I see an external Winback option on the data consent page$/, () => {
    dataConsentPage.assertDataConsentExternalWinbackModalVisible();
});

Then(/^I opt to (stay on|leave) the data consent page$/, (winbackButton) => {
    dataConsentPage.selectStayOrLeaveButtonFromWinbackModal(winbackButton);
});

Then(/^I should remain on the manage data consent page$/, () => {
    dataConsentPage.waitForManageDataConsentPageLoad();
});

Then(/^I should remain on the native (data consent|manage consent) interstitial page$/, (page) => {
    dataConsentPage.shouldSeePageTitle(page);
});

Then(/^I should remain on the manage consent interstitial page$/, () => {
    dataConsentPage.waitForManageDataConsentPageLoad();
});

Then(/^I select the cookies policy link$/, () => {
    dataConsentPage.selectDataConsentAccordionLink();
});

Then(/^I select the cookies policy link on the interstitial page$/, () => {
    dataConsentPage.selectInterstitialDataConsentAccordionLink();
});

Then(/^I make a new selection for my consents$/, () => {
    dataConsentPage.setStateForDataConsentPreferences(['marketingPrefs', 'performancePrefs'], 'consentOptedOut');
});

Then(/^data consents are switched on$/, () => {
    dataConsentPage.setStateForDataConsentPreferences(['marketingPrefs', 'performancePrefs'], 'consentOptedIn');
});

Then(/^data consents are switched off$/, () => {
    dataConsentPage.setStateForDataConsentPreferences(['marketingPrefs', 'performancePrefs'], 'consentOptedOut');
});

When(/data consents are both (consentOptedIn|consentOptedOut)$/, (consentOption) => {
    dataConsentPage.setStateForDataConsentPreferences(['marketingPrefs', 'performancePrefs'], consentOption);
});

Then(/^I confirm my selection on the data consent page$/, () => {
    dataConsentPage.submitDataConsentPreferences();
});

Then(/I am taken to the manage data consent page with a back button visible$/, () => {
    dataConsentPage.shouldSeeGlobalBackButton();
});

Then(/^The confirm option is then enabled$/, () => {
    dataConsentPage.dataConsentConfirmButtonIsEnabled();
});

Then(/^The confirm button should be disabled$/, () => {
    dataConsentPage.dataConsentConfirmButtonIsDisabled();
});

When(/^I reset the app$/, () => {
    authPage.selectTab('more');
    moreMenuPage.selectSettings();
    settingsPage.navigateToReset();
    resetPage.selectResetAppButton();
    resetConfirmationPopUpPage.waitForPageLoad();
    resetConfirmationPopUpPage.selectContinueButton();
});

Then(/^The Privacy Management settings on that device must be cleared$/, () => {
    dataConsentPage.shouldSeePageTitle();
    dataConsentPage.selectManageConsentButton();
    dataConsentPage.waitForManageDataConsentPageLoad();
    dataConsentPage.dataConsentConfirmButtonIsDisabled();
});

Then(/^I have not made a selection for my consent$/, () => {
    dataConsentPage.dataConsentConfirmButtonIsDisabled();
});

When(/^I have set my privacy setting already for the app$/, () => {
    dataConsentPage.shouldSeeInterstitialOrHomePage();
    authPage.selectTab('more');
    moreMenuPage.selectYourProfile();
    personalDetailsPage.selectDataConsentLink();
    dataConsentPage.dataConsentConfirmButtonIsEnabled();
});

Then(/^I must be taken out of my journey to external page$/, () => {
    dataConsentPage.waitForExternalDataConsentPageLoad();
});

Then(/^I see a native data consent interstitial page$/, () => {
    dataConsentPage.shouldSeePageTitle();
});

Then(/I select the option to manage consent$/, () => {
    dataConsentPage.selectManageConsentButton();
});

Then(/I should be taken to the my customize consents page$/, () => {
    dataConsentPage.waitForManageDataConsentPageLoad();
});

When(/^I navigate to manage data consent page$/, () => {
    personalDetailsPage.selectDataConsentLink();
    dataConsentPage.waitForManageDataConsentPageLoad();
});

When(/^I am on the manage consent page/, () => {
    dataConsentPage.selectManageConsentButton();
    dataConsentPage.waitForManageDataConsentPageLoad();
});

Then(/^I have not made a selection for all my options$/, () => {
    dataConsentPage.dataConsentPerformanceButtonsIsUnselected();
});

Then(/^No action must be taken on the (interstitial|manage consent|data consent) page$/, (pageType) => {
    dataConsentPage.shouldSeePageTitle(pageType);
});

When(/^I have not made a selection for my data consent usage already on that device$/, () => {
    dataConsentPage.noConsentSelected();
});

Then(/^I see a native manage BNGA data consent interstitial page without a back button$/, () => {
    dataConsentPage.noBackButtonOnInterstitial();
});

When(/^I select the option to accept the data consent$/, () => {
    dataConsentPage.acceptAndConfirmDataConsent();
});

When(/^I select the option not to accept the data consent$/, () => {
    dataConsentPage.declineDataConsentAndConfirm();
});

Then(/^I should be displayed the manage data consent page$/, () => {
    dataConsentPage.viewBNGADataConsentPage();
});

Then(/^I should have option to make changes to my data consent settings$/, () => {
    dataConsentPage.viewPerformanceConsent();
});

When(/^I have accepted the data consent policy earlier$/, () => {
    dataConsentPage.acceptAndConfirmDataConsent();
});

When(/^I now I have declined to give consent to data consent settings$/, () => {
    yourProfilePage.selectDataConsentLink();
    dataConsentPage.declineDataConsent();
});

Then(/^I confirm the changes to my data consent settings$/, () => {
    dataConsentPage.confirmConsent();
});

When(/^now I have accepted to give consent to data consent settings$/, () => {
    dataConsentPage.acceptDataConsent();
});

When(/^I navigate away from the manage data consent page$/, () => {
    homePage.selectPayAndTransfer();
});

Then(/^I should be shown the winback option$/, () => {
    dataConsentPage.viewWinBackAlert();
});

Then(/^I should be shown the header winback option$/, () => {
    dataConsentPage.viewHeaderWinBackAlert();
});

Then(/^I should not be shown the winback option$/, () => {
    dataConsentPage.noWinbackToLeaveTheApp();
});

When(/^I navigate from the manage data consent page via device back button$/, () => {
    dataConsentPage.clickBackButton();
});

When(/^I have made changes to the data consent$/, () => {
    dataConsentPage.changeConsentPreference();
});

When(/^I select the cookies policy link on the consent page$/, () => {
    dataConsentPage.selectCookiePolicyLink();
});

Then(/^I must be on the manage BNGA data consent page$/, () => {
    dataConsentPage.shouldSeePageTitle('data consent');
});

When(/^I'm presented with a winback option on data consent page$/, () => {
    dataConsentPage.viewWinBackAlert();
});

When(/^I have not made a selection for my option and try to confirm my choice$/, () => {
    dataConsentPage.noConsentSelected();
});

Then(/^The data consent settings on that device must be cleared$/, () => {
    dataConsentPage.noConsentSelected();
});

When(/^I have set my privacy setting already on device$/, () => {
    dataConsentPage.setConsentOnDevice();
});
