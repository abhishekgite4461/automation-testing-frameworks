const {Given, When, Then} = require('cucumber');
const PersonalDetailsPage = require('../../../pages/settings/personalDetails/personalDetails.page');
const NavigationTask = require('../../../tasks/navigation.task');

const personalDetailsPage = new PersonalDetailsPage();

Given(/^I am on my personal details page$/, () => {
    NavigationTask.openYourProfileFromMoreMenu();
    personalDetailsPage.waitForPageLoad();
    personalDetailsPage.isPageVisible();
    personalDetailsPage.isPageVisible();
});

Then(/^I should be on the your personal details page$/, () => {
    personalDetailsPage.isPageVisible();
});

Then(/^I should be shown the list of personal details$/, (details) => {
    const personalDetails = details.hashes();
    personalDetailsPage.shouldSeePersonalDetails(personalDetails);
});

When(/^I choose to update a phone number$/, () => {
    personalDetailsPage.selectMobileDetails();
});

When(/^I choose to update my email address$/, () => {
    personalDetailsPage.selectEmailAddressDetails();
});

When(/^I select marketing preferences link$/, () => {
    personalDetailsPage.selectPrimaryMarketingPreferencesLink();
});

When(/^I select marketing preferences link for the secondary brand$/, () => {
    personalDetailsPage.selectSecondaryMarketingPreferencesLink();
});

When(/^the link to the marketing hub isn't displayed$/, () => {
    personalDetailsPage.verifyPrimaryMarketingPreferencesLinkVisibility(false);
});

When(/^I should be shown updated personal details screen$/, () => {
    personalDetailsPage.isPageVisible();
});

When(/^No marketing preferences links are displayed$/, () => {
    personalDetailsPage.verifyPrimaryAndSecondaryMarketingPreferencesLinkNotVisible(false);
});

When(/^The marketing preference links for both primary and secondary brands should be visible$/, () => {
    personalDetailsPage.verifyPrimaryAndSecondaryMarketingPreferencesLinkIsVisible(true);
});

When(/^I select the marketing preferences link for the (primary|secondary) brand$/, (link) => {
    personalDetailsPage.selectPrimaryOrSecondaryMarketingPreferencesLink(link);
});

When(/^Marketing preference links for only the (primary|secondary) brand is visible$/, (link) => {
    personalDetailsPage.verifyEitherPrimaryOrSecondaryMarketingPreferencesLink(link);
});

When(/^I should see my new address on the personal details page$/, () => {
    personalDetailsPage.verifyUpdatedAddress();
});

When(/^The manage data consent link is displayed$/, () => {
    personalDetailsPage.verifyDataConsentLinkVisibility();
});

Then(/^I should be shown the list of business details$/, (details) => {
    const businessDetails = details.hashes();
    personalDetailsPage.shouldSeeBusinessDetails(businessDetails);
});

When(/^I am on the update phone number page$/, NavigationTask.updatePhoneNumber);
