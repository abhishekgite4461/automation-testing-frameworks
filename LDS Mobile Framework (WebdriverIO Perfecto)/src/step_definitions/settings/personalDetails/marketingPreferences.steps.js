const {When, Then} = require('cucumber');
require('chai');

const MarketingPreferencesHubPage = require('../../../pages/settings/personalDetails/marketingPreferencesHub.page');
const MarketingPreferencesConfirmationPage = require('../../../pages/settings/personalDetails/marketingPreferencesConfirmation.page');
const AlertPopUpPage = require('../../../pages/alertPopUp.page');
const LeadsPage = require('../../../pages/leads.page');

const marketingPreferencesHubPage = new MarketingPreferencesHubPage();
const marketingPreferencesConfirmationPage = new MarketingPreferencesConfirmationPage();
const errorRetrievingPreferencesFromLeadPopUp = new AlertPopUpPage('settings/personalDetails/errorRetrievingPreferencesFromLead.screen');
const leadsPage = new LeadsPage();

Then(/I'm on the marketing hub page$/, () => {
    marketingPreferencesHubPage.waitForMarketingPreferenceHubPageLoad();
});

Then(/the following error message appears (.*).$/, (errorMessage) => {
    marketingPreferencesHubPage.isOcisErrorMessageVisible(errorMessage);
});

Then(/An error pop up appears saying marketing choices cannot be retrieved$/, () => {
    errorRetrievingPreferencesFromLeadPopUp.waitForPageLoad();
});

Then(/I confirm I have read the error message$/, () => {
    errorRetrievingPreferencesFromLeadPopUp.selectConfirmButton();
});

When(/I click the (optedIn|optedOut) button for the (internetPrefs|emailPrefs|postPrefs|thirdPartyPrefs|smsPrefs|phonePrefs) section$/, (button, preferenceSection) => {
    marketingPreferencesHubPage.selectButtonInSection(button, preferenceSection);
});

Then(/the button for (internetPrefs|emailPrefs|postPrefs|thirdPartyPrefs|smsPrefs|phonePrefs) should be (optedIn|optedOut|unselected)$/, (preferenceSection, buttonState) => {
    marketingPreferencesHubPage.assertButtonStateForSection(buttonState, preferenceSection);
});

Then(/^I toggle the (internetPrefs|emailPrefs|postPrefs|thirdPartyPrefs|smsPrefs|phonePrefs) marketing option$/, (preferenceSection) => {
    marketingPreferencesHubPage.swipeToTopOfMarketingHub();
    marketingPreferencesHubPage.toggleMarketingPreferences(preferenceSection);
});

Then(/^I toggle on the (internetPrefs|emailPrefs|postPrefs|thirdPartyPrefs|smsPrefs|phonePrefs) marketing option$/, (preferenceSection) => {
    marketingPreferencesHubPage.toggleMarketingPreferences(preferenceSection);
});

Then(/^I toggle the (internetPrefs|emailPrefs|postPrefs|thirdPartyPrefs|smsPrefs|phonePrefs) marketing option for the (primary|secondary) brand$/, (preferenceSection, link) => {
    marketingPreferencesHubPage.swipeToVisibleCoServicedText(link);
    marketingPreferencesHubPage.toggleMarketingPreferences(preferenceSection);
});

Then(/^I navigate to the marketing hub via the lead$/, () => {
    leadsPage.clickMarketingHubLink();
    marketingPreferencesHubPage.waitForMarketingPreferenceHubPageLoad();
});

Then(/^I opt in to all the marketing prefernces$/, () => {
    marketingPreferencesHubPage.setStateForPreferences(['internetPrefs', 'emailPrefs', 'postPrefs', 'thirdPartyPrefs', 'smsPrefs', 'phonePrefs'], 'optedIn');
});

Then(/^I opt out to all the marketing prefernces$/, () => {
    marketingPreferencesHubPage.setStateForPreferences(['internetPrefs', 'emailPrefs', 'postPrefs', 'thirdPartyPrefs', 'smsPrefs', 'phonePrefs'], 'optedOut');
});

Then(/^I opt in to all the marketing preferences for the (primary|secondary) brand$/, (link) => {
    marketingPreferencesHubPage.setStateForPreferences(['internetPrefs', 'emailPrefs', 'postPrefs', 'thirdPartyPrefs', 'smsPrefs', 'phonePrefs'], 'optedIn', link);
});

Then(/^I submit my preferences$/, () => {
    marketingPreferencesHubPage.submitPreferences();
});

Then(/^I opt in for all options except email$/, () => {
    marketingPreferencesHubPage.setStateForPreferences(['internetPrefs', 'postPrefs', 'thirdPartyPrefs', 'smsPrefs', 'phonePrefs'], 'optedIn');
});

Then(/^I dismiss the preferences error message$/, () => {
    marketingPreferencesHubPage.dismissErrorMessage();
});

Then(/^I'm presented with a Success message$/, () => {
    marketingPreferencesConfirmationPage.isPageVisible();
});

Then(/^my marketing prefences are in an (optedIn|optedOut|unselected) state$/, (preferenceSection) => {
    marketingPreferencesHubPage.assertPreferencesState(['internetPrefs', 'emailPrefs', 'postPrefs', 'thirdPartyPrefs', 'smsPrefs', 'phonePrefs'], preferenceSection);
});

Then(/^I have succesfully updated my preferences$/, () => {
    marketingPreferencesHubPage.waitForMarketingPreferenceHubPageLoad();
    marketingPreferencesHubPage.toggleMarketingPreferences('internetPrefs');
    marketingPreferencesHubPage.submitPreferences();
    marketingPreferencesConfirmationPage.isPageVisible();
});

Then(/^I select the option to return to accounts$/, () => {
    marketingPreferencesConfirmationPage.selectReturnToAccoutns();
});

Then(/^I select the option to return to my profile$/, () => {
    marketingPreferencesConfirmationPage.selectYourProfile();
});

Then(/^I'm presented with a Winback option$/, () => {
    marketingPreferencesHubPage.assertWinbackModalvisible();
});

Then(/^I opt to (stay on|leave) the marketing preferences page$/, (winbackButton) => {
    marketingPreferencesHubPage.selectButtonFromWinbackModal(winbackButton);
});

Then(/^I must remain on the (primary|secondary) brand marketing consent page$/, (link) => {
    marketingPreferencesHubPage.swipeToVisibleCoServicedText(link);
});

Then(/^I select the link on the accordion section$/, () => {
    marketingPreferencesHubPage.selectAccordionLink();
});

Then(/^I select continue to navigate to the secondary marketing preferences screen$/, () => {
    marketingPreferencesHubPage.continueToSecondaryPreferences();
});

Then(/^I'm on the marketing preferences hub$/, () => {
    marketingPreferencesHubPage.shouldNotSeeCoServicingPageWhenPrefsSet();
});

Then(/^I must be taken out of my journey to the external page$/, () => {
    marketingPreferencesHubPage.waitForMarketingPreferencePageLoad();
});

Then(/^I will be taken to the (primary|secondary) brand consents page$/, (link) => {
    marketingPreferencesHubPage.swipeToVisibleCoServicedText(link);
});

Then(/^An error message will be shown informing to complete all consents$/, () => {
    marketingPreferencesHubPage.verifyErrorMessage();
});

When(/^I see a progress indicator showing 50% as complete$/, () => {
    marketingPreferencesHubPage.verifyProgressBarPrimary();
});

When(/^I see a progress indicator showing 100% as complete$/, () => {
    marketingPreferencesHubPage.verifyProgressBarSecondary();
});

When(/^I select the marketing preferences link in the lead interstitial$/, () => {
    leadsPage.clickMarketingHubLink();
});
