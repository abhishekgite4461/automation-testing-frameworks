const {Given, When, Then} = require('cucumber');
const ChangePhoneNumbersPage = require('../../../pages/settings/personalDetails/changePhoneNumbers.page');
const ConfirmPhoneNumbersPage = require('../../../pages/settings/personalDetails/confirmPhoneNumbers.page');
const ConfirmPasswordPage = require('../../../pages/confirmAuthenticationPassword.page');
const NavigationTask = require('../../../tasks/navigation.task');

const UserDetailsHelper = require('../../../utils/userDetails.helper');
const DeviceHelper = require('../../../utils/device.helper');

const PhoneNumberType = require('../../../enums/phoneNumberType.enum');

const changePhoneNumbersPage = new ChangePhoneNumbersPage();
const confirmPhoneNumbersPage = new ConfirmPhoneNumbersPage();
const confirmPasswordPage = new ConfirmPasswordPage();

const extensionNumber = UserDetailsHelper.generatePhoneExtensionNumber();

Then(/^I should be displayed the confirm phone number update screen$/, () => {
    confirmPhoneNumbersPage.isPageVisible();
});

When(/^I choose to continue after updating my (.*) number$/, function (numberType) {
    NavigationTask.updatePhoneNumber();
    const data = this.dataRepository.get();
    const editedNumberType = PhoneNumberType.fromStringByEscapingSpace(numberType);
    switch (editedNumberType) {
    case PhoneNumberType.MOBILE:
        changePhoneNumbersPage.changeMobileNumber(data.phoneNumber);
        changePhoneNumbersPage.clickContinueButton();
        break;
    case PhoneNumberType.HOME:
        changePhoneNumbersPage.changeHomeNumber(data.phoneNumber);
        changePhoneNumbersPage.clickContinueButton();
        break;
    case PhoneNumberType.WORK:
        changePhoneNumbersPage.changeWorkNumber(data.phoneNumber);
        changePhoneNumbersPage.clickContinueButton();
        break;
    case PhoneNumberType.WORK_EXTN:
        changePhoneNumbersPage.changeWorkExtension(extensionNumber);
        changePhoneNumbersPage.clickContinueButton();
        break;
    default:
        throw new Error(`${editedNumberType} - number type is not defined`);
    }
});

Then(/^I should be shown mobile, home, work and work extension numbers$/, () => {
    confirmPhoneNumbersPage.phoneNumberFieldsIsVisible();
});

Then(/^I should see non-edited numbers as masked except (.*)$/, (numberType) => {
    const editedNumberType = PhoneNumberType.fromStringByEscapingSpace(numberType);
    confirmPhoneNumbersPage.areNonEditedNumbersMasked(editedNumberType);
});

Then(/^The edited (.*) number is not masked$/, (numberType) => {
    const editedNumberType = PhoneNumberType.fromStringByEscapingSpace(numberType);
    confirmPhoneNumbersPage.areEditedNumbersNotMasked(editedNumberType);
});

Then(/^I should be shown options to continue or cancel the changes$/, () => {
    confirmPhoneNumbersPage.editButtonIsVisible();
    confirmPhoneNumbersPage.confirmButtonIsVisible();
});

Then(/^I should be asked to confirm the action by entering my IB password$/, () => {
    confirmPasswordPage.isPageVisible();
});

Given(/^I am on the confirm phone number update screen$/, function () {
    NavigationTask.updatePhoneNumber();
    const data = this.dataRepository.get();
    changePhoneNumbersPage.changeMobileNumber(data.phoneNumber);
    changePhoneNumbersPage.clickContinueButton();
    confirmPhoneNumbersPage.isPageVisible();
});

When(/^I choose to cancel the update phone number request$/, () => {
    confirmPhoneNumbersPage.clickEditButton();
});

When(/^I choose to continue with the update phone number request$/, () => {
    confirmPhoneNumbersPage.clickConfirmButton();
});

Given(/^I have submitted request to change mobile, home and work$/, function () {
    NavigationTask.navigateToProfile();
    NavigationTask.updatePhoneNumber();
    const data = this.dataRepository.get();
    changePhoneNumbersPage.changeAndSubmitPhoneNumbers(
        !DeviceHelper.isStub() ? DeviceHelper.mobileNumber()
            .replace('+44', '') : data.phoneNumber,
        !DeviceHelper.isStub() ? DeviceHelper.mobileNumber()
            .replace('+44', '') : data.workNumber
    );
    confirmPhoneNumbersPage.isPageVisible();
    confirmPhoneNumbersPage.clickConfirmButton();
});

Given(/^I have submitted request to change (.*) home number$/, (homeNumber) => {
    changePhoneNumbersPage.waitForPageLoad();
    changePhoneNumbersPage.changeHomeNumberUAT(homeNumber);
    changePhoneNumbersPage.clickContinueButton();
    confirmPhoneNumbersPage.waitForPageLoad();
    confirmPhoneNumbersPage.isPageVisible();
    confirmPhoneNumbersPage.clickConfirmButton();
});

When(/^The request has been successfully processed$/, function () {
    confirmPasswordPage.isPageVisible();
    const data = this.dataRepository.get();
    confirmPasswordPage.enterPasswordAndConfirm(data.password);
});

When(/^I enter password and submit password$/, function () {
    const data = this.dataRepository.get();
    confirmPasswordPage.enterPasswordAndConfirm(data.password);
});

When(/^I request phone number update from update phone number screen$/, function () {
    NavigationTask.updatePhoneNumber();
    const data = this.dataRepository.get();
    changePhoneNumbersPage.changeAndSubmitPhoneNumbers(data.phoneNumber, data.workNumber, extensionNumber);
    confirmPhoneNumbersPage.isPageVisible();
    confirmPhoneNumbersPage.clickConfirmButton();
    confirmPasswordPage.isPageVisible();
    confirmPasswordPage.enterPasswordAndConfirm(data.password);
});
