const {When, Then} = require('cucumber');
const PersonalDetailsPage = require('../../../pages/settings/personalDetails/personalDetails.page');
const ChangeAddressPage = require('../../../pages/settings/personalDetails/changeAddress.page');
const PostCodeEntryPage = require('../../../pages/changeOfAddress/postCodeEntry.page');
const NavigationTask = require('../../../tasks/navigation.task');

const changeAddressPage = new ChangeAddressPage();
const personalDetailsPage = new PersonalDetailsPage();
const postCodeEntryPage = new PostCodeEntryPage();

When(/^I choose to update address from the personal details$/, () => {
    personalDetailsPage.selectAddressDetails();
});

When(/^I am on the address update page$/, () => {
    NavigationTask.navigateToProfile();
    personalDetailsPage.selectAddressDetails();
});

Then(/^I select address details$/, () => {
    personalDetailsPage.selectAddressDetails();
});

Then(/^I should be on the address update page$/, () => {
    changeAddressPage.verifyTitle();
});

Then(/^I should be on the new address update page$/, () => {
    postCodeEntryPage.verifyPostCodeEntryTitle();
});

Then(/^I should be shown my current address$/, () => {
    changeAddressPage.shouldShowCurrentAddressAndPostcode();
});

Then(/^I should be shown an option to change my address using secure call$/, () => {
    changeAddressPage.shouldShowChangeAddressButton();
});

Then(/^I should be shown an option to change my address using secure call for non UK address or child account$/, () => {
    postCodeEntryPage.isVisibleEnterPostcodeCallUsText();
});

When(/^I select the option to change my address using secure call$/, () => {
    changeAddressPage.clickChangeAddressButton();
});

Then(/^I should be on the change of address postcode search screen$/, () => {
    postCodeEntryPage.verifyPostCodeEntryTitle();
});

When(/^I select the option to change my address using postcode$/, () => {
    changeAddressPage.clickChangeAddressButton();
});
