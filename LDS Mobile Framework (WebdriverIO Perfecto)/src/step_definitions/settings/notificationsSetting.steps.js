const {When, Then} = require('cucumber');

const NotificationsSettingPage = require('../../pages/settings/notificationsSetting.page');

const notificationsSettingPage = new NotificationsSettingPage();

Then(/^I should be on the notifications setting page$/, () => {
    notificationsSettingPage.waitForNotificationsPageLoad();
});

When(/^I select the smart alerts enable in notification page$/, () => {
    notificationsSettingPage.enableSmartAlertToggleStatusOn();
});

Then(/^I should shown smart alerts toggle button is enabled$/, () => {
    notificationsSettingPage.checkToggleStatusOn();
});
