const {Then} = require('cucumber');
const DeviceHelper = require('../../utils/device.helper');

const RealTimeAlertsPage = require('../../pages/settings/realTimeAlerts.page');
const Actions = require('../../pages/common/actions');

const realTimeAlertsPage = new RealTimeAlertsPage();

Then(/^I opt in for Account alerts$/, () => {
    realTimeAlertsPage.selectOptInAccountAlerts();
});

Then(/^I opt in for multiple device Account alerts$/, () => {
    realTimeAlertsPage.selectOptInAccountAlertsMDM();
});

Then(/^I (allow|don’t allow) notifications for my device$/, (type) => {
    realTimeAlertsPage.selectAPNSAllowButton(type);
});

Then(/^I should see a (success|opted out) confirmation$/, (type) => {
    realTimeAlertsPage.verifyConfirmationAlert(type);
});

Then(/^I should be opted (in for|out of) Account alerts$/, (type) => {
    realTimeAlertsPage.verifyAccountAlertsToggleValue(type);
});

Then(/^I opt out of the Account alerts$/, () => {
    realTimeAlertsPage.selectOptOutAccountAlerts();
});

Then(/^I will not be opted in for Account alerts$/, () => {
    realTimeAlertsPage.verifyAccountAlertsToggleValue(false);
});

Then(/^I see the push prompt (A|B) screen$/, (screentype) => {
    realTimeAlertsPage.verifyPushPromptScreen(screentype);
});

Then(/^I see the push prompt screen$/, (screentype) => {
    realTimeAlertsPage.verifyPushPromptScreen(screentype);
});

Then(/^I chose (Not now|to opt in)$/, (type) => {
    realTimeAlertsPage.selectOption(type);
});

Then(/^I login again after (\d+) days from today's date$/, (days) => {
    if (DeviceHelper.isAndroid()) {
        Actions.pressAndroidMenuKey();
        realTimeAlertsPage.selectDeviceSettings();
    } else {
        Actions.appInBackground();
        Actions.launchDeviceSettings();
    }
    realTimeAlertsPage.selectDateOption();
    realTimeAlertsPage.incrementDate(days);
});

Then(/^I have turned off device push$/, () => {
    const appName = browser.capabilities.brand;
    if (DeviceHelper.isAndroid()) {
        Actions.pressAndroidMenuKey();
        realTimeAlertsPage.selectDeviceSettings();
        realTimeAlertsPage.selectAppsOption();
    } else {
        Actions.appInBackground();
        Actions.launchDeviceSettings();
    }
    realTimeAlertsPage.selectAppNotifications(appName);
    realTimeAlertsPage.turnOffDeviceNotifications();
});

Then(/^I should be on the login conflict prompt screen$/, () => {
    realTimeAlertsPage.verifyConflictPromptScreen();
});

Then(/^I choose to turn on push$/, () => {
    realTimeAlertsPage.selectGoToDeviceSettings();
});

Then(/^I should be taken to the OS device settings$/, () => {
    realTimeAlertsPage.verifyDeviceSettingsPage();
});

Then(/^I (turn|do not turn) on push at device settings level$/, (userPreference) => {
    if (userPreference === 'turn') {
        realTimeAlertsPage.turnOnDeviceNotifications();
    } else {
        realTimeAlertsPage.turnOffDeviceNotifications();
    }
});

Then(/^I navigate back to the app$/, () => {
    if (DeviceHelper.isAndroid()) {
        Actions.pressAndroidBackKey();
    } else {
        realTimeAlertsPage.selectReturnToAppButton();
    }
});

Then(/^my session is (still|no longer) active$/, (status) => {
    if (status === 'no longer') {
        browser.pause(300000);
    }
    realTimeAlertsPage.verifySessionStatus(status);
});

Then(/^I choose to (turn|not turn) on push in push conflict screen$/, (userPreference) => {
    if (userPreference === 'turn') {
        realTimeAlertsPage.selectGoToDeviceSettings();
    } else {
        realTimeAlertsPage.selectTurnOffInApp();
    }
});

Then(/^I should see a confirmation that I have chosen not to activate notifications$/, () => {
    realTimeAlertsPage.verifyTurnOffConfirmationPage2();
});

Then(/^I select ok on this confirmation that I have chosen not to activate notifications$/, () => {
    realTimeAlertsPage.selectOptOutOkButton();
});

Then(/^I should see unsuccessful message with two options$/, () => {
    realTimeAlertsPage.verifyTurnOffConfirmationPage1();
    realTimeAlertsPage.verifyOptions();
});

Then(/^I select (Not now|to opt out)$/, (userPreference) => {
    if (userPreference === 'Not now') {
        realTimeAlertsPage.selectNotNowButton();
    } else {
        realTimeAlertsPage.selectTurnOffInApp();
    }
});

Then(/^I choose to navigate through three push prompt screens$/, () => {
    realTimeAlertsPage.navigatePushPromptScreenBbyNextButton();
    realTimeAlertsPage.navigatePushPromptScreenBbySwipe();
});

Then(/^I chose No Thanks option$/, () => {
    realTimeAlertsPage.selectNoThanksButton();
});

Then(/^I should see the conflict screen$/, () => {
    realTimeAlertsPage.VerifyConflictAlert();
});

Then(/^I should see the conflict screen pop up$/, () => {
    realTimeAlertsPage.ConflictPopupDeviceSettings();
});

Then(/^I select Not Now from Conflict pop up$/, () => {
    realTimeAlertsPage.ClickNotNowOnConflictPopup();
});

Then(/^I should see overlay advising that I did not complete the correct actions$/, () => {
    realTimeAlertsPage.verifyIncompleteActionsOverlay();
});

Then(/^I select ok on the notifications not activated overlay$/, () => {
    realTimeAlertsPage.selectIncompleteActionsOverlayOKButton();
});

Then(/^I should (now|not) be able to interact with the alert toggles$/, (userAbility) => {
    realTimeAlertsPage.checkToggleStatus(userAbility);
});

Then(/^I should see instructions on how to enable notifications$/, () => {
    realTimeAlertsPage.shouldSeeEnableNotificationsInstruction();
});

Then(/^The alert toggles should be shown in the correct position according to my current status$/, () => {
    realTimeAlertsPage.verifyCorrectAlertToggleStatus();
});
