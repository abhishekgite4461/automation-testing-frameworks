const {When, Then} = require('cucumber');
const SettingsPage = require('../../pages/settings/settings.page');
const LegalInfoPage = require('../../pages/settings/legalInfo.page');

const LegalAndPrivacyPage = require('../../pages/settings/legalAndPrivacy.page');
const CookieUseAndPermissionsPage = require('../../pages/settings/cookieUseAndPermission.page');
const ThirdPartyAcknowledgementsPage = require('../../pages/settings/thirdPartyAcknowledgement.page');
const WinbackPopUpPage = require('../../pages/alertPopUp.page');
const NavigationTask = require('../../tasks/navigation.task');

const settingsPage = new SettingsPage();
const legalInfoPage = new LegalInfoPage();
const legalAndPrivacyPage = new LegalAndPrivacyPage();
const cookieUseAndPermissionsPage = new CookieUseAndPermissionsPage();
const thirdPartyAcknowledgementsPage = new ThirdPartyAcknowledgementsPage();

When(/^I navigate to the legal info option from the settings page$/, () => {
    NavigationTask.openSettingsFromMoreMenu();
    settingsPage.selectLegalInfo();
});

When(/^I select the legal info option from the settings page$/, () => {
    settingsPage.selectLegalInfo();
});

Then(/^I am on the legal information page$/, () => {
    legalInfoPage.isPageVisible();
});

Then(/^I am on the pre auth legal information page overlay$/, () => {
    legalInfoPage.isPageVisible();
});

Then(/^the page should have links for legal and privacy, cookie use and third party acknowledgements$/, () => {
    legalInfoPage.isLegalAndPrivacyLinkVisible();
    legalInfoPage.isCookieUseAndPermissionLinkVisible();
    legalInfoPage.isThirdPartyAcknowledgementsLinkVisible();
});

When(/^I select the legal and privacy link/, () => {
    legalInfoPage.selectLegalAndPrivacyLink();
});

When(/^I select the cookie use and permissions link/, () => {
    legalInfoPage.selectCookieUseAndPermissionsLink();
});

When(/^I select the third party acknowledgements link/, () => {
    legalInfoPage.selectThirdPartyAcknowledgementsLink();
});

Then(/^I should be shown the legal and privacy information page$/, () => {
    legalAndPrivacyPage.isPageVisible();
});

Then(/^I should be shown the cookie use and permissions page$/, () => {
    cookieUseAndPermissionsPage.isPageVisible();
});

Then(/^I should be shown the third party acknowledgements page$/, () => {
    thirdPartyAcknowledgementsPage.isPageVisible();
});

When(/^I select an external link on the (.*) page$/, (page) => {
    if (page === 'Legal and privacy') {
        legalInfoPage.selectLegalAndPrivacyLink();
        legalAndPrivacyPage.selectTermsAndConditionsLink();
    } else if (page === 'Cookie use and permissions') {
        legalInfoPage.selectCookieUseAndPermissionsLink();
        cookieUseAndPermissionsPage.selectCookiePolicyLink();
    } else {
        throw new Error(`Unknown legal info page: ${page}`);
    }
});

Then(/^I should be displayed a native winback with options to proceed or stay on the page$/, () => {
    const winback = new WinbackPopUpPage();
    winback.waitForPageLoad();
    winback.isConfirmButtonVisible();
    winback.isCancelButtonVisible();
    winback.selectCancelButton();
});

Then(/^I select (legal and privacy|cookie use and permissions|third party acknowledgements) from the legal information page overlay$/, (page) => {
    if (page === 'legal and privacy') {
        legalInfoPage.selectLegalAndPrivacyLink();
    } else if (page === 'cookie use and permissions') {
        legalInfoPage.selectCookieUseAndPermissionsLink();
    } else if (page === 'third party acknowledgements') {
        legalInfoPage.selectThirdPartyAcknowledgementsLink();
    } else {
        throw new Error(`Unknown legal info page: ${page}`);
    }
});

Then(/^I should be on the (.*) legal info item page overlay$/, (page) => {
    if (page === 'legal and privacy') {
        legalAndPrivacyPage.isPageVisible();
    } else if (page === 'cookie use and permissions') {
        cookieUseAndPermissionsPage.isPageVisible();
    } else if (page === 'third party acknowledgements') {
        thirdPartyAcknowledgementsPage.isPageVisible();
    } else {
        throw new Error(`Unknown legal info page: ${page}`);
    }
});

Then(/^I navigate back to legal info home page$/, () => {
    legalAndPrivacyPage.navigateBackToLegalInfo();
});

Then(/^I select the dismiss button from the legal information page overlay$/, () => {
    legalInfoPage.selectDismissButton();
});

Then(/^I select the dismiss button from the legal info item page overlay$/, () => {
    legalAndPrivacyPage.selectDismissButton();
});

Then(/^I select the back button from the legal info item page overlay$/, () => {
    legalAndPrivacyPage.selectBackButton();
});

Then(/^I should see the following links on legal info page$/, (legalInfo) => {
    const fieldsToValidate = legalInfo.hashes();
    legalInfoPage.viewFieldsOnLegalInfoPage(fieldsToValidate);
});

Then(/^I should see the legal and privacy information page$/, () => {
    legalAndPrivacyPage.viewLeagalAndPrivacyPage();
});

When(/^I select to view any external link$/, () => {
    legalAndPrivacyPage.selectTermsAndConditionsLink();
});
