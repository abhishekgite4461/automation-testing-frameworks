const {Then} = require('cucumber');
const MultipleDeviceManagementNotificationsPage = require('../../pages/settings/multipleDeviceManagementNotifications.page');

const multipleDeviceManagementNotificationsPage = new MultipleDeviceManagementNotificationsPage();

Then(/^I should be on the notifications page$/, () => {
    multipleDeviceManagementNotificationsPage.waitForNotificationsPageLoad();
    multipleDeviceManagementNotificationsPage.waitForAccountAlertsTileLoad();
});

Then(/^I select to enable notifications$/, () => {
    multipleDeviceManagementNotificationsPage.waitForNotificationsPageLoad();
    multipleDeviceManagementNotificationsPage.selectAllowNotificationsButton();
});

Then(/^I should see the alert toggle in the correct position according to my current status$/, () => {
    multipleDeviceManagementNotificationsPage.checkToggleStatusOn();
});

Then(/^I can see an option to enable notifications$/, () => {
    multipleDeviceManagementNotificationsPage.waitForGoToDeviceButton();
});

Then(/^I select Go to Device Settings Button$/, () => {
    multipleDeviceManagementNotificationsPage.newSelectGoToDeviceSettings();
});
