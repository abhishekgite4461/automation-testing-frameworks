const {Given, When, Then} = require('cucumber');

const BusinessSelectionPage = require('../pages/businessSelection.page');

const businessSelectionPage = new BusinessSelectionPage();
const AccountMatcher = require('../data/utils/accountMatcher');

Then(/^I should not see any business selection screen$/, () => {
    businessSelectionPage.shouldNotSeeBusinessSelectionPage();
});

Then(/^I should see business selection screen$/, () => {
    businessSelectionPage.waitForPageLoad();
    businessSelectionPage.shouldSeeSignatoryTitleIsPresent();
    businessSelectionPage.shouldSeeDelegateTitleIsPresent();
});

Then(/^I select business with (.*) account and (.*) access from business selection screen$/,
    (accountType, accessLevel) => {
        businessSelectionPage.waitForPageLoad();
        const businessName = AccountMatcher.getBusinessByAccessAndAccount(accountType, accessLevel);
        businessSelectionPage.selectBusinessWithinAccessLevel(accessLevel, businessName);
    });

Then(/^I select (.*) business with (.*) access from the business selection screen$/,
    (businessName, accessLevel) => {
        businessSelectionPage.waitForPageLoad();
        businessSelectionPage.selectBusinessWithinAccessLevel(accessLevel, businessName);
    });

When(/^I select first business of (.*) from selection screen$/, (accessLevel) => {
    businessSelectionPage.waitForPageLoad();
    businessSelectionPage.selectFirstBusinessOfAccessLevel(accessLevel);
});

When(/^I select first signatory business from selection screen$/, () => {
    businessSelectionPage.waitForPageLoad();
    businessSelectionPage.selectFirstSignatoryBusiness();
});

Given(/^I have access to multiple business$/, () => {
    businessSelectionPage.shouldSeeBusinessSelectionPage();
});

When(/^I select a business from the business selection screen$/, () => {
    businessSelectionPage.waitForPageLoad();
    businessSelectionPage.selectFirstBusiness();
});

Then(/^I should see switch business screen$/, () => {
    businessSelectionPage.waitForPageLoad();
});

Then(/^I should see switch business screen with (.*) business$/, (accessLevel) => {
    businessSelectionPage.waitForPageLoad();
    businessSelectionPage.shouldSeeAccessLevelTitleIsPresent(accessLevel);
});

Then(/^I should see switch business screen with delegate view only$/, () => {
    businessSelectionPage.waitForPageLoad();
    businessSelectionPage.shouldSeeDelegateTitleIsPresent();
});

Then(/^I should see business selection screen with available business$/, () => {
    businessSelectionPage.waitForPageLoad();
});

Then(/^I should see contact us option in business selection screen$/, () => {
    businessSelectionPage.shouldSeeContactUsButton();
});

Then(/^I should see all the (.*) businesses listed$/, (accessLevel) => {
    if (accessLevel === 'FULL_SIGNATORY') {
        businessSelectionPage.shouldSeeSignatoryTitleIsPresent();
    } else {
        businessSelectionPage.shouldSeeDelegateTitleIsPresent();
    }
    businessSelectionPage.shouldSeeBusinessInAlphabeticOrder(accessLevel);
});
