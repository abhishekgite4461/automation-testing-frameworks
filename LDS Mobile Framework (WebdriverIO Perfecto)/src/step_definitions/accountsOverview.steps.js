const {When, Then} = require('cucumber');
const AccountsOverviewPage = require('../pages/accountsOverview.page');

const accountsOverviewPage = new AccountsOverviewPage();

Then(/^I should see all the accounts in the account overview page$/, function (table) {
    accountsOverviewPage.waitForPageLoad();
    for (const account of table.hashes()) {
        const data = this.dataRepository.get();
        accountsOverviewPage.validateAccounts(data[account.name]);
    }
});

Then(/^I should see alert on the below (.*)$/, function (accountName) {
    const data = this.dataRepository.get();
    accountsOverviewPage.validateAlert(data[accountName]);
});

Then(/^I see the Account Link lead placement in the Account Overview page of (.*)$/, function (accountName) {
    const data = this.dataRepository.get();
    accountsOverviewPage.shouldSeeAccountLink(data[accountName]);
});

Then(/^I am (shown|not shown) Open Banking promo tile$/, (isShown) => {
    accountsOverviewPage.shouldSeeAccountAggregationTile(isShown === 'shown');
});

When(/^I am shown the option to add external accounts$/, () => {
    accountsOverviewPage.shouldSeeAddExternalAccount();
});

When(/^I am not shown the option to view my external accounts$/, () => {
    accountsOverviewPage.shouldNotSeeViewExternalAccount();
});

Then(/^I should (see|not see) ghost tile for external accounts load$/, (isShown) => {
    accountsOverviewPage.shouldSeeGhostTile(isShown === 'see');
});

Then(/^I select the ghost tile to validate it is not clickable$/, () => {
    accountsOverviewPage.selectGhostTile();
});

Then(/^I wait for ghost tile to disappear$/, () => {
    accountsOverviewPage.waitForGhostTileToDisappear();
});

Then(/^I should (see|not see) external (accounts|error accounts|expired accounts) loaded$/, (isShown, account) => {
    accountsOverviewPage.shouldSeeExternalAccountsLoaded(isShown === 'see', account);
});

Then(/^I proceed to add my external account$/, () => {
    accountsOverviewPage.selectAddExternalAccount();
});

Then(/^I should see the Open Banking Promo tile at the end of homepage$/, () => {
    accountsOverviewPage.shouldSeeOBPromoTileAsLastTile();
});

Then(/^I am (shown|not shown) the every day offers tile on home page$/, (isShown) => {
    accountsOverviewPage.shouldSeeRewardsTile(isShown === 'shown');
});

Then(/^I should see the Open Banking Promo tile before the every day offers tile$/, () => {
    accountsOverviewPage.shouldSeeRewardsTile(true);
    accountsOverviewPage.shouldSeeAddExternalAccount();
});

Then(/^I should be shown the list of added external bank account tiles above the Open Banking Promo Tile$/, () => {
    accountsOverviewPage.shouldSeeExternalAccountTilesAbovePromoTile();
});

Then(/^I should see below fields in external provider bank (.*) with account type (Current|Savings|Credit|.*)$/, function (bankName, accountType, table) {
    const data = this.dataRepository.get();
    const tableData = table.raw();
    accountsOverviewPage.shouldSeeInformationInExternalAccountTile(data[bankName], accountType, tableData);
});

Then(/^I should see (full|expired) error tile for external bank (.*)$/, function (errorType, bankName) {
    const data = this.dataRepository.get();
    accountsOverviewPage.shouldSeeExternalBankFullErrorTile(errorType, data[bankName]);
});

Then(/^I should (see|not see) (Any|Recently added|Renewed for 90 days) flag on external bank (.*) (Current|Credit|Savings|Islamic) account tile$/, function (isVisible, flag, bankName, accountType) {
    const data = this.dataRepository.get();
    accountsOverviewPage.shouldSeeFlagOnExternalProviderAccountTile(isVisible === 'see', flag, data[bankName], accountType);
});

Then(/^I should see balance (.*) on external bank (.*) (Current|Savings|Credit) account tile$/, function (balance, bankName, accountType) {
    const data = this.dataRepository.get();
    accountsOverviewPage.shouldSeeBalanceOnExternalProviderAccountTile(balance, data[bankName], accountType);
});

When(/^I am shown the Covid19 tile on home page$/, () => {
    accountsOverviewPage.shouldSeeCovid19Tile();
});

When(/^I select Find out how button on Covid19 tile$/, () => {
    accountsOverviewPage.selectFindOutMoreOnCovid19Tile();
});

When(/^I am shown the reward hub entry point tile on home page$/, () => {
    accountsOverviewPage.shouldSeeRewardHubEntryTile();
});

When(/^I select Find out how button on reward hub entry point tile$/, () => {
    accountsOverviewPage.selectFindOutMoreOnRewardHubEntryTile();
});

Then(/^analytics are logged for clicking the Covid19 tile$/, () => {
    accountsOverviewPage.shouldSeeAnalyticsForCovid19Tile();
});

Then(/^analytics are logged for clicking the Halifax Reward Hub tile$/, () => {
    accountsOverviewPage.shouldSeeAnalyticsForHalifaxRewardHubTile();
});
