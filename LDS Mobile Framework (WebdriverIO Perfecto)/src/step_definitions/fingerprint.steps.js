const {Given, When, Then} = require('cucumber');

const FingerprintInterstitialPage = require('../pages/fingerprint/fingerprintInterstitial.page');
const FingerprintEnrolmentPage = require('../pages/fingerprint/fingerprintEnrolment.page');
const FingerprintLoginPage = require('../pages/fingerprint/fingerprintLogin.page');
const FingerprintNotRegisteredPage = require('../pages/fingerprint/fingerprintNotRegistered.page');
const GlobalMenuPage = require('../pages/globalMenu.page');
const SettingsPage = require('../pages/settings/settings.page');
const SecuritySettingsPage = require('../pages/settings/securitySettings.page');
const logger = require('../utils/logger');

const securitySettingsPage = new SecuritySettingsPage();
const fingerprintInterstitialPage = new FingerprintInterstitialPage();
const fingerprintLoginPage = new FingerprintLoginPage();
const fingerprintEnrolmentPage = new FingerprintEnrolmentPage();
const fingerprintNotRegisteredPage = new FingerprintNotRegisteredPage();

Given(/^I have a fingerprint enabled phone$/, () => {
    // TODO actually check - however, there are no fingerprint methods within Appium
    logger.warn('Asserting fingerprint enabled phone');
});

Given(/^I have( no)? fingerprints registered$/, (negate) => {
    // TODO actually check - however, there are no fingerprint methods within Appium
    logger.warn(`Asserting${negate} fingerprints`);
});

When(/^I opt in for android fingerprint from the interstitial$/, () => {
    fingerprintInterstitialPage.waitForPageLoad();
    fingerprintInterstitialPage.optIn();
});

When(/^I opt out for android fingerprint from the interstitial$/, () => {
    fingerprintInterstitialPage.waitForPageLoad();
    fingerprintInterstitialPage.optOut();
});

When(/^I opt (in|out) for android fingerprint from settings$/, () => {
    securitySettingsPage.waitForPageLoad();
    securitySettingsPage.toggleFingerprintOptInStatus();
});

Then(/^I should be opted in for android fingerprint$/, () => {
    securitySettingsPage.waitForPageLoad();
    securitySettingsPage.shouldSeeFingerprintEnrolled();
});

Then(/^I should be opted out for android fingerprint$/, () => {
    securitySettingsPage.waitForPageLoad();
    securitySettingsPage.shouldSeeFingerprintNotEnrolled();
});

Then(/^I should be on the touch id\/fingerprint interstitial page$/, () => {
    fingerprintInterstitialPage.waitForPageLoad();
});

const iOptInInterstitial = function () {
    fingerprintInterstitialPage.waitForPageLoad();
    fingerprintInterstitialPage.optIn();
};

const declineFraudMessage = function () {
    fingerprintEnrolmentPage.waitForPageLoad();
    fingerprintEnrolmentPage.declineFraudMessage();
    fingerprintEnrolmentPage.dismissFailureMessage();
};

const acceptFraudMessage = function () {
    fingerprintEnrolmentPage.waitForPageLoad();
    fingerprintEnrolmentPage.acceptFraudMessage();
    fingerprintEnrolmentPage.dismissSuccessMessage();
};

Then(/^I accept the fraud message$/, acceptFraudMessage);

Then(/^I decline the fraud message$/, declineFraudMessage);

Then(/^I am notified that no fingerprints are registered in the phone$/, () => {
    fingerprintNotRegisteredPage.waitForPageLoad();
    fingerprintNotRegisteredPage.validateNoFingerprintMessage();
});

When(/^I dismiss the no fingerprints prompt$/, () => {
    fingerprintNotRegisteredPage.waitForPageLoad();
    fingerprintNotRegisteredPage.dismissNoFingerprintsMessage();
});

Then(/^I should be on the fingerprint interstitial page$/, () => {
    fingerprintInterstitialPage.waitForPageLoad();
});

Then(/^the fingerprint status is opted in$/, () => {
    securitySettingsPage.waitForPageLoad();
    securitySettingsPage.shouldSeeFingerprintEnrolled();
});

Then(/^the fingerprint status is opted out$/, () => {
    securitySettingsPage.waitForPageLoad();
    securitySettingsPage.shouldSeeFingerprintNotEnrolled();
});

const iDeclineFraudMessageAfterOptin = function () {
    iOptInInterstitial();
    declineFraudMessage();
};

const iAcceptFraudMessageAfterOptin = function () {
    iOptInInterstitial();
    acceptFraudMessage();
};

When(/^I opt in for fingerprint from the interstitial$/, iOptInInterstitial);

When(/^I decline the fraud message after the opt in from interstitial$/, iDeclineFraudMessageAfterOptin);

When(/^I have opted in to fingerprint authentication$/, iAcceptFraudMessageAfterOptin);

When(/^I opt out for fingerprint from the interstitial$/, () => {
    fingerprintInterstitialPage.waitForPageLoad();
    fingerprintInterstitialPage.optOut();
});

When(/^I will be presented with the fraud message$/, () => {
    fingerprintEnrolmentPage.waitForPageLoad();
    fingerprintEnrolmentPage.fraudMessage();
});

When(/^I am displayed with opt in or opt out of fingerprint authentication$/, () => {
    fingerprintInterstitialPage.waitForPageLoad();
    fingerprintInterstitialPage.title();
});

const iToggleFingerprint = function () {
    securitySettingsPage.waitForPageLoad();
    securitySettingsPage.toggleFingerprintOptInStatus();
};

Then(/^I opt in for fingerprint from settings$/, iToggleFingerprint);

Then(/^I opt out for fingerprint from settings$/, iToggleFingerprint);

Then(/^I should not see Opt In interstitial$/, () => {
    fingerprintInterstitialPage.waitForPageLoad();
    fingerprintInterstitialPage.noInterstitial();
});

const iNavigateToSecurityScreen = function () {
    const globalMenuPage = new GlobalMenuPage();
    globalMenuPage.openGlobalMenu();
    globalMenuPage.navigateToSettings();
    const settingsPage = new SettingsPage();
    settingsPage.selectSecuritySettings();
};

When(/^I navigate to the security settings screen$/, iNavigateToSecurityScreen);

const iOptedInFromSettings = function () {
    iNavigateToSecurityScreen();
    iToggleFingerprint();
    acceptFraudMessage();
};

When(/^I have opted in for fingerprint authentication from settings$/, iOptedInFromSettings);

const loginPrompt = function () {
    fingerprintLoginPage.waitForPageLoad();
};

Then(/^I am notified with the fingerprint login prompt$/, loginPrompt);

const cancelLoginPrompt = function () {
    fingerprintLoginPage.cancelFingerprintLogin();
};

When(/^I cancel the fingerprint login$/, cancelLoginPrompt);

const cancelLoginFallbackMI = function () {
    loginPrompt();
    cancelLoginPrompt();
};

When(/^I select on the cancel button from the fingerprint login prompt/, cancelLoginFallbackMI);

Then(/^I should be on the security settings page$/, () => {
    securitySettingsPage.waitForPageLoad();
});
