const {When, Then} = require('cucumber');
const SpendingRewardsPage = require('../pages/spendingRewards.page');

const spendingRewardsPage = new SpendingRewardsPage();

When(/^I should be on the spending rewards registration page$/, () => {
    spendingRewardsPage.waitForSpendingRewardsPageLoad();
});

When(/^I confirm my selection on the spending rewards page$/, () => {
    spendingRewardsPage.waitForSpendingRewardsPageLoad();
    spendingRewardsPage.clickYesForEverydayOfferButton();
});

When(/^I opt in for spending rewards$/, () => {
    spendingRewardsPage.waitForSpendingRewardsPageLoad();
    spendingRewardsPage.clickYesForEverydayOfferButton();
    spendingRewardsPage.clickRegisterPopUpConfirmButton();
});

When(/^I do not confirm my selection on spending rewards page$/, () => {
    spendingRewardsPage.waitForSpendingRewardsPageLoad();
    spendingRewardsPage.clickDoNotRegisterButton();
});

Then(/^I should be on the everyday view offers page$/, () => {
    spendingRewardsPage.waitForSpendingRewardsViewOfferPageLoad();
});

When(/^I select Active offers on everyday offers page$/, () => {
    spendingRewardsPage.selectActiveOffers();
});

When(/^I select New Offers on everyday offers page$/, () => {
    spendingRewardsPage.selectNewOffers();
});

When(/^I select Expired offers on everyday offers page$/, () => {
    spendingRewardsPage.selectExpiredOffers();
});

When(/^I should be on the spending rewards webview page$/, () => {
    spendingRewardsPage.waitForSpendingRewardsPageLoad();
});

When(/^I should see below options in the everyday offers page$/, (pageFields) => {
    const tableData = pageFields.hashes();
    spendingRewardsPage.validateFields(tableData);
});

When(/^I should see below options in the everyday opt in page$/, (pageFields) => {
    const tableData = pageFields.hashes();
    spendingRewardsPage.validateOtions(tableData);
});

When(/^I should be on New Tab selected by default$/, () => {
    spendingRewardsPage.verifyNewTab();
});

When(/^I select Active tab on everyday offers main page$/, () => {
    spendingRewardsPage.selectActiveOffers();
});

Then(/^I should be on Active tab$/, () => {
    spendingRewardsPage.verifyActiveTab();
});

When(/^I select everyday offers link to navigate everyday offers main page$/, () => {
    spendingRewardsPage.selectEverydayOffersLink();
});

When(/^I select expired offers option on everyday offers main page$/, () => {
    spendingRewardsPage.selectExpiredOffers();
});

Then(/^I should be on expired offers page$/, () => {
    spendingRewardsPage.verifyExpiredOffersPage();
});

When(/^I select setting option from everyday offers main page$/, () => {
    spendingRewardsPage.selectSettings();
});

Then(/^I should be on everyday offers setting page$/, () => {
    spendingRewardsPage.verifySettingPageTitle();
});

When(/^I select find out more option from everyday offers main page$/, () => {
    spendingRewardsPage.selectFindOutMore();
});

Then(/^I should be on find out more everyday offer page$/, () => {
    spendingRewardsPage.verifyFindOutMorePageTitle();
});
