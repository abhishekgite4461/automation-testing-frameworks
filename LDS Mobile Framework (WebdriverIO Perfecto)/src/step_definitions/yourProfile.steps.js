const {When} = require('cucumber');
const YourProfilePage = require('../pages/yourProfile.page');

const yourProfilePage = new YourProfilePage();
When(/^I should not see the market preference options on your profile page$/, () => {
    yourProfilePage.verifyMarketPreferenceIsNotVisible();
});

When(/^I select the manage data consent$/, () => {
    yourProfilePage.selectDataConsentLink();
});
