const path = require('path');
const fs = require('fs');
const Excel = require('exceljs/lib/exceljs.nodejs');
const denodeify = require('denodeify');
const Gherkin = require('@cucumber/gherkin');
const readdir = denodeify(require('recursive-readdir'));
const yargs = require('yargs');
const {IdGenerator} = require('@cucumber/messages');

const readFile = denodeify(fs.readFile);

const parser = new Gherkin.Parser(new Gherkin.AstBuilder(IdGenerator.incrementing()));

const baseNonEnvRunningTags = ['@stubOnly',
    '@noExistingData',
    '@instrument',
    '@sensorInstrument',
    '@outOfScope',
    '@rebootDevice',
    '@appiumOnly',
    '@manual',
    '@manualOnly',
    '@pending',
    '@team-uat',
    '@wkWebview',
    '@platinum',
    '@exhaustiveData'
];

const nonRunningSit10Tags = ['@sca',
    '@inAppSearch',
    '@coaLiberty',
    '@cop',
    '@appWarnBan'];

const {argv} = yargs(process.argv)
    .usage('Usage: npm test-map -- --featureFolder <Folders you want to pick tests from> --appType <App type>')
    .option({
        featureFolder: {
            alias: 'ff',
            default: './src/features/RNGA',
            describe: 'Folder path from where you want to read the feature files.'
        },
        outputFile: {
            alias: 'o',
            default: './TestMap.xlsx',
            describe: 'The output file for the testMap'
        },
        appType: {
            default: 'RNGA',
            choices: ['RNGA', 'BNGA'],
            describe: 'AppType you want to look into'
        },
        verbose: {
            alias: 'v',
            default: false,
            boolean: true
        }
    })
    .help();

function getFeatureFiles(featureFilesFolder) {
    return readdir(path.resolve(featureFilesFolder), ['!*.feature']);
}

function initializeScenarioInfo(tags, scenarioName) {
    let brandCount = 0;
    const capitalizeFirstCharacter = (word) => word.charAt(0).toUpperCase() + word.slice(1);
    const getOwners = () => tags.filter((tag) => tag.match(/@team-/)).map((t) => t.replace(/@|-/g, ' ').trim());
    const testID = (scenarioName.match(/TC(_)?\d+/) || ['null'])[0];
    const getPlatform = () => {
        const platform = tags.filter((tag) => tag.match(/@ios|@android/))[0];
        return platform ? capitalizeFirstCharacter(platform.replace('@', '')) : 'Both';
    };
    const isRNGA = tags.indexOf('@rnga') > -1;
    const isValidFor = (brand) => {
        const isBrandThere = tags.indexOf(`@${brand}`) > -1;
        const isMbnaSuite = tags.indexOf('@mbnaSuite') > -1;
        const specialBrandTags = tags.filter((t) => t.match(/^@(lds|bos|hfx|mbna)$/));
        const isBNGA = tags.indexOf('@bnga') > -1;
        let verdict = true;
        if (brand === 'mbna') {
            verdict = (isMbnaSuite || isBrandThere);
        } else if (brand === 'hfx' && isBNGA) {
            verdict = false;
        } else if (specialBrandTags[0]) {
            verdict = isBrandThere;
        }
        brandCount += (verdict ? 1 : 0);
        return verdict ? 'Yes' : 'No';
    };

    const isManual = tags.indexOf('@manual') > -1 || tags.indexOf('@manualOnly') > -1 || tags.indexOf('@pending') > -1;
    const getStubOrEnv = () => {
        const stubOrEnv = tags.filter((tag) => tag.match(/@stubOnly|@environmentOnly/))[0];
        return stubOrEnv ? capitalizeFirstCharacter(stubOrEnv.slice(1, -4)) : 'Both';
    };
    const isAppiumOnly = tags.indexOf('@appiumOnly') > -1;
    const isOptimisedScenario = tags.includes('@optFeature') && tags.some((tag) => ['@unEnrolled', '@genericAccount', '@otherAccount'].includes(tag));
    const isBaseNonRunningScenario = tags.some((tag) => baseNonEnvRunningTags.includes(tag));
    const isNonRunningSit10EnvScenario = tags.some((tag) => nonRunningSit10Tags.includes(tag));
    const isRunningSit02EnvScenario = tags.some((tag) => ['@sit02Job'].includes(tag));
    const actualSit02Runs = () => {
        if (isRNGA) {
            return (isOptimisedScenario && !isBaseNonRunningScenario && isRunningSit02EnvScenario) ? 'Yes' : 'No';
        }
        return 'No';
    };
    const actualSit10Runs = () => {
        if (isRNGA) {
            return (isOptimisedScenario && !isBaseNonRunningScenario && !isNonRunningSit10EnvScenario && !isRunningSit02EnvScenario) ? 'Yes' : 'No';
        }
        return (tags.includes('@optFeature') && !isBaseNonRunningScenario && !isNonRunningSit10EnvScenario) ? 'Yes' : 'No';
    };
    return {
        testID,
        tags: tags.join(', '),
        owners: getOwners().join(', '),
        automationStatus: isManual ? 'Manual' : 'Automated',
        envOrStub: getStubOrEnv(),
        platform: getPlatform(),
        platfromCount: getPlatform() === 'Both' ? 2 : 1,
        lds: isValidFor('lds'),
        bos: isValidFor('bos'),
        hfx: isValidFor('hfx'),
        mbna: isValidFor('mbna'),
        brandCount,
        appiumOrPerfecto: isAppiumOnly ? 'Appium' : 'Perfecto',
        actualSit10Runs: actualSit10Runs(),
        actualSit02Runs: actualSit02Runs(),
        optimisedScenario: tags.includes('@optFeature') ? 'Yes' : 'No'
    };
}

function parseFeatureFile(featureFile) {
    return readFile(featureFile).then((raw) => {
        const gherkinDoc = parser.parse(raw.toString());
        const pickles = new Gherkin.compile(gherkinDoc, '', IdGenerator.incrementing());
        const parsedFeatureFile = pickles.reduce((a, p) => {
            a[p.name] = a[p.name] || ({...initializeScenarioInfo(p.tags.map((t) => t.name), p.name)});
            a[p.name].examples = ++a[p.name].examples || 1;
            return a;
        }, {});
        return parsedFeatureFile;
    });
}

async function parseFeatureFilesFromFolder(folderPath) {
    const featureFiles = await getFeatureFiles(folderPath);
    return featureFiles.reduce((accumulator, featureFilePath) => {
        const featureFile = path.basename(featureFilePath);
        return accumulator.then((acc) => parseFeatureFile(featureFilePath).then((pff) => {
            acc[featureFile] = pff;
            return acc;
        }));
    }, Promise.resolve({}));
}

const initializeSheet = (sheet) => {
    sheet.columns = [
        {header: 'FeatureFile', width: 20, outlineLevel: 1, alignment: {wrapText: true}},
        {header: 'TestID', width: 10, outlineLevel: 1},
        {header: 'Scenario', width: 30, outlineLevel: 1, alignment: {wrapText: true}},
        {header: 'Examples', width: 10, outlineLevel: 1},
        {header: 'Owner', width: 15, outlineLevel: 1},
        {header: 'AutomationStatus', width: 10, outlineLevel: 1},
        {header: 'Environment/Stub', width: 10, outlineLevel: 1},
        {header: 'Platform', width: 10, outlineLevel: 1},
        {header: 'pCount', width: 10, outlineLevel: 1},
        {header: 'MBNA', width: 10, outlineLevel: 1},
        {header: 'LDS', width: 10, outlineLevel: 1},
        {header: 'HFX', width: 10, outlineLevel: 1},
        {header: 'BOS', width: 10, outlineLevel: 1},
        {header: 'ExecutionCount', width: 10, outlineLevel: 1},
        {header: 'Tags', width: 10, outlineLevel: 1},
        {header: 'Perfecto/Appium', width: 10, outlineLevel: 1},
        {header: 'Actual Sit10 Runs', width: 10, outlineLevel: 1},
        {header: 'Actual Sit02 Runs', width: 10, outlineLevel: 1},
        {header: 'Opt Feature Tag', width: 10, outlineLevel: 1}
    ];
};

async function generateWorkBook(featureFolder) {
    const workbook = new Excel.Workbook();
    const sheet = workbook.addWorksheet('testMap', {views: [{state: 'frozen', xSplit: 2, ySplit: 1}]});
    initializeSheet(sheet);

    const parsedFeatureFiles = await parseFeatureFilesFromFolder(featureFolder);
    Object.keys(parsedFeatureFiles).forEach((featureFileName) => {
        const allScenarios = parsedFeatureFiles[featureFileName];
        Object.keys(allScenarios).forEach((scenarioName, scenarioIndex) => {
            const scenarioInformation = allScenarios[scenarioName];
            const executionCount = scenarioInformation.examples
                * scenarioInformation.platfromCount
                * scenarioInformation.brandCount;
            const rowData = [
                scenarioIndex ? '' : featureFileName,
                scenarioInformation.testID,
                scenarioName,
                scenarioInformation.examples,
                scenarioInformation.owners,
                scenarioInformation.automationStatus,
                scenarioInformation.envOrStub,
                scenarioInformation.platform,
                scenarioInformation.platfromCount,
                scenarioInformation.mbna,
                scenarioInformation.lds,
                scenarioInformation.hfx,
                scenarioInformation.bos,
                executionCount,
                scenarioInformation.tags,
                scenarioInformation.appiumOrPerfecto,
                scenarioInformation.actualSit10Runs,
                scenarioInformation.actualSit02Runs,
                scenarioInformation.optimisedScenario
            ];
            sheet.addRow(rowData);
        });
    });
    return workbook;
}

generateWorkBook(argv.featureFolder).then((workbook) => workbook.xlsx.writeFile(argv.outputFile));
