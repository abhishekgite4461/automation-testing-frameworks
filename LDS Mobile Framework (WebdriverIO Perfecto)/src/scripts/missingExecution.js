const {exec} = require('child_process');
const _ = require('lodash');

const cucumberJs = './node_modules/.bin/cucumber-js';
const request = require('request');
const moment = require('moment');
const presets = require('../presets/presetRunTypes.js');
const {cartesianProduct} = require('../utils/utils.js');
const logger = require('../utils/logger');

const {PERFECTO_TOKEN} = process.env;
const allExpectedScenarios = {};
const CQL_NAME = 'lloyds';
const REPORTING_SERVER_URL = `https://${CQL_NAME}.reporting.perfectomobile.com`;
const startOfDay = moment()
    .startOf('day')
    .add(4, 'hours')
    .unix() * 1000;
const PresetToCheck = [
    'weeknightMasterAndroidNonGeneric',
    'weeknightMasterAndroidGeneric',
    'weeknighMasterIosGeneric',
    'weeknighMasterIosNonGeneric'
];
const allExecutedScenarios = {};

/**
 *
 * @param jobName - Job Name in perfecto to retrieve test execution
 * @param deviceId deviceId where tests are executed
 */
function fetchPerfectoExecutions(jobName, deviceId) {
    const apiUrl = `${REPORTING_SERVER_URL}/export/api/v1/test-executions`;

    const payload = {
        'startExecutionTime[0]': startOfDay,
        'endExecutionTime[0]': (new Date().getTime()),
        'jobName[0]': jobName,
        'deviceId[0]': deviceId
    };
    if (!PERFECTO_TOKEN) throw new Error('PERFECTO_TOKEN must be defined');
    return new Promise(((resolve, reject) => {
        request.get({
            url: apiUrl,
            qs: payload,
            headers: {PERFECTO_AUTHORIZATION: PERFECTO_TOKEN}
        }, (err, resp, body) => {
            if (err) {
                reject(err);
            } else {
                resolve(JSON.parse(body).resources);
            }
        });
    }));
}

function getPerfectoExecutions(presetName, brand, platform, deviceId) {
    return new Promise((resolve, reject) => {
        if (!allExecutedScenarios[presetName][brand]) {
            allExecutedScenarios[presetName][brand] = {};
        }
        // need to expand this for multiple devices
        fetchPerfectoExecutions(`nga-3-aft-${presetName}`, deviceId)
            .then((res) => {
                allExecutedScenarios[presetName][brand][platform][deviceId] = res;
                resolve();
            })
            .catch(reject);
    });
}

const executeDryRun = (presetName, platform, brand, deviceId) => new Promise((resolve, reject) => {
    const platformExpression = platform === 'Android' ? 'not @ios' : 'not @android';
    const typeTag = [`@${presets[presetName].type.toString()}`.toLowerCase()];
    const brandTag = `@${brand.toLowerCase()}`;
    const excludedBrandTags = ['@lds', '@hfx', '@bos', '@mbna'].filter((tag) => tag !== brandTag);
    const brandExpression = `(${brandTag} or (not ${excludedBrandTags.join(' and not ')}))`;
    const notSitO2 = ['not @sit02Job'];
    const extraTags = ['not @pending', 'not @manual', 'not @appium'].join(' and ');
    const tagExpression = [presets[presetName].tags(brand), extraTags, platformExpression, brandExpression, notSitO2, typeTag].join(' and ');
    const command = `${cucumberJs} src -t "${tagExpression}" --dry-run | grep "Scenario"`;
    exec(command,
        (error, stdout) => {
            if (error) {
                reject(error);
            } else {
                try {
                    const scenariosList = stdout.trim()
                        .split('\n');
                    const scenarios = [];
                    for (let i = 0; i < scenariosList.length; i++) {
                        scenarios.push(scenariosList[i].match(/Scenario: (.*?) #/)[1]);
                    }
                    allExpectedScenarios[presetName][brand][platform][deviceId] = scenarios;
                    resolve(scenarios);
                } catch (err) {
                    reject(err);
                }
            }
        });
});

const getExecutedAndExpectedScenarios = (presetName) => new Promise(((resolve, reject) => {
    allExpectedScenarios[presetName] = {};
    allExecutedScenarios[presetName] = {};
    const presetPlatform = presets[presetName].platform;
    const presetBrand = presets[presetName].brand;
    const brandPlatformMap = _.map(cartesianProduct(
        presetPlatform, presetBrand
    ));

    Promise.all(_.flatMap(brandPlatformMap, (brandPlatform) => {
        const deviceId = presets[presetName].deviceIds(...brandPlatform)[0];
        const [platform, brand] = brandPlatform;
        if (!allExpectedScenarios[presetName][brand]) {
            allExpectedScenarios[presetName][brand] = {};
            allExecutedScenarios[presetName][brand] = {};
        }
        if (!allExpectedScenarios[presetName][brand][platform]) {
            allExpectedScenarios[presetName][brand][platform] = {};
            allExecutedScenarios[presetName][brand][platform] = {};
        }
        allExpectedScenarios[presetName][brand][platform][deviceId] = {};
        allExecutedScenarios[presetName][brand][platform][deviceId] = {};
        return [executeDryRun(presetName, platform, brand, deviceId),
            getPerfectoExecutions(presetName, brand, platform, deviceId)];
    }))
        .then(resolve)
        .catch(reject);
}));

const compareActualAndExpected = () => {
    const jobNames = Object.keys(allExpectedScenarios);
    jobNames.forEach(((presetName) => {
        const brands = Object.keys(allExpectedScenarios[presetName]);
        brands.forEach(((brand) => {
            const platforms = Object.keys(allExpectedScenarios[presetName][brand]);
            platforms.forEach((platform) => {
                const deviceIds = Object.keys(allExpectedScenarios[presetName][brand][platform]);
                deviceIds.forEach((deviceId) => {
                    const executedScenarios = _.map(allExecutedScenarios[presetName][brand][platform][deviceId],
                        (scenario) => scenario.name);
                    const expectedScenarios = allExpectedScenarios[presetName][brand][platform][deviceId];
                    const differenceArray = expectedScenarios
                        .filter((x) => !executedScenarios.includes(x))
                        .concat(executedScenarios.filter((x) => !expectedScenarios.includes(x)));
                    logger.info(`${presetName}:${brand}:${platform}`);
                    logger.info(`expectedScenarios ${expectedScenarios.length}`);
                    logger.info(`executedScenarios ${executedScenarios.length}`);
                    logger.info(`Difference in scenarios  ${differenceArray.length}`);
                    logger.info(`${JSON.stringify(differenceArray)}`);
                });
            });
        }));
    }));
};

const validateFunctionalEnvironmentPresets = function () {
    Promise.all(_.map(PresetToCheck, (presetName) => getExecutedAndExpectedScenarios(presetName)))
        .then(compareActualAndExpected)
        .catch((err) => logger.error((err.message)));
};

module.exports = {
    validateFunctionalEnvironmentPresets
};
