const _ = require('lodash');

const {PerfectoRestClient} = require('../utils/perfectoRestClient');
const {DeviceReservation} = require('./createDeviceReservations');
const logger = require('../utils/logger');

const perfectoRestClient = new PerfectoRestClient();

const isDeviceConnected = async (deviceId) => {
    let errMessage = null;
    // eslint-disable-next-line no-return-assign
    const errAssign = (err) => errMessage = err;
    const executionId = await perfectoRestClient.startExecution();
    const res = await perfectoRestClient.isDeviceConnected(executionId, deviceId)
        .then(() => `${deviceId} is CONNECTED`)
        .catch(errAssign);
    if (!errMessage) {
        return res;
    }
    throw new Error(errMessage.message);
};

function handleRejection(p) {
    return p.catch((err) => (err.message));
}

async function devicesConnected() {
    const res = await Promise.all(_.map(DeviceReservation.getPresetDevices('weeknightFunctionalEnvironmentBnga'), (deviceId) => handleRejection(isDeviceConnected(deviceId))));
    res.concat(await Promise.all(_.map(DeviceReservation.getPresetDevices('weeknightFunctionalEnvironmentMasterAndroid'), (deviceId) => handleRejection(isDeviceConnected(deviceId)))));
    res.concat(await Promise.all(_.map(DeviceReservation.getPresetDevices('weeknightFunctionalEnvironmentMasterIos'), (deviceId) => handleRejection(isDeviceConnected(deviceId)))));
    const filterFailedDevices = _.filter(res, (val) => !(JSON.stringify(val)
        .includes(' is CONNECTED')));
    logger.info('-------------------------');
    if (filterFailedDevices.length > 0) {
        logger.info(filterFailedDevices);
        process.exit(1);
    }
    logger.info('All Devices are CONNECTED');
    logger.info('-------------------------');
}

module.exports = {
    devicesConnected,
    isDeviceConnected
};
