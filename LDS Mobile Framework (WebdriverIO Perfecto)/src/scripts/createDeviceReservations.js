const moment = require('moment');
const _ = require('lodash');
const logger = require('../utils/logger');
const {PerfectoRestClient} = require('../utils/perfectoRestClient');
const presetRunTypes = require('../presets/presetRunTypes');
const {cartesianProduct} = require('../utils/utils');

const perfectoClient = new PerfectoRestClient();

const args = process.argv.slice(2);
const preset = process.env.PRESET || args[0];
const timeoutMins = process.env.TIMEOUT_HOURS || 20;

// calculate the unix time by adding extra mins to current time
const addMins = (min) => moment(new Date())
    .add(min, 'minutes')
    .unix() * 1000;

class DeviceReservation {
    static getPresetDevices(presetName) {
        const presetInfo = presetRunTypes[presetName];
        if (presetInfo && presetInfo.deviceIds) {
            try {
                return presetInfo.deviceIds();
            } catch (err) {
                const cc = cartesianProduct(presetInfo.platform, presetInfo.brand);
                const deviceList = _.map(cc, (val) => presetInfo.deviceIds.apply(this, val));
                return deviceList.reduce((a, b) => a.concat(b), []);
            }
        }
        return 0;
    }

    static forceDeviceReservation(deviceList, startTime, endTime) {
        return perfectoClient.removeDeviceReservations(deviceList, startTime, endTime)
            .then((failures) => {
                if (failures.length !== 0) {
                    logger.error(`Failed to delete some device reservations: \n\t${failures.join('\n\t')}`);
                }
                return perfectoClient.makeDeviceReservations(deviceList, startTime, endTime)
                    .then((reservations) => {
                        logger.info(`Reservations made : ${reservations}`);
                    })
                    .catch(() => false);
            })
            .catch(() => Promise.resolve('removeDeviceReservations failed'));
    }

    /** *
     *
     * @param presetName - reserve devices used in this preset
     * @param startTimeMins - current time offset in minutes to start reservation
     * @param endTimeMins -  total time in minutes
     */
    static createPresetReservation(presetName = preset, startTimeMins = 1, endTimeMins = 20) {
        const deviceList = DeviceReservation.getPresetDevices(presetName);
        const endTime = (process.env.TIMEOUT_HOURS * 60) || endTimeMins;
        if (presetName && deviceList.length > 0) {
            return DeviceReservation.forceDeviceReservation(
                deviceList, addMins(startTimeMins), addMins(endTime)
            );
        }
        return Promise.resolve('Invalid preset or No devices to reserve');
    }
}

function createReservation() {
    DeviceReservation.createPresetReservation(preset, 1, timeoutMins)
        .then((res) => {
            logger.info(res);
        });
}

module.exports = {
    DeviceReservation,
    createReservation,
    createPresetReservation: DeviceReservation.createPresetReservation,
    forceDeviceReservation: DeviceReservation.forceDeviceReservation,
    getPresetDevices: DeviceReservation.getPresetDevices
};
