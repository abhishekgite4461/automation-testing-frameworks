const {exec} = require('child_process');
const _ = require('lodash');
const yargs = require('yargs');

const logger = require('../utils/logger');
const presets = require('../presets/presetRunTypes');
const {cartesianProduct} = require('../utils/utils');
const {PerfectoRestClient} = require('../utils/perfectoRestClient');
const TDaaS2Client = require('../utils/tdaas2Client');
const verifyAndTransformAccounts = require('../utils/accountSplitter.2.0');

const {argv} = yargs(process.argv)
    .usage('Usage: npm run data-verification -- --preset <preset to run>')
    .option({
        preset: {
            alias: 'p',
            default: 'weeknightFunctionalEnvironmentMasterAndroid',
            choices: presets.symbols().map((preset) => Symbol.keyFor(preset.symbol)),
            describe: 'the preset you want to verify data for'
        },
        verbose: {
            alias: 'v',
            default: false,
            boolean: true
        }
    })
    .help();

const cucumberjs = './node_modules/.bin/cucumber-js';

const perfectoClient = new PerfectoRestClient();

const extraTags = [
    'not @pending',
    'not @manual',
    'not @appium'
].join(' and ');

const tagAsPerAppType = (appType) => `and ${appType === 'RNGA' ? '@rnga' : '@bnga'}`;

const crawlThroughPresets = (verifyThisPreset) => new Promise((resolve, reject) => {
    const phoneNumberPromises = [];
    const tagGroups = {};
    const presetToCheck = presets[verifyThisPreset];
    if (presetToCheck.dataRepository !== 'remote') {
        logger.info('Data analysis skipped.');
        logger.info(`${verifyThisPreset} is not using the remote data repository`);
        process.exit(0);
    }
    if (presetToCheck.dataRepository) {
        cartesianProduct(presetToCheck.platform, presetToCheck.server, presetToCheck.brand, presetToCheck.type)
            .forEach((element) => {
                const [platform, server, brand, type] = element;
                presetToCheck.deviceIds(platform, brand)
                    .forEach((device) => {
                        const tags = `(${typeof presetToCheck.tags !== 'function' ? presetToCheck.tags : presetToCheck.tags(brand)}) and ${extraTags} ${tagAsPerAppType(type)}`;
                        if (!tagGroups[tags]) {
                            tagGroups[tags] = [];
                        }
                        phoneNumberPromises.push(perfectoClient.getAllDeviceInfo(device)
                            .then((deviceInfo) => {
                                tagGroups[tags].push({
                                    phoneNumber: deviceInfo.phoneNumber,
                                    dataRepository: presetToCheck.dataRepository,
                                    brand,
                                    server,
                                    type
                                });
                            }).catch((error) => {
                                logger.error(error);
                                reject(error);
                            }));
                    });
            });
    }
    Promise.all(phoneNumberPromises).then(() => {
        if (argv.verbose) {
            logger.info(tagGroups);
        }
        resolve(tagGroups);
    });
});

const cucumberDryRun = (tagExpression) => new Promise((resolve, reject) => {
    const command = `${cucumberjs} src -t '${tagExpression}' --dry-run | grep "Step:.*\\(Given\\|And\\)"`;
    if (argv.verbose) {
        logger.info(command);
    }
    exec(command,
        (error, stdout) => {
            if (error) {
                reject(error);
            } else {
                const parsed = stdout.split('\n').reduce((array, line) => {
                    const result = line.match(/Given (.*) - /)
                        || line.match(/And (.*) - /);
                    if (result) {
                        const step = result[1].substring(0, result[1].length - 5);
                        const accounts = step.match(/^I am a (.*) user$/)
                            || step.match(/^I am a (.*) user on the home page$/)
                            || step.match(/^I am an enrolled "(.*)" user logged into the app$/)
                            || step.match(/^I am a (.*) user on the MI page$/)
                            || step.match(/^I submit ".*" MI as "(.*)" user on the MI page$/);
                        if (accounts) {
                            const upperCaseAccounts = accounts[1].toUpperCase();
                            array[upperCaseAccounts] = ++array[upperCaseAccounts] || 1;
                        } else {
                            // TODO: Find a smarter way to deal with this.
                            // logger.warn(`Not matching ${step}`);
                        }
                    }
                    return array;
                }, {});
                if (argv.verbose) {
                    logger.info(parsed);
                }
                resolve(parsed);
            }
        });
});

const tdaas2Client = new TDaaS2Client();
const foundCustomers = [];

crawlThroughPresets(argv.preset).then((tagGroups) => {
    const tags = Object.keys(tagGroups);
    return Promise.all(tags.map((tag) => cucumberDryRun(tag)))
        .then((accounts) => Promise.all(_.zip(tags, accounts).map(((x) => {
            const tagExpression = x[0];
            const dataRequests = tagGroups[tagExpression];
            const rawAccountTypes = Object.keys(x[1]);
            const accountTypes = rawAccountTypes.map((a) => verifyAndTransformAccounts(a));
            return Promise.all(dataRequests.map((dataRequest) => {
                const problems = [];
                let promise = Promise.resolve();
                accountTypes.forEach((a) => {
                    promise = promise.then(() => tdaas2Client.createReservation(
                        dataRequest.server,
                        dataRequest.type,
                        dataRequest.brand,
                        dataRequest.brand !== 'MBNA' ? a : 'CREDITCARD',
                        dataRequest.phoneNumber
                    )
                        .then((customer) => {
                            foundCustomers.push(customer);
                            return tdaas2Client.deleteReservation(customer);
                        })
                        .catch((error) => {
                            // eslint-disable-next-line no-underscore-dangle
                            const request = error.response.request._data;
                            problems.push({
                                error,
                                request
                            });
                        }));
                });
                return promise.then(() => problems);
            }));
        }))));
})
    .then((a) => _.flattenDeep(a))
    .then((problems) => {
        problems.forEach((problem) => {
            logger.error({
                status: problem.error.status,
                request: problem.request
            });
        });
        logger.info(`You are missing ${problems.length} types of data in TDaaS 2.0`);
        if (argv.verbose) {
            logger.info('Below are all the customers that will be used during execution:');
            logger.info(`[ ${foundCustomers.map((customer) => `ObjectId('${customer._id}')`).join(', ')} ]`);
        }
    });
