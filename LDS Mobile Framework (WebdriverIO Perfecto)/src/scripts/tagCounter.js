const {exec} = require('child_process');

const logger = require('../utils/logger');

const cucumberjs = 'node_modules/.bin/cucumber-js';

const teamTags = [
    '@team-mpt',
    '@team-statements',
    '@team-payments',
    '@team-panm',
    '@team-outboundmessaging',
    '@team-mbna',
    '@team-login',
    '@team-release',
    '@team-servicing',
    '@team-pi',
    '@team-aov'
];
const tags = [
    '@exhaustiveData',
    '@noExistingData',
    '@pending',
    '@defect',
    '@iosDefect',
    '@androidDefect',
    '@manual'
];

// The below is just to be able to use `flatMap` on node 6, as it wasn't added until node 11.
const concat = (x, y) => x.concat(y);
const flatMap = (f, xs) => xs.map(f).reduce(concat, []);
// eslint-disable-next-line no-extend-native
Array.prototype.flatMap = function (f) {
    return flatMap(f, this);
};

logger.info('Counting tags... (takes about half a minute)');
Promise.all(teamTags.flatMap((teamTag) => tags.map((tag) => ({teamTag, tag})))
    .map(({teamTag, tag}) => new Promise(
        (resolve, reject) => exec(`${cucumberjs} src -t '${teamTag} and ${tag}' --dry-run | grep "^[0-9]* scenario"`,
            (error, stdout) => {
                if (error) {
                    reject(error);
                } else {
                    const count = parseInt(stdout.match(/^[0-9]*/)[0], 10);
                    resolve({teamTag, tag, count});
                }
            })
    )))
    .then((result) => {
        let output = '\n';
        let currentTeam = null;
        result.forEach((entry) => {
            if (currentTeam !== entry.teamTag) {
                currentTeam = entry.teamTag;
                output = output.concat(`\n${entry.teamTag}\n`);
            }
            output = output.concat(`  ${entry.tag}: ${entry.count}\n`);
        });
        logger.info(`${output}`);
    });
