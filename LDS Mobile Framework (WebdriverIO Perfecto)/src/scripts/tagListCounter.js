const {exec} = require('child_process');
const tagList = require('../gherkin_lint_rules/tagList');
const logger = require('../utils/logger');

const cucumberjs = './node_modules/.bin/cucumber-js';

const countTags = () => Object.keys(tagList)
    .map((key) => new Promise((resolve, reject) => {
        exec(`${cucumberjs} src -t '${key}' --dry-run | grep "^[0-9]* scenario"`,
            (error, stdout) => {
                if (error) {
                    reject(error);
                } else {
                    const count = parseInt(stdout.match(/^[0-9]*/)[0], 10);
                    resolve({key, count});
                }
            });
    }));

Promise.all(countTags())
    .then((res) => logger.info(res))
    .catch((err) => logger.error(err));
