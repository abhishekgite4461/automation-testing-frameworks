const {PerfectoRestClient} = require('../utils/perfectoRestClient');
const utils = require('../utils/utils');
const logger = require('../utils/logger');

utils.envVariableExists('PERFECTO_USER');
utils.envVariableExists('PERFECTO_PWD');

const perfectoRestClient = new PerfectoRestClient();

const MEDIA_FOLDER = 'media/applications';
let itemCount = 0;

/*
* The method queries the root folder and deletes all the artifacts under
* the root folder. Each delete operation is separated by a 500ms delay
* to allow perfecto to delete and refresh. This ensures that containing
* folder when deleted do not report 'Not Empty' error.
* */
const deletePerfectoArtifacts = (artifactRoot) => perfectoRestClient.listRepositoryItem(artifactRoot)
    .then((result) => result.items.reverse().map((item) => item.slice(item.indexOf('/') + 1)))
    .then((itemList) => itemList.forEach(
        (itemName) => setTimeout(() => perfectoRestClient.deleteRepositoryItem(`${artifactRoot}/${itemName}`)
            .then((result) => {
                if (result.status === 'Success') {
                    logger.info(`Successfully deleted artifact '${itemName}'`);
                } else {
                    logger.info(`Failed to delete artifact '${itemName}': ${result.completionCode}`);
                }
            }), 500 * itemCount++)
    ))
    .catch((err) => {
        logger.error(err);
        process.exit(1);
    });

deletePerfectoArtifacts(MEDIA_FOLDER);
