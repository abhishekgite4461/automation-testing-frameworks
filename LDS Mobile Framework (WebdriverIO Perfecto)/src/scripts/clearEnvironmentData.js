const yargs = require('yargs');
const logger = require('../utils/logger');

const Tdaas2Client = require('../utils/tdaas2Client');

const tdaas2Client = new Tdaas2Client();
const EnvironmentApiClient = require('../utils/environmentApiClient');
const presets = require('../presets/presetRunTypes');

const {argv} = yargs(process.argv)
    .usage('Usage: npm run clearData -- --preset <preset to run>')
    .option({
        preset: {
            alias: 'p',
            default: 'weeknightMasterAndroidGeneric',
            choices: presets.symbols().map((preset) => Symbol.keyFor(preset.symbol)),
            describe: 'the preset you want to clear data for'
        }
    })
    .help();

const userIds = [];
const info = {
    brand: 'LDS',
    appType: 'RNGA',
    group: 'jenkins'
};

const environment = presets[argv.preset].server[0];
const shouldClearData = ['SIT10-W', 'PUT12'].includes(environment);
if (shouldClearData) {
    Promise.resolve(tdaas2Client.getAllCustomers()).then((result) => {
        result.forEach((value) => {
            if (value.groups[0] === info.group) {
                if (value.environment === environment) {
                    userIds.push(value.username);
                }
            }
        });
        EnvironmentApiClient.clearDeviceList(environment, info.brand, info.appType, userIds[0], userIds);
    }).catch((err) => {
        logger.error(err);
    });
}
