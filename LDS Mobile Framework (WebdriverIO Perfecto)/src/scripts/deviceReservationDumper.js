const denodify = require('denodeify');
const Mustache = require('mustache');
const moment = require('moment');
const writeFile = denodify(require('fs').writeFile);

const logger = require('../utils/logger');
const {PerfectoRestClient} = require('../utils/perfectoRestClient');

const perfectoClient = new PerfectoRestClient();

const template = `<html>
<head>

    <style type="text/css">
           table {
       padding: 10px;
       box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
       border-radius: 3px;
    }
    td {
       background-color: #EBEBEB;
       padding: 10px;
    }
    th {
       padding: 10px;
    }
    tr {
        background-color: #1b1e24;
        color: #000000;
        font-size: 16px;
    }
    </style>
    <title></title>
</head>
<body>
    <h1>Reserved devices</h1>
    <p>From -3 hours to +5 hours. Timestamps in UTC.</p>
    <table style="width:50%;">
        <tr style='color: #ffffff'>
            <th>Reserved By</th>
            <th>Device Id</th>
            <th>Start Time</th>
            <th>End Time</th>
        </tr>
        {{#reservations}}
        <tr>
            <td>{{reservedBy}}</td>
            <td>{{deviceId}}</td>
            <td>{{startTime}}</td>
            <td>{{endTime}}</td>
        </tr>
        {{/reservations}}
    </table>
</body>
</html>`;

const render = (view) => Mustache.render(template, view);

perfectoClient.getAllDeviceIds()
    .then((deviceIds) => {
        const startTimeMilis = (moment().subtract(3, 'hours').unix() * 1000);
        const endTimeMilis = (moment().add(5, 'hours').unix() * 1000);
        return perfectoClient.getCurrentReservations(deviceIds, startTimeMilis, endTimeMilis);
    })
    .then((reservations) => {
        const view = reservations.map((reservation) => ({
            startTime: reservation.startTime.formatted,
            endTime: reservation.endTime.formatted,
            status: reservation.status,
            created: reservation.created.formatted,
            lastModified: reservation.lastModified.formatted,
            reservedBy: reservation.reservedBy,
            deviceId: reservation.deviceId
        }));
        return render({reservations: view});
    })
    .then((report) => {
        writeFile('reservationReport.html', report);
        return report;
    })
    .then((result) => logger.info(result))
    .catch((err) => {
        logger.error(err);
        process.exit(1);
    });
