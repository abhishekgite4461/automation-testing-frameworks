/**
 * This script is designed to be called by the CI server to aggregate the results of several large
 * test runs into a single summary document.
 */
const fs = require('fs');
const denodeify = require('denodeify');
const process = require('process');
const request = require('request-promise');
const Gherkin = require('@cucumber/gherkin');
const Mustache = require('mustache');
const _ = require('lodash');
const {TagExpressionParser} = require('@cucumber/tag-expressions');
const {IdGenerator} = require('@cucumber/messages');

const reportId = require('yargs').demandCommand(1).argv._[2];

const logger = require('../utils/logger');
const {getFeatureFiles} = require('../runner/runner');
const devices = require('../wdio_config/perfectoDeviceList');
const compatibilityTeams = require('../presets/compatibilityTeams');

const readFile = denodeify(fs.readFile);
const writeFile = denodeify(fs.writeFile);

const gherkinParser = new Gherkin.Parser(new Gherkin.AstBuilder(IdGenerator.incrementing()));

const percentage = (x) => (x * 100).toFixed(2);
const average = (x) => (_.sum(x) / x.length).toFixed(2);

const fetchTestReportInfo = (primer) => {
    const fetchSingle = (uri, parser) => {
        if (uri === undefined) {
            return {};
        }
        return request({uri})
            .then(JSON.parse)
            .then(parser)
            .catch((err) => {
                logger.error(`Error parsing report from ${uri}, error was ${err}`);
                logger.error(err.stack);
                return {};
            });
    };

    return Promise.all(Object.keys(primer.jobs).map((jobName) => {
        const jobUrls = primer.jobs[jobName];
        const basicInfo = {
            jobName,
            url: jobUrls.url,
            aggregatedReportUrl: jobUrls.aggregatedReportUrl,
            reportingJobUrl: primer.buildUrl,
            brand: primer.brand,
            jenkinsTags: primer.tags,
            branch: primer.branch,
            buildDuration: jobUrls.buildDuration
        };
        const enrichedJsonInfo = fetchSingle(jobUrls.enrichedJsonUrl, (enrichedJson) => ({
            featureCount: enrichedJson.featureCount,
            scenarios: enrichedJson.scenarios,
            features: enrichedJson.features
        }));
        const testrunnerInfo = fetchSingle(jobUrls.testrunnerReportUrl, (testrunnerReport) => ({
            testrunnerReport
        }));
        return Promise.all([enrichedJsonInfo, testrunnerInfo])
            .then((data) => Object.assign({}, basicInfo, ...data));
    }));
};

const mungeTestReportInfo = (rawJobs, featureFileInfo) => {
    const calculateHeadlineJobSuccessRate = (job) => {
        let testrunnerSuccess = 0;
        if (job.testrunnerReport) {
            testrunnerSuccess = (job.testrunnerReport.totalRunners - job.testrunnerReport.errors.length)
                / job.testrunnerReport.totalRunners;
        }
        const successIfRunnerSpawned = job.featureCount ? parseFloat(job.featureCount.passedPercentage) / 100 : 0;

        return testrunnerSuccess * successIfRunnerSpawned;
    };

    const totalFeaturesExecuted = _.sum(rawJobs.map((job) => (job.featureCount ? job.featureCount.passed : 0)));
    const totalFeaturesCount = _.sum(rawJobs.map((job) => (job.featureCount ? job.featureCount.total : 0)));

    const totalScenariosExecuted = _.sum(rawJobs.map((job) => (job.scenarios ? job.scenarios.passed : 0)));
    const totalScenarios = _.sum(rawJobs.map((job) => (job.scenarios ? job.scenarios.total : 0)));

    const totalRunnersExecuted = _.sum(rawJobs.map((job) => (
        job.testrunnerReport ? (job.testrunnerReport.totalRunners - job.testrunnerReport.errors.length) : 0)));
    const totalRunners = _.sum(rawJobs.map((job) => (job.testrunnerReport ? job.testrunnerReport.totalRunners : 0)));

    const headlinePercentage = percentage(_.mean(
        rawJobs.map(calculateHeadlineJobSuccessRate)
    ));

    const reportUrl = rawJobs[0].reportingJobUrl;

    const jobTypeFilter = (jobNameFilter) => (job) => _.includes(job.jobName, jobNameFilter);

    const calculateTagLevelBreakdown = (jobs, jobNameFilter) => {
        // Features
        let primaryFeatures = 0;
        let primaryFeaturesFailed = 0;
        let primaryFeaturesPassed = 0;

        let secondaryFeatures = 0;
        let secondaryFeaturesFailed = 0;
        let secondaryFeaturesPassed = 0;

        let remainingFeatures = 0;
        let remainingFeaturesFailed = 0;
        let remainingFeaturesPassed = 0;

        // Scenarios
        let primaryScenarios = 0;
        let primaryScenariosFailed = 0;
        let primaryScenariosPassed = 0;

        let secondaryScenarios = 0;
        let secondaryScenariosFailed = 0;
        let secondaryScenariosPassed = 0;

        let remainingScenarios = 0;
        let remainingScenariosFailed = 0;
        let remainingScenariosPassed = 0;

        // Flags
        let primaryFlag = false;
        let secondaryFlag = false;
        let othersFlag = false;

        jobs.filter(jobTypeFilter(jobNameFilter)).forEach((job) => {
            if (job.features === undefined) {
                return;
            }
            job.features.forEach((feature) => {
                const featuretags = feature.tags.map((tag) => tag.name);
                if (featuretags.includes('@primary')) {
                    primaryFeatures++;
                    if (feature.isFailed) {
                        primaryFeaturesFailed++;
                    } else {
                        primaryFeaturesPassed++;
                    }
                } else if (featuretags.includes('@secondary')) {
                    secondaryFeatures++;
                    if (feature.isFailed) {
                        secondaryFeaturesFailed++;
                    } else {
                        secondaryFeaturesPassed++;
                    }
                } else {
                    remainingFeatures++;
                    if (feature.isFailed) {
                        remainingFeaturesFailed++;
                    } else {
                        remainingFeaturesPassed++;
                    }
                }
                feature.elements.forEach((scenario) => {
                    const scenarioTags = scenario.tags.map((tag) => tag.name);
                    if (scenarioTags.includes('@primary')) {
                        primaryFlag = true;
                        primaryScenarios++;
                        if (scenario.failed !== 0) {
                            primaryScenariosFailed++;
                        } else {
                            primaryScenariosPassed++;
                        }
                    } else if (scenarioTags.includes('@secondary')) {
                        secondaryFlag = true;
                        secondaryScenarios++;
                        if (scenario.failed !== 0) {
                            secondaryScenariosFailed++;
                        } else {
                            secondaryScenariosPassed++;
                        }
                    } else {
                        othersFlag = true;
                        remainingScenarios++;
                        if (scenario.failed !== 0) {
                            remainingScenariosFailed++;
                        } else {
                            remainingScenariosPassed++;
                        }
                    }
                });
            });
        });
        return {
            isPrimaryAvailable: primaryFlag,
            isSecondaryAvailable: secondaryFlag,
            isOthersAvailable: othersFlag,
            features: {
                primary: {
                    pass: percentage(primaryFeaturesPassed / primaryFeatures),
                    fail: percentage(primaryFeaturesFailed / primaryFeatures)
                },
                secondary: {
                    pass: percentage(secondaryFeaturesPassed / secondaryFeatures),
                    fail: percentage(secondaryFeaturesFailed / secondaryFeatures)
                },
                others: {
                    pass: percentage(remainingFeaturesPassed / remainingFeatures),
                    fail: percentage(remainingFeaturesFailed / remainingFeatures)
                }
            },
            scenarios: {
                primary: {
                    pass: percentage(primaryScenariosPassed / primaryScenarios),
                    fail: percentage(primaryScenariosFailed / primaryScenarios)
                },
                secondary: {
                    pass: percentage(secondaryScenariosPassed / secondaryScenarios),
                    fail: percentage(secondaryScenariosFailed / secondaryScenarios)
                },
                others: {
                    pass: percentage(remainingScenariosPassed / remainingScenarios),
                    fail: percentage(remainingScenariosFailed / remainingScenarios)
                }
            }
        };
    };

    const calculateFeatureSuccessRate = (jobs, jobNameFilter) => _.mean(
        jobs.filter(jobTypeFilter(jobNameFilter)).map((job) => (
            job.featureCount ? parseFloat(job.featureCount.passedPercentage) : 0))
    ).toFixed(2);

    const calculateScenarioSuccessRate = (jobs, jobNameFilter) => _.mean(
        jobs.filter(jobTypeFilter(jobNameFilter)).map((job) => (
            job.scenarios ? parseFloat(job.scenarios.passedPercentage) : 0))
    ).toFixed(2);

    const calculateFeatureFailedRate = (jobs, jobNameFilter) => _.mean(
        jobs.filter(jobTypeFilter(jobNameFilter)).map((job) => (
            job.featureCount ? parseFloat(job.featureCount.failedPercentage) : 0))
    ).toFixed(2);

    const calculateScenarioFailedRate = (jobs, jobNameFilter) => _.mean(
        jobs.filter(jobTypeFilter(jobNameFilter)).map((job) => (
            job.scenarios ? parseFloat(job.scenarios.failedPercentage) : 0))
    ).toFixed(2);

    const calculateTestrunnerSuccessRate = (jobs, jobNameFilter) => percentage(_.mean(
        jobs.filter(jobTypeFilter(jobNameFilter)).map((
            job
        ) => ((job.testrunnerReport.totalRunners - job.testrunnerReport.errors.length)
                / job.testrunnerReport.totalRunners))
    ));

    const mapJobsWithShortNames = (jobs, jobNameFilter) => {
        const job = jobs.filter(jobTypeFilter(jobNameFilter));
        if (job.length === 1) {
            return job[0];
        }
        return {};
    };

    const getTeams = (feature) => (feature.tags.map((t) => t.name)
        .filter((t) => t.startsWith('@team-'))
        .map((name) => name.replace('@team-', '')));

    const featureSuccessRate = {};
    const scenarioSuccessRate = {};
    const featureFailedRate = {};
    const scenarioFailedRate = {};
    const featureScenarioPerJob = {};
    const testrunnerSuccessRate = {};
    const tagsLevelData = {};
    const successRateFields = ['FunctionalStubs', 'StubsIos', 'StubsAndroid',
        'StubsIosLds', 'StubsIosHfx', 'StubsIosBos', 'StubsAndroidLds', 'StubsAndroidHfx',
        'StubsAndroidBos', 'FunctionalEnvironment', 'EnvironmentIos', 'EnvironmentAndroid',
        'EnvironmentIosLds', 'EnvironmentIosHfx', 'EnvironmentIosBos', 'EnvironmentAndroidLds',
        'EnvironmentAndroidHfx', 'EnvironmentAndroidBos', 'UATRegression', 'UATIos', 'UATAndroid',
        'UATIosLds', 'UATAndroidLds', 'Compatibility', 'CompatibilityIos',
        'CompatibilityAndroid'];

    successRateFields.forEach((field) => {
        featureSuccessRate[field] = calculateFeatureSuccessRate(rawJobs, field);
        scenarioSuccessRate[field] = calculateScenarioSuccessRate(rawJobs, field);
        featureFailedRate[field] = calculateFeatureFailedRate(rawJobs, field);
        scenarioFailedRate[field] = calculateScenarioFailedRate(rawJobs, field);
        featureScenarioPerJob[field] = mapJobsWithShortNames(rawJobs, field);
        testrunnerSuccessRate[field] = calculateTestrunnerSuccessRate(rawJobs, field);
        tagsLevelData[field] = calculateTagLevelBreakdown(rawJobs, field);
    });

    function millisecondsToHoursMinutes(d) {
        const buildTime = Number(d);
        const h = Math.floor(buildTime / 3600000);
        const m = Math.floor(buildTime % 3600000 / 60000);

        const hDisplay = h > 0 ? h + (h === 1 ? ' hour, ' : ' hours, ') : '';
        const mDisplay = m > 0 ? m + (m === 1 ? ' minute ' : ' minutes ') : '';

        return hDisplay + mDisplay;
    }

    const iOSBuildDuration = millisecondsToHoursMinutes(average(rawJobs.filter(jobTypeFilter('Ios')).map((job) => job.buildDuration)));

    const androidBuildDuration = millisecondsToHoursMinutes(average(rawJobs.filter(jobTypeFilter('Android')).map((job) => job.buildDuration)));

    /**
     * This function looks through each relevant job and for every single matching
     * feature it looks for the data in feature
     * run data available and collates the data for features passed/failed and total scenarios
     */

    const fetchFeatureFileTeamBreakdown = (jobType, featureData) => {
        const featureFile = _.cloneDeep(featureData);
        rawJobs.filter(jobTypeFilter(jobType)).forEach((job) => {
            if (job.features) {
                job.features.forEach((feature) => {
                    featureFile.teamBreakdown.forEach((row) => {
                        const teams = getTeams(feature).join(', ');
                        if (row.teams === teams) {
                            if (feature.name && (row.name.trim() === feature.name.trim())) {
                                if (!('data' in row)) {
                                    row.data = {passed: 0, failed: 0, total: 0};
                                }
                                row.data.passed += feature.scenarios.passed;
                                row.data.failed += feature.scenarios.failed;
                                row.data.total += feature.scenarios.total;
                            }
                        }
                    });
                    featureFile.teamBreakdown.forEach((row) => {
                        if (row.data && typeof row.data.passed !== 'undefined'
                            && typeof row.data.total !== 'undefined') {
                            row.data.pr = percentage(row.data.passed / row.data.total);
                        }
                    });
                });
            }
        });
        return featureFile;
    };

    const functionalStubTeamBreakdown = fetchFeatureFileTeamBreakdown('FunctionalStub', featureFileInfo);
    const functionalEnvironmentTeamBreakdown = fetchFeatureFileTeamBreakdown('FunctionalEnvironment', featureFileInfo);
    const uatRegressionTeamBreakdown = fetchFeatureFileTeamBreakdown('UATRegression', featureFileInfo);
    const compatibilityTeamBreakdown = fetchFeatureFileTeamBreakdown('Compatibility', featureFileInfo);

    const functionalStubTeamData = combineTeamData(functionalStubTeamBreakdown);
    const functionalEnvironmentTeamData = combineTeamData(functionalEnvironmentTeamBreakdown);
    const uatRegressionTeamData = combineTeamData(uatRegressionTeamBreakdown);
    const compatibilityTeamData = combineTeamData(compatibilityTeamBreakdown);

    function combineTeamData(teamData) {
        const teamGrouping = _.groupBy(teamData.teamBreakdown, (obj) => obj.teams);
        const groupTeamData = [];
        let data = {};

        for (const team in teamGrouping) {
            if (teamGrouping[team]) {
                data = {
                    totalScenarios: 0,
                    matchingScenarios: 0,
                    automatedNotPendingScenarios: 0,
                    pendingAutomatedScenarios: 0,
                    manualScenarios: 0,
                    passedScenarios: 0,
                    failedScenarios: 0,
                    totalRun: 0
                };

                for (const row in teamGrouping[team]) {
                    if (teamGrouping[team][row]) {
                        data.teams = teamGrouping[team][row].teams;
                        data.totalScenarios += teamGrouping[team][row].totalScenarios;
                        data.matchingScenarios += teamGrouping[team][row].matchingScenarios;
                        data.automatedNotPendingScenarios += teamGrouping[team][row]
                            .automatedNotPendingScenarios;
                        data.pendingAutomatedScenarios += teamGrouping[team][row]
                            .pendingAutomatedScenarios;
                        data.manualScenarios += teamGrouping[team][row].manualScenarios;
                        if (teamGrouping[team][row].data) {
                            data.passedScenarios += teamGrouping[team][row].data.passed;
                            data.failedScenarios += teamGrouping[team][row].data.failed;
                            data.totalRun += teamGrouping[team][row].data.total;
                        }
                    }
                }
                data.pr = data.passedScenarios ? percentage(data.passedScenarios / data.totalRun) : '0';
                groupTeamData.push(data);
            }
        }
        return groupTeamData;
    }

    const jobs = rawJobs.map((job) => {
        const features = (() => {
            if (job.featureCount) {
                return {
                    passedPercentage: job.featureCount.passedPercentage,
                    passed: job.featureCount.passed,
                    total: job.featureCount.total
                };
            }
            return {};
        })();
        const scenarios = (() => {
            if (job.scenarios) {
                return {
                    passedPercentage: job.scenarios.passedPercentage,
                    passed: job.scenarios.passed,
                    total: job.scenarios.total
                };
            }
            return {};
        })();
        const resultsAvailable = Object.keys(features).length !== 0
                && Object.keys(scenarios).length !== 0;

        const [testrunnerReport, testrunnerDetailedFailures] = (() => {
            if (job.testrunnerReport) {
                if (job.testrunnerReport.errors.length === 0) {
                    return [`All ${job.testrunnerReport.totalRunners} test runners launched successfully.`, []];
                }
                const uniqueErrors = {};
                job.testrunnerReport.errors.forEach((error) => {
                    if (uniqueErrors[error]) {
                        uniqueErrors[error] += 1;
                    } else {
                        uniqueErrors[error] = 1;
                    }
                });

                const detailedErrors = [];
                for (const uniqueError of Object.keys(uniqueErrors)) {
                    detailedErrors.push(`(x${uniqueErrors[uniqueError]}) ${uniqueError}`);
                }

                return [`Attempted to launch ${job.testrunnerReport.totalRunners} test runners.  `
                        + `Found ${job.testrunnerReport.errors.length} errors.`, detailedErrors];
            }
            return ['Test runner report unavailable (see Jenkins logs)', []];
        })();
        const testrunnerDetailedFailuresAvailable = testrunnerDetailedFailures.length !== 0;

        return {
            title: `${job.jobName} results:`,
            url: job.url,
            features,
            scenarios,
            resultsAvailable,
            aggregatedReportUrl: job.aggregatedReportUrl,
            testrunnerReport,
            testrunnerDetailedFailures,
            testrunnerDetailedFailuresAvailable
        };
    });

    return {
        totalFeaturesExecuted,
        totalFeaturesCount,
        totalScenariosExecuted,
        totalScenarios,
        totalRunnersExecuted,
        totalRunners,
        testrunnerSuccessRate,
        scenarioSuccessRate,
        featureSuccessRate,
        featureFailedRate,
        scenarioFailedRate,
        headlinePercentage,
        tagsLevelData,
        reportUrl,
        iOSBuildDuration,
        androidBuildDuration,
        jobs,
        featureScenarioPerJob,
        functionalStubTeamBreakdown,
        functionalEnvironmentTeamBreakdown,
        uatRegressionTeamBreakdown,
        compatibilityTeamBreakdown,
        functionalStubTeamData,
        functionalEnvironmentTeamData,
        uatRegressionTeamData,
        compatibilityTeamData
    };
};

const extractScenarioInfo = (config, groupedScenarios) => {
    const matchingFeatures = _.filter(groupedScenarios, (feature) => config.tagFilter(
        _.flatten(feature.map((scenario) => scenario.tags.map((tag) => tag.name)))
    )).length;

    let matchingScenarios = 0;

    // Automated.
    let automatedScenarios = 0;
    let matchingScenariosNotManual = 0;
    let matchingScenariosNotManualAndNotPending = 0;

    // Manual.
    let matchingScenariosManual = 0;
    let matchingScenariosManualAndPending = 0;

    // Platform
    let androidOnly = 0;
    let iosOnly = 0;

    _.flatten(groupedScenarios).forEach((scenario) => {
        const tags = scenario.tags.map((tag) => tag.name);
        if (!tags.includes('@manual') && !tags.includes('@pending')) {
            automatedScenarios++;
        }
        if (config.tagFilter(tags)) {
            matchingScenarios++;

            // Automated.
            if (!tags.includes('@manual')) {
                matchingScenariosNotManual++;
            }
            if (!tags.includes('@manual') && !tags.includes('@pending')) {
                matchingScenariosNotManualAndNotPending++;
            }

            // Manual.
            if (tags.includes('@manual')) {
                matchingScenariosManual++;
            }
            if (tags.includes('@manual') && tags.includes('@pending')) {
                matchingScenariosManualAndPending++;
            }

            // Platform
            if (tags.includes('@android')) {
                androidOnly++;
            }
            if (tags.includes('@ios')) {
                iosOnly++;
            }
        }
    });

    return {

        automatedScenarios,
        matchingScenarios,
        matchingFeatures,

        matchingFeaturesPercentage: percentage(matchingFeatures / config.totalFeatures),
        // Automated.
        matchingScenariosNotManual,
        matchingScenariosNotManualPercentage:
            percentage(matchingScenariosNotManual / matchingScenarios),
        matchingScenariosNotManualAndNotPending,
        matchingScenariosNotManualAndNotPendingPercentage:
            percentage(matchingScenariosNotManualAndNotPending / matchingScenariosNotManual),

        // Manual.
        matchingScenariosManual,
        matchingScenariosManualPercentage:
            percentage(matchingScenariosManual / matchingScenarios),
        matchingScenariosManualAndPending,
        matchingScenariosManualAndPendingPercentage:
            percentage(matchingScenariosManualAndPending / matchingScenariosManual),

        // Platform
        androidOnly,
        androidOnlyPercentage: percentage(androidOnly / matchingScenarios),
        iosOnly,
        iosOnlyPercentage: percentage(iosOnly / matchingScenarios)
    };
};

const extractTeamBreakdown = (config, features) => _(features).flatMap(({parsed, compiled}) => {
    const tagsPerScenario = compiled.map((scenario) => scenario.tags.map((t) => t.name));
    const tagsPerMatchingScenario = tagsPerScenario.filter((t) => config.tagFilter(t));

    if (tagsPerMatchingScenario.length === 0) {
        return [];
    }

    const teams = (parsed.feature.tags
        .map((t) => t.name)
        .filter((t) => t.startsWith('@team-'))
        .map((name) => name.replace('@team-', ''))
        .join(', '));
    const {name} = parsed.feature;
    const matchingScenarios = tagsPerMatchingScenario.length;

    const totalScenarios = tagsPerScenario.length;

    const automatedNotPendingScenarios = tagsPerMatchingScenario.filter((t) => !t.includes('@manual') && !t.includes('@pending')).length;
    const pendingAutomatedScenarios = tagsPerMatchingScenario.filter((t) => !t.includes('@manual') && t.includes('@pending')).length;
    const manualScenarios = tagsPerMatchingScenario.filter((t) => t.includes('@manual')).length;

    const automatedScenarios = tagsPerMatchingScenario.filter((t) => !t.includes('@manual')).length;
    const automationCoverage = percentage(automatedScenarios / matchingScenarios);

    return [{
        teams,
        name,
        matchingScenarios,
        totalScenarios,
        automatedScenarios,
        pendingAutomatedScenarios,
        manualScenarios,
        automatedNotPendingScenarios,
        automationCoverage
    }];
}).sortBy(['teams', 'name']).value();

const getFeatureFileInfo = (config) => getFeatureFiles().then(
    (filenames) => Promise.all(filenames.map((filename) => readFile(filename).then((raw) => {
        const parsed = gherkinParser.parse(raw.toString());
        return {
            parsed,
            compiled: new Gherkin.compile(parsed, '', IdGenerator.incrementing())
        };
    })))
)
    .then((featureFiles) => ({

        ...extractScenarioInfo(config, featureFiles.map((d) => d.compiled)),
        teamBreakdown: extractTeamBreakdown(config, featureFiles)
    }));

const template = fs.readFileSync('src/template/reportAggregatorTemplate.html', 'utf-8');

const render = (view) => Mustache.render(template, view);

// Produces something like '@team-core, @team-performance, @team-panm or @team-payments'
const buildTeamList = (teams) => [teams.slice(0, -1).join(', '), teams[teams.length - 1]].filter((x) => x !== '').join(', or ');

const configs = {
    weekend: {
        title: '{$brand}, Compatibility, Stubs, {$branch}',
        subtitle: 'All devices.  All tests tagged @nga3, @rnga, and not @appium',
        tagFilter: (tags) => tags.includes('@nga3')
            && tags.includes('@rnga')
            && !tags.includes('@appium')
            && !tags.includes('@environmentOnly'),
        isCompatibility: true,
        isWeekend: true,
        // 200 is an estimate from James Wardle of the number of NGA3 feature files which will
        // eventually be written.
        totalFeatures: 200
    },
    weeknightFunctionalStubs: {
        title: 'Functional, @primary, Stubs, {$branch}',
        subtitle: 'One known good device per brand.  All tests tagged @primary, @nga3, @rnga, and not (@appium, or @environmentOnly)',
        tagFilter: (tags) => tags.includes('@nga3')
            && tags.includes('@rnga')
            && tags.includes('@primary')
            && !tags.includes('@appium')
            && !tags.includes('@environmentOnly'),
        // We expect to run about 20% of all features and 40 = 200 / 5.
        isWeeknightFunctionalStubs: true,
        totalFeatures: 40
    },
    weeknightFunctionalEnvironment: {
        title: 'Functional, Env, {$branch}',
        subtitle: 'One known good Andriod and iOS device for Lloyds.  All tests tagged @nga3, @rnga, and not (@appium, or @stubOnly)',
        tagFilter: (tags) => tags.includes('@nga3')
            && tags.includes('@rnga')
            && tags.includes('@optFeature')
            && !tags.includes('@appium')
            && !tags.includes('@stubOnly'),
        // We expect to run about 20% of all features and 40 = 200 / 5.
        isWeeknightFunctionalEnvironment: true,
        totalFeatures: 40
    },
    weeknightUATRegression: {
        title: 'UAT Regression, @uat, Env, {$branch}',
        subtitle: 'One known good Andriod and iOS device for Lloyds.  All tests tagged @uat, @nga3, @rnga, and not (@appium)',
        tagFilter: (tags) => tags.includes('@nga3')
            && tags.includes('@rnga')
            && tags.includes('@team-uat')
            && tags.includes('@environmentOnly')
            && !tags.includes('@appium'),
        isWeeknightUATRegression: true,
        totalFeatures: 40
    },
    weeknightCompatibility: {
        title: 'Compatibility, @all, Stub, {$branch}',
        subtitle: `All tests tagged @nga3, @rnga, and ${buildTeamList(compatibilityTeams.WEEKNIGHT.slice(), '')}, and not (@appium, or @environmentOnly)`,
        tagFilter: (tags) => tags.includes('@nga3')
            && tags.includes('@rnga')
            && _.intersection(tags, compatibilityTeams.WEEKNIGHT).length > 0
            && !tags.includes('@appium')
            && !tags.includes('@environmentOnly'),
        isCompatibility: true,
        // We expect to run about 20% of all features and 40 = 200 / 5.
        totalFeatures: 40
    }
};

const config = configs[reportId];
if (config === undefined) {
    throw new Error(`Unknown report-aggregator config value: ${reportId}`);
}

function getDeviceCount(currentReportId) {
    const weekendDeviceCount = {};
    const weeknightCompatibilityDeviceCount = {};
    const weeknightFunctionalStubsDeviceCount = {};
    const weeknightFunctionalEnvironmentDeviceCount = {};
    const weeknightUATRegressionDeviceCount = {};

    ['iOS', 'Android'].forEach((platform) => {
        if (currentReportId === 'weekend') {
            weekendDeviceCount[platform] = devices.weekend[platform].length;
        } else if (currentReportId === 'weeknightCompatibility') {
            weeknightCompatibilityDeviceCount[platform] = devices.weeknightCompatibilityStubs[platform].length;
        } else if (currentReportId === 'weeknightFunctionalStubs') {
            const shortUrl = {};
            ['RNGA', 'BNGA'].forEach((type) => {
                ['LDS', 'BOS', 'HFX', 'MBNA'].forEach((brand) => {
                    if (devices.weeknightFunctionalStubs[type][platform][brand]) {
                        if (shortUrl[brand]) {
                            shortUrl[brand].push(devices.weeknightFunctionalStubs[type][platform][brand]);
                        } else {
                            shortUrl[brand] = devices.weeknightFunctionalStubs[type][platform][brand];
                        }
                    }
                });
            });
            weeknightFunctionalStubsDeviceCount[platform] = shortUrl.LDS.length
                + shortUrl.HFX.length + shortUrl.BOS.length;
        } else if (currentReportId === 'weeknightFunctionalEnvironment') {
            const shortUrl = devices.weeknightFunctionalEnvironment[platform];
            weeknightFunctionalEnvironmentDeviceCount[platform] = shortUrl.LDS.length
                + shortUrl.HFX.length + shortUrl.BOS.length;
        } else if (currentReportId === 'weeknightUATRegression') {
            const shortUrl = devices.weeknightUATRegression[platform];
            if (shortUrl) {
                weeknightUATRegressionDeviceCount[platform] = shortUrl.LDS.length;
            }
        }
    });
    return {
        weekendDeviceCount,
        weeknightCompatibilityDeviceCount,
        weeknightFunctionalStubsDeviceCount,
        weeknightFunctionalEnvironmentDeviceCount,
        weeknightUATRegressionDeviceCount
    };
}

const devicesCount = getDeviceCount(reportId);

function buildConfig(rawJobs) {
    const {brand} = rawJobs[0];
    const tagExpression = rawJobs[0].jenkinsTags;
    const codeBranch = rawJobs[0].branch;
    if (brand) {
        const brandNames = {
            LDS: 'Lloyds',
            HFX: 'Halifax',
            BOS: 'Bank of Scotland'
        };
        config.title = config.title.replace('{$brand}', brandNames[brand]);
    }
    if (tagExpression) {
        config.subtitle = `${config.subtitle} and ${tagExpression}`;
        const originalFiler = config.tagFilter;
        config.tagFilter = (tags) => {
            const parser = new TagExpressionParser();
            const expressionNode = parser.parse(tagExpression);
            return originalFiler(tags) && expressionNode.evaluate(tags);
        };
    }
    if (codeBranch) {
        config.title = config.title.replace('{$branch}', codeBranch);
    }
    return config;
}

readFile('reportPrimer.json')
    .then(JSON.parse)
    .then(fetchTestReportInfo)
    .then((rawJobs) => Promise.all([
        rawJobs,
        getFeatureFileInfo(buildConfig(rawJobs))
    ]))
    .then(([rawJobs, featureFileInfo]) => render({
        ...mungeTestReportInfo(rawJobs, featureFileInfo),
        ...featureFileInfo,
        ...config,
        devicesCount
    }))
    .then((report) => writeFile('report.html', report))
    .catch((err) => {
        logger.error('Unable to generate report', err);
        process.exit(1);
    });
