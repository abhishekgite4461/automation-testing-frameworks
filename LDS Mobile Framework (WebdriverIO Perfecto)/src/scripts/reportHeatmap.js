/**
 * Converts multiple enriched-output.json files, as found in
 * reports/cucumber/html/enriched-output.json to a heatmap showing the pass rate split by device,
 * team, tag category, and brand.
 */
const fs = require('fs');
const denodeify = require('denodeify');
const request = require('request-promise');
const Mustache = require('mustache');
const _ = require('lodash');

const logger = require('../utils/logger');
const {cartesianProduct} = require('../utils/utils');

const USAGE = 'Usage: npm run report-heatmap <input> [<input> ...]  '
    + 'where each <input> is a local enriched-output.json or remote HTTP or HTTPS URL';

const COLOUR_SKEW = 4;
const COLOUR_SATURATION = 80;
const COLOUR_LIGHTNESS = 50;

function retrieveInput(input) {
    if (input.startsWith('http')) {
        return request.get({uri: input});
    }
    return denodeify(fs.readFile)(input);
}

let suppressFurtherBrandDefaultLogs = false;
function warnAboutBrandDefaulting() {
    if (!suppressFurtherBrandDefaultLogs) {
        logger.warn('Brand data not available, defaulting to LDS!');
        suppressFurtherBrandDefaultLogs = true;
    }
}

/**
 * Extract info from enriched JSON report, return in the format
 * List<Pair<Category,Result>>
 * Where Category is something like {
 *    device: 'Google Pixel XL ..',
 *    team: 'core',
 *    brand: 'LDS',
 *    tagCategory: 'primary'
 * }
 * And Result is something like: {
 *   'passed': 2,
 *   'failed': 1,
 * }
 */
function extractRawFeatureData({tags, metadata, elements}) {
    const teams = tags.map((tag) => tag.name)
        .filter((name) => name.startsWith('@team-'))
        .map((name) => name.slice(6));

    const {device: deviceDetails, platform: {name: platform}} = metadata;

    const brand = (() => {
        if (_.has(metadata, 'brand')) {
            return metadata.brand;
        }
        warnAboutBrandDefaulting();
        return 'LDS';
    })();

    const [deviceId, deviceName] = deviceDetails.split('|').map((elem) => elem.trim());
    const device = serialize({platform, deviceName, deviceId});

    const primary = {
        passed: 0,
        failed: 0
    };
    const all = {
        passed: 0,
        failed: 0
    };

    elements.forEach((element) => {
        if (element.keyword !== 'Scenario') {
            throw new Error('Unknown element type, expected Scenario.');
        }

        // Any failed steps count as a failure
        const failed = _.sum([
            element.failed,
            element.notdefined,
            element.skipped,
            element.pending,
            element.ambiguous
        ]) > 0 ? 1 : 0;
        const passed = 1 - failed;

        if (element.tags.map((tag) => tag.name).includes('@primary')) {
            primary.passed += passed;
            primary.failed += failed;
        }
        all.passed += passed;
        all.failed += failed;
    });

    // This does cause double counting for tests which are owned by multiple teams, see comment
    // in html.
    return _.flatMap(teams, (team) => [
        [{device, team, brand, tagCategory: 'primary'}, _.clone(primary)],
        [{device, team, brand, tagCategory: 'all'}, _.clone(all)]
    ]);
}

function serialize(obj) {
    return JSON.stringify(obj, Object.keys(obj).sort());
}

function deserialize(obj) {
    return JSON.parse(obj);
}

/**
 * Convert List<Pair<Category,Result>> to Map<Category, Result> by amalgamating the results.
 */
function amalgateByCategory(rawFeatureData) {
    return _(rawFeatureData)
        .groupBy(([k]) => serialize(k))
        .mapValues((group) => _(group)
            .mapValues(([, v]) => v)
            .reduce((acc, x) => _.mapValues(acc, (v, k) => v + x[k]))
            .valueOf())
        .valueOf();
}

/**
 * Take Map<Category, Result>, where category is like {
 *    device: ..,
 *    team: ..,
 *    brand: ..,
 *    tagCategory: ..
 * }
 * and a return list of all devices, teams, brands, and tagCategories.
 */
function getCategories(amalgamatedByCategory) {
    // Categories match those returned on the LHS of extractRawFeatureData.
    const devices = new Set();
    const teams = new Set();
    const brands = new Set();
    const tagCategories = new Set();
    _.forOwn(amalgamatedByCategory, (v, k) => {
        const {device, team, brand, tagCategory} = deserialize(k);
        devices.add(device);
        teams.add(team);
        brands.add(brand);
        tagCategories.add(tagCategory);
    });
    return {
        devices: _.sortBy(Array.from(devices), (device) => {
            const unpickled = deserialize(device);
            return unpickled.platform + unpickled.deviceName;
        }),
        teams: Array.from(teams).sort(),
        brands: Array.from(brands).sort(),
        tagCategories: Array.from(tagCategories).sort()
    };
}

/**
 * Convert Map<Category, Result> to Map<Category, ProcessedResult> where Result is as
 * returned by extractRawFeatureData and ProcessedResult is something like {
 *   text: '50%',
 *   colour: 'hsl(120, 80%, 50%)'
 * }
 * Fills in results for any missing categories.
 *
 * Also creates row and column sums.
 */
function processData(amalgamatedByCategory, {devices, teams, brands, tagCategories}) {
    const noData = {
        text: 'No Data',
        colour: '#000000'
    };

    const createCellContent = (passed, total) => {
        if (total === 0) {
            return noData;
        }
        const passrate = passed / total;
        // Math.pow is restricted by airbnb rules in eslint, but ** is not available in node 6.
        // eslint-disable-next-line no-restricted-properties
        const hue = ((Math.pow(passrate, COLOUR_SKEW)) * 120).toFixed(0);
        return {
            text: `${(passrate * 100).toFixed(0)} % (${total})`,
            colour: `hsl(${hue}, ${COLOUR_SATURATION}%, ${COLOUR_LIGHTNESS}%)`
        };
    };

    const columnTotals = _.fromPairs(cartesianProduct(teams, brands).map(([team, brand]) => {
        const {passed, failed} = cartesianProduct(devices, tagCategories).map(([device, tagCategory]) => {
            const result = amalgamatedByCategory[serialize({device, team, brand, tagCategory})];
            if (result !== undefined) {
                return result;
            }
            return {passed: 0, failed: 0};
        }).reduce((acc, x) => {
            acc.passed += x.passed;
            acc.failed += x.failed;
            return acc;
        }, {passed: 0, failed: 0});

        return [serialize({team, brand}), createCellContent(passed, (passed + failed))];
    }));

    const rowTotals = _.fromPairs(cartesianProduct(devices, tagCategories).map(([device, tagCategory]) => {
        const {passed, failed} = cartesianProduct(teams, brands).map(([team, brand]) => {
            const result = amalgamatedByCategory[serialize({device, team, brand, tagCategory})];
            if (result !== undefined) {
                return result;
            }
            return {passed: 0, failed: 0};
        }).reduce((acc, x) => {
            acc.passed += x.passed;
            acc.failed += x.failed;
            return acc;
        }, {passed: 0, failed: 0});

        return [serialize({device, tagCategory}), createCellContent(passed, (passed + failed))];
    }));

    const processed = {};
    cartesianProduct(devices, teams, brands, tagCategories).forEach(([device, team, brand, tagCategory]) => {
        const pickled = serialize({device, team, brand, tagCategory});
        const result = amalgamatedByCategory[pickled];
        processed[pickled] = (() => {
            if (result !== undefined) {
                const {passed, failed} = result;
                const total = passed + failed;
                return createCellContent(passed, total);
            }
            return noData;
        })();
    });
    return {processed, rowTotals, columnTotals};
}

/**
 * Convert Map<Category, ProcessedResult> to rows, columns, etc, where row and column headings are
 * based on the category.
 */
function tabularise({teams, devices, brands, tagCategories}, {processed, rowTotals, columnTotals}) {
    const headingsLevel1 = teams.map((team) => ({
        title: team,
        columnspan: brands.length
    }));
    const headingsLevel2 = _.flatMap(teams, (team) => brands.map((brand) => _.merge({
        title: brand
    }, columnTotals[serialize({team, brand})])));

    const rows = _.flatMap(devices, (device) => {
        const {platform, deviceName, deviceId} = deserialize(device);
        let headingLevel1 = {
            title: platform,
            titleStrong: deviceName,
            subtitle: deviceId,
            rowspan: tagCategories.length
        };

        return tagCategories.map((tagCategory) => {
            const headingLevel2 = _.merge(rowTotals[serialize({device, tagCategory})], {
                title: tagCategory
            });

            const cells = _.flatMap(teams, (team) => brands.map(
                (brand) => processed[serialize({team, device, brand, tagCategory})]
            ));
            const row = {
                headingLevel1,
                headingLevel2,
                cells
            };
            headingLevel1 = null;
            return row;
        });
    });

    const cellWidth = `${(100 / (headingsLevel2.length + 2)).toFixed(1)}%`;
    return {
        headingsLevel1,
        headingsLevel2,
        rows,
        cellWidth
    };
}

const template = `
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        table {
            width: 100%;
        }
        .standard-cell {
            width: {{cellWidth}};
        }
    </style>
</head>
<body>
<div class="container-fluid">
    {{#table}}
    <table class="table">
        <thead class="thead">
            <tr class="tr">
                <td rowspan="2"/>
                <td rowspan="2"/>
                {{#headingsLevel1}}
                <td colspan={{columnspan}} class="td">{{title}}</td>
                {{/headingsLevel1}}
            </tr>
            <tr class="tr">
                {{#headingsLevel2}}
                <td class="td" style="background-color: {{colour}}">
                    <strong>{{title}}</strong>
                    {{text}}
                </td>
                {{/headingsLevel2}}
            </tr>
        </thead>

        <tbody class="tbody">
            {{#rows}}
            <tr class="tr">
                {{#headingLevel1}}
                <td class="td standard-cell" rowspan={{rowspan}}>
                    {{title}} | <strong>{{titleStrong}}</strong>
                    <div>{{subtitle}}</div>
                </td>
                {{/headingLevel1}}

                {{#headingLevel2}}
                 <td class="td standard-cell" style="background-color: {{colour}}">
                    <strong>{{title}}</strong>
                    {{text}}
                </td>
                {{/headingLevel2}}

                {{#cells}}
                <td class="td standard-cell" style="background-color: {{colour}}">{{text}}</td>
                {{/cells}}
            </tr>
            {{/rows}}
        </tbody>
    </table>
    {{/table}}
</div>
<div class="container">
    <div class="info-warning">
        <strong>Inputs:</strong>
        {{#inputs}}
        <li class="list-group-item">{{.}}</li>
        {{/inputs}}
    </div>
    <div class="alert-warning">
        <ul class="list-group">
            <strong>Caveats:</strong>
            <li class="list-group-item">
                <strong>Report only reflects tests which have been run.</strong>
                For example, if the report contains only @primary tests, then the all row is meaningless!
            </li>
            <li class="list-group-item">
                Any tests which are owned by multiple teams will be appear in multiple columns.
            </li>
            <li class="list-group-item">
                Any devices which fail to spwan any test runners will not appear at all.
            </li>
        </ul>
    </div>
</div>
</body>
</html>
`;

const calculateInput = (primer) => Object.keys(primer.jobs).map((jobName) => primer.jobs[jobName].enrichedJsonUrl);

let inputs = process.argv.slice(2);

if (inputs.length === 0) {
    logger.error(USAGE);
    process.exit(1);
} else if (_.includes(inputs, 'reportPrimer.json')) {
    inputs = calculateInput(JSON.parse(fs.readFileSync('reportPrimer.json')));
}
logger.info(`Running with inputs: ${inputs.join(', ')}`);

Promise.all(inputs.map((input) => retrieveInput(input)))
    .then((jsons) => {
        const features = _.flatMap(jsons, ((json) => JSON.parse(json).features));
        const rawFeatureData = _.flatMap(features, (feature) => extractRawFeatureData(feature));
        const amalgamatedByCategory = amalgateByCategory(rawFeatureData);
        const categories = getCategories(amalgamatedByCategory);
        const processedData = processData(amalgamatedByCategory, categories);
        const table = tabularise(categories, processedData);
        return Mustache.render(template, {table, inputs});
    })
    .then((report) => denodeify(fs.writeFile)('heatMap.html', report))
    .catch((err) => {
        logger.error('Heatmap report generation failed', err);
        logger.error(USAGE);
        process.exit(1);
    });
