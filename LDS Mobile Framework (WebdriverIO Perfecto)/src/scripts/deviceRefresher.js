const {PerfectoRestClient} = require('../utils/perfectoRestClient');
const deviceList = require('../wdio_config/perfectoDeviceList');
const utils = require('../utils/utils');
const logger = require('../utils/logger');

utils.envVariableExists('PERFECTO_USER');
utils.envVariableExists('PERFECTO_PWD');

const perfectoRestClient = new PerfectoRestClient();
const deviceListForReboot = deviceList.ALL.iOS.concat(deviceList.ALL.Android);

perfectoRestClient.rebootAllDevices(deviceListForReboot)
    .then((result) => result.forEach((deviceResult) => {
        if (!deviceResult.isSuccess) {
            logger.info(`Failed to reboot ${deviceResult.deviceId}: ${deviceResult.error}`);
        }
    }))
    .catch((err) => {
        logger.error(err);
        process.exit(1);
    });
