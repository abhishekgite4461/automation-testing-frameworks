/**
 * This script will be used to fetch the features/scenarios data from the QA code base.
 * This script is developer for QA Managers so to extract data from the Codebase.
 */
const fs = require('fs');
const denodeify = require('denodeify');
const process = require('process');
const Gherkin = require('@cucumber/gherkin');
const {IdGenerator} = require('@cucumber/messages');
const Mustache = require('mustache');
const _ = require('lodash');

const reportId = require('yargs').demandCommand(1).argv._[2];
const logger = require('../utils/logger');
const {getFeatureFiles} = require('../runner/runner');

const readFile = denodeify(fs.readFile);
const writeFile = denodeify(fs.writeFile);

const gherkinParser = new Gherkin.Parser(new Gherkin.AstBuilder(IdGenerator.incrementing()));

const percentage = (x) => (x * 100).toFixed(2);

const mungeTestReportInfo = (featureFileInfo) => {
    const teamData = combineTeamData(featureFileInfo);

    function combineTeamData(featureFiles) {
        const teamGrouping = _.groupBy(featureFiles.teamBreakdown, (obj) => obj.teams);
        const groupTeamData = [];
        let data = {};
        let row = {};

        const total = {
            totalScenarios: 0,
            matchingScenarios: 0,
            automatedNotPendingScenarios: 0,
            pendingAutomatedScenarios: 0,
            manualScenarios: 0,
            primaryScenarios: 0,
            primaryandManualScenarios: 0,
            primaryandPendingScenarios: 0,
            primaryandStubScenarios: 0,
            primaryandIosScenarios: 0,
            primaryandAndroidScenarios: 0,
            primaryScenariosAutomated: 0,
            passedScenarios: 0,
            failedScenarios: 0,
            totalRun: 0
        };

        for (const team in teamGrouping) {
            if (teamGrouping[team]) {
                data = {
                    totalScenarios: 0,
                    matchingScenarios: 0,
                    automatedNotPendingScenarios: 0,
                    pendingAutomatedScenarios: 0,
                    manualScenarios: 0,
                    passedScenarios: 0,
                    primaryScenarios: 0,
                    primaryandManualScenarios: 0,
                    primaryandPendingScenarios: 0,
                    primaryandStubScenarios: 0,
                    primaryandIosScenarios: 0,
                    primaryandAndroidScenarios: 0,
                    primaryScenariosAutomated: 0,
                    failedScenarios: 0,
                    totalRun: 0
                };

                for (row in teamGrouping[team]) {
                    if (teamGrouping[team][row]) {
                        data.teams = teamGrouping[team][row].teams;
                        data.totalScenarios += teamGrouping[team][row].totalScenarios;
                        data.matchingScenarios += teamGrouping[team][row].matchingScenarios;
                        data.automatedNotPendingScenarios += teamGrouping[team][row]
                            .automatedNotPendingScenarios;
                        data.pendingAutomatedScenarios += teamGrouping[team][row]
                            .pendingAutomatedScenarios;
                        data.manualScenarios += teamGrouping[team][row].manualScenarios;
                        data.primaryScenarios += teamGrouping[team][row].primaryScenarios;

                        data.primaryandManualScenarios += teamGrouping[team][row].primaryandManualScenarios;
                        data.primaryandPendingScenarios += teamGrouping[team][row].primaryandPendingScenarios;
                        data.primaryandStubScenarios += teamGrouping[team][row].primaryandStubScenarios;

                        data.primaryandIosScenarios += teamGrouping[team][row].primaryandIosScenarios;
                        data.primaryandAndroidScenarios += teamGrouping[team][row].primaryandAndroidScenarios;

                        data.primaryScenariosAutomated += teamGrouping[team][row].primaryScenariosAutomated;
                        if (teamGrouping[team][row].data) {
                            data.passedScenarios += teamGrouping[team][row].data.passed;
                            data.failedScenarios += teamGrouping[team][row].data.failed;
                            data.totalRun += teamGrouping[team][row].data.total;
                        }
                    }
                }
                data.pr = data.passedScenarios ? percentage(data.passedScenarios / data.totalRun) : 'N/A';
                data.automatedNotPendingScenariosPercentage = data.automatedNotPendingScenarios ? percentage(data.automatedNotPendingScenarios / data.totalScenarios) : '0';
                data.primaryScenariosAutomatedPercentage = data.primaryScenariosAutomated ? percentage(data.primaryScenariosAutomated / data.primaryScenarios) : '0';
                groupTeamData.push(data);

                total.teams = 'Total';
                total.totalScenarios += data.totalScenarios;
                total.matchingScenarios += data.matchingScenarios;
                total.automatedNotPendingScenarios += data.automatedNotPendingScenarios;
                total.pendingAutomatedScenarios += data.pendingAutomatedScenarios;
                total.manualScenarios += data.manualScenarios;
                total.primaryScenarios += data.primaryScenarios;

                total.primaryandManualScenarios += data.primaryandManualScenarios;
                total.primaryandPendingScenarios += data.primaryandPendingScenarios;
                total.primaryandStubScenarios += data.primaryandStubScenarios;

                total.primaryandIosScenarios += data.primaryandIosScenarios;
                total.primaryandAndroidScenarios += data.primaryandAndroidScenarios;

                total.primaryScenariosAutomated += data.primaryScenariosAutomated;

                total.passedScenarios += data.passedScenarios;
                total.failedScenarios += data.failedScenarios;
                total.totalRun += data.totalRun;

                total.pr = total.passedScenarios ? percentage(total.passedScenarios / total.totalRun) : 'N/A';
                total.automatedNotPendingScenariosPercentage = total.automatedNotPendingScenarios ? percentage(total.automatedNotPendingScenarios / total.totalScenarios) : '0';
                total.primaryScenariosAutomatedPercentage = total.primaryScenariosAutomated ? percentage(total.primaryScenariosAutomated / total.primaryScenarios) : '0';
            }
        }
        groupTeamData.push(total);
        return groupTeamData;
    }

    return {
        teamData
    };
};

const extractScenarioInfo = (config, groupedScenarios) => {
    const matchingFeatures = _.filter(groupedScenarios, (feature) => config.tagFilter(
        _.flatten(feature.map((scenario) => scenario.tags.map((tag) => tag.name)))
    )).length;

    let matchingScenarios = 0;

    // Automated.
    let automatedScenarios = 0;
    let matchingScenariosNotManual = 0;
    let matchingScenariosNotManualAndNotPending = 0;

    // Manual.
    let matchingScenariosManual = 0;
    let matchingScenariosManualAndPending = 0;

    // Platform
    let androidOnly = 0;
    let iosOnly = 0;

    _.flatten(groupedScenarios).forEach((scenario) => {
        const tags = scenario.tags.map((tag) => tag.name);
        if (!tags.includes('@manual') && !tags.includes('@pending')) {
            automatedScenarios++;
        }
        if (config.tagFilter(tags)) {
            matchingScenarios++;

            // Automated.
            if (!tags.includes('@manual')) {
                matchingScenariosNotManual++;
            }
            if (!tags.includes('@manual') && !tags.includes('@pending')) {
                matchingScenariosNotManualAndNotPending++;
            }

            // Manual.
            if (tags.includes('@manual')) {
                matchingScenariosManual++;
            }
            if (tags.includes('@manual') && tags.includes('@pending')) {
                matchingScenariosManualAndPending++;
            }

            // Platform
            if (tags.includes('@android')) {
                androidOnly++;
            }
            if (tags.includes('@ios')) {
                iosOnly++;
            }
        }
    });

    return {

        automatedScenarios,
        matchingScenarios,
        matchingFeatures,

        // Automated.
        matchingScenariosNotManual,
        matchingScenariosNotManualPercentage:
            percentage(matchingScenariosNotManual / matchingScenarios),
        matchingScenariosNotManualAndNotPending,
        matchingScenariosNotManualAndNotPendingPercentage:
            percentage(matchingScenariosNotManualAndNotPending / matchingScenariosNotManual),

        // Manual.
        matchingScenariosManual,
        matchingScenariosManualPercentage:
            percentage(matchingScenariosManual / matchingScenarios),
        matchingScenariosManualAndPending,
        matchingScenariosManualAndPendingPercentage:
            percentage(matchingScenariosManualAndPending / matchingScenariosManual),

        // Platform
        androidOnly,
        androidOnlyPercentage: percentage(androidOnly / matchingScenarios),
        iosOnly,
        iosOnlyPercentage: percentage(iosOnly / matchingScenarios)
    };
};

const extractTeamBreakdown = (config, features) => _(features).flatMap(({parsed, compiled}) => {
    const tagsPerScenario = compiled.map((scenario) => scenario.tags.map((t) => t.name));
    const tagsPerMatchingScenario = tagsPerScenario.filter((t) => config.tagFilter(t));

    if (tagsPerMatchingScenario.length === 0) {
        return [];
    }

    const teams = (parsed.feature.tags
        .map((t) => t.name)
        .filter((t) => t.startsWith('@team-'))
        .map((name) => name.replace('@team-', ''))
        .join(', '));
    const {name} = parsed.feature;
    const matchingScenarios = tagsPerMatchingScenario.length;

    const totalScenarios = tagsPerScenario.length;

    const automatedNotPendingScenarios = tagsPerMatchingScenario.filter((t) => !t.includes('@manual') && !t.includes('@pending')).length;
    const pendingAutomatedScenarios = tagsPerMatchingScenario.filter((t) => !t.includes('@manual') && t.includes('@pending')).length;
    const manualScenarios = tagsPerMatchingScenario.filter((t) => t.includes('@manual')).length;

    const primaryScenarios = tagsPerMatchingScenario.filter((t) => t.includes('@primary')).length;

    const primaryandManualScenarios = tagsPerMatchingScenario.filter((t) => t.includes('@primary') && t.includes('@manual')).length;
    const primaryandPendingScenarios = tagsPerMatchingScenario.filter((t) => t.includes('@primary') && t.includes('@pending')).length;
    const primaryandStubScenarios = tagsPerMatchingScenario.filter((t) => t.includes('@primary') && t.includes('@stubOnly')).length;

    const primaryandIosScenarios = tagsPerMatchingScenario.filter((t) => t.includes('@primary') && t.includes('@ios')).length;
    const primaryandAndroidScenarios = tagsPerMatchingScenario.filter((t) => t.includes('@primary') && t.includes('@android')).length;

    const primaryScenariosAutomated = tagsPerMatchingScenario.filter((t) => t.includes('@primary') && !t.includes('@manual')).length;

    const automatedScenarios = tagsPerMatchingScenario.filter((t) => !t.includes('@manual')).length;
    const automationCoverage = percentage(automatedScenarios / matchingScenarios);

    return [{
        teams,
        name,
        matchingScenarios,
        totalScenarios,
        automatedScenarios,
        pendingAutomatedScenarios,
        manualScenarios,
        primaryScenarios,
        primaryandManualScenarios,
        primaryandPendingScenarios,
        primaryandStubScenarios,
        primaryandIosScenarios,
        primaryandAndroidScenarios,
        primaryScenariosAutomated,
        automatedNotPendingScenarios,
        automationCoverage
    }];
}).sortBy(['teams', 'name']).value();

const getFeatureFileInfo = (config) => getFeatureFiles().then((filenames) => Promise.all(
    filenames.map((filename) => readFile(filename).then((raw) => {
        const parsed = gherkinParser.parse(raw.toString());
        return {
            parsed,
            compiled: new Gherkin.compile(parsed, '', IdGenerator.incrementing())
        };
    }))
))
    .then((featureFiles) => ({

        ...extractScenarioInfo(config, featureFiles.map((d) => d.compiled)),
        teamBreakdown: extractTeamBreakdown(config, featureFiles)
    }));

const template = `<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style media="screen" type="text/css">
        .jumbotron {
            text-align: center;
        }
    </style>
</head>
<body>

<div class="container">

    <table class="table table-bordered table-striped">
        <thead class="thead-inverse">
        <tr style='background-color: #D3D3D3'>
            <th>Feature Team Names</th>
            <th>Scenarios</th>
            <th>Actually Running</th>
            <th>Pending Automation</th>
            <th>Manual</th>
            <th>Automation Coverage (Running)</th>
            <th>Primary</th>
            <th>Primary & Manual</th>
            <th>Primary & Pending</th>
            <th>Primary & Stub</th>
            <th>Primary & iOS</th>
            <th>Primary & Android</th>
            <th>Primary Automation Percentage</th>
        </tr>
        </thead>
        <tbody>

        {{#teamData}}
        <tr>
            <td>{{teams}}</td>
            <td>{{totalScenarios}}</td>
            <td>{{automatedNotPendingScenarios}}</td>
            <td>{{pendingAutomatedScenarios}}</td>
            <td>{{manualScenarios}}</td>
            <td>{{automatedNotPendingScenariosPercentage}}%</td>
            <td>{{primaryScenarios}}</td>
            <td>{{primaryandManualScenarios}}</td>
            <td>{{primaryandPendingScenarios}}</td>
            <td>{{primaryandStubScenarios}}</td>
            <td>{{primaryandIosScenarios}}</td>
            <td>{{primaryandAndroidScenarios}}</td>
            <td>{{primaryScenariosAutomatedPercentage}}%</td>
        </tr>
        {{/teamData}}

        </tbody>
    </table>

</div>`;

const render = (view) => Mustache.render(template, view);

const configs = {
    RNGA: {
        tagFilter: (tags) => tags.includes('@nga3') && tags.includes('@rnga') && !tags.includes('@appium')
    },
    BNGA: {
        tagFilter: (tags) => tags.includes('@nga3') && tags.includes('@bnga') && !tags.includes('@appium')
    }
};

const config = configs[reportId];
if (config === undefined) {
    throw new Error(`Unknown config value: ${reportId}`);
}

Promise.all([getFeatureFileInfo(config)])
    .then(([featureFileInfo]) => render({
        ...mungeTestReportInfo(featureFileInfo)
    }))
    .then((report) => writeFile('featureDataReport.html', report))
    .catch((err) => {
        logger.error('Unable to generate report', err);
        process.exit(1);
    });
