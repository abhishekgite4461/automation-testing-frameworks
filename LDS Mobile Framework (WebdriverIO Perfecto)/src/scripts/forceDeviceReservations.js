const moment = require('moment');

const DeviceReservation = require('./createDeviceReservations');
const devices = require('../wdio_config/perfectoDeviceList');

// If its a Friday today, reserve the devices until next Monday
const addDays = ((moment()
    .day() !== 5) ? 1 : 3);

// calculate the unix time for the given time hour taking additional days to be added, hours and minutes parameters
const getTimeMillis = (days = 0, hrs = 0, min = 0) => moment()
    .add(days, 'days')
    .hours(hrs)
    .minutes(min)
    .seconds(0)
    .milliseconds(0)
    .unix() * 1000;

const deviceIds = devices.ALL.Android.concat(devices.ALL.iOS);

const devicesForFunctionalEnv = devices.asDeviceList(devices.weeknightFunctionalEnvironment);

const devicesForUAT = devices.asDeviceList(devices.weeknightUATRegression);

const devicesForPlatinumEnv = devices.asDeviceList(devices.weeknightFunctionalEnvironmentPlatinumMaster);

const allDevicesExceptFunctionalEnvUAT = deviceIds.filter((element) => !(devicesForFunctionalEnv.includes(element)
    || devicesForUAT.includes(element)));

DeviceReservation.forceDeviceReservation(allDevicesExceptFunctionalEnvUAT,
    getTimeMillis(0, 17, 50), getTimeMillis(addDays, 6, 0));

DeviceReservation.forceDeviceReservation(devicesForFunctionalEnv,
    getTimeMillis(0, 17, 20), getTimeMillis(addDays, 10, 0));

DeviceReservation.forceDeviceReservation(devicesForPlatinumEnv,
    getTimeMillis(addDays, 15, 0), getTimeMillis(addDays, 16, 0));

// Reserve only on wednesdays for exhaustive jobs
if (moment()
    .day() === 3) {
    DeviceReservation.forceDeviceReservation(devicesForFunctionalEnv, getTimeMillis(0, 11, 0), getTimeMillis(0, 12, 0));
}

// Disabling UAT device reservation until enough data and builds to test
// forceDeviceReservation(devicesForUAT, startTimeMillis, uatEndTimeMillis);
