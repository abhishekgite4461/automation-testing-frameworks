const util = require('util');

const logger = require('../utils/logger');
const {PerfectoRestClient} = require('../utils/perfectoRestClient');
const devices = require('../wdio_config/perfectoDeviceList');

const perfectoClient = new PerfectoRestClient();

const deviceIds = devices.ALL.Android.concat(devices.ALL.iOS);

perfectoClient.uninstallAllApplications(deviceIds)
    .then((responses) => {
        logger.info(`Status of uninstall of all applications on devices:\n\t${responses.map((response) => util.inspect(response)).join('\n\t')}`);
    }).catch((err) => {
        logger.error('uninstallAllApplications failed', err);
        process.exit(1);
    });
