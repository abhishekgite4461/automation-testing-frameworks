const EnvironmentApiClient = require('../utils/environmentApiClient');
const logger = require('../utils/logger');

const envDetail = {
    SIT02: {
        server: 'SIT02-W',
        boxGrpNo: '916'
    },
    SIT10: {
        server: 'SIT10-W',
        boxGrpNo: '947'
    }
};

Promise.resolve(EnvironmentApiClient.setFuse(envDetail.SIT10.server, envDetail.SIT10.boxGrpNo)).then(() => {
    EnvironmentApiClient.setFuse(
        envDetail.SIT02.server, envDetail.SIT02.boxGrpNo
    );
}).catch((err) => {
    logger.error(err);
});
