/* eslint import/no-dynamic-require: "off" */
const fs = require('fs');
const path = require('path');

class ScreenFactory {
    static getNativeScreen(name) {
        const appVersion = browser.capabilities.appVersion.toLowerCase();
        const platform = browser.capabilities.platformName.toLowerCase();
        const appType = browser.capabilities.appType.toLowerCase();
        const brand = browser.capabilities.brand.toLowerCase();

        return ScreenFactory.createScreen([
            `./native/${appVersion}/${platform}/${appType}/${brand}/${name}.js`,
            `./native/${appVersion}/${platform}/${appType}/${name}.js`,
            `./native/${appVersion}/${platform}/${brand}/${name}.js`,
            `./native/${appVersion}/${platform}/${name}.js`
        ]);
    }

    static getWebScreen(name) {
        const brand = browser.capabilities.brand.toLowerCase();

        return ScreenFactory.createScreen([
            `./web/${brand}/${name}.js`,
            `./web/${name}.js`
        ]);
    }

    static createScreen(trialPaths) {
        for (const trialPath of trialPaths) {
            if (fs.existsSync(path.join(__dirname, trialPath))) {
                const Screen = require(trialPath);
                return new Screen();
            }
        }
        throw new Error(`Screen ${trialPaths.join(' or ')} not found!`);
    }
}

module.exports = ScreenFactory;
