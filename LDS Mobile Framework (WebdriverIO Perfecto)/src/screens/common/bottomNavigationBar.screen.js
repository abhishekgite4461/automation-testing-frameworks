const Screen = require('../screen');

class BaseBottomNavigationBar extends Screen {
    constructor() {
        super();
        this.homeTab = undefined;
        this.applyTab = undefined;
        this.payAndTransferTab = undefined;
        this.supportTab = undefined;
        this.moreTab = undefined;
    }
}

module.exports = BaseBottomNavigationBar;
