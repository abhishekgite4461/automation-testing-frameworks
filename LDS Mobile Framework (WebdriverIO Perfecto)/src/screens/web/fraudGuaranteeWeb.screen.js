const Screen = require('../screen');
const helper = require('./web.helper.js');
const WebElement = require('../../enums/webElement.enum');

class fraudGuaranteeWeb extends Screen {
    constructor() {
        super();
        this.fraudGuaranteeTitle = helper.byText(WebElement.SPAN, 'Protecting yourself from fraud');
    }
}

module.exports = fraudGuaranteeWeb;
