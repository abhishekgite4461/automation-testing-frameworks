const BaseForgottenLoginDetailsScreen = require('../preforgottenLoginDetails.screen');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');

class ForgottenLoginDetailsScreen extends BaseForgottenLoginDetailsScreen {
    constructor() {
        super();
        this.title = helper.byText(WebElement.H2, 'Make sure you\'ve got');
        this.closeButton = helper.byText(WebElement.BUTTON, 'Close');
    }
}

module.exports = ForgottenLoginDetailsScreen;
