const BaseDmcDetailsScreen = require('../dmcDetails.screen.js');
const helper = require('../web.helper.js');
const WebElement = require('../../../enums/webElement.enum.js');

class DmcDetailsScreen extends BaseDmcDetailsScreen {
    constructor() {
        super();
        this.title = helper.byText(WebElement.STRONG, 'Your account details');
        this.webCloseButton = helper.byText(WebElement.BUTTON, 'Close');
    }
}

module.exports = DmcDetailsScreen;
