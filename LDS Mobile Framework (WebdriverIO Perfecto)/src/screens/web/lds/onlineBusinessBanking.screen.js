const BaseOnlineBusinessBankingScreen = require('../onlineBusinessBanking.screen');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');

class OnlineBusinessBanking extends BaseOnlineBusinessBankingScreen {
    constructor() {
        super();
        this.title = `//*[@class="active"]${helper.byText(WebElement.SPAN, 'Banking online')}`;
    }
}
module.exports = OnlineBusinessBanking;
