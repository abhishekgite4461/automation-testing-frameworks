const BaseProductHubOverdraftsApplyDropDownWebScreen = require('../../productHub/productHubLoanApply.screen');

class ProductHubOverdraftsApplyDropDownWebScreen extends BaseProductHubOverdraftsApplyDropDownWebScreen {
    constructor() {
        super();
        this.retiredLabel = 'Retired';
        this.selectEmploymentOption = '(//select[@id="frmApplyForOverdraft:slctEmploymentStatus1"])[1]';
    }
}

module.exports = ProductHubOverdraftsApplyDropDownWebScreen;
