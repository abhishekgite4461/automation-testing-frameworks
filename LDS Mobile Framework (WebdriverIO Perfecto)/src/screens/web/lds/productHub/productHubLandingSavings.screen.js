const BaseProductHubLandingSavingsScreen = require('../../productHub/productHubLandingSavings.screen');
const helper = require('../../web.helper');
const WebElement = require('../../../../enums/webElement.enum');
const DeviceHelper = require('../../../../utils/device.helper');

class ProductHubLandingSavingsScreen extends BaseProductHubLandingSavingsScreen {
    constructor() {
        super();
        if (DeviceHelper.isUAT()) {
            this.compareSavings = helper.byText(WebElement.H1, 'Savings and investments');
        } else {
            this.compareSavings = helper.byText(WebElement.H1, 'Compare savings accounts - open an account today');
        }
        if (DeviceHelper.isAndroid()) {
            this.savingsAccountName = helper.byText(WebElement.SPAN, 'Easy Saver');
            this.select = '//*[preceding-sibling::h3/span[text()="Easy Saver"]][1]//a';
            this.OthersavingsAccountName = helper.byText(WebElement.SPAN, 'Help to Buy: ISA');
            this.Otherselect = '//*[preceding-sibling::h3/span[text()="Help to Buy: ISA"]][1]//a';
        } else {
            this.savingsAccountName = helper.byText(WebElement.SPAN, 'Cash ISA Saver');
            this.select = '//*[preceding-sibling::h3/span[text()="Cash ISA Saver"]][1]//a';
            this.OthersavingsAccountName = helper.byText(WebElement.SPAN, 'Easy Saver');
            this.Otherselect = '//*[preceding-sibling::h3/span[text()="Easy Saver"]][1]//a';
        }
    }
}

module.exports = ProductHubLandingSavingsScreen;
