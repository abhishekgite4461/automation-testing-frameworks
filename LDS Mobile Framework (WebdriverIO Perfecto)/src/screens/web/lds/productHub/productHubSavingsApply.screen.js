const BaseProductHubSavingsApplyScreen = require('../../productHub/productHubSavingsApply.screen');
const helper = require('../../web.helper');
const WebElement = require('../../../../enums/webElement.enum');
const DeviceHelper = require('../../../../utils/device.helper');

class ProductHubSavingsApplyScreen extends BaseProductHubSavingsApplyScreen {
    constructor() {
        super();
        this.applynow = '//div[2]/div[1]/button[1]/span[1]';
        this.applynowISA = '//Input[@value="Apply now"]';
        this.acceptTermsandConditions = helper.byPartialText(WebElement.LABEL, 'conditions');
        this.openAccount = '//*[@title = "Open account"]';
        if (DeviceHelper.isAndroid()) {
            this.accountOpeningSuccessMessage = '//h1[text()="Your Easy Saver account is now open" or '
                + 'text()="You\'ve opened a Easy Saver" or text()="Congratulations, you have just opened a Easy Saver"]';
        } else {
            this.accountOpeningSuccessMessage = '//h1[text()="Your Cash ISA Saver account is now open" or '
                + 'text()="You\'ve opened a Cash ISA Saver" or text()="Congratulations, you have just opened '
                + 'a Cash ISA Saver"]';
        }
        this.acctSortCodeandNumber = helper.byStartText(WebElement.P, 'Sort code');
    }
}

module.exports = ProductHubSavingsApplyScreen;
