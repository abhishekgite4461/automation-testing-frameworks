const BaseDataConsentScreen = require('../dataConsent.screen');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');

class DataConsentScreen extends BaseDataConsentScreen {
    constructor() {
        super();
        this.cookiesPolicyPagetitle = helper.byText(WebElement.H1, 'Cookies policy');
    }
}

module.exports = DataConsentScreen;
