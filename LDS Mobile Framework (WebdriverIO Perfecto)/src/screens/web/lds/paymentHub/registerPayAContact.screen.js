const BaseRegisterPayAContactScreen = require('../../paymentHub/registerPayAContact.screen');
const helper = require('../../web.helper');
const WebElement = require('../../../../enums/webElement.enum');

class RegisterPayAContactScreen extends BaseRegisterPayAContactScreen {
    constructor() {
        super();
        this.p2pTitle = helper.byText(WebElement.H1, 'Register for Pay a Contact');
        this.registerButton = helper.byId(WebElement.INPUT,
            'frmIntrdocution:btnP2PRegisterIntro');
        this.continueButton = helper.byId(WebElement.INPUT,
            'frmViewAccountOverview:btnP2PChooseProxy');
        this.agreeCheckBox = helper.byId(WebElement.INPUT,
            'frmSelectAccount:acceptTsAndCs');
        this.continuetoP2PButton = helper.byId(WebElement.INPUT,
            'frmSelectAccount:btnP2PChooseAccount');
        this.continueToP2PAuthentication = helper.byId(WebElement.A,
            'frmViewAccountOverview:lkAccountOverviewBottom');
        this.P2PsuccessMessage = helper.byText(WebElement.H1, 'Registration successful');
        this.lnkP2PSetting = helper.byId(WebElement.A,
            'frmViewAccountOverview:lnkP2PSettings');
        this.lkDeRegisterFromP2P = helper.byId(WebElement.A,
            'frmViewAccountOverview:lkDeRegisterFromP2P_1');
        this.authPassword = helper.byId(WebElement.INPUT,
            'frmRegistrationFailure:txtAuthPwd');
        this.continueToDereg = helper.byId(WebElement.INPUT,
            'frmRegistrationFailure:btnBackToAccountOverView');
        this.deregSuceesMsg = helper.byPartialText(WebElement.H1, 'Pay a Contact mobile number de-registration successful');
    }
}

module.exports = RegisterPayAContactScreen;
