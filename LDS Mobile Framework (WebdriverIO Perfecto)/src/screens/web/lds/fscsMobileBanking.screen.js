const BaseFscsMobileBankingScreen = require('../fscsMobileBanking.screen');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');

class FscsMobileBankingScreen extends BaseFscsMobileBankingScreen {
    constructor() {
        super();
        this.title = helper.byText(WebElement.H1, 'Compensation Scheme');
    }
}

module.exports = FscsMobileBankingScreen;
