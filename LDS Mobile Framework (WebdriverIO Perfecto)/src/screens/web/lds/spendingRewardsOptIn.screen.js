const Screen = require('../../screen');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');

class SpendingRewardsOptInScreen extends Screen {
    constructor() {
        super();
        this.title = helper.byText(WebElement.H1, 'Everyday Offers');
        this.expiredOffers = helper.byText(WebElement.A, 'Expired offers');
        this.settings = helper.byText(WebElement.A, 'Settings');
        this.findOutMore = helper.byText(WebElement.A, 'Find out more');
        this.newTab = '//li[@class="current"]/span[contains(text(),"New")]';
        this.activeOffers = helper.byPartialText(WebElement.A, 'Active');
        this.activeTab = '//li[@class="current"]/span[contains(text(),"Active")]';
        this.selectEverydayOffersLink = helper.byText(WebElement.H1, 'Everyday Offers');
        this.expiredOffersPageTitle = helper.byText(WebElement.H2, 'Expired offers');
        this.settingPageTitle = helper.byText(WebElement.H2, 'Change your Everyday Offers settings');
        this.findOutMorePageTitle = helper.byText(WebElement.H1, 'Find out more about Everyday Offers');
    }
}

module.exports = SpendingRewardsOptInScreen;
