const Screen = require('../screen');

class ViewSpendingInsightsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.debitCard = undefined;
        this.debitCardInsights = undefined;
    }
}

module.exports = ViewSpendingInsightsScreen;
