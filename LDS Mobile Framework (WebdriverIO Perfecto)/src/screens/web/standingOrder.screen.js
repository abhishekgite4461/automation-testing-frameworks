const Screen = require('../screen');
const helper = require('./web.helper');
const WebElement = require('../../enums/webElement.enum');

class StandingOrderScreen extends Screen {
    constructor() {
        super();
        this.standingOrderTab = helper.byText(WebElement.SPAN, 'Standing orders');
        this.directDebitTab = '//A[text()="Direct Debits"  or text()="Direct debits"]';
        this.setupStandingOrderButton = helper.byText(WebElement.A, 'Set up a standing order');
        this.amendStandingOrderButton = (Ref) => `//P[preceding-sibling::DL//*[text()='${Ref}'][1]]/a[text()='Amend']`;
        this.deleteStandingOrderButton = (Ref) => `//P[preceding-sibling::DL//*[text()='${Ref}'][1]]/a[text()='Delete']`;
        this.soDeleteMessageTitle = helper.byStartText(WebElement.P, 'Deleted standing order to');
    }
}

module.exports = StandingOrderScreen;
