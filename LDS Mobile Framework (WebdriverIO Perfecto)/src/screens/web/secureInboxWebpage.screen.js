const Screen = require('../screen');

class SecureInboxWebpageScreen extends Screen {
    constructor() {
        super();
        this.inboxMessage = '//*[@id="MsgCorsLnk_1"]';
        this.archiveButton = '(//a[@id="archiveMessageMobile"])[1]';
        this.undoArchiveButton = '(//a[@id="lnkUndoArch"])[1]';
        this.verifyUndoArchiveMessage = '/html/body/div[2]/text()';
        this.pdfMessage = '(//a[@id="downloadReadMessage"])[1]';
        // this.verifyMessage = DeviceHelper.getAppName();
    }
}

module.exports = SecureInboxWebpageScreen;
