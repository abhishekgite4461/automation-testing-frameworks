const Screen = require('../screen');
const helper = require('./web.helper');
const WebElement = require('../../enums/webElement.enum');

class InternationalPaymentScreen extends Screen {
    constructor() {
        super();
        this.title = helper.byPartialText(WebElement.H2, 'International payment');
        this.recipients = helper.byText(WebElement.H2, 'International payments recipients');
        this.makePayment = '//p[preceding-sibling::h4[text()="Digital"]]/a[contains(@id,"lnkMakePayment")]';
        this.makeIPPaymentTitle = helper.byText(WebElement.H1, 'Make an international payment');
        this.currency = '//label[contains(text(), "sterling equivalent") or contains(text(), "Sterling equivalent"]';
        this.currencyToPayIn = helper.byPartialText(WebElement.LABEL, 'terling equivalent');
        this.ipAmount = helper.byId(WebElement.INPUT, 'frm1:paymentAmount');
        this.ipContinue = helper.byId(WebElement.INPUT, 'frm1:Btncontinue');
        this.confirmIPPayment = helper.byText(WebElement.H1, 'Confirm international payment details');
        this.ipTermsAndConditions = helper.byId(WebElement.INPUT, 'frmForm11:bchkbxAgreeTerms');
        this.ipPassword = helper.byId(WebElement.INPUT, 'frmForm11:txtAuthPwd');
        this.ipConfirm = helper.byId(WebElement.INPUT, 'frmForm11:btnContinue');
        this.ipPaymentConfirmation = helper.byPartialText(WebElement.H1, 'International payment confirmed');
        this.ippaymentreferenceno = helper.byPartialText(WebElement.LABEL, 'FP');
        this.txtBicSwift = helper.byId(WebElement.INPUT, 'quickRecipientAdd:txtBicSwift');
        this.contuneButton = helper.byId(WebElement.INPUT, 'quickRecipientAdd:Btncontinue');
        this.recipientBankAddr = helper.byId(WebElement.INPUT, 'recipientDetails:txtBankAddress1');
        this.IBAN = helper.byId(WebElement.INPUT, 'recipientDetails:txtIban1');
        this.recipientName = helper.byId(WebElement.INPUT, 'recipientDetails:txtRecipientName');
        this.recipientAddr = helper.byId(WebElement.INPUT, 'recipientDetails:txtAddress1');
        this.continueRecipientDetailsButton = helper.byId(WebElement.INPUT, 'recipientDetails:Btncontinue');
        this.confirmButton = helper.byId(WebElement.INPUT, 'myForm:Btncontinue');
        this.radioButton = (numberType) => `//LABEL[contains(text(),"${numberType}")]//preceding::input[1]`;
        this.successPage = helper.byPartialText(WebElement.H1, 'International recipient successfully created');
    }
}

module.exports = InternationalPaymentScreen;
