const Screen = require('../screen');

class ChangeAccountTypeScreen extends Screen {
    constructor() {
        super();
        this.changeAccountPagetitle = undefined;
        this.changeThisAccountButton = undefined;
        this.updateYoueEmailAddressPagetitle = undefined;
        this.correctEmailButton = undefined;
        this.continueButton = undefined;
        this.selectAccountYouWantPagetitle = undefined;
        this.findOutMoreButton = undefined;
        this.accountYouWantPage = undefined;
        this.yourAccountOfferPage = undefined;
        this.completeApplicationTickBox = undefined;
        this.completeApplicationButton = undefined;
        this.openAccountSuccessMessage = undefined;
        this.beforeYouStartPage = undefined;
        this.beforeYouContinuePageTitle = undefined;
        this.ageYes = undefined;
        this.residentYes = undefined;
        this.beforeYouContinueButton = undefined;
        this.YourNeedsTitle = undefined;
        this.FamilyTravelInsuranceHeader = undefined;
        this.haveSpouseYes = undefined;
        this.haveChildNo = undefined;
        this.similarTravelInsNo = undefined;
        this.familyTravelAbrodYes = undefined;
        this.wantTranvelInsYes = undefined;
        this.yourNeedsContinue = undefined;
        this.vehicleBreakdownHeader = undefined;
        this.similarBreakCoverNo = undefined;
        this.travelVehicleYes = undefined;
        this.responsibleYes = undefined;
        this.breakdownCoverYes = undefined;
        this.mobilePhoneHeader = undefined;
        this.alreadyHaveMobileInsNo = undefined;
        this.ownMobilePhoneYes = undefined;
        this.payExecessPhoneYes = undefined;
        this.wantMobileInsYes = undefined;
        this.homeEmergencyHeader = undefined;
        this.similarCoverNo = undefined;
        this.payEmergencyRepairYes = undefined;
        this.wantHomeEmerCoverYes = undefined;
        this.yourNeedsSummaryHeader = undefined;
        this.yourEligibilityTitle = undefined;
        this.yourEligibilityContinue = undefined;
        this.yourEligibilityDay = undefined;
        this.yourEligibilityMonth = undefined;
        this.yourEligibilityYear = undefined;
        this.yourEligibilityResidentYes = undefined;
        this.medicalConditionButtonSelfNo = undefined;
        this.medicalConditionButtonPartnerNo = undefined;
        this.selectVehicleBreakdownFinalCheckNo = undefined;
        this.mobileInsuranceFinalCheckButton = undefined;
        this.homeEmergencyFinalCheckButton = undefined;
        this.yourEligibilitySummaryHeader = undefined;
        this.summaryPageTitle = undefined;
        this.summaryPageContinueButton = undefined;
        this.AddOfferButton = undefined;
    }
}

module.exports = ChangeAccountTypeScreen;
