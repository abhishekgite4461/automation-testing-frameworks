const Screen = require('../screen');

class PreForgottenLoginDetailsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.closeButton = undefined;
        this.pageTitle = undefined;
    }
}
module.exports = PreForgottenLoginDetailsScreen;
