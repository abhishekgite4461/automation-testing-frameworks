const Screen = require('../screen');
const helper = require('./web.helper');
const WebElement = require('../../enums/webElement.enum');

class StandingOrderAmendScreen extends Screen {
    constructor() {
        super();
        this.soReference = helper.byId(WebElement.INPUT, 'frmAmendStandingOrder:strReferenceValue');
        this.soAmount = helper.byId(WebElement.INPUT, 'frmAmendStandingOrder:crRegularAmount');
        this.soAmendButton = helper.byId(WebElement.INPUT, 'frmAmendStandingOrder:lnkAmendSO');
        this.soPassword = helper.byId(WebElement.INPUT, 'frmConfirmAmendSO:password');
        this.soConfirmAmendButton = helper.byId(WebElement.INPUT, 'frmConfirmAmendSO:lnkAmendConfirmSO');
        this.soChangeSucessTitle = helper.byText(WebElement.H1, 'Standing order changed');
        this.soChangeSucessMessage = helper.byText(WebElement.P, 'Your standing order has been successfully changed.');
        this.soViewStandingOrdersButton = helper.byText(WebElement.A, 'View standing orders');
    }
}

module.exports = StandingOrderAmendScreen;
