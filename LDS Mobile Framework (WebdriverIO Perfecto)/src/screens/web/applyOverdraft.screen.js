const Screen = require('../screen');
const helper = require('./web.helper.js');
const WebElement = require('../../enums/webElement.enum');

class ApplyOverdraftScreen extends Screen {
    constructor() {
        super();
        this.selectAccount = helper.byDataSelector(WebElement.BUTTON, 'select-button-0');
        this.infoModalTitle = helper.byDataSelector(WebElement.H4, 'label-overdraft-modal');
        this.infoModalContinueButton = helper.byDataSelector(WebElement.BUTTON, 'overdraft-modal-continue-btn');
        this.letmetypeLink = helper.byDataSelector(WebElement.A, 'dismiss-slider');
        this.overdraftInputBox = helper.byDataSelector(WebElement.INPUT, 'overdraft-slider-input-amount-input');
        this.feeCalculator = helper.byDataSelector(WebElement.H4, 'understand-od-title');
        this.overdraftContinueButton = helper.byDataSelector(WebElement.BUTTON, 'continue-button');
        this.overdraftRemoveButton = helper.byDataSelector(WebElement.BUTTON, 'continue-button');

        // IandE screen
        this.ineScreenHeader = helper.byText(WebElement.H1, 'About you');
        this.somethingAboutToChangeNo = helper.byDataSelector(WebElement.BUTTON, 'verify-email-no');
        this.employmentStatus = helper.byDataSelector(WebElement.SELECT, 'employment-dropdown');
        this.employmentStatusFullTime = helper.byText(WebElement.OPTION, 'Employed Full Time');
        this.income = helper.byDataSelector(WebElement.INPUT, 'income-input-input');
        this.mortagage = helper.byDataSelector(WebElement.INPUT, 'rent-input-input');
        this.maintenance = helper.byDataSelector(WebElement.INPUT, 'other-input-input');
        this.ineContinueButton = helper.byDataSelector(WebElement.BUTTON, 'continue-button');

        // Email screen
        this.emailScreenHeader = helper.byText(WebElement.INPUT, 'other-input-input');
        this.userEmail = helper.byDataSelector(WebElement.P, 'user-email');
        this.emailCorrectYes = helper.byDataSelector(WebElement.BUTTON, 'verify-email-yes');
        this.emailCorrectNo = helper.byDataSelector(WebElement.BUTTON, 'verify-email-no');
        this.emailScreenContinueButton = helper.byDataSelector(WebElement.BUTTON, 'continue-button');

        // PCCI screen
        this.pcciScreenHeader = helper.byText(WebElement.H1, 'Pre-contract credit information for your arranged overdraft');
        this.pcciAgreementCheckbox = helper.byDataSelector(WebElement.SPAN, 'agreement-check-checkbox');
        this.pcciContinueButton = helper.byDataSelector(WebElement.BUTTON, 'continue-button');

        // Success Page
        this.successScreenHeader = helper.byDataSelector(WebElement.H1, 'success-header');
        this.overdraftValue = helper.byDataSelector(WebElement.H2, 'overdraft-value');
        this.surveyButton = helper.byDataSelector(WebElement.BUTTON, 'survey-btn');
        this.emailSentText = helper.byDataSelector(WebElement.HEADER, 'success-message-title');
        this.aovButton = helper.byDataSelector(WebElement.A, 'back-to-aov-link');
    }
}

module.exports = ApplyOverdraftScreen;
