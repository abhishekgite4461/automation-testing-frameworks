const Screen = require('../../screen');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');

class LoanCalculatorScreen extends Screen {
    constructor() {
        super();
        this.title = helper.byClass(WebElement.H1, 'welcome-header tl-lvl2');
        this.homeImprovementOption = helper.byClass(WebElement.DIV, 'purpose-container box-home');
        this.continue = helper.byDataSelector(WebElement.BUTTON, 'loan-purpose-continue');
        this.loanCalculatorText = helper.byDataSelector(WebElement.H1, 'loan-calculator-text');
        this.amountField = helper.byId(WebElement.INPUT, 'likely-amount-input-input');
        this.termField = helper.byId(WebElement.INPUT, 'likely-term-input-input');
        this.calculateButton = helper.byDataSelector(WebElement.BUTTON, 'calc-form-recalculate');
        this.continueApplicationButton = helper.byDataSelector(WebElement.BUTTON, 'apply-button-enabled');
        this.negativeButton = helper.byDataSelector(WebElement.BUTTON, 'toggle-button-no');
        this.unlikelyContinueButton = helper.byDataSelector(WebElement.SPAN, 'unlikely-continue-button');
        this.applyContinueButton = helper.byDataSelector(WebElement.BUTTON, 'content-applyButton');
        this.employmentStatus = helper.byDataSelector(WebElement.SELECT, 'input-employment-status-select');
        this.employedPartTimeState = helper.byValue(WebElement.OPTION, 'Employed part-time');
        this.salaryField = helper.byId(WebElement.INPUT, 'monthlySalaryAfterTax-input');
        this.spendingExpensesAccordian = helper.byDataSelector(WebElement.A, 'accordion-spending-expenses');
        this.shareOfMortagageAmountField = helper.byId(WebElement.INPUT, 'monthlyShareOfMortgage-input');
        this.dependentField = helper.byId(WebElement.INPUT, 'noOfDepend-input');
        this.regularMonthlyExpenseNoButton = helper.byDataSelector(WebElement.BUTTON, 'otherRegularMonthlyExpenses-no');
        this.regularMonthlyIncomeNoButton = helper.byDataSelector(WebElement.BUTTON, 'otherRegularMonthlyIncome-no');
        this.contactDetailsAccordian = helper.byDataSelector(WebElement.DIV, 'accordion-head-Your-contact details');
        this.agreeEmailCheckBox = helper.byValue(WebElement.INPUT, 'agree');
        this.applyLoanPageHeader = helper.byDataSelector(WebElement.H1, 'page-header');
    }
}

module.exports = LoanCalculatorScreen;
