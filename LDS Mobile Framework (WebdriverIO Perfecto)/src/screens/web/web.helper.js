class WebHelper {
    static byText(webElement, text) {
        return `//${webElement.element}[text()="${text}"]`;
    }

    static byPartialText(webElement, text) {
        return `//${webElement.element}[contains(text(),"${text}")]`;
    }

    static byStartText(webElement, text) {
        return `//${webElement.element}[starts-with(text(),"${text}")]`;
    }

    static byId(webElement, id) {
        return `//${webElement.element}[@id="${id}"]`;
    }

    static byClass(webElement, className) {
        return `//${webElement.element}[@class="${className}"]`;
    }

    static byType(webElement, type) {
        return `//${webElement.element}[@type="${type}"]`;
    }

    static byDataSelector(webElement, dataSelector) {
        return `//${webElement.element}[@data-selector="${dataSelector}"]`;
    }

    static byValue(webElement, text) {
        return `//${webElement.element}[@value="${text}"]`;
    }
}

module.exports = WebHelper;
