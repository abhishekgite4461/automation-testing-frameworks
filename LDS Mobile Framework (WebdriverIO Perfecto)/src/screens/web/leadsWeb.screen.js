const Screen = require('../screen');
const helper = require('./web.helper');
const WebElement = require('../../enums/webElement.enum');

class LeadsWebScreen extends Screen {
    constructor() {
        super();
        this.bigPromptMarketingPreferenceHubLink = '//*[@id="marketingPreferencesPage"]';
        this.endOfThePage = '//*[@id="LastParagraph"]';
        this.securitySettingsLead = helper.byPartialText(WebElement.A, 'nga:security-settings');
        this.spendingRewardsRegistrationLead = helper.byPartialText(WebElement.A, 'nga:spending-rewards-registration');
    }
}

module.exports = LeadsWebScreen;
