const Screen = require('../screen');

class PdfStatementCreditCard extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.viewPDFButton = undefined;
    }
}

module.exports = PdfStatementCreditCard;
