const Screen = require('../screen');
const helper = require('./web.helper.js');
const WebElement = require('../../enums/webElement.enum');

class DataPrivacyWebScreen extends Screen {
    constructor() {
        super();
        this.title = helper.byPartialText(WebElement.H1, 'Privacy');
    }
}

module.exports = DataPrivacyWebScreen;
