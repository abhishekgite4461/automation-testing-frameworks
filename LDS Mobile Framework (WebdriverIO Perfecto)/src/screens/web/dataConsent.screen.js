const Screen = require('../screen');

class DataConsentWebScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
    }
}

module.exports = DataConsentWebScreen;
