const Screen = require('../screen');

class SpendingRewardsOptInScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.expiredOffers = undefined;
        this.settings = undefined;
        this.findOutMore = undefined;
        this.newTab = undefined;
        this.activeOffers = undefined;
        this.activeTab = undefined;
        this.selectEverydayOffersLink = undefined;
        this.expiredOffersPageTitle = undefined;
        this.settingPageTitle = undefined;
        this.findOutMorePageTitle = undefined;
    }
}

module.exports = SpendingRewardsOptInScreen;
