const Screen = require('../screen');
const helper = require('./web.helper');
const WebElement = require('../../enums/webElement.enum');

class CMASurveyScreen extends Screen {
    constructor() {
        super();
        this.cmaSurveyResult = helper.byText(WebElement.H2, 'Independent service quality survey results');
    }
}

module.exports = CMASurveyScreen;
