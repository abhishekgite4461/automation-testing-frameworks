const BaseProductHubLoanApplyDropDownWebScreen = require('../../productHub/productHubLoanApply.screen');

class ProductHubLoanApplyDropDownWebScreen extends BaseProductHubLoanApplyDropDownWebScreen {
    constructor() {
        super();
        this.selectLoanPurposeOption = 'Boat Purchase';
        this.selectLoanPurposeOptionLabel = '(//select[@id="frmLoanCalculatorElig:slctLoanPurpose"])[1]';
        this.selectEmploymentOption = 'Retired';
        this.selectEmploymentOptionLabel = '(//select[@id="frmTermsCondition:slctEmploymentStatus"])[1]';
    }
}

module.exports = ProductHubLoanApplyDropDownWebScreen;
