const BaseProductHubSavingsApplyScreen = require('../../productHub/productHubSavingsApply.screen');
const helper = require('../../web.helper');
const WebElement = require('../../../../enums/webElement.enum');
const DeviceHelper = require('../../../../utils/device.helper');

class ProductHubSavingsApplyScreen extends BaseProductHubSavingsApplyScreen {
    constructor() {
        super();
        this.applynow = '//div[2]/div[1]/button[1]/span[1]';
        this.applynowISA = '//Input[@value="Apply now"]';
        this.acceptTermsandConditions = helper.byPartialText(WebElement.LABEL, 'conditions');
        this.openAccount = '//*[@title = "Apply now"]';
        this.acctSortCodeandNumber = helper.byStartText(WebElement.P, 'Sort code');
        if (DeviceHelper.isAndroid()) {
            this.accountOpeningSuccessMessage = '//h1[text()="Your Everyday Saver account is now open" or '
                + 'text()= "You\'ve opened a Everyday Saver"]';
        } else {
            this.accountOpeningSuccessMessage = '//h1[text()="Your ISA Saver Variable account is now open" or '
                    + 'text()= "You\'ve opened a ISA Saver Variable"]';
        }
    }
}

module.exports = ProductHubSavingsApplyScreen;
