const BaseProductHubChangeAccountTypeScreen = require('../../productHub/productHubChangeAccountType.screen');
const WebElement = require('../../../../enums/webElement.enum');
const helper = require('../../web.helper');

class ProductHubChangeAccountTypeScreen extends BaseProductHubChangeAccountTypeScreen {
    constructor() {
        super();
        this.changeAccountTypeTitle = helper.byText(WebElement.H1, 'YOUR OPTIONS');
    }
}

module.exports = ProductHubChangeAccountTypeScreen;
