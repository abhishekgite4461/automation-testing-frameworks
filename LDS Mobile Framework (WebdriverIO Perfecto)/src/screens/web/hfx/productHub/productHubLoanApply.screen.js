const BaseProductHubLoanApplyScreen = require('../../productHub/productHubLoanApply.screen');
const helper = require('../../web.helper');
const WebElement = require('../../../../enums/webElement.enum');

class ProductHubLoanApplyScreen extends BaseProductHubLoanApplyScreen {
    constructor() {
        super();
        this.helloTitle = helper.byClass(WebElement.SPAN, 'content-value');
        this.continueLoanButton = helper.byClass(WebElement.BUTTON, 'btn btn-primary btn-chevron continue');
        this.loanTitle = helper.byName(WebElement.SPAN, 'content-unlikelyHeader');
        this.selectLoanPurposeDropDown = (LoanPurpose) => `//option[contains(.,'${LoanPurpose}')]`;
        this.selectLoanPurposeOption = helper.byClass(WebElement.DIV, 'purpose-container box-home');
        this.payBackLoantoghgleButton = helper.byClass(WebElement.BUTTON, 'tab-btn btn extraMargino');
        this.continueApplicationButton = helper.byClass(WebElement.BUTTON, 'btn btn-primary btn-chevron apply-button');
        this.yourApplicationTitle = helper.byText(WebElement.H1, 'Your loan application');
        this.selectEmploymentStatus = helper.byText(WebElement.OPTION, 'Not working (Independent means)');
        this.closestMatchOption = helper.byName(WebElement.A, 'accordion-spending-expenses');
        this.monthlyIncomeField = helper.byId(WebElement.INPUT, 'monthlyShareOfMortgage-input');
        this.dependentsField = helper.byId(WebElement.INPUT, 'noOfDepend-input');
        this.regularMonthlyIncomeField = helper.byName(WebElement.BUTTON, 'otherRegularMonthlyIncome-no');
        this.childCareCostsField = helper.byName(WebElement.BUTTON, 'otherRegularMonthlyExpenses-no');
        this.selectFutureChange = helper.byClass(WebElement.SPAN, 'tick-btn-description');
        this.contactDetailsClosestMatchOption = helper.byName(WebElement.A, 'accordion-contact-details');
        this.confirmEmailAddressCheckbox = helper.byClass(WebElement.DIV, 'check-box-component');
        this.acknowledgeCheckbox = helper.byName(WebElement.LABEL, 'i-and-e-agree-label');
        this.submitloanButton = helper.byName(WebElement.BUTTON, 'i-and-e-form-submit');
        this.loanDeclinePageTitle = helper.byPartialText(WebElement.H1, 'Your ');
        this.applyLoanPage = helper.byName(WebElement.SPAN, 'content-unlikelyHeader');
        this.applyLoanContinueLoanButton = helper.byClass(WebElement.BUTTON, 'btn btn-primary continue pull-right ');
        this.verifyPurposeLoanPage = helper.byName(WebElement.SPAN, 'content-loan-purpose');
        this.purposeLoanContinueButton = helper.byClass(WebElement.BUTTON, 'btn btn-primary continue pull-right ');
        this.loanAmountField = helper.byId(WebElement.INPUT, 'likely-amount-input-input');
        this.termField = helper.byId(WebElement.INPUT, 'likely-term-input-input');
        this.loanCalculatorPageTitle = helper.byName(WebElement.SPAN, 'content-headingLabel');
        this.loanCalculateButton = helper.byClass(WebElement.BUTTON, 'button btn btn-default btn-primary recalculate');
        this.futureChangeOptionNoButton = helper.byName(WebElement.BUTTON, 'toggle-button-no');
        this.yourApplicationTitlePage = helper.byName(WebElement.H1, 'page-header');
    }
}

module.exports = ProductHubLoanApplyScreen;
