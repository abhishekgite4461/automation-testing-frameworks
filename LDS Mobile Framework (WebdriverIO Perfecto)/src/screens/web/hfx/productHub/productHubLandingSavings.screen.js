const BaseProductHubLandingSavingsScreen = require('../../productHub/productHubLandingSavings.screen');
const helper = require('../../web.helper');
const WebElement = require('../../../../enums/webElement.enum');
const DeviceHelper = require('../../../../utils/device.helper');

class ProductHubLandingSavingsScreen extends BaseProductHubLandingSavingsScreen {
    constructor() {
        super();
        if (DeviceHelper.isUAT()) {
            this.compareSavings = helper.byText(WebElement.H1, 'Savings and investments');
        } else {
            this.compareSavings = helper.byText(WebElement.H1, 'Compare savings accounts - open an account today');
        }
        if (DeviceHelper.isAndroid()) {
            this.savingsAccountName = helper.byText(WebElement.SPAN, 'Everyday Saver');
            this.select = '//*[preceding-sibling::h3/span[text()="Everyday Saver"]][1]//a';
            this.OthersavingsAccountName = helper.byText(WebElement.SPAN, 'ISA Saver Fixed 5 Year');
            this.Otherselect = '//*[preceding-sibling::h3/span[text()="ISA Saver Fixed 5 Year"]][1]//a';
        } else {
            this.savingsAccountName = helper.byText(WebElement.SPAN, 'ISA Saver Variable');
            this.select = '//*[preceding-sibling::h3/span[text()="ISA Saver Variable"]][1]//a';
            this.OthersavingsAccountName = helper.byText(WebElement.SPAN, 'Regular Saver');
            this.Otherselect = '//*[preceding-sibling::h3/span[text()="Regular Saver"]][1]//a';
        }
    }
}

module.exports = ProductHubLandingSavingsScreen;
