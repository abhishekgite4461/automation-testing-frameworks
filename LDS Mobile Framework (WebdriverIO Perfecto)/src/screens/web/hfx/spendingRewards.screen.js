const BaseSpendingRewardsScreen = require('../spendingRewards.screen');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');

class SpendingRewardsScreen extends BaseSpendingRewardsScreen {
    constructor() {
        super();
        this.title = helper.byText(WebElement.H1, 'Cashback Extras');
        this.selectEverydayOffersLink = helper.byText(WebElement.H1, 'Cashback Extras');
        this.settingPageTitle = helper.byText(WebElement.H2, 'Change your Cashback Extras settings');
        this.findOutMorePageTitle = helper.byText(WebElement.H1, 'Find out more about Cashback Extras');
    }
}

module.exports = SpendingRewardsScreen;
