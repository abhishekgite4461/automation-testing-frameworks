const BaseOnlinePaperPreference = require('../onlinePaperPreference.screen');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');
const ViewClasses = require('../../../enums/androidViewClass.enum');

class OnlinePaperPreference extends BaseOnlinePaperPreference {
    constructor() {
        super();
        this.title = helper.byPartialText(WebElement.H1, 'Manage my online and paper preferences');
        this.acceptCookie = helper.byText(ViewClasses.BUTTON, 'Accept all');
        this.customisePreferences = `${helper.byId(WebElement.INPUT, 'custom-paperless')}/parent::span`;
        this.togglePreference = "(//dd[@class='toggleswitch'])[1]";
        this.updatePreferences = helper.byId(WebElement.INPUT, 'managepaperless:m-mp-continue');
        this.confirmPreferenceHeader = helper.byClass(WebElement.DIV, 'm-mobile-paperless-content m-mobile-paperless-manage manage-confirmation');
        this.termsAndCondition = helper.byClass(WebElement.SPAN, 'checkbox-mask');
        this.password = helper.byType(WebElement.INPUT, 'password');
        this.confirmPreferenceButton = helper.byId(WebElement.INPUT, 'confirmpaperlesschanges:m-mp-continue');
        this.onlinePaperPreferenceSuccessHeader = helper.byPartialText(WebElement.H1, 'updated your preferences');
    }
}

module.exports = OnlinePaperPreference;
