const viewSpendingInsightsScreen = require('../viewSpendingInsights.screen');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');

class ViewSpendingInsightsScreen extends viewSpendingInsightsScreen {
    constructor() {
        super();
        this.title = helper.byPartialText(WebElement.H1, 'Your spend');
        this.debitCard = helper.byClass(WebElement.SPAN, 'sn-si-fo-fa-re');
        this.debitCardInsights = helper.byClass(WebElement.LABEL, 'sn-si-accessibility-hidden');
    }
}

module.exports = ViewSpendingInsightsScreen;
