const BaseInternationalPaymentScreen = require('../internationalPayment.screen');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');

class InternationalPaymentScreen extends BaseInternationalPaymentScreen {
    constructor() {
        super();
        this.title = helper.byPartialText(WebElement.LABEL, 'Pay and transfer');
        this.recipients = helper.byText(WebElement.H2, 'International payments recipients');
        this.addnewinternationalrecipient = helper.byClass(WebElement.SPAN, 'add-beneficiary-link__icon');
        this.selectCountryOfRecipientBank = helper.byClass(WebElement.SELECT, 'country-list__select-control ');
        this.countryRadioButton = '//div[@class="country-list__select-control "]';
        this.countryRadioButton_2 = helper.byClass(WebElement.INPUT, 'search-bar');
        this.countryRadioButton_1 = (country) => `//ul[@class="countries-modal-list"]/li/span[contains(.,'${country}')]`;
        this.recipientName = helper.byName(WebElement.INPUT, 'name');
        this.recipientBankAddr = helper.byName(WebElement.INPUT, 'beneficiaryAddressAddLine1');
        this.txtBicSwift = helper.byName(WebElement.INPUT, 'bic');
        this.IBAN = helper.byName(WebElement.INPUT, 'iban');
        this.ipContinue = helper.byClass(WebElement.BUTTON, 'nav-card__continue-btn');
        this.sendingCurrencyRecipient = helper.byClass(WebElement.DIV, 'currency-selector-button');
        this.currencySelectionButton = helper.byClass(WebElement.LI, 'currency-list-item');
        this.continueRecipientDetailsButton = helper.byClass(WebElement.BUTTON, 'nav-card__continue-btn');
        this.ipAmount = helper.byName(WebElement.INPUT, 'sellingCurrencyAmount');
        this.reference = helper.byName(WebElement.INPUT, 'reference');
        this.ipPassword = helper.byClass(WebElement.INPUT, 'user-password-form__input d');
        this.contuneButton = helper.byClass(WebElement.BUTTON, 'nav-card__continue-btn');
        this.radioButton = (number) => `//span[contains(.,'${number}')]`;
        this.ipConfirm = helper.byClass(WebElement.BUTTON, 'nav-card__button');
        this.successPage = helper.byClass(WebElement.DIV, 'confirm-payment-page__confirmation-copy');
        this.selectIPRecipient = helper.byClass(WebElement.BUTTON, 'beneficiary-details__int-payment-btn');
        this.selectExistingRecipient = (existRecipient) => `//p[contains(.,'${existRecipient}')]`;
        this.ipPaymentConfirmation = helper.byPartialText(WebElement.H1, 'International payment confirmed');
        this.makePayment = '//p[preceding-sibling::h4[text()="Digital"]]/a[contains(@id,"lnkMakePayment")]';
        this.makeIPPaymentTitle = helper.byText(WebElement.H1, 'Make an international payment');
        this.currency = '//label[contains(text(), "sterling equivalent") or contains(text(), "Sterling equivalent"]';
        this.currencyToPayIn = helper.byPartialText(WebElement.LABEL, 'terling equivalent');
        this.confirmIPPayment = helper.byText(WebElement.H1, 'Confirm international payment details');
        this.ipTermsAndConditions = helper.byId(WebElement.INPUT, 'frmForm11:bchkbxAgreeTerms');
        this.ippaymentreferenceno = helper.byPartialText(WebElement.LABEL, 'FP');
        this.recipientAddr = helper.byId(WebElement.INPUT, 'recipientDetails:txtAddress1');
        this.confirmButton = helper.byId(WebElement.INPUT, 'myForm:Btncontinue');
        this.paymentCurrencyOKButton = helper.byClass(WebElement.BUTTON, 'selected-currency-change-warning__cta-button');
        this.currencySelectionOKButton = helper.byClass(WebElement.BUTTON, 'non-default-currency-warning__cta-button');
        this.allCurrencySearchTextBox = helper.byClass(WebElement.INPUT, 'search-bar');
        this.confirmationNumber = helper.byPartialText(WebElement.P, 'FP');
        this.countryRecipientBankPage = helper.byClass(WebElement.H2, 'country-list-with-errors__heading');
        this.selectCountryPage = helper.byClass(WebElement.H2, 'countries-modal-list__title');
        this.paymentCurrencyWarningPage = helper.byClass(WebElement.H2, 'selected-currency-change-warning__heading');
        this.toAccountIBANRecipientDetails = (toAccount) => `//p[contains(.,'${toAccount}')]`;
        this.reviewPaymentPage = helper.byClass(WebElement.SPAN, 'review-payment__payment-copy');
        this.enterEIAPage = helper.byClass(WebElement.SPAN, 'initialise-call__heading');
    }
}

module.exports = InternationalPaymentScreen;
