const BaseForgottenLoginDetailsScreen = require('../forgottenLoginDetails.screen');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');

class ForgottenLoginDetailsScreen extends BaseForgottenLoginDetailsScreen {
    constructor() {
        super();
        this.title = helper.byText(WebElement.SPAN, 'Forgotten your sign-in details?');
    }
}

module.exports = ForgottenLoginDetailsScreen;
