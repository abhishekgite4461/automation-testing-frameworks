const BaseAddRewardExtraScreen = require('../addRewardExtra.screen');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');

class AddRewardExtraScreen extends BaseAddRewardExtraScreen {
    constructor() {
        super();
        this.titleAddRewardExtra = helper.byPartialText(WebElement.H1, 'Reward Extras');
        this.alreadyHaveRewardExtras = helper.byPartialText(WebElement.H1, 'You already have Reward Extras');
        this.backToAccountOverviewTrackURCA = helper.byClass(WebElement.A, 'udf-anchor pull-left margin-top no-margin-top nav-container');
        this.backToAccountOverviewTrackRCA = helper.byClass(WebElement.A, 'udf-anchor pull-left no-margin-top height-adjust nav-container');
        this.addRewardExtraOffers = helper.byPartialText(WebElement.H1, 'updated your preferences');
        this.offerAccordion = helper.byClass(WebElement.DIV, 'udf-accordion-header banner-tile udf-icon-arrow-down ');
        this.chooseThisOptionAddURCA = helper.byClass(WebElement.SPAN, 'btn-label');
        this.titleLetsGetStarted = helper.byPartialText(WebElement.H3, 'Your offer will be added to this account');
        this.continueOnLetsGetStarted = helper.byPartialText(WebElement.SPAN, 'Continue');
        this.titleYourEmail = helper.byPartialText(WebElement.H3, 'Confirm your email address');
        this.yesButtonAddURCA = helper.byPartialText(WebElement.SPAN, 'Yes');
        this.continueOnYourEmail = helper.byPartialText(WebElement.SPAN, 'Continue');
        this.titleTheLegalBits = helper.byPartialText(WebElement.H1, 'The legal bits');
        this.iConfirmCheckBoxAddURCA = helper.byClass(WebElement.SPAN, 'checkbox-selection udf-icon-tick ');
        this.rcaIneligibilityMessage = helper.byClass(WebElement.DIV, 'udf-notification ');
        this.titleRCAIneligibility = helper.byPartialText(WebElement.H1, 'You can’t add Reward Extras to this account');
        this.titleUrcaRewardExtra = helper.byPartialText(WebElement.H1, 'Reward Extras Tracker');
        this.offerProgressMessage = helper.byPartialText(WebElement.P, 'Your offer progress for');
        this.showMyOfferProgress = helper.byPartialText(WebElement.SPAN, 'Show my offer progress');
    }
}
module.exports = AddRewardExtraScreen;
