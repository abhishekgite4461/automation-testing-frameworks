const BasePdfStatementCreditCard = require('../pdfStatementCreditCard.screen');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');

class PdfStatementCreditCard extends BasePdfStatementCreditCard {
    constructor() {
        super();
        this.title = helper.byPartialText(WebElement.H1, 'PDF statements');
        this.viewPDFButton = helper.byClass(WebElement.A, 'm-action-button m-action-button-on-white no-arrow');
    }
}

module.exports = PdfStatementCreditCard;
