const Screen = require('../../screen');

class RegisterPayAContactScreen extends Screen {
    constructor() {
        super();
        this.p2pTitle = undefined;
        this.registerButton = undefined;
        this.continueButton = undefined;
        this.agreeCheckBox = undefined;
        this.continuetoP2PButton = undefined;
        this.continueToP2PAuthentication = undefined;
        this.P2PsuccessMessage = undefined;
    }
}

module.exports = RegisterPayAContactScreen;
