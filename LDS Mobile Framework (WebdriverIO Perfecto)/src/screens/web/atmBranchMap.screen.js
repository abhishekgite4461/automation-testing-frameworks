const Screen = require('../screen');

class AtmBranchMap extends Screen {
    constructor() {
        super();
        this.googleMapContainer = '//*[@class="map-inner map-extent"]';
        this.mapSearchBox = '//*[contains(@class, \'ml-searchbox-button-placeholder\')]';
    }
}

module.exports = AtmBranchMap;
