const Screen = require('../screen');
const helper = require('./web.helper');
const WebElement = require('../../enums/webElement.enum');

class StandingOrderDeleteScreen extends Screen {
    constructor() {
        super();
        this.soDeleteTitle = helper.byText(WebElement.H1, 'Delete standing order');
        this.soDeletePassword = helper.byId(WebElement.INPUT, 'frmCancelSO:password');
        this.soDeleteSOButton = helper.byId(WebElement.INPUT, 'frmCancelSO:btnCancelSO');
        this.soCancelSOButton = helper.byId(WebElement.INPUT, 'cancelProcess');
    }
}

module.exports = StandingOrderDeleteScreen;
