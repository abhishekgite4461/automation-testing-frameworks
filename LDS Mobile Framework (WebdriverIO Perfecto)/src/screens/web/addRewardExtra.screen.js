const Screen = require('../screen');
const helper = require('./web.helper.js');
const WebElement = require('../../enums/webElement.enum');

class AddRewardExtra extends Screen {
    constructor() {
        super();
        this.acceptCookie = undefined;
        this.pageHeaderAddRewardExtra = helper.byDataSelector(WebElement.H1, 'reward-offer-page-heading');
        this.propRewardFilmRental = helper.byDataSelector(WebElement.DIV, 'reward-offer-page-container-RT004');
        this.propRewardCinemaTicket = helper.byDataSelector(WebElement.DIV, 'reward-offer-page-container-RT002');
        this.propRewardDigitalMagazine = helper.byDataSelector(WebElement.DIV, 'reward-offer-page-container-RT003');
        this.propRewardCashBack = helper.byDataSelector(WebElement.DIV, 'reward-offer-page-container-RT001');
        this.categorySpendAccordion = helper.byDataSelector(WebElement.DIV, 'reward-category-page-container-CT001');
        this.categorySaveAccordion = helper.byDataSelector(WebElement.DIV, 'reward-category-page-container-CT002');
        this.titleCategoryPage = helper.byDataSelector(WebElement.P, 'reward-category-page-sub-heading');
        this.chooseThisOptionSave = helper.byDataSelector(WebElement.BUTTON, 'choose-category-CT002-btn');
        this.chooseThisOptionSpend = helper.byDataSelector(WebElement.BUTTON, 'choose-category-CT001-btn');
        this.chooseHowToEarn = helper.byDataSelector(WebElement.BUTTON, 'choose-reward-btn');
        this.pageHeadingLetsGetStarted = helper.byDataSelector(WebElement.H1, 'account-selection-page-heading');
        this.continueOnLetsGetStarted = helper.byDataSelector(WebElement.BUTTON, 'continue-button');
        this.pageHeadingYourEmail = helper.byDataSelector(WebElement.H1, 'verify-email-page-heading');
        this.yesButtonEmail = helper.byDataSelector(WebElement.BUTTON, 'verify-email-yes');
        this.continueOnYourEmail = helper.byDataSelector(WebElement.BUTTON, 'continue-button');
        this.pageHeadingTheLegalBits = helper.byDataSelector(WebElement.H1, 't-and-c-page-heading');
        this.iConfirmCheckBoxLegalBits = helper.byDataSelector(WebElement.SPAN, '-checkbox');
        this.titleTrackRewardExtra = helper.byDataSelector(WebElement.H1, 'engage-hub-page-header');
        this.pageHeadingTrackRewardExtraDate = helper.byDataSelector(WebElement.P, 'engage-hub-accrual-date');
        this.titleTrackRewardQualifyingStatus = helper.byDataSelector(WebElement.HEADER, 'proposition-component-qualifyingStatus-title');
        this.trackRewardExtraProgress = helper.byDataSelector(WebElement.BUTTON, 'proposition-component-qualifyingStatus-toggle');
        this.trackRewardExtraQualificationDesc = helper.byDataSelector(WebElement.DIV, 'qualification-component-description');
        this.titleAlreadyHaveReward = helper.byDataSelector(WebElement.H1, 'error-page-heading');
        this.AlreadyHaveRewardmessage = helper.byPartialText(WebElement.H1, 'You already have Reward Extras');
        this.titleIneligibilityProduct = helper.byPartialText(WebElement.H1, 'You can’t add Reward Extras to this account');
        this.rcaIneligibilityMessage = undefined;
        this.titleRCAIneligibility = undefined;
        this.offerProgressMessage = undefined;
        this.alreadyHaveRewardExtras = undefined;
        this.backToAccountOverviewTrackURCA = undefined;
        this.backToAccountOverviewTrackRCA = undefined;
        this.addRewardExtraOffers = undefined;
        this.offerAccordion = undefined;
    }
}
module.exports = AddRewardExtra;
