const Screen = require('../../screen');

class ProductHubSavingsApplyScreen extends Screen {
    constructor() {
        super();
        this.applynow = undefined;
        this.accountOpeningSuccessMessage = undefined;
    }
}

module.exports = ProductHubSavingsApplyScreen;
