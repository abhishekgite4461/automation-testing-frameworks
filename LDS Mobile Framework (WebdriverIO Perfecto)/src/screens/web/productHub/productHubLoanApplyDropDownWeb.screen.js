const Screen = require('../../screen');

class ProductHubLoanApplyDropDownWebScreen extends Screen {
    constructor() {
        super();
        this.selectLoanPurposeOption = undefined;
        this.selectLoanPurposeOptionLabel = undefined;
        this.selectEmploymentOption = undefined;
        this.selectEmploymentOptionLabel = undefined;
    }
}

module.exports = ProductHubLoanApplyDropDownWebScreen;
