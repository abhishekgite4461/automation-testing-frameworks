const BaseProductHubChangeAccountTypeScreen = require('../../screen');

class ProductHubChangeAccountTypeScreen extends BaseProductHubChangeAccountTypeScreen {
    constructor() {
        super();
        this.changeAccountTypeTitle = undefined;
    }
}

module.exports = ProductHubChangeAccountTypeScreen;
