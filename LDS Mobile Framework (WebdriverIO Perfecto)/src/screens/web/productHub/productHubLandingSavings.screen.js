const Screen = require('../../screen');

class ProductHubLandingSavingsScreen extends Screen {
    constructor() {
        super();
        this.compareSavings = undefined;
        this.select = undefined;
        this.OthersavingsAccountName = undefined;
        this.Otherselect = undefined;
    }
}

module.exports = ProductHubLandingSavingsScreen;
