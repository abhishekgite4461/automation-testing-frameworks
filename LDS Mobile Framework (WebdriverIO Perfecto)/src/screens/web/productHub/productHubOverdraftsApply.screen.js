const Screen = require('../../screen');

class ProductHubOverdraftsApplyScreen extends Screen {
    constructor() {
        super();
        this.overdraftPageTitle = undefined;
        this.applyNowButton = undefined;
        this.verifyOverdraftsDetailsPage = undefined;
        this.overdraftAmountField = undefined;
        this.selectEmployment = undefined;
        this.monthlyIncome = undefined;
        this.monthlyIncomeAfterTax = undefined;
        this.monthlyOutgoings = undefined;
        this.dependents = undefined;
        this.childCareCost = undefined;
        this.continueOverdraftsButton = undefined;
        this.creditInformationPageTitle = undefined;
        this.agreementCheckboxOne = undefined;
        this.agreementCheckboxTwo = undefined;
        this.passwordField = undefined;
        this.submitButton = undefined;
        this.overdraftDeclinePageTitle = undefined;
        this.overdraftPopUpTitle = undefined;
    }
}

module.exports = ProductHubOverdraftsApplyScreen;
