const Screen = require('../../screen');

class ProductHubOverdraftsApplyDropDownWebScreen extends Screen {
    constructor() {
        super();
        this.retiredLabel = undefined;
        this.selectEmploymentOption = undefined;
    }
}

module.exports = ProductHubOverdraftsApplyDropDownWebScreen;
