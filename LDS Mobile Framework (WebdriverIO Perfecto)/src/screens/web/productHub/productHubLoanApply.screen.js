const Screen = require('../../screen');

class ProductHubLoanApplyScreen extends Screen {
    constructor() {
        super();
        this.helloTitle = undefined;
        this.continueLoanButton = undefined;
        this.loanTitle = undefined;
        this.termField = undefined;
        this.loanAmountField = undefined;
        this.selectLoanPurposeDropDown = undefined;
        this.flexibleLoanButton = undefined;
        this.repaymentOptionTitle = undefined;
        this.closestMatchOption = undefined;
        this.yourApplicationTitle = undefined;
        this.selectEmploymentStatus = undefined;
        this.monthlyIncomeField = undefined;
        this.monthlyOutgoingField = undefined;
        this.dependentsField = undefined;
        this.childCareCostsField = undefined;
        this.agreementCheckboxOne = undefined;
        this.agreementCheckboxTwo = undefined;
        this.checkEligibilityButton = undefined;
        this.loanDeclinePageTitle = undefined;
    }
}

module.exports = ProductHubLoanApplyScreen;
