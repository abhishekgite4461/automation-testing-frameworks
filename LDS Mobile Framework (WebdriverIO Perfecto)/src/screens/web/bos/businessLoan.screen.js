const BaseBusinessLoanScreen = require('../businessLoan.screen.js');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');

class BusinessLoanScreen extends BaseBusinessLoanScreen {
    constructor() {
        super();
        this.title = helper.byText(WebElement.H1, 'Business Loan');
    }
}

module.exports = BusinessLoanScreen;
