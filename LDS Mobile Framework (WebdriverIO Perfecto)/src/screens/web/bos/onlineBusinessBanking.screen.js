const BaseOnlineBusinessBankingScreen = require('../onlineBusinessBanking.screen');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');

class OnlineBusinessBanking extends BaseOnlineBusinessBankingScreen {
    constructor() {
        super();
        this.title = helper.byText(WebElement.H2, 'Safe and secure online business banking');
    }
}

module.exports = OnlineBusinessBanking;
