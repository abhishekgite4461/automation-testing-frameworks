const BaseProductHubOverdraftsApplyScreen = require('../../productHub/productHubOverdraftsApply.screen');
const helper = require('../../web.helper');
const WebElement = require('../../../../enums/webElement.enum');

class ProductHubOverdraftsApplyScreen extends BaseProductHubOverdraftsApplyScreen {
    constructor() {
        super();
        this.overdraftPageTitle = helper.byStartText(WebElement.H1,
            'Apply for a Planned Overdraft');
        this.applyNowButton = helper.byId(WebElement.INPUT,
            'frmOverdraftApply:lstApplyOverdraftList:0:btnApplyNow');
        this.verifyOverdraftsDetailsPage = helper.byStartText(WebElement.H1,
            'Your planned overdraft details');
        this.overdraftAmountField = helper.byId(WebElement.INPUT,
            'frmApplyForOverdraft:strOverdraftLimitODR');
        this.selectEmployment = helper.byId(WebElement.SELECT,
            'frmApplyForOverdraft:slctEmploymentStatus1');
        this.monthlyIncome = helper.byId(WebElement.INPUT,
            'frmApplyForOverdraft:txtMonthlySalary1');
        this.monthlyIncomeAfterTax = helper.byId(WebElement.INPUT,
            'frmApplyForOverdraft:txtOtherIncome1');
        this.monthlyOutgoings = helper.byId(WebElement.INPUT,
            'frmApplyForOverdraft:txtMonthlyMortgage1');
        this.dependents = helper.byId(WebElement.INPUT,
            'frmApplyForOverdraft:noOfFinancialDependents1');
        this.childCareCost = helper.byId(WebElement.INPUT,
            'frmApplyForOverdraft:txtMonthlySpending1');
        this.continueOverdraftsButton = helper.byId(WebElement.INPUT,
            'frmApplyForOverdraft:lkDoContinueOverdraftODR');
        this.creditInformationPageTitle = helper.byStartText(WebElement.H1,
            'Pre-Contract');
        this.agreementCheckboxOne = helper.byId(WebElement.INPUT,
            'frmConfirmOverdraft:agreeTerms02');
        this.agreementCheckboxTwo = helper.byId(WebElement.INPUT,
            'frmConfirmOverdraft:agreeTerms01');
        this.passwordField = helper.byId(WebElement.INPUT,
            'frmConfirmOverdraft:txtAuthPwd');
        this.submitButton = helper.byId(WebElement.INPUT,
            'frmConfirmOverdraft:lkSubmitApplication');
        this.overdraftDeclinePageTitle = helper.byPartialText(WebElement.H1,
            'declined your planned overdraft application');
        this.continueButton = helper.byId(WebElement.INPUT,
            'frmApplyForOverdraft:lkDoContinueOverdraftODR');
        this.employementStatusDropDown = 'Please select';
        this.overdraftPopUpTitle = helper.byText(WebElement.H1,
            'About overdrafts');
    }
}

module.exports = ProductHubOverdraftsApplyScreen;
