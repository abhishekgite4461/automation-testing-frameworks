const BaseProductHubSavingsApplyScreen = require('../../productHub/productHubSavingsApply.screen');
const helper = require('../../web.helper');
const WebElement = require('../../../../enums/webElement.enum');
const DeviceHelper = require('../../../../utils/device.helper');

class ProductHubSavingsApplyScreen extends BaseProductHubSavingsApplyScreen {
    constructor() {
        super();
        this.applynow = '//div[2]/div[1]/button[1]/span[1]';
        this.applynowISA = '//Input[@value="Apply now"]';
        this.acceptTermsandConditions = helper.byPartialText(WebElement.LABEL, 'conditions');
        this.openAccount = '//*[@title = "Open account"]';
        this.acctSortCodeandNumber = helper.byStartText(WebElement.P, 'Sort code');
        if (DeviceHelper.isAndroid()) {
            this.accountOpeningSuccessMessage = '//h1[text()="Your Fixed Cash ISA 2 Year account is now open" or '
                + 'text()="You\'ve opened a Fixed Cash ISA 2 Year" or text()="Congratulations, you have just opened '
                + 'a Fixed Cash ISA 2 Year"]';
        } else {
            this.accountOpeningSuccessMessage = '//h1[text()="Your Access Saver account is now open" or '
                + 'text()="You\'ve opened a Access Saver" or '
                + 'text()="Congratulations, you have just opened a Access Saver"]';
        }
    }
}

module.exports = ProductHubSavingsApplyScreen;
