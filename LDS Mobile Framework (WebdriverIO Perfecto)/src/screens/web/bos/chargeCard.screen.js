const BaseChargeCardScreen = require('../chargeCard.screen');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');

class ChargeCardScreen extends BaseChargeCardScreen {
    constructor() {
        super();
        this.chargeCardTitle = helper.byText(WebElement.SPAN, 'Business Charge Card');
    }
}

module.exports = ChargeCardScreen;
