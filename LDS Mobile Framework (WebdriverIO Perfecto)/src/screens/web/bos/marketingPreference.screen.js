const BaseMarketingPreferenceScreen = require('../marketingPreference.screen');
const helper = require('../web.helper');
const WebElement = require('../../../enums/webElement.enum');

class MarketingPreferenceScreen extends BaseMarketingPreferenceScreen {
    constructor() {
        super();
        this.title = helper.byText(WebElement.H1, 'Our Brands');
    }
}

module.exports = MarketingPreferenceScreen;
