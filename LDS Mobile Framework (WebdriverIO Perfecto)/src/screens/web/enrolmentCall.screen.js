const Screen = require('../screen');

class EnrolmentCallScreen extends Screen {
    constructor() {
        super();
        this.eiaPin = '//p[@class="authNumber"]';
    }
}

module.exports = EnrolmentCallScreen;
