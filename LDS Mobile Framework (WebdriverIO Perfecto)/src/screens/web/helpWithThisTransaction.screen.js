const Screen = require('../screen');
const helper = require('./web.helper');
const WebElement = require('../../enums/webElement.enum');

class HelpWithThisTransactionScreen extends Screen {
    constructor() {
        super();
        this.covid19SupportWebPage = helper.byText(WebElement.H1, 'Coronavirus: help and support');
        this.dontRecogniseTransactionWebPage = helper.byText(WebElement.H1, 'Common business and retailer names');
        this.expectingRefundWebPage = helper.byText(WebElement.H1, 'I recognise a transaction but want to question it');
        this.disputeMessageWebPage = helper.byText(WebElement.H1, 'I don\'t recognise a transaction');
    }
}

module.exports = HelpWithThisTransactionScreen;
