const Screen = require('../screen');

class ForgottenLoginDetailsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.backToApp = undefined;
    }
}

module.exports = ForgottenLoginDetailsScreen;
