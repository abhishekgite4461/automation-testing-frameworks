const Screen = require('../screen');

class OnlinePaperPreference extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.acceptCookie = undefined;
        this.customisePreferences = undefined;
        this.togglePreference = undefined;
        this.updatePreferences = undefined;
        this.confirmPreferenceHeader = undefined;
        this.termsAndCondition = undefined;
        this.password = undefined;
        this.confirmPreferenceButton = undefined;
        this.onlinePaperPreferenceSuccessHeader = undefined;
    }
}

module.exports = OnlinePaperPreference;
