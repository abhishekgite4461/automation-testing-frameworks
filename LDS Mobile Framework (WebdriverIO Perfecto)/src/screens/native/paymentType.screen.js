const Screen = require('../screen');

class PaymentTypeScreen extends Screen {
    constructor() {
        super();
        this.payContactError = undefined;
        this.payContact = undefined;
    }
}

module.exports = PaymentTypeScreen;
