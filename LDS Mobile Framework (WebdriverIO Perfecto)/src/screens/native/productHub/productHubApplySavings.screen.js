const Screen = require('../../screen');

class ProductHubApplySavingsScreen extends Screen {
    constructor() {
        super();
        this.applyNow = undefined;
        this.acceptTC = undefined;
        this.openAccount = undefined;
        this.accountOpeningSuccessMessage = undefined;
        this.acctSortCodeandNumber = undefined;
        this.accountNum = undefined;
    }
}

module.exports = ProductHubApplySavingsScreen;
