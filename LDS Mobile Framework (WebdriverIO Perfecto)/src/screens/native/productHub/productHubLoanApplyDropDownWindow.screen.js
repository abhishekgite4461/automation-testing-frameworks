const Screen = require('../../screen');

class ProductHubLoanApplyDropDownWindowScreen extends Screen {
    constructor() {
        super();
        this.selectLoanPurposeOption = undefined;
        this.selectEmploymentOption = undefined;
    }
}

module.exports = ProductHubLoanApplyDropDownWindowScreen;
