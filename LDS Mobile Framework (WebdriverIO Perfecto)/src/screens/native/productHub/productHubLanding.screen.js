const Screen = require('../../screen');

class ProductHubLandingScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.loanTermField = undefined;
        this.numberPad = undefined;
    }
}

module.exports = ProductHubLandingScreen;
