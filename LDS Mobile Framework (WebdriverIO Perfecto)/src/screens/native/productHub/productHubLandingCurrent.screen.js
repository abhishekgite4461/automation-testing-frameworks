const Screen = require('../../screen');

class ProductHubLandingCurrentScreen extends Screen {
    constructor() {
        super();
        this.currentAccountOverdraft = undefined;
    }
}

module.exports = ProductHubLandingCurrentScreen;
