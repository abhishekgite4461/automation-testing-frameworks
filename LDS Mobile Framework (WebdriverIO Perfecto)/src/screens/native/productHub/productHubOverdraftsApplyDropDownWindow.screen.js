const Screen = require('../../screen');

class ProductHubOverdraftsApplyDropDownWindowScreen extends Screen {
    constructor() {
        super();
        this.selectEmploymentOption = undefined;
        this.retiredLabel = undefined;
    }
}

module.exports = ProductHubOverdraftsApplyDropDownWindowScreen;
