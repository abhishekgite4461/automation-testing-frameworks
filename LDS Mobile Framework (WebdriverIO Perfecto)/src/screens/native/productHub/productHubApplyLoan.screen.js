const Screen = require('../../screen');

class ProductHubApplyLoanScreen extends Screen {
    constructor() {
        super();
        this.helloTitle = undefined;
        this.continueLoanButton = undefined;
        this.loanTitle = undefined;
        this.termField = undefined;
        this.loanAmountField = undefined;
        this.selectLoanPurposeDropDown = undefined;
        this.flexibleLoanButton = undefined;
        this.repaymentOptionTitle = undefined;
        this.closestMatchOption = undefined;
        this.yourApplicationTitle = undefined;
        this.selectEmploymentStatus = undefined;
        this.selectEmploymentStatusLabel = undefined;
        this.monthlyIncomeField = undefined;
        this.monthlyOutgoingField = undefined;
        this.dependentsField = undefined;
        this.childCareCostsField = undefined;
        this.agreementCheckboxOne = undefined;
        this.agreementCheckboxTwo = undefined;
        this.agreementForStatements = undefined;
        this.checkEligibilityButton = undefined;
        this.loanDeclinePageTitle = undefined;
        this.bookAppointmentOption = undefined;
        this.bookAppointment = undefined;
        this.titleFindBranch = undefined;
        this.postcode = undefined;
        this.findBranch = undefined;
        this.branchResults = undefined;
        this.continueFindBranch = undefined;
        this.titleAppointment = undefined;
    }
}

module.exports = ProductHubApplyLoanScreen;
