const Screen = require('../../screen');

class ProductHubLandingSavingsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.compareSavings = undefined;
        this.savingsAccountName = undefined;
        this.select = undefined;
    }
}

module.exports = ProductHubLandingSavingsScreen;
