const Screen = require('../../screen');

class ProductHubScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.loan = undefined;
        this.loanCalculator = undefined;
        this.currentAccount = undefined;
        this.currentAccountOverdraft = undefined;

        this.featured = undefined;
        this.businessCurrentAccount = undefined;
        this.businessSavingsAccount = undefined;
        this.currentAccounts = undefined;
        this.loans = undefined;
        this.creditCards = undefined;
        this.savings = undefined;
        this.insurance = undefined;
        this.mortgages = undefined;
        this.featuredExpanded = undefined;
        this.businessCurrentAccountCollapsed = undefined;
        this.productOptions = undefined;
        this.loanCalculator = undefined;
        this.carInsurance = undefined;
        this.compareAccounts = undefined;
        this.overdrafts = undefined;
        this.travelMoney = undefined;
        this.internationalPayments = undefined;
        this.calculator = undefined;
        this.borrowMore = undefined;
        this.personalLoan = undefined;
        this.carFinance = undefined;
        this.compareCreditCards = undefined;
        this.checkMyEligibility = undefined;
        this.balanceTransfer = undefined;
        this.compareSavingsAccounts = undefined;
        this.instantAccess = undefined;
        this.topUpISA = undefined;
        this.cashIsa = undefined;
        this.fixedTerm = undefined;
        this.homeInsurance = undefined;
        this.carInsurance = undefined;
        this.vanInsurance = undefined;
        this.travelInsurance = undefined;
        this.agreementInPrinciple = undefined;
        this.bookBranchAppointment = undefined;
        this.switchToNewDeal = undefined;
        this.otherMortgageOptions = undefined;
        this.businessLoan = undefined;
        this.applyToIncreaseOverDraft = undefined;
        this.discoverAndApplyTitle = undefined;
        this.shareDealing = undefined;
        this.marketsAndInsights = undefined;
    }
}

module.exports = ProductHubScreen;
