const Screen = require('../../screen');

class ProductHubLandingHomeInsuranceScreen extends Screen {
    constructor() {
        super();
        this.homeInsurance = undefined;
    }
}

module.exports = ProductHubLandingHomeInsuranceScreen;
