const Screen = require('../../screen');

class ProductHubApplyOverdraftScreen extends Screen {
    constructor() {
        super();
        this.overdraftPopUpTitle = undefined;
        this.overdraftPageTitle = undefined;
        this.overdraftAmountValue = undefined;
        this.overdraftAmountField = undefined;
        this.applyButton = undefined;
        this.employementStatusSelector = undefined;
        this.selectEmployment = undefined;
        this.monthlyIncomeAfterTax = undefined;
        this.monthlyOutgoings = undefined;
        this.childCareCost = undefined;
        this.lifeChangingEventField = undefined;
        this.overdraftDeclinePageTitle = undefined;
        this.continueButton = undefined;
        this.aboutDigitalLabel = undefined;
        this.overdraftPayBackButton = undefined;
        this.aboutOverdraftContinueButton = undefined;
        this.monthlyIncomeAfterTaxJoint = undefined;
        this.monthlyOutgoingsJoint = undefined;
        this.childCareCostJoint = undefined;
        this.letMeJustTypeLink = undefined;
        this.overdraftDays = undefined;
    }
}

module.exports = ProductHubApplyOverdraftScreen;
