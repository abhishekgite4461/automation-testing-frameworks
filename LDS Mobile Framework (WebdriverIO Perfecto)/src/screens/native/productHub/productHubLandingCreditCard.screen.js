const Screen = require('../../screen');

class ProductHubLandingCreditCardScreen extends Screen {
    constructor() {
        super();
        this.compareCreditCard = undefined;
    }
}

module.exports = ProductHubLandingCreditCardScreen;
