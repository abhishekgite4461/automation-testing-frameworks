const Screen = require('../../screen');

class ProductHubLandingLoanScreen extends Screen {
    constructor() {
        super();
        this.loanCalculator = undefined;
    }
}

module.exports = ProductHubLandingLoanScreen;
