const Screen = require('../screen');

class SecureInboxScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.inboxMessage = undefined;
        this.archiveButton = undefined;
        this.undoArchiveButton = undefined;
        this.verifyUndoArchiveMessage = undefined;
        this.pdfMessage = undefined;
        this.folderName = undefined;
        this.accountLabel = undefined;
        this.filteredMessage = undefined;
        this.noMessagesLabel = undefined;
    }
}

module.exports = SecureInboxScreen;
