const Screen = require('../../screen');

class ActivationCodeScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.activationCode = undefined;
        this.confirmCode = undefined;
        this.requestNewCode = undefined;
        this.errorMessage = undefined;
    }
}

module.exports = ActivationCodeScreen;
