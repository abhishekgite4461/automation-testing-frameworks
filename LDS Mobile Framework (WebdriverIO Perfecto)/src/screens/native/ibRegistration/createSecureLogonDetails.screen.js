const Screen = require('../../screen');

class CreateSecureLogonDetailsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.userId = undefined;
        this.userIdCheck = undefined;
        this.userIdOptionsTitle = undefined;
        this.userIdOptionsBack = undefined;
        this.userIdOptionsSuggestion = undefined;
        this.password = undefined;
        this.hideShowPassword = undefined;
        this.passwordfindOutMore = undefined;
        this.findOutMorePopUp = undefined;
        this.findOutMorePopUpClose = undefined;
        this.confirmPassword = undefined;
        this.hideShowConfirmPassword = undefined;
        this.userIdTip = undefined;
        this.passwordTip = undefined;
        this.confirmPasswordTip = undefined;
        this.commercialAccountMessage = undefined;
        this.commercialUserId = undefined;
        this.keyboardDeleteButton = undefined;
        this.nextButton = undefined;
        this.errorIcon = undefined;
        this.loadingIndicator = undefined;
    }
}

module.exports = CreateSecureLogonDetailsScreen;
