const Screen = require('../../screen');

class AccountDetailsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.errorMessage = undefined;
        this.errorMessageDismiss = undefined;
        this.accountType = undefined;
        this.accountTypeSelector = undefined;
        this.savingsSelector = undefined;
        this.loanSelector = undefined;
        this.mortgageSelector = undefined;
        this.creditCardsSelector = undefined;
        this.sortcodeSection = undefined;
        this.sortcodeFirstSection = undefined;
        this.sortcodeSecondSection = undefined;
        this.sortcodeThirdSection = undefined;
        this.accountNumber = undefined;
        this.loanReferenceNumber = undefined;
        this.loanTip = undefined;
        this.mortgageNumber = undefined;
        this.mortgageTip = undefined;
        this.creditCardNumber = undefined;
        this.creditCardTip = undefined;
        this.nextButton = undefined;
        this.backButton = undefined;
        this.keyboardDeleteButton = undefined;
    }
}

module.exports = AccountDetailsScreen;
