const Screen = require('../../screen');

class CreateMemorableInfoScreen extends Screen {
    constructor() {
        super();

        this.title = undefined;
        this.userIdErrorMessage = undefined;
        this.memorableInfo = undefined;
        this.confirmMemorableInfo = undefined;
        this.continueButton = undefined;
        this.backButton = undefined;
        this.memorableInfoTip = undefined;
        this.hideShowMemorableInfo = undefined;
        this.hideShowConfirmMemorableInfo = undefined;
        this.keyboardDeleteButton = undefined;
        this.confirmMemorableInfoTip = undefined;
        this.findOutMoreMemorableInfo = undefined;
        this.findOutMoreTipClose = undefined;
        this.findOutMoreTitle = undefined;
        this.customeKeyboardDeleteButton = undefined;
    }
}

module.exports = CreateMemorableInfoScreen;
