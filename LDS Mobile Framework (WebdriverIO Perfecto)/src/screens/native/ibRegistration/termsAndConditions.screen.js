const Screen = require('../../screen');

class TermsAndConditionsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.agreeButton = undefined;
        this.backButton = undefined;
        this.footerSection = undefined;
    }
}

module.exports = TermsAndConditionsScreen;
