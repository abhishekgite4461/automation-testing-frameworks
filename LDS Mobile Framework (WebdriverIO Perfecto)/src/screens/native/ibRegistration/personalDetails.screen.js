const Screen = require('../../screen');

class PersonalDetailsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.errorMessage = undefined;
        this.firstName = undefined;
        this.lastName = undefined;
        this.emailId = undefined;
        this.dateOfBirth = undefined;
        this.postCode = undefined;
        this.postCodeTip = undefined;
        this.backButton = undefined;
        this.nextButton = undefined;
        this.errorMessageDismiss = undefined;
        this.dobOkButton = undefined;
        this.dateField = undefined;
        this.monthField = undefined;
        this.yearField = undefined;
        this.youthErrorMessage = undefined;
        this.backButtonDOBError = undefined;
    }
}

module.exports = PersonalDetailsScreen;
