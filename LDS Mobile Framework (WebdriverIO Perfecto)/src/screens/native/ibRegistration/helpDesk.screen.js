const Screen = require('../../screen');

class HelpDeskScreen extends Screen {
    constructor() {
        super();
        this.helpCMSTitle = undefined;
        this.helpDeskError = undefined;
        this.helpDeskCMSError = undefined;
    }
}

module.exports = HelpDeskScreen;
