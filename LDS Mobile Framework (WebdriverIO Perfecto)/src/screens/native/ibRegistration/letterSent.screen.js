const Screen = require('../../screen');

class LetterSentScreen extends Screen {
    constructor() {
        super();
        this.letterSentTitle = undefined;
        this.registeredUsername = undefined;
        this.letterSentText = undefined;
        this.navigateBackButton = undefined;
    }
}

module.exports = LetterSentScreen;
