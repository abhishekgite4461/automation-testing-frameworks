const Screen = require('../../screen');

class NewWelcomeBenefitsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.registerButton = undefined;
        this.logOnButton = undefined;
        this.webTitle = undefined;
        this.fraudLink = undefined;
        this.demoLink = undefined;
        this.customerLink = undefined;
        this.alertDialog = undefined;
        this.yesLeaveTheApp = undefined;
        this.cancelLeaveTheApp = undefined;
        this.fraudGuaranteeTitle = undefined;
        this.appDemoTitle = undefined;
        this.newCustomerTitle = undefined;
    }
}

module.exports = NewWelcomeBenefitsScreen;
