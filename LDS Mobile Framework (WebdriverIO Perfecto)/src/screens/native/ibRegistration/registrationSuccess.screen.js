const Screen = require('../../screen');

class RegistrationSuccessScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.subTitle = undefined;
        this.continueButton = undefined;
        this.successMessage = undefined;
        this.footerMessage = undefined;
    }
}

module.exports = RegistrationSuccessScreen;
