const Screen = require('../../screen');

class PostCodeErrorScreen extends Screen {
    constructor() {
        super();
        this.postCodeTitle = undefined;
        this.postCode = undefined;
        this.nextButton = undefined;
    }
}

module.exports = PostCodeErrorScreen;
