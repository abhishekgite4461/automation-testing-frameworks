const Screen = require('../screen');

class DataConsent extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.performanceConsentToggleON = undefined;
        this.performanceConsentToggleOFF = undefined;
        this.confirmConsent = undefined;
        this.performanceConsent = undefined;
        this.winBackTitle = undefined;
        this.dialogTitle = undefined;
        this.cookiePolicyLink = undefined;
    }
}

module.exports = DataConsent;
