const Screen = require('../screen');

class EnvironmentScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.switchToggle = undefined;
        this.nextButton = undefined;
    }
}

module.exports = EnvironmentScreen;
