const Screen = require('../../screen');

class TransferPaymentFillDetailsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.amount = undefined;
        this.continueButton = undefined;
        this.isaAccount = undefined;
        this.standingOrderSwitch = undefined;
        this.confirmButton = undefined;
    }
}

module.exports = TransferPaymentFillDetailsScreen;
