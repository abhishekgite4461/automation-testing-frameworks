const Screen = require('../../screen');

class AmendStandingOrderScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.amendStandingOrderAmount = undefined;
        this.amendButton = undefined;
        this.deleteButton = undefined;
        this.newlyCreatedStandingOrder = undefined;
    }
}
module.exports = AmendStandingOrderScreen;
