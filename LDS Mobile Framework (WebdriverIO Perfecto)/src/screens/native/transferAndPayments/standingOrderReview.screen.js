const Screen = require('../../screen');

class StandingOrderReviewScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.confirmStandingOrderButton = undefined;
        this.fromAccountDetails = undefined;
        this.toAccountDetails = undefined;
        this.amountValue = undefined;
        this.dateFromLabel = undefined;
        this.reviewPaymentEditButton = undefined;
        this.reviewPaymentConfirmButton = undefined;
        this.navigationBarBackButton = undefined;
        this.remitterName = undefined;
        this.remitterSortCode = undefined;
        this.remitterAccountNumber = undefined;
        this.benefeciaryName = undefined;
        this.benefeciarySortCode = undefined;
        this.benefeciaryAccountNumber = undefined;
        this.amount = undefined;
        this.reference = undefined;
        this.standingOrderDates = undefined;
        this.frequency = undefined;
        this.standingOrderStartDate = undefined;
        this.standingOrderEndDate = undefined;
    }
}

module.exports = StandingOrderReviewScreen;
