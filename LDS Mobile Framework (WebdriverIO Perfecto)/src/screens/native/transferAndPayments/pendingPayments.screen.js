const Screen = require('../../screen');

class PendingPaymentScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.pageContainer = undefined;
        this.actionMenu = undefined;
        this.futureTransaction = undefined;
        this.backButton = undefined;
    }
}

module.exports = PendingPaymentScreen;
