const Screen = require('../../screen');

class DirectDebitScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.selectDirectDebitTab = undefined;
        this.cancelButton = undefined;
        this.directDebitCancelled = undefined;
    }
}
module.exports = DirectDebitScreen;
