const Screen = require('../../screen');

class UkBeneficiaryDetailScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.beneficiaryTileIcon = undefined;
        this.paymentHubAddUkAccountAdditionalContextCopy = undefined;
        this.paymentHubAddUkAccountPayeeNameLabel = undefined;
        this.payeeNameUk = undefined;
        this.paymentHubAddUkAccountSortCodeLabel = undefined;
        this.sortCodeField = undefined;
        this.paymentHubAddUkAccountAccountNumberLabel = undefined;
        this.accountNumberUk = undefined;
        this.paymentHubAddUkAccountCancelButton = undefined;
        this.paymentHubAddUkAccountContinueButton = undefined;
        this.navigationToolbarBackButton = undefined;
        this.arrangementSummaryListFromAccount = undefined;
        this.errorMessageUkBeneficiary = undefined;
        this.errorMessageExclamatory = undefined;
        this.paymentHubAddUkAccountSecurityInfoText = undefined;
        this.debitCardTile = undefined;
        this.paymentHubAddUkAccountTypeCheckBox = undefined;
        this.invalidAccountNumberErrorMessage = undefined;
        this.closeInvalidAccountNumberErrorMessage = undefined;
    }
}

module.exports = UkBeneficiaryDetailScreen;
