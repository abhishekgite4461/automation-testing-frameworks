const Screen = require('../../screen');

class PaymentBlockedScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
    }
}

module.exports = PaymentBlockedScreen;
