const Screen = require('../../screen');

class StandingOrderSetupSuccessScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.viewStandingOrdersButton = undefined;
    }
}
module.exports = StandingOrderSetupSuccessScreen;
