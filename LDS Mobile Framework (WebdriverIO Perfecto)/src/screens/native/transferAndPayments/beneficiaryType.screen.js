const Screen = require('../../screen');

class BeneficiaryTypeScreen extends Screen {
    constructor() {
        super();
        this.ukAccount = undefined;
        this.internationalBankAccount = undefined;
        this.ukMobileNumber = undefined;
        this.payingUkMobileNumberLink = undefined;
    }
}

module.exports = BeneficiaryTypeScreen;
