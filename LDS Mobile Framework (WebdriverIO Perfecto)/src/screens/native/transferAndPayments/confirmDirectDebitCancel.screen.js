const Screen = require('../../screen');

class CancelDirectDebitScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.cancelPassword = undefined;
        this.confirmCancelDirectDebit = undefined;
    }
}

module.exports = CancelDirectDebitScreen;
