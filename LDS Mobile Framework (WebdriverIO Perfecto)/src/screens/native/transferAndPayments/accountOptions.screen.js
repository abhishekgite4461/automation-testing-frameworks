const Screen = require('../../screen');

class AccountOptionsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.oneOfYourAccountsTab = undefined;
    }
}

module.exports = AccountOptionsScreen;
