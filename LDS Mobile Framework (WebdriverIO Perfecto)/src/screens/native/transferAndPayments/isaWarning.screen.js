const Screen = require('../../screen');

class IsaWarningScreen extends Screen {
    constructor() {
        super();
        this.renewWarningMessage = undefined;
    }
}

module.exports = IsaWarningScreen;
