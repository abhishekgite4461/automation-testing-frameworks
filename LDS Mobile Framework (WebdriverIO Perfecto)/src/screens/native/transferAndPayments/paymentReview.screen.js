const Screen = require('../../screen');

class PaymentReviewScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.confirmButton = undefined;
        this.cancelButton = undefined;
    }
}

module.exports = PaymentReviewScreen;
