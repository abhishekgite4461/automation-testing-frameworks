const Screen = require('../../screen');

class SetupStandingOrderScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.selectAccountDropdown = undefined;
        this.selectAccountFromList = undefined;
        this.standingOrderReferenceText = undefined;
        this.standingOrderAmount = undefined;
        this.selectFrequencyDropdown = undefined;
        this.standingOrderFrequency = undefined;
        this.continueButton = undefined;
        this.transactionLimitExceeded = undefined;
    }
}

module.exports = SetupStandingOrderScreen;
