const Screen = require('../../screen');

class DirectDebitNonCancellableScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.nonCancellableMessage = undefined;
    }
}
module.exports = DirectDebitNonCancellableScreen;
