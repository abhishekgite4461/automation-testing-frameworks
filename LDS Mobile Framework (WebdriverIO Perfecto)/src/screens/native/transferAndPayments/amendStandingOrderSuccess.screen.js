const Screen = require('../../screen');

class AmendStandingOrderSuccessScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.viewStandingOrders = undefined;
    }
}
module.exports = AmendStandingOrderSuccessScreen;
