const Screen = require('../../screen');

class RateAppPaymentHubScreen extends Screen {
    constructor() {
        super();
        this.rateApp = undefined;
        this.closeRateApp = undefined;
    }
}

module.exports = RateAppPaymentHubScreen;
