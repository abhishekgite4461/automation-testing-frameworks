const Screen = require('../../screen');

class PaymentSuccessScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.paymentSuccess = undefined;
        this.paymentProcessed = undefined;
        this.bannerLeadPlacement = undefined;
    }
}

module.exports = PaymentSuccessScreen;
