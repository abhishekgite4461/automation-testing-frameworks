const Screen = require('../../screen');

class ConfirmStandingOrderAmendments extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.confirmAmendPassword = undefined;
        this.confirmAmendStandingOrder = undefined;
    }
}
module.exports = ConfirmStandingOrderAmendments;
