const Screen = require('../../screen');

class NoEligibleAccountsErrorScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.homeButon = undefined;
        this.applyForAccountsButton = undefined;
        this.closeButton = undefined;
    }
}

module.exports = NoEligibleAccountsErrorScreen;
