const Screen = require('../../screen');

class StandingOrderScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.setupStandingOrderButton = undefined;
        this.amendButton = undefined;
        this.deleteButton = undefined;
        this.deleteSuccess = undefined;
        this.newStandingOrder = undefined;
        this.nonCancellableSoMessage = undefined;
        this.amount = undefined;
        this.standingOrderSwitch = undefined;
        this.paymentHubContinueButton = undefined;
        this.standingOrder = undefined;
    }
}

module.exports = StandingOrderScreen;
