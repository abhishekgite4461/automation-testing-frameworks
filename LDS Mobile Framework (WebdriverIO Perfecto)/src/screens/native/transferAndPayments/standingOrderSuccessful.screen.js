const Screen = require('../../screen');

class StandingOrderSuccessfulScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.standingOrderSuccessHeader = undefined;
        this.viewStandingOrdersButton = undefined;
        this.soNotSetupErrorMessage = undefined;
    }
}

module.exports = StandingOrderSuccessfulScreen;
