const Screen = require('../../screen');

class UkCompanySearchResultsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.paymentHubAddUkAccountSelectionPayeeNameValue = undefined;
        this.paymentHubAddUkAccountSelectionSortCodeValue = undefined;
        this.paymentHubAddUkAccountSelectionAccountNumberValue = undefined;
        this.paymentHubAddUkAccountSelectionCompanyList = undefined;
        this.paymentHubAddUkAccountReviewReferenceInput = undefined;
        this.paymentHubAddUkAccountReviewReferenceConfirmInput = undefined;
        this.paymentHubAddUkAccountReviewContinueButton = undefined;
        this.paymentHubAddUkAccountReviewCancelButton = undefined;
        this.paymentHubAddUkAccountSelectionTitle = undefined;
        this.paymentHubAddUkAccountReviewPayeeNameValue = undefined;
        this.paymentHubAddUkAccountReviewSortCodeValue = undefined;
        this.paymentHubAddUkAccountReviewAccountNumberValue = undefined;
    }
}

module.exports = UkCompanySearchResultsScreen;
