const Screen = require('../../screen');

class TransferReviewScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.confirmButton = undefined;
        this.cancelButton = undefined;
    }
}

module.exports = TransferReviewScreen;
