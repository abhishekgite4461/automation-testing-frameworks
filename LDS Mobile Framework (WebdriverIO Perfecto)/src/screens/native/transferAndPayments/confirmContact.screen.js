const Screen = require('../../screen');

class ConfirmContactScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.nameAndPhoneNumberInConfirm = undefined;
        this.confirmContinueButton = undefined;
        this.confirmCancelButton = undefined;
    }
}

module.exports = ConfirmContactScreen;
