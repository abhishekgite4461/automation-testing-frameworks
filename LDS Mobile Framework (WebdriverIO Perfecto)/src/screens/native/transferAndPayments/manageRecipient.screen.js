const Screen = require('../../screen');

class ManageRecipientScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.makePaymentButton = undefined;
        this.deleteRecipientButton = undefined;
        this.closeButton = undefined;
        this.actionMenuHeader = undefined;
        this.dialogPositiveAction = undefined;
        this.dialogNegativeAction = undefined;
        this.viewPendingPayment = undefined;
        this.dialogueBoxTitle = undefined;
        this.standingOrderDialogBox = undefined;
        this.viewButton = undefined;
        this.closeButtonStandingOrder = undefined;
    }
}

module.exports = ManageRecipientScreen;
