const Screen = require('../../screen');

class TransferSuccessScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.viewTransactionButton = undefined;
        this.transferPaymentButton = undefined;
        this.bannerLeadPlacement = undefined;
    }
}

module.exports = TransferSuccessScreen;
