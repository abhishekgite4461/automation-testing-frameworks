const Screen = require('../../screen');

class PaymentUnsuccessfulScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.paymentUnsuccessful = undefined;
        this.bannerLeadPlacement = undefined;
    }
}

module.exports = PaymentUnsuccessfulScreen;
