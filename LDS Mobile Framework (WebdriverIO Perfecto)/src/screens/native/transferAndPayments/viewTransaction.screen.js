const Screen = require('../../screen');

class ViewTransactionScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.remainingAllowanceAmount = undefined;
    }
}

module.exports = ViewTransactionScreen;
