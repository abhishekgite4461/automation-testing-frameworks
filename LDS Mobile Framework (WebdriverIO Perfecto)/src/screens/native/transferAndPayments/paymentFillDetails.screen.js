const Screen = require('../../screen');

class PaymentFillDetailsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.payeeName = undefined;
        this.payeeAccountNumber = undefined;
        this.referenceField = undefined;
        this.amount = undefined;
        this.continueButton = undefined;
        this.contactButton = undefined;
        this.mobileNumber = undefined;
    }
}

module.exports = PaymentFillDetailsScreen;
