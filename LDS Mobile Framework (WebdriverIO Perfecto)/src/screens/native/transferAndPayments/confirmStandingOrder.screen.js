const Screen = require('../../screen');

class ConfirmStandingOrderScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.okButton = undefined;
        this.confirmButton = undefined;
    }
}
module.exports = ConfirmStandingOrderScreen;
