const Screen = require('../../screen');

class TransferUnsuccessfulScreen extends Screen {
    constructor() {
        super();
        this.bannerLeadPlacement = undefined;
    }
}

module.exports = TransferUnsuccessfulScreen;
