const Screen = require('../../screen');

class ConfirmationOfPayeeScreen extends Screen {
    constructor() {
        super();
        this.paymentHubCopNotMatchTitle = undefined;
        this.paymentHubCopBAMMMessage = undefined;
        this.paymentHubCopBAMMNamePlayBack = undefined;
        this.paymentHubCopNotMatchEdit = undefined;
        this.paymentHubCopNotMatchContinue = undefined;
        this.businessNameMatch = undefined;
        this.personalAccountMatch = undefined;
        this.paymentHubCopNotMatchTitle = undefined;
        this.partialNameMatch = undefined;
        this.serviceError = undefined;
        this.paymentHubCopANNMMessageOne = undefined;
        this.paymentHubCopANNMMessageTwo = undefined;
        this.paymentHubCopNotMatchShareDetailsMessage = undefined;
        this.paymentHubCopNotMatchShareDetailsButton = undefined;
    }
}

module.exports = ConfirmationOfPayeeScreen;
