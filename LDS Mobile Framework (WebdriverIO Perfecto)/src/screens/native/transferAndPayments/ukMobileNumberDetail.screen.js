const Screen = require('../../screen');

class UkMobileNumberDetailScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.beneficiaryTileIcon = undefined;
        this.mobileNumber = undefined;
        this.amount = undefined;
        this.reference = undefined;
        this.continueButtonInPaymentHubPage = undefined;
        this.continue = undefined;
        this.addBeneficiaryP2pReviewContinueButton = undefined;
        this.addBeneficiaryP2pReviewCancelButton = undefined;
        this.transferSuccessHeader = undefined;
        this.anotherTransferButton = undefined;
        this.viewTransactionButton = undefined;
        this.errorMessageForAddPaym = undefined;
        this.paymentHubReviewTitle = undefined;
        this.editPaymentReviewButton = undefined;
        this.toFieldValue = undefined;
        this.verifyingaddPayMPage = undefined;
    }
}

module.exports = UkMobileNumberDetailScreen;
