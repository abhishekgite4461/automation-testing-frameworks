const Screen = require('../../screen');

class searchRecipientScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.searchIcon = undefined;
        this.addNewPayee = undefined;
        this.addNewPayeeFooter = undefined;
        this.cancelSearch = undefined;
        this.clearSearch = undefined;
        this.partialAddNewPayeeFooterText = undefined;
        this.searchEditText = undefined;
        this.firstRecipientFromList = undefined;
        this.noResultMessage = undefined;
        this.yourAccountsGroupHeader = undefined;
        this.recipientCell = undefined;
        this.accountResultByName = undefined;
        this.benificiaryResultByName = undefined;
        this.deleteRecipient = undefined;
        this.deleteConfirm = undefined;
        this.firstRecipientFromListManageRecipient = undefined;
        this.searchResult = undefined;
        this.secondRecipientFromListManageRecipient = undefined;
        this.searchedBusinessAccount = undefined;
    }
}

module.exports = searchRecipientScreen;
