const Screen = require('../../screen');

class DeleteStandingOrderScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.confirmDeletePassword = undefined;
        this.confirmDeleteButton = undefined;
    }
}
module.exports = DeleteStandingOrderScreen;
