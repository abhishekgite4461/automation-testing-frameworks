const Screen = require('../screen');

class EnrolmentGracePeriodScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.continueButton = undefined;
    }
}

module.exports = EnrolmentGracePeriodScreen;
