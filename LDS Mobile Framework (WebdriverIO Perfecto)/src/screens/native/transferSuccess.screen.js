const Screen = require('../screen');

class TransferSuccessScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.amount = undefined;
    }
}
module.exports = TransferSuccessScreen;
