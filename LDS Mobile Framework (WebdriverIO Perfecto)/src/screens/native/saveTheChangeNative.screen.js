const Screen = require('../screen');

class SaveTheChangeNativeScreen extends Screen {
    constructor() {
        super();
        this.infoPageNavigator = undefined;
        this.setUpSTCOption = undefined;
        this.alreadyRegistered = undefined;
        this.openNewSavingAccount = undefined;
        this.stcSavingAccount = undefined;
        this.acceptTC = undefined;
        this.turnONSTC = undefined;
        this.stcSuccess = undefined;
        this.backToAccounts = undefined;
        this.saveTheChangeTurnOffButton = undefined;
    }
}

module.exports = SaveTheChangeNativeScreen;
