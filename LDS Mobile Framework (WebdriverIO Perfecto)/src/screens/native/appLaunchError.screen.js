const Screen = require('../screen');

class appLaunchErrorScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.technicalError = undefined;
        this.internetConnectionError = undefined;
    }
}

module.exports = appLaunchErrorScreen;
