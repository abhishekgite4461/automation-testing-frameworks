const Screen = require('../../screen');

class YourProfileScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.salesMessageToggleButtonON = undefined;
        this.salesMessageToggleButtonOFF = undefined;
        this.emailUpdateToggleButtonON = undefined;
        this.emailUpdateToggleButtonOFF = undefined;
        this.smsUpdateToggleButtonON = undefined;
        this.smsUpdateToggleButtonOFF = undefined;
        this.salesMessageToggleButton = undefined;
        this.smsUpdateToggleButton = undefined;
        this.emailUpdateToggleButton = undefined;
        this.okayButton = undefined;
    }
}
module.exports = YourProfileScreen;
