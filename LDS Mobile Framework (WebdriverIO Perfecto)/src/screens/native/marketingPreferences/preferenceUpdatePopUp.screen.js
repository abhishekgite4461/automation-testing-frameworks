const Screen = require('../../screen');

class PreferenceUpdatePopUpScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.okButton = undefined;
    }
}
module.exports = PreferenceUpdatePopUpScreen;
