const Screen = require('../screen');

class ContactScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.contactRecipient = undefined;
    }
}

module.exports = ContactScreen;
