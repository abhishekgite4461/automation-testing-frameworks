const Screen = require('../screen');

class SpendingRewardsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.optInEverydayOfferButton = undefined;
        this.noOptInEverydayOfferButton = undefined;
        this.activeOffers = undefined;
        this.NewOffers = undefined;
        this.expiredOffers = undefined;
        this.settings = undefined;
        this.findOutMore = undefined;
        this.newTab = undefined;
        this.activeTab = undefined;
        this.selectEverydayOffersLink = undefined;
        this.expiredOffersPageTitle = undefined;
        this.settingPageTitle = undefined;
        this.findOutMorePageTitle = undefined;
    }
}

module.exports = SpendingRewardsScreen;
