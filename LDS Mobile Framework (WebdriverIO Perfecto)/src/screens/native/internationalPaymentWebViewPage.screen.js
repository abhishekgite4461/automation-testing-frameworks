const Screen = require('../screen');

class InternationalPaymentWebViewPageScreen extends Screen {
    constructor() {
        super();
        this.IPtitle = undefined;
    }
}

module.exports = InternationalPaymentWebViewPageScreen;
