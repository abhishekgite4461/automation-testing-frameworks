const Screen = require('../screen');

class RenameAccountScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.renameButton = undefined;
        this.textField = undefined;
        this.confirmationBox = undefined;
        this.backToYourAccountsButton = undefined;
        this.ghostAccountName = undefined;
    }
}

module.exports = RenameAccountScreen;
