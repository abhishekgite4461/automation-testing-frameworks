const Screen = require('../screen');

class ReEnterPasswordScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.reEnterPasswordField = undefined;
        this.passwordField = undefined;
        this.fscsTile = undefined;
        this.cmsTile = undefined;
        this.continueButton = undefined;
    }
}

module.exports = ReEnterPasswordScreen;
