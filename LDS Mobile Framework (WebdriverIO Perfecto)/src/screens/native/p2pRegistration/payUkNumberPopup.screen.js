const Screen = require('../../screen');

class PayUkNumberPopupScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.close = undefined;
        this.registerNow = undefined;
    }
}

module.exports = PayUkNumberPopupScreen;
