const Screen = require('../../screen');

class PayContactRegistrationScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.registerButton = undefined;
        this.deRegisterButton = undefined;
        this.deRegisterSuccessMessage = undefined;
    }
}

module.exports = PayContactRegistrationScreen;
