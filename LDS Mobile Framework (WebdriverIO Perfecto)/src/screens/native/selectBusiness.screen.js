const Screen = require('../screen');

class SelectBusinessScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.business = undefined;
    }
}

module.exports = SelectBusinessScreen;
