const Screen = require('../screen');

class OnlinePaperPreferenceWebViewSuccessScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
    }
}

module.exports = OnlinePaperPreferenceWebViewSuccessScreen;
