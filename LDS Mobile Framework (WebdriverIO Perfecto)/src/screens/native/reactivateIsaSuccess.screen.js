const Screen = require('../screen');

class ReactivateIsaSuccessScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.reactivateIsaSuccessHeader = undefined;
        this.reactivateIsaSupportingCopy = undefined;
        this.reactivateIsaAccountNameText = undefined;
        this.reactivateIsaRemainingAllowanceText = undefined;
        this.reactivateIsaRemainingAllowanceAmount = undefined;
        this.reactivateIsaAddMoreMoneyButton = undefined;
        this.reactivateIsaToVerifyExternalTransferButton = undefined;
        this.reactivateIsaAccountSortCode = undefined;
        this.reactivateIsaAccountNameText = undefined;
        this.reactivateIsaInstructionalMessage = undefined;
    }
}

module.exports = ReactivateIsaSuccessScreen;
