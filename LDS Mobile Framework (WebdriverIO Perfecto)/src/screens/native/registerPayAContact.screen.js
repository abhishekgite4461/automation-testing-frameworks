const Screen = require('../screen');

class RegisterPayAContactWebViewPageScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.registerButton = undefined;
        this.continueButton = undefined;
        this.agreeCheckBox = undefined;
        this.continuetoP2PButton = undefined;
        this.continueToP2PAuthentication = undefined;
        this.P2PsuccessMessage = undefined;
        this.lnkP2PSetting = undefined;
        this.lkDeRegisterFromP2P = undefined;
        this.authPassword = undefined;
        this.continueToDereg = undefined;
        this.deregSuceesMsg = undefined;
    }
}

module.exports = RegisterPayAContactWebViewPageScreen;
