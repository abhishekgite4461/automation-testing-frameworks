const Screen = require('../screen');

class YourProfileScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.toggleButtonOne = undefined;
        this.toggleButtonTwo = undefined;
        this.toggleButtonThree = undefined;
    }
}

module.exports = YourProfileScreen;
