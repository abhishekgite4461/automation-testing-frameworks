const Screen = require('../screen');

class UpComingPaymentScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.upComingPayment = undefined;
    }
}

module.exports = UpComingPaymentScreen;
