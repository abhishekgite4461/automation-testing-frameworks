const Screen = require('../../screen');

class PayCCbyMC extends Screen {
    constructor() {
        super();
        this.anotherUkBankAccount = undefined;
        this.dismissErrorBanner = undefined;
        this.selectProviderButton = undefined;
    }
}

module.exports = PayCCbyMC;
