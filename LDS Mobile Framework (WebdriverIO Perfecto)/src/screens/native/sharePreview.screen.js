const Screen = require('../screen');

class SharePreviewScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.paymentSentHeader = undefined;
        this.sharePreviewShareButton = undefined;
        this.nativeSharePanel = undefined;
        this.payerName = undefined;
        this.nameAndNumberPreview = undefined;
        this.beneficiaryAccountNumber = undefined;
        this.bankIcon = undefined;
        this.paymentReceiptDateValue = undefined;
        this.paymentReceiptDateField = undefined;
        this.emailAppIcon = undefined;
        this.displayAccountDetails = undefined;
        this.beneficiaryName = undefined;
        this.beneficiarySortCode = undefined;
        this.referenceValue = undefined;
        this.amountValue = undefined;
    }
}

module.exports = SharePreviewScreen;
