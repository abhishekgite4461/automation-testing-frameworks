const Screen = require('../screen');

class RequestOtpConfirmationScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.goToIbButton = undefined;
    }
}

module.exports = RequestOtpConfirmationScreen;
