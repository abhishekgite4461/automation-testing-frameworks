const Screen = require('../../screen');

class GoogleMapScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.backButton = undefined;
    }
}

module.exports = GoogleMapScreen;
