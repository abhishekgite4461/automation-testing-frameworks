const Screen = require('../../screen');

class DetailedMapViewScreen extends Screen {
    constructor() {
        super();
        this.getDirectionButton = undefined;
        this.backButton = undefined;
    }
}

module.exports = DetailedMapViewScreen;
