const Screen = require('../../screen');

class AtmBranchFilterScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.openNow = undefined;
        this.openWeekends = undefined;
        this.okButton = undefined;
    }
}

module.exports = AtmBranchFilterScreen;
