const Screen = require('../../screen');

class AtmBranchMapViewScreen extends Screen {
    constructor() {
        super();
        this.firstMapView = undefined;
    }
}

module.exports = AtmBranchMapViewScreen;
