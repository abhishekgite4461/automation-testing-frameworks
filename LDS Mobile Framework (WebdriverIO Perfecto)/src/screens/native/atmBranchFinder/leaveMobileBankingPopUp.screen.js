const Screen = require('../../screen');

class LeaveMobileBankingPopUpScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.okButton = undefined;
    }
}

module.exports = LeaveMobileBankingPopUpScreen;
