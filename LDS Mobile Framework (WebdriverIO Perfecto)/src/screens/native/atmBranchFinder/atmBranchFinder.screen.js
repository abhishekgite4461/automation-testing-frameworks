const Screen = require('../../screen');

class AtmBranchFinderScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.searchBranchesTile = undefined;
        this.findNearbyAtmTile = undefined;
        this.noteMessage = undefined;
    }
}

module.exports = AtmBranchFinderScreen;
