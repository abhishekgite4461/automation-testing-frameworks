const Screen = require('../../screen');

class AtmBranchScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.searchBranch = undefined;
        this.searchBranchLocation = undefined;
        this.branchButton = undefined;
        this.atmButton = undefined;
        this.bothButton = undefined;
        this.listViewButton = undefined;
        this.filterButton = undefined;
        this.currentLocation = undefined;
        this.mapViewButton = undefined;
        this.atmBranchInMap = undefined;
        this.atmInfoIcon = undefined;
    }
}

module.exports = AtmBranchScreen;
