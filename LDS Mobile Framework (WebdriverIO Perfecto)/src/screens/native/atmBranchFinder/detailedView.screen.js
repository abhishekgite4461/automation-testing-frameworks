const Screen = require('../../screen');

class AtmBranchDetailedViewScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.icon = undefined;
        this.mapAddress = undefined;
        this.openingHours = undefined;
        this.facilities = undefined;
        this.facilitiesTitle = undefined;
        this.getDirectionButton = undefined;
        this.backButton = undefined;
    }
}

module.exports = AtmBranchDetailedViewScreen;
