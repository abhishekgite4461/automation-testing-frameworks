/**
 * XCUITest is used on iOS 10.X.X and iOS 11.X.X devices
 * TODO MOB3-8001 remove UI Automation support for iOS 9.X.X devices
 */
module.exports = {
    byName: (arg) => `~${arg}`,
    byLabel: (arg) => `~${arg}`,
    byValue: (value) => `//*[@value="${value}"]`,
    byPartialLabel: (label) => `//*[contains(@label, "${label}")]`,
    byVisibleLabel: (label) => `//*[@label="${label}" and @visible="true"]`,
    byVisibleName: (arg) => `//*[@name="${arg}" and @hittable="true"]`,
    byPartialName: (name) => `//*[contains(@name, "${name}")]`,
    byPartialValue: (name) => `//*[contains(@value, "${name}")]`,
    byTypeAndIndex: (type, index) => `(//*[@type="${type}"])[${index}]`,
    byNameAndIndex: (name, index) => `(//*[@name="${name}"])[${index}]`,
    byType: (type) => `//*[@type="${type}"]`,
    byEnabledName: (arg) => `//*[@name="${arg}" and @enabled="true"]`
};
