const Screen = require('../../screen');

class WhitelistedBiller extends Screen {
    constructor() {
        super();
        this.lloydsBiller = undefined;
    }
}

module.exports = WhitelistedBiller;
