const Screen = require('../screen');

class EnrolmentSuccessScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.continueButton = undefined;
    }
}

module.exports = EnrolmentSuccessScreen;
