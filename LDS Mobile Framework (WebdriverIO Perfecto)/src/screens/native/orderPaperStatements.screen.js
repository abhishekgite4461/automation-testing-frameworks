const Screen = require('../screen');

class OrderPaperStatementsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.firstStatement = undefined;
        this.continue = undefined;
        this.titleConfirm = undefined;
        this.total = undefined;
        this.changeOrder = undefined;
        this.updateAddress = undefined;
        this.deliverAddress = undefined;
        this.confirmAndPay = undefined;
        this.chooseAccount = undefined;
        this.accountName = undefined;
    }
}

module.exports = OrderPaperStatementsScreen;
