const Screen = require('../screen');

class Calendar extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.paymentDate = undefined;
        this.nextMonthButton = undefined;
        this.confirmDate = undefined;
        this.cancel = undefined;
    }
}

module.exports = Calendar;
