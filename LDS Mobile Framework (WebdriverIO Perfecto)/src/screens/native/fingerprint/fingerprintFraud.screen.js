const Screen = require('../../screen');

class FingerprintFraudScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.yesButton = undefined;
        this.noButton = undefined;
    }
}

module.exports = FingerprintFraudScreen;
