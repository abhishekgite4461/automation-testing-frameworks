const Screen = require('../../screen');

class FingerprintLoginScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.cancel = undefined;
    }
}

module.exports = FingerprintLoginScreen;
