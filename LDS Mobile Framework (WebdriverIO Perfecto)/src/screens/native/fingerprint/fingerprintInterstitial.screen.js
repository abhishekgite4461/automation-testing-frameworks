const Screen = require('../../screen');

class FingerprintInterstitialScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.yesButton = undefined;
        this.noButton = undefined;
    }
}

module.exports = FingerprintInterstitialScreen;
