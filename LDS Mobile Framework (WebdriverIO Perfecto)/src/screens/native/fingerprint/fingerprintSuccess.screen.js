const Screen = require('../../screen');

class FingerprintSuccessScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.okButton = undefined;
    }
}

module.exports = FingerprintSuccessScreen;
