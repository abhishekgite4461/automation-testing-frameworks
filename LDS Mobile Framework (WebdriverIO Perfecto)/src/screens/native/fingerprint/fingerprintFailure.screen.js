const Screen = require('../../screen');

class FingerprintFailureScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.okButton = undefined;
    }
}

module.exports = FingerprintFailureScreen;
