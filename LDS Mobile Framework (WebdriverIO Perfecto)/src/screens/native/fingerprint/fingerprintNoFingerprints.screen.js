const Screen = require('../../screen');

class FingerprintNoFingerprintsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.okButton = undefined;
    }
}

module.exports = FingerprintNoFingerprintsScreen;
