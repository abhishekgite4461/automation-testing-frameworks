const Screen = require('../screen');

class AppWarnScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.updateAppButton = undefined;
        this.continueWithoutUpdating = undefined;
    }
}

module.exports = AppWarnScreen;
