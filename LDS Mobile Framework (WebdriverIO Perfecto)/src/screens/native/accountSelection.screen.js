const Screen = require('../screen');

class AccountSelection extends Screen {
    constructor() {
        super();
        this.accountSelectionAccountName = undefined;
        this.accountSelectionAccountNumber = undefined;
        this.accountSelectionSortCode = undefined;
        this.accountSelectionAccountBalance = undefined;
        this.currentAccountWarningMessage = undefined;
    }
}

module.exports = AccountSelection;
