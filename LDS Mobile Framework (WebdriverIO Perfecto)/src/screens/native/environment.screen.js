const Screen = require('../screen');

class EnvironmentScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.server = undefined;
        this.resetFeatureConfigButton = undefined;
        // MQE-1075 TODO
        this.oldResetFeatureConfigButton = undefined;
        this.okButton = undefined;
        this.quickStubButton = undefined;
        this.quickStubText = undefined;
        this.unfinishedFeaturesButton = undefined;
        this.deenrolledErrorMessage = undefined;
        this.deenrolledContinueButton = undefined;
        this.inputAppVersion = undefined;
    }
}

module.exports = EnvironmentScreen;
