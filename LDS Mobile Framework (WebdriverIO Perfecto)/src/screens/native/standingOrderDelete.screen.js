const Screen = require('../screen');

class StandingOrder extends Screen {
    constructor() {
        super();
        this.soDeleteTitle = undefined;
        this.soDeletePassword = undefined;
        this.soDeleteSOButton = undefined;
        this.StandingOrderDeleteScreen = undefined;
        this.soDeleteSOButtonBnga = undefined;
        this.soDeleteMessageTitleBnga = undefined;
    }
}

module.exports = StandingOrder;
