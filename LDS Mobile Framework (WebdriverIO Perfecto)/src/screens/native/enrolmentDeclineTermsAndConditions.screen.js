const Screen = require('../screen');

class EnrolmentDeclineTermsAndConditionsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.confirmDeclineButton = undefined;
        this.backButton = undefined;
    }
}

module.exports = EnrolmentDeclineTermsAndConditionsScreen;
