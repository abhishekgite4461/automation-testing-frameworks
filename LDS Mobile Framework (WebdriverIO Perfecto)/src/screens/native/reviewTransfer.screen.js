const Screen = require('../screen');

class ReviewTransferScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.editButton = undefined;
        this.confirmButton = undefined;
        this.fromDetail = undefined;
        this.toDetail = undefined;
        this.amountDetail = undefined;
        this.reference = undefined;
        this.htbIsaWarningMessage = undefined;
        this.topupIsaWarningMessage = undefined;
        this.withdrawFromIsaToNonIsaAccountWarningMessage = undefined;
        this.withdrawFromIsaToIsaAccountWarningMessage = undefined;
        this.mpaTransferApprovalRequiredMessage = undefined;
    }
}

module.exports = ReviewTransferScreen;
