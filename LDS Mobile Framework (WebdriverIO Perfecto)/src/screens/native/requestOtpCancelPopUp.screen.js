const Screen = require('../screen');

class RequestOtpCancelPopUpScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.confirmButton = undefined;
        this.cancelButton = undefined;
    }
}

module.exports = RequestOtpCancelPopUpScreen;
