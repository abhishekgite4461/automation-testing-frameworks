const Screen = require('../screen');

class MemorableInformationScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.iBRegMiTitle = undefined;
        this.memorableCharacter = undefined;
        this.firstMemorableCharacter = undefined;
        this.fscsTitle = undefined;
        this.miForgotYourPassword = undefined;
        this.cmsTile = undefined;
        this.firstOrdinal = undefined;
        this.secondOrdinal = undefined;
        this.thirdOrdinal = undefined;
        this.miTextboxes = undefined;
        this.miCharacters = undefined;
        this.continueToMyAccounts = undefined;
        this.touchButton = undefined;
        this.dataConsent = undefined;
        this.acceptAll = undefined;
        this.tooltipLink = undefined;
        this.tooltipPopup = undefined;
        this.expressLogonUnavailableTitle = undefined;
        this.expressLogonUnavailableOKButon = undefined;
        this.manageDataConsent = undefined;
        this.manageDataConsentYesButton = undefined;
        this.manageDataConsentConfirmButton = undefined;
        this.keepUsingMI = undefined;
    }
}

module.exports = MemorableInformationScreen;
