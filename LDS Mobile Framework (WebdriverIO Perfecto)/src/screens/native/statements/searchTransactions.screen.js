const Screen = require('../../screen');

class SearchTransactionsScreen extends Screen {
    constructor() {
        super();
        this.searchHeader = undefined;
        this.searchDialogBox = undefined;
        this.searchInstruction = undefined;
        this.clearSearch = undefined;
        this.searchResultsTransactionsList = undefined;
        this.searchFurtherBack = undefined;
        this.noMoreTransactionsMessage = undefined;
        this.noTransactionsToSearch = undefined;
        this.searchResultsCount = undefined;
        this.transactionListEmpty = undefined;
        this.backButton = undefined;
        this.searchInfo = undefined;
        this.ZeroResultsDateRangeMessage = undefined;
    }
}

module.exports = SearchTransactionsScreen;
