const Screen = require('../../screen');

class MonthlyPdfScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.monthSelectionCloseButton = undefined;
        this.monthSelectionViewButton = undefined;
        this.pdfPreviewScreen = undefined;
        this.allowPdf = undefined;
        this.digitalInboxScreen = undefined;
        this.monthField = undefined;
        this.yearField = undefined;
        this.pdfViewer = undefined;
        this.inboxBackButton = undefined;
    }
}

module.exports = MonthlyPdfScreen;
