const Screen = require('../../screen');

class AboutPendingTransactionsOverlayScreen extends Screen {
    constructor() {
        super();
        this.alertTitle = undefined;
        this.aboutPendingTransactionsDescription = undefined;
        this.closeAlert = undefined;
    }
}

module.exports = AboutPendingTransactionsOverlayScreen;
