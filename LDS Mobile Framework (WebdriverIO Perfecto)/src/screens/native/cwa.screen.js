const Screen = require('../screen');

class CwaScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.backToApp = undefined;
    }
}

module.exports = CwaScreen;
