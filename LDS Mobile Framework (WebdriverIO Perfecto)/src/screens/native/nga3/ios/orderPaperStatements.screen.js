const BaseOrderPaperStatementsPageScreen = require('../../orderPaperStatements.screen');
const helper = require('../../ios.helper');

class OrderPaperStatementsPageScreen extends BaseOrderPaperStatementsPageScreen {
    constructor() {
        super();
        this.title = helper.byName('Order paper statements');
        this.titleConfirm = helper.byName('Confirm your order');
        this.firstStatement = '(//XCUIElementTypeStaticText[@name="STATEMENT"])[1]';
        this.continue = helper.byName('Continue');
        this.total = helper.byName('£');
        this.changeOrder = helper.byName('Change order');
        this.updateAddress = helper.byName('How to update your address');
        this.deliverAddress = helper.byName('EC1Y 4XX');
        this.confirmAndPay = helper.byName('Confirm Order & Pay');
        this.chooseAccount = helper.byName('Choose an account');
        this.accountName = (accName) => `//*[@name="${accName}"]`;
    }
}

module.exports = OrderPaperStatementsPageScreen;
