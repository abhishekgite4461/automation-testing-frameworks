const BaseLoanAnnualStatement = require('../../loanAnnualStatement.screen');
const helper = require('../../ios.helper');

class LoanAnnualStatement extends BaseLoanAnnualStatement {
    constructor() {
        super();
        this.title = helper.byName('Annual Statements');
    }
}

module.exports = LoanAnnualStatement;
