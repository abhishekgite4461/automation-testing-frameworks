const BaseReplacementCardAndPin = require('../../replacementCardAndPin.screen.js');

class ReplacementCardAndPin extends BaseReplacementCardAndPin {
    constructor() {
        super();
        this.title = '//XCUIElementTypeStaticText[@name="Replacement cards and PINs" or contains(@label,"Card replacement")]';
    }
}

module.exports = ReplacementCardAndPin;
