const BaseSupportHubHomePageScreen = require('../../supportHubHomePage.screen');
const helper = require('../../ios.helper.js');

class SupportHubHomePageScreen extends BaseSupportHubHomePageScreen {
    constructor() {
        super();
        this.title = helper.byName('supportHubMainScreen');
        this.callUsButton = helper.byName('supportHubCallUs');
        this.mobileChatButton = helper.byName('supportHubChatToUs');
        this.changeOfAddress = helper.byName('changeOfAddressAction');
        this.lostOrStolenCard = helper.byName('lostOrStolenAction');
        this.replaceCardOrPin = helper.byName('replaceCardOrPinAction');
        this.resetPassword = helper.byName('resetPasswordAction');
        this.atmBranchFinder = helper.byName('supportHubAtmAndBranchFinder');
        this.appFeedback = helper.byName('supportHubProvideAppFeedback');
        this.cmaSurvey = helper.byName('supportHubCMASurvey');
        this.overLayTitle = helper.byName('Sorry');
        this.dismissOverLay = helper.byName('alertActionConfirmButton');
        this.carousals = '//XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeButton';
        this.updateDetailsButton = helper.byName('customerProfileButton');
        this.mortgagePaymentHolidaysLink = helper.byName('covidMortgageLink');
        this.creditcardPaymentHolidaysLink = helper.byName('creditPaymentLink');
        this.loanPaymentHolidaysLink = helper.byName('loanPaymentLink');
        this.overdraftChargesLink = helper.byName('overdraftChargesLink');
        this.travelAndDisruptionCoverLink = helper.byName('travelCoverLink');
        this.moreInfoAboutCoronavirusLink = helper.byLabel('More information about coronavirus');
        this.viewPinOrReplaceCard = helper.byName('viewPinOrReplaceCardAction');
        this.viewPinTile = helper.byName('View PIN');
    }
}

module.exports = SupportHubHomePageScreen;
