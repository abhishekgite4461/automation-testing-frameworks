const BaseMessageUsHomePageScreen = require('../../messageUsHomePage.screen');
const helper = require('../../ios.helper.js');

class MessageUsHomePageScreen extends BaseMessageUsHomePageScreen {
    constructor() {
        super();
        this.title = helper.byName('Need help? Message us.');
        this.errorMessage = helper.byName('Sorry');
        this.chatCloseButton = helper.byName('CrossBarButton');
        this.chatSendButton = helper.byName('Send');
        this.typeYourQuestionField = helper.byName('Type your question');
        this.chatMessageTimeStamp = helper.byName('Read');
        this.pushOptInHeader = helper.byName('pushOptInHeader');
        this.pushOptInDescription = helper.byName('pushOptInDescription');
        this.notNowButton = helper.byName('Denied');
        this.activateButton = helper.byName('pushOptInActiveButton');
    }
}

module.exports = MessageUsHomePageScreen;
