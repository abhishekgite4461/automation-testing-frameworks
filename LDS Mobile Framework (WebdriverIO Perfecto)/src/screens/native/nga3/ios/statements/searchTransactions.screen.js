const BaseSearchTransactionsScreen = require('../../../statements/searchTransactions.screen.js');
const helper = require('../../../ios.helper.js');

class SearchTransactionsScreen extends BaseSearchTransactionsScreen {
    constructor() {
        super();
        this.searchHeader = helper.byName('Search');
        this.searchDialogBox = helper.byName('searchBarOverlay');
        this.searchInstruction = helper.byName('Type a description or an amount');
        this.clearSearch = helper.byName('Clear text');
        this.searchResultsTransactionsList = helper.byName('transactionSearchResultsTableView');
        this.backButton = helper.byName('Back');
        this.searchResultsCount = helper.byName('searchInfoView');
        this.searchInfo = helper.byName('searchInfoView');
        this.transactionListEmpty = helper.byLabel('Empty list');
    }
}

module.exports = SearchTransactionsScreen;
