const BaseAboutPendingTransactionsOverlayScreen = require('../../../statements/aboutPendingTransactionsOverlay.screen.js');
const helper = require('../../../ios.helper');

class AboutPendingTransactionsOverlayScreen extends BaseAboutPendingTransactionsOverlayScreen {
    constructor() {
        super();
        this.alertTitle = helper.byName('aboutPendingTransactionsAlertTitle');
        this.aboutPendingTransactionsDescription = helper.byName('aboutPendingTransactionsAlertMessage');
        this.closeAlert = helper.byName('aboutPendingTransactionsAlertAction');
    }
}

module.exports = AboutPendingTransactionsOverlayScreen;
