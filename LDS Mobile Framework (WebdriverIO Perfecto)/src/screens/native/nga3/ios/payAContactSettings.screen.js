const BasePayAContactSettings = require('../../payAContactSettings.screen');
const helper = require('../../ios.helper');

class PayAContactSettings extends BasePayAContactSettings {
    constructor() {
        super();
        this.title = helper.byName('Pay a Contact settings');
    }
}

module.exports = PayAContactSettings;
