const BaseAppFeedbackScreen = require('../../appFeedback.screen');
const helper = require('../../ios.helper');

class AppFeedbackScreen extends BaseAppFeedbackScreen {
    constructor() {
        super();
        this.title = helper.byName('Feedback');
        this.feedbackForm = helper.byName('How would you rate the app?, This field is required');
        this.submitButton = helper.byName('Submit');
    }
}

module.exports = AppFeedbackScreen;
