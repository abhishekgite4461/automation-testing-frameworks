const BasePayCCbyMasterCardScreen = require('../../../payCreditCardbyMasterCard/payCreditCardbyMasterCard.screen');
const helper = require('../../../ios.helper');

class PayCCbyMasterCardScreen extends BasePayCCbyMasterCardScreen {
    constructor() {
        super();
        // TOD0 PJO-9060 add name locator
        this.anotherUkBankAccount = helper.byLabel('Another UK Bank Account');
        this.selectProviderButton = helper.byName('continueButton');
        this.editButton = helper.byName('reviewPaymentEditButton');
        this.confirmButton = helper.byName('reviewPaymentConfirmButton');
        this.dismissErrorBanner = helper.byName('closeIcon');
        this.selectProviderButton = helper.byName('continueButton');
        this.selectedBankName = (bankName) => `//*[@label="${bankName}" and @name="masterCardProviderName"]`;
    }
}

module.exports = PayCCbyMasterCardScreen;
