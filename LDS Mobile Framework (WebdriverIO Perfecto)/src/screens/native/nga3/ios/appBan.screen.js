const BaseAppBanScreen = require('../../appBan.screen');
const helper = require('../../ios.helper');

class AppBanScreen extends BaseAppBanScreen {
    constructor() {
        super();
        this.title = helper.byName('appBanTitle');
        this.goToOurMobileSiteButton = helper.byName('appBanIbButton');
        this.updateAppButton = helper.byName('appBanUpdateAppButton');
    }
}

module.exports = AppBanScreen;
