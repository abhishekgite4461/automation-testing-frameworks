const BaseAlertPopUpScreen = require('../../alertPopUp.screen');
const helper = require('../../ios.helper');

class AlertPopUpScreen extends BaseAlertPopUpScreen {
    constructor() {
        super();
        this.title = helper.byName('alertActionConfirmButton');
        this.confirmButton = helper.byName('alertActionConfirmButton');
        this.cancelButton = helper.byName('alertActionCancelButton');
    }
}

module.exports = AlertPopUpScreen;
