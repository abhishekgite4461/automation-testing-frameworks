const BaseEnrolmentTermsAndConditionsDeclinedError = require('../../enrolmentTermsAndConditionsDeclinedError.screen');
const helper = require('../../ios.helper');

class EnrolmentTermsAndConditionsDeclinedErrorScreen extends BaseEnrolmentTermsAndConditionsDeclinedError {
    constructor() {
        super();
        this.title = helper.byName('errorPageHeader');
        this.termsAndConditionsDeclinedError = helper.byPartialValue('Sorry, it seems you haven’t accepted our');
    }
}

module.exports = EnrolmentTermsAndConditionsDeclinedErrorScreen;
