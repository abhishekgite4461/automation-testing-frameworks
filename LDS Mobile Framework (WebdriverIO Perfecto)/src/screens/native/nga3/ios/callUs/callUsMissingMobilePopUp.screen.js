const helper = require('../../../ios.helper');
const BaseMissingMobilePopUpScreen = require('../../../callUs/callUsMissingMobilePopUp.screen');

class CallUsMissingMobilePopUpScreen extends BaseMissingMobilePopUpScreen {
    constructor() {
        super();
        this.title = helper.byName('callUsMissingNumberTitle');
        this.addPhoneNumber = helper.byName('callUsAddMobileNumberButton');
        this.continueCall = helper.byName('callUsContinueCallButton');
    }
}

module.exports = CallUsMissingMobilePopUpScreen;
