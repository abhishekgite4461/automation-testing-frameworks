const helper = require('../../../ios.helper');
const BaseSecureCallScreen = require('../../../callUs/callUs.screen.js');

class CallUsScreen extends BaseSecureCallScreen {
    constructor() {
        super();
        this.title = helper.byName('callUsScreenTitle');
        this.callUsButton = helper.byName('callUsButton');
        this.callTimeLabel = helper.byName('callUsOpeningHours');
        this.textPhoneLabel = helper.byName('callUsTextphoneContent');
        this.referenceNumber = helper.byName('arrangementReference');
        this.accountNumber = helper.byName('arrangementAccountNumber');
        this.sortCode = helper.byName('arrangementSortCode');
        this.lostOrStolenSelfServiceOption = helper.byName('CallUsLostOrStolenButton');
        this.lostOrStolenSelfServiceButton = helper.byName('CallUsLostOrStolenButton');
        this.resetYourPasswordSelfServiceButton = helper.byName('CallUsResetPasswordButton');
        this.replacementCardSelfServiceButton = helper.byName('CallUsReplacementCardButton');
        this.phoneNumber = helper.byName('callUsButton');
        this.phoneNumberText = '//*[contains(@name, "+44") or contains(@name,"03") or contains(@name,"08")]';
        this.cancelButton = helper.byName('Cancel');
        this.callButton = helper.byName('Call');
        this.backButton = '//XCUIElementTypeNavigationBar/XCUIElementTypeButton[1]';
        this.modalBackButton = '//*[contains(@name, "Back") or contains(@label,"Call us")]';
        this.closeButton = helper.byName('CrossBarButton');
        this.phoneNumberOnDialler = '//*[contains(@name, "+44") or contains(@name,"03") or contains(@name,"08")]';
        this.endCallButton = helper.byName('End call');
        this.networkPopup = helper.byName('OK');
        this.businessAccountsCallUsPage = helper.byName('Account query');
        this.internetBankingCallUsPage = helper.byName('Internet Banking');
        this.otherBankingQueryCallUsPage = helper.byName('Other query');
        this.suspectedFraudCallUsPage = helper.byName('Suspected fraud');
        this.lostOrStolenCardCallUsPage = helper.byName('Lost/stolen cards');
        this.emergencyCashAbroadCallUsPage = helper.byName('Emergency cash');
        this.medicalAssistanceAbroadCallUsPage = helper.byName('Medical assistance');
        this.accountCallUsPage = helper.byName('Account query');
    }
}

module.exports = CallUsScreen;
