const BaseMyAccountsScreen = require('../../../callUs/myAccounts.screen');
const helper = require('../../../ios.helper');

class MyAccountsScreen extends BaseMyAccountsScreen {
    constructor() {
        super();
        this.title = helper.byName('callUsExistingAccountsScreen');
        this.currentAccount = helper.byName('callUsArrangementCell-CurrentAccount');
        this.savingsAccount = helper.byName('callUsArrangementCell-SavingsAccount');
        this.creditAccount = helper.byName('callUsArrangementCell-CreditCardAccount');
        this.isaAccount = helper.byName('callUsArrangementCell-ISA');
        this.htbIsaAccount = helper.byName('callUsArrangementCell-ISA’');
        this.cbsLoanAccount = helper.byName('callUsArrangementCell-LoanAccount');
        this.loanAccount = helper.byName('callUsArrangementCell-LoanAccount');
        this.mortgageAccount = helper.byName('callUsArrangementCell-MortgageAccount');
        this.mortgageUfssAccount = helper.byName('callUsArrangementCell-MortgageAccount');
        this.termDepositAccount = helper.byName('termDepositAccount');
        this.investmentAccount = helper.byName('investmentAccount');
        this.homeInsurance = helper.byName('homeInsurance');
        this.treasureFtd = helper.byName('treasureFtd');
        this.treasure32Dcn = helper.byName('treasure32Dcn');
        this.accountNameLabel = (account) => `//*[contains(@label, "${account}") or 
            contains(@label, "${account.toUpperCase()}")]`;
    }
}

module.exports = MyAccountsScreen;
