const helper = require('../../../ios.helper');
const BaseChangeAddressCallUsScreen = require('../../../callUs/changeAddressCallUs.screen');

class ChangeAddressCallUsScreen extends BaseChangeAddressCallUsScreen {
    constructor() {
        super();
        this.legalInfo = helper.byName('callUsLegalInfo');
        this.callUsButton = helper.byName('callUsButton');
    }
}

module.exports = ChangeAddressCallUsScreen;
