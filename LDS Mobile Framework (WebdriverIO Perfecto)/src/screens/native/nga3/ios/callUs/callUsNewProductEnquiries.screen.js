const helper = require('../../../ios.helper');
const BaseNewProductEnquiries = require('../../../callUs/callUsNewProductEnquiries.screen');

class CallUsNewProductEnquiries extends BaseNewProductEnquiries {
    constructor() {
        super();
        this.title = helper.byName('New products');
        this.newCurrentAccount = helper.byName('CallUsNewCurrentAccountButton');
        this.newSavingsAccount = helper.byName('CallUsNewSavingsAccountButton');
        this.newIsaAccount = helper.byName('CallUsNewIsaAccountButton');
        this.newCreditAccount = helper.byName('CallUsNewCreditCardButton');
        this.newLoanAccount = helper.byName('CallUsNewLoanButton');
        this.newMortgagesAccount = helper.byName('CallUsNewMortgageButton');
    }
}

module.exports = CallUsNewProductEnquiries;
