const BaseReEnterPasswordScreen = require('../../reEnterPassword.screen');
const helper = require('../../ios.helper');

class ReEnterPasswordScreen extends BaseReEnterPasswordScreen {
    constructor() {
        super();
        this.title = helper.byLabel('Re-enter password'); // TODO MORE-680 use accessibility label
        this.continueButton = helper.byName('enterMiPasswordContinueButton');
        this.continueButton = helper.byName('reenterPasswordContinueButton');
        this.reEnterPasswordField = helper.byName('reenterPasswordInput');
        this.fscsTile = helper.byName('Financial Services Compensation Scheme notice. Protected');
    }
}

module.exports = ReEnterPasswordScreen;
