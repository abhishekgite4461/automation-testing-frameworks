const BaseRenewSavingsAccountScreen = require('../../renewSavingsAccount.screen');
const helper = require('../../ios.helper');

class RenewSavingsAccountScreen extends BaseRenewSavingsAccountScreen {
    constructor() {
        super();
        this.title = helper.byName('Select a savings account to renew');
        this.renew = '(//XCUIElementTypeStaticText[@name="Renew"])[1]';
        this.renewAccountPage = helper.byName('Renew your account');
        this.savingAccount = '(//XCUIElementTypeLink)[1]';
        this.renewSaving = helper.byName('Renew');
        this.applyNow = '//XCUIElementTypeButton[contains(@name, "Apply now")]';
        this.confirmPage = helper.byName('Confirm your selection');
        this.confirm = '//XCUIElementTypeButton[@name="Confirm renewal"]';
        this.termsAndConditions = helper.byPartialName('I agree to the conditions');
        this.successPage = helper.byPartialName('successfully renewed your savings account');
    }
}

module.exports = RenewSavingsAccountScreen;
