const BaseMoreScreen = require('../../more.screen');
const helper = require('../../ios.helper');

class MoreScreen extends BaseMoreScreen {
    constructor() {
        super();
        this.title = helper.byName('MoreMenuScreen');
        this.moreSwitchBusiness = helper.byName('moreSwitchBusiness');
        this.businessName = (buisness) => `//XCUIElementTypeCell[@name='moreSwitchBusiness']/
        XCUIElementTypeStaticText[@value="${buisness}"]`;
        this.yourProfile = helper.byName('moreCustomerProfile');
        this.inbox = helper.byName('moreInbox');
        this.settings = helper.byName('moreSettings');
        this.cardManagement = helper.byName('moreCardManagment');
        this.lostStolenCard = helper.byName('moreLostOrStolenCard');
        this.replacementCardPin = helper.byName('moreReplacments');
        this.chequeScanning = helper.byName('chequesManagmentSection');
        this.depositCheque = helper.byName('moreDepositCheque');
        this.viewDepositHistory = helper.byName('moreChequeDepositHistory');
        this.logout = helper.byName('MoreMenuLogOffButton');
        this.darkURL = helper.byName('settingsDarkUrlTile');
    }
}

module.exports = MoreScreen;
