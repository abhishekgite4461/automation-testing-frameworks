const BaseErrorScreen = require('../../error.screen');
const helper = require('../../ios.helper');

class ErrorScreen extends BaseErrorScreen {
    constructor() {
        super();
        this.title = helper.byName('errorPageHeader');
        this.revokedIDErrorMessage = helper.byPartialValue('9200200');
        this.SCARevokedErrorMessage = helper.byPartialValue('your account has been locked');
        this.suspendedIDErrorMessage = helper.byPartialValue('suspended');
        this.inactiveIDErrorMessage = helper.byPartialValue('9200141');
        this.logonToMobileBanking = helper.byName('errorPageContinueButton');
        this.partiallyRegisteredErrorMessage = '//*[contains(@value,"or have not completed the authentication process for full access") or contains(@value,"or haven\'t completed the authentication process for full access")]';
        this.telephoneNumber = '//*[contains(@value,"+44") or contains(@value,"034")]';
        this.twoFactorAuthErrorMessage = helper.byPartialValue('Sorry, we couldn’t complete your request. Please try later or call us');
        this.compromisedPasswordErrorMessage = helper.byPartialValue('temporarily suspended');
        this.homeButton = helper.byName('Home');
        this.maxDeviceEnrolErrorMessage = helper.byPartialValue('exceeded the maximum number of devices you');
        this.suspendedCardError = helper.byPartialValue('9200161');
        this.oldCardExpiredError = helper.byPartialValue('1000027');
    }
}

module.exports = ErrorScreen;
