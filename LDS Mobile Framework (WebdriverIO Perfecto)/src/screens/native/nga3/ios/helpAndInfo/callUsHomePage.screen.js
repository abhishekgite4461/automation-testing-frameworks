const BaseCallUsHomePageScreen = require('../../../helpAndInfo/callUsHomePage.screen');
const helper = require('../../../ios.helper');

class CallUsHomePageScreen extends BaseCallUsHomePageScreen {
    constructor() {
        super();
        this.title = helper.byName('CallUsHubTitle');
        this.yourAccountsTile = helper.byName('CallUsArrangementsTile');
        this.personalAccountsTile = helper.byName('CallUsArrangementsTile');
        this.newProductEnquireTile = helper.byName('CallUsNewProductTile');
        this.internetBankingTile = helper.byName('CallUsInternetBankingTile');
        this.otherBankingQueryTile = helper.byName('CallUsOtherQueryTile');
        this.securityTravelLabel = helper.byName('CallUsEmergenciesLabel');
        this.suspectedFraudTab = helper.byName('CallUsSuspectedFraudButton');
        this.lostOrStolenCardTab = helper.byName('CallUsLostOrStolenButton');
        this.emergencyCashAbroadTab = helper.byName('CallUsEmergencyCashButton');
        this.medicalAssistAbroadTab = helper.byName('CallUsMedicalAssistanceButton');
        this.globalMenuButton = helper.byName('globalMainMenu');
        this.closeButton = helper.byName('CrossBarButton');
        this.cmaSurveyLink = helper.byName('CallUsCMASurveyTitle');
        this.businessAccountsTile = helper.byLabel('Business accounts');
    }
}

module.exports = CallUsHomePageScreen;
