const BaseSecureInboxScreen = require('../../secureInbox.screen.js');
const helper = require('../../ios.helper');

class SecureInboxScreen extends BaseSecureInboxScreen {
    constructor() {
        super();
        this.title = '(//XCUIElementTypeOther[contains(@name,"Inbox")])[1]';
        this.inboxMessage = '//XCUIElementTypeOther[contains(@name,"Your Inbox - Mobile")]'
            + '/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]';
        this.archiveButton = '//XCUIElementTypeLink[@name="Archive"]';
        this.undoArchiveButton = '//XCUIElementTypeLink[@name="UNDO"]';
        this.verifyUndoArchiveMessage = '//XCUIElementTypeStaticText[contains(@name,"action has been successful."])';
        this.pdfMessage = '//XCUIElementTypeStaticText [@name="View PDF"]';
        this.stopView = '//XCUIElementTypeButton[@name="Stop" or @name="CrossBarButton"]';
        this.folderName = (accountName) => `//XCUIElementTypeOther[contains(@label, "${accountName}")]`;
        this.accountLabel = (accountName) => `//XCUIElementTypeStaticText[@label="${accountName}"]`;
        this.noMessagesLabel = helper.byValue('You currently have no messages in this category.');
        this.filteredMessage = (accountName) => `(//XCUIElementTypeStaticText[contains(@label, "${accountName}")])[2]`;
        this.inbox = '//XCUIElementTypeOther[contains(@name,"Inbox (")]';
    }
}

module.exports = SecureInboxScreen;
