const BaseEnrolmentSuccessScreen = require('../../enrolmentSuccess.screen');
const helper = require('../../ios.helper');

class EnrolmentSuccessScreen extends BaseEnrolmentSuccessScreen {
    constructor() {
        super();
        this.title = helper.byName('Congratulations');
        this.continueButton = helper.byName('enrollmentSuccessContinueButton');
    }
}

module.exports = EnrolmentSuccessScreen;
