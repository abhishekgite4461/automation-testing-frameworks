const BaseReviewDepositScreen = require('../../ics/reviewDeposit.screen');
const helper = require('../../../../ios.helper');

class ReviewDepositScreen extends BaseReviewDepositScreen {
    constructor() {
        super();
        this.title = helper.byName('chequeReviewDepositDepositInformationTextView');
        this.amountMismatchConfirm = helper.byName('alertActionConfirmButton');
        this.amountMismatchCancel = helper.byName('alertActionCancelButton');
        this.amountConfirmAlert = helper.byName('Confirm amount');
    }
}

module.exports = ReviewDepositScreen;
