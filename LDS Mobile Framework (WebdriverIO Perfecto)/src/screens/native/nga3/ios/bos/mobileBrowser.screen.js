const BaseMobileBrowserScreen = require('../mobileBrowser.screen');

class MobileBrowserScreen extends BaseMobileBrowserScreen {
    constructor() {
        super();
        this.brandUrl = 'online.bankofscotland.co.uk';
        this.returnToApp = '//*[@label="Return to Bank of Scot"]';
    }
}

module.exports = MobileBrowserScreen;
