const BaseAppStoreScreen = require('../appStore.screen');
const helper = require('../../../ios.helper');

class AppStoreScreen extends BaseAppStoreScreen {
    constructor() {
        super();
        this.appText = helper.byLabel('Bank of Scotland Mobile Bank');
    }
}

module.exports = AppStoreScreen;
