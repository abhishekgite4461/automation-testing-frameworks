const BaseFscsScreen = require('../fscs.screen');

class FscsScreen extends BaseFscsScreen {
    constructor() {
        super();
        this.compensationSchemeLabel = '//XCUIElementTypeOther[@name="Financial Services Compensation Scheme"]';
    }
}

module.exports = FscsScreen;
