const BaseMobileBrowserScreen = require('../mobileBrowser.screen');

class covid19MobileBrowserSupportScreen extends BaseMobileBrowserScreen {
    constructor() {
        super();
        this.mortgagePaymentHolidayUrl = 'lloydsbank.com/mortgages/help-and-guidance/managing-your-mortgage.html';
        this.creditcardPaymentHolidayUrl = 'lloydsbank.com/help-guidance/coronavirus.html#manage';
        this.loanPaymentHolidayUrl = 'lloydsbank.com/loans/repayment-holidays.html';
        this.overdraftChargesUrl = 'lloydsbank.com/help-guidance/coronavirus.html#manage';
        this.travelAndDisruptionCoverUrl = 'lloydsbank.com/help-guidance/coronavirus.html#travel';
        this.moreInfoAboutCoronavirusUrl = 'lloydsbank.com/help-guidance/coronavirus.html';
    }
}

module.exports = covid19MobileBrowserSupportScreen;
