const BaseMobileBrowserScreen = require('../mobileBrowser.screen');

class MobileBrowserScreen extends BaseMobileBrowserScreen {
    constructor() {
        super();
        this.brandUrl = 'online.lloydsbank.co.uk';
        this.returnToApp = '//*[@label="Return to Lloyds Bank"]';
    }
}

module.exports = MobileBrowserScreen;
