const BaseDepositHistoryScreen = require('../../ics/depositHistory.screen');
const helper = require('../../../../ios.helper');

class DepositHistoryScreen extends BaseDepositHistoryScreen {
    constructor() {
        super();
        this.technicalErrorText = helper.byValue('Deposit history unavailable');
        this.accountErrorText = helper.byValue("Sorry, we can't show some of the information "
        + "you've asked for right now. Please try later.");
    }
}

module.exports = DepositHistoryScreen;
