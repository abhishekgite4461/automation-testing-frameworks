const BaseDepositChequeScreen = require('../../ics/chequeDeposit.screen');
const helper = require('../../../../ios.helper');

class ChequeDepositScreen extends BaseDepositChequeScreen {
    constructor() {
        super();
        this.technicalErrorText = helper.byValue("Sorry, there's a technical problem");
        this.accountErrorText = helper.byValue("Sorry, but this account information isn't available right now. Please try again later.");
    }
}

module.exports = ChequeDepositScreen;
