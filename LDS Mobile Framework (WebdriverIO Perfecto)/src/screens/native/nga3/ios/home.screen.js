const BaseHomeScreen = require('../../home.screen');
const helper = require('../../ios.helper');
const DeviceHelper = require('../../../../utils/device.helper');

class HomeScreen extends BaseHomeScreen {
    constructor() {
        super();
        this.title = helper.byName('Home');
        this.account = (accountName) => `//*[contains(@label, 'Menu for ${accountName}.')]/ancestor::XCUIElementTypeCell`;
        this.pensionAccount = helper.byValue('Workplace Pension');
        this.hittablePensionAccount = '//*[@value="Workplace Pension" and @hittable="true"]';
        this.accountIterator = (accountName) => `//*[contains(@label,"${accountName}")]/ancestor::XCUIElementTypeCell`;
        this.pensionAccountIterator = (accountName) => `//*[contains(@label,"${accountName}")]
            /ancestor::XCUIElementTypeCell`;
        this.accountTileContainer = (accountTileName) => `//*[@name= '${accountTileName}']`;
        this.accountTileContainer = (accountTileName) => `//*[@name= '${accountTileName}']`;
        this.accountSpecificActionMenu = (accountName) => `//*[@label
            ="Menu for ${accountName}. Shows all options for this account"]`;
        this.externalBankActionMenuButton = (bankName, accountType) => `//*[contains(@label, "${bankName}") 
            and contains(@label, "${accountType}")]/..//*[@name="accountOptionsButton"]`;
        this.accountDetailTile = (index) => `//XCUIElementTypeCell[${(parseInt(index, 10) + 1).toString()}]`;
        this.asmLeadTile = '//*[@name="itemAccountStyleModuleCardView"]';
        this.firstClickableAccountTile = (index) => (DeviceHelper.isGreaterThaniOS10() ? this.accountDetailTile(index) : "//*[@name='accountName' and @hittable='true']");
        this.primaryAccount = "(//*[@name='accountName'])[1]";
        this.accountNameLabel = (account) => helper.byVisibleLabel(`Account name. ${account}`);
        this.accountName = (accountTileName) => `${this.accountTileContainer(accountTileName)}//*[@name='accountName']`;
        this.accountNameElement = (index) => browser.element(`~accountTile${index}`).element('~accountName');
        this.accountNumber = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='voiceOverView']`;
        this.accountTileLogo = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='voiceOverView']`;
        this.cbsLoanAccountNumber = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='rightSubtitle']`;
        this.accountBalance = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='voiceOverView']`;
        this.loanCurrentBalance = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='balance']`;
        this.cbsCurrentBalance = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='balance']`;
        this.accountInfoBalance = (accountTileName) => `${this.accountTileContainer(accountTileName)}
       //*[@name='voiceOverView']`;
        this.cbsLoanCurrentBalance = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='labelOneKey']`;
        this.overdraftLimit = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='balance']/following-sibling::*[1]`;
        this.availableCredit = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='voiceOverView']`;
        this.accountLimit = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='labelTwoKey']`;
        this.interestRate = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='balance']/following-sibling::*[1]`;
        this.mortgageBalanceOnDate = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='balance']/following-sibling::*[1]`;
        this.overdueAmount = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='secondDetailValue']`;
        this.sortCode = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='voiceOverView']`;
        this.cbsLoanSortCode = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='leftSubtitle']`;
        this.loanAccountNumber = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='voiceOverView']`;
        this.creditCardAccountNumber = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='voiceOverView']`;
        this.mortgageAccountNumber = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='leftSubtitle']`;
        this.mortgageMonthlyPayment = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='balance']/following-sibling::*[1]`;
        this.originalLoanAmount = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='balance']/following-sibling::*[1]`;
        this.maturityDate = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='balance']/following-sibling::*[1]`;
        this.accountTileIndex = (account) => `${account}/../../*[contains(@name,'accountTile')]`;
        this.alertMessage = (account) => `//*[contains(@label,"${account}")]/..//*[@name='messageIndicator']`;
        this.scottishWidowsBalance = '//*[@name="balance" and @hittable="true"]';
        this.scottishWidowsPolicyNumber = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='voiceOverView']`;
        this.actionMenu = '//*[@name="actionMenu" and @visible="true"]';
        this.transferPaymentTile = helper.byName('homePageTransferPayment');
        this.yourAccountsTile = helper.byName('homePageYourAccounts');
        this.atmBranchFinderTile = helper.byName('homePageAtmBranchFinder');
        this.helpInfoTile = helper.byName('homePageHelpAndInfo');
        this.everydayOfferTile = helper.byName('spendingRewardsSignUpTile');
        this.technicalErrorEverydayOffer = helper.byPartialLabel("Error message.For technical reasons we can't register you for Everyday Offers at the moment.");
        this.registerForEverydayOffer = helper.byName('spendingRewardsSignUpTile');
        this.viewEverydayOffer = helper.byName('spendingRewardsViewOffersTile');
        this.callUsTile = helper.byName('homePageCallUs');
        this.hostSystemDownErrorMessage = helper.byPartialLabel("Sorry, we can't show some of the account info you asked for right now. Please try later.");
        this.pendingPaymentErrorMessage = helper.byPartialLabel('You have an urgent request awaiting your approval. Please go to the desktop site');
        this.dismissErrorMessage = helper.byName('closeIcon');
        this.firstAccount = '//*[@name="accountTile0"]';
        this.firstAccountBalance = '(//*[@name="balance"])[1]';
        this.firstAvailableBalance = '(//*[@name="firstDetailValue"])[1]';
        this.actionMenuOfCurrentAccount = helper.byLabel('Menu for Islamic Current Account. Shows all options for this account');
        this.actionMenuOfSavingAccount = helper.byLabel('Menu for Easy Saver. Shows all options for this account');
        this.actionMenuOfisaAccount = helper.byLabel('Menu for Cash ISA Saver. Shows all options for this account');
        this.actionMenuOfCreditCardAccount = '//*[@label="Menu for Lloyds Bank Platinum MasterCard. Shows all options for this account" or @label="Menu for Lloyds Bank Choice Rewards. Shows all options for this account"]';
        this.actionMenuOfLoanAccount = helper.byLabel('Menu for Lloyds Bank Personal Loan. Shows all options for this account');
        this.actionMenuOfMortgageAccount = helper.byLabel('Menu for Your mortgage. Shows all options for this account');
        this.errorMessageInvalidAccount = '//*[contains(@value,"Sorry, you don\'t have any accounts that can be accessed through Mobile or Internet Banking") or contains(@value,"Sorry, Mobile and Online Banking can\'t be used with the type of accounts you have")]';
        this.alertMessageDormantAccount = helper.byPartialValue('This account has been inactive for over');
        this.nonAccessibleNga = helper.byPartialValue('You can\'t view your investment accounts in the app or Mobile website.');
        this.accountlinkLead = (account) => `${helper.byPartialLabel(`${account}`)}
        /following-sibling::*[@name='leadWebView']`;
        this.offsetRepossessedMortgageName = (accountName) => `//*[@label="${accountName}"]`;
        this.offsetRepossessedMortgageNumber = (accountName) => `//*[@label="${accountName}"]
        /following-sibling::*[@name="leftSubtitle"]`;
        this.offsetRepossessedMortgageBalance = (accountName) => `//*[@label="${accountName}"]
        /following::*[@name="balance"]`;
        this.offsetRepossessedMortgageMonthlyPayment = (accountName) => `//*[@label="${accountName}"]
        /following::*[contains(@name,"Current monthly payment.")][1]`;
        this.offsetRepossessedMortgageBalanceAsAtDate = (accountName) => `//*[@label="${accountName}"]
        /following::*[contains(@name,"Balance as of.")][1]`;
        this.accountByName = (accountName) => `//*[contains(@label, 'Menu for ${accountName}.')]`;
        this.creditCardAccountTileProgressBar = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='progressView']`;
        this.accountTileProgressAvailableCreditLabel = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='leftProgressKeyLabel']`;
        this.accountTileProgressAvailableCreditValue = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='leftProgressValueLabel']`;
        this.accountTileProgressCreditLimitLabel = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name="rightProgressKeyLabel"]`;
        this.accountTileProgressCreditLimitValue = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='rightProgressValueLabel']`;
        this.accountTileMinimumPaymentLabel = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='voiceOverView']`;
        this.accountTileMinimumPaymentValue = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='firstDetailValue']`;
        this.accountTilePaymentDueDateLabel = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='voiceOverView']`;
        this.accountTilePaymentDueDateValue = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='secondDetailValue']`;
        this.accountTileStatementBalanceLabel = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='voiceOverView']`;
        this.accountTileStatementBalanceValue = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='thirdDetailValueLabel']`;
        this.balanceTransferTile = helper.byName('firstOptionTitleLabel');
        this.moneyTransferTile = helper.byName('secondOptionTitleLabel');
        this.notAvailableInterestRateValue = '//*[@name="accountName"]/following::*[contains(@name, "Not available")]';
        this.percentInterestRateValue = '//*[contains(@name,"%")]';
        this.tapAndHoldInterestRateValue = '//*[contains(@name,"tap and hold for interest rates.")]';
        this.multipleNotification = helper.byName('notificationCentreList');
        this.singleNotification = helper.byName('notificationCentreSingle');
        this.notificationWebViewJourney = helper.byName('WebViewScreen');
        this.multipleNotificationView = helper.byName('homeMultipleNotificationView');
        this.notificationCentreCloseButton = helper.byName('notificationCentreCloseButton');
        this.multipleNotificationCentreHolder = helper.byName('notificationCentreHolder');
        this.reportIssueButton = helper.byName('Report Issue');
        this.cantApplyErrorMessage = helper.byName('ProductsHubUnavailableDescription');
        this.cantMakePaymentErrorMessage = helper.byName('PaymentsHubUnavailableTitle');
        this.homeScreenErrorBanner = helper.byName('errorMessage');
        this.externalProviderBanksName = helper.byName('providerLabel');
        this.externalProviderBanksNameText = (number) => helper.byNameAndIndex('providerLabel', number);
        this.externalProviderAccountTile = (bankName, accountType) => `//*[contains(@name, "externalAccountTile")]
        //*[contains(@label, "${bankName}") and contains(@label, "${accountType}")]`;
        this.switchBusinessButton = helper.byLabel('Switch business');
        this.renewAccountSharingButtonOnErrorTile = (bankName) => `//*[contains(@label, "${bankName}")]
            /..//*[@name="renewConsentButton"]`;
        this.settingsButtonOnErrorTile = (bankName) => `//*[contains(@label,"${bankName}")]
            /..//*[@name="settingsButton"]`;
        this.externalBankAccountErrorTile = (bankName) => `//*[contains(@name,"externalAccountErrorTile")]
            /*/*[contains(@label,'${bankName}')]`;
        this.priorityMessageNormal = helper.byName('PriorityMessageActionView');
        this.priorityDismissMessage = helper.byName('PriorityMessageDismissView');
        this.businessName = (accountTileName) => `${this.accountTileContainer(accountTileName)}//*[@name='businessName']`;
        this.thirtyTwoDayNoticeAccountNumber = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='leftSubtitle']`;
        // TODO - 1055 Create accessibility Id
        this.updateNowMessage = helper.byLabel('Upgrade now');
        this.priorityMessageCallAction = helper.byLabel('An app update is available. Upgrade now');
        this.voiceIDScreen = helper.byLabel('Voice ID');
        this.asmVoiceIDScreen = helper.byLabel('Promotion.');
        this.registerVoiceIDButton = helper.byLabel('Call us to register');
        this.notNowButton = helper.byLabel('Not now');
        this.voiceIDHeading = `${helper.byLabel('Use your voice as your password')}|${helper.byLabel('USE YOUR VOICE AS YOUR PASSWORD')}`;
        this.allBusinessName = helper.byName('businessName');
        this.noActionMenuAccounts = (accountName) => `(//*[contains(@label, '${accountName}')])[1]/
        following-sibling::XCUIElementTypeOther`;
        this.headerToolbar = helper.byName('titleView');
        this.accountContainer = helper.byPartialName('AccountTile');
        this.payCreditCard = helper.byName('payCreditCardAction');
        this.viewTransactions = helper.byName('viewTransactionsAction');
        this.cardManagement = helper.byName('cardControlsAction');
        this.pdfStatement = helper.byName('pdfStatementsAction');
        this.manageCreditLimitAction = helper.byName('manageCreditLimitAction');
        this.upcomingPaymentsQuickLink = (accountName) => `//*[@name= '${accountName}']//*[@label="Upcoming payments"]`;
        this.freezeCardQuickLink = (accountName) => `//*[@name= '${accountName}']//*[@label="Freeze card"]`;
        this.notificationsQuickLink = (accountName) => `//*[@name= '${accountName}']//*[@label="Notifications"]`;
        this.moreQuickLink = (accountName) => `//*[@name= '${accountName}']
        //*[@label="View more options for this account"]`;
        this.accountSpecificMoreQuicklink = (accountName) => `//*[contains(@label,'${accountName}')]
        /ancestor::XCUIElementTypeCell//*[@label="View more options for this account"]`;
        this.accountSpecificUpcomingPaymentsQuicklink = (accountName) => `//*[contains(@label,'${accountName}')]
        /ancestor::XCUIElementTypeCell//*[@label="Upcoming payments"]`;
        this.accountSpecificFreezeCardQuicklink = (accountName) => `//*[contains(@label,'${accountName}')]
        /ancestor::XCUIElementTypeCell//*[@label="Freeze card"]`;
        this.accountSpecificNotificationsQuicklink = (accountName) => `//*[contains(@label,'${accountName}')]
        /ancestor::XCUIElementTypeCell//*[@label="Notifications"]`;
        // TODO:Id missing MPT-5434
        this.accountTileOverdraftLimitLabel = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='firstDetailKey']`;
        this.accountTileOverdraftLimitValue = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='firstDetailValue']`;
        this.accountTileRemainingOverdraftLabel = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='secondDetailKey']`;
        this.accountTileRemainingOverdraftValue = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='secondDetailValue']`;
        this.accountTileOverdraftLimitDetails = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='voiceOverView']`;
        this.accountTileRemainingOverdraftDetails = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        //*[@name='voiceOverView']`;
        this.accountNameLabelAfterRename = (accountTileName) => `//*[@name='voiceOverView' and contains(@label, 
        "${accountTileName}")]`;
        this.noActionMenuBOSAccounts = (accountName) => `(//*[contains(@label, '${accountName}')])[1]`;
        this.payAndTransferQuickLink = (accountName) => `//*[@name= '${accountName}']//*[@label="Pay & Transfer"]`;
        this.manageCardQuickLink = (accountName) => `//*[@name= '${accountName}']//*[@label="Manage card"]`;
        this.accountSpecificPayAndTransferQuicklink = (accountName) => `//*[contains(@label,'${accountName}')]
        /ancestor::XCUIElementTypeCell//*[@name="payAndTransferAction"]`;
        this.accountSpecificManageCardQuicklink = (accountName) => `//*[contains(@label,'${accountName}')]
        /ancestor::XCUIElementTypeCell//*[@name="manageCardAction"]`;
        this.payAndTransferQuickLinkText = 'Pay & Transfer';
        this.manageCardQuickLinkText = 'Manage card';
    }
}

module.exports = HomeScreen;
