const BaseAppLaunchErrorScreen = require('../../appLaunchError.screen');
const helper = require('../../ios.helper');

class AppLaunchErrorScreen extends BaseAppLaunchErrorScreen {
    constructor() {
        super();
        this.title = helper.byName('errorPageHeader');
        this.technicalError = helper.byName('errorPageMessage');
        this.internetConnectionError = helper.byPartialLabel('Please check your Internet connection');
    }
}

module.exports = AppLaunchErrorScreen;
