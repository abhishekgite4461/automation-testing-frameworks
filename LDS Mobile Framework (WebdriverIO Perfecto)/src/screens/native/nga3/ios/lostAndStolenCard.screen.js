const BaseLostAndStolenCard = require('../../lostAndStolenCard.screen.js');

class LostAndStolenCard extends BaseLostAndStolenCard {
    constructor() {
        super();
        this.title = '//XCUIElementTypeStaticText[@name="Lost or stolen cards"]';
    }
}

module.exports = LostAndStolenCard;
