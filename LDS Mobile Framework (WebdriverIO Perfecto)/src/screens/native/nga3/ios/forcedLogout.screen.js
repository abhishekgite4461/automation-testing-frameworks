const BaseForcedLogoutScreen = require('../../forcedLogout.screen');
const helper = require('../../ios.helper');

class ForcedLogoutScreen extends BaseForcedLogoutScreen {
    constructor() {
        super();
        this.title = helper.byName('errorPageHeader');
        this.errorIcon = helper.byName('alerticon');
        this.errorMessage = helper.byName('errorPageMessage');
        this.logonButton = helper.byName('errorPageContinueButton');
    }
}

module.exports = ForcedLogoutScreen;
