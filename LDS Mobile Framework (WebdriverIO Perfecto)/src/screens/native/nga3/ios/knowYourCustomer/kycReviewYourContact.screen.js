const BaseKnowYourCustomerScreen = require('../../../knowYourCustomer/kycReviewYourContact.screen');
const helper = require('../../../ios.helper');

class KycReviewYourContactScreen extends BaseKnowYourCustomerScreen {
    constructor() {
        super();
        this.title = helper.byLabel('Review your contact details');
        this.securityText = helper.byName('kycMessageTextView');
        this.mobileNumber = helper.byName('kycEmailInputField');
        this.mobileLabel = helper.byName('kycMobileLabel');
        this.emailId = helper.byName('kycMobileInputField');
        this.emailLabel = helper.byName('kycEmailLabel');
        this.detailsCorrect = helper.byName('kycConfirmButton');
        this.updateLater = helper.byName('kycSkipButton');
        this.notNowButton = helper.byName('kycIgnoreSkipButton');
        this.editDetails = helper.byName('kycIgnoreEditButton');
    }
}

module.exports = KycReviewYourContactScreen;
