const helper = require('../../ios.helper');
const BaseAccountSelection = require('../../accountSelection.screen');

class accountSelection extends BaseAccountSelection {
    constructor() {
        super();
        this.accountSelectionAccountName = helper.byName('viewAccountSelectorAccountName');
        this.accountSelectionAccountNumber = helper.byName('viewAccountSelectorAccountNumber');
        this.accountSelectionSortCode = helper.byName('viewAccountSelectorSortCode');
        this.accountSelectionAccountBalance = helper.byName('viewAccountSelectorAccountBalance');
        this.currentAccountWarningMessage = helper.byName('accountSelectorHeaderText');
    }
}

module.exports = accountSelection;
