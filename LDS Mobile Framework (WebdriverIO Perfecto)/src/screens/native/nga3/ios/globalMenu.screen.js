const BaseGlobalMenuScreen = require('../../globalMenu.screen');
const helper = require('../../ios.helper');

class GlobalMenuScreen extends BaseGlobalMenuScreen {
    constructor() {
        super();
        this.title = '//XCUIElementTypeButton[@name="globalCustomerName"]';
        this.globalMenuButton = helper.byName('globalMainMenu');
        this.globalIcon = helper.byName('globalMenuIcon');
        this.yourProfileButton = helper.byName('globalMyProfile');
        this.customerName = '//XCUIElementTypeButton[@name="globalCustomerName"]';
        this.myAccountsButton = helper.byName('globalMyAccounts');
        this.newAccountButton = helper.byName('globalNewAccount');
        this.transferAndPaymentButton = helper.byName('globalTransferPayment');
        this.moveMoneyButton = helper.byName('globalMoveMoney');
        this.payContactSettings = helper.byName('globalPayContactSettings');
        this.everyDayOfferButton = helper.byName('globalEverydayOffer');
        this.registerForEveryDayOfferButton = helper.byName('globalRegisterEverydayOffer');
        this.everyDayOfferViewOffers = helper.byName('globalEverydayOfferViewOffers');
        this.everyDayOfferManageSettings = helper.byName('globalEverydayOfferManageSettings');
        this.inboxButton = helper.byName('globalInbox');
        this.productHubButton = helper.byName('globalViewProducts');
        this.atmBranchFinderButton = helper.byName('globalAtmBranchFinder');
        this.callUsButton = helper.byName('globalCallUs');
        this.goingAbroadButton = helper.byName('globalGoingAway');
        this.lostStolenCardButton = helper.byName('globalLostStolenCard');
        this.replacementCardAndPinButton = helper.byName('globalReplacementCard');
        this.settingsButton = helper.byName('globalSetting');
        this.logoutButton = helper.byName('globalLogout');
        this.settingsButton = helper.byName('globalSetting');
        this.fingerprint = helper.byName('globalTouchId');
        this.chequeButton = helper.byName('globalCheques');
        this.depositChequeButton = helper.byName('globalDepositCheque');
        this.depositHistoryOption = helper.byName('globalDepositHistory');
        this.cardManagementOption = helper.byName('globalCardManagement');
        this.standingOrderMenu = helper.byName('globalStandingOrders');
        this.directDebitMenu = helper.byName('globalDirectDebits');
        this.globalBackButton = '//XCUIElementTypeNavigationBar/XCUIElementTypeButton[1]';
        this.mobileChatButton = helper.byName('globalMessageUs');
    }
}

module.exports = GlobalMenuScreen;
