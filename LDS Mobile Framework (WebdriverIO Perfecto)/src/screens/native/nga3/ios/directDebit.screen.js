const BaseDirectDebit = require('../../directDebit.screen.js');
const helper = require('../../ios.helper');

class DirectDebit extends BaseDirectDebit {
    constructor() {
        super();
        this.title = helper.byLabel('Direct Debits');
    }
}

module.exports = DirectDebit;
