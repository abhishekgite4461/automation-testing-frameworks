const BaseAccountsOverviewScreen = require('../../accountsOverview.screen');
const helper = require('../../ios.helper');

class AccountsOverviewScreen extends BaseAccountsOverviewScreen {
    constructor() {
        super();
        this.title = helper.byPartialName('Your accounts page');
        this.stickyFooterPlaceHolder = helper.byName('stickyFooterLead');
        this.alertMessage = (accountName) => `//*[@label="Account name. ${accountName}"]
        /following-sibling::*[@name='arrangementTileAlertNotification']`;
        this.accountAggregationLoadingTile = helper.byName('accAggLoadingTile');
        this.externalAccountTiles = helper.byPartialName('externalAccount');
        this.openBankingPromoTile = helper.byName('accAggHomeTileView');
        this.addExternalAccount = helper.byName('accAggHomeTileAddAccountButton');
        this.viewExternalAccount = helper.byName('accAggHomeTileViewAccountButton');
        this.earnCashbackTile = helper.byName('spendingRewardsSignUpTile');
        this.externalProviderAccountActionMenu = (bankName, accountType) => `//*[contains(@name, "${bankName}") 
        and contains(@name, "${accountType}")]/following-sibling::*[@name="accountOptionsButton"]`;
        this.recentlyAddedFlag = (bankName) => `//*[@label="${bankName}"]/parent::*
        [contains(@name, "externalAccountTile")]//child::*[@name="accountOptionsButton"]`;
        this.arrangementsList = '//*[contains(@name, "externalAccountTile") or @name="accAggHomeTileView"]/*/*[1]';
        this.externalAccountNumberList = `//XCUIElementTypeCell[contains(@name,'externalAccountTile')]
            //XCUIElementTypeStaticText[contains(@label,'Account number.')]`;
        this.nonEligibleExternalFromAccountList = `//XCUIElementTypeCell[contains(@name,'externalAccountTile')]
            //XCUIElementTypeOther[contains(@label,'Savings')]
            /following-sibling::XCUIElementTypeStaticText[contains(@name,'rightSubtitle')]`;
        this.covid19Tile = helper.byName('covidSupportPromoTileView');
        this.findOutMoreOnCovid19Tile = '//*[@label="Find out how"]';
        this.rewardHubEntryTile = helper.byName('RewardsHubScreen');
        this.findOutMoreOnRewardHubEntryTile = '//*[@label="Find out more"]';
    }
}

module.exports = AccountsOverviewScreen;
