const BaseCreditCardPdfStatementsWebViewPageScreen = require('../../creditCardPdfStatementsWebViewPage.screen');
const helper = require('../../ios.helper');

class CreditCardPdfStatementsWebViewPageScreen extends BaseCreditCardPdfStatementsWebViewPageScreen {
    constructor() {
        super();
        this.title = helper.byName('PDF statements');
    }
}

module.exports = CreditCardPdfStatementsWebViewPageScreen;
