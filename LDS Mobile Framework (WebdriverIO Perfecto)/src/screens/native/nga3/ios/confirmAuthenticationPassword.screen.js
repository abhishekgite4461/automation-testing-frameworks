const BaseConfirmAuthenticationPasswordScreen = require('../../confirmAuthenticationPassword.screen');
const helper = require('../../ios.helper');

class ConfirmAuthenticationPasswordScreen extends BaseConfirmAuthenticationPasswordScreen {
    constructor() {
        super();
        this.title = helper.byName('passwordAlertTitleLabel');
        this.passwordField = helper.byName('passwordAlertPasswordInputField');
        this.forgottenPasswordLabel = helper.byName('passwordAlertActionForgotPasswordButton');
        this.errorMessage = helper.byPartialLabel('8000067');
        this.confirmButton = helper.byName('alertActionConfirmButton');
        this.cancelButton = helper.byName('alertActionCancelButton');
        this.scaErrorMessage = helper.byPartialValue('That\'s not right. Please try again.');
        this.scaFinalAttemptWarning = helper.byPartialValue('This is your final attempt');
    }
}

module.exports = ConfirmAuthenticationPasswordScreen;
