const helper = require('../../../ios.helper');
const BaseAtmBranchFinderScreen = require('../../../atmBranchFinder/atmBranchFinder.screen.js');

class AtmBranchFinderScreen extends BaseAtmBranchFinderScreen {
    constructor() {
        super();
        this.title = helper.byName('branchFinderTitle');
        this.searchBranchesTile = helper.byName('branchFinderSearchBranchesTile');
        this.findNearbyAtmTile = helper.byName('branchFinderFindNearbyATMsTile');
        this.noteMessage = helper.byName('branchFinderDescription');
        this.mapBackButton = helper.byPartialName('Return to');
    }
}

module.exports = AtmBranchFinderScreen;
