const AlertPopUpScreen = require('../../alertPopUp.screen');
const helper = require('../../../../ios.helper');

class AbandonChangesWinbackScreen extends AlertPopUpScreen {
    constructor() {
        super();
        this.title = helper.byName('editDetailsWinbackAlert');
        this.confirmButton = helper.byName('alertActionConfirmButton');
        this.cancelButton = helper.byName('alertActionCancelButton');
    }
}

module.exports = AbandonChangesWinbackScreen;
