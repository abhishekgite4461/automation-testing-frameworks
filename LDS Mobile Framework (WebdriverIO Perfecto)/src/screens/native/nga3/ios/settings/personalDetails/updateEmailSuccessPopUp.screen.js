const AlertPopUpScreen = require('../../alertPopUp.screen');
const helper = require('../../../../ios.helper');

class UpdateEmailSuccessPopUpScreen extends AlertPopUpScreen {
    constructor() {
        super();
        this.title = helper.byName('personalDetailsEditEmailSuccessAlert');
        this.confirmButton = helper.byName('personalDetailsEditEmailSuccessDismissButton');
    }
}

module.exports = UpdateEmailSuccessPopUpScreen;
