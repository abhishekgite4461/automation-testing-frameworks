const BasePersonalDetailsScreen = require('../../../../settings/personalDetails/personalDetails.screen');
const helper = require('../../../../ios.helper');

class PersonalDetailsScreen extends BasePersonalDetailsScreen {
    constructor() {
        super();
        this.title = helper.byName('settingsPersonalDetailsTable');
        this.fullName = helper.byName('settingsPersonalDetailsFullName');
        this.userIdLabel = helper.byName('settingsPersonalDetailsUserIdLabel');
        this.userId = helper.byName('settingsPersonalDetailsUserId');
        this.mobileNumberLabel = helper.byName('settingsPersonalDetailsMobileNumberLabel');
        this.mobileNumber = helper.byName('settingsPersonalDetailsMobileNumber');
        this.homeNumberLabel = helper.byName('settingsPersonalDetailsHomeNumberLabel');
        this.homeNumber = helper.byName('settingsPersonalDetailsHomeNumber');
        this.workNumberLabel = helper.byName('settingsPersonalDetailsWorkNumberLabel');
        this.workNumber = helper.byName('settingsPersonalDetailsWorkNumber');
        this.workExtensionNumberLabel = helper.byName('settingsPersonalDetailsWorkNumberExtensionLabel');
        this.workExtensionNumber = helper.byName('settingsPersonalDetailsWorkNumberExtension');
        this.addressLabel = helper.byName('settingsPersonalDetailsAddressLabel');
        this.address = helper.byName('settingsPersonalDetailsAddress');
        this.postcodeLabel = helper.byName('settingsPersonalDetailsPostcodeLabel');
        this.postcode = helper.byName('settingsPersonalDetailsPostcode');
        this.emailLabel = helper.byName('settingsPersonalDetailsEmailLabel');
        this.email = helper.byName('settingsPersonalDetailsEmail');
        this.emailContainer = helper.byName('settingsPersonalDetailsEmailCell');
        this.addressContainer = helper.byName('settingsPersonalDetailsAddressCell');
        this.primaryMarketingLink = helper.byName('myProfilePrimaryMarketingPreferencesLink');
        this.secondaryMarketingLink = helper.byName('myProfileSecondaryMarketingPreferencesLink');
        this.dataConsentLink = helper.byName('settingsPersonalDetailsPrivacyManagementCell');
        this.businessName = helper.byName('businessTitle');
        this.businessRegisteredAddressText = helper.byLabel('Business registered address');
        this.businessAddress = helper.byName('businessAddressLabel');
        this.businessPostcode = helper.byName('businessPostCodeLabel');
    }
}

module.exports = PersonalDetailsScreen;
