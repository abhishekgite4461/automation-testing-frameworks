const BaseChangeEmailScreen = require('../../../../settings/personalDetails/changeEmail.screen');
const helper = require('../../../../ios.helper');

class ChangeEmailScreen extends BaseChangeEmailScreen {
    constructor() {
        super();
        this.enterEmailAddressField = helper.byName('personalDetailsEditEmailEmailField');
        this.reenterEmailAddressField = helper.byName('personalDetailsEditEmailReenterEmailField');
        this.confirmButton = helper.byName('personalDetailsEditEmailConfirmButton');
        this.errorMessage = helper.byName('errorMessage');
        this.dismissErrorMessage = helper.byLabel('Dismiss error message');
    }
}

module.exports = ChangeEmailScreen;
