const BaseLeadsScreen = require('../../../../settings/personalDetails/marketingPreferencesHub.screen.js');
const helper = require('../../../../ios.helper.js');

class MarketingPreferencesConfirmationScreen extends BaseLeadsScreen {
    constructor() {
        super();
        this.title = helper.byName('consentConfirmationScreen');
        this.profileButton = helper.byName('consentConfirmationProfileButton');
        this.accountsButton = helper.byName('consentConfirmationAccountsButton');
    }
}
module.exports = MarketingPreferencesConfirmationScreen;
