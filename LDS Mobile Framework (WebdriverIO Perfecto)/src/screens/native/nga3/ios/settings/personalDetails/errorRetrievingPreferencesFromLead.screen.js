const AlertPopUpScreen = require('../../alertPopUp.screen');
const helper = require('../../../../ios.helper');

class ErrorRetrievingPreferencesFromLead extends AlertPopUpScreen {
    constructor() {
        super();
        this.title = helper.byName('marketingPreferencesNotRetrievedViaLeadAlert');
    }
}

module.exports = ErrorRetrievingPreferencesFromLead;
