const BasePhoneOutcomeScreen = require('../../../../settings/personalDetails/phoneOutcome.screen.js');
const helper = require('../../../../ios.helper');

class PhoneOutcomeScreen extends BasePhoneOutcomeScreen {
    constructor() {
        super();
        this.title = helper.byName('changePhoneNumberSuccessScreenDescription');
        this.backToButton = helper.byName('backToContactDetailsButton');
        this.message = helper.byPartialLabel('912001');
    }
}

module.exports = PhoneOutcomeScreen;
