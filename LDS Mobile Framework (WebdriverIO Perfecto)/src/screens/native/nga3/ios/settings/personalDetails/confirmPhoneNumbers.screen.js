const BaseConfirmPhoneNumbersScreen = require('../../../../settings/personalDetails/confirmPhoneNumbers.screen');
const helper = require('../../../../ios.helper');

class ConfirmPhoneNumbersScreen extends BaseConfirmPhoneNumbersScreen {
    constructor() {
        super();
        this.title = helper.byName('settingsConfirmPhoneNumbersTitle');
        this.mobileNumberField = helper.byName('settingsConfirmPhoneNumbersMobileNumberLabel');
        this.homeNumberField = helper.byName('settingsConfirmPhoneNumbersHomeNumberLabel');
        this.workNumberField = helper.byName('settingsConfirmPhoneNumbersWorkNumberLabel');
        this.workExtensionField = helper.byName('settingsConfirmPhoneNumbersWorkExtensionNumberLabel');
        this.confirmButton = helper.byName('settingsConfirmPhoneNumbersConfirmButton');
        this.editButton = helper.byName('settingsConfirmPhoneNumbersEditButton');
    }
}

module.exports = ConfirmPhoneNumbersScreen;
