const BaseChangePhoneNumbersScreen = require('../../../../settings/personalDetails/changePhoneNumbers.screen');
const helper = require('../../../../ios.helper');

class ChangePhoneNumbersScreen extends BaseChangePhoneNumbersScreen {
    constructor() {
        super();
        this.title = helper.byName('editPhoneNumbersTitle');
        this.mobileNumberField = helper.byName('editPhoneNumbersMobileNumberInput');
        this.homeNumberField = helper.byName('editPhoneNumbersHomeNumberInput');
        this.workNumberField = helper.byName('editPhoneNumbersWorkNumberInput');
        this.workExtensionField = helper.byName('editPhoneNumbersWorkExtensionInput');
        this.mobilePaymLabel = helper.byName('editPhoneNumbersMobileNumberPaymLabel');
        this.paymInfoLabel = helper.byName('editPhoneNumbersPaymInfoLabel');
        this.continueButton = helper.byName('editPhoneNumbersContinueButton');
    }
}

module.exports = ChangePhoneNumbersScreen;
