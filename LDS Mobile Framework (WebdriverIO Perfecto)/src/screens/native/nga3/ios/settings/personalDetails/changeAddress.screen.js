const BaseChangeAddressScreen = require('../../../../settings/personalDetails/changeAddress.screen');
const helper = require('../../../../ios.helper');

class ChangeAddressScreen extends BaseChangeAddressScreen {
    constructor() {
        super();
        this.title = helper.byName('coaPostcodeScreenTitle');
        this.changeAddressButton = helper.byName('settingsPersonalDetailsAddressCell');
        this.addressLabel = helper.byName('settingsPersonalDetailsAddressLabel');
        this.address = helper.byName('settingsPersonalDetailsAddress');
        this.postcodeLabel = helper.byName('settingsPersonalDetailsPostcodeLabel');
        this.postcode = helper.byName('settingsPersonalDetailsPostcode');
    }
}

module.exports = ChangeAddressScreen;
