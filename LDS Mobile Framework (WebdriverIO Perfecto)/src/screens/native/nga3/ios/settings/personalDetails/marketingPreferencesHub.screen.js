const BaseLeadsScreen = require('../../../../settings/personalDetails/marketingPreferencesHub.screen.js');
const helper = require('../../../../ios.helper.js');

class MarketingPreferencesHubScreen extends BaseLeadsScreen {
    constructor() {
        super();
        this.title = helper.byName('marketingPreferencesContainer');
        this.marketingHubIntroduction = '//*[@name="Choose how you’d like us to contact you"]';
        this.bosMarketingHubIntroductionText = helper.byName('Choose how you’d like Bank of Scotland to contact you');
        this.halMarketingHubIntroductionText = helper.byName('Choose how you’d like Halifax to contact you');
        this.progressIndicator = helper.byName('marketingPreferencesTitleCellProgressBar');
        this.optedIn = helper.byPartialLabel('Yes');
        this.optedOut = helper.byPartialLabel('No');
        this.internetPrefs = '//*[@name="marketingPreferencesOnlineBankingPreferencesToggle"]';
        this.emailPrefs = '//*[@name="marketingPreferencesEmailPreferencesToggle"]';
        this.postPrefs = '//*[@name="marketingPreferencesLetterPreferencesToggle"]';
        this.thirdPartyPrefs = '//*[@name="marketingPreferencesDeviceMessagingPreferencesToggle"]';
        this.smsPrefs = '//*[@name="marketingPreferencesSMSPreferencesToggle"]';
        this.phonePrefs = '//*[@name="marketingPreferencesPhonePreferencesToggle"]';
        this.submitButton = helper.byName('Confirm');
        this.continueButton = helper.byName('Continue');
        this.notification = helper.byName('errorMessage');
        this.closeErrorMessage = helper.byName('closeIcon');
        this.winbackModal = helper.byName('unsavedMarketingPreferencesModal');
        this.winbackLeave = helper.byName('alertActionConfirmButton');
        this.winbackStay = helper.byName('alertActionCancelButton');
        this.accordionLink = helper.byName('marketingPreferencesPrivacyPolicyLink');
        this.inCompleteErrorMessage = helper.byName('incompleteMarketingPreferencesNotification');
        this.errorMessageClose = helper.byName('closeIcon');
    }
}

module.exports = MarketingPreferencesHubScreen;
