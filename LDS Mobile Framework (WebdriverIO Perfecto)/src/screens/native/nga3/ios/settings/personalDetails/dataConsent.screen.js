const BaseDataConsentScreen = require('../../../../settings/personalDetails/dataConsent.screen.js');
const helper = require('../../../../ios.helper');

class DataConsentScreen extends BaseDataConsentScreen {
    constructor() {
        super();
        this.dataConsentInterstitialPageTitle = helper.byName('analyticsConsentInterstitial');
        this.dataConsentManageConsentPageTitle = helper.byName('AnalyticsConsentScreen');
        this.consentOptedIn = helper.byPartialLabel('Yes');
        this.consentOptedOut = helper.byPartialLabel('No');
        this.marketingPrefs = helper.byPartialName('analyticsConsentsMarketingToggle');
        this.performancePrefs = helper.byPartialName('analyticsConsentsPerformanceToggle');
        this.marketingPrefsText = helper.byPartialValue('Products, services and offers');
        this.performancePrefsText = helper.byPartialValue('Performance');
        this.dataConsentAcceptAllButton = helper.byName('analyticsConsentAcceptAllButton');
        this.dataConsentManageConsentButton = helper.byName('analyticsConsentManageConsentsButton');
        this.dataConsentConfirmButton = helper.byName('analyticsConsentConfirmButton');
        this.dataConsentCookiePolicyLink = helper.byName('analyticsConsentFooterLink');
        this.interstitialDataConsentCookiePolicyLink = helper.byName('analyticsConsentFooterLink');
        this.globalBackButton = '//XCUIElementTypeNavigationBar/XCUIElementTypeButton[1]';
        this.dataConsentWinbackModal = helper.byName('AlertView');
        this.dataConsentExternalWinbackModal = helper.byName('AlertView');
        this.dataConsentForWinbackLeave = helper.byName('alertActionConfirmButton');
        this.dataConsentForWinbackStay = helper.byName('alertActionCancelButton');
        this.cookiesPolicyPagetitle = helper.byPartialName('Cookie policy');
        this.acceptCookiesPolicyPageOnWebPage = helper.byPartialName('Accept all');
        this.KeepChoicesCookiesPolicyPageOnWebPage = helper.byPartialName('Keep my choices');
    }
}

module.exports = DataConsentScreen;
