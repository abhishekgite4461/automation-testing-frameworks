const BaseLegalAndPrivacyScreen = require('../../../settings/legalAndPrivacy.screen');
const helper = require('../../../ios.helper');

class LegalAndPrivacyScreen extends BaseLegalAndPrivacyScreen {
    constructor() {
        super();
        this.title = helper.byPartialLabel('Mobile Banking is designed for use in the UK');
        this.termsAndConditionsLink = helper.byPartialLabel('Banking terms and conditions');
        this.dismissButton = helper.byName('CrossBarButton');
        this.backButton = helper.byLabel('Back');
    }
}

module.exports = LegalAndPrivacyScreen;
