const BaseThirdPartyAcknowledgementScreen = require('../../../settings/thirdPartyAcknowledgement.screen');
const helper = require('../../../ios.helper');

class ThirdPartyAcknowledgementScreen extends BaseThirdPartyAcknowledgementScreen {
    constructor() {
        super();
        this.title = helper.byName('ThirdPartyAcknowledgementsScreen');
    }
}

module.exports = ThirdPartyAcknowledgementScreen;
