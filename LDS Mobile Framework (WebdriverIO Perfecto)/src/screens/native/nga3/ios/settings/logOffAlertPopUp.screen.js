const BaseLogOffAlertPopUpScreen = require('../../../settings/logOffAlertPopUp.screen');
const helper = require('../../../ios.helper');

class LogOffAlertPopUpScreen extends BaseLogOffAlertPopUpScreen {
    constructor() {
        super();
        this.title = helper.byName('logOffAlertPopUpScreen');
        this.logOffButton = helper.byName('alertActionCancelButton');
        this.continueButton = helper.byName('alertActionConfirmButton');
    }
}

module.exports = LogOffAlertPopUpScreen;
