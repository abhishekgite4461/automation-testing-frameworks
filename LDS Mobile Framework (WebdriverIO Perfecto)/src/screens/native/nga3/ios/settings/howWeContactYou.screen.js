const BaseContactYouScreen = require('../../../settings/howWeContactYou.screen');
const helper = require('../../../ios.helper');

class HowWeContactYou extends BaseContactYouScreen {
    constructor() {
        super();
        this.title = helper.byName('settingsTitle');
        this.onlinePaperPreference = helper.byName('Online and paper preferences');
        this.statementFreqPreference = helper.byName('Statement preferences');
        this.passbookTransactionsUpdates = helper.byName('Passbook transactions updates');
        this.marketingPreference = helper.byName('Marketing choices');
        this.realTimeAlert = helper.byName('Real time alert');
    }
}

module.exports = HowWeContactYou;
