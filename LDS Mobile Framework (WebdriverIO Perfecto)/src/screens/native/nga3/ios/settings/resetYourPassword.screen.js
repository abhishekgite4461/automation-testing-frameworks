const BaseResetPasswordScreen = require('../../../settings/resetYourPassword.screen');
const helper = require('../../../ios.helper');

class ResetYourPasswordScreen extends BaseResetPasswordScreen {
    constructor() {
        super();
        this.title = helper.byName('resetPasswordTitle');
        this.enterNewPasswordEditBox = helper.byName('resetPasswordEnterNewPasswordEditBox');
        this.confirmNewPasswordEditBox = helper.byName('resetPasswordConfirmPasswordEditBox');
        this.clickSubmitButton = helper.byName('resetPasswordSubmitButton');
        this.errorMessage = helper.byName('errorMessage');
        this.nonIdenticalErrorMessage = helper.byPartialLabel('Please ensure that the values you have entered are identical');
        this.noPersonalDetailsErrorMessage = helper.byPartialLabel('Please choose a password that doesn’t include personal details');
        this.nonSecurePasswordErrorMessage = helper.byPartialLabel('This password is not secure enough.');
        this.lastFivePasswordErrorMessage = '//*[contains(@label,"Please choose a password that is different from your last five passwords.") or contains(@label,"Please choose a password that is different from your last 5 passwords.")]';
        this.easyToGuessPasswordErrorMessage = helper.byPartialLabel('Avoid common, easy-to-guess passwords');
        this.tipsForPasswordAccordion = helper.byName('tipsForGoodPasswordToolTip');
        this.tipsForPasswordToolTip = helper.byName('resetPasswordSuccessPopUpTitle');
        this.closeButton = helper.byName('tipsForGoodPasswordPopUpCloseButton');
    }
}

module.exports = ResetYourPasswordScreen;
