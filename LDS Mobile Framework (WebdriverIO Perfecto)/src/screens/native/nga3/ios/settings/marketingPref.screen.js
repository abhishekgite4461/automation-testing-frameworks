const BaseMarketingPrefScreen = require('../../../settings/marketingPref.screen');
const helper = require('../../../ios.helper');

class MarketingPref extends BaseMarketingPrefScreen {
    constructor() {
        super();
        this.title = helper.byName('Title');
        this.halifaxMarketing = helper.byName('Halifax');
        this.bosMarketing = helper.byName('Bank of Scotland');
    }
}

module.exports = MarketingPref;
