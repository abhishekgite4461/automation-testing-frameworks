const BaseSecuritySettingsScreen = require('../../../settings/securitySettings.screen');
const helper = require('../../../ios.helper');

class SecuritySettingsScreen extends BaseSecuritySettingsScreen {
    constructor() {
        super();
        this.title = helper.byName('securityDetailsTitle');
        this.securityDetailsUserIDLabel = helper.byName('securityDetailsUserIDLabel');
        this.securityDetailsUserIDValue = helper.byName('securityDetailsUserIDValue');
        this.securityDetailsDeviceNameLabel = helper.byName('securityDetailsDeviceNameLabel');
        this.securityDetailsDeviceNameValue = helper.byName('securityDetailsDeviceNameValue');
        this.securityDetailsDeviceTypeLabel = helper.byName('securityDetailsDeviceTypeLabel');
        this.securityDetailsDeviceTypeValue = helper.byName('securityDetailsDeviceTypeValue');
        this.securityDetailsAppVersionLabel = helper.byName('securityDetailsAppVersionLabel');
        this.securityDetailsAppVersionValue = helper.byName('securityDetailsAppVersionValue');
        this.securityDetailsForgottenPasswordButton = helper.byName('securityDetailsForgottenPasswordButton');
        this.securityDetailsAutoLogOffButton = helper.byName('securityDetailsAutoLogOffButton');
        this.fingerprintToggle = helper.byName('fingerprintSecuritySettingsOptinToggle');
        this.securityFingerPrintToggleCell = helper.byName('fingerprintSecuritySettingsToggleCell');
        this.securityFingerprintCell = helper.byName('fingerprintSecuritySettingsLogonCell');
    }
}

module.exports = SecuritySettingsScreen;
