const BaseAutoLogOffSettingsScreen = require('../../../settings/autoLogOffSettings.screen');
const helper = require('../../../ios.helper');

class AutoLogOffSettingsScreen extends BaseAutoLogOffSettingsScreen {
    constructor() {
        super();
        this.title = helper.byLabel('Your security settings');
        this.autoLogOffImmediately = helper.byName('logoffImmediate');
        this.autoLogOff1minute = helper.byName('logoffAfterOneMin');
        this.autoLogOff2minute = helper.byName('logoffAfterTwoMin');
        this.autoLogOff5minute = helper.byName('logoffAfterFiveMin');
        this.autoLogOff10minute = helper.byName('logoffAfterTenMin');
        this.confirmButton = helper.byEnabledName('enrollmentSecuritySettingsSaveButton');
    }
}

module.exports = AutoLogOffSettingsScreen;
