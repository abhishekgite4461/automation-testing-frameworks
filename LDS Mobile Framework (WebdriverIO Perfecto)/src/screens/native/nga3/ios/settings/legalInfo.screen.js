const BaseLegalInfoScreen = require('../../../settings/legalInfo.screen');
const helper = require('../../../ios.helper');

class LegalInfoScreen extends BaseLegalInfoScreen {
    constructor() {
        super();
        this.title = '//*[@label="Legal information" and @name="Legal information"]';
        this.legalAndPrivacy = helper.byName('tapToViewLegalAndPrivacyInformation');
        this.cookieUseAndPermissions = helper.byName('tapToViewCookieUseAndPermissionsInformation');
        this.thirdPartyAcknowledgements = helper.byName('tapToViewThirdPartyAcknowledgementsInformation');
        this.dismissButton = helper.byName('CrossBarButton');
        this.legalInformationText = helper.byName('settingsLegalInformationDescription');
    }
}

module.exports = LegalInfoScreen;
