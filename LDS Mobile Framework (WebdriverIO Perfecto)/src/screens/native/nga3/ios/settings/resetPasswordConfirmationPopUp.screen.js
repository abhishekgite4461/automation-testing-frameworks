const BaseResetPasswordConfirmationPopUpScreen = require('../../../settings/resetPasswordConfirmationPopUp.screen');
const helper = require('../../../ios.helper');

class ResetPasswordConfirmationPopUpScreen extends BaseResetPasswordConfirmationPopUpScreen {
    constructor() {
        super();
        this.title = helper.byName('resetPasswordSuccessPopUpTitle');
        this.OkAndLogOffButton = helper.byName('alertActionConfirmButton');
    }
}

module.exports = ResetPasswordConfirmationPopUpScreen;
