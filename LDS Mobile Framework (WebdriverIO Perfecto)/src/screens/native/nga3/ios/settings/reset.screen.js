const BaseResetScreen = require('../../../settings/reset.screen');
const helper = require('../../../ios.helper');

class ResetScreen extends BaseResetScreen {
    constructor() {
        super();
        this.title = helper.byName('settingsResettingTheAppTitle');
        this.resetButton = helper.byName('settingsResetAppButton');
        this.resetAppInfo = helper.byPartialLabel('Make sure you’ve updated your mobile phone number before de-registering your device');
        this.resetAppWarningMessage = helper.byPartialLabel('can only be reversed by re-registering');
    }
}

module.exports = ResetScreen;
