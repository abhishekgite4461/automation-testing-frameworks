const BaseDarkUrlScreen = require('../../../settings/darkUrl.screen');
const helper = require('../../../ios.helper');

class DarkUrlScreen extends BaseDarkUrlScreen {
    constructor() {
        super();
        this.title = helper.byName('Base URL');
        this.darkUrlButton = (label) => helper.byLabel(`${label}`);
    }
}

module.exports = DarkUrlScreen;
