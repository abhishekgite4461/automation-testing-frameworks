const BaseResetConfirmationScreenScreen = require('../../../settings/resetConfirmation.screen');
const helper = require('../../../ios.helper');

class ResetConfirmationScreen extends BaseResetConfirmationScreenScreen {
    constructor() {
        super();
        this.title = helper.byName('resetAppConfirmationPopUpTitle');
        this.goButton = helper.byName('alertActionConfirmButton');
        this.cancelButton = helper.byName('alertActionCancelButton');
    }
}

module.exports = ResetConfirmationScreen;
