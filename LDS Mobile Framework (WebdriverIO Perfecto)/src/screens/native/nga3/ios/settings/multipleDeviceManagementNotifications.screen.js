const BaseMultipleDeviceManagementNotificationsScreen = require('../../../settings/multipleDeviceManagementNotifications.screen');
const helper = require('../../../ios.helper');

// TODO : MCC-2233 devs to add accessibility IDs

class MultipleDeviceManagementNotificationsScreen extends BaseMultipleDeviceManagementNotificationsScreen {
    constructor() {
        super();
        this.header = helper.byName('Notifications');
        this.headerDescription = helper.byPartialName('send you notifications right now as you need to allow them on this device first');
        this.allowNotificationsButton = '//XCUIElementTypeButton[@name="notificationStatusButton"]';
        this.apnsTitle = helper.byPartialName('Notifications may include alerts, sounds and icon badge');
        this.apnsAllowButton = helper.byName('Allow');
        this.apnsDontAllowButton = helper.byName('Don’t Allow');
        this.accountNotificationsTile = helper.byName('accountNotificationsCell');
        this.accountNotificationsTileTitle = helper.byName('Account notifications');
        this.accountNotificationsTileText = helper.byPartialName('when regular payments leave your account and when money has been paid in');
        this.accountNotificationsToggleSwitch = helper.byName('pushNotificationsSwitch');
        this.confirmationAlertTitle = helper.byLabel('Success Box. Notifications on.');
        this.okButton = helper.byName('alertActionConfirmButton');
        this.conflictTitle = helper.byPartialName('need to allow them on your');
        this.goToDeviceSettingsButton = helper.byPartialName('goToDeviceSettingsButton');
        this.deviceSettingsnotificationsButton = helper.byName('Notifications');
        this.deviceAllowNotificationsButton = helper.byName('Allow Notifications');
        this.deviceAllowNotificationsToggleButton = '//XCUIElementTypeSwitch[@name="Allow Notifications"]';
    }
}

module.exports = MultipleDeviceManagementNotificationsScreen;
