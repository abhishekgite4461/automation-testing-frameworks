const BaseRealTimeAlertsScreen = require('../../../settings/realTimeAlerts.screen');
const helper = require('../../../ios.helper');

// TODO : MCC-2233 devs to add accessibility IDs

class RealTimeAlertsScreen extends BaseRealTimeAlertsScreen {
    constructor() {
        super();
        this.title = helper.byName('notificationsTitle');
        this.accountAlertsToggleSwitch = helper.byName('Toggle Switch.');
        this.apnsTitle = helper.byPartialName('Notifications may include alerts, sounds and icon badge');
        this.apnsAllowButton = helper.byName('Allow');
        this.apnsDontAllowButton = helper.byName('Don’t Allow');
        this.confirmationAlertTitle = helper.byLabel('Success Box. Notifications on.');
        this.okButton = '//XCUIElementTypeButton[@name="alertActionConfirmButton"]';
        this.optOutConfirmationAlertTitle = helper.byPartialLabel('You can turn on anytime by going to Settings and Info in the app .');
        this.pushPromptScreenBTitle = helper.byPartialName('money is important');
        this.pushPromptScreenBSecondTitle = helper.byPartialName('Your time is precious');
        this.pushPromptScreenBThirdTitle = helper.byPartialName('Get notifications');
        this.nextOption = helper.byLabel('Next');
        this.notNowOption = helper.byName('Not now');
        this.turnOnNotificationsOption = helper.byName('Turn on notifications');
        this.generalButton = helper.byName('General');
        this.dateAndTimeButton = helper.byName('Date & Time');
        this.setAutomaticDateButton = helper.byName('Set Automatically');
        this.setAutomaticDateToggle = '//XCUIElementTypeSwitch[@name="Set Automatically"]';
        this.datePickerTitle = helper.byPartialName('2019');
        this.bosDeviceSettings = helper.byName('Bank of Scot');
        this.halifaxDeviceSettings = helper.byName('Halifax');
        this.lloydsDeviceSettings = helper.byName('Lloyds Bank');
        this.deviceNotifications = helper.byName('Notifications');
        this.conflictScreenTitle = helper.byName('pushOptInHeader');
        this.goToDeviceSettingsButton = helper.byName('pushOptInActiveButton');
        this.turnOffInAppButton = helper.byName('optMeOutOfServiceButton');
        this.optOutOkButton = helper.byName('OK');
        this.iosDeviceNotificationsButton = helper.byName('Notifications');
        this.deviceNotificationsStatusOff = helper.byValue('0');
        this.deviceNotificationsStatusOn = helper.byValue('1');
        this.returnToApp = helper.byPartialName('Return to');
        this.newAccountAlertsToggleSwitch = helper.byName('pushNotificationsSwitch');
        this.appNotificationsDeviceTitle = helper.byPartialName('TO ACCESS');
        this.incompleteActionsOverlay = helper.byPartialName('To receive notifications');
        this.enableNotificationInstruction = helper.byPartialName('Tap Notifications');
    }
}

module.exports = RealTimeAlertsScreen;
