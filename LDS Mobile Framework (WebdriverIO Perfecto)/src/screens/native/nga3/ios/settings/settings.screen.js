const BaseSettingsScreen = require('../../../settings/settings.screen');
const helper = require('../../../ios.helper');

class SettingsScreen extends BaseSettingsScreen {
    constructor() {
        super();
        this.title = helper.byName('Settings and Info');
        this.personalDetailsButton = helper.byName('settingsYourPersonalDetailsTile');
        this.everydayOfferButton = helper.byName('settingsEverydayOffersTile');
        this.securitySettingsButton = helper.byName('settingsYourSecuritySettingsTile');
        this.payAContactButton = helper.byName('settingsPayAContactSettingsTile');
        this.goingAbroadButton = helper.byName('settingsGoingAbroadTile');
        this.resetAppButton = helper.byName('settingsResetMobileBankingTile');
        this.legalInfo = helper.byName('settingsLegalInformationTile');
        this.howWeContactYou = helper.byName('How we contact you');
        this.realTimeAlertsButton = helper.byName('settingsNotificationPreferencesTile');
        this.settingsList = '//XCUIElementTypeCell/XCUIElementTypeStaticText[1]';
        this.settingsOpenBankingTile = helper.byName('settingsOpenBankingTile');
        this.settingsOpenBankingTileErrorBanner = helper.byName('errorMessage');
        this.businessDetailsButton = helper.byName('settingsBusinessDetailsTile');
    }
}

module.exports = SettingsScreen;
