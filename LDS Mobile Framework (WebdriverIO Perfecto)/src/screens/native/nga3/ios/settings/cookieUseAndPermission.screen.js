const BaseCookieUseAndPermissionScreen = require('../../../settings/cookieUseAndPermission.screen');
const helper = require('../../../ios.helper');

class CookieUseAndPermissionScreen extends BaseCookieUseAndPermissionScreen {
    constructor() {
        super();
        this.title = helper.byPartialLabel('Location');
        this.cookiePolicyLink = helper.byPartialLabel('Cookie Policy');
    }
}

module.exports = CookieUseAndPermissionScreen;
