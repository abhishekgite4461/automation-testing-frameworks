const BaseStandingOrderAmendScreen = require('../../../standingOrderDelete.screen');
const helper = require('../../../ios.helper');

class StandingOrderScreen extends BaseStandingOrderAmendScreen {
    constructor() {
        super();
        this.soDeleteTitle = '//XCUIElementTypeOther[@name="Delete standing order"]';
        this.soDeletePassword = '//XCUIElementTypeOther[preceding-sibling::XCUIElementTypeOther[contains(@name,"password")]]/XCUIElementTypeSecureTextField';
        this.soDeleteSOButton = '//XCUIElementTypeButton[@name="Delete standing order"]';
        this.soDeleteSOButtonBnga = '(//XCUIElementTypeStaticText[@name="Delete standing order"])[2]';
        this.soDeleteMessageTitle = helper.byPartialName('Deleted standing order to');
        this.soDeleteMessageTitleBnga = '//XCUIElementTypeStaticText[contains(@name,"has been deleted.")]';
    }
}

module.exports = StandingOrderScreen;
