const BaseReactivateIsaScreen = require('../../../reactivateIsa.screen');
const helper = require('../../../ios.helper');

class ReactivateIsaScreen extends BaseReactivateIsaScreen {
    constructor() {
        super();
        this.title = helper.byName('Reactivate ISA');
        this.reactivateIsaInformation = helper.byName('reactivateIsaInformation');
        this.reactivateIsaAccountNameText = helper.byName('reactivateIsaAccountNameText');
        this.reactivateIsaAccountSortCode = helper.byName('reactivateIsaAccountSortCode');
        this.reactivateIsaAccountNumber = helper.byName('reactivateIsaAccountNumber');
        this.reactivateIsaAccountHolderName = helper.byName('reactivateIsaAccountHolderName');
        this.reactivateIsaAccountHolderAddress = helper.byName('reactivateIsaAccountHolderAddress');
        this.reactivateIsaAccountHolderDateOfBirth = helper.byName('reactivateIsaAccountHolderDateOfBirth');
        this.reactivateIsaNationalInsuranceNo = helper.byName('National insurance:');
        // TODO: MOB3-12585 IOS and android we don't have proper IDs for reactivateISA
        this.reactivateIsaUpdateAddressLinkText = helper.byPartialLabel('How do I update my address?');
        this.reactivateIsaUpdateNiNumberLinkText = helper.byName('reactivateIsaUpdateNiNumberLinkTextView');
        this.reactivateIsaConfirmButton = helper.byName('reactivateIsaConfirmButton');
        this.passwordConfirmationDialogPanel = helper.byName('passwordAlertTitleLabel');
        this.passwordConfirmationDialogForgotPasswordLabel = helper.byName('passwordAlertActionForgotPasswordButton');
        this.reactivateIsaCancelButton = helper.byName('reactivateIsaCancelButton');
        this.reactivateAgreementCheckBox = helper.byName('reactivateIsaAgreementCheckBox');
        this.reactivateIsaEligibilityCriteriaWebView = helper.byName('reactivateIsaEligibilityCriteriaWebView');
        this.dialogPositiveAction = helper.byName('alertActionConfirmButton');
        this.dialogNegativeAction = helper.byName('alertActionCancelButton');
        this.reactivateIsaUpdateNiModal = helper.byName('Updating your national insurance number.');
        this.reactivateCloseButton = helper.byName('alertActionConfirmButton');
        this.settingsPersonalDetailsAddressTitle = helper.byName('settingsPersonalDetailsAddressTitle');
        this.settingsPersonalDetailsAddressLineOne = helper.byName('settingsPersonalDetailsAddress');
        this.settingsPersonalDetailsPostcode = helper.byName('settingsPersonalDetailsPostcode');
        this.settingsPersonalDetailsChangeAddressDescription = helper.byName('settingsPersonalDetailsChangeAddressDescription');
        this.settingsPersonalDetailsChangeAddressButton = helper.byName('settingsPersonalDetailsChangeAddressButton');
        this.passwordConfirmationDialogPasswordBox = helper.byName('passwordAlertPasswordInputField');
        this.reactivateIsaHeader = helper.byName('reactivateIsaInformation');
    }
}

module.exports = ReactivateIsaScreen;
