const BaseSuccessTransferScreen = require('../../../successTransfer.screen');
const helper = require('../../../ios.helper');

class SuccessTransferScreen extends BaseSuccessTransferScreen {
    constructor() {
        super();
        this.title = helper.byName('transferSuccessHeader');
        this.viewTransactionButton = helper.byName('viewTransactionsButton');
        this.makeAnotherTransferButton = helper.byName('anotherTransferButton');
        this.fromDetail = helper.byName('fromAccountDetails');
        this.toDetail = helper.byName('toAccountDetails');
        this.amountDetail = helper.byName('amountValue');
        this.successMessage = helper.byValue('Success');
        this.notNow = helper.byName('Not Now');
        this.fromAccountNameDetail = helper.byName('fromAccountNameLabel');
        this.fromSortCodeDetail = helper.byName('fromSortCodeLabel');
        this.fromAccountNumberDetail = helper.byName('fromAccountNumberLabel');
        this.toAccountNameDetail = helper.byName('toAccountNameLabel');
        this.toSortCodeDetail = helper.byName('toSortCodeLabel');
        this.toAccountNumberDetail = helper.byName('toAccountNumberLabel');
        this.mpaTransferApprovalSuccessMessage = helper.byLabel('Request transfer approval');
    }
}

module.exports = SuccessTransferScreen;
