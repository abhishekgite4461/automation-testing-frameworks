const BaseStandingOrderReviewScreen = require('../../../transferAndPayments/standingOrderReview.screen');
const helper = require('../../../ios.helper');

class StandingOrderReviewScreen extends BaseStandingOrderReviewScreen {
    constructor() {
        super();
        this.title = helper.byName('reviewPaymentHeader');
        this.fromAccountDetails = helper.byName('fromAccountDetails');
        this.toAccountDetails = helper.byName('toAccountDetails');
        this.amountValue = helper.byName('amountValue');
        this.dateFromLabel = helper.byName('dateFromLabel');
        this.reviewPaymentEditButton = helper.byName('reviewPaymentEditButton');
        this.reviewPaymentConfirmButton = helper.byName('reviewPaymentConfirmButton');
        this.navigationBarBackButton = helper.byName('navigationBarBackButton');
        this.remitterName = helper.byName('fromAccountNameLabel');
        this.remitterSortCode = helper.byName('fromSortCodeLabel');
        this.remitterAccountNumber = helper.byName('fromAccountNumberLabel');
        this.benefeciaryName = helper.byName('toAccountNameLabel');
        this.benefeciarySortCode = helper.byName('toSortCodeLabel');
        this.benefeciaryAccountNumber = helper.byName('toAccountNumberLabel');
        this.amount = helper.byName('amountValue');
        this.reference = helper.byName('referenceValue');
        this.standingOrderStartDate = helper.byName('dateFromLabel');
        this.standingOrderEndDate = helper.byName('dateUntilLabel');
    }
}

module.exports = StandingOrderReviewScreen;
