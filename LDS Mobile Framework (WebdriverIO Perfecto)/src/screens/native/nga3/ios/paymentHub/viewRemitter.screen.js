const BaseViewRemitterScreen = require('../../../viewRemitter.screen');
const helper = require('../../../ios.helper');

class ViewRemitterScreen extends BaseViewRemitterScreen {
    constructor() {
        super();
        this.title = helper.byName('SelectRemittingAccountView');
        this.accountName = helper.byName('viewRemitterAccountName');
        this.accountNameLabel = (account) => helper.byLabel(account);
        this.aggAccount = (accountSortCode, accountNumber) => `//XCUIElementTypeStaticText[contains(@label, 
            '${accountSortCode}')]/following-sibling::XCUIElementTypeStaticText[contains(@label,'${accountNumber}')]`;
        this.searchedBusinessAccount = (businessName, accName) => `//*[starts-with(@label, 
            'Account name. ${accName}.') and contains(@label, '${businessName}')]`;
    }
}

module.exports = ViewRemitterScreen;
