const BaseReviewTransferScreen = require('../../../reviewTransfer.screen');
const helper = require('../../../ios.helper');

class ReviewTransferScreen extends BaseReviewTransferScreen {
    constructor() {
        super();
        this.title = helper.byName('PaymentTransferReviewScreen');
        this.editButton = helper.byName('reviewTransferEditButton');
        this.confirmButton = helper.byName('reviewTransferConfirmButton');
        this.fromDetail = helper.byName('fromAccountDetails');
        this.toDetail = helper.byName('toAccountDetails');
        this.amountDetail = helper.byName('amountValue');
        this.reference = helper.byName('referenceValue');
        this.htbIsaWarningMessage = helper.byPartialValue('initial deposit of up to £1,000 within 21 calendar days of opening your account');
        this.topupIsaWarningMessage = helper.byPartialValue('to this ISA in this tax year.');
        this.withdrawFromIsaToNonIsaAccountWarningMessage = helper.byPartialValue('re attempting to transfer money out of your tax-free ISA account');
        this.withdrawFromIsaToIsaAccountWarningMessage = helper.byPartialValue('re attempting to move money from one ISA to another ISA');
        this.mpaTransferApprovalRequiredMessage = helper.byPartialValue('Further approval is needed to continue');
    }
}

module.exports = ReviewTransferScreen;
