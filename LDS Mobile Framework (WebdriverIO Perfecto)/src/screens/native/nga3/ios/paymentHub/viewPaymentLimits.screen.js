const BaseViewPaymentLimitsScreen = require('../../../viewPaymentLimits.screen');

// TODO : PJO-9424 devs to add accessibility IDs

class ViewPaymentLimitsScreen extends BaseViewPaymentLimitsScreen {
    constructor() {
        super();
        this.onlineMobileBanking = '//*[@label="Online & Mobile Banking"]';
        this.ukPaymentsDailyLimit = '//*[@label="UK payments daily limit"]';
        this.ukPaymentResetMessage = '//*[@label="Limits reset at 11:59pm each day."]';
        this.transferYourOwnAccount = '//*[contains(@value,"Transfers between your own")]';
        this.telephonyBanking = '//*[@label="Telephone Banking"]';
        this.ukTelephonyBanking = '//*[@label="UK payments"]';
        this.paymentLimitLeftToday = '(//*[contains(@label,"left today")])[2]';
    }
}

module.exports = ViewPaymentLimitsScreen;
