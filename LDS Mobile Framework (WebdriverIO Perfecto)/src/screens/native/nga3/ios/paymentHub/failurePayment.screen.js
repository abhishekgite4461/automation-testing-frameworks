const BaseFailurePaymentScreen = require('../../../failurePayment.screen');
const helper = require('../../../ios.helper');

class FailurePaymentScreen extends BaseFailurePaymentScreen {
    constructor() {
        super();
        this.title = "//*[@name='Header' and not(child::*)]";
        this.internetErrorMessage = helper.byPartialValue("your Internet connection has been lost so we can't confirm this payment");
        this.pendingPaymentErrorMessage = '//*[contains(@value,"Sorry, we can’t complete this payment") and contains(@value,"a payment pending")]';
        this.prevYearISAErrorMessage = helper.byPartialValue('You can’t make this transaction because you’ve already subscribed to');
        this.transferAndPaymentButton = helper.byName('paymentsTransfersButton');
        this.callUsButton = helper.byName('callUsButton');
    }
}

module.exports = FailurePaymentScreen;
