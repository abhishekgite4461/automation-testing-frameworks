const BaseViewRecipientScreen = require('../../../viewRecipient.screen');
const helper = require('../../../ios.helper');

class ViewRecipientScreen extends BaseViewRecipientScreen {
    constructor() {
        super();
        this.title = helper.byName('viewRecipientSearchButton');
        this.internationalPayment = (accountNumber) => helper.byLabel(accountNumber);
        this.internationalPaymentWithField = "//*[@name='viewRecipientInternationalPaymentManageButton']/preceding-sibling::XCUIElementTypeStaticText[2]";
        this.paymentAccountLocator = (label) => `//*[contains(@label,"${label}") and @visible='true']`;
        this.accountNameLabel = (account) => `(//*[contains(@label,"${account}") and not(
    following-sibling::XCUIElementTypeStaticText[@name="viewRecipientPaymentCell" and @label="Pending payment"])])[1]`;
        this.accountLabel = (account) => helper.byLabel(account);
        this.recipientAccount = "//*[contains(@name,'viewRecipientPaymentSortCode') and not(following-sibling::*[@label='Pending payment'])]";
        this.firstRecipientTransferCell = '//*[@name="viewRecipientTransferCell"][1]';
        this.externalBeneficiaryAccounts = "//*[@name='viewRecipientPaymentAccountName' or @name='viewRecipientP2pAccountName' or @name='viewRecipientInternationalPayeeName']";
        this.myAccounts = helper.byPartialLabel('accounts');
        this.pendingPayment = `(${helper.byVisibleName('viewRecipientPaymentPendingLabel')})[1]`;
        this.zeroRemainingAllowance = () => "//*[@name='viewRecipientTransferCell']/*[contains(@label,'NIL') and contains(@value, 'Remaining')]";
        this.manageButtonMobileNumber = helper.byName('viewRecipientP2pManageButton');
        this.manageButtonUKAccountNumber = "//*[@name='viewRecipientPaymentCell']//*[@label='Manage']";
        this.manageButtonPendingPayment = "(//*[@label='Payment due soon']/../*[@name='viewRecipientPaymentManageButton'])[1]";
        this.recipientList = helper.byName('viewRecipientPaymentAccountName');
        this.manageButtonRecipientName = `${helper.byPartialLabel('DELETE')}//*[contains(@label,'Manage')]`;
    }
}

module.exports = ViewRecipientScreen;
