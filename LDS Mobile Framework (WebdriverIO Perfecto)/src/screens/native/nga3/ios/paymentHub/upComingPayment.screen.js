const BaseupComingPaymentScreen = require('../../../upComingPayment.screen');

class UpComingPaymentScreen extends BaseupComingPaymentScreen {
    constructor() {
        super();
        this.title = '//XCUIElementTypeOther//XCUIElementTypeOther[@name="Upcoming payments"]';
        this.upComingPayment = (transactionType, account, amount) => `//*[contains(@name, 
            '${account.toUpperCase()} ${transactionType} £${amount}')]`;
    }
}

module.exports = UpComingPaymentScreen;
