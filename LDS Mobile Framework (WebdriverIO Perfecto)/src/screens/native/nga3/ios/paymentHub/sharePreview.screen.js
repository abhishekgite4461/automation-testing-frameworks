const BaseSharePreviewScreen = require('../../../sharePreview.screen');
const helper = require('../../../ios.helper');

class SharePreviewScreen extends BaseSharePreviewScreen {
    constructor() {
        super();
        this.title = helper.byName('paymentsReceiptScreen');
        this.paymentSentHeader = helper.byName('paymentReceiptSubHeader');
        this.sharePreviewShareButton = helper.byName('paymentReceiptButton');
        this.nativeSharePanel = helper.byName('ActivityListView');
        this.nameAndNumberPreview = helper.byName('toAccountDetails');
        this.payerName = helper.byName('fromAccountNameLabel');
        this.beneficiaryName = helper.byName('toAccountNameLabel');
        this.beneficiarySortCode = helper.byName('toSortCodeLabel');
        this.beneficiaryAccountNumber = helper.byName('toSortCodeLabeltoAccountNumberLabel');
        this.bankIcon = '//XCUIElementTypeOther[contains(@name,"paymentsReceiptScreen")]//XCUIElementTypeCell[1]';
        this.paymentReceiptDateValue = (index) => `//*[@value='${index}' and @visible='true']`;
        this.paymentReceiptDateField = helper.byName('dateValue');
        this.emailAppIcon = helper.byPartialName('Mail');
        this.displayAccountDetails = helper.byLabel('Hi. Here are my account details.');
        this.nativeShareCancelButton = helper.byName('Cancel');
        this.referenceValue = helper.byName('referenceValue');
        this.amountValue = helper.byName('amountValue');
        this.emailAppDeleteDraftButton = helper.byName('Delete Draft'); // TODO: Id missing MPT-5433
    }
}

module.exports = SharePreviewScreen;
