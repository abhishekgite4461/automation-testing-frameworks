const BaseStandingOrderSuccessScreen = require('../../../transferAndPayments/standingOrderSuccessful.screen');

const helper = require('../../../ios.helper');

class StandingOrderSuccessfulScreen extends BaseStandingOrderSuccessScreen {
    constructor() {
        super();
        this.standingOrderSuccessHeader = helper.byName('standingOrderSuccessHeader');
        this.viewStandingOrdersButton = helper.byName('paymentViewStandingOrders');
        this.soNotSetupErrorMessage = helper.byName('errorPageMessage');
    }
}

module.exports = StandingOrderSuccessfulScreen;
