const BaseCalendarScreen = require('../../../calendar.screen');
const helper = require('../../../ios.helper');

class CalendarScreen extends BaseCalendarScreen {
    constructor() {
        super();
        this.title = helper.byName('calendarTitle');
        this.nextMonthButton = helper.byName('forwardArrow');
        this.prevMonthButton = helper.byName('backwardArrow');
        this.lastDayOfPayment = (index) => `//*[@name='${index}']`;
        this.monthName = (index) => `//*[@name='${index}' and @visible='true']`;
        this.cancel = helper.byName('Cancel');
    }
}

module.exports = CalendarScreen;
