const BaseSuccessPaymentScreen = require('../../../successPayment.screen');
const helper = require('../../../ios.helper');

class SuccessPaymentScreen extends BaseSuccessPaymentScreen {
    constructor() {
        super();
        this.title = helper.byName('paymentSuccessHeader');
        this.viewTransactionButton = helper.byName('paymentViewTransaction');
        this.makeAnotherPaymentButton = helper.byName('paymentAnotherPayment');
        this.fromDetail = helper.byName('fromAccountDetails');
        this.toDetail = helper.byName('toAccountDetails');
        this.amountDetail = helper.byName('amountValue');
        this.homeButton = helper.byName('homeButton');
        this.dateValue = (index) => `//*[@value='${index}' and @visible='true']`;
        this.paymentHubSuccessPaymentSummary = helper.byName('toAccountDetails');
        this.viewStandingOrders = helper.byName('paymentViewStandingOrders');
        this.notNow = helper.byName('Not Now');
        this.mpaPaymentApprovalSuccessMessage = helper.byLabel('Payment submitted for approval');
        this.shareReceiptButton = helper.byName('paymentReceiptButton');
        this.fromBusinessName = helper.byName('fromBusinessNameLabel');
        this.paymentScheduledTitle = helper.byName('Payment scheduled'); // PAYMOB-2157 raised to add ID property.
        this.paymentProcessed = helper.byPartialValue('payment of');
        this.bannerLeadPlacement = helper.byName('leadWebView');
    }
}

module.exports = SuccessPaymentScreen;
