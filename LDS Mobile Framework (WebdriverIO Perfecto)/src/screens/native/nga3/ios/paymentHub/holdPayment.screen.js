const BaseHoldPaymentScreen = require('../../../declinePayment.screen');
const helper = require('../../../ios.helper');

class HoldPaymentScreen extends BaseHoldPaymentScreen {
    constructor() {
        super();
        this.title = helper.byName('Header');
        this.errorMessage = helper.byPartialValue('your security, we need to speak to you about this payment before');
        this.transferAndPaymentButton = helper.byName('paymentsTransfersButton');
        this.paymentHoldErrorMessage = helper.byPartialValue('For your security, we need to speak to you about this payment ');
        this.callUsOption = helper.byName('callUsButton');
    }
}

module.exports = HoldPaymentScreen;
