const BaseFailureTransferScreen = require('../../../failureTransfer.screen');
const helper = require('../../../ios.helper');

class FailureTransferScreen extends BaseFailureTransferScreen {
    constructor() {
        super();
        this.title = "//*[@name='Header' and not(child::*)]";
        this.internetErrorMessage = helper.byPartialValue("your Internet connection has been lost so we can't confirm this transfer");
        this.transferAndPaymentButton = helper.byName('paymentsTransfersButton');
        this.callUsButton = helper.byName('callUsButton');
        this.isaTransferTitle = helper.byName('errorPageMessage');
        this.moreThanFundingLimitHTBTransferTitle = helper.byName('errorPageHeader');
        this.isaAlreadySubscribedErrorMessage = '//*[contains(@value,"You can’t make this transaction because you’ve already subscribed to") and contains(@value,"ISA in the current tax year")]';
        this.htbIsaAboveFundLimitErrorMessage = '//*[contains(@value,"make more than one deposit in a calendar month") and contains(@value,"to deposit more than £200")]';
    }
}

module.exports = FailureTransferScreen;
