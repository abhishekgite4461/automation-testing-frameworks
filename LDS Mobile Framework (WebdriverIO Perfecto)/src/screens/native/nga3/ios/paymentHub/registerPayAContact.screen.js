const BaseRegisterPayAContactScreen = require('../../../registerPayAContact.screen');
const helper = require('../../../ios.helper');

class RegisterPayAContactScreen extends BaseRegisterPayAContactScreen {
    constructor() {
        super();
        this.title = helper.byName('Register for Pay a Contact');
        this.registerButton = helper.byName('Register');
        this.continueButton = helper.byName('Continue');
        this.agreeCheckBox = '//XCUIElementTypeStaticText[contains(@name,"I have read and agree")]';
        this.continuetoP2PButton = helper.byName('Continue');
        this.continueToP2PAuthentication = '//XCUIElementTypeLink[@name="Continue"]';
        this.P2PsuccessMessage = '//XCUIElementTypeStaticText[@name="Registration successful"]';
        this.lnkP2PSetting = '//XCUIElementTypeStaticText[@name="Go to Pay a Contact settings"]';
        this.lkDeRegisterFromP2P = '//XCUIElementTypeStaticText[@name="De-register from Pay a Contact"]';
        this.authPassword = '//XCUIElementTypeSecureTextField';
        this.continueToDereg = helper.byName('Continue');
        this.deregSuceesMsg = helper.byName('Pay a Contact mobile number de-registration successful');
    }
}

module.exports = RegisterPayAContactScreen;
