const BaseReviewPaymentScreen = require('../../../reviewPayment.screen');
const helper = require('../../../ios.helper');

class ReviewPaymentScreen extends BaseReviewPaymentScreen {
    constructor() {
        super();
        this.title = helper.byName('reviewPaymentHeader');
        this.editButton = helper.byName('reviewPaymentEditButton');
        this.confirmButton = helper.byName('reviewPaymentConfirmButton');
        this.fromDetail = helper.byName('fromAccountDetails');
        this.toDetail = helper.byName('toAccountDetails');
        this.fromAccountNameDetail = helper.byName('fromAccountNameLabel');
        this.fromSortCodeDetail = helper.byName('fromSortCodeLabel');
        this.fromAccountNumberDetail = helper.byName('fromAccountNumberLabel');
        this.toAccountNameDetail = helper.byName('toAccountNameLabel');
        this.toSortCodeDetail = helper.byName('toSortCodeLabel');
        this.toAccountNumberDetail = helper.byName('toAccountNumberLabel');
        this.amountDetail = helper.byName('amountValue');
        this.toCreditCardNumberDetail = helper.byName('toSortCodeLabel');
        this.withdrawFromIsaToNonLbgAccountWarningMessage = helper.byPartialValue('You\'re attempting to withdraw from your tax-free ISA');
        this.dateValue = (index) => `//*[@value='${index}' and @visible='true']`;
        this.isaWarningMessage = helper.byPartialName('You\'re attempting to withdraw from your tax-free ISA');
        this.isaTransferAgreementCheckBox = helper.byName('checkbox');
        this.futurePaymentMessage = helper.byPartialValue('the next working day');
        this.toP2PAccountNameDetail = helper.byName('toAccountNameLabel');
        this.toMobileNum = helper.byName('toSortCodeLabel');
        this.mpaPaymentApprovalRequiredMessage = helper.byPartialValue('Payment will be submitted for approval');
        this.toMortgageAccountDetail = helper.byName('toSortCodeLabel');
        this.subAccountInfo = helper.byName('subAccountValue');
        this.reference = helper.byName('referenceValue');
    }
}

module.exports = ReviewPaymentScreen;
