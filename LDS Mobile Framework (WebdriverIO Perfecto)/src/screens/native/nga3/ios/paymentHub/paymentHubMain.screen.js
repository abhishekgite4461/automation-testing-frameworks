const BasePaymentHubMainScreen = require('../../../paymentHubMain.screen');
const helper = require('../../../ios.helper');

class PaymentHubMainScreen extends BasePaymentHubMainScreen {
    constructor() {
        super();
        this.title = helper.byName('fromAccountDetails');
        this.fromAccountName = helper.byName('fromAccountNameLabel');
        this.toAccountName = helper.byName('toAccountNameLabel');
        this.fromAccountNameDetail = helper.byName('fromAccountNameLabel');
        this.fromSortCodeDetail = helper.byName('fromSortCodeLabelfromAccountNumberLabel');
        this.fromAccountNumberDetail = helper.byName('fromSortCodeLabelfromAccountNumberLabel');
        this.toAccountNameDetail = helper.byName('toAccountNameLabel');
        this.toSortCodeDetail = helper.byName('toSortCodeLabeltoAccountNumberLabel');
        this.toAccountNumberDetail = helper.byName('toSortCodeLabeltoAccountNumberLabel');
        this.fromAccountPane = helper.byName('fromAccountDetails');
        this.toAccountPane = helper.byName('toAccountDetails');
        this.toCreditCardNumberDetail = helper.byName('toSortCodeLabeltoAccountNumberLabel');
        this.toMortgageAccountDetail = helper.byName('toSortCodeLabel');
        this.paymentAccountNameDetail = helper.byName('toAccountNameLabel');
        this.paymentSortCodeDetail = helper.byName('toSortCodeLabel');
        this.paymentAccountNumberDetail = helper.byName('toAccountNumberLabel');
        this.paymentsHubUnavailable = helper.byName('PaymentsHubUnavailableTitle');
        this.paymentsHubUnavailableDetails = helper.byName('You don\'t have an active online account to make a payment or transfer from.');
        this.amount = helper.byName('amountTextbox');
        this.verifySuccessConfirmPage = helper.byName('Pay & transfer');
        this.reference = helper.byName('referenceTextbox');
        this.date = helper.byName('dateField');
        this.selectToAccount = (toExtProvider, toAggAccount) => `//XCUIElementTypeCell[contains(@label, 
            '${toExtProvider}')]//XCUIElementTypeStaticText[@label='${toAggAccount}']`;
        this.calendarButton = helper.byName('dateField');
        this.paymentDate = (index) => `//*[@value='${index}' and @visible='true']`;
        this.helpTextAmount = helper.byName('amountHelpText');
        this.standingOrderToggleSwitch = helper.byName('standingOrderSwitch');
        this.continueButton = helper.byName('continueButton');
        this.fasterPaymetWarning = helper.byName('errorPageMessage');
        this.fasterPaymetWarningPopUp = helper.byPartialLabel('problem with our payments');
        this.statusLink = helper.byName('alertActionCancelButton');
        this.headerHomeButton = helper.byName('homeButton');
        this.insufficientMoneyErrorMessage = helper.byPartialLabel('enough money');
        this.transferLimitExceededErrorMessage = helper.byPartialLabel('limit exceeded');
        this.closeErrorMessage = helper.byName('closeIcon');
        this.internetTransactionLimitMessage = '//*[contains(@label, "is more than you can transfer online") or contains(@label,"is more than the transfer transaction limit")'
            + ' or contains(@label, "is more than the online transfer limit")]';
        this.accountDailyLimitMessage = '//*[contains(@label, "this amount is more than the online daily limit for this account")]';
        this.monthlyCapLimitMessage = '//*[contains(@label, "payment exceeds the monthly limit") or contains(@label,"payment is more than the monthly amount")]';
        this.cbsProhibitiveMessage = helper.byPartialLabel('Sorry, you can\'t complete this request online. Please call us on');
        this.toAccount = helper.byLabel('Choose who to pay');
        this.paymentAccountLocator = (fromAccount) => `//*[@label='${fromAccount}'and @name="viewRemitterAccountName"]`;
        this.toolTipForMinimumAndMaximumDailyLimit = '//*[contains(@label, "Limit: Min £1, Max Daily £300")]';
        this.done = helper.byName('done');
        this.beneficiaryAccountDetail = helper.byLabel('toAccountDetails');
        this.errorMessage = helper.byName('errorMessage');
        this.errorMessageForMinimumAndMaximumDailyLimit = helper.byName('errorMessage');
        this.minISATransferErrorMessage = helper.byPartialValue('Sorry, this is less than the minimum amount you can transfer into your ISA. Please transfer a higher');
        this.maxISATransferErrorMessage = helper.byPartialValue('Transferring this amount would either exceed your remaining ISA allowance');
        this.frequencyPicker = helper.byName('standingOrderFrequencyLabel');
        this.startDate = helper.byName('From:');
        this.endDate = helper.byName('Until:');
        this.remitterAmount = () => helper.byName('fromBalanceLabel');
        this.recipientAmount = () => helper.byName('toBalanceLabel');
        this.paymentHubStandingOrderFirstPaymentDate = helper.byName('standingOrderStartDateTextField');
        this.paymentHubStandingOrderLastPaymentDate = helper.byName('standingOrderEndDateTextField');
        this.frequencySelector = helper.byType('XCUIElementTypePickerWheel');
        this.dateSelector = (date) => helper.byLabel(date);
        this.nextMonthButton = helper.byName('forwardArrow');
        this.previousMonthButton = helper.byName('backwardArrow');
        this.monthPickerWheel = '//XCUIElementTypePicker[1]/XCUIElementTypePickerWheel[1]';
        this.yearPickerWheel = '//XCUIElementTypePicker[2]/XCUIElementTypePickerWheel[1]';
        this.yearPickerWheelOnly = '//XCUIElementTypePickerWheel';
        this.warningMessageTitle = helper.byName('alertTitle');
        this.cancelCalendar = helper.byName('Cancel');
        this.monthYearPicker = '//*[@label="backwardArrow"]/../XCUIElementTypeButton[2]';
        this.standingOrderReference = helper.byName('referenceTextbox');
        this.warningMessageContent = helper.byPartialName('Make sure your standing order');
        this.warningMessageContentHTB = helper.byPartialName('You can make an initial');
        this.additionalOptionCreditCard = helper.byName('selectAmountButton');
        this.minimumAmountCreditCard = helper.byName('minimumPayment');
        this.statementBalanceCreditCard = helper.byName('statementBalance');
        this.statementBalanceWithInstalmentPlanCreditCard = helper.byPartialName('minimum amount plus instalment plan payment');
        this.minimumAmountWithInstalmentPlanCreditCard = helper.byPartialName('main balance plus instalment plan payment');
        this.selectFrequencyDropdown = helper.byName('standingOrderFrequencyLabel');
        this.toolTipForReferenceOnPaymentHub = helper.byName('referenceHelpText');
        this.toolTipForCorporatePayee = helper.byPartialLabel('I change this reference?');
        this.toP2PAccountNameDetail = helper.byName('toAccountNameLabel');
        this.paymentHubRecipientsList = helper.byName('RecipientList');
        this.toMobileNum = helper.byName('toSortCodeLabel');
        this.fraudsterWarning = helper.byPartialLabel('Are you sure you know where your money is going?');
        this.timeLapseWarningErrorMessage = '//*[contains(@label,"For security please wait until 60 minutes")]';
        this.timeLapseWithOverPaymentWarningErrorMessage = '//*[contains(@label,"before making a payment over £1000 to a new recipient.")]';
        this.creditCardValidationErrorMessage = helper.byPartialLabel('Error message.Sorry, you can\'t make a payment to your credit card account greater than the outstanding balance.');
        this.noticeWarningMessage = helper.byLabel('Account charge notice');
        this.p2pToAccountNameDetail = helper.byName('toAccountNameLabel');
        this.clickFrom = helper.byName('fromLabel');
        this.clickTo = helper.byName('toAccountDetails');
        this.fromAccountList = helper.byPartialName('viewRemitterAccountNumber');
        this.fromExternalAccount = (extProvider, extAccount) => `//XCUIElementTypeCell[contains(@label,'${extProvider}')]
            //XCUIElementTypeStaticText[contains(@label,'${extAccount}')]`;
        this.eligibleOwnAccount = (accountType) => `//XCUIElementTypeStaticText
            [contains(@label,'${accountType}') and @name='viewRecipientTransferAccountName']`;
        this.subAccountToggle = helper.byName('mortgagePaySubAccountSwitch');
        this.subAccountList = helper.byName('subAccountsCell');
        this.subAccountDetails = helper.byName('subAccountsDescription');
        this.learnAboutOverpaymentToolTip = helper.byName('learnAboutMortgageOverpaymentViewModelText');
        this.subAccountToolTip = helper.byName('learnAboutSubAccountsViewModelText');
        this.informationAboutOverpaymentPopup = helper.byLabel('AlertView');
        this.preSelectedSubAccount = helper.byName('subAccountsCell');
        this.sortCodeAndAccountNumber = (accountSortCode, accountNumber) => `//XCUIElementTypeStaticText[contains(@label,
            '${accountSortCode}')]/following-sibling::XCUIElementTypeStaticText[contains(@label,'${accountNumber}')]`;
        this.monthInCalendar = (month, year) => helper.byLabel(`${month} ${year}`);
        this.overDraftAmount = () => helper.byName('fromBalanceLabel');
        this.viewPaymentLimits = '//*[@label="View payment limits"]'; // TODO : PJO-9424
    }
}

module.exports = PaymentHubMainScreen;
