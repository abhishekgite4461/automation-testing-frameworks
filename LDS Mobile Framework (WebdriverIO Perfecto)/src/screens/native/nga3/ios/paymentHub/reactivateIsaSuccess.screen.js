const BaseReactivateIsaSuccessScreen = require('../../../reactivateIsaSuccess.screen.js');
const helper = require('../../../ios.helper');

class ReactivateIsaSuccessScreen extends BaseReactivateIsaSuccessScreen {
    constructor() {
        super();
        this.title = helper.byName('ISA reactivated');
        // TODO: MOB3-12585 IOS and android we don't have proper IDs for reactivateISA
        this.reactivateIsaSuccessHeader = helper.byName('ISA Reactivation Success');
        this.reactivateIsaSupportingCopy = helper.byName('Your Cash ISA account is now reactivated and ready for you to start saving again. You can only fund one ISA in a tax year.');
        // TODO: MOB3-12585 IOS and android we don't have proper IDs for reactivateISA
        this.reactivateIsaRemainingAllowanceText = helper.byName('Remaining Allowance:');
        this.reactivateIsaRemainingAllowanceAmount = helper.byName('reactivateIsaRemainingBalanceText');
        this.reactivateIsaAddMoreMoneyButton = helper.byLabel('Add More Money');
        // TODO: MOB3-12585 IOS and android we don't have proper IDs for reactivateISA
        this.reactivateIsaAccountNameText = helper.byName('Account:');
        this.reactivateIsaAccountSortCode = helper.byName('reactivateIsaAccountSortCode');
        this.reactivateIsaAccountNumber = helper.byName('reactivateIsaAccountNumber');
        this.reactivateIsaToVerifyExternalTransferButton = helper.byName('reactivateIsaAccountNumber');
        this.reactivateIsaInstructionalMessage = helper.byName('reactivateIsaInfoLabelIdentifier');
    }
}

module.exports = ReactivateIsaSuccessScreen;
