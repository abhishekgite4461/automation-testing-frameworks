const BaseReactivateIsaSuccessScreen = require('../../../reactivateIsa.screen');
const helper = require('../../../ios.helper');

class ChangeAddressModal extends BaseReactivateIsaSuccessScreen {
    constructor() {
        super();
        this.title = helper.byName('Update Your Address');// TODO: MQE-1018
    }
}

module.exports = ChangeAddressModal;
