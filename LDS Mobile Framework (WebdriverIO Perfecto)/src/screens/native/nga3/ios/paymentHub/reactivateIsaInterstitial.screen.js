const BaseReactivateIsaInterstitialScreen = require('../../../reactivateIsaInterstitial.screen');
const helper = require('../../../ios.helper');

class ReactivateIsaInterstitialScreen extends BaseReactivateIsaInterstitialScreen {
    constructor() {
        super();
        // TODO: MOB3-12748 Add automation Id
        this.title = helper.byName('Reactivate ISA');
        this.reactivateIsaButton = helper.byName('reactivateIsaConfirmButton');
    }
}

module.exports = ReactivateIsaInterstitialScreen;
