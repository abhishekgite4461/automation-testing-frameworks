const BaseDeclinePaymentScreen = require('../../../declinePayment.screen');
const helper = require('../../../ios.helper');

class DeclinePaymentScreen extends BaseDeclinePaymentScreen {
    constructor() {
        super();
        this.title = helper.byName('errorPageHeader');
        this.errorMessage = helper.byName('errorPageMessage');
        this.preAuthButton = helper.byName('preAuthMenu');
        this.logonButton = helper.byName('errorPageContinueButton');
        this.pageHeader = 'payment blocked';
    }
}

module.exports = DeclinePaymentScreen;
