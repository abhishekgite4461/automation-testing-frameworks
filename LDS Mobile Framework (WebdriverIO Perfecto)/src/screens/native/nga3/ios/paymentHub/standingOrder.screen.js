const BaseStandingOrderScreen = require('../../../standingOrder.screen');
const helper = require('../../../ios.helper');

class StandingOrderScreen extends BaseStandingOrderScreen {
    constructor() {
        super();
        this.standingOrderTab = helper.byName('Standing orders');
        this.directDebitTab = '//XCUIElementTypeLink[@name="Direct Debits"]';
        this.amendButton = '(//XCUIElementTypeLink[@name="Amend"])[1]';
        this.deleteButton = '(//XCUIElementTypeLink[@name="Delete"])[1]';
        this.savingAccountName = helper.byName('INST ACCESS ONLINE');
        this.dueDate = helper.byName('Next due:');
        this.reference = helper.byName('Ref:');
        this.paidTo = helper.byName('Paid to:');
        this.declineSoPage = helper.byLabel('Standing order declined');
        this.exitInternetBanking = helper.byName('exitButton');
        this.underReviewSoPage = helper.byLabel('Standing order under review');
        this.selectViewStandingOrderOption = helper.byName('standingOrdersButton');
    }
}

module.exports = StandingOrderScreen;
