const BaseStandingOrderAmendScreen = require('../../../standingOrderAmend.screen');
const helper = require('../../../ios.helper');

class StandingOrderAmendScreen extends BaseStandingOrderAmendScreen {
    constructor() {
        super();
        this.title = '//XCUIElementTypeStaticText[@name="Confirm standing order amendments"]';
        this.soReference = '(//XCUIElementTypeOther[preceding-sibling::XCUIElementTypeOther[@name="Ref"]]/XCUIElementTypeTextField)[1]';
        this.soAmount = '(//XCUIElementTypeOther[preceding-sibling::XCUIElementTypeOther[@name="Amount"]]/XCUIElementTypeTextField)[1]';
        this.soPassword = '//XCUIElementTypeOther[preceding-sibling::XCUIElementTypeOther[contains(@name,"password")]]/XCUIElementTypeSecureTextField';
        this.soAmendButton = helper.byName('Amend');
        this.soConfirmAmendButton = helper.byName('Confirm amendments');
        this.soChangeSucessTitle = '//XCUIElementTypeOther[@name="Standing order changed"]';
        this.soChangeSucessMessage = helper.byName('Your standing order has been successfully changed.');
        this.soViewStandingOrdersButton = '//XCUIElementTypeLink[@name="View standing orders"]';
    }
}

module.exports = StandingOrderAmendScreen;
