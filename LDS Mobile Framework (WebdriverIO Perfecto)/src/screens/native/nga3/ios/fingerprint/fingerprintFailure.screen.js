const BaseFingerprintRiskRejectionScreen = require('../../../fingerprint/fingerprintFailure.screen');
const helper = require('../../../ios.helper');

class FingerprintRiskRejectionScreen extends BaseFingerprintRiskRejectionScreen {
    constructor() {
        super();
        this.title = helper.byName('fingerprintRiskRejectionAlert');
        this.okButton = helper.byName('alertActionConfirmButton');
    }
}

module.exports = FingerprintRiskRejectionScreen;
