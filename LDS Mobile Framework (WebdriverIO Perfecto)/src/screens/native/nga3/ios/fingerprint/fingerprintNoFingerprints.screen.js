const BaseFingerprintNoFingerprintsScreen = require('../../../fingerprint/fingerprintNoFingerprints.screen');
const helper = require('../../../ios.helper');

class FingerprintNoFingerprintsScreen extends BaseFingerprintNoFingerprintsScreen {
    constructor() {
        super();
        this.title = `${helper.byName('fingerprintsNotFoundAlert')}/descendant::*[2]`;
        this.okButton = helper.byName('alertActionConfirmButton');
    }
}

module.exports = FingerprintNoFingerprintsScreen;
