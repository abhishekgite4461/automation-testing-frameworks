const BaseFingerprintInterstitialScreen = require('../../../fingerprint/fingerprintInterstitial.screen');
const helper = require('../../../ios.helper');

class FingerprintInterstitialScreen extends BaseFingerprintInterstitialScreen {
    constructor() {
        super();
        // Banner title property is invisible. Hence keeping page title
        this.title = '//*[contains(@label, "Touch ID is fast and safe.") or contains(@label, "Face ID is fast and safe.")]';
        this.yesButton = helper.byName('fingerprintInterstitialOptInButton');
        this.noButton = helper.byName('fingerprintInterstitialUseMiButton');
    }
}

module.exports = FingerprintInterstitialScreen;
