const BaseFingerprintLoginScreen = require('../../../fingerprint/fingerprintLogin.screen');
const helper = require('../../../ios.helper');

class FingerprintLoginScreen extends BaseFingerprintLoginScreen {
    constructor() {
        super();
        this.title = helper.byName('MesaGlyph');
        this.cancel = helper.byLabel('Cancel');
    }
}

module.exports = FingerprintLoginScreen;
