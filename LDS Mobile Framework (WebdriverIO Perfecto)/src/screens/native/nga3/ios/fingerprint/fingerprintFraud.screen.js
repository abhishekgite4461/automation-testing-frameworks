const BaseFingerprintFraudScreen = require('../../../fingerprint/fingerprintFraud.screen');
const helper = require('../../../ios.helper');

class FingerprintFraudScreen extends BaseFingerprintFraudScreen {
    constructor() {
        super();
        this.title = helper.byName('fingerprintRiskAlert');
        this.yesButton = helper.byName('alertActionConfirmButton');
        this.noButton = helper.byName('alertActionCancelButton');
    }
}

module.exports = FingerprintFraudScreen;
