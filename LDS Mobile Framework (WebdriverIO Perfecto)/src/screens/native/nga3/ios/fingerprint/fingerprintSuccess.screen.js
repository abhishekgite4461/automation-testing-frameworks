const BaseFingerprintSuccessScreen = require('../../../fingerprint/fingerprintSuccess.screen');
const helper = require('../../../ios.helper');

class FingerprintSuccessScreen extends BaseFingerprintSuccessScreen {
    constructor() {
        super();
        this.title = helper.byName('fingerprintOptedInAlert');
        this.okButton = helper.byName('alertActionConfirmButton');
    }
}

module.exports = FingerprintSuccessScreen;
