const BaseSwitchScreen = require('../../switch.screen');
const helper = require('../../ios.helper');

class SwitchScreen extends BaseSwitchScreen {
    constructor() {
        super();
        this.title = '//*[@label="functional" or @label="FUNCTIONAL"]';
        this.switchToggle = (name) => helper.byName(`${name} switch`);
        this.nextButton = helper.byLabel('Next');
    }
}

module.exports = SwitchScreen;
