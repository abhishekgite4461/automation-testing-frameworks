const BaseMobileBrowserScreen = require('../../mobileBrowser.screen');
const helper = require('../../ios.helper');

class MobileBrowserScreen extends BaseMobileBrowserScreen {
    constructor() {
        super();
        this.title = helper.byName('BrowserWindow');
        this.browserUrl = '//*[@label="Address"]';
    }
}

module.exports = MobileBrowserScreen;
