const BaseMemorableInformationScreen = require('../../memorableInformation.screen');
const helper = require('../../ios.helper');

class MemorableInformationScreen extends BaseMemorableInformationScreen {
    constructor() {
        super();
        this.title = helper.byLabel('Memorable Information');
        this.iBRegMiTitle = helper.byLabel('Code confirmed');
        this.memorableCharacter = (ordinal) => helper.byPartialLabel(`Enter the ${ordinal}`);
        this.firstOrdinal = `${helper.byType('XCUIElementTypeSecureTextField')}[1]`;
        this.secondOrdinal = `${helper.byType('XCUIElementTypeSecureTextField')}[2]`;
        this.thirdOrdinal = `${helper.byType('XCUIElementTypeSecureTextField')}[3]`;
        this.miCustomKey = (key) => helper.byName(key);
        this.firstMemorableCharacter = helper.byName('enterMemorableInformationInput0');
        this.memorableInformationCharacter = (ordinal) => helper.byName(`enterMemorableInformationInput${ordinal}`);
        this.miTextboxes = helper.byType('XCUIElementTypeSecureTextField');
        this.fscsTitle = helper.byName('enterMIFSCS');
        this.miForgotYourPassword = helper.byName('forgotLogonDetailsLink');
        this.miCharacters = (ordinal) => `${helper.byPartialLabel('Enter the')}[${ordinal}]`;
        this.continueToMyAccounts = helper.byName('leadsInterstitialConfirmAndContinueButton');
        this.touchButton = helper.byPartialLabel('Not now');
        this.dataConsent = helper.byName('analyticsConsentInterstitial');
        this.acceptAll = helper.byName('analyticsConsentAcceptAllButton');
        this.tooltipLink = helper.byName('Help');
        this.closeToolTip = helper.byName('alertActionConfirmButton');
        this.tooltipPopup = '//*[@name="AlertView"]/child::*/child::*';
        this.expressLogonUnavailableTitle = helper.byName('ExpressLogonUnavailableTitle');
        this.expressLogonUnavailableOKButon = helper.byName('alertActionConfirmButton');
        this.manageDataConsent = helper.byName('analyticsConsentDecriptionLabel');
        this.keepUsingMI = helper.byName('fingerprintInterstitialUseMiButton');
    }
}

module.exports = MemorableInformationScreen;
