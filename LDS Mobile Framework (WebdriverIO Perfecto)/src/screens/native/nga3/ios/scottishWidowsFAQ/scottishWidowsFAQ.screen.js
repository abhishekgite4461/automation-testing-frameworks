const BaseScottishWidowsFAQScreen = require('../../../scottishWidowsFAQ/scottishWidowsFAQ.screen');
const helper = require('../../../ios.helper');

class ScottishWidowsFAQScreen extends BaseScottishWidowsFAQScreen {
    constructor() {
        super();
        // TODO COE-99
        this.title = helper.byName('FAQs');
        this.whatcanIdonextOption = helper.byName('What can I do next?');
        this.scottishWidowHelpDeskNumber = helper.byPartialValue('0345 769 7318');
    }
}

module.exports = ScottishWidowsFAQScreen;
