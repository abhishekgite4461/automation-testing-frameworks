const BaseModifySpendingRewardsWebViewScreen = require('../../modifySpendingRewardsWebViewScreen.screen');
const helper = require('../../ios.helper');

class ModifySpendingRewardsWebViewScreen extends BaseModifySpendingRewardsWebViewScreen {
    constructor() {
        super();
        this.title = helper.byName('Everyday Offers');
    }
}

module.exports = ModifySpendingRewardsWebViewScreen;
