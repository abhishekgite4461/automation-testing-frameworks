const BaseReEnterMemorableInformationScreen = require('../../reEnterMemorableInformation.screen');
const helper = require('../../ios.helper');

class ReEnterMemorableInformationScreen extends BaseReEnterMemorableInformationScreen {
    constructor() {
        super();
        this.title = helper.byLabel('Memorable Information');
        this.memorableCharacter = (ordinal) => helper.byPartialLabel(`Enter the ${ordinal}`);
        this.wrongMIError = helper.byPartialValue('Please re-enter your details and try again.');
        this.closeButton = helper.byName('closeIcon');
        this.wrongPasswordError = helper.byPartialValue('8000067');
        this.firstMemorableCharacter = helper.byName('enterMemorableInformationInput0');
        this.fscsTitle = helper.byName('enterMIFSCS');
    }
}

module.exports = ReEnterMemorableInformationScreen;
