const BaseEnrolmentEIABypassScreen = require('../../enrolmentEIABypass.screen');
const helper = require('../../ios.helper');

class EnrolmentEIABypassScreen extends BaseEnrolmentEIABypassScreen {
    constructor() {
        super();
        this.title = helper.byName('enrolmentEIABypassTitle');
        this.continueButton = helper.byName('enrolmentEIABypassContinueButton');
    }
}

module.exports = EnrolmentEIABypassScreen;
