const BaseLegalAndPrivacyScreen = require('../../../../settings/legalAndPrivacy.screen');
const helper = require('../../../../ios.helper');

class LegalAndPrivacyScreen extends BaseLegalAndPrivacyScreen {
    constructor() {
        super();
        this.title = helper.byPartialLabel('The Card Services App is designed for use in the UK');
    }
}

module.exports = LegalAndPrivacyScreen;
