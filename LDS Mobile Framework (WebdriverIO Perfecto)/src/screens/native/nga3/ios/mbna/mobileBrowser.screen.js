const BaseMobileBrowserScreen = require('../mobileBrowser.screen');

class MobileBrowserScreen extends BaseMobileBrowserScreen {
    constructor() {
        super();
        this.brandUrl = 'mbna.co.uk';
        this.returnToApp = '//*[@label="Return to MBNA"]';
    }
}

module.exports = MobileBrowserScreen;
