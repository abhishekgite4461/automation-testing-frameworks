const BaseAppStoreScreen = require('../appStore.screen');
const helper = require('../../../ios.helper');

class AppStoreScreen extends BaseAppStoreScreen {
    constructor() {
        super();
        this.appText = helper.byLabel('MBNA Card Services app');
        this.appHeaderVersion = helper.byPartialName('search the app');
    }
}

module.exports = AppStoreScreen;
