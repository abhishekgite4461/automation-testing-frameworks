const BaseLogoutScreen = require('../logout.screen');
const helper = require('../../../ios.helper');

class LogoutScreen extends BaseLogoutScreen {
    constructor() {
        super();
        this.appResetcompleteMessage = helper.byLabel('Thanks for using the Card Services App.');
    }
}

module.exports = LogoutScreen;
