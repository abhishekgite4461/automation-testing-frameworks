const BaseAppStoreScreen = require('../../appStore.screen');
const helper = require('../../ios.helper');

class AppStoreScreen extends BaseAppStoreScreen {
    constructor() {
        super();
        this.title = '//*[@name="ProductPageExtension.ProductView" or @label="App Store"]';
        this.DoneOrCancelButton = '//*[@label="Cancel" or @label="Done"]';
        this.appHeaderVersion = helper.byPartialName('new app search');
    }
}

module.exports = AppStoreScreen;
