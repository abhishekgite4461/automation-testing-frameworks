const BaseEnrolmentCallScreen = require('../../enrolmentCall.screen');
const helper = require('../../ios.helper');

class EnrolmentCallScreen extends BaseEnrolmentCallScreen {
    constructor() {
        super();
        this.title = helper.byLabel('Security call');
        this.eiaPhoneNumbers = helper.byName('eia_number_selection_numbers_table');
        this.callMeNowButton = helper.byName('enrollmentCallMeNowButton');
        this.yourPhone = helper.byName('your numberPhoneNumber');
        this.homePhone = helper.byName('homePhoneNumber');
        this.mobilePhone = helper.byName('mobilePhoneNumber');
        this.workPhone = helper.byName('workPhoneNumber');
        this.cancelButton = helper.byName('enrollmentCancelButton');
        this.exitYes = helper.byName('alertActionConfirmButton');
        this.exitCancel = helper.byName('alertActionCancelButton');
        this.eiaCallingTitle = helper.byLabel('Calling now');
        this.eiaPin1 = helper.byName('eia_calling_pin_1');
        this.eiaPin2 = helper.byName('eia_calling_pin_2');
        this.eiaPin3 = helper.byName('eia_calling_pin_3');
        this.eiaPin4 = helper.byName('eia_calling_pin_4');
        this.accept = helper.byLabel('Accept');
        this.keypad = helper.byLabel('keypad');
        this.dialNumber = (label) => helper.byLabel(label);
        this.numberRadioButton = (numberType) => helper.byName(`Your ${numberType.toUpperCase()} number is`);
        this.selectPhoneNumber = (number) => `(//*[contains(@value, ${number})])[1]`;
        this.selectRadioButton = (number) => `(//*[contains(@value, ${number})] 
        /following::*[contains(@name,"eia_number_cell_radio_button")])[1]`;
    }
}

module.exports = EnrolmentCallScreen;
