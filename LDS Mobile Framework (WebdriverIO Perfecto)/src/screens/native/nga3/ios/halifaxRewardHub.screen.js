const halifaxRewardHub = require('../../halifaxRewardHub.screen.js');
const helper = require('../../ios.helper');

class RewardHub extends halifaxRewardHub {
    constructor() {
        super();
        this.title = helper.byName('rewardsHubHeader');
    }
}

module.exports = RewardHub;
