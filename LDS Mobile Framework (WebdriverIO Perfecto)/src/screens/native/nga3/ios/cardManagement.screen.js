const BaseCardManagementScreen = require('../../cardManagement.screen');
const helper = require('../../ios.helper');

class CardManagementScreen extends BaseCardManagementScreen {
    constructor() {
        super();
        this.title = helper.byPartialName('Card Management Page');
        this.freezCardTransactions = helper.byPartialName('Freeze card transactions');
        this.lostAndStolenCards = helper.byPartialName('Lost or stolen cards');
        this.replaceCardAndPin = helper.byPartialName('Replace card and PIN.');
    }
}

module.exports = CardManagementScreen;
