const BaseCommonScreen = require('../../../common.screen');
const helper = require('../../../ios.helper');

class CommonScreen extends BaseCommonScreen {
    constructor() {
        super();
        this.backButton = '//XCUIElementTypeNavigationBar/XCUIElementTypeButton[1]';
        this.modalBackButton = '//*[@name="CrossBarButton"]/preceding-sibling::XCUIElementTypeButton';
        this.homeButton = helper.byLabel('homeButton');
        this.contactUs = helper.byName('CallUsBarButton');
        this.cancelLink = helper.byName('viewRecipientCancelButton');
        this.modalCloseIcon = '//XCUIElementTypeNavigationBar/XCUIElementTypeButton[@name="CrossBarButton"]';
        this.winBackTitle = '(//*[@name="alertActionCancelButton"]/../../../XCUIElementTypeOther[1])[last()]';
        this.winBackStayButton = '(//XCUIElementTypeButton[@name="alertActionCancelButton"])[last()]';
        this.winBackLeaveButton = '(//XCUIElementTypeButton[@name="alertActionConfirmButton"])[last()]';
        this.winBackOkButton = helper.byName('alertActionConfirmButton');
        this.winBackCancelButton = helper.byName('alertActionCancelButton');
        this.winBackCloseButton = helper.byName('alertActionConfirmButton');
        this.headerBackButton = '//XCUIElementTypeNavigationBar/XCUIElementTypeButton[1]';
        this.loadingSpinner = helper.byName('spinnerLoadingTitle');
        this.errorMessageOkButton = '//*[@resource-id="android:id/button1"]';
        this.pleaseWaitSpinner = helper.byLabel('CaptionLabel');
        this.checkingDetailsSpinner = helper.byLabel('CaptionLabel');
        this.accountNameMatch = helper.byLabel('CaptionLabel');
    }
}

module.exports = CommonScreen;
