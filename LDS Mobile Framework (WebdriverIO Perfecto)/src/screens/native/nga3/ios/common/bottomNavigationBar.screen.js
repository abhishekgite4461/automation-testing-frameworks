const BaseBottomNavigationBar = require('../../../../common/bottomNavigationBar.screen');
const helper = require('../../../ios.helper');

class BottomNavigationBarScreen extends BaseBottomNavigationBar {
    constructor() {
        super();
        this.homeTab = helper.byLabel('Home');
        this.applyTab = '//*[contains(@name,"Tab.Apply") or contains(@name,"Apply")]';
        this.payAndTransferTab = '//*[contains(@name,"Tab.Payments") or contains(@name,"Pay & transfer")]';
        this.supportTab = helper.byLabel('Support');
        this.moreTab = helper.byLabel('More');
    }
}

module.exports = BottomNavigationBarScreen;
