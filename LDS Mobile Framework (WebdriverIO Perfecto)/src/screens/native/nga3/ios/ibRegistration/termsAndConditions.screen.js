const BaseTnCScreen = require('../../../ibRegistration/termsAndConditions.screen');
const helper = require('../../../ios.helper');

class TermsAndConditionsScreen extends BaseTnCScreen {
    constructor() {
        super();
        this.title = helper.byName('termsAndConditionsScreen');
        this.agreeButton = helper.byName('registrationPrimaryButton');
        this.backButton = helper.byName('registrationSecondaryButton');
        this.footerSection = helper.byName('termsAndConditionsFooterLabel');
    }
}

module.exports = TermsAndConditionsScreen;
