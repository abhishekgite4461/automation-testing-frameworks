const BasePostCodeErrorScreen = require('../../../ibRegistration/postCodeError.screen.js');
const helper = require('../../../ios.helper');

class PostCodeErrorScreen extends BasePostCodeErrorScreen {
    constructor() {
        super();
        this.postCodeTitle = helper.byName('registrationTitle');
        this.postCode = helper.byName('postCodeInput');
        this.nextButton = helper.byName('registrationPrimaryButton');
    }
}

module.exports = PostCodeErrorScreen;
