const BasePersonalDetailsScreen = require('../../../ibRegistration/personalDetails.screen.js');
const helper = require('../../../ios.helper');

class PersonalDetailsScreen extends BasePersonalDetailsScreen {
    constructor() {
        super();
        this.title = helper.byName('registrationTitle');
        this.errorMessage = helper.byName('errorMessage');
        this.firstName = helper.byName('personalDetailsFirstNameInput');
        this.lastName = helper.byName('personalDetailsLastNameInput');
        this.emailId = helper.byName('personalDetailsEmailInput');
        this.dateOfBirth = helper.byName('personalDetailsDateOfBirthInput');
        this.postCode = helper.byName('personalDetailsPostcodeInput');
        this.postCodeTip = helper.byName('tipMessageView');
        this.backButton = helper.byName('registrationSecondaryButton');
        this.nextButton = helper.byName('registrationPrimaryButton');
        this.errorMessageDismiss = helper.byName('closeIcon');
        this.dobOkButton = helper.byLabel('Done');
        this.dateField = helper.byTypeAndIndex('XCUIElementTypePickerWheel', '1');
        this.monthField = helper.byTypeAndIndex('XCUIElementTypePickerWheel', '2');
        this.yearField = helper.byTypeAndIndex('XCUIElementTypePickerWheel', '3');
        this.youthErrorMessage = helper.byName('alertTitle');
        this.backButtonDOBError = helper.byName('alertActionConfirmButton');
    }
}

module.exports = PersonalDetailsScreen;
