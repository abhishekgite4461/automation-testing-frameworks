const BaseCreateMemorableInfoScreen = require('../../../ibRegistration/createMemorableInfo.screen.js');
const helper = require('../../../ios.helper');

class CreateMemorableInfoScreen extends BaseCreateMemorableInfoScreen {
    constructor() {
        super();
        this.title = helper.byName('Memorable information');
        this.memorableInfo = helper.byName('miInput');
        this.confirmMemorableInfo = helper.byName('reEnterMIInput');
        this.memorableInfoTip = helper.byName('miTip');
        this.confirmMemorableInfoTip = helper.byName('reEnterMITip');
        this.hideShowMemorableInfo = helper.byName('showHideButton');
        this.hideShowConfirmMemorableInfo = helper.byName('showHideButton');
        this.findOutMoreMemorableInfo = helper.byName('readMoreButton');
        this.findOutMoreTitle = helper.byName('alertTitle');
        this.nextButton = helper.byName('registrationPrimaryButton');
        this.backButton = helper.byName('registrationSecondaryButton');
        this.findOutMoreTipClose = helper.byName('alertActionConfirmButton');
        this.customeKeyboardDeleteButton = helper.byName('Delete');
    }
}

module.exports = CreateMemorableInfoScreen;
