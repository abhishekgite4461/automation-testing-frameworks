const BaseCreateSecureLogonDetailsScreen = require('../../../ibRegistration/createSecureLogonDetails.screen.js');
const helper = require('../../../ios.helper');

class CreateSecureLogonDetailsScreen extends BaseCreateSecureLogonDetailsScreen {
    constructor() {
        super();
        this.title = helper.byName('registrationTitle');
        this.userId = helper.byName('loginDetailsUserIdInput');
        this.userIdCheck = helper.byName('checkButton');
        this.userIdOptionsTitle = helper.byName('dialogUserIdSuggestionsMessage');
        this.userIdOptionsSuggestion = helper.byName('dialogUserIdSuggestions1');
        this.userIdOptionsBack = helper.byName('Back');
        this.password = helper.byName('loginDetailsPasswordInput');
        this.hideShowPassword = helper.byName('showHideButton');
        this.passwordfindOutMore = helper.byVisibleName('readMoreButton');
        this.findOutMorePopUp = helper.byName('alertTitle');
        this.findOutMorePopUpClose = helper.byName('alertActionConfirmButton');
        this.confirmPassword = helper.byName('loginDetailsReenterPasswordInput');
        this.hideShowConfirmPassword = helper.byName('showHideButton');
        this.userIdTip = helper.byName('loginDetailsUserIdTip');
        this.passwordTip = helper.byName('loginDetailsPasswordTip');
        this.confirmPasswordTip = helper.byName('loginDetailsReenterPasswordTip');
        this.commercialAccountMessage = helper.byName('existingUsernameTip');
        this.commercialUserId = helper.byName('loginDetailsCommercialMandateUserIdText');
        this.keyboardDeleteButton = helper.byName('delete');
        this.nextButton = helper.byName('registrationPrimaryButton');
        this.errorIcon = undefined;
        this.loadingIndicator = helper.byName('spinnerLoadingTitle');
    }
}

module.exports = CreateSecureLogonDetailsScreen;
