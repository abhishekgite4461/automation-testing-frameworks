const Screen = require('../../../../screen');
const helper = require('../../../ios.helper');

class HelpDeskScreen extends Screen {
    constructor() {
        super();
        this.helpCMSTitle = helper.byName('errorHeader');
        this.helpDeskError = helper.byName('errorDescription');
        this.helpDeskCMSError = helper.byName('errorDescription');
    }
}

module.exports = HelpDeskScreen;
