const BaseRegistrationSuccessScreen = require('../../../ibRegistration/registrationSuccess.screen');
const helper = require('../../../ios.helper');

class RegistrationSuccessScreen extends BaseRegistrationSuccessScreen {
    constructor() {
        super();
        this.title = helper.byLabel('Congratulations');
        this.subTitle = helper.byName('registrationSuccessSubtitle');
        this.continueButton = helper.byName('registrationSuccessContinueButton');
        this.successMessage = helper.byName('registrationSuccessBody');
        this.footerMessage = helper.byName('registrationSuccessFooter');
    }
}

module.exports = RegistrationSuccessScreen;
