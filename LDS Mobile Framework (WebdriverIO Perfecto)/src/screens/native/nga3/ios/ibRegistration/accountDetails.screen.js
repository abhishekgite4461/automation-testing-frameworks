const BaseAccountDetailsScreen = require('../../../ibRegistration/accountDetails.screen.js');
const helper = require('../../../ios.helper');

class AccountDetailsScreen extends BaseAccountDetailsScreen {
    constructor() {
        super();
        this.title = helper.byName('registrationTitle');
        this.errorMessage = helper.byName('errorMessage');
        this.errorMessageDismiss = helper.byName('closeIcon');
        this.accountType = helper.byName('accountDetailsAccountTypePicker');
        this.accountTypeSelector = helper.byType('XCUIElementTypePickerWheel');
        this.sortcodeSection = helper.byName('sortCodeInputField');
        this.sortcodeFirstSection = helper.byName('sortCodeBoxOne');
        this.sortcodeSecondSection = helper.byName('sortCodeBoxTwo');
        this.sortcodeThirdSection = helper.byName('sortCodeBoxThree');
        this.accountNumber = helper.byName('accountDetailsAccountNumberInput');
        this.loanReferenceNumber = helper.byName('accountDetailsLoanNumberInput');
        this.loanTip = helper.byName('accountDetailsLoanNumberTip');
        this.mortgageNumber = helper.byName('accountDetailsMortgageNumberInput');
        this.mortgageTip = helper.byName('accountDetailsMortgageNumberTip');
        this.creditCardNumber = helper.byName('accountDetailsCreditCardNumberInput');
        this.creditCardTip = helper.byName('accountDetailsCreditCardNumberTip');
        this.nextButton = helper.byName('registrationPrimaryButton');
        this.backButton = helper.byName('registrationSecondaryButton');
        this.keyboardDeleteButton = helper.byPartialName('elete');
    }
}

module.exports = AccountDetailsScreen;
