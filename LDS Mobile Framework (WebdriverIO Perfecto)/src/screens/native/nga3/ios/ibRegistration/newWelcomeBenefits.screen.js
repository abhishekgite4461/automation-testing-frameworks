const BaseNewWelcomeBenefitsScreen = require('../../../welcomeBenefits.screen');
const helper = require('../../../ios.helper');

class NewWelcomeBenefitsScreen extends BaseNewWelcomeBenefitsScreen {
    constructor() {
        super();
        this.title = helper.byLabel('Welcome');
        this.registerButton = helper.byName('registerButton');
        this.registerForIbButton = helper.byName('loginRegisterForIb');
        this.logOnButton = helper.byName('loginButton');
        this.webTitle = helper.byName('Select an account for registration');
        this.fraudLink = 'guarantee';
        this.demoLink = 'video on setting up the app';
        this.customerLink = helper.byName('welcomeSection4Body');
        this.alertDialog = helper.byName('alertTitle');
        this.yesLeaveTheApp = helper.byName('alertActionConfirmButton');
        this.cancelLeaveTheApp = helper.byName('alertActionCancelButton');
        this.fraudGuaranteeTitle = helper.byValue('Lloyds Banking Group PLC, secure and validated connection');
        this.appDemoTitle = helper.byValue('https://www.youtube.com/embed/XFgEYu5qf68?autoplay=1&rel=0&autohide=1&showinfo=0&wmode=transparent');
        this.newCustomerTitle = helper.byValue('https://www.lloydsbank.com/current-accounts.asp??=#current-accounts');
        this.legalInfoButton = helper.byName('legalInformationButton');
    }
}

module.exports = NewWelcomeBenefitsScreen;
