const BaseActivationCodeScreen = require('../../../ibRegistration/activationCode.screen');
const helper = require('../../../ios.helper');

class ActivationCodeScreen extends BaseActivationCodeScreen {
    constructor() {
        super();
        this.title = helper.byName('registrationTitle');
        this.activationCode = helper.byName('registrationActivationCodeInputField');
        this.confirmCode = helper.byName('registrationPrimaryButton');
        this.requestNewCode = helper.byName('registrationSecondaryButton');
        this.errorMessage = helper.byName('errorMessage');
    }
}

module.exports = ActivationCodeScreen;
