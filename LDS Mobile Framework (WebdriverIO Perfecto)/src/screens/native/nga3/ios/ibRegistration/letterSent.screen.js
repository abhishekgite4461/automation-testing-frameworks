const BaseLetterSentScreen = require('../../../ibRegistration/letterSent.screen');
const helper = require('../../../ios.helper');

class LetterSentScreen extends BaseLetterSentScreen {
    constructor() {
        super();
        this.letterSentTitle = helper.byLabel('New code ordered');
        this.registeredUsername = helper.byName('registrationLetterSentUserIdLabel');
        this.letterSentText = helper.byName('registrationLetterSentMessage');
    }
}

module.exports = LetterSentScreen;
