const BaseInternationalPaymentScreen = require('../../internationalPayment.screen');
const helper = require('../../ios.helper');

class InternationalPaymentScreen extends BaseInternationalPaymentScreen {
    constructor() {
        super();
        this.title = '//XCUIElementTypeButton[@name="Select an international recipient"]';
        this.recipients = '//XCUIElementTypeStaticText[@name="International payments recipients"]';
        this.makePayment = '(//XCUIElementTypeStaticText[@name="Make payment"])[1]';
        this.ipAmount = '//XCUIElementTypeStaticText[contains(@name,"equivalent")]';
        this.ipAmountField = '//XCUIElementTypeOther[preceding-sibling::XCUIElementTypeOther'
            + '//XCUIElementTypeOther[contains(@name,"equivalent")]]'
            + '/XCUIElementTypeTextField';
        this.ipContinue = helper.byName('Continue');
        this.confirmIPPayment = '//XCUIElementTypeOther[@name="Confirm payment details"]';
        this.ipTermsAndConditions = helper.byPartialName('to the payment');
        this.ipPassword = '//XCUIElementTypeSecureTextField';
        this.ipConfirm = helper.byName('Continue');
        this.ipPaymentConfirmation = '//XCUIElementTypeStaticText[@name="International payment confirmed"]';
        this.txtBicSwift = '//XCUIElementTypeTextField';
        this.contuneButton = helper.byName('Continue');
        this.recipientBankAddr = '(//XCUIElementTypeTextField)[1]';
        this.IBAN = '(//XCUIElementTypeTextField)[4]';
        this.recipientName = '(//XCUIElementTypeTextField)[5]';
        this.recipientAddr = '(//XCUIElementTypeTextField)[6]';
        this.continueRecipientDetailsButton = helper.byName('Continue');
        this.confirmButton = helper.byName('Confirm');
        this.authenticationPage = '//XCUIElementTypeStaticText[@name="Confirm international recipient’s details"]';
        this.radioButton = '//XCUIElementTypeStaticText[contains(@name,"Mobile")]';
        this.successPage = '//XCUIElementTypeStaticText[@name="International recipient successfully created"]';
    }
}

module.exports = InternationalPaymentScreen;
