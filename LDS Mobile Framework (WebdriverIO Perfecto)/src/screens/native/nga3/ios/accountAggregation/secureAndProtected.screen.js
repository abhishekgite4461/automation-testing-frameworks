const BaseSecureAndProtectedScreen = require('../../../accountAggregation/secureAndProtected.screen');
const helper = require('../../../ios.helper');

class SecureAndProtectedScreen extends BaseSecureAndProtectedScreen {
    constructor() {
        super();
        this.secureAndProtectedTitle = helper.byName('accAggSecurityHeader');
        this.secureAndProtectedFirstInformationPoint = helper.byName('accAggSecurityfirstInfo');
        this.secureAndProtectedSecondInformationPoint = helper.byName('accAggSecuritySecondInfo');
        this.secureAndProtectedThirdInformationPoint = helper.byName('accAggSecurityThirdInfo');
        this.secureAndProtectedNextButton = helper.byName('accAggSecurityNextButton');
    }
}

module.exports = SecureAndProtectedScreen;
