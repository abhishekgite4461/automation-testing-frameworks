const BaseSetUpIn3StepsScreen = require('../../../accountAggregation/setUpIn3Steps.screen');
const helper = require('../../../ios.helper');

class SetUpIn3StepsScreen extends BaseSetUpIn3StepsScreen {
    constructor() {
        super();
        this.setUpIn3StepsTitle = helper.byName('firstDescriptionLabel');
        this.setUpIn3StepsNextButton = helper.byName('selectProviderButton');
        this.setUpIn3StepsProviderImage = helper.byName('bank_icon');
        this.setUpIn3StepsProviderDescription = helper.byName('firstDescriptionLabel');
        this.setUpIn3StepsLoginProviderImage = helper.byName('mobile_icon');
        this.setUpIn3StepsLoginProviderDescription = helper.byName('secondDescriptionLabel');
        this.setUpIn3StepsProviderPermissionImage = helper.byName('secure_icon');
        this.setUpIn3StepsProviderPermissionDescription = helper.byName('thirdDescriptionLabel');
    }
}

module.exports = SetUpIn3StepsScreen;
