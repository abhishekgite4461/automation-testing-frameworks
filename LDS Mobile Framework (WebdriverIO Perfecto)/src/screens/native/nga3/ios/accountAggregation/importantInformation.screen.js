const BaseImportantInformationScreen = require('../../../accountAggregation/importantInformation.screen');
const helper = require('../../../ios.helper');

class ImportantInformationScreen extends BaseImportantInformationScreen {
    constructor() {
        super();
        this.importantInformationGraphicImage = helper.byName('accAggConsentGraphicImage');
        this.importantInformationText = helper.byName('accAggConsentHeadingText');
        this.importantInformationFirstBulletDescription = helper.byName('accAggConsentFirstBulletDescription');
        this.importantInformationSecondBulletDescription = helper.byName('accAggConsentSecondBulletDescription');
        this.importantInformationThirdBulletDescription = helper.byName('accAggConsentThirdBulletDescription');
        this.importantInformationForthBulletDescription = helper.byName('accAggConsentFourthBulletDescription');
        this.importantInformationTnC = helper.byName('accAggConsentTandCCheckBox');
        this.importantInformationTnCDesciption = helper.byNameAndIndex('analyticsConsentFooterLink', 4);
        this.importantInformationConsent = helper.byName('accAggConsentCheckBox');
        this.importantInformationConsentDescription = "//*[@name='accAggConsentCheckBox']/parent::*//*[contains(@type, 'Text') and contains(@value, 'accept')]";
        this.importantInformationConfirmButton = helper.byName('accAggConsentNextButton');
        this.importantInformationRenewConsent = helper.byName('renewConsentCheckBox');
    }
}

module.exports = ImportantInformationScreen;
