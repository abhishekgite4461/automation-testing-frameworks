const BaseAccountProviderScreen = require('../../../accountAggregation/accountProvider.screen');
const helper = require('../../../ios.helper');

class ConsentSettingsScreen extends BaseAccountProviderScreen {
    constructor() {
        super();
        this.consentSettingsPage = helper.byName('ConsentManagementScreen');
        this.mainHeaderTitle = helper.byName('consentManagementHeader');
        this.introductoryPara = helper.byName('consentManagementBody');
        this.accountsSectionHeader = helper.byName('consentManagementConsentsInfo');
        this.consentProviderLabel = (bankName) => `//*[@name='sectionTitle' and @label='${bankName}']`;
        this.consentExpiryMessage = (bankName) => `//*[@name='manageConsentCell-${bankName}']//*[@name='messageLabel']`;
        this.providerManageButton = (bankName) => `//*[@name='manageConsentCell-${bankName}']//*[@name='manageButton']`;
        this.authorisedAccounts = (bankName) => `//*[@name='manageConsentCell-${bankName}']/following::*
            //*[@name='accAggManageAccountName' and contains(@label,'${bankName}')]`;
        this.consentStatusFailedMessage = (bankName) => `//*[@name='manageConsentCell-${bankName}']
            /following::*//*[@name='errorNoAccounts']`;
        this.consentStatusExpiredMessage = (bankName) => `//*[@name='manageConsentCell-${bankName}']
            /following::*//*[@name='expiredMessage']`;
        this.consentStatusRevokedMessage = (bankName) => `//*[@label='${bankName}']/following::*
            //*[@name='revokedMessage' and contains(@label, '${bankName}')]`;
        this.providerManageDrawer = helper.byName('ActionMenuTableView');
        this.providerManageDrawerHeader = (bankName) => `//*[@name='actionMenuHeader' and 
            contains(@label,'${bankName}')]`;
        this.providerManageDrawerList = "//*[contains(@name,'ConsentOptionCell')]/child::*[1]";
        this.providerManageDrawerOption = (option) => `//*[contains(@name,'ConsentOptionCell')]
            /child::*[@label='${option}']`;
        this.removeConsentAlert = helper.byName('AlertView');
        this.removeConsentAlertHeader = helper.byName('alertTitle');
        this.consentSettingErrorBanner = helper.byName('errorMessage');
        this.consentSettingErrorBannerCloseIcon = helper.byName('closeIcon');
    }
}

module.exports = ConsentSettingsScreen;
