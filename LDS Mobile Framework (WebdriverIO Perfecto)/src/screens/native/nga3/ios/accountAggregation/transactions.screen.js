const BaseTransactionsScreen = require('../../../accountAggregation/transactions.screen');
const helper = require('../../../ios.helper');

class TransactionsScreen extends BaseTransactionsScreen {
    constructor() {
        super();
        this.viewTransactionsPage = helper.byValue('View Transactions');
        this.successDeepLink = helper.byValue('Renew Consent- Natwest');
        this.failureDeepLink = helper.byValue('Renew Consent Failure');
        this.loggedOffError = helper.byName('errorPageHeader');
    }
}

module.exports = TransactionsScreen;
