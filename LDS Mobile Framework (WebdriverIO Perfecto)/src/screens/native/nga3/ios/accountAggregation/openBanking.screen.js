const BaseOpenBankingScreen = require('../../../accountAggregation/openBanking.screen');
const helper = require('../../../ios.helper');

class OpenBankingScreen extends BaseOpenBankingScreen {
    constructor() {
        super();
        this.screenTitle = (screenTitle) => `//*[contains(@name,"${screenTitle}")]
            /child::*[contains(@name,"${screenTitle}")]`;
        this.openBankingInformationHeader = helper.byName('accAggInfoHeader');
        this.openBankingInformationHeaderDescription = helper.byName('accAggInfoHeaderDescription');
        this.openBankingNextButton = helper.byName('accAggInfoConfirmButton');
        this.backIcon = '//*[@name="CrossBarButton"]/preceding-sibling::XCUIElementTypeButton';
        this.crossIcon = helper.byName('CrossBarButton');
    }
}

module.exports = OpenBankingScreen;
