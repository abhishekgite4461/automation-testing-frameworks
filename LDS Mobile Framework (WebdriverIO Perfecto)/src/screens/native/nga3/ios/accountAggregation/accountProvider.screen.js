const BaseAccountProviderScreen = require('../../../accountAggregation/accountProvider.screen');
const helper = require('../../../ios.helper');

class AccountProviderScreen extends BaseAccountProviderScreen {
    constructor() {
        super();
        this.accountProviderTitle = helper.byName('accAggProvidersPageTitle');
        this.accountProviderListView = helper.byName('accAggProvidersListView');
        this.accountProviderTextView = helper.byNameAndIndex('accAggProviderItemTextView', 1);
        this.providerBankName = (bankName) => `//*[@label="${bankName}" and @name="accAggProviderItemTextView"]`;
        this.accountProviderImageView = helper.byNameAndIndex('accAggProviderItemImageView', 1);
        this.accountProviderFooterTitle = helper.byName('accAggProvidersFooterTitle');
        this.accountProviderFooterMessage = helper.byName('accAggProvidersFooterMessage');
    }
}

module.exports = AccountProviderScreen;
