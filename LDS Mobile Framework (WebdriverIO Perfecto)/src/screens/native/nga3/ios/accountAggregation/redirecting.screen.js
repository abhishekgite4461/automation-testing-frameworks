const BaseRedirectingScreen = require('../../../accountAggregation/redirecting.screen');
const helper = require('../../../ios.helper');

class RedirectingScreen extends BaseRedirectingScreen {
    constructor() {
        super();
        this.redirectLoadingProgress = helper.byName('accAggRedirectLoadingProgress');
        this.redirectText = helper.byName('accAggRedirectText');
        this.continueButton = helper.byName('alertActionConfirmButton');
        this.cancelButton = helper.byName('alertActionCancelButton');
        this.loggedOutErrorScreen = helper.byName('loggedOutErrorScreen');
        this.logOffErrorScreenTitle = helper.byLabel('errorPageHeader');
        this.redirectExternalBrowserErrorScreen = helper.byName('accAggRedirectView');
        this.loggingOffUserErrorScreen = helper.byName('errorLogoffPage');
        this.redirectExternalBrowserErrorScreenTitle = helper.byName('accAggRedirectIncompleteInfoText');
        this.openAppButton = helper.byLabel('Open');
        this.browserAddress = '//*[@label="Address"]';
        this.redirectHomeButton = helper.byName('accAggRedirectHomeButton');
        this.accessLocationButton = helper.byLabel('Allow');
        this.backToAppButtonOld = helper.byPartialLabel('Return to');
        this.backToAppButtonNew = helper.byPartialLabel('◀︎');
    }
}

module.exports = RedirectingScreen;
