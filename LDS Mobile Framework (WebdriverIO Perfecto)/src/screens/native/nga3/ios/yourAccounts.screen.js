const BaseYourAccountsScreen = require('../../yourAccounts.screen');
const helper = require('../../ios.helper');

class YourAccountsScreen extends BaseYourAccountsScreen {
    constructor() {
        super();
        this.title = helper.byName('accountTile0');
        this.savingsAccountName = (srtCodeAcctNo) => helper.byPartialLabel(`${srtCodeAcctNo}`);
    }
}

module.exports = YourAccountsScreen;
