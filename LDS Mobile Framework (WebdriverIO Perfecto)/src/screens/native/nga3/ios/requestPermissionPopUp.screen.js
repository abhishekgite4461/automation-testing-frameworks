const BaseRequestPermissionPopUpScreen = require('../../requestPermissionPopUp.screen');
const helper = require('../../ios.helper');
const DeviceHelper = require('../../../../utils/device.helper');

class RequestPermissionPopUpScreen extends BaseRequestPermissionPopUpScreen {
    constructor() {
        super();
        this.title = helper.byLabel(`Allow "${DeviceHelper.getAppName()}" to access your 
            location while you are using the app?`);
        this.permissionAcceptButton = helper.byLabel('Allow');
    }
}

module.exports = RequestPermissionPopUpScreen;
