const BaseStagingOrderWebviewScreen = require('../../../amendOrDeleteStandingOrder/standingOrderWebview.screen');
const helper = require('../../../ios.helper');

class StandingOrderWebviewScreen extends BaseStagingOrderWebviewScreen {
    constructor() {
        super();
        this.webviewTitle = helper.byName('WebViewScreen');
    }
}

module.exports = StandingOrderWebviewScreen;
