const BaseDirectDebitVTDScreen = require('../../../amendOrDeleteStandingOrder/directDebitVTD.screen');
const helper = require('../../../ios.helper');

class DirectDebitVTDScreen extends BaseDirectDebitVTDScreen {
    constructor() {
        super();
        this.title = helper.byName('name');
        this.directDebitDelete = helper.byName('deleteButton');
        this.merchantVTD = helper.byName('nameValue');
        this.amountVTD = helper.byName('amountValue');
        this.referenceVTD = helper.byName('referenceValue');
        this.frequencyVTD = helper.byName('frequencyValue');
        this.lastPaid = helper.byName('lastPaidValue');
        this.nextPaid = helper.byName('nextDueValue');
    }
}

module.exports = DirectDebitVTDScreen;
