const BaseStandingOrderVTDScreen = require('../../../amendOrDeleteStandingOrder/standingOrderVTD.screen');
const helper = require('../../../ios.helper');

class StandingOrderVTDScreen extends BaseStandingOrderVTDScreen {
    constructor() {
        super();
        this.title = helper.byName('standingOrder');
        this.standingOrderDelete = helper.byName('deleteButton');
        this.standingOrderAmend = helper.byName('amendButton');
        this.MerchantVTD = helper.byName('nameValue');
        this.AmountVTD = helper.byName('amountValue');
        this.accountNoVTD = helper.byName('accountValue');
        this.referenceVTD = helper.byName('referenceValue');
        this.FrequencyVTD = helper.byName('frequencyValue');
        this.LastPaid = helper.byName('lastPaidValue');
        this.NextPaid = helper.byName('nextDueValue');
        this.endDate = helper.byName('endDateValue');
    }
}

module.exports = StandingOrderVTDScreen;
