const BaseNativeAmendStandingOrderScreen = require('../../../amendOrDeleteStandingOrder/nativeAmendStandingOrder.screen');
const helper = require('../../../ios.helper');

class NativeAmendStandingOrderScreen extends BaseNativeAmendStandingOrderScreen {
    constructor() {
        super();
        this.pageTitle = (screenTitle) => `//*[contains(@name,'${screenTitle}')]
            /child::*[contains(@name,'${screenTitle}')]`;
        this.selectedStandingOrderAccount = helper.byName('viewRemitterAccountName');
        this.deleteButtonForStandingOrder = (standingOrderName) => `${this.accountNameForStandingOrder(standingOrderName)}
            /../child::*[@name='standingOrderSecondaryButton' and @label='Delete']`;
        this.amendButtonForStandingOrder = (standingOrderName) => `${this.accountNameForStandingOrder(standingOrderName)}
            /../child::*[@name='standingOrderPrimaryButton' and @label='Amend']`;
        this.accountNameForStandingOrder = (standingOrderName) => `//*[@name='standingOrderTitle' and 
            @label='${standingOrderName}']`;
        this.setUpStandingOrderButton = helper.byName('continueButton');
        this.directDebitsTitle = helper.byName('directDebits');
        this.merchantName = helper.byName('directDebitTitle');
        this.deleteOption = helper.byName('Delete');
        this.nextDueDateDD = helper.byName('secondDetailIdentifier');
        this.amount = helper.byName('directDebitAmount');
        this.frequency = helper.byName('directDebitFrequency');
        this.noStandingOrdersTitle = helper.byName('noStandingOrderHeader');
        this.noDirectDebitsHeader = helper.byName('noDirectDebitsHeader');
        this.unableToLoadDirectDebits = helper.byName('directDebitsErrorMessage');
        this.winbackText = helper.byPartialLabel('delete the standing order');
        this.standingOrdesTitle = helper.byLabel('editButton');
        this.firstSOAmount = helper.byName('standingOrderAmount');
        this.firstStandingOrder = helper.byName('amendableCell');
        this.firstDirectDebit = '//*[@label="Direct Debits"]/following::*[@name="directDebitCell"][1]';
    }
}

module.exports = NativeAmendStandingOrderScreen;
