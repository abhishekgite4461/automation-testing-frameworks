const BaseDeleteDirectDebitInfoScreen = require('../../../amendOrDeleteStandingOrder/deleteDirectDebitInfo.screen');
const helper = require('../../../ios.helper');

class DeleteDirectDebitInfoScreen extends BaseDeleteDirectDebitInfoScreen {
    constructor() {
        super();
        this.deleteDDInfoPage = helper.byName('DeleteDirectDebitScreenSubTitle');
        this.getHelpButton = helper.byLabel('Get help'); // TODO:PJO-9501
        this.deleteDDButton = helper.byLabel('Delete');
        this.keepDDButton = helper.byLabel('Keep');
    }
}

module.exports = DeleteDirectDebitInfoScreen;
