const BaseMobileBrowserScreen = require('../../mobileBrowser.screen');

class mobileBrowserScreen extends BaseMobileBrowserScreen {
    constructor() {
        super();
        this.brandUrl = 'online-business.bankofscotland.co.uk';
        this.returnToApp = '//*[@label="Return to Bank of Scot"]';
    }
}

module.exports = mobileBrowserScreen;
