const BaseStandingOrderScreen = require('../../../paymentHub/standingOrder.screen');
const helper = require('../../../../../ios.helper');

class StandingOrderScreen extends BaseStandingOrderScreen {
    constructor() {
        super();
        this.savingAccountName = helper.byName('BB INST ONLINE');
    }
}

module.exports = StandingOrderScreen;
