const BaseSoftTokenScreen = require('../../../softToken.screen');
const helper = require('../../../ios.helper');

class SoftTokenScreen extends BaseSoftTokenScreen {
    constructor() {
        super();
        this.title = helper.byName('softTokenTitleLabel');
        this.passCode = helper.byName('enterSoftTokenInputField');
        this.continueButton = helper.byName('enterSoftTokenContinueButton');
        this.softTokenInfoLink = helper.byName('softTokenHelpLabel');
        this.whatisSoftTokenOverlay = helper.byName('softTokenAlertTitle');
        this.closeButton = helper.byName('alertActionConfirmButton');
        this.incorrectPasscodeErrorMessage = helper.byPartialValue('8000065: The passcode you entered is not correct');
        this.closeErrorBannerIcon = helper.byName('closeIcon');
        this.incorrectPasscodeMaxAttemptsReachedErrorMessage = helper.byPartialValue('9320100: You have entered too many incorrect passcode');
    }
}

module.exports = SoftTokenScreen;
