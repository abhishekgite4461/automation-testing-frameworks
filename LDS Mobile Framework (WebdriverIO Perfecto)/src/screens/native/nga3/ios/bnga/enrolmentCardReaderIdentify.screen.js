const BaseEnrolmentCardReaderIdentifyScreen = require('../../../enrolmentCardReaderIdentify.screen');
const helper = require('../../../ios.helper');

class EnrolmentCardReaderIdentifyScreen extends BaseEnrolmentCardReaderIdentifyScreen {
    constructor() {
        super();
        this.title = helper.byName('cardReaderSubtitle');
        this.paymentCardReaderAuthenticationTitle = helper.byLabel('Card reader SIGN');
        this.troubleCardReader = helper.byName('cardReaderHelpLink');
        this.helpPopUpCancelButton = helper.byName('alertActionCancelButton');
        this.helpPopUpContinueButton = helper.byName('alertActionConfirmButton');
        this.dialogTitle = helper.byName('alertTitle');
        this.lastFiveCardDigitField = helper.byName('cardNumberDigitsUserInput');
        this.passcodeField = helper.byName('enterPasscodeUserInput');
        this.closeErrorBanner = helper.byName('closeIcon');
        this.continue = helper.byName('continueButton');
        this.invalidCardError = helper.byPartialValue('1000022');
        this.expiredCardError = helper.byPartialValue('1000022');
        this.suspendedCardError = helper.byPartialValue('9200161');
        this.oldCardExpiredError = helper.byPartialValue('1000023');
        this.cardAlreadyExistError = helper.byPartialValue('1000021');
        this.incorrectPasscodeError = helper.byPartialValue('Sorry, the details you’ve entered are incorrect. Please re-enter your details and try again.');
    }
}

module.exports = EnrolmentCardReaderIdentifyScreen;
