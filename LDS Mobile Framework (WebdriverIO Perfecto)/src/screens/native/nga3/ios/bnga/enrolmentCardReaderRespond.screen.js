const BaseEnrolmentCardReaderRespondScreen = require('../../../enrolmentCardReaderRespond.screen');
const helper = require('../../../ios.helper');

class EnrolmentCardReaderRespondScreen extends BaseEnrolmentCardReaderRespondScreen {
    constructor() {
        super();
        this.title = helper.byName('Registration');
        this.paymentCardReaderAuthenticationTitle = helper.byLabel('Card reader SIGN');
        this.lastFiveCardDigitField = helper.byName('cardNumberDigitsUserInput');
        this.challengeCode = helper.byName('cardReaderNumberText');
        this.passcodeField = helper.byName('enterPasscodeUserInput');
        this.continueButton = helper.byName('continueButton');
        this.havingTroubleWithCardReaderLink = helper.byName('cardReaderHelpLink');
        this.havingTroubleWithCardReaderPopUpTitle = helper.byName('alertTitle');
        this.havingTroublePopUpCloseButton = helper.byName('alertActionCancelButton');
        this.havingTroublePopUpContinueButton = helper.byName('alertActionConfirmButton');
        this.invalidCardError = helper.byPartialValue('1000022');
        this.expiredCardError = helper.byPartialValue('1000022');
        this.suspendedCardError = helper.byPartialValue('9200161');
        this.oldCardExpiredError = helper.byPartialValue('1000023');
        this.cardAlreadyExistError = helper.byPartialValue('1000021');
        this.incorrectPasscodeError = helper.byPartialValue('Sorry, the details you’ve entered are incorrect. Please re-enter your details and try again.');
    }
}

module.exports = EnrolmentCardReaderRespondScreen;
