const BaseWelcomeRegisterScreen = require('../../../welcomeRegister.screen');
const helper = require('../../../ios.helper');

class WelcomeRegisterScreen extends BaseWelcomeRegisterScreen {
    constructor() {
        super();
        this.title = helper.byName('What you\'ll need');
        this.getStartedButton = helper.byName('welcomeGetStartedButton');
        this.onlineBankingRegistrationLink = helper.byName('welcomeSection1Body');
        this.infoAboutCardReader = helper.byName('welcomeSection2Body');
        this.whyYouNeedACard = helper.byName('welcomeSection3Body');
        this.dialogTitle = helper.byName('alertTitle');
    }
}

module.exports = WelcomeRegisterScreen;
