const BaseMobileBrowserScreen = require('../../mobileBrowser.screen');

class mobileBrowserScreen extends BaseMobileBrowserScreen {
    constructor() {
        super();
        this.brandUrl = 'onlinebusiness.lloydsbank.co.uk';
        this.returnToApp = '//*[@label="Return to Bank of Scot"]';
    }
}

module.exports = mobileBrowserScreen;
