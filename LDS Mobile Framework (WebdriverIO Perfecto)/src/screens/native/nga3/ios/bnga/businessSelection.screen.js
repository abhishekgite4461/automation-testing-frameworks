const BaseBusinessSelectionScreen = require('../../../businessSelection.screen');
const helper = require('../../../ios.helper');

class BusinessSelectionScreen extends BaseBusinessSelectionScreen {
    constructor() {
        super();
        this.title = helper.byName('businessSelectionSubheader');
        this.businessName = helper.byName('businessTitle');
        this.businessSelectionSignatoryHeader = "//XCUIElementTypeOther[@name='Select business']";
        this.businessSelectionDelegateHeader = "//XCUIElementTypeOther[@label='Delegate and view only Businesses']";
        this.delegateAndViewOnlyBusinesses = "//XCUIElementTypeOther[@label='Delegate and view only Businesses']/../following-sibling::XCUIElementTypeCell[@name='businessTitle']/child::XCUIElementTypeStaticText";
        this.signatoryBusinesses = "//XCUIElementTypeTable/XCUIElementTypeOther[2]/preceding-sibling::*[@name='businessTitle']/child::XCUIElementTypeStaticText";
        this.signatoryBusiness = (businessName) => `//XCUIElementTypeTable/XCUIElementTypeOther[2]/preceding-sibling
        ::XCUIElementTypeCell/XCUIElementTypeStaticText[@label="${businessName}"]`;
        this.delegateBusiness = (businessName) => `//*[@label="Delegate and view only Businesses"]/../following-sibling
        ::XCUIElementTypeCell/XCUIElementTypeStaticText[@label="${businessName}"]`;
        this.firstBusinessName = "//*[@name='businessTitle'][1]";
        this.firstSignatoryBusiness = "//XCUIElementTypeOther[@label='Your Businesses']/../following-sibling::XCUIElementTypeCell[@name='businessTitle'][1]";
        this.firstDelegateBusiness = "//XCUIElementTypeOther[@label='Delegate and view only Businesses']/../following-sibling::XCUIElementTypeCell[@name='businessTitle'][1]";
    }
}

module.exports = BusinessSelectionScreen;
