const BaseAccountStatementScreen = require('../../statements/accountStatement.screen.js');
const helper = require('../../../../ios.helper.js');

class AccountStatementScreen extends BaseAccountStatementScreen {
    constructor() {
        super();
        this.noteText = helper.byLabel('NOTE: Pending cheques will show in your account balance but you can only use the money when it clears.');
    }
}

module.exports = AccountStatementScreen;
