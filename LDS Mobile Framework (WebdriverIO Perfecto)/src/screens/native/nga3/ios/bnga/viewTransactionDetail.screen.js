const BaseViewTransactionDetailScreen = require('../viewTransactionDetail.screen');
const helper = require('../../../ios.helper.js');

class ViewTransactionDetailScreen extends BaseViewTransactionDetailScreen {
    constructor() {
        super();
        this.debitDisclaimerText = helper.byLabel('This transaction has yet to be taken from your account '
            + 'balance but has been deducted from your available funds.');
        this.creditDisclaimerText = helper.byLabel('Please note. This has yet to be added to your available funds');
    }
}

module.exports = ViewTransactionDetailScreen;
