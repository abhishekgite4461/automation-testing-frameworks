const BaseEnrolmentSecurityScreen = require('../../../enrolmentSecurity.screen');
const helper = require('../../../ios.helper');

class EnrolmentSecurityScreen extends BaseEnrolmentSecurityScreen {
    constructor() {
        super();
        this.title = helper.byPartialLabel('security settings');
        this.continueButton = helper.byName('enrollmentSecuritySettingsSaveButton');
        this.defaultTime = helper.byName('logoffAfterTenMin');
    }
}

module.exports = EnrolmentSecurityScreen;
