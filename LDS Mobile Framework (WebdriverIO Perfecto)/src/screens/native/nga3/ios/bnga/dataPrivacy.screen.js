const BaseDataPrivacyScreen = require('../../../dataPrivacy.screen');
const helper = require('../../../ios.helper');

class DataPrivacyScreen extends BaseDataPrivacyScreen {
    constructor() {
        super();
        this.title = helper.byName('Data privacy');
        this.dataPrivacyNotice = helper.byPartialName('Data Privacy Notice');
        this.cookiePolicy = helper.byName('cookiesLink');
    }
}

module.exports = DataPrivacyScreen;
