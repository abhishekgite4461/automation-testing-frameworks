const BaseCreateMemorableInformationScreen = require('../../../createMemorableInformation.screen');
const helper = require('../../../ios.helper');

class CreateMemorableInformationScreen extends BaseCreateMemorableInformationScreen {
    constructor() {
        super();
        this.title = helper.byName('registrationTitle');
        this.enterMI = helper.byName('miInput');
        this.reEnterMI = helper.byName('reEnterMIInput');
        this.nextButton = helper.byName('registrationPrimaryButton');
    }
}

module.exports = CreateMemorableInformationScreen;
