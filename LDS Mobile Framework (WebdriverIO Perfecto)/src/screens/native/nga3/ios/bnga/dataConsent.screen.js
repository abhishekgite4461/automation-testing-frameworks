const BaseDataConsent = require('../../../dataConsent.screen');
const helper = require('../../../ios.helper');

class DataConsent extends BaseDataConsent {
    constructor() {
        super();
        this.title = helper.byLabel('Data consent');
        this.performanceConsentToggleON = '//*[@name="analyticsConsentsPerformanceToggle"]/*[@name="Yes"]';
        this.performanceConsentToggleOFF = '//*[@name="analyticsConsentsPerformanceToggle"]/*[@name="No"]';
        this.confirmConsent = helper.byName('analyticsConsentConfirmButton');
        this.performanceConsent = helper.byName('analyticsConsentsPerformanceToggle');
        this.winBackTitle = helper.byName('analyticsConsentWinbackPopup');
        this.dialogTitle = helper.byName('analyticsConsentExternalWinbackPopup');
        this.cookiePolicyLink = helper.byName('analyticsConsentFooterLink');
    }
}

module.exports = DataConsent;
