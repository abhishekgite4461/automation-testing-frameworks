const BaseSpendingRewardsCongratulationsPopUpScreen = require('../../spendingRewardsCongratulationPopUp.screen');
const helper = require('../../ios.helper.js');

class SpendingRewardsCongratulationsPopUpScreen
    extends BaseSpendingRewardsCongratulationsPopUpScreen {
    constructor() {
        super();
        this.title = helper.byName('Congratulations');
        this.okButton = helper.byName('alertActionConfirmButton');
    }
}

module.exports = SpendingRewardsCongratulationsPopUpScreen;
