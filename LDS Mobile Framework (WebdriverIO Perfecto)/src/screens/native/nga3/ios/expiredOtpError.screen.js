const BaseExpiredOtpErrorScreen = require('../../expiredOtpError.screen');
const helper = require('../../ios.helper');

class ExpiredOtpErrorScreen extends BaseExpiredOtpErrorScreen {
    constructor() {
        super();
        this.title = helper.byLabel('Problem occurred');
        this.expiredOtpErrorMessage = helper.byPartialValue('Sorry, this One Time Password is no longer valid.');
        this.goToLogonButton = helper.byName('errorPageContinueButton');
    }
}

module.exports = ExpiredOtpErrorScreen;
