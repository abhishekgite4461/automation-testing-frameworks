const BaseFscsScreen = require('../../fscs.screen');
const helper = require('../../ios.helper');

class FscsScreen extends BaseFscsScreen {
    constructor() {
        super();
        this.title = helper.byName('leaveTheAppTitle');
        this.leaveButton = helper.byName('alertActionConfirmButton');
        this.stayButton = helper.byName('alertActionCancelButton');
        this.compensationSchemeLabel = '//XCUIElementTypeOther[@name="Compensation Scheme"]';
    }
}

module.exports = FscsScreen;
