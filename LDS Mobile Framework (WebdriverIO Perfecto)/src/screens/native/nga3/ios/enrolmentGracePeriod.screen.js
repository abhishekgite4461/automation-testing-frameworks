const BaseEnrolmentGracePeriodScreen = require('../../enrolmentGracePeriod.screen');
const helper = require('../../ios.helper');

class EnrolmentGracePeriodScreen extends BaseEnrolmentGracePeriodScreen {
    constructor() {
        super();
        this.title = helper.byName('enrolmentEIABypassTitle');
        this.continueButton = helper.byName('enrolmentEIABypassContinueButton');
    }
}

module.exports = EnrolmentGracePeriodScreen;
