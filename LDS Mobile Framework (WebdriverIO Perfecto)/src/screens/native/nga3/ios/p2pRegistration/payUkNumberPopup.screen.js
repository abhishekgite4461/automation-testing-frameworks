const BasePayUkNumberPopupScreen = require('../../../p2pRegistration/payUkNumberPopup.screen');
const helper = require('../../../ios.helper');

class PayUkNumberPopupScreen extends BasePayUkNumberPopupScreen {
    constructor() {
        super();
        this.title = helper.byName('Paying a UK mobile number'); // // TODO add accessibility IDs for the dialog title for pop up MOB3-10328
        this.close = helper.byName('alertActionCancelButton');
        this.registerNow = helper.byName('alertActionConfirmButton');
    }
}

module.exports = PayUkNumberPopupScreen;
