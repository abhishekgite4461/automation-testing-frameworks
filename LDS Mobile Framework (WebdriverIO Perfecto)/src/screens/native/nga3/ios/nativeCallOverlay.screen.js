const BaseNativeCallOverlayScreen = require('../../nativeCallOverlay.screen');

class NativeCallOverlayScreen extends BaseNativeCallOverlayScreen {
    constructor() {
        super();
        this.title = '//*[contains(@name, "+44") or @value="No SIM Card Installed"]';
    }
}

module.exports = NativeCallOverlayScreen;
