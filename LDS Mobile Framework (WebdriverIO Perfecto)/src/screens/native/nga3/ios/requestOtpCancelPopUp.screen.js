const BaseRequestOtpCancelPopUpScreen = require('../../requestOtpCancelPopUp.screen');
const helper = require('../../ios.helper');

class RequestOtpCancelPopUpScreen extends BaseRequestOtpCancelPopUpScreen {
    constructor() {
        super();
        this.title = helper.byName('requestOTPCancelPopupTitle');
        this.confirmButton = helper.byName('alertActionConfirmButton');
        this.cancelButton = helper.byName('alertActionCancelButton');
    }
}

module.exports = RequestOtpCancelPopUpScreen;
