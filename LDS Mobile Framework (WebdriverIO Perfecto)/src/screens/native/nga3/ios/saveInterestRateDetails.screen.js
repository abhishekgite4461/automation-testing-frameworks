const BaseAccountStatementScreen = require('../../saveInterestRateDetails.screen');
const helper = require('../../ios.helper');

class AccountStatementScreen extends BaseAccountStatementScreen {
    constructor() {
        super();
        this.holdAccountTile = '//*[contains(@label,"Interest rate.")]'; // '//*[@label="Account name. Easy Saver" and @visible="true"][1]';// '//*[@name="accountName" and @visible="true"][1]';
        this.title = helper.byName('statementsBlock'); // To Do need to add proper id for iOS MOB3-4132
        this.interestDetailRow = helper.byName('View interest rate details');
        this.actionMenu = '//*[@name="actionMenu" and @visible="true"]';
        this.closeButton = helper.byName('CrossBarButton');
        this.accountName = helper.byName('interestRateDetailAccountNameLabel');
        this.accountBalance = helper.byName('interestRateDetailBalanceLabel');
        this.accountTypeIcon = helper.byName('interestRateDetailAccountTypeImage');
        this.interestRateLabel = helper.byName('interestRateDetailInterestRateLabel');
        this.interestRateDescription = helper.byName('interestRateDetailInterestDescriptionLabel');
        this.interestRateScreen = '//*[@name="interestRateDetailParentContainer" and @visible="true"]';
        this.interestRateInfo = '(//*[contains(@label,"% gross")])[position()=2]';
        this.interestRateForFirstTier = helper.byName('interestRateDetailList0');
        this.interestRateForSecondTier = helper.byName('interestRateDetailList1');
        this.interestRateForThirdTier = helper.byName('interestRateDetailList2');
    }
}

module.exports = AccountStatementScreen;
