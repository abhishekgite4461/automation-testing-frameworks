const BaseEnvironmentScreen = require('../../environment.screen');
const helper = require('../../ios.helper');

class EnvironmentScreen extends BaseEnvironmentScreen {
    constructor() {
        super();
        this.title = helper.byLabel('Environment Selector');
        this.resetFeatureConfigButton = helper.byLabel('Reset');
        // MQE-1075 TODO
        this.oldResetFeatureConfigButton = helper.byLabel('Reset Feature Configs');
        this.server = (name) => helper.byLabel(name);
        this.clearTextButton = helper.byName('Clear text');
        this.quickStubText = helper.byName('environmentQuickStubText');
        this.quickStubButton = helper.byName('environmentQuickStubButton');
        this.featureConfigText = helper.byName('featureConfigText');
        this.featureConfigGoButton = helper.byName('Customise Feature Configs');
        // Hard coding below value since it is a STUB selector only
        this.unfinishedFeaturesCheckbox = helper.byName('isAnalyticsOptInEnabled switch');
        this.navBackButton = '//XCUIElementTypeNavigationBar/XCUIElementTypeButton[1]';
        this.deenrolledErrorMessage = helper.byPartialValue('Sorry, your device has been de-registered for security reasons.');
        this.deenrolledContinueButton = helper.byName('errorPageContinueButton');
        this.inputAppVersion = helper.byName('appHeaderText');
    }
}

module.exports = EnvironmentScreen;
