const BaseAppWarnScreen = require('../../appWarn.screen');
const helper = require('../../ios.helper');

class AppWarnScreen extends BaseAppWarnScreen {
    constructor() {
        super();
        this.title = helper.byName('appWarnTitle');
        this.updateAppButton = helper.byName('appWarnUpdateAppButton');
        this.continueWithoutUpdating = helper.byName('appWarnAlternateActionButton');
    }
}

module.exports = AppWarnScreen;
