const BaseRequestOtpScreen = require('../../requestOtp.screen');
const helper = require('../../ios.helper');

class RequestOtpScreen extends BaseRequestOtpScreen {
    constructor() {
        super();
        this.title = helper.byName('requestOTPHeader');
        this.confirmButton = helper.byName('requestOTPConfirmButton');
        this.cancelButton = helper.byName('requestOTPCancelButton');
    }
}

module.exports = RequestOtpScreen;
