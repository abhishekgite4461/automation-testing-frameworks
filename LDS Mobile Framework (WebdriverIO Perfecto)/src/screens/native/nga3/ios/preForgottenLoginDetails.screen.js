const BaseForgottenLoginDetailsScreen = require('../../../web/preforgottenLoginDetails.screen');
const helper = require('../../ios.helper');

class ForgottenLoginDetailsScreen extends BaseForgottenLoginDetailsScreen {
    constructor() {
        super();
        this.title = helper.byName("Make sure you've got");
        this.closeButton = helper.byName('Close');
        this.pageTitle = helper.byName("Make sure you've got");
    }
}

module.exports = ForgottenLoginDetailsScreen;
