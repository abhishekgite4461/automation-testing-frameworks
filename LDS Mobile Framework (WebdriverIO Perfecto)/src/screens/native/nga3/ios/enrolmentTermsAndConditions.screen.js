const BaseEnrolmentTermsAndConditions = require('../../enrolmentTermsAndConditions.screen');
const helper = require('../../ios.helper');

class EnrolmentTermsAndConditions extends BaseEnrolmentTermsAndConditions {
    constructor() {
        super();
        this.title = helper.byName('termsAndConditionContainer');
        this.acceptButton = helper.byName('agreeButton');
        this.declineButton = helper.byName('declineButton');
    }
}

module.exports = EnrolmentTermsAndConditions;
