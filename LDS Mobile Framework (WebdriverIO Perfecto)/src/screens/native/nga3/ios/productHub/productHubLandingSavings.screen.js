const BaseProductHubLandingSavingsScreen = require('../../../productHub/productHubLandingSavings.screen');
const helper = require('../../../ios.helper');

class ProductHubLandingSavingsScreen extends BaseProductHubLandingSavingsScreen {
    constructor() {
        super();
        this.title = helper.byName('Compare savings accounts - open an account today');
        this.compareSavings = helper.byValue('Compare savings account');
        this.savingsAccountName = (accountName) => `//XCUIElementTypeStaticText[@name="${accountName}"]`;
        this.select = '//XCUIElementTypeLink[@name="Select"]';
    }
}

module.exports = ProductHubLandingSavingsScreen;
