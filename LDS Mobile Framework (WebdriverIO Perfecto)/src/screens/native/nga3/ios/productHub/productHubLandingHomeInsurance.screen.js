const BaseProductHubLandingHomeInsuranceScreen = require('../../../productHub/productHubLandingHomeInsurance.screen');
const helper = require('../../../ios.helper');

class ProductHubLandingHomeInsuranceScreen extends BaseProductHubLandingHomeInsuranceScreen {
    constructor() {
        super();
        this.homeInsurance = helper.byValue('Home Insurance');
    }
}

module.exports = ProductHubLandingHomeInsuranceScreen;
