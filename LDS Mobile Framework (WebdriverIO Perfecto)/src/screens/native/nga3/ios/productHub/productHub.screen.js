const helper = require('../../../ios.helper');
const BaseProductHubScreen = require('../../../productHub/productHub.screen');

class ProductHubScreen extends BaseProductHubScreen {
    constructor() {
        super();

        this.title = helper.byName('productHubList');
        this.loan = helper.byName('Loans');
        this.currentAccountOverdraft = helper.byName('Overdrafts');
        this.expandIcon = (productName) => `//*[@label='${productName}']`;
        this.collapseIcon = (productName) => `//*[@name='${productName}']`;
        this.featured = helper.byName('Featured');
        this.businessCurrentAccount = '(//XCUIElementTypeStaticText[@name="Business current account"])[1]';
        this.businessSavingsAccount = helper.byName('Business savings account');
        this.cards = helper.byName('Cards');
        this.loans = helper.byName('Loans');
        this.businessInsurance = helper.byPartialName('Business insurance.');
        this.international = helper.byName('International');
        this.businessGuidesAndInfo = helper.byName('Business guides & info');
        this.currentAccounts = helper.byName('Current accounts');
        this.loans = helper.byName('Loans');
        this.creditCards = helper.byName('Credit cards');
        this.savings = helper.byName('Savings');
        this.insurance = helper.byName('Insurance');
        this.mortgages = helper.byName('Mortgages');
        this.featuredExpanded = helper.byName('Featured. 3 options available.');
        this.businessCurrentAccountCollapsed = helper.byPartialName('Business current account.');

        this.loanCalculator = helper.byName('Loan calculator');

        this.compareAccounts = helper.byName('Compare accounts');
        this.overdrafts = '//*[@label="Overdrafts. 1 options available."]';
        this.overdraftsOptions = '//*[preceding-sibling::*[@label="Overdrafts. 1 options available."]//*[@label="Overdrafts"]][1]';
        this.travelMoney = helper.byName('Travel money');
        this.internationalPayments = helper.byName('International payments');

        this.calculator = helper.byName('Calculator');
        this.borrowMore = helper.byName('Borrow more');
        this.personalLoan = helper.byName('Personal loan');
        this.carFinance = helper.byName('Car finance');

        this.compareCreditCards = helper.byName('Compare credit cards');
        this.balanceTransfer = helper.byName('Balance transfer');

        this.compareSavingsAccounts = helper.byName('Compare savings accounts');
        this.instantAccess = helper.byName('Instant access');
        this.topUpISA = helper.byName('Top up ISA');
        this.cashIsa = helper.byName('Cash ISA');
        this.fixedTerm = helper.byName('Fixed term');

        this.vanInsurance = helper.byName('Van insurance');
        this.travelInsurance = helper.byName('Travel insurance');
        this.homeInsurance = helper.byVisibleLabel('Home insurance');
        this.carInsurance = helper.byName('Car insurance');

        this.agreementInPrinciple = helper.byName('Get agreement in principle');
        this.bookBranchAppointment = helper.byName('Book a branch appointment');
        this.switchToNewDeal = helper.byName('Switch to a new deal');
        this.otherMortgageOptions = helper.byName('Other mortgage options');
        this.businessLoan = helper.byName('Business loan');
        this.applyToIncreaseOverDraft = helper.byName('Apply for or increase your overdraft');
        this.discoverAndApplyTitle = helper.byLabel('Discover and apply');
        this.loansAndFinance = helper.byPartialName('Loans');
        this.shareDealing = helper.byName('Share dealing. 2 options available.');
        this.shareDealingOption = '(//XCUIElementTypeStaticText[@name="Share dealing"])[1]';
        this.marketsAndInsights = helper.byPartialName('Markets and Insights');
    }
}

module.exports = ProductHubScreen;
