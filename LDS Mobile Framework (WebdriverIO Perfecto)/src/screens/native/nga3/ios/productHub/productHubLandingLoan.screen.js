const BaseProductHubLandingLoanScreen = require('../../../productHub/productHubLandingLoan.screen');
const helper = require('../../../ios.helper');

class ProductHubLandingLoanScreen extends BaseProductHubLandingLoanScreen {
    constructor() {
        super();
        this.loanCalculator = helper.byValue('Loans');
    }
}

module.exports = ProductHubLandingLoanScreen;
