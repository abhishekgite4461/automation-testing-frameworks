const BaseProductHubLandingCreditCardScreen = require('../../../productHub/productHubLandingCreditCard.screen');
const helper = require('../../../ios.helper');

class ProductHubLandingCreditCardScreen extends BaseProductHubLandingCreditCardScreen {
    constructor() {
        super();
        this.compareCreditCard = helper.byValue('Compare Credit Cards');
    }
}

module.exports = ProductHubLandingCreditCardScreen;
