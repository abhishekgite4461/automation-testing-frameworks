const BaseProductHubApplyLoanScreen = require('../../../productHub/productHubApplyLoan.screen');

class ProductHubApplyLoanScreen extends BaseProductHubApplyLoanScreen {
    constructor() {
        super();
        this.helloTitle = '//XCUIElementTypeStaticText[@name="Hello "]';
        this.continueLoanButton = '//XCUIElementTypeLink[contains(@name,"Continue")]';
        this.loanTitle = '//XCUIElementTypeStaticText[@name="Get an instant loan quote"]';
        this.termField = '(//XCUIElementTypeOther[following-sibling::'
            + 'XCUIElementTypeOther//XCUIElementTypeStaticText[@name="months"]]/XCUIElementTypeTextField)[2]';
        this.loanAmountField = '//XCUIElementTypeTextField[preceding-sibling::'
            + 'XCUIElementTypeOther[@name="I want to borrow *"]]';
        this.selectLoanPurposeDropDown = '(//XCUIElementTypeOther[preceding-sibling::'
            + 'XCUIElementTypeOther[@name="Purpose of loan *"]])[1]';
        this.selectLoanPurposeOption = '//XCUIElementTypePickerWheel';
        this.flexibleLoanButton = '//XCUIElementTypeButton[@name="Personal Loan"]';
        this.personalLoanButton = '//XCUIElementTypeButton[@name="Personal Loan"]';
        this.repaymentOptionTitle = '//XCUIElementTypeStaticText[contains(@name,"repayment option")]';
        this.closestMatchOption = '//XCUIElementTypeButton[@name="Select loan"]';
        this.yourApplicationTitle = '//XCUIElementTypeStaticText[@name="Your application"]';
        this.selectEmploymentStatus = '(//XCUIElementTypeOther[preceding-sibling::'
            + 'XCUIElementTypeOther[@name="Your employment status *"]])[1]';
        this.selectEmploymentOption = 'Retired';
        this.selectEmploymentStatusLabel = 'Please select';
        this.monthlyIncomeField = '(//XCUIElementTypeOther[preceding-sibling::'
            + 'XCUIElementTypeOther/XCUIElementTypeStaticText[contains(@name,"2. What is your monthly income after tax?")]]'
            + '/XCUIElementTypeTextField)[1]';
        this.monthlyOutgoingField = '(//XCUIElementTypeOther[preceding-sibling::XCUIElementTypeOther'
            + '//XCUIElementTypeStaticText[contains(@name,"If you do not pay rent or mortgage each month '
            + 'please enter £0")]]/XCUIElementTypeTextField)[1]';
        this.dependentsField = '//XCUIElementTypeTextField[preceding-sibling::'
            + 'XCUIElementTypeOther/'
            + 'XCUIElementTypeStaticText[@name="4. How many people who live with you depend on you financially?"]]';
        this.childCareCostsField = '//XCUIElementTypeOther[preceding-sibling::XCUIElementTypeOther'
            + '/XCUIElementTypeStaticText[contains(@name,"5. How much do you pay")]]/XCUIElementTypeTextField';
        this.agreementCheckboxOne = '//XCUIElementTypeOther[contains(@name,"not aware of")]';
        this.agreementCheckboxTwo = '//XCUIElementTypeOther[contains(@name,"I agree to manage my loan")]';
        this.agreementForStatements = '//XCUIElementTypeStaticText[contains(@name,"I acknowledge that my")]';
        this.checkEligibilityButton = '//XCUIElementTypeButton[@name="Apply for this loan"]';
        this.loanDeclinePageTitle = '//XCUIElementTypeStaticText[contains(@name,"Sorry")]';
        this.bookAppointmentOption = '//XCUIElementTypeLink[contains(@name,"Book an Appointment")]';
        this.bookAppointment = '//XCUIElementTypeButton[@name="Book an appointment"]';
        this.titleFindBranch = '//XCUIElementTypeStaticText[@name="Find a branch"]';
        this.postcode = '//XCUIElementTypeTextField';
        this.findBranch = '//XCUIElementTypeButton[@name="Find a branch"]';
        this.branchResults = (postcode) => `//XCUIElementTypeStaticText[contains(@name, ${postcode}`;
        this.continueFindBranch = '//XCUIElementTypeLink[@name="Select and continue"]';
        this.titleAppointment = '//XCUIElementTypeStaticText[@name="Choose an appointment time"]';
    }
}

module.exports = ProductHubApplyLoanScreen;
