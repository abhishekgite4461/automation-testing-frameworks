/* eslint-disable max-len */
const BaseProductHubApplySavingsScreen = require('../../../productHub/productHubApplySavings.screen');
const helper = require('../../../ios.helper');

class ProductHubApplySavingsScreen extends BaseProductHubApplySavingsScreen {
    constructor() {
        super();
        this.applyNow = '//XCUIElementTypeButton[contains(@name,"Apply now")]';
        this.acceptTC = '//XCUIElementTypeSwitch';
        this.openAccount = helper.byName('Open account');
        this.accountOpeningSuccessMessage = (accountName) => `//XCUIElementTypeStaticText[@name = 'Your ${accountName} account is now open' or @name = 'Congratulations, you have just opened a ${accountName}']`;
        this.acctAccountNumber = '//XCUIElementTypeStaticText[preceding-sibling::XCUIElementTypeStaticText[contains(@name,"Account number:")]]';
        this.sortCode = '//XCUIElementTypeStaticText[@name="Sort code:"]/following-sibling::XCUIElementTypeStaticText[1]';
        this.accountNum = (accountNum) => `//*[@label="Account number. ${accountNum}"]`;
    }
}

module.exports = ProductHubApplySavingsScreen;
