const BaseProductHubApplyOverdraftScreen = require('../../../productHub/productHubApplyOverdraft.screen');
const helper = require('../../../ios.helper');

class ProductHubApplyOverdraftScreen extends BaseProductHubApplyOverdraftScreen {
    constructor() {
        super();
        this.overdraftPopUpTitle = '//XCUIElementTypeStaticText[@name="About overdrafts"]';
        this.overdraftPageTitle = '//XCUIElementTypeStaticText[@name="Choose an amount"]';
        this.overdraftAmountValue = '(//XCUIElementTypeOther[preceding-sibling::XCUIElementTypeOther'
            + '/XCUIElementTypeStaticText[contains(@name,"could borrow up to")]])[1]/XCUIElementTypeStaticText[2]';
        this.overdraftAmountField = '//XCUIElementTypeTextField[@name="Change overdraft demand"]';
        this.applyButton = '//XCUIElementTypeButton[@name="Apply"]';
        this.employementStatusSelector = '(//XCUIElementTypeOther[@name="Employment status"])[2]';
        this.selectEmployment = 'Retired';
        this.monthlyIncomeAfterTax = '//XCUIElementTypeTextField[@name="Your monthly take home pay and other allowed income"]';
        this.monthlyOutgoings = '//XCUIElementTypeTextField[contains(@name,"How much do you pay towards your mortgage, rent")]';
        this.childCareCost = '//XCUIElementTypeTextField[contains(@name,"How much do you pay towards childcare costs")]';
        this.lifeChangingEventField = '//XCUIElementTypeSwitch[@name="No"]';
        this.overdraftDeclinePageTitle = '//XCUIElementTypeStaticText[@name="Sorry your arranged overdraft '
            + 'application has been declined"]';
        this.continueButton = '//XCUIElementTypeButton[@name="Continue"]';
        this.aboutYouLabel = '//XCUIElementTypeStaticText[@name="About you"]';
        this.employmentStatus = '//XCUIElementTypePickerWheel';
        this.overdraftAmountLimitLabel = '//XCUIElementTypeStaticText[@name="Arranged overdraft limit you would like"]';
        this.helpButton = helper.byLabel('Need some help?');
        this.callDetailsLabel = helper.byLabel('If you need some help with your application you can call:');
        this.bookAppointmentLabel = helper.byPartialLabel('Or we can arrange an appointment with a colleague in your closest branch');
        this.findYourNearestBranchLink = helper.byLabel('find your nearest branch');
        this.fromOutsideUk = helper.byLabel('From outside the UK,');
        this.ukNumber = (number) => `(//*[@label="${number}"])[1]`;
        this.outsideUKNumber = (number) => `(//*[@label="${number}"])[1]`;
        this.leaveAppTitle = helper.byName('alertTitle');
        this.leaveAppOkButton = helper.byName('alertActionConfirmButton');
        this.leaveAppCancelButton = helper.byName('alertActionCancelButton');
    }
}

module.exports = ProductHubApplyOverdraftScreen;
