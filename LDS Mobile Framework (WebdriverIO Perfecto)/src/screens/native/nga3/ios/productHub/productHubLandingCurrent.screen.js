const BaseProductHubLandingCurrentScreen = require('../../../productHub/productHubLandingCurrent.screen');
const helper = require('../../../ios.helper');

class ProductHubLandingCurrentScreen extends BaseProductHubLandingCurrentScreen {
    constructor() {
        super();
        this.currentAccountOverdraft = helper.byValue('Overdrafts');
    }
}

module.exports = ProductHubLandingCurrentScreen;
