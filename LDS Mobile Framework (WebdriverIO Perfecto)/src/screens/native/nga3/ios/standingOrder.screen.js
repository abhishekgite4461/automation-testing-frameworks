const BaseStandingOrder = require('../../standingOrder.screen');
const helper = require('../../ios.helper');

class StandingOrder extends BaseStandingOrder {
    constructor() {
        super();
        this.title = helper.byLabel('Standing orders');
        this.setUpNewStandingOrderButton = helper.byLabel('Set up a standing order');
    }
}

module.exports = StandingOrder;
