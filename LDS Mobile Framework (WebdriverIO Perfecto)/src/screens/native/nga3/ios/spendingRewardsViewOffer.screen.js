const BaseSpendingRewardsViewOfferScreen = require('../../spendingRewardsViewOffer.screen');
const helper = require('../../ios.helper');

class SpendingRewardsViewOfferScreen extends BaseSpendingRewardsViewOfferScreen {
    constructor() {
        super();
        this.title = helper.byName('Everyday Offers');
    }
}

module.exports = SpendingRewardsViewOfferScreen;
