const BaseEnterPostalOtpScreen = require('../../enterPostalOtp.screen');
const helper = require('../../ios.helper');

class EnterPostalOtpScreen extends BaseEnterPostalOtpScreen {
    constructor() {
        super();
        this.title = helper.byName('enterOTPTitleLabel');
        this.otpNotRecievedToolTip = `${helper.byName('enterOTPContinueButton')}/following-sibling::*[@name='enterOTPDescriptionLabel']`;
        this.logoffOption = helper.byName('logOffButton');
        this.invalidOTPErrorMessage = helper.byPartialValue('9200306');
        this.errorCloseButton = helper.byName('closeIcon');
        this.enterOTPField = helper.byName('enterOTPInputField');
        this.continueButton = helper.byName('enterOTPContinueButton');
    }
}

module.exports = EnterPostalOtpScreen;
