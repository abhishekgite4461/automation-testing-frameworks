const BaseEnrolmentDeclineTermsAndConditions = require('../../enrolmentTermsAndConditionsDeclinedError.screen');
const helper = require('../../ios.helper');

class EnrolmentDeclineTermsAndConditions extends BaseEnrolmentDeclineTermsAndConditions {
    constructor() {
        super();
        this.title = helper.byName('declinePopUpTitle');
        this.confirmDeclineButton = helper.byName('declineAndLogoutButton');
        this.backButton = helper.byName('backToTCButton');
    }
}

module.exports = EnrolmentDeclineTermsAndConditions;
