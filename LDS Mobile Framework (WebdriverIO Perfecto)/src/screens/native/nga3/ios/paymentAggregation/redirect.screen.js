const BaseRedirectScreen = require('../../../paymentAggregation/redirect.screen');
const helper = require('../../../ios.helper');

class RedirectScreen extends BaseRedirectScreen {
    constructor() {
        super();
        this.redirectScreenPaymentAgg = helper.byName('paymentRedirectErrorIcon');
        this.redirectScreenTitlePaymentAgg = helper.byName('paymentRedirectErrorInfo');
        this.redirectScreenAuthenticationInProgressTitle = helper.byName('paymentRedirectErrorInfo');
        this.redirectLoadingProgressPayAgg = helper.byName('payAggRedirectLoadingProgress');
        this.redirectTextPayAgg = helper.byName('payAggRedirectText');
    }
}

module.exports = RedirectScreen;
