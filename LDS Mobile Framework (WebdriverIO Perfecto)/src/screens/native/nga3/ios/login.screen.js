const BaseLoginScreen = require('../../login.screen');
const helper = require('../../ios.helper');

class LoginScreen extends BaseLoginScreen {
    constructor() {
        super();
        this.title = helper.byName('CallUsBarButton');
        // MQE-1075 TODO
        this.oldTitle = helper.byName('ContactUsBarButton');
        this.userIdField = helper.byName('loginUsernameInput');
        this.passwordField = helper.byName('loginPasswordInput');
        this.continueButton = helper.byName('loginContinueButton');
        this.userIdErrorMessage = '//*[contains(@label, "incorrect user") or contains(@label,"wrong username")]';
        this.loginForgotYourPassword = helper.byName('loginForgotLoginButton');
        this.mandateLessErrorMessage = helper.byPartialValue('not registered for');
        this.registerForIb = helper.byName('loginRegisterForIb');
        this.errorMessage = '//*[contains(@label, "already set up for Online Banking") or contains(@label,"already set up for Internet Banking")]';
        this.errorMessageDismiss = helper.byName('closeIcon');
        this.specialCharactersRestrictionMessage = helper.byName('If your password contains special characters like exclamation marks or ampersands you will need to update it');
        this.updateYourPasswordLink = helper.byName('specialCharactersLink');
        this.pasteOptionInToolTip = helper.byName('Paste');
        this.copyOptionInToolTip = helper.byName('Copy');
        this.selectAllOptionInToolTip = helper.byName('Select All');
    }
}

module.exports = LoginScreen;
