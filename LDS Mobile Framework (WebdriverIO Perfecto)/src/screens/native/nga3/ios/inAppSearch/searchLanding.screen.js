const helper = require('../../../ios.helper');
const BaseSearchLandingScreen = require('../../../inAppSearch/searchLanding.screen.js');

// TODO: Created JIRA ticket-AVC-5279 to give valid element ID as per coding standards which will be fixed by devs
//  when feature is ready in environments. currently no valid element IDs are available

class searchLandingScreen extends BaseSearchLandingScreen {
    constructor() {
        super();
        this.title = helper.byName('Search');
        this.searchIcon = helper.byName('globalSearch');
        this.searchBar = helper.byName('What can we help you with?');
        this.searchClear = helper.byName('Clear text');
        this.resetMobileBankingAppLink = helper.byName('Reset Mobile Banking app');
        this.resetMobileBankingAppPage = helper.byName('Reset app');
        this.overdraftsPage = helper.byName('Overdrafts');
        this.managestatementLink = helper.byLabel('Manage statement preferences');
        this.managepaperfreeLink = helper.byLabel('Manage paper-free preferences');
        this.managestatementPage = helper.byLabel('Choose the frequency of your statements');
        this.managepaperfreePage = helper.byLabel('Manage your online and paper preferences');
        this.setupeverydayoffersLink = helper.byName('Set up Everyday Offers');
        this.setupeverydayoffersPage = helper.byName('Everyday Offers');
        this.loanRepaymentHolidayLink = helper.byName('Loan Repayment Holiday');
        this.mortgageRepaymentHolidayLink = helper.byName('Mortgage Repayment Holiday');
        this.creditcardPaymentHolidayLink = helper.byName('Credit Card Payment Holiday');
        this.changeYourAddressLink = helper.byName('Change your address');
        this.changeYourAddressPage = helper.byName('Change address');
        this.manageAndViewYourMarketingChoicesLink = helper.byName('Manage/view your marketing choices');
        this.manageAndViewYourMarketingChoicesPage = helper.byName('Your profile');
        this.manageYourPersonalDetailsLink = helper.byName('Manage your personal details');
        this.manageYourPersonalDetailsPage = helper.byName('settingsPersonalDetailsTable');
        this.viewYourDataConsentsLink = helper.byName('View your data consents for this device');
        this.viewYourDataConsentsPage = helper.byName('Data consent');
        this.changeYourPhoneNumberLink = helper.byName('Change your phone number');
        this.changeYourPhoneNumberPage = helper.byName('Your phone numbers');
        this.changeYourEmailAddressLink = helper.byName('Change your email address');
        this.changeYourEmailAddressPage = helper.byName('Update email');
        this.makePaymentLink = helper.byName('Make a Payment');
        this.payCreditCardLink = helper.byName('Pay Credit Card');
        this.payAndTransferPage = helper.byName('Pay & transfer');
        this.chequeDepositHistoryLink = helper.byName('View Deposit Cheque History');
        this.chequeDepositHistoryPage = helper.byName('depositHistoryTab');
        this.depositAChequeLink = helper.byName('Deposit a Cheque');
        this.depositAChequePage = helper.byName('depositChequeTab');
        this.searchSuggestions = helper.byName('Check the spelling is correct. Try different keywords. To search for transactions, go to your statements page.');
        this.NeedMoreHelpTitle = helper.byName('Need more help?');
        this.messageUsButton = helper.byName('functionalitySearchNoResultsMessageUsButton');
        this.helpAndSupportButton = helper.byName('functionalitySearchNoResultsHelpAndSupportButton');
        this.supportHubPage = helper.byName('Support');
        this.searchBar = helper.byName('What can we help you with?');
        this.searchButton = '//XCUIElementTypeButton[contains(@name, "Search") or contains(@name, "search")]';
        this.viewPinLink = helper.byName('View Pin');
        this.viewPinPage = helper.byName('Card and PIN Settings');
        this.replaceCardAndPinLink = helper.byName('Replace card and PIN');
        this.cardHasBeenLostOrStolenLink = helper.byName('Card has been lost or stolen');
        this.fingerprintFaceScanLogonSettingsLink = helper.byName('Touch ID/Face ID logon setting');
        this.securitySettingsPage = helper.byName('Security settings');
        this.chooseAutoSignOutSettingLink = helper.byName('Choose auto logoff setting');
        this.autoLogoutPage = helper.byName('Auto logoff');
        this.viewYourUserIdLink = helper.byName('View my User ID');
        this.viewYourDeviceTypeLink = helper.byName('View my device Type');
        this.viewYourDeviceNameLink = helper.byName('View my device name');
        this.viewYourCurrentAppVersionLink = helper.byName('View my current app version');
        this.viewAmendYourSecuritySettingsLink = helper.byName('View/amend my security settings');
        this.managePaperFreePreferenceLink = helper.byName('Manage paper-free preferences');
    }
}
module.exports = searchLandingScreen;
