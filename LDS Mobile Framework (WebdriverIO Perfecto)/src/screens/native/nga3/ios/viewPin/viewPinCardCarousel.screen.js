const BaseCardCarouselScreen = require('../../../viewPin/viewPinCardCarousel.screen');
const helper = require('../../../ios.helper');

// TODO:MCC-2831 JIRA issue to change the valid element id

class ViewPinCardCarouselScreen extends BaseCardCarouselScreen {
    constructor() {
        super();
        this.viewPinButton = helper.byName('ViewPinButton');
        this.enterPasswordDialogTitle = helper.byName('passwordAlertTitleLabel');
        this.dialogPasswordField = helper.byName('passwordAlertPasswordInputField');
        this.dialogCancelButton = helper.byName('alertActionCancelButton');
        this.dialogOkButton = helper.byName('alertActionConfirmButton');
    }
}
module.exports = ViewPinCardCarouselScreen;
