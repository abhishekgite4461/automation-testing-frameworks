const Screen = require('../../../viewPin/viewPin.screen');
const helper = require('../../../ios.helper');

// TODO:MCC-2831 JIRA issue to change the valid element id

class viewPinScreen extends Screen {
    constructor() {
        super();
        this.revealPinButton = helper.byName('RevealPinButton');
        this.PIN = '//*[@name="ViewPinScreen"]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]';
    }
}

module.exports = viewPinScreen;
