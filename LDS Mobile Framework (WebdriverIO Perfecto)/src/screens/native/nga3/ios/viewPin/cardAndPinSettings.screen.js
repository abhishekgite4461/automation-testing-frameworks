const BaseCardAndPinSettingsScreen = require('../../../viewPin/cardAndPinSettings.screen');
const helper = require('../../../ios.helper');

// TODO:MCC-2831 JIRA issue to change the valid element id

class CardAndPinSettingsScreen extends BaseCardAndPinSettingsScreen {
    constructor() {
        super();
        this.title = helper.byName('View PIN');
        this.viewPinTile = helper.byName('View PIN');
    }
}

module.exports = CardAndPinSettingsScreen;
