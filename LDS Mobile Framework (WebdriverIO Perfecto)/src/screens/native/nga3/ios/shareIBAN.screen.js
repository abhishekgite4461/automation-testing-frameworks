const helper = require('../../ios.helper');
const BaseShareIBAN = require('../../shareIBAN.screen');

// TODO: Created JIRA ticket-ENG-2195 to give valid element ID as per coding standards which will be fixed by devs

class shareIBAN extends BaseShareIBAN {
    constructor() {
        super();
        this.shareIbanPageTitle = helper.byName('shareAccountDetailsHeaderText');
        this.ibanwarning = helper.byName('shareAccountDetailsHeaderText');
        this.nameUKaccount = helper.byName('accountNameLabel');
        this.nameUKaccountValue = helper.byName('accountNameValue');
        this.sortCode = helper.byName('sortCodeLabel');
        this.sortCodeValue = helper.byName('sortCodeValue');
        this.accountNumber = helper.byName('accountNumberLabel');
        this.accountNumberValue = helper.byName('accountNumberValue');
        this.nameIntAccount = helper.byLabel('Name on the account:');
        this.nameIntAccountValue = helper.byName('accountNameValue');
        this.iban = helper.byName('ibanLabel');
        this.ibanValue = helper.byName('ibanValue');
        this.bic = helper.byName('bicLabel');
        this.bicValue = helper.byName('bicValue');
        this.sendUKDetailsButton = helper.byName('sendUKBankDetails');
        this.sendIntDetailsButton = helper.byName('sendInternationalBankDetails');
        this.closeIcon = helper.byName('CrossBarButton');
        this.errorMessage = helper.byName('errorInFetchingInternationalBankDetails');
        this.ibanSearchMessage = helper.byName('View your International Bank Account Number (IBAN)');
        this.bicSearchMessage = helper.byName('View your Bank Identifier Code (BIC)');
        this.ibanErrorMessage = helper.byName('errorInFetchingInternationalBankDetails');
    }
}

module.exports = shareIBAN;
