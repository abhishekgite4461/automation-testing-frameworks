const BaseEnterPostalOtpTooltipScreen = require('../../enterPostalOtpTooltip.screen');
const helper = require('../../ios.helper');

class EnterPostalOtpTooltipScreen extends BaseEnterPostalOtpTooltipScreen {
    constructor() {
        super();
        this.title = helper.byName('enterOtpNotReceivedTooltipPopupTitle');
        this.closeButton = helper.byName('alertActionCancelButton');
        this.contactUsButton = helper.byName('alertActionConfirmButton');
    }
}

module.exports = EnterPostalOtpTooltipScreen;
