const BaseWelcomeBenefitsScreen = require('../../welcomeBenefits.screen');
const helper = require('../../ios.helper');

class WelcomeBenefitsScreen extends BaseWelcomeBenefitsScreen {
    constructor() {
        super();
        this.title = helper.byName('Welcome');
        this.nextButton = helper.byName('nextButton');
        this.makeYourBankingEasier = helper.byName('welcomeSection1Body');
        this.concernedAboutFraud = helper.byName('welcomeSection2Body');
        this.dataPrivacy = helper.byName('welcomeSection3Body');
        this.becomeACustomer = helper.byName('welcomeSection4Body');
    }
}

module.exports = WelcomeBenefitsScreen;
