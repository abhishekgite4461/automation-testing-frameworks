const BaseInterestRateSavingAccountScreen = require('../../interestRateSavingAccount.screen');
const helper = require('../../ios.helper');

class InterestRateSavingAccountScreen extends BaseInterestRateSavingAccountScreen {
    constructor() {
        super();
        this.title = helper.byName('interestRateDetailParentContainer');
        this.availableBalalce = helper.byName('Account balance. NIL');
        this.interestRatetText = helper.byName('interestRateDetailInterestRateLabel');
    }
}

module.exports = InterestRateSavingAccountScreen;
