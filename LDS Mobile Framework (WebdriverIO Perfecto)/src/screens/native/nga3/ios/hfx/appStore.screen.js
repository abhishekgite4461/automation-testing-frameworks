const BaseAppStoreScreen = require('../appStore.screen');
const helper = require('../../../ios.helper');

class AppStoreScreen extends BaseAppStoreScreen {
    constructor() {
        super();
        this.appText = helper.byLabel('Halifax Mobile Banking');
    }
}

module.exports = AppStoreScreen;
