const BaseNewWelcomeBenefitsScreen = require('../../ibRegistration/newWelcomeBenefits.screen');
const helper = require('../../../../ios.helper');

class NewWelcomeBenefitsScreen extends BaseNewWelcomeBenefitsScreen {
    constructor() {
        super();
        this.fraudGuaranteeTitle = helper.byPartialValue('halifax.co.uk');
        this.appDemoTitle = helper.byValue('https://www.youtube.com/embed/XFgEYu5qf68?autoplay=1&rel=0&autohide=1&showinfo=0&wmode=transparent');
        this.newCustomerTitle = helper.byValue('https://www.lloydsbank.com/current-accounts.asp??=#current-accounts');
    }
}

module.exports = NewWelcomeBenefitsScreen;
