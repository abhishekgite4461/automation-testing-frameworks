const BaseFscsScreen = require('../fscs.screen');

class FscsScreen extends BaseFscsScreen {
    constructor() {
        super();
        this.compensationSchemeLabel = '//XCUIElementTypeOther[@name="FINANCIAL SERVICES COMPENSATION SCHEME."]';
    }
}

module.exports = FscsScreen;
