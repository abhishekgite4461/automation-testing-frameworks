const BaseFingerprintInterstitialScreen = require('../../fingerprint/fingerprintInterstitial.screen');

class FingerprintInterstitialScreen extends BaseFingerprintInterstitialScreen {
    constructor() {
        super();
        // Banner title property is invisible. Hence keeping page title
        this.title = '//*[contains(@label, "TOUCH ID IS FAST AND SAFE.") or contains(@label, "FACE ID IS FAST AND SAFE.") or contains(@label, "Touch ID is fast and safe.") or contains(@label, "Face ID is fast and safe.")]';
    }
}

module.exports = FingerprintInterstitialScreen;
