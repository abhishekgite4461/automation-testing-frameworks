const BaseMobileBrowserScreen = require('../mobileBrowser.screen');

class MobileBrowserScreen extends BaseMobileBrowserScreen {
    constructor() {
        super();
        this.brandUrl = 'halifax-online.co.uk';
        this.returnToApp = '//*[@label="Return to Halifax"]';
    }
}

module.exports = MobileBrowserScreen;
