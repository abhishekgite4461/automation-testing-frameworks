const BaseChooseRecipientScreen = require('../../transferAndPayments/searchRecipient.screen');
const helper = require('../../../../ios.helper');

class searchRecipientScreen extends BaseChooseRecipientScreen {
    constructor() {
        super();
        this.yourAccountsGroupHeader = helper.byName('My accounts.');
        this.bannerAccount = helper.byLabel('EASY SAVER');
    }
}

module.exports = searchRecipientScreen;
