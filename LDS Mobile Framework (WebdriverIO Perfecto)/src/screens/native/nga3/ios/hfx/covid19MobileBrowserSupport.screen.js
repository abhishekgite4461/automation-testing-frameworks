const BaseMobileBrowserScreen = require('../mobileBrowser.screen');

class covid19MobileBrowserSupportScreen extends BaseMobileBrowserScreen {
    constructor() {
        super();
        this.mortgagePaymentHolidayUrl = 'halifax.co.uk/mortgages/existing-customers/payment-holidays/';
        this.creditcardPaymentHolidayUrl = 'halifax.co.uk/helpcentre/coronavirus/#manage';
        this.loanPaymentHolidayUrl = 'halifax.co.uk/loans/help-guidance/managing-your-loan/';
        this.overdraftChargesUrl = 'halifax.co.uk/helpcentre/coronavirus/#manage';
        this.travelAndDisruptionCoverUrl = 'halifax.co.uk/helpcentre/coronavirus/#travel';
        this.moreInfoAboutCoronavirusUrl = 'halifax.co.uk/helpcentre/coronavirus/';
    }
}

module.exports = covid19MobileBrowserSupportScreen;
