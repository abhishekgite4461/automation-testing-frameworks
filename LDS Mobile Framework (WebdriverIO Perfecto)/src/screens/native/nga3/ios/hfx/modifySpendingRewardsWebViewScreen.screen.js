const BaseModifySpendingRewardsWebViewScreen = require('../../../modifySpendingRewardsWebViewScreen.screen');
const helper = require('../../../ios.helper');

class ModifySpendingRewardsWebViewScreen extends BaseModifySpendingRewardsWebViewScreen {
    constructor() {
        super();
        this.title = helper.byName('Cashback Extras');
    }
}

module.exports = ModifySpendingRewardsWebViewScreen;
