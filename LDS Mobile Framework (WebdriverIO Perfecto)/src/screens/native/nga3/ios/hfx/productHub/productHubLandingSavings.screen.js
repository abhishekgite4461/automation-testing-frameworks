const BaseProductHubLandingSavingsScreen = require('../../productHub/productHubLandingSavings.screen');
const helper = require('../../../../ios.helper');

class ProductHubLandingSavingsScreen extends BaseProductHubLandingSavingsScreen {
    constructor() {
        super();
        this.title = helper.byName('Savings and investments');
    }
}

module.exports = ProductHubLandingSavingsScreen;
