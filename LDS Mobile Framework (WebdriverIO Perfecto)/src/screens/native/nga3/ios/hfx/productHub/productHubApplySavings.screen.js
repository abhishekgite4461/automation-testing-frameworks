/* eslint-disable max-len */
const BaseProductHubApplySavingsScreen = require('../../productHub/productHubApplySavings.screen');

class ProductHubApplySavingsScreen extends BaseProductHubApplySavingsScreen {
    constructor() {
        super();
        this.accountOpeningSuccessMessage = (accountName) => `//XCUIElementTypeStaticText[@name = "Your ${accountName} account is now open" or contains(@name, 'opened a ${accountName}')]`;
        this.openAccount = '//*[@name ="Open account" or @name = "Apply now"]';
        this.acctAccountNumber = '//XCUIElementTypeStaticText[preceding-sibling::XCUIElementTypeStaticText[contains(@name,"Account number")]]';
        this.sortCode = '//XCUIElementTypeStaticText[contains(@name,"Sort code")]/following-sibling::XCUIElementTypeStaticText[1]';
    }
}

module.exports = ProductHubApplySavingsScreen;
