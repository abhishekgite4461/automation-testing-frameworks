const BaseProductHubApplyOverdraftScreen = require('../../productHub/productHubApplyOverdraft.screen');

class ProductHubApplyOverdraftScreen extends BaseProductHubApplyOverdraftScreen {
    constructor() {
        super();
        this.overdraftPageTitle = '//XCUIElementTypeStaticText[@name="CHOOSE AN AMOUNT"]';
        this.overdraftDeclinePageTitle = '//XCUIElementTypeStaticText[@name="SORRY YOUR ARRANGED OVERDRAFT '
            + 'APPLICATION HAS BEEN DECLINED"]';
        this.aboutYouLabel = '//XCUIElementTypeStaticText[@name="ABOUT YOU"]';
    }
}

module.exports = ProductHubApplyOverdraftScreen;
