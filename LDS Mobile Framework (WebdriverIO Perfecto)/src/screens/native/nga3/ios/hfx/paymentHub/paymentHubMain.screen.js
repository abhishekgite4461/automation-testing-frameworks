const BasePaymentHubMainScreen = require('../../paymentHub/paymentHubMain.screen');
const helper = require('../../../../ios.helper');

class PaymentHubMainScreen extends BasePaymentHubMainScreen {
    constructor() {
        super();
        this.title = helper.byName('fromAccountDetails');
        this.toAccount = helper.byName('toAccountNameLabel');
    }
}

module.exports = PaymentHubMainScreen;
