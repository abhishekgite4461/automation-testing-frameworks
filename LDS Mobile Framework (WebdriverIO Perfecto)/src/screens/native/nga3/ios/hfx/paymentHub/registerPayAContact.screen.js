const BaseRegisterPayAContactScreen = require('../../paymentHub/registerPayAContact.screen');
const helper = require('../../../../ios.helper');

class RegisterPayAContactScreen extends BaseRegisterPayAContactScreen {
    constructor() {
        super();
        this.registerButton = helper.byName('Register now');
    }
}

module.exports = RegisterPayAContactScreen;
