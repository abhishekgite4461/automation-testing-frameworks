const BaseSpendingRewardsScreen = require('../../../spendingRewards.screen');
const helper = require('../../../ios.helper');

class SpendingRewardsScreen extends BaseSpendingRewardsScreen {
    constructor() {
        super();
        this.title = helper.byName('spendingRewardsTitle');
        this.optInEverydayOfferButton = helper.byName('spendingRewardsOptInButton');
        this.noOptInEverydayOfferButton = helper.byName('spendingRewardsNotNowOptInButton');
        this.expiredOffers = '//XCUIElementTypeLink[@name="Expired offers"]';
        this.settings = '//XCUIElementTypeLink[@name="Settings"]';
        this.findOutMore = '//XCUIElementTypeLink[@name="Find out more"]';
        this.newTab = helper.byPartialName('New');
        this.activeOffers = '//XCUIElementTypeLink[contains(@name, "Active")]';
        this.activeTab = '//XCUIElementTypeStaticText[contains(@name, "Active")]';
        this.selectEverydayOffersLink = '//XCUIElementTypeStaticText[@name="Everyday Offers" '
            + 'or @name="Cashback Extras"]';
        this.expiredOffersPageTitle = '//XCUIElementTypeStaticText[@name="Expired offers"  '
            + 'or @name="Cashback Extras"]';
        this.settingPageTitle = '//XCUIElementTypeStaticText[@name="Change your Everyday Offers settings"]';
        this.findOutMorePageTitle = '//XCUIElementTypeStaticText[@name="Find out more about Everyday Offers"]';
        this.spendingRewardsEnjoyDescription = helper.byPartialLabel('Earn cashback in your current account');
        this.spendingRewardsShopTitle = helper.byPartialLabel('Shop with your Halifax cards');
        this.activateTheOffers = helper.byPartialLabel('Activate the offers');
        this.spendingRewardsOptInButton = helper.byPartialLabel('Yes, sign me up');
        this.spendingRewardsNotNowOptInButton = helper.byPartialLabel('Not now');
    }
}

module.exports = SpendingRewardsScreen;
