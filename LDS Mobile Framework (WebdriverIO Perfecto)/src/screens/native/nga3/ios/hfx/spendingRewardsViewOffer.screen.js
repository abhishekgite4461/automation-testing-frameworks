const BaseSpendingRewardsViewOfferScreen = require('../../../spendingRewardsViewOffer.screen');
const helper = require('../../../ios.helper');

class SpendingRewardsViewOfferScreen extends BaseSpendingRewardsViewOfferScreen {
    constructor() {
        super();
        this.title = helper.byName('Cashback Extras');
    }
}

module.exports = SpendingRewardsViewOfferScreen;
