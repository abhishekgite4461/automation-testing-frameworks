const BaseOnlinePaperPreferenceWebViewPageScreen = require('../../onlinePaperPreferenceWebViewPage.screen');
const helper = require('../../ios.helper');

class OnlinePaperPreferenceWebViewPageScreen extends BaseOnlinePaperPreferenceWebViewPageScreen {
    constructor() {
        super();
        this.title = helper.byName('Manage your online and paper preferences');
        this.backButton = '//XCUIElementTypeNavigationBar/XCUIElementTypeButton[1]';
    }
}

module.exports = OnlinePaperPreferenceWebViewPageScreen;
