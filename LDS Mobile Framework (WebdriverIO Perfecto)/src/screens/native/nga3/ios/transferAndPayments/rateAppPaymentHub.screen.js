const BaseRateAppPaymentHubScreen = require('../../../transferAndPayments/rateAppPaymentHub.screen');
const helper = require('../../../ios.helper');

class RateAppPaymentHubScreen extends BaseRateAppPaymentHubScreen {
    constructor() {
        super();
        this.rateApp = helper.byPartialLabel('Tap a star to rate it on the App Store');
        this.closeRateApp = helper.byPartialLabel('Not Now');
    }
}

module.exports = RateAppPaymentHubScreen;
