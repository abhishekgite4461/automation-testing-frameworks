const BaseChooseRecipientScreen = require('../../../transferAndPayments/ukMobileNumberDetail.screen');
const helper = require('../../../ios.helper');

class UkMobileNumberDetailScreen extends BaseChooseRecipientScreen {
    constructor() {
        super();
        this.title = helper.byName('fromAccountDetails');
        this.beneficiaryTileIcon = helper.byName('fromAccountDetails');
        this.mobileNumber = helper.byName('mobileCellInputField');
        this.amount = helper.byName('amountCellInputField');
        this.reference = helper.byName('referenceCellInputField');
        this.continueButtonInPaymentHubPage = helper.byName('Continue');
        this.editPaymentReviewButton = helper.byName('editPaymentReviewButton');
        this.addBeneficiaryP2pReviewContinueButton = helper.byName('addBeneficiaryP2pReviewContinueButton');
        this.addBeneficiaryP2pReviewCancelButton = helper.byName('addBeneficiaryP2pReviewCancelButton');
        this.transferSuccessHeader = helper.byName('transferSuccessHeader');
        this.makeAnotherPaymentButton = helper.byName('anotherTransferButton');
        this.viewTransactionButton = helper.byName('viewTransactionsButton');
        this.errorMessageForAddPaym = helper.byName('errorMessage');
        this.recentsTipMessage = helper.byValue('This will also appear on the payee’s statement');
        this.errorMessageForMinimumAndMaximumDailyLimit = helper.byName('(Limit: Min £1, Max daily £300)');
        this.toFieldValue = helper.byValue('Select contact or add number');
        this.verifyingaddPayMPage = helper.byName('AddP2PBeneficiaryScreen');
    }
}

module.exports = UkMobileNumberDetailScreen;
