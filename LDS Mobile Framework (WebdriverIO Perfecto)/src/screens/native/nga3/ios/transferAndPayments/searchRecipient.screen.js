const BaseChooseRecipientScreen = require('../../../transferAndPayments/searchRecipient.screen');
const helper = require('../../../ios.helper');

class searchRecipientScreen extends BaseChooseRecipientScreen {
    constructor() {
        super();
        this.title = helper.byName('SelectRecipientView');
        this.searchIcon = helper.byName('viewRecipientSearchButton');
        this.searchEditText = helper.byName('viewRecipientSearchTextBox');
        this.searchedAccount = (accName) => `(//XCUIElementTypeCell/*[contains(@label,"${accName}") 
            and @visible="true"])[1]`;
        this.searchedAccountPayee = (accName) => `//*[@name='viewRecipientPaymentCell' and 
            contains(@label,'${accName}')]`;
        this.aggAccount = (accountSortCode, accountNumber) => `//XCUIElementTypeStaticText[contains(@label, 
            '${accountSortCode}')]/following-sibling::XCUIElementTypeStaticText[contains(@label,'${accountNumber}')]`;
        this.searchedByPartialTextAccount = (accName) => `//XCUIElementTypeCell/*[contains(@label,"${accName}")]`;
        this.addNewPayee = helper.byName('viewRecipientAddNewPayeeButton');
        this.yourAccountsGroupHeader = helper.byName('Your accounts.');
        this.recipientCell = '//*[@name=\'viewRecipientTransferCell\'][1]';
        this.accountResultByName = (label) => helper.byLabel(label);
        this.benificiaryResultByName = (label) => `//*[contains(@label,'${label}') or 
            contains(@label,'${label.toUpperCase()}')]`;
        this.firstRecipientFromList = '(//*[@name=\'viewRecipientPaymentCell\'])[1]';
        this.recipientWithName = (index) => helper.byVisibleName(`viewRecipientPaymentCell${index}`);
        this.addNewPayeeFooter = helper.byName('viewRecipientFooterInfo');
        this.cancelSearch = helper.byLabel('Cancel');
        this.clearSearch = helper.byLabel('clear');
        this.partialAddNewPayeeFooterText = helper.byPartialLabel('add a new payee');
        this.bannerRecipient = helper.byLabel('ATWELL MARTIN LTD');
        this.bannerAccount = helper.byLabel('Easy Saver');
        this.transferToList = (recipient) => `//*[preceding-sibling::*[contains(@name,"accounts")] 
            and contains(@label,'${recipient}')][1]`;
        this.noResultMessage = "//*[@name='viewRecipientFooterInfo' and contains(@label, 'Please try again or tap to pay someone new')]";
        this.firstRecipientFromListManageRecipient = helper.byLabel('Manage');
        this.deleteRecipient = helper.byLabel('Delete payee');
        this.deleteConfirm = helper.byLabel('Delete');
        this.secondRecipientFromListManageRecipient = "(//*[@name='viewRecipientPaymentCell'])[2]//*[@label='Manage']";
        this.searchResult = (accountName) => helper.byName(accountName);
        this.searchedBusinessAccount = (businessName, accName) => `//*[starts-with(@label, 'Account name. 
            ${accName}.') and contains(@label, '${businessName}')]`;
    }
}

module.exports = searchRecipientScreen;
