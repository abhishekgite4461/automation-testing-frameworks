const BaseConfirmContactScreen = require('../../../transferAndPayments/confirmContact.screen');
const helper = require('../../../ios.helper');

class ConfirmContactScreen extends BaseConfirmContactScreen {
    constructor() {
        super();
        this.title = helper.byLabel('Confirm contact');
        this.confirmButton = helper.byLabel('Confirm');
        this.nameAndPhoneNumberInConfirm = helper.byName('addBeneficiaryP2pReviewUserDetailsAccessibilityView');
        this.confirmContinueButton = helper.byLabel('Continue');
        this.confirmCancelButton = helper.byName('addBeneficiaryP2pReviewCancelButton');
    }
}

module.exports = ConfirmContactScreen;
