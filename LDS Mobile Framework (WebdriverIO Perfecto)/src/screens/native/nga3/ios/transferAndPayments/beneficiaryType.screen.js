const BaseBeneficiaryTypeScreen = require('../../../transferAndPayments/beneficiaryType.screen');
const helper = require('../../../ios.helper');

class BeneficiaryTypeScreen extends BaseBeneficiaryTypeScreen {
    constructor() {
        super();
        this.ukAccount = helper.byName('addBeneficiaryUKAccountLabel');
        this.internationalBankAccount = helper.byName('addBeneficiaryInternationalAccountLabel');
        this.ukMobileNumber = helper.byLabel('addBeneficiaryPaymLabel');
        this.payingUkMobileNumberLink = helper.byName('beneficiaryPayingUKMobileNumberText');
    }
}

module.exports = BeneficiaryTypeScreen;
