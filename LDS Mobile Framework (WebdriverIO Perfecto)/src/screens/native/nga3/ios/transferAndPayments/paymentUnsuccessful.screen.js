const BasePaymentUnsuccessfulScreen = require('../../../transferAndPayments/paymentUnsuccessful.screen');
const helper = require('../../../ios.helper');

class PaymentUnsuccessfulScreen extends BasePaymentUnsuccessfulScreen {
    constructor() {
        super();
        this.title = helper.byLabel('Payment unsuccessful');
        this.bannerLeadPlacement = helper.byName('leadWebView');
        this.cbsProhibitiveMessage = helper.byPartialValue('can\'t complete this request online. Please call us on');
        this.BngacbsProhibitiveMessage = helper.byPartialValue('Sorry, you can\'t set up this arrangement online. Please call us on');
    }
}

module.exports = PaymentUnsuccessfulScreen;
