const BaseChooseRecipientScreen = require('../../../transferAndPayments/ukCompanySearchResults.screen');
const helper = require('../../../ios.helper');

class UkCompanySearchResultsScreen extends BaseChooseRecipientScreen {
    constructor() {
        super();
        this.paymentHubAddUkAccountSelectionPayeeNameValue = helper.byName('payeeNameValue');
        this.paymentHubAddUkAccountSelectionSortCodeValue = helper.byName('accountNumberValue');
        this.paymentHubAddUkAccountSelectionAccountNumberValue = helper.byName('sortCodeValue');
        this.paymentHubAddUkAccountSelectionCompanyList = "(//*[@name='viewRemitterCell'])[1]";
        this.businessBeneficiaryPayeeNotes = helper.byName('payeeNotesValue');
        this.paymentHubAddUkAccountReviewReferenceInput = helper.byName('addBeneficiaryReviewPayeeReferenceInput');
        this.paymentHubAddUkAccountReviewReferenceConfirmInput = helper.byName('addBeneficiaryReviewPayeeReferenceConfirmationInput');
        this.paymentHubAddUkAccountReviewContinueButton = helper.byLabel('Continue');
        this.paymentHubAddUkAccountReviewCancelButton = helper.byLabel('Cancel');
        this.paymentHubAddUkAccountSelectionTitle = helper.byPartialLabel('Select a company to pay:');
        this.paymentHubAddUkAccountReviewPayeeNameValue = helper.byName('addBeneficiaryReviewPayeeNameLabel');
        this.paymentHubAddUkAccountReviewSortCodeValue = helper.byName('addBeneficiaryReviewPayeeSortCodeLabel');
        this.paymentHubAddUkAccountReviewAccountNumberValue = helper.byName('addBeneficiaryReviewPayeeAccountNumberLabel');
    }
}

module.exports = UkCompanySearchResultsScreen;
