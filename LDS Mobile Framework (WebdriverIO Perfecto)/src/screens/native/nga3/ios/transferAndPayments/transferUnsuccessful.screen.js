const BaseTransferUnsuccessfulScreen = require('../../../transferAndPayments/transferUnsuccessful.screen');
const helper = require('../../../ios.helper');

class TransferUnsuccessfulScreen extends BaseTransferUnsuccessfulScreen {
    constructor() {
        super();
        this.bannerLeadPlacement = helper.byName('leadWebView');
    }
}

module.exports = TransferUnsuccessfulScreen;
