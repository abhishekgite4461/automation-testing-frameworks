const BaseManageRecipientScreen = require('../../../transferAndPayments/manageRecipient.screen');
const helper = require('../../../ios.helper');

class ManageRecipientScreen extends BaseManageRecipientScreen {
    constructor() {
        super();
        // below objects needs to be changed once automation is taken
        this.deleteRecipientButton = helper.byName('Delete payee');
        this.makePaymentButton = helper.byName('Make a payment');
        this.closeButton = helper.byName('actionMenuCloseButton');
        this.actionMenuHeader = "//*[contains(@label,'Options for')]";
        this.dialogNegativeAction = helper.byName('alertActionCancelButton');
        this.dialogPositiveAction = helper.byName('alertActionConfirmButton');
        this.viewPendingPayment = helper.byName('View this payment');
        this.dialogueBoxTitle = helper.byName('alertTitle');
        this.standingOrderDialogBox = helper.byPartialLabel('because you have a standing order set up');
        this.viewButton = helper.byName('alertActionConfirmButton');
        this.closeButtonStandingOrder = helper.byName('alertActionCancelButton');
        this.cancelSearch = helper.byLabel('Cancel');
        this.paymentDeletedLabel = helper.byLabel('Payee deleted');
    }
}

module.exports = ManageRecipientScreen;
