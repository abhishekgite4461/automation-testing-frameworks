const BasePaymentSuccessScreen = require('../../../transferAndPayments/paymentSuccess.screen');
const helper = require('../../../ios.helper');

class PaymentSuccessScreen extends BasePaymentSuccessScreen {
    constructor() {
        super();
        this.title = helper.byLabel('paymentSuccessHeader');
        this.bannerLeadPlacement = helper.byName('leadWebView');
        this.paymentProcessed = helper.byPartialValue('payment of');
    }
}

module.exports = PaymentSuccessScreen;
