const BaseNoEligibleAccountsErrorScreen = require('../../../transferAndPayments/noEligibleAccountsError.screen');
const helper = require('../../../ios.helper');

class NoEligibleAccountsErrorScreen extends BaseNoEligibleAccountsErrorScreen {
    constructor() {
        super();
        this.title = helper.byName('PaymentsHubUnavailableTitle');
        this.applyForAccountsButton = helper.byName('PaymentsHubUnavailableApplyButton');
        this.homeButon = helper.byName('errorScreenButtonSecondary');
        this.closeButton = helper.byName('CrossBarButton');
    }
}

module.exports = NoEligibleAccountsErrorScreen;
