const BaseTransferSuccessScreen = require('../../../transferAndPayments/transferSuccess.screen');
const helper = require('../../../ios.helper');

class TransferSuccessScreen extends BaseTransferSuccessScreen {
    constructor() {
        super();
        this.title = helper.byLabel('Transfer successful');
        this.bannerLeadPlacement = helper.byName('leadWebView');
    }
}

module.exports = TransferSuccessScreen;
