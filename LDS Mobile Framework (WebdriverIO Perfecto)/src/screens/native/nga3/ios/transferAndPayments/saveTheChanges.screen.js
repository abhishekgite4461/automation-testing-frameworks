const BaseSaveTheChanges = require('../../../saveTheChanges.screen.js');
const helper = require('../../../ios.helper');

class SaveTheChanges extends BaseSaveTheChanges {
    constructor() {
        super();
        this.title = '//XCUIElementTypeStaticText[@label="Save the Change®" '
            + 'or @label="Your Save the Change®"]';
        this.homebutton = helper.byLabel('Home');
    }
}

module.exports = SaveTheChanges;
