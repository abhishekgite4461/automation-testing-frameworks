const BaseChooseRecipientScreen = require('../../../transferAndPayments/ukBeneficiaryDetail.screen');
const helper = require('../../../ios.helper');

class UkBeneficiaryDetailScreen extends BaseChooseRecipientScreen {
    constructor() {
        super();
        this.beneficiaryTileIcon = helper.byName('fromAccountDetails');
        this.paymentHubAddUkAccountAdditionalContextCopy = helper.byName('Enter the account details of the person or company you wish to pay');
        // TODO MOB3-12816 Locator is missing, needs to be added
        this.paymentHubAddUkAccountPayeeNameLabel = helper.byLabel('Payee\'s name');
        this.payeeNameUk = helper.byName('beneficiaryNameInputField');
        this.paymentHubAddUkAccountSortCodeLabel = helper.byName('accountDetailsSortCode');
        this.sortCodeField = helper.byName('sortCodeInputField');
        this.paymentHubAddUkAccountAccountNumberLabel = helper.byName('accountNumberInputField');
        this.accountNumberUk = helper.byName('accountNumberInputField');
        this.paymentHubAddUkAccountCancelButton = helper.byLabel('Cancel');
        this.paymentHubAddUkAccountContinueButton = helper.byLabel('Continue');
        this.navigationToolbarBackButton = '//XCUIElementTypeNavigationBar/XCUIElementTypeButton[1]';
        this.arrangementSummaryListFromAccount = helper.byName('viewRemitterAccountName');
        this.errorMessageUkBeneficiary = helper.byName('errorMessage');
        this.errorMessageExclamatory = helper.byName('beneficiaryNameInputField');
        this.paymentHubAddUkAccountSecurityInfoText = helper.byName('Fraud warning - Paying someone unexpected?');
        // TODO PJO-9060 add name locator
        this.debitCardTile = helper.byLabel('Debit card');
        this.paymentHubAddUkAccountTypeCheckBox = helper.byName('checkbox');
        this.invalidAccountNumberErrorMessage = helper.byPartialLabel('Please call your payee to check their details.');
        this.closeInvalidAccountNumberErrorMessage = helper.byName('closeIcon');
        // TODO STAMOB-2242 add IDS locator
        this.yourUKBankAccount = helper.byName('YOUR UK BANK ACCOUNT');
        this.anotherUKBankAccount = (ukBankAccount) => helper.byName(`${ukBankAccount}`);
        this.confirmPaymentPage = helper.byName('reviewPaymentConfirmButton');
        this.confirmPaymentCheckBox = helper.byName('checkbox');
        this.confirmButton = helper.byName('reviewPaymentConfirmButton');
        this.importantInformationCotinueButton = helper.byName('continueButton');
        this.importantInformationPage = helper.byName('Important information');
        this.dialogBoxContinueButton = helper.byName('alertActionConfirmButton');
        this.finishYourPaymentDialogBox = helper.byName('alertMessageBody');
        this.unfinishedPaymentPage = helper.byName('Unfinished payment');
        this.fromAccountName = helper.byName('fromAccountNameLabel');
        this.toAccountName = helper.byName('toAccountNameLabel');
        this.toSortCode = helper.byName('toSortCodeLabel');
        this.amountValue = helper.byName('amountValue');
        this.dateValue = helper.byName('dateValue');
        this.termsAndCondition = helper.byName('Terms and Conditions');
        this.dataPrivacyNotice = helper.byName('Data Privacy Notice.');
    }
}

module.exports = UkBeneficiaryDetailScreen;
