const BasePaymentReviewScreen = require('../../../transferAndPayments/paymentReview.screen');
const helper = require('../../../ios.helper');

class PaymentReviewScreen extends BasePaymentReviewScreen {
    constructor() {
        super();
        this.title = helper.byPartialLabel('Confirm payment');
        this.confirmButton = helper.byLabel('Confirm');
        this.cancelButton = helper.byLabel('Cancel');
    }
}

module.exports = PaymentReviewScreen;
