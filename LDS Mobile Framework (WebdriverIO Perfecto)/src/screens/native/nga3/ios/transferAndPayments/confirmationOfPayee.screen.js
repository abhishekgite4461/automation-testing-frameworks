const BaseConfirmationOfPayeeScreen = require('../../../transferAndPayments/confirmationOfPayee.screen');
const helper = require('../../../ios.helper');

class ConfirmationOfPayeeScreen extends BaseConfirmationOfPayeeScreen {
    constructor() {
        super();
        this.title = helper.byLabel('mismatchScreenSubTitle');
        this.businessNameMatch = helper.byPartialLabel('This is a business account');
        this.personalAccountMatch = helper.byPartialLabel('This is a personal account');
        this.panMbanMContent = helper.byPartialValue('Is that the account you wanted to pay?');
        this.paymentHubCopNotMatchEdit = helper.byName('AddUKBeneficiaryMismatchSecondaryButton');
        this.serviceError = helper.byPartialLabel('We can’t confirm these details');
        this.paymentHubCopNotMatchContinue = helper.byName('AddUKBeneficiaryMismatchPrimaryButton');
        this.partialNameMatch = helper.byPartialValue('That’s not the full name on that account');
        this.paymentHubCopNotMatchTitle = helper.byName('AddUKBeneficiaryMismatchScreen');
        this.paymentHubCopANNMMessageOne = '//*[contains(@label,"That name doesn’t match that account number") or contains(@label,"That name doesn\'t match that account number")]';
        this.paymentHubCopANNMMessageTwo = '//*[contains(@label,"The name and account number still don’t match") or contains(@label,"The name and account number still don\'t match")]';
        this.paymentHubCopBAMMNamePlayBack = (recipientName) => helper.byPartialLabel(
            `This is a business account. It belongs to ${recipientName}`
        );
        this.paymentHubCopNotMatchShareDetailsMessage = '//*[contains(@label, "Name") and @name= "paymentMessageAndButtonCell"]';
        this.paymentHubCopNotMatchShareDetailsButton = '//*[@name= "paymentMessageAndButtonCell" and contains(@label, "Share details")]';
    }
}

module.exports = ConfirmationOfPayeeScreen;
