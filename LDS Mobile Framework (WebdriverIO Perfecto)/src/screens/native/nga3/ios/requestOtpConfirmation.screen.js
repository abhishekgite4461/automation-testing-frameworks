const BaseRequestOtpConfirmationScreen = require('../../requestOtpConfirmation.screen');
const helper = require('../../ios.helper');

class RequestOtpConfirmationScreen extends BaseRequestOtpConfirmationScreen {
    constructor() {
        super();
        this.title = helper.byName('requestOTPConfirmTitle');
        this.goToIbButton = helper.byName('requestOTPConfirmIBButton');
    }
}

module.exports = RequestOtpConfirmationScreen;
