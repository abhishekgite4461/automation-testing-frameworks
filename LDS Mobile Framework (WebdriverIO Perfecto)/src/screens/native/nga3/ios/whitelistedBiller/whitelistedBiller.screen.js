const BaseWhitelistedBillerScreen = require('../../../whitelistedBiller/whitelistedBiller.screen');
const helper = require('../../../ios.helper');

const errorBannerMessage = "Sorry, we don't recognise the reference you have entered. Please check and try again.";
const apiFailBannerMessage = "Sorry, we can't find any companies";

class WhitelistedBillerScreen extends BaseWhitelistedBillerScreen {
    constructor() {
        super();
        this.lloydsBiller = (account) => helper.byName(account);
        // TODO: PAT-1424 Add ID for below elements
        this.refBiller = helper.byPartialValue('digit reference');
        this.refErrorBanner = helper.byPartialLabel(errorBannerMessage);
        this.apiFailErrorBanner = helper.byPartialLabel(apiFailBannerMessage);
    }
}

module.exports = WhitelistedBillerScreen;
