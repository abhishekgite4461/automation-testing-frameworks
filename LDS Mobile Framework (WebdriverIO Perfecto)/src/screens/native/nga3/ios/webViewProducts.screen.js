const WebViewProductsScreenBase = require('../../webViewProducts.screen');
const helper = require('../../ios.helper');

class WebViewProductsScreen extends WebViewProductsScreenBase {
    constructor() {
        super();
        this.manageCreditLimitTitle = helper.byValue('Increase your credit card limit');
        this.repaymentHolidayTitle = helper.byValue('Ask for a repayment holiday');
        this.additionalPaymentTitle = helper.byValue('Make an Additional Payment to Your Loan');
        this.balanceTransferTitle = helper.byValue('Balance Transfer / Money Transfer');
        this.borrowMoreTitle = helper.byValue('Borrow more');
        this.loanClosureTitle = helper.byPartialName('Loan closure Stub');
        this.renewYourSavingsAccountTitle = helper.byValue('Renew your savings account');
        this.selectSavingsAccountToRenew = helper.byVisibleName('Select a savings account to renew');
        this.selectSavingsAccountToRenewText = helper.byPartialLabel('Select a savings account to renew');
        this.pensionTransferTitle = helper.byValue('Pension Transfer');
        this.overdraftPopUpTitle = helper.byValue('Overdrafts');
        this.changeAccountTypeTitle = helper.byValue('Change Account Type');
    }
}

module.exports = WebViewProductsScreen;
