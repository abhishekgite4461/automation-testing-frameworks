const BaseAddressChangedOtpErrorScreen = require('../../addressChangedOtpError.screen');
const helper = require('../../ios.helper');

class AddressChangedOtpError extends BaseAddressChangedOtpErrorScreen {
    constructor() {
        super();
        this.title = helper.byName('errorPageHeader');
        this.addressChangedOtpErrorMessage = helper.byPartialValue('Sorry, as you have recently changed your address we can\'t send you a One Time Password.');
        this.goToLogonButton = helper.byName('errorPageContinueButton');
    }
}

module.exports = AddressChangedOtpError;
