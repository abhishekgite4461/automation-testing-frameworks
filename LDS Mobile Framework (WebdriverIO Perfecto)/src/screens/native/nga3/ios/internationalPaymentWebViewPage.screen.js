const BaseInternationalPaymentWebViewPageScreen = require('../../internationalPaymentWebViewPage.screen');
const helper = require('../../ios.helper');

class InternationalPaymentWebViewPageScreen extends BaseInternationalPaymentWebViewPageScreen {
    constructor() {
        super();
        this.IPtitle = helper.byName('International payment');
    }
}

module.exports = InternationalPaymentWebViewPageScreen;
