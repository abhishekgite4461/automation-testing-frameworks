const BaseRenameAccountScreen = require('../../renameAccount.screen');
const helper = require('../../ios.helper');

class RenameAccountScreen extends BaseRenameAccountScreen {
    constructor() {
        super();
        this.title = helper.byName('Rename account');
        this.renameButton = helper.byName('renameButton');
        this.textField = helper.byName('renameTextField');
        this.confirmationBox = helper.byName('AlertView');
        this.backToYourAccountsButton = helper.byName('alertActionConfirmButton');
    }
}

module.exports = RenameAccountScreen;
