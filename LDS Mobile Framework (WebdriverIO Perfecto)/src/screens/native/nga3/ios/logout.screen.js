const BaseLogoutScreen = require('../../logout.screen');
const helper = require('../../ios.helper');

class LogoutScreen extends BaseLogoutScreen {
    constructor() {
        super();
        this.title = helper.byName('errorPageHeader');
        this.loginButton = helper.byName('errorPageContinueButton');
        this.loggOffcompleteMessage = helper.byPartialLabel('You\'re now logged');
        this.appResetcompleteMessage = '//*[contains(@label,"All done? See you soon") or contains(@label,"Thanks for using Mobile Banking.") or contains(@label,"now logged out") or contains(@label,"now logged off")]';
        this.sessionTimedOutMessage = '//*[contains(@value,"This session has timed out.") or contains(@value,"This session’s timed out.")]';
        this.surveyButton = helper.byName('logoffPageStartSurveyButton');
    }
}

module.exports = LogoutScreen;
