const BaseAddressListScreen = require('../../../changeOfAddress/addressList.screen');
const helper = require('../../../ios.helper');

class AddressListScreen extends BaseAddressListScreen {
    constructor() {
        super();
        this.title = helper.byName('coaAddressListHeadingView');
        this.addressListText = helper.byName('coaAddressListSubTitleView');
        this.addressNotOnListButton = helper.byName('coaAddressListMissingButton');
        this.addressToBeSelected = helper.byName('coaAddressItemView-0');
        this.addressToBeSelectedUAT = helper.byPartialName('Red Lion Court');
        this.addressListAllItems = (number) => helper.byName(`coaAddressItemView-${number}`);
        this.addressListItem = helper.byPartialName('coaAddressItemView');
        this.addressList = helper.byName('coaAddressListView');
    }
}

module.exports = AddressListScreen;
