const BaseAddressChangedScreen = require('../../../changeOfAddress/addressList.screen');
const helper = require('../../../ios.helper');

class AddressChangedScreen extends BaseAddressChangedScreen {
    constructor() {
        super();
        this.titleIcon = helper.byName('coaSuccessScreenTitleIcon');
        this.title = helper.byName('coaSuccessScreenTitleView');
        this.addressChangeSuccessMessage = helper.byName('coaSuccessScreenMessageView');
        this.backToHomeButton = helper.byName('coaHomeButton');
        this.homeInsuranceLeadTitle = helper.byName('HomeInsurance/background');
        this.homeInsuranceLeadIcon = helper.byName('HomeInsurance/home');
        this.homeInsuranceLeadButton = '//*[@name="HomeInsurance/home"]/following-sibling::XCUIElementTypeButton';
    }
}

module.exports = AddressChangedScreen;
