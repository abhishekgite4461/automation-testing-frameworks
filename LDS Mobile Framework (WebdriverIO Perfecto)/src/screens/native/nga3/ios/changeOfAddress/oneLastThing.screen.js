const BaseOneLastThingScreen = require('../../../changeOfAddress/oneLastThing.screen');
const helper = require('../../../ios.helper');

class OneLastThingScreen extends BaseOneLastThingScreen {
    constructor() {
        super();
        this.title = helper.byName('coaConfirmationScreenTitle');
        this.oneLastThingSubtitleText = (number) => helper.byNameAndIndex('coaConfirmationAccountInfoTitleView', number);
        this.oneLastThingText = (number) => helper.byNameAndIndex('coaConfirmationAccountInfoContentView', number);
        this.oneLastThingSubtitles = helper.byName('coaConfirmationAccountInfoTitleView');
        this.oneLastThingTexts = helper.byName('coaConfirmationAccountInfoContentView');
        this.iUnderstandButton = helper.byName('coaConfirmationButton');
        this.enterPasswordDialogTitle = helper.byName('passwordAlertTitleLabel');
        this.dialogPasswordField = helper.byName('passwordAlertPasswordInputField');
        this.dialogForgottenPassword = helper.byName('passwordAlertActionForgotPasswordButton');
        this.dialogCancelButton = helper.byName('alertActionCancelButton');
        this.dialogOkButton = helper.byName('alertActionConfirmButton');
    }
}

module.exports = OneLastThingScreen;
