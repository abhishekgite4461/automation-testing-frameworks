const BaseConfirmAddressScreen = require('../../../changeOfAddress/confirmAddress.screen');
const helper = require('../../../ios.helper');

class ConfirmAddressScreen extends BaseConfirmAddressScreen {
    constructor() {
        super();
        this.title = helper.byName('coaAddressDetailsScreenTitle');
        this.mapView = helper.byName('coaAddressDetailsMapView');
        this.mapPlaceholder = helper.byName('coaAddressDetailsMapPlaceholderView');
        this.newAddress = helper.byName('coaAddressDetailsAddressView');
        this.confirmAddressButton = helper.byName('coaAddressDetailsContinueButton');
    }
}

module.exports = ConfirmAddressScreen;
