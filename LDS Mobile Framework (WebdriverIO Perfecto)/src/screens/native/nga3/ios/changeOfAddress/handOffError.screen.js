const BaseHandOffErrorScreen = require('../../../changeOfAddress/handOffError.screen');
const helper = require('../../../ios.helper');

class HandOffErrorScreen extends BaseHandOffErrorScreen {
    constructor() {
        super();
        this.title = helper.byName('coaErrorTitleView');
        this.handOffErrorText = helper.byName('coaErrorMessageView');
        this.callUsButton = helper.byName('coaErrorCallUsButton');
        this.backToPersonalDetailsButton = helper.byName('coaErrorPersonalDetailButton');
        this.forcedLogoffTitle = helper.byName('errorPageHeader');
        this.forcedLogoffMessage = helper.byName('errorPageMessage');
        this.forcedLogoffLogoonButton = helper.byName('errorPageContinueButton');
    }
}

module.exports = HandOffErrorScreen;
