const BasePostCodeEntryScreen = require('../../../changeOfAddress/postCodeEntry.screen.js');
const helper = require('../../../ios.helper');

class PostCodeEntryScreen extends BasePostCodeEntryScreen {
    constructor() {
        super();
        this.title = helper.byName('coaPostcodeScreenTitle');
        this.subtitle = helper.byName('coaPostcodeFieldLabel');
        this.postCode = helper.byName('coaPostcodeInputField');
        this.postCodeTipView = helper.byName('coaPostcodeInputTipView');
        this.enterPostcodeHelpText = helper.byName('coaPostcodeAddressChangeInfoView');
        this.enterPostcodeCallUsText = helper.byName('coaPostcodeCallUsView');
        this.backArrow = '(//XCUIElementTypeButton)[1]';
        this.findAddress = helper.byName('coaPostcodeSubmitButton');
        this.keyboardDeleteButton = helper.byPartialName('elete');
        this.webviewChangeAddress = helper.byName('webJourneyWebView');
    }
}

module.exports = PostCodeEntryScreen;
