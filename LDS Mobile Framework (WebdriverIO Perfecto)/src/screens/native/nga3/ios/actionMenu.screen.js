const BaseActionMenuScreen = require('../../actionMenu.screen');
const helper = require('../../ios.helper');

class ActionMenuScreen extends BaseActionMenuScreen {
    constructor() {
        super();
        this.title = helper.byName('actionMenuCloseButton');
        this.viewTransactionsOption = helper.byName('View transactions');
        this.transferAndPaymentOption = helper.byName('actionMenuPaymentsAndTransfers');
        this.sendMoneyOutsideTheUKOption = helper.byName('Send money outside the UK');
        this.standingOrderOption = helper.byName('actionMenuStandingOrders');
        this.directDebitOption = helper.byName('actionMenuDirectDebits');
        this.saveTheChangeOption = helper.byName('Save the Change®');
        this.changeAccountTypeOption = helper.byName('Change account type');
        this.orderPaperStatementOption = helper.byName('actionMenuOrderPaperStatements');
        this.lostOrStolenCardOption = helper.byName('actionMenuLostOrStolenCards');
        this.replacementCardAndPinOption = helper.byName('actionMenuReplacementCardsAndPins');
        this.viewPendingPaymentOption = helper.byName('actionMenuViewPaymentsDueSoon');
        this.closeButton = helper.byName('actionMenuCloseButton');
        this.renewYourSavingsAccountOption = helper.byName('actionMenuRenewYourSavingsAccount');
        this.viewInterestRateOption = helper.byName('actionMenuViewInterestRateDetails');
        this.balanceMoneyTransferOption = helper.byName('actionMenuBalanceAndMoneyTransfers');
        this.manageCreditLimitOption = helper.byName('actionMenuManageYourCreditLimits');
        this.pdfStatementsOption = helper.byName('actionMenuPdfStatements');
        this.setupInstallmentOption = helper.byName('actionMenuSetUpInstalmentPlan');
        this.maintainInstallmentOption = helper.byName('actionMenuManageInstalmentPlans');
        this.reactivateIsaOption = helper.byName('actionMenuReactivateIsa');
        this.topupIsaOption = helper.byName('actionMenuTopUpIsa');
        this.applyForOverdraftOption = helper.byName('actionMenuOverdraft');
        this.loanAdditionalPaymentOption = helper.byName('actionMenuAdditionalPayment');
        this.loanRepaymentHolidayOption = helper.byName('actionMenuRepaymentHoliday');
        this.loanClosureOption = helper.byName('actionMenuLoanClosure');
        this.loanBorrowMoreOption = helper.byName('Borrow more');
        this.loanAnnualStatementsOption = helper.byName('actionMenuAnnualStatements');
        this.pendingPaymentOption = helper.byLabel('View pending payments Button');
        this.interestDetailRow = helper.byName('View interest rate details');
        this.payCreditCardOption = helper.byName('actionMenuPayCreditCard');
        this.reactivateISA = helper.byName('Reactivate ISA');
        this.cardManagementOption = helper.byName('actionMenuCardManagement');
        this.manageOverdraftOption = helper.byName('actionMenuManagePlannedOverdraft');
        this.chequeDepositOption = helper.byName('actionMenuDepositCheque');
        this.selectViewInterestRateDetails = helper.byLabel('Check interest rate details');
        this.viewUpcomingPayment = helper.byName('View upcoming payments');
        this.actionMenuHeader = '//*[@name="ActionMenuTableView"]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]';
        this.externalAccountActionMenu = helper.byName('ActionMenuTableView');
        this.externalAccountMenuOptionsLabel = (label) => `${helper.byLabel(`${label}`)}`;
        this.externalBankAccountNumberOfActionMenuOptions = '//*[@name="ActionMenuTableView"]//*[contains(@name, "OptionCell")]';
        this.actionMenuOption = (index) => `(//*[@name="ActionMenuTableView"]//*[contains(@name, "OptionCell")]
        //XCUIElementTypeStaticText)[${index}]`;
        this.changeAccountTypeOption = helper.byName('actionMenuChangeAccountType');
        this.makeAnOverpaymentOption = helper.byName('actionMenuMortgageOverpayment');
        this.pensionTransferOption = helper.byName('actionMenuPensionTransfer');
        this.viewSpendingInsights = helper.byName('View spending insights');// TODO: STAMOB-1508 Change ID
        this.pensionTransfer = helper.byName('actionMenuPensionTransfer');
        this.sendBankDetailsOption = helper.byName('actionMenuShareAccountDetails');
        this.shareAccountDetail = helper.byName('Share account details'); // //TODO: PAYMOB-2193 add ID
        this.standingOrderAndDirectDebitOption = helper.byName('Standing orders & Direct Debits');
        this.renameAccountOption = helper.byName('Rename account');
    }
}

module.exports = ActionMenuScreen;
