const BaseSaveTheChangeNativeScreen = require('../../saveTheChangeNative.screen');
const helper = require('../../ios.helper');

class SaveTheChangeNativeScreen extends BaseSaveTheChangeNativeScreen {
    constructor() {
        super();
        this.infoPageNavigator = helper.byName('pageControlId');
        this.setUpSTCOption = helper.byName('Set up Save the Change®');
        this.alreadyRegistered = helper.byName('saveTheChangeTurnOffLabelText');
        this.openNewSavingAccount = helper.byName('SaveTheChangeOpenAccountButton');
        this.stcSavingAccount = helper.byName('viewRemitterCell');
        this.acceptTC = helper.byName('checkbox');
        this.turnONSTC = helper.byName('saveTheChangeReviewRegistrationContinueButton');
        this.stcSuccess = helper.byName('saveTheChangeRegistrationSuccessHeader');
        this.backToAccounts = helper.byName('saveTheChangeRegistrationSuccessContinueButton');
        this.saveTheChangeTurnOffButton = helper.byName('saveTheChangeTurnOffButton');
        this.SaveTheChangeDeleteSuccessHeader = helper.byName('SaveTheChangeDeleteSuccessHeader');
    }
}

module.exports = SaveTheChangeNativeScreen;
