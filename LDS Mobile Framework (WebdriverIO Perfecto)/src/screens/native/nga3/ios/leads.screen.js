const BaseLeadsScreen = require('../../leads.screen');
const helper = require('../../ios.helper');

class LeadsScreen extends BaseLeadsScreen {
    constructor() {
        super();
        this.leadsholder = helper.byName('leadsInterstitialWebView');
        this.leadsButton = helper.byName('leadsInterstitialConfirmAndContinueButton');
        this.endOfThePage = helper.byName('These Terms shall be governed by and construed in accordance with'
            + ' English law. Disputes arising in connection with'
            + ' these Terms shall be subject to the exclusive jurisdiction'
            + ' of the English courts.');
        this.bigPromptEndOfPage = helper.byName('200');
        this.bigPromptLink = helper.byName('here');
        this.highestPriorityLead = helper.byName('This Lead has the HIGHEST priority');
        this.productHubLead = helper.byPartialName('nga:product-hub');
        this.callUsLead = helper.byPartialName('nga:callus');
        this.personalDetailsLead = helper.byPartialName('nga:update-personal-details');
        this.updatePhoneLead = helper.byPartialName('nga:update-personal-details-phone');
        this.updateAddressLead = helper.byPartialName('nga:update-personal-details-address');
        this.spendingRewardsRegistrationLead = helper.byPartialName('nga:spending-rewards-registration');
        this.securitySettingsLead = helper.byPartialName('nga:security-settings');
        this.callUsInternetBankingLead = helper.byPartialName('nga:call-us-internet-banking');
        this.callUsOtherBankingQueryLead = helper.byPartialName('nga:call-us-other-banking-query');
        this.callUsLostOrStolenLead = helper.byPartialName('nga:call-us-lost-or-stolen');
        this.callUsSuspectedFraudLead = helper.byPartialName('nga:call-us-suspected-fraud');
        this.callUsMedicalAssistanceLead = helper.byPartialName('nga:call-us-medical-assistance');
        this.callUsEmergencyCashLead = helper.byPartialName('nga:call-us-emergency-cash');
        this.callUsNewCurrentAccountLead = helper.byPartialName('nga:call-us-new-current-account');
        this.callUsNewCreditCardLead = helper.byPartialName('nga:call-us-new-credit-card');
        this.callUsNewIsaAccountLead = helper.byPartialName('nga:call-us-new-isa-account');
        this.callUsNewLoanLead = helper.byPartialName('nga:call-us-new-loan');
        this.callUsNewMortgageLead = helper.byPartialName('nga:call-us-new-mortgage');
        this.callUsNewSavingsLead = helper.byPartialName('nga:call-us-new-savings');
        this.asm1Lead = helper.byName('leadWebView');
        // TODO PIDI-607
        this.asmChequeTile = helper.byLabel('Promotion.');
        this.chequeHeader = helper.byLabel('Cheque scanning');
        this.depositCheque = helper.byLabel('Deposit Cheque');
        this.viewDemo = helper.byLabel('View Demo');
        this.chequeDemo = helper.byName('demoPageControl');
        this.closeDemo = helper.byLabel('Close Demo');
        this.payChequeHeader = helper.byLabel('Pay in cheques with your app');
        this.payChequeDesc = '//XCUIElementTypeStaticText[2]';
        this.addExternalAccountLink = helper.byValue('Add External Account Now');
    }
}

module.exports = LeadsScreen;
