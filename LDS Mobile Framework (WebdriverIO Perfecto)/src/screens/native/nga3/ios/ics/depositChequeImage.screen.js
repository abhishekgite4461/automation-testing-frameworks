const BaseDepositChequeImageScreen = require('../../../ics/depositChequeImage.screen');
const helper = require('../../../ios.helper');

class DepositChequeImageScreen extends BaseDepositChequeImageScreen {
    constructor() {
        super();
        this.imageCloseButton = helper.byName('zoomInCloseButton');
    }
}

module.exports = DepositChequeImageScreen;
