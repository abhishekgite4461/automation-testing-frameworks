const BaseDepositHistoryReviewScreen = require('../../../ics/depositHistoryReview.screen');
const helper = require('../../../ios.helper');

class DepositHistoryReviewScreen extends BaseDepositHistoryReviewScreen {
    constructor() {
        super();
        this.tapHintFrontImage = helper.byName('depositDetailsFrontZoomButton');
        this.depositStatus = helper.byName('depositDetailsStatusValueLabel');
        this.depositAccountType = helper.byName('depositDetailsAccountName');
        this.depositedDate = helper.byName('depositDetailsDisplayDateLabel');
        this.depositAmount = helper.byName('depositDetailsDisplayAmountLabel');
        this.depositReference = helper.byName('depositDetailsdepositDetailsReferenceValueLabel');

        this.pendingDetailPageTitle = helper.byLabel('Pending');
        this.statusTitleOnReviewPage = helper.byName('depositDetailsReasonForStatusTextView');

        this.fundsAvailableDetailPageTitle = helper.byLabel('Funds available');

        this.RejectedChequePageTitle = helper.byLabel('Rejected');
        this.depositHistorybutton = helper.byName('chequeSuccessDepositDepositHistoryButton');
        this.depositBusiness = helper.byName('icsBusinessName');
    }
}

module.exports = DepositHistoryReviewScreen;
