const Screen = require('../../../../screen');
const helper = require('../../../ios.helper');

class ChequeDepositScreen extends Screen {
    constructor() {
        super();
        this.chequeAmountField = helper.byName('amountTextbox');
        this.chequeReferenceField = helper.byName('referenceTextbox');
        this.title = helper.byName('depositChequeTab');
        this.chequeLimitService = helper.byName('amountHelpText');
        this.chequeErrorMsg = helper.byName('errorMessage');
        this.depositAccount = helper.byName('depositChequeArrangementView');
        this.captureFrontOfChequeButton = helper.byName('depositChequeFrontCameraButton');
        this.captureBackOfImageButton = helper.byName('depositChequeBackCameraButton');
        this.useChequeImageButton = helper.byName('Use');
        this.moreInformationDialogbox = helper.byName('moreInformationAlertTitle');
        this.moreInformationOkButton = helper.byName('moreInformationAlertOkButton');
        this.depositChequeDisabled = helper.byName('depositSwitchesOffTitle');
        this.allowCamera = helper.byName('OK');
        this.closeDemobutton = helper.byName('demoCloseButton');
        this.closeCamerabutton = helper.byName('closeButton');
        this.denyPermission = helper.byName('Don’t Allow');
        this.settingButton = helper.byName('Go to Settings');
        this.depositHistoryText = helper.byName('depositHistoryTab');
        this.depositHomeBackButton = '//XCUIElementTypeNavigationBar/XCUIElementTypeButton[1]';
        this.winBackTitle = helper.byName('depositChequeWinbackTitle');
        this.homeAccountActionButton = helper.byName('actionMenu');
        this.passwordOkButton = helper.byLabel('alertActionConfirmButton');
        this.frontOfChequeThumbnail = helper.byName('depositChequeFrontCameraButton');
        this.backOfChequeThumbnail = helper.byName('depositChequeBackCameraButton');
        this.cameraPopupAlert = helper.byName('OK');
        this.reviewDepositButton = helper.byName('depositChequeReviewButton');
        this.errorMessageOkButton = helper.byName('OK');
        this.fastChequeIcon = helper.byName('paymlogo');
        this.stayButton = helper.byName('alertActionCancelButton');
        this.leaveButton = helper.byName('alertActionConfirmButton');
        this.currentMainBalance = helper.byName('depositChequeArrangementView');
        this.problemScanningAlertPopupMessage = helper.byName('infoCloseButton');
        this.chequeAmountFeildHelpText = helper.byName('amountHelpText');
        this.chequeReferenceFieldHelpText = helper.byName('referenceHelpText');
    }
}

module.exports = ChequeDepositScreen;
