const BaseReviewDepositScreen = require('../../../ics/reviewDeposit.screen');
const helper = require('../../../ios.helper');

class ReviewDepositScreen extends BaseReviewDepositScreen {
    constructor() {
        super();
        this.title = helper.byName('chequeReviewDepositDepositInformationTextView');
        this.reviewDepositConfirmButton = helper.byName('chequeReviewDepositConfirmButton');
        this.frontOfChequeHint = helper.byName('chequeReviewDepositFrontReviewImageButton');
        this.BackOfChequeHint = helper.byName('chequeReviewDepositBackReviewImageButton');
        this.depositHistorybutton = helper.byName('chequeSuccessDepositDepositHistoryButton');
        this.passwordConfirmationText = helper.byName('passwordAlertPasswordInputField');
        this.passwordConfirmButton = helper.byName('alertActionConfirmButton');
        this.amountMismatchConfirm = helper.byName('alertActionConfirmButton');
        this.amountMismatchCancel = helper.byName('alertActionCancelButton');
        this.amountConfirmAlert = helper.byName('Confirm amount');
        this.reviewBusinessName = helper.byName('depositDetailsBusinessName');
        this.reviewDepositAccount = helper.byName('depositDetailsAccountName');
        this.reviewChequeAmount = helper.byName('chequeReviewDepositDepositAmountLabel');
        this.reviewReferenceText = helper.byName('chequeReviewDepositUserReferenceLabel');
        this.reviewFrontofCheque = helper.byName('chequeReviewDepositFrontReviewImageButton');
        this.reviewFrontChequeMagnifierIcon = helper.byName('ZoomInFrontIcon');
        this.reviewBackofCheque = helper.byName('chequeReviewDepositBackReviewImageButton');
        this.reviewBackChequeMagnifierIcon = helper.byName('ZoomInBackIcon');
        this.reviewCancelButton = helper.byName('chequeReviewDepositDepositCancelButton');
        this.reviewDepositConfirmButton = helper.byName('chequeReviewDepositConfirmButton');
    }
}

module.exports = ReviewDepositScreen;
