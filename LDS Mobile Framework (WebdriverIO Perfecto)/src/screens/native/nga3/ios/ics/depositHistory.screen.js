const BaseDepositHistoryScreen = require('../../../ics/depositHistory.screen');
const helper = require('../../../ios.helper');

class DepositHistoryScreen extends BaseDepositHistoryScreen {
    constructor() {
        super();
        this.depositChequeText = helper.byName('depositChequeTab');
        this.depositHistoryTitle = helper.byName('depositHistoryTab');
        this.helpandInfoNavigationTile = helper.byName('depositHistoryHelpInfo');
        this.depositHistoryList = helper.byName('depositHistoryTableView');
        this.depositDate = helper.byName('depositHistoryDateLabel');
        this.depositReference = helper.byName('depositHistoryReferenceLabel');
        this.depositStatus = helper.byName('depositHistoryStatusLabel');
        this.depositAmount = helper.byName('depositHistoryAmountLabel');
        this.accountNavigationImage = helper.byName('depositHistoryArrangementView');
        this.accountList = helper.byName('viewRemitterBusinessName');
        this.changeAccount = "(//*[@name='viewRemitterBusinessName'])[2]";
        this.noMoretransactionMessage = helper.byName('dispositHistoryFooterLabel');
        // TODO : MOB3-9904
        this.noTransactionMessage = helper.byPartialLabel('99 days.');
        this.accountPicker = helper.byName('depositHistoryArrangementView');
        this.referenceTextVisibility = (reference) => `//*[contains(@label,'${reference}')]
        [@name='depositHistoryReferenceLabel']`;
        this.referenceOnTransactionDetailPage = (reference) => `//*[contains(@label,'${reference}')]
        [@name='depositDetailsdepositDetailsReferenceValueLabel']`;
        this.referenceIsNotVisibleOnDetailPage = (reference) => `//*[contains(@label,'${reference}')]
        [@name='depositDetailsdepositDetailsReferenceValueLabel']`;
        this.selectPendingOption = helper.byLabel('Pending');
        this.selectFundsAvailableOption = helper.byLabel('Funds available');
        this.selectRejectedOption = helper.byLabel('Rejected');
        this.selectReferenceOnTransactionList = helper.byName('depositHistoryAmountLabel');
    }
}

module.exports = DepositHistoryScreen;
