const BaseIcsMoreInformation = require('../../../ics/icsMoreInformation.screen');
const helper = require('../../../ios.helper');

class IcsMoreInformation extends BaseIcsMoreInformation {
    constructor() {
        super();
        this.moreInformationDialogbox = helper.byName('moreInformationAlertTitle');
        this.moreInformationCheckbox = helper.byName('moreInformationAlertCheckBoxButton');
        this.moreInformationText = helper.byName('moreInformationButton');
        this.moreInformationOkButton = helper.byName('moreInformationAlertOkButton');
        this.moreInformationContent = helper.byName('moreInformationAlertBodyText');
    }
}

module.exports = IcsMoreInformation;
