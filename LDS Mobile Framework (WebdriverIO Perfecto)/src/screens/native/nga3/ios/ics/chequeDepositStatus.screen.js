const BaseChequeDepositStatusScreen = require('../../../ics/chequeDepositStatus.screen');
const helper = require('../../../ios.helper');

class ChequeDepositStatusScreen extends BaseChequeDepositStatusScreen {
    constructor() {
        super();
        this.title = helper.byName('chequeSuccessDepositSuccessLabel');
        this.fastChequeIcon = helper.byName('paymlogo');
        this.viewDepositHistoryButton = helper.byName('chequeSuccessDepositDepositHistoryButton');
        this.sortCodeError = helper.byPartialValue('can’t process this type of cheque using cheque imaging');
        this.duplicateChequeMessage = '//*[contains(@value,"already deposited a cheque") or contains(@value,"already paid in a cheque")]';
        this.dailyLimitError = helper.byPartialValue('daily limit for cheque');
        this.depositAnotherCheque = helper.byName('chequeSuccessDepositdepositChequeButton');
        this.amountDeposited = helper.byPartialValue('chequeSuccessDepositAmountValue');
        this.successBusinessName = helper.byName('depositDetailsBusinessName');
        this.successSortCode = helper.byName('Sort code. 770802');
        this.successDepositAccount = helper.byName('depositDetailsAccountName');
        this.dueDate = helper.byName('chequeSuccessDepositDueDateValue');
        this.successReferenceText = helper.byName('chequeSuccessDepositReferenceValue');
    }
}

module.exports = ChequeDepositStatusScreen;
