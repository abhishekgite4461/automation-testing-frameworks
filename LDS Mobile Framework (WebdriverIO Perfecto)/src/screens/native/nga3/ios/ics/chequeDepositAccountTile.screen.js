const BaseChequeDepositAccountTileScreen = require('../../../ics/chequeDepositAccountTile.screen');
const helper = require('../../../ios.helper');

class ChequeDepositAccountTileScreen extends BaseChequeDepositAccountTileScreen {
    constructor() {
        super();
        this.accountsDisplayed = helper.byName('arrangementTableViewAccountBalance');
    }
}

module.exports = ChequeDepositAccountTileScreen;
