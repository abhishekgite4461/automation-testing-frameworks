const BaseCallUsHomePageScreen = require('../../../helpAndInfo/callUsHomePage.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class CallUsHomePageScreen extends BaseCallUsHomePageScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'clickToCallHubTitle');
        this.closeButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarCloseButton');
        this.yourAccountsTile = helper.byResourceId(ViewClasses.BUTTON, 'clickToCallHubArrangementsTile');
        this.personalAccountsTile = helper.byResourceId(ViewClasses.BUTTON, 'clickToCallHubArrangementsTile');
        this.newProductEnquireTile = helper.byResourceId(ViewClasses.BUTTON, 'clickToCallHubNewProductTile');
        this.internetBankingTile = helper.byResourceId(ViewClasses.BUTTON, 'clickToCallHubInternetBankingTile');
        this.otherBankingQueryTile = helper.byResourceId(ViewClasses.BUTTON, 'clickToCallHubOtherQueryTile');
        this.securityTravelLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'clickToCallHubSecurityAndTravelEmergenciesLabel');
        this.suspectedFraudTab = helper.byText(ViewClasses.TEXT_VIEW, 'Suspected fraud');
        this.lostOrStolenCardTab = helper.byText(ViewClasses.TEXT_VIEW, 'Lost or stolen cards');
        this.emergencyCashAbroadTab = helper.byText(ViewClasses.TEXT_VIEW, 'Emergency cash abroad');
        this.medicalAssistAbroadTab = helper.byText(ViewClasses.TEXT_VIEW, 'Medical assistance abroad');
        this.globalMenuButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationHeaderMenuButton');
        this.unAuthMenuButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'preAuthMenu');
        this.businessAccountsTile = helper.byResourceId(ViewClasses.BUTTON, 'clickToCallHubArrangementsTile');
        this.cmaSurveyLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'clickToCallCompetitionSurveyTextView');
        this.downloadOption = helper.byText(ViewClasses.BUTTON, 'Download');
    }
}
module.exports = CallUsHomePageScreen;
