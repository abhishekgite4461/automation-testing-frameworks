const BaseHomeScreen = require('../../home.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class HomeScreen extends BaseHomeScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.VIEW_GROUP, 'navigationTabHome');
        this.reportIssueButton = helper.byText(ViewClasses.TEXT_VIEW, 'Report Issue');
        this.homeContainer = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'baseContent');
        this.account = (accountName) => helper.byText(ViewClasses.TEXT_VIEW,
            `${accountName}`);
        this.accountIterator = (accountName) => `//*[@text="${accountName}"]
         /ancestor::android.widget.RelativeLayout[1]`;
        this.accountContainer = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'accountTile');
        this.allBusinessName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileABusinessName');
        this.accountTileContainer = (index) => helper.byContentDesc(ViewClasses.RELATIVE_LAYOUT, `accountTile${index}`);
        this.accountSpecificActionMenu = (accountName) => `//*[contains(@text,"${accountName}")]/../*
            [contains(@resource-id,'accountTileActionMenu')]`;
        this.firstClickableAccountTile = () => "//*[contains(@resource-id, 'arrangementsCardList')]/android.widget.FrameLayout[position()=2 and contains(@resource-id, 'accountTile')]";
        this.dismissErrorMessage = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'notificationCloseImageView');
        this.pendingPaymentErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'You have an urgent request awaiting your approval. Please go to the desktop site');
        this.accountDetailTile = (index) => helper.byContentDesc(ViewClasses.RELATIVE_LAYOUT, `accountTile${index}`);
        this.primaryAccount = `(${helper.byPartialResourceId(ViewClasses.FRAME_LAYOUT, 'accountTile')}${helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'accountTileAccountName')})[1]`;
        this.accountNameLabel = (account) => helper.byText(ViewClasses.TEXT_VIEW, `${account}`);
        this.asmLeadTile = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'styleModuleAccount');
        this.accountName = (index) => `${helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, `accountTile${index}`)}
        ${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileAccountName')}`;
        this.accountTileFooter = (index) => `${this.accountTileContainer(index)}
        /descendant::android.widget.TextView[contains(@text,'£')][last()]`;
        this.accountNameElement = (index) => browser.element(this.accountName(index));
        this.balanceField = (index) => `${this.accountTileContainer(index)}
        /descendant::android.widget.TextView[last()]`;
        this.cbsLoanAccountNumber = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileAccountNumber')}`;
        this.cbsLoanSortCode = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileAccountSortCode')}`;
        this.cbsCurrentBalance = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMainBalance')}`;
        this.accountNumber = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileAccountNumber')}`;
        this.sortCode = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileAccountSortCode')}`;
        this.accountBalance = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMainBalance')}`;
        this.accountInfoBalance = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileTopValue')}`;
        this.accountTileLogo = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.IMAGE_VIEW, 'accountTileCoServiceIcon')}`;
        this.interestRate = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMidValue')}`;
        this.overdraftLimit = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMidValue')}`;
        this.creditCardAccountNumber = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileReferenceNumber')}`;
        this.creditCardAccountBalance = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMainBalance')}`;
        this.interestRateForIsa = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileInterestRateText')}`;
        this.availableCredit = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileAvailableCreditText')}`;
        this.overdueAmount = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileOverdueMoneyText')}`;
        this.mortgageAccountNumber = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileReferenceNumber')}`;
        this.mortgageMonthlyPayment = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMonthlyPayment')}`;
        this.mortgageBalanceOnDate = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileBalanceDate')}`;
        this.loanAccountNumber = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileReferenceNumber')}`;
        this.loanCurrentBalance = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMainBalance')}`;
        this.originalLoanAmount = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileOriginalLoanBalanceText')}`;
        this.maturityDate = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileTopValue')}`;
        this.alertMessage = (account) => `(//*[@text='${account}'])[1]/parent::android.widget.RelativeLayout
        /descendant::android.widget.TextView[contains(@resource-id,':id/arrangementTileAlertNotification')]`;
        this.registerForEverydayOffer = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'everydayOffersCard');
        this.actionMenu = helper.byResourceId(ViewClasses.IMAGE_BUTTON, 'accountTileActionMenu');
        this.transferPaymentTile = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'paymentsAndTransfers');
        this.yourAccountsTile = helper.byContentDesc(ViewClasses.RELATIVE_LAYOUT, 'homeTileYourAccounts');
        this.atmBranchFinderTile = helper.byContentDesc(ViewClasses.RELATIVE_LAYOUT, 'homeTileATMBranchFinder');
        this.helpInfoTile = helper.byContentDesc(ViewClasses.RELATIVE_LAYOUT, 'homeTileHelpAndInfo');
        this.everydayOfferTile = helper.byContentDesc(ViewClasses.RELATIVE_LAYOUT, 'homeTileEverydayOffer');
        this.callUsTile = helper.byText(ViewClasses.TEXT_VIEW, 'Call us');
        this.globalMenuButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationHeaderMenuButton');
        this.notAvailableInterestRateValue = helper.byText(ViewClasses.TEXT_VIEW, 'Not available');
        this.percentInterestRateValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMidValue');
        this.tapAndHoldInterestRateValue = helper.byText(ViewClasses.TEXT_VIEW, 'Tap & hold for rates');
        this.accountTile = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'accountTileContentView');
        this.accountTileIndex = (account) => `${account}/../../../android.widget.LinearLayout`;
        this.hostSystemDownErrorMessage = helper.byText(ViewClasses.TEXT_VIEW, "Sorry, we can't show some of the account info you asked for right now. Please try later.");
        this.firstAccountBalance = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMainBalance');
        this.firstAvailableBalance = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileTopValue');
        this.actionMenuOfCurrentAccount = helper.byContentDesc(ViewClasses.IMAGE_BUTTON, 'Menu for Islamic Current Account. Shows all options for this account');
        this.actionMenuOfSavingAccount = helper.byContentDesc(ViewClasses.IMAGE_BUTTON, 'Menu for Easy Saver. Shows all options for this account');
        this.actionMenuOfisaAccount = helper.byContentDesc(ViewClasses.IMAGE_BUTTON, 'Menu for Cash ISA Saver. Shows all options for this account');
        this.actionMenuOfLoanAccount = helper.byContentDesc(ViewClasses.IMAGE_BUTTON, 'Menu for Lloyds Bank Personal Loan. Shows all options for this account');
        this.actionMenuOfMortgageAccount = helper.byContentDesc(ViewClasses.IMAGE_BUTTON, 'Menu for Your mortgage. Shows all options for this account');
        this.errorMessageInvalidAccount = helper.byPartialText(ViewClasses.TEXT_VIEW, "Sorry, you don't have any accounts that can be accessed through Mobile or Internet Banking");
        this.alertMessageDormantAccount = helper.byPartialText(ViewClasses.TEXT_VIEW, 'This account has been inactive for over');

        this.errorMessageInvalidPostcode = helper.byPartialText(ViewClasses.TEXT_VIEW, "Please enter a valid postcode. If you don't have one, leave this field blank");

        this.nonAccessibleNga = helper.byPartialText(ViewClasses.TEXT_VIEW, ' You can\'t view your investment accounts in the app or Mobile website');
        this.nonAccessibleNga = helper.byPartialText(ViewClasses.TEXT_VIEW, 'You can\'t view your investment accounts in the app or Mobile website');
        this.accountlinkLead = (account) => `${helper.byText(ViewClasses.TEXT_VIEW, account)}
        /../../../android.webkit.WebView`;
        this.actionMenuOfCreditCardAccount = helper.byContentDesc(ViewClasses.IMAGE_BUTTON, 'Menu for Lloyds Bank Choice Rewards. Shows all options for this account');
        this.accountByName = (accountName) => helper.byPartialText(ViewClasses.TEXT_VIEW, accountName);
        this.businessName = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileAccountName')}`;
        this.switchBusinessButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarSwitchButton');
        this.switchBusinessTile = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarSwitchButton');
        this.multipleNotification = helper.byResourceId(ViewClasses.VIEW_GROUP, 'notificationSummaryContainer');
        this.singleNotification = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'notificationCentreHolder');
        this.notificationWebViewJourney = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'baseContentInner')}//android.webkit.WebView[1]`;
        this.multipleNotificationView = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'homeNotificationCentreFragment');
        this.notificationCentreCloseButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'notificationCentreCloseButton');
        this.multipleNotificationCentreHolder = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'notificationCentreHolder');
        this.currentAccountWithViewTransactionsAccountName = helper.byText(ViewClasses.TEXT_VIEW, 'Islamic Current Account');
        this.offsetRepossessedMortgageName = (accountName) => `${helper.byContentDesc(ViewClasses.TEXT_VIEW,
            `Account name ${accountName}.`)}`;
        this.offsetRepossessedMortgageNumber = (accountName) => `${helper.byContentDesc(ViewClasses.TEXT_VIEW,
            `Account name ${accountName}.`)}/following::*[@resource-id,"id/accountTileReferenceNumber"][1]`;
        this.offsetRepossessedMortgageBalance = (accountName) => `${helper.byContentDesc(ViewClasses.TEXT_VIEW,
            `Account name ${accountName}.`)}/following::*[@resource-id,"id/accountTileMainBalance"][2]`;
        this.offsetRepossessedMortgageMonthlyPayment = (accountName) => `${helper.byContentDesc(ViewClasses.TEXT_VIEW,
            `Account name ${accountName}.`)}/following::*[@resource-id,"id/accountTileMonthlyPayment"][5]`;
        this.offsetRepossessedMortgageBalanceAsAtDate = (accountName) => `${helper.byContentDesc(ViewClasses.TEXT_VIEW,
            `Account name ${accountName}.`)}/following::*[@resource-id,"id/accountTileBalanceDateLabel"][8]`;
        this.homeScreenErrorBanner = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'errorNotificationView');
        this.cantApplyErrorMessage = `${helper.byPartialText(ViewClasses.TEXT_VIEW, 'You must be an authorised user')}|${helper.byPartialText(ViewClasses.TEXT_VIEW, 'YOU MUST BE AN AUTHORISED USER')}`;
        this.errorMessageFeatureUnavailable = `${helper.byPartialText(ViewClasses.TEXT_VIEW, 'This feature is not available')}|${helper.byPartialText(ViewClasses.TEXT_VIEW, 'THIS FEATURE IS NOT AVAILABLE')}`;
        this.cantMakePaymentErrorMessage = `${helper.byPartialText(ViewClasses.TEXT_VIEW, 'have an eligible account')}|${helper.byPartialText(ViewClasses.TEXT_VIEW, 'HAVE AN ELIGIBLE ACCOUNT')}`;
        this.scottishWidowsBalance = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMainBalance');
        this.scottishWidowsPolicyNumber = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileReferenceNumber')}`;
        this.externalProviderBanksName = helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'arrangementTileAlertNotification');
        this.externalBankActionMenuButton = (bankName, accountType) => `//*[contains(@content-desc, "${bankName}") 
            and contains(@content-desc, "${accountType}")]//*[contains(@resource-id, "accountTileCoServiceIcon")]
            /../..//*[contains(@resource-id, "accountTileActionMenu")]`;
        this.externalProviderBanksNameText = (number) => helper.byResourceIdAndIndex(ViewClasses.TEXT_VIEW, 'arrangementTileAlertNotification', number);
        this.externalProviderAccountTile = (bankName, accountType) => `//*[contains(@content-desc, "${bankName}")
            and contains(@content-desc, "${accountType}")]//*[contains(@resource-id, "accountTileMainBalance")]`;
        this.viewEverydayOffer = helper.byResourceId(ViewClasses.TEXT_VIEW, 'everydayOffersCardCallToAction');
        this.externalBankAccountErrorTile = (bankName) => `//*[contains(@resource-id, "accAggErrorTileMessageView") 
        and contains(@text, "${bankName}")]`;
        this.renewAccountSharingButtonOnErrorTile = (bankName) => `//*[contains
        (@resource-id, "accAggErrorTileMessageView") and contains(@text, "${bankName}")]
        /following-sibling::*[contains(@resource-id, "accAggErrorTileReconsentNowButton")]`;
        this.settingsButtonOnErrorTile = (bankName) => `//*[contains(@resource-id, "accAggErrorTileMessageView") and 
        contains(@text, "${bankName}")]/following-sibling::*[contains(@resource-id, "accAggErrorTileSettingsIcon")]`;
        this.technicalErrorEverydayOffer = helper.byResourceId(ViewClasses.TEXT_VIEW, 'notificationContentTextView');
        this.priorityMessageNormal = helper.byResourceId(ViewClasses.TEXT_VIEW, 'priorityMessageTileTitle');
        this.priorityDismissMessage = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'priorityMessageTileDismissButton');
        this.priorityMessageCallAction = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'priorityMessageUpdateTileWithAction');
        // TODO  PI-1055 Create accessibility Id
        this.updateNowMessage = helper.byText(ViewClasses.TEXT_VIEW, 'Update now');
        this.headerToolbar = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationToolbarText');
        this.welcomeMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Welcome');
        this.homeMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Home');
        this.voiceIDScreen = helper.byResourceId(ViewClasses.BUTTON, 'voiceIdOptInRegisterButton');
        this.asmVoiceIDScreen = '//android.view.View[contains(@content-desc,"Tap to register for Voice ID") or contains(@text,"Tap to register for Voice ID")]';
        this.registerVoiceIDButton = helper.byResourceId(ViewClasses.BUTTON, 'voiceIdOptInRegisterButton');
        this.thirtyTwoDayNoticeAccountNumber = (accountTileName) => `${this.accountTileContainer(accountTileName)}
        /descendant::*[contains(@resource-id,'accountTileReferenceNumber')]`;
        this.voiceIDHeading = `${helper.byPartialText(ViewClasses.TEXT_VIEW, 'Voice ID for Telephone Banking')}|${helper.byText(ViewClasses.TEXT_VIEW, 'VOICE ID FOR TELEPHONE BANKING')}`;
        this.viewRatesLinkInterestRateValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMidValue');
        this.creditCardAccountTileProgressBar = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.PROGRESS_BAR, 'accountTileAvailableCreditProgress')}`;
        this.accountTileProgressAvailableCreditLabel = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileProgressAvailableCreditLabel')}`;
        this.accountTileProgressAvailableCreditValue = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileProgressAvailableCreditValue')}`;
        this.accountTileProgressCreditLimitLabel = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileProgressCreditLimitLabel')}`;
        this.accountTileProgressCreditLimitValue = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileProgressCreditLimitValue')}`;
        this.accountTileMinimumPaymentLabel = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMinimumPaymentLabel')}`;
        this.accountTileMinimumPaymentValue = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMinimumPaymentText')}`;
        this.accountTilePaymentDueDateLabel = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTilePaymentDueLabel')}`;
        this.accountTilePaymentDueDateValue = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTilePaymentDueText')}`;
        this.accountTileStatementBalanceLabel = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileStatementBalanceLabel')}`;
        this.accountTileStatementBalanceValue = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileStatementBalanceText')}`;
        this.balanceOrMoneyTransferTile = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'balanceOrMoneyTransferTile');
        this.accountTileOverdraftLimitLabel = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileTopLabel')}`;
        this.accountTileOverdraftLimitValue = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileTopValue')}`;
        this.accountTileRemainingOverdraftLabel = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileBottomLabel')}`;
        this.accountTileRemainingOverdraftValue = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileBottomValue')}`;
        this.upcomingPaymentsQuickLink = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'quickLinksButton1')}
        //*[contains(@resource-id,"ngaButtonTitle")]`;
        this.freezeCardQuickLink = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'quickLinksButton2')}`;
        this.notificationsQuickLink = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'quickLinksButton3')}`;
        this.moreQuickLink = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'quickLinksButton4')}
        //*[contains(@resource-id,"ngaButtonTitle")]`;
        this.accountSpecificMoreQuicklink = (accountName) => `//*[@text="${accountName}"]
        /following::*[contains(@resource-id,'quickLinksButton4')]`;
        this.payCreditCard = helper.byText(ViewClasses.TEXT_VIEW, 'Pay credit card');// TODO: STAMOB-2151 add content desc or ID
        this.viewTransactions = helper.byText(ViewClasses.TEXT_VIEW, 'View transactions');// TODO: STAMOB-2151 add content desc or ID
        this.cardManagement = helper.byText(ViewClasses.TEXT_VIEW, 'Card management');// TODO: STAMOB-2151 add content desc or ID
        this.pdfStatement = helper.byText(ViewClasses.TEXT_VIEW, 'PDF statements');// TODO: STAMOB-2151 add content desc or ID
        this.manageCreditLimitAction = helper.byText(ViewClasses.TEXT_VIEW, 'Manage your credit limits');// TODO: STAMOB-2151 add content desc or ID
        this.accountSpecificUpcomingPaymentsQuicklink = (accountName) => `//*[@text="${accountName}"]
        /following::*[contains(@resource-id,'quickLinksButton1')][1]`;
        this.accountSpecificFreezeCardQuicklink = (accountName) => `//*[@text="${accountName}"]
        /following::*[contains(@resource-id,'quickLinksButton2')][1]`;
        this.accountSpecificNotificationsQuicklink = (accountName) => `//*[@text="${accountName}"]
        /following::*[contains(@resource-id,'quickLinksButton3')][1]`;
        this.payAndTransferQuickLink = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'quickLinksButton2')}
        //*[contains(@resource-id,"ngaButtonTitle")]`;
        this.manageCardQuickLink = (index) => `${this.accountTileContainer(index)}${helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'quickLinksButton3')}
        //*[contains(@resource-id,"ngaButtonTitle")]`;
        this.payAndTransferQuickLinkText = 'Pay &\ntransfer';
        this.manageCardQuickLinkText = 'Manage\ncard';
    }
}

module.exports = HomeScreen;
