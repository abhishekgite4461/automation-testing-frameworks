const BaseAppLaunchErrorScreen = require('../../appLaunchError.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class AppLaunchErrorScreen extends BaseAppLaunchErrorScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenTitle');
        this.technicalError = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Sorry, we\'re having technical problems.');
        this.internetConnectionError = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Check your Internet connection and try again');
        this.internetConnectionErrorInBrowser = 'No Internet';
    }
}

module.exports = AppLaunchErrorScreen;
