const BaseAlertPopUpScreen = require('../../alertPopUp.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class AlertPopUpScreen extends BaseAlertPopUpScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.contentText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogContentText');
        this.confirmButton = helper.byText(ViewClasses.TEXT_VIEW, 'OK');
        this.cancelButton = helper.byText(ViewClasses.TEXT_VIEW, 'Cancel');
        this.leaveButton = helper.byText(ViewClasses.BUTTON, 'Leave');
        this.stayButton = helper.byText(ViewClasses.BUTTON, 'Stay');
    }
}

module.exports = AlertPopUpScreen;
