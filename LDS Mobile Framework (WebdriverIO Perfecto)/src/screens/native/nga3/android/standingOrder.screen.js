const BaseStandingOrder = require('../../standingOrder.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class StandingOrder extends BaseStandingOrder {
    constructor() {
        super();
        this.title = helper.byContentDesc(ViewClasses.VIEW, 'Standing orders');
        this.setUpNewStandingOrderButton = helper.byContentDesc(ViewClasses.VIEW, 'Set up a standing order');
    }
}

module.exports = StandingOrder;
