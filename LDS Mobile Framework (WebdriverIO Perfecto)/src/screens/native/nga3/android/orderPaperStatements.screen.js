const BaseOrderPaperStatementsPageScreen = require('../../orderPaperStatements.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class OrderPaperStatementsPageScreen extends BaseOrderPaperStatementsPageScreen {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.VIEW, 'Order paper statements');
        this.titleConfirm = '//android.view.View[contains(@text,"Confirm your order") or contains(@text,"Confirm my order")]';
        this.firstStatement = helper.byPartialText(ViewClasses.CHECK_BOX, 'STATEMENT 1');
        this.continue = helper.byText(ViewClasses.BUTTON, 'Continue');
        this.total = helper.byPartialText(ViewClasses.VIEW, '£');
        this.changeOrder = helper.byText(ViewClasses.BUTTON, 'Change order');
        this.updateAddress = helper.byText(ViewClasses.VIEW, 'How to update your address');
        this.deliverAddress = helper.byText(ViewClasses.VIEW, 'EC1Y 4XX');
        this.confirmAndPay = helper.byText(ViewClasses.BUTTON, 'Confirm Order & Pay');
        this.chooseAccount = helper.byPartialText(ViewClasses.VIEW, 'Choose an account');
        this.accountName = (accName) => helper.byText(ViewClasses.TEXT_VIEW, accName);
    }
}

module.exports = OrderPaperStatementsPageScreen;
