const BaseAddressChangedOtpErrorScreen = require('../../addressChangedOtpError.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class AddressChangedOtpError extends BaseAddressChangedOtpErrorScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenTitle');
        this.addressChangedOtpErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Sorry, as you have recently changed your address we can\'t send you a One Time Password.');
        this.goToLogonButton = helper.byResourceId(ViewClasses.BUTTON, 'errorScreenButtonPrimary');
    }
}

module.exports = AddressChangedOtpError;
