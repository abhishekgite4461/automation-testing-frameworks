const BaseLogoutScreen = require('../../logout.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class LogoutScreen extends BaseLogoutScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'logoffPageTitle');
        this.loginButton = helper.byResourceId(ViewClasses.BUTTON, 'logoffPageLogonToMobileBankingButton');
        this.loggOffcompleteMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'You\'re now logged');
        this.appResetcompleteMessage = '//android.widget.TextView[contains(@text,"Thanks for using") or contains(@text,"See you soon") or contains(@text,"now logged out")]';
        this.sessionTimedOutMessage = '//android.widget.TextView[contains(@text,"This session has timed out.") or contains(@text,"This session\'s timed out.")]';
        this.surveyButton = helper.byResourceId(ViewClasses.BUTTON, 'logoffPageStartSurveyButton');
    }
}

module.exports = LogoutScreen;
