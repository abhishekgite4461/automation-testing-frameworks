const BaseAccountsOverviewScreen = require('../../accountsOverview.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class AccountsOverviewScreen extends BaseAccountsOverviewScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'yourAccountsContainer');
        this.alertMessage = (accountName) => `//android.widget.TextView[@text='${accountName}']
        /ancestor::android.widget.LinearLayout/
        following-sibling::*[contains(@resource-id,":id/arrangementTileAlertNotification")]`;
        this.stickyFooterPlaceHolder = '//android.widget.FrameLayout[2]/'
            + 'android.widget.FrameLayout[1]/android.webkit.WebView[1]';
        this.accountAggregationLoadingTile = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'accAggLoadingTile');
        this.externalAccountTiles = helper.byContentDesc(ViewClasses.VIEW, 'External Account');
        this.externalErrorAccountTiles = helper.byPartialResourceId(ViewClasses.FRAME_LAYOUT, 'accAggErrorTileView');
        this.openBankingPromoTile = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'accAggHomeTileView');
        this.addExternalAccount = helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'accAggHomeTileAdd');
        this.viewExternalAccount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggHomeTileViewAccountButton');
        this.earnCashbackTile = helper.byResourceId(ViewClasses.TEXT_VIEW, 'everydayOffersCardCallToAction');
        this.externalAccountTileLocator = (bankName, accountType) => (
            `//android.widget.FrameLayout[contains(@content-desc, "${bankName}") 
                    and contains(@content-desc, "${accountType}")]`);
        this.externalProviderAccountLogo = (bankName, accountType) => this.externalAccountTileLocator(bankName, accountType).concat(helper.byResourceId(ViewClasses.IMAGE_VIEW, 'accountTileCoServiceIcon'));
        this.externalProviderAccountName = (bankName, accountType) => this.externalAccountTileLocator(bankName, accountType).concat(helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileAccountName'));
        this.externalProviderAccountNumber = (bankName, accountType) => this.externalAccountTileLocator(bankName, accountType).concat(helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileAccountNumber'));
        this.externalProviderCreditCardNumber = (bankName, accountType) => this.externalAccountTileLocator(bankName, accountType).concat(helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileReferenceNumber'));
        this.externalProviderAvailableCredit = (bankName, accountType) => this.externalAccountTileLocator(bankName, accountType).concat(helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggAvailableCreditLabel'));
        this.externalProviderAccountSortCode = (bankName, accountType) => this.externalAccountTileLocator(bankName, accountType).concat(helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileAccountSortCode'));
        this.externalProviderAccountBalance = (bankName, accountType) => this.externalAccountTileLocator(bankName, accountType).concat(helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMainBalance'));
        this.externalProviderAccountActionMenu = (bankName, accountType) => this.externalAccountTileLocator(bankName, accountType).concat(helper.byResourceId(ViewClasses.IMAGE_BUTTON, 'accountTileActionMenu'));
        this.externalProviderAccountFlag = (bankName, accountType) => this.externalAccountTileLocator(bankName, accountType).concat(helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggAccountTileRecentLabel'));
        this.arrangementsList = helper.byResourceId(ViewClasses.RECYCLER_VIEW, 'arrangementsCardList').concat('/child::*');
        this.externalAccountNumberList = `//android.widget.RelativeLayout
            //android.view.View[contains(@content-desc,"External Account")]/..
            //android.widget.TextView[contains(@resource-id,"accountTileAccountNumber")]`;
        this.nonEligibleExternalFromAccountList = `//android.widget.RelativeLayout
            //android.view.View[contains(@content-desc,"External Account")]
            /..//android.widget.TextView[contains(@content-desc,"Savings")]
            /following-sibling::android.widget.TextView[contains(@resource-id,"accountTileAccountNumber")]`;
        this.covid19Tile = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'covid19HomeTileView');
        this.findOutMoreOnCovid19Tile = helper.byResourceId(ViewClasses.TEXT_VIEW, 'covid19HomeTileButton');
        this.rewardHubEntryTile = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'homePromoTile');
        this.findOutMoreOnRewardHubEntryTile = helper.byResourceId(ViewClasses.TEXT_VIEW, 'homePromoTileButton');
    }
}

module.exports = AccountsOverviewScreen;
