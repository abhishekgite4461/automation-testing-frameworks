const BaseRedirectScreen = require('../../../paymentAggregation/redirect.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class RedirectScreen extends BaseRedirectScreen {
    constructor() {
        super();
        this.redirectScreenPaymentAgg = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'payAggRedirectErrorImage');
        this.redirectScreenTitlePaymentAgg = helper.byResourceId(ViewClasses.TEXT_VIEW, 'payAggRedirectIncompleteInfoText');
        this.redirectScreenAuthenticationInProgressTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'payAggRedirectIncompleteInfoText');
        this.redirectLoadingProgressPayAgg = helper.byResourceId(ViewClasses.PROGRESS_BAR, 'payAggRedirectLoadingProgress');
        this.redirectTextPayAgg = helper.byResourceId(ViewClasses.TEXT_VIEW, 'payAggRedirectText');
    }
}

module.exports = RedirectScreen;
