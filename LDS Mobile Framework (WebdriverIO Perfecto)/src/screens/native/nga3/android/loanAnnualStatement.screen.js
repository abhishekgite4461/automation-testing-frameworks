const BaseLoanAnnualStatement = require('../../loanAnnualStatement.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class LoanAnnualStatement extends BaseLoanAnnualStatement {
    constructor() {
        super();
        this.title = helper.byContentDesc(ViewClasses.VIEW, 'Annual Statements');
    }
}

module.exports = LoanAnnualStatement;
