const BaseFscsScreen = require('../fscs.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class FscsScreen extends BaseFscsScreen {
    constructor() {
        super();
        this.compensationSchemeLabel = helper.byPartialText(ViewClasses.EDIT_TEXT, 'www.halifax.co.uk');
    }
}

module.exports = FscsScreen;
