const BaseConfirmationOfPayeeScreen = require('../../transferAndPayments/confirmationOfPayee.screen');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class ConfirmationOfPayeeScreen extends BaseConfirmationOfPayeeScreen {
    constructor() {
        super();
        this.paymentHubCopBAMMMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'THAT\'S NOT THE FULL NAME ON THAT BUSINESS ACCOUNT');
        this.paymentHubCopBAMMNamePlayBack = (recipientName) => helper.byPartialText(ViewClasses.TEXT_VIEW,
            `This is a business account. It belongs to ${recipientName}`);
        this.paymentHubCopNotMatchEdit = helper.byPartialResourceId(ViewClasses.BUTTON, 'paymentHubCopNotMatchEditDetails');
        this.serviceError = helper.byPartialText(ViewClasses.TEXT_VIEW, 'WE CAN\'T CONFIRM THESE DETAILS');
        this.paymentHubCopNotMatchContinue = helper.byPartialResourceId(ViewClasses.BUTTON, 'paymentHubCopNotMatchContinue');
        this.partialNameMatch = helper.byPartialText(ViewClasses.TEXT_VIEW, 'THAT\'S NOT THE FULL NAME ON THAT ACCOUNT');
        this.businessNameMatch = helper.byPartialText(ViewClasses.TEXT_VIEW, 'THIS IS A BUSINESS ACCOUNT');
        this.personalAccountMatch = helper.byPartialText(ViewClasses.TEXT_VIEW, 'THIS IS A PERSONAL ACCOUNT');
    }
}

module.exports = ConfirmationOfPayeeScreen;
