const BaseChooseRecipientScreen = require('../../transferAndPayments/searchRecipient.screen');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class searchRecipientScreen extends BaseChooseRecipientScreen {
    constructor() {
        super();
        this.yourAccountsGroupHeader = helper.byText(ViewClasses.TEXT_VIEW, 'My accounts');
    }
}

module.exports = searchRecipientScreen;
