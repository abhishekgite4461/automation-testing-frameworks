const halifaxRewardHub = require('../../../halifaxRewardHub.screen.js');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class RewardHub extends halifaxRewardHub {
    constructor() {
        super();
        this.title = "//*[contains(@text, 'My Extras')]";
        this.cashbackExtraCallToAction = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'rewardHubListCardView')}[1]${helper.byResourceId(ViewClasses.TEXT_VIEW, 'findMoreLinkText')}`;
        this.rewardsExtraCallToAction = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'rewardHubListCardView')}[2]${helper.byResourceId(ViewClasses.TEXT_VIEW, 'findMoreLinkText')}`;
        this.mortgagePrizeDrawCallToAction = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'rewardHubListCardView')}[3]${helper.byResourceId(ViewClasses.TEXT_VIEW, 'findMoreLinkText')}`;
        this.saversPrizeDrawCallToAction = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'rewardHubListCardView')}[4]${helper.byResourceId(ViewClasses.TEXT_VIEW, 'findMoreLinkText')}`;
        this.optInView = helper.byResourceId(ViewClasses.VIEW, 'optInView');
        this.cashbackExtraOptInText = helper.byText(ViewClasses.TEXT_VIEW, 'Latest offers:');
        this.cashbackExtraNotOptInText = helper.byText(ViewClasses.TEXT_VIEW, 'Choose an offer and get cashback when you use your debit card.');
        this.rewardExtraNotOptInText = helper.byText(ViewClasses.TEXT_VIEW, 'Earn a reward each month. Pick an offer that\'s right for you.');
        this.cashbackExtraNotEligibleText = helper.byText(ViewClasses.TEXT_VIEW, 'With our current accounts you can earn cashback when you shop.');
        this.rewardExtraNotEligibleText = helper.byText(ViewClasses.TEXT_VIEW, 'You can earn a reward each month. Choose an offer that suits you.');
        this.optInTodayView = helper.byText(ViewClasses.TEXT_VIEW, 'Your Rewards Extra Tracker will update soon.');
        this.onTrackText = helper.byText(ViewClasses.TEXT_VIEW, 'Great! You\'re on track for your reward this month.');
        this.notOnTrackIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'propositionStatusIcon');
        this.notOnTrackText = helper.byText(ViewClasses.TEXT_VIEW, 'You’re not on track for your reward this month.');
        this.saversRegisteredText = helper.byText(ViewClasses.TEXT_VIEW, 'You’ve registered. You could be selected to win money.');
        this.saversNotRegisteredText = helper.byText(ViewClasses.TEXT_VIEW, 'A chance to win money as a Halifax saver.');
        this.mortgageRegisteredText = helper.byText(ViewClasses.TEXT_VIEW, 'You\'ve registered. You’ve a chance to win a prize.');
        this.mortgageNotRegisteredText = helper.byText(ViewClasses.TEXT_VIEW, 'Register for our monthly mortgage prize draw.');
        this.cashbackExtraWebPage = helper.byText(ViewClasses.VIEW, 'CASHBACK EXTRAS');
        this.rewardsExtraWebPage = helper.byText(ViewClasses.VIEW, 'REWARD EXTRAS TRACKER');
        this.mortgagePrizeDrawWebPage = helper.byText(ViewClasses.VIEW, 'HALIFAX PRIZE DRAW HUB.');
        this.saversPrizeDrawWebPage = helper.byText(ViewClasses.VIEW, 'HALIFAX PRIZE DRAW HUB.');
    }
}

module.exports = RewardHub;
