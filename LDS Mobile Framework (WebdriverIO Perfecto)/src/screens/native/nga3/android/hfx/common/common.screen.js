const BaseCommonScreen = require('../../common/common.screen');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');
const helper = require('../../../../android.helper');

class CommonScreen extends BaseCommonScreen {
    constructor() {
        super();
        this.accountNameMatch = helper.byPartialText(ViewClasses.TEXT_VIEW, 'confirmed that account name and details');
    }
}

module.exports = CommonScreen;
