const BaseProductHubApplySavingsScreen = require('../../productHub/productHubApplySavings.screen');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class ProductHubApplySavingsScreen extends BaseProductHubApplySavingsScreen {
    constructor() {
        super();
        this.accountOpeningSuccessMessage = (accountName) => `//android.view.View[@text = 'Your ${accountName} account 
            is now open' or contains(@text, 'opened a ${accountName}')]`;
        this.openAccount = '//android.widget.Button[@text = "Apply now" or @text = "Open account"]';
        this.acctSortCodeandNumber = helper.byPartialText(ViewClasses.VIEW, 'Sort code');
    }
}

module.exports = ProductHubApplySavingsScreen;
