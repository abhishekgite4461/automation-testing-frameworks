const BaseProductHubApplyOverdraftScreen = require('../../productHub/productHubApplyOverdraft.screen');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class ProductHubApplyOverdraftScreen extends BaseProductHubApplyOverdraftScreen {
    constructor() {
        super();
        this.overdraftPageTitle = helper.byText(ViewClasses.VIEW, 'CHOOSE AN AMOUNT');
        this.overdraftDeclinePageTitle = helper.byText(ViewClasses.VIEW, 'SORRY YOUR ARRANGED OVERDRAFT '
            + 'APPLICATION HAS BEEN DECLINED');
        this.aboutYouLabel = helper.byText(ViewClasses.VIEW, 'ABOUT YOU');
    }
}

module.exports = ProductHubApplyOverdraftScreen;
