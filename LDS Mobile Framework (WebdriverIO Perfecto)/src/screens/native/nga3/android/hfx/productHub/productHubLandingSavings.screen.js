const BaseProductHubLandingSavingsScreen = require('../../productHub/productHubLandingSavings.screen');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class ProductHubLandingSavingsScreen extends BaseProductHubLandingSavingsScreen {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.VIEW, 'Savings and investments');
    }
}

module.exports = ProductHubLandingSavingsScreen;
