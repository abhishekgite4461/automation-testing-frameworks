const BaseChequeDepositScreen = require('../../ics/chequeDeposit.screen');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class ChequeDepositScreen extends BaseChequeDepositScreen {
    constructor() {
        super();
        this.technicalErrorText = helper.byText(ViewClasses.TEXT_VIEW, "Sorry, there's a technical problem");
        this.accountErrorText = helper.byText(ViewClasses.TEXT_VIEW, "Sorry, this account info isn't available right now. Please try again later.");
    }
}

module.exports = ChequeDepositScreen;
