const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');
const BaseSearchLandingScreen = require('../../inAppSearch/searchLanding.screen.js');

// TODO: Created JIRA ticket-AVC-6287 to give valid element ID as per coding standards which will be fixed by devs
//  when feature is ready in environments. currently no valid element IDs are available

class searchLandingScreen extends BaseSearchLandingScreen {
    constructor() {
        super();
        this.fingerprintFaceScanLogonSettingsLink = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Fingerprint/face scan sign-in settings');
        this.fingerprintFaceScanLogonSettingsPage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Security settings');
        this.chooseAutoSignOutSettingLink = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Choose auto sign-out setting');
    }
}
module.exports = searchLandingScreen;
