const BaseLegalInfoScreen = require('../../settings/legalInfo.screen');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class LegalInfoScreen extends BaseLegalInfoScreen {
    constructor() {
        super();
        this.cookieUseAndPermissions = helper.byText(ViewClasses.TEXT_VIEW, 'My data');
    }
}

module.exports = LegalInfoScreen;
