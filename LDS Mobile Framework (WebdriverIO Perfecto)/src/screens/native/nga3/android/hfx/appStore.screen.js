const BaseAppStoreScreen = require('../appStore.screen');

class AppStoreScreen extends BaseAppStoreScreen {
    constructor() {
        super();
        this.appText = "//*[@text='Halifax: the banking app that gives you extra']";
    }
}

module.exports = AppStoreScreen;
