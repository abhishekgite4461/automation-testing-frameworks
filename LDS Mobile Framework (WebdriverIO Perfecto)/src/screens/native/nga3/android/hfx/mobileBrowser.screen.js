const BaseMobileBrowserScreen = require('../mobileBrowser.screen');

class mobileBrowserScreen extends BaseMobileBrowserScreen {
    constructor() {
        super();
        this.brandUrl = 'halifax-online.co.uk';
    }
}

module.exports = mobileBrowserScreen;
