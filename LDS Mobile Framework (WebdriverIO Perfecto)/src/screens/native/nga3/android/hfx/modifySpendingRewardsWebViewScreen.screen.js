const BaseModifySpendingRewardsWebViewScreen = require('../../../modifySpendingRewardsWebViewScreen.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ModifySpendingRewardsWebViewScreen extends BaseModifySpendingRewardsWebViewScreen {
    constructor() {
        super();
        this.title = helper.byPartialText(ViewClasses.VIEW, 'Cashback Extras');
    }
}

module.exports = ModifySpendingRewardsWebViewScreen;
