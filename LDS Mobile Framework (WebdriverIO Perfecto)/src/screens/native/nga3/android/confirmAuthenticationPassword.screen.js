const BaseConfirmAuthenticationPasswordScreen = require('../../confirmAuthenticationPassword.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class ConfirmAuthenticationPasswordScreen extends BaseConfirmAuthenticationPasswordScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.passwordField = helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText');
        this.forgottenPasswordLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'passwordConfirmationDialogForgotPasswordLabel');
        this.errorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, '8000067');
        this.confirmButton = helper.byText(ViewClasses.TEXT_VIEW, 'OK');
        this.scaErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'That\'s not right. Please try again.');
        this.scaFinalAttemptWarning = helper.byPartialText(ViewClasses.TEXT_VIEW, 'This is your final attempt');
    }
}

module.exports = ConfirmAuthenticationPasswordScreen;
