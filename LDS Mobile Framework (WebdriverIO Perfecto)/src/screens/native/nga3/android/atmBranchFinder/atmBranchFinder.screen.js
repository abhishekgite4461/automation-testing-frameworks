const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');
const BaseAtmBranchFinderScreen = require('../../../atmBranchFinder/atmBranchFinder.screen.js');

class AtmBranchFinderScreen extends BaseAtmBranchFinderScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'branchFinderTitle');
        this.searchBranchesTile = helper.byResourceId(ViewClasses.BUTTON, 'searchBranches');
        this.findNearbyAtmTile = helper.byResourceId(ViewClasses.BUTTON, 'findNearByAtm');
        this.noteMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'branchFinderNote');
        this.googleMapContainer = '//androidx.drawerlayout.widget.DrawerLayout';
        this.mapSearchBox = '//android.widget.EditText/android.widget.TextView';
        this.mapBackButton = '//android.widget.FrameLayout[@content-desc="Back"]/android.widget.ImageView';
    }
}

module.exports = AtmBranchFinderScreen;
