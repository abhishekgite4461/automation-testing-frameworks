const BaseWhitelistedBillerScreen = require('../../../whitelistedBiller/whitelistedBiller.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

const errorBannerMessage = "Sorry, we don't recognise the reference you have entered. Please check and try again.";
const apiFailBannerMessage = "Sorry, we can't find any companies";

class WhitelistedBillerScreen extends BaseWhitelistedBillerScreen {
    constructor() {
        super();
        this.lloydsBiller = (account) => helper.byText(ViewClasses.TEXT_VIEW, `${account}`);
        // TODO: PAT-1424 Add ID for below elements
        this.refBiller = helper.byPartialText(ViewClasses.EDIT_TEXT, 'digit reference');
        this.refErrorBanner = helper.byPartialText(ViewClasses.TEXT_VIEW, errorBannerMessage);
        this.apiFailErrorBanner = helper.byPartialText(ViewClasses.TEXT_VIEW, apiFailBannerMessage);
    }
}

module.exports = WhitelistedBillerScreen;
