const BaseSwitchScreen = require('../../switch.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class SwitchScreen extends BaseSwitchScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.EXPANDABLE_LIST_VIEW, 'switchSelectorListView');
        this.switchToggle = (name) => `//*[android.${ViewClasses.TEXT_VIEW.namespace}.`
                                    + `${ViewClasses.TEXT_VIEW.class}/@text="${name}"]/android.widget.Switch`;
        this.nextButton = helper.byResourceId(ViewClasses.BUTTON, 'switchSelectorNextButton');
    }
}

module.exports = SwitchScreen;
