const BaseRenameAccountScreen = require('../../renameAccount.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class RenameAccountScreen extends BaseRenameAccountScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'renameAccountRenameButton');
        this.renameButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'renameAccountRenameButton');
        this.textField = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'renameAccountInputField');
        this.confirmationBox = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'renameAccountSuccessDialog');
        this.backToYourAccountsButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.ghostAccountName = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'renameAccountInputField')}
        //following::*[contains(@resource-id,'inputFieldEditText')]`;
    }
}

module.exports = RenameAccountScreen;
