const BaseReEnterMemorableInformationScreen = require('../../reEnterMemorableInformation.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class ReEnterMemorableInformationScreen extends BaseReEnterMemorableInformationScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'enterMiSubtitle');
        this.memorableCharacter = (ordinal) => helper.byText(ViewClasses.TEXT_VIEW, ordinal);
        this.wrongMIError = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Please re-enter your details and try again.');
        this.closeButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'notificationCloseImageView');
        this.wrongPasswordError = helper.byPartialText(ViewClasses.TEXT_VIEW, '8000067');
        this.firstMemorableCharacter = helper.byResourceId(ViewClasses.EDIT_TEXT, 'miInputOne');
        this.fscsTitle = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'enterMiFscsTile');
    }
}

module.exports = ReEnterMemorableInformationScreen;
