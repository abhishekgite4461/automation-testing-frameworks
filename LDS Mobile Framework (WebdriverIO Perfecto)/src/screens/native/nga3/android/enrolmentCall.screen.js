const BaseEnrolmentCallScreen = require('../../enrolmentCall.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class EnrolmentCallScreen extends BaseEnrolmentCallScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'eiaSelectPhoneTitle');
        this.callMeNowButton = helper.byResourceId(ViewClasses.BUTTON, 'callMeNowButton');
        this.cancelButton = helper.byResourceId(ViewClasses.BUTTON, 'cancelEiaButton');
        this.exitYes = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.exitCancel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.eiaCallingTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'eiaCallBody');
        this.eiaPin1 = helper.byResourceId(ViewClasses.TEXT_VIEW, 'eiaPinFirstDigit');
        this.eiaPin2 = helper.byResourceId(ViewClasses.TEXT_VIEW, 'eiaPinSecondDigit');
        this.eiaPin3 = helper.byResourceId(ViewClasses.TEXT_VIEW, 'eiaPinThirdDigit');
        this.eiaPin4 = helper.byResourceId(ViewClasses.TEXT_VIEW, 'eiaPinFourthDigit');
        this.dialNumber = (text) => helper.byText(ViewClasses.TEXT_VIEW, text);
        this.keypad = helper.byResourceId(ViewClasses.BUTTON, 'dialpadButton');
        this.numberRadioButton = (numberType) => helper.byPartialText(ViewClasses.TEXT_VIEW, numberType);
        this.eiaPhoneNumbers = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'eiaPhoneNumbers');
        this.yourPhone = "//*[@text='YourPhone:']/following-sibling::android.widget.TextView";
        this.homePhone = "//*[@text='Home:']/following-sibling::android.widget.TextView";
        this.mobilePhone = "//*[@text='Mobile:']/following-sibling::android.widget.TextView";
        this.workPhone = "//*[@text='Work:']/following-sibling::android.widget.TextView";
        this.selectRadioButton = (number) => `(//*[contains(@text,'${number}')]/../
        following::android.widget.Button[1][@enabled='true'])[1]`;
        this.mobilePhoneSection = "//android.widget.TextView[contains(@text,'Mobile')]/../../ancestor::android.widget.LinearLayout[1]";
        this.selectPhoneNumber = (number) => `(//*[contains(@text,'${number}')]/../
        following::android.widget.Button[1][@enabled='true'])[1]`;
    }
}

module.exports = EnrolmentCallScreen;
