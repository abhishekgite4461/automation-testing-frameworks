const BaseInternationalPaymentWebViewPageScreen = require('../../internationalPaymentWebViewPage.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class InternationalPaymentWebViewPageScreen extends BaseInternationalPaymentWebViewPageScreen {
    constructor() {
        super();
        this.IPtitle = helper.byContentDesc(ViewClasses.VIEW, 'International payment');
    }
}

module.exports = InternationalPaymentWebViewPageScreen;
