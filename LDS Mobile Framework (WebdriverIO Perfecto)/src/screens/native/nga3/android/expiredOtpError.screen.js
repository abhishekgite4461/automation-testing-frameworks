const BaseExpiredOtpErrorScreen = require('../../expiredOtpError.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class ExpiredOtpErrorScreen extends BaseExpiredOtpErrorScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenTitle');
        this.expiredOtpErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Sorry, this One Time Password is no longer valid.');
        this.goToLogonButton = helper.byResourceId(ViewClasses.BUTTON, 'errorScreenButtonPrimary');
    }
}

module.exports = ExpiredOtpErrorScreen;
