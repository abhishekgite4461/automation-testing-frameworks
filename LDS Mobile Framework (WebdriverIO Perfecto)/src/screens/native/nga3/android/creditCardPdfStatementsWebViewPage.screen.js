const BaseCreditCardPdfStatementsWebViewPageScreen = require('../../creditCardPdfStatementsWebViewPage.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class CreditCardPdfStatementsWebViewPageScreen extends BaseCreditCardPdfStatementsWebViewPageScreen {
    constructor() {
        super();
        this.title = helper.byContentDesc(ViewClasses.VIEW, 'PDF statements');
    }
}

module.exports = CreditCardPdfStatementsWebViewPageScreen;
