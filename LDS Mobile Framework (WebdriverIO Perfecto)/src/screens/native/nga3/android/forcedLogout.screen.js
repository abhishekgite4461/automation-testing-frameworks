const BaseForcedLogoutScreen = require('../../forcedLogout.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class ForcedLogoutScreen extends BaseForcedLogoutScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenTitle');
        this.errorIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'errorScreenIcon');
        this.errorMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenMessage');
        this.logonButton = helper.byResourceId(ViewClasses.BUTTON, 'errorScreenButtonPrimary');
    }
}

module.exports = ForcedLogoutScreen;
