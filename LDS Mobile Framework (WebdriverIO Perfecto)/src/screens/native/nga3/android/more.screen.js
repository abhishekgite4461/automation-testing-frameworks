const BaseMoreScreen = require('../../more.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class MoreScreen extends BaseMoreScreen {
    constructor() {
        super();
        this.title = `${helper.byResourceId(ViewClasses.VIEW_GROUP, 'navigationToolbar')}${helper.byPartialText(ViewClasses.TEXT_VIEW, 'More')}`;
        this.moreSwitchBusiness = helper.byResourceId(ViewClasses.VIEW_GROUP, 'moreSwitchBusiness');
        this.businessName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'moreListItemDescription');
        this.yourProfile = `${helper.byResourceId(ViewClasses.VIEW_GROUP, 'moreCustomerProfile')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'moreListItemTitle')}`;
        this.inbox = helper.byResourceId(ViewClasses.VIEW_GROUP, 'moreInbox');
        this.settings = helper.byResourceId(ViewClasses.VIEW_GROUP, 'moreSettings');
        this.chequeScanning = helper.byResourceId(ViewClasses.VIEW_GROUP, 'moreCheques');
        this.cardManagement = `${helper.byResourceId(ViewClasses.VIEW_GROUP, 'moreCardManagement')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'moreListItemTitle')}`;
        this.lostStolenCard = `${helper.byResourceId(ViewClasses.VIEW_GROUP, 'moreLostOrStolenCard')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'moreListItemTitle')}`;
        this.replacementCardPin = `${helper.byResourceId(ViewClasses.VIEW_GROUP, 'moreReplacementCardPin')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'moreListItemTitle')}`;
        this.depositCheque = `${helper.byResourceId(ViewClasses.VIEW_GROUP, 'moreDepositCheque')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'moreListItemTitle')}`;
        this.viewDepositHistory = `${helper.byResourceId(ViewClasses.VIEW_GROUP, 'moreDepositHistory')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'moreListItemTitle')}`;
        this.logout = "//*[contains(@resource-id, 'id/moreLogOffButton') or contains(@resource-id, 'id/toolbarLogoffButton')]";
        this.darkURL = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsDarkUrlTile');
    }
}

module.exports = MoreScreen;
