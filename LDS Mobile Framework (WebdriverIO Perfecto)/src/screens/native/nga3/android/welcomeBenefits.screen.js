const BaseWelcomeBenefitsScreen = require('../../welcomeBenefits.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class WelcomeBenefitsScreen extends BaseWelcomeBenefitsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'welcomeSectionOneIcon');
        this.nextButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationHeaderPageNextButton');
        this.makeYourBankingEasier = helper.byResourceId(ViewClasses.TEXT_VIEW, 'welcomeSectionOneBody');
        this.concernedAboutFraud = helper.byResourceId(ViewClasses.TEXT_VIEW, 'welcomeSectionTwoBody');
        this.dataPrivacy = helper.byResourceId(ViewClasses.TEXT_VIEW, 'welcomeSectionThreeBody');
        this.becomeACustomer = helper.byResourceId(ViewClasses.TEXT_VIEW, 'welcomeSectionFourBody');
    }
}

module.exports = WelcomeBenefitsScreen;
