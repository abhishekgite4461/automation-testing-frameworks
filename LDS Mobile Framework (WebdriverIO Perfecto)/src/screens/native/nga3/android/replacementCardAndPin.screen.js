const BaseReplacementCardAndPin = require('../../replacementCardAndPin.screen.js');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

// TODO DPLDA-175 Need to migrate into webview approach
class ReplacementCardAndPin extends BaseReplacementCardAndPin {
    constructor() {
        super();
        this.title = '//android.view.View[contains(@text,"Card replacement") or contains(@text,"Replacement cards and PINs") or contains(@content-desc,"Card replacement")]';
        this.replaceCard = helper.byResourceIdWebView(ViewClasses.CHECK_BOX, 'frmOrderCollateral:debitCardAccountList:0:debitCards:0:replaceCard1');
        this.replaceCardCredit = helper.byResourceIdWebView(ViewClasses.CHECK_BOX, 'frmOrderCollateral:creditCardAccountList:0:creditCards:0:replaceCard1');
        this.pin = helper.byResourceIdWebView(ViewClasses.CHECK_BOX, 'frmOrderCollateral:debitCardAccountList:0:debitCards:0:newPin1');
        this.pinCredit = helper.byResourceIdWebView(ViewClasses.CHECK_BOX, 'frmOrderCollateral:creditCardAccountList:0:creditCards:0:newPin1');
        this.confirmTitle = helper.byText(ViewClasses.VIEW, 'Confirm your selections');
        this.continue = helper.byPartialResourceIdWebView(ViewClasses.BUTTON, 'btnDoConfirmcancelCard');
        this.password = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'lostAndStolenConfirm:txtAuthPwd');
    }
}

module.exports = ReplacementCardAndPin;
