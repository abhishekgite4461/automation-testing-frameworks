const BaseAppBanScreen = require('../../appBan.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class AppBanScreen extends BaseAppBanScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenTitle');
        this.updateAppButton = helper.byResourceId(ViewClasses.BUTTON, 'errorScreenButtonPrimary');
        this.goToOurMobileSiteButton = helper.byResourceId(ViewClasses.BUTTON, 'errorScreenButtonSecondary');
    }
}

module.exports = AppBanScreen;
