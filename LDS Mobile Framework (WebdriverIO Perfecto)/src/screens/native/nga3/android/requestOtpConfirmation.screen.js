const BaseRequestOtpConfirmationScreen = require('../../requestOtpConfirmation.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class RequestOtpConfirmationScreen extends BaseRequestOtpConfirmationScreen {
    constructor() {
        super();
        this.title = '//android.widget.TextView[contains(@text,"Password request confirmed") or contains(@text,"PASSWORD REQUEST CONFIRMED")]';
        this.goToIbButton = helper.byResourceId(ViewClasses.BUTTON, 'okButton');
    }
}

module.exports = RequestOtpConfirmationScreen;
