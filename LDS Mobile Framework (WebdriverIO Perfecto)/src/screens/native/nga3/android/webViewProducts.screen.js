const WebViewProductsScreenBase = require('../../webViewProducts.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class WebViewProductsScreen extends WebViewProductsScreenBase {
    constructor() {
        super();
        this.manageCreditLimitTitleText = helper.byText(ViewClasses.VIEW, 'Increase your credit card limit');
        this.repaymentHolidayTitleText = helper.byText(ViewClasses.VIEW, 'Ask for a repayment holiday');
        this.additionalPaymentTitleText = helper.byText(ViewClasses.VIEW, 'Make an Additional Payment to Your Loan');
        this.balanceTransferTitleText = helper.byText(ViewClasses.VIEW, 'Balance Transfer / Money Transfer');
        this.borrowMoreTitleText = helper.byText(ViewClasses.VIEW, 'Borrow more');
        this.loanClosureTitleText = helper.byPartialText(ViewClasses.VIEW, 'Loan closure');
        this.renewYourSavingsAccountTitleText = helper.byText(ViewClasses.VIEW, 'Renew your savings account');
        this.selectSavingsAccountToRenewText = helper.byText(ViewClasses.VIEW, 'Select a savings account to renew');
        this.pensionTransferTitleText = helper.byText(ViewClasses.VIEW, 'Pension Transfer');
        this.overdraftPopUpText = helper.byText(ViewClasses.VIEW, 'Overdrafts');
        this.changeAccountTypeText = helper.byText(ViewClasses.VIEW, 'Change Account Type');

        this.manageCreditLimitTitle = helper.byContentDesc(ViewClasses.VIEW, 'Increase your credit card limit');
        this.repaymentHolidayTitle = helper.byContentDesc(ViewClasses.VIEW, 'Ask for a repayment holiday');
        this.additionalPaymentTitle = helper.byContentDesc(ViewClasses.VIEW, 'Make an Additional Payment to Your Loan');
        this.balanceTransferTitle = helper.byContentDesc(ViewClasses.VIEW, 'Balance Transfer / Money Transfer');
        this.borrowMoreTitle = helper.byContentDesc(ViewClasses.VIEW, 'Borrow more');
        this.loanClosureTitle = helper.byPartialContentDesc(ViewClasses.VIEW, 'Loan closure');
        this.renewYourSavingsAccountTitle = helper.byContentDesc(ViewClasses.VIEW, 'Renew your savings account');
        this.selectSavingsAccountToRenew = helper.byContentDesc(ViewClasses.VIEW, 'Select a savings account to renew');
        this.pensionTransferTitle = helper.byContentDesc(ViewClasses.VIEW, 'Pension Transfer');
        this.overdraftPopUpTitle = helper.byContentDesc(ViewClasses.VIEW, 'Overdrafts');
        this.changeAccountTypeTitle = helper.byContentDesc(ViewClasses.VIEW, 'Change Account Type');
    }
}

module.exports = WebViewProductsScreen;
