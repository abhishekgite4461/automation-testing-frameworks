const BaseAccountStatementScreen = require('../../../statements/accountStatement.screen.js');
const helper = require('../../../android.helper.js');
const ViewClasses = require('../../../../../enums/androidViewClass.enum.js');

const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'];
const date = new Date();

class AccountStatementScreen extends BaseAccountStatementScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.VIEW_GROUP, 'statementSectionsTabBar');
        this.noteText = helper.byText(ViewClasses.TEXT_VIEW, 'NOTE: Uncleared cheques show in your account balance but not in \'balance after pending\'. You can only use the money when it clears.');
        this.statementLayout = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'statementLayout');
        this.recentsTab = helper.byText(ViewClasses.TEXT_VIEW, 'Recent');
        this.transactionsSinceMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionSinceHeader');
        this.firstTranscationHeader = '//*[@name="Transactions" and @visible="true"]';
        this.firstTranscationDate = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionType');
        this.firstTranscationDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDescription');
        this.firstTranscationAmount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionAmount');
        this.firstCreditIndicator = helper.byResourceId(ViewClasses.VIEW, 'transactionCreditIndicator');
        this.onlineCreditCartStatementLimitText = helper.byText(ViewClasses.TEXT_VIEW, 'Online'
            + ' statements only go back 6 months. For older transactions, please contact your branch.');
        this.firstTransactionRunningBalance = helper.byResourceId(ViewClasses.TEXT_VIEW, 'runningBalance');
        this.currentMonthIndicator = helper.byText(
            ViewClasses.TEXT_VIEW,
            monthNames[date.getMonth()]
        );
        this.accountTile = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'accountTile');
        this.accountDetailContainer = '//*[contains(@resource-id,"transactionList")]';
        this.totalAmountField = helper.byResourceId(ViewClasses.TEXT_VIEW, 'totalAmountOut');
        this.transactionList = '//*[contains(@resource-id,"transactionList")]';
        this.endOfTranscationMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'endOfTransactionsMessage');
        this.statementBalanceDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'statementbalancedescription');
        this.statementBalance = helper.byResourceIdAndIndex(ViewClasses.TEXT_VIEW, 'statementbalance', '2');
        this.minimumPaymentDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'minimumpaymentdescription');
        this.minimumPayment = helper.byResourceIdAndIndex(ViewClasses.TEXT_VIEW, 'minimumpayment', '2');
        this.dueDateDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'duedatedescription');
        this.dueDate = helper.byResourceIdAndIndex(ViewClasses.TEXT_VIEW, 'duedate', '2');
        this.statementDateDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'statementdatedescription');
        this.statementDate = helper.byResourceIdAndIndex(ViewClasses.TEXT_VIEW, 'statementdate', '2');
        this.noTransactionsMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'noTransactionsMessage');
        this.transactionsHeader = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionHeader');
        this.actionMenu = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'accountTileActionMenu');
        this.directDebitTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'EON Energy');
        this.standingOrderTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Virgin Mobile');
        this.savingNoTransactionMessage = helper.byText(ViewClasses.TEXT_VIEW, 'There are no transactions to display for this period.');
        this.savingEndOfTransactionMessage = helper.byText(ViewClasses.TEXT_VIEW, 'There are no more transactions to display for this period.');
        this.currentInOutsTransactionMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'totalAmountOut');
        this.pendingTransactionDate = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDate');
        this.pendingTransactionDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDescription');
        this.pendingTransactionAmount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionAmount');
        this.pendingTransactionTab = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionsAccordionTitle');
        this.pendingTransactionsAccordionOpenCloseIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'transactionsAccordionOpenCloseIcon');
        this.pendingTransactionType = (type) => helper.byText(ViewClasses.TEXT_VIEW, `${type} card transactions`);
        this.pendingTransactionPanelOpen = helper.byContentDesc(ViewClasses.RELATIVE_LAYOUT, 'Hide all pending transactions. You have pending transactions. Link.');
        this.pendingTransactionPanelClosed = helper.byContentDesc(ViewClasses.RELATIVE_LAYOUT, 'Show all pending transactions. You have pending transactions. Link.');
        this.statementsDescription = () => '//*[contains(@resource-id,"transactionList")]';
        this.pendingTransactionIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'transactionsAccordionOpenCloseIcon');
        this.aboutPendingTransaction = helper.byResourceId(ViewClasses.TEXT_VIEW, 'pendingTransactionAboutTitle');
        this.pendingTransactionInfo = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'pendingTransactionAboutIcon');
        this.previousMonthStatementHolder = helper.byPartialContentDesc(ViewClasses.TEXT_VIEW, 'Use three fingers to swipe left or right for other months.');
        this.cashWithdrawalInATMTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'LOYD FINSBURY SQUA');
        this.cashDepositedInIDMTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Cash Deposit (CASH)');
        this.cashDepositInBranchTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'FINSBURY SQUARE');
        this.chequeDepositInBranchTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Deposit (CHEQUE)');
        this.fasterPaymentsIncomingTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Correction by bank');
        this.fasterPaymentsOutgoingTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'IB transfer');
        this.FPOTransactionType = helper.byPartialText(ViewClasses.TEXT_VIEW, 'AUTOHFX02');
        this.transfersTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'M N OPQURSTUV');
        this.transfersOutTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Transfer Out');
        this.billPaymentTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Girish Kulkarni');
        this.bankGiroCreditTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Tiago');
        this.mobilePaymentsInboundTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Mobile Payment In');
        this.mobilePaymentsOutgoingTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Mobile Payment Out');
        this.MPOTransactionType = '//android.widget.TextView[contains(@text, "H TESTRAPIDNEW-HXI") or contains(@text,"D TEST")]';
        this.chequeWithdrawalInBranchTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Cheque Withdrawal');
        this.onlineDebitCardTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Amazon UK Marketplace');
        this.contactLessPurchaseTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'TFL.GOV.UK-CP');
        this.chipAndPinPurchaseWithCashBackTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'WAITROSE 132');
        this.chipAndPinPurchaseTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'WAITROSE 732');
        this.monthsTab = helper.byResourceId(ViewClasses.HORIZONTAL_SCROLL_VIEW, 'statement_months_tab_bar');
        this.contactLessPurchaseCreditCardTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'W M MORRISON STORE     ');
        this.chipAndPinPurchaseCreditCardTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'JOHN LEWIS             ');
        this.manualOrPaperCreditCardTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'WWW.LEONPAULFENCINGCEN ');
        this.onlineCreditCardTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'NETFLIX.COM            ');
        this.directDebitInCardTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'DIRECT DEBIT PAYMENT - ');
        this.accountDetailContainerPreviousMonth = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'item_statement_transaction_details');
        this.pendingTransactionAboutIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'pendingTransactionAboutIcon');
        this.aboutPendingTransactionsTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'pendingTransactionAboutTitle');
        this.chargesCreditCardTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'CASH FEE');
        this.interestCreditCardTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'INTEREST');
        this.noPendingPaymentsMessage = helper.byText(
            ViewClasses.TEXT_VIEW,
            'You have no pending payments. You can view future one-off UK payments leaving your account in the next 30 days here.  Go to \'Pending transactions\' in your most recent statement to see any paid-in cheques and debit card payments that haven\'t cleared.'
        );
        this.cashWithdrawalATMCreditCardTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'E01743200000000        ');
        this.plusAccordion = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'loanDetailsAccordionOpenCloseIcon');
        this.minusAccordion = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'loanDetailsAccordionOpenCloseIcon');
        this.nextRepaymentDue = helper.byText(ViewClasses.TEXT_VIEW, 'Next repayment due:');
        this.remainingTerm = helper.byText(ViewClasses.TEXT_VIEW, 'Remaining term:');
        this.remainingAllowanceISA = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileTopValue');
        this.openingDate = helper.byText(ViewClasses.TEXT_VIEW, 'Opening date of loan:');
        this.monthlyRepaymentAmount = helper.byText(ViewClasses.TEXT_VIEW, 'Monthly repayment amount:');
        this.originalLoanTerm = helper.byText(ViewClasses.TEXT_VIEW, 'Original loan term:');
        this.noteOnAnnualStatement = helper.byText(ViewClasses.TEXT_VIEW, 'This is NOT your '
            + 'annual statement. To view your annual statement, please log in to our desktop site.');
        this.cbsLoanMessage = helper.byText(ViewClasses.TEXT_VIEW, 'Sorry these transactions are not available to view '
            + 'here. Please visit our desktop site to view your account activity.');
        this.cashPaymentAtBranchTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Cash Deposit');
        this.chequePaymentTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Cheque Deposit');
        this.monthFormat = (currentMonth) => helper.byText(ViewClasses.TEXT_VIEW, currentMonth);
        this.currentYearIndicator = helper.byResourceId(ViewClasses.TEXT_VIEW, 'loanDetailsAccordionTitle');
        this.previousYearStatementHolder = helper.byPartialContentDesc(ViewClasses.TEXT_VIEW, 'Use three fingers to swipe left or right for other years.');
        this.loanEndOf = helper.byText(ViewClasses.TEXT_VIEW, 'There are no more transactions to display for this period.');
        this.yearFormat = (yearFormat) => helper.byText(ViewClasses.TEXT_VIEW, yearFormat);
        this.loanEndOfTransactionMessage = helper.byText(ViewClasses.TEXT_VIEW, 'There are no more transactions to display for this period.');
        this.currentMonthStatementsPage = helper.byText(
            ViewClasses.TEXT_VIEW,
            monthNames[date.getMonth()]
        );
        this.recentStatementsPage = helper.byText(ViewClasses.TEXT_VIEW, 'Recent');
        this.currentYearStatementsPage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'loanDetailsAccordionTitle');
        this.deductedTransactionAmount = (amountDesc) => helper.byPartialContentDesc(
            ViewClasses.RELATIVE_LAYOUT,
            amountDesc
        );
        this.cashWithdrawalInBranchTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Cash out at branch');
        this.currentNoTransactionMessage = helper.byText(ViewClasses.TEXT_VIEW, 'There are no transactions to display for this period.');
        this.loanNoTransactionMessage = helper.byText(ViewClasses.TEXT_VIEW, 'There are no transactions to display for this period.');
        this.dueSoonTransactionList = '//*[contains(@resource-id, "/dueSoonPaymentsTransactionList")]';
        this.pendingDirectDebitTransactionType = '//*[@text="You have pending transactions"]/following::*[contains(@resource-id,":id/transactionType")][2]';
        this.pendingCreditCardTransactionType = '//*[@text="You have pending transactions"]/following::*[contains(@resource-id,":id/transactionType")][3]';
        this.pendingChequeTransactionAmount = `${this.noteText}/following::*[@resource-id, ":id/transactionAmount"][1]`;
        this.pendingChequeTransactionDescription = `${this.noteText}/following::*[@resource-id, 
        ":id/transactionDescription"][1]`;
        this.mortgageSummaryTab = helper.byText(ViewClasses.TEXT_VIEW, 'Summary');
        this.mortgageEndOfTransactionMessage = helper.byText(ViewClasses.TEXT_VIEW, 'There are no more transactions to display for this period.');
        this.mortgageSubAccountTab = helper.byText(ViewClasses.TEXT_VIEW, 'Sub accounts');
        this.subAccountTypeNo = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mortgageTitle');
        this.mortgageSubAccountBalance = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mortgageBalance');
        this.mortgageSubAccountType = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mortgageTypeLabel');
        this.interestRateAsAtDate = helper.byResourceId(ViewClasses.TEXT_VIEW, 'subAccountMortgageAsAt');
        this.currentMonthlyPaymentDetails = helper.byText(ViewClasses.TEXT_VIEW, 'Current monthly payment*:');
        this.subAccountCurrentMonthlyPayment = helper.byText(ViewClasses.TEXT_VIEW, 'Current monthly payment:');
        this.nextPaymentDueOnDate = helper.byText(ViewClasses.TEXT_VIEW, 'Next payment due on:');
        this.disclaimerText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'disclaimer');
        this.nextPaymentDue = helper.byText(ViewClasses.TEXT_VIEW, 'Next payment due:');
        this.remainingFullTerm = helper.byText(ViewClasses.TEXT_VIEW, 'Remaining full term:');
        this.mortgageType = helper.byText(ViewClasses.TEXT_VIEW, 'Mortgage Type:');
        this.mortgageHolders = helper.byText(ViewClasses.TEXT_VIEW, 'Mortgage holder(s):');
        this.propertyAddress = helper.byText(ViewClasses.TEXT_VIEW, 'Property address:');
        this.subAccountMortgageOpenedDate = helper.byText(ViewClasses.TEXT_VIEW, 'Sub account opened:');
        this.mortgageOpenedDate = helper.byText(ViewClasses.TEXT_VIEW, 'Mortgage opened:');
        this.arrearsBalance = helper.byText(ViewClasses.TEXT_VIEW, 'Arrears balance:');
        this.currentBalance = helper.byText(ViewClasses.TEXT_VIEW, 'Current balance*:');
        this.currentMonthlyPayment = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Current monthly');
        this.minimumMonthlyPayment = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Minimum monthly');
        this.interestRateAsOfDate = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mortgageInterestAsAtLabel');
        this.staticDisclaimerText = helper.byText(ViewClasses.TEXT_VIEW, 'YOUR HOME MAY BE REPOSSESSED IF YOU DO NOT KEEP UP '
            + 'REPAYMENTS ON YOUR MORTGAGE.');
        this.currentBalanceInfoLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mortgageHowIsBalanceCalculated');
        this.monthlyPaymentsInfoLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mortgageHowMonthlyPaymentsCalculated');
        this.mortgageAccordion = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'mortgageAccordion');
        this.mortgageNoTransactionMessage = helper.byText(ViewClasses.TEXT_VIEW, 'There are no transactions to display for this period.');
        this.currentBalanceInfoPage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.monthlyPaymentInfoPage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.closeInfoPage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.callUs = '//android.widget.TextView[contains(@text,"call us")]';
        this.paymentType = helper.byText(ViewClasses.TEXT_VIEW, 'Payment type:');
        this.originalAmount = helper.byText(ViewClasses.TEXT_VIEW, 'Original amount:');
        this.originalFullTerm = helper.byText(ViewClasses.TEXT_VIEW, 'Original full term:');
        this.remainingAmount = helper.byText(ViewClasses.TEXT_VIEW, 'Remaining amount**:');
        this.mortgageRateHistoryTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'rateHistoryTitle');
        this.mortgageRateHistoryDate = helper.byResourceId(ViewClasses.TEXT_VIEW, 'interestRateDate');
        this.mortgageRateHistoryType = helper.byResourceId(ViewClasses.TEXT_VIEW, 'interestRatePaymentType');
        this.mortgageRateHistoryRate = helper.byResourceId(ViewClasses.TEXT_VIEW, 'interestRate');
        this.subAccount = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'detailsPanel');
        this.subAccountDetailsTab = helper.byText(ViewClasses.TEXT_VIEW, 'Details');
        this.subAccountInterestRateAsAtDate = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mortgageInterestAsAtLabel');
        this.firstStatement = `//android.widget.RelativeLayout${helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDescription')}`;
        this.policyNumber = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'homeInsurancePolicyNumberHolder');
        this.enquiriesPhoneNumber = helper.byResourceId(ViewClasses.TABLE_LAYOUT, 'homeInsurancePolicyPhoneNumbers');
        this.policyUpdatedDate = helper.byResourceId(ViewClasses.TEXT_VIEW, 'homeInsurancePolicyAmendmentDate');
        this.policyHolders = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'homeInsurancePolicyInvolvedPartiesHolder');
        this.insuredPropertyAddress = helper.byResourceId(ViewClasses.TEXT_VIEW, 'homeInsurancePolicyInsuredAddress');
        this.whatsIncludedInPolicyLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'homeInsurancePolicyWhatsIncluded');
        this.optionalCoverYouCouldAddLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'homeInsurancePolicyOptionalCover');
        this.costAndPaymentDetailsLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'homeInsurancePolicyCostsAndPayments');
        this.policyScheduleNote = helper.byResourceId(ViewClasses.TEXT_VIEW, 'homeInsurancePolicyTermsAndConditions');
        this.termsDisclaimer = helper.byResourceId(ViewClasses.TEXT_VIEW, 'homeInsurancePolicyLegalDisclaimer');
        this.homeInsuranceTitle = helper.byContentDesc(ViewClasses.VIEW, 'Home Insurance Cost and payment details including any applicable discounts');
        this.premiumText = helper.byContentDesc(ViewClasses.VIEW, 'Your premium');
        this.totalAnnualCost = helper.byContentDesc(ViewClasses.VIEW, 'Total annual cost:');
        this.monthlyAmount = helper.byContentDesc(ViewClasses.VIEW, 'Monthly amount*:');
        this.monthlyPaymentDay = helper.byContentDesc(ViewClasses.VIEW, 'Monthly payment day*:');
        this.paymentMethod = helper.byContentDesc(ViewClasses.VIEW, 'Payment method:');
        this.paymentTypeText = helper.byContentDesc(ViewClasses.VIEW, 'Direct Debit');
        this.policyPaymentDisclaimer = helper.byContentDesc(ViewClasses.VIEW, '* These may vary due to changes in your billing arrangements');
        this.optionalCoverName = helper.byContentDesc(ViewClasses.VIEW, 'Home Insurance Optional cover');
        this.optionalCoverDescription = helper.byContentDesc(ViewClasses.VIEW, 'These items aren\'t part of your policy, but you can add them at any time by calling us.');
        this.coreCoverName = helper.byContentDesc(ViewClasses.VIEW, 'Home Insurance What\'s included in your policy');
        this.warningAlert = helper.byText(ViewClasses.TEXT_VIEW, 'You have an alert waiting');
        this.rePossessedMessageWarningAlert = helper.byText(ViewClasses.TEXT_VIEW, 'Your mortgage is in possession with the bank and cannot be viewed online. Please call 0845 6008117 for more information.');
        this.alertOverlay = helper.byText(ViewClasses.TEXT_VIEW, 'Alert');
        this.offSetMessageWarningAlert = helper.byText(ViewClasses.TEXT_VIEW, 'This is an \'offset\' mortgage and all details cannot be viewed online. Please call 0345 603 1637 for more information.');
        this.coreCoverLimit = helper.byPartialContentDesc(ViewClasses.VIEW, 'Limit:');
        this.coreCoverExcess = helper.byContentDesc(ViewClasses.VIEW, 'Subsidence Excess');
        this.subCoverName = helper.byContentDesc(ViewClasses.VIEW, 'Contents - Accidental Damage');
        this.subCoverLimit = helper.byPartialContentDesc(ViewClasses.VIEW, 'Limit:');
        this.subCoverExcess = helper.byPartialContentDesc(ViewClasses.VIEW, 'Standard excess:');
        this.subCoverPremium = helper.byPartialContentDesc(ViewClasses.VIEW, 'Annual cost:');
        this.fixedBondMessage = helper.byText(ViewClasses.TEXT_VIEW, 'Sorry these transactions are not available to view here. Please visit our desktop site to view your account activity.');
        this.dormantMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'This account has been inactive for over 3 years and needs to be activated again.');
        this.pendingPaymentTransactionType = helper.byPartialText(ViewClasses.TEXT_VIEW, 'SELPENDINGBENE');
        this.creditEndOfTransactionMessage = helper.byText(ViewClasses.TEXT_VIEW, 'There are no more transactions to display for this period.');
        this.optionToFilterTransactions = helper.byResourceId(ViewClasses.BUTTON, 'creditCardFilterButton');
        this.nextStatementDueDate = helper.byText(ViewClasses.TEXT_VIEW, 'Next statement due:');
        this.transactionType = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionType');
        this.creditCardNoTransactionMessage = helper.byText(ViewClasses.TEXT_VIEW, 'There are no transactions to display for this period.');
        this.chargeCardNoTransactionMessage = helper.byText(ViewClasses.TEXT_VIEW, 'There are no transactions to display for this period.');
        this.filterOverlay = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationToolbarText');
        this.accountTransactions = helper.byText(ViewClasses.TEXT_VIEW, 'Account Transactions');
        this.accountTransactionDescription = helper.byText(ViewClasses.TEXT_VIEW, 'Account transaction');
        this.cardEnding = '//android.widget.TextView[contains(@text,"Card ending:")]';
        this.cardEndingDescription = '//android.widget.TextView[contains(@text,"Card ending")]';
        this.cardEndingTransactionTotal = helper.byResourceId(ViewClasses.TEXT_VIEW, 'filter_transaction_total_title');
        this.cancelButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarCloseButton');
        this.filterErrorAccountlevel = helper.byText(ViewClasses.TEXT_VIEW, 'There are no cardholder transactions to filter. This may be because there are only account-level '
            + 'transactions, such as payments to the account.');
        this.typeCardFilter = helper.byText(ViewClasses.TEXT_VIEW, 'Card ending:');
        this.typeAccountFilter = helper.byText(ViewClasses.TEXT_VIEW, 'Account Transactions');
        this.total = helper.byResourceId(ViewClasses.TEXT_VIEW, 'filter_transaction_total_title');
        this.clearFilter = helper.byText(ViewClasses.BUTTON, 'Clear');
        this.directDebitAmountDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'directdebitamountdescription');
        this.chargeEndOfTransactionMessage = helper.byText(ViewClasses.TEXT_VIEW, 'There are no more transactions to display for this period.');
        this.pendingCreditCardTransactionsSubHeader = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionSectionHeader');
        this.firstTranscationType = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionType');
        this.pendingDeditCardTransactionsSubHeader = helper.byText(ViewClasses.TEXT_VIEW, 'Debit card transactions');
        this.pendingChequeTransactionsSubHeader = helper.byText(ViewClasses.TEXT_VIEW, 'Cheques being processed');
        this.paymentReceivedTransactionType = helper.byPartialText(ViewClasses.TEXT_VIEW, 'PAYMENT RECEIVED');
        this.contactUs = helper.byText(ViewClasses.TEXT_VIEW, 'Contact us');
        this.filterErrorTooManyTransactions = helper.byText(ViewClasses.TEXT_VIEW, 'Sorry, the filter cannot be used as there are too many transactions to display on the page.');
        this.aerInfoLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'treasuryGrossInterestRate');
        this.nominatedAccountInfoLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'treasurySettlementAccount');
        this.aerInfoPage = helper.byText(ViewClasses.TEXT_VIEW, 'What does Annual equivalent rate / gross interest rate mean?');
        this.nominatedAccountInfoPage = helper.byText(ViewClasses.TEXT_VIEW, 'What does nominated account mean?');
        this.aerInfo = helper.byText(ViewClasses.TEXT_VIEW, 'AER – The AER stands for Annual Equivalent Rate and is the notional rate which illustrates the gross rate as if paid and compounded on an annual basis. As every advert for a savings product will contain an AER you will be able to compare more easily what '
            + 'return you can expect from your savings over time.  Gross Rate – Gross rate means that no tax will be automatically deducted from interest on your behalf. You are responsible for paying any tax due to HM Revenue and Customs.');
        this.nominatedAccountInfo = helper.byText(ViewClasses.TEXT_VIEW, 'The nominated account is the account you specified when your deposit was set up. We will debit this account when you add funds to your deposit account, and we will send funds back '
            + 'here when you withdraw from your deposit account.');
        this.productNotAvailable = helper.byText(ViewClasses.TEXT_VIEW, 'We’re sorry but information about this product is not currently available. Please try later. Call us on 0345 305 5555 (8am-5pm, Monday to Friday) if you need any help.');
        this.aerGrossInterestRate = helper.byText(ViewClasses.TEXT_VIEW, 'AER / gross interest rate*:');
        this.nominatedAccount = helper.byText(ViewClasses.TEXT_VIEW, 'Nominated account**:');
        this.basicTaxRateStatus = helper.byText(ViewClasses.TEXT_VIEW, 'Basic tax rate status:');
        this.accountOpenedOn = helper.byText(ViewClasses.TEXT_VIEW, 'Account opened on:');
        this.currentMaturityInstructions = helper.byText(ViewClasses.TEXT_VIEW, 'Current maturity instructions:');
        this.dateInstructionReceived = helper.byText(ViewClasses.TEXT_VIEW, 'Date instruction received:');
        this.maturityDate = helper.byText(ViewClasses.TEXT_VIEW, 'Maturity date:');
        this.daysToMaturity = helper.byText(ViewClasses.TEXT_VIEW, 'Days to maturity:');
        this.estimatedGrossInterestForTermOfDeposit = helper.byText(ViewClasses.TEXT_VIEW, 'Estimated gross interest for term of deposit:');
        this.balanceLastUpdated = helper.byText(ViewClasses.TEXT_VIEW, 'Balance last updated:');
        this.multipleSettlementAccountTextMsg = helper.byText(ViewClasses.TEXT_VIEW, 'As you have more than one settlement account associated with this product, we’re unable to display all the details involved on screen. Please call us on 0345 305 5555 (8am-5pm, Monday to Friday) and one of our '
            + 'advisors will be happy to provide you further account details.');
        this.dailyOdFeeTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'DAILY OD FEE');
        this.upcomingPaymentPage = helper.byText(ViewClasses.VIEW, 'Upcoming payments');
        this.scottishWidowsStatementText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'pensionCellInfo');
        this.scottishWidowsFindOUtMoreButton = helper.byResourceId(ViewClasses.BUTTON, 'pensionCellButton');
        this.scottishWidowsfirstFAQ = helper.byText(ViewClasses.TEXT_VIEW, 'Who are Scottish Widows?');
        this.typeFilter = helper.byResourceId(ViewClasses.TEXT_VIEW, 'filter_showing_title');
        this.statementsSection = '//*[contains(@resource-id,"transactionList")]';
        this.pendingLabel = helper.byText(ViewClasses.TEXT_VIEW, 'Pending');
        this.exchangeRateAndAdditionalInformationTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'FORGN PYT249819167');
        this.exchangeRateWithNoAdditionalInformationTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'FORGN PYT211319997');
        this.noExchangeRateOrAdditionalInformationTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'FORGN PYT211319987');
        this.additionalInformationAndNoExchangeRateTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'FORGN PYT249819237');
        this.creditPendingTransactionErrorMessage = helper.byText(ViewClasses.TEXT_VIEW, 'For technical reasons we can\'t show details of pending credit card transactions right now.');
        this.debitChequePendingTransactionErrorMessage = helper.byText(ViewClasses.TEXT_VIEW, 'For technical reasons we can\'t show details of pending cheques or debit card transactions right now.');
        this.cashBackBankGiroCreditTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'CASHBACK');
        this.spendingRewardsHubSTUBPage = helper.byText(ViewClasses.VIEW, 'New Spending Rewards');
        this.accountName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileAccountName');
        this.accountNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileAccountNumber');
        this.sortCode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileAccountSortCode');
        this.accountBalance = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMainBalance');
        this.homeInsuranceStaticProductTitle = helper.byText(ViewClasses.TEXT_VIEW, 'Home Insurance');
        this.policyCoverType = helper.byText(ViewClasses.TEXT_VIEW, 'Buildings and Contents');
        this.insuredAddress = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTilePropertyAddress');
        this.periodOfCover = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTilePeriodOfCover');
        this.availableFunds = helper.byText(ViewClasses.TEXT_VIEW, 'Available:');
        this.pendingChequeTransactionType = `${this.noteText}/following::*[@text="Pending"][1]`;
        this.allTabStatementsPage = helper.byText(ViewClasses.TEXT_VIEW, 'All');
        this.currentEndOfTransactionMessage = helper.byText(ViewClasses.TEXT_VIEW, 'There are no more transactions to display for this period.');
        this.noCurrentInOutsTransactionMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'totalAmountOut');
        this.accountDebitedTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Amazon UK Marketplace');
        this.accountCreditedTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Amazon Refund');
        this.refundTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'Amazon UK Refund');
        this.searchIcon = helper.byResourceId(ViewClasses.VIEW, 'allTransactionsSearchBarOverlay');
        this.errorMessageRedBanner = helper.byResourceId(ViewClasses.TEXT_VIEW, 'notificationContentTextView');
        this.errorMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'messageText');
        this.tryAgainLink = helper.byText(ViewClasses.TEXT_VIEW, 'tap here to try again');
        this.mortgageSummaryStatementsPage = helper.byText(ViewClasses.TEXT_VIEW, 'Summary');
        this.pendingDebitTransactionDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDescription');
        this.pendingDebitTransactionAmount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionAmount');
        this.accountInfoBalance = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileTopValue');
        this.overDraftLimit = helper.byResourceId(ViewClasses.TEXT_VIEW, 'overdraftLimitLabel');
        this.interestRateLabel = helper.byText(ViewClasses.TEXT_VIEW, 'Interest rate:');
        this.interestRateDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMidValue');
        this.creditLimit = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileCreditLimitLabel');
        this.availableCredit = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileAvailableCreditLabel');
        this.overDueAmount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileOverdueMoneyLabel');
        this.homeInsurancePolicynumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'homeInsurancePolicyNumber');
        this.headerAccountBalance = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationToolbarSubText');
        this.accountTileBusinessName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileABusinessName');
        this.monthlyPayment = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMonthlyPaymentLabel');
        this.balanceAsOf = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileBalanceDateLabel');
        this.NotFinalSettlementText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileRedemptionLabel');
        this.originalLoan = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileOriginalLoanLabel');
        this.accountlinkLead = (account) => `${helper.byText(ViewClasses.TEXT_VIEW, account)}
        /../../../android.webkit.WebView`;
        this.grossInterestRateLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileFTDGrossInterestRateLabel');
        this.viewRatesLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMidValue');
        this.rateOfInterest = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMidValue');
        this.actionMenuHeader = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileActionMenuHeader');
        this.fundValueDate = helper.byResourceId(ViewClasses.TEXT_VIEW, 'pensionFundValueDate');
        this.balanceAfterPendingValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'balanceAfterPendingValue');
        this.balanceAfterPendingLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'balanceAfterPendingLabel');
        this.overDraftLimitValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'overdraftLimitValue');
        this.accountTrueBalance = helper.byResourceIdAndIndex(ViewClasses.TEXT_VIEW, 'transactionAmount', '1');
        this.overDraftLimitLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'overdraftLimitLabel');
        this.overDraftMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Daily arranged overdraft fees may be charged if your '
            + 'account balance is negative at midnight. Currently your account balance is');
        this.rePossessedMessageWarning = '//*[contains(@text,"Your mortgage is in possession with the bank")]';
        this.offSetMessageWarning = '//*[contains(@text,"an \'offset\' mortgage and your details can\'t be viewed online.") or contains(@text,"an offset mortgage and your details can\'t be viewed online.")]';
        this.pendingCreditTransactionDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDescription');
        this.pendingCreditTransactionAmount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionAmount');
        this.noOverDraft = helper.byText(ViewClasses.TEXT_VIEW, '£0.00');
        this.spendingInsightsPage = '//*[@resource-id="si-total-spend"]';
        this.remainingAllowance = helper.byText(ViewClasses.TEXT_VIEW, 'Remaining allowance:');
        this.htbInterestRate = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileInterestRateText');
        this.accountTileContent = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'accountTileContentView');
        this.balanceAfterPending = this.balanceAfterPendingValue;
        this.overdraftLimitProgressBar = helper.byResourceId(ViewClasses.PROGRESS_BAR, 'accountTileOverdraftRemainingProgress');
        this.overDraftRemainingLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'overdraftRemainingLabel');
        this.overDraftRemainingValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'overdraftRemainingValue');
        this.unclearedChequesHeader = helper.byText(ViewClasses.TEXT_VIEW, 'UNCLEARED CHEQUES');
        this.chequeNoteText = helper.byText(ViewClasses.TEXT_VIEW, 'NOTE: Uncleared cheques show in your account balance but not in \'balance after pending\'.'
            + ' You can only use the money when it clears.');
        this.pendingHeader = helper.byText(ViewClasses.TEXT_VIEW, 'PENDING');
        // TODO STAMOB-1912
        this.accountBalanceHeader = '//*[contains(@text,"Account balance")]';
        this.totalPending = `${this.pendingHeader}/following-sibling::*[@resource-id, ":id/transactionAmount"]`;
        this.totalChequesPending = `${this.unclearedChequesHeader}/following-sibling::*[@resource-id, 
        ":id/transactionAmount"]`;
        this.exposedPendingChequeTransactionAmount = `${this.chequeNoteText}/following::*[@resource-id, 
        ":id/pendingAmount"][4]`;
        this.exposedPendingChequeTransactionDescription = `${this.chequeNoteText}/following::*[@resource-id, 
        ":id/pendingDescription"][1]`;
        this.exposedPendingDebitTransactionDescription = `${this.pendingHeader}/following::*[@resource-id, 
        ":id/pendingDescription"][4]`;
        this.exposedPendingDebitTransactionAmount = `${this.pendingHeader}/following::*[@resource-id, 
        ":id/pendingAmount"][5]`;
        this.debitPendingErrorMessage = `${this.pendingHeader}
        /following::*[@text="Sorry - we can't show pending transactions at this time"]`;
        this.chequePendingErrorMessage = `${this.unclearedChequesHeader}
        /following::*[@text="Sorry – we can't show uncleared cheques at this time"]`;
        this.exposedPendingChequeTransactionType = `${this.exposedPendingChequeTransactionDescription}`;
        this.exposedPendingDirectDebitTransactionType = `${this.exposedPendingDebitTransactionDescription}`;
        this.chevron = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'chevron');
        this.chevronOnPendingChequeLine = `//*[contains(@content-desc,
         "Cheque logo")][1]${this.chevron}`;
        this.chevronOnPendingTransactionLine = `//*[contains(@content-desc,
         "Pending Transaction logo")][1]${this.chevron}`;
        this.debitCardTransactionType = helper.byText(ViewClasses.TEXT_VIEW, 'TFL.GOV.UK-CP');
        this.transactionLogo = `(${helper.byResourceId(ViewClasses.IMAGE_VIEW, 'logoImageView')})[1]`;
    }
}

module.exports = AccountStatementScreen;
