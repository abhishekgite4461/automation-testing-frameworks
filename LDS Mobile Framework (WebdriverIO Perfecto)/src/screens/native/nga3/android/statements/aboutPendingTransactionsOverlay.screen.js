const BaseAboutPendingTransactionsOverlayScreen = require('../../../statements/aboutPendingTransactionsOverlay.screen.js');
const helper = require('../../../android.helper.js');
const ViewClasses = require('../../../../../enums/androidViewClass.enum.js');

class AboutPendingTransactionsOverlayScreen extends BaseAboutPendingTransactionsOverlayScreen {
    constructor() {
        super();
        this.alertTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.aboutPendingTransactionsDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogContentText');
        this.closeAlert = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
    }
}

module.exports = AboutPendingTransactionsOverlayScreen;
