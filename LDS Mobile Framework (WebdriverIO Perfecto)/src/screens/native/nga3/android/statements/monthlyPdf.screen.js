const BaseMonthlyPdfScreen = require('../../../statements/monthlyPdf.screen.js');
const helper = require('../../../android.helper.js');
const ViewClasses = require('../../../../../enums/androidViewClass.enum.js');

class MonthlyPdfScreen extends BaseMonthlyPdfScreen {
    constructor() {
        super();
        // TODO STAMOB-2524 Add IDs
        this.title = helper.byText(ViewClasses.TEXT_VIEW, 'Select the date you want to view');
        this.picker = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'pdfMonthAndYearPicker');
        this.monthSelectionCloseButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarCloseButton');
        this.monthSelectionViewButton = helper.byResourceId(ViewClasses.BUTTON, 'pdfViewPdf');
        this.pdfPreviewScreen = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'pdf_view');
        this.monthField = `(${helper.byPartialResourceId(ViewClasses.EDIT_TEXT, 'numberpicker_input')})[1]`;
        this.yearField = `(${helper.byPartialResourceId(ViewClasses.EDIT_TEXT, 'numberpicker_input')})[2]`;
        this.allowPdf = helper.byResourceId(ViewClasses.BUTTON, 'permission_allow_button');
        this.digitalInboxScreen = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationToolbarText');
        this.inboxBackButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarBackButton');
        this.pdfViewer = `${helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'text1')}[1]`;
    }
}

module.exports = MonthlyPdfScreen;
