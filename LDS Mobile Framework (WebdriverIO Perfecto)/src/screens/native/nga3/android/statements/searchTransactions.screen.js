const BaseSearchTransactionsScreen = require('../../../statements/searchTransactions.screen.js');
const helper = require('../../../android.helper.js');
const ViewClasses = require('../../../../../enums/androidViewClass.enum.js');

class SearchTransactionsScreen extends BaseSearchTransactionsScreen {
    constructor() {
        super();
        this.searchHeader = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationToolbarText');
        this.searchDialogBox = helper.byResourceId(ViewClasses.EDIT_TEXT, 'searchEditText');
        this.searchInstruction = helper.byText(ViewClasses.EDIT_TEXT, 'Type a name or an amount');
        this.clearSearch = helper.byResourceId(ViewClasses.TEXT_VIEW, 'clearView');
        this.searchInfo = helper.byResourceId(ViewClasses.TEXT_VIEW, 'searchInfoView');
        this.searchFurtherBack = helper.byResourceId(ViewClasses.BUTTON, 'searchMoreTransactionsButton');
        this.searchResultsTransactionsList = '//*[contains(@resource-id,"allTransactionList")]';
        this.transactionListEmpty = helper.byResourceId(ViewClasses.VIEW, 'allTransactionListEmptyView');
        this.noMoreTransactionsMessage = helper.byText(ViewClasses.TEXT_VIEW, 'No more transactions to show.');
        this.backButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarBackButton');
        this.noTransactionsToSearch = helper.byText(ViewClasses.TEXT_VIEW, 'There aren\'t any transactions on this account yet');
        this.searchResultsCount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'searchInfoView');
        this.ZeroResultsDateRangeMessage = helper.byText(ViewClasses.TEXT_VIEW, '0 new results found in this date range.');
    }
}

module.exports = SearchTransactionsScreen;
