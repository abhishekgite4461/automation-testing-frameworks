const Screen = require('../../../../screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class HelpDeskScreen extends Screen {
    constructor() {
        super();
        this.helpCMSTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'cmsMessageFragmentTitle');
        this.helpDeskError = helper.byResourceId(ViewClasses.TEXT_VIEW, 'messageScreenBody');
        this.helpDeskCMSError = helper.byResourceId(ViewClasses.TEXT_VIEW, 'cmsMessageFragmentWebView');
    }
}

module.exports = HelpDeskScreen;
