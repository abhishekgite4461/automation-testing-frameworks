const BasePostCodeErrorScreen = require('../../../ibRegistration/postCodeError.screen.js');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class PostCodeErrorScreen extends BasePostCodeErrorScreen {
    constructor() {
        super();
        this.postCodeTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'postcodeTitle');
        this.postCode = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'postcodeInput')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.nextButton = helper.byResourceId(ViewClasses.BUTTON, 'postcodeNextButton');
    }
}

module.exports = PostCodeErrorScreen;
