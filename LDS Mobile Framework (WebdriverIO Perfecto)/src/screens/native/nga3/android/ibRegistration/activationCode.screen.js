const BaseActivationCodeScreen = require('../../../ibRegistration/activationCode.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ActivationCodeScreen extends BaseActivationCodeScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'registrationActivationCodeTitle');
        this.activationCode = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'registrationActivationCodeInputField')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.confirmCode = helper.byResourceId(ViewClasses.BUTTON, 'registrationActivationCodeContinueButton');
        this.requestNewCode = helper.byResourceId(ViewClasses.BUTTON, 'registrationActivationCodeResendButton');
        this.errorMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'notificationContentTextView');
    }
}

module.exports = ActivationCodeScreen;
