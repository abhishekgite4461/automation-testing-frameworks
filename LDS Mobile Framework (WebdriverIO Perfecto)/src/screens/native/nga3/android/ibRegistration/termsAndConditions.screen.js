const helper = require('../../../android.helper');
const BaseTnCScreen = require('../../../ibRegistration/termsAndConditions.screen');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class TermsAndConditionsScreen extends BaseTnCScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'commonTermsAndConditionsBottomContentView');
        this.agreeButton = helper.byResourceId(ViewClasses.BUTTON, 'commonPrimaryButton');
        this.backButton = helper.byResourceId(ViewClasses.BUTTON, 'commonSecondaryButton');
        this.footerSection = helper.byResourceId(ViewClasses.TEXT_VIEW, 'commonTermsAndConditionsFooterLabel');
    }
}

module.exports = TermsAndConditionsScreen;
