const BaseCreateSecureLogonDetailsScreen = require('../../../ibRegistration/createSecureLogonDetails.screen.js');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class CreateSecureLogonDetailsScreen extends BaseCreateSecureLogonDetailsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'loginDetailsTitle');
        this.userId = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'loginDetailsUserIdInput')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.userIdCheck = helper.byText(ViewClasses.TEXT_VIEW, 'Check');
        this.userIdOptionsTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.userIdOptionsBack = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.userIdOptionsSuggestion = helper.byResourceIdAndIndex(ViewClasses.BUTTON, 'radioOptionButton', 1);
        this.password = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'loginDetailsPasswordInput')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.hideShowPassword = helper.byResourceId(ViewClasses.TEXT_VIEW, 'inputFieldControllerButton');
        this.passwordfindOutMore = helper.byResourceId(ViewClasses.TEXT_VIEW, 'inputFieldTipLink');
        this.findOutMorePopUp = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.findOutMorePopUpClose = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.confirmPassword = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'loginDetailsReenterPasswordInput')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.hideShowConfirmPassword = helper.byResourceId(ViewClasses.TEXT_VIEW, 'inputFieldControllerButton');
        this.userIdTip = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'loginDetailsUserIdTip');
        this.passwordTip = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'loginDetailsPasswordTip');
        this.confirmPasswordTip = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'loginDetailsReenterPasswordTip');
        this.commercialAccountMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'loginDetailsCommercialMandateUserIdMessage');
        this.commercialUserId = helper.byResourceId(ViewClasses.TEXT_VIEW, 'loginDetailsCommercialMandateUserIdView');
        this.nextButton = helper.byResourceId(ViewClasses.BUTTON, 'commonPrimaryButton');
        this.errorIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'inputFieldStatusIcon');
        this.loadingIndicator = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogLoadingTitle');
    }
}

module.exports = CreateSecureLogonDetailsScreen;
