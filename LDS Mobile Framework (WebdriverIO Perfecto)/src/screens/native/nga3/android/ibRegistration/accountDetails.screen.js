const BaseAccountDetailsScreen = require('../../../ibRegistration/accountDetails.screen.js');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class AccountDetailsScreen extends BaseAccountDetailsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountDetailsTitle');
        this.errorMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'notificationContentTextView');
        this.errorMessageDismiss = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'notificationCloseImageView');
        this.accountType = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'accountDetailsAccountTypePicker');
        this.savingsSelector = helper.byResourceId(ViewClasses.TEXT_VIEW, 'registrationAccountTypeCurrent');
        this.loanSelector = helper.byResourceId(ViewClasses.TEXT_VIEW, 'registrationAccountTypeLoan');
        this.mortgageSelector = helper.byResourceId(ViewClasses.TEXT_VIEW, 'registrationAccountTypeMortgage');
        this.creditCardsSelector = helper.byResourceId(ViewClasses.TEXT_VIEW, 'registrationAccountTypeCreditCard');
        this.sortcodeFirstSection = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'sortCodeBoxOne')}/android.widget.RelativeLayout/android.widget.EditText`;
        this.sortcodeSecondSection = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'sortCodeBoxTwo')}/android.widget.RelativeLayout/android.widget.EditText`;
        this.sortcodeThirdSection = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'sortCodeBoxThree')}/android.widget.RelativeLayout/android.widget.EditText`;
        this.accountNumber = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'accountDetailsAccountNumberInput')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.loanReferenceNumber = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'accountDetailsLoanReferenceInput')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.loanTip = helper.byResourceId(ViewClasses.TEXT_VIEW, 'inputFieldTipMessage');
        this.mortgageNumber = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'accountDetailsMortgageNumberInput')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.mortgageTip = helper.byResourceId(ViewClasses.TEXT_VIEW, 'inputFieldTipMessage');
        this.creditCardNumber = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'accountDetailsCreditCardNumberInput')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.creditCardTip = helper.byResourceId(ViewClasses.TEXT_VIEW, 'inputFieldTipMessage');
        this.nextButton = helper.byResourceId(ViewClasses.BUTTON, 'commonPrimaryButton');
        this.backButton = helper.byResourceId(ViewClasses.BUTTON, 'commonSecondaryButton');
    }
}

module.exports = AccountDetailsScreen;
