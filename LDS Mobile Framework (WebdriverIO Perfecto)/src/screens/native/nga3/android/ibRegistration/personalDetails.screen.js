const BasePersonalDetailsScreen = require('../../../ibRegistration/personalDetails.screen.js');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class PersonalDetailsScreen extends BasePersonalDetailsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'personalDetailsTitle');
        this.errorMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'notificationContentTextView');
        this.firstName = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'personalDetailsFirstNameInput')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.lastName = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'personalDetailsLastNameInput')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.emailId = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'personalDetailsEmailInput')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.dateOfBirth = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'personalDetailsDateOfBirthInput');
        this.postCode = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'personalDetailsPostcodeInput')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.postCodeTip = helper.byResourceId(ViewClasses.TEXT_VIEW, 'tipMessageView');
        this.backButton = helper.byResourceId(ViewClasses.BUTTON, 'commonSecondaryButton');
        this.nextButton = helper.byResourceId(ViewClasses.BUTTON, 'commonPrimaryButton');
        this.errorMessageDismiss = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'notificationCloseImageView');
        this.dobOkButton = '//*[@resource-id="android:id/button1"]';
        this.dobCancelButton = '//*[@resource-id="android:id/button2"]';
    }
}

module.exports = PersonalDetailsScreen;
