const BaseRegistrationSuccessScreen = require('../../../ibRegistration/registrationSuccess.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class RegistrationSuccessScreen extends BaseRegistrationSuccessScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'registrationSuccessBody');
        this.subTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'registrationSuccessBody');
        this.continueButton = helper.byResourceId(ViewClasses.BUTTON, 'registrationSuccessContinueButton');
        this.successMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'registrationSuccessBody');
        this.footerMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'registrationSuccessFooter');
    }
}

module.exports = RegistrationSuccessScreen;
