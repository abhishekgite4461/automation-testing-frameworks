const BaseLetterSentScreen = require('../../../ibRegistration/letterSent.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class LetterSentScreen extends BaseLetterSentScreen {
    constructor() {
        super();
        this.letterSentTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'registrationLetterSentTitleView');
        this.registeredUsername = helper.byResourceId(ViewClasses.TEXT_VIEW, 'registrationLetterSentUserIdView');
        this.letterSentText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'registrationLetterSentMessageView');
    }
}

module.exports = LetterSentScreen;
