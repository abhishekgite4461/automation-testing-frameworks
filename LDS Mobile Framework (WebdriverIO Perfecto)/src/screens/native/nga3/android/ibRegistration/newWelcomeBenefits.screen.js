const BaseNewWelcomeBenefitsScreen = require('../../../welcomeBenefits.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class NewWelcomeBenefitsScreen extends BaseNewWelcomeBenefitsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationToolbarText');
        this.registerButton = helper.byResourceId(ViewClasses.BUTTON, 'commonSecondaryButton');
        this.registerForIbButton = helper.byText(ViewClasses.TEXT_VIEW, 'Register for Online Card Services');
        this.logOnButton = helper.byResourceId(ViewClasses.BUTTON, 'commonPrimaryButton');
        this.webTitle = helper.byContentDesc(ViewClasses.VIEW, 'Select an account for registration');
        this.fraudLink = 'guarantee';
        this.demoLink = 'video on setting';
        this.customerLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'welcomeSectionFourBody');
        this.alertDialog = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.yesLeaveTheApp = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.cancelLeaveTheApp = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.fraudGuaranteeTitle = helper.byText(ViewClasses.EDIT_TEXT, 'https://www.lloydsbank.com/help-guidance/security.asp');
        this.appDemoTitle = helper.byText(ViewClasses.EDIT_TEXT, 'https://www.youtube.com/embed/XFgEYu5qf68?autoplay=1&rel=0&autohide=1&showinfo=0&wmode=transparent');
        this.newCustomerTitle = helper.byText(ViewClasses.EDIT_TEXT, 'https://www.lloydsbank.com/current-accounts.asp??=#current-accounts');
        this.legalInfoButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'ngaButtonTitle');
    }
}

module.exports = NewWelcomeBenefitsScreen;
