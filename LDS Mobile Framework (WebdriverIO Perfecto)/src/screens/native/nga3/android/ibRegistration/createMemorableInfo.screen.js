const BaseCreateMemorableInfoScreen = require('../../../ibRegistration/createMemorableInfo.screen.js');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class CreateMemorableInfoScreen extends BaseCreateMemorableInfoScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'createMiTitle');
        this.userIdErrorMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorViewText');
        this.memorableInfo = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'createMiInputField')}//android.widget.EditText`;
        this.confirmMemorableInfo = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'createMiConfirmInputField')}//android.widget.EditText`;
        this.backButton = helper.byResourceId(ViewClasses.BUTTON, 'commonSecondaryButton');
        this.nextButton = helper.byResourceId(ViewClasses.BUTTON, 'commonPrimaryButton');
        this.memorableInfoTip = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'createMiTipView');
        this.confirmMemorableInfoTip = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'createMiConfirmTipView');
        this.hideShowMemorableInfo = helper.byResourceId(ViewClasses.TEXT_VIEW, 'inputFieldControllerButton');
        this.hideShowConfirmMemorableInfo = helper.byResourceId(ViewClasses.TEXT_VIEW, 'inputFieldControllerButton');
        this.findOutMoreMemorableInfo = helper.byResourceId(ViewClasses.TEXT_VIEW, 'inputFieldTipLink');
        this.findOutMoreTipClose = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.findOutMoreTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
    }
}

module.exports = CreateMemorableInfoScreen;
