const BaseInterestRateSavingAccountScreen = require('../../interestRateSavingAccount.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class InterestRateSavingAccountScreen extends BaseInterestRateSavingAccountScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'listHeader');
        this.availableBalalce = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Available NIL.');
        this.interestRatetText = helper.byPartialText(ViewClasses.TEXT_VIEW, '% gross');
    }
}

module.exports = InterestRateSavingAccountScreen;
