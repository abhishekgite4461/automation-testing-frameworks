const BaseEnterPostalOtpScreen = require('../../enterPostalOtp.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class EnterPostalOtpScreen extends BaseEnterPostalOtpScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'otpBodyText');
        this.otpNotRecievedToolTip = helper.byResourceId(ViewClasses.TEXT_VIEW, 'contactButton');
        this.preAuthHeader = helper.byResourceId(ViewClasses.TEXT_VIEW, 'logOffMenu');
        this.logoffOption = helper.byResourceId(ViewClasses.TEXT_VIEW, 'title');
        this.enterOTPField = helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText');
        this.continueButton = helper.byResourceId(ViewClasses.BUTTON, 'otpContinueButton');
        this.invalidOTPErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, '9200306');
        this.errorCloseButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'notificationCloseImageView');
    }
}

module.exports = EnterPostalOtpScreen;
