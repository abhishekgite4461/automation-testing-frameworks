const BaseFingerprintSuccessScreen = require('../../../fingerprint/fingerprintSuccess.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class FingerprintSuccessScreen extends BaseFingerprintSuccessScreen {
    constructor() {
        super();
        this.okButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'fingerprintOptedInModal');
    }
}

module.exports = FingerprintSuccessScreen;
