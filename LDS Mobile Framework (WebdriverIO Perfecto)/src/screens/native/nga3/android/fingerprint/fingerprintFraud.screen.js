const BaseFingerprintFraudScreen = require('../../../fingerprint/fingerprintFraud.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class FingerprintFraudScreen extends BaseFingerprintFraudScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'fingerprintRiskModal');
        this.yesButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.noButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
    }
}

module.exports = FingerprintFraudScreen;
