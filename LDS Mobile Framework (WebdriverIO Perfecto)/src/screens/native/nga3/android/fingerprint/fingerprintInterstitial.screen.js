const BaseFingerprintInterstitialScreen = require('../../../fingerprint/fingerprintInterstitial.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class FingerprintInterstitialScreen extends BaseFingerprintInterstitialScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.BUTTON, 'fingerprintInterstitialOptInButton');
        this.yesButton = helper.byResourceId(ViewClasses.BUTTON, 'fingerprintInterstitialOptInButton');
        this.noButton = helper.byResourceId(ViewClasses.BUTTON, 'fingerprintInterstitialOptOutButton');
    }
}

module.exports = FingerprintInterstitialScreen;
