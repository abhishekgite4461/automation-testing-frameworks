const BaseFingerprintLoginScreen = require('../../../fingerprint/fingerprintLogin.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class FingerprintLoginScreen extends BaseFingerprintLoginScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'fingerprintLoginModal');
        this.cancel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
    }
}

module.exports = FingerprintLoginScreen;
