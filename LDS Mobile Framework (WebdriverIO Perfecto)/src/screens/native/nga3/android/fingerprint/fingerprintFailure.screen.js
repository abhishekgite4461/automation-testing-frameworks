const BaseFingerprintRiskRejectionScreen = require('../../../fingerprint/fingerprintFailure.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class FingerprintRiskRejectionScreen extends BaseFingerprintRiskRejectionScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'fingerprintRiskRejectionModal');
        this.okButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
    }
}

module.exports = FingerprintRiskRejectionScreen;
