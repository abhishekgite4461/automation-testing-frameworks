const BaseFingerprintNoFingerprintsScreen = require('../../../fingerprint/fingerprintNoFingerprints.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class FingerprintNoFingerprintsScreen extends BaseFingerprintNoFingerprintsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'fingerprintsNotFoundModal');
        this.okButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
    }
}

module.exports = FingerprintNoFingerprintsScreen;
