const BaseEnvironmentScreen = require('../../environment.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class EnvironmentScreen extends BaseEnvironmentScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'environmentAppVersionItem');
        this.resetFeatureConfigButton = helper.byResourceId(ViewClasses.BUTTON, 'environmentResetUnfinishedFeaturesButton');
        // MQE-1075 TODO
        this.oldResetFeatureConfigButton = helper.byResourceId(ViewClasses.BUTTON, 'environmentResetUnfinishedFeaturesButton');
        this.server = (name) => helper.byText(ViewClasses.TEXT_VIEW, name);
        this.quickStubText = helper.byResourceId(ViewClasses.EDIT_TEXT, 'environmentQuickStubText');
        this.quickStubButton = helper.byResourceId(ViewClasses.BUTTON, 'environmentQuickStubButton');
        this.featureConfigText = helper.byResourceId(ViewClasses.EDIT_TEXT, 'featureConfigs');
        this.featureConfigGoButton = helper.byResourceId(ViewClasses.BUTTON, 'featureconfigQuickStubButton');
        this.unfinishedFeaturesCheckbox = helper.byResourceIdAndIndex(ViewClasses.CHECK_BOX, 'featureEnabledCheckBox', 1);
        this.deenrolledErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Sorry, your device has been de-registered for security reasons.');
        this.deenrolledContinueButton = helper.byResourceId(ViewClasses.BUTTON, 'errorScreenButtonPrimary');
        this.inputAppVersion = helper.byResourceId(ViewClasses.EDIT_TEXT, 'appHeaderVersion');
    }
}

module.exports = EnvironmentScreen;
