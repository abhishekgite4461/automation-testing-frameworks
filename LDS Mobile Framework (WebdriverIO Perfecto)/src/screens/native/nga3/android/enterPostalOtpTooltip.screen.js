const BaseEnterPostalOtpTooltipScreen = require('../../enterPostalOtpTooltip.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class EnterPostalOtpTooltipScreen extends BaseEnterPostalOtpTooltipScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'otpContactDialog');
        this.closeButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.contactUsButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
    }
}

module.exports = EnterPostalOtpTooltipScreen;
