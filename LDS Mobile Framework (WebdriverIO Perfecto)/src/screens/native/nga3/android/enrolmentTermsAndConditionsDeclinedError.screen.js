const BaseEnrolmentTermsAndConditionsDeclinedError = require('../../enrolmentTermsAndConditionsDeclinedError.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class EnrolmentTermsAndConditionsDeclinedErrorScreen extends BaseEnrolmentTermsAndConditionsDeclinedError {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenTitle');
        this.termsAndConditionsDeclinedError = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Sorry, it seems you haven\'t accepted our');
    }
}

module.exports = EnrolmentTermsAndConditionsDeclinedErrorScreen;
