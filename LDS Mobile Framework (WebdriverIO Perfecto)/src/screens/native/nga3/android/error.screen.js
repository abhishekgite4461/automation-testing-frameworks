const BaseErrorScreen = require('../../error.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class ErrorScreen extends BaseErrorScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'errorScreenIcon');
        this.revokedIDErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, '9200200');
        this.SCARevokedErrorMessage = '//android.widget.TextView[contains(@text,"your account has been locked")]';
        this.suspendedIDErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'suspended');
        this.inactiveIDErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, '9200141');
        this.logonToMobileBanking = helper.byResourceId(ViewClasses.BUTTON, 'errorScreenButtonPrimary');
        this.compromisedPasswordErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'temporarily suspended');
        this.partiallyRegisteredErrorMessage = '//android.widget.TextView[contains(@text,"or have not completed the authentication process for full access") or contains(@text,"or haven\'t completed the authentication process for full access")]';
        this.telephoneNumber = helper.byPartialText(ViewClasses.TEXT_VIEW, '+44');
        this.twoFactorAuthErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Sorry, we couldn’t complete your request. Please try later or call us');
        this.homeButton = helper.byResourceId(ViewClasses.BUTTON, 'errorScreenButtonPrimary');
        this.hardTokenUserErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Sorry, you can not use this app ');
        this.softTokenUserErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'you can no longer use a soft token to register for Business');
        this.suspendedCardError = helper.byPartialText(ViewClasses.TEXT_VIEW, '9200161');
        this.oldCardExpiredError = helper.byPartialText(ViewClasses.TEXT_VIEW, '1000027');
        this.maxDeviceEnrolErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'exceeded the maximum number of devices you');
    }
}

module.exports = ErrorScreen;
