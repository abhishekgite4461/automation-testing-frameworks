const BaseChequeDepositScreen = require('../../../ics/chequeDeposit.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ChequeDepositScreen extends BaseChequeDepositScreen {
    constructor() {
        super();
        this.title = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Deposit Cheque');
        this.chequeAmountField = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'icsAmountField')}//android.widget.EditText`;
        this.chequeReferenceField = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'icsReferenceMessage')}//android.widget.EditText`;
        this.chequeAmountFeildHelpText = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'icsAmountInputFieldTipView');
        this.chequeReferenceFieldHelpText = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'icsReferenceInputFieldTipView');
        this.chequeErrorMsg = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'errorNotificationView');
        this.chequeLimitService = helper.byResourceId(ViewClasses.TEXT_VIEW, 'icsAccountLimitMessage');
        this.depositAccount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle');
        this.captureFrontOfChequeButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'icsFrontCameraMessage');
        this.captureBackOfImageButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'icsBackCameraMessage');
        this.moreInformationOkButton = helper.byText(ViewClasses.BUTTON, 'OK');
        this.moreInformationDialogbox = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.depositHistoryText = helper.byText(ViewClasses.TEXT_VIEW, 'Deposit History');
        this.winBackTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.depositChequeDisabled = helper.byResourceId(ViewClasses.TEXT_VIEW, 'icsErrorScreenTitle');
        this.cameraPopupAlert = helper.byResourceId(ViewClasses.BUTTON, 'permission_allow_button');
        this.useChequeImageButton = helper.byContentDesc(ViewClasses.TEXT_VIEW, 'Use cheque image');
        this.frontOfChequeThumbnail = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'icsFrontCamera');
        this.backOfChequeThumbnail = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'icsBackCamera');
        this.reviewDepositButton = helper.byResourceId(ViewClasses.BUTTON, 'icsReviewDepositButton');
        this.allowCamera = '//*[@resource-id="com.android.packageinstaller:id/permission_allow_button"]';
        this.closeCamerabutton = helper.byResourceId(ViewClasses.BUTTON, 'customBtnCancel');
        this.selectCheckBox = '//*[@resource-id="com.android.packageinstaller:id/do_not_ask_checkbox"]';
        this.denyPermission = '//*[@resource-id="com.android.packageinstaller:id/permission_deny_button"]';
        this.settingAlert = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.closeDemobutton = helper.byResourceId(ViewClasses.IMAGE_BUTTON, 'viewDemoClose');
        this.depositHomeBackButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarBackButton');
        this.homeAccountActionButton = helper.byResourceId(ViewClasses.IMAGE_BUTTON, 'accountTileActionMenu');
        this.errorMessageOkButton = '//*[@resource-id="android:id/button1"]';
        this.leaveButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.stayButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.currentMainBalance = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementBalance');
        this.navigateSettings = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.navigatePermissionSettings = helper.byPartialText(ViewClasses.TEXT_VIEW, 'permissions');
        this.selectBackOnSettings = helper.byContentDesc(ViewClasses.IMAGE_BUTTON, 'Navigate up');
        this.enableCameraPermissions = '//*[contains(@resource-id,"id/list")]/android.widget.LinearLayout[1]//android.widget.LinearLayout[2]/android.widget.Switch[1]';
        this.frontOfChequeImageThumbnail = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'icsChequeFrontImage');
        this.backOfChequeImageThumbnail = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'icsChequeBackImage');
    }
}
module.exports = ChequeDepositScreen;
