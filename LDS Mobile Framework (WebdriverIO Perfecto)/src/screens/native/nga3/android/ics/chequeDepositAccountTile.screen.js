const BaseChequeDepositAccountTileScreen = require('../../../ics/chequeDepositAccountTile.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ChequeDepositAccountTileScreen extends BaseChequeDepositAccountTileScreen {
    constructor() {
        super();
        this.accountsDisplayed = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'arrangementView');
    }
}

module.exports = ChequeDepositAccountTileScreen;
