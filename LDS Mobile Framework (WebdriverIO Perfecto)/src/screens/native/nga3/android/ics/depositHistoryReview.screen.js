const BaseDepositHistoryReviewScreen = require('../../../ics/depositHistoryReview.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class DepositHistoryReviewScreen extends BaseDepositHistoryReviewScreen {
    constructor() {
        super();
        this.tapHintFrontImage = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'icsZoomImage');
        this.depositStatus = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailStatus');
        this.depositAccountType = helper.byResourceId(ViewClasses.TEXT_VIEW, 'icsAccountType');
        this.depositedDate = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailDepositDate');
        this.depositAmount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailDepositAmount');
        this.depositReference = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailDepositReference');
        this.pendingDetailPageTitle = helper.byText(ViewClasses.TEXT_VIEW, 'Pending');
        this.statusTitleOnReviewPage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionReasonForStatus');
        this.fundsAvailableDetailPageTitle = helper.byText(ViewClasses.TEXT_VIEW, 'Funds Available');
        this.RejectedChequePageTitle = helper.byText(ViewClasses.TEXT_VIEW, 'Rejected');
        this.depositHistorybutton = helper.byResourceId(ViewClasses.BUTTON, 'icsDepositChequeDepositHistory');
        this.depositBusiness = helper.byResourceId(ViewClasses.TEXT_VIEW, 'icsBusinessName');
    }
}

module.exports = DepositHistoryReviewScreen;
