const BaseIcsMoreInformation = require('../../../ics/icsMoreInformation.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class IcsMoreInformation extends BaseIcsMoreInformation {
    constructor() {
        super();
        this.moreInformationDialogbox = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.moreInformationCheckbox = helper.byResourceId(ViewClasses.CHECK_BOX, 'checkboxDialogContainer');
        this.moreInformationText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'icsMoreInformation');
        this.moreInformationOkButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.moreInformationContent = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogContentInformation');
    }
}

module.exports = IcsMoreInformation;
