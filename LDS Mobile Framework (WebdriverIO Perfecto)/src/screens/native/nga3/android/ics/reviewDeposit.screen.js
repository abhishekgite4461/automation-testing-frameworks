const BaseReviewDepositScreen = require('../../../ics/reviewDeposit.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ReviewDepositScreen extends BaseReviewDepositScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'reviewDepositedInstruction');
        this.reviewDepositConfirmButton = helper.byResourceId(ViewClasses.BUTTON, 'icsReviewDepositAccept');
        this.frontOfChequeHint = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'icsZoomImage');
        this.BackOfChequeHint = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'icsZoomChequeBackImage');
        this.depositHistorybutton = helper.byResourceId(ViewClasses.BUTTON, 'icsDepositChequeDepositHistory');
        this.passwordConfirmationText = helper.byResourceId(ViewClasses.EDIT_TEXT, 'passwordConfirmationDialogPasswordBox');
        this.passwordConfirmButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.amountMismatchConfirm = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.amountMismatchCancel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.amountConfirmAlert = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.reviewBusinessName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'icsBusinessName');
        this.reviewDepositAccount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'icsAccountNo');
        this.reviewChequeAmount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailDepositAmount');
        this.reviewReferenceText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailDepositReference');
        this.reviewFrontChequeMagnifierIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'icsZoomImage');
        this.reviewBackChequeMagnifierIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'icsZoomChequeBackImage');
        this.reviewCancelButton = helper.byResourceId(ViewClasses.BUTTON, 'icsReviewDepositCancel');
        this.reviewFrontofCheque = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'icsChequeFrontImage');
        this.reviewBackofCheque = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'icsChequeBackImage');
    }
}

module.exports = ReviewDepositScreen;
