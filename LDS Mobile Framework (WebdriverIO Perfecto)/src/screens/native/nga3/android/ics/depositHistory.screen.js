const BaseDepositHistoryScreen = require('../../../ics/depositHistory.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class DepositHistoryScreen extends BaseDepositHistoryScreen {
    constructor() {
        super();
        this.depositChequeText = helper.byText(ViewClasses.TEXT_VIEW, 'Deposit Cheque');
        this.depositHistoryTitle = helper.byText(ViewClasses.TEXT_VIEW, 'Deposit History');
        this.helpandInfoNavigationTile = helper.byResourceId(ViewClasses.TEXT_VIEW, 'icsHelpAndInfo');
        this.depositHistoryList = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDate');
        this.depositDate = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDate');
        this.depositReference = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDescription');
        this.depositStatus = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionStatus');
        this.depositAmount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionAmount');
        this.accountNavigationImage = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'beneficiaryTileIcon');
        this.accountList = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'arrangementView');
        this.changeAccount = "(//*[contains(@resource-id,'arrangementView')])[2]";
        this.noMoretransactionMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'noTransactionsMessage');
        this.pendingStatus = helper.byText(ViewClasses.TEXT_VIEW, 'Rejected');
        this.noTransactionMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'noTransactionsMessage');
        this.accountType = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle');
        this.sortCode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSortCode');
        this.accountNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber');
        this.ledgerBalance = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementBalance');
        this.availableBalance = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementBalanceComments');
        this.referenceTextVisibility = (reference) => `//*[contains(@resourceid,'transactionDescription')]
        [text()='${reference}']`;
        this.referenceOnTransactionDetailPage = (reference) => `//*[contains(@resourceid,
        'transactionDetailDepositReference')][text()='${reference}']`;
        this.referenceIsNotVisibleOnDetailPage = (reference) => `//*[contains(@resourceid,
        'transactionDetailDepositReference')][text()='${reference}']`;
        this.selectPendingOption = helper.byText(ViewClasses.TEXT_VIEW, 'Pending');
        this.selectFundsAvailableOption = helper.byText(ViewClasses.TEXT_VIEW, 'Funds Available');
        this.selectRejectedOption = helper.byText(ViewClasses.TEXT_VIEW, 'Rejected');
        this.selectReferenceOnTransactionList = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionAmount');
    }
}

module.exports = DepositHistoryScreen;
