const BaseChequeDepositStatusScreen = require('../../../ics/chequeDepositStatus.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ChequeDepositStatusScreen extends BaseChequeDepositStatusScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'icsDepositChequeStatusHeading');
        this.fastChequeIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'icsDepositChequeLogo');
        this.viewDepositHistoryButton = helper.byResourceId(ViewClasses.BUTTON, 'icsDepositChequeDepositHistory');
        this.sortCodeError = helper.byPartialText(ViewClasses.TEXT_VIEW, 'can’t process this type of cheque using cheque imaging');
        this.duplicateChequeMessage = '//android.widget.TextView[contains(@text, "already deposited a cheque") or contains(@text,"already paid in a cheque")]';
        this.dailyLimitError = helper.byPartialText(ViewClasses.TEXT_VIEW, 'daily limit for cheque');
        this.depositAnotherCheque = helper.byResourceId(ViewClasses.BUTTON, 'icsDepositChequeDepositAnotherCheque');
        this.amountDeposited = helper.byResourceId(ViewClasses.TEXT_VIEW, 'icsDepositChequeAmount');
        this.successSortCode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'icsAccountSortCode');
        this.successDepositAccount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'icsAccountNo');
        this.successReferenceText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'IcsDepositChequeReferenceValue');
        this.dueDate = helper.byResourceId(ViewClasses.TEXT_VIEW, 'icsDepositChequeEstimatedDateValue');
        this.successBusinessName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'icsBusinessName');
    }
}

module.exports = ChequeDepositStatusScreen;
