const BaseDepositChequeImageScreen = require('../../../ics/depositChequeImage.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class DepositChequeImageScreen extends BaseDepositChequeImageScreen {
    constructor() {
        super();
        this.imageCloseButton = helper.byResourceId(ViewClasses.BUTTON, 'icsReviewChequeCloseButton');
    }
}

module.exports = DepositChequeImageScreen;
