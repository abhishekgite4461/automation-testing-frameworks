const BaseInternationalPaymentScreen = require('../../internationalPayment.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class InternationalPaymentScreen extends BaseInternationalPaymentScreen {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.BUTTON, 'Select an international recipient');
        this.recipients = helper.byText(ViewClasses.VIEW, 'International payments recipients');
        this.makePayment = helper.byPartialResourceIdWebView(ViewClasses.VIEW, 'frmDataTable:tblRecipientList');
        this.ipAmount = helper.byResourceIdWebView(ViewClasses.RADIO_BUTTON, 'frm1:currencyAmount:1');
        this.ipAmountField = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'frm1:paymentAmount');
        this.ipContinue = helper.byResourceIdWebView(ViewClasses.BUTTON, 'frm1:Btncontinue');
        this.confirmIPPayment = helper.byText(ViewClasses.VIEW, 'Confirm payment details');
        this.ipTermsAndConditions = helper.byResourceIdWebView(ViewClasses.CHECK_BOX, 'frmForm11:bchkbxAgreeTerms');
        this.ipPassword = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'frmForm11:txtAuthPwd');
        this.ipConfirm = helper.byResourceIdWebView(ViewClasses.BUTTON, 'frmForm11:btnContinue');
        this.ipPaymentConfirmation = helper.byText(ViewClasses.VIEW, 'International payment confirmed');
        this.txtBicSwift = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'quickRecipientAdd:txtBicSwift');
        this.contuneButton = helper.byResourceIdWebView(ViewClasses.BUTTON, 'quickRecipientAdd:Btncontinue');
        this.recipientBankAddr = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'recipientDetails:txtBankAddress1');
        this.IBAN = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'recipientDetails:txtIban1');
        this.recipientName = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'recipientDetails:txtRecipientName');
        this.recipientAddr = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'recipientDetails:txtAddress1');
        this.continueRecipientDetailsButton = helper.byResourceIdWebView(ViewClasses.BUTTON, 'recipientDetails:Btncontinue');
        this.confirmButton = helper.byResourceIdWebView(ViewClasses.BUTTON, 'myForm:Btncontinue');
        this.authenticationPage = helper.byText(ViewClasses.VIEW, 'Confirm international recipient’s details');
        this.radioButton = helper.byResourceIdWebView(ViewClasses.RADIO_BUTTON, 'myForm:numbers:2');
        this.successPage = helper.byText(ViewClasses.VIEW, 'International recipient successfully created');
    }
}

module.exports = InternationalPaymentScreen;
