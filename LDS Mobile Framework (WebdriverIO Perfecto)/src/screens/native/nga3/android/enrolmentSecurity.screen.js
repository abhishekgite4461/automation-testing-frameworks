const BaseEnrolmentSecurityScreen = require('../../enrolmentSecurity.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class EnrolmentSecurityScreen extends BaseEnrolmentSecurityScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'autoLogOffOptions'); // TODO MORE-540
        this.continueButton = helper.byResourceId(ViewClasses.BUTTON, 'saveButton');
        this.defaultTime = helper.byPartialText(ViewClasses.TEXT_VIEW, '10 minutes');
    }
}

module.exports = EnrolmentSecurityScreen;
