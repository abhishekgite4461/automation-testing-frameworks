const BaseMobileBrowserScreen = require('../mobileBrowser.screen');

class mobileBrowserScreen extends BaseMobileBrowserScreen {
    constructor() {
        super();
        this.brandUrl = 'online.mbna.co.uk';
    }
}

module.exports = mobileBrowserScreen;
