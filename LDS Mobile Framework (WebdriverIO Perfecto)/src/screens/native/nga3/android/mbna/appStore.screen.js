const BaseAppStoreScreen = require('../appStore.screen');

class AppStoreScreen extends BaseAppStoreScreen {
    constructor() {
        super();
        this.appText = "//*[@text='MBNA - Card Services App']";
    }
}

module.exports = AppStoreScreen;
