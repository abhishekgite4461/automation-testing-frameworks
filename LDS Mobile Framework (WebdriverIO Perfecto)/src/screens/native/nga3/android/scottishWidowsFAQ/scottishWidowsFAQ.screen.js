const BaseScottishWidowsFAQScreen = require('../../../scottishWidowsFAQ/scottishWidowsFAQ.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ScottishWidowsFAQScreen extends BaseScottishWidowsFAQScreen {
    constructor() {
        super();
        // TODO COE-99
        this.title = helper.byContentDesc(ViewClasses.TEXT_VIEW, 'F.A.Qs. Heading.');
        this.whatcanIdonextOption = helper.byText(ViewClasses.TEXT_VIEW, 'What can I do next?');
        this.scottishWidowHelpDeskNumber = helper.byPartialText(ViewClasses.TEXT_VIEW, '0345 769 7318');
    }
}

module.exports = ScottishWidowsFAQScreen;
