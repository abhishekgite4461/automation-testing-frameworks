const BaseFscsScreen = require('../../fscs.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class FscsScreen extends BaseFscsScreen {
    constructor() {
        super();
        // MOB3-5992 TODO Change the automation id of pop-up title
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.leaveButton = helper.byText(ViewClasses.TEXT_VIEW, 'Leave');
        this.stayButton = helper.byText(ViewClasses.TEXT_VIEW, 'Stay');
        this.compensationSchemeLabel = helper.byPartialText(ViewClasses.EDIT_TEXT, 'www.lloydsbank.com');
    }
}

module.exports = FscsScreen;
