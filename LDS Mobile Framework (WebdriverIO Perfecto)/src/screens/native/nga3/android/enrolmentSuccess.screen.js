const BaseEnrolmentSuccessScreen = require('../../enrolmentSuccess.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class EnrolmentSuccessScreen extends BaseEnrolmentSuccessScreen {
    constructor() {
        super();
        // TODO MORE-540
        this.title = helper.byText(ViewClasses.TEXT_VIEW, 'Your device is now registered with Mobile Banking.');
        this.continueButton = helper.byResourceId(ViewClasses.BUTTON, 'continueButton');
    }
}

module.exports = EnrolmentSuccessScreen;
