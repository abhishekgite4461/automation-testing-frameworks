const BaseProductHubLandingLoanScreen = require('../../../productHub/productHubLandingLoan.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ProductHubLandingLoanScreen extends BaseProductHubLandingLoanScreen {
    constructor() {
        super();
        this.loanCalculatorText = helper.byText(ViewClasses.VIEW, 'Loans');

        this.loanCalculator = helper.byContentDesc(ViewClasses.VIEW, 'Loans');
    }
}

module.exports = ProductHubLandingLoanScreen;
