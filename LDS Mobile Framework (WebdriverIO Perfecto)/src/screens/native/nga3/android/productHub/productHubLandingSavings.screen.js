const BaseProductHubLandingSavingsScreen = require('../../../productHub/productHubLandingSavings.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ProductHubLandingSavingsScreen extends BaseProductHubLandingSavingsScreen {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.VIEW, 'Compare savings accounts - open an account today');
        this.compareSavings = helper.byText(ViewClasses.VIEW, 'Compare savings account');
        this.savingsAccountName = (accountName) => `${helper.byText(ViewClasses.VIEW,
            `${accountName}`)}`;
        this.select = helper.byText(ViewClasses.VIEW, 'Select link');
    }
}

module.exports = ProductHubLandingSavingsScreen;
