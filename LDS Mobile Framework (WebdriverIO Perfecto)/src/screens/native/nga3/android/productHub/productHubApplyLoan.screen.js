const BaseProductHubApplyLoanScreen = require('../../../productHub/productHubApplyLoan.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ProductHubApplyLoanScreen extends BaseProductHubApplyLoanScreen {
    constructor() {
        super();
        this.helloTitle = helper.byPartialText(ViewClasses.VIEW, 'Hello');
        this.continueLoanButton = helper.byPartialText(ViewClasses.VIEW, 'Continue');
        this.loanTitle = helper.byText(ViewClasses.VIEW, 'Get an instant loan quote');
        this.termField = helper.byPartialResourceIdWebView(ViewClasses.EDIT_TEXT, 'frmLoanCalculatorElig:txtELoanTerm');
        this.loanAmountField = helper.byPartialResourceIdWebView(ViewClasses.EDIT_TEXT, 'frmLoanCalculatorElig:txtLoanValue');
        this.selectLoanPurposeDropDown = helper.byResourceIdWebView(ViewClasses.SPINNER, 'frmLoanCalculatorElig:slctLoanPurpose');
        this.selectLoanPurposeOption = helper.byText(ViewClasses.CHECKED_TEXT_VIEW, 'Boat Purchase');
        this.repaymentOptionTitle = helper.byPartialText(ViewClasses.VIEW, 'Choose the option that suits you best from the list below');
        this.closestMatchOption = helper.byText(ViewClasses.BUTTON, 'Select loan');
        this.yourApplicationTitle = helper.byText(ViewClasses.VIEW, 'Your application');
        this.selectEmploymentStatus = helper.byResourceIdWebView(ViewClasses.SPINNER, 'frmTermsCondition:slctEmploymentStatus');
        this.selectEmploymentOption = helper.byText(ViewClasses.CHECKED_TEXT_VIEW, 'Retired');
        this.monthlyIncomeField = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'frmTermsCondition:monthlyIncomeCCR1');
        this.monthlyOutgoingField = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'frmTermsCondition:monthlymortgageCCR2');
        this.dependentsField = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'frmTermsCondition:financiallyDependentCCR4');
        this.childCareCostsField = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'frmTermsCondition:monthlyspendingsCCR3');
        this.agreementCheckboxOne = helper.byResourceIdWebView(ViewClasses.CHECK_BOX, 'frmTermsCondition:conductRisk');
        this.agreementCheckboxTwo = helper.byResourceIdWebView(ViewClasses.CHECK_BOX, 'frmTermsCondition:bchkbxAgreeTerms');
        this.agreementForStatements = helper.byResourceIdWebView(ViewClasses.CHECK_BOX, 'frmTermsCondition:paperLesscheckboxForELoan');
        this.checkEligibilityButton = helper.byResourceIdWebView(ViewClasses.BUTTON, 'frmTermsCondition:btnApplyForThisLoan');
        this.loanDeclinePageTitle = helper.byPartialText(ViewClasses.VIEW, 'Sorry');
        this.flexibleLoanButton = helper.byText(ViewClasses.BUTTON, 'Personal Loan');
        this.bookAppointmentOption = helper.byText(ViewClasses.VIEW, 'Book an Appointment');
        this.bookAppointment = helper.byResourceIdWebView(ViewClasses.BUTTON, 'rhc:appointment:onlineaptmtbooking:clickOAB');
        this.titleFindBranch = helper.byText(ViewClasses.VIEW, 'Find a branch');
        this.postcode = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'frmBranchFinder:txtSearchBranch');
        this.findBranch = helper.byResourceIdWebView(ViewClasses.BUTTON, 'frmBranchFinder:btnSearch');
        this.branchResults = (postCode) => `helper.byPartialText(ViewClasses.VIEW, ${postCode})`;
        this.continueFindBranch = helper.byPartialResourceIdWebView(ViewClasses.VIEW, 'frmOABBranchLocator:branchResults');
        this.titleAppointment = helper.byText(ViewClasses.VIEW, 'Choose an appointment time');
    }
}

module.exports = ProductHubApplyLoanScreen;
