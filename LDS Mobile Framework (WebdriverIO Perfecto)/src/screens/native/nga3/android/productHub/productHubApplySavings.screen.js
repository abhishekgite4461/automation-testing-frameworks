const BaseProductHubApplySavingsScreen = require('../../../productHub/productHubApplySavings.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ProductHubApplySavingsScreen extends BaseProductHubApplySavingsScreen {
    constructor() {
        super();
        this.applyNow = helper.byPartialText(ViewClasses.BUTTON, 'Apply now');
        this.acceptTC = '//android.widget.CheckBox';
        this.openAccount = helper.byText(ViewClasses.BUTTON, 'Open account');
        this.accountOpeningSuccessMessage = (accountName) => `//android.view.View[@text = 'Your ${accountName} 
        account is now open' or @text = 'Congratulations, you have just opened a ${accountName}']`;
        this.acctSortCodeandNumber = helper.byPartialText(ViewClasses.VIEW, 'Sort code:');
        this.accountNum = (accountNum) => helper.byText(ViewClasses.TEXT_VIEW, `${accountNum}`);
        this.acceptAll = helper.byResourceIdWebView(ViewClasses.BUTTON, 'accept');
    }
}

module.exports = ProductHubApplySavingsScreen;
