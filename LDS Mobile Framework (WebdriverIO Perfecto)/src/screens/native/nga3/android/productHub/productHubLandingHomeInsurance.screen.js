const BaseProductHubLandingHomeInsuranceScreen = require('../../../productHub/productHubLandingHomeInsurance.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ProductHubLandingHomeInsuranceScreen extends BaseProductHubLandingHomeInsuranceScreen {
    constructor() {
        super();
        this.homeInsuranceText = helper.byText(ViewClasses.VIEW, 'Home Insurance');

        this.homeInsurance = helper.byContentDesc(ViewClasses.VIEW, 'Home Insurance');
    }
}

module.exports = ProductHubLandingHomeInsuranceScreen;
