const BaseProductHubApplyOverdraftScreen = require('../../../productHub/productHubApplyOverdraft.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

// TODO DPLDA-175 Need to migrate into webview approach
class ProductHubApplyOverdraftScreen extends BaseProductHubApplyOverdraftScreen {
    constructor() {
        super();
        this.overdraftPopUpTitle = helper.byText(ViewClasses.VIEW, 'About overdrafts');
        this.overdraftPageTitle = helper.byText(ViewClasses.VIEW, 'Choose an amount');
        this.overdraftAmountValue = undefined;
        this.overdraftAmountField = '//android.widget.EditText[preceding-sibling::'
            + 'android.view.View[@text="Arranged overdraft limit you would like"]]';
        this.applyButton = helper.byText(ViewClasses.BUTTON, 'Apply');
        this.employementStatusSelector = helper.byText(ViewClasses.SPINNER, 'Please select');
        this.selectEmployment = helper.byText(ViewClasses.CHECKED_TEXT_VIEW, 'Retired');
        this.monthlyIncomeAfterTax = '(//android.view.View[preceding-sibling::'
            + 'android.view.View[contains(@text,"Your monthly take home pay")]]/android.widget.EditText)[1]';
        this.monthlyOutgoings = '(//android.view.View[preceding-sibling::'
            + 'android.view.View[contains(@text,"your mortgage, rent and/or board")]]/android.widget.EditText)[1]';
        this.childCareCost = '(//android.view.View[preceding-sibling::'
            + 'android.view.View[contains(@text,"towards childcare costs, maintenance and school")]]'
            + '/android.widget.EditText)[1]';
        this.lifeChangingEventField = helper.byText(ViewClasses.TOGGLE_BUTTON, 'No');
        this.overdraftDeclinePageTitle = helper.byText(ViewClasses.VIEW, 'Sorry your arranged overdraft '
            + 'application has been declined');
        this.continueButton = helper.byText(ViewClasses.BUTTON, 'Continue');
        this.aboutYouLabel = helper.byText(ViewClasses.VIEW, 'About you');
        this.employmentStatus = undefined;
        this.overdraftAmountLimitLabel = undefined;
        this.helpButton = helper.byText(ViewClasses.BUTTON, 'Need some help?');
        this.callDetailsLabel = helper.byText(ViewClasses.VIEW, 'If you need some help with your application you can call:');
        this.bookAppointmentLabel = helper.byPartialText(ViewClasses.VIEW, 'Or we can arrange an appointment with a colleague in your closest branch');
        this.findYourNearestBranchLink = helper.byText(ViewClasses.VIEW, 'find your nearest branch');
        this.fromOutsideUk = helper.byPartialText(ViewClasses.VIEW, 'From outside the UK,');
        this.ukNumber = (number) => `(${helper.byText(ViewClasses.VIEW, `${number}`)})[1]`;
        this.outsideUKNumber = (number) => `(${helper.byText(ViewClasses.VIEW, `${number}`)})[1]`;
        this.leaveAppTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.leaveAppOkButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.leaveAppCancelButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.overdraftPayBackButton = helper.byText(ViewClasses.TOGGLE_BUTTON, 'No');
        this.aboutOverdraftContinueButton = helper.byText(ViewClasses.BUTTON, 'Continue');
        this.aboutDigitalLabel = helper.byText(ViewClasses.VIEW, 'ABOUT DIGITAL');
        this.monthlyIncomeAfterTaxJoint = '(//android.view.View[preceding-sibling::'
            + 'android.view.View[contains(@text,"Their monthly take home pay and other allowed income")]]/android.widget.EditText)[1]';
        this.monthlyOutgoingsJoint = '(//android.view.View[preceding-sibling::'
            + 'android.view.View[contains(@text,"their mortgage, rent and/or cost")]]/android.widget.EditText)[1]';
        this.childCareCostJoint = '(//android.view.View[preceding-sibling::'
            + 'android.view.View[contains(@text,"towards childcare costs, maintenance and school")]]'
            + '/android.widget.EditText)[1]';
        this.letMeJustTypeLink = helper.byText(ViewClasses.VIEW, 'Let me just type it');
        this.overdraftAmountFieldNew = '(//android.view.View[preceding-sibling::'
            + 'android.view.View[contains(@text,"Arranged overdraft limit you would like")]]'
            + '/android.widget.EditText)[1]';
        this.overdraftDays = '(//android.view.View[preceding-sibling::'
            + 'android.view.View[contains(@text,"How many days do you think")]]'
            + '/android.widget.EditText)[1]';
    }
}

module.exports = ProductHubApplyOverdraftScreen;
