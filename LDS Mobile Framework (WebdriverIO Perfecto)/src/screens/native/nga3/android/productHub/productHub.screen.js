const Screen = require('../../../../screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ProductHubScreen extends Screen {
    constructor() {
        super();
        this.title = '//*[contains(@resource-id, "productHubList")]';
        this.loan = helper.byText(ViewClasses.TEXT_VIEW, 'Loans');

        this.expandIcon = (productName) => helper.byContentDesc(ViewClasses.LINEAR_LAYOUT,
            `${productName}. clicked to expand.`);
        this.collapseIcon = (productName) => helper.byContentDesc(ViewClasses.LINEAR_LAYOUT,
            `${productName}. clicked to collapse.`);

        this.featured = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Featured');

        this.businessCurrentAccount = helper.byText(ViewClasses.TEXT_VIEW, 'Business current account');
        this.businessSavingsAccount = helper.byText(ViewClasses.TEXT_VIEW, 'Business savings account');
        this.cards = helper.byText(ViewClasses.TEXT_VIEW, 'Cards');
        this.businessInsurance = helper.byText(ViewClasses.TEXT_VIEW, 'Business insurance');
        this.international = helper.byText(ViewClasses.TEXT_VIEW, 'International');
        this.businessGuidesAndInfo = helper.byText(ViewClasses.TEXT_VIEW, 'Business guides & info');
        this.businessLoan = helper.byText(ViewClasses.TEXT_VIEW, 'Business loan');
        this.featuredExpanded = helper.byContentDesc(ViewClasses.TEXT_VIEW, 'Featured. clicked to collapse.');
        this.businessCurrentAccountCollapsed = helper.byContentDesc(ViewClasses.TEXT_VIEW, 'Business current account. clicked to expand.');

        this.currentAccounts = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Current accounts');
        this.loans = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Loans');
        this.creditCards = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Credit cards');
        this.savings = helper.byText(ViewClasses.TEXT_VIEW, 'Savings');
        this.insurance = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Insurance');
        this.mortgages = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Mortgages');

        this.loanCalculator = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Loan calculator');

        this.compareAccounts = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Compare accounts');
        this.overdrafts = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Overdrafts');
        this.currentAccountOverdraft = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Overdrafts');
        this.overdraftsOptions = '//*[contains(@resource-id,"productHubChildTitle") and @text="Overdrafts"]';
        this.travelMoney = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Travel money');
        this.internationalPayments = helper.byPartialText(ViewClasses.TEXT_VIEW, 'International payment');

        this.calculator = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Calculator');
        this.borrowMore = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Borrow more');
        this.personalLoan = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Personal loan');
        this.carFinance = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Car finance');

        this.compareCreditCards = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Compare credit cards');
        this.balanceTransfer = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Balance transfer');

        this.compareSavingsAccounts = helper.byText(ViewClasses.TEXT_VIEW, 'Compare savings accounts');
        this.instantAccess = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Instant access');
        this.topUpISA = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Top up ISA');
        this.cashIsa = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Cash ISA');
        this.fixedTerm = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Fixed term');

        this.vanInsurance = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Van insurance');
        this.travelInsurance = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Travel insurance');
        this.homeInsurance = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Home insurance');
        this.carInsurance = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Car insurance');

        this.agreementInPrinciple = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Get agreement in principle');
        this.bookBranchAppointment = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Book a branch appointment');
        this.switchToNewDeal = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Switch to a new deal');
        this.otherMortgageOptions = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Other mortgage options');
        this.discoverAndApplyTitle = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Discover and apply');
        this.loansAndFinance = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Loans');
        this.shareDealing = helper.byText(ViewClasses.TEXT_VIEW, 'Share dealing');
        this.shareDealingOption = '//*[contains(@resource-id,"productHubChildTitle") and @text="Share dealing"]';
        this.marketsAndInsights = helper.byText(ViewClasses.TEXT_VIEW, 'Markets and Insights');
    }
}

module.exports = ProductHubScreen;
