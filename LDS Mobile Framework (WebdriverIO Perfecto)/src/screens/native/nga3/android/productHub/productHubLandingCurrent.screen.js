const BaseProductHubLandingCurrentScreen = require('../../../productHub/productHubLandingCurrent.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ProductHubLandingCurrentScreen extends BaseProductHubLandingCurrentScreen {
    constructor() {
        super();
        this.currentAccountOverdraftText = helper.byText(ViewClasses.VIEW, 'Overdrafts');

        this.currentAccountOverdraft = helper.byContentDesc(ViewClasses.VIEW, 'Overdrafts');
    }
}

module.exports = ProductHubLandingCurrentScreen;
