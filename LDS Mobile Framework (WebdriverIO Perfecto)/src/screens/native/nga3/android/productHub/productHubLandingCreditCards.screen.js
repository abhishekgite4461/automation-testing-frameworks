const BaseProductHubLandingCreditCardScreen = require('../../../productHub/productHubLandingCreditCard.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ProductHubLandingCreditCardScreen extends BaseProductHubLandingCreditCardScreen {
    constructor() {
        super();
        this.compareCreditCardText = helper.byText(ViewClasses.VIEW, 'Compare Credit Cards');

        this.compareCreditCard = helper.byContentDesc(ViewClasses.VIEW, 'Compare Credit Cards');
    }
}

module.exports = ProductHubLandingCreditCardScreen;
