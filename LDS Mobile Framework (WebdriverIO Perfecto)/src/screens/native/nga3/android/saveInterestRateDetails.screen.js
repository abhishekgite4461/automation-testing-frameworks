const BaseSavingInterestRateScreen = require('../../saveInterestRateDetails.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class SavingInterestRateScreen extends BaseSavingInterestRateScreen {
    constructor() {
        super();
        this.actionMenu = helper.byResourceId(ViewClasses.IMAGE_BUTTON, 'accountTileActionMenu');
        this.interestDetailRow = helper.byText(ViewClasses.TEXT_VIEW, 'View interest rate details');
        this.closeButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarCloseButton');
        this.accountName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'interestRateDetailAccountNameLabel');
        this.accountBalance = helper.byResourceId(ViewClasses.TEXT_VIEW, 'interestRateDetailBalanceLabel');
        this.interestRateScreen = helper.byResourceId(ViewClasses.VIEW, 'touch_outside');
        this.holdAccountTile = helper.byResourceId(ViewClasses.TEXT_VIEW, 'interestRateDetailInterestRateLabel'); // helper.byText(ViewClasses.TEXT_VIEW, 'Easy Saver');
        this.interestRateLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'interestRateDetailInterestRateLabel');
        this.interestRateDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'interestRateDetailDescriptionLabel');
        this.interestRateForFirstTier = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'interestRateDetailList0');
        this.interestRateDescriptionForFirstTier = `${helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'interestRateDetailList0')}
            ${helper.byResourceId(ViewClasses.TEXT_VIEW, 'interestRateDetailDescriptionLabel')}`;
        this.interestRateForSecondTier = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'interestRateDetailList1');
        this.interestRateForThirdTier = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'interestRateDetailList2');
        this.interestRateInfo = '//*[contains(@text,"% gross")]';
    }
}

module.exports = SavingInterestRateScreen;
