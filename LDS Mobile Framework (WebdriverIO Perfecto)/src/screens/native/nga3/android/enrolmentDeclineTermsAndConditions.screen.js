const BaseEnrolmentDeclineTermsAndConditionsScreen = require('../../enrolmentDeclineTermsAndConditions.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class EnrolmentDeclineTermsAndConditionsScreen extends BaseEnrolmentDeclineTermsAndConditionsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'termsAndConditionsWinbackTitle');
        this.confirmDeclineButton = helper.byResourceId(ViewClasses.BUTTON, 'termsAndConditionsWinbackDeclineButton');
        this.backButton = helper.byResourceId(ViewClasses.BUTTON, 'termsAndConditionsWinbackBackButton');
    }
}

module.exports = EnrolmentDeclineTermsAndConditionsScreen;
