const BaseNativeCallOverlayScreen = require('../../nativeCallOverlay.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class NativeCallOverlayScreen extends BaseNativeCallOverlayScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'permission_message');
    }
}

module.exports = NativeCallOverlayScreen;
