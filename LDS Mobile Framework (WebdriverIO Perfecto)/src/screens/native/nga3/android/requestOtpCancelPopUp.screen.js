const BaseRequestOtpCancelPopUpScreen = require('../../requestOtpCancelPopUp.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class RequestOtpCancelPopUpScreen extends BaseRequestOtpCancelPopUpScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'enrollmentCancelAlertDialog');
        this.cancelButton = helper.byText(ViewClasses.TEXT_VIEW, 'Yes');
        this.cancelButton = helper.byText(ViewClasses.TEXT_VIEW, 'Cancel');
    }
}

module.exports = RequestOtpCancelPopUpScreen;
