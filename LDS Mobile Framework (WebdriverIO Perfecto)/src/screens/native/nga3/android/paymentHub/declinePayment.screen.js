const BaseDeclinePaymentScreen = require('../../../declinePayment.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class DeclinePaymentScreen extends BaseDeclinePaymentScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenTitle');
        this.errorMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenMessage');
        this.preAuthButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'preAuthMenu');
        this.logonButton = helper.byResourceId(ViewClasses.BUTTON, 'errorScreenButtonPrimary');
        this.pageHeader = 'payment blocked';
    }
}

module.exports = DeclinePaymentScreen;
