const BaseSharePreviewScreen = require('../../../sharePreview.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class SharePreviewScreen extends BaseSharePreviewScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubReceiptTitle');
        this.paymentSentHeader = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubReceiptTitle');
        this.sharePreviewShareButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubSharePreviewShareButton');
        this.nativeSharePanel = helper.byPartialResourceId(ViewClasses.SCROLL_VIEW, 'contentPanel');
        this.payerName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle');
        this.beneficiaryName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryTitle');
        this.beneficiarySortCode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiarySortCode');
        this.beneficiaryAccountNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryAccountNumber');
        this.nameAndNumberPreview = helper.byPartialResourceId(ViewClasses.RELATIVE_LAYOUT, 'mobileView');
        this.bankIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'paymentHubBankIcon');
        this.paymentReceiptDateValue = (index) => `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentReceiptDateTextTile')}${helper.byText(ViewClasses.TEXT_VIEW, `${index}`)}`;
        this.paymentReceiptDateField = helper.byPartialResourceId(ViewClasses.FRAME_LAYOUT, 'paymentReceiptDateTextTile');
        this.emailAppIcon = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Gmail');
        this.displayAccountDetails = helper.byPartialText(ViewClasses.EDIT_TEXT, 'Hi. Here are my account details.');
        this.referenceValue = helper.byResourceIdAndIndex(ViewClasses.TEXT_VIEW, 'infoTextTileTitle', '2');
        this.amountValue = helper.byResourceIdAndIndex(ViewClasses.TEXT_VIEW, 'infoTextTileTitle', '1');
    }
}

module.exports = SharePreviewScreen;
