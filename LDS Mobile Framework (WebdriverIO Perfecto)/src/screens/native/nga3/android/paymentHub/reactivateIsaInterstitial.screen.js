const BaseReactivateIsaInterstitialScreen = require('../../../reactivateIsaInterstitial.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ReactivateIsaInterstitialScreen extends BaseReactivateIsaInterstitialScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'reactivateIsaHeader');
        this.reactivateIsaButton = helper.byResourceId(ViewClasses.BUTTON, 'reactivateIsaDeclarationButton');
    }
}

module.exports = ReactivateIsaInterstitialScreen;
