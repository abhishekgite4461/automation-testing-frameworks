/**
 * Created by a9883098 on 23/01/2018.
 */
const BaseCalendarScreen = require('../../../calendar.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class CalendarScreen extends BaseCalendarScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'calendarInformationText');
        this.nextMonthButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'nextMonthButton');
        this.prevMonthButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'previousMonthButton');
        this.lastDayOfPayment = (index) => `${helper.byText(ViewClasses.TEXT_VIEW, `${index}`)}
        /parent::android.widget.LinearLayout`;
        this.monthName = (index) => `${helper.byText(ViewClasses.TEXT_VIEW, `${index}`)}`;
        this.cancel = helper.byText(ViewClasses.BUTTON, 'Cancel');
    }
}

module.exports = CalendarScreen;
