const BaseupComingPaymentScreen = require('../../../upComingPayment.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class UpComingPaymentScreen extends BaseupComingPaymentScreen {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.VIEW, 'Upcoming payments');
        this.upComingPayment = (transactionType, account, amount) => `//*[contains(@text, 
            ${account.toUpperCase()} ${transactionType} £${amount}')]`;
    }
}

module.exports = UpComingPaymentScreen;
