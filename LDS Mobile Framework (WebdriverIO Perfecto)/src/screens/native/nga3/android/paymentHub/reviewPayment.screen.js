const BaseReviewPaymentScreen = require('../../../reviewPayment.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ReviewPaymentScreen extends BaseReviewPaymentScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubReviewTitle');
        this.editButton = helper.byResourceId(ViewClasses.BUTTON, 'editPaymentReviewButton');
        this.confirmButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubReviewConfirmButton');
        this.fromAccountNameDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryFromArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle')}`;
        this.fromSortCodeDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryFromArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSortCode')}`;
        this.fromAccountNumberDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryFromArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber')}`;
        this.toAccountNameDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryToBeneficiaryView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryTitle')}`;
        this.toSortCodeDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryToBeneficiaryView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiarySortCode')}`;
        this.toAccountNumberDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryToBeneficiaryView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryAccountNumber')}`;
        this.toCreditCardNumberDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryToArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementReference')}`;
        this.toMortgageAccountDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperToPlaceholder')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementReference')}`;
        this.remitterAmount = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperFromPlaceholder')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementBalanceComments')}`;
        this.amountDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryAmountInfoTextTile')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'infoTextTileTitle')}`;
        this.withdrawFromIsaToNonLbgAccountWarningMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubReviewPrimaryInfoText');
        this.isaTransferAgreementCheckBox = helper.byResourceId(ViewClasses.CHECK_BOX, 'paymentHubReviewWarningConfirmationCheckBox');
        this.dateValue = (index) => `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryWhenInfoTextTile')}${helper.byText(ViewClasses.TEXT_VIEW, `${index}`)}`;
        this.futurePaymentMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'the next working day');
        this.detailContainer = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubSuccessPaymentSummary');
        this.toP2PAccountNameDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryToMobileNumberView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryTitle')}`;
        this.toMobileNum = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryToMobileNumberView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryPhoneNumber')}`;
        this.mpaPaymentApprovalRequiredMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Payment will be submitted for approval');
        this.subAccountInfo = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummarySubAccountInfoTextTile')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'infoTextTileTitle')}`;
        this.reference = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryReferenceInfoTextTile')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'infoTextTileTitle')}`;
    }
}

module.exports = ReviewPaymentScreen;
