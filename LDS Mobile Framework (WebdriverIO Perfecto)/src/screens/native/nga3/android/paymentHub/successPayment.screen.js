const BaseSuccessPaymentScreen = require('../../../successPayment.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class SuccessPaymentScreen extends BaseSuccessPaymentScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubSuccessTitle');
        this.viewTransactionButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubSuccessViewTransactionsButton');
        this.makeAnotherPaymentButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubSuccessAnotherPaymentButton');
        this.paymentHubSuccessPaymentSummary = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubSuccessPaymentSummary');
        this.fromAccountNameDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryFromArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle')}`;
        this.fromSortCodeDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryFromArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSortCode')}`;
        this.fromAccountNumberDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryFromArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber')}`;
        this.toAccountNameDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryToArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle')}`;
        this.toSortCodeDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryToArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSortCode')}`;
        this.toAccountNumberDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryToArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber')}`;
        this.toCreditCardNumberDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryToArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementReference')}`;
        this.amountDetail = helper.byResourceId(ViewClasses.TEXT_VIEW, 'infoTextTileTitle');
        this.amount = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryAmountInfoTextTile')}
        ${helper.byResourceId(ViewClasses.TEXT_VIEW, 'infoTextTileTitle')}`;
        this.referencePane = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryReferenceInfoTextTile');
        this.reference = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryReferenceInfoTextTile')}
        ${helper.byResourceId(ViewClasses.TEXT_VIEW, 'infoTextTileTitle')}`;
        this.dateValue = (index) => `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryWhenInfoTextTile')}${helper.byText(ViewClasses.TEXT_VIEW, `${index}`)}`;
        this.viewStandingOrders = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubSuccessStandingOrderPaymentButton');
        this.notNow = helper.byResourceId(ViewClasses.BUTTON, 'dialogNeutralAction');
        this.mpaPaymentApprovalSuccessMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Payment submitted for approval');
        this.shareReceiptButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubSuccessShareTransactionButton');
        this.fromBusinessName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSecondTitle');
        this.paymentScheduledTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationToolbarText'); // PAYMOB-2156 raised to add ID property.
        this.paymentProcessed = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubSuccessMessage');
        this.bannerLeadPlacement = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'transactionSuccessBannerLeadContainer');
    }
}

module.exports = SuccessPaymentScreen;
