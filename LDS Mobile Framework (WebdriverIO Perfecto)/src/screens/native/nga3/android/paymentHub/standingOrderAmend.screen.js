const BaseStandingOrderAmendScreen = require('../../../standingOrderAmend.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class StandingOrderAmendScreen extends BaseStandingOrderAmendScreen {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.VIEW, 'Confirm standing order amendments');
        this.soReference = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'frmAmendStandingOrder:strReferenceValue');
        this.soAmount = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'frmAmendStandingOrder:crRegularAmount');
        this.soPassword = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'frmConfirmAmendSO:password');
        this.soAmendButton = helper.byResourceIdWebView(ViewClasses.BUTTON, 'frmAmendStandingOrder:lnkAmendSO');
        this.soConfirmAmendButton = helper.byResourceIdWebView(ViewClasses.BUTTON, 'frmConfirmAmendSO:lnkAmendConfirmSO');
        this.soChangeSucessTitle = helper.byText(ViewClasses.VIEW, 'Standing order changed');
        this.soChangeSucessMessage = helper.byText(ViewClasses.VIEW, 'Your standing order has been successfully changed.');
        this.soViewStandingOrdersButton = helper.byResourceIdWebView(ViewClasses.VIEW, 'viewSODD');
    }
}

module.exports = StandingOrderAmendScreen;
