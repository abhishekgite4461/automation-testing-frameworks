const BaseFailurePaymentScreen = require('../../../failurePayment.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class FailurePaymentScreen extends BaseFailurePaymentScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'fragmentTransactionFailureScreenTitle');
        this.internetErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, "your Internet connection has been lost so we can't confirm this payment");
        this.pendingPaymentErrorMessage = '//android.widget.TextView[contains(@text,"Sorry, we can’t complete this payment") and contains(@text,"a payment pending")]';
        this.transferAndPaymentButton = helper.byResourceId(ViewClasses.BUTTON, 'fragmentTransactionFailurePrimaryButton');
    }
}

module.exports = FailurePaymentScreen;
