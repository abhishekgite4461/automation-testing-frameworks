const BaseStandingOrderReviewScreen = require('../../../transferAndPayments/standingOrderReview.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class StandingOrderReviewScreen extends BaseStandingOrderReviewScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubReviewTitle');
        this.reviewPaymentEditButton = helper.byResourceId(ViewClasses.BUTTON, 'editPaymentReviewButton');
        this.reviewPaymentConfirmButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubReviewConfirmButton');
        this.navigationBarBackButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarBackButton');
        this.remitterName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle');
        this.remitterSortCode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSortCode');
        this.remitterAccountNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber');
        this.benefeciaryName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryTitle');
        this.benefeciarySortCode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiarySortCode');
        this.benefeciaryAccountNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryAccountNumber');
        this.amount = "//*[contains(@resource-id,'paymentSummaryAmountInfoTextTile')]/descendant::*[contains(@resource-id,'infoTextTileTitle')]";
        this.reference = "//*[contains(@resource-id,'paymentSummaryReferenceInfoTextTile')]/descendant::*[contains(@resource-id,'infoTextTileTitle')]";
        this.standingOrderDates = "//*[contains(@resource-id,'paymentSummaryWhenInfoTextTile')]/descendant::*[contains(@resource-id,'infoTextTileTitle')]";
        this.frequency = "//*[contains(@resource-id,'paymentSummaryFrequencyTextTile')]/descendant::*[contains(@resource-id,'infoTextTileTitle')]";
    }
}

module.exports = StandingOrderReviewScreen;
