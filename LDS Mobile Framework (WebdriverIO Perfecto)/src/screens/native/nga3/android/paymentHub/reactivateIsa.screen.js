const BaseReactivateIsaScreen = require('../../../reactivateIsa.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ReactivateIsaScreen extends BaseReactivateIsaScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'reactivateIsaHeader');
        this.reactivateIsaHeader = helper.byResourceId('reactivateIsaHeader');
        this.reactivateIsaInformation = helper.byResourceId(ViewClasses.TEXT_VIEW, 'reactivateIsaInformation');
        // TODO: MOB3-12585 IOS and android we don't have proper IDs for reactivateISA
        this.reactivateIsaAccountNameText = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Account:');
        this.reactivateIsaAccountSortCode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSortCode');
        this.reactivateIsaAccountNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber');
        this.reactivateIsaAccountHolderName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'reactivateIsaAccountHolderName');
        this.reactivateIsaAccountHolderAddress = helper.byResourceId(ViewClasses.TEXT_VIEW, 'reactivateIsaAccountHolderAddress');
        this.reactivateIsaAccountHolderDateOfBirth = helper.byResourceId(ViewClasses.TEXT_VIEW, 'reactivateIsaAccountHolderDateOfBirth');
        this.reactivateIsaNationalInsuranceNo = helper.byPartialText(ViewClasses.TEXT_VIEW, 'National Insurance No.:');
        this.reactivateIsaUpdateAddressLinkText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'reactivateIsaUpdateAddressLinkTextView');
        this.reactivateIsaUpdateNiNumberLinkText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'reactivateIsaUpdateNiNumberLinkTextView');
        this.reactivateIsaConfirmButton = helper.byResourceId(ViewClasses.BUTTON, 'reactivateIsaConfirmButton');
        this.reactivateIsaCancelButton = helper.byResourceId(ViewClasses.BUTTON, 'reactivateIsaCancelButton');
        this.reactivateAgreementCheckBox = helper.byResourceId(ViewClasses.CHECK_BOX, 'checkBoxAgreement');
        this.reactivateIsaEligibilityCriteriaWebView = helper.byContentDesc(ViewClasses.WEB_VIEW, 'ISA_Declaration');
        this.reactivateIsaUpdateNiModal = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'reactivateIsaUpdateNiModal');
        this.reactivateCloseButton = helper.byResourceId(ViewClasses.BUTTON, 'dialogNeutralAction');
        this.leaveThisPageDialogBox = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'reactivateIsaLeaveDeclarationModal');
        this.addUkAccountWinbackModal = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'addUkAccountWinbackModal');
        this.dialogNegativeAction = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.dialogPositiveAction = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.settingsPersonalDetailsAddressTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsAddressTitle');
        this.settingsPersonalDetailsAddressLineOne = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsAddressLineOne');
        this.settingsPersonalDetailsPostcode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsPostcode');
        this.settingsPersonalDetailsChangeAddressDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsChangeAddressDescription');
        this.settingsPersonalDetailsChangeAddressButton = helper.byResourceId(ViewClasses.BUTTON, 'settingsPersonalDetailsChangeAddressButton');
        this.passwordConfirmationDialogPanel = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'passwordConfirmationDialog');
        this.passwordConfirmationDialogPasswordBox = helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText');
        this.passwordConfirmationDialogForgotPasswordLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'passwordConfirmationDialogForgotPasswordLabel');
    }
}

module.exports = ReactivateIsaScreen;
