const BaseViewRecipientScreen = require('../../../viewRecipient.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ViewRecipientScreen extends BaseViewRecipientScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.EDIT_TEXT, 'baseSearchBarEditText');
        this.accountNameLabel = (account) => `//*[contains(@text,"${account}") and not(
    following-sibling::*[@text="Payment due soon"])]`;
        this.accountLabel = (account) => `//*[contains(@text,"${account}") and not(
    following-sibling::*[@text="Payment due soon"])]`;
        this.internationalPayment = (accountNumber) => helper.byText(ViewClasses.TEXT_VIEW, accountNumber);
        this.internationalPaymentWithField = helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryIbanField');
        this.recipientAccount = '(//android.widget.LinearLayout[contains(@resource-id,":id/beneficiaryAccountDetail") and not(following-sibling::android.widget.TextView[contains(@text,"Payment due soon")])])[1]';
        this.firstRecipientTransferCell = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle');
        this.externalBeneficiaryAccounts = helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryTitle');
        this.myAccounts = helper.byPartialText(ViewClasses.TEXT_VIEW, 'accounts');
        this.zeroRemainingAllowance = (account) => `//android.widget.TextView[@text='${account}']
    /..//android.widget.TextView[contains(@text,"Remaining allowance: £0.00")]`;
        this.manageButton = "(//android.widget.TextView[contains(@resource-id,'beneficiaryManageButton') and not(parent::*/android.widget.RelativeLayout[contains(@resource-id,'beneficiaryDetailsWrapper')]/android.widget.TextView[@text='Payment due soon'])])[1]";
        this.recipientList = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'paymentHubRecipientsList');
        this.pendingPayment = helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryPendingPayment');
        this.manageButtonPendingPayment = `${helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryPendingPayment')}/../following::android.widget.TextView[1]`;
        this.manageButtonUKAccountNumber = "(//android.widget.TextView[@text='Manage' and not(parent::android.widget.RelativeLayout[@content-desc='viewRecipientUkBankAccount']/android.widget.RelativeLayout/android.widget.TextView[contains(@text,'Payment due soon')])])[1]";
        this.manageButtonMobileNumber = `${helper.byContentDesc(ViewClasses.RELATIVE_LAYOUT, 'viewRecipientPayByMobileNumber')}${helper.byText(ViewClasses.TEXT_VIEW, 'Manage')}`;
        this.manageButtonRecipientName = `${helper.byPartialText(ViewClasses.TEXT_VIEW, 'DELETE')}/..//*[contains(@text,'Manage')]`;
    }
}

module.exports = ViewRecipientScreen;
