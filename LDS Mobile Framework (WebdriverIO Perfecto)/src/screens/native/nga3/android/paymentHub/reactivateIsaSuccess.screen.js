const BaseReactivateIsaSuccessScreen = require('../../../reactivateIsaSuccess.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ReactivateIsaSuccessScreen extends BaseReactivateIsaSuccessScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'reactivateIsaSuccessHeader');
        this.reactivateIsaSuccessHeader = helper.byResourceId(ViewClasses.TEXT_VIEW, 'reactivateIsaSuccessHeader');
        this.reactivateIsaSupportingCopy = helper.byResourceId(ViewClasses.TEXT_VIEW, 'reactivateIsaSupportingCopy');
        this.reactivateIsaRemainingAllowanceText = helper.byText(ViewClasses.TEXT_VIEW, 'Remaining allowance:');
        this.reactivateIsaRemainingAllowanceAmount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'reactivateIsaRemainingAllowanceAmount');
        this.reactivateIsaAddMoreMoneyButton = helper.byResourceId(ViewClasses.BUTTON, 'reactivateIsaAddMoreMoneyButton');
        this.reactivateIsaAccountNameText = helper.byText(ViewClasses.TEXT_VIEW, 'Account:');
        this.reactivateIsaAccountSortCode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSortCode');
        this.reactivateIsaAccountNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber');
        this.reactivateIsaToVerifyExternalTransferButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber');
        this.reactivateIsaInstructionalMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber');
    }
}

module.exports = ReactivateIsaSuccessScreen;
