const BaseReviewTransferScreen = require('../../../reviewTransfer.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ReviewTransferScreen extends BaseReviewTransferScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubReviewTitle');
        this.editButton = helper.byResourceId(ViewClasses.BUTTON, 'editPaymentReviewButton');
        this.confirmButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubReviewConfirmButton');
        this.fromAccountNameDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryFromArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle')}`;
        this.fromSortCodeDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryFromArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSortCode')}`;
        this.fromAccountNumberDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryFromArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber')}`;
        this.toAccountNameDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryToArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle')}`;
        this.toSortCodeDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryToArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSortCode')}`;
        this.toAccountNumberDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryToArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber')}`;
        this.remitterAmount = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperFromPlaceholder')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementBalanceComments')}`;
        this.amountDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryAmountInfoTextTile')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'infoTextTileTitle')}`;
        this.reference = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryReferenceInfoTextTile')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'infoTextTileTitle')}`;
        this.htbIsaWarningMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'initial deposit of up to £1,000 within 21 calendar days of opening your account');
        this.topupIsaWarningMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'to this ISA in this tax year.');
        this.withdrawFromIsaToNonIsaAccountWarningMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 're attempting to transfer money out of your tax-free ISA account');
        this.withdrawFromIsaToIsaAccountWarningMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 're attempting to move money from one ISA to another ISA');
        this.mpaTransferApprovalRequiredMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Further approval is needed to continue');
    }
}

module.exports = ReviewTransferScreen;
