const BaseFailureTransferScreenn = require('../../../failureTransfer.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class FailureTransferScreen extends BaseFailureTransferScreenn {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'fragmentTransactionFailureScreenTitle');
        this.internetErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'your Internet connection has been lost');
        this.transferAndPaymentButton = helper.byResourceId(ViewClasses.BUTTON, 'fragmentTransactionFailurePrimaryButton');
        this.isaTransferTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'fragmentTransactionFailureScreenTitle');
        this.moreThanFundingLimitHTBTransferTitle = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'errorScreenIcon');
        this.isaAlreadySubscribedErrorMessage = '//android.widget.TextView[contains(@text,"You can\'t make this transaction because you\'ve already subscribed to") and contains(@text,"ISA in the current tax year")]';
        this.htbIsaAboveFundLimitErrorMessage = '//android.widget.TextView[contains(@text,"make more than one deposit in a calendar month") and contains(@text,"to deposit more than £200")]';
        this.homeButton = helper.byResourceId(ViewClasses.BUTTON, 'errorScreenButtonPrimary');
    }
}

module.exports = FailureTransferScreen;
