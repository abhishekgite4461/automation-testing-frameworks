const BaseStandingOrderAmendScreen = require('../../../standingOrderDelete.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class StandingOrderScreen extends BaseStandingOrderAmendScreen {
    constructor() {
        super();
        this.soDeleteTitle = helper.byText(ViewClasses.VIEW, 'Delete standing order');
        this.soDeletePassword = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'frmCancelSO:password');
        this.soDeleteSOButton = helper.byResourceIdWebView(ViewClasses.BUTTON, 'frmCancelSO:btnCancelSO');
        this.soDeleteMessageTitle = helper.byPartialText(ViewClasses.VIEW, 'Deleted standing order');
        this.soDeleteMessageTitleBnga = helper.byPartialText(ViewClasses.VIEW, 'has been deleted.');
        this.soDeleteSOButtonBnga = helper.byResourceIdWebView(ViewClasses.VIEW, 'frmCancelCommSO:lnkCancelSO');
    }
}

module.exports = StandingOrderScreen;
