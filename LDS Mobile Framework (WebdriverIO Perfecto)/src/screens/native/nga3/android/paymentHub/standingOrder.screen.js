const BaseStandingOrderScreen = require('../../../standingOrder.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class StandingOrderScreen extends BaseStandingOrderScreen {
    constructor() {
        super();
        this.standingOrderTab = helper.byText(ViewClasses.VIEW, 'Standing orders');
        this.directDebitTab = helper.byResourceIdWebView(ViewClasses.VIEW, 'pnlTabSO2');
        this.amendButton = helper.byResourceIdWebView(ViewClasses.VIEW, 'msgTableSO:0:amendlnk');
        this.deleteButton = helper.byResourceIdWebView(ViewClasses.VIEW, 'msgTableSO:0:deletelnk');
        this.savingAccountName = helper.byText(ViewClasses.VIEW, 'INST ACCESS ONLINE');
        this.dueDate = helper.byText(ViewClasses.VIEW, 'Next due:');
        this.reference = helper.byText(ViewClasses.VIEW, 'Ref:');
        this.paidTo = helper.byText(ViewClasses.VIEW, 'Paid to:');
        this.underReviewSoPage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubReviewTitle');
        this.selectViewStandingOrderOption = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubReviewPrimaryButton');
        this.declineSoPage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'standingOrderDeclinedErrorTitle');
        this.exitInternetBanking = helper.byResourceId(ViewClasses.BUTTON, 'standingOrderDeclinedExitButton');
    }
}

module.exports = StandingOrderScreen;
