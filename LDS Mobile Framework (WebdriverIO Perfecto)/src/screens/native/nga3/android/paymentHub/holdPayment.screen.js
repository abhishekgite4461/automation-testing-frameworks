const BaseHoldPaymentScreen = require('../../../holdPayment.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class HoldPaymentScreen extends BaseHoldPaymentScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubReviewTitle');
        this.errorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'your security, we need to speak to you about this payment before');
        this.transferAndPaymentButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubFailurePrimaryButton');
        this.paymentHoldErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'For your security, we need to speak to you about this payment');
        this.callUsOption = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubReviewPrimaryButton');
    }
}

module.exports = HoldPaymentScreen;
