const BaseViewRemitterScreen = require('../../../viewRemitter.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ViewRemitterScreen extends BaseViewRemitterScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSummaryTitle');
        this.titleBNGA = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'navigationToolbarBackButtonContainer');
        this.accountName = `${helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'arrangementView')}/android.widget.TextView`;
        this.accountNameLabel = (account) => helper.byPartialText(ViewClasses.TEXT_VIEW, account);
        this.aggAccount = (accountSortCode, accountNumber) => `//android.widget.TextView[@text='${accountSortCode}']
            /following-sibling::android.widget.TextView[@text='${accountNumber}']`;
        this.searchedBusinessAccount = (businessName, accName) => `//*[contains(@text,"${businessName}") and 
        (following-sibling::android.widget.TextView[contains(@text,"${accName}")])]`;
    }
}

module.exports = ViewRemitterScreen;
