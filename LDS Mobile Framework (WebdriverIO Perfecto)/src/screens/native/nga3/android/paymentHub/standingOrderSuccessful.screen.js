const BaseStandingOrderReviewScreen = require('../../../transferAndPayments/standingOrderSuccessful.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class StandingOrderSuccessfulScreen extends BaseStandingOrderReviewScreen {
    constructor() {
        super();
        this.standingOrderSuccessHeader = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubSuccessTitle');
        this.viewStandingOrdersButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubSuccessStandingOrderPaymentButton');
        this.soNotSetupErrorMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'fragmentTransactionFailureScreenTitle');
    }
}

module.exports = StandingOrderSuccessfulScreen;
