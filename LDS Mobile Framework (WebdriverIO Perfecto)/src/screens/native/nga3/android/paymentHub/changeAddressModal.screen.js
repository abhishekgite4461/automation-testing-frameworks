const BaseReactivateIsaSuccessScreen = require('../../../reactivateIsaSuccess.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ChangeAddressModal extends BaseReactivateIsaSuccessScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'callUsScreenTitle');
    }
}

module.exports = ChangeAddressModal;
