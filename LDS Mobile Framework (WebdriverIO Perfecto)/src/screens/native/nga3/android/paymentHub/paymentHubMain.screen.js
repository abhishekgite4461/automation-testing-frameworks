const BasePaymentHubMainScreen = require('../../../paymentHubMain.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class PaymentHubMainScreen extends BasePaymentHubMainScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperFromPlaceholder');
        this.fromAccountPane = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperFromPlaceholder');
        this.toAccountPane = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperToPlaceholder');
        this.fromAccountName = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperFromPlaceholder')}
        ${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle')}`;
        this.toAccountName = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperToPlaceholder')}
        ${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle')}`;
        this.p2pToAccountNameDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperToPlaceholder')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryTitle')}`;
        this.toAccount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementUnselectedTitle');
        this.amount = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubAmountAmountInputField')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.selectFrequencyDropdown = helper.byResourceId(ViewClasses.TEXT_VIEW, 'titledPickerViewItemTextView');
        this.toolTipForCorporatePayee = helper.byResourceId(ViewClasses.TEXT_VIEW, 'inputFieldTipMessage');
        this.continueButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubContinueButton');
        this.fasterPaymetWarning = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubUnselectedErrorTitle');
        this.fasterPaymetWarningPopUp = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'paymentFasterPaymentErrorDialogId');
        this.statusLink = helper.byPartialText(ViewClasses.TEXT_VIEW, 'our service status page');
        this.noticeWarningMessage = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'transferNoticeAccountWarningModal');
        this.remitterAmount = (appType) => {
            const index = appType === 'RNGA' ? '1' : '2';
            return `(${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperFromPlaceholder')}${helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'arrangementBalance')})[${index}]`;
        };
        this.overDraftAmount = (appType) => {
            const index = appType === 'RNGA' ? '1' : '2';
            return `(${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperFromPlaceholder')}${helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'arrangementBalanceComments')})[${index}]`;
        };
        this.aggRemitterAmount = '(//*[contains(@resource-id,"arrangementAccountBalanceDetail")]/*[not (contains(@resource-id,"Comments"))])[1]';
        this.recipientAmount = (appType) => {
            const index = appType === 'RNGA' ? '1' : '2';
            return `(${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperToPlaceholder')}${helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'arrangementBalance')})[${index}]`;
        };
        this.insufficientMoneyErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'enough money');
        this.closeErrorMessage = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'notificationCloseImageView');
        this.fromAccountNameDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperFromPlaceholder')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle')}`;
        this.fromSortCodeDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperFromPlaceholder')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSortCode')}`;
        this.fromAccountNumberDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperFromPlaceholder')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber')}`;
        this.toAccountNameDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperToPlaceholder')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle')}`;
        this.toSortCodeDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperToPlaceholder')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSortCode')}`;
        this.toAccountNumberDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperToPlaceholder')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber')}`;
        this.toCreditCardNumberDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperToPlaceholder')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementReference')}`;
        this.paymentAccountNameDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperToPlaceholder')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryTitle')}`;
        this.paymentSortCodeDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperToPlaceholder')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiarySortCode')}`;
        this.paymentAccountNumberDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperToPlaceholder')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryAccountNumber')}`;
        this.referenceDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubReferenceInputWithHintField')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'inputFieldEditText')}`;
        this.date = helper.byResourceId(ViewClasses.TEXT_VIEW, 'calendarPickerViewDateTextView');
        this.paymentDate = (index) => `${helper.byText(ViewClasses.TEXT_VIEW, `${index}`)}`;
        this.calendarButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'calendarPickerViewImageView');
        this.additionalOptionCreditCard = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'inputFieldIconRight');
        this.statementBalanceCreditCard = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubAmountToPayFirstAction');
        this.minimumAmountCreditCard = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubAmountToPaySecondAction');
        this.paymentAccountLocator = (label) => `//*[contains(@resource-id,"arrangementTitle") and @text='${label}']`;
        this.headerHomeButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarBackButton');
        this.beneficiaryAccountDetail = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'beneficiaryAccountDetail');
        this.errorMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenMessage');
        this.reference = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubReferenceInputWithHintField')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.toolTipForMinimumAndMaximumDailyLimit = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubAmountInputFieldTipView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'inputFieldTipMessage')}`;
        this.toolTipForReferenceOnPaymentHub = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubReferenceInputFieldTipView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'inputFieldTipMessage')}`;
        this.errorMessageForMinimumAndMaximumDailyLimit = helper.byResourceId(ViewClasses.TEXT_VIEW, 'notificationContentTextView');
        this.internetTransactionLimitMessage = '//android.widget.TextView[contains(@text,"is more than you can transfer online") '
            + 'or contains(@text,"is more than the online transfer limit")or contains(@text,"is more than the transfer transaction limit") ]';
        this.accountDailyLimitMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'this amount is more than the online daily limit for this account');
        this.cbsProhibitiveMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Sorry, you can\'t complete this request online. Please call us on');
        this.monthlyCapLimitMessage = '//android.widget.TextView[contains(@text,"payment exceeds the monthly limit") or contains(@text,"payment is more than the monthly amount")]';
        this.standingOrderToggleSwitch = helper.byResourceId(ViewClasses.SWITCH, 'paymentHubStandingOrderSwitch');
        this.standingOrderFrequency = (frequency) => helper.byText(ViewClasses.TEXT_VIEW, frequency);
        this.minISATransferErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Sorry, this is less than the minimum amount you can transfer into your ISA. Please transfer a higher');
        this.maxISATransferErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Transferring this amount would either exceed your remaining ISA allowance');
        this.frequencyPicker = helper.byResourceId(ViewClasses.TEXT_VIEW, 'titledPickerViewItemTextView');
        this.frequencySelector = (frequencyType) => helper.byText(ViewClasses.TEXT_VIEW, frequencyType);
        this.paymentHubStandingOrderFirstPaymentDate = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubStandingOrderFirstPayment')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'calendarPickerViewDateTextView')}`;
        this.paymentHubStandingOrderLastPaymentDate = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubStandingOrderLastPayment')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'calendarPickerViewDateTextView')}`;
        this.nextMonthButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'nextMonthButton');
        this.dateSelector = (date) => helper.byText(ViewClasses.TEXT_VIEW, date);
        this.monthYearPicker = helper.byResourceId(ViewClasses.TEXT_VIEW, 'headerTitle');
        this.cancelCalendar = helper.byText(ViewClasses.BUTTON, 'Cancel');
        this.yearPickerWheelOnly = (year) => `//android.widget.NumberPicker[2]/android.widget.Button
            [contains(@text,"${year}")]`;
        this.standingOrderReference = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubReferenceInputWithHintField')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.monthPickerWheel = '//android.widget.NumberPicker[1]/android.widget.Button[2]';
        this.monthYearSelectButton = helper.byResourceId(ViewClasses.BUTTON, 'monthYearPickerSelectButton');
        this.yearPickerWheel = '//android.widget.NumberPicker[1]/android.widget.Button[2]';
        this.warningMessageTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.warningMessageContent = helper.byPartialContentDesc(ViewClasses.TEXT_VIEW, 'standing order');
        this.warningMessageContentHTB = helper.byPartialContentDesc(ViewClasses.TEXT_VIEW, 'You can make an initial');
        this.statementBalanceWithInstalmentPlanCreditCard = helper.byText(ViewClasses.TEXT_VIEW, 'MINIMUM + INSTALMENT PLAN PAYMENT:');
        this.minimumAmountWithInstalmentPlanCreditCard = helper.byText(ViewClasses.TEXT_VIEW, 'MAIN BALANCE + INSTALMENT PLAN PAYMENT:');
        this.toP2PAccountNameDetail = helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryTitle');
        this.toMobileNum = helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryPhoneNumber');
        this.paymentHubRecipientsList = '//*[contains(@resource-id,":id/paymentHubRecipientsList")]';
        this.paymentHubRemitanceList = '//*[contains(@resource-id,":id/arrangementSummaryList")]';
        this.closeIcon = helper.byResourceId(ViewClasses.TEXT_VIEW, 'overlayToolbarCloseIcon');
        this.internetLostMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Sorry, you are not currently connected to the Internet');
        this.fraudsterWarning = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Are you sure you know where your money is going?');
        this.timeLapseWarningErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'For security please wait until 60 minutes');
        this.timeLapseWithOverPaymentWarningErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'before making a payment over £1000 to a new recipient.');
        this.paymentsHubUnavailable = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenTitle');
        this.creditCardAmount = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperToPlaceholder')}${helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'arrangementBalance')}`;
        this.creditCardValidationErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'credit card account greater than the outstanding balance');
        this.clickFrom = helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryAccountTileHeader');
        this.fromAccountList = helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumb');
        this.fromExternalAccount = (extProvider, extAccount) => `//android.widget.TextView[contains(@content-desc,
            '${extProvider}')]/..//android.widget.TextView[contains(@text,'${extAccount}')]`;
        this.clickTo = helper.byText(ViewClasses.TEXT_VIEW, 'To:');
        this.informationAboutOverpaymentPopup = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'mortgageOverpaymentMoreInfoDialogId');
        this.learnAboutOverpaymentToolTip = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mortgagePaymentMoreInfoTextView');
        this.subAccountToolTip = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mortgagePaymentSubAccountInfoTextView');
        this.subAccountList = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'mortgageSubaccountRadioGroup');
        this.subAccountToggle = helper.byResourceId(ViewClasses.SWITCH, 'paymentHubSubAccountSwitch');
        this.selectToAccount = (toExtProvider, toAggAccount) => `//android.widget.TextView[contains(@content-desc,
            '${toExtProvider}')]/..//android.widget.TextView[contains(@text,'${toAggAccount}')]`;
        this.eligibleOwnAccount = (accountName) => `//android.widget.TextView[contains(@content-desc,'${accountName}')]`;
        this.preSelectedSubAccount = helper.byResourceId(ViewClasses.BUTTON, 'radioOptionButton');
        this.subAccountDetails = helper.byResourceId(ViewClasses.TEXT_VIEW, 'radioOptionDescription');
        this.subAccountMoreInfoPopUp = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'mortgageOverpaymentSubAccountInfoDialogId');
        this.sortCodeAndAccountNumber = (accountSortCode, accountNumber) => `//android.widget.TextView[@text='
            ${accountSortCode}']/following-sibling::android.widget.TextView[@text='${accountNumber}']`;
        this.helpTextAmount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'inputFieldTipMessage');
        this.monthInCalendar = (month, year) => helper.byText(ViewClasses.TEXT_VIEW, `${month} ${year}`);
        this.viewPaymentLimits = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubAmountLimitButton');
    }
}

module.exports = PaymentHubMainScreen;
