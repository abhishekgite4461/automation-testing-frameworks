const BaseViewPaymentLimitsScreen = require('../../../viewPaymentLimits.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

// TODO : PJO-9424 devs to add accessibility IDs

class ViewPaymentLimitsScreen extends BaseViewPaymentLimitsScreen {
    constructor() {
        super();
        this.onlineMobileBanking = helper.byResourceId(ViewClasses.TEXT_VIEW, 'limitHubHeader');
        this.ukPaymentsDailyLimit = helper.byResourceId(ViewClasses.TEXT_VIEW, 'limitHubTitle');
        this.ukPaymentResetMessage = '//*[@text="Limits reset at 11:59pm each day."]';
        this.transferYourOwnAccount = '//*[contains(@text,"Transfers between your own")]';
        this.telephonyBanking = '//*[@text="Telephone Banking"]';
        this.ukTelephonyBanking = '//*[@text="UK payments"]';
        this.paymentLimitLeftToday = helper.byResourceIdAndIndex(ViewClasses.TEXT_VIEW, 'limitHubSubDetail', 1);
    }
}

module.exports = ViewPaymentLimitsScreen;
