const BaseSuccessTransferScreen = require('../../../successTransfer.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class SuccessTransferScreen extends BaseSuccessTransferScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubSuccessTitle');
        this.viewTransactionButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubSuccessViewTransactionsButton');
        this.makeAnotherTransferButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubSuccessAnotherPaymentButton');
        this.fromAccountNameDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryFromArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle')}`;
        this.fromSortCodeDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryFromArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSortCode')}`;
        this.fromAccountNumberDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryFromArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber')}`;
        this.toAccountNameDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryToArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle')}`;
        this.toSortCodeDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryToArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSortCode')}`;
        this.toAccountNumberDetail = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentSummaryToArrangementView')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber')}`;
        this.remitterAmount = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubWrapperFromPlaceholder')}${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementBalanceComments')}`;
        this.summaryContainer = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubSuccessPaymentSummary');
        this.amountDetail = '//*[preceding-sibling::*[@text="Amount:"]]/android.widget.TextView';
        this.successMessage = helper.byText(ViewClasses.TEXT_VIEW, 'Success');
        this.notNow = helper.byResourceId(ViewClasses.BUTTON, 'dialogNeutralAction');
        this.mpaTransferApprovalSuccessMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Transfer successfully submitted for approval.');
    }
}

module.exports = SuccessTransferScreen;
