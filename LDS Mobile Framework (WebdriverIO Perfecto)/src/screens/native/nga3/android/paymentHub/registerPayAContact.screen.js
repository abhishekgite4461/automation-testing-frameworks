const BaseRegisterPayAContactScreen = require('../../../registerPayAContact.screen.js');
const helper = require('../../../android.helper.js');
const ViewClasses = require('../../../../../enums/androidViewClass.enum.js');

class RegisterPayAContactScreen extends BaseRegisterPayAContactScreen {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.VIEW, 'Register for Pay a Contact');
        this.registerButton = helper.byResourceIdWebView(ViewClasses.BUTTON, 'frmIntrdocution:btnP2PRegisterIntro');
        this.continueButton = helper.byResourceIdWebView(ViewClasses.BUTTON, 'frmViewAccountOverview:btnP2PChooseProxy');
        this.agreeCheckBox = helper.byResourceIdWebView(ViewClasses.CHECK_BOX, 'frmSelectAccount:acceptTsAndCs');
        this.continuetoP2PButton = helper.byResourceIdWebView(ViewClasses.BUTTON, 'frmSelectAccount:btnP2PChooseAccount');
        this.continueToP2PAuthentication = helper.byResourceIdWebView(ViewClasses.VIEW, 'frmViewAccountOverview:lkAccountOverviewBottom');
        this.P2PsuccessMessage = helper.byText(ViewClasses.VIEW, 'Registration successful');
        this.lnkP2PSetting = helper.byResourceIdWebView(ViewClasses.VIEW, 'frmViewAccountOverview:lnkP2PSettings');
        this.lkDeRegisterFromP2P = helper.byResourceIdWebView(ViewClasses.VIEW, 'frmViewAccountOverview:lkDeRegisterFromP2P_1');
        this.authPassword = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'frmRegistrationFailure:txtAuthPwd');
        this.continueToDereg = helper.byResourceIdWebView(ViewClasses.BUTTON, 'frmRegistrationFailure:btnBackToAccountOverView');
        this.deregSuceesMsg = helper.byText(ViewClasses.VIEW, 'Pay a Contact mobile number de-registration successful');
    }
}

module.exports = RegisterPayAContactScreen;
