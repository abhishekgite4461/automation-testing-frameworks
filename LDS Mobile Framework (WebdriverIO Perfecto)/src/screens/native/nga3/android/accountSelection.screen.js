const BaseAccountSelection = require('../../accountSelection.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class accountSelection extends BaseAccountSelection {
    constructor() {
        super();
        this.accountSelectionAccountName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle');
        this.accountSelectionAccountNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber');
        this.accountSelectionSortCode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSortCode');
        this.accountSelectionAccountBalance = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementBalance');
        this.currentAccountWarningMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'noCurrentAccountsMessage');
    }
}

module.exports = accountSelection;
