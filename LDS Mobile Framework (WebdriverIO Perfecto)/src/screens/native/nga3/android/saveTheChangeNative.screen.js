const BaseSecureInboxScreen = require('../../secureInbox.screen.js');
const helper = require('../../android.helper.js');
const ViewClasses = require('../../../../enums/androidViewClass.enum.js');

class SecureInboxScreen extends BaseSecureInboxScreen {
    constructor() {
        super();
        this.infoPageNavigator = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'saveTheChangeItemAnimation');
        this.setUpSTCOption = helper.byResourceId(ViewClasses.BUTTON, 'saveTheChangeSetupButton');
        this.alreadyRegistered = helper.byResourceId('alreadyregistered');// TODO: PAYMOB-2321
        this.openNewSavingAccount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'openNewSavingAccount');// TODO: PAYMOB-2321
        this.stcSavingAccount = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'savingsAccount');
        this.acceptTC = helper.byResourceId(ViewClasses.CHECK_BOX, 'checkBoxStcAgreement');
        this.turnONSTC = helper.byResourceId(ViewClasses.BUTTON, 'saveTheChangeReviewButton');
        this.stcSuccess = helper.byResourceId(ViewClasses.TEXT_VIEW, 'saveTheChangeSuccessTitle');
        this.backToAccounts = helper.byResourceId(ViewClasses.BUTTON, 'saveTheChangeSuccessBackButton');
        this.SaveTheChangeDeleteSuccessHeader = helper.byResourceId(ViewClasses.TEXT_VIEW, 'saveTheChangeSuccessTitle');
        this.saveTheChangeTurnOffButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'saveTheChangeManageDelete');
    }
}

module.exports = SecureInboxScreen;
