const BaseAppFeedbackScreen = require('../../appFeedback.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class AppFeedbackScreen extends BaseAppFeedbackScreen {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.TEXT_VIEW, 'Your feedback will help us to improve the app');
        this.feedbackForm = helper.byText(ViewClasses.TEXT_VIEW, 'How would you rate the app? *');
        this.submitButton = helper.byText(ViewClasses.BUTTON, 'Submit');
    }
}

module.exports = AppFeedbackScreen;
