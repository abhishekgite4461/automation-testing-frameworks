const BaseDirectDebit = require('../../directDebit.screen.js');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class DirectDebit extends BaseDirectDebit {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.VIEW, 'Direct Debits');
    }
}

module.exports = DirectDebit;
