const BaseBrowserScreen = require('../../browser.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class BrowserScreen extends BaseBrowserScreen {
    constructor() {
        super();
        this.chromeBrowserTabs = `${helper.byPartialResourceId(ViewClasses.IMAGE_BUTTON, 'tab_switcher_button')}|${helper.byPartialResourceId(ViewClasses.IMAGE_VIEW, 'tab_switcher_button')}`;
        this.chromeMenuButton = helper.byPartialResourceId(ViewClasses.IMAGE_BUTTON, 'menu_button');
        this.chromeCloseAllTabs = helper.byContentDesc(ViewClasses.TEXT_VIEW, 'Close all tabs');
        this.chromeNewTabButton = `${helper.byPartialResourceId(ViewClasses.BUTTON, 'new_tab_button')}|${helper.byPartialResourceId(ViewClasses.IMAGE_BUTTON, 'new_tab_button')}|${helper.byPartialResourceId(ViewClasses.IMAGE_BUTTON, 'tab_switcher_new_tab_button')}`;
        this.androidBrowserTabs = helper.byText(ViewClasses.TEXT_VIEW, 'Tabs');
        this.androidcloseAllTabs = helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'closeall_layout_text');
    }
}

module.exports = BrowserScreen;
