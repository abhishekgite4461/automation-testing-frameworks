const BaseMemorableInformationScreen = require('../../memorableInformation.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class MemorableInformationScreen extends BaseMemorableInformationScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'enterMiSubtitle');
        this.iBRegMiTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationToolbarText');
        this.memorableCharacter = (ordinal) => helper.byText(ViewClasses.TEXT_VIEW, ordinal);
        this.firstOrdinal = helper.byResourceId(ViewClasses.TEXT_VIEW, 'miLabelOne');
        this.secondOrdinal = helper.byResourceId(ViewClasses.TEXT_VIEW, 'miLabelTwo');
        this.thirdOrdinal = helper.byResourceId(ViewClasses.TEXT_VIEW, 'miLabelThree');
        this.memorableInformationCharacter = (ordinal) => {
            const field = ['One', 'Two', 'Three'][ordinal];
            return `${helper.byResourceId(ViewClasses.VIEW_GROUP, `miInput${field}`)}//android.widget.EditText`;
        };
        this.memorableInformationCharacterAppium = (ordinal) => {
            const field = ['One', 'Two', 'Three'][ordinal];
            return `${helper.byResourceId(ViewClasses.VIEW_GROUP, `miInput${field}`)}//android.widget.EditText`;
        };
        this.firstMemorableCharacter = `${helper.byResourceId(ViewClasses.VIEW_GROUP, 'miInputOne')}//android.widget.EditText`;
        this.secondMemorableCharacter = `${helper.byResourceId(ViewClasses.VIEW_GROUP, 'miInputTwo')}//android.widget.EditText`;
        this.thirdMemorableCharacter = `${helper.byResourceId(ViewClasses.VIEW_GROUP, 'miInputThree')}//android.widget.EditText`;
        this.fscsTitle = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'enterMiFscsTile');
        this.miTextboxes = '//android.widget.EditText[contains(@resource-id,":id/miInput")]';
        this.miForgotYourPassword = helper.byResourceId(ViewClasses.TEXT_VIEW, 'enterMiForgotDetailsLink');
        this.miCharacters = (ordinal) => `${helper.byResourceId(ViewClasses.EDIT_TEXT, 'miInput')}[${ordinal}]`;
        this.continueToMyAccounts = helper.byResourceId(ViewClasses.BUTTON, 'interstitialLeadContinueButton');
        this.touchButton = helper.byResourceId(ViewClasses.BUTTON, 'fingerprintInterstitialOptOutButton');
        this.dataConsent = helper.byResourceId(ViewClasses.SCROLL_VIEW, 'analyticsConsentInterstitial');
        this.acceptAll = helper.byResourceId(ViewClasses.BUTTON, 'analyticsConsentAcceptAllButton');
        this.tooltipLink = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'enterMiHelpIcon');
        this.tooltipPopup = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.closeToolTip = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.expressLogonUnavailableOKButon = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.expressLogonUnavailableTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        // BNGA data consent page
        this.manageDataConsent = helper.byResourceId(ViewClasses.SCROLL_VIEW, 'manageAnalyticsConsents');
        this.manageDataConsentYesButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mandatorySelectSwitchLeft');
        this.manageDataConsentConfirmButton = helper.byResourceId(ViewClasses.BUTTON, 'analyticsConsentConfirmButton');
        this.keepUsingMI = helper.byResourceId(ViewClasses.BUTTON, 'fingerprintInterstitialUseMiButton');
    }
}

module.exports = MemorableInformationScreen;
