const BaseSecureInboxScreen = require('../../secureInbox.screen.js');
const helper = require('../../android.helper.js');
const ViewClasses = require('../../../../enums/androidViewClass.enum.js');

class SecureInboxScreen extends BaseSecureInboxScreen {
    constructor() {
        super();
        this.title = helper.byPartialText(ViewClasses.VIEW, 'Inbox');
        this.inboxMessage = helper.byResourceIdWebView(ViewClasses.VIEW, 'MsgCorsLnk_1');
        this.archiveButton = helper.byResourceIdWebView(ViewClasses.VIEW, 'archiveMessageMobile');
        this.undoArchiveButton = helper.byResourceIdWebView(ViewClasses.VIEW, 'lnkUndoArch');
        this.verifyUndoArchiveMessage = helper.byPartialText(ViewClasses.VIEW, 'action has been successful.');
        this.pdfMessage = helper.byText(ViewClasses.VIEW, 'Download PDF');
        this.folderName = (accountName) => `${helper.byPartialText(ViewClasses.RADIO_BUTTON, `${accountName}`)}`;
        this.accountLabel = (accountName) => `${helper.byText(ViewClasses.VIEW, `${accountName}`)}`;
        this.noMessagesLabel = helper.byText(ViewClasses.VIEW, 'You currently have no messages in this category.');
        this.filteredMessage = (accountName) => `(${helper.byPartialText(ViewClasses.VIEW, `${accountName}`)})[2]`;
        this.inbox = helper.byText(ViewClasses.VIEW, 'inbox folders');
    }
}

module.exports = SecureInboxScreen;
