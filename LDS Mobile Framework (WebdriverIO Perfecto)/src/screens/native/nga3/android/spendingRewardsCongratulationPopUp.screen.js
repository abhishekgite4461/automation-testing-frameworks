const BaseSpendingRewardsCongratulationsPopUpScreen = require('../../spendingRewardsCongratulationPopUp.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class SpendingRewardsCongratulationsPopUpScreen
    extends BaseSpendingRewardsCongratulationsPopUpScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'spendingRewardsSuccessModal');
        this.okButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
    }
}

module.exports = SpendingRewardsCongratulationsPopUpScreen;
