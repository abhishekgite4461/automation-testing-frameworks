const BaseRequestPermissionPopUpScreen = require('../../requestPermissionPopUp.screen');

class RequestPermissionPopUpScreen extends BaseRequestPermissionPopUpScreen {
    constructor() {
        super();
        this.title = '//*[@resource-id="com.android.packageinstaller:id/permission_message"]';
        this.permissionAcceptButton = '//*[@resource-id="com.android.packageinstaller:id/permission_allow_button"]';
        this.setPhoneDefaultOption = '//android.widget.GridView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]';
    }
}

module.exports = RequestPermissionPopUpScreen;
