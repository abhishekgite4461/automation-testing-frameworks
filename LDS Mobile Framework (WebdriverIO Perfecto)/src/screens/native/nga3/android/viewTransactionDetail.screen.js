const BaseViewTransactionDetailScreen = require('../../viewTransactionDetail.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');
const DeviceHelper = require('../../../../utils/device.helper');

class ViewTransactionDetailScreen extends BaseViewTransactionDetailScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.HORIZONTAL_SCROLL_VIEW, 'statement_months_tab_bar');
        this.amount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'txvCurrency');
        this.moveMoney = helper.byResourceId(ViewClasses.BUTTON, 'btnMoveMoney');
        this.unSureTransaction = helper.byResourceId(ViewClasses.TEXT_VIEW, 'txvUnsure');
        this.closeButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'imgDownArrow');
        this.transactionDate = helper.byResourceId(ViewClasses.TEXT_VIEW, 'txvDate');
        this.transactionType = helper.byResourceId(ViewClasses.TEXT_VIEW, 'txvType');
        this.errorText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'txvErrorMsgVtd2');
        this.postedTransaction = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDescription');
        this.vtdContainer = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'modalContent');
        this.date = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailsDateLabel');
        this.vtdCloseButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarCloseButton');
        this.vtdAmount = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'transactionDetailsHeaderTransactionAmountParent');
        this.furtherDetailsLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailsDisclaimer');
        this.unsureTransactionButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailsUnsureButton');
        this.chargeText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailsTransactionTypeField');
        this.statementsDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDescription');
        this.manageStandingOrderButton = helper.byText(ViewClasses.BUTTON, 'Manage standing orders');
        this.standingOrderText = helper.byText(ViewClasses.TEXT_VIEW, 'Standing Order');
        this.payeeField = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailsPayeeFirstField');
        this.referenceDirectDebit = helper.byText(ViewClasses.TEXT_VIEW, 'Reference:');
        this.viewDirectDebit = helper.byText(ViewClasses.BUTTON, 'View Direct Debits');
        this.directDebitText = helper.byText(ViewClasses.TEXT_VIEW, 'Direct Debit');
        this.transferPaymentsButton = helper.byResourceId(ViewClasses.BUTTON, 'transactionDetailsPaymentsAndTransfersButton');
        this.mobilePaymentInText = helper.byText(ViewClasses.TEXT_VIEW, 'Mobile Payment Inbound');
        this.mobilePaymentOutText = helper.byText(ViewClasses.TEXT_VIEW, 'Mobile Payment Outbound');
        this.depositText = helper.byText(ViewClasses.TEXT_VIEW, 'Deposit');
        this.cashText = helper.byText(ViewClasses.TEXT_VIEW, 'Cash');
        this.fasterPaymentsInText = helper.byText(ViewClasses.TEXT_VIEW, 'Faster Payments Incoming');
        this.fasterPaymentsOutText = helper.byText(ViewClasses.TEXT_VIEW, 'Faster Payments Outgoing');
        this.payeeFieldAdditionalDetails = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailsPayeeSecondField');
        this.transferText = helper.byText(ViewClasses.TEXT_VIEW, 'Transfer');
        this.billPaymentText = helper.byText(ViewClasses.TEXT_VIEW, 'Bill Payment');
        this.bankGiroCreditText = helper.byText(ViewClasses.TEXT_VIEW, 'Bank Giro Credit');
        this.cashPointText = helper.byText(ViewClasses.TEXT_VIEW, 'Cashpoint');
        this.payeeFieldCardNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailsPayeeSecondField');
        this.chequeText = helper.byText(ViewClasses.TEXT_VIEW, 'Cheque');
        this.dueSoon = helper.byText(ViewClasses.TEXT_VIEW, 'Due Soon');
        this.onlinePhoneMailText = helper.byText(ViewClasses.TEXT_VIEW, 'Online/phone/mail order purchase');
        this.cardEndingNumber = helper.byText(ViewClasses.TEXT_VIEW, 'Card ending:');
        this.dateOfTransaction = helper.byText(ViewClasses.TEXT_VIEW, 'Date of transaction:');
        this.contactLessPurchaseText = helper.byText(ViewClasses.TEXT_VIEW, 'Contactless purchase');
        this.chipAndPinText = helper.byText(ViewClasses.TEXT_VIEW, 'Chip and PIN purchase');
        this.pendingPaymentTransaction = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDescription');
        this.dueSoonLozenge = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailsLozengeLabel');
        this.outgoingPaymentText = helper.byText(ViewClasses.TEXT_VIEW, 'Outgoing Payment');
        this.sortCodeAccountNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailsPayeeSecondField');
        this.changeOrDeleteText = helper.byText(ViewClasses.TEXT_VIEW, 'To change or delete this payment, please visit the desktop site.');
        this.referenceField = helper.byText(ViewClasses.TEXT_VIEW, 'Reference:');
        this.datePendingPayment = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailsHeaderBottomLabel');
        this.creditCardText = helper.byText(ViewClasses.TEXT_VIEW, 'Credit Card');
        this.manualPaperText = helper.byText(ViewClasses.TEXT_VIEW, 'Manual card/paper purchase');
        this.debitCardText = helper.byText(ViewClasses.TEXT_VIEW, 'Debit Card');
        this.debitDisclaimerText = helper.byText(ViewClasses.TEXT_VIEW, '* This transaction has yet to be taken from your account balance but has been deducted from your \'balance after pending\'.');
        this.notYetAvailable = helper.byText(ViewClasses.TEXT_VIEW, 'Not yet available');
        this.chequesText = helper.byText(ViewClasses.TEXT_VIEW, 'Cheque(s)');
        this.depositedOn = helper.byText(ViewClasses.TEXT_VIEW, 'Deposited on:');
        this.availableBy = helper.byText(ViewClasses.TEXT_VIEW, 'Available by:');
        this.creditDisclaimerText = helper.byText(ViewClasses.TEXT_VIEW, '* Please note this has yet to be added to your \'balance after pending\'.');
        this.creditCardDisclaimerText = helper.byText(ViewClasses.TEXT_VIEW, '* This transaction has yet to be added to your balance but has '
            + 'been deducted from your available funds.');
        this.transactionAmount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailsAmountLabel');
        this.transactionDescription = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'transactionDetailsPayeeField');
        if (DeviceHelper.isUAT()) {
            this.vtdAmount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailsAmountLabel');
            this.unsureDueTransactionButton = helper.byText(ViewClasses.TEXT_VIEW, 'DUE SOON');
        }
        this.unsureAboutTransactionPage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Help');
        this.timeOfTransaction = helper.byResourceId(ViewClasses.TABLE_ROW, 'transactionDetailsTimeOfTransaction');
        this.sortCode = helper.byResourceId(ViewClasses.TABLE_ROW, 'transactionDetailsSortCode');
        this.accountNumber = helper.byResourceId(ViewClasses.TABLE_ROW, 'transactionDetailsAccountNumber');
        this.viewInformationLink = '//android.widget.TextView[contains(@text,"tap here")]';
        this.winBackTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.stayButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.googleMap = helper.byContentDesc(ViewClasses.VIEW, 'Google Map');
        this.cashBack = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailsHeaderBottomLabel');
        this.cashBackReferenceField = helper.byText(ViewClasses.TEXT_VIEW, 'Cashback:');
        this.dailyODFeeDescription = helper.byText(ViewClasses.TEXT_VIEW, 'DAILY OD FEE');
        this.exchangeRate = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transaction_details_table_row_title');
        this.registerSpendingRewardsButton = helper.byText(ViewClasses.BUTTON, 'Register for Everyday Offers');
        this.viewYourOffers = helper.byResourceId(ViewClasses.TEXT_VIEW, 'everydayOffersCardCallToAction');
        this.cashBackDescription = helper.byText(ViewClasses.TEXT_VIEW, 'CASHBACK');
        this.selectAccountHeading = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Select account');
        this.cashBackAmount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transaction_details_table_row_content');
        this.accountDebitedDate = helper.byText(ViewClasses.TEXT_VIEW, 'Account debited date:');
        this.accountCreditedDate = helper.byText(ViewClasses.TEXT_VIEW, 'Account credited date:');
        this.transactionTypeField = helper.byResourceId(ViewClasses.TEXT_VIEW, 'transactionDetailsTransactionTypeField');
        this.dontRecogniseTransaction = helper.byText(ViewClasses.TEXT_VIEW, 'Don\'t recognise the transaction?');
        this.expectingRefund = helper.byText(ViewClasses.TEXT_VIEW, 'Expecting a refund?');
        this.multipleDates = helper.byText(ViewClasses.TEXT_VIEW, 'Don\'t know why there are multiple dates?');
        this.travelDisputes = helper.byText(ViewClasses.TEXT_VIEW, 'Travel Disputes'); // TODO - STAMOB-2280
        this.travelDisputes = helper.byText(ViewClasses.TEXT_VIEW, 'Holiday no longer going ahead? Find out if you can get your money back'); // TODO - STAMOB-2280
        this.covid19Support = helper.byText(ViewClasses.TEXT_VIEW, 'COVID-19 Help and Support');
        this.helpDetailsPage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'helpTransactionDetailsScreenTitle');
        this.helpDetailsPageText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'helpTransactionDetailsScreenBody');
        this.dontRecogniseTransactionWebLink = helper.byPartialText(ViewClasses.TEXT_VIEW, 'here');
        this.expectingRefundWebLink = helper.byPartialText(ViewClasses.TEXT_VIEW, 'our website');
        this.multipleRatesDetailsPage = helper.byText(ViewClasses.TEXT_VIEW, 'Where debit card transactions are processed '
            + 'on weekends and Bank Holidays, up to three different dates may be shown: the date the transaction occurred, the date '
            + 'your account was debited/credited and the date the transaction appeared on your statement.');
        this.covid19SupportWebLink = helper.byPartialText(ViewClasses.TEXT_VIEW, 'our website');
        this.checkTheGuidanceHeading = helper.byText(ViewClasses.TEXT_VIEW, 'Check the latest guidance');
        this.disputeMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'checkOurWebsiteLink');
        this.contactUs = helper.byResourceId(ViewClasses.TEXT_VIEW, 'contactUsLink');
        this.infoHeading = helper.byResourceId(ViewClasses.TEXT_VIEW, 'header');
        this.whatIsPendingTransaction = helper.byText(ViewClasses.TEXT_VIEW, 'What is a pending transaction?');
        this.cancelPendingTransaction = helper.byText(ViewClasses.TEXT_VIEW, 'Looking to cancel a pending transaction?');
        this.whatIsPendingTransactionDetailsPage = helper.byText(ViewClasses.TEXT_VIEW, 'A pending transaction is a '
            + 'transaction where the money has been put aside for the merchant to take, but hasn\'t been taken yet. The merchant will '
            + 'often take the money after a couple of days, but it can sometimes take longer.');
        this.cancelPendingTransactionDetailsPage = helper.byText(ViewClasses.TEXT_VIEW, 'A pending transaction cannot usually be '
            + 'cancelled as the money has been put aside for the merchant to take, it just hasn\'t been taken yet. The merchant will often '
            + 'take the money after a couple of days, but it can sometimes take longer.');
        this.travelDisputesNavigationHeader = '//*[contains(@resource-id,":id/navigationToolbarText")]/following::*[contains(@resource-id,":id/baseContent")]';
        this.travelDisputesTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'helpTransactionDetailsScreenTitle');
        this.travelCopy = helper.byResourceId(ViewClasses.TEXT_VIEW, 'helpTransactionDetailsScreenBody');
        this.getStartedButton = helper.byResourceId(ViewClasses.BUTTON, 'travelDisputeButton');
        this.beforeYouBeginNavigationHeader = '//*[contains(@resource-id,":id/navigationToolbarText")]/following::*[contains(@resource-id,":id/header")]';
        this.pageHeader = helper.byResourceId(ViewClasses.TEXT_VIEW, 'header');
        this.warningText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'continueWarningText');
        this.updateWarningText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'updateWarningText');
        this.detailsLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'detailsLabel');
        this.addressLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'addressLabel');
        this.addressField = helper.byResourceId(ViewClasses.TEXT_VIEW, 'addressField');
        this.mobileNumberLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mobileNumberLabel');
        this.mobileNumberField = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mobileNumberField');
        this.emailLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'emailLabel');
        this.emailAddress = helper.byResourceId(ViewClasses.TEXT_VIEW, 'emailField');
        this.updateDetailsPrimaryButton = helper.byResourceId(ViewClasses.BUTTON, 'updatePrimaryButton');
        this.updateDetailsSecondaryButton = helper.byResourceId(ViewClasses.BUTTON, 'updateSecondaryButton');
        this.continueButton = helper.byResourceId(ViewClasses.BUTTON, 'continueButton');
        this.gFormsPageTitle = helper.byText(ViewClasses.VIEW, 'Stub GForm');
        this.vtdLogo = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'transactionDetailsTransactionLogoImage');
    }
}

module.exports = ViewTransactionDetailScreen;
