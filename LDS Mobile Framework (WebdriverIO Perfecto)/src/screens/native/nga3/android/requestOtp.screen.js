const BaseRequestOtpScreen = require('../../requestOtp.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class RequestOtpScreen extends BaseRequestOtpScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'otpTitle');
        this.confirmButton = helper.byResourceId(ViewClasses.BUTTON, 'confirmButton');
        this.cancelButton = helper.byResourceId(ViewClasses.BUTTON, 'cancelButton');
    }
}

module.exports = RequestOtpScreen;
