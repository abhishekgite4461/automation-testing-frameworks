const BaseDepositHistoryScreen = require('../../ics/depositHistory.screen');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class DepositHistoryScreen extends BaseDepositHistoryScreen {
    constructor() {
        super();
        this.technicalErrorText = helper.byText(ViewClasses.TEXT_VIEW, 'Deposit history unavailable');
        this.accountErrorText = helper.byText(ViewClasses.TEXT_VIEW, "Sorry, we can't show some of the information you've asked for right now. Please try later.");
    }
}

module.exports = DepositHistoryScreen;
