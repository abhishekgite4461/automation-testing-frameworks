const BaseMobileBrowserScreen = require('../mobileBrowser.screen');

class mobileBrowserScreen extends BaseMobileBrowserScreen {
    constructor() {
        super();
        this.brandUrl = 'online.lloydsbank.co.uk';
    }
}

module.exports = mobileBrowserScreen;
