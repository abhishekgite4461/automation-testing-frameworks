const BaseAppStoreScreen = require('../appStore.screen');

class AppStoreScreen extends BaseAppStoreScreen {
    constructor() {
        super();
        this.appText = "//*[@text='Lloyds Bank Mobile Banking: by your side']";
    }
}

module.exports = AppStoreScreen;
