const BaseAutoLogOffSettingsScreen = require('../../../settings/autoLogOffSettings.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class AutoLogOffSettingsScreen extends BaseAutoLogOffSettingsScreen {
    constructor() {
        super();
        // TODO MOB3-14692
        this.title = helper.byPartialText(ViewClasses.TEXT_VIEW, 'To keep you secure');
        this.autoLogOffImmediately = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'logoffImmediate');
        this.autoLogOff1minute = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'logoffAfterOneMin');
        this.autoLogOff2minute = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'logoffAfterTwoMin');
        this.autoLogOff5minute = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'logoffAfterFiveMin');
        this.autoLogOff10minute = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'logoffAfterTenMin');
        this.confirmButton = helper.byEnabledResourceId(ViewClasses.BUTTON, 'saveButton');
    }
}

module.exports = AutoLogOffSettingsScreen;
