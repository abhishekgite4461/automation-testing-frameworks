const BaseSecuritySettingsScreen = require('../../../settings/securitySettings.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class SecuritySettingsScreen extends BaseSecuritySettingsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'securityDetailsTitle');
        this.securityDetailsUserIDLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'securityDetailsUserIDLabel');
        this.securityDetailsUserIDValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'securityDetailsUserIDValue');
        this.securityDetailsAppVersionLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'securityDetailsAppVersionLabel');
        this.securityDetailsAppVersionValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'securityDetailsAppVersionValue');
        this.securityDetailsDeviceNameLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'securityDetailsDeviceNameLabel');
        this.securityDetailsDeviceNameValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'securityDetailsDeviceNameValue');
        this.securityDetailsDeviceTypeLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'securityDetailsDeviceTypeLabel');
        this.securityDetailsDeviceTypeValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'securityDetailsDeviceTypeValue');
        this.securityDetailsForgottenPasswordButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'securityDetailsForgottenPasswordButton');
        this.securityDetailsAutoLogOffButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'securityDetailsAutoLogOffButton');
        this.fingerprintToggle = helper.byResourceId(ViewClasses.SWITCH, 'settingsSecurityFingerprintSwitch');
        this.securityFingerPrintToggleCell = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsSecurityFingerprintSwitchContainer');
        this.securityFingerprintCell = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsSecurityFingerprintSection');
    }
}

module.exports = SecuritySettingsScreen;
