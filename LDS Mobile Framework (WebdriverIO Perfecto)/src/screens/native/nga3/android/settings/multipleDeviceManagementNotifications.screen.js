const BaseMultipleDeviceManagementNotificationsScreen = require('../../../settings/multipleDeviceManagementNotifications.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class MultipleDeviceManagementNotificationsScreen extends BaseMultipleDeviceManagementNotificationsScreen {
    constructor() {
        super();
        // TODO : MCC-2233 devs to add accessibility IDs
        this.header = helper.byPartialContentDesc(ViewClasses.TEXT_VIEW, 'Notifications');
        this.goToDeviceSettingsButton = helper.byText(ViewClasses.BUTTON, 'Go to device settings');
        this.accountNotificationsTile = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mdm_header_body');
    }
}
module.exports = MultipleDeviceManagementNotificationsScreen;
