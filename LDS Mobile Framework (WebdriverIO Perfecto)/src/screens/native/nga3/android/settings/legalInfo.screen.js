const BaseLegalInfoScreen = require('../../../settings/legalInfo.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class LegalInfoScreen extends BaseLegalInfoScreen {
    constructor() {
        super();
        this.title = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Legal information.');
        this.legalInformationText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'legalInformationMessage');
        this.legalAndPrivacy = helper.byText(ViewClasses.TEXT_VIEW, 'Legal and privacy');
        this.cookieUseAndPermissions = helper.byText(ViewClasses.TEXT_VIEW, 'Your data');
        this.thirdPartyAcknowledgements = helper.byText(ViewClasses.TEXT_VIEW, 'Third party acknowledgements');
        this.dismissButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarCloseButton');
    }
}

module.exports = LegalInfoScreen;
