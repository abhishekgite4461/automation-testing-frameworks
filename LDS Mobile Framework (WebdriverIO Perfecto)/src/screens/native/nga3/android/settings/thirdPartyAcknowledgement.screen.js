const BaseThirdPartyAcknowledgementScreen = require('../../../settings/thirdPartyAcknowledgement.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ThirdPartyAcknowledgementScreen extends BaseThirdPartyAcknowledgementScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'legalInformationStaticContent');
    }
}

module.exports = ThirdPartyAcknowledgementScreen;
