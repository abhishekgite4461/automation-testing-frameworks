const BaseResetConfirmationScreen = require('../../../settings/resetConfirmation.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ResetConfirmationScreen extends BaseResetConfirmationScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'disenrollDialog');
        this.goButton = helper.byText(ViewClasses.TEXT_VIEW, 'Go');
        this.cancelButton = helper.byText(ViewClasses.TEXT_VIEW, 'Cancel');
    }
}

module.exports = ResetConfirmationScreen;
