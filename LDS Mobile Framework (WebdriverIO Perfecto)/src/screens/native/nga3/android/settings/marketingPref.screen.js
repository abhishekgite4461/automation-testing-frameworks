const BaseMarketingPrefScreen = require('../../../settings/marketingPref.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class MarketingPref extends BaseMarketingPrefScreen {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.TEXT_VIEW, 'Title');
        this.halifaxMarketing = helper.byText(ViewClasses.TEXT_VIEW, 'Halifax');
        this.bosMarketing = helper.byText(ViewClasses.TEXT_VIEW, 'Bank of Scotland');
    }
}

module.exports = MarketingPref;
