const BaseResetScreen = require('../../../settings/reset.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ResetScreen extends BaseResetScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsResettingTheAppTitle');
        this.resetButton = helper.byResourceId(ViewClasses.BUTTON, 'settingsResetAppButton');
        this.resetAppWarningMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Make sure you’ve updated your mobile phone number before de-registering your device');
        this.resetAppInfo = helper.byPartialText(ViewClasses.TEXT_VIEW, 'can only be reversed by re-registering');
    }
}

module.exports = ResetScreen;
