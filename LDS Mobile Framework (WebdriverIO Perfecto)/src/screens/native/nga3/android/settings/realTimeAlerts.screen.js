const BaseRealTimeAlertsScreen = require('../../../settings/realTimeAlerts.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class RealTimeAlertsScreen extends BaseRealTimeAlertsScreen {
    constructor() {
        super();
        this.title = helper.byPartialContentDesc(ViewClasses.TEXT_VIEW, 'Notifications');
        this.heading = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Notifications');
        this.accountAlertsToggleSwitch = helper.byResourceId(ViewClasses.SWITCH, 'push_notifications_preference_switch');
        this.newAccountAlertsToggleSwitch = helper.byResourceId(ViewClasses.SWITCH, 'mdm_preference_switch');
        this.confirmationAlertTitle = helper.byText(ViewClasses.TEXT_VIEW, 'Notifications on');
        this.optOutConfirmationAlertTitle = helper.byPartialText(ViewClasses.TEXT_VIEW, 'no longer receive notifications');
        this.okButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.optOutOkButton = helper.byText(ViewClasses.BUTTON, 'OK');
        this.pushPromptScreenATitle = helper.byPartialText(ViewClasses.TEXT_VIEW, 'notifications direct to your device');
        this.notNowOption = helper.byResourceId(ViewClasses.TEXT_VIEW, 'pushNotificationsOptInNotNowButton');
        this.notNowConflictOption = helper.byResourceId(ViewClasses.BUTTON, 'pushNotificationsLaterButton');
        this.turnOnNotificationsOption = helper.byResourceId(ViewClasses.BUTTON, 'pushNotificationsOptInActivateNowButton');
        this.deviceSettings = helper.byContentDesc(ViewClasses.FRAME_LAYOUT, 'Settings');
        this.generalManagement = helper.byText(ViewClasses.TEXT_VIEW, 'General management');
        this.dateAndTime = helper.byText(ViewClasses.TEXT_VIEW, 'Date and time');
        this.automaticDateAndTime = helper.byText(ViewClasses.TEXT_VIEW, 'Automatic date and time');
        this.setDate = helper.byText(ViewClasses.TEXT_VIEW, 'Set date');
        this.nextCalenderMonthOption = helper.byContentDesc(ViewClasses.IMAGE_BUTTON, 'Next month');

        this.futureDate = (date, monthIndex) => {
            const month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][monthIndex];
            return helper.byPartialContentDesc(ViewClasses.VIEW, `${date} ${month}`);
        };

        this.doneOption = helper.byText(ViewClasses.BUTTON, 'DONE');
        this.pushPromptScreenBTitle = helper.byPartialText(ViewClasses.TEXT_VIEW, 'money is important');
        this.nextOption = helper.byResourceId(ViewClasses.TEXT_VIEW, 'pushNotificationsOptInMessagingNotificationsNext');
        this.pushPromptScreenBSecondTitle = helper.byPartialText(ViewClasses.TEXT_VIEW, 'track of your money');
        this.pushPromptScreenBThirdTitle = helper.byPartialText(ViewClasses.TEXT_VIEW, 'quick and easy way');
        this.apps = helper.byText(ViewClasses.TEXT_VIEW, 'Apps');
        this.bosDeviceSettings = helper.byText(ViewClasses.TEXT_VIEW, 'Bank of Scot');
        this.halifaxDeviceSettings = helper.byText(ViewClasses.TEXT_VIEW, 'Halifax');
        this.lloydsDeviceSettings = helper.byText(ViewClasses.TEXT_VIEW, 'Lloyds Bank');
        this.deviceNotifications = helper.byText(ViewClasses.TEXT_VIEW, 'Notifications');
        this.deviceNotificationsStatusOn = helper.byText(ViewClasses.TEXT_VIEW, 'ON');
        this.deviceNotificationsStatusOff = helper.byText(ViewClasses.TEXT_VIEW, 'OFF');
        this.conflictScreenTitle = helper.byText(ViewClasses.TEXT_VIEW, 'Continue with notifications?');
        this.turnOffInAppButton = helper.byContentDesc(ViewClasses.TEXT_VIEW, 'Turn off in app button');
        this.turnOffConfirmationLoginPageTitle = helper.byPartialText(ViewClasses.TEXT_VIEW, 'turned on notification');
        this.turnOffConfirmationLoginPageOkButton = helper.byResourceId(ViewClasses.BUTTON, 'pushNotificationsOptedOutOkButton');
        this.appNotificationsDeviceTitle = helper.byText(ViewClasses.TEXT_VIEW, 'APP NOTIFICATIONS');
        this.apps = helper.byText(ViewClasses.TEXT_VIEW, 'Apps');
        this.lloydsbank = helper.byText(ViewClasses.TEXT_VIEW, 'Lloyds Bank');
        this.notifications = helper.byText(ViewClasses.TEXT_VIEW, 'Notifications');
        this.conflictprompt = helper.byText(ViewClasses.TEXT_VIEW, 'Continue with notifications?');
        this.conflictpopup = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'conflict_dialog_container');
        this.notnowconflictpopup = helper.byText(ViewClasses.TEXT_VIEW, 'Not now');
        this.noThanksNotificationsOption = helper.byText(ViewClasses.TEXT_VIEW, 'No Thanks');
        this.incompleteActionsOverlay = helper.byPartialText(ViewClasses.TEXT_VIEW, 'receive notifications right now as');
        this.incompleteActionsOverlayOKButton = helper.byText(ViewClasses.BUTTON, 'OK');
        this.enableNotificationInstruction = helper.byPartialText(ViewClasses.TEXT_VIEW, 'device settings first');
    }
}

module.exports = RealTimeAlertsScreen;
