const BaseCookieUseAndPermissionScreen = require('../../../settings/cookieUseAndPermission.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class CookieUseAndPermissionScreen extends BaseCookieUseAndPermissionScreen {
    constructor() {
        super();
        this.title = '//*[contains(@text, \'data. Heading.\')]';
        this.cookiePolicyLink = helper.byPartialContentDesc(ViewClasses.VIEW, 'Cookie Policy');
    }
}

module.exports = CookieUseAndPermissionScreen;
