const BaseLegalAndPrivacyScreen = require('../../../settings/legalAndPrivacy.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class LegalAndPrivacyScreenScreen extends BaseLegalAndPrivacyScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationToolbarText');
        this.termsAndConditionsLink = helper.byPartialContentDesc(ViewClasses.VIEW, 'Internet Banking terms and conditions');
        this.dismissButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarCloseButton');
        this.backButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarBackButton');
    }
}

module.exports = LegalAndPrivacyScreenScreen;
