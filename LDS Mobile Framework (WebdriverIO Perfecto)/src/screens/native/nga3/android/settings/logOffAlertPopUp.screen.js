const BaseLogOffAlertPopUpScreen = require('../../../settings/logOffAlertPopUp.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class LogOffAlertPopUpScreen extends BaseLogOffAlertPopUpScreen {
    constructor() {
        super();
        // TODO MOB3-6990 Need to add accessibility ID for title
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.logOffButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.continueButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
    }
}

module.exports = LogOffAlertPopUpScreen;
