const BaseResetYourPasswordScreen = require('../../../settings/resetYourPassword.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ResetYourPasswordScreen extends BaseResetYourPasswordScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'resetPasswordTitle');
        this.enterNewPasswordEditBox = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'resetPasswordEnterNewPasswordEditBox');
        this.confirmNewPasswordEditBox = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'resetPasswordConfirmPasswordEditBox');
        this.clickSubmitButton = helper.byResourceId(ViewClasses.BUTTON, 'resetPasswordSubmitButton');
        this.errorMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'notificationContentTextView');
        this.nonIdenticalErrorMessage = helper.byText(ViewClasses.TEXT_VIEW, 'Please ensure that the values you have entered are identical.');
        this.noPersonalDetailsErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'include personal details like your name, birthday or email address');
        this.nonSecurePasswordErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'This password is not secure enough.');
        this.lastFivePasswordErrorMessage = '//*[contains(@text,"Please choose a password that is different from your last five passwords.") or contains(@text,"Please choose a password that is different from your last 5 passwords.")]';
        this.easyToGuessPasswordErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Avoid common, easy-to-guess passwords');
        this.tipsForPasswordAccordion = helper.byResourceId(ViewClasses.TEXT_VIEW, 'resetPasswordTooltipTitle');
        this.tipsForPasswordToolTip = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.closeButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
    }
}

module.exports = ResetYourPasswordScreen;
