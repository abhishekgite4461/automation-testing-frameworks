const BaseDarkUrlScreen = require('../../../settings/darkUrl.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class DarkUrlScreen extends BaseDarkUrlScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'editTextUrlTitle');
        this.darkUrlButton = helper.byResourceId(ViewClasses.BUTTON, 'buttonSubmit');
    }
}

module.exports = DarkUrlScreen;
