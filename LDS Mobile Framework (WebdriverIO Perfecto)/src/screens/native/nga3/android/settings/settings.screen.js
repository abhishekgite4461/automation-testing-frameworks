const BaseSettingsScreen = require('../../../settings/settings.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class SettingsScreen extends BaseSettingsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsYourPersonalDetailsTile');
        this.personalDetailsButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsYourPersonalDetailsTile');
        this.everydayOfferButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsEverydayOffersTile');
        this.securitySettingsButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsYourSecuritySettingsTile');
        this.payAContactButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsPayAContactSettingsTile');
        this.goingAbroadButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsGoingAbroadTile');
        this.resetAppButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsResetMobileBankingTile');
        this.legalInfo = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsLegalInfoTile');
        this.howWeContactYou = helper.byText(ViewClasses.TEXT_VIEW, 'How we contact you');
        this.realTimeAlertsButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsRealTimeAlertsTile');
        this.businessDetailsButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsBusinessDetailsTile');
        this.settingsList = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsTitle');
        this.settingsOpenBankingTile = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsOpenBankingTile');
        this.settingsOpenBankingTileErrorBanner = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'errorNotificationView');
    }
}

module.exports = SettingsScreen;
