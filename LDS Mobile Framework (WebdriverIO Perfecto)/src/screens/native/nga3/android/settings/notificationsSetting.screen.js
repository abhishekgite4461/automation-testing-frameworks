const BaseNotificationsSettingScreen = require('../../../settings/notificationsSetting.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class NotificationsSettingScreen extends BaseNotificationsSettingScreen {
    constructor() {
        super();
        // TODO RTL-2373 Need to add ResourceID for header
        this.header = helper.byText(ViewClasses.TEXT_VIEW, 'Notifications. Heading.');
        this.smartAlertsToggleSwitch = helper.byResourceId(ViewClasses.SWITCH, 'push_notifications_preference_switch');
    }
}
module.exports = NotificationsSettingScreen;
