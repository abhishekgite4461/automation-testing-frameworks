const BaseResetPasswordConfirmationPopUpScreen = require('../../../settings/resetPasswordConfirmationPopUp.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ResetPasswordConfirmationPopUpScreen extends BaseResetPasswordConfirmationPopUpScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'successResetPasswordModal');
        this.OkAndLogOffButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
    }
}

module.exports = ResetPasswordConfirmationPopUpScreen;
