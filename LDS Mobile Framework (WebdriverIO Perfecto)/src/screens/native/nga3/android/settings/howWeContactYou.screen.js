const BaseContactYouScreen = require('../../../settings/howWeContactYou.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class HowWeContactYou extends BaseContactYouScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsTitle');
        this.onlinePaperPreference = helper.byText(ViewClasses.TEXT_VIEW, 'Online and paper preferences');
        this.statementFreqPreference = helper.byText(ViewClasses.TEXT_VIEW, 'Statement frequency preferences');
        this.passbookTransactionsUpdates = helper.byText(ViewClasses.TEXT_VIEW, 'Passbook transactions updates');
        this.marketingPreference = helper.byText(ViewClasses.TEXT_VIEW, 'Marketing choices');
        this.realTimeAlert = helper.byText(ViewClasses.TEXT_VIEW, 'Real time alert');
    }
}

module.exports = HowWeContactYou;
