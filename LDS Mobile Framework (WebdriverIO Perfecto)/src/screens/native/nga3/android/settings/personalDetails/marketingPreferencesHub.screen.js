const BaseLeadsScreen = require('../../../../settings/personalDetails/marketingPreferencesHub.screen.js');
const helper = require('../../../../android.helper.js');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class MarketingPreferencesHubScreen extends BaseLeadsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'marketingHub');
        this.marketingHubIntroduction = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'marketingHubIntroduction');
        this.bosMarketingHubIntroductionText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'marketingHubIntroductionText');
        this.halMarketingHubIntroductionText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'marketingHubIntroductionText');
        this.progressIndicator = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'marketingHubProgressView');
        this.optedIn = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mandatorySelectSwitchLeft');
        this.optedOut = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mandatorySelectSwitchRight');
        this.internetPrefs = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'marketingPreferencesOnlineInternalPreferencesToggle');
        this.emailPrefs = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'marketingPreferencesEmailPreferencesToggle');
        this.postPrefs = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'marketingPreferencesMailPreferencesToggle');
        this.thirdPartyPrefs = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'marketingPreferencesOnlineThirdPartyPreferencesToggle');
        this.smsPrefs = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'marketingPreferencesSmsPreferencesToggle');
        this.phonePrefs = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'marketingPreferencesPhonePreferencesToggle');
        this.submitButton = helper.byResourceId(ViewClasses.BUTTON, 'marketingPreferencesSubmitButton');
        this.notification = helper.byResourceId(ViewClasses.TEXT_VIEW, 'notificationContentTextView');
        this.closeErrorMessage = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'notificationCloseImageView');
        this.winbackModal = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'marketingPreferencesWinBackModal');
        this.winbackLeave = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.winbackStay = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.accordionLink = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Lloyds Banking Group');
        this.inCompleteErrorMessage = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'incompleteMarketingPreferencesNotification');
        this.errorMessageClose = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'notificationCloseImageView');
    }
}

module.exports = MarketingPreferencesHubScreen;
