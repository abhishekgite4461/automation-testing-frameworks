const BaseDataConsentScreen = require('../../../../settings/personalDetails/dataConsent.screen.js');
const helper = require('../../../../android.helper.js');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class DataConsentScreen extends BaseDataConsentScreen {
    constructor() {
        super();
        this.dataConsentInterstitialPageTitle = helper.byResourceId(ViewClasses.SCROLL_VIEW, 'analyticsConsentInterstitial');
        this.dataConsentManageConsentPageTitle = helper.byResourceId(ViewClasses.SCROLL_VIEW, 'manageAnalyticsConsents');
        this.consentOptedIn = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mandatorySelectSwitchLeft');
        this.consentOptedOut = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mandatorySelectSwitchRight');
        this.dataConsentWinbackModal = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'analyticsConsentsInternalWinBackId');
        this.dataConsentExternalWinbackModal = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'analyticsConsentsExternalWinBackId');
        this.marketingPrefs = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'analyticsConsentsMarketingToggle');
        this.performancePrefs = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'analyticsConsentsPerformanceToggle');
        this.dataConsentAcceptAllButton = helper.byResourceId(ViewClasses.BUTTON, 'analyticsConsentAcceptAllButton');
        this.dataConsentManageConsentButton = helper.byResourceId(ViewClasses.BUTTON, 'analyticsConsentManageConsentsButton');
        this.dataConsentConfirmButton = helper.byResourceId(ViewClasses.BUTTON, 'analyticsConsentConfirmButton');
        this.dataConsentCookiePolicyLink = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Cookies policy');
        this.interstitialDataConsentCookiePolicyLink = '//android.widget.TextView[contains(@text,"Cookies policy.")]';
        this.globalBackButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarBackButton');
        this.dataConsentForWinbackLeave = helper.byText(ViewClasses.TEXT_VIEW, 'Leave');
        this.dataConsentForWinbackStay = helper.byText(ViewClasses.TEXT_VIEW, 'Stay');
        this.cookiesPolicyPagetitle = helper.byText(ViewClasses.VIEW, 'Cookie policy');
    }
}

module.exports = DataConsentScreen;
