const AlertPopUpScreen = require('../../alertPopUp.screen');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class AbandonChangesWinbackScreen extends AlertPopUpScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'winBackDialogModal');
    }
}

module.exports = AbandonChangesWinbackScreen;
