const BaseLeadsScreen = require('../../../../settings/personalDetails/marketingPreferencesConfirmation.screen.js');
const helper = require('../../../../android.helper.js');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class MarketingPreferencesConfirmationScreen extends BaseLeadsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'marketingPreferencesUpdateSuccessHeaderText');
        this.profileButton = helper.byResourceId(ViewClasses.BUTTON, 'consentConfirmationProfileButton');
        this.accountsButton = helper.byResourceId(ViewClasses.BUTTON, 'consentConfirmationAccountsButton');
    }
}
module.exports = MarketingPreferencesConfirmationScreen;
