const BaseChangeAddressScreen = require('../../../../settings/personalDetails/changeAddress.screen');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class ChangeAddressScreen extends BaseChangeAddressScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaPostcodeScreenTitle');
        this.changeAddressButton = helper.byResourceId(ViewClasses.BUTTON, 'settingsPersonalDetailsChangeAddressButton');
        this.addressLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsAddressLabel');
        this.address = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsAddress');
        this.postcodeLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsPostcodeLabel');
        this.postcode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsPostcode');
    }
}

module.exports = ChangeAddressScreen;
