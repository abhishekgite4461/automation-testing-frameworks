const BaseChangePhoneNumbersScreen = require('../../../../settings/personalDetails/changePhoneNumbers.screen');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class ChangePhoneNumbersScreen extends BaseChangePhoneNumbersScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'personalDetailsEditPhoneScreenDescription');
        this.mobileNumberField = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'personalDetailsEditMobileNumberField')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.homeNumberField = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'personalDetailsEditHomeNumberField')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.workNumberField = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'personalDetailsEditWorkNumberField')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.workExtensionField = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'personalDetailsEditWorkNumberExtensionField');
        this.mobilePaymLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'personalDetailsP2pWarningMessage');
        this.paymInfoLabel = helper.byText(ViewClasses.TEXT_VIEW, 'Registered for Pay a Contact');
        this.continueButton = helper.byResourceId(ViewClasses.BUTTON, 'personalDetailsEditPhoneConfirmButton');
        this.textOnEditPhoneNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'personalDetailsEditPhoneBottomDescription');
        this.textOnConfirmPhoneNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'confirmPhoneNumberDescription');
    }
}

module.exports = ChangePhoneNumbersScreen;
