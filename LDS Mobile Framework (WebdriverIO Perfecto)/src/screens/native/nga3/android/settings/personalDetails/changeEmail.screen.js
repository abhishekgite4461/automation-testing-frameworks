const BaseChangeEmailScreen = require('../../../../settings/personalDetails/changeEmail.screen');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class ChangeEmailScreen extends BaseChangeEmailScreen {
    constructor() {
        super();
        this.enterEmailAddressField = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'personalDetailsEditEmailEmailField')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.reenterEmailAddressField = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'personalDetailsEditEmailReenterEmailField')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.confirmButton = helper.byResourceId(ViewClasses.BUTTON, 'personalDetailsEditEmailConfirmButton');
        this.errorMessage = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'errorNotificationView');
        this.dismissErrorMessage = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'notificationCloseImageView');
    }
}

module.exports = ChangeEmailScreen;
