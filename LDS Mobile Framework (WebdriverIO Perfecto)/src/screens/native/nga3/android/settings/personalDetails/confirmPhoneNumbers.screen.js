const BaseConfirmPhoneNumbersScreen = require('../../../../settings/personalDetails/confirmPhoneNumbers.screen');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class ConfirmPhoneNumbersScreen extends BaseConfirmPhoneNumbersScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'confirmPhoneTitle');
        this.mobileNumberField = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsMobileNumber');
        this.homeNumberField = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsHomeNumber');
        this.workNumberField = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsWorkNumber');
        this.workExtensionField = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsExtensionNumber');
        this.confirmButton = helper.byResourceId(ViewClasses.BUTTON, 'confirmPhoneConfirmButton');
        this.editButton = helper.byResourceId(ViewClasses.BUTTON, 'confirmPhoneEditButton');
    }
}

module.exports = ConfirmPhoneNumbersScreen;
