const AlertPopUpScreen = require('../../alertPopUp.screen');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class UpdateEmailSuccessPopUpScreen extends AlertPopUpScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'personalDetailsEmailUpdatedSuccessModal');
    }
}

module.exports = UpdateEmailSuccessPopUpScreen;
