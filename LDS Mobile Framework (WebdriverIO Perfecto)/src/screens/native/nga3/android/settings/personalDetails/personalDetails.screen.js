const BasePersonalDetailsScreen = require('../../../../settings/personalDetails/personalDetails.screen.js');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class PersonalDetailsScreen extends BasePersonalDetailsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsTitle');
        this.fullName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsFullName');
        this.userIdLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsUserIdLabel');
        this.userId = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsUserId');
        this.mobileNumberLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsMobileNumberLabel');
        this.mobileNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsMobileNumber');
        this.homeNumberLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsHomeNumberLabel');
        this.homeNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsHomeNumber');
        this.workNumberLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsWorkNumberLabel');
        this.workNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsWorkNumber');
        this.addressLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsAddressLabel');
        this.address = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsAddress');
        this.postcodeLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsPostcodeLabel');
        this.postcode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsPostcode');
        this.emailLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsEmailLabel');
        this.email = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsPersonalDetailsEmail');
        this.emailContainer = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsPersonalDetailsEmailViewContainer');
        this.addressContainer = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsPersonalDetailsAddressViewContainer');
        this.primaryMarketingLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'myProfilePrimaryMarketingPreferencesLink');
        this.secondaryMarketingLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'myProfileSecondaryMarketingPreferencesLink');
        this.dataConsentLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'myProfileDataConsentLink');
        this.businessName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'businessDetailsName');
        this.businessRegisteredAddressText = helper.byText(ViewClasses.TEXT_VIEW, 'Business registered address');
        this.businessAddress = helper.byResourceId(ViewClasses.TEXT_VIEW, 'businessDetailsAddressLabel');
        this.businessPostcode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'businessDetailsPostcodeLabel');
    }
}

module.exports = PersonalDetailsScreen;
