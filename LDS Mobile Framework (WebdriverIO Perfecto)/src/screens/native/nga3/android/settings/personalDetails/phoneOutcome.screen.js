const BasePhoneOutcomeScreen = require('../../../../settings/personalDetails/phoneOutcome.screen.js');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class PhoneOutcomeScreen extends BasePhoneOutcomeScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'personalDetailsPhoneOutcomeTitle');
        this.message = helper.byResourceId(ViewClasses.TEXT_VIEW, 'personalDetailsPhoneOutcomeDescription');
        this.backToButton = helper.byResourceId(ViewClasses.BUTTON, 'personalDetailsPhoneOutcomeFinishButton');
    }
}

module.exports = PhoneOutcomeScreen;
