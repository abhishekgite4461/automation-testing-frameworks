const BaseCardCarouselScreen = require('../../../viewPin/viewPinCardCarousel.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

// TODO:MCC-2831 JIRA issue to change the valid element id

class ViewPinCardCarouselScreen extends BaseCardCarouselScreen {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.TEXT_VIEW, 'View PIN Text');
        this.creditCardName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'textLabel');
        this.creditCardLastfourdigits = helper.byResourceId(ViewClasses.TEXT_VIEW, 'textPan');
        this.scrollOption = helper.byResourceId(ViewClasses.TEXT_VIEW, 'viewPagerIndicator');
        this.viewPinButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'btnViewPin');
        this.enterPasswordDialogTitle = helper.byText(ViewClasses.TEXT_VIEW, 'Enter password');
        this.dialogPasswordField = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'inputFieldEditText');
        this.dialogForgottenPassword = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'passwordConfirmationDialogForgotPasswordLabel');
        this.dialogCancelButton = helper.byText(ViewClasses.TEXT_VIEW, 'Cancel');
        this.dialogOkButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
    }
}

module.exports = ViewPinCardCarouselScreen;
