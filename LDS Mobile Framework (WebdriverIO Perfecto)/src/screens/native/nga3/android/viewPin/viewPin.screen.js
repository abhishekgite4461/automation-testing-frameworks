const Screen = require('../../../viewPin/viewPin.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

// TODO:MCC-2831 JIRA issue to change the valid element id

class viewPinScreen extends Screen {
    constructor() {
        super();
        this.revealPinButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'revealPinButton');
        this.PIN = helper.byResourceId(ViewClasses.VIEW, 'revealPinText');
    }
}
module.exports = viewPinScreen;
