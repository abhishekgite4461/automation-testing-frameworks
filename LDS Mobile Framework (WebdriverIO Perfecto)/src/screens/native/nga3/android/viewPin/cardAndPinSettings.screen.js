const BaseCardAndPinSettingsScreen = require('../../../viewPin/cardAndPinSettings.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

// TODO:MCC-2831 JIRA issue to change the valid element id

class CardAndPinSettingsScreen extends BaseCardAndPinSettingsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.id, 'viewPin');
        this.viewPinTile = '//*[@text="View PIN"]';
        this.replaceCardAndPinOption = helper.byResourceId(ViewClasses.id, 'replaceCardItem');
    }
}

module.exports = CardAndPinSettingsScreen;
