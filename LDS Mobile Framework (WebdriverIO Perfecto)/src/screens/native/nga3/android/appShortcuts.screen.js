const appShortcutsScreen = require('../../appShortcuts.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class AppShortCutsScreen extends appShortcutsScreen {
    constructor() {
        super();
        this.appByName = (appName) => helper.byText(ViewClasses.TEXT_VIEW, appName);
        this.branchFinder = helper.byText(ViewClasses.TEXT_VIEW, 'Branch finder');
        this.payAndTransfer = helper.byText(ViewClasses.TEXT_VIEW, 'Pay and transfer');
        this.ourWebsite = helper.byText(ViewClasses.TEXT_VIEW, 'Our website');
        this.manageCards = helper.byText(ViewClasses.TEXT_VIEW, 'Manage cards');
        this.closebutton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarCloseButton');
        this.contactus = helper.byText(ViewClasses.TEXT_VIEW, 'Contact us');
        this.hfxBranchInMaps = helper.byText(ViewClasses.TEXT_VIEW, 'Halifax Bank Branch');
        this.ldsBranchInMaps = helper.byText(ViewClasses.TEXT_VIEW, 'Lloyds Bank Branch');
        this.bosBranchInMaps = helper.byText(ViewClasses.TEXT_VIEW, 'Halifax Bank of Scotland Branch');
        this.hfxWebsite = helper.byText(ViewClasses.EDIT_TEXT, 'https://www.halifax.co.uk');
        this.ldsWebsite = helper.byText(ViewClasses.EDIT_TEXT, 'https://www.lloydsbank.com');
        this.bosWebsite = helper.byText(ViewClasses.EDIT_TEXT, 'https://www.bankofscotland.co.uk');
    }
}

module.exports = AppShortCutsScreen;
