const BasePayAContactSettingsScreen = require('../../payAContactSettings.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class PayAContactSettingsScreen extends BasePayAContactSettingsScreen {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.TEXT_VIEW, 'Pay a Contact settings');
    }
}

module.exports = PayAContactSettingsScreen;
