const BaseLoginScreen = require('../../login.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class LoginScreen extends BaseLoginScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'loginContainer');
        // MQE-1075 TODO
        this.oldTitle = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'loginContainer');
        this.userIdField = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'loginUsernameInput')}//android.widget.EditText`;
        this.passwordField = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'loginPasswordInput')}//android.widget.EditText`;
        this.continueButton = helper.byResourceId(ViewClasses.BUTTON, 'loginContinueButton');
        this.userIdErrorMessage = '//android.widget.TextView[contains(@text,"incorrect user") or contains(@text,"wrong username")]';
        this.loginForgotYourPassword = helper.byResourceId(ViewClasses.TEXT_VIEW, 'loginForgotLogonDetails');
        this.mandateLessErrorMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'not registered for');
        this.registerForIb = helper.byResourceId(ViewClasses.BUTTON, 'loginNotRegisteredButton');
        this.errorMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'notificationContentTextView');
        this.errorMessageDismiss = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'notificationCloseImageView');
        this.specialCharactersRestrictionMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'If your password contains special characters like');
        this.updateYourPasswordLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'loginUpdatePasswordLink');
    }
}

module.exports = LoginScreen;
