const BaseConfirmAddressScreen = require('../../../changeOfAddress/confirmAddress.screen.js');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ConfirmAddressScreen extends BaseConfirmAddressScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaAddressDetailsScreenTitle');
        this.mapView = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'coaAddressDetailsMapView');
        this.mapPlaceholder = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'coaAddressDetailsMapPlaceholderView');
        this.newAddress = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaAddressDetailsAddressView');
        this.confirmAddressButton = helper.byResourceId(ViewClasses.BUTTON, 'coaAddressDetailsContinueButton');
    }
}

module.exports = ConfirmAddressScreen;
