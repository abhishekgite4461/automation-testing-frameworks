const BaseAddressChangedScreen = require('../../../changeOfAddress/addressChanged.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class AddressChangedScreen extends BaseAddressChangedScreen {
    constructor() {
        super();
        this.titleIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'coaSuccessScreenTitleIcon');
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaSuccessScreenTitleView');
        this.addressChangeSuccessMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaSuccessScreenMessageView');
        this.backToHomeButton = helper.byResourceId(ViewClasses.BUTTON, 'coaSuccessScreenPersonalDetailButton');
        this.homeInsuranceLeadTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaSuccessScreenLeadTitle');
        this.homeInsuranceLeadIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'coaSuccessScreenLeadIcon');
        this.homeInsuranceLeadButton = helper.byResourceId(ViewClasses.BUTTON, 'coaSuccessScreenLeadAction');
    }
}

module.exports = AddressChangedScreen;
