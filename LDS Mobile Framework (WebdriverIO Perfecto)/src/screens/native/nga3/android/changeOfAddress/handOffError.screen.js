const BaseHandOffErrorScreen = require('../../../changeOfAddress/handOffError.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class HandOffErrorScreen extends BaseHandOffErrorScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaErrorTitleView');
        this.handOffErrorText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaErrorMessageView');
        this.callUsButton = helper.byText(ViewClasses.BUTTON, 'Call us');
        this.backToPersonalDetailsButton = helper.byText(ViewClasses.BUTTON, 'Back to your personal details');
        this.forcedLogoffTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenTitle');
        this.forcedLogoffMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenMessage');
        this.forcedLogoffLogoonButton = helper.byResourceId(ViewClasses.BUTTON, 'errorScreenButtonPrimary');
    }
}

module.exports = HandOffErrorScreen;
