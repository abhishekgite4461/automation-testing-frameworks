const BaseOneLastThingScreen = require('../../../changeOfAddress/oneLastThing.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class OneLastThingScreen extends BaseOneLastThingScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaConfirmationScreenTitle');
        this.oneLastThingSubtitleText = (number) => helper.byResourceIdAndIndex(ViewClasses.TEXT_VIEW, 'coaConfirmationAccountInfoTitleView', number);
        this.oneLastThingText = (number) => helper.byResourceIdAndIndex(ViewClasses.TEXT_VIEW, 'coaConfirmationAccountInfoContentView', number);
        this.oneLastThingSubtitles = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaConfirmationAccountInfoTitleView');
        this.oneLastThingTexts = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaConfirmationAccountInfoContentView');
        this.iUnderstandButton = helper.byResourceId(ViewClasses.BUTTON, 'coaConfirmationButton');
        this.enterPasswordDialogTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.dialogPasswordField = helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText');
        this.dialogForgottenPassword = helper.byResourceId(ViewClasses.TEXT_VIEW, 'passwordConfirmationDialogForgotPasswordLabel');
        this.dialogCancelButton = helper.byText(ViewClasses.TEXT_VIEW, 'Cancel');
        this.dialogOkButton = helper.byText(ViewClasses.TEXT_VIEW, 'OK');
    }
}

module.exports = OneLastThingScreen;
