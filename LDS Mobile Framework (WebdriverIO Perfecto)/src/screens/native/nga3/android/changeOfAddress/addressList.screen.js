const BaseAddressListScreen = require('../../../changeOfAddress/addressList.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class AddressListScreen extends BaseAddressListScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaAddressListHeadingView');
        this.addressListText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaAddressListSubTitleView');
        this.addressNotOnListButton = helper.byResourceId(ViewClasses.BUTTON, 'coaAddressListMissingButton');
        this.addressToBeSelected = helper.byResourceIdAndIndex(ViewClasses.TEXT_VIEW, 'coaAddressItemView', 1);
        this.addressToBeSelectedUAT = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Red Lion Court');
        this.addressListItem = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaAddressItemView');
        this.addressList = helper.byResourceId(ViewClasses.RECYCLER_VIEW, 'coaAddressRecyclerView');
    }
}

module.exports = AddressListScreen;
