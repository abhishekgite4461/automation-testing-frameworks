const BasePostCodeEntryScreen = require('../../../changeOfAddress/postCodeEntry.screen.js');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class PostCodeEntryScreen extends BasePostCodeEntryScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaPostcodeScreenTitle');
        this.subtitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaPostcodeFieldLabel');
        this.postCode = helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText');
        this.postCodeTipView = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'coaPostcodeInputTipView');
        this.errorIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'inputFieldStatusIcon');
        this.enterPostcodeHelpText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaPostcodeAddressChangeInfoView');
        this.enterPostcodeCallUsText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'coaPostcodeCallUsView');
        this.backArrow = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarBackButton');
        this.findAddress = helper.byResourceId(ViewClasses.BUTTON, 'coaPostcodeSubmitButton');
        this.webviewChangeAddress = helper.byResourceId(ViewClasses.WEB_VIEW, 'webJourneyWebView');
    }
}

module.exports = PostCodeEntryScreen;
