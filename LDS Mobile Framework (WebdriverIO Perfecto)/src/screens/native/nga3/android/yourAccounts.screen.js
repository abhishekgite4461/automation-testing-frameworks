const BaseYourAccountsScreen = require('../../yourAccounts.screen');
const helper = require('../../android.helper.js');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class YourAccountsScreen extends BaseYourAccountsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'accounts_overview_scrollViewContent');
        this.savingsAccountName = (srtCodeAcctNo) => helper.byText(ViewClasses.TEXT_VIEW, srtCodeAcctNo);
    }
}

module.exports = YourAccountsScreen;
