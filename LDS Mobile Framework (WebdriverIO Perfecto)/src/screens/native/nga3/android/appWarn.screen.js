const BaseAppWarnScreen = require('../../appWarn.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class AppWarnScreen extends BaseAppWarnScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenTitle');
        this.updateAppButton = helper.byResourceId(ViewClasses.BUTTON, 'errorScreenButtonPrimary');
        this.continueWithoutUpdating = helper.byResourceId(ViewClasses.BUTTON, 'errorScreenButtonSecondary');
    }
}

module.exports = AppWarnScreen;
