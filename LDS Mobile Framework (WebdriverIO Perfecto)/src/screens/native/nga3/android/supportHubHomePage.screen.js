const BaseSupportHubHomePageScreen = require('../../supportHubHomePage.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class SupportHubHomePageScreen extends BaseSupportHubHomePageScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'supportHubPageTitle');
        this.callUsButton = '//*[contains(@resource-id,":id/supportHubCallUsTile")]';
        this.mobileChatButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'supportHubChatToUsTile');
        this.mobileChatButtonLabel = (label) => helper.byText(ViewClasses.TEXT_VIEW, label);
        this.changeOfAddress = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'supportHubChangeOfAddress');
        this.lostOrStolenCard = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'supportHubLostAndStolenCard');
        this.replaceCardOrPin = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'supportHubReplaceCardOrPin');
        this.resetPassword = '//*[contains(@resource-id, "supportHubResetPassword")]';
        this.atmBranchFinder = helper.byResourceId(ViewClasses.TEXT_VIEW, 'supportHubAtmAndBranchFinder');
        this.appFeedback = helper.byResourceId(ViewClasses.TEXT_VIEW, 'supportHubProvideAppFeedback');
        this.cmaSurvey = helper.byResourceId(ViewClasses.TEXT_VIEW, 'supportHubCMASurvey');
        this.emailAccountSelectionOverlay = '//*[contains(@resource-id, "resolver_list")]';
        this.overLayTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'ngaButtonTitle');
        this.carousals = '//com.grppl.android.shell.CMBlloydsTSB73.qaBuildType.yUf[1]';
        this.updateDetailsButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'supportHubUpdateDetailsTile');
        this.mortgagePaymentHolidaysLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'supportHubCovid19MortgageHolidays');
        this.creditcardPaymentHolidaysLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'supportHubCovid19CreditCardHolidays');
        this.loanPaymentHolidaysLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'supportHubCovid19LoanHolidays');
        this.overdraftChargesLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'supportHubCovid19OverdraftCharges');
        this.travelAndDisruptionCoverLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'supportHubCovid19TravelCover');
        this.moreInfoAboutCoronavirusLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'supportHubCovid19MoreAboutCoronavirus');
        this.carousals = '//com.grppl.android.shell.CMBlloydsTSB73.qaBuildType.REx/android.widget.LinearLayout';
        this.viewPinOrReplaceCard = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'supportHubCardSupportServices');
        this.viewPinTile = '//*[@text="View PIN"]';
        this.covidHubLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'supportHubCoronavirusHelpAndSupport');
        this.covidHubNativePage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'experienceHubItemHeading');
    }
}

module.exports = SupportHubHomePageScreen;
