const BaseEnrolmentTermsAndConditionsScreen = require('../../enrolmentTermsAndConditions.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class EnrolmentTermsAndConditions extends BaseEnrolmentTermsAndConditionsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'commonTermsAndConditionsFooterLabel');
        this.acceptButton = helper.byResourceId(ViewClasses.BUTTON, 'commonPrimaryButton');
        this.declineButton = helper.byResourceId(ViewClasses.BUTTON, 'commonSecondaryButton');
    }
}

module.exports = EnrolmentTermsAndConditions;
