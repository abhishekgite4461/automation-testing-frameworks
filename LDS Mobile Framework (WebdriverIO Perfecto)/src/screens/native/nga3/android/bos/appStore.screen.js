const BaseAppStoreScreen = require('../appStore.screen');

class AppStoreScreen extends BaseAppStoreScreen {
    constructor() {
        super();
        this.appText = "//*[@text='Bank of Scotland Mobile Banking: secure on the go']";
    }
}

module.exports = AppStoreScreen;
