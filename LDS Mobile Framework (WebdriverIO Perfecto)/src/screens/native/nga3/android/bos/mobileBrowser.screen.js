const BaseMobileBrowserScreen = require('../mobileBrowser.screen');

class mobileBrowserScreen extends BaseMobileBrowserScreen {
    constructor() {
        super();
        this.brandUrl = 'online.bankofscotland.co.uk';
    }
}

module.exports = mobileBrowserScreen;
