const BaseSupportHubHomePageScreen = require('../supportHubHomePage.screen');

class SupportHubHomePageScreen extends BaseSupportHubHomePageScreen {
    constructor() {
        super();
        this.carousals = '//com.grppl.android.shell.BOS.qaBuildType.yUf[1]';
    }
}

module.exports = SupportHubHomePageScreen;
