const BaseNewWelcomeBenefitsScreen = require('../../ibRegistration/newWelcomeBenefits.screen');
const helper = require('../../../../android.helper');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum');

class NewWelcomeBenefitsScreen extends BaseNewWelcomeBenefitsScreen {
    constructor() {
        super();
        this.fraudGuaranteeTitle = helper.byText(ViewClasses.EDIT_TEXT, 'https://www.bankofscotland.co.uk/securityandprivacy/security/');
        this.appDemoTitle = helper.byText(ViewClasses.EDIT_TEXT, 'https://www.youtube.com/embed/XFgEYu5qf68?autoplay=1&rel=0&autohide=1&showinfo=0&wmode=transparent');
        this.newCustomerTitle = helper.byText(ViewClasses.EDIT_TEXT, 'https://www.lloydsbank.com/current-accounts.asp??=#current-accounts');
    }
}

module.exports = NewWelcomeBenefitsScreen;
