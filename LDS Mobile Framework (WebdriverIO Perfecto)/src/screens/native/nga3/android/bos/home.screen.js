const BaseHomeScreen = require('../home.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');
const DeviceHelper = require('../../../../../utils/device.helper');

class HomeScreen extends BaseHomeScreen {
    constructor() {
        super();
        if (!DeviceHelper.isStub()) {
            this.availableCredit = () => helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileAvailableCreditText');
        }
    }
}

module.exports = HomeScreen;
