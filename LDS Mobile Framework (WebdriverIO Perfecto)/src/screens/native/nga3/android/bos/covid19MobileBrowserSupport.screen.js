const BaseMobileBrowserScreen = require('../mobileBrowser.screen');

class covid19MobileBrowserSupportScreen extends BaseMobileBrowserScreen {
    constructor() {
        super();
        this.mortgagePaymentHolidayUrl = 'halifax.co.uk/mortgages/existing-customers/impacted-by-coronavirus/';
        this.creditcardPaymentHolidayUrl = 'bankofscotland.co.uk/helpcentre/coronavirus.html#finance';
        this.loanPaymentHolidayUrl = 'bankofscotland.co.uk/loans/existing-loans.html';
        this.overdraftChargesUrl = 'bankofscotland.co.uk/helpcentre/coronavirus.html#finance';
        this.travelAndDisruptionCoverUrl = 'bankofscotland.co.uk/helpcentre/coronavirus.html#travel';
        this.moreInfoAboutCoronavirusUrl = 'bankofscotland.co.uk/helpcentre/coronavirus.html';
    }
}

module.exports = covid19MobileBrowserSupportScreen;
