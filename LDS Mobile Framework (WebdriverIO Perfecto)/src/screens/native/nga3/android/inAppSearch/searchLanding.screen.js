const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');
const BaseSearchLandingScreen = require('../../../inAppSearch/searchLanding.screen.js');

// TODO: Created JIRA ticket-AVC-6287 to give valid element ID as per coding standards which will be fixed by devs
//  when feature is ready in environments. currently no valid element IDs are available

class searchLandingScreen extends BaseSearchLandingScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarSearchButton');
        this.searchIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarSearchButton');
        this.searchBar = helper.byResourceId(ViewClasses.EDIT_TEXT, 'searchEditText');
        this.resetMobileBankingAppLink = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'navigationResetAppButton');
        this.resetMobileBankingAppPage = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'settingsResetAppButton');
        this.viewPinLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'itemInAppSearch');
        this.viewPinPage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Card and PIN settings');
        this.fingerprintFaceScanLogonSettingsLink = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Fingerprint/face scan logon settings');
        this.securitySettingsPage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'securityDetailsTitle');
        this.chooseAutoSignOutSettingLink = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Choose auto logout setting');
        this.autoLogoutPage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'settingsAutoLogOffTitle');
        this.viewYourUserIdLink = helper.byPartialText(ViewClasses.TEXT_VIEW, 'View your User ID');
        this.viewYourCurrentAppVersionLink = helper.byPartialText(ViewClasses.TEXT_VIEW, 'View your current app version');
        this.viewAmendYourSecuritySettingsLink = helper.byPartialText(ViewClasses.TEXT_VIEW, 'View/amend your security settings');
        this.viewYourDeviceTypeLink = helper.byPartialText(ViewClasses.TEXT_VIEW, 'View your device type');
        this.viewYourDeviceNameLink = helper.byPartialText(ViewClasses.TEXT_VIEW, 'View your device name');
    }
}
module.exports = searchLandingScreen;
