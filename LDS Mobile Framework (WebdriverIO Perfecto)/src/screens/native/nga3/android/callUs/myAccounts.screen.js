const BaseMyAccountsScreen = require('../../../callUs/myAccounts.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class MyAccountsScreen extends BaseMyAccountsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'arrangementView');
        this.savingsAccount = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'savingsAccount');
        this.currentAccount = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'currentAccount');
        this.creditAccount = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'creditAccount');
        this.isaAccount = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'isaAccount');
        this.htbIsaAccount = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'htbIsaAccount');
        this.cbsLoanAccount = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'cbsPersonalLoanAccount');
        this.loanAccount = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'nonCbsPersonalLoanAccount');
        this.mortgageAccount = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'mortgageAccount');
        this.mortgageUfssAccount = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'mortgageUfssAccount');
        this.termDepositAccount = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'termDepositAccount');
        this.investmentAccount = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'investmentAccount');
        this.homeInsurance = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'homeInsurance');
        this.treasureFtd = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'treasureFtd');
        this.treasure32Dcn = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'treasure32Dcn');
        this.accountNameLabel = (account) => `//android.widget.TextView[contains(@text,"${account}") or 
            contains(@text,"${account.toUpperCase()}")]`;
    }
}

module.exports = MyAccountsScreen;
