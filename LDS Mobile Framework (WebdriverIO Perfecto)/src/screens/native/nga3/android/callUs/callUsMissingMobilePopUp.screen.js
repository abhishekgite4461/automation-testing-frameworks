const AlertPopUpScreen = require('../alertPopUp.screen');

class CallUsMissingMobilePopUpScreen extends AlertPopUpScreen {
    constructor() {
        super();
        this.addPhoneNumber = this.confirmButton;
        this.continueCall = this.cancelButton;
    }
}

module.exports = CallUsMissingMobilePopUpScreen;
