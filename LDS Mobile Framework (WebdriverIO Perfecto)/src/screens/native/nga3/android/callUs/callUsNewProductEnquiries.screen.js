const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');
const BaseNewProductEnquiries = require('../../../callUs/callUsNewProductEnquiries.screen');

class CallUsNewProductEnquiries extends BaseNewProductEnquiries {
    constructor() {
        super();
        this.title = helper.byPartialText(ViewClasses.TEXT_VIEW, 'New products');
        this.newCurrentAccount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'clickToCallHubNewCurrentAccountButton');
        this.newSavingsAccount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'clickToCallNewSavingsAccountButton');
        this.newIsaAccount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'clickToCallHubNewIsaAccountButton');
        this.newCreditAccount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'clickToCallHubNewCreditCardButton');
        this.newLoanAccount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'clickToCallHubNewLoanButton');
        this.newMortgagesAccount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'clickToCallHubNewMortgageButton');
    }
}

module.exports = CallUsNewProductEnquiries;
