const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');
const BaseSecureCallScreen = require('../../../callUs/callUs.screen.js');

class CallUsScreen extends BaseSecureCallScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'clickToCallHubTitle');
        this.callUsButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'callUsButton');
        this.callTimeLabel = helper.byMultipleResourceIds(
            ViewClasses.TEXT_VIEW,
            'callUsOpenAllHours', 'callUsRestrictedOpeningHours'
        );
        this.textPhoneLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'callUsTextphoneContent');
        this.backButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarBackButton');
        this.modalBackButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarBackButton');
        this.closeButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarCloseButton');
        this.referenceNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementReference');
        this.accountNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementAccountNumber');
        this.sortCode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementSortCode');
        this.lostOrStolenSelfServiceOption = helper.byResourceId(ViewClasses.BUTTON, 'lostOrStolenSelfServiceButton');
        this.lostOrStolenSelfServiceButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'selfServiceOptionsItemLostOrStolenCard');
        this.resetYourPasswordSelfServiceButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'selfServiceOptionsItemPasswordReset');
        this.replacementCardSelfServiceButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'selfServiceOptionsReplacementCard');
        this.phoneNumber = helper.byResourceId(ViewClasses.BUTTON, 'callUsButton');
        this.businessAccountsCallUsPage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Business accounts');
        this.phoneNumberText = helper.byResourceId(ViewClasses.BUTTON, 'callUsButton');
        this.internetBankingCallUsPage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Internet Banking');
        this.otherBankingQueryCallUsPage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Other query');
        this.suspectedFraudCallUsPage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Suspected fraud');
        this.lostOrStolenCardCallUsPage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Lost or stolen cards');
        this.emergencyCashAbroadCallUsPage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Emergency cash');
        this.medicalAssistanceAbroadCallUsPage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Medical assistance');
        this.accountCallUsPage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Account query');
    }
}

module.exports = CallUsScreen;
