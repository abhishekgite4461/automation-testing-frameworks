const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');
const BaseChangeAddressCallUsScreen = require('../../../callUs/changeAddressCallUs.screen');

class ChangeAddressCallUsScreen extends BaseChangeAddressCallUsScreen {
    constructor() {
        super();
        this.legalInfo = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Note: we can\'t change your address this way if:');
        this.callUsButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'callUsButton');
    }
}

module.exports = ChangeAddressCallUsScreen;
