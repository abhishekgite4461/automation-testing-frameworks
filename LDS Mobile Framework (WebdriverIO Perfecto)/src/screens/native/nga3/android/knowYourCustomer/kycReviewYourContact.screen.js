const BaseKnowYourCustomerScreen = require('../../../knowYourCustomer/kycReviewYourContact.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class KycReviewYourContactScreen extends BaseKnowYourCustomerScreen {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.TEXT_VIEW, 'Review your contact details');
        this.securityText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'kycMessageTextView');
        this.mobileNumber = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'kycEmailInputField');
        this.mobileLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'Mobile number');
        this.emailId = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'kycMobileInputField');
        this.emailLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'Email address');
        this.detailsCorrect = helper.byText(ViewClasses.BUTTON, 'My details are correct');
        this.updateLater = helper.byText(ViewClasses.TEXT_VIEW, 'Update Later');
        this.notNowButton = helper.byResourceId(ViewClasses.BUTTON, 'kycWarningNotNowButton');
        this.editDetails = helper.byResourceId(ViewClasses.BUTTON, 'kycWarningEditDetailsButton');
    }
}

module.exports = KycReviewYourContactScreen;
