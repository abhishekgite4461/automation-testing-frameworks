const BaseGlobalMenuScreen = require('../../globalMenu.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class GlobalMenuScreen extends BaseGlobalMenuScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'globalMenuProfileName');
        this.globalMenuButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationHeaderMenuButton');
        this.globalIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'globalMenuProfilePicture');
        this.yourProfileButton = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'globalProfile');
        this.customerName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'globalMenuProfileName');
        this.myAccountsButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'globalAccounts');
        this.newAccountButton = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalNewAccount');
        this.businessName = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'globalTitleBusinessName');

        this.transferAndPaymentButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'globalTransferPayment');
        this.moveMoneyButton = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalMoveMoney');
        this.standingOrderMenu = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalStandingOrder');
        this.payContactSettings = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalPayContact');

        this.chequeButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'globalCheques');
        this.depositChequeButton = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalDepositCheque');
        this.depositHistoryOption = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalDepositHistory');

        this.registerForEveryDayOfferButton = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalRegisterForSpendingRewards');
        this.inboxButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'globalInbox');
        this.productHubButton = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalViewProducts');
        this.atmBranchFinderButton = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalAtmBranchFinder');

        this.callUsButton = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalCallUs');
        this.mobileChatButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'globalMobileChat');
        this.fingerprint = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalFingerprint');
        this.lostStolenCardButton = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalLostStolenCard');
        this.replacementCardAndPinButton = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalReplacementCardPin');
        this.feedbackSubmitButton = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalTellUs');

        this.settingsButton = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalSettings');

        this.everyDayOfferButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'globalEverydayOffer');
        this.everyDayOfferViewOffers = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalViewOffers');
        this.everyDayOfferManageSettings = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalManageSettings');

        this.cardManagementOption = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalCardManagement');
        this.directDebitMenu = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'globalDirectDebit');

        this.manageSettings = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'manageSettings');
        this.goingAbroadButton = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'globalGoingAway');
        this.logoutButton = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'globalLogOffButton');
        this.globalBackButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarBackButton');
    }
}

module.exports = GlobalMenuScreen;
