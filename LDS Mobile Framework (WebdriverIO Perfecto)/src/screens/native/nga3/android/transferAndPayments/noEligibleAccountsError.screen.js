const BaseNoEligibleAccountsErrorScreen = require('../../../transferAndPayments/noEligibleAccountsError.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class NoEligibleAccountsErrorScreen extends BaseNoEligibleAccountsErrorScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenTitle');
        this.applyForAccountsButton = helper.byResourceId(ViewClasses.BUTTON, 'errorScreenButtonPrimary');
        this.homeButon = helper.byResourceId(ViewClasses.BUTTON, 'errorScreenButtonSecondary');
    }
}

module.exports = NoEligibleAccountsErrorScreen;
