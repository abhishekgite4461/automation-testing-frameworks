const BaseChooseRecipientScreen = require('../../../transferAndPayments/searchRecipient.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class searchRecipientScreen extends BaseChooseRecipientScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'paymentHubSearchBarView');
        this.searchIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'baseSearchBarIcon');
        this.searchEditText = helper.byResourceId(ViewClasses.EDIT_TEXT, 'baseSearchBarEditText');
        this.searchedAccountPayee = (accName) => `//*[contains(@resource-id,"beneficiaryTitle") 
            and contains(@content-desc,'${accName}')][1]`;
        this.searchedAccount = (accName) => `(//*[contains(@resource-id,"viewRecipientTransferCell")]
            //*[contains(@text, "${accName}")])[1]`;
        this.aggAccount = (accountSortCode, accountNumber) => `//android.widget.TextView[@text='${accountSortCode}']
            /following-sibling::android.widget.TextView[@text='${accountNumber}']`;
        this.searchedByPartialTextAccount = (accName) => `//*[contains(@resource-id,"viewRecipientTransferCell")]
            //*[contains(@text,"${accName}")]`;
        this.searchedBusinessAccount = (businessName, accName) => `//*[contains(@text,"${businessName}") 
            and (following-sibling::*[@text="${accName}"])]`;
        this.addNewPayeeIcon = helper.byResourceId('plus');
        this.bannerRecipient = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'beneficiaryAccountContainer');
        this.bannerAccount = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'arrangementView');
        this.addNewPayee = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubRecipientsAddNew');
        this.yourAccountsGroupHeader = helper.byText(ViewClasses.TEXT_VIEW, 'Your accounts');
        this.recipientCellByIndex = (index) => "//*[contains(@resource-id,'/paymentHubRecipientsList')]/"
            + `android.widget.RelativeLayout[${index}]//*[contains(@resource-id,'arrangementView')]`;
        this.recipientWithName = (index) => "//*[contains(@resource-id,'/paymentHubRecipientsList')]/"
            + `android.widget.RelativeLayout[${index}]//*[contains(@resource-id,'beneficiaryView')]`;
        this.accountResultByName = (label) => `//*[contains(@resource-id,
            '/arrangementTitle') and @text='${label}']`;
        this.benificiaryResultByName = (label) => `//*[contains(@resource-id,
            '/beneficiaryTitle') and (contains(@text, '${label}') or contains(@text, '${label.toUpperCase()}'))]`;
        this.accountNameLabel = (account) => `//*[contains(@text,"${account}") and not(
            following-sibling::*[@text="Payment due soon"])]`;
        this.firstRecipientFromList = "(//*[contains(@resource-id, 'beneficiaryTitle')])[1]";
        this.transferToList = (label) => `//*[preceding-sibling::*[contains(@text, 'accounts')]]
            //*[contains(@text,'${label}')]`;
        this.addNewPayeeFooter = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubRecipientsFooter');
        this.cancelSearch = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'baseSearchBarIcon');
        this.partialAddNewPayeeFooterText = helper.byPartialText(ViewClasses.TEXT_VIEW, 'add new payee');
        this.noResultMessage = "//*[contains(@resource-id, 'id/paymentHubRecipientsFooter') and contains(@text, 'No match found.')]";
        this.firstRecipientFromListManageRecipient = helper.byResourceId(ViewClasses.TEXT_VIEW, 'beneficiaryManageButton');
        this.deleteRecipient = helper.byResourceId(ViewClasses.TEXT_VIEW, 'manageRecipientActionMenuDeleteRecipient');
        this.deleteConfirm = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.searchResult = (accountName) => helper.byText(ViewClasses.TEXT_VIEW, accountName);
        this.secondRecipientFromListManageRecipient = "(//*[contains(@text, 'Manage')])[2]";
    }
}

module.exports = searchRecipientScreen;
