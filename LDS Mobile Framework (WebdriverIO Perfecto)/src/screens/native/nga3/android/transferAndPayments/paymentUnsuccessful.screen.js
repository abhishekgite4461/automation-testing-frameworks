const BasePaymentUnsuccessfulScreen = require('../../../transferAndPayments/paymentUnsuccessful.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class PaymentUnsuccessfulScreen extends BasePaymentUnsuccessfulScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'fragmentTransactionFailureScreenTitle');
        this.paymentUnsuccessful = helper.byResourceId(ViewClasses.TEXT_VIEW, 'fragmentTransactionFailureScreenMessage');
        this.bannerLeadPlacement = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'errorNotificationView');
        this.cbsProhibitiveMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'can\'t complete this request online. Please call us on');
        this.BngacbsProhibitiveMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Sorry, you can\'t set up this arrangement online. Please call us on');
    }
}

module.exports = PaymentUnsuccessfulScreen;
