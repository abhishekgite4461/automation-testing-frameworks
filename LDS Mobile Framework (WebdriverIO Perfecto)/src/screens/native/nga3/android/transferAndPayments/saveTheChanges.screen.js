const BaseSaveTheChanges = require('../../../saveTheChanges.screen.js');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class SaveTheChanges extends BaseSaveTheChanges {
    constructor() {
        super();
        this.title = helper.byPartialText(ViewClasses.VIEW, 'Save the Change®');
        this.homebutton = helper.byText(ViewClasses.TEXT_VIEW, 'Home');
    }
}

module.exports = SaveTheChanges;
