const BaseConfirmationOfPayeeScreen = require('../../../transferAndPayments/confirmationOfPayee.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ConfirmationOfPayeeScreen extends BaseConfirmationOfPayeeScreen {
    constructor() {
        super();
        this.paymentHubCopNotMatchTitle = helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'paymentHubCopNotMatchTitle');
        this.paymentHubCopBAMMMessage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'That\'s not the full name on that business account');
        this.paymentHubCopBAMMNamePlayBack = (recipientName) => helper.byPartialText(ViewClasses.TEXT_VIEW,
            `This is a business account. It belongs to ${recipientName}`);
        this.paymentHubCopNotMatchEdit = helper.byPartialResourceId(ViewClasses.BUTTON, 'paymentHubCopNotMatchEditDetails');
        this.serviceError = helper.byPartialText(ViewClasses.TEXT_VIEW, 'We can\'t confirm these details');
        this.paymentHubCopNotMatchContinue = helper.byPartialResourceId(ViewClasses.BUTTON, 'paymentHubCopNotMatchContinue');
        this.paymentHubCopANNMMessageOne = '//*[contains(@text,"That name doesn’t match that account number") or contains(@text,"That name doesn\'t match that account number")]';
        this.paymentHubCopANNMMessageTwo = '//*[contains(@text,"The name and account number still don’t match") or contains(@text,"The name and account number still don\'t match")]';
        this.bankNameToolTip = (bankName) => `//*[contains(@resource-id, 
        'inputFieldTipMessage') and contains(@text, '${bankName}')]`;
        this.partialNameMatch = helper.byPartialText(ViewClasses.TEXT_VIEW, 'That\'s not the full name on that account');
        this.businessNameMatch = helper.byPartialText(ViewClasses.TEXT_VIEW, 'This is a business account');
        this.personalAccountMatch = helper.byPartialText(ViewClasses.TEXT_VIEW, 'This is a personal account');
        this.panMbanMContent = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Is that the account you wanted to pay? If not,');
        this.paymentHubCopNotMatchShareDetailsMessage = helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'paymentHubCopNotMatchShareDetailsMessage');
        this.paymentHubCopNotMatchShareDetailsButton = helper.byPartialResourceId(ViewClasses.BUTTON, 'paymentHubCopNotMatchShareDetailsButton');
    }
}

module.exports = ConfirmationOfPayeeScreen;
