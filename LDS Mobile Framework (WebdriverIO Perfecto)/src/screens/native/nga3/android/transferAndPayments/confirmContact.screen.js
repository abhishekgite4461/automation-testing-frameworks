const BaseConfirmContactScreen = require('../../../transferAndPayments/confirmContact.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ConfirmContactScreen extends BaseConfirmContactScreen {
    constructor() {
        super();
        // TODO: MOB3-9994 Proper IDs needs to be used
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubConfirmUkNumberTitle');
        this.confirmButton = helper.byText(ViewClasses.BUTTON, 'Confirm');
        this.nameAndPhoneNumberInConfirm = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'paymentHubConfirmUkNumberToLayout');
        this.paymentHubReviewTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubReviewTitle');
        this.confirmContinueButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubConfirmUkNumberContinueButton');
        this.confirmCancelButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubConfirmUkNumberCancelButton');
    }
}

module.exports = ConfirmContactScreen;
