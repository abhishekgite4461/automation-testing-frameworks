const BaseTransferSuccessfulScreen = require('../../../transferAndPayments/transferSuccess.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class TransferSuccessScreen extends BaseTransferSuccessfulScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubSuccessTitle');
        this.viewTransactionButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubSuccessViewTransactionsButton');
        this.transferPaymentButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubSuccessAnotherPaymentButton');
        this.bannerLeadPlacement = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'transactionSuccessBannerLeadContainer');
    }
}

module.exports = TransferSuccessScreen;
