const BasePaymentSuccessScreen = require('../../../transferAndPayments/rateAppPaymentHub.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class RateAppPaymentHubScreen extends BasePaymentSuccessScreen {
    constructor() {
        super();
        this.rateApp = helper.byText(ViewClasses.TEXT_VIEW, 'Rate this app');
        this.closeRateApp = helper.byText(ViewClasses.TEXT_VIEW, 'Close');
    }
}

module.exports = RateAppPaymentHubScreen;
