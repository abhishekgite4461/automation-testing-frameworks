const BaseBeneficiaryTypeScreen = require('../../../transferAndPayments/beneficiaryType.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class BeneficiaryTypeScreen extends BaseBeneficiaryTypeScreen {
    constructor() {
        super();
        this.ukAccount = '//*[contains(@resource-id,"id/addRecipientCategoryRecycler")]/android.widget.LinearLayout[1]';
        this.internationalBankAccount = '//*[contains(@resource-id,"id/addRecipientCategoryRecycler")]/android.widget.LinearLayout[2]';
        this.ukMobileNumber = '//*[contains(@resource-id,"id/addRecipientCategoryRecycler")]/android.widget.LinearLayout[3]';
        this.payingUkMobileNumberLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'addRecipientCategoryFooter');
    }
}

module.exports = BeneficiaryTypeScreen;
