const BasePaymentReviewScreen = require('../../../transferAndPayments/paymentReview.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class PaymentReviewScreen extends BasePaymentReviewScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubReviewTitle');
        this.confirmButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubReviewConfirmButton');
        this.editButton = helper.byResourceId(ViewClasses.BUTTON, 'editPaymentReviewButton');
    }
}

module.exports = PaymentReviewScreen;
