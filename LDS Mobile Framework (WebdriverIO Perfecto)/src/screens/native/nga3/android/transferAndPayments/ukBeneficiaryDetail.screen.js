const BaseChooseRecipientScreen = require('../../../transferAndPayments/ukBeneficiaryDetail.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class UkBeneficiaryDetailScreen extends BaseChooseRecipientScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'addRecipientCategoryContent');
        this.beneficiaryTileIcon = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'currentAccount');
        this.paymentHubAddUkAccountAdditionalContextCopy = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubAddUkAccountAdditionalContextCopy');
        this.paymentHubAddUkAccountPayeeNameLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubAddUkAccountPayeeNameLabel');
        this.payeeNameUk = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubAddUkAccountPayeeNameInput')}/android.widget.RelativeLayout/android.widget.EditText`;
        this.sortCodeField = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubAddUkAccountSortCodeInput')}/android.widget.RelativeLayout/android.widget.EditText`;
        this.paymentHubAddUkAccountSortCodeLabel = helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'paymentHubAddUkAccountSortCodeLabel');
        this.paymentHubAddUkAccountAccountNumberLabel = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubAddUkAccountAccountNumberLabel');
        this.accountNumberUk = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubAddUkAccountAccountNumberInput')}/android.widget.RelativeLayout/android.widget.EditText`;
        this.paymentHubAddUkAccountCancelButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubAddUkAccountCancelButton');
        this.paymentHubAddUkAccountContinueButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubAddUkAccountContinueButton');
        this.navigationToolbarBackButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarBackButton');
        this.arrangementSummaryListFromAccount = helper.byResourceId(ViewClasses.RECYCLER_VIEW, 'arrangementSummaryList');
        this.errorMessageUkBeneficiary = helper.byResourceId(ViewClasses.TEXT_VIEW, 'notificationContentTextView');
        this.errorMessageExclamatory = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'notificationTypeImageView');
        this.paymentHubAddUkAccountSecurityInfoText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubAddUKAccountSecurityInfoText');
        this.debitCardTile = helper.byResourceId(ViewClasses.TEXT_VIEW, 'ukDebitCardTitle');
        this.paymentHubAddUkAccountTypeCheckBox = helper.byPartialResourceId(ViewClasses.CHECK_BOX, 'paymentHubAddUkAccountTypeCheckBox');
        this.invalidAccountNumberErrorMessage = '//*[contains(@text,"This account doesn’t exist Please call your payee to check their details.")'
            + ' or contains(@text,"This account doesn\'t exist Please call your payee to check their details.")]';
        this.closeInvalidAccountNumberErrorMessage = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'notificationCloseImageView');
        this.yourUKBankAccount = helper.byText(ViewClasses.TEXT_VIEW, 'Your UK Bank Account');
        this.anotherUKBankAccount = (ukBankAccount) => helper.byText(ViewClasses.TEXT_VIEW, ukBankAccount);
        this.confirmPaymentPage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationToolbarText'); // TODO: STAMOB-2133 add ID
        this.confirmPaymentCheckBox = helper.byResourceId(ViewClasses.CHECK_BOX, 'paymentHubReviewWarningConfirmationCheckBox');
        this.confirmButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubReviewConfirmButton');
        this.cotinueButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.fromAccountName = `${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle')}[1]`;
        this.toAccountName = `${helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle')}[2]`;
        this.toSortCode = helper.byResourceId(ViewClasses.RELATIVE_LAYOUT, 'arrangementDetailsWrapper');
        this.amountValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'infoTextTileTitle');
        this.dateValue = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'paymentSummaryWhenInfoTextTile');
        this.paymentMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubReviewPaymentMessage');
        this.approveYourPaymentPage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationToolbarText');
        this.dataLineInfoOne = helper.byResourceId(ViewClasses.TEXT_VIEW, 'masterCardConsentDataInfoLineOne');
        this.dataLineInfoTwo = helper.byResourceId(ViewClasses.TEXT_VIEW, 'masterCardConsentDataInfoLineTwo');
        this.termsAndCondition = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Terms and Conditions'); // TODO: STAMOB-2133 add ID
        this.dataPrivacyNotice = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Data Privacy Notice'); // TODO: STAMOB-2133 add ID
        this.dialogBoxContinueButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.goToYourBankDialogBox = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'dialogHeaderContainer');
        this.approveYourPaymentCotinueButton = helper.byResourceId(ViewClasses.BUTTON, 'continue_button');
        this.bankNotListed = helper.byResourceId(ViewClasses.TEXT_VIEW, 'masterCardProviderHeaderTitle');
        this.payUKDebitCard = helper.byResourceId(ViewClasses.VIEW_GROUP, 'masterCardPayByDebitCardItemView');
        this.pickBankPayForm = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'navigationToolbarTitleContainer');
        this.unfinishedPaymentPage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'payAggRedirectText');
        this.amountInputField = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'inputFieldIconRight');
        this.statementBalance = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubAmountToPayFirstAction');
        this.minimumAmount = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubAmountToPaySecondAction');
        this.cancelButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubCancelDialogButton');
    }
}

module.exports = UkBeneficiaryDetailScreen;
