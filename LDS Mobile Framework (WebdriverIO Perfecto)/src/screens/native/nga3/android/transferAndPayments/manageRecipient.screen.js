const BaseManageRecipientScreen = require('../../../transferAndPayments/manageRecipient.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ManageRecipientScreen extends BaseManageRecipientScreen {
    constructor() {
        super();
        this.makePaymentButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'manageRecipientActionMenuMakePayment');
        this.deleteRecipientButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'manageRecipientActionMenuDeleteRecipient');
        this.actionMenuHeader = helper.byResourceId(ViewClasses.TEXT_VIEW, 'manageRecipientActionMenuHeader');
        this.dialogueBoxTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.dialogNegativeAction = helper.byText(ViewClasses.TEXT_VIEW, 'Keep');
        this.dialogPositiveAction = helper.byText(ViewClasses.TEXT_VIEW, 'Delete');
        this.viewPendingPayment = helper.byResourceId(ViewClasses.TEXT_VIEW, 'manageRecipientActionMenuViewPendingPayment');
        this.closeButton = '//*[contains(@resource-id,"touch_outside")]';
        this.viewButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.cancelSearch = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'baseSearchBarIcon');
    }
}

module.exports = ManageRecipientScreen;
