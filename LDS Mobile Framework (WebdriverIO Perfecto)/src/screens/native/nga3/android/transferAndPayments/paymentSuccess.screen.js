const BasePaymentSuccessScreen = require('../../../transferAndPayments/paymentSuccess.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class PaymentSuccessScreen extends BasePaymentSuccessScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubSuccessTitle');
        this.paymentProcessed = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubSuccessMessage');
        this.bannerLeadPlacement = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'transactionSuccessBannerLeadContainer');
    }
}

module.exports = PaymentSuccessScreen;
