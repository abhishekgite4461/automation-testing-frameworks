const BaseChooseRecipientScreen = require('../../../transferAndPayments/ukCompanySearchResults.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class UkCompanySearchResultsScreen extends BaseChooseRecipientScreen {
    constructor() {
        super();
        this.paymentHubAddUkAccountSelectionPayeeNameValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubAddUkAccountSelectionPayeeNameValue');
        this.paymentHubAddUkAccountSelectionSortCodeValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubAddUkAccountSelectionSortCodeValue');
        this.paymentHubAddUkAccountSelectionAccountNumberValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubAddUkAccountSelectionAccountNumberValue');
        this.paymentHubAddUkAccountSelectionCompanyList = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'businessBeneficiary0');
        this.businessBeneficiaryPayeeNotes = helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'businessBeneficiaryNotes');
        this.businessBeneficiarySortCode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubAddUkAccountSelectionSortCodeLabel');
        this.businessBeneficiaryAccountNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubAddUkAccountSelectionAccountNumberLabel');
        this.paymentHubAddUkAccountReviewReferenceInput = helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText');
        this.paymentHubAddUkAccountReviewReferenceConfirmInput = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubAddUKAccountReviewReferenceConfirmInput')}/android.widget.RelativeLayout/android.widget.EditText`;
        this.paymentHubAddUkAccountReviewContinueButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubAddUKAccountReviewContinueButton');
        this.paymentHubAddUkAccountReviewCancelButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubAddUKAccountReviewCancelButton');
        this.paymentHubAddUkAccountSelectionTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubAddUkAccountSelectionTitle');
        this.paymentHubAddUkAccountReviewPayeeNameValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubAddUKAccountReviewPayeeNameValue');
        this.paymentHubAddUkAccountReviewSortCodeValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubAddUKAccountReviewSortCodeValue');
        this.paymentHubAddUkAccountReviewAccountNumberValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubAddUKAccountReviewAccountNumberValue');
    }
}

module.exports = UkCompanySearchResultsScreen;
