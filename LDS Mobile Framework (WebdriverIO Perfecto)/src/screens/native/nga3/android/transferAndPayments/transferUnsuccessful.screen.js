const BaseTransferSuccessfulScreen = require('../../../transferAndPayments/transferUnsuccessful.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class TransferUnsuccessfulScreen extends BaseTransferSuccessfulScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'fragmentTransactionFailureScreenTitle');
        this.bannerLeadPlacement = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'errorNotificationView');
    }
}

module.exports = TransferUnsuccessfulScreen;
