const BaseChooseRecipientScreen = require('../../../transferAndPayments/ukMobileNumberDetail.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class UkMobileNumberDetailScreen extends BaseChooseRecipientScreen {
    constructor() {
        super();
        // TODO: MOB3-9994 Proper IDs needs to be used below
        this.title = helper.byText(ViewClasses.TEXT_VIEW, 'From:');

        this.verifyingaddPayMPage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'paymentHubAddUkNumberToValue');

        this.beneficiaryTileIcon = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementView');

        this.mobileNumber = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubAddUkNumberMobileNumberInput')}
                       ${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;

        this.amount = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubAddUkNumberAmountInputField')}
                       ${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;

        this.reference = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubAddUkNumberReferenceInputWithHintField')}
                          ${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;

        this.continueButtonInPaymentHubPage = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubAddUkNumberContinueButton');
        this.recentsTipMessage = helper.byText(ViewClasses.TEXT_VIEW, 'This will also appear on the payee’s statement');
        this.errorMessageForMinimumAndMaximumDailyLimit = helper.byResourceId(ViewClasses.TEXT_VIEW, 'notificationContentTextView');
        this.editPaymentReviewButton = helper.byResourceId(ViewClasses.BUTTON, 'editPaymentReviewButton');
        this.makeAnotherPaymentButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubSuccessAnotherPaymentButton');
        this.viewTransactionButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubSuccessViewTransactionsButton');
        this.errorMessageForAddPaym = helper.byResourceId(ViewClasses.TEXT_VIEW, 'notificationContentTextView');
        this.toFieldValue = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'paymentHubAddUkNumberMobileNumberInput')}
                       ${helper.byText(ViewClasses.TEXT_VIEW, 'Select contact or add number')}`;
    }
}

module.exports = UkMobileNumberDetailScreen;
