const BaseMobileBrowserScreen = require('../../mobileBrowser.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class mobileBrowserScreen extends BaseMobileBrowserScreen {
    constructor() {
        super();
        this.title = helper.byPartialResourceId(ViewClasses.FRAME_LAYOUT, 'control_container');
        this.browserUrl = helper.byPartialResourceId(ViewClasses.EDIT_TEXT, 'url_bar');
    }
}

module.exports = mobileBrowserScreen;
