const BaseEnrolmentGracePeriodScreen = require('../../enrolmentGracePeriod.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class EnrolmentGracePeriodScreen extends BaseEnrolmentGracePeriodScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'enrolmentEiaBypassTitle');
        this.continueButton = helper.byResourceId(ViewClasses.BUTTON, 'enrolmentEiaBypassContinueButton');
    }
}

module.exports = EnrolmentGracePeriodScreen;
