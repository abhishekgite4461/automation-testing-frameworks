const BaseAppStoreScreen = require('../../appStore.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class AppStoreScreen extends BaseAppStoreScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'error_logo');
    }
}

module.exports = AppStoreScreen;
