const BaseBottomNavigationBar = require('../../../../common/bottomNavigationBar.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class BottomNavigationBarScreen extends BaseBottomNavigationBar {
    constructor() {
        super();
        this.homeTab = helper.byResourceId(ViewClasses.VIEW_GROUP, 'navigationTabHome');
        this.applyTab = helper.byResourceId(ViewClasses.VIEW_GROUP, 'navigationTabApply');
        this.payAndTransferTab = helper.byResourceId(ViewClasses.VIEW_GROUP, 'navigationTabPay');
        this.supportTab = helper.byResourceId(ViewClasses.VIEW_GROUP, 'navigationTabSupport');
        this.moreTab = helper.byResourceId(ViewClasses.VIEW_GROUP, 'navigationTabMore');
    }
}

module.exports = BottomNavigationBarScreen;
