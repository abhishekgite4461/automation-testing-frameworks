const BaseCommonScreen = require('../../../common.screen');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');
const helper = require('../../../android.helper');

class CommonScreen extends BaseCommonScreen {
    constructor() {
        super();
        this.closeButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarCloseButton');
        this.modalBackButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarBackButton');
        this.backButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarBackButton');
        this.homeButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarLogoView');
        this.contactUs = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarCallUsButton');
        this.modalCloseIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarCloseButton');
        this.headerBackButton = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarBackButton');
        this.winBackTitle = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'dialogHeaderContainer');
        this.winBackStayButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.winBackLeaveButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.winBackOkButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.winBackCancelButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.winBackCloseButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.loadingSpinner = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogLoadingTitle');
        this.errorMessageOkButton = '//*[@resource-id="android:id/button1"]';
        this.pleaseWaitSpinner = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Please wait');
        this.checkingDetailsSpinner = helper.byPartialText(ViewClasses.TEXT_VIEW, 'checking the account name and details');
        this.accountNameMatch = '//*[contains(@text,"Account name and details confirmed") or contains(@text,"confirmed that account name and details")]';
    }
}

module.exports = CommonScreen;
