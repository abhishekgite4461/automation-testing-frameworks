const BaseActionMenuScreen = require('../../actionMenu.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');
const DeviceHelper = require('../../../../utils/device.helper');

class ActionMenuScreen extends BaseActionMenuScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'tileArrangementActionMenu');
        this.viewTransactionsOption = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuViewTransactionsButton');
        this.transferAndPaymentOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuMoveMoneyButton'
        );
        this.sendMoneyOutsideTheUKOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuInternationalPaymentsButton'
        );
        this.standingOrderOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuStandingOrdersButton'
        );
        this.directDebitOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuDirectDebitsButton'
        );
        this.saveTheChangeOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuSaveTheChangeButton'
        );
        this.changeAccountTypeOption = helper.byContentDesc(
            ViewClasses.TEXT_VIEW,
            'Change account type'
        );
        this.orderPaperStatementOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuOrderPaperStatementButton'
        );
        this.lostOrStolenCardOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuLostOrStolenButton'
        );
        this.replacementCardAndPinOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuCardsAndPinsButton'
        );
        this.viewPendingPaymentOption = helper.byText(ViewClasses.TEXT_VIEW, 'View payments due soon');
        this.renewYourSavingsAccountOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuRenewSavingsAccountButton'
        );
        this.viewInterestRateOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuViewInterestRatesButton'
        );
        this.balanceMoneyTransferOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuBalanceTransfersButton'
        );
        this.manageCreditLimitOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuManageCreditLimitButton'
        );
        this.pdfStatementsOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuPdfStatementsButton'
        );
        this.sendBankDetailsOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuShareAccountDetailsButton'
        );
        this.setupInstallmentOption = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuSetupInstalmentPlanButton');
        this.maintainInstallmentOption = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuManageInstalmentPlansButton');
        this.reactivateIsaOption = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuReactivateIsaButton');
        this.topupIsaOption = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuTopUpIsaButton');
        this.loanRepaymentHolidayOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuRepaymentHolidayButton'
        );
        this.loanClosureOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuLoanClosureButton'
        );
        this.loanBorrowMoreOption = helper.byText(
            ViewClasses.TEXT_VIEW,
            'Borrow more'
        );
        this.loanAnnualStatementsOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuAnnualStatementsButton'
        );
        this.payCreditCardOption = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuPayCreditCardButton');
        this.reactivateISA = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuReactivateIsaButton');
        this.interestDetailRow = helper.byText(ViewClasses.TEXT_VIEW, 'View interest rate details');
        this.loanAdditionalPaymentOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuAdditionalPaymentButton'
        );
        this.manageOverdraftOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuManageOverdraftButton'
        );
        this.applyForOverdraftOption = helper.byContentDesc(
            ViewClasses.LINEAR_LAYOUT,
            'accountTileActionMenuApplyForOverdraftButton'
        );
        this.cardManagementOption = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuCardManagementButton');
        this.chequeDepositOption = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuIcsDepositChequeButton');

        if (DeviceHelper.isUAT()) {
            this.selectViewInterestRateDetails = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuViewInterestRatesButton');
        } else {
            this.selectViewInterestRateDetails = helper.byResourceId(ViewClasses.TEXT_VIEW, 'text1');
        }

        this.dueSoon = helper.byText(ViewClasses.TEXT_VIEW, 'View payments due soon');
        this.viewUpcomingPayment = helper.byText(ViewClasses.TEXT_VIEW, 'View upcoming payments');
        this.actionMenuHeader = helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'accAggExternalAccountActionHeader');
        this.externalAccountActionMenu = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'accAggExternalAccountActionMenu');
        this.externalAccountMenuOptionsLabel = (text) => helper.byText(ViewClasses.TEXT_VIEW, `${text}`);
        this.externalBankAccountNumberOfActionMenuOptions = helper.byResourceId(ViewClasses.TEXT_VIEW, 'tileArrangementMenuItemText');
        this.actionMenuOption = (index) => helper.byResourceIdAndIndex(ViewClasses.TEXT_VIEW, 'tileArrangementMenuItemText', index);
        this.changeAccountTypeOption = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuChangeAccountTypeButton');
        this.makeAnOverpaymentOption = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuMakeOverPaymentButton');
        this.pensionTransferOption = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuPensionTransferButton');
        this.pensionTransfer = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuPensionTransferButton');
        this.viewSpendingInsights = helper.byText(ViewClasses.TEXT_VIEW, 'View spending insights');// TODO: STAMOB-1507 add content desc
        this.actionMenuOptions = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'tileArrangementActionMenu');
        this.shareAccountDetail = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuShareAccountDetailsButton');
        this.standingOrderAndDirectDebitOption = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuStandingOrdersAndDirectDebitsButton');
        this.renameAccountOption = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuRenameAccountButton');
        this.viewSpendingInsightsOption = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuSpendingInsightsButton');
        this.downloadTransactionsOption = helper.byContentDesc(ViewClasses.LINEAR_LAYOUT, 'accountTileActionMenuMonthlyPdfsButton');
    }
}

module.exports = ActionMenuScreen;
