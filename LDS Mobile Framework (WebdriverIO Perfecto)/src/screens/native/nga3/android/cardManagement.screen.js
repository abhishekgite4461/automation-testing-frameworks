const BaseCardManagementScreen = require('../../cardManagement.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class CardManagementScreen extends BaseCardManagementScreen {
    constructor() {
        super();
        this.title = "//*[@text='Card Management']";
        this.freezCardTransactions = helper.byResourceIdWebView(ViewClasses.VIEW, 'cardBlock-link');
        this.lostAndStolenCards = helper.byResourceIdWebView(ViewClasses.VIEW, 'lostAndStolen-link');
        this.replaceCardAndPin = helper.byResourceIdWebView(ViewClasses.VIEW, 'replaceCard-link');
    }
}

module.exports = CardManagementScreen;
