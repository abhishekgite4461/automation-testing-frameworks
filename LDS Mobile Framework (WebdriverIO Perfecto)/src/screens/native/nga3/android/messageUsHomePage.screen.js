const BaseMessageUsHomePageScreen = require('../../messageUsHomePage.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class MessageUsHomePageScreen extends BaseMessageUsHomePageScreen {
    constructor() {
        super();
        this.title = helper.byPartialContentDesc(ViewClasses.TEXT_VIEW, 'Need help? Message us');
        this.errorMessage = helper.byText(ViewClasses.TEXT_VIEW, 'Sorry');
        this.chatCloseButton = helper.byContentDesc(ViewClasses.IMAGE_VIEW, 'Close');
        this.chatSendButton = helper.byResourceId(ViewClasses.BUTTON, 'lpui_enter_message_send');
        this.typeYourQuestionField = helper.byResourceId(ViewClasses.EDIT_TEXT, 'lpui_enter_message_text');
        this.chatMessageTimeStamp = helper.byResourceId(ViewClasses.TEXT_VIEW, 'lpui_message_timestamp');
    }
}

module.exports = MessageUsHomePageScreen;
