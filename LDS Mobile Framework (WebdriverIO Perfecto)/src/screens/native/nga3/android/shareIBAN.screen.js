const BaseShareIBAN = require('../../shareIBAN.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

// TODO: Created JIRA ticket-ENG-2195 to give valid element ID as per coding standards which will be fixed by devs

class shareIBAN extends BaseShareIBAN {
    constructor() {
        super();
        this.shareIbanPageTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsHeaderText');
        this.ibanwarning = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsHeaderText');
        this.nameUKaccount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsAccountNameLabel');
        this.nameUKaccountValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsAccountName');
        this.sortCode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsSortCodeLabel');
        this.sortCodeValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsSortCode');
        this.accountNumber = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsAccountNumberLabel');
        this.accountNumberValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsAccountNumber');
        this.nameIntAccount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsIBanAccountNameLabel');
        this.nameIntAccountValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsIBanAccountName');
        this.iban = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsIBanNumberLabel');
        this.ibanValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsIBanNumber');
        this.bic = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsBicNumberLabel');
        this.bicValue = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsBicNumber');
        this.sendUKDetailsButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsShareBankDetails');
        this.sendIntDetailsButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsShareInternationalBankDetails');
        this.closeIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarCloseButton');
        this.ibanErrorMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'shareAccDetailsError');
        this.bicSearchMessage = helper.byText(ViewClasses.TEXT_VIEW, 'View your Bank Identifier Code (BIC)');
        this.ibanSearchMessage = helper.byText(ViewClasses.TEXT_VIEW, 'View your International Bank Account Number (IBAN)');
    }
}

module.exports = shareIBAN;
