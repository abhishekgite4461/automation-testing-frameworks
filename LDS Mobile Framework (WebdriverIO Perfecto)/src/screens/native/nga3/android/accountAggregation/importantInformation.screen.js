const BaseImportantInformationScreen = require('../../../accountAggregation/importantInformation.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ImportantInformationScreen extends BaseImportantInformationScreen {
    constructor() {
        super();
        this.importantInformationBankImage = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'accAggConsentBankImage');
        this.importantInformationGraphicImage = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'accAggConsentGraphicImage');
        this.importantInformationSelectedBankImage = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'accAggConsentSelectedBankImage');
        this.importantInformationText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggConsentHeadingText');
        this.importantInformationFirstBulletDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggConsentFirstBulletDescription');
        this.importantInformationSecondBulletDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggConsentSecondBulletDescription');
        this.importantInformationThirdBulletDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggConsentThirdBulletDescription');
        this.importantInformationForthBulletDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggConsentForthBulletDescription');
        this.importantInformationTnC = helper.byResourceId(ViewClasses.CHECK_BOX, 'accAggConsentTandCCheckBox');
        this.importantInformationTnCDesciption = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggConsentTandCDescription');
        this.importantInformationConsent = helper.byResourceId(ViewClasses.CHECK_BOX, 'accAggConsentCheckBox');
        this.importantInformationConsentDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggConsentDataPrivacyDescription');
        this.importantInformationConfirmButton = helper.byResourceId(ViewClasses.BUTTON, 'accAggConsentConfirmButton');
        this.importantInformationRenewConsent = helper.byResourceId(ViewClasses.CHECK_BOX, 'accAggConsentCheckBox');
    }
}

module.exports = ImportantInformationScreen;
