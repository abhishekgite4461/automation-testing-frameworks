const BaseTransactionsScreen = require('../../../accountAggregation/transactions.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class TransactionsScreen extends BaseTransactionsScreen {
    constructor() {
        super();
        this.viewTransactionsPage = '//android.view.View[contains(@text,"View Transactions") or contains(@content-desc,"View Transactions")]';
        this.successDeepLink = helper.byText(ViewClasses.VIEW, 'Renew Consent- Natwest');
        this.failureDeepLink = helper.byText(ViewClasses.VIEW, 'Renew Consent Failure');
        this.loggedOffError = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenTitle');
    }
}

module.exports = TransactionsScreen;
