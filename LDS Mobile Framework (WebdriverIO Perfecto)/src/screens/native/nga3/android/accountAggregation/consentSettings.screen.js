const BaseConsentSettingsScreen = require('../../../accountAggregation/consentSettings.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ConsentSettingsScreen extends BaseConsentSettingsScreen {
    constructor() {
        super();
        this.consentSettingsPage = "//*[contains(@resource-id, 'accAggManageConsentsListView')]";
        this.mainHeaderTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggRemoveProviderHeader');
        this.introductoryPara = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggRemoveProviderBody');
        this.accountsSectionHeader = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggRemoveProviderListTitle');
        this.consentProviderLabel = (bankName) => `//*[contains(@resource-id,
         'accAggManageConsentsProviderNameHeaderText') and @text='${bankName}']`;
        this.consentExpiryMessage = (bankName) => this.consentSettingsLocator(bankName, 'accAggConsentExpireText');
        this.providerManageButton = (bankName) => this.consentSettingsLocator(bankName, 'accAggConsentManageBtn');
        this.authorisedAccounts = (bankName) => this.consentSettingsLocator(bankName, 'accAggManageConsentAccountName');
        this.consentStatusFailedMessage = (bankName) => this.consentSettingsLocator(bankName, 'accAggManageConsentsConsentDetailsNotAvailableText');
        this.consentStatusExpiredMessage = (bankName) => this.consentSettingsLocator(bankName, 'accAggManageConsentsConsentExpiredText');
        this.consentStatusRevokedMessage = (bankName) => this.consentSettingsLocator(bankName, 'accAggManageConsentsConsentRemovedText');
        this.providerManageDrawer = helper.byResourceId(ViewClasses.VIEW_GROUP, 'accAggManageConsentsActionMenu');
        this.providerManageDrawerHeader = (bankName) => `//*[contains(@resource-id, 'accAggManageConsentsActionHeader') 
            and @text='${bankName}']`;
        this.providerManageDrawerList = "//*[contains(@resource-id, 'accAggManageConsentsActionMenu')]"
            + "/child::*//*[contains(@resource-id, 'tileArrangementMenuItemText')]";
        this.providerManageDrawerOption = (option) => `${this.providerManageDrawerList}[@text='${option}']`;
        this.removeConsentAlert = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'accAggRemoveConsentSuccessWinBackId');
        this.removeConsentAlertHeader = `//*[contains(@resource-id, 'accAggRemoveConsentSuccessWinBackId')]
        /child::*//*[contains(@resource-id, 'dialogTitle')]`;
        this.consentSettingsLocator = (bankName, loc) => `//*[contains(@resource-id,
            'accAggManageConsentsProviderNameHeaderText') and 
            @text='${bankName}']/following-sibling::*//*[contains(@resource-id,'${loc}')]`;
        this.consentSettingErrorBanner = helper.byResourceId(ViewClasses.TEXT_VIEW, 'notificationContentTextView');
        this.consentSettingErrorBannerCloseIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'notificationCloseImageView');
    }
}

module.exports = ConsentSettingsScreen;
