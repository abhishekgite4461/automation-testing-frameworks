const BaseAccountProviderScreen = require('../../../accountAggregation/accountProvider.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class AccountProviderScreen extends BaseAccountProviderScreen {
    constructor() {
        super();
        this.accountProviderTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggProvidersPageTitle');
        this.accountProviderListView = helper.byResourceId(ViewClasses.RECYCLER_VIEW, 'accAggProvidersListView');
        this.accountProviderTextView = helper.byResourceIdAndIndex(ViewClasses.TEXT_VIEW, 'accAggProviderItemTextView', 1);
        this.accountProviderImageView = helper.byResourceIdAndIndex(ViewClasses.IMAGE_VIEW, 'accAggProviderItemImageView', 1);
        this.providerBankName = (bankName) => helper.byText(ViewClasses.TEXT_VIEW, `${bankName}`);
        this.accountProviderFooterTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggProvidersFooterTitle');
        this.accountProviderFooterMessage = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggProvidersFooterMessage');
    }
}

module.exports = AccountProviderScreen;
