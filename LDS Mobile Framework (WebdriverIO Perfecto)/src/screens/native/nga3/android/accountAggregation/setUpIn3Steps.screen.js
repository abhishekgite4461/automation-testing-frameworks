const BaseSetUpIn3StepsScreen = require('../../../accountAggregation/setUpIn3Steps.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class SetUpIn3StepsScreen extends BaseSetUpIn3StepsScreen {
    constructor() {
        super();
        this.setUpIn3StepsTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggInstFirstDescriptionLabel');
        this.setUpIn3StepsNextButton = helper.byResourceId(ViewClasses.BUTTON, 'accAggInstSelectProviderButton');
        this.setUpIn3StepsProviderImage = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'accAggInstBankImageView');
        this.setUpIn3StepsProviderDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggInstFirstDescriptionLabel');
        this.setUpIn3StepsLoginProviderImage = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'accAggInstTabletImageView');
        this.setUpIn3StepsLoginProviderDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggInstSecondDescriptionLabel');
        this.setUpIn3StepsProviderPermissionImage = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'accAggInstSecureImageView');
        this.setUpIn3StepsProviderPermissionDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggInstThirdDescriptionLabel');
    }
}

module.exports = SetUpIn3StepsScreen;
