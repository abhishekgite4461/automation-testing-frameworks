const BaseOpenBankingScreen = require('../../../accountAggregation/openBanking.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class OpenBankingScreen extends BaseOpenBankingScreen {
    constructor() {
        super();
        this.screenTitle = (screenTitle) => `//*[contains(@resource-id,"navigationToolbarText") 
            and contains(@text, "${screenTitle}")]`;
        this.openBankingImage = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'accAggInfoOnboardingImage');
        this.openBankingInformationHeader = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggInfoHeader');
        this.openBankingInformationHeaderDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggInfoHeadingDescription');
        this.openBankingNextButton = helper.byResourceId(ViewClasses.BUTTON, 'accAggInfoConfirmButton');
        this.backIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarBackButton');
        this.crossIcon = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarCloseButton');
    }
}

module.exports = OpenBankingScreen;
