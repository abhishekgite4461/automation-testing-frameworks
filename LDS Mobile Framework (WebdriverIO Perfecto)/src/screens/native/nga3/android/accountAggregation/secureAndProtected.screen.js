const BaseSecureAndProtectedScreen = require('../../../accountAggregation/secureAndProtected.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class SecureAndProtectedScreen extends BaseSecureAndProtectedScreen {
    constructor() {
        super();
        this.secureAndProtectedTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggSecurityHeader');
        this.secureAndProtectedFirstInformationPoint = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'accAggSecurityfirstInfo');
        this.secureAndProtectedSecondInformationPoint = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'accAggSecuritySecondInfo');
        this.secureAndProtectedThirdInformationPoint = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'accAggSecurityThirdInfo');
        this.secureAndProtectedNextButton = helper.byResourceId(ViewClasses.BUTTON, 'accAggSecurityNextButton');
    }
}

module.exports = SecureAndProtectedScreen;
