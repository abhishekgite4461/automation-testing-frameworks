const BaseRedirectingScreen = require('../../../accountAggregation/redirecting.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class RedirectingScreen extends BaseRedirectingScreen {
    constructor() {
        super();
        this.redirectLoadingProgress = helper.byResourceId(ViewClasses.PROGRESS_BAR, 'accAggRedirectLoadingProgress');
        this.redirectText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggRedirectText');
        this.continueButton = helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.cancelButton = helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.loggingOffUserErrorScreen = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'loggedOutErrorScreen');
        this.logOffErrorScreenTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'errorScreenTitle');
        this.redirectHomeButton = helper.byResourceId(ViewClasses.BUTTON, 'accAggRedirectHomeButton');
        this.accessLocationButton = helper.byText('Block');
        this.redirectExternalBrowserErrorScreen = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggRedirectIncompleteDescriptionText');
        this.redirectExternalBrowserErrorScreenTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accAggRedirectIncompleteInfoText');
        this.digitalInbox = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogContentText');
    }
}

module.exports = RedirectingScreen;
