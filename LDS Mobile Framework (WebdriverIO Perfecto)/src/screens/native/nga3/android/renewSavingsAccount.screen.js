const BaseRenewSavingsAccountScreen = require('../../renewSavingsAccount.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class RenewSavingsAccountScreen extends BaseRenewSavingsAccountScreen {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.VIEW, 'Select a savings account to renew');
        this.renew = helper.byResourceIdWebView(ViewClasses.VIEW, 'chooseFromAccounts:0:lnkConvertChooseFromNgb');
        this.renewAccountPage = helper.byText(ViewClasses.VIEW, 'Renew your account');
        this.savingAccount = helper.byResourceIdWebView(ViewClasses.VIEW, 'frmConvertForm:productDetailsNGB:0:productdisplayNGB');
        this.renewSaving = helper.byResourceIdWebView(ViewClasses.BUTTON, 'frmConvertForm:productDetailsNGB:0:btnConvertNGBSMR');
        this.applyNow = helper.byText(ViewClasses.BUTTON, 'Apply now');
        this.confirmPage = helper.byText(ViewClasses.VIEW, 'Confirm your selection');
        this.confirm = helper.byText(ViewClasses.BUTTON, 'Confirm renewal');
        this.termsAndConditions = '//android.widget.CheckBox';
        this.successPage = helper.byPartialText(ViewClasses.VIEW, 'successfully renewed your savings account');
    }
}

module.exports = RenewSavingsAccountScreen;
