const BasePayUkNumberPopupScreen = require('../../../p2pRegistration/payUkNumberPopup.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class PayUkNumberPopupScreen extends BasePayUkNumberPopupScreen {
    constructor() {
        super();
        // TODO add accessibility IDs for the dialog title for pop up MOB3-10328
        this.title = helper.byContentDesc(ViewClasses.TEXT_VIEW, 'Alert box. Paying a UK mobile number. Heading');
        this.close = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.registerNow = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
    }
}

module.exports = PayUkNumberPopupScreen;
