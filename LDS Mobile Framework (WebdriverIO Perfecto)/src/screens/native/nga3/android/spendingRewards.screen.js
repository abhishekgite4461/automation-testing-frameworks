const BaseSpendingRewardsScreen = require('../../spendingRewards.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class SpendingRewardsScreen extends BaseSpendingRewardsScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'spendingRewardsTitle');
        this.optInEverydayOfferButton = helper.byResourceId(ViewClasses.BUTTON, 'spendingRewardsOptInButton');
        this.activateTheOffers = helper.byResourceId(ViewClasses.TEXT_VIEW, 'spendingRewardsClickTitle');
        this.spendingRewardsShopTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'spendingRewardsShopTitle');
        this.spendingRewardsEnjoyDescription = helper.byResourceId(ViewClasses.TEXT_VIEW, 'spendingRewardsEnjoyDescription');
        this.spendingRewardsOptInButton = helper.byResourceId(ViewClasses.BUTTON, 'spendingRewardsOptInButton');
        this.spendingRewardsNotNowOptInButton = helper.byResourceId(ViewClasses.BUTTON, 'spendingRewardsNotNowOptInButton');
        this.expiredOffers = helper.byResourceIdWebView(ViewClasses.VIEW, 'lnkExpired');
        this.settings = helper.byResourceIdWebView(ViewClasses.VIEW, 'lnkPreferences');
        this.findOutMore = helper.byResourceIdWebView(ViewClasses.VIEW, 'lnkLearnMore');
        this.newTab = helper.byResourceIdWebView(ViewClasses.VIEW, 'txtEnjoyOffersSSR');
        this.activeOffers = helper.byResourceIdWebView(ViewClasses.VIEW, 'lnkShop');
        this.activeTab = helper.byResourceIdWebView(ViewClasses.VIEW, 'txtActiveSSR');
        this.selectEverydayOffersLink = helper.byResourceIdWebView(ViewClasses.VIEW, 'lnkSmartRewardsOffersStateTwo');
        this.expiredOffersPageTitle = helper.byPartialText(ViewClasses.VIEW, 'Expired offers');
        this.settingPageTitle = helper.byPartialText(ViewClasses.VIEW, 'Change your Everyday Offers settings');
        this.findOutMorePageTitle = helper.byPartialText(ViewClasses.VIEW, 'Change your Everyday Offers settings');
    }
}

module.exports = SpendingRewardsScreen;
