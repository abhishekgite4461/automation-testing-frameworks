const BaseForgottenLoginDetailsScreen = require('../../../web/preforgottenLoginDetails.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class ForgottenLoginDetailsScreen extends BaseForgottenLoginDetailsScreen {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.VIEW, "Make sure you've got");
        this.closeButton = helper.byText(ViewClasses.BUTTON, 'Close');
        this.pageTitle = helper.byText(ViewClasses.VIEW, 'FORGOTTEN YOUR SIGN-IN DETAILS?');
    }
}

module.exports = ForgottenLoginDetailsScreen;
