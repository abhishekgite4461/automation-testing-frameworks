const BaseReEnterPasswordScreen = require('../../reEnterPassword.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class ReEnterPasswordScreen extends BaseReEnterPasswordScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'loginTitle');
        this.reEnterPasswordField = helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText');
        this.continueButton = helper.byResourceId(ViewClasses.BUTTON, 'enterMiPasswordContinueButton');
        this.fscsTile = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'enterMiFscsTile');
    }
}

module.exports = ReEnterPasswordScreen;
