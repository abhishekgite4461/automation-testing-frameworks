const BaseLeadsScreen = require('../../leads.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class LeadsScreen extends BaseLeadsScreen {
    constructor() {
        super();
        this.leadsholder = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'interstitialContainer');
        this.leadsButton = helper.byResourceId(ViewClasses.BUTTON, 'interstitialLeadContinueButton');
        this.endOfPageNotDisplayedAlert = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'endOfPageNotDisplayedModal');
        this.endOfThePage = helper.byText(ViewClasses.WEB_VIEW, '9. GOVERNING LAW AND JURISDICTIONThese Terms shall be '
            + 'governed by and construed in accordance with English law. Disputes arising in connection with these Terms '
            + 'shall be subject to the exclusive jurisdiction of the English courts.');
        this.bigPromptEndOfPage = '//android.webkit.WebView[1]/android.view.View[14]';
        this.bigPromptLink = '//android.webkit.WebView[1]/android.view.View[3]/android.view.View[2]';
        this.highestPriorityLead = '//android.webkit.WebView[1]/android.view.View[3]';
        this.productHubLead = helper.byPartialText(ViewClasses.VIEW, 'nga:product-hub');
        this.callUsLead = helper.byPartialText(ViewClasses.VIEW, 'nga:callus');
        this.personalDetailsLead = helper.byPartialText(ViewClasses.VIEW, 'nga:update-personal-details');
        this.updatePhoneLead = helper.byPartialText(ViewClasses.VIEW, 'nga:update-personal-details-phone');
        this.updateAddressLead = helper.byPartialText(ViewClasses.VIEW, 'nga:update-personal-details-address');
        this.spendingRewardsRegistrationLead = helper.byPartialText(ViewClasses.VIEW, 'nga:spending-rewards-registration');
        this.securitySettingsLead = helper.byPartialText(ViewClasses.VIEW, 'nga:security-settings');
        this.callUsInternetBankingLead = helper.byPartialText(ViewClasses.VIEW, 'nga:call-us-internet-banking');
        this.callUsOtherBankingQueryLead = helper.byPartialText(ViewClasses.VIEW, 'nga:call-us-other-banking-query');
        this.callUsLostOrStolenLead = helper.byPartialText(ViewClasses.VIEW, 'nga:call-us-lost-or-stolen');
        this.callUsSuspectedFraudLead = helper.byPartialText(ViewClasses.VIEW, 'nga:call-us-suspected-fraud');
        this.callUsMedicalAssistanceLead = helper.byPartialText(ViewClasses.VIEW, 'nga:call-us-medical-assistance');
        this.callUsEmergencyCashLead = helper.byPartialText(ViewClasses.VIEW, 'nga:call-us-emergency-cash');
        this.callUsNewCurrentAccountLead = helper.byPartialText(ViewClasses.VIEW, 'nga:call-us-new-current-account');
        this.callUsNewCreditCardLead = helper.byPartialText(ViewClasses.VIEW, 'nga:call-us-new-credit-card');
        this.callUsNewIsaAccountLead = helper.byPartialText(ViewClasses.VIEW, 'nga:call-us-new-isa-account');
        this.callUsNewLoanLead = helper.byPartialText(ViewClasses.VIEW, 'nga:call-us-new-loan');
        this.callUsNewMortgageLead = helper.byPartialText(ViewClasses.VIEW, 'nga:call-us-new-mortgage');
        this.callUsNewSavingsLead = helper.byPartialText(ViewClasses.VIEW, 'nga:call-us-new-savings');
        this.asm1Lead = '//*[@resource-id="asm"]';
        this.addExternalAccountLink = helper.byText(ViewClasses.VIEW, 'Add External Account Now');
    }
}

module.exports = LeadsScreen;
