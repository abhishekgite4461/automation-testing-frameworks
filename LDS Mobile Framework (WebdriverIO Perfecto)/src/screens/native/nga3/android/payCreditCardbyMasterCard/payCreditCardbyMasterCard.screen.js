const BasePayCCbyMasterCardScreen = require('../../../payCreditCardbyMasterCard/payCreditCardbyMasterCard.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class PayCCbyMasterCardScreen extends BasePayCCbyMasterCardScreen {
    constructor() {
        super();
        this.anotherUkBankAccount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'ukBankProviderTitle');
        this.dismissErrorBanner = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'notificationCloseImageView');
        this.selectProviderButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubContinueButton');
        this.selectedBankName = (bankName) => helper.byPartialText(ViewClasses.TEXT_VIEW, `${bankName}`);
        this.editButton = helper.byResourceId(ViewClasses.BUTTON, 'editPaymentReviewButton');
        this.confirmButton = helper.byResourceId(ViewClasses.BUTTON, 'paymentHubReviewConfirmButton');
        this.confirmPaymentCheckBox = helper.byResourceId(ViewClasses.CHECK_BOX, 'paymentHubReviewWarningConfirmationCheckBox');
        this.impInfoScreenCheckBox = helper.byResourceId(ViewClasses.CHECK_BOX, 'masterCardConsentTermsAndConditionsPrivacyCheckBox');
        this.impInfoToBankLogo = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'masterCardConsentToBankImage');
        this.impInfoBankLogo = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'masterCardConsentBankImage');
        this.continueBtn = helper.byResourceId(ViewClasses.BUTTON, 'continue_button');
        // TOD0 PJO-9060 add id locator
        this.providerListScreen = '//*[@text="Your UK Bank Account"]';
        this.paybyUKdebitCard = '//*[@text="Use your UK Debit card"]';
        this.reviewPaymentPage = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Confirm payment');
    }
}

module.exports = PayCCbyMasterCardScreen;
