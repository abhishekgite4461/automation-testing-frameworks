const BaseDeleteDirectDebitInfoScreen = require('../../../amendOrDeleteStandingOrder/deleteDirectDebitInfo.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class DeleteDirectDebitInfoScreen extends BaseDeleteDirectDebitInfoScreen {
    constructor() {
        super();
        this.deleteDDInfoPage = helper.byPartialResourceId(ViewClasses.TEXT_VIEW, 'directDebitCancellationTitle');
        this.getHelpButton = helper.byPartialText(ViewClasses.BUTTON, 'Get help'); // TODO:PJO-9503
        this.deleteDDButton = helper.byPartialText(ViewClasses.BUTTON, 'Delete');
        this.keepDDButton = helper.byPartialText(ViewClasses.BUTTON, 'Keep');
    }
}

module.exports = DeleteDirectDebitInfoScreen;
