const BaseStagingOrderWebviewScreen = require('../../../amendOrDeleteStandingOrder/standingOrderWebview.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class StandingOrderWebviewScreen extends BaseStagingOrderWebviewScreen {
    constructor() {
        super();
        this.webviewTitle = helper.byText(ViewClasses.VIEW, 'Set up a standing order');
    }
}

module.exports = StandingOrderWebviewScreen;
