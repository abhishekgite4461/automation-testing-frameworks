const BaseNativeAmendStandingOrderScreen = require('../../../amendOrDeleteStandingOrder/nativeAmendStandingOrder.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class NativeAmendStandingOrderScreen extends BaseNativeAmendStandingOrderScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'scheduledPaymentDetailsContainer');
        this.standingOrderDelete = helper.byResourceId(ViewClasses.BUTTON, 'scheduledPaymentDetailsDeleteButton');
        this.standingOrderAmend = helper.byResourceId(ViewClasses.BUTTON, 'scheduledPaymentDetailsAmendButton');
        this.MerchantVTD = helper.byResourceId(ViewClasses.TEXT_VIEW, 'scheduledPaymentDetailsRecipient');
        this.AmountVTD = helper.byResourceId(ViewClasses.TEXT_VIEW, 'scheduledPaymentDetailsAmount');
        this.accountNoVTD = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'scheduledPaymentDetailsAccountDetails');
        this.referenceVTD = helper.byResourceId(ViewClasses.TEXT_VIEW, 'scheduledPaymentDetailsReference');
        this.FrequencyVTD = helper.byResourceId(ViewClasses.TEXT_VIEW, 'scheduledPaymentDetailsFrequency');
        this.NextPaid = helper.byResourceId(ViewClasses.TEXT_VIEW, 'scheduledPaymentDetailsNextPayment');
        this.endDate = helper.byResourceId(ViewClasses.TEXT_VIEW, 'scheduledPaymentDetailsPaymentEnd');
    }
}

module.exports = NativeAmendStandingOrderScreen;
