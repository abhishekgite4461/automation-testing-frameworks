const BaseDirectDebitVTDScreen = require('../../../amendOrDeleteStandingOrder/directDebitVTD.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class DirectDebitVTDScreen extends BaseDirectDebitVTDScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'title');
        this.directDebitDelete = helper.byResourceId(ViewClasses.BUTTON, 'scheduledPaymentDetailsDeleteButton');
        this.merchantVTD = helper.byResourceId(ViewClasses.TEXT_VIEW, 'scheduledPaymentDetailsRecipient');
        this.amountVTD = helper.byResourceId(ViewClasses.TEXT_VIEW, 'scheduledPaymentDetailsAmount');
        this.frequencyVTD = helper.byResourceId(ViewClasses.TEXT_VIEW, 'scheduledPaymentDetailsFrequency');
        this.lastPaid = helper.byResourceId(ViewClasses.TEXT_VIEW, 'scheduledPaymentDetailsLastPayment');
        this.nextPaid = helper.byResourceId(ViewClasses.TEXT_VIEW, 'scheduledPaymentDetailsNextPayment');
        this.referenceVTD = helper.byResourceId(ViewClasses.TEXT_VIEW, 'scheduledPaymentDetailsReference');
    }
}

module.exports = DirectDebitVTDScreen;
