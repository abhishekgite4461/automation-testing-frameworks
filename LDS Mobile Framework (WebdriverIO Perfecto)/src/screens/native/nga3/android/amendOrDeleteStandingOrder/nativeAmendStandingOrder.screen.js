const BaseNativeAmendStandingOrderScreen = require('../../../amendOrDeleteStandingOrder/nativeAmendStandingOrder.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class NativeAmendStandingOrderScreen extends BaseNativeAmendStandingOrderScreen {
    constructor() {
        super();
        this.pageTitle = (screenTitle) => `//*[contains(@resource-id,'navigationToolbarText') 
            and contains(@text, '${screenTitle}')]`;
        this.selectedStandingOrderAccount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'arrangementTitle');
        this.deleteButtonForStandingOrder = (standingOrderName) => `${this.accountNameForStandingOrder(standingOrderName)}
            /following-sibling::*[contains(@resource-id,'standing_order_delete')]`;
        this.amendButtonForStandingOrder = (standingOrderName) => `${this.accountNameForStandingOrder(standingOrderName)}
            /following-sibling::*[contains(@resource-id,'standing_order_update')]`;
        this.accountNameForStandingOrder = (standingOrderName) => `//*[contains(@resource-id,'standing_order_name') 
            and @text='${standingOrderName}']`;
        this.setUpStandingOrderButton = helper.byText(ViewClasses.TEXT_VIEW, 'Setup Standing Order');// TODO: PAYMOB-2373
        this.nativePaymentHubActionMenu = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'navigationToolbarModalActionMenu');
        this.standingOrdesTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'standingOrderHeader');
        this.directDebitsTitle = helper.byResourceId('Direct Debits');
        this.deleteOption = helper.byResourceId('Delete');
        this.nextDueDateDD = helper.byResourceId(ViewClasses.TEXT_VIEW, 'scheduled_payment_next_due_date');
        this.merchantName = helper.byResourceId('directDebitTitle');
        this.amount = helper.byResourceId('directDebitAmount');
        this.frequency = helper.byResourceId('directDebitFrequency');
        this.noStandingOrdersTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'noStandingOrdersTitle');
        this.noDirectDebitsHeader = helper.byResourceId(ViewClasses.TEXT_VIEW, 'noScheduledPaymentMessage');
        this.unableToLoadDirectDebits = helper.byPartialText(ViewClasses.TEXT_VIEW, 'show Direct Debit details right now. Please try again later');
        this.winbackText = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogContentText');
        this.firstSOAmount = helper.byResourceId(ViewClasses.TEXT_VIEW, 'scheduled_payment_amount');// TODO: PAYMOB-2352
        this.firstStandingOrder = helper.byResourceId(ViewClasses.VIEW_GROUP, 'nativeStandingOrderItem');
        this.firstDirectDebit = '//*[@content-desc="Direct Debits. Heading."]/following::*[contains(@resource-id,"nativeStandingOrderItem")][1]';
    }
}

module.exports = NativeAmendStandingOrderScreen;
