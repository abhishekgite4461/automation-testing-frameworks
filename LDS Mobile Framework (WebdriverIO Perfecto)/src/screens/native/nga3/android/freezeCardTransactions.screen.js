const BaseFreezeCardTransactions = require('../../freezeCardTransactions.screen');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

class FreezeCardTransactions extends BaseFreezeCardTransactions {
    constructor() {
        super();
        // TODO DPLDA-175 Need to migrate into webview approach
        this.title = '//android.view.View[contains(@text,"Card Freezes") or contains(@content-desc,"Card Freezes")]';
        this.abroad = helper.byPartialResourceIdWebView(ViewClasses.BUTTON, 'TCT_CROSS_BORDER-STATE');
        this.online = helper.byPartialResourceIdWebView(ViewClasses.BUTTON, 'TCT_E_COMMERCE-STATE');
        this.terminals = helper.byPartialResourceIdWebView(ViewClasses.BUTTON, 'TCT_BRICK_AND_MORTAR-STATE');
        this.reportLostAndStolen = helper.byResourceIdWebView(ViewClasses.BUTTON, 'Lost_Stolen_Link');
        this.freezeGambling = helper.byPartialText(ViewClasses.BUTTON, 'Freeze gambling');
        this.waitForReadyToFreezePage = helper.byPartialText(ViewClasses.VIEW, 'to stop your gambling transactions');
        this.freezeButtonGambling = helper.byResourceIdWebView(ViewClasses.BUTTON, 'freezeButton');
        this.cancelFreezeButton = helper.byResourceIdWebView(ViewClasses.BUTTON, 'areYouSureNoButton');
    }
}

module.exports = FreezeCardTransactions;
