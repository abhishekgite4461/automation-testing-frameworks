const BaseLostAndStolenCard = require('../../lostAndStolenCard.screen.js');
const helper = require('../../android.helper');
const ViewClasses = require('../../../../enums/androidViewClass.enum');

// TODO DPLDA-175 Need to migrate into webview approach
class LostAndStolenCard extends BaseLostAndStolenCard {
    constructor() {
        super();
        this.title = '//android.view.View[contains(@text,"Lost or stolen cards") or contains(@content-desc,"Lost or stolen cards")]';
        this.lost = helper.byResourceIdWebView(ViewClasses.RADIO_BUTTON, 'frmLostAndStolenCards:cardLostStolen:0');
        this.cancelCardDebit = helper.byResourceIdWebView(ViewClasses.CHECK_BOX, 'frmLostAndStolenCards:debitCardaccountList:0:debitCardList:0:cancelCard1');
        this.cancelCardCredit = helper.byResourceIdWebView(ViewClasses.CHECK_BOX, 'frmLostAndStolenCards:creditCardAccountList:0:creditCardList:0:cancelCard1');
        this.newPin = helper.byResourceIdWebView(ViewClasses.CHECK_BOX, 'frmLostAndStolenCards:debitCardaccountList:0:debitCardList:0:newPin1');
        this.pin = helper.byResourceIdWebView(ViewClasses.CHECK_BOX, 'frmOrderCollateral:creditCardAccountList:0:creditCards:0:newPin1');
        this.continue = helper.byPartialResourceIdWebView(ViewClasses.BUTTON, 'btnDoConfirmcancelCard');
        this.confirmTitle = helper.byText(ViewClasses.VIEW, 'Confirm your selections');
        this.password = helper.byResourceIdWebView(ViewClasses.EDIT_TEXT, 'lostAndStolenConfirm:txtAuthPwd');
    }
}

module.exports = LostAndStolenCard;
