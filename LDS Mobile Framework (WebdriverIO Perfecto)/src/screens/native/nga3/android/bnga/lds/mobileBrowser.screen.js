const BaseMobileBrowserScreen = require('../../mobileBrowser.screen');

class mobileBrowserScreen extends BaseMobileBrowserScreen {
    constructor() {
        super();
        this.brandUrl = 'onlinebusiness.lloydsbank.co.uk';
    }
}

module.exports = mobileBrowserScreen;
