const BaseStandingOrderScreen = require('../../../paymentHub/standingOrder.screen');
const helper = require('../../../../../android.helper');
const ViewClasses = require('../../../../../../../enums/androidViewClass.enum');

class StandingOrderScreen extends BaseStandingOrderScreen {
    constructor() {
        super();
        this.savingAccountName = helper.byText(ViewClasses.VIEW, 'BB INST ONLINE');
    }
}

module.exports = StandingOrderScreen;
