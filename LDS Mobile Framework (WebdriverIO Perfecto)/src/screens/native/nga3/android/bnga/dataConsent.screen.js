const BaseDataConsent = require('../../../dataConsent.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class DataConsent extends BaseDataConsent {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.TEXT_VIEW, 'Data consent. Heading.  ');
        this.performanceConsentToggleON = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mandatorySelectSwitchLeft');
        this.performanceConsentToggleOFF = helper.byResourceId(ViewClasses.TEXT_VIEW, 'mandatorySelectSwitchRight');
        this.confirmConsent = helper.byResourceId(ViewClasses.BUTTON, 'analyticsConsentConfirmButton');
        this.performanceConsent = helper.byResourceId(ViewClasses.LINEAR_LAYOUT, 'mandatorySelectOptionButton');
        this.winBackTitle = helper.byText(ViewClasses.TEXT_VIEW, 'Leave the app to view page?');
        this.dialogTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
    }
}

module.exports = DataConsent;
