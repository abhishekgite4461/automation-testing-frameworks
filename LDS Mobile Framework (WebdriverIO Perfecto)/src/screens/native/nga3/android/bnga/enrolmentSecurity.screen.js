const BaseEnrolmentSecurityScreen = require('../../../enrolmentSecurity.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class EnrolmentSecurityScreen extends BaseEnrolmentSecurityScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationToolbarText');
        this.continueButton = helper.byResourceId(ViewClasses.BUTTON, 'saveButton');
        this.defaultTime = helper.byPartialText(ViewClasses.TEXT_VIEW, 'After 10 minutes of inactivity');
    }
}

module.exports = EnrolmentSecurityScreen;
