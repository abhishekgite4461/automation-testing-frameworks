const BaseDataPrivacyScreen = require('../../../dataPrivacy.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class DataPrivacyScreen extends BaseDataPrivacyScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationToolbarText');
        this.dataPrivacyNotice = helper.byResourceId(ViewClasses.TEXT_VIEW, 'welcomeDataPrivacyText1');
        this.cookiePolicy = helper.byResourceId(ViewClasses.TEXT_VIEW, 'welcomeDataPrivacyText2');
    }
}

module.exports = DataPrivacyScreen;
