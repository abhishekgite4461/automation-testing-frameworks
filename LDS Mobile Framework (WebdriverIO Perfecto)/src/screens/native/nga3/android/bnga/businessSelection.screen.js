const BaseBusinessSelectionScreen = require('../../../businessSelection.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class BusinessSelectionScreen extends BaseBusinessSelectionScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'businessSelectionSubHeader');
        this.businessName = helper.byResourceId(ViewClasses.TEXT_VIEW, 'businessSelectionNameView');
        this.businessSelectionSignatoryHeader = helper.byText(ViewClasses.TEXT_VIEW, 'Your Businesses');
        this.businessSelectionDelegateHeader = helper.byText(ViewClasses.TEXT_VIEW, 'Delegate and view only Businesses');
        this.firstSignatoryBusiness = `${this.businessSelectionSignatoryHeader}
        /following-sibling::android.widget.TextView[1]`;
        this.firstDelegateBusiness = `${this.businessSelectionDelegateHeader}
        /following-sibling::android.widget.TextView[1]`;
        this.signatoryBusiness = (businessName) => `${this.businessSelectionSignatoryHeader}
        /following-sibling::android.widget.TextView[@text="${businessName}"]`;
        this.delegateBusiness = (businessName) => `${this.businessSelectionDelegateHeader}
        /following-sibling::android.widget.TextView[@text="${businessName}"]`;
        this.firstBusinessName = `${this.businessName}[1]`;
        this.signatoryBusinesses = `${this.businessSelectionDelegateHeader}
        /preceding-sibling::*[not(@text='Your Businesses')]`;
        this.delegateAndViewOnlyBusinesses = `${this.businessSelectionDelegateHeader}
        /following-sibling::*[@text]`;
    }
}

module.exports = BusinessSelectionScreen;
