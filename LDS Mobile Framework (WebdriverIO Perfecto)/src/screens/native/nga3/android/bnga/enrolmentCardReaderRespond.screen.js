const BaseEnrolmentCardReaderRespondScreen = require('../../../enrolmentCardReaderRespond.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class EnrolmentCardReaderRespondScreen extends BaseEnrolmentCardReaderRespondScreen {
    constructor() {
        super();
        this.title = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Registration');
        this.paymentCardReaderAuthenticationTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationToolbarText');
        this.lastFiveCardDigitField = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'card_reader_card_last_5_digits')}//android.widget.EditText`;
        this.challengeCode = helper.byResourceId(ViewClasses.TEXT_VIEW, 'card_reader_challenge_number');
        this.passcodeField = helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'card_reader_passcode');
        this.continueButton = helper.byResourceId(ViewClasses.BUTTON, 'card_reader_continue_button');
        this.havingTroubleWithCardReaderLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'card_reader_trouble_link');
        this.havingTroubleWithCardReaderPopUpTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.havingTroublePopUpCloseButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.havingTroublePopUpContinueButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.invalidCardError = helper.byPartialText(ViewClasses.TEXT_VIEW, '1000022');
        this.expiredCardError = helper.byPartialText(ViewClasses.TEXT_VIEW, '1000022');
        this.incorrectPasscodeError = helper.byPartialText(ViewClasses.TEXT_VIEW, '1000019');
        this.cardAlreadyExistError = helper.byPartialText(ViewClasses.TEXT_VIEW, '1000021');
    }
}

module.exports = EnrolmentCardReaderRespondScreen;
