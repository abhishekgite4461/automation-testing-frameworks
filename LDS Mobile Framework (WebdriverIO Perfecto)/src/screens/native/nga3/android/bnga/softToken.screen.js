const BaseSoftTokenScreen = require('../../../softToken.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class SoftTokenScreen extends BaseSoftTokenScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'softTokenMainBody');
        this.passCode = helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText');
        this.continueButton = helper.byResourceId(ViewClasses.BUTTON, 'softTokenContinueButton');
        this.softTokenInfoLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'softTokenInfoLink');
        this.whatisSoftTokenOverlay = helper.byPartialText(ViewClasses.TEXT_VIEW, 'What is a software token?');
        this.closeButton = helper.byPartialText(ViewClasses.BUTTON, 'Close');
    }
}

module.exports = SoftTokenScreen;
