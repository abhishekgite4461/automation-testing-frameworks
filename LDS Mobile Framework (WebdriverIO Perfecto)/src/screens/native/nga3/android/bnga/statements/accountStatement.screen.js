const BaseAccountStatementScreen = require('../../statements/accountStatement.screen.js');
const helper = require('../../../../android.helper.js');
const ViewClasses = require('../../../../../../enums/androidViewClass.enum.js');

class AccountStatementScreen extends BaseAccountStatementScreen {
    constructor() {
        super();
        this.noteText = helper.byText(ViewClasses.TEXT_VIEW, 'NOTE: Pending cheques will show in your account balance but you can only use the money when it clears.');
        this.pendingChequeTransactionAmount = `${this.noteText}/following::*[@resource-id, ":id/transactionAmount"][1]`;
        this.pendingChequeTransactionDescription = `${this.noteText}/following::*[@resource-id, 
        ":id/transactionDescription"][1]`;
        this.pendingChequeTransactionType = `${this.noteText}/following::*[@text="Pending"][1]`;
        this.overDraftLimit = helper.byResourceId(ViewClasses.TEXT_VIEW, 'accountTileMidValue');
    }
}

module.exports = AccountStatementScreen;
