const BaseStandingOrder = require('../../../standingOrder.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class StandingOrder extends BaseStandingOrder {
    constructor() {
        super();
        this.title = helper.byText(ViewClasses.VIEW, 'Standing orders');
    }
}

module.exports = StandingOrder;
