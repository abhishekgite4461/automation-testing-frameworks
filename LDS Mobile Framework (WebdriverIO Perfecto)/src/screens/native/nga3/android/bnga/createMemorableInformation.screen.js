const BaseCreateMemorableInformationScreen = require('../../../createMemorableInformation.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class CreateMemorableInformationScreen extends BaseCreateMemorableInformationScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'createMiTitle');
        this.enterMI = helper.byPartialContentDesc(ViewClasses.EDIT_TEXT, 'Create your memorable information');
        this.reEnterMI = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'createMiConfirmInputField')}${helper.byResourceId(ViewClasses.EDIT_TEXT, 'inputFieldEditText')}`;
        this.nextButton = helper.byText(ViewClasses.BUTTON, 'Next');
    }
}

module.exports = CreateMemorableInformationScreen;
