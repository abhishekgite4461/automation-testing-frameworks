const BaseEnrolmentCardReaderIdentifyScreen = require('../../../enrolmentCardReaderIdentify.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class EnrolmentCardReaderIdentifyScreen extends BaseEnrolmentCardReaderIdentifyScreen {
    constructor() {
        super();
        this.title = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Confirm identity');
        this.paymentCardReaderAuthenticationTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'navigationToolbarText');
        this.lastFiveCardDigitField = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'card_reader_card_last_5_digits')}//android.widget.EditText`;
        this.passcodeField = `${helper.byResourceId(ViewClasses.FRAME_LAYOUT, 'card_reader_passcode')}//android.widget.EditText`;
        this.continue = helper.byResourceId(ViewClasses.BUTTON, 'card_reader_continue_button');
        this.helpPopUpCancelButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.helpPopUpContinueButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.troubleCardReader = helper.byResourceId(ViewClasses.TEXT_VIEW, 'card_reader_trouble_link');
        this.cancelButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogNegativeAction');
        this.closeErrorBanner = helper.byResourceId(ViewClasses.IMAGE_VIEW, 'notificationCloseImageView');
        this.continueButton = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogPositiveAction');
        this.dialogTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
        this.incorrectPasscodeError = helper.byPartialText(ViewClasses.TEXT_VIEW, 'Sorry, the details you’ve entered are incorrect. Please re-enter your details and try again.');
        this.invalidCardError = helper.byPartialText(ViewClasses.TEXT_VIEW, '1000022');
        this.expiredCardError = helper.byPartialText(ViewClasses.TEXT_VIEW, '1000022');
        this.suspendedCardError = helper.byPartialText(ViewClasses.TEXT_VIEW, '9200161');
        this.oldCardExpiredError = helper.byPartialText(ViewClasses.TEXT_VIEW, '1000023');
        this.cardAlreadyExistError = helper.byPartialText(ViewClasses.TEXT_VIEW, '1000021');
    }
}

module.exports = EnrolmentCardReaderIdentifyScreen;
