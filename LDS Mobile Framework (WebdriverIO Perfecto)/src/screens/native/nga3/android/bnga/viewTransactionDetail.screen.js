const BaseViewTransactionDetailScreen = require('../viewTransactionDetail.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class ViewTransactionDetailScreen extends BaseViewTransactionDetailScreen {
    constructor() {
        super();
        this.debitDisclaimerText = helper.byText(ViewClasses.TEXT_VIEW, '* This transaction has yet to be taken from your account balance '
            + 'but has been deducted from your available funds.');
        this.creditDisclaimerText = helper.byText(ViewClasses.TEXT_VIEW, '* Please note this has yet to be added to your available funds.');
    }
}

module.exports = ViewTransactionDetailScreen;
