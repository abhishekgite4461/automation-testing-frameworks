const BaseWelcomeRegisterScreen = require('../../../welcomeRegister.screen');
const helper = require('../../../android.helper');
const ViewClasses = require('../../../../../enums/androidViewClass.enum');

class WelcomeRegisterScreen extends BaseWelcomeRegisterScreen {
    constructor() {
        super();
        this.title = helper.byResourceId(ViewClasses.TEXT_VIEW, 'welcomeYouNeedBankingDetailsTitle');
        this.getStartedButton = helper.byResourceId(ViewClasses.BUTTON, 'welcomeRegisterGetStartedButton');
        this.onlineBankingRegistrationLink = helper.byResourceId(ViewClasses.TEXT_VIEW, 'welcomeYouNeedBankingDetailsDescriptionLink');
        this.infoAboutCardReader = helper.byResourceId(ViewClasses.TEXT_VIEW, 'welcomeYouNeedDebitCardDescription');
        this.whyYouNeedACard = helper.byResourceId(ViewClasses.TEXT_VIEW, 'welcomeCardReaderDescription');
        this.dialogTitle = helper.byResourceId(ViewClasses.TEXT_VIEW, 'dialogTitle');
    }
}

module.exports = WelcomeRegisterScreen;
