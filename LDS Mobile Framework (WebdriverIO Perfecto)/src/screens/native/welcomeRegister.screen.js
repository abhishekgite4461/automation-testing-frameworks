const Screen = require('../screen');

class WelcomeRegisterScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.getStartedButton = undefined;
        this.onlineBankingRegistrationLink = undefined;
        this.infoAboutCardReader = undefined;
        this.whyYouNeedACard = undefined;
    }
}

module.exports = WelcomeRegisterScreen;
