const Screen = require('../screen');

class EnrolmentCallScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.callMeNowButton = undefined;
        this.cancelButton = undefined;
        this.exitYes = undefined;
        this.exitCancel = undefined;
        this.eiaCallingTitle = undefined;
        this.eiaPin1 = undefined;
        this.eiaPin2 = undefined;
        this.eiaPin3 = undefined;
        this.eiaPin4 = undefined;
        this.accept = undefined;
        this.keypad = undefined;
        this.numberRadioButton = undefined;
        this.yourPhone = undefined;
        this.homePhone = undefined;
        this.mobilePhone = undefined;
        this.workPhone = undefined;
        this.selectPhoneNumber = undefined;
        this.mobilePhoneSection = undefined;
    }
}

module.exports = EnrolmentCallScreen;
