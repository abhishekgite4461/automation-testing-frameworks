const Screen = require('../screen');

class RenewSavingsAccountScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.renew = undefined;
        this.renewAccountPage = undefined;
        this.savingAccount = undefined;
        this.renewSaving = undefined;
        this.applyNow = undefined;
        this.confirmPage = undefined;
        this.confirm = undefined;
        this.termsAndConditions = undefined;
        this.successPage = undefined;
    }
}

module.exports = RenewSavingsAccountScreen;
