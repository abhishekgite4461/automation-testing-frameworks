const Screen = require('../../screen');

class ScottishWidowsFAQScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.whatcanIdonextOption = undefined;
        this.scottishWidowHelpDeskNumber = undefined;
    }
}

module.exports = ScottishWidowsFAQScreen;
