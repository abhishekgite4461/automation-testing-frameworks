const Screen = require('../screen');

class BusinessSelectionScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.businessName = undefined;
        this.businessSelectionSignatoryHeader = undefined;
        this.businessSelectionDelegateHeader = undefined;
        this.signatoryBusiness = undefined;
        this.delegateBusiness = undefined;
        this.firstDelegateBusiness = undefined;
        this.firstSignatoryBusiness = undefined;
        this.businessToSelect = undefined;
        this.firstBusinessName = undefined;
    }
}

module.exports = BusinessSelectionScreen;
