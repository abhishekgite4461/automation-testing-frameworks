const Screen = require('../screen');

class ForcedLogoutScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.errorIcon = undefined;
        this.errorMessage = undefined;
        this.logonButton = undefined;
    }
}

module.exports = ForcedLogoutScreen;
