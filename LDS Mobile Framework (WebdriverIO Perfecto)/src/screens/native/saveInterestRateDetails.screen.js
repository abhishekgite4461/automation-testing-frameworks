// const Screen = require('../screen');

class SavingsInterestRateDetails {
    constructor() {
        this.holdAccountTile = undefined;
        this.title = undefined;
        this.interestDetailRow = undefined;
        this.actionMenu = undefined;
        this.closeButton = undefined;
        this.accountName = undefined;
        this.accountBalance = undefined;
        this.accountTypeIcon = undefined;
        this.interestRate = undefined;
        this.interestRateDescription = undefined;
        this.interestRateScreen = undefined;
        this.interestRateForFirstTier = undefined;
        this.interestRateDescriptionForFirstTier = undefined;
        this.interestRateForCapTier2 = undefined;
        this.interestRateForSecondTier = undefined;
        this.interestRateForCapTier3 = undefined;
        this.interestRateInfo = undefined;
    }
}
module.exports = SavingsInterestRateDetails;
