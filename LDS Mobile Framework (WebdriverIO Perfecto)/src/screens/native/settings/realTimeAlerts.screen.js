const Screen = require('../../screen');

class RealTimeAlertsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.accountAlertsToggleSwitch = undefined;
        this.apnsTitle = undefined;
        this.apnsAllowButton = undefined;
        this.apnsDontAllowButton = undefined;
        this.confirmationAlertTitle = undefined;
        this.optOutConfirmationAlertTitle = undefined;
        this.pushPromptScreenATitle = undefined;
        this.notNowOption = undefined;
        this.turnOnNotificationsOption = undefined;
        this.deviceSettings = undefined;
        this.searchOption = undefined;
        this.settingsOption = undefined;
        this.dateAndTime = undefined;
        this.nextCalenderMonthOption = undefined;
        this.setDate = undefined;
        this.calenView = undefined;
        this.futureDate = undefined;
        this.doneOption = undefined;
        this.pushPromptScreenBTitle = undefined;
        this.nextOption = undefined;
        this.deviceApps = undefined;
        this.pushPromptScreenBSecondTitle = undefined;
        this.pushPromptScreenBThirdTitle = undefined;
        this.bosDeviceSettings = undefined;
        this.halifaxDeviceSettings = undefined;
        this.lloydsDeviceSettings = undefined;
        this.apps = undefined;
        this.deviceNotifications = undefined;
        this.deviceNotificationsStatusOn = undefined;
        this.deviceNotificationsStatusOff = undefined;
        this.conflictScreenTitle = undefined;
        this.goToDeviceSettingsButton = undefined;
        this.turnOffInAppButton = undefined;
        this.turnOffConfirmationLoginPageTitle = undefined;
        this.turnOffConfirmationLoginPageOkButton = undefined;
        this.appNotificationsDeviceTitle = undefined;
        this.optOutOkButton = undefined;
        this.noThanksNotificationsOption = undefined;
        this.apps = undefined;
        this.lloydsbank = undefined;
        this.notifications = undefined;
        this.conflictprompt = undefined;
        this.conflictpopup = undefined;
        this.notnowconflictpopup = undefined;
        this.noThanksNotificationsOption = undefined;
        this.incompleteActionsOverlay = undefined;
        this.incompleteActionsOverlayOKButton = undefined;
        this.enableNotificationInstruction = undefined;
    }
}

module.exports = RealTimeAlertsScreen;
