const Screen = require('../../screen');

class NotificationsSettingScreen extends Screen {
    constructor() {
        super();
        this.header = undefined;
        this.smartAlertsToggleSwitch = undefined;
    }
}
module.exports = NotificationsSettingScreen;
