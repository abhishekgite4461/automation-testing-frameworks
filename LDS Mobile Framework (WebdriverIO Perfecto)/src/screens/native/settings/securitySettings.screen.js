const Screen = require('../../screen');

class SecuritySettingsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.fingerprintToggle = undefined;
        this.securityDetailsUserIDLabel = undefined;
        this.securityDetailsUserIDValue = undefined;
        this.securityDetailsAppVersionLabel = undefined;
        this.securityDetailsAppVersionValue = undefined;
        this.securityDetailsDeviceNameLabel = undefined;
        this.securityDetailsDeviceNameValue = undefined;
        this.securityDetailsDeviceTypeLabel = undefined;
        this.securityDetailsDeviceTypeValue = undefined;
        this.securityDetailsForgottenPasswordButton = undefined;
        this.securityDetailsAutoLogOffButton = undefined;
        this.autoLogOffOption = undefined;
        this.resetPassword = undefined;
        this.securityFingerPrintToggleCell = undefined;
        this.securityFingerprintCell = undefined;
    }
}

module.exports = SecuritySettingsScreen;
