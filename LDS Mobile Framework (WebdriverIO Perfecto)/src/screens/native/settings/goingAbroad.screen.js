const Screen = require('../../screen');

class GoingAbroadScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.backButton = undefined;
        this.okButton = undefined;
    }
}
module.exports = GoingAbroadScreen;
