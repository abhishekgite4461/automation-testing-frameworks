const Screen = require('../../screen');

class HowWeContactYou extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.onlinePaperPreference = undefined;
        this.statementFreqPreference = undefined;
        this.passbookAccountNotifications = undefined;
        this.marketingPreference = undefined;
        this.realTimeAlert = undefined;
    }
}

module.exports = HowWeContactYou;
