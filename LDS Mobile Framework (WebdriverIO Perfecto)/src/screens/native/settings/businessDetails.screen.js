const Screen = require('../../screen');

class BusinessDetailsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.backButton = undefined;
    }
}

module.exports = BusinessDetailsScreen;
