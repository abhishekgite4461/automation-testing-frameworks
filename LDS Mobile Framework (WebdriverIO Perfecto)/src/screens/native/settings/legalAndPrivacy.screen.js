const Screen = require('../../screen');

class LegalAndPrivacyScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.termsAndConditionsLink = undefined;
        this.legalAndPrivacyPage = undefined;
        this.selectPrivacyPolicyLink = undefined;
    }
}

module.exports = LegalAndPrivacyScreen;
