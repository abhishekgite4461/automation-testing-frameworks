const Screen = require('../../../screen');

class ChangeEmailScreen extends Screen {
    constructor() {
        super();
        this.enterEmailAddressField = undefined;
        this.reenterEmailAddressField = undefined;
        this.confirmButton = undefined;
        this.errorMessage = undefined;
        this.dismissErrorMessage = undefined;
    }
}

module.exports = ChangeEmailScreen;
