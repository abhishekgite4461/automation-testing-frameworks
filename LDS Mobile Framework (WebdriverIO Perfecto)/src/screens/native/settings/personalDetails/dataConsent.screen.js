const Screen = require('../../../screen');

class DataConsentScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.dataConsentInterstitialPageTitle = undefined;
        this.dataConsentManageConsentPageTitle = undefined;
        this.consentOptedIn = undefined;
        this.consentOptedOut = undefined;
        this.marketingPrefs = undefined;
        this.performancePrefs = undefined;
        this.dataConsentAcceptAllButton = undefined;
        this.dataConsentManageConsentButton = undefined;
        this.dataConsentConfirmButton = undefined;
        this.dataConsentCookiePolicyLink = undefined;
        this.interstitialDataConsentCookiePolicyLink = undefined;
        this.globalBackButton = undefined;
        this.dataConsentWinbackModal = undefined;
        this.dataConsentExternalWinbackModal = undefined;
        this.dataConsentForWinbackLeave = undefined;
        this.dataConsentForWinbackStay = undefined;
    }
}

module.exports = DataConsentScreen;
