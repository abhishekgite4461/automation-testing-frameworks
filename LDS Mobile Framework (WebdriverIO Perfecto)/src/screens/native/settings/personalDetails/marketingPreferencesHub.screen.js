const Screen = require('../../../screen');

class MarketingPreferencesHubScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.marketingHubIntroduction = undefined;
        this.bosMarketingHubIntroductionText = undefined;
        this.halMarketingHubIntroductionText = undefined;
        this.progressIndicator = undefined;
        this.optedIn = undefined;
        this.optedOut = undefined;
        this.internetPrefs = undefined;
        this.emailPrefs = undefined;
        this.postPrefs = undefined;
        this.thirdPartyPrefs = undefined;
        this.smsPrefs = undefined;
        this.phonePrefs = undefined;
        this.submitButton = undefined;
        this.notification = undefined;
        this.closeErrorMessage = undefined;
        this.winbackModal = undefined;
        this.winbackLeave = undefined;
        this.winbackStay = undefined;
        this.accordionLink = undefined;
        this.inCompleteErrorMessage = undefined;
        this.errorMessageClose = undefined;
    }
}

module.exports = MarketingPreferencesHubScreen;
