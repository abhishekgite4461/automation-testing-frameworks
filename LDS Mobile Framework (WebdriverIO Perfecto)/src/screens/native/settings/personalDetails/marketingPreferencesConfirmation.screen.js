const Screen = require('../../../screen');

class MarketingPreferencesConfirmationScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.profileButton = undefined;
        this.accountsButton = undefined;
    }
}

module.exports = MarketingPreferencesConfirmationScreen;
