const Screen = require('../../../screen');

class ConfirmPhoneNumbersScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.mobileNumberField = undefined;
        this.homeNumberField = undefined;
        this.workNumberField = undefined;
        this.workExtensionField = undefined;
        this.confirmButton = undefined;
        this.editButton = undefined;
    }
}

module.exports = ConfirmPhoneNumbersScreen;
