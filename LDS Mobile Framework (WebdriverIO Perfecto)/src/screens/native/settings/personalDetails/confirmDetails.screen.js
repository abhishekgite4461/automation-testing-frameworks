const Screen = require('../../../screen');

class ConfirmDetailsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.backToContactsButton = undefined;
    }
}

module.exports = ConfirmDetailsScreen;
