const Screen = require('../../../screen');

class ConfirmNumberScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.okButton = undefined;
    }
}

module.exports = ConfirmNumberScreen;
