const Screen = require('../../../screen');

class PersonalDetailsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.fullName = undefined;
        this.userIdLabel = undefined;
        this.userId = undefined;
        this.mobileNumberLabel = undefined;
        this.mobileNumber = undefined;
        this.homeNumberLabel = undefined;
        this.homeNumber = undefined;
        this.workNumberLabel = undefined;
        this.workNumber = undefined;
        this.workExtensionNumberLabel = undefined;
        this.workExtensionNumber = undefined;
        this.addressLabel = undefined;
        this.address = undefined;
        this.postcodeLabel = undefined;
        this.postcode = undefined;
        this.emailLabel = undefined;
        this.email = undefined;
        this.emailContainer = undefined;
        this.addressContainer = undefined;
        this.primaryMarketingLink = undefined;
        this.secondaryMarketingLink = undefined;
        this.emailAddressField = undefined;
        this.updatedEmailAddressField = undefined;
        this.backButton = undefined;
        this.dataConsentLink = undefined;
    }
}

module.exports = PersonalDetailsScreen;
