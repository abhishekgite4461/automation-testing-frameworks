const Screen = require('../../../screen');

class ChangePhoneNumbersScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.mobileNumberField = undefined;
        this.homeNumberField = undefined;
        this.workNumberField = undefined;
        this.workExtensionField = undefined;
        this.mobilePaymLabel = undefined;
        this.paymInfoLabel = undefined;
        this.continueButton = undefined;
        this.mobileNumberLabel = undefined;
        this.homeNumberLabel = undefined;
        this.workNumberLabel = undefined;
        this.confirmEditButton = undefined;
        this.mobileNumberError = undefined;
        this.keyboardDone = undefined;
    }
}

module.exports = ChangePhoneNumbersScreen;
