const Screen = require('../../../screen');

class PhoneOutcomeScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.backToButton = undefined;
        this.message = undefined;
    }
}

module.exports = PhoneOutcomeScreen;
