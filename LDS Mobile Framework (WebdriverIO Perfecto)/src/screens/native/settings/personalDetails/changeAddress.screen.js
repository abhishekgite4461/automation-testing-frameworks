const Screen = require('../../../screen');

class ChangeAddressScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.changeAddressButton = undefined;
        this.addressLabel = undefined;
        this.address = undefined;
        this.postcodeLabel = undefined;
        this.postcode = undefined;
    }
}

module.exports = ChangeAddressScreen;
