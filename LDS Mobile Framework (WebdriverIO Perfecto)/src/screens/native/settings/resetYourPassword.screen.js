const Screen = require('../../screen');

class ResetYourPasswordScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.enterNewPasswordEditBox = undefined;
        this.confirmNewPasswordEditBox = undefined;
        this.clickSubmitButton = undefined;
        this.resetSuccessfulPopupMessage = undefined;
        this.LogOffButton = undefined;
        this.errorMessage = undefined;
        this.nonIdenticalErrorMessage = undefined;
        this.noPersonalDetailsErrorMessage = undefined;
        this.nonSecurePasswordErrorMessage = undefined;
        this.lastFivePasswordErrorMessage = undefined;
        this.easyToGuessPasswordErrorMessage = undefined;
        this.tipsForPasswordAccordion = undefined;
        this.tipsForPasswordToolTip = undefined;
        this.closeButton = undefined;
    }
}

module.exports = ResetYourPasswordScreen;
