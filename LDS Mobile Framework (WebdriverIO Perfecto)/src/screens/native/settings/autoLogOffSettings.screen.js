const Screen = require('../../screen');

class AutoLogOffSettingsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.confirmButton = undefined;
        this.inactivityTimeTenMins = undefined;
        this.autoLogOffImmediately = undefined;
        this.autoLogOff1minute = undefined;
        this.autoLogOff2minute = undefined;
        this.autoLogOff5minute = undefined;
        this.autoLogOff10minute = undefined;
    }
}

module.exports = AutoLogOffSettingsScreen;
