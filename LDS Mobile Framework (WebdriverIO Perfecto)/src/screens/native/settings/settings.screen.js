const Screen = require('../../screen');

class SettingsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.securitySettingsButton = undefined;
        this.resetAppButton = undefined;
        this.personalDetailsButton = undefined;
        this.payAContactButton = undefined;
        this.goingAbroadButton = undefined;
        this.backButton = undefined;
        this.everydayOfferButton = undefined;
        this.businessDetailsButton = undefined;
        this.legalInfo = undefined;
        this.howWeContactYou = undefined;
        this.realTimeAlertsButton = undefined;
        this.settingsList = undefined;
        this.settingsOpenBankingTile = undefined;
        this.settingsOpenBankingTileErrorBanner = undefined;
    }
}

module.exports = SettingsScreen;
