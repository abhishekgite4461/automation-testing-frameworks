const Screen = require('../../screen');

class ResetPasswordConfirmationPopUpScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.OkAndLogOffButton = undefined;
    }
}

module.exports = ResetPasswordConfirmationPopUpScreen;
