const Screen = require('../../screen');

class MarketingPrefScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.halifaxMarketing = undefined;
        this.bosMarketing = undefined;
    }
}

module.exports = MarketingPrefScreen;
