const Screen = require('../../screen');

class CookieUseAndPermissionScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.cookiePolicyLink = undefined;
    }
}

module.exports = CookieUseAndPermissionScreen;
