const Screen = require('../../screen');

class ResetConfirmationScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.goButton = undefined;
        this.resetConfirmationDialog = undefined;
        this.cancelButton = undefined;
    }
}

module.exports = ResetConfirmationScreen;
