const Screen = require('../../screen');

class ResetScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.resetButton = undefined;
        this.resetAppInfo = undefined;
        this.resetAppWarningMessage = undefined;
    }
}

module.exports = ResetScreen;
