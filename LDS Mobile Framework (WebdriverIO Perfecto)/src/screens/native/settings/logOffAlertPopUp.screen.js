const Screen = require('../../screen');

class AutoLogOffPopUpScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.logOffButton = undefined;
        this.continueButton = undefined;
    }
}

module.exports = AutoLogOffPopUpScreen;
