const Screen = require('../../screen');

class LegalInfoScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.legalAndPrivacy = undefined;
        this.cookieUseAndPermissions = undefined;
        this.thirdPartyAcknowledgements = undefined;
        this.dismissButton = undefined;
        this.legalInformationText = undefined;
    }
}
module.exports = LegalInfoScreen;
