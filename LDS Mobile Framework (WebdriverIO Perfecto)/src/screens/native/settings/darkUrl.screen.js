const Screen = require('../../screen');

class DarkUrlScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.darkUrlButton = undefined;
    }
}

module.exports = DarkUrlScreen;
