const Screen = require('../../screen');

class MultipleDeviceManagementNotificationsScreen extends Screen {
    constructor() {
        super();
        this.header = undefined;
    }
}
module.exports = MultipleDeviceManagementNotificationsScreen;
