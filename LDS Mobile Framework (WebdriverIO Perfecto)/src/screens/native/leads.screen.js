const Screen = require('../screen');

class LeadsScreen extends Screen {
    constructor() {
        super();
        this.leadsholder = undefined;
        this.leadsButton = undefined;
        this.endOfPageNotDisplayedAlert = undefined;
        this.endOfThePage = undefined;
        this.bigPromptEndOfPage = undefined;
        this.bigPromptLink = undefined;
        this.highestPriorityLead = undefined;
        this.spendingRewardsRegistrationLead = undefined;
        this.securitySettingsLead = undefined;
        this.callUsInternetBankingLead = undefined;
        this.callUsOtherBankingQueryLead = undefined;
        this.callUsLostOrStolenLead = undefined;
        this.callUsSuspectedFraudLead = undefined;
        this.callUsMedicalAssistanceLead = undefined;
        this.callUsEmergencyCashLead = undefined;
        this.callUsNewCurrentAccountLead = undefined;
        this.callUsNewCreditCardLead = undefined;
        this.callUsNewIsaAccountLead = undefined;
        this.callUsNewLoanLead = undefined;
        this.callUsNewMortgageLead = undefined;
        this.callUsNewSavingsLead = undefined;
        this.asm1Lead = undefined;
        this.asmChequeTile = undefined;
        this.chequeHeader = undefined;
        this.depositCheque = undefined;
        this.viewDemo = undefined;
        this.addExternalAccountLink = undefined;
    }
}

module.exports = LeadsScreen;
