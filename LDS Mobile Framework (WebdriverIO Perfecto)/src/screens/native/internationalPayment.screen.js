const Screen = require('../screen');

class InternationalPaymentScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.recipients = undefined;
        this.makePayment = undefined;
        this.ipAmount = undefined;
        this.ipAmountField = undefined;
        this.ipContinue = undefined;
        this.confirmIPPayment = undefined;
        this.ipTermsAndConditions = undefined;
        this.ipPassword = undefined;
        this.ipConfirm = undefined;
        this.ipPaymentConfirmation = undefined;
        this.txtBicSwift = undefined;
        this.contuneButton = undefined;
        this.recipientBankAddr = undefined;
        this.IBAN = undefined;
        this.recipientName = undefined;
        this.recipientAddr = undefined;
        this.continueRecipientDetailsButton = undefined;
        this.confirmButton = undefined;
        this.authenticationPage = undefined;
        this.radioButton = undefined;
        this.successPage = undefined;
    }
}

module.exports = InternationalPaymentScreen;
