const Screen = require('../screen');

class ViewTransactionScreen extends Screen {
    constructor() {
        super();
        this.viewTransactionTitle = undefined;
        this.noTransactionAvailable = undefined;
        this.actionMenu = undefined;
    }
}

module.exports = ViewTransactionScreen;
