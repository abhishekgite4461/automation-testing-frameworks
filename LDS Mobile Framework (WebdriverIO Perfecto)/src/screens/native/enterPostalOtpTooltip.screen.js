const Screen = require('../screen');

class EnterPostalOtpTooltipScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.closeButton = undefined;
        this.contactUsButton = undefined;
    }
}

module.exports = EnterPostalOtpTooltipScreen;
