const Screen = require('../screen');

class AppShortCutsScreen extends Screen {
    constructor() {
        super();
        this.appByName = undefined;
        this.branchFinder = undefined;
        this.ourWebsite = undefined;
        this.contactus = undefined;
        this.callusdetails = undefined;
        this.closebutton = undefined;
        this.hfxBranchInMaps = undefined;
        this.ldsBranchInMaps = undefined;
        this.bosBranchInMaps = undefined;
        this.hfxWebsite = undefined;
        this.ldsWebsite = undefined;
        this.bosWebsite = undefined;
    }
}

module.exports = AppShortCutsScreen;
