const Screen = require('../screen');

class LogoutScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.loginButton = undefined;
        this.cmsTile = undefined;
        this.fscsTile = undefined;
        this.appBanningMessage = undefined;
        this.loggOffcompleteMessage = undefined;
        this.appResetcompleteMessage = undefined;
        this.sessionTimedOutMessage = undefined;
        this.surveyButton = undefined;
    }
}

module.exports = LogoutScreen;
