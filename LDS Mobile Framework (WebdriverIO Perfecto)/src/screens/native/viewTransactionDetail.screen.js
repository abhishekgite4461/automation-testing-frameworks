const Screen = require('../screen');

class ViewTransactionDetailScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.amount = undefined;
        this.moveMoney = undefined;
        this.unSureTransaction = undefined;
        this.closeButton = undefined;
        this.transactionDate = undefined;
        this.transactionType = undefined;
        this.errorText = undefined;
        this.postedTransaction = undefined;
        this.unsureTransactionButton = undefined;
        this.vtdContainer = undefined;
        this.vtdCloseButton = undefined;
        this.statementsDescription = undefined;
        this.manageStandingOrderButton = undefined;
        this.standingOrderText = undefined;
        this.payeeField = undefined;
        this.referenceDirectDebit = undefined;
        this.viewDirectDebit = undefined;
        this.directDebitText = undefined;
        this.transferPaymentsButton = undefined;
        this.mobilePaymentInText = undefined;
        this.mobilePaymentOutText = undefined;
        this.cashPointText = undefined;
        this.cashText = undefined;
        this.bankGiroCreditText = undefined;
        this.billPaymentText = undefined;
        this.fasterPaymentsInText = undefined;
        this.fasterPaymentsOutText = undefined;
        this.payeeFieldAdditionalDetails = undefined;
        this.depositText = undefined;
        this.transferText = undefined;
        this.payeeFieldCardNumber = undefined;
        this.chequeText = undefined;
        this.dueSoon = undefined;
        this.chipAndPinText = undefined;
        this.contactLessPurchaseText = undefined;
        this.onlinePhoneMailText = undefined;
        this.dateOfTransaction = undefined;
        this.cardEndingNumber = undefined;
        this.pendingPaymentTransaction = undefined;
        this.referenceField = undefined;
        this.outgoingPaymentText = undefined;
        this.dueSoonLozenge = undefined;
        this.sortCodeAccountNumber = undefined;
        this.changeOrDeleteText = undefined;
        this.datePendingPayment = undefined;
        this.creditCardText = undefined;
        this.manualPaperText = undefined;
        this.depositedOn = undefined;
        this.pendingStatus = undefined;
        this.dateOfPendingPayment = undefined;
        this.pendingTransactionDescription = undefined;
        this.outgoingTransaction = undefined;
        this.incomingTransaction = undefined;
        this.speakToUsButton = undefined;
        this.unsureDueTransactionButton = undefined;
        this.viewInformationLink = undefined;
        this.winBack = undefined;
    }
}

module.exports = ViewTransactionDetailScreen;
