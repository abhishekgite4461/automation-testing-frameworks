const Screen = require('../screen');

class CardManagementScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.selectLostAndStolenCard = undefined;
        this.selectReplacementCardAndPin = undefined;
        this.freezCardTransactions = undefined;
        this.lostAndStolenCards = undefined;
        this.replaceCardAndPin = undefined;
    }
}

module.exports = CardManagementScreen;
