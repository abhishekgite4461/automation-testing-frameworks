const Screen = require('../screen');

class AddressChangedOTPError extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.addressChangedOtpErrorMessage = undefined;
        this.goToLogonButton = undefined;
    }
}

module.exports = AddressChangedOTPError;
