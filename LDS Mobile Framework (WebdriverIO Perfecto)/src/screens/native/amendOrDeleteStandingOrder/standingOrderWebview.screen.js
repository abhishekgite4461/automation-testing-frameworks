const Screen = require('../../screen');

class StandingOrderWebviewScreen extends Screen {
    constructor() {
        super();
        this.webviewTitle = undefined;
    }
}

module.exports = StandingOrderWebviewScreen;
