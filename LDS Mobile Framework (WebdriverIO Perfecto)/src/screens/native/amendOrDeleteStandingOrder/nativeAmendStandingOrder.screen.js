const Screen = require('../../screen');

class NativeAmendStandingOrderScreen extends Screen {
    constructor() {
        super();
        this.pageTitle = undefined;
        this.selectedStandingOrderAccount = undefined;
        this.deleteButtonForStandingOrder = undefined;
        this.amendButtonForStandingOrder = undefined;
        this.accountNameForStandingOrder = undefined;
        this.setUpStandingOrderButton = undefined;
        this.directDebitsTitle = undefined;
        this.deleteOption = undefined;
        this.nextDueDateDD = undefined;
        this.merchantName = undefined;
        this.firstStandingOrder = undefined;
        this.amount = undefined;
        this.frequency = undefined;
        this.noStandingOrdersTitle = undefined;
        this.noDirectDebitsHeader = undefined;
        this.unableToLoadDirectDebits = undefined;
        this.winbackText = undefined;
        this.firstSOAmount = undefined;
        this.standingOrdesTitle = undefined;
        this.firstDirectDebit = undefined;
    }
}

module.exports = NativeAmendStandingOrderScreen;
