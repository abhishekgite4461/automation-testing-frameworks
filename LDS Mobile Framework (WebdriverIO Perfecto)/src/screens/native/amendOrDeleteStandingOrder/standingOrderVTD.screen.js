const Screen = require('../../screen');

class StandingOrderVTDScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.MerchantVTD = undefined;
        this.AmountVTD = undefined;
        this.FrequencyVTD = undefined;
        this.LastPaid = undefined;
        this.NextPaid = undefined;
        this.endDate = undefined;
        this.standingOrderAmend = undefined;
        this.standingOrderDelete = undefined;
        this.accountNoVTD = undefined;
        this.referenceVTD = undefined;
    }
}

module.exports = StandingOrderVTDScreen;
