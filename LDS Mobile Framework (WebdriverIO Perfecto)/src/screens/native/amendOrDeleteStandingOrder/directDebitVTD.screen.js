const Screen = require('../../screen');

class DirectDebitVTDScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.directDebitDelete = undefined;
        this.merchantVTD = undefined;
        this.amountVTD = undefined;
        this.frequencyVTD = undefined;
        this.lastPaid = undefined;
        this.nextPaid = undefined;
    }
}

module.exports = DirectDebitVTDScreen;
