const Screen = require('../../screen');

class DeleteDirectDebitInfoScreen extends Screen {
    constructor() {
        super();
        this.deleteDDInfoPage = undefined;
    }
}

module.exports = DeleteDirectDebitInfoScreen;
