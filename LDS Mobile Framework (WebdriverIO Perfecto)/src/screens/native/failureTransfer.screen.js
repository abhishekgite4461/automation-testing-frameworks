const Screen = require('../screen');

class FailureTransferScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.internetErrorMessage = undefined;
        this.transferAndPaymentButton = undefined;
        this.callUsButton = undefined;
        this.isaTransferTitle = undefined;
        this.isaAlreadySubscribedErrorMessage = undefined;
        this.moreThanFundingLimitHTBTransferTitle = undefined;
        this.htbIsaAboveFundLimitErrorMessage = undefined;
        this.homeButton = undefined;
    }
}

module.exports = FailureTransferScreen;
