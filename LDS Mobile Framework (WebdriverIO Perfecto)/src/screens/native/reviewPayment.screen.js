const Screen = require('../screen');

class ReviewPaymentScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.editButton = undefined;
        this.confirmButton = undefined;
        this.fromDetail = undefined;
        this.toDetail = undefined;
        this.amountDetail = undefined;
        this.toCreditCardNumberDetail = undefined;
        this.withdrawFromIsaToNonLbgAccountWarningMessage = undefined;
        this.isaTransferAgreementCheckBox = undefined;
        this.detailContainer = undefined;
        this.toP2PAccountNameDetail = undefined;
        this.toMobileNum = undefined;
        this.fromAccountNameDetail = undefined;
        this.fromSortCodeDetail = undefined;
        this.fromAccountNumberDetail = undefined;
        this.toAccountNameDetail = undefined;
        this.toSortCodeDetail = undefined;
        this.toAccountNumberDetail = undefined;
        this.mpaPaymentApprovalRequiredMessage = undefined;
        this.toMortgageAccountDetail = undefined;
        this.subAccountInfo = undefined;
        this.reference = undefined;
    }
}

module.exports = ReviewPaymentScreen;
