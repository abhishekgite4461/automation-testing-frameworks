const Screen = require('../../screen');

class CallUsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.callUsButton = undefined;
        this.callTimeLabel = undefined;
        this.textPhoneLabel = undefined;
        this.textPhoneIcon = undefined;
        this.accountNumber = undefined;
        this.sortCode = undefined;
        this.referenceNumber = undefined;
        this.globalMenuIcon = undefined;
        this.lostOrStolenSelfServiceOption = undefined;
        this.lostOrStolenSelfServiceButton = undefined;
        this.resetYourPasswordSelfServiceButton = undefined;
        this.replacementCardSelfServiceButton = undefined;
        this.phoneNumber = undefined;
        this.backButton = undefined;
        this.modalBackButton = undefined;
        this.closeButton = undefined;
        this.phoneNumberText = undefined;
        this.cancelButton = undefined;
        this.callButton = undefined;
        this.phoneNumberOnDialler = undefined;
        this.endCallButton = undefined;
        this.networkPopup = undefined;
    }
}

module.exports = CallUsScreen;
