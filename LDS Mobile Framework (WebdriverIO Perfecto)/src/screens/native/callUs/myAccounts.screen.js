const Screen = require('../../screen');

class MyAccountsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.savingsAccount = undefined;
        this.currentAccount = undefined;
        this.creditAccount = undefined;
        this.isaAccount = undefined;
        this.htbIsaAccount = undefined;
        this.cbsLoanAccount = undefined;
        this.loanAccount = undefined;
        this.mortgageAccount = undefined;
        this.mortgageUfssAccount = undefined;
        this.termDepositAccount = undefined;
        this.investmentAccount = undefined;
        this.homeInsurance = undefined;
        this.treasureFtd = undefined;
        this.treasure32Dcn = undefined;
    }
}
module.exports = MyAccountsScreen;
