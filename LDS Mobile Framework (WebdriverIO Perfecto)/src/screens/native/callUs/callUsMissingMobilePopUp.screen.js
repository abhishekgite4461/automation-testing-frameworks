const AlertPopUpScreen = require('../alertPopUp.screen');

class CallUsMissingMobilePopUpScreen extends AlertPopUpScreen {
    constructor() {
        super();
        this.title = undefined;
        this.addPhoneNumber = undefined;
        this.continueCall = undefined;
    }
}

module.exports = CallUsMissingMobilePopUpScreen;
