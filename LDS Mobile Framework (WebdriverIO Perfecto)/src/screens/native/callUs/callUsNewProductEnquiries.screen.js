const Screen = require('../../screen');

class NewProductEnquiries extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.newCurrentAccount = undefined;
        this.newSavingsAccount = undefined;
        this.newIsaAccount = undefined;
        this.newCreditAccount = undefined;
        this.newLoanAccount = undefined;
        this.newMortgagesAccount = undefined;
    }
}

module.exports = NewProductEnquiries;
