const Screen = require('../screen');

class ReactivateIsaInterstitialScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.reactivateIsaButton = undefined;
    }
}

module.exports = ReactivateIsaInterstitialScreen;
