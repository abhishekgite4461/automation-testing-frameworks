const Screen = require('../screen');

class HoldPaymentScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.errorMessage = undefined;
        this.transferAndPaymentButton = undefined;
        this.paymentHoldErrorMessage = undefined;
        this.callUsOption = undefined;
    }
}

module.exports = HoldPaymentScreen;
