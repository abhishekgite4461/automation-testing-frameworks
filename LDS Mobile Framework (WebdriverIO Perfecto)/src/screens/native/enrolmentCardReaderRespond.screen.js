const Screen = require('../screen');

class EnrolmentCardReaderRespondScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.lastFiveCardDigitField = undefined;
        this.challengeCode = undefined;
        this.passcodeField = undefined;
        this.continueButton = undefined;
        this.havingTroubleWithCardReaderLink = undefined;
        this.havingTroublePopUpCloseButton = undefined;
        this.havingTroublePopUpContinueButton = undefined;
        this.havingTroubleWithCardReaderPopUpTitle = undefined;
        this.invalidCardError = undefined;
        this.expiredCardError = undefined;
        this.suspendedCardError = undefined;
        this.oldCardExpiredError = undefined;
        this.cardAlreadyExistError = undefined;
        this.incorrectPasscodeError = undefined;
        this.closeErrorBanner = undefined;
    }
}

module.exports = EnrolmentCardReaderRespondScreen;
