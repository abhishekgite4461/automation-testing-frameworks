const Screen = require('../screen');

class LoginScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        // MQE-1075 TODO
        this.oldTitle = undefined;
        this.userIdField = undefined;
        this.passwordField = undefined;
        this.continueButton = undefined;
        this.userIdErrorMessage = undefined;
        this.loginForgotYourPassword = undefined;
        this.mandateLessErrorMessage = undefined;
        this.registerForIb = undefined;
        this.inaccessibleErrorMessage = undefined;
        this.errorMessage = undefined;
        this.errorMessageDismiss = undefined;
        this.specialCharactersRestrictionMessage = undefined;
        this.updateYourPasswordLink = undefined;
        this.pasteOptionInToolTip = undefined;
        this.copyOptionInToolTip = undefined;
        this.selectAllOptionInToolTip = undefined;
    }
}

module.exports = LoginScreen;
