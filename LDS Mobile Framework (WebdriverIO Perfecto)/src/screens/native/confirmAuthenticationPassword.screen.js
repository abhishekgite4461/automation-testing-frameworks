const BaseAlertPopUpScreen = require('./alertPopUp.screen');

class ConfirmAuthenticationPasswordScreen extends BaseAlertPopUpScreen {
    constructor() {
        super();
        this.title = undefined;
        this.passwordField = undefined;
        this.forgottenPasswordLabel = undefined;
        this.errorMessage = undefined;
    }
}

module.exports = ConfirmAuthenticationPasswordScreen;
