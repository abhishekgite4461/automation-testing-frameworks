const Screen = require('../screen');

class SpendingRewardsCongratulationPopUpScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.okButton = undefined;
    }
}

module.exports = SpendingRewardsCongratulationPopUpScreen;
