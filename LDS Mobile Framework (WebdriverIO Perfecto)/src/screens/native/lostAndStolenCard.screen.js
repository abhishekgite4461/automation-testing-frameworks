const Screen = require('../screen');

class LostAndStolenCard extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.backButton = undefined;
        this.lost = undefined;
        this.cancelCardDebit = undefined;
        this.cancelCardCredit = undefined;
        this.newPin = undefined;
        this.continue = undefined;
        this.confirmTitle = undefined;
        this.password = undefined;
    }
}

module.exports = LostAndStolenCard;
