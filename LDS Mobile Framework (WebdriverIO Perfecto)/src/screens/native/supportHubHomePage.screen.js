const Screen = require('../screen');

class SupportHubHomePageScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.callUsButton = undefined;
        this.mobileChatButton = undefined;
        this.mobileChatButtonLabel = undefined;
        this.changeOfAddress = undefined;
        this.lostOrStolenCard = undefined;
        this.replaceCardOrPin = undefined;
        this.resetPassword = undefined;
        this.atmBranchFinder = undefined;
        this.appFeedback = undefined;
        this.cmaSurvey = undefined;
        this.overLayTitle = undefined;
        this.dismissOverLay = undefined;
        this.carousals = undefined;
        this.suspectFraud = undefined;
        this.updateDetailsButton = undefined;
        this.mortgagePaymentHolidaysLink = undefined;
        this.creditcardPaymentHolidaysLink = undefined;
        this.loanPaymentHolidaysLink = undefined;
        this.overdraftChargesLink = undefined;
        this.travelAndDisruptionCoverLink = undefined;
        this.moreInfoAboutCoronavirusLink = undefined;
        this.covidHubLink = undefined;
        this.covidHubNativePage = undefined;
    }
}

module.exports = SupportHubHomePageScreen;
