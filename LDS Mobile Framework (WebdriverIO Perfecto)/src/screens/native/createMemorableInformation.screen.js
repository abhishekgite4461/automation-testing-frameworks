const Screen = require('../screen');

class CreateMemorableInformationScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.enterMI = undefined;
        this.reEnterMI = undefined;
        this.nextButton = undefined;
        this.backButton = undefined;
    }
}

module.exports = CreateMemorableInformationScreen;
