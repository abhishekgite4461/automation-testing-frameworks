const Screen = require('../screen');

class BrowserScreen extends Screen {
    constructor() {
        super();
        this.chromeBrowserTabs = undefined;
        this.chromeMenuButton = undefined;
        this.chromeCloseAllTabs = undefined;
        this.chromeNewTabButton = undefined;
        this.androidBrowserTabs = undefined;
        this.androidcloseAllTabs = undefined;
    }
}

module.exports = BrowserScreen;
