const Screen = require('../screen');

class MortgageAccountScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.yourMortgageDetails = undefined;
    }
}

module.exports = MortgageAccountScreen;
