const Screen = require('../screen');

class ReEnterMemorableInformationScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.memorableCharacter = undefined;
        this.wrongMIError = undefined;
        this.wrongPasswordError = undefined;
        this.miForgotYourPassword = undefined;
        this.firstMemorableCharacter = undefined;
        this.fscsTitle = undefined;
    }
}

module.exports = ReEnterMemorableInformationScreen;
