const Screen = require('../screen');

class MoreScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.moreSwitchBusiness = undefined;
        this.yourProfile = undefined;
        this.inbox = undefined;
        this.settings = undefined;
        this.cardManagement = undefined;
        this.lostStolenCard = undefined;
        this.replacementCardPin = undefined;
        this.chequeScanning = undefined;
        this.depositCheque = undefined;
        this.viewDepositHistory = undefined;
        this.logout = undefined;
        this.darkURL = undefined;
    }
}

module.exports = MoreScreen;
