const Screen = require('../screen');

class ErrorScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.revokedIDErrorMessage = undefined;
        this.suspendedIDErrorMessage = undefined;
        this.inactiveIDErrorMessage = undefined;
        this.logonToMobileBanking = undefined;
        this.compromisedPasswordErrorMessage = undefined;
        this.partiallyRegisteredErrorMessage = undefined;
        this.telephoneNumber = undefined;
        this.twoFactorAuthErrorMessage = undefined;
        this.inaccessibleErrorMessage = undefined;
        this.htbIsaAboveFundLimitErrorMessage = undefined;
        this.homeButton = undefined;
        this.hardTokenUserErrorMessage = undefined;
        this.softTokenUserErrorMessage = undefined;
        this.maxDeviceEnrolErrorMessage = undefined;
        this.suspendedCardError = undefined;
        this.oldCardExpiredError = undefined;
    }
}

module.exports = ErrorScreen;
