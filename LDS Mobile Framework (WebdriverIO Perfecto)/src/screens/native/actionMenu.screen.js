const Screen = require('../screen');

class ActionMenuScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.viewTransactionsOption = undefined;
        this.transferAndPaymentOption = undefined;
        this.internationalPaymentOption = undefined;
        this.standingOrderOption = undefined;
        this.directDebitOption = undefined;
        this.saveTheChangesOption = undefined;
        this.changeAccountTypeOption = undefined;
        this.orderPaperStatementOption = undefined;
        this.lostOrStolenCardOption = undefined;
        this.replacementCardAndPinOption = undefined;
        this.viewPendingPaymentOption = undefined;
        this.closeButton = undefined;
        this.renewYourSavingsAccountOption = undefined;
        this.viewInterestRateOption = undefined;
        this.balanceMoneyTransferOption = undefined;
        this.manageCreditLimitOption = undefined;
        this.pdfStatementsOption = undefined;
        this.setupInstallmentOption = undefined;
        this.maintainInstallmentOption = undefined;
        this.payCreditCardOption = undefined;
        this.pendingPaymentOption = undefined;
        this.viewPendingPayment = undefined;
        this.reactivateIsa = undefined;
        this.interestDetailRow = undefined;
        this.reactivateISA = undefined;
        this.reactivateIsaOption = undefined;
        this.loanAdditionalPaymentOption = undefined;
        this.loanAnnualStatementsOption = undefined;
        this.loanBorrowMoreOption = undefined;
        this.loanClosureOption = undefined;
        this.loanRepaymentHolidayOption = undefined;
        this.manageOverdraftOption = undefined;
        this.applyForOverdraftOption = undefined;
        this.chequeDepositOption = undefined;
        this.selectViewInterestRateDetails = undefined;
        this.topupIsaOption = undefined;
        this.dueSoon = undefined;
        this.actionMenuHeader = undefined;
        this.externalAccountActionMenu = undefined;
        this.externalAccountMenuOptionsLabel = undefined;
        this.externalBankAccountNumberOfActionMenuOptions = undefined;
        this.actionMenuOption = undefined;
        this.cardManagementOption = undefined;
        this.sendMoneyOutsideTheUKOption = undefined;
        this.makeAnOverpaymentOption = undefined;
        this.pensionTransferOption = undefined;
        this.sendBankDetailsOption = undefined;
        this.shareAccountDetail = undefined;
        this.standingOrderAndDirectDebitOption = undefined;
        this.renameAccountOption = undefined;
        this.downloadTransactionsOption = undefined;
    }
}

module.exports = ActionMenuScreen;
