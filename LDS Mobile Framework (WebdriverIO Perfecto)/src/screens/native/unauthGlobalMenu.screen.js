const Screen = require('../screen');

class UnauthGlobalMenuScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.legalInfo = undefined;
    }
}

module.exports = UnauthGlobalMenuScreen;
