const Screen = require('../screen');

class SplashScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
    }
}

module.exports = SplashScreen;
