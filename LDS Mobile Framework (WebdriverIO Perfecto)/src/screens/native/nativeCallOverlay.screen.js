const Screen = require('../screen');

class NativeCallOverlayScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
    }
}

module.exports = NativeCallOverlayScreen;
