const Screen = require('../screen');

class AppBanScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.updateAppButton = undefined;
        this.goToOurMobileSiteButton = undefined;
    }
}

module.exports = AppBanScreen;
