const Screen = require('../screen');

class EnterPostalOtpScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.otpNotRecievedToolTip = undefined;
        this.logoffOption = undefined;
        this.invalidOTPErrorMessage = undefined;
        this.errorCloseButton = undefined;
        this.enterOTPField = undefined;
        this.continueButton = undefined;
    }
}

module.exports = EnterPostalOtpScreen;
