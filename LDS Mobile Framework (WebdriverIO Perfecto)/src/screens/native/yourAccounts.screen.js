const Screen = require('../screen');

class YourAccountsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.creditCard = undefined;
        this.savingsAccountName = undefined;
    }
}

module.exports = YourAccountsScreen;
