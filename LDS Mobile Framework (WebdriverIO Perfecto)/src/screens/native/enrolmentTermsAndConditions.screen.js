const Screen = require('../screen');

class EnrolmentTermsAndConditions extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.acceptButton = undefined;
        this.declineButton = undefined;
    }
}

module.exports = EnrolmentTermsAndConditions;
