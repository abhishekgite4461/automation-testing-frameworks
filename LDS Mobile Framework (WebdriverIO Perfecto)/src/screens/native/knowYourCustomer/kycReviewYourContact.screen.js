const Screen = require('../../screen');

class KycReviewYourContactScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.securityText = undefined;
        this.mobileLabel = undefined;
        this.mobileNumber = undefined;
        this.emailLabel = undefined;
        this.emailId = undefined;
        this.detailsCorrect = undefined;
        this.updateLater = undefined;
        this.notNowButton = undefined;
        this.editDetails = undefined;
    }
}

module.exports = KycReviewYourContactScreen;
