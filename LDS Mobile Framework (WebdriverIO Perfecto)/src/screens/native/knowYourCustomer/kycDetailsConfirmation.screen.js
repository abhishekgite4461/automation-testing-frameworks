const Screen = require('../../screen');

class KycDetailsConfirmationScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.securityText = undefined;
        this.mobileLabel = undefined;
        this.mobileText = undefined;
        this.emailLabel = undefined;
        this.emailText = undefined;
        this.detailsCorrectButton = undefined;
        this.updateLaterButton = undefined;
    }
}

module.exports = KycDetailsConfirmationScreen;
