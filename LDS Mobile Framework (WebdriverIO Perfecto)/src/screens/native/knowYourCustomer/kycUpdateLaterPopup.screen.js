const Screen = require('../../screen');

class KycUpdateLaterPopUpScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.alertText = undefined;
        this.cancelButton = undefined;
        this.yesButton = undefined;
    }
}

module.exports = KycUpdateLaterPopUpScreen;
