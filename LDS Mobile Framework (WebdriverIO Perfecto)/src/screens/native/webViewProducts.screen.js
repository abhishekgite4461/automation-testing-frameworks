const Screen = require('../screen');

class WebViewProductsScreen extends Screen {
    constructor() {
        super();
        this.manageCreditLimitTitle = undefined;
        this.repaymentHolidayTitle = undefined;
        this.additionalPaymentTitle = undefined;
        this.balanceTransferTitle = undefined;
        this.borrowMoreTitle = undefined;
        this.loanClosureTitle = undefined;
        this.renewYourSavingsAccountTitle = undefined;
        this.selectSavingsAccountToRenew = undefined;
        this.pensionTransferTitle = undefined;
        this.overdraftPopUpTitle = undefined;
        this.changeAccountTypeTitle = undefined;
    }
}

module.exports = WebViewProductsScreen;
