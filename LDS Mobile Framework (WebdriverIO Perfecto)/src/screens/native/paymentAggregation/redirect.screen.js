const Screen = require('../../screen');

class RedirectScreen extends Screen {
    constructor() {
        super();
        this.redirectScreenPaymentAgg = undefined;
        this.redirectScreenTitlePaymentAgg = undefined;
        this.redirectScreenAuthenticationInProgressTitle = undefined;
        this.redirectLoadingProgressPayAgg = undefined;
        this.redirectTextPayAgg = undefined;
        this.redirectScreenPayAggRedirectIncompleteDescriptionText = undefined;
    }
}

module.exports = RedirectScreen;
