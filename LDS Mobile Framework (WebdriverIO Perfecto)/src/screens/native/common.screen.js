const Screen = require('../screen');

class CommonScreen extends Screen {
    constructor() {
        super();
        this.closeButton = undefined;
        this.modalBackButton = undefined;
        this.backButton = undefined;
        this.homeButton = undefined;
        this.winBackTitle = undefined;
        this.winBackStayButton = undefined;
        this.winBackLeaveButton = undefined;
        this.winBackOkButton = undefined;
        this.winBackCancelButton = undefined;
        this.cancelLink = undefined;
        this.winBackCloseButton = undefined;
        this.pleaseWaitSpinner = undefined;
        this.checkingDetailsSpinner = undefined;
        this.accountNameMatch = undefined;
    }
}

module.exports = CommonScreen;
