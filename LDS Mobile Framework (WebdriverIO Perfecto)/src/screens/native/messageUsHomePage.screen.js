const Screen = require('../screen');

class MessageUsHomePageScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.errorMessage = undefined;
        this.chatCloseButton = undefined;
        this.chatSendButton = undefined;
        this.typeYourQuestionField = undefined;
        this.chatMessageTimeStamp = undefined;
        this.pushOptInHeader = undefined;
        this.pushOptInDescription = undefined;
        this.notNowButton = undefined;
        this.activateButton = undefined;
    }
}

module.exports = MessageUsHomePageScreen;
