const Screen = require('../screen');

class StandingOrder extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.setUpNewStandingOrderButton = undefined;
        this.standingOrderTab = undefined;
        this.directDebitTab = undefined;
        this.amendButton = undefined;
        this.deleteButton = undefined;
        this.savingAccountName = undefined;
        this.dueDate = undefined;
        this.reference = undefined;
        this.paidTo = undefined;
        this.declineSoPage = undefined;
        this.exitInternetBanking = undefined;
        this.underReviewSoPage = undefined;
        this.selectViewStandingOrderOption = undefined;
    }
}

module.exports = StandingOrder;
