const Screen = require('../screen');

class AppStoreScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.appText = undefined;
        this.DoneOrCancelButton = undefined;
        this.appHeaderVersion = undefined;
    }
}

module.exports = AppStoreScreen;
