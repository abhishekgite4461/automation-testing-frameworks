const Screen = require('../screen');

class EnrolmentCardReaderIdentifyScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.lastFiveCardDigitField = undefined;
        this.passCode = undefined;
        this.troubleCardReader = undefined;
        this.helpPopUpCancelButton = undefined;
        this.helpPopUpContinueButton = undefined;
        this.dialogTitle = undefined;
        this.lastFiveCardDigitField = undefined;
        this.passcodeField = undefined;
        this.continueButton = undefined;
        this.invalidCardError = undefined;
        this.expiredCardError = undefined;
        this.suspendedCardError = undefined;
        this.oldCardExpiredError = undefined;
        this.incorrectPasscodeError = undefined;
        this.closeErrorBanner = undefined;
    }
}
module.exports = EnrolmentCardReaderIdentifyScreen;
