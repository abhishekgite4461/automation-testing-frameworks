const Screen = require('../screen');

class OnlinePaperPreferenceWebViewPageScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.backButton = undefined;
        this.goFullyPaperFree = undefined;
        this.customisePreferences = undefined;
        this.byPostSwitchButton = undefined;
        this.onlineSwitchButton = undefined;
        this.updatePreferences = undefined;
        this.confirmPreferencesTitle = undefined;
        this.termsAndCondition = undefined;
        this.password = undefined;
        this.confirmPreference = undefined;
        this.backPreferenceButton = undefined;
        this.forgetPassword = undefined;
        this.updatePreferencesFooter = undefined;
        this.pageFooter = undefined;
        this.winback = undefined;
        this.forgetPasswordWinBackTitle = undefined;
        this.stayButton = undefined;
        this.leaveButton = undefined;
        this.forgetPasswordWinback = undefined;
        this.forgotPasswordCancelButton = undefined;
        this.forgotPasswordOkButton = undefined;
    }
}

module.exports = OnlinePaperPreferenceWebViewPageScreen;
