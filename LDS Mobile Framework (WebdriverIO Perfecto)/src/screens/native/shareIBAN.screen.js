const Screen = require('../screen');

class ShareIBAN extends Screen {
    constructor() {
        super();
        this.shareIbanPageTitle = undefined;
        this.ibanwarning = undefined;
        this.nameUKaccount = undefined;
        this.nameUKaccountValue = undefined;
        this.sortCode = undefined;
        this.sortCodeValue = undefined;
        this.accountNumber = undefined;
        this.accountNumberValue = undefined;
        this.nameIntAccount = undefined;
        this.nameIntAccountValue = undefined;
        this.iban = undefined;
        this.ibanValue = undefined;
        this.bic = undefined;
        this.bicValue = undefined;
        this.sendUKDetailsButton = undefined;
        this.sendIntDetailsButton = undefined;
        this.closeIcon = undefined;
        this.ibanErrorMessage = undefined;
        this.bicSearchMessage = undefined;
        this.ibanSearchMessage = undefined;
    }
}

module.exports = ShareIBAN;
