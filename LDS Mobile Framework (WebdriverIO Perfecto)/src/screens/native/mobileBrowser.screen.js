const Screen = require('../screen');

class mobileBrowserScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.browserUrl = undefined;
        this.brandUrl = undefined;
        this.returnToApp = undefined;
    }
}

module.exports = mobileBrowserScreen;
