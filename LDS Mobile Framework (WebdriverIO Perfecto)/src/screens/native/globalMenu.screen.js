const Screen = require('../screen');

class GlobalMenuScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.settingsButton = undefined;
        this.logoutButton = undefined;
        this.myAccountsButton = undefined;
        this.transferAndPaymentButton = undefined;
        this.moveMoneyButton = undefined;
        this.productHubButton = undefined;
        this.atmBranchFinderButton = undefined;
        this.helpInfoButton = undefined;
        this.yourProfileButton = undefined;
        this.standingOrderMenu = undefined;
        this.directDebitMenu = undefined;
        this.inboxButton = undefined;
        this.callUsButton = undefined;
        this.globalMenuButton = undefined;
        this.applePayButton = undefined;
        this.callUsOption = undefined;
        this.goingAbroadButton = undefined;
        this.fingerprint = undefined;
        this.depositChequeButton = undefined;
        this.chequeButton = undefined;
        this.depositHistoryOption = undefined;
        this.everyDayOfferManageSettings = undefined;
        this.everyDayOfferViewOffers = undefined;
        this.lostStolenCardButton = undefined;
        this.replacementCardAndPinButton = undefined;
        this.everyDayOfferButton = undefined;
        this.registerForEveryDayOfferButton = undefined;
        this.globalBackButton = undefined;
        this.payContactSettings = undefined;
        this.businessName = undefined;
        this.switchAccounts = undefined;
        this.newAccountButton = undefined;
        this.feedbackSubmitButton = undefined;
        this.mobileChatButton = undefined;
    }
}

module.exports = GlobalMenuScreen;
