const Screen = require('../screen');

class WelcomeBenefitsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.nextButton = undefined;
        this.loginButton = undefined;
        this.registerButton = undefined;
        this.legalInfoButton = undefined;
        this.makeYourBankingEasier = undefined;
        this.concernedAboutFraud = undefined;
        this.dataPrivacy = undefined;
        this.becomeACustomer = undefined;
    }
}

module.exports = WelcomeBenefitsScreen;
