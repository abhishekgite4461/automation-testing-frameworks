const Screen = require('../screen');

class FailurePaymentScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.internetErrorMessage = undefined;
        this.pendingPaymentErrorMessage = undefined;
        this.transferAndPaymentButton = undefined;
        this.callUsButton = undefined;
        this.prevYearISAErrorMessage = undefined;
    }
}

module.exports = FailurePaymentScreen;
