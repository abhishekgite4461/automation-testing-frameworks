const Screen = require('../../screen');

class cardAndPinSettingsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.stickyFooterPlaceHolder = undefined;
        this.accountAggregationLoadingTile = undefined;
        this.externalAccountTiles = undefined;
        this.openBankingPromoTile = undefined;
        this.addExternalAccount = undefined;
        this.viewExternalAccount = undefined;
        this.earnCashbackTile = undefined;
        this.externalProviderAccountLogo = undefined;
        this.externalProviderAccountName = undefined;
        this.externalProviderAccountNumber = undefined;
        this.externalProviderAccountSortCode = undefined;
        this.externalProviderAccountBalance = undefined;
        this.externalProviderCreditCardNumber = undefined;
        this.externalProviderAvailableCredit = undefined;
        this.externalProviderAccountActionMenu = undefined;
        this.externalProviderAccountFlag = undefined;
        this.externalAccountNumberList = undefined;
        this.nonEligibleExternalFromAccountList = undefined;
        this.arrangementsList = undefined;
    }
}

module.exports = cardAndPinSettingsScreen;
