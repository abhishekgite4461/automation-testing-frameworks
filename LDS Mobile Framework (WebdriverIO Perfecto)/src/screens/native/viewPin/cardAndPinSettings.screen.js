const Screen = require('../../screen');

class cardAndPinSettingsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.viewPinTile = undefined;
        this.replaceCardAndPinOption = undefined;
    }
}

module.exports = cardAndPinSettingsScreen;
