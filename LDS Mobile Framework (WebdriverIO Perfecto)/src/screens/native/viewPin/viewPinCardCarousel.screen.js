const Screen = require('../../screen');

class cardAndPinSettingsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.creditCardName = undefined;
        this.creditCardLastfourdigits = undefined;
        this.scrollOption = undefined;
        this.viewPinButton = undefined;
        this.enterPasswordDialogTitle = undefined;
        this.dialogPasswordField = undefined;
        this.dialogForgottenPassword = undefined;
        this.dialogCancelButton = undefined;
        this.dialogOkButton = undefined;
    }
}
module.exports = cardAndPinSettingsScreen;
