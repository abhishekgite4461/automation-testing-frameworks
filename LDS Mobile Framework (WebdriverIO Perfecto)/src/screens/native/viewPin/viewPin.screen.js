const Screen = require('../../screen');

class viewPinScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.revealPinButton = undefined;
        this.PIN = undefined;
    }
}

module.exports = viewPinScreen;
