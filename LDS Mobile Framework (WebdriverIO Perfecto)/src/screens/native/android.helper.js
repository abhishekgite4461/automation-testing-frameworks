const androidXClasses = ['RecyclerView'];

const apiPackage = (viewClass) => ((androidXClasses.includes(viewClass.class)) ? 'androidx' : 'android');
const selectorString = (viewClass) => `//${apiPackage(viewClass)}.${viewClass.namespace}.${viewClass.class}`;

class AndroidHelper {
    static byResourceId(viewClass, id) {
        // TODO DPLDA-237 Add the app package
        return `${selectorString(viewClass)}[contains(@resource-id,":id/${id}")]`;
    }

    static byEnabledResourceId(viewClass, id) {
        // TODO DPLDA-237 Add the app package
        return `${selectorString(viewClass)}[contains(@resource-id,":id/${id}") and @enabled="true"]`;
    }

    static byPartialResourceId(viewClass, id) {
        return `${selectorString(viewClass)}[contains(@resource-id,":id/${id}")]`;
    }

    static byPartialResourceIdWebView(viewClass, id) {
        return `${selectorString(viewClass)}[contains(@resource-id,"${id}")]`;
    }

    static byResourceIdWebView(viewClass, id) {
        return `${selectorString(viewClass)}[@resource-id="${id}"]`;
    }

    static byText(viewClass, text) {
        return `${selectorString(viewClass)}[@text="${text}"]`;
    }

    static byPartialText(viewClass, text) {
        return `${selectorString(viewClass)}[contains(@text,"${text}")]`;
    }

    static byContentDesc(viewClass, text) {
        return `${selectorString(viewClass)}[@content-desc="${text}"]`;
    }

    static byPartialContentDesc(viewClass, text) {
        return `${selectorString(viewClass)}[contains(@content-desc,"${text}")]`;
    }

    static byResourceIdAndIndex(viewClass, id, index) {
        // TODO DPLDA-237 Add the app package
        return `(${selectorString(viewClass)}[contains(@resource-id,":id/${id}")])[${index}]`;
    }

    /**
     * Helper method that produces a locator which will identify elements that have a resource
     * id in the provided argument list. This is useful if a screen has one of a selection of
     * IDs depending on the scenario and the precise one is not important.
     */
    static byMultipleResourceIds(viewClass, ...resourceIds) {
        // TODO DPLDA-237 Add the app package
        const resourceIdString = resourceIds.map((id) => `contains(@resource-id,":id/${id}")`)
            .join(' or ');
        return `${selectorString(viewClass)}[${resourceIdString}]`;
    }
}

module.exports = AndroidHelper;
