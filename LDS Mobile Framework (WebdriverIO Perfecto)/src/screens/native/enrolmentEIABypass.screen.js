const Screen = require('../screen');

class EnrolmentEIABypassScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.continueButton = undefined;
    }
}

module.exports = EnrolmentEIABypassScreen;
