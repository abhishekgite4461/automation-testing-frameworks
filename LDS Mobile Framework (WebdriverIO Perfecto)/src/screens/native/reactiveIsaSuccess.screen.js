const Screen = require('../screen');

class reactiveIsaSuccessScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.reactivateIsaSuccessHeader = undefined;
        this.reactivateIsaSupportingCopy = undefined;
        this.reactivateIsaAccountNameText = undefined;
        this.arrangementSortCode = undefined;
        this.arrangementAccountNumber = undefined;
        this.reactivateIsaRemainingAllowanceAmount = undefined;
        this.reactivateIsaAddMoreMoneyButton = undefined;
    }
}

module.exports = reactiveIsaSuccessScreen;
