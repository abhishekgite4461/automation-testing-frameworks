const Screen = require('../screen');

class DataPrivacyScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.dataPrivacyNotice = undefined;
        this.cookiePolicy = undefined;
    }
}

module.exports = DataPrivacyScreen;
