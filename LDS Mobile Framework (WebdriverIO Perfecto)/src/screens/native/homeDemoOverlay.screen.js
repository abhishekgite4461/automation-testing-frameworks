const Screen = require('../screen');

class HomeDemoOverlayScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.yesButton = undefined;
    }
}

module.exports = HomeDemoOverlayScreen;
