const Screen = require('../screen');

class PayAContactSettingsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.nativeBackButton = undefined;
    }
}

module.exports = PayAContactSettingsScreen;
