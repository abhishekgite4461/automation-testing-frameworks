const Screen = require('../screen');

class softTokenScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.passCode = undefined;
        this.continueButton = undefined;
        this.softTokenInfoLink = undefined;
        this.whatisSoftTokenOverlay = undefined;
        this.closeButton = undefined;
        this.incorrectPasscodeErrorMessage = undefined;
        this.closeErrorBannerIcon = undefined;
        this.incorrectPasscodeMaxAttemptsReachedErrorMessage = undefined;
    }
}

module.exports = softTokenScreen;
