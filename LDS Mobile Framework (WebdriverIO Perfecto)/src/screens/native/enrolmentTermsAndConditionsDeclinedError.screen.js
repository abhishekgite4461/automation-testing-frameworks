const Screen = require('../screen');

class EnrolmentTermsAndConditionsDeclinedErrorScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.termsAndConditionsDeclinedError = undefined;
        this.goToLogonButton = undefined;
    }
}

module.exports = EnrolmentTermsAndConditionsDeclinedErrorScreen;
