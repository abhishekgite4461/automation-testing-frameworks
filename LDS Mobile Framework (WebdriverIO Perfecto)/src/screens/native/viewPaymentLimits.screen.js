const Screen = require('../screen');

class ViewPaymentLimitsScreen extends Screen {
    constructor() {
        super();
        this.onlineMobileBanking = undefined;
        this.ukPaymentsDailyLimit = undefined;
        this.ukPaymentResetMessage = undefined;
        this.transferYourOwnAccount = undefined;
        this.telephonyBanking = undefined;
        this.ukTelephonyBanking = undefined;
    }
}

module.exports = ViewPaymentLimitsScreen;
