const Screen = require('../../screen');

class searchLandingScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.searchIcon = undefined;
        this.searchClear = undefined;
        this.resetMobileBankingAppLink = undefined;
        this.resetMobileBankingAppPage = undefined;
        this.overdraftsPage = undefined;
        this.managestatementPage = undefined;
        this.managepaperfreePage = undefined;
        this.managestatementLink = undefined;
        this.managepaperfreeLink = undefined;
        this.setupeverydayoffersPage = undefined;
        this.setupeverydayoffersLink = undefined;
        this.loanRepaymentHolidayLink = undefined;
        this.mortgageRepaymentHolidayLink = undefined;
        this.creditcardPaymentHolidayLink = undefined;
        this.changeYourAddressLink = undefined;
        this.changeYourAddressPage = undefined;
        this.manageAndViewYourMarketingChoicesLink = undefined;
        this.manageAndViewYourMarketingChoicesPage = undefined;
        this.manageYourPersonalDetailsLink = undefined;
        this.manageYourPersonalDetailsPage = undefined;
        this.viewYourDataConsentsLink = undefined;
        this.viewYourDataConsentsPage = undefined;
        this.changeYourPhoneNumberLink = undefined;
        this.changeYourPhoneNumberPage = undefined;
        this.changeYourEmailAddressLink = undefined;
        this.changeYourEmailAddressPage = undefined;
        this.makePaymentLink = undefined;
        this.payCreditCardLink = undefined;
        this.payAndTransferPage = undefined;
        this.chequeDepositHistoryLink = undefined;
        this.chequeDepositHistoryPage = undefined;
        this.depositAChequeLink = undefined;
        this.depositAChequePage = undefined;
        this.checkSpellingMessage = undefined;
        this.NeedMoreHelpTitle = undefined;
        this.messageUsButton = undefined;
        this.helpAndSupportButton = undefined;
        this.supportHubPage = undefined;
        this.searchButton = undefined;
        this.viewPinLink = undefined;
        this.viewPinPage = undefined;
        this.replaceCardAndPinLink = undefined;
        this.ReplacecardandPINPage = undefined;
        this.cardHasBeenLostOrStolenLink = undefined;
        this.fingerprintFaceScanLogonSettingsLink = undefined;
        this.securitySettingsPage = undefined;
        this.chooseAutoSignOutSettingLink = undefined;
        this.autoLogoutPage = undefined;
        this.viewYourUserIdLink = undefined;
        this.viewYourCurrentAppVersionLink = undefined;
        this.viewAmendYourSecuritySettingsLink = undefined;
        this.viewYourDeviceTypeLink = undefined;
        this.viewYourDeviceNameLink = undefined;
        this.managePaperFreePreferenceLink = undefined;
    }
}

module.exports = searchLandingScreen;
