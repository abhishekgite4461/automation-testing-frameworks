const Screen = require('../screen');

class EnrolmentSecurityScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.continueButton = undefined;
        this.defaultTime = undefined;
    }
}

module.exports = EnrolmentSecurityScreen;
