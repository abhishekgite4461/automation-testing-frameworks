const Screen = require('../screen');

class RequestPermissionPopUpScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.permissionAcceptButton = undefined;
        this.setPhoneDefaultOption = undefined;
    }
}

module.exports = RequestPermissionPopUpScreen;
