const Screen = require('../screen');

class ExpiredOTPErrorScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.expiredOtpErrorMessage = undefined;
        this.goToLogonButton = undefined;
    }
}

module.exports = ExpiredOTPErrorScreen;
