const Screen = require('../screen');

class DeclinePaymentScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.errorMessage = undefined;
        this.logonButton = undefined;
        this.pageHeader = undefined;
    }
}

module.exports = DeclinePaymentScreen;
