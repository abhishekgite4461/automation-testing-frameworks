const Screen = require('../screen');

class SuccessTransferScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.viewTransactionButton = undefined;
        this.makeAnotherTransferButton = undefined;
        this.fromDetail = undefined;
        this.toDetail = undefined;
        this.amountDetail = undefined;
        this.successMessage = undefined;
        this.notNow = undefined;
        this.fromAccountNameDetail = undefined;
        this.fromSortCodeDetail = undefined;
        this.fromAccountNumberDetail = undefined;
        this.toAccountNameDetail = undefined;
        this.toSortCodeDetail = undefined;
        this.toAccountNumberDetail = undefined;
        this.mpaTransferApprovalSuccessMessage = undefined;
    }
}

module.exports = SuccessTransferScreen;
