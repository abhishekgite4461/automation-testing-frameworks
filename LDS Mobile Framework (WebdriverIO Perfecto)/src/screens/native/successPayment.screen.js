const Screen = require('../screen');

class SuccessPaymentScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.viewTransactionButton = undefined;
        this.makeAnotherPaymentButton = undefined;
        this.fromDetail = undefined;
        this.toDetail = undefined;
        this.amountDetail = undefined;
        this.notNow = undefined;
        this.mpaPaymentApprovalSuccessMessage = undefined;
        this.shareReceiptButton = undefined;
        this.fromBusinessName = undefined;
        this.paymentProcessed = undefined;
        this.bannerLeadPlacement = undefined;
    }
}

module.exports = SuccessPaymentScreen;
