const Screen = require('../screen');

class FscsScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.leaveButton = undefined;
        this.stayButton = undefined;
        this.compensationSchemeLabel = undefined;
    }
}

module.exports = FscsScreen;
