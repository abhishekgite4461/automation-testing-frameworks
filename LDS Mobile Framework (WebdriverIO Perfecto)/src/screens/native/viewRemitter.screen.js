const Screen = require('../screen');

class ViewRemitterScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.titleBNGA = undefined;
        this.accountName = undefined;
        this.accountNameLabel = undefined;
    }
}

module.exports = ViewRemitterScreen;
