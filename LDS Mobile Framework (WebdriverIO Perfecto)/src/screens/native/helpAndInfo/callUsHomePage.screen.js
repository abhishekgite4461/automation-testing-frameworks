const Screen = require('../../screen');

class CallUsHomePageScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.yourAccountsTile = undefined;
        this.personalAccountsTile = undefined;
        this.newProductEnquireTile = undefined;
        this.internetBankingTile = undefined;
        this.otherBankingQueryTile = undefined;
        this.securityTravelLabel = undefined;
        this.suspectedFraudTab = undefined;
        this.lostOrStolenCardTab = undefined;
        this.emergencyCashAbroadTab = undefined;
        this.medicalAssistAbroadTab = undefined;
        this.globalMenuIcon = undefined;
        this.backButton = undefined;
        this.unAuthMenuButton = undefined;
        this.closeButton = undefined;
        this.cmaSurveyLink = undefined;
        this.cmaSurveyResult = undefined;
        this.downloadOption = undefined;
    }
}
module.exports = CallUsHomePageScreen;
