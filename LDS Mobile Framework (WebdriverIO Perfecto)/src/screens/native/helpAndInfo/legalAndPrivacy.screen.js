const Screen = require('../../screen');

class LegalAndPrivacyScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.backButton = undefined;
    }
}

module.exports = LegalAndPrivacyScreen;
