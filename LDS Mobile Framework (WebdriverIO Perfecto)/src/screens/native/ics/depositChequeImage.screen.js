const Screen = require('../../screen');

class DepositChequeImageScreen extends Screen {
    constructor() {
        super();
        this.imageCloseButton = undefined;
    }
}

module.exports = DepositChequeImageScreen;
