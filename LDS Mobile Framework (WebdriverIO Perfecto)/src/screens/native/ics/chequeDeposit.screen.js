const Screen = require('../../screen');

class ChequeDepositScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.chequeAmountField = undefined;
        this.chequeLimitService = undefined;
        this.chequeReferenceField = undefined;
        this.chequeAmountFeildHelpText = undefined;
        this.chequeReferenceFieldHelpText = undefined;
        this.chequeErrorMsg = undefined;
        this.depositAccount = undefined;
        this.captureFrontOfChequeButton = undefined;
        this.captureBackOfChequeButton = undefined;
        this.moreInformationDialogbox = undefined;
        this.moreInformationOkButton = undefined;
        this.depositChequeDisabled = undefined;
        this.depositHistoryText = undefined;
        this.technicalErrorText = undefined;
        this.accountErrorText = undefined;
        this.cameraPopupAlert = undefined;
        this.useChequeImageButton = undefined;
        this.frontOfChequeThumbnail = undefined;
        this.backOfChequeThumbnail = undefined;
        this.reviewDepositButton = undefined;
        this.allowCamera = undefined;
        this.closeCamerabutton = undefined;
        this.selectCheckBox = undefined;
        this.denyPermission = undefined;
        this.settingAlert = undefined;
        this.closeDemobutton = undefined;
        this.settingButton = undefined;
        this.depositHomeBackButton = undefined;
        this.homeAccountActionButton = undefined;
        this.leaveButton = undefined;
        this.stayButton = undefined;
        this.errorMessageOkButton = undefined;
        this.fastChequeIcon = undefined;
        this.currentMainBalance = undefined;
        this.problemScanningAlertPopupMessage = undefined;
        this.navigateSettings = undefined;
        this.navigatePermissionSettings = undefined;
        this.enableCameraPermissions = undefined;
        this.selectBackOnSettings = undefined;
        this.winBackTitle = undefined;
        this.frontOfChequeImageThumbnail = undefined;
        this.backOfChequeImageThumbnail = undefined;
    }
}
module.exports = ChequeDepositScreen;
