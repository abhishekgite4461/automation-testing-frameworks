const Screen = require('../../screen');

class DepositHistoryReviewScreen extends Screen {
    constructor() {
        super();
        this.tapHintFrontImage = undefined;
        this.depositStatus = undefined;
        this.depositAccountType = undefined;
        this.depositedDate = undefined;
        this.depositAmount = undefined;
        this.depositReference = undefined;
        this.pendingDetailPageTitle = undefined;
        this.statusTitleOnReviewPage = undefined;
        this.fundsAvailableDetailPageTitle = undefined;
        this.RejectedChequePageTitle = undefined;
        this.depositHistorybutton = undefined;
        this.depositBusiness = undefined;
    }
}

module.exports = DepositHistoryReviewScreen;
