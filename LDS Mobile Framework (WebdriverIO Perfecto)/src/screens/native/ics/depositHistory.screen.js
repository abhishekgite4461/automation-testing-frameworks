const Screen = require('../../screen');

class DepositHistoryScreen extends Screen {
    constructor() {
        super();
        this.depositHistoryTitle = undefined;
        this.helpandInfoNavigationTile = undefined;
        this.depositChequeText = undefined;
        this.technicalErrorText = undefined;
        this.accountErrorText = undefined;
        this.depositHistoryList = undefined;
        this.depositReference = undefined;
        this.depositStatus = undefined;
        this.depositAmount = undefined;
        this.accountNavigationImage = undefined;
        this.accountList = undefined;
        this.changeAccount = undefined;
        this.noMoretransactionMessage = undefined;
        this.noTransactionMessage = undefined;
        this.accountPicker = undefined;
        this.accountType = undefined;
        this.sortCode = undefined;
        this.accountNumber = undefined;
        this.ledgerBalance = undefined;
        this.availableBalance = undefined;
        this.referenceTextVisibility = undefined;
        this.referenceOnTransactionDetailPage = undefined;
        this.referenceIsNotVisible = undefined;
        this.referenceIsNotVisibleOnDetailPage = undefined;
        this.selectPendingOption = undefined;
        this.selectFundsAvailableOption = undefined;
        this.selectRejectedOption = undefined;
        this.selectReferenceOnTransactionList = undefined;
        this.depositDate = undefined;
    }
}

module.exports = DepositHistoryScreen;
