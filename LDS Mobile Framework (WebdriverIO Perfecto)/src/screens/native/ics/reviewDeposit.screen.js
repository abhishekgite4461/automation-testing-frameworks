const Screen = require('../../screen');

class ReviewDepositScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.reviewDepositConfirmButton = undefined;
        this.reviewBusinessName = undefined;
        this.reviewDepositAccount = undefined;
        this.reviewChequeAmount = undefined;
        this.reviewReferenceText = undefined;
        this.reviewFrontofCheque = undefined;
        this.reviewBackofCheque = undefined;
        this.reviewCancelButton = undefined;
        this.frontOfChequeHint = undefined;
        this.BackOfChequeHint = undefined;
        this.depositHistorybutton = undefined;
        this.passwordConfirmationText = undefined;
        this.passwordConfirmButton = undefined;
        this.amountMismatchConfirm = undefined;
        this.amountMismatchCancel = undefined;
        this.amountConfirmAlert = undefined;
    }
}

module.exports = ReviewDepositScreen;
