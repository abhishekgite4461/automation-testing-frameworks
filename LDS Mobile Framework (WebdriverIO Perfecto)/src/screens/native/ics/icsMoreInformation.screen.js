const Screen = require('../../screen');

class IcsMoreInformation extends Screen {
    constructor() {
        super();
        this.moreInformationOkButton = undefined;
        this.moreInformationDialogbox = undefined;
        this.moreInformationCheckbox = undefined;
        this.moreInformationText = undefined;
        this.moreInformationContent = undefined;
    }
}

module.exports = IcsMoreInformation;
