const Screen = require('../../screen');

class ChequeDepositStatusScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.fastChequeIcon = undefined;
        this.viewDepositHistoryButton = undefined;
        this.sortCodeError = undefined;
        this.duplicateChequeMessage = undefined;
        this.dailyLimitError = undefined;
        this.depositAnotherCheque = undefined;
        this.amountDeposited = undefined;
        this.successBusinessName = undefined;
        this.successSortCode = undefined;
        this.successDepositAccount = undefined;
        this.successReferenceText = undefined;
        this.dueDate = undefined;
    }
}

module.exports = ChequeDepositStatusScreen;
