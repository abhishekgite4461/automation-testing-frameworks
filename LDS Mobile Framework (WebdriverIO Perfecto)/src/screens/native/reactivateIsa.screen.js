const Screen = require('../screen');

class ReactivateIsaScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.reactivateIsaHeader = undefined;
        this.reactivateIsaInformation = undefined;
        this.reactivateIsaAccountNameText = undefined;
        this.reactivateIsaAccountSortCode = undefined;
        this.reactivateIsaAccountNumber = undefined;
        this.reactivateIsaAccountHolderName = undefined;
        this.reactivateIsaNationalInsuranceNo = undefined;
        this.reactivateIsaAccountHolderAddress = undefined;
        this.reactivateIsaAccountHolderDateOfBirth = undefined;
        this.reactivateIsaUpdateAddressLinkText = undefined;
        this.reactivateIsaUpdateNiNumberLinkText = undefined;
        this.reactivateIsaConfirmButton = undefined;
        this.reactivateIsaCancelButton = undefined;
        this.reactivateAgreementCheckBox = undefined;
        this.reactivateIsaEligibilityCriteriaWebView = undefined;
        this.reactivateIsaUpdateNiModal = undefined;
        this.reactivateCloseButton = undefined;
        this.leaveThisPageDialogBox = undefined;
        this.addUkAccountWinbackModal = undefined;
        this.dialogPositiveAction = undefined;
        this.dialogNegativeAction = undefined;
        this.settingsPersonalDetailsAddressTitle = undefined;
        this.settingsPersonalDetailsAddressLineOne = undefined;
        this.settingsPersonalDetailsPostcode = undefined;
        this.settingsPersonalDetailsChangeAddressDescription = undefined;
        this.settingsPersonalDetailsChangeAddressButton = undefined;
        this.passwordConfirmationDialogPanel = undefined;
        this.passwordConfirmationDialogPasswordBox = undefined;
        this.passwordConfirmationDialogForgotPasswordLabel = undefined;
    }
}

module.exports = ReactivateIsaScreen;
