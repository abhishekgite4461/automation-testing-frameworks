const Screen = require('../../screen');

class OneLastThingScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.oneLastThingSubtitleText = undefined;
        this.oneLastThingText = undefined;
        this.oneLastThingSubtitles = undefined;
        this.oneLastThingTexts = undefined;
        this.iUnderstandButton = undefined;
        this.enterPasswordDialogTitle = undefined;
        this.dialogPasswordField = undefined;
        this.dialogForgottenPassword = undefined;
        this.dialogCancelButton = undefined;
        this.dialogOkButton = undefined;
    }
}

module.exports = OneLastThingScreen;
