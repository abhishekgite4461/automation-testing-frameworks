const Screen = require('../../screen');

class ConfirmAddressScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.mapView = undefined;
        this.mapPlaceholder = undefined;
        this.newAddress = undefined;
        this.confirmAddressButton = undefined;
    }
}

module.exports = ConfirmAddressScreen;
