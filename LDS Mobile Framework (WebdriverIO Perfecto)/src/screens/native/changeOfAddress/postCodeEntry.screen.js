const Screen = require('../../screen');

class PostCodeEntryScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.subtitle = undefined;
        this.postCode = undefined;
        this.postCodeTipView = undefined;
        this.errorIcon = undefined;
        this.enterPostcodeHelpText = undefined;
        this.enterPostcodeCallUsText = undefined;
        this.backArrow = undefined;
        this.findAddress = undefined;
        this.keyboardDeleteButton = undefined;
        this.webviewChangeAddress = undefined;
    }
}

module.exports = PostCodeEntryScreen;
