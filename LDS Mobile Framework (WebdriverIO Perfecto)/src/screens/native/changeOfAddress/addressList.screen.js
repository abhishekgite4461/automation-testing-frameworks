const Screen = require('../../screen');

class AddressListScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.addressListText = undefined;
        this.addressNotOnListButton = undefined;
        this.addressToBeSelected = undefined;
        this.addressToBeSelectedUAT = undefined;
        this.addressList = undefined;
        this.addressListItem = undefined;
    }
}

module.exports = AddressListScreen;
