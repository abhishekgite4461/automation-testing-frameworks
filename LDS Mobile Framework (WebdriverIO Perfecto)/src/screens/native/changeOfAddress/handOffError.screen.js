const Screen = require('../../screen');

class HandOffErrorScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.handOffErrorText = undefined;
        this.callUsButton = undefined;
        this.backToPersonalDetailsButton = undefined;
        this.forcedLogoffTitle = undefined;
        this.forcedLogoffMessage = undefined;
        this.forcedLogoffLogoonButton = undefined;
    }
}

module.exports = HandOffErrorScreen;
