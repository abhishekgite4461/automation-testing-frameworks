const Screen = require('../../screen');

class AddressChangedScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.titleIcon = undefined;
        this.addressChangeSuccessMessage = undefined;
        this.backToHomeButton = undefined;
        this.homeInsuranceLeadTitle = undefined;
        this.homeInsuranceLeadIcon = undefined;
        this.homeInsuranceLeadButton = undefined;
    }
}

module.exports = AddressChangedScreen;
