const Screen = require('../screen');

class ViewPendingPaymentScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.swipeToClose = undefined;
    }
}

module.exports = ViewPendingPaymentScreen;
