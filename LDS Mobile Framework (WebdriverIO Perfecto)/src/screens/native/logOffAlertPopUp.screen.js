const Screen = require('../screen');

class LogOffAlertPopUpScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.logOffButton = undefined;
        this.continueButton = undefined;
    }
}

module.exports = LogOffAlertPopUpScreen;
