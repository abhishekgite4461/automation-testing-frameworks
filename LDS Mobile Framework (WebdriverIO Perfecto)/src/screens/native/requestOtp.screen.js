const Screen = require('../screen');

class RequestOtpScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.confirmButton = undefined;
        this.cancelButton = undefined;
    }
}

module.exports = RequestOtpScreen;
