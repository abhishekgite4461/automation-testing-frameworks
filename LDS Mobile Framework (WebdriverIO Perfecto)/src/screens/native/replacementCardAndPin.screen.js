const Screen = require('../screen');

class ReplacementCardAndPin extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.backButton = undefined;
        this.replaceCard = undefined;
        this.pin = undefined;
        this.replaceCardCredit = undefined;
        this.pinCredit = undefined;
        this.continue = undefined;
        this.confirmTitle = undefined;
        this.password = undefined;
        this.continue = undefined;
    }
}

module.exports = ReplacementCardAndPin;
