const Screen = require('../screen');

class AppFeedbackScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.feedbackForm = undefined;
        this.submitButton = undefined;
    }
}

module.exports = AppFeedbackScreen;
