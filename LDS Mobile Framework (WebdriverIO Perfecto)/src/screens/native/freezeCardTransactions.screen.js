const Screen = require('../screen');

class FreezeCardTransactions extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.abroad = undefined;
        this.online = undefined;
        this.terminals = undefined;
        this.reportLostAndStolen = undefined;
        this.freezeGambling = undefined;
        this.waitForReadyToFreezePage = undefined;
        this.freezeButtonGambling = undefined;
        this.cancelFreezeButton = undefined;
    }
}

module.exports = FreezeCardTransactions;
