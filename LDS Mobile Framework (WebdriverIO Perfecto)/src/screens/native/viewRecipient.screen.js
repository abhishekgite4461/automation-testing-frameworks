const Screen = require('../screen');

class ViewRecipientScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.pendingPayment = undefined;
        this.paymentAccountLocator = undefined;
        this.accountNameLabel = undefined;
        this.accountLabel = undefined;
        this.firstRecipientTransferCell = undefined;
        this.internationalPayment = undefined;
        this.internationalPaymentWithField = undefined;
        this.externalBeneficiaryAccounts = undefined;
        this.myAccounts = undefined;
        this.zeroRemainingAllowance = undefined;
        this.manageActionMenu = undefined;
        this.manageButtonPendingPayment = undefined;
        this.recipientList = undefined;
        this.manageButtonUKAccountNumber = undefined;
        this.manageButtonMobileNumber = undefined;
        this.manageButtonRecipientName = undefined;
    }
}

module.exports = ViewRecipientScreen;
