const Screen = require('../screen');

class BaseAlertPopUpScreen extends Screen {
    constructor() {
        super();
        this.title = undefined;
        this.confirmButton = undefined;
        this.cancelButton = undefined;
        this.leaveButton = undefined;
        this.stayButton = undefined;
    }
}

module.exports = BaseAlertPopUpScreen;
