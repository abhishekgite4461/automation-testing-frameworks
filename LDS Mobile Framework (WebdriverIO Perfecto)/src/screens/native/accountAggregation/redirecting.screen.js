const Screen = require('../../screen');

class RedirectingScreen extends Screen {
    constructor() {
        super();
        this.redirectLoadingProgress = undefined;
        this.redirectText = undefined;
        this.continueButton = undefined;
        this.cancelButton = undefined;
        this.loggingOffUserErrorScreen = undefined;
        this.logOffErrorScreenTitle = undefined;
        this.redirectExternalBrowserErrorScreen = undefined;
        this.redirectExternalBrowserErrorScreenTitle = undefined;
        this.openAppButton = undefined;
        this.browserAddress = undefined;
        this.redirectHomeButton = undefined;
        this.accessLocationButton = undefined;
        this.backToAppButtonOld = undefined;
        this.backToAppButtonNew = undefined;
        this.digitalInbox = undefined;
    }
}

module.exports = RedirectingScreen;
