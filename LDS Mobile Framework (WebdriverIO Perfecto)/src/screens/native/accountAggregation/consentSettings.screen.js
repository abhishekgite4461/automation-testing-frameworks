const Screen = require('../../screen');

class ConsentSettingsScreen extends Screen {
    constructor() {
        super();
        this.consentSettingsPage = undefined;
        this.mainHeaderTitle = undefined;
        this.introductoryPara = undefined;
        this.accountsSectionHeader = undefined;
        this.consentProviderLabel = undefined;
        this.consentExpiryMessage = undefined;
        this.providerManageButton = undefined;
        this.authorisedAccounts = undefined;
        this.consentStatusFailedMessage = undefined;
        this.consentStatusExpiredMessage = undefined;
        this.consentStatusRevokedMessage = undefined;
        this.providerManageDrawer = undefined;
        this.providerManageDrawerHeader = undefined;
        this.providerManageDrawerList = undefined;
        this.providerManageDrawerOption = undefined;
        this.removeConsentAlert = undefined;
        this.removeConsentAlertHeader = undefined;
        this.consentSettingErrorBanner = undefined;
        this.consentSettingErrorBannerCloseIcon = undefined;
    }
}

module.exports = ConsentSettingsScreen;
