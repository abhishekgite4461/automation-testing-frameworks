const Screen = require('../../screen');

class AccountProviderScreen extends Screen {
    constructor() {
        super();
        this.accountProviderTitle = undefined;
        this.accountProviderListView = undefined;
        this.accountProviderTextView = undefined;
        this.accountProviderImageView = undefined;
        this.accountProviderFooterTitle = undefined;
        this.accountProviderFooterMessage = undefined;
        this.providerBankName = undefined;
    }
}

module.exports = AccountProviderScreen;
