const Screen = require('../../screen');

class SecureAndProtectedScreen extends Screen {
    constructor() {
        super();
        this.secureAndProtectedTitle = undefined;
        this.secureAndProtectedFirstInformationPoint = undefined;
        this.secureAndProtectedSecondInformationPoint = undefined;
        this.secureAndProtectedThirdInformationPoint = undefined;
        this.secureAndProtectedNextButton = undefined;
    }
}

module.exports = SecureAndProtectedScreen;
