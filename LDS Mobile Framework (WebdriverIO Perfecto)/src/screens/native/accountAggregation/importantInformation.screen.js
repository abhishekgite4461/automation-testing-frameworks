const Screen = require('../../screen');

class ImportantInformationScreen extends Screen {
    constructor() {
        super();
        this.importantInformationBankImage = undefined;
        this.importantInformationGraphicImage = undefined;
        this.importantInformationSelectedBankImage = undefined;
        this.importantInformationText = undefined;
        this.importantInformationFirstBulletDescription = undefined;
        this.importantInformationSecondBulletDescription = undefined;
        this.importantInformationThirdBulletDescription = undefined;
        this.importantInformationForthBulletDescription = undefined;
        this.importantInformationTnC = undefined;
        this.importantInformationTnCDesciption = undefined;
        this.importantInformationConsent = undefined;
        this.importantInformationConsentDescription = undefined;
        this.importantInformationConfirmButton = undefined;
        this.importantInformationRenewConsent = undefined;
    }
}

module.exports = ImportantInformationScreen;
