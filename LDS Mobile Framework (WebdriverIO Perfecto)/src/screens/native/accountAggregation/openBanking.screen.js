const Screen = require('../../screen');

class OpenBankingScreen extends Screen {
    constructor() {
        super();
        this.screenTitle = undefined;
        this.openBankingInformationHeader = undefined;
        this.openBankingInformationHeaderDescription = undefined;
        this.openBankingImage = undefined;
        this.openBankingNextButton = undefined;
        this.backIcon = undefined;
        this.crossIcon = undefined;
    }
}

module.exports = OpenBankingScreen;
