const Screen = require('../../screen');

class SetUpIn3StepsScreen extends Screen {
    constructor() {
        super();
        this.setUpIn3StepsTitle = undefined;
        this.setUpIn3StepsNextButton = undefined;
        this.setUpIn3StepsProviderImage = undefined;
        this.setUpIn3StepsProviderDescription = undefined;
        this.setUpIn3StepsLoginProviderImage = undefined;
        this.setUpIn3StepsLoginProviderDescription = undefined;
        this.setUpIn3StepsProviderPermissionImage = undefined;
        this.setUpIn3StepsProviderPermissionDescription = undefined;
    }
}

module.exports = SetUpIn3StepsScreen;
