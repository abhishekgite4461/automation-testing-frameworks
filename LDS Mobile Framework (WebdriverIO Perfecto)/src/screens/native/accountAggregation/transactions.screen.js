const Screen = require('../../screen');

class TransactionsScreen extends Screen {
    constructor() {
        super();
        this.viewTransactionsPage = undefined;
        this.successDeepLink = undefined;
        this.failureDeepLink = undefined;
        this.loggedOffError = undefined;
    }
}

module.exports = TransactionsScreen;
