const moment = require('moment');
const {createLogger, format, transports} = require('winston');

const {combine} = format;

const {pid} = process;

const logger = createLogger({
    level: 'info',
    silent: (process.env.NODE_ENV === 'test'),
    format: combine(
        format((info) => {
            info.level = info.level.toUpperCase();
            return info;
        })(),
        format.colorize(),
        format.printf(
            (info) => `${pid} ${moment().utc()} ${info.level} ${info.message}`
        )
    ),
    transports: [new transports.Console()]
});

// Format errors nicely if they are the second argument, eg. you can now do:
// logger.error('Oh No!  A wild error appeared', new Error('foo'));
// Without this winston will strip the error out of the log message.
/* eslint prefer-rest-params: "off" */
const original = logger.log;
logger.log = function wrap() {
    const args = Array.prototype.slice.call(arguments, 0);
    if (args.length >= 3 && args[2] instanceof Error) {
        args[2] = args[2].stack;
    }
    return original.apply(this, args);
};

logger.info('Initialized logger', {pid, channel: process.channel !== undefined, argv1: process.argv[1]});

process.on('unhandledRejection', (error) => logger.error(`UNHANDLED PROMISE REJECTION! ---> ${error}`));

module.exports = logger;
