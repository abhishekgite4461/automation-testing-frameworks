const logger = require('./logger');

module.exports = class AppInstaller {
    constructor(
        deviceState,
        installFunction,
        unInstallFunction,
        sleepTime = 3000
    ) {
        this.deviceState = deviceState;
        this.install = installFunction;
        this.unInstall = unInstallFunction;
        this.sleepTime = sleepTime;
    }

    installApp(instrumention, sensorInstrumention, forceInstall = false) {
        // MOB3-12460 App installation is slower with instrumentation enabled, its takes between 2-15 mins on iOS
        // Hence App isn't installed with instrumentation by default.
        const instrument = instrumention;
        const sensorInstrument = sensorInstrumention;
        const shouldInstall = forceInstall || this.deviceState.isAppInstallationRequired(instrument, sensorInstrument);
        if (shouldInstall) {
            logger.info(`Installing App with instrument:${instrument} and sensorInstrument:${sensorInstrument}`);
            this.deviceState.setAppInstalled(false, false, false);
            this.unInstall();
            browser.pause(this.sleepTime);
            logger.info('Uninstallation has completed. Attempting to install.');
            for (let i = 0; i < 4; i++) {
                try {
                    logger.info(`Install: Attempt number ${i + 1}`);
                    this.install(instrument, sensorInstrument);
                    break;
                } catch (e) {
                    logger.info('Install: failed with error: ', e);
                    logger.info(`Install: retry after ${i + 1} minute.`);
                    if (i === 3) {
                        throw e;
                    }
                    browser.pause(60000 * (i + 1));
                }
            }
            logger.info('Install completed without error.');
            this.deviceState.setAppInstalled(true, instrument, sensorInstrument);
            this.deviceState.setEnrolledUser(null);
        }
        return shouldInstall;
    }

    forceInstallApp(instrumention, sensorInstrumention) {
        return this.installApp(instrumention, sensorInstrumention, true);
    }
};
